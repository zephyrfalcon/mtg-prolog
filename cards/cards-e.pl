% Card-specific data

% Found in: 7ED, 8ED, 9ED, S00, S99
card_name('eager cadet', 'Eager Cadet').
card_type('eager cadet', 'Creature — Human Soldier').
card_types('eager cadet', ['Creature']).
card_subtypes('eager cadet', ['Human', 'Soldier']).
card_colors('eager cadet', ['W']).
card_text('eager cadet', '').
card_mana_cost('eager cadet', ['W']).
card_cmc('eager cadet', 1).
card_layout('eager cadet', 'normal').
card_power('eager cadet', 1).
card_toughness('eager cadet', 1).

% Found in: JOU, ORI
card_name('eagle of the watch', 'Eagle of the Watch').
card_type('eagle of the watch', 'Creature — Bird').
card_types('eagle of the watch', ['Creature']).
card_subtypes('eagle of the watch', ['Bird']).
card_colors('eagle of the watch', ['W']).
card_text('eagle of the watch', 'Flying, vigilance').
card_mana_cost('eagle of the watch', ['2', 'W']).
card_cmc('eagle of the watch', 3).
card_layout('eagle of the watch', 'normal').
card_power('eagle of the watch', 2).
card_toughness('eagle of the watch', 1).

% Found in: 5DN
card_name('early frost', 'Early Frost').
card_type('early frost', 'Instant').
card_types('early frost', ['Instant']).
card_subtypes('early frost', []).
card_colors('early frost', ['U']).
card_text('early frost', 'Tap up to three target lands.').
card_mana_cost('early frost', ['1', 'U']).
card_cmc('early frost', 2).
card_layout('early frost', 'normal').

% Found in: 6ED, 7ED, 9ED, MIR
card_name('early harvest', 'Early Harvest').
card_type('early harvest', 'Instant').
card_types('early harvest', ['Instant']).
card_subtypes('early harvest', []).
card_colors('early harvest', ['G']).
card_text('early harvest', 'Target player untaps all basic lands he or she controls.').
card_mana_cost('early harvest', ['1', 'G', 'G']).
card_cmc('early harvest', 3).
card_layout('early harvest', 'normal').

% Found in: ODY
card_name('earnest fellowship', 'Earnest Fellowship').
card_type('earnest fellowship', 'Enchantment').
card_types('earnest fellowship', ['Enchantment']).
card_subtypes('earnest fellowship', []).
card_colors('earnest fellowship', ['W']).
card_text('earnest fellowship', 'Each creature has protection from its colors.').
card_mana_cost('earnest fellowship', ['1', 'W']).
card_cmc('earnest fellowship', 2).
card_layout('earnest fellowship', 'normal').

% Found in: JUD
card_name('earsplitting rats', 'Earsplitting Rats').
card_type('earsplitting rats', 'Creature — Rat').
card_types('earsplitting rats', ['Creature']).
card_subtypes('earsplitting rats', ['Rat']).
card_colors('earsplitting rats', ['B']).
card_text('earsplitting rats', 'When Earsplitting Rats enters the battlefield, each player discards a card.\nDiscard a card: Regenerate Earsplitting Rats.').
card_mana_cost('earsplitting rats', ['3', 'B']).
card_cmc('earsplitting rats', 4).
card_layout('earsplitting rats', 'normal').
card_power('earsplitting rats', 2).
card_toughness('earsplitting rats', 1).

% Found in: 10E, 2ED, 3ED, 4ED, CED, CEI, DPA, LEA, LEB, S99
card_name('earth elemental', 'Earth Elemental').
card_type('earth elemental', 'Creature — Elemental').
card_types('earth elemental', ['Creature']).
card_subtypes('earth elemental', ['Elemental']).
card_colors('earth elemental', ['R']).
card_text('earth elemental', '').
card_mana_cost('earth elemental', ['3', 'R', 'R']).
card_cmc('earth elemental', 5).
card_layout('earth elemental', 'normal').
card_power('earth elemental', 4).
card_toughness('earth elemental', 5).

% Found in: ODY
card_name('earth rift', 'Earth Rift').
card_type('earth rift', 'Sorcery').
card_types('earth rift', ['Sorcery']).
card_subtypes('earth rift', []).
card_colors('earth rift', ['R']).
card_text('earth rift', 'Destroy target land.\nFlashback {5}{R}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('earth rift', ['3', 'R']).
card_cmc('earth rift', 4).
card_layout('earth rift', 'normal').

% Found in: DDI, M11
card_name('earth servant', 'Earth Servant').
card_type('earth servant', 'Creature — Elemental').
card_types('earth servant', ['Creature']).
card_subtypes('earth servant', ['Elemental']).
card_colors('earth servant', ['R']).
card_text('earth servant', 'Earth Servant gets +0/+1 for each Mountain you control.').
card_mana_cost('earth servant', ['5', 'R']).
card_cmc('earth servant', 6).
card_layout('earth servant', 'normal').
card_power('earth servant', 4).
card_toughness('earth servant', 4).

% Found in: GPT
card_name('earth surge', 'Earth Surge').
card_type('earth surge', 'Enchantment').
card_types('earth surge', ['Enchantment']).
card_subtypes('earth surge', []).
card_colors('earth surge', ['G']).
card_text('earth surge', 'Each land gets +2/+2 as long as it\'s a creature.').
card_mana_cost('earth surge', ['3', 'G']).
card_cmc('earth surge', 4).
card_layout('earth surge', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB
card_name('earthbind', 'Earthbind').
card_type('earthbind', 'Enchantment — Aura').
card_types('earthbind', ['Enchantment']).
card_subtypes('earthbind', ['Aura']).
card_colors('earthbind', ['R']).
card_text('earthbind', 'Enchant creature\nWhen Earthbind enters the battlefield, if enchanted creature has flying, Earthbind deals 2 damage to that creature and Earthbind gains \"Enchanted creature loses flying.\"').
card_mana_cost('earthbind', ['R']).
card_cmc('earthbind', 1).
card_layout('earthbind', 'normal').

% Found in: LGN
card_name('earthblighter', 'Earthblighter').
card_type('earthblighter', 'Creature — Human Cleric').
card_types('earthblighter', ['Creature']).
card_subtypes('earthblighter', ['Human', 'Cleric']).
card_colors('earthblighter', ['B']).
card_text('earthblighter', '{2}{B}, {T}, Sacrifice a Goblin: Destroy target land.').
card_mana_cost('earthblighter', ['1', 'B']).
card_cmc('earthblighter', 2).
card_layout('earthblighter', 'normal').
card_power('earthblighter', 1).
card_toughness('earthblighter', 1).

% Found in: MOR
card_name('earthbrawn', 'Earthbrawn').
card_type('earthbrawn', 'Instant').
card_types('earthbrawn', ['Instant']).
card_subtypes('earthbrawn', []).
card_colors('earthbrawn', ['G']).
card_text('earthbrawn', 'Target creature gets +3/+3 until end of turn.\nReinforce 1—{1}{G} ({1}{G}, Discard this card: Put a +1/+1 counter on target creature.)').
card_mana_cost('earthbrawn', ['1', 'G']).
card_cmc('earthbrawn', 2).
card_layout('earthbrawn', 'normal').

% Found in: TMP
card_name('earthcraft', 'Earthcraft').
card_type('earthcraft', 'Enchantment').
card_types('earthcraft', ['Enchantment']).
card_subtypes('earthcraft', []).
card_colors('earthcraft', ['G']).
card_text('earthcraft', 'Tap an untapped creature you control: Untap target basic land.').
card_mana_cost('earthcraft', ['1', 'G']).
card_cmc('earthcraft', 2).
card_layout('earthcraft', 'normal').
card_reserved('earthcraft').

% Found in: BFZ
card_name('earthen arms', 'Earthen Arms').
card_type('earthen arms', 'Sorcery').
card_types('earthen arms', ['Sorcery']).
card_subtypes('earthen arms', []).
card_colors('earthen arms', ['G']).
card_text('earthen arms', 'Put two +1/+1 counters on target permanent.\nAwaken 4—{6}{G} (If you cast this spell for {6}{G}, also put four +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_mana_cost('earthen arms', ['1', 'G']).
card_cmc('earthen arms', 2).
card_layout('earthen arms', 'normal').

% Found in: CSP
card_name('earthen goo', 'Earthen Goo').
card_type('earthen goo', 'Creature — Ooze').
card_types('earthen goo', ['Creature']).
card_subtypes('earthen goo', ['Ooze']).
card_colors('earthen goo', ['R']).
card_text('earthen goo', 'Trample\nCumulative upkeep {R} or {G} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nEarthen Goo gets +1/+1 for each age counter on it.').
card_mana_cost('earthen goo', ['2', 'R']).
card_cmc('earthen goo', 3).
card_layout('earthen goo', 'normal').
card_power('earthen goo', 2).
card_toughness('earthen goo', 2).

% Found in: ICE, ME2
card_name('earthlink', 'Earthlink').
card_type('earthlink', 'Enchantment').
card_types('earthlink', ['Enchantment']).
card_subtypes('earthlink', []).
card_colors('earthlink', ['B', 'R', 'G']).
card_text('earthlink', 'At the beginning of your upkeep, sacrifice Earthlink unless you pay {2}.\nWhenever a creature dies, that creature\'s controller sacrifices a land.').
card_mana_cost('earthlink', ['3', 'B', 'R', 'G']).
card_cmc('earthlink', 6).
card_layout('earthlink', 'normal').
card_reserved('earthlink').

% Found in: ICE
card_name('earthlore', 'Earthlore').
card_type('earthlore', 'Enchantment — Aura').
card_types('earthlore', ['Enchantment']).
card_subtypes('earthlore', ['Aura']).
card_colors('earthlore', ['G']).
card_text('earthlore', 'Enchant land you control\nTap enchanted land: Target blocking creature gets +1/+2 until end of turn. Activate this ability only if enchanted land is untapped.').
card_mana_cost('earthlore', ['G']).
card_cmc('earthlore', 1).
card_layout('earthlore', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, CED, CEI, CMD, LEA, LEB, M10, PO2, POR
card_name('earthquake', 'Earthquake').
card_type('earthquake', 'Sorcery').
card_types('earthquake', ['Sorcery']).
card_subtypes('earthquake', []).
card_colors('earthquake', ['R']).
card_text('earthquake', 'Earthquake deals X damage to each creature without flying and each player.').
card_mana_cost('earthquake', ['X', 'R']).
card_cmc('earthquake', 1).
card_layout('earthquake', 'normal').

% Found in: CHK
card_name('earthshaker', 'Earthshaker').
card_type('earthshaker', 'Creature — Spirit').
card_types('earthshaker', ['Creature']).
card_subtypes('earthshaker', ['Spirit']).
card_colors('earthshaker', ['R']).
card_text('earthshaker', 'Whenever you cast a Spirit or Arcane spell, Earthshaker deals 2 damage to each creature without flying.').
card_mana_cost('earthshaker', ['4', 'R', 'R']).
card_cmc('earthshaker', 6).
card_layout('earthshaker', 'normal').
card_power('earthshaker', 4).
card_toughness('earthshaker', 5).

% Found in: MMA, MOR, pLPA
card_name('earwig squad', 'Earwig Squad').
card_type('earwig squad', 'Creature — Goblin Rogue').
card_types('earwig squad', ['Creature']).
card_subtypes('earwig squad', ['Goblin', 'Rogue']).
card_colors('earwig squad', ['B']).
card_text('earwig squad', 'Prowl {2}{B} (You may cast this for its prowl cost if you dealt combat damage to a player this turn with a Goblin or Rogue.)\nWhen Earwig Squad enters the battlefield, if its prowl cost was paid, search target opponent\'s library for three cards and exile them. Then that player shuffles his or her library.').
card_mana_cost('earwig squad', ['3', 'B', 'B']).
card_cmc('earwig squad', 5).
card_layout('earwig squad', 'normal').
card_power('earwig squad', 5).
card_toughness('earwig squad', 3).

% Found in: 7ED, 8ED, USG
card_name('eastern paladin', 'Eastern Paladin').
card_type('eastern paladin', 'Creature — Zombie Knight').
card_types('eastern paladin', ['Creature']).
card_subtypes('eastern paladin', ['Zombie', 'Knight']).
card_colors('eastern paladin', ['B']).
card_text('eastern paladin', '{B}{B}, {T}: Destroy target green creature.').
card_mana_cost('eastern paladin', ['2', 'B', 'B']).
card_cmc('eastern paladin', 4).
card_layout('eastern paladin', 'normal').
card_power('eastern paladin', 3).
card_toughness('eastern paladin', 3).

% Found in: AVR
card_name('eaten by spiders', 'Eaten by Spiders').
card_type('eaten by spiders', 'Instant').
card_types('eaten by spiders', ['Instant']).
card_subtypes('eaten by spiders', []).
card_colors('eaten by spiders', ['G']).
card_text('eaten by spiders', 'Destroy target creature with flying and all Equipment attached to that creature.').
card_mana_cost('eaten by spiders', ['2', 'G']).
card_cmc('eaten by spiders', 3).
card_layout('eaten by spiders', 'normal').

% Found in: DST
card_name('eater of days', 'Eater of Days').
card_type('eater of days', 'Artifact Creature — Leviathan').
card_types('eater of days', ['Artifact', 'Creature']).
card_subtypes('eater of days', ['Leviathan']).
card_colors('eater of days', []).
card_text('eater of days', 'Flying, trample\nWhen Eater of Days enters the battlefield, you skip your next two turns.').
card_mana_cost('eater of days', ['4']).
card_cmc('eater of days', 4).
card_layout('eater of days', 'normal').
card_power('eater of days', 9).
card_toughness('eater of days', 8).

% Found in: BNG, pPRE
card_name('eater of hope', 'Eater of Hope').
card_type('eater of hope', 'Creature — Demon').
card_types('eater of hope', ['Creature']).
card_subtypes('eater of hope', ['Demon']).
card_colors('eater of hope', ['B']).
card_text('eater of hope', 'Flying\n{B}, Sacrifice another creature: Regenerate Eater of Hope.\n{2}{B}, Sacrifice two other creatures: Destroy target creature.').
card_mana_cost('eater of hope', ['5', 'B', 'B']).
card_cmc('eater of hope', 7).
card_layout('eater of hope', 'normal').
card_power('eater of hope', 6).
card_toughness('eater of hope', 4).

% Found in: DRK, MED
card_name('eater of the dead', 'Eater of the Dead').
card_type('eater of the dead', 'Creature — Horror').
card_types('eater of the dead', ['Creature']).
card_subtypes('eater of the dead', ['Horror']).
card_colors('eater of the dead', ['B']).
card_text('eater of the dead', '{0}: If Eater of the Dead is tapped, exile target creature card from a graveyard and untap Eater of the Dead.').
card_mana_cost('eater of the dead', ['4', 'B']).
card_cmc('eater of the dead', 5).
card_layout('eater of the dead', 'normal').
card_power('eater of the dead', 3).
card_toughness('eater of the dead', 4).

% Found in: DRB, ME4, POR
card_name('ebon dragon', 'Ebon Dragon').
card_type('ebon dragon', 'Creature — Dragon').
card_types('ebon dragon', ['Creature']).
card_subtypes('ebon dragon', ['Dragon']).
card_colors('ebon dragon', ['B']).
card_text('ebon dragon', 'Flying\nWhen Ebon Dragon enters the battlefield, you may have target opponent discard a card.').
card_mana_cost('ebon dragon', ['5', 'B', 'B']).
card_cmc('ebon dragon', 7).
card_layout('ebon dragon', 'normal').
card_power('ebon dragon', 5).
card_toughness('ebon dragon', 4).

% Found in: 5DN
card_name('ebon drake', 'Ebon Drake').
card_type('ebon drake', 'Creature — Drake').
card_types('ebon drake', ['Creature']).
card_subtypes('ebon drake', ['Drake']).
card_colors('ebon drake', ['B']).
card_text('ebon drake', 'Flying\nWhenever a player casts a spell, you lose 1 life.').
card_mana_cost('ebon drake', ['2', 'B']).
card_cmc('ebon drake', 3).
card_layout('ebon drake', 'normal').
card_power('ebon drake', 3).
card_toughness('ebon drake', 3).

% Found in: FEM, ME2
card_name('ebon praetor', 'Ebon Praetor').
card_type('ebon praetor', 'Creature — Avatar Praetor').
card_types('ebon praetor', ['Creature']).
card_subtypes('ebon praetor', ['Avatar', 'Praetor']).
card_colors('ebon praetor', ['B']).
card_text('ebon praetor', 'First strike, trample\nAt the beginning of your upkeep, put a -2/-2 counter on Ebon Praetor.\nSacrifice a creature: Remove a -2/-2 counter from Ebon Praetor. If the sacrificed creature was a Thrull, put a +1/+0 counter on Ebon Praetor. Activate this ability only during your upkeep and only once each turn.').
card_mana_cost('ebon praetor', ['4', 'B', 'B']).
card_cmc('ebon praetor', 6).
card_layout('ebon praetor', 'normal').
card_power('ebon praetor', 5).
card_toughness('ebon praetor', 5).
card_reserved('ebon praetor').

% Found in: 5ED, 6ED, BTD, FEM, ME2, PD3
card_name('ebon stronghold', 'Ebon Stronghold').
card_type('ebon stronghold', 'Land').
card_types('ebon stronghold', ['Land']).
card_subtypes('ebon stronghold', []).
card_colors('ebon stronghold', []).
card_text('ebon stronghold', 'Ebon Stronghold enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\n{T}, Sacrifice Ebon Stronghold: Add {B}{B} to your mana pool.').
card_layout('ebon stronghold', 'normal').

% Found in: ONS
card_name('ebonblade reaper', 'Ebonblade Reaper').
card_type('ebonblade reaper', 'Creature — Human Cleric').
card_types('ebonblade reaper', ['Creature']).
card_subtypes('ebonblade reaper', ['Human', 'Cleric']).
card_colors('ebonblade reaper', ['B']).
card_text('ebonblade reaper', 'Whenever Ebonblade Reaper attacks, you lose half your life, rounded up.\nWhenever Ebonblade Reaper deals combat damage to a player, that player loses half his or her life, rounded up.\nMorph {3}{B}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('ebonblade reaper', ['2', 'B']).
card_cmc('ebonblade reaper', 3).
card_layout('ebonblade reaper', 'normal').
card_power('ebonblade reaper', 1).
card_toughness('ebonblade reaper', 1).

% Found in: MIR
card_name('ebony charm', 'Ebony Charm').
card_type('ebony charm', 'Instant').
card_types('ebony charm', ['Instant']).
card_subtypes('ebony charm', []).
card_colors('ebony charm', ['B']).
card_text('ebony charm', 'Choose one —\n• Target opponent loses 1 life and you gain 1 life.\n• Exile up to three target cards from a single graveyard.\n• Target creature gains fear until end of turn. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('ebony charm', ['B']).
card_cmc('ebony charm', 1).
card_layout('ebony charm', 'normal').

% Found in: 3ED, 4ED, ARN, ME4
card_name('ebony horse', 'Ebony Horse').
card_type('ebony horse', 'Artifact').
card_types('ebony horse', ['Artifact']).
card_subtypes('ebony horse', []).
card_colors('ebony horse', []).
card_text('ebony horse', '{2}, {T}: Untap target attacking creature you control. Prevent all combat damage that would be dealt to and dealt by that creature this turn.').
card_mana_cost('ebony horse', ['3']).
card_cmc('ebony horse', 3).
card_layout('ebony horse', 'normal').

% Found in: SOK
card_name('ebony owl netsuke', 'Ebony Owl Netsuke').
card_type('ebony owl netsuke', 'Artifact').
card_types('ebony owl netsuke', ['Artifact']).
card_subtypes('ebony owl netsuke', []).
card_colors('ebony owl netsuke', []).
card_text('ebony owl netsuke', 'At the beginning of each opponent\'s upkeep, if that player has seven or more cards in hand, Ebony Owl Netsuke deals 4 damage to him or her.').
card_mana_cost('ebony owl netsuke', ['2']).
card_cmc('ebony owl netsuke', 2).
card_layout('ebony owl netsuke', 'normal').

% Found in: HML, ME4
card_name('ebony rhino', 'Ebony Rhino').
card_type('ebony rhino', 'Artifact Creature — Rhino').
card_types('ebony rhino', ['Artifact', 'Creature']).
card_subtypes('ebony rhino', ['Rhino']).
card_colors('ebony rhino', []).
card_text('ebony rhino', 'Trample').
card_mana_cost('ebony rhino', ['7']).
card_cmc('ebony rhino', 7).
card_layout('ebony rhino', 'normal').
card_power('ebony rhino', 4).
card_toughness('ebony rhino', 5).

% Found in: APC
card_name('ebony treefolk', 'Ebony Treefolk').
card_type('ebony treefolk', 'Creature — Treefolk').
card_types('ebony treefolk', ['Creature']).
card_subtypes('ebony treefolk', ['Treefolk']).
card_colors('ebony treefolk', ['B', 'G']).
card_text('ebony treefolk', '{B}{G}: Ebony Treefolk gets +1/+1 until end of turn.').
card_mana_cost('ebony treefolk', ['1', 'B', 'G']).
card_cmc('ebony treefolk', 3).
card_layout('ebony treefolk', 'normal').
card_power('ebony treefolk', 3).
card_toughness('ebony treefolk', 3).

% Found in: TMP
card_name('echo chamber', 'Echo Chamber').
card_type('echo chamber', 'Artifact').
card_types('echo chamber', ['Artifact']).
card_subtypes('echo chamber', []).
card_colors('echo chamber', []).
card_text('echo chamber', '{4}, {T}: An opponent chooses target creature he or she controls. Put a token that\'s a copy of that creature onto the battlefield. That token gains haste until end of turn. Exile the token at the beginning of the next end step. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('echo chamber', ['4']).
card_cmc('echo chamber', 4).
card_layout('echo chamber', 'normal').

% Found in: SOM
card_name('echo circlet', 'Echo Circlet').
card_type('echo circlet', 'Artifact — Equipment').
card_types('echo circlet', ['Artifact']).
card_subtypes('echo circlet', ['Equipment']).
card_colors('echo circlet', []).
card_text('echo circlet', 'Equipped creature can block an additional creature.\nEquip {1}').
card_mana_cost('echo circlet', ['2']).
card_cmc('echo circlet', 2).
card_layout('echo circlet', 'normal').

% Found in: C13, ROE
card_name('echo mage', 'Echo Mage').
card_type('echo mage', 'Creature — Human Wizard').
card_types('echo mage', ['Creature']).
card_subtypes('echo mage', ['Human', 'Wizard']).
card_colors('echo mage', ['U']).
card_text('echo mage', 'Level up {1}{U} ({1}{U}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 2-3\n2/4\n{U}{U}, {T}: Copy target instant or sorcery spell. You may choose new targets for the copy.\nLEVEL 4+\n2/5\n{U}{U}, {T}: Copy target instant or sorcery spell twice. You may choose new targets for the copies.').
card_mana_cost('echo mage', ['1', 'U', 'U']).
card_cmc('echo mage', 3).
card_layout('echo mage', 'leveler').
card_power('echo mage', 2).
card_toughness('echo mage', 3).

% Found in: DDN, LGN
card_name('echo tracer', 'Echo Tracer').
card_type('echo tracer', 'Creature — Human Wizard').
card_types('echo tracer', ['Creature']).
card_subtypes('echo tracer', ['Human', 'Wizard']).
card_colors('echo tracer', ['U']).
card_text('echo tracer', 'Morph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Echo Tracer is turned face up, return target creature to its owner\'s hand.').
card_mana_cost('echo tracer', ['2', 'U']).
card_cmc('echo tracer', 3).
card_layout('echo tracer', 'normal').
card_power('echo tracer', 2).
card_toughness('echo tracer', 2).

% Found in: DTK
card_name('echoes of the kin tree', 'Echoes of the Kin Tree').
card_type('echoes of the kin tree', 'Enchantment').
card_types('echoes of the kin tree', ['Enchantment']).
card_subtypes('echoes of the kin tree', []).
card_colors('echoes of the kin tree', ['W']).
card_text('echoes of the kin tree', '{2}{W}: Bolster 1. (Choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_mana_cost('echoes of the kin tree', ['1', 'W']).
card_cmc('echoes of the kin tree', 2).
card_layout('echoes of the kin tree', 'normal').

% Found in: DST
card_name('echoing calm', 'Echoing Calm').
card_type('echoing calm', 'Instant').
card_types('echoing calm', ['Instant']).
card_subtypes('echoing calm', []).
card_colors('echoing calm', ['W']).
card_text('echoing calm', 'Destroy target enchantment and all other enchantments with the same name as that enchantment.').
card_mana_cost('echoing calm', ['1', 'W']).
card_cmc('echoing calm', 2).
card_layout('echoing calm', 'normal').

% Found in: CNS, DST, MMA
card_name('echoing courage', 'Echoing Courage').
card_type('echoing courage', 'Instant').
card_types('echoing courage', ['Instant']).
card_subtypes('echoing courage', []).
card_colors('echoing courage', ['G']).
card_text('echoing courage', 'Target creature and all other creatures with the same name as that creature get +2/+2 until end of turn.').
card_mana_cost('echoing courage', ['1', 'G']).
card_cmc('echoing courage', 2).
card_layout('echoing courage', 'normal').

% Found in: DST
card_name('echoing decay', 'Echoing Decay').
card_type('echoing decay', 'Instant').
card_types('echoing decay', ['Instant']).
card_subtypes('echoing decay', []).
card_colors('echoing decay', ['B']).
card_text('echoing decay', 'Target creature and all other creatures with the same name as that creature get -2/-2 until end of turn.').
card_mana_cost('echoing decay', ['1', 'B']).
card_cmc('echoing decay', 2).
card_layout('echoing decay', 'normal').

% Found in: DST
card_name('echoing ruin', 'Echoing Ruin').
card_type('echoing ruin', 'Sorcery').
card_types('echoing ruin', ['Sorcery']).
card_subtypes('echoing ruin', []).
card_colors('echoing ruin', ['R']).
card_text('echoing ruin', 'Destroy target artifact and all other artifacts with the same name as that artifact.').
card_mana_cost('echoing ruin', ['1', 'R']).
card_cmc('echoing ruin', 2).
card_layout('echoing ruin', 'normal').

% Found in: DDF, DST, MMA
card_name('echoing truth', 'Echoing Truth').
card_type('echoing truth', 'Instant').
card_types('echoing truth', ['Instant']).
card_subtypes('echoing truth', []).
card_colors('echoing truth', ['U']).
card_text('echoing truth', 'Return target nonland permanent and all other permanents with the same name as that permanent to their owners\' hands.').
card_mana_cost('echoing truth', ['1', 'U']).
card_cmc('echoing truth', 2).
card_layout('echoing truth', 'normal').

% Found in: DDG, FUT
card_name('edge of autumn', 'Edge of Autumn').
card_type('edge of autumn', 'Sorcery').
card_types('edge of autumn', ['Sorcery']).
card_subtypes('edge of autumn', []).
card_colors('edge of autumn', ['G']).
card_text('edge of autumn', 'If you control four or fewer lands, search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.\nCycling—Sacrifice a land. (Sacrifice a land, Discard this card: Draw a card.)').
card_mana_cost('edge of autumn', ['1', 'G']).
card_cmc('edge of autumn', 2).
card_layout('edge of autumn', 'normal').

% Found in: PC2
card_name('edge of malacol', 'Edge of Malacol').
card_type('edge of malacol', 'Plane — Belenon').
card_types('edge of malacol', ['Plane']).
card_subtypes('edge of malacol', ['Belenon']).
card_colors('edge of malacol', []).
card_text('edge of malacol', 'If a creature you control would untap during your untap step, put two +1/+1 counters on it instead.\nWhenever you roll {C}, untap each creature you control.').
card_layout('edge of malacol', 'plane').

% Found in: EVE
card_name('edge of the divinity', 'Edge of the Divinity').
card_type('edge of the divinity', 'Enchantment — Aura').
card_types('edge of the divinity', ['Enchantment']).
card_subtypes('edge of the divinity', ['Aura']).
card_colors('edge of the divinity', ['W', 'B']).
card_text('edge of the divinity', 'Enchant creature\nAs long as enchanted creature is white, it gets +1/+2.\nAs long as enchanted creature is black, it gets +2/+1.').
card_mana_cost('edge of the divinity', ['W/B']).
card_cmc('edge of the divinity', 1).
card_layout('edge of the divinity', 'normal').

% Found in: SCG
card_name('edgewalker', 'Edgewalker').
card_type('edgewalker', 'Creature — Human Cleric').
card_types('edgewalker', ['Creature']).
card_subtypes('edgewalker', ['Human', 'Cleric']).
card_colors('edgewalker', ['W', 'B']).
card_text('edgewalker', 'Cleric spells you cast cost {W}{B} less to cast. This effect reduces only the amount of colored mana you pay. (For example, if you cast a Cleric spell with mana cost {1}{W}, it costs {1} to cast.)').
card_mana_cost('edgewalker', ['1', 'W', 'B']).
card_cmc('edgewalker', 3).
card_layout('edgewalker', 'normal').
card_power('edgewalker', 2).
card_toughness('edgewalker', 2).

% Found in: CM1, CMD, CNS, VMA
card_name('edric, spymaster of trest', 'Edric, Spymaster of Trest').
card_type('edric, spymaster of trest', 'Legendary Creature — Elf Rogue').
card_types('edric, spymaster of trest', ['Creature']).
card_subtypes('edric, spymaster of trest', ['Elf', 'Rogue']).
card_supertypes('edric, spymaster of trest', ['Legendary']).
card_colors('edric, spymaster of trest', ['U', 'G']).
card_text('edric, spymaster of trest', 'Whenever a creature deals combat damage to one of your opponents, its controller may draw a card.').
card_mana_cost('edric, spymaster of trest', ['1', 'G', 'U']).
card_cmc('edric, spymaster of trest', 3).
card_layout('edric, spymaster of trest', 'normal').
card_power('edric, spymaster of trest', 2).
card_toughness('edric, spymaster of trest', 2).

% Found in: ROE
card_name('eel umbra', 'Eel Umbra').
card_type('eel umbra', 'Enchantment — Aura').
card_types('eel umbra', ['Enchantment']).
card_subtypes('eel umbra', ['Aura']).
card_colors('eel umbra', ['U']).
card_text('eel umbra', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature gets +1/+1.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_mana_cost('eel umbra', ['1', 'U']).
card_cmc('eel umbra', 2).
card_layout('eel umbra', 'normal').

% Found in: CHK
card_name('eerie procession', 'Eerie Procession').
card_type('eerie procession', 'Sorcery — Arcane').
card_types('eerie procession', ['Sorcery']).
card_subtypes('eerie procession', ['Arcane']).
card_colors('eerie procession', ['U']).
card_text('eerie procession', 'Search your library for an Arcane card, reveal that card, and put it into your hand. Then shuffle your library.').
card_mana_cost('eerie procession', ['2', 'U']).
card_cmc('eerie procession', 3).
card_layout('eerie procession', 'normal').

% Found in: KTK
card_name('efreet weaponmaster', 'Efreet Weaponmaster').
card_type('efreet weaponmaster', 'Creature — Efreet Monk').
card_types('efreet weaponmaster', ['Creature']).
card_subtypes('efreet weaponmaster', ['Efreet', 'Monk']).
card_colors('efreet weaponmaster', ['W', 'U', 'R']).
card_text('efreet weaponmaster', 'First strike\nWhen Efreet Weaponmaster enters the battlefield or is turned face up, another target creature you control gets +3/+0 until end of turn.\nMorph {2}{U}{R}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('efreet weaponmaster', ['3', 'U', 'R', 'W']).
card_cmc('efreet weaponmaster', 6).
card_layout('efreet weaponmaster', 'normal').
card_power('efreet weaponmaster', 4).
card_toughness('efreet weaponmaster', 3).

% Found in: LRW
card_name('ego erasure', 'Ego Erasure').
card_type('ego erasure', 'Tribal Instant — Shapeshifter').
card_types('ego erasure', ['Tribal', 'Instant']).
card_subtypes('ego erasure', ['Shapeshifter']).
card_colors('ego erasure', ['U']).
card_text('ego erasure', 'Changeling (This card is every creature type.)\nCreatures target player controls get -2/-0 and lose all creature types until end of turn.').
card_mana_cost('ego erasure', ['2', 'U']).
card_cmc('ego erasure', 3).
card_layout('ego erasure', 'normal').

% Found in: JOU, pMEI
card_name('eidolon of blossoms', 'Eidolon of Blossoms').
card_type('eidolon of blossoms', 'Enchantment Creature — Spirit').
card_types('eidolon of blossoms', ['Enchantment', 'Creature']).
card_subtypes('eidolon of blossoms', ['Spirit']).
card_colors('eidolon of blossoms', ['G']).
card_text('eidolon of blossoms', 'Constellation — Whenever Eidolon of Blossoms or another enchantment enters the battlefield under your control, draw a card.').
card_mana_cost('eidolon of blossoms', ['2', 'G', 'G']).
card_cmc('eidolon of blossoms', 4).
card_layout('eidolon of blossoms', 'normal').
card_power('eidolon of blossoms', 2).
card_toughness('eidolon of blossoms', 2).

% Found in: BNG
card_name('eidolon of countless battles', 'Eidolon of Countless Battles').
card_type('eidolon of countless battles', 'Enchantment Creature — Spirit').
card_types('eidolon of countless battles', ['Enchantment', 'Creature']).
card_subtypes('eidolon of countless battles', ['Spirit']).
card_colors('eidolon of countless battles', ['W']).
card_text('eidolon of countless battles', 'Bestow {2}{W}{W} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nEidolon of Countless Battles and enchanted creature each get +1/+1 for each creature you control and +1/+1 for each Aura you control.').
card_mana_cost('eidolon of countless battles', ['1', 'W', 'W']).
card_cmc('eidolon of countless battles', 3).
card_layout('eidolon of countless battles', 'normal').
card_power('eidolon of countless battles', 0).
card_toughness('eidolon of countless battles', 0).

% Found in: JOU
card_name('eidolon of rhetoric', 'Eidolon of Rhetoric').
card_type('eidolon of rhetoric', 'Enchantment Creature — Spirit').
card_types('eidolon of rhetoric', ['Enchantment', 'Creature']).
card_subtypes('eidolon of rhetoric', ['Spirit']).
card_colors('eidolon of rhetoric', ['W']).
card_text('eidolon of rhetoric', 'Each player can\'t cast more than one spell each turn.').
card_mana_cost('eidolon of rhetoric', ['2', 'W']).
card_cmc('eidolon of rhetoric', 3).
card_layout('eidolon of rhetoric', 'normal').
card_power('eidolon of rhetoric', 1).
card_toughness('eidolon of rhetoric', 4).

% Found in: JOU
card_name('eidolon of the great revel', 'Eidolon of the Great Revel').
card_type('eidolon of the great revel', 'Enchantment Creature — Spirit').
card_types('eidolon of the great revel', ['Enchantment', 'Creature']).
card_subtypes('eidolon of the great revel', ['Spirit']).
card_colors('eidolon of the great revel', ['R']).
card_text('eidolon of the great revel', 'Whenever a player casts a spell with converted mana cost 3 or less, Eidolon of the Great Revel deals 2 damage to that player.').
card_mana_cost('eidolon of the great revel', ['R', 'R']).
card_cmc('eidolon of the great revel', 2).
card_layout('eidolon of the great revel', 'normal').
card_power('eidolon of the great revel', 2).
card_toughness('eidolon of the great revel', 2).

% Found in: CHK
card_name('eiganjo castle', 'Eiganjo Castle').
card_type('eiganjo castle', 'Legendary Land').
card_types('eiganjo castle', ['Land']).
card_subtypes('eiganjo castle', []).
card_supertypes('eiganjo castle', ['Legendary']).
card_colors('eiganjo castle', []).
card_text('eiganjo castle', '{T}: Add {W} to your mana pool.\n{W}, {T}: Prevent the next 2 damage that would be dealt to target legendary creature this turn.').
card_layout('eiganjo castle', 'normal').

% Found in: SOK
card_name('eiganjo free-riders', 'Eiganjo Free-Riders').
card_type('eiganjo free-riders', 'Creature — Human Soldier').
card_types('eiganjo free-riders', ['Creature']).
card_subtypes('eiganjo free-riders', ['Human', 'Soldier']).
card_colors('eiganjo free-riders', ['W']).
card_text('eiganjo free-riders', 'Flying\nAt the beginning of your upkeep, return a white creature you control to its owner\'s hand.').
card_mana_cost('eiganjo free-riders', ['3', 'W']).
card_cmc('eiganjo free-riders', 4).
card_layout('eiganjo free-riders', 'normal').
card_power('eiganjo free-riders', 3).
card_toughness('eiganjo free-riders', 4).

% Found in: CHK
card_name('eight-and-a-half-tails', 'Eight-and-a-Half-Tails').
card_type('eight-and-a-half-tails', 'Legendary Creature — Fox Cleric').
card_types('eight-and-a-half-tails', ['Creature']).
card_subtypes('eight-and-a-half-tails', ['Fox', 'Cleric']).
card_supertypes('eight-and-a-half-tails', ['Legendary']).
card_colors('eight-and-a-half-tails', ['W']).
card_text('eight-and-a-half-tails', '{1}{W}: Target permanent you control gains protection from white until end of turn.\n{1}: Target spell or permanent becomes white until end of turn.').
card_mana_cost('eight-and-a-half-tails', ['W', 'W']).
card_cmc('eight-and-a-half-tails', 2).
card_layout('eight-and-a-half-tails', 'normal').
card_power('eight-and-a-half-tails', 2).
card_toughness('eight-and-a-half-tails', 2).

% Found in: VAN
card_name('eight-and-a-half-tails avatar', 'Eight-and-a-Half-Tails Avatar').
card_type('eight-and-a-half-tails avatar', 'Vanguard').
card_types('eight-and-a-half-tails avatar', ['Vanguard']).
card_subtypes('eight-and-a-half-tails avatar', []).
card_colors('eight-and-a-half-tails avatar', []).
card_text('eight-and-a-half-tails avatar', '{1}: Until end of turn, target permanent you control gains protection from a color chosen at random from colors it doesn\'t have protection from.').
card_layout('eight-and-a-half-tails avatar', 'vanguard').

% Found in: ME3, PTK
card_name('eightfold maze', 'Eightfold Maze').
card_type('eightfold maze', 'Instant').
card_types('eightfold maze', ['Instant']).
card_subtypes('eightfold maze', []).
card_colors('eightfold maze', ['W']).
card_text('eightfold maze', 'Cast Eightfold Maze only during the declare attackers step and only if you\'ve been attacked this step.\nDestroy target attacking creature.').
card_mana_cost('eightfold maze', ['2', 'W']).
card_cmc('eightfold maze', 3).
card_layout('eightfold maze', 'normal').

% Found in: MIR
card_name('ekundu cyclops', 'Ekundu Cyclops').
card_type('ekundu cyclops', 'Creature — Cyclops').
card_types('ekundu cyclops', ['Creature']).
card_subtypes('ekundu cyclops', ['Cyclops']).
card_colors('ekundu cyclops', ['R']).
card_text('ekundu cyclops', 'If a creature you control attacks, Ekundu Cyclops also attacks if able.').
card_mana_cost('ekundu cyclops', ['3', 'R']).
card_cmc('ekundu cyclops', 4).
card_layout('ekundu cyclops', 'normal').
card_power('ekundu cyclops', 3).
card_toughness('ekundu cyclops', 4).

% Found in: 6ED, MIR
card_name('ekundu griffin', 'Ekundu Griffin').
card_type('ekundu griffin', 'Creature — Griffin').
card_types('ekundu griffin', ['Creature']).
card_subtypes('ekundu griffin', ['Griffin']).
card_colors('ekundu griffin', ['W']).
card_text('ekundu griffin', 'Flying, first strike').
card_mana_cost('ekundu griffin', ['3', 'W']).
card_cmc('ekundu griffin', 4).
card_layout('ekundu griffin', 'normal').
card_power('ekundu griffin', 2).
card_toughness('ekundu griffin', 2).

% Found in: 3ED, 4ED, ARN
card_name('el-hajjâj', 'El-Hajjâj').
card_type('el-hajjâj', 'Creature — Human Wizard').
card_types('el-hajjâj', ['Creature']).
card_subtypes('el-hajjâj', ['Human', 'Wizard']).
card_colors('el-hajjâj', ['B']).
card_text('el-hajjâj', 'Whenever El-Hajjâj deals damage, you gain that much life.').
card_mana_cost('el-hajjâj', ['1', 'B', 'B']).
card_cmc('el-hajjâj', 3).
card_layout('el-hajjâj', 'normal').
card_power('el-hajjâj', 1).
card_toughness('el-hajjâj', 1).

% Found in: VAN
card_name('eladamri', 'Eladamri').
card_type('eladamri', 'Vanguard').
card_types('eladamri', ['Vanguard']).
card_subtypes('eladamri', []).
card_colors('eladamri', []).
card_text('eladamri', '{0}: The next 1 damage that would be dealt to target creature you control is dealt to you instead.').
card_layout('eladamri', 'vanguard').

% Found in: PLS
card_name('eladamri\'s call', 'Eladamri\'s Call').
card_type('eladamri\'s call', 'Instant').
card_types('eladamri\'s call', ['Instant']).
card_subtypes('eladamri\'s call', []).
card_colors('eladamri\'s call', ['W', 'G']).
card_text('eladamri\'s call', 'Search your library for a creature card, reveal that card, and put it into your hand. Then shuffle your library.').
card_mana_cost('eladamri\'s call', ['G', 'W']).
card_cmc('eladamri\'s call', 2).
card_layout('eladamri\'s call', 'normal').

% Found in: TMP
card_name('eladamri\'s vineyard', 'Eladamri\'s Vineyard').
card_type('eladamri\'s vineyard', 'Enchantment').
card_types('eladamri\'s vineyard', ['Enchantment']).
card_subtypes('eladamri\'s vineyard', []).
card_colors('eladamri\'s vineyard', ['G']).
card_text('eladamri\'s vineyard', 'At the beginning of each player\'s precombat main phase, add {G}{G} to that player\'s mana pool.').
card_mana_cost('eladamri\'s vineyard', ['G']).
card_cmc('eladamri\'s vineyard', 1).
card_layout('eladamri\'s vineyard', 'normal').

% Found in: TMP
card_name('eladamri, lord of leaves', 'Eladamri, Lord of Leaves').
card_type('eladamri, lord of leaves', 'Legendary Creature — Elf Warrior').
card_types('eladamri, lord of leaves', ['Creature']).
card_subtypes('eladamri, lord of leaves', ['Elf', 'Warrior']).
card_supertypes('eladamri, lord of leaves', ['Legendary']).
card_colors('eladamri, lord of leaves', ['G']).
card_text('eladamri, lord of leaves', 'Other Elf creatures have forestwalk. (They can\'t be blocked as long as defending player controls a Forest.)\nOther Elves have shroud. (They can\'t be the targets of spells or abilities.)').
card_mana_cost('eladamri, lord of leaves', ['G', 'G']).
card_cmc('eladamri, lord of leaves', 2).
card_layout('eladamri, lord of leaves', 'normal').
card_power('eladamri, lord of leaves', 2).
card_toughness('eladamri, lord of leaves', 2).
card_reserved('eladamri, lord of leaves').

% Found in: VAN
card_name('eladamri, lord of leaves avatar', 'Eladamri, Lord of Leaves Avatar').
card_type('eladamri, lord of leaves avatar', 'Vanguard').
card_types('eladamri, lord of leaves avatar', ['Vanguard']).
card_subtypes('eladamri, lord of leaves avatar', []).
card_colors('eladamri, lord of leaves avatar', []).
card_text('eladamri, lord of leaves avatar', 'At the beginning of each player\'s precombat main phase, that player adds {G}{G} to his or her mana pool.').
card_layout('eladamri, lord of leaves avatar', 'vanguard').

% Found in: ROE
card_name('eland umbra', 'Eland Umbra').
card_type('eland umbra', 'Enchantment — Aura').
card_types('eland umbra', ['Enchantment']).
card_subtypes('eland umbra', ['Aura']).
card_colors('eland umbra', ['W']).
card_text('eland umbra', 'Enchant creature\nEnchanted creature gets +0/+4.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_mana_cost('eland umbra', ['1', 'W']).
card_cmc('eland umbra', 2).
card_layout('eland umbra', 'normal').

% Found in: DKA
card_name('elbrus, the binding blade', 'Elbrus, the Binding Blade').
card_type('elbrus, the binding blade', 'Legendary Artifact — Equipment').
card_types('elbrus, the binding blade', ['Artifact']).
card_subtypes('elbrus, the binding blade', ['Equipment']).
card_supertypes('elbrus, the binding blade', ['Legendary']).
card_colors('elbrus, the binding blade', []).
card_text('elbrus, the binding blade', 'Equipped creature gets +1/+0.\nWhen equipped creature deals combat damage to a player, unattach Elbrus, the Binding Blade, then transform it.\nEquip {1}').
card_mana_cost('elbrus, the binding blade', ['7']).
card_cmc('elbrus, the binding blade', 7).
card_layout('elbrus, the binding blade', 'double-faced').
card_sides('elbrus, the binding blade', 'withengar unbound').

% Found in: ISD
card_name('elder cathar', 'Elder Cathar').
card_type('elder cathar', 'Creature — Human Soldier').
card_types('elder cathar', ['Creature']).
card_subtypes('elder cathar', ['Human', 'Soldier']).
card_colors('elder cathar', ['W']).
card_text('elder cathar', 'When Elder Cathar dies, put a +1/+1 counter on target creature you control. If that creature is a Human, put two +1/+1 counters on it instead.').
card_mana_cost('elder cathar', ['2', 'W']).
card_cmc('elder cathar', 3).
card_layout('elder cathar', 'normal').
card_power('elder cathar', 2).
card_toughness('elder cathar', 2).

% Found in: 5ED, 6ED, 7ED, ICE
card_name('elder druid', 'Elder Druid').
card_type('elder druid', 'Creature — Elf Cleric Druid').
card_types('elder druid', ['Creature']).
card_subtypes('elder druid', ['Elf', 'Cleric', 'Druid']).
card_colors('elder druid', ['G']).
card_text('elder druid', '{3}{G}, {T}: You may tap or untap target artifact, creature, or land.').
card_mana_cost('elder druid', ['3', 'G']).
card_cmc('elder druid', 4).
card_layout('elder druid', 'normal').
card_power('elder druid', 2).
card_toughness('elder druid', 2).

% Found in: 4ED, LEG, MED
card_name('elder land wurm', 'Elder Land Wurm').
card_type('elder land wurm', 'Creature — Dragon Wurm').
card_types('elder land wurm', ['Creature']).
card_subtypes('elder land wurm', ['Dragon', 'Wurm']).
card_colors('elder land wurm', ['W']).
card_text('elder land wurm', 'Defender, trample\nWhen Elder Land Wurm blocks, it loses defender.').
card_mana_cost('elder land wurm', ['4', 'W', 'W', 'W']).
card_cmc('elder land wurm', 7).
card_layout('elder land wurm', 'normal').
card_power('elder land wurm', 5).
card_toughness('elder land wurm', 5).

% Found in: CON, DDH
card_name('elder mastery', 'Elder Mastery').
card_type('elder mastery', 'Enchantment — Aura').
card_types('elder mastery', ['Enchantment']).
card_subtypes('elder mastery', ['Aura']).
card_colors('elder mastery', ['U', 'B', 'R']).
card_text('elder mastery', 'Enchant creature\nEnchanted creature gets +3/+3 and has flying.\nWhenever enchanted creature deals damage to a player, that player discards two cards.').
card_mana_cost('elder mastery', ['3', 'U', 'B', 'R']).
card_cmc('elder mastery', 6).
card_layout('elder mastery', 'normal').

% Found in: ISD
card_name('elder of laurels', 'Elder of Laurels').
card_type('elder of laurels', 'Creature — Human Advisor').
card_types('elder of laurels', ['Creature']).
card_subtypes('elder of laurels', ['Human', 'Advisor']).
card_colors('elder of laurels', ['G']).
card_text('elder of laurels', '{3}{G}: Target creature gets +X/+X until end of turn, where X is the number of creatures you control.').
card_mana_cost('elder of laurels', ['2', 'G']).
card_cmc('elder of laurels', 3).
card_layout('elder of laurels', 'normal').
card_power('elder of laurels', 2).
card_toughness('elder of laurels', 3).

% Found in: SOK
card_name('elder pine of jukai', 'Elder Pine of Jukai').
card_type('elder pine of jukai', 'Creature — Spirit').
card_types('elder pine of jukai', ['Creature']).
card_subtypes('elder pine of jukai', ['Spirit']).
card_colors('elder pine of jukai', ['G']).
card_text('elder pine of jukai', 'Whenever you cast a Spirit or Arcane spell, reveal the top three cards of your library. Put all land cards revealed this way into your hand and the rest on the bottom of your library in any order.\nSoulshift 2 (When this creature dies, you may return target Spirit card with converted mana cost 2 or less from your graveyard to your hand.)').
card_mana_cost('elder pine of jukai', ['2', 'G']).
card_cmc('elder pine of jukai', 3).
card_layout('elder pine of jukai', 'normal').
card_power('elder pine of jukai', 2).
card_toughness('elder pine of jukai', 1).

% Found in: LEG
card_name('elder spawn', 'Elder Spawn').
card_type('elder spawn', 'Creature — Spawn').
card_types('elder spawn', ['Creature']).
card_subtypes('elder spawn', ['Spawn']).
card_colors('elder spawn', ['U']).
card_text('elder spawn', 'At the beginning of your upkeep, unless you sacrifice an Island, sacrifice Elder Spawn and it deals 6 damage to you.\nElder Spawn can\'t be blocked by red creatures.').
card_mana_cost('elder spawn', ['4', 'U', 'U', 'U']).
card_cmc('elder spawn', 7).
card_layout('elder spawn', 'normal').
card_power('elder spawn', 6).
card_toughness('elder spawn', 6).
card_reserved('elder spawn').

% Found in: M13
card_name('elderscale wurm', 'Elderscale Wurm').
card_type('elderscale wurm', 'Creature — Wurm').
card_types('elderscale wurm', ['Creature']).
card_subtypes('elderscale wurm', ['Wurm']).
card_colors('elderscale wurm', ['G']).
card_text('elderscale wurm', 'Trample\nWhen Elderscale Wurm enters the battlefield, if your life total is less than 7, your life total becomes 7.\nAs long as you have 7 or more life, damage that would reduce your life total to less than 7 reduces it to 7 instead.').
card_mana_cost('elderscale wurm', ['4', 'G', 'G', 'G']).
card_cmc('elderscale wurm', 7).
card_layout('elderscale wurm', 'normal').
card_power('elderscale wurm', 7).
card_toughness('elderscale wurm', 7).

% Found in: PC2
card_name('elderwood scion', 'Elderwood Scion').
card_type('elderwood scion', 'Creature — Elemental').
card_types('elderwood scion', ['Creature']).
card_subtypes('elderwood scion', ['Elemental']).
card_colors('elderwood scion', ['W', 'G']).
card_text('elderwood scion', 'Trample, lifelink\nSpells you cast that target Elderwood Scion cost {2} less to cast.\nSpells your opponents cast that target Elderwood Scion cost {2} more to cast.').
card_mana_cost('elderwood scion', ['3', 'G', 'W']).
card_cmc('elderwood scion', 5).
card_layout('elderwood scion', 'normal').
card_power('elderwood scion', 4).
card_toughness('elderwood scion', 4).

% Found in: ROE
card_name('eldrazi conscription', 'Eldrazi Conscription').
card_type('eldrazi conscription', 'Tribal Enchantment — Eldrazi Aura').
card_types('eldrazi conscription', ['Tribal', 'Enchantment']).
card_subtypes('eldrazi conscription', ['Eldrazi', 'Aura']).
card_colors('eldrazi conscription', []).
card_text('eldrazi conscription', 'Enchant creature\nEnchanted creature gets +10/+10 and has trample and annihilator 2. (Whenever it attacks, defending player sacrifices two permanents.)').
card_mana_cost('eldrazi conscription', ['8']).
card_cmc('eldrazi conscription', 8).
card_layout('eldrazi conscription', 'normal').

% Found in: BFZ
card_name('eldrazi devastator', 'Eldrazi Devastator').
card_type('eldrazi devastator', 'Creature — Eldrazi').
card_types('eldrazi devastator', ['Creature']).
card_subtypes('eldrazi devastator', ['Eldrazi']).
card_colors('eldrazi devastator', []).
card_text('eldrazi devastator', 'Trample').
card_mana_cost('eldrazi devastator', ['8']).
card_cmc('eldrazi devastator', 8).
card_layout('eldrazi devastator', 'normal').
card_power('eldrazi devastator', 8).
card_toughness('eldrazi devastator', 9).

% Found in: ZEN
card_name('eldrazi monument', 'Eldrazi Monument').
card_type('eldrazi monument', 'Artifact').
card_types('eldrazi monument', ['Artifact']).
card_subtypes('eldrazi monument', []).
card_colors('eldrazi monument', []).
card_text('eldrazi monument', 'Creatures you control get +1/+1 and have flying and indestructible.\nAt the beginning of your upkeep, sacrifice a creature. If you can\'t, sacrifice Eldrazi Monument.').
card_mana_cost('eldrazi monument', ['5']).
card_cmc('eldrazi monument', 5).
card_layout('eldrazi monument', 'normal').

% Found in: BFZ
card_name('eldrazi skyspawner', 'Eldrazi Skyspawner').
card_type('eldrazi skyspawner', 'Creature — Eldrazi Drone').
card_types('eldrazi skyspawner', ['Creature']).
card_subtypes('eldrazi skyspawner', ['Eldrazi', 'Drone']).
card_colors('eldrazi skyspawner', []).
card_text('eldrazi skyspawner', 'Devoid (This card has no color.)\nFlying\nWhen Eldrazi Skyspawner enters the battlefield, put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('eldrazi skyspawner', ['2', 'U']).
card_cmc('eldrazi skyspawner', 3).
card_layout('eldrazi skyspawner', 'normal').
card_power('eldrazi skyspawner', 2).
card_toughness('eldrazi skyspawner', 1).

% Found in: DDP
card_name('eldrazi spawn', 'Eldrazi Spawn').
card_type('eldrazi spawn', ' Creature — Eldrazi Spawn').
card_types('eldrazi spawn', ['Creature']).
card_subtypes('eldrazi spawn', ['Eldrazi', 'Spawn']).
card_colors('eldrazi spawn', []).
card_text('eldrazi spawn', 'Sacrifice this creature: Add {1} to your mana pool.').
card_layout('eldrazi spawn', 'token').
card_power('eldrazi spawn', 0).
card_toughness('eldrazi spawn', 1).

% Found in: DDP, MM2, ROE
card_name('eldrazi temple', 'Eldrazi Temple').
card_type('eldrazi temple', 'Land').
card_types('eldrazi temple', ['Land']).
card_subtypes('eldrazi temple', []).
card_colors('eldrazi temple', []).
card_text('eldrazi temple', '{T}: Add {1} to your mana pool.\n{T}: Add {2} to your mana pool. Spend this mana only to cast colorless Eldrazi spells or activate abilities of colorless Eldrazi.').
card_layout('eldrazi temple', 'normal').

% Found in: DRK
card_name('electric eel', 'Electric Eel').
card_type('electric eel', 'Creature — Fish').
card_types('electric eel', ['Creature']).
card_subtypes('electric eel', ['Fish']).
card_colors('electric eel', ['U']).
card_text('electric eel', 'When Electric Eel enters the battlefield, it deals 1 damage to you.\n{R}{R}: Electric Eel gets +2/+0 until end of turn and deals 1 damage to you.').
card_mana_cost('electric eel', ['U']).
card_cmc('electric eel', 1).
card_layout('electric eel', 'normal').
card_power('electric eel', 1).
card_toughness('electric eel', 1).

% Found in: RTR
card_name('electrickery', 'Electrickery').
card_type('electrickery', 'Instant').
card_types('electrickery', ['Instant']).
card_subtypes('electrickery', []).
card_colors('electrickery', ['R']).
card_text('electrickery', 'Electrickery deals 1 damage to target creature you don\'t control.\nOverload {1}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_mana_cost('electrickery', ['R']).
card_cmc('electrickery', 1).
card_layout('electrickery', 'normal').

% Found in: CMD, GPT, MM2, MMA, pCMP, pMEI
card_name('electrolyze', 'Electrolyze').
card_type('electrolyze', 'Instant').
card_types('electrolyze', ['Instant']).
card_subtypes('electrolyze', []).
card_colors('electrolyze', ['U', 'R']).
card_text('electrolyze', 'Electrolyze deals 2 damage divided as you choose among one or two target creatures and/or players.\nDraw a card.').
card_mana_cost('electrolyze', ['1', 'U', 'R']).
card_cmc('electrolyze', 3).
card_layout('electrolyze', 'normal').

% Found in: ZEN
card_name('electropotence', 'Electropotence').
card_type('electropotence', 'Enchantment').
card_types('electropotence', ['Enchantment']).
card_subtypes('electropotence', []).
card_colors('electropotence', ['R']).
card_text('electropotence', 'Whenever a creature enters the battlefield under your control, you may pay {2}{R}. If you do, that creature deals damage equal to its power to target creature or player.').
card_mana_cost('electropotence', ['2', 'R']).
card_cmc('electropotence', 3).
card_layout('electropotence', 'normal').

% Found in: MRD
card_name('electrostatic bolt', 'Electrostatic Bolt').
card_type('electrostatic bolt', 'Instant').
card_types('electrostatic bolt', ['Instant']).
card_subtypes('electrostatic bolt', []).
card_colors('electrostatic bolt', ['R']).
card_text('electrostatic bolt', 'Electrostatic Bolt deals 2 damage to target creature. If it\'s an artifact creature, Electrostatic Bolt deals 4 damage to it instead.').
card_mana_cost('electrostatic bolt', ['R']).
card_cmc('electrostatic bolt', 1).
card_layout('electrostatic bolt', 'normal').

% Found in: USG
card_name('electryte', 'Electryte').
card_type('electryte', 'Creature — Beast').
card_types('electryte', ['Creature']).
card_subtypes('electryte', ['Beast']).
card_colors('electryte', ['R']).
card_text('electryte', 'Whenever Electryte deals combat damage to defending player, it deals damage equal to its power to each blocking creature.').
card_mana_cost('electryte', ['3', 'R', 'R']).
card_cmc('electryte', 5).
card_layout('electryte', 'normal').
card_power('electryte', 3).
card_toughness('electryte', 3).

% Found in: EVG
card_name('elemental', 'Elemental').
card_type('elemental', 'Creature — Elemental').
card_types('elemental', ['Creature']).
card_subtypes('elemental', ['Elemental']).
card_colors('elemental', []).
card_text('elemental', 'Trample').
card_layout('elemental', 'token').
card_power('elemental', 7).
card_toughness('elemental', 7).

% Found in: ZEN
card_name('elemental appeal', 'Elemental Appeal').
card_type('elemental appeal', 'Sorcery').
card_types('elemental appeal', ['Sorcery']).
card_subtypes('elemental appeal', []).
card_colors('elemental appeal', ['R']).
card_text('elemental appeal', 'Kicker {5} (You may pay an additional {5} as you cast this spell.)\nPut a 7/1 red Elemental creature token with trample and haste onto the battlefield. Exile it at the beginning of the next end step. If Elemental Appeal was kicked, that creature gets +7/+0 until end of turn.').
card_mana_cost('elemental appeal', ['R', 'R', 'R', 'R']).
card_cmc('elemental appeal', 4).
card_layout('elemental appeal', 'normal').

% Found in: ICE, ME2
card_name('elemental augury', 'Elemental Augury').
card_type('elemental augury', 'Enchantment').
card_types('elemental augury', ['Enchantment']).
card_subtypes('elemental augury', []).
card_colors('elemental augury', ['U', 'B', 'R']).
card_text('elemental augury', '{3}: Look at the top three cards of target player\'s library, then put them back in any order.').
card_mana_cost('elemental augury', ['U', 'B', 'R']).
card_cmc('elemental augury', 3).
card_layout('elemental augury', 'normal').

% Found in: ORI
card_name('elemental bond', 'Elemental Bond').
card_type('elemental bond', 'Enchantment').
card_types('elemental bond', ['Enchantment']).
card_subtypes('elemental bond', []).
card_colors('elemental bond', ['G']).
card_text('elemental bond', 'Whenever a creature with power 3 or greater enters the battlefield under your control, draw a card.').
card_mana_cost('elemental bond', ['2', 'G']).
card_cmc('elemental bond', 3).
card_layout('elemental bond', 'normal').

% Found in: SHM
card_name('elemental mastery', 'Elemental Mastery').
card_type('elemental mastery', 'Enchantment — Aura').
card_types('elemental mastery', ['Enchantment']).
card_subtypes('elemental mastery', ['Aura']).
card_colors('elemental mastery', ['R']).
card_text('elemental mastery', 'Enchant creature\nEnchanted creature has \"{T}: Put X 1/1 red Elemental creature tokens with haste onto the battlefield, where X is this creature\'s power. Exile them at the beginning of the next end step.\"').
card_mana_cost('elemental mastery', ['3', 'R']).
card_cmc('elemental mastery', 4).
card_layout('elemental mastery', 'normal').

% Found in: DIS
card_name('elemental resonance', 'Elemental Resonance').
card_type('elemental resonance', 'Enchantment — Aura').
card_types('elemental resonance', ['Enchantment']).
card_subtypes('elemental resonance', ['Aura']).
card_colors('elemental resonance', ['G']).
card_text('elemental resonance', 'Enchant permanent\nAt the beginning of your precombat main phase, add mana equal to enchanted permanent\'s mana cost to your mana pool. (Mana cost includes color. If a mana symbol has multiple colors, choose one.)').
card_mana_cost('elemental resonance', ['2', 'G', 'G']).
card_cmc('elemental resonance', 4).
card_layout('elemental resonance', 'normal').

% Found in: DD2
card_name('elemental shaman', 'Elemental Shaman').
card_type('elemental shaman', 'Creature — Elemental Shaman').
card_types('elemental shaman', ['Creature']).
card_subtypes('elemental shaman', ['Elemental', 'Shaman']).
card_colors('elemental shaman', []).
card_text('elemental shaman', '').
card_layout('elemental shaman', 'token').
card_power('elemental shaman', 3).
card_toughness('elemental shaman', 1).

% Found in: DDD
card_name('elephant', 'Elephant').
card_type('elephant', 'Creature — Elephant').
card_types('elephant', ['Creature']).
card_subtypes('elephant', ['Elephant']).
card_colors('elephant', []).
card_text('elephant', '').
card_layout('elephant', 'token').
card_power('elephant', 3).
card_toughness('elephant', 3).

% Found in: ODY
card_name('elephant ambush', 'Elephant Ambush').
card_type('elephant ambush', 'Instant').
card_types('elephant ambush', ['Instant']).
card_subtypes('elephant ambush', []).
card_colors('elephant ambush', ['G']).
card_text('elephant ambush', 'Put a 3/3 green Elephant creature token onto the battlefield.\nFlashback {6}{G}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('elephant ambush', ['2', 'G', 'G']).
card_cmc('elephant ambush', 4).
card_layout('elephant ambush', 'normal').

% Found in: VIS
card_name('elephant grass', 'Elephant Grass').
card_type('elephant grass', 'Enchantment').
card_types('elephant grass', ['Enchantment']).
card_subtypes('elephant grass', []).
card_colors('elephant grass', ['G']).
card_text('elephant grass', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nBlack creatures can\'t attack you.\nNonblack creatures can\'t attack you unless their controller pays {2} for each creature he or she controls that\'s attacking you.').
card_mana_cost('elephant grass', ['G']).
card_cmc('elephant grass', 1).
card_layout('elephant grass', 'normal').

% Found in: ARN, ME4
card_name('elephant graveyard', 'Elephant Graveyard').
card_type('elephant graveyard', 'Land').
card_types('elephant graveyard', ['Land']).
card_subtypes('elephant graveyard', []).
card_colors('elephant graveyard', []).
card_text('elephant graveyard', '{T}: Add {1} to your mana pool.\n{T}: Regenerate target Elephant.').
card_layout('elephant graveyard', 'normal').
card_reserved('elephant graveyard').

% Found in: CNS, DD3_GVL, DDD, JUD, VMA
card_name('elephant guide', 'Elephant Guide').
card_type('elephant guide', 'Enchantment — Aura').
card_types('elephant guide', ['Enchantment']).
card_subtypes('elephant guide', ['Aura']).
card_colors('elephant guide', ['G']).
card_text('elephant guide', 'Enchant creature\nEnchanted creature gets +3/+3.\nWhen enchanted creature dies, put a 3/3 green Elephant creature token onto the battlefield.').
card_mana_cost('elephant guide', ['2', 'G']).
card_cmc('elephant guide', 3).
card_layout('elephant guide', 'normal').

% Found in: PCY
card_name('elephant resurgence', 'Elephant Resurgence').
card_type('elephant resurgence', 'Sorcery').
card_types('elephant resurgence', ['Sorcery']).
card_subtypes('elephant resurgence', []).
card_colors('elephant resurgence', ['G']).
card_text('elephant resurgence', 'Each player puts a green Elephant creature token onto the battlefield. Those creatures have \"This creature\'s power and toughness are each equal to the number of creature cards in its controller\'s graveyard.\"').
card_mana_cost('elephant resurgence', ['1', 'G']).
card_cmc('elephant resurgence', 2).
card_layout('elephant resurgence', 'normal').

% Found in: MM2, NPH, pJGP
card_name('elesh norn, grand cenobite', 'Elesh Norn, Grand Cenobite').
card_type('elesh norn, grand cenobite', 'Legendary Creature — Praetor').
card_types('elesh norn, grand cenobite', ['Creature']).
card_subtypes('elesh norn, grand cenobite', ['Praetor']).
card_supertypes('elesh norn, grand cenobite', ['Legendary']).
card_colors('elesh norn, grand cenobite', ['W']).
card_text('elesh norn, grand cenobite', 'Vigilance\nOther creatures you control get +2/+2.\nCreatures your opponents control get -2/-2.').
card_mana_cost('elesh norn, grand cenobite', ['5', 'W', 'W']).
card_cmc('elesh norn, grand cenobite', 7).
card_layout('elesh norn, grand cenobite', 'normal').
card_power('elesh norn, grand cenobite', 4).
card_toughness('elesh norn, grand cenobite', 7).

% Found in: MRD
card_name('elf replica', 'Elf Replica').
card_type('elf replica', 'Artifact Creature — Elf').
card_types('elf replica', ['Artifact', 'Creature']).
card_subtypes('elf replica', ['Elf']).
card_colors('elf replica', []).
card_text('elf replica', '{1}{G}, Sacrifice Elf Replica: Destroy target enchantment.').
card_mana_cost('elf replica', ['3']).
card_cmc('elf replica', 3).
card_layout('elf replica', 'normal').
card_power('elf replica', 2).
card_toughness('elf replica', 2).

% Found in: EVG
card_name('elf warrior', 'Elf Warrior').
card_type('elf warrior', 'Creature — Elf Warrior').
card_types('elf warrior', ['Creature']).
card_subtypes('elf warrior', ['Elf', 'Warrior']).
card_colors('elf warrior', []).
card_text('elf warrior', '').
card_layout('elf warrior', 'token').
card_power('elf warrior', 1).
card_toughness('elf warrior', 1).

% Found in: 8ED, DDE, INV
card_name('elfhame palace', 'Elfhame Palace').
card_type('elfhame palace', 'Land').
card_types('elfhame palace', ['Land']).
card_subtypes('elfhame palace', []).
card_colors('elfhame palace', []).
card_text('elfhame palace', 'Elfhame Palace enters the battlefield tapped.\n{T}: Add {G} or {W} to your mana pool.').
card_layout('elfhame palace', 'normal').

% Found in: INV
card_name('elfhame sanctuary', 'Elfhame Sanctuary').
card_type('elfhame sanctuary', 'Enchantment').
card_types('elfhame sanctuary', ['Enchantment']).
card_subtypes('elfhame sanctuary', []).
card_colors('elfhame sanctuary', ['G']).
card_text('elfhame sanctuary', 'At the beginning of your upkeep, you may search your library for a basic land card, reveal that card, and put it into your hand. If you do, you skip your draw step this turn and shuffle your library.').
card_mana_cost('elfhame sanctuary', ['1', 'G']).
card_cmc('elfhame sanctuary', 2).
card_layout('elfhame sanctuary', 'normal').

% Found in: DKA
card_name('elgaud inquisitor', 'Elgaud Inquisitor').
card_type('elgaud inquisitor', 'Creature — Human Cleric').
card_types('elgaud inquisitor', ['Creature']).
card_subtypes('elgaud inquisitor', ['Human', 'Cleric']).
card_colors('elgaud inquisitor', ['W']).
card_text('elgaud inquisitor', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)\nWhen Elgaud Inquisitor dies, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_mana_cost('elgaud inquisitor', ['3', 'W']).
card_cmc('elgaud inquisitor', 4).
card_layout('elgaud inquisitor', 'normal').
card_power('elgaud inquisitor', 2).
card_toughness('elgaud inquisitor', 2).

% Found in: AVR
card_name('elgaud shieldmate', 'Elgaud Shieldmate').
card_type('elgaud shieldmate', 'Creature — Human Soldier').
card_types('elgaud shieldmate', ['Creature']).
card_subtypes('elgaud shieldmate', ['Human', 'Soldier']).
card_colors('elgaud shieldmate', ['U']).
card_text('elgaud shieldmate', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Elgaud Shieldmate is paired with another creature, both creatures have hexproof. (They can\'t be the targets of spells or abilities your opponents control.)').
card_mana_cost('elgaud shieldmate', ['3', 'U']).
card_cmc('elgaud shieldmate', 4).
card_layout('elgaud shieldmate', 'normal').
card_power('elgaud shieldmate', 2).
card_toughness('elgaud shieldmate', 3).

% Found in: M14
card_name('elite arcanist', 'Elite Arcanist').
card_type('elite arcanist', 'Creature — Human Wizard').
card_types('elite arcanist', ['Creature']).
card_subtypes('elite arcanist', ['Human', 'Wizard']).
card_colors('elite arcanist', ['U']).
card_text('elite arcanist', 'When Elite Arcanist enters the battlefield, you may exile an instant card from your hand.\n{X}, {T}: Copy the exiled card. You may cast the copy without paying its mana cost. X is the converted mana cost of the exiled card.').
card_mana_cost('elite arcanist', ['3', 'U']).
card_cmc('elite arcanist', 4).
card_layout('elite arcanist', 'normal').
card_power('elite arcanist', 1).
card_toughness('elite arcanist', 1).

% Found in: 7ED, 8ED, USG
card_name('elite archers', 'Elite Archers').
card_type('elite archers', 'Creature — Human Soldier Archer').
card_types('elite archers', ['Creature']).
card_subtypes('elite archers', ['Human', 'Soldier', 'Archer']).
card_colors('elite archers', ['W']).
card_text('elite archers', '{T}: Elite Archers deals 3 damage to target attacking or blocking creature.').
card_mana_cost('elite archers', ['5', 'W']).
card_cmc('elite archers', 6).
card_layout('elite archers', 'normal').
card_power('elite archers', 3).
card_toughness('elite archers', 3).

% Found in: ME4, POR
card_name('elite cat warrior', 'Elite Cat Warrior').
card_type('elite cat warrior', 'Creature — Cat Warrior').
card_types('elite cat warrior', ['Creature']).
card_subtypes('elite cat warrior', ['Cat', 'Warrior']).
card_colors('elite cat warrior', ['G']).
card_text('elite cat warrior', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('elite cat warrior', ['2', 'G']).
card_cmc('elite cat warrior', 3).
card_layout('elite cat warrior', 'normal').
card_power('elite cat warrior', 2).
card_toughness('elite cat warrior', 3).

% Found in: ISD, pMGD
card_name('elite inquisitor', 'Elite Inquisitor').
card_type('elite inquisitor', 'Creature — Human Soldier').
card_types('elite inquisitor', ['Creature']).
card_subtypes('elite inquisitor', ['Human', 'Soldier']).
card_colors('elite inquisitor', ['W']).
card_text('elite inquisitor', 'First strike, vigilance\nProtection from Vampires, from Werewolves, and from Zombies').
card_mana_cost('elite inquisitor', ['W', 'W']).
card_cmc('elite inquisitor', 2).
card_layout('elite inquisitor', 'normal').
card_power('elite inquisitor', 2).
card_toughness('elite inquisitor', 2).

% Found in: 8ED, TMP
card_name('elite javelineer', 'Elite Javelineer').
card_type('elite javelineer', 'Creature — Human Soldier').
card_types('elite javelineer', ['Creature']).
card_subtypes('elite javelineer', ['Human', 'Soldier']).
card_colors('elite javelineer', ['W']).
card_text('elite javelineer', 'Whenever Elite Javelineer blocks, it deals 1 damage to target attacking creature.').
card_mana_cost('elite javelineer', ['2', 'W']).
card_cmc('elite javelineer', 3).
card_layout('elite javelineer', 'normal').
card_power('elite javelineer', 2).
card_toughness('elite javelineer', 2).

% Found in: FRF
card_name('elite scaleguard', 'Elite Scaleguard').
card_type('elite scaleguard', 'Creature — Human Soldier').
card_types('elite scaleguard', ['Creature']).
card_subtypes('elite scaleguard', ['Human', 'Soldier']).
card_colors('elite scaleguard', ['W']).
card_text('elite scaleguard', 'When Elite Scaleguard enters the battlefield, bolster 2. (Choose a creature with the least toughness among creatures you control and put two +1/+1 counters on it.)\nWhenever a creature you control with a +1/+1 counter on it attacks, tap target creature defending player controls.').
card_mana_cost('elite scaleguard', ['4', 'W']).
card_cmc('elite scaleguard', 5).
card_layout('elite scaleguard', 'normal').
card_power('elite scaleguard', 2).
card_toughness('elite scaleguard', 3).

% Found in: BNG
card_name('elite skirmisher', 'Elite Skirmisher').
card_type('elite skirmisher', 'Creature — Human Soldier').
card_types('elite skirmisher', ['Creature']).
card_subtypes('elite skirmisher', ['Human', 'Soldier']).
card_colors('elite skirmisher', ['W']).
card_text('elite skirmisher', 'Heroic — Whenever you cast a spell that targets Elite Skirmisher, you may tap target creature.').
card_mana_cost('elite skirmisher', ['2', 'W']).
card_cmc('elite skirmisher', 3).
card_layout('elite skirmisher', 'normal').
card_power('elite skirmisher', 3).
card_toughness('elite skirmisher', 1).

% Found in: DDF, M10, M11, M12
card_name('elite vanguard', 'Elite Vanguard').
card_type('elite vanguard', 'Creature — Human Soldier').
card_types('elite vanguard', ['Creature']).
card_subtypes('elite vanguard', ['Human', 'Soldier']).
card_colors('elite vanguard', ['W']).
card_text('elite vanguard', '').
card_mana_cost('elite vanguard', ['W']).
card_cmc('elite vanguard', 1).
card_layout('elite vanguard', 'normal').
card_power('elite vanguard', 2).
card_toughness('elite vanguard', 1).

% Found in: DDF, M11, M12, M13, M14
card_name('elixir of immortality', 'Elixir of Immortality').
card_type('elixir of immortality', 'Artifact').
card_types('elixir of immortality', ['Artifact']).
card_subtypes('elixir of immortality', []).
card_colors('elixir of immortality', []).
card_text('elixir of immortality', '{2}, {T}: You gain 5 life. Shuffle Elixir of Immortality and your graveyard into their owner\'s library.').
card_mana_cost('elixir of immortality', ['1']).
card_cmc('elixir of immortality', 1).
card_layout('elixir of immortality', 'normal').

% Found in: MIR
card_name('elixir of vitality', 'Elixir of Vitality').
card_type('elixir of vitality', 'Artifact').
card_types('elixir of vitality', ['Artifact']).
card_subtypes('elixir of vitality', []).
card_colors('elixir of vitality', []).
card_text('elixir of vitality', 'Elixir of Vitality enters the battlefield tapped.\n{T}, Sacrifice Elixir of Vitality: You gain 4 life.\n{8}, {T}, Sacrifice Elixir of Vitality: You gain 8 life.').
card_mana_cost('elixir of vitality', ['4']).
card_cmc('elixir of vitality', 4).
card_layout('elixir of vitality', 'normal').

% Found in: 5ED, DKM, ICE, ME2
card_name('elkin bottle', 'Elkin Bottle').
card_type('elkin bottle', 'Artifact').
card_types('elkin bottle', ['Artifact']).
card_subtypes('elkin bottle', []).
card_colors('elkin bottle', []).
card_text('elkin bottle', '{3}, {T}: Exile the top card of your library. Until the beginning of your next upkeep, you may play that card.').
card_mana_cost('elkin bottle', ['3']).
card_cmc('elkin bottle', 3).
card_layout('elkin bottle', 'normal').

% Found in: VIS
card_name('elkin lair', 'Elkin Lair').
card_type('elkin lair', 'World Enchantment').
card_types('elkin lair', ['Enchantment']).
card_subtypes('elkin lair', []).
card_supertypes('elkin lair', ['World']).
card_colors('elkin lair', ['R']).
card_text('elkin lair', 'At the beginning of each player\'s upkeep, that player exiles a card at random from his or her hand. The player may play that card this turn. At the beginning of the next end step, if the player hasn\'t played the card, he or she puts it into his or her graveyard.').
card_mana_cost('elkin lair', ['3', 'R']).
card_cmc('elkin lair', 4).
card_layout('elkin lair', 'normal').
card_reserved('elkin lair').

% Found in: HOP
card_name('eloren wilds', 'Eloren Wilds').
card_type('eloren wilds', 'Plane — Shandalar').
card_types('eloren wilds', ['Plane']).
card_subtypes('eloren wilds', ['Shandalar']).
card_colors('eloren wilds', []).
card_text('eloren wilds', 'Whenever a player taps a permanent for mana, that player adds one mana to his or her mana pool of any type that permanent produced.\nWhenever you roll {C}, target player can\'t cast spells until a player planeswalks.').
card_layout('eloren wilds', 'plane').

% Found in: SHM
card_name('elsewhere flask', 'Elsewhere Flask').
card_type('elsewhere flask', 'Artifact').
card_types('elsewhere flask', ['Artifact']).
card_subtypes('elsewhere flask', []).
card_colors('elsewhere flask', []).
card_text('elsewhere flask', 'When Elsewhere Flask enters the battlefield, draw a card.\nSacrifice Elsewhere Flask: Choose a basic land type. Each land you control becomes that type until end of turn.').
card_mana_cost('elsewhere flask', ['2']).
card_cmc('elsewhere flask', 2).
card_layout('elsewhere flask', 'normal').

% Found in: SOM
card_name('elspeth tirel', 'Elspeth Tirel').
card_type('elspeth tirel', 'Planeswalker — Elspeth').
card_types('elspeth tirel', ['Planeswalker']).
card_subtypes('elspeth tirel', ['Elspeth']).
card_colors('elspeth tirel', ['W']).
card_text('elspeth tirel', '+2: You gain 1 life for each creature you control.\n−2: Put three 1/1 white Soldier creature tokens onto the battlefield.\n−5: Destroy all other permanents except for lands and tokens.').
card_mana_cost('elspeth tirel', ['3', 'W', 'W']).
card_cmc('elspeth tirel', 5).
card_layout('elspeth tirel', 'normal').
card_loyalty('elspeth tirel', 4).

% Found in: ALA, DDF, MD1, MMA
card_name('elspeth, knight-errant', 'Elspeth, Knight-Errant').
card_type('elspeth, knight-errant', 'Planeswalker — Elspeth').
card_types('elspeth, knight-errant', ['Planeswalker']).
card_subtypes('elspeth, knight-errant', ['Elspeth']).
card_colors('elspeth, knight-errant', ['W']).
card_text('elspeth, knight-errant', '+1: Put a 1/1 white Soldier creature token onto the battlefield.\n+1: Target creature gets +3/+3 and gains flying until end of turn.\n−8: You get an emblem with \"Artifacts, creatures, enchantments, and lands you control have indestructible.\"').
card_mana_cost('elspeth, knight-errant', ['2', 'W', 'W']).
card_cmc('elspeth, knight-errant', 4).
card_layout('elspeth, knight-errant', 'normal').
card_loyalty('elspeth, knight-errant', 4).

% Found in: DDO, THS
card_name('elspeth, sun\'s champion', 'Elspeth, Sun\'s Champion').
card_type('elspeth, sun\'s champion', 'Planeswalker — Elspeth').
card_types('elspeth, sun\'s champion', ['Planeswalker']).
card_subtypes('elspeth, sun\'s champion', ['Elspeth']).
card_colors('elspeth, sun\'s champion', ['W']).
card_text('elspeth, sun\'s champion', '+1: Put three 1/1 white Soldier creature tokens onto the battlefield.\n−3: Destroy all creatures with power 4 or greater.\n−7: You get an emblem with \"Creatures you control get +2/+2 and have flying.\"').
card_mana_cost('elspeth, sun\'s champion', ['4', 'W', 'W']).
card_cmc('elspeth, sun\'s champion', 6).
card_layout('elspeth, sun\'s champion', 'normal').
card_loyalty('elspeth, sun\'s champion', 4).

% Found in: GTC
card_name('elusive krasis', 'Elusive Krasis').
card_type('elusive krasis', 'Creature — Fish Mutant').
card_types('elusive krasis', ['Creature']).
card_subtypes('elusive krasis', ['Fish', 'Mutant']).
card_colors('elusive krasis', ['U', 'G']).
card_text('elusive krasis', 'Elusive Krasis can\'t be blocked.\nEvolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)').
card_mana_cost('elusive krasis', ['1', 'G', 'U']).
card_cmc('elusive krasis', 3).
card_layout('elusive krasis', 'normal').
card_power('elusive krasis', 0).
card_toughness('elusive krasis', 4).

% Found in: DTK
card_name('elusive spellfist', 'Elusive Spellfist').
card_type('elusive spellfist', 'Creature — Human Monk').
card_types('elusive spellfist', ['Creature']).
card_subtypes('elusive spellfist', ['Human', 'Monk']).
card_colors('elusive spellfist', ['U']).
card_text('elusive spellfist', 'Whenever you cast a noncreature spell, Elusive Spellfist gets +1/+0 until end of turn and can\'t be blocked this turn.').
card_mana_cost('elusive spellfist', ['1', 'U']).
card_cmc('elusive spellfist', 2).
card_layout('elusive spellfist', 'normal').
card_power('elusive spellfist', 1).
card_toughness('elusive spellfist', 3).

% Found in: 6ED, POR, VIS
card_name('elven cache', 'Elven Cache').
card_type('elven cache', 'Sorcery').
card_types('elven cache', ['Sorcery']).
card_subtypes('elven cache', []).
card_colors('elven cache', ['G']).
card_text('elven cache', 'Return target card from your graveyard to your hand.').
card_mana_cost('elven cache', ['2', 'G', 'G']).
card_cmc('elven cache', 4).
card_layout('elven cache', 'normal').

% Found in: FEM
card_name('elven fortress', 'Elven Fortress').
card_type('elven fortress', 'Enchantment').
card_types('elven fortress', ['Enchantment']).
card_subtypes('elven fortress', []).
card_colors('elven fortress', ['G']).
card_text('elven fortress', '{1}{G}: Target blocking creature gets +0/+1 until end of turn.').
card_mana_cost('elven fortress', ['G']).
card_cmc('elven fortress', 1).
card_layout('elven fortress', 'normal').

% Found in: FEM, ME2
card_name('elven lyre', 'Elven Lyre').
card_type('elven lyre', 'Artifact').
card_types('elven lyre', ['Artifact']).
card_subtypes('elven lyre', []).
card_colors('elven lyre', []).
card_text('elven lyre', '{1}, {T}, Sacrifice Elven Lyre: Target creature gets +2/+2 until end of turn.').
card_mana_cost('elven lyre', ['2']).
card_cmc('elven lyre', 2).
card_layout('elven lyre', 'normal').
card_reserved('elven lyre').

% Found in: EXO
card_name('elven palisade', 'Elven Palisade').
card_type('elven palisade', 'Enchantment').
card_types('elven palisade', ['Enchantment']).
card_subtypes('elven palisade', []).
card_colors('elven palisade', ['G']).
card_text('elven palisade', 'Sacrifice a Forest: Target attacking creature gets -3/-0 until end of turn.').
card_mana_cost('elven palisade', ['G']).
card_cmc('elven palisade', 1).
card_layout('elven palisade', 'normal').

% Found in: 10E, 4ED, 5ED, 6ED, DPA, ITP, LEG, ONS, RQS
card_name('elven riders', 'Elven Riders').
card_type('elven riders', 'Creature — Elf').
card_types('elven riders', ['Creature']).
card_subtypes('elven riders', ['Elf']).
card_colors('elven riders', ['G']).
card_text('elven riders', 'Elven Riders can\'t be blocked except by Walls and/or creatures with flying.').
card_mana_cost('elven riders', ['3', 'G', 'G']).
card_cmc('elven riders', 5).
card_layout('elven riders', 'normal').
card_power('elven riders', 3).
card_toughness('elven riders', 3).

% Found in: STH, TPR
card_name('elven rite', 'Elven Rite').
card_type('elven rite', 'Sorcery').
card_types('elven rite', ['Sorcery']).
card_subtypes('elven rite', []).
card_colors('elven rite', ['G']).
card_text('elven rite', 'Distribute two +1/+1 counters among one or two target creatures.').
card_mana_cost('elven rite', ['1', 'G']).
card_cmc('elven rite', 2).
card_layout('elven rite', 'normal').

% Found in: TMP
card_name('elven warhounds', 'Elven Warhounds').
card_type('elven warhounds', 'Creature — Hound').
card_types('elven warhounds', ['Creature']).
card_subtypes('elven warhounds', ['Hound']).
card_colors('elven warhounds', ['G']).
card_text('elven warhounds', 'Whenever Elven Warhounds becomes blocked by a creature, put that creature on top of its owner\'s library.').
card_mana_cost('elven warhounds', ['3', 'G']).
card_cmc('elven warhounds', 4).
card_layout('elven warhounds', 'normal').
card_power('elven warhounds', 2).
card_toughness('elven warhounds', 2).

% Found in: DDJ, DRK, ME3, RAV, pFNM
card_name('elves of deep shadow', 'Elves of Deep Shadow').
card_type('elves of deep shadow', 'Creature — Elf Druid').
card_types('elves of deep shadow', ['Creature']).
card_subtypes('elves of deep shadow', ['Elf', 'Druid']).
card_colors('elves of deep shadow', ['G']).
card_text('elves of deep shadow', '{T}: Add {B} to your mana pool. Elves of Deep Shadow deals 1 damage to you.').
card_mana_cost('elves of deep shadow', ['G']).
card_cmc('elves of deep shadow', 1).
card_layout('elves of deep shadow', 'normal').
card_power('elves of deep shadow', 1).
card_toughness('elves of deep shadow', 1).

% Found in: CMD, CNS, SCG, VMA, pARL
card_name('elvish aberration', 'Elvish Aberration').
card_type('elvish aberration', 'Creature — Elf Mutant').
card_types('elvish aberration', ['Creature']).
card_subtypes('elvish aberration', ['Elf', 'Mutant']).
card_colors('elvish aberration', ['G']).
card_text('elvish aberration', '{T}: Add {G}{G}{G} to your mana pool.\nForestcycling {2} ({2}, Discard this card: Search your library for a Forest card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('elvish aberration', ['5', 'G']).
card_cmc('elvish aberration', 6).
card_layout('elvish aberration', 'normal').
card_power('elvish aberration', 4).
card_toughness('elvish aberration', 5).

% Found in: C14, M10, M11, M12, M13
card_name('elvish archdruid', 'Elvish Archdruid').
card_type('elvish archdruid', 'Creature — Elf Druid').
card_types('elvish archdruid', ['Creature']).
card_subtypes('elvish archdruid', ['Elf', 'Druid']).
card_colors('elvish archdruid', ['G']).
card_text('elvish archdruid', 'Other Elf creatures you control get +1/+1.\n{T}: Add {G} to your mana pool for each Elf you control.').
card_mana_cost('elvish archdruid', ['1', 'G', 'G']).
card_cmc('elvish archdruid', 3).
card_layout('elvish archdruid', 'normal').
card_power('elvish archdruid', 2).
card_toughness('elvish archdruid', 2).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, CED, CEI, ITP, LEA, LEB, RQS
card_name('elvish archers', 'Elvish Archers').
card_type('elvish archers', 'Creature — Elf Archer').
card_types('elvish archers', ['Creature']).
card_subtypes('elvish archers', ['Elf', 'Archer']).
card_colors('elvish archers', ['G']).
card_text('elvish archers', 'First strike').
card_mana_cost('elvish archers', ['1', 'G']).
card_cmc('elvish archers', 2).
card_layout('elvish archers', 'normal').
card_power('elvish archers', 2).
card_toughness('elvish archers', 1).

% Found in: 9ED, ALL, DKM
card_name('elvish bard', 'Elvish Bard').
card_type('elvish bard', 'Creature — Elf Shaman').
card_types('elvish bard', ['Creature']).
card_subtypes('elvish bard', ['Elf', 'Shaman']).
card_colors('elvish bard', ['G']).
card_text('elvish bard', 'All creatures able to block Elvish Bard do so.').
card_mana_cost('elvish bard', ['3', 'G', 'G']).
card_cmc('elvish bard', 5).
card_layout('elvish bard', 'normal').
card_power('elvish bard', 2).
card_toughness('elvish bard', 4).

% Found in: 10E, 9ED, EXO
card_name('elvish berserker', 'Elvish Berserker').
card_type('elvish berserker', 'Creature — Elf Berserker').
card_types('elvish berserker', ['Creature']).
card_subtypes('elvish berserker', ['Elf', 'Berserker']).
card_colors('elvish berserker', ['G']).
card_text('elvish berserker', 'Whenever Elvish Berserker becomes blocked, it gets +1/+1 until end of turn for each creature blocking it.').
card_mana_cost('elvish berserker', ['G']).
card_cmc('elvish berserker', 1).
card_layout('elvish berserker', 'normal').
card_power('elvish berserker', 1).
card_toughness('elvish berserker', 1).

% Found in: LRW
card_name('elvish branchbender', 'Elvish Branchbender').
card_type('elvish branchbender', 'Creature — Elf Druid').
card_types('elvish branchbender', ['Creature']).
card_subtypes('elvish branchbender', ['Elf', 'Druid']).
card_colors('elvish branchbender', ['G']).
card_text('elvish branchbender', '{T}: Until end of turn, target Forest becomes an X/X Treefolk creature in addition to its other types, where X is the number of Elves you control.').
card_mana_cost('elvish branchbender', ['2', 'G']).
card_cmc('elvish branchbender', 3).
card_layout('elvish branchbender', 'normal').
card_power('elvish branchbender', 2).
card_toughness('elvish branchbender', 2).

% Found in: 10E, 7ED, 8ED, 9ED, DPA, INV, pSUS
card_name('elvish champion', 'Elvish Champion').
card_type('elvish champion', 'Creature — Elf').
card_types('elvish champion', ['Creature']).
card_subtypes('elvish champion', ['Elf']).
card_colors('elvish champion', ['G']).
card_text('elvish champion', 'Other Elf creatures get +1/+1 and have forestwalk. (They can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('elvish champion', ['1', 'G', 'G']).
card_cmc('elvish champion', 3).
card_layout('elvish champion', 'normal').
card_power('elvish champion', 2).
card_toughness('elvish champion', 2).

% Found in: VAN
card_name('elvish champion avatar', 'Elvish Champion Avatar').
card_type('elvish champion avatar', 'Vanguard').
card_types('elvish champion avatar', ['Vanguard']).
card_subtypes('elvish champion avatar', []).
card_colors('elvish champion avatar', []).
card_text('elvish champion avatar', 'You begin the game with a 1/1 green Elf creature token on the battlefield. It has \"{T}: Add {G} to your mana pool.\"').
card_layout('elvish champion avatar', 'vanguard').

% Found in: DD3_EVG, DPA, EVG, LRW
card_name('elvish eulogist', 'Elvish Eulogist').
card_type('elvish eulogist', 'Creature — Elf Shaman').
card_types('elvish eulogist', ['Creature']).
card_subtypes('elvish eulogist', ['Elf', 'Shaman']).
card_colors('elvish eulogist', ['G']).
card_text('elvish eulogist', 'Sacrifice Elvish Eulogist: You gain 1 life for each Elf card in your graveyard.').
card_mana_cost('elvish eulogist', ['G']).
card_cmc('elvish eulogist', 1).
card_layout('elvish eulogist', 'normal').
card_power('elvish eulogist', 1).
card_toughness('elvish eulogist', 1).

% Found in: FEM, ME2
card_name('elvish farmer', 'Elvish Farmer').
card_type('elvish farmer', 'Creature — Elf').
card_types('elvish farmer', ['Creature']).
card_subtypes('elvish farmer', ['Elf']).
card_colors('elvish farmer', ['G']).
card_text('elvish farmer', 'At the beginning of your upkeep, put a spore counter on Elvish Farmer.\nRemove three spore counters from Elvish Farmer: Put a 1/1 green Saproling creature token onto the battlefield.\nSacrifice a Saproling: You gain 2 life.').
card_mana_cost('elvish farmer', ['1', 'G']).
card_cmc('elvish farmer', 2).
card_layout('elvish farmer', 'normal').
card_power('elvish farmer', 0).
card_toughness('elvish farmer', 2).
card_reserved('elvish farmer').

% Found in: TMP, TPR
card_name('elvish fury', 'Elvish Fury').
card_type('elvish fury', 'Instant').
card_types('elvish fury', ['Instant']).
card_subtypes('elvish fury', []).
card_colors('elvish fury', ['G']).
card_text('elvish fury', 'Buyback {4} (You may pay an additional {4} as you cast this spell. If you do, put this card into your hand as it resolves.)\nTarget creature gets +2/+2 until end of turn.').
card_mana_cost('elvish fury', ['G']).
card_cmc('elvish fury', 1).
card_layout('elvish fury', 'normal').

% Found in: ONS
card_name('elvish guidance', 'Elvish Guidance').
card_type('elvish guidance', 'Enchantment — Aura').
card_types('elvish guidance', ['Enchantment']).
card_subtypes('elvish guidance', ['Aura']).
card_colors('elvish guidance', ['G']).
card_text('elvish guidance', 'Enchant land\nWhenever enchanted land is tapped for mana, its controller adds {G} to his or her mana pool for each Elf on the battlefield (in addition to the mana the land produces).').
card_mana_cost('elvish guidance', ['2', 'G']).
card_cmc('elvish guidance', 3).
card_layout('elvish guidance', 'normal').

% Found in: LRW
card_name('elvish handservant', 'Elvish Handservant').
card_type('elvish handservant', 'Creature — Elf Warrior').
card_types('elvish handservant', ['Creature']).
card_subtypes('elvish handservant', ['Elf', 'Warrior']).
card_colors('elvish handservant', ['G']).
card_text('elvish handservant', 'Whenever a player casts a Giant spell, you may put a +1/+1 counter on Elvish Handservant.').
card_mana_cost('elvish handservant', ['G']).
card_cmc('elvish handservant', 1).
card_layout('elvish handservant', 'normal').
card_power('elvish handservant', 1).
card_toughness('elvish handservant', 1).

% Found in: DD3_EVG, EVG, LRW
card_name('elvish harbinger', 'Elvish Harbinger').
card_type('elvish harbinger', 'Creature — Elf Druid').
card_types('elvish harbinger', ['Creature']).
card_subtypes('elvish harbinger', ['Elf', 'Druid']).
card_colors('elvish harbinger', ['G']).
card_text('elvish harbinger', 'When Elvish Harbinger enters the battlefield, you may search your library for an Elf card, reveal it, then shuffle your library and put that card on top of it.\n{T}: Add one mana of any color to your mana pool.').
card_mana_cost('elvish harbinger', ['2', 'G']).
card_cmc('elvish harbinger', 3).
card_layout('elvish harbinger', 'normal').
card_power('elvish harbinger', 1).
card_toughness('elvish harbinger', 2).

% Found in: ICE
card_name('elvish healer', 'Elvish Healer').
card_type('elvish healer', 'Creature — Elf Cleric').
card_types('elvish healer', ['Creature']).
card_subtypes('elvish healer', ['Elf', 'Cleric']).
card_colors('elvish healer', ['W']).
card_text('elvish healer', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn. If that creature is green, prevent the next 2 damage instead.').
card_mana_cost('elvish healer', ['2', 'W']).
card_cmc('elvish healer', 3).
card_layout('elvish healer', 'normal').
card_power('elvish healer', 1).
card_toughness('elvish healer', 2).

% Found in: USG
card_name('elvish herder', 'Elvish Herder').
card_type('elvish herder', 'Creature — Elf').
card_types('elvish herder', ['Creature']).
card_subtypes('elvish herder', ['Elf']).
card_colors('elvish herder', ['G']).
card_text('elvish herder', '{G}: Target creature gains trample until end of turn.').
card_mana_cost('elvish herder', ['G']).
card_cmc('elvish herder', 1).
card_layout('elvish herder', 'normal').
card_power('elvish herder', 1).
card_toughness('elvish herder', 1).

% Found in: SHM
card_name('elvish hexhunter', 'Elvish Hexhunter').
card_type('elvish hexhunter', 'Creature — Elf Shaman').
card_types('elvish hexhunter', ['Creature']).
card_subtypes('elvish hexhunter', ['Elf', 'Shaman']).
card_colors('elvish hexhunter', ['W', 'G']).
card_text('elvish hexhunter', '{G/W}, {T}, Sacrifice Elvish Hexhunter: Destroy target enchantment.').
card_mana_cost('elvish hexhunter', ['G/W']).
card_cmc('elvish hexhunter', 1).
card_layout('elvish hexhunter', 'normal').
card_power('elvish hexhunter', 1).
card_toughness('elvish hexhunter', 1).

% Found in: UNH
card_name('elvish house party', 'Elvish House Party').
card_type('elvish house party', 'Creature — Elf Rogue').
card_types('elvish house party', ['Creature']).
card_subtypes('elvish house party', ['Elf', 'Rogue']).
card_colors('elvish house party', ['G']).
card_text('elvish house party', 'Elvish House Party\'s power and toughness are each equal to the current hour, using the twelve-hour system.').
card_mana_cost('elvish house party', ['4', 'G', 'G']).
card_cmc('elvish house party', 6).
card_layout('elvish house party', 'normal').
card_power('elvish house party', '*').
card_toughness('elvish house party', '*').

% Found in: FEM, ME2
card_name('elvish hunter', 'Elvish Hunter').
card_type('elvish hunter', 'Creature — Elf Archer').
card_types('elvish hunter', ['Creature']).
card_subtypes('elvish hunter', ['Elf', 'Archer']).
card_colors('elvish hunter', ['G']).
card_text('elvish hunter', '{1}{G}, {T}: Target creature doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('elvish hunter', ['1', 'G']).
card_cmc('elvish hunter', 2).
card_layout('elvish hunter', 'normal').
card_power('elvish hunter', 1).
card_toughness('elvish hunter', 1).

% Found in: UGL
card_name('elvish impersonators', 'Elvish Impersonators').
card_type('elvish impersonators', 'Creature — Elves').
card_types('elvish impersonators', ['Creature']).
card_subtypes('elvish impersonators', ['Elves']).
card_colors('elvish impersonators', ['G']).
card_text('elvish impersonators', 'When you play Elvish Impersonators, roll two six-sided dice one after the other. Elvish Impersonators comes into play with power equal to the first die roll and toughness equal to the second.').
card_mana_cost('elvish impersonators', ['3', 'G']).
card_cmc('elvish impersonators', 4).
card_layout('elvish impersonators', 'normal').
card_power('elvish impersonators', '*').
card_toughness('elvish impersonators', '*').

% Found in: UDS
card_name('elvish lookout', 'Elvish Lookout').
card_type('elvish lookout', 'Creature — Elf').
card_types('elvish lookout', ['Creature']).
card_subtypes('elvish lookout', ['Elf']).
card_colors('elvish lookout', ['G']).
card_text('elvish lookout', 'Shroud (This creature can\'t be the target of spells or abilities.)').
card_mana_cost('elvish lookout', ['G']).
card_cmc('elvish lookout', 1).
card_layout('elvish lookout', 'normal').
card_power('elvish lookout', 1).
card_toughness('elvish lookout', 1).

% Found in: 7ED, 8ED, BRB, USG, pSUS
card_name('elvish lyrist', 'Elvish Lyrist').
card_type('elvish lyrist', 'Creature — Elf').
card_types('elvish lyrist', ['Creature']).
card_subtypes('elvish lyrist', ['Elf']).
card_colors('elvish lyrist', ['G']).
card_text('elvish lyrist', '{G}, {T}, Sacrifice Elvish Lyrist: Destroy target enchantment.').
card_mana_cost('elvish lyrist', ['G']).
card_cmc('elvish lyrist', 1).
card_layout('elvish lyrist', 'normal').
card_power('elvish lyrist', 1).
card_toughness('elvish lyrist', 1).

% Found in: C14, M14, M15, pFNM
card_name('elvish mystic', 'Elvish Mystic').
card_type('elvish mystic', 'Creature — Elf Druid').
card_types('elvish mystic', ['Creature']).
card_subtypes('elvish mystic', ['Elf', 'Druid']).
card_colors('elvish mystic', ['G']).
card_text('elvish mystic', '{T}: Add {G} to your mana pool.').
card_mana_cost('elvish mystic', ['G']).
card_cmc('elvish mystic', 1).
card_layout('elvish mystic', 'normal').
card_power('elvish mystic', 1).
card_toughness('elvish mystic', 1).

% Found in: ONS
card_name('elvish pathcutter', 'Elvish Pathcutter').
card_type('elvish pathcutter', 'Creature — Elf Scout').
card_types('elvish pathcutter', ['Creature']).
card_subtypes('elvish pathcutter', ['Elf', 'Scout']).
card_colors('elvish pathcutter', ['G']).
card_text('elvish pathcutter', '{2}{G}: Target Elf creature gains forestwalk until end of turn. (It can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('elvish pathcutter', ['3', 'G']).
card_cmc('elvish pathcutter', 4).
card_layout('elvish pathcutter', 'normal').
card_power('elvish pathcutter', 1).
card_toughness('elvish pathcutter', 2).

% Found in: 8ED, ONS
card_name('elvish pioneer', 'Elvish Pioneer').
card_type('elvish pioneer', 'Creature — Elf Druid').
card_types('elvish pioneer', ['Creature']).
card_subtypes('elvish pioneer', ['Elf', 'Druid']).
card_colors('elvish pioneer', ['G']).
card_text('elvish pioneer', 'When Elvish Pioneer enters the battlefield, you may put a basic land card from your hand onto the battlefield tapped.').
card_mana_cost('elvish pioneer', ['G']).
card_cmc('elvish pioneer', 1).
card_layout('elvish pioneer', 'normal').
card_power('elvish pioneer', 1).
card_toughness('elvish pioneer', 1).

% Found in: 10E, 7ED, 8ED, 9ED, M10, UDS
card_name('elvish piper', 'Elvish Piper').
card_type('elvish piper', 'Creature — Elf Shaman').
card_types('elvish piper', ['Creature']).
card_subtypes('elvish piper', ['Elf', 'Shaman']).
card_colors('elvish piper', ['G']).
card_text('elvish piper', '{G}, {T}: You may put a creature card from your hand onto the battlefield.').
card_mana_cost('elvish piper', ['3', 'G']).
card_cmc('elvish piper', 4).
card_layout('elvish piper', 'normal').
card_power('elvish piper', 1).
card_toughness('elvish piper', 1).

% Found in: DD3_EVG, DPA, EVG, LRW
card_name('elvish promenade', 'Elvish Promenade').
card_type('elvish promenade', 'Tribal Sorcery — Elf').
card_types('elvish promenade', ['Tribal', 'Sorcery']).
card_subtypes('elvish promenade', ['Elf']).
card_colors('elvish promenade', ['G']).
card_text('elvish promenade', 'Put a 1/1 green Elf Warrior creature token onto the battlefield for each Elf you control.').
card_mana_cost('elvish promenade', ['3', 'G']).
card_cmc('elvish promenade', 4).
card_layout('elvish promenade', 'normal').

% Found in: ALL, ME2, POR
card_name('elvish ranger', 'Elvish Ranger').
card_type('elvish ranger', 'Creature — Elf').
card_types('elvish ranger', ['Creature']).
card_subtypes('elvish ranger', ['Elf']).
card_colors('elvish ranger', ['G']).
card_text('elvish ranger', '').
card_mana_cost('elvish ranger', ['2', 'G']).
card_cmc('elvish ranger', 3).
card_layout('elvish ranger', 'normal').
card_power('elvish ranger', 4).
card_toughness('elvish ranger', 1).

% Found in: FEM
card_name('elvish scout', 'Elvish Scout').
card_type('elvish scout', 'Creature — Elf Scout').
card_types('elvish scout', ['Creature']).
card_subtypes('elvish scout', ['Elf', 'Scout']).
card_colors('elvish scout', ['G']).
card_text('elvish scout', '{G}, {T}: Untap target attacking creature you control. Prevent all combat damage that would be dealt to and dealt by it this turn.').
card_mana_cost('elvish scout', ['G']).
card_cmc('elvish scout', 1).
card_layout('elvish scout', 'normal').
card_power('elvish scout', 1).
card_toughness('elvish scout', 1).

% Found in: 8ED, ONS
card_name('elvish scrapper', 'Elvish Scrapper').
card_type('elvish scrapper', 'Creature — Elf').
card_types('elvish scrapper', ['Creature']).
card_subtypes('elvish scrapper', ['Elf']).
card_colors('elvish scrapper', ['G']).
card_text('elvish scrapper', '{G}, {T}, Sacrifice Elvish Scrapper: Destroy target artifact.').
card_mana_cost('elvish scrapper', ['G']).
card_cmc('elvish scrapper', 1).
card_layout('elvish scrapper', 'normal').
card_power('elvish scrapper', 1).
card_toughness('elvish scrapper', 1).

% Found in: C13, C14, RAV
card_name('elvish skysweeper', 'Elvish Skysweeper').
card_type('elvish skysweeper', 'Creature — Elf Warrior').
card_types('elvish skysweeper', ['Creature']).
card_subtypes('elvish skysweeper', ['Elf', 'Warrior']).
card_colors('elvish skysweeper', ['G']).
card_text('elvish skysweeper', '{4}{G}, Sacrifice a creature: Destroy target creature with flying.').
card_mana_cost('elvish skysweeper', ['G']).
card_cmc('elvish skysweeper', 1).
card_layout('elvish skysweeper', 'normal').
card_power('elvish skysweeper', 1).
card_toughness('elvish skysweeper', 1).

% Found in: LGN
card_name('elvish soultiller', 'Elvish Soultiller').
card_type('elvish soultiller', 'Creature — Elf Mutant').
card_types('elvish soultiller', ['Creature']).
card_subtypes('elvish soultiller', ['Elf', 'Mutant']).
card_colors('elvish soultiller', ['G']).
card_text('elvish soultiller', 'When Elvish Soultiller dies, choose a creature type. Shuffle all creature cards of that type from your graveyard into your library.').
card_mana_cost('elvish soultiller', ['3', 'G', 'G']).
card_cmc('elvish soultiller', 5).
card_layout('elvish soultiller', 'normal').
card_power('elvish soultiller', 5).
card_toughness('elvish soultiller', 4).

% Found in: ALL, ME2
card_name('elvish spirit guide', 'Elvish Spirit Guide').
card_type('elvish spirit guide', 'Creature — Elf Spirit').
card_types('elvish spirit guide', ['Creature']).
card_subtypes('elvish spirit guide', ['Elf', 'Spirit']).
card_colors('elvish spirit guide', ['G']).
card_text('elvish spirit guide', 'Exile Elvish Spirit Guide from your hand: Add {G} to your mana pool.').
card_mana_cost('elvish spirit guide', ['2', 'G']).
card_cmc('elvish spirit guide', 3).
card_layout('elvish spirit guide', 'normal').
card_power('elvish spirit guide', 2).
card_toughness('elvish spirit guide', 2).

% Found in: ONS
card_name('elvish vanguard', 'Elvish Vanguard').
card_type('elvish vanguard', 'Creature — Elf Warrior').
card_types('elvish vanguard', ['Creature']).
card_subtypes('elvish vanguard', ['Elf', 'Warrior']).
card_colors('elvish vanguard', ['G']).
card_text('elvish vanguard', 'Whenever another Elf enters the battlefield, put a +1/+1 counter on Elvish Vanguard.').
card_mana_cost('elvish vanguard', ['1', 'G']).
card_cmc('elvish vanguard', 2).
card_layout('elvish vanguard', 'normal').
card_power('elvish vanguard', 1).
card_toughness('elvish vanguard', 1).

% Found in: ALA, C14, DPA, M10, M13, ORI, pFNM
card_name('elvish visionary', 'Elvish Visionary').
card_type('elvish visionary', 'Creature — Elf Shaman').
card_types('elvish visionary', ['Creature']).
card_subtypes('elvish visionary', ['Elf', 'Shaman']).
card_colors('elvish visionary', ['G']).
card_text('elvish visionary', 'When Elvish Visionary enters the battlefield, draw a card.').
card_mana_cost('elvish visionary', ['1', 'G']).
card_cmc('elvish visionary', 2).
card_layout('elvish visionary', 'normal').
card_power('elvish visionary', 1).
card_toughness('elvish visionary', 1).

% Found in: 9ED, DD3_EVG, DPA, EVG, MOR, ONS
card_name('elvish warrior', 'Elvish Warrior').
card_type('elvish warrior', 'Creature — Elf Warrior').
card_types('elvish warrior', ['Creature']).
card_subtypes('elvish warrior', ['Elf', 'Warrior']).
card_colors('elvish warrior', ['G']).
card_text('elvish warrior', '').
card_mana_cost('elvish warrior', ['G', 'G']).
card_cmc('elvish warrior', 2).
card_layout('elvish warrior', 'normal').
card_power('elvish warrior', 2).
card_toughness('elvish warrior', 3).

% Found in: AVR
card_name('emancipation angel', 'Emancipation Angel').
card_type('emancipation angel', 'Creature — Angel').
card_types('emancipation angel', ['Creature']).
card_subtypes('emancipation angel', ['Angel']).
card_colors('emancipation angel', ['W']).
card_text('emancipation angel', 'Flying\nWhen Emancipation Angel enters the battlefield, return a permanent you control to its owner\'s hand.').
card_mana_cost('emancipation angel', ['1', 'W', 'W']).
card_cmc('emancipation angel', 3).
card_layout('emancipation angel', 'normal').
card_power('emancipation angel', 3).
card_toughness('emancipation angel', 3).

% Found in: LGN
card_name('embalmed brawler', 'Embalmed Brawler').
card_type('embalmed brawler', 'Creature — Zombie').
card_types('embalmed brawler', ['Creature']).
card_subtypes('embalmed brawler', ['Zombie']).
card_colors('embalmed brawler', ['B']).
card_text('embalmed brawler', 'Amplify 1 (As this creature enters the battlefield, put a +1/+1 counter on it for each Zombie card you reveal in your hand.)\nWhenever Embalmed Brawler attacks or blocks, you lose 1 life for each +1/+1 counter on it.').
card_mana_cost('embalmed brawler', ['2', 'B']).
card_cmc('embalmed brawler', 3).
card_layout('embalmed brawler', 'normal').
card_power('embalmed brawler', 2).
card_toughness('embalmed brawler', 2).

% Found in: MMQ
card_name('embargo', 'Embargo').
card_type('embargo', 'Enchantment').
card_types('embargo', ['Enchantment']).
card_subtypes('embargo', []).
card_colors('embargo', ['U']).
card_text('embargo', 'Nonland permanents don\'t untap during their controllers\' untap steps.\nAt the beginning of your upkeep, you lose 2 life.').
card_mana_cost('embargo', ['3', 'U']).
card_cmc('embargo', 4).
card_layout('embargo', 'normal').

% Found in: GTC, ODY
card_name('ember beast', 'Ember Beast').
card_type('ember beast', 'Creature — Beast').
card_types('ember beast', ['Creature']).
card_subtypes('ember beast', ['Beast']).
card_colors('ember beast', ['R']).
card_text('ember beast', 'Ember Beast can\'t attack or block alone.').
card_mana_cost('ember beast', ['2', 'R']).
card_cmc('ember beast', 3).
card_layout('ember beast', 'normal').
card_power('ember beast', 3).
card_toughness('ember beast', 4).

% Found in: SHM
card_name('ember gale', 'Ember Gale').
card_type('ember gale', 'Sorcery').
card_types('ember gale', ['Sorcery']).
card_subtypes('ember gale', []).
card_colors('ember gale', ['R']).
card_text('ember gale', 'Creatures target player controls can\'t block this turn. Ember Gale deals 1 damage to each white and/or blue creature that player controls.').
card_mana_cost('ember gale', ['3', 'R']).
card_cmc('ember gale', 4).
card_layout('ember gale', 'normal').

% Found in: M11
card_name('ember hauler', 'Ember Hauler').
card_type('ember hauler', 'Creature — Goblin').
card_types('ember hauler', ['Creature']).
card_subtypes('ember hauler', ['Goblin']).
card_colors('ember hauler', ['R']).
card_text('ember hauler', '{1}, Sacrifice Ember Hauler: Ember Hauler deals 2 damage to target creature or player.').
card_mana_cost('ember hauler', ['R', 'R']).
card_cmc('ember hauler', 2).
card_layout('ember hauler', 'normal').
card_power('ember hauler', 2).
card_toughness('ember hauler', 2).

% Found in: JUD
card_name('ember shot', 'Ember Shot').
card_type('ember shot', 'Instant').
card_types('ember shot', ['Instant']).
card_subtypes('ember shot', []).
card_colors('ember shot', ['R']).
card_text('ember shot', 'Ember Shot deals 3 damage to target creature or player.\nDraw a card.').
card_mana_cost('ember shot', ['6', 'R']).
card_cmc('ember shot', 7).
card_layout('ember shot', 'normal').

% Found in: THS, pPRE
card_name('ember swallower', 'Ember Swallower').
card_type('ember swallower', 'Creature — Elemental').
card_types('ember swallower', ['Creature']).
card_subtypes('ember swallower', ['Elemental']).
card_colors('ember swallower', ['R']).
card_text('ember swallower', '{5}{R}{R}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)\nWhen Ember Swallower becomes monstrous, each player sacrifices three lands.').
card_mana_cost('ember swallower', ['2', 'R', 'R']).
card_cmc('ember swallower', 4).
card_layout('ember swallower', 'normal').
card_power('ember swallower', 4).
card_toughness('ember swallower', 5).

% Found in: CON
card_name('ember weaver', 'Ember Weaver').
card_type('ember weaver', 'Creature — Spider').
card_types('ember weaver', ['Creature']).
card_subtypes('ember weaver', ['Spider']).
card_colors('ember weaver', ['G']).
card_text('ember weaver', 'Reach (This creature can block creatures with flying.)\nAs long as you control a red permanent, Ember Weaver gets +1/+0 and has first strike.').
card_mana_cost('ember weaver', ['2', 'G']).
card_cmc('ember weaver', 3).
card_layout('ember weaver', 'normal').
card_power('ember weaver', 2).
card_toughness('ember weaver', 3).

% Found in: CHK
card_name('ember-fist zubera', 'Ember-Fist Zubera').
card_type('ember-fist zubera', 'Creature — Zubera Spirit').
card_types('ember-fist zubera', ['Creature']).
card_subtypes('ember-fist zubera', ['Zubera', 'Spirit']).
card_colors('ember-fist zubera', ['R']).
card_text('ember-fist zubera', 'When Ember-Fist Zubera dies, it deals damage to target creature or player equal to the number of Zubera that died this turn.').
card_mana_cost('ember-fist zubera', ['1', 'R']).
card_cmc('ember-fist zubera', 2).
card_layout('ember-fist zubera', 'normal').
card_power('ember-fist zubera', 1).
card_toughness('ember-fist zubera', 2).

% Found in: ONS
card_name('embermage goblin', 'Embermage Goblin').
card_type('embermage goblin', 'Creature — Goblin Wizard').
card_types('embermage goblin', ['Creature']).
card_subtypes('embermage goblin', ['Goblin', 'Wizard']).
card_colors('embermage goblin', ['R']).
card_text('embermage goblin', 'When Embermage Goblin enters the battlefield, you may search your library for a card named Embermage Goblin, reveal it, and put it into your hand. If you do, shuffle your library.\n{T}: Embermage Goblin deals 1 damage to target creature or player.').
card_mana_cost('embermage goblin', ['3', 'R']).
card_cmc('embermage goblin', 4).
card_layout('embermage goblin', 'normal').
card_power('embermage goblin', 1).
card_toughness('embermage goblin', 1).

% Found in: ORI
card_name('embermaw hellion', 'Embermaw Hellion').
card_type('embermaw hellion', 'Creature — Hellion').
card_types('embermaw hellion', ['Creature']).
card_subtypes('embermaw hellion', ['Hellion']).
card_colors('embermaw hellion', ['R']).
card_text('embermaw hellion', 'Trample (This creature can deal excess combat damage to defending player or planeswalker while attacking.)\nIf another red source you control would deal damage to a permanent or player, it deals that much damage plus 1 to that permanent or player instead.').
card_mana_cost('embermaw hellion', ['3', 'R', 'R']).
card_cmc('embermaw hellion', 5).
card_layout('embermaw hellion', 'normal').
card_power('embermaw hellion', 4).
card_toughness('embermaw hellion', 5).

% Found in: SOM
card_name('embersmith', 'Embersmith').
card_type('embersmith', 'Creature — Human Artificer').
card_types('embersmith', ['Creature']).
card_subtypes('embersmith', ['Human', 'Artificer']).
card_colors('embersmith', ['R']).
card_text('embersmith', 'Whenever you cast an artifact spell, you may pay {1}. If you do, Embersmith deals 1 damage to target creature or player.').
card_mana_cost('embersmith', ['1', 'R']).
card_cmc('embersmith', 2).
card_layout('embersmith', 'normal').
card_power('embersmith', 2).
card_toughness('embersmith', 1).

% Found in: SHM
card_name('emberstrike duo', 'Emberstrike Duo').
card_type('emberstrike duo', 'Creature — Elemental Warrior Shaman').
card_types('emberstrike duo', ['Creature']).
card_subtypes('emberstrike duo', ['Elemental', 'Warrior', 'Shaman']).
card_colors('emberstrike duo', ['B', 'R']).
card_text('emberstrike duo', 'Whenever you cast a black spell, Emberstrike Duo gets +1/+1 until end of turn.\nWhenever you cast a red spell, Emberstrike Duo gains first strike until end of turn.').
card_mana_cost('emberstrike duo', ['1', 'B/R']).
card_cmc('emberstrike duo', 2).
card_layout('emberstrike duo', 'normal').
card_power('emberstrike duo', 1).
card_toughness('emberstrike duo', 1).

% Found in: DD3_EVG, EVG, FUT
card_name('emberwilde augur', 'Emberwilde Augur').
card_type('emberwilde augur', 'Creature — Goblin Shaman').
card_types('emberwilde augur', ['Creature']).
card_subtypes('emberwilde augur', ['Goblin', 'Shaman']).
card_colors('emberwilde augur', ['R']).
card_text('emberwilde augur', 'Sacrifice Emberwilde Augur: Emberwilde Augur deals 3 damage to target player. Activate this ability only during your upkeep.').
card_mana_cost('emberwilde augur', ['1', 'R']).
card_cmc('emberwilde augur', 2).
card_layout('emberwilde augur', 'normal').
card_power('emberwilde augur', 2).
card_toughness('emberwilde augur', 1).

% Found in: MIR
card_name('emberwilde caliph', 'Emberwilde Caliph').
card_type('emberwilde caliph', 'Creature — Djinn').
card_types('emberwilde caliph', ['Creature']).
card_subtypes('emberwilde caliph', ['Djinn']).
card_colors('emberwilde caliph', ['U', 'R']).
card_text('emberwilde caliph', 'Flying, trample\nEmberwilde Caliph attacks each turn if able.\nWhenever Emberwilde Caliph deals damage, you lose that much life.').
card_mana_cost('emberwilde caliph', ['2', 'U', 'R']).
card_cmc('emberwilde caliph', 4).
card_layout('emberwilde caliph', 'normal').
card_power('emberwilde caliph', 4).
card_toughness('emberwilde caliph', 4).
card_reserved('emberwilde caliph').

% Found in: MIR
card_name('emberwilde djinn', 'Emberwilde Djinn').
card_type('emberwilde djinn', 'Creature — Djinn').
card_types('emberwilde djinn', ['Creature']).
card_subtypes('emberwilde djinn', ['Djinn']).
card_colors('emberwilde djinn', ['R']).
card_text('emberwilde djinn', 'Flying\nAt the beginning of each player\'s upkeep, that player may pay {R}{R} or 2 life. If he or she does, the player gains control of Emberwilde Djinn.').
card_mana_cost('emberwilde djinn', ['2', 'R', 'R']).
card_cmc('emberwilde djinn', 4).
card_layout('emberwilde djinn', 'normal').
card_power('emberwilde djinn', 5).
card_toughness('emberwilde djinn', 4).
card_reserved('emberwilde djinn').

% Found in: APC
card_name('emblazoned golem', 'Emblazoned Golem').
card_type('emblazoned golem', 'Artifact Creature — Golem').
card_types('emblazoned golem', ['Artifact', 'Creature']).
card_subtypes('emblazoned golem', ['Golem']).
card_colors('emblazoned golem', []).
card_text('emblazoned golem', 'Kicker {X} (You may pay an additional {X} as you cast this spell.)\nSpend only colored mana on X. No more than one mana of each color may be spent this way.\nIf Emblazoned Golem was kicked, it enters the battlefield with X +1/+1 counters on it.').
card_mana_cost('emblazoned golem', ['2']).
card_cmc('emblazoned golem', 2).
card_layout('emblazoned golem', 'normal').
card_power('emblazoned golem', 1).
card_toughness('emblazoned golem', 2).

% Found in: FUT
card_name('emblem of the warmind', 'Emblem of the Warmind').
card_type('emblem of the warmind', 'Enchantment — Aura').
card_types('emblem of the warmind', ['Enchantment']).
card_subtypes('emblem of the warmind', ['Aura']).
card_colors('emblem of the warmind', ['R']).
card_text('emblem of the warmind', 'Enchant creature you control\nCreatures you control have haste.').
card_mana_cost('emblem of the warmind', ['1', 'R']).
card_cmc('emblem of the warmind', 2).
card_layout('emblem of the warmind', 'normal').

% Found in: KTK
card_name('embodiment of spring', 'Embodiment of Spring').
card_type('embodiment of spring', 'Creature — Elemental').
card_types('embodiment of spring', ['Creature']).
card_subtypes('embodiment of spring', ['Elemental']).
card_colors('embodiment of spring', ['U']).
card_text('embodiment of spring', '{1}{G}, {T}, Sacrifice Embodiment of Spring: Search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.').
card_mana_cost('embodiment of spring', ['U']).
card_cmc('embodiment of spring', 1).
card_layout('embodiment of spring', 'normal').
card_power('embodiment of spring', 0).
card_toughness('embodiment of spring', 3).

% Found in: ODY
card_name('embolden', 'Embolden').
card_type('embolden', 'Instant').
card_types('embolden', ['Instant']).
card_subtypes('embolden', []).
card_colors('embolden', ['W']).
card_text('embolden', 'Prevent the next 4 damage that would be dealt this turn to any number of target creatures and/or players, divided as you choose.\nFlashback {1}{W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('embolden', ['2', 'W']).
card_cmc('embolden', 3).
card_layout('embolden', 'normal').

% Found in: ARC
card_name('embrace my diabolical vision', 'Embrace My Diabolical Vision').
card_type('embrace my diabolical vision', 'Scheme').
card_types('embrace my diabolical vision', ['Scheme']).
card_subtypes('embrace my diabolical vision', []).
card_colors('embrace my diabolical vision', []).
card_text('embrace my diabolical vision', 'When you set this scheme in motion, each player shuffles his or her hand and graveyard into his or her library. You draw seven cards, then each other player draws four cards.').
card_layout('embrace my diabolical vision', 'scheme').

% Found in: UNH
card_name('emcee', 'Emcee').
card_type('emcee', 'Creature — Human Rogue').
card_types('emcee', ['Creature']).
card_subtypes('emcee', ['Human', 'Rogue']).
card_colors('emcee', ['W']).
card_text('emcee', 'Whenever another creature comes into play, you may stand up and say in a deep, booming voice \"Presenting . . . \" and that creature\'s name. If you do, put a +1/+1 counter on that creature.').
card_mana_cost('emcee', ['2', 'W']).
card_cmc('emcee', 3).
card_layout('emcee', 'normal').
card_power('emcee', 0).
card_toughness('emcee', 1).

% Found in: VIS
card_name('emerald charm', 'Emerald Charm').
card_type('emerald charm', 'Instant').
card_types('emerald charm', ['Instant']).
card_subtypes('emerald charm', []).
card_colors('emerald charm', ['G']).
card_text('emerald charm', 'Choose one —\n• Untap target permanent.\n• Destroy target non-Aura enchantment.\n• Target creature loses flying until end of turn.').
card_mana_cost('emerald charm', ['G']).
card_cmc('emerald charm', 1).
card_layout('emerald charm', 'normal').

% Found in: CHR, LEG
card_name('emerald dragonfly', 'Emerald Dragonfly').
card_type('emerald dragonfly', 'Creature — Insect').
card_types('emerald dragonfly', ['Creature']).
card_subtypes('emerald dragonfly', ['Insect']).
card_colors('emerald dragonfly', ['G']).
card_text('emerald dragonfly', 'Flying\n{G}{G}: Emerald Dragonfly gains first strike until end of turn.').
card_mana_cost('emerald dragonfly', ['1', 'G']).
card_cmc('emerald dragonfly', 2).
card_layout('emerald dragonfly', 'normal').
card_power('emerald dragonfly', 1).
card_toughness('emerald dragonfly', 1).

% Found in: C14, TMP
card_name('emerald medallion', 'Emerald Medallion').
card_type('emerald medallion', 'Artifact').
card_types('emerald medallion', ['Artifact']).
card_subtypes('emerald medallion', []).
card_colors('emerald medallion', []).
card_text('emerald medallion', 'Green spells you cast cost {1} less to cast.').
card_mana_cost('emerald medallion', ['2']).
card_cmc('emerald medallion', 2).
card_layout('emerald medallion', 'normal').

% Found in: M10
card_name('emerald oryx', 'Emerald Oryx').
card_type('emerald oryx', 'Creature — Antelope').
card_types('emerald oryx', ['Creature']).
card_subtypes('emerald oryx', ['Antelope']).
card_colors('emerald oryx', ['G']).
card_text('emerald oryx', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('emerald oryx', ['3', 'G']).
card_cmc('emerald oryx', 4).
card_layout('emerald oryx', 'normal').
card_power('emerald oryx', 2).
card_toughness('emerald oryx', 3).

% Found in: ROE
card_name('emerge unscathed', 'Emerge Unscathed').
card_type('emerge unscathed', 'Instant').
card_types('emerge unscathed', ['Instant']).
card_subtypes('emerge unscathed', []).
card_colors('emerge unscathed', ['W']).
card_text('emerge unscathed', 'Target creature you control gains protection from the color of your choice until end of turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('emerge unscathed', ['W']).
card_cmc('emerge unscathed', 1).
card_layout('emerge unscathed', 'normal').

% Found in: ZEN, pWPN
card_name('emeria angel', 'Emeria Angel').
card_type('emeria angel', 'Creature — Angel').
card_types('emeria angel', ['Creature']).
card_subtypes('emeria angel', ['Angel']).
card_colors('emeria angel', ['W']).
card_text('emeria angel', 'Flying\nLandfall — Whenever a land enters the battlefield under your control, you may put a 1/1 white Bird creature token with flying onto the battlefield.').
card_mana_cost('emeria angel', ['2', 'W', 'W']).
card_cmc('emeria angel', 4).
card_layout('emeria angel', 'normal').
card_power('emeria angel', 3).
card_toughness('emeria angel', 3).

% Found in: BFZ
card_name('emeria shepherd', 'Emeria Shepherd').
card_type('emeria shepherd', 'Creature — Angel').
card_types('emeria shepherd', ['Creature']).
card_subtypes('emeria shepherd', ['Angel']).
card_colors('emeria shepherd', ['W']).
card_text('emeria shepherd', 'Flying\nLandfall — Whenever a land enters the battlefield under your control, you may return target nonland permanent card from your graveyard to your hand. If that land is a Plains, you may return that nonland permanent card to the battlefield instead.').
card_mana_cost('emeria shepherd', ['5', 'W', 'W']).
card_cmc('emeria shepherd', 7).
card_layout('emeria shepherd', 'normal').
card_power('emeria shepherd', 4).
card_toughness('emeria shepherd', 4).

% Found in: C14, ZEN
card_name('emeria, the sky ruin', 'Emeria, the Sky Ruin').
card_type('emeria, the sky ruin', 'Land').
card_types('emeria, the sky ruin', ['Land']).
card_subtypes('emeria, the sky ruin', []).
card_colors('emeria, the sky ruin', []).
card_text('emeria, the sky ruin', 'Emeria, the Sky Ruin enters the battlefield tapped.\nAt the beginning of your upkeep, if you control seven or more Plains, you may return target creature card from your graveyard to the battlefield.\n{T}: Add {W} to your mana pool.').
card_layout('emeria, the sky ruin', 'normal').

% Found in: DST
card_name('emissary of despair', 'Emissary of Despair').
card_type('emissary of despair', 'Creature — Spirit').
card_types('emissary of despair', ['Creature']).
card_subtypes('emissary of despair', ['Spirit']).
card_colors('emissary of despair', ['B']).
card_text('emissary of despair', 'Flying\nWhenever Emissary of Despair deals combat damage to a player, that player loses 1 life for each artifact he or she controls.').
card_mana_cost('emissary of despair', ['1', 'B', 'B']).
card_cmc('emissary of despair', 3).
card_layout('emissary of despair', 'normal').
card_power('emissary of despair', 2).
card_toughness('emissary of despair', 1).

% Found in: DST
card_name('emissary of hope', 'Emissary of Hope').
card_type('emissary of hope', 'Creature — Spirit').
card_types('emissary of hope', ['Creature']).
card_subtypes('emissary of hope', ['Spirit']).
card_colors('emissary of hope', ['W']).
card_text('emissary of hope', 'Flying\nWhenever Emissary of Hope deals combat damage to a player, you gain 1 life for each artifact that player controls.').
card_mana_cost('emissary of hope', ['1', 'W', 'W']).
card_cmc('emissary of hope', 3).
card_layout('emissary of hope', 'normal').
card_power('emissary of hope', 2).
card_toughness('emissary of hope', 1).

% Found in: DGM
card_name('emmara tandris', 'Emmara Tandris').
card_type('emmara tandris', 'Legendary Creature — Elf Shaman').
card_types('emmara tandris', ['Creature']).
card_subtypes('emmara tandris', ['Elf', 'Shaman']).
card_supertypes('emmara tandris', ['Legendary']).
card_colors('emmara tandris', ['W', 'G']).
card_text('emmara tandris', 'Prevent all damage that would be dealt to creature tokens you control.').
card_mana_cost('emmara tandris', ['5', 'G', 'W']).
card_cmc('emmara tandris', 7).
card_layout('emmara tandris', 'normal').
card_power('emmara tandris', 5).
card_toughness('emmara tandris', 7).

% Found in: TMP, TPR
card_name('emmessi tome', 'Emmessi Tome').
card_type('emmessi tome', 'Artifact').
card_types('emmessi tome', ['Artifact']).
card_subtypes('emmessi tome', []).
card_colors('emmessi tome', []).
card_text('emmessi tome', '{5}, {T}: Draw two cards, then discard a card.').
card_mana_cost('emmessi tome', ['4']).
card_cmc('emmessi tome', 4).
card_layout('emmessi tome', 'normal').

% Found in: 8ED, 9ED, UDS
card_name('emperor crocodile', 'Emperor Crocodile').
card_type('emperor crocodile', 'Creature — Crocodile').
card_types('emperor crocodile', ['Creature']).
card_subtypes('emperor crocodile', ['Crocodile']).
card_colors('emperor crocodile', ['G']).
card_text('emperor crocodile', 'When you control no other creatures, sacrifice Emperor Crocodile.').
card_mana_cost('emperor crocodile', ['3', 'G']).
card_cmc('emperor crocodile', 4).
card_layout('emperor crocodile', 'normal').
card_power('emperor crocodile', 5).
card_toughness('emperor crocodile', 5).

% Found in: INV
card_name('empress galina', 'Empress Galina').
card_type('empress galina', 'Legendary Creature — Merfolk').
card_types('empress galina', ['Creature']).
card_subtypes('empress galina', ['Merfolk']).
card_supertypes('empress galina', ['Legendary']).
card_colors('empress galina', ['U']).
card_text('empress galina', '{U}{U}, {T}: Gain control of target legendary permanent. (This effect lasts indefinitely.)').
card_mana_cost('empress galina', ['3', 'U', 'U']).
card_cmc('empress galina', 5).
card_layout('empress galina', 'normal').
card_power('empress galina', 1).
card_toughness('empress galina', 3).

% Found in: PTK
card_name('empty city ruse', 'Empty City Ruse').
card_type('empty city ruse', 'Sorcery').
card_types('empty city ruse', ['Sorcery']).
card_subtypes('empty city ruse', []).
card_colors('empty city ruse', ['W']).
card_text('empty city ruse', 'Target opponent skips all combat phases of his or her next turn.').
card_mana_cost('empty city ruse', ['W']).
card_cmc('empty city ruse', 1).
card_layout('empty city ruse', 'normal').

% Found in: RAV
card_name('empty the catacombs', 'Empty the Catacombs').
card_type('empty the catacombs', 'Sorcery').
card_types('empty the catacombs', ['Sorcery']).
card_subtypes('empty the catacombs', []).
card_colors('empty the catacombs', ['B']).
card_text('empty the catacombs', 'Each player returns all creature cards from his or her graveyard to his or her hand.').
card_mana_cost('empty the catacombs', ['3', 'B']).
card_cmc('empty the catacombs', 4).
card_layout('empty the catacombs', 'normal').

% Found in: KTK
card_name('empty the pits', 'Empty the Pits').
card_type('empty the pits', 'Instant').
card_types('empty the pits', ['Instant']).
card_subtypes('empty the pits', []).
card_colors('empty the pits', ['B']).
card_text('empty the pits', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nPut X 2/2 black Zombie creature tokens onto the battlefield tapped.').
card_mana_cost('empty the pits', ['X', 'X', 'B', 'B', 'B', 'B']).
card_cmc('empty the pits', 4).
card_layout('empty the pits', 'normal').

% Found in: MMA, TSP
card_name('empty the warrens', 'Empty the Warrens').
card_type('empty the warrens', 'Sorcery').
card_types('empty the warrens', ['Sorcery']).
card_subtypes('empty the warrens', []).
card_colors('empty the warrens', ['R']).
card_text('empty the warrens', 'Put two 1/1 red Goblin creature tokens onto the battlefield.\nStorm (When you cast this spell, copy it for each spell cast before it this turn.)').
card_mana_cost('empty the warrens', ['3', 'R']).
card_cmc('empty the warrens', 4).
card_layout('empty the warrens', 'normal').

% Found in: BOK
card_name('empty-shrine kannushi', 'Empty-Shrine Kannushi').
card_type('empty-shrine kannushi', 'Creature — Human Cleric').
card_types('empty-shrine kannushi', ['Creature']).
card_subtypes('empty-shrine kannushi', ['Human', 'Cleric']).
card_colors('empty-shrine kannushi', ['W']).
card_text('empty-shrine kannushi', 'Empty-Shrine Kannushi has protection from the colors of permanents you control.').
card_mana_cost('empty-shrine kannushi', ['W']).
card_cmc('empty-shrine kannushi', 1).
card_layout('empty-shrine kannushi', 'normal').
card_power('empty-shrine kannushi', 1).
card_toughness('empty-shrine kannushi', 1).

% Found in: ALA
card_name('empyrial archangel', 'Empyrial Archangel').
card_type('empyrial archangel', 'Creature — Angel').
card_types('empyrial archangel', ['Creature']).
card_subtypes('empyrial archangel', ['Angel']).
card_colors('empyrial archangel', ['W', 'U', 'G']).
card_text('empyrial archangel', 'Flying\nShroud (This creature can\'t be the target of spells or abilities.)\nAll damage that would be dealt to you is dealt to Empyrial Archangel instead.').
card_mana_cost('empyrial archangel', ['4', 'G', 'W', 'W', 'U']).
card_cmc('empyrial archangel', 8).
card_layout('empyrial archangel', 'normal').
card_power('empyrial archangel', 5).
card_toughness('empyrial archangel', 8).

% Found in: VMA, WTH, pARL
card_name('empyrial armor', 'Empyrial Armor').
card_type('empyrial armor', 'Enchantment — Aura').
card_types('empyrial armor', ['Enchantment']).
card_subtypes('empyrial armor', ['Aura']).
card_colors('empyrial armor', ['W']).
card_text('empyrial armor', 'Enchant creature\nEnchanted creature gets +1/+1 for each card in your hand.').
card_mana_cost('empyrial armor', ['1', 'W', 'W']).
card_cmc('empyrial armor', 3).
card_layout('empyrial armor', 'normal').

% Found in: MRD
card_name('empyrial plate', 'Empyrial Plate').
card_type('empyrial plate', 'Artifact — Equipment').
card_types('empyrial plate', ['Artifact']).
card_subtypes('empyrial plate', ['Equipment']).
card_colors('empyrial plate', []).
card_text('empyrial plate', 'Equipped creature gets +1/+1 for each card in your hand.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery. This card enters the battlefield unattached and stays on the battlefield if the creature leaves.)').
card_mana_cost('empyrial plate', ['2']).
card_cmc('empyrial plate', 2).
card_layout('empyrial plate', 'normal').

% Found in: DDP, ROE
card_name('emrakul\'s hatcher', 'Emrakul\'s Hatcher').
card_type('emrakul\'s hatcher', 'Creature — Eldrazi Drone').
card_types('emrakul\'s hatcher', ['Creature']).
card_subtypes('emrakul\'s hatcher', ['Eldrazi', 'Drone']).
card_colors('emrakul\'s hatcher', ['R']).
card_text('emrakul\'s hatcher', 'When Emrakul\'s Hatcher enters the battlefield, put three 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('emrakul\'s hatcher', ['4', 'R']).
card_cmc('emrakul\'s hatcher', 5).
card_layout('emrakul\'s hatcher', 'normal').
card_power('emrakul\'s hatcher', 3).
card_toughness('emrakul\'s hatcher', 3).

% Found in: MM2, ROE, pPRE
card_name('emrakul, the aeons torn', 'Emrakul, the Aeons Torn').
card_type('emrakul, the aeons torn', 'Legendary Creature — Eldrazi').
card_types('emrakul, the aeons torn', ['Creature']).
card_subtypes('emrakul, the aeons torn', ['Eldrazi']).
card_supertypes('emrakul, the aeons torn', ['Legendary']).
card_colors('emrakul, the aeons torn', []).
card_text('emrakul, the aeons torn', 'Emrakul, the Aeons Torn can\'t be countered.\nWhen you cast Emrakul, take an extra turn after this one.\nFlying, protection from colored spells, annihilator 6\nWhen Emrakul is put into a graveyard from anywhere, its owner shuffles his or her graveyard into his or her library.').
card_mana_cost('emrakul, the aeons torn', ['15']).
card_cmc('emrakul, the aeons torn', 15).
card_layout('emrakul, the aeons torn', 'normal').
card_power('emrakul, the aeons torn', 15).
card_toughness('emrakul, the aeons torn', 15).

% Found in: ROE
card_name('enatu golem', 'Enatu Golem').
card_type('enatu golem', 'Artifact Creature — Golem').
card_types('enatu golem', ['Artifact', 'Creature']).
card_subtypes('enatu golem', ['Golem']).
card_colors('enatu golem', []).
card_text('enatu golem', 'When Enatu Golem dies, you gain 4 life.').
card_mana_cost('enatu golem', ['6']).
card_cmc('enatu golem', 6).
card_layout('enatu golem', 'normal').
card_power('enatu golem', 3).
card_toughness('enatu golem', 5).

% Found in: DTK
card_name('encase in ice', 'Encase in Ice').
card_type('encase in ice', 'Enchantment — Aura').
card_types('encase in ice', ['Enchantment']).
card_subtypes('encase in ice', ['Aura']).
card_colors('encase in ice', ['U']).
card_text('encase in ice', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant red or green creature\nWhen Encase in Ice enters the battlefield, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_mana_cost('encase in ice', ['1', 'U']).
card_cmc('encase in ice', 2).
card_layout('encase in ice', 'normal').

% Found in: LEG
card_name('enchanted being', 'Enchanted Being').
card_type('enchanted being', 'Creature — Human').
card_types('enchanted being', ['Creature']).
card_subtypes('enchanted being', ['Human']).
card_colors('enchanted being', ['W']).
card_text('enchanted being', 'Prevent all combat damage that would be dealt to Enchanted Being by enchanted creatures.').
card_mana_cost('enchanted being', ['1', 'W', 'W']).
card_cmc('enchanted being', 3).
card_layout('enchanted being', 'normal').
card_power('enchanted being', 2).
card_toughness('enchanted being', 2).

% Found in: SHM
card_name('enchanted evening', 'Enchanted Evening').
card_type('enchanted evening', 'Enchantment').
card_types('enchanted evening', ['Enchantment']).
card_subtypes('enchanted evening', []).
card_colors('enchanted evening', ['W', 'U']).
card_text('enchanted evening', 'All permanents are enchantments in addition to their other types.').
card_mana_cost('enchanted evening', ['3', 'W/U', 'W/U']).
card_cmc('enchanted evening', 5).
card_layout('enchanted evening', 'normal').

% Found in: CHR, LEG, USG
card_name('enchantment alteration', 'Enchantment Alteration').
card_type('enchantment alteration', 'Instant').
card_types('enchantment alteration', ['Instant']).
card_subtypes('enchantment alteration', []).
card_colors('enchantment alteration', ['U']).
card_text('enchantment alteration', 'Attach target Aura attached to a creature or land to another permanent of that type.').
card_mana_cost('enchantment alteration', ['U']).
card_cmc('enchantment alteration', 1).
card_layout('enchantment alteration', 'normal').

% Found in: ONS
card_name('enchantress\'s presence', 'Enchantress\'s Presence').
card_type('enchantress\'s presence', 'Enchantment').
card_types('enchantress\'s presence', ['Enchantment']).
card_subtypes('enchantress\'s presence', []).
card_colors('enchantress\'s presence', ['G']).
card_text('enchantress\'s presence', 'Whenever you cast an enchantment spell, draw a card.').
card_mana_cost('enchantress\'s presence', ['2', 'G']).
card_cmc('enchantress\'s presence', 3).
card_layout('enchantress\'s presence', 'normal').

% Found in: BFZ
card_name('encircling fissure', 'Encircling Fissure').
card_type('encircling fissure', 'Instant').
card_types('encircling fissure', ['Instant']).
card_subtypes('encircling fissure', []).
card_colors('encircling fissure', ['W']).
card_text('encircling fissure', 'Prevent all combat damage that would be dealt this turn by creatures target opponent controls.\nAwaken 2—{4}{W} (If you cast this spell for {4}{W}, also put two +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_mana_cost('encircling fissure', ['2', 'W']).
card_cmc('encircling fissure', 3).
card_layout('encircling fissure', 'normal').

% Found in: ROE
card_name('enclave cryptologist', 'Enclave Cryptologist').
card_type('enclave cryptologist', 'Creature — Merfolk Wizard').
card_types('enclave cryptologist', ['Creature']).
card_subtypes('enclave cryptologist', ['Merfolk', 'Wizard']).
card_colors('enclave cryptologist', ['U']).
card_text('enclave cryptologist', 'Level up {1}{U} ({1}{U}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-2\n0/1\n{T}: Draw a card, then discard a card.\nLEVEL 3+\n0/1\n{T}: Draw a card.').
card_mana_cost('enclave cryptologist', ['U']).
card_cmc('enclave cryptologist', 1).
card_layout('enclave cryptologist', 'leveler').
card_power('enclave cryptologist', 0).
card_toughness('enclave cryptologist', 1).

% Found in: CNS, WWK
card_name('enclave elite', 'Enclave Elite').
card_type('enclave elite', 'Creature — Merfolk Soldier').
card_types('enclave elite', ['Creature']).
card_subtypes('enclave elite', ['Merfolk', 'Soldier']).
card_colors('enclave elite', ['U']).
card_text('enclave elite', 'Multikicker {1}{U} (You may pay an additional {1}{U} any number of times as you cast this spell.)\nIslandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nEnclave Elite enters the battlefield with a +1/+1 counter on it for each time it was kicked.').
card_mana_cost('enclave elite', ['2', 'U']).
card_cmc('enclave elite', 3).
card_layout('enclave elite', 'normal').
card_power('enclave elite', 2).
card_toughness('enclave elite', 2).

% Found in: UDS
card_name('encroach', 'Encroach').
card_type('encroach', 'Sorcery').
card_types('encroach', ['Sorcery']).
card_subtypes('encroach', []).
card_colors('encroach', ['B']).
card_text('encroach', 'Target player reveals his or her hand. You choose a nonbasic land card from it. That player discards that card.').
card_mana_cost('encroach', ['B']).
card_cmc('encroach', 1).
card_layout('encroach', 'normal').

% Found in: M14, pFNM
card_name('encroaching wastes', 'Encroaching Wastes').
card_type('encroaching wastes', 'Land').
card_types('encroaching wastes', ['Land']).
card_subtypes('encroaching wastes', []).
card_colors('encroaching wastes', []).
card_text('encroaching wastes', '{T}: Add {1} to your mana pool.\n{4}, {T}, Sacrifice Encroaching Wastes: Destroy target nonbasic land.').
card_layout('encroaching wastes', 'normal').

% Found in: M13, M15
card_name('encrust', 'Encrust').
card_type('encrust', 'Enchantment — Aura').
card_types('encrust', ['Enchantment']).
card_subtypes('encrust', ['Aura']).
card_colors('encrust', ['U']).
card_text('encrust', 'Enchant artifact or creature\nEnchanted permanent doesn\'t untap during its controller\'s untap step and its activated abilities can\'t be activated.').
card_mana_cost('encrust', ['1', 'U', 'U']).
card_cmc('encrust', 3).
card_layout('encrust', 'normal').

% Found in: KTK
card_name('end hostilities', 'End Hostilities').
card_type('end hostilities', 'Sorcery').
card_types('end hostilities', ['Sorcery']).
card_subtypes('end hostilities', []).
card_colors('end hostilities', ['W']).
card_text('end hostilities', 'Destroy all creatures and all permanents attached to creatures.').
card_mana_cost('end hostilities', ['3', 'W', 'W']).
card_cmc('end hostilities', 5).
card_layout('end hostilities', 'normal').

% Found in: STH, TPR
card_name('endangered armodon', 'Endangered Armodon').
card_type('endangered armodon', 'Creature — Elephant').
card_types('endangered armodon', ['Creature']).
card_subtypes('endangered armodon', ['Elephant']).
card_colors('endangered armodon', ['G']).
card_text('endangered armodon', 'When you control a creature with toughness 2 or less, sacrifice Endangered Armodon.').
card_mana_cost('endangered armodon', ['2', 'G', 'G']).
card_cmc('endangered armodon', 4).
card_layout('endangered armodon', 'normal').
card_power('endangered armodon', 4).
card_toughness('endangered armodon', 5).

% Found in: PCY
card_name('endbringer\'s revel', 'Endbringer\'s Revel').
card_type('endbringer\'s revel', 'Enchantment').
card_types('endbringer\'s revel', ['Enchantment']).
card_subtypes('endbringer\'s revel', []).
card_colors('endbringer\'s revel', ['B']).
card_text('endbringer\'s revel', '{4}: Return target creature card from a graveyard to its owner\'s hand. Any player may activate this ability but only any time he or she could cast a sorcery.').
card_mana_cost('endbringer\'s revel', ['2', 'B']).
card_cmc('endbringer\'s revel', 3).
card_layout('endbringer\'s revel', 'normal').

% Found in: ONS
card_name('endemic plague', 'Endemic Plague').
card_type('endemic plague', 'Sorcery').
card_types('endemic plague', ['Sorcery']).
card_subtypes('endemic plague', []).
card_colors('endemic plague', ['B']).
card_text('endemic plague', 'As an additional cost to cast Endemic Plague, sacrifice a creature.\nDestroy all creatures that share a creature type with the sacrificed creature. They can\'t be regenerated.').
card_mana_cost('endemic plague', ['3', 'B']).
card_cmc('endemic plague', 4).
card_layout('endemic plague', 'normal').

% Found in: C13, POR
card_name('endless cockroaches', 'Endless Cockroaches').
card_type('endless cockroaches', 'Creature — Insect').
card_types('endless cockroaches', ['Creature']).
card_subtypes('endless cockroaches', ['Insect']).
card_colors('endless cockroaches', ['B']).
card_text('endless cockroaches', 'When Endless Cockroaches dies, return it to its owner\'s hand.').
card_mana_cost('endless cockroaches', ['1', 'B', 'B']).
card_cmc('endless cockroaches', 3).
card_layout('endless cockroaches', 'normal').
card_power('endless cockroaches', 1).
card_toughness('endless cockroaches', 1).

% Found in: EVE
card_name('endless horizons', 'Endless Horizons').
card_type('endless horizons', 'Enchantment').
card_types('endless horizons', ['Enchantment']).
card_subtypes('endless horizons', []).
card_colors('endless horizons', ['W']).
card_text('endless horizons', 'When Endless Horizons enters the battlefield, search your library for any number of Plains cards and exile them. Then shuffle your library.\nAt the beginning of your upkeep, you may put a card you own exiled with Endless Horizons into your hand.').
card_mana_cost('endless horizons', ['3', 'W']).
card_cmc('endless horizons', 4).
card_layout('endless horizons', 'normal').

% Found in: M15
card_name('endless obedience', 'Endless Obedience').
card_type('endless obedience', 'Sorcery').
card_types('endless obedience', ['Sorcery']).
card_subtypes('endless obedience', []).
card_colors('endless obedience', ['B']).
card_text('endless obedience', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nPut target creature card from a graveyard onto the battlefield under your control.').
card_mana_cost('endless obedience', ['4', 'B', 'B']).
card_cmc('endless obedience', 6).
card_layout('endless obedience', 'normal').

% Found in: BFZ
card_name('endless one', 'Endless One').
card_type('endless one', 'Creature — Eldrazi').
card_types('endless one', ['Creature']).
card_subtypes('endless one', ['Eldrazi']).
card_colors('endless one', []).
card_text('endless one', 'Endless One enters the battlefield with X +1/+1 counters on it.').
card_mana_cost('endless one', ['X']).
card_cmc('endless one', 0).
card_layout('endless one', 'normal').
card_power('endless one', 0).
card_toughness('endless one', 0).

% Found in: ISD
card_name('endless ranks of the dead', 'Endless Ranks of the Dead').
card_type('endless ranks of the dead', 'Enchantment').
card_types('endless ranks of the dead', ['Enchantment']).
card_subtypes('endless ranks of the dead', []).
card_colors('endless ranks of the dead', ['B']).
card_text('endless ranks of the dead', 'At the beginning of your upkeep, put X 2/2 black Zombie creature tokens onto the battlefield, where X is half the number of Zombies you control, rounded down.').
card_mana_cost('endless ranks of the dead', ['2', 'B', 'B']).
card_cmc('endless ranks of the dead', 4).
card_layout('endless ranks of the dead', 'normal').

% Found in: TMP
card_name('endless scream', 'Endless Scream').
card_type('endless scream', 'Enchantment — Aura').
card_types('endless scream', ['Enchantment']).
card_subtypes('endless scream', ['Aura']).
card_colors('endless scream', ['B']).
card_text('endless scream', 'Enchant creature\nEndless Scream enters the battlefield with X scream counters on it.\nEnchanted creature gets +1/+0 for each scream counter on Endless Scream.').
card_mana_cost('endless scream', ['X', 'B']).
card_cmc('endless scream', 1).
card_layout('endless scream', 'normal').

% Found in: SOK
card_name('endless swarm', 'Endless Swarm').
card_type('endless swarm', 'Sorcery').
card_types('endless swarm', ['Sorcery']).
card_subtypes('endless swarm', []).
card_colors('endless swarm', ['G']).
card_text('endless swarm', 'Put a 1/1 green Snake creature token onto the battlefield for each card in your hand.\nEpic (For the rest of the game, you can\'t cast spells. At the beginning of each of your upkeeps, copy this spell except for its epic ability.)').
card_mana_cost('endless swarm', ['5', 'G', 'G', 'G']).
card_cmc('endless swarm', 8).
card_layout('endless swarm', 'normal').

% Found in: 5DN
card_name('endless whispers', 'Endless Whispers').
card_type('endless whispers', 'Enchantment').
card_types('endless whispers', ['Enchantment']).
card_subtypes('endless whispers', []).
card_colors('endless whispers', ['B']).
card_text('endless whispers', 'Each creature has \"When this creature dies, choose target opponent. That player puts this card from its owner\'s graveyard onto the battlefield under his or her control at the beginning of the next end step.\"').
card_mana_cost('endless whispers', ['2', 'B', 'B']).
card_cmc('endless whispers', 4).
card_layout('endless whispers', 'normal').

% Found in: USG
card_name('endless wurm', 'Endless Wurm').
card_type('endless wurm', 'Creature — Wurm').
card_types('endless wurm', ['Creature']).
card_subtypes('endless wurm', ['Wurm']).
card_colors('endless wurm', ['G']).
card_text('endless wurm', 'Trample\nAt the beginning of your upkeep, sacrifice Endless Wurm unless you sacrifice an enchantment.').
card_mana_cost('endless wurm', ['3', 'G', 'G']).
card_cmc('endless wurm', 5).
card_layout('endless wurm', 'normal').
card_power('endless wurm', 9).
card_toughness('endless wurm', 9).

% Found in: USG
card_name('endoskeleton', 'Endoskeleton').
card_type('endoskeleton', 'Artifact').
card_types('endoskeleton', ['Artifact']).
card_subtypes('endoskeleton', []).
card_colors('endoskeleton', []).
card_text('endoskeleton', 'You may choose not to untap Endoskeleton during your untap step.\n{2}, {T}: Target creature gets +0/+3 for as long as Endoskeleton remains tapped.').
card_mana_cost('endoskeleton', ['2']).
card_cmc('endoskeleton', 2).
card_layout('endoskeleton', 'normal').

% Found in: C13, MM2, TSP
card_name('endrek sahr, master breeder', 'Endrek Sahr, Master Breeder').
card_type('endrek sahr, master breeder', 'Legendary Creature — Human Wizard').
card_types('endrek sahr, master breeder', ['Creature']).
card_subtypes('endrek sahr, master breeder', ['Human', 'Wizard']).
card_supertypes('endrek sahr, master breeder', ['Legendary']).
card_colors('endrek sahr, master breeder', ['B']).
card_text('endrek sahr, master breeder', 'Whenever you cast a creature spell, put X 1/1 black Thrull creature tokens onto the battlefield, where X is that spell\'s converted mana cost.\nWhen you control seven or more Thrulls, sacrifice Endrek Sahr, Master Breeder.').
card_mana_cost('endrek sahr, master breeder', ['4', 'B']).
card_cmc('endrek sahr, master breeder', 5).
card_layout('endrek sahr, master breeder', 'normal').
card_power('endrek sahr, master breeder', 2).
card_toughness('endrek sahr, master breeder', 2).

% Found in: DIS
card_name('ends', 'Ends').
card_type('ends', 'Instant').
card_types('ends', ['Instant']).
card_subtypes('ends', []).
card_colors('ends', ['W', 'R']).
card_text('ends', 'Target player sacrifices two attacking creatures.').
card_mana_cost('ends', ['3', 'R', 'W']).
card_cmc('ends', 5).
card_layout('ends', 'split').

% Found in: EVE
card_name('endure', 'Endure').
card_type('endure', 'Instant').
card_types('endure', ['Instant']).
card_subtypes('endure', []).
card_colors('endure', ['W']).
card_text('endure', 'Prevent all damage that would be dealt to you and permanents you control this turn.').
card_mana_cost('endure', ['3', 'W', 'W']).
card_cmc('endure', 5).
card_layout('endure', 'normal').

% Found in: SOK
card_name('enduring ideal', 'Enduring Ideal').
card_type('enduring ideal', 'Sorcery').
card_types('enduring ideal', ['Sorcery']).
card_subtypes('enduring ideal', []).
card_colors('enduring ideal', ['W']).
card_text('enduring ideal', 'Search your library for an enchantment card and put it onto the battlefield. Then shuffle your library.\nEpic (For the rest of the game, you can\'t cast spells. At the beginning of each of your upkeeps, copy this spell except for its epic ability.)').
card_mana_cost('enduring ideal', ['5', 'W', 'W']).
card_cmc('enduring ideal', 7).
card_layout('enduring ideal', 'normal').

% Found in: ICE, TSB
card_name('enduring renewal', 'Enduring Renewal').
card_type('enduring renewal', 'Enchantment').
card_types('enduring renewal', ['Enchantment']).
card_subtypes('enduring renewal', []).
card_colors('enduring renewal', ['W']).
card_text('enduring renewal', 'Play with your hand revealed.\nIf you would draw a card, reveal the top card of your library instead. If it\'s a creature card, put it into your graveyard. Otherwise, draw a card.\nWhenever a creature is put into your graveyard from the battlefield, return it to your hand.').
card_mana_cost('enduring renewal', ['2', 'W', 'W']).
card_cmc('enduring renewal', 4).
card_layout('enduring renewal', 'normal').

% Found in: DTK
card_name('enduring scalelord', 'Enduring Scalelord').
card_type('enduring scalelord', 'Creature — Dragon').
card_types('enduring scalelord', ['Creature']).
card_subtypes('enduring scalelord', ['Dragon']).
card_colors('enduring scalelord', ['W', 'G']).
card_text('enduring scalelord', 'Flying\nWhenever one or more +1/+1 counters are placed on another creature you control, you may put a +1/+1 counter on Enduring Scalelord.').
card_mana_cost('enduring scalelord', ['4', 'G', 'W']).
card_cmc('enduring scalelord', 6).
card_layout('enduring scalelord', 'normal').
card_power('enduring scalelord', 4).
card_toughness('enduring scalelord', 4).

% Found in: DTK
card_name('enduring victory', 'Enduring Victory').
card_type('enduring victory', 'Instant').
card_types('enduring victory', ['Instant']).
card_subtypes('enduring victory', []).
card_colors('enduring victory', ['W']).
card_text('enduring victory', 'Destroy target attacking or blocking creature. Bolster 1. (Choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_mana_cost('enduring victory', ['4', 'W']).
card_cmc('enduring victory', 5).
card_layout('enduring victory', 'normal').

% Found in: DIS
card_name('enemy of the guildpact', 'Enemy of the Guildpact').
card_type('enemy of the guildpact', 'Creature — Spirit').
card_types('enemy of the guildpact', ['Creature']).
card_subtypes('enemy of the guildpact', ['Spirit']).
card_colors('enemy of the guildpact', ['B']).
card_text('enemy of the guildpact', 'Protection from multicolored').
card_mana_cost('enemy of the guildpact', ['4', 'B']).
card_cmc('enemy of the guildpact', 5).
card_layout('enemy of the guildpact', 'normal').
card_power('enemy of the guildpact', 4).
card_toughness('enemy of the guildpact', 2).

% Found in: TMP
card_name('energizer', 'Energizer').
card_type('energizer', 'Artifact Creature — Juggernaut').
card_types('energizer', ['Artifact', 'Creature']).
card_subtypes('energizer', ['Juggernaut']).
card_colors('energizer', []).
card_text('energizer', '{2}, {T}: Put a +1/+1 counter on Energizer.').
card_mana_cost('energizer', ['4']).
card_cmc('energizer', 4).
card_layout('energizer', 'normal').
card_power('energizer', 2).
card_toughness('energizer', 2).

% Found in: ALL, MED
card_name('energy arc', 'Energy Arc').
card_type('energy arc', 'Instant').
card_types('energy arc', ['Instant']).
card_subtypes('energy arc', []).
card_colors('energy arc', ['W', 'U']).
card_text('energy arc', 'Untap any number of target creatures. Prevent all combat damage that would be dealt to and dealt by those creatures this turn.').
card_mana_cost('energy arc', ['W', 'U']).
card_cmc('energy arc', 2).
card_layout('energy arc', 'normal').

% Found in: MIR
card_name('energy bolt', 'Energy Bolt').
card_type('energy bolt', 'Sorcery').
card_types('energy bolt', ['Sorcery']).
card_subtypes('energy bolt', []).
card_colors('energy bolt', ['W', 'R']).
card_text('energy bolt', 'Choose one —\n• Energy Bolt deals X damage to target player.\n• Target player gains X life.').
card_mana_cost('energy bolt', ['X', 'R', 'W']).
card_cmc('energy bolt', 2).
card_layout('energy bolt', 'normal').
card_reserved('energy bolt').

% Found in: 5DN, DDF
card_name('energy chamber', 'Energy Chamber').
card_type('energy chamber', 'Artifact').
card_types('energy chamber', ['Artifact']).
card_subtypes('energy chamber', []).
card_colors('energy chamber', []).
card_text('energy chamber', 'At the beginning of your upkeep, choose one —\n• Put a +1/+1 counter on target artifact creature.\n• Put a charge counter on target noncreature artifact.').
card_mana_cost('energy chamber', ['2']).
card_cmc('energy chamber', 2).
card_layout('energy chamber', 'normal').

% Found in: USG
card_name('energy field', 'Energy Field').
card_type('energy field', 'Enchantment').
card_types('energy field', ['Enchantment']).
card_subtypes('energy field', []).
card_colors('energy field', ['U']).
card_text('energy field', 'Prevent all damage that would be dealt to you by sources you don\'t control.\nWhen a card is put into your graveyard from anywhere, sacrifice Energy Field.').
card_mana_cost('energy field', ['1', 'U']).
card_cmc('energy field', 2).
card_layout('energy field', 'normal').

% Found in: 3ED, 4ED, 5ED, ATQ, ITP, ME4, MMQ, RQS
card_name('energy flux', 'Energy Flux').
card_type('energy flux', 'Enchantment').
card_types('energy flux', ['Enchantment']).
card_subtypes('energy flux', []).
card_colors('energy flux', ['U']).
card_text('energy flux', 'All artifacts have \"At the beginning of your upkeep, sacrifice this artifact unless you pay {2}.\"').
card_mana_cost('energy flux', ['2', 'U']).
card_cmc('energy flux', 3).
card_layout('energy flux', 'normal').

% Found in: ICE, ME2
card_name('energy storm', 'Energy Storm').
card_type('energy storm', 'Enchantment').
card_types('energy storm', ['Enchantment']).
card_subtypes('energy storm', []).
card_colors('energy storm', ['W']).
card_text('energy storm', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nPrevent all damage that would be dealt by instant and sorcery spells.\nCreatures with flying don\'t untap during their controllers\' untap steps.').
card_mana_cost('energy storm', ['1', 'W']).
card_cmc('energy storm', 2).
card_layout('energy storm', 'normal').
card_reserved('energy storm').

% Found in: 4ED, LEG
card_name('energy tap', 'Energy Tap').
card_type('energy tap', 'Sorcery').
card_types('energy tap', ['Sorcery']).
card_subtypes('energy tap', []).
card_colors('energy tap', ['U']).
card_text('energy tap', 'Tap target untapped creature you control. If you do, add {X} to your mana pool, where X is that creature\'s converted mana cost.').
card_mana_cost('energy tap', ['U']).
card_cmc('energy tap', 1).
card_layout('energy tap', 'normal').

% Found in: MIR
card_name('energy vortex', 'Energy Vortex').
card_type('energy vortex', 'Enchantment').
card_types('energy vortex', ['Enchantment']).
card_subtypes('energy vortex', []).
card_colors('energy vortex', ['U']).
card_text('energy vortex', 'As Energy Vortex enters the battlefield, choose an opponent.\nAt the beginning of your upkeep, remove all energy counters from Energy Vortex.\nAt the beginning of the chosen player\'s upkeep, Energy Vortex deals 3 damage to that player unless he or she pays {1} for each energy counter on Energy Vortex.\n{X}: Put X energy counters on Energy Vortex. Activate this ability only during your upkeep.').
card_mana_cost('energy vortex', ['3', 'U', 'U']).
card_cmc('energy vortex', 5).
card_layout('energy vortex', 'normal').
card_reserved('energy vortex').

% Found in: 5ED, ICE, ME2
card_name('enervate', 'Enervate').
card_type('enervate', 'Instant').
card_types('enervate', ['Instant']).
card_subtypes('enervate', []).
card_colors('enervate', ['U']).
card_text('enervate', 'Tap target artifact, creature, or land.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('enervate', ['1', 'U']).
card_cmc('enervate', 2).
card_layout('enervate', 'normal').

% Found in: 6ED, 9ED, MIR, TMP
card_name('enfeeblement', 'Enfeeblement').
card_type('enfeeblement', 'Enchantment — Aura').
card_types('enfeeblement', ['Enchantment']).
card_subtypes('enfeeblement', ['Aura']).
card_colors('enfeeblement', ['B']).
card_text('enfeeblement', 'Enchant creature (Target a creature as you cast this. This card enters the battlefield attached to that creature.)\nEnchanted creature gets -2/-2.').
card_mana_cost('enfeeblement', ['B', 'B']).
card_cmc('enfeeblement', 2).
card_layout('enfeeblement', 'normal').

% Found in: 5DN, MMA
card_name('engineered explosives', 'Engineered Explosives').
card_type('engineered explosives', 'Artifact').
card_types('engineered explosives', ['Artifact']).
card_subtypes('engineered explosives', []).
card_colors('engineered explosives', []).
card_text('engineered explosives', 'Sunburst (This enters the battlefield with a charge counter on it for each color of mana spent to cast it.)\n{2}, Sacrifice Engineered Explosives: Destroy each nonland permanent with converted mana cost equal to the number of charge counters on Engineered Explosives.').
card_mana_cost('engineered explosives', ['X']).
card_cmc('engineered explosives', 0).
card_layout('engineered explosives', 'normal').

% Found in: 7ED, ULG, pFNM
card_name('engineered plague', 'Engineered Plague').
card_type('engineered plague', 'Enchantment').
card_types('engineered plague', ['Enchantment']).
card_subtypes('engineered plague', []).
card_colors('engineered plague', ['B']).
card_text('engineered plague', 'As Engineered Plague enters the battlefield, choose a creature type.\nAll creatures of the chosen type get -1/-1.').
card_mana_cost('engineered plague', ['2', 'B']).
card_cmc('engineered plague', 3).
card_layout('engineered plague', 'normal').

% Found in: ODY
card_name('engulfing flames', 'Engulfing Flames').
card_type('engulfing flames', 'Instant').
card_types('engulfing flames', ['Instant']).
card_subtypes('engulfing flames', []).
card_colors('engulfing flames', ['R']).
card_text('engulfing flames', 'Engulfing Flames deals 1 damage to target creature. It can\'t be regenerated this turn.\nFlashback {3}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('engulfing flames', ['R']).
card_cmc('engulfing flames', 1).
card_layout('engulfing flames', 'normal').

% Found in: SOM
card_name('engulfing slagwurm', 'Engulfing Slagwurm').
card_type('engulfing slagwurm', 'Creature — Wurm').
card_types('engulfing slagwurm', ['Creature']).
card_subtypes('engulfing slagwurm', ['Wurm']).
card_colors('engulfing slagwurm', ['G']).
card_text('engulfing slagwurm', 'Whenever Engulfing Slagwurm blocks or becomes blocked by a creature, destroy that creature. You gain life equal to that creature\'s toughness.').
card_mana_cost('engulfing slagwurm', ['5', 'G', 'G']).
card_cmc('engulfing slagwurm', 7).
card_layout('engulfing slagwurm', 'normal').
card_power('engulfing slagwurm', 7).
card_toughness('engulfing slagwurm', 7).

% Found in: FRF
card_name('enhanced awareness', 'Enhanced Awareness').
card_type('enhanced awareness', 'Instant').
card_types('enhanced awareness', ['Instant']).
card_subtypes('enhanced awareness', []).
card_colors('enhanced awareness', ['U']).
card_text('enhanced awareness', 'Draw three cards, then discard a card.').
card_mana_cost('enhanced awareness', ['4', 'U']).
card_cmc('enhanced awareness', 5).
card_layout('enhanced awareness', 'normal').

% Found in: DIS
card_name('enigma eidolon', 'Enigma Eidolon').
card_type('enigma eidolon', 'Creature — Spirit').
card_types('enigma eidolon', ['Creature']).
card_subtypes('enigma eidolon', ['Spirit']).
card_colors('enigma eidolon', ['U']).
card_text('enigma eidolon', '{U}, Sacrifice Enigma Eidolon: Target player puts the top three cards of his or her library into his or her graveyard.\nWhenever you cast a multicolored spell, you may return Enigma Eidolon from your graveyard to your hand.').
card_mana_cost('enigma eidolon', ['3', 'U']).
card_cmc('enigma eidolon', 4).
card_layout('enigma eidolon', 'normal').
card_power('enigma eidolon', 2).
card_toughness('enigma eidolon', 2).

% Found in: ARB, PC2
card_name('enigma sphinx', 'Enigma Sphinx').
card_type('enigma sphinx', 'Artifact Creature — Sphinx').
card_types('enigma sphinx', ['Artifact', 'Creature']).
card_subtypes('enigma sphinx', ['Sphinx']).
card_colors('enigma sphinx', ['W', 'U', 'B']).
card_text('enigma sphinx', 'Flying\nWhen Enigma Sphinx is put into your graveyard from the battlefield, put it into your library third from the top.\nCascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)').
card_mana_cost('enigma sphinx', ['4', 'W', 'U', 'B']).
card_cmc('enigma sphinx', 7).
card_layout('enigma sphinx', 'normal').
card_power('enigma sphinx', 5).
card_toughness('enigma sphinx', 4).

% Found in: VAN
card_name('enigma sphinx avatar', 'Enigma Sphinx Avatar').
card_type('enigma sphinx avatar', 'Vanguard').
card_types('enigma sphinx avatar', ['Vanguard']).
card_subtypes('enigma sphinx avatar', []).
card_colors('enigma sphinx avatar', []).
card_text('enigma sphinx avatar', 'Whenever you cast a colored artifact spell for the first time in a turn, search your library for a colored artifact card chosen at random whose converted mana cost is less than that spell\'s converted mana cost. You may play that card without paying its mana cost. If you don\'t, put that card on the bottom of your library.').
card_layout('enigma sphinx avatar', 'vanguard').

% Found in: M14
card_name('enlarge', 'Enlarge').
card_type('enlarge', 'Sorcery').
card_types('enlarge', ['Sorcery']).
card_subtypes('enlarge', []).
card_colors('enlarge', ['G']).
card_text('enlarge', 'Target creature gets +7/+7 and gains trample until end of turn. It must be blocked this turn if able. (If a creature with trample would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('enlarge', ['3', 'G', 'G']).
card_cmc('enlarge', 5).
card_layout('enlarge', 'normal').

% Found in: ORI
card_name('enlightened ascetic', 'Enlightened Ascetic').
card_type('enlightened ascetic', 'Creature — Cat Monk').
card_types('enlightened ascetic', ['Creature']).
card_subtypes('enlightened ascetic', ['Cat', 'Monk']).
card_colors('enlightened ascetic', ['W']).
card_text('enlightened ascetic', 'When Enlightened Ascetic enters the battlefield, you may destroy target enchantment.').
card_mana_cost('enlightened ascetic', ['1', 'W']).
card_cmc('enlightened ascetic', 2).
card_layout('enlightened ascetic', 'normal').
card_power('enlightened ascetic', 1).
card_toughness('enlightened ascetic', 1).

% Found in: 6ED, MIR, pARL
card_name('enlightened tutor', 'Enlightened Tutor').
card_type('enlightened tutor', 'Instant').
card_types('enlightened tutor', ['Instant']).
card_subtypes('enlightened tutor', []).
card_colors('enlightened tutor', ['W']).
card_text('enlightened tutor', 'Search your library for an artifact or enchantment card and reveal that card. Shuffle your library, then put the card on top of it.').
card_mana_cost('enlightened tutor', ['W']).
card_cmc('enlightened tutor', 1).
card_layout('enlightened tutor', 'normal').

% Found in: ARB, PC2
card_name('enlisted wurm', 'Enlisted Wurm').
card_type('enlisted wurm', 'Creature — Wurm').
card_types('enlisted wurm', ['Creature']).
card_subtypes('enlisted wurm', ['Wurm']).
card_colors('enlisted wurm', ['W', 'G']).
card_text('enlisted wurm', 'Cascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)').
card_mana_cost('enlisted wurm', ['4', 'G', 'W']).
card_cmc('enlisted wurm', 6).
card_layout('enlisted wurm', 'normal').
card_power('enlisted wurm', 5).
card_toughness('enlisted wurm', 5).

% Found in: APC
card_name('enlistment officer', 'Enlistment Officer').
card_type('enlistment officer', 'Creature — Human Soldier').
card_types('enlistment officer', ['Creature']).
card_subtypes('enlistment officer', ['Human', 'Soldier']).
card_colors('enlistment officer', ['W']).
card_text('enlistment officer', 'First strike\nWhen Enlistment Officer enters the battlefield, reveal the top four cards of your library. Put all Soldier cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('enlistment officer', ['3', 'W']).
card_cmc('enlistment officer', 4).
card_layout('enlistment officer', 'normal').
card_power('enlistment officer', 2).
card_toughness('enlistment officer', 3).

% Found in: 10E, 8ED, 9ED, LGN, M10
card_name('enormous baloth', 'Enormous Baloth').
card_type('enormous baloth', 'Creature — Beast').
card_types('enormous baloth', ['Creature']).
card_subtypes('enormous baloth', ['Beast']).
card_colors('enormous baloth', ['G']).
card_text('enormous baloth', '').
card_mana_cost('enormous baloth', ['6', 'G']).
card_cmc('enormous baloth', 7).
card_layout('enormous baloth', 'normal').
card_power('enormous baloth', 7).
card_toughness('enormous baloth', 7).

% Found in: 8ED, 9ED, DPA, SCG
card_name('enrage', 'Enrage').
card_type('enrage', 'Instant').
card_types('enrage', ['Instant']).
card_subtypes('enrage', []).
card_colors('enrage', ['R']).
card_text('enrage', 'Target creature gets +X/+0 until end of turn.').
card_mana_cost('enrage', ['X', 'R']).
card_cmc('enrage', 1).
card_layout('enrage', 'normal').

% Found in: CNS
card_name('enraged revolutionary', 'Enraged Revolutionary').
card_type('enraged revolutionary', 'Creature — Human Warrior').
card_types('enraged revolutionary', ['Creature']).
card_subtypes('enraged revolutionary', ['Human', 'Warrior']).
card_colors('enraged revolutionary', ['R']).
card_text('enraged revolutionary', 'Dethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)').
card_mana_cost('enraged revolutionary', ['2', 'R']).
card_cmc('enraged revolutionary', 3).
card_layout('enraged revolutionary', 'normal').
card_power('enraged revolutionary', 2).
card_toughness('enraged revolutionary', 1).

% Found in: TMP
card_name('enraging licid', 'Enraging Licid').
card_type('enraging licid', 'Creature — Licid').
card_types('enraging licid', ['Creature']).
card_subtypes('enraging licid', ['Licid']).
card_colors('enraging licid', ['R']).
card_text('enraging licid', '{R}, {T}: Enraging Licid loses this ability and becomes an Aura enchantment with enchant creature. Attach it to target creature. You may pay {R} to end this effect.\nEnchanted creature has haste.').
card_mana_cost('enraging licid', ['1', 'R']).
card_cmc('enraging licid', 2).
card_layout('enraging licid', 'normal').
card_power('enraging licid', 1).
card_toughness('enraging licid', 1).

% Found in: BOK
card_name('enshrined memories', 'Enshrined Memories').
card_type('enshrined memories', 'Sorcery').
card_types('enshrined memories', ['Sorcery']).
card_subtypes('enshrined memories', []).
card_colors('enshrined memories', ['G']).
card_text('enshrined memories', 'Reveal the top X cards of your library. Put all creature cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('enshrined memories', ['X', 'G']).
card_cmc('enshrined memories', 1).
card_layout('enshrined memories', 'normal').

% Found in: ORI
card_name('enshrouding mist', 'Enshrouding Mist').
card_type('enshrouding mist', 'Instant').
card_types('enshrouding mist', ['Instant']).
card_subtypes('enshrouding mist', []).
card_colors('enshrouding mist', ['W']).
card_text('enshrouding mist', 'Target creature gets +1/+1 until end of turn. Prevent all damage that would be dealt to it this turn. If it\'s renowned, untap it.').
card_mana_cost('enshrouding mist', ['W']).
card_cmc('enshrouding mist', 1).
card_layout('enshrouding mist', 'normal').

% Found in: DD3_GVL, DDD, NPH, PLC
card_name('enslave', 'Enslave').
card_type('enslave', 'Enchantment — Aura').
card_types('enslave', ['Enchantment']).
card_subtypes('enslave', ['Aura']).
card_colors('enslave', ['B']).
card_text('enslave', 'Enchant creature\nYou control enchanted creature.\nAt the beginning of your upkeep, enchanted creature deals 1 damage to its owner.').
card_mana_cost('enslave', ['4', 'B', 'B']).
card_cmc('enslave', 6).
card_layout('enslave', 'normal').

% Found in: TOR
card_name('enslaved dwarf', 'Enslaved Dwarf').
card_type('enslaved dwarf', 'Creature — Dwarf').
card_types('enslaved dwarf', ['Creature']).
card_subtypes('enslaved dwarf', ['Dwarf']).
card_colors('enslaved dwarf', ['R']).
card_text('enslaved dwarf', '{R}, Sacrifice Enslaved Dwarf: Target black creature gets +1/+0 and gains first strike until end of turn.').
card_mana_cost('enslaved dwarf', ['R']).
card_cmc('enslaved dwarf', 1).
card_layout('enslaved dwarf', 'normal').
card_power('enslaved dwarf', 1).
card_toughness('enslaved dwarf', 1).

% Found in: MMQ
card_name('enslaved horror', 'Enslaved Horror').
card_type('enslaved horror', 'Creature — Horror').
card_types('enslaved horror', ['Creature']).
card_subtypes('enslaved horror', ['Horror']).
card_colors('enslaved horror', ['B']).
card_text('enslaved horror', 'When Enslaved Horror enters the battlefield, each other player may return a creature card from his or her graveyard to the battlefield.').
card_mana_cost('enslaved horror', ['3', 'B']).
card_cmc('enslaved horror', 4).
card_layout('enslaved horror', 'normal').
card_power('enslaved horror', 4).
card_toughness('enslaved horror', 4).

% Found in: ALL
card_name('enslaved scout', 'Enslaved Scout').
card_type('enslaved scout', 'Creature — Goblin Scout').
card_types('enslaved scout', ['Creature']).
card_subtypes('enslaved scout', ['Goblin', 'Scout']).
card_colors('enslaved scout', ['R']).
card_text('enslaved scout', '{2}: Enslaved Scout gains mountainwalk until end of turn. (It can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('enslaved scout', ['2', 'R']).
card_cmc('enslaved scout', 3).
card_layout('enslaved scout', 'normal').
card_power('enslaved scout', 2).
card_toughness('enslaved scout', 2).

% Found in: NMS
card_name('ensnare', 'Ensnare').
card_type('ensnare', 'Instant').
card_types('ensnare', ['Instant']).
card_subtypes('ensnare', []).
card_colors('ensnare', ['U']).
card_text('ensnare', 'You may return two Islands you control to their owner\'s hand rather than pay Ensnare\'s mana cost.\nTap all creatures.').
card_mana_cost('ensnare', ['3', 'U']).
card_cmc('ensnare', 4).
card_layout('ensnare', 'normal').

% Found in: 7ED, 8ED, STH
card_name('ensnaring bridge', 'Ensnaring Bridge').
card_type('ensnaring bridge', 'Artifact').
card_types('ensnaring bridge', ['Artifact']).
card_subtypes('ensnaring bridge', []).
card_colors('ensnaring bridge', []).
card_text('ensnaring bridge', 'Creatures with power greater than the number of cards in your hand can\'t attack.').
card_mana_cost('ensnaring bridge', ['3']).
card_cmc('ensnaring bridge', 3).
card_layout('ensnaring bridge', 'normal').

% Found in: M15
card_name('ensoul artifact', 'Ensoul Artifact').
card_type('ensoul artifact', 'Enchantment — Aura').
card_types('ensoul artifact', ['Enchantment']).
card_subtypes('ensoul artifact', ['Aura']).
card_colors('ensoul artifact', ['U']).
card_text('ensoul artifact', 'Enchant artifact\nEnchanted artifact is a creature with base power and toughness 5/5 in addition to its other types.').
card_mana_cost('ensoul artifact', ['1', 'U']).
card_cmc('ensoul artifact', 2).
card_layout('ensoul artifact', 'normal').

% Found in: 5DN
card_name('ensouled scimitar', 'Ensouled Scimitar').
card_type('ensouled scimitar', 'Artifact — Equipment').
card_types('ensouled scimitar', ['Artifact']).
card_subtypes('ensouled scimitar', ['Equipment']).
card_colors('ensouled scimitar', []).
card_text('ensouled scimitar', '{3}: Ensouled Scimitar becomes a 1/5 Spirit artifact creature with flying until end of turn. (Equipment that\'s a creature can\'t equip a creature.)\nEquipped creature gets +1/+5.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('ensouled scimitar', ['3']).
card_cmc('ensouled scimitar', 3).
card_layout('ensouled scimitar', 'normal').

% Found in: PCY
card_name('entangler', 'Entangler').
card_type('entangler', 'Enchantment — Aura').
card_types('entangler', ['Enchantment']).
card_subtypes('entangler', ['Aura']).
card_colors('entangler', ['W']).
card_text('entangler', 'Enchant creature\nEnchanted creature can block any number of creatures.').
card_mana_cost('entangler', ['2', 'W', 'W']).
card_cmc('entangler', 4).
card_layout('entangler', 'normal').

% Found in: LRW
card_name('entangling trap', 'Entangling Trap').
card_type('entangling trap', 'Enchantment').
card_types('entangling trap', ['Enchantment']).
card_subtypes('entangling trap', []).
card_colors('entangling trap', ['W']).
card_text('entangling trap', 'Whenever you clash, tap target creature an opponent controls. If you won, that creature doesn\'t untap during its controller\'s next untap step. (This ability triggers after the clash ends.)').
card_mana_cost('entangling trap', ['1', 'W']).
card_cmc('entangling trap', 2).
card_layout('entangling trap', 'normal').

% Found in: M10
card_name('entangling vines', 'Entangling Vines').
card_type('entangling vines', 'Enchantment — Aura').
card_types('entangling vines', ['Enchantment']).
card_subtypes('entangling vines', ['Aura']).
card_colors('entangling vines', ['G']).
card_text('entangling vines', 'Enchant tapped creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_mana_cost('entangling vines', ['3', 'G']).
card_cmc('entangling vines', 4).
card_layout('entangling vines', 'normal').

% Found in: UNH
card_name('enter the dungeon', 'Enter the Dungeon').
card_type('enter the dungeon', 'Sorcery').
card_types('enter the dungeon', ['Sorcery']).
card_subtypes('enter the dungeon', []).
card_colors('enter the dungeon', ['B']).
card_text('enter the dungeon', 'Players play a Magic subgame under the table starting at 5 life, using their libraries as their decks. After the subgame ends, the winner searches his or her library for two cards, puts those cards into his or her hand, then shuffles his or her library.').
card_mana_cost('enter the dungeon', ['B', 'B']).
card_cmc('enter the dungeon', 2).
card_layout('enter the dungeon', 'normal').

% Found in: GTC
card_name('enter the infinite', 'Enter the Infinite').
card_type('enter the infinite', 'Sorcery').
card_types('enter the infinite', ['Sorcery']).
card_subtypes('enter the infinite', []).
card_colors('enter the infinite', ['U']).
card_text('enter the infinite', 'Draw cards equal to the number of cards in your library, then put a card from your hand on top of your library. You have no maximum hand size until your next turn.').
card_mana_cost('enter the infinite', ['8', 'U', 'U', 'U', 'U']).
card_cmc('enter the infinite', 12).
card_layout('enter the infinite', 'normal').

% Found in: DGM, pLPA
card_name('entering', 'Entering').
card_type('entering', 'Sorcery').
card_types('entering', ['Sorcery']).
card_subtypes('entering', []).
card_colors('entering', ['B', 'R']).
card_text('entering', 'Put a creature card from a graveyard onto the battlefield under your control. It gains haste until end of turn.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('entering', ['4', 'B', 'R']).
card_cmc('entering', 6).
card_layout('entering', 'split').

% Found in: ORI
card_name('enthralling victor', 'Enthralling Victor').
card_type('enthralling victor', 'Creature — Human Warrior').
card_types('enthralling victor', ['Creature']).
card_subtypes('enthralling victor', ['Human', 'Warrior']).
card_colors('enthralling victor', ['R']).
card_text('enthralling victor', 'When Enthralling Victor enters the battlefield, gain control of target creature an opponent controls with power 2 or less until end of turn. Untap that creature. It gains haste until end of turn. (It can attack and {T} this turn.)').
card_mana_cost('enthralling victor', ['3', 'R']).
card_cmc('enthralling victor', 4).
card_layout('enthralling victor', 'normal').
card_power('enthralling victor', 3).
card_toughness('enthralling victor', 2).

% Found in: ODY, PD3, pJGP
card_name('entomb', 'Entomb').
card_type('entomb', 'Instant').
card_types('entomb', ['Instant']).
card_subtypes('entomb', []).
card_colors('entomb', ['B']).
card_text('entomb', 'Search your library for a card and put that card into your graveyard. Then shuffle your library.').
card_mana_cost('entomb', ['B']).
card_cmc('entomb', 1).
card_layout('entomb', 'normal').

% Found in: NPH
card_name('entomber exarch', 'Entomber Exarch').
card_type('entomber exarch', 'Creature — Cleric').
card_types('entomber exarch', ['Creature']).
card_subtypes('entomber exarch', ['Cleric']).
card_colors('entomber exarch', ['B']).
card_text('entomber exarch', 'When Entomber Exarch enters the battlefield, choose one —\n• Return target creature card from your graveyard to your hand.\n• Target opponent reveals his or her hand, you choose a noncreature card from it, then that player discards that card.').
card_mana_cost('entomber exarch', ['2', 'B', 'B']).
card_cmc('entomber exarch', 4).
card_layout('entomber exarch', 'normal').
card_power('entomber exarch', 2).
card_toughness('entomber exarch', 2).

% Found in: ONS
card_name('entrails feaster', 'Entrails Feaster').
card_type('entrails feaster', 'Creature — Zombie Cat').
card_types('entrails feaster', ['Creature']).
card_subtypes('entrails feaster', ['Zombie', 'Cat']).
card_colors('entrails feaster', ['B']).
card_text('entrails feaster', 'At the beginning of your upkeep, you may exile a creature card from a graveyard. If you do, put a +1/+1 counter on Entrails Feaster. If you don\'t, tap Entrails Feaster.').
card_mana_cost('entrails feaster', ['B']).
card_cmc('entrails feaster', 1).
card_layout('entrails feaster', 'normal').
card_power('entrails feaster', 1).
card_toughness('entrails feaster', 1).

% Found in: AVR, V15
card_name('entreat the angels', 'Entreat the Angels').
card_type('entreat the angels', 'Sorcery').
card_types('entreat the angels', ['Sorcery']).
card_subtypes('entreat the angels', []).
card_colors('entreat the angels', ['W']).
card_text('entreat the angels', 'Put X 4/4 white Angel creature tokens with flying onto the battlefield.\nMiracle {X}{W}{W} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_mana_cost('entreat the angels', ['X', 'X', 'W', 'W', 'W']).
card_cmc('entreat the angels', 3).
card_layout('entreat the angels', 'normal').

% Found in: DIS
card_name('entropic eidolon', 'Entropic Eidolon').
card_type('entropic eidolon', 'Creature — Spirit').
card_types('entropic eidolon', ['Creature']).
card_subtypes('entropic eidolon', ['Spirit']).
card_colors('entropic eidolon', ['B']).
card_text('entropic eidolon', '{B}, Sacrifice Entropic Eidolon: Target player loses 1 life and you gain 1 life.\nWhenever you cast a multicolored spell, you may return Entropic Eidolon from your graveyard to your hand.').
card_mana_cost('entropic eidolon', ['3', 'B']).
card_cmc('entropic eidolon', 4).
card_layout('entropic eidolon', 'normal').
card_power('entropic eidolon', 2).
card_toughness('entropic eidolon', 2).

% Found in: EXO
card_name('entropic specter', 'Entropic Specter').
card_type('entropic specter', 'Creature — Specter Spirit').
card_types('entropic specter', ['Creature']).
card_subtypes('entropic specter', ['Specter', 'Spirit']).
card_colors('entropic specter', ['B']).
card_text('entropic specter', 'Flying\nAs Entropic Specter enters the battlefield, choose an opponent.\nEntropic Specter\'s power and toughness are each equal to the number of cards in the chosen player\'s hand.\nWhenever Entropic Specter deals damage to a player, that player discards a card.').
card_mana_cost('entropic specter', ['3', 'B', 'B']).
card_cmc('entropic specter', 5).
card_layout('entropic specter', 'normal').
card_power('entropic specter', '*').
card_toughness('entropic specter', '*').

% Found in: JUD
card_name('envelop', 'Envelop').
card_type('envelop', 'Instant').
card_types('envelop', ['Instant']).
card_subtypes('envelop', []).
card_colors('envelop', ['U']).
card_text('envelop', 'Counter target sorcery spell.').
card_mana_cost('envelop', ['U']).
card_cmc('envelop', 1).
card_layout('envelop', 'normal').

% Found in: 5DN
card_name('eon hub', 'Eon Hub').
card_type('eon hub', 'Artifact').
card_types('eon hub', ['Artifact']).
card_subtypes('eon hub', []).
card_colors('eon hub', []).
card_text('eon hub', 'Players skip their upkeep steps.').
card_mana_cost('eon hub', ['5']).
card_cmc('eon hub', 5).
card_layout('eon hub', 'normal').

% Found in: BNG
card_name('ephara\'s enlightenment', 'Ephara\'s Enlightenment').
card_type('ephara\'s enlightenment', 'Enchantment — Aura').
card_types('ephara\'s enlightenment', ['Enchantment']).
card_subtypes('ephara\'s enlightenment', ['Aura']).
card_colors('ephara\'s enlightenment', ['W', 'U']).
card_text('ephara\'s enlightenment', 'Enchant creature\nWhen Ephara\'s Enlightenment enters the battlefield, put a +1/+1 counter on enchanted creature.\nEnchanted creature has flying.\nWhenever a creature enters the battlefield under your control, you may return Ephara\'s Enlightenment to its owner\'s hand.').
card_mana_cost('ephara\'s enlightenment', ['1', 'W', 'U']).
card_cmc('ephara\'s enlightenment', 3).
card_layout('ephara\'s enlightenment', 'normal').

% Found in: BNG
card_name('ephara\'s radiance', 'Ephara\'s Radiance').
card_type('ephara\'s radiance', 'Enchantment — Aura').
card_types('ephara\'s radiance', ['Enchantment']).
card_subtypes('ephara\'s radiance', ['Aura']).
card_colors('ephara\'s radiance', ['W']).
card_text('ephara\'s radiance', 'Enchant creature\nEnchanted creature has \"{1}{W}, {T}: You gain 3 life.\"').
card_mana_cost('ephara\'s radiance', ['W']).
card_cmc('ephara\'s radiance', 1).
card_layout('ephara\'s radiance', 'normal').

% Found in: THS
card_name('ephara\'s warden', 'Ephara\'s Warden').
card_type('ephara\'s warden', 'Creature — Human Cleric').
card_types('ephara\'s warden', ['Creature']).
card_subtypes('ephara\'s warden', ['Human', 'Cleric']).
card_colors('ephara\'s warden', ['W']).
card_text('ephara\'s warden', '{T}: Tap target creature with power 3 or less.').
card_mana_cost('ephara\'s warden', ['3', 'W']).
card_cmc('ephara\'s warden', 4).
card_layout('ephara\'s warden', 'normal').
card_power('ephara\'s warden', 1).
card_toughness('ephara\'s warden', 2).

% Found in: BNG
card_name('ephara, god of the polis', 'Ephara, God of the Polis').
card_type('ephara, god of the polis', 'Legendary Enchantment Creature — God').
card_types('ephara, god of the polis', ['Enchantment', 'Creature']).
card_subtypes('ephara, god of the polis', ['God']).
card_supertypes('ephara, god of the polis', ['Legendary']).
card_colors('ephara, god of the polis', ['W', 'U']).
card_text('ephara, god of the polis', 'Indestructible\nAs long as your devotion to white and blue is less than seven, Ephara isn\'t a creature.\nAt the beginning of each upkeep, if you had another creature enter the battlefield under your control last turn, draw a card.').
card_mana_cost('ephara, god of the polis', ['2', 'W', 'U']).
card_cmc('ephara, god of the polis', 4).
card_layout('ephara, god of the polis', 'normal').
card_power('ephara, god of the polis', 6).
card_toughness('ephara, god of the polis', 5).

% Found in: M15
card_name('ephemeral shields', 'Ephemeral Shields').
card_type('ephemeral shields', 'Instant').
card_types('ephemeral shields', ['Instant']).
card_subtypes('ephemeral shields', []).
card_colors('ephemeral shields', ['W']).
card_text('ephemeral shields', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nTarget creature gains indestructible until end of turn. (Damage and effects that say \"destroy\" don\'t destroy it.)').
card_mana_cost('ephemeral shields', ['1', 'W']).
card_cmc('ephemeral shields', 2).
card_layout('ephemeral shields', 'normal').

% Found in: EXO, TPR, VMA
card_name('ephemeron', 'Ephemeron').
card_type('ephemeron', 'Creature — Illusion').
card_types('ephemeron', ['Creature']).
card_subtypes('ephemeron', ['Illusion']).
card_colors('ephemeron', ['U']).
card_text('ephemeron', 'Flying\nDiscard a card: Return Ephemeron to its owner\'s hand.').
card_mana_cost('ephemeron', ['4', 'U', 'U']).
card_cmc('ephemeron', 6).
card_layout('ephemeron', 'normal').
card_power('ephemeron', 4).
card_toughness('ephemeron', 4).

% Found in: DTK
card_name('epic confrontation', 'Epic Confrontation').
card_type('epic confrontation', 'Sorcery').
card_types('epic confrontation', ['Sorcery']).
card_subtypes('epic confrontation', []).
card_colors('epic confrontation', ['G']).
card_text('epic confrontation', 'Target creature you control gets +1/+2 until end of turn. It fights target creature you don\'t control. (Each deals damage equal to its power to the other.)').
card_mana_cost('epic confrontation', ['1', 'G']).
card_cmc('epic confrontation', 2).
card_layout('epic confrontation', 'normal').

% Found in: RTR
card_name('epic experiment', 'Epic Experiment').
card_type('epic experiment', 'Sorcery').
card_types('epic experiment', ['Sorcery']).
card_subtypes('epic experiment', []).
card_colors('epic experiment', ['U', 'R']).
card_text('epic experiment', 'Exile the top X cards of your library. For each instant and sorcery card with converted mana cost X or less among them, you may cast that card without paying its mana cost. Then put all cards exiled this way that weren\'t cast into your graveyard.').
card_mana_cost('epic experiment', ['X', 'U', 'R']).
card_cmc('epic experiment', 2).
card_layout('epic experiment', 'normal').

% Found in: LRW
card_name('epic proportions', 'Epic Proportions').
card_type('epic proportions', 'Enchantment — Aura').
card_types('epic proportions', ['Enchantment']).
card_subtypes('epic proportions', ['Aura']).
card_colors('epic proportions', ['G']).
card_text('epic proportions', 'Flash\nEnchant creature\nEnchanted creature gets +5/+5 and has trample.').
card_mana_cost('epic proportions', ['4', 'G', 'G']).
card_cmc('epic proportions', 6).
card_layout('epic proportions', 'normal').

% Found in: JUD
card_name('epic struggle', 'Epic Struggle').
card_type('epic struggle', 'Enchantment').
card_types('epic struggle', ['Enchantment']).
card_subtypes('epic struggle', []).
card_colors('epic struggle', ['G']).
card_text('epic struggle', 'At the beginning of your upkeep, if you control twenty or more creatures, you win the game.').
card_mana_cost('epic struggle', ['2', 'G', 'G']).
card_cmc('epic struggle', 4).
card_layout('epic struggle', 'normal').

% Found in: ODY
card_name('epicenter', 'Epicenter').
card_type('epicenter', 'Sorcery').
card_types('epicenter', ['Sorcery']).
card_subtypes('epicenter', []).
card_colors('epicenter', ['R']).
card_text('epicenter', 'Target player sacrifices a land.\nThreshold — Each player sacrifices all lands he or she controls instead if seven or more cards are in your graveyard.').
card_mana_cost('epicenter', ['4', 'R']).
card_cmc('epicenter', 5).
card_layout('epicenter', 'normal').

% Found in: BNG
card_name('epiphany storm', 'Epiphany Storm').
card_type('epiphany storm', 'Enchantment — Aura').
card_types('epiphany storm', ['Enchantment']).
card_subtypes('epiphany storm', ['Aura']).
card_colors('epiphany storm', ['R']).
card_text('epiphany storm', 'Enchant creature\nEnchanted creature has \"{R}, {T}, Discard a card: Draw a card.\"').
card_mana_cost('epiphany storm', ['R']).
card_cmc('epiphany storm', 1).
card_layout('epiphany storm', 'normal').

% Found in: C14, FUT, MMA
card_name('epochrasite', 'Epochrasite').
card_type('epochrasite', 'Artifact Creature — Construct').
card_types('epochrasite', ['Artifact', 'Creature']).
card_subtypes('epochrasite', ['Construct']).
card_colors('epochrasite', []).
card_text('epochrasite', 'Epochrasite enters the battlefield with three +1/+1 counters on it if you didn\'t cast it from your hand.\nWhen Epochrasite dies, exile it with three time counters on it and it gains suspend. (At the beginning of your upkeep, remove a time counter. When the last is removed, cast this card without paying its mana cost. It has haste.)').
card_mana_cost('epochrasite', ['2']).
card_cmc('epochrasite', 2).
card_layout('epochrasite', 'normal').
card_power('epochrasite', 1).
card_toughness('epochrasite', 1).

% Found in: TOR
card_name('equal treatment', 'Equal Treatment').
card_type('equal treatment', 'Instant').
card_types('equal treatment', ['Instant']).
card_subtypes('equal treatment', []).
card_colors('equal treatment', ['W']).
card_text('equal treatment', 'If any source would deal 1 or more damage to a creature or player this turn, it deals 2 damage to that creature or player instead.\nDraw a card.').
card_mana_cost('equal treatment', ['1', 'W']).
card_cmc('equal treatment', 2).
card_layout('equal treatment', 'normal').

% Found in: 7ED, EXO
card_name('equilibrium', 'Equilibrium').
card_type('equilibrium', 'Enchantment').
card_types('equilibrium', ['Enchantment']).
card_subtypes('equilibrium', []).
card_colors('equilibrium', ['U']).
card_text('equilibrium', 'Whenever you cast a creature spell, you may pay {1}. If you do, return target creature to its owner\'s hand.').
card_mana_cost('equilibrium', ['1', 'U', 'U']).
card_cmc('equilibrium', 3).
card_layout('equilibrium', 'normal').

% Found in: LEG
card_name('equinox', 'Equinox').
card_type('equinox', 'Enchantment — Aura').
card_types('equinox', ['Enchantment']).
card_subtypes('equinox', ['Aura']).
card_colors('equinox', ['W']).
card_text('equinox', 'Enchant land\nEnchanted land has \"{T}: Counter target spell if it would destroy a land you control.\"').
card_mana_cost('equinox', ['W']).
card_cmc('equinox', 1).
card_layout('equinox', 'normal').

% Found in: VIS
card_name('equipoise', 'Equipoise').
card_type('equipoise', 'Enchantment').
card_types('equipoise', ['Enchantment']).
card_subtypes('equipoise', []).
card_colors('equipoise', ['W']).
card_text('equipoise', 'At the beginning of your upkeep, for each land target player controls in excess of the number you control, choose a land he or she controls, then the chosen permanents phase out. Repeat this process for artifacts and creatures. (While they\'re phased out, they\'re treated as though they don\'t exist. They phase in before that player untaps during his or her next untap step.)').
card_mana_cost('equipoise', ['2', 'W']).
card_cmc('equipoise', 3).
card_layout('equipoise', 'normal').
card_reserved('equipoise').

% Found in: BOK, UDS
card_name('eradicate', 'Eradicate').
card_type('eradicate', 'Sorcery').
card_types('eradicate', ['Sorcery']).
card_subtypes('eradicate', []).
card_colors('eradicate', ['B']).
card_text('eradicate', 'Exile target nonblack creature. Search its controller\'s graveyard, hand, and library for all cards with the same name as that creature and exile them. Then that player shuffles his or her library.').
card_mana_cost('eradicate', ['2', 'B', 'B']).
card_cmc('eradicate', 4).
card_layout('eradicate', 'normal').

% Found in: KTK, M13, ULG
card_name('erase', 'Erase').
card_type('erase', 'Instant').
card_types('erase', ['Instant']).
card_subtypes('erase', []).
card_colors('erase', ['W']).
card_text('erase', 'Exile target enchantment.').
card_mana_cost('erase', ['W']).
card_cmc('erase', 1).
card_layout('erase', 'normal').

% Found in: UNH
card_name('erase (not the urza\'s legacy one)', 'Erase (Not the Urza\'s Legacy One)').
card_type('erase (not the urza\'s legacy one)', 'Instant').
card_types('erase (not the urza\'s legacy one)', ['Instant']).
card_subtypes('erase (not the urza\'s legacy one)', []).
card_colors('erase (not the urza\'s legacy one)', ['W']).
card_text('erase (not the urza\'s legacy one)', 'If you control two or more white permanents that share an artist, you may play Erase (Not the Urza\'s Legacy One) without paying its mana cost.\nRemove target enchantment from the game.').
card_mana_cost('erase (not the urza\'s legacy one)', ['2', 'W']).
card_cmc('erase (not the urza\'s legacy one)', 3).
card_layout('erase (not the urza\'s legacy one)', 'normal').

% Found in: SOK
card_name('erayo\'s essence', 'Erayo\'s Essence').
card_type('erayo\'s essence', 'Legendary Enchantment').
card_types('erayo\'s essence', ['Enchantment']).
card_subtypes('erayo\'s essence', []).
card_supertypes('erayo\'s essence', ['Legendary']).
card_colors('erayo\'s essence', ['U']).
card_text('erayo\'s essence', 'Whenever an opponent casts a spell for the first time in a turn, counter that spell.').
card_mana_cost('erayo\'s essence', ['1', 'U']).
card_cmc('erayo\'s essence', 2).
card_layout('erayo\'s essence', 'flip').

% Found in: SOK
card_name('erayo, soratami ascendant', 'Erayo, Soratami Ascendant').
card_type('erayo, soratami ascendant', 'Legendary Creature — Moonfolk Monk').
card_types('erayo, soratami ascendant', ['Creature']).
card_subtypes('erayo, soratami ascendant', ['Moonfolk', 'Monk']).
card_supertypes('erayo, soratami ascendant', ['Legendary']).
card_colors('erayo, soratami ascendant', ['U']).
card_text('erayo, soratami ascendant', 'Flying\nWhenever the fourth spell of a turn is cast, flip Erayo, Soratami Ascendant.').
card_mana_cost('erayo, soratami ascendant', ['1', 'U']).
card_cmc('erayo, soratami ascendant', 2).
card_layout('erayo, soratami ascendant', 'flip').
card_power('erayo, soratami ascendant', 1).
card_toughness('erayo, soratami ascendant', 1).
card_sides('erayo, soratami ascendant', 'erayo\'s essence').

% Found in: DKA
card_name('erdwal ripper', 'Erdwal Ripper').
card_type('erdwal ripper', 'Creature — Vampire').
card_types('erdwal ripper', ['Creature']).
card_subtypes('erdwal ripper', ['Vampire']).
card_colors('erdwal ripper', ['R']).
card_text('erdwal ripper', 'Haste\nWhenever Erdwal Ripper deals combat damage to a player, put a +1/+1 counter on it.').
card_mana_cost('erdwal ripper', ['1', 'R', 'R']).
card_cmc('erdwal ripper', 3).
card_layout('erdwal ripper', 'normal').
card_power('erdwal ripper', 2).
card_toughness('erdwal ripper', 1).

% Found in: THS
card_name('erebos\'s emissary', 'Erebos\'s Emissary').
card_type('erebos\'s emissary', 'Enchantment Creature — Snake').
card_types('erebos\'s emissary', ['Enchantment', 'Creature']).
card_subtypes('erebos\'s emissary', ['Snake']).
card_colors('erebos\'s emissary', ['B']).
card_text('erebos\'s emissary', 'Bestow {5}{B} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nDiscard a creature card: Erebos\'s Emissary gets +2/+2 until end of turn. If Erebos\'s Emissary is an Aura, enchanted creature gets +2/+2 until end of turn instead.\nEnchanted creature gets +3/+3.').
card_mana_cost('erebos\'s emissary', ['3', 'B']).
card_cmc('erebos\'s emissary', 4).
card_layout('erebos\'s emissary', 'normal').
card_power('erebos\'s emissary', 3).
card_toughness('erebos\'s emissary', 3).

% Found in: ORI
card_name('erebos\'s titan', 'Erebos\'s Titan').
card_type('erebos\'s titan', 'Creature — Giant').
card_types('erebos\'s titan', ['Creature']).
card_subtypes('erebos\'s titan', ['Giant']).
card_colors('erebos\'s titan', ['B']).
card_text('erebos\'s titan', 'As long as your opponents control no creatures, Erebos\'s Titan has indestructible. (Damage and effects that say \"destroy\" don\'t destroy it.)\nWhenever a creature card leaves an opponent\'s graveyard, you may discard a card. If you do, return Erebos\'s Titan from your graveyard to your hand.').
card_mana_cost('erebos\'s titan', ['1', 'B', 'B', 'B']).
card_cmc('erebos\'s titan', 4).
card_layout('erebos\'s titan', 'normal').
card_power('erebos\'s titan', 5).
card_toughness('erebos\'s titan', 5).

% Found in: THS
card_name('erebos, god of the dead', 'Erebos, God of the Dead').
card_type('erebos, god of the dead', 'Legendary Enchantment Creature — God').
card_types('erebos, god of the dead', ['Enchantment', 'Creature']).
card_subtypes('erebos, god of the dead', ['God']).
card_supertypes('erebos, god of the dead', ['Legendary']).
card_colors('erebos, god of the dead', ['B']).
card_text('erebos, god of the dead', 'Indestructible\nAs long as your devotion to black is less than five, Erebos isn\'t a creature. (Each {B} in the mana costs of permanents you control counts toward your devotion to black.)\nYour opponents can\'t gain life.\n{1}{B}, Pay 2 life: Draw a card.').
card_mana_cost('erebos, god of the dead', ['3', 'B']).
card_cmc('erebos, god of the dead', 4).
card_layout('erebos, god of the dead', 'normal').
card_power('erebos, god of the dead', 5).
card_toughness('erebos, god of the dead', 7).

% Found in: 3ED, 4ED, 5ED, ARN, MED
card_name('erg raiders', 'Erg Raiders').
card_type('erg raiders', 'Creature — Human Warrior').
card_types('erg raiders', ['Creature']).
card_subtypes('erg raiders', ['Human', 'Warrior']).
card_colors('erg raiders', ['B']).
card_text('erg raiders', 'At the beginning of your end step, if Erg Raiders didn\'t attack this turn, Erg Raiders deals 2 damage to you unless it came under your control this turn.').
card_mana_cost('erg raiders', ['1', 'B']).
card_cmc('erg raiders', 2).
card_layout('erg raiders', 'normal').
card_power('erg raiders', 2).
card_toughness('erg raiders', 3).

% Found in: ARN, ATH, BTD, CHR, JUD, VMA
card_name('erhnam djinn', 'Erhnam Djinn').
card_type('erhnam djinn', 'Creature — Djinn').
card_types('erhnam djinn', ['Creature']).
card_subtypes('erhnam djinn', ['Djinn']).
card_colors('erhnam djinn', ['G']).
card_text('erhnam djinn', 'At the beginning of your upkeep, target non-Wall creature an opponent controls gains forestwalk until your next upkeep. (It can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('erhnam djinn', ['3', 'G']).
card_cmc('erhnam djinn', 4).
card_layout('erhnam djinn', 'normal').
card_power('erhnam djinn', 4).
card_toughness('erhnam djinn', 5).

% Found in: VAN
card_name('erhnam djinn avatar', 'Erhnam Djinn Avatar').
card_type('erhnam djinn avatar', 'Vanguard').
card_types('erhnam djinn avatar', ['Vanguard']).
card_subtypes('erhnam djinn avatar', []).
card_colors('erhnam djinn avatar', []).
card_text('erhnam djinn avatar', 'Whenever you cast a creature spell, put a 1/1 green Saproling creature token onto the battlefield.').
card_layout('erhnam djinn avatar', 'vanguard').

% Found in: MMQ
card_name('erithizon', 'Erithizon').
card_type('erithizon', 'Creature — Beast').
card_types('erithizon', ['Creature']).
card_subtypes('erithizon', ['Beast']).
card_colors('erithizon', ['G']).
card_text('erithizon', 'Whenever Erithizon attacks, put a +1/+1 counter on target creature of defending player\'s choice.').
card_mana_cost('erithizon', ['2', 'G', 'G']).
card_cmc('erithizon', 4).
card_layout('erithizon', 'normal').
card_power('erithizon', 4).
card_toughness('erithizon', 4).

% Found in: HML, TSB
card_name('eron the relentless', 'Eron the Relentless').
card_type('eron the relentless', 'Legendary Creature — Human Rogue').
card_types('eron the relentless', ['Creature']).
card_subtypes('eron the relentless', ['Human', 'Rogue']).
card_supertypes('eron the relentless', ['Legendary']).
card_colors('eron the relentless', ['R']).
card_text('eron the relentless', 'Haste\n{R}{R}{R}: Regenerate Eron the Relentless.').
card_mana_cost('eron the relentless', ['3', 'R', 'R']).
card_cmc('eron the relentless', 5).
card_layout('eron the relentless', 'normal').
card_power('eron the relentless', 5).
card_toughness('eron the relentless', 2).

% Found in: 4ED, DRK
card_name('erosion', 'Erosion').
card_type('erosion', 'Enchantment — Aura').
card_types('erosion', ['Enchantment']).
card_subtypes('erosion', ['Aura']).
card_colors('erosion', ['U']).
card_text('erosion', 'Enchant land\nAt the beginning of the upkeep of enchanted land\'s controller, destroy that land unless that player pays {1} or 1 life.').
card_mana_cost('erosion', ['U', 'U', 'U']).
card_cmc('erosion', 3).
card_layout('erosion', 'normal').

% Found in: ALL, ME2
card_name('errand of duty', 'Errand of Duty').
card_type('errand of duty', 'Instant').
card_types('errand of duty', ['Instant']).
card_subtypes('errand of duty', []).
card_colors('errand of duty', ['W']).
card_text('errand of duty', 'Put a 1/1 white Knight creature token with banding onto the battlefield. (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('errand of duty', ['1', 'W']).
card_cmc('errand of duty', 2).
card_layout('errand of duty', 'normal').

% Found in: TSP
card_name('errant doomsayers', 'Errant Doomsayers').
card_type('errant doomsayers', 'Creature — Human Rebel').
card_types('errant doomsayers', ['Creature']).
card_subtypes('errant doomsayers', ['Human', 'Rebel']).
card_colors('errant doomsayers', ['W']).
card_text('errant doomsayers', '{T}: Tap target creature with toughness 2 or less.').
card_mana_cost('errant doomsayers', ['1', 'W']).
card_cmc('errant doomsayers', 2).
card_layout('errant doomsayers', 'normal').
card_power('errant doomsayers', 1).
card_toughness('errant doomsayers', 1).

% Found in: DD2, DD3_JVC, DDM, MMA, TSP
card_name('errant ephemeron', 'Errant Ephemeron').
card_type('errant ephemeron', 'Creature — Illusion').
card_types('errant ephemeron', ['Creature']).
card_subtypes('errant ephemeron', ['Illusion']).
card_colors('errant ephemeron', ['U']).
card_text('errant ephemeron', 'Flying\nSuspend 4—{1}{U} (Rather than cast this card from your hand, you may pay {1}{U} and exile it with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_mana_cost('errant ephemeron', ['6', 'U']).
card_cmc('errant ephemeron', 7).
card_layout('errant ephemeron', 'normal').
card_power('errant ephemeron', 4).
card_toughness('errant ephemeron', 4).

% Found in: ICE
card_name('errant minion', 'Errant Minion').
card_type('errant minion', 'Enchantment — Aura').
card_types('errant minion', ['Enchantment']).
card_subtypes('errant minion', ['Aura']).
card_colors('errant minion', ['U']).
card_text('errant minion', 'Enchant creature\nAt the beginning of the upkeep of enchanted creature\'s controller, that player may pay any amount of mana. Errant Minion deals 2 damage to that player. Prevent X of that damage, where X is the amount of mana that player paid this way.').
card_mana_cost('errant minion', ['2', 'U']).
card_cmc('errant minion', 3).
card_layout('errant minion', 'normal').

% Found in: 5ED, ICE, ME2
card_name('errantry', 'Errantry').
card_type('errantry', 'Enchantment — Aura').
card_types('errantry', ['Enchantment']).
card_subtypes('errantry', ['Aura']).
card_colors('errantry', ['R']).
card_text('errantry', 'Enchant creature\nEnchanted creature gets +3/+0 and can only attack alone.').
card_mana_cost('errantry', ['1', 'R']).
card_cmc('errantry', 2).
card_layout('errantry', 'normal').

% Found in: ONS, PC2
card_name('erratic explosion', 'Erratic Explosion').
card_type('erratic explosion', 'Sorcery').
card_types('erratic explosion', ['Sorcery']).
card_subtypes('erratic explosion', []).
card_colors('erratic explosion', ['R']).
card_text('erratic explosion', 'Choose target creature or player. Reveal cards from the top of your library until you reveal a nonland card. Erratic Explosion deals damage equal to that card\'s converted mana cost to that creature or player. Put the revealed cards on the bottom of your library in any order.').
card_mana_cost('erratic explosion', ['2', 'R']).
card_cmc('erratic explosion', 3).
card_layout('erratic explosion', 'normal').

% Found in: MMA, PLC
card_name('erratic mutation', 'Erratic Mutation').
card_type('erratic mutation', 'Instant').
card_types('erratic mutation', ['Instant']).
card_subtypes('erratic mutation', []).
card_colors('erratic mutation', ['U']).
card_text('erratic mutation', 'Choose target creature. Reveal cards from the top of your library until you reveal a nonland card. That creature gets +X/-X until end of turn, where X is that card\'s converted mana cost. Put all cards revealed this way on the bottom of your library in any order.').
card_mana_cost('erratic mutation', ['2', 'U']).
card_cmc('erratic mutation', 3).
card_layout('erratic mutation', 'normal').

% Found in: EXO, TPR
card_name('erratic portal', 'Erratic Portal').
card_type('erratic portal', 'Artifact').
card_types('erratic portal', ['Artifact']).
card_subtypes('erratic portal', []).
card_colors('erratic portal', []).
card_text('erratic portal', '{1}, {T}: Return target creature to its owner\'s hand unless its controller pays {1}.').
card_mana_cost('erratic portal', ['4']).
card_cmc('erratic portal', 4).
card_layout('erratic portal', 'normal').

% Found in: DIS
card_name('error', 'Error').
card_type('error', 'Instant').
card_types('error', ['Instant']).
card_subtypes('error', []).
card_colors('error', ['U', 'B']).
card_text('error', 'Counter target multicolored spell.').
card_mana_cost('error', ['U', 'B']).
card_cmc('error', 2).
card_layout('error', 'split').

% Found in: MIR
card_name('ersatz gnomes', 'Ersatz Gnomes').
card_type('ersatz gnomes', 'Artifact Creature — Gnome').
card_types('ersatz gnomes', ['Artifact', 'Creature']).
card_subtypes('ersatz gnomes', ['Gnome']).
card_colors('ersatz gnomes', []).
card_text('ersatz gnomes', '{T}: Target spell becomes colorless.\n{T}: Target permanent becomes colorless until end of turn.').
card_mana_cost('ersatz gnomes', ['3']).
card_cmc('ersatz gnomes', 3).
card_layout('ersatz gnomes', 'normal').
card_power('ersatz gnomes', 1).
card_toughness('ersatz gnomes', 1).

% Found in: VAN
card_name('ertai', 'Ertai').
card_type('ertai', 'Vanguard').
card_types('ertai', ['Vanguard']).
card_subtypes('ertai', []).
card_colors('ertai', []).
card_text('ertai', 'Creatures you control have hexproof. (They can\'t be the targets of spells or abilities your opponents control.)').
card_layout('ertai', 'vanguard').

% Found in: WTH
card_name('ertai\'s familiar', 'Ertai\'s Familiar').
card_type('ertai\'s familiar', 'Creature — Illusion').
card_types('ertai\'s familiar', ['Creature']).
card_subtypes('ertai\'s familiar', ['Illusion']).
card_colors('ertai\'s familiar', ['U']).
card_text('ertai\'s familiar', 'Phasing (This phases in or out before you untap during each of your untap steps. While it\'s phased out, it\'s treated as though it doesn\'t exist.)\nWhen Ertai\'s Familiar phases out or leaves the battlefield, put the top three cards of your library into your graveyard.\n{U}: Until your next upkeep, Ertai\'s Familiar can\'t phase out.').
card_mana_cost('ertai\'s familiar', ['1', 'U']).
card_cmc('ertai\'s familiar', 2).
card_layout('ertai\'s familiar', 'normal').
card_power('ertai\'s familiar', 2).
card_toughness('ertai\'s familiar', 2).
card_reserved('ertai\'s familiar').

% Found in: TMP
card_name('ertai\'s meddling', 'Ertai\'s Meddling').
card_type('ertai\'s meddling', 'Instant').
card_types('ertai\'s meddling', ['Instant']).
card_subtypes('ertai\'s meddling', []).
card_colors('ertai\'s meddling', ['U']).
card_text('ertai\'s meddling', 'X can\'t be 0.\nTarget spell\'s controller exiles it with X delay counters on it.\nAt the beginning of each of that player\'s upkeeps, if that card is exiled, remove a delay counter from it. If the card has no delay counters on it, he or she puts it onto the stack as a copy of the original spell.').
card_mana_cost('ertai\'s meddling', ['X', 'U']).
card_cmc('ertai\'s meddling', 1).
card_layout('ertai\'s meddling', 'normal').

% Found in: PLS
card_name('ertai\'s trickery', 'Ertai\'s Trickery').
card_type('ertai\'s trickery', 'Instant').
card_types('ertai\'s trickery', ['Instant']).
card_subtypes('ertai\'s trickery', []).
card_colors('ertai\'s trickery', ['U']).
card_text('ertai\'s trickery', 'Counter target spell if it was kicked.').
card_mana_cost('ertai\'s trickery', ['U']).
card_cmc('ertai\'s trickery', 1).
card_layout('ertai\'s trickery', 'normal').

% Found in: PLS
card_name('ertai, the corrupted', 'Ertai, the Corrupted').
card_type('ertai, the corrupted', 'Legendary Creature — Human Wizard').
card_types('ertai, the corrupted', ['Creature']).
card_subtypes('ertai, the corrupted', ['Human', 'Wizard']).
card_supertypes('ertai, the corrupted', ['Legendary']).
card_colors('ertai, the corrupted', ['W', 'U', 'B']).
card_text('ertai, the corrupted', '{U}, {T}, Sacrifice a creature or enchantment: Counter target spell.').
card_mana_cost('ertai, the corrupted', ['2', 'W', 'U', 'B']).
card_cmc('ertai, the corrupted', 5).
card_layout('ertai, the corrupted', 'normal').
card_power('ertai, the corrupted', 3).
card_toughness('ertai, the corrupted', 4).

% Found in: EXO
card_name('ertai, wizard adept', 'Ertai, Wizard Adept').
card_type('ertai, wizard adept', 'Legendary Creature — Human Wizard').
card_types('ertai, wizard adept', ['Creature']).
card_subtypes('ertai, wizard adept', ['Human', 'Wizard']).
card_supertypes('ertai, wizard adept', ['Legendary']).
card_colors('ertai, wizard adept', ['U']).
card_text('ertai, wizard adept', '{2}{U}{U}, {T}: Counter target spell.').
card_mana_cost('ertai, wizard adept', ['2', 'U']).
card_cmc('ertai, wizard adept', 3).
card_layout('ertai, wizard adept', 'normal').
card_power('ertai, wizard adept', 1).
card_toughness('ertai, wizard adept', 1).
card_reserved('ertai, wizard adept').

% Found in: ODY
card_name('escape artist', 'Escape Artist').
card_type('escape artist', 'Creature — Human Wizard').
card_types('escape artist', ['Creature']).
card_subtypes('escape artist', ['Human', 'Wizard']).
card_colors('escape artist', ['U']).
card_text('escape artist', 'Escape Artist can\'t be blocked.\n{U}, Discard a card: Return Escape Artist to its owner\'s hand.').
card_mana_cost('escape artist', ['1', 'U']).
card_cmc('escape artist', 2).
card_layout('escape artist', 'normal').
card_power('escape artist', 1).
card_toughness('escape artist', 1).

% Found in: PLS
card_name('escape routes', 'Escape Routes').
card_type('escape routes', 'Enchantment').
card_types('escape routes', ['Enchantment']).
card_subtypes('escape routes', []).
card_colors('escape routes', ['U']).
card_text('escape routes', '{2}{U}: Return target white or black creature you control to its owner\'s hand.').
card_mana_cost('escape routes', ['2', 'U']).
card_cmc('escape routes', 3).
card_layout('escape routes', 'normal').

% Found in: ROE
card_name('escaped null', 'Escaped Null').
card_type('escaped null', 'Creature — Zombie Berserker').
card_types('escaped null', ['Creature']).
card_subtypes('escaped null', ['Zombie', 'Berserker']).
card_colors('escaped null', ['B']).
card_text('escaped null', 'Lifelink\nWhenever Escaped Null blocks or becomes blocked, it gets +5/+0 until end of turn.').
card_mana_cost('escaped null', ['3', 'B']).
card_cmc('escaped null', 4).
card_layout('escaped null', 'normal').
card_power('escaped null', 1).
card_toughness('escaped null', 2).

% Found in: TMP
card_name('escaped shapeshifter', 'Escaped Shapeshifter').
card_type('escaped shapeshifter', 'Creature — Shapeshifter').
card_types('escaped shapeshifter', ['Creature']).
card_subtypes('escaped shapeshifter', ['Shapeshifter']).
card_colors('escaped shapeshifter', ['U']).
card_text('escaped shapeshifter', 'As long as an opponent controls a creature with flying not named Escaped Shapeshifter, Escaped Shapeshifter has flying. The same is true for first strike, trample, and protection from any color.').
card_mana_cost('escaped shapeshifter', ['3', 'U', 'U']).
card_cmc('escaped shapeshifter', 5).
card_layout('escaped shapeshifter', 'normal').
card_power('escaped shapeshifter', 3).
card_toughness('escaped shapeshifter', 4).
card_reserved('escaped shapeshifter').

% Found in: ALA
card_name('esper battlemage', 'Esper Battlemage').
card_type('esper battlemage', 'Artifact Creature — Human Wizard').
card_types('esper battlemage', ['Artifact', 'Creature']).
card_subtypes('esper battlemage', ['Human', 'Wizard']).
card_colors('esper battlemage', ['U']).
card_text('esper battlemage', '{W}, {T}: Prevent the next 2 damage that would be dealt to you this turn.\n{B}, {T}: Target creature gets -1/-1 until end of turn.').
card_mana_cost('esper battlemage', ['2', 'U']).
card_cmc('esper battlemage', 3).
card_layout('esper battlemage', 'normal').
card_power('esper battlemage', 2).
card_toughness('esper battlemage', 2).

% Found in: ALA
card_name('esper charm', 'Esper Charm').
card_type('esper charm', 'Instant').
card_types('esper charm', ['Instant']).
card_subtypes('esper charm', []).
card_colors('esper charm', ['W', 'U', 'B']).
card_text('esper charm', 'Choose one —\n• Destroy target enchantment.\n• Draw two cards.\n• Target player discards two cards.').
card_mana_cost('esper charm', ['W', 'U', 'B']).
card_cmc('esper charm', 3).
card_layout('esper charm', 'normal').

% Found in: CON
card_name('esper cormorants', 'Esper Cormorants').
card_type('esper cormorants', 'Artifact Creature — Bird').
card_types('esper cormorants', ['Artifact', 'Creature']).
card_subtypes('esper cormorants', ['Bird']).
card_colors('esper cormorants', ['W', 'U']).
card_text('esper cormorants', 'Flying').
card_mana_cost('esper cormorants', ['2', 'W', 'U']).
card_cmc('esper cormorants', 4).
card_layout('esper cormorants', 'normal').
card_power('esper cormorants', 3).
card_toughness('esper cormorants', 3).

% Found in: ALA, C13
card_name('esper panorama', 'Esper Panorama').
card_type('esper panorama', 'Land').
card_types('esper panorama', ['Land']).
card_subtypes('esper panorama', []).
card_colors('esper panorama', []).
card_text('esper panorama', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Esper Panorama: Search your library for a basic Plains, Island, or Swamp card and put it onto the battlefield tapped. Then shuffle your library.').
card_layout('esper panorama', 'normal').

% Found in: ARB
card_name('esper sojourners', 'Esper Sojourners').
card_type('esper sojourners', 'Artifact Creature — Vedalken Wizard').
card_types('esper sojourners', ['Artifact', 'Creature']).
card_subtypes('esper sojourners', ['Vedalken', 'Wizard']).
card_colors('esper sojourners', ['W', 'U', 'B']).
card_text('esper sojourners', 'When you cycle Esper Sojourners or it dies, you may tap or untap target permanent.\nCycling {2}{U} ({2}{U}, Discard this card: Draw a card.)').
card_mana_cost('esper sojourners', ['W', 'U', 'B']).
card_cmc('esper sojourners', 3).
card_layout('esper sojourners', 'normal').
card_power('esper sojourners', 2).
card_toughness('esper sojourners', 3).

% Found in: ARB
card_name('esper stormblade', 'Esper Stormblade').
card_type('esper stormblade', 'Artifact Creature — Vedalken Wizard').
card_types('esper stormblade', ['Artifact', 'Creature']).
card_subtypes('esper stormblade', ['Vedalken', 'Wizard']).
card_colors('esper stormblade', ['W', 'U', 'B']).
card_text('esper stormblade', 'As long as you control another multicolored permanent, Esper Stormblade gets +1/+1 and has flying.').
card_mana_cost('esper stormblade', ['W/B', 'U']).
card_cmc('esper stormblade', 2).
card_layout('esper stormblade', 'normal').
card_power('esper stormblade', 2).
card_toughness('esper stormblade', 1).

% Found in: CON, DDF, MMA
card_name('esperzoa', 'Esperzoa').
card_type('esperzoa', 'Artifact Creature — Jellyfish').
card_types('esperzoa', ['Artifact', 'Creature']).
card_subtypes('esperzoa', ['Jellyfish']).
card_colors('esperzoa', ['U']).
card_text('esperzoa', 'Flying\nAt the beginning of your upkeep, return an artifact you control to its owner\'s hand.').
card_mana_cost('esperzoa', ['2', 'U']).
card_cmc('esperzoa', 3).
card_layout('esperzoa', 'normal').
card_power('esperzoa', 4).
card_toughness('esperzoa', 3).

% Found in: RTR
card_name('essence backlash', 'Essence Backlash').
card_type('essence backlash', 'Instant').
card_types('essence backlash', ['Instant']).
card_subtypes('essence backlash', []).
card_colors('essence backlash', ['U', 'R']).
card_text('essence backlash', 'Counter target creature spell. Essence Backlash deals damage equal to that spell\'s power to its controller.').
card_mana_cost('essence backlash', ['2', 'U', 'R']).
card_cmc('essence backlash', 4).
card_layout('essence backlash', 'normal').

% Found in: TMP
card_name('essence bottle', 'Essence Bottle').
card_type('essence bottle', 'Artifact').
card_types('essence bottle', ['Artifact']).
card_subtypes('essence bottle', []).
card_colors('essence bottle', []).
card_text('essence bottle', '{3}, {T}: Put an elixir counter on Essence Bottle.\n{T}, Remove all elixir counters from Essence Bottle: You gain 2 life for each elixir counter removed this way.').
card_mana_cost('essence bottle', ['2']).
card_cmc('essence bottle', 2).
card_layout('essence bottle', 'normal').

% Found in: 10E, DPA, DST, M13
card_name('essence drain', 'Essence Drain').
card_type('essence drain', 'Sorcery').
card_types('essence drain', ['Sorcery']).
card_subtypes('essence drain', []).
card_colors('essence drain', ['B']).
card_text('essence drain', 'Essence Drain deals 3 damage to target creature or player and you gain 3 life.').
card_mana_cost('essence drain', ['4', 'B']).
card_cmc('essence drain', 5).
card_layout('essence drain', 'normal').

% Found in: ROE
card_name('essence feed', 'Essence Feed').
card_type('essence feed', 'Sorcery').
card_types('essence feed', ['Sorcery']).
card_subtypes('essence feed', []).
card_colors('essence feed', ['B']).
card_text('essence feed', 'Target player loses 3 life. You gain 3 life and put three 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('essence feed', ['5', 'B']).
card_cmc('essence feed', 6).
card_layout('essence feed', 'normal').

% Found in: ICE, ME2
card_name('essence filter', 'Essence Filter').
card_type('essence filter', 'Sorcery').
card_types('essence filter', ['Sorcery']).
card_subtypes('essence filter', []).
card_colors('essence filter', ['G']).
card_text('essence filter', 'Destroy all enchantments or all nonwhite enchantments.').
card_mana_cost('essence filter', ['1', 'G', 'G']).
card_cmc('essence filter', 3).
card_layout('essence filter', 'normal').

% Found in: CST, ICE, ME2
card_name('essence flare', 'Essence Flare').
card_type('essence flare', 'Enchantment — Aura').
card_types('essence flare', ['Enchantment']).
card_subtypes('essence flare', ['Aura']).
card_colors('essence flare', ['U']).
card_text('essence flare', 'Enchant creature\nEnchanted creature gets +2/+0.\nAt the beginning of the upkeep of enchanted creature\'s controller, put a -0/-1 counter on that creature.').
card_mana_cost('essence flare', ['U']).
card_cmc('essence flare', 1).
card_layout('essence flare', 'normal').

% Found in: ONS
card_name('essence fracture', 'Essence Fracture').
card_type('essence fracture', 'Sorcery').
card_types('essence fracture', ['Sorcery']).
card_subtypes('essence fracture', []).
card_colors('essence fracture', ['U']).
card_text('essence fracture', 'Return two target creatures to their owners\' hands.\nCycling {2}{U} ({2}{U}, Discard this card: Draw a card.)').
card_mana_cost('essence fracture', ['3', 'U', 'U']).
card_cmc('essence fracture', 5).
card_layout('essence fracture', 'normal').

% Found in: AVR
card_name('essence harvest', 'Essence Harvest').
card_type('essence harvest', 'Sorcery').
card_types('essence harvest', ['Sorcery']).
card_subtypes('essence harvest', []).
card_colors('essence harvest', ['B']).
card_text('essence harvest', 'Target player loses X life and you gain X life, where X is the greatest power among creatures you control.').
card_mana_cost('essence harvest', ['2', 'B']).
card_cmc('essence harvest', 3).
card_layout('essence harvest', 'normal').

% Found in: INV
card_name('essence leak', 'Essence Leak').
card_type('essence leak', 'Enchantment — Aura').
card_types('essence leak', ['Enchantment']).
card_subtypes('essence leak', ['Aura']).
card_colors('essence leak', ['U']).
card_text('essence leak', 'Enchant permanent\nAs long as enchanted permanent is red or green, it has \"At the beginning of your upkeep, sacrifice this permanent unless you pay its mana cost.\"').
card_mana_cost('essence leak', ['U']).
card_cmc('essence leak', 1).
card_layout('essence leak', 'normal').

% Found in: ISD
card_name('essence of the wild', 'Essence of the Wild').
card_type('essence of the wild', 'Creature — Avatar').
card_types('essence of the wild', ['Creature']).
card_subtypes('essence of the wild', ['Avatar']).
card_colors('essence of the wild', ['G']).
card_text('essence of the wild', 'Creatures you control enter the battlefield as a copy of Essence of the Wild.').
card_mana_cost('essence of the wild', ['3', 'G', 'G', 'G']).
card_cmc('essence of the wild', 6).
card_layout('essence of the wild', 'normal').
card_power('essence of the wild', 6).
card_toughness('essence of the wild', 6).

% Found in: DPA, M10, M13, M14
card_name('essence scatter', 'Essence Scatter').
card_type('essence scatter', 'Instant').
card_types('essence scatter', ['Instant']).
card_subtypes('essence scatter', []).
card_colors('essence scatter', ['U']).
card_text('essence scatter', 'Counter target creature spell.').
card_mana_cost('essence scatter', ['1', 'U']).
card_cmc('essence scatter', 2).
card_layout('essence scatter', 'normal').

% Found in: LGN, TSB
card_name('essence sliver', 'Essence Sliver').
card_type('essence sliver', 'Creature — Sliver').
card_types('essence sliver', ['Creature']).
card_subtypes('essence sliver', ['Sliver']).
card_colors('essence sliver', ['W']).
card_text('essence sliver', 'Whenever a Sliver deals damage, its controller gains that much life.').
card_mana_cost('essence sliver', ['3', 'W']).
card_cmc('essence sliver', 4).
card_layout('essence sliver', 'normal').
card_power('essence sliver', 3).
card_toughness('essence sliver', 3).

% Found in: ICE
card_name('essence vortex', 'Essence Vortex').
card_type('essence vortex', 'Instant').
card_types('essence vortex', ['Instant']).
card_subtypes('essence vortex', []).
card_colors('essence vortex', ['U', 'B']).
card_text('essence vortex', 'Destroy target creature unless its controller pays life equal to its toughness. A creature destroyed this way can\'t be regenerated.').
card_mana_cost('essence vortex', ['1', 'U', 'B']).
card_cmc('essence vortex', 3).
card_layout('essence vortex', 'normal').

% Found in: C14, DDH, PLC
card_name('essence warden', 'Essence Warden').
card_type('essence warden', 'Creature — Elf Shaman').
card_types('essence warden', ['Creature']).
card_subtypes('essence warden', ['Elf', 'Shaman']).
card_colors('essence warden', ['G']).
card_text('essence warden', 'Whenever another creature enters the battlefield, you gain 1 life.').
card_mana_cost('essence warden', ['G']).
card_cmc('essence warden', 1).
card_layout('essence warden', 'normal').
card_power('essence warden', 1).
card_toughness('essence warden', 1).

% Found in: MM2, SOM
card_name('etched champion', 'Etched Champion').
card_type('etched champion', 'Artifact Creature — Soldier').
card_types('etched champion', ['Artifact', 'Creature']).
card_subtypes('etched champion', ['Soldier']).
card_colors('etched champion', []).
card_text('etched champion', 'Metalcraft — Etched Champion has protection from all colors as long as you control three or more artifacts.').
card_mana_cost('etched champion', ['3']).
card_cmc('etched champion', 3).
card_layout('etched champion', 'normal').
card_power('etched champion', 2).
card_toughness('etched champion', 2).

% Found in: MM2, NPH
card_name('etched monstrosity', 'Etched Monstrosity').
card_type('etched monstrosity', 'Artifact Creature — Golem').
card_types('etched monstrosity', ['Artifact', 'Creature']).
card_subtypes('etched monstrosity', ['Golem']).
card_colors('etched monstrosity', []).
card_text('etched monstrosity', 'Etched Monstrosity enters the battlefield with five -1/-1 counters on it.\n{W}{U}{B}{R}{G}, Remove five -1/-1 counters from Etched Monstrosity: Target player draws three cards.').
card_mana_cost('etched monstrosity', ['5']).
card_cmc('etched monstrosity', 5).
card_layout('etched monstrosity', 'normal').
card_power('etched monstrosity', 10).
card_toughness('etched monstrosity', 10).

% Found in: 5DN, HOP, MM2, MMA
card_name('etched oracle', 'Etched Oracle').
card_type('etched oracle', 'Artifact Creature — Wizard').
card_types('etched oracle', ['Artifact', 'Creature']).
card_subtypes('etched oracle', ['Wizard']).
card_colors('etched oracle', []).
card_text('etched oracle', 'Sunburst (This enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.)\n{1}, Remove four +1/+1 counters from Etched Oracle: Target player draws three cards.').
card_mana_cost('etched oracle', ['4']).
card_cmc('etched oracle', 4).
card_layout('etched oracle', 'normal').
card_power('etched oracle', 0).
card_toughness('etched oracle', 0).

% Found in: VAN
card_name('etched oracle avatar', 'Etched Oracle Avatar').
card_type('etched oracle avatar', 'Vanguard').
card_types('etched oracle avatar', ['Vanguard']).
card_subtypes('etched oracle avatar', []).
card_colors('etched oracle avatar', []).
card_text('etched oracle avatar', 'You may pay {W}{U}{B}{R}{G} rather than pay the mana cost for spells that you cast.').
card_layout('etched oracle avatar', 'vanguard').

% Found in: SOK
card_name('eternal dominion', 'Eternal Dominion').
card_type('eternal dominion', 'Sorcery').
card_types('eternal dominion', ['Sorcery']).
card_subtypes('eternal dominion', []).
card_colors('eternal dominion', ['U']).
card_text('eternal dominion', 'Search target opponent\'s library for an artifact, creature, enchantment, or land card. Put that card onto the battlefield under your control. Then that player shuffles his or her library.\nEpic (For the rest of the game, you can\'t cast spells. At the beginning of each of your upkeeps, copy this spell except for its epic ability. You may choose a new target for the copy.)').
card_mana_cost('eternal dominion', ['7', 'U', 'U', 'U']).
card_cmc('eternal dominion', 10).
card_layout('eternal dominion', 'normal').

% Found in: C13, SCG, VMA, pPRO
card_name('eternal dragon', 'Eternal Dragon').
card_type('eternal dragon', 'Creature — Dragon Spirit').
card_types('eternal dragon', ['Creature']).
card_subtypes('eternal dragon', ['Dragon', 'Spirit']).
card_colors('eternal dragon', ['W']).
card_text('eternal dragon', 'Flying\n{3}{W}{W}: Return Eternal Dragon from your graveyard to your hand. Activate this ability only during your upkeep.\nPlainscycling {2} ({2}, Discard this card: Search your library for a Plains card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('eternal dragon', ['5', 'W', 'W']).
card_cmc('eternal dragon', 7).
card_layout('eternal dragon', 'normal').
card_power('eternal dragon', 5).
card_toughness('eternal dragon', 5).

% Found in: DRK
card_name('eternal flame', 'Eternal Flame').
card_type('eternal flame', 'Sorcery').
card_types('eternal flame', ['Sorcery']).
card_subtypes('eternal flame', []).
card_colors('eternal flame', ['R']).
card_text('eternal flame', 'Eternal Flame deals X damage to target opponent, where X is the number of Mountains you control. It deals half X damage, rounded up, to you.').
card_mana_cost('eternal flame', ['2', 'R', 'R']).
card_cmc('eternal flame', 4).
card_layout('eternal flame', 'normal').
card_reserved('eternal flame').

% Found in: M15
card_name('eternal thirst', 'Eternal Thirst').
card_type('eternal thirst', 'Enchantment — Aura').
card_types('eternal thirst', ['Enchantment']).
card_subtypes('eternal thirst', ['Aura']).
card_colors('eternal thirst', ['B']).
card_text('eternal thirst', 'Enchant creature\nEnchanted creature has lifelink and \"Whenever a creature an opponent controls dies, put a +1/+1 counter on this creature.\" (Damage dealt by a creature with lifelink also causes its controller to gain that much life.)').
card_mana_cost('eternal thirst', ['1', 'B']).
card_cmc('eternal thirst', 2).
card_layout('eternal thirst', 'normal').

% Found in: 4ED, 5ED, LEG
card_name('eternal warrior', 'Eternal Warrior').
card_type('eternal warrior', 'Enchantment — Aura').
card_types('eternal warrior', ['Enchantment']).
card_subtypes('eternal warrior', ['Aura']).
card_colors('eternal warrior', ['R']).
card_text('eternal warrior', 'Enchant creature\nEnchanted creature has vigilance.').
card_mana_cost('eternal warrior', ['R']).
card_cmc('eternal warrior', 1).
card_layout('eternal warrior', 'normal').

% Found in: 5DN, CMD, DDJ, MMA, pFNM
card_name('eternal witness', 'Eternal Witness').
card_type('eternal witness', 'Creature — Human Shaman').
card_types('eternal witness', ['Creature']).
card_subtypes('eternal witness', ['Human', 'Shaman']).
card_colors('eternal witness', ['G']).
card_text('eternal witness', 'When Eternal Witness enters the battlefield, you may return target card from your graveyard to your hand.').
card_mana_cost('eternal witness', ['1', 'G', 'G']).
card_cmc('eternal witness', 3).
card_layout('eternal witness', 'normal').
card_power('eternal witness', 2).
card_toughness('eternal witness', 1).

% Found in: BNG, TSP
card_name('eternity snare', 'Eternity Snare').
card_type('eternity snare', 'Enchantment — Aura').
card_types('eternity snare', ['Enchantment']).
card_subtypes('eternity snare', ['Aura']).
card_colors('eternity snare', ['U']).
card_text('eternity snare', 'Enchant creature\nWhen Eternity Snare enters the battlefield, draw a card.\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_mana_cost('eternity snare', ['5', 'U']).
card_cmc('eternity snare', 6).
card_layout('eternity snare', 'normal').

% Found in: ZEN
card_name('eternity vessel', 'Eternity Vessel').
card_type('eternity vessel', 'Artifact').
card_types('eternity vessel', ['Artifact']).
card_subtypes('eternity vessel', []).
card_colors('eternity vessel', []).
card_text('eternity vessel', 'Eternity Vessel enters the battlefield with X charge counters on it, where X is your life total.\nLandfall — Whenever a land enters the battlefield under your control, you may have your life total become the number of charge counters on Eternity Vessel.').
card_mana_cost('eternity vessel', ['6']).
card_cmc('eternity vessel', 6).
card_layout('eternity vessel', 'normal').

% Found in: MIR
card_name('ether well', 'Ether Well').
card_type('ether well', 'Instant').
card_types('ether well', ['Instant']).
card_subtypes('ether well', []).
card_colors('ether well', ['U']).
card_text('ether well', 'Put target creature on top of its owner\'s library. If that creature is red, you may put it on the bottom of its owner\'s library instead.').
card_mana_cost('ether well', ['3', 'U']).
card_cmc('ether well', 4).
card_layout('ether well', 'normal').

% Found in: ARB, MM2
card_name('ethercaste knight', 'Ethercaste Knight').
card_type('ethercaste knight', 'Artifact Creature — Human Knight').
card_types('ethercaste knight', ['Artifact', 'Creature']).
card_subtypes('ethercaste knight', ['Human', 'Knight']).
card_colors('ethercaste knight', ['W', 'U']).
card_text('ethercaste knight', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_mana_cost('ethercaste knight', ['W', 'U']).
card_cmc('ethercaste knight', 2).
card_layout('ethercaste knight', 'normal').
card_power('ethercaste knight', 1).
card_toughness('ethercaste knight', 3).

% Found in: FRF
card_name('ethereal ambush', 'Ethereal Ambush').
card_type('ethereal ambush', 'Instant').
card_types('ethereal ambush', ['Instant']).
card_subtypes('ethereal ambush', []).
card_colors('ethereal ambush', ['U', 'G']).
card_text('ethereal ambush', 'Manifest the top two cards of your library. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_mana_cost('ethereal ambush', ['3', 'G', 'U']).
card_cmc('ethereal ambush', 5).
card_layout('ethereal ambush', 'normal').

% Found in: RTR
card_name('ethereal armor', 'Ethereal Armor').
card_type('ethereal armor', 'Enchantment — Aura').
card_types('ethereal armor', ['Enchantment']).
card_subtypes('ethereal armor', ['Aura']).
card_colors('ethereal armor', ['W']).
card_text('ethereal armor', 'Enchant creature\nEnchanted creature gets +1/+1 for each enchantment you control and has first strike.').
card_mana_cost('ethereal armor', ['W']).
card_cmc('ethereal armor', 1).
card_layout('ethereal armor', 'normal').

% Found in: 6ED, MIR
card_name('ethereal champion', 'Ethereal Champion').
card_type('ethereal champion', 'Creature — Avatar').
card_types('ethereal champion', ['Creature']).
card_subtypes('ethereal champion', ['Avatar']).
card_colors('ethereal champion', ['W']).
card_text('ethereal champion', 'Pay 1 life: Prevent the next 1 damage that would be dealt to Ethereal Champion this turn.').
card_mana_cost('ethereal champion', ['2', 'W', 'W', 'W']).
card_cmc('ethereal champion', 5).
card_layout('ethereal champion', 'normal').
card_power('ethereal champion', 3).
card_toughness('ethereal champion', 4).

% Found in: CHK
card_name('ethereal haze', 'Ethereal Haze').
card_type('ethereal haze', 'Instant — Arcane').
card_types('ethereal haze', ['Instant']).
card_subtypes('ethereal haze', ['Arcane']).
card_colors('ethereal haze', ['W']).
card_text('ethereal haze', 'Prevent all damage that would be dealt by creatures this turn.').
card_mana_cost('ethereal haze', ['W']).
card_cmc('ethereal haze', 1).
card_layout('ethereal haze', 'normal').

% Found in: RAV
card_name('ethereal usher', 'Ethereal Usher').
card_type('ethereal usher', 'Creature — Spirit').
card_types('ethereal usher', ['Creature']).
card_subtypes('ethereal usher', ['Spirit']).
card_colors('ethereal usher', ['U']).
card_text('ethereal usher', '{U}, {T}: Target creature can\'t be blocked this turn.\nTransmute {1}{U}{U} ({1}{U}{U}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Transmute only as a sorcery.)').
card_mana_cost('ethereal usher', ['5', 'U']).
card_cmc('ethereal usher', 6).
card_layout('ethereal usher', 'normal').
card_power('ethereal usher', 2).
card_toughness('ethereal usher', 3).

% Found in: LRW
card_name('ethereal whiskergill', 'Ethereal Whiskergill').
card_type('ethereal whiskergill', 'Creature — Elemental').
card_types('ethereal whiskergill', ['Creature']).
card_subtypes('ethereal whiskergill', ['Elemental']).
card_colors('ethereal whiskergill', ['U']).
card_text('ethereal whiskergill', 'Flying\nEthereal Whiskergill can\'t attack unless defending player controls an Island.').
card_mana_cost('ethereal whiskergill', ['3', 'U']).
card_cmc('ethereal whiskergill', 4).
card_layout('ethereal whiskergill', 'normal').
card_power('ethereal whiskergill', 4).
card_toughness('ethereal whiskergill', 3).

% Found in: ARB
card_name('etherium abomination', 'Etherium Abomination').
card_type('etherium abomination', 'Artifact Creature — Horror').
card_types('etherium abomination', ['Artifact', 'Creature']).
card_subtypes('etherium abomination', ['Horror']).
card_colors('etherium abomination', ['U', 'B']).
card_text('etherium abomination', 'Unearth {1}{U}{B} ({1}{U}{B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('etherium abomination', ['3', 'U', 'B']).
card_cmc('etherium abomination', 5).
card_layout('etherium abomination', 'normal').
card_power('etherium abomination', 4).
card_toughness('etherium abomination', 3).

% Found in: ALA
card_name('etherium astrolabe', 'Etherium Astrolabe').
card_type('etherium astrolabe', 'Artifact').
card_types('etherium astrolabe', ['Artifact']).
card_subtypes('etherium astrolabe', []).
card_colors('etherium astrolabe', ['U']).
card_text('etherium astrolabe', 'Flash\n{B}, {T}, Sacrifice an artifact: Draw a card.').
card_mana_cost('etherium astrolabe', ['2', 'U']).
card_cmc('etherium astrolabe', 3).
card_layout('etherium astrolabe', 'normal').

% Found in: ALA, MMA
card_name('etherium sculptor', 'Etherium Sculptor').
card_type('etherium sculptor', 'Artifact Creature — Vedalken Artificer').
card_types('etherium sculptor', ['Artifact', 'Creature']).
card_subtypes('etherium sculptor', ['Vedalken', 'Artificer']).
card_colors('etherium sculptor', ['U']).
card_text('etherium sculptor', 'Artifact spells you cast cost {1} less to cast.').
card_mana_cost('etherium sculptor', ['1', 'U']).
card_cmc('etherium sculptor', 2).
card_layout('etherium sculptor', 'normal').
card_power('etherium sculptor', 1).
card_toughness('etherium sculptor', 2).

% Found in: PC2
card_name('etherium-horn sorcerer', 'Etherium-Horn Sorcerer').
card_type('etherium-horn sorcerer', 'Artifact Creature — Minotaur Wizard').
card_types('etherium-horn sorcerer', ['Artifact', 'Creature']).
card_subtypes('etherium-horn sorcerer', ['Minotaur', 'Wizard']).
card_colors('etherium-horn sorcerer', ['U', 'R']).
card_text('etherium-horn sorcerer', '{1}{U}{R}: Return Etherium-Horn Sorcerer to its owner\'s hand.\nCascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)').
card_mana_cost('etherium-horn sorcerer', ['4', 'U', 'R']).
card_cmc('etherium-horn sorcerer', 6).
card_layout('etherium-horn sorcerer', 'normal').
card_power('etherium-horn sorcerer', 3).
card_toughness('etherium-horn sorcerer', 6).

% Found in: CON
card_name('ethersworn adjudicator', 'Ethersworn Adjudicator').
card_type('ethersworn adjudicator', 'Artifact Creature — Vedalken Knight').
card_types('ethersworn adjudicator', ['Artifact', 'Creature']).
card_subtypes('ethersworn adjudicator', ['Vedalken', 'Knight']).
card_colors('ethersworn adjudicator', ['U']).
card_text('ethersworn adjudicator', 'Flying\n{1}{W}{B}, {T}: Destroy target creature or enchantment.\n{2}{U}: Untap Ethersworn Adjudicator.').
card_mana_cost('ethersworn adjudicator', ['4', 'U']).
card_cmc('ethersworn adjudicator', 5).
card_layout('ethersworn adjudicator', 'normal').
card_power('ethersworn adjudicator', 4).
card_toughness('ethersworn adjudicator', 4).

% Found in: ALA, MMA
card_name('ethersworn canonist', 'Ethersworn Canonist').
card_type('ethersworn canonist', 'Artifact Creature — Human Cleric').
card_types('ethersworn canonist', ['Artifact', 'Creature']).
card_subtypes('ethersworn canonist', ['Human', 'Cleric']).
card_colors('ethersworn canonist', ['W']).
card_text('ethersworn canonist', 'Each player who has cast a nonartifact spell this turn can\'t cast additional nonartifact spells.').
card_mana_cost('ethersworn canonist', ['1', 'W']).
card_cmc('ethersworn canonist', 2).
card_layout('ethersworn canonist', 'normal').
card_power('ethersworn canonist', 2).
card_toughness('ethersworn canonist', 2).

% Found in: ARB, ARC
card_name('ethersworn shieldmage', 'Ethersworn Shieldmage').
card_type('ethersworn shieldmage', 'Artifact Creature — Vedalken Wizard').
card_types('ethersworn shieldmage', ['Artifact', 'Creature']).
card_subtypes('ethersworn shieldmage', ['Vedalken', 'Wizard']).
card_colors('ethersworn shieldmage', ['W', 'U']).
card_text('ethersworn shieldmage', 'Flash\nWhen Ethersworn Shieldmage enters the battlefield, prevent all damage that would be dealt to artifact creatures this turn.').
card_mana_cost('ethersworn shieldmage', ['1', 'W', 'U']).
card_cmc('ethersworn shieldmage', 3).
card_layout('ethersworn shieldmage', 'normal').
card_power('ethersworn shieldmage', 2).
card_toughness('ethersworn shieldmage', 2).

% Found in: ARB
card_name('etherwrought page', 'Etherwrought Page').
card_type('etherwrought page', 'Artifact').
card_types('etherwrought page', ['Artifact']).
card_subtypes('etherwrought page', []).
card_colors('etherwrought page', ['W', 'U', 'B']).
card_text('etherwrought page', 'At the beginning of your upkeep, choose one —\n• You gain 2 life.\n• Look at the top card of your library. You may put that card into your graveyard.\n• Each opponent loses 1 life.').
card_mana_cost('etherwrought page', ['1', 'W', 'U', 'B']).
card_cmc('etherwrought page', 4).
card_layout('etherwrought page', 'normal').

% Found in: PTK
card_name('eunuchs\' intrigues', 'Eunuchs\' Intrigues').
card_type('eunuchs\' intrigues', 'Sorcery').
card_types('eunuchs\' intrigues', ['Sorcery']).
card_subtypes('eunuchs\' intrigues', []).
card_colors('eunuchs\' intrigues', ['R']).
card_text('eunuchs\' intrigues', 'Target opponent chooses a creature he or she controls. Other creatures he or she controls can\'t block this turn.').
card_mana_cost('eunuchs\' intrigues', ['2', 'R']).
card_cmc('eunuchs\' intrigues', 3).
card_layout('eunuchs\' intrigues', 'normal').

% Found in: LEG, MED, VMA
card_name('eureka', 'Eureka').
card_type('eureka', 'Sorcery').
card_types('eureka', ['Sorcery']).
card_subtypes('eureka', []).
card_colors('eureka', ['G']).
card_text('eureka', 'Starting with you, each player may put a permanent card from his or her hand onto the battlefield. Repeat this process until no one puts a card onto the battlefield.').
card_mana_cost('eureka', ['2', 'G', 'G']).
card_cmc('eureka', 4).
card_layout('eureka', 'normal').
card_reserved('eureka').

% Found in: 10E, 7ED, 8ED, 9ED, DPA, STH
card_name('evacuation', 'Evacuation').
card_type('evacuation', 'Instant').
card_types('evacuation', ['Instant']).
card_subtypes('evacuation', []).
card_colors('evacuation', ['U']).
card_text('evacuation', 'Return all creatures to their owners\' hands.').
card_mana_cost('evacuation', ['3', 'U', 'U']).
card_cmc('evacuation', 5).
card_layout('evacuation', 'normal').

% Found in: BNG
card_name('evanescent intellect', 'Evanescent Intellect').
card_type('evanescent intellect', 'Enchantment — Aura').
card_types('evanescent intellect', ['Enchantment']).
card_subtypes('evanescent intellect', ['Aura']).
card_colors('evanescent intellect', ['U']).
card_text('evanescent intellect', 'Enchant creature\nEnchanted creature has \"{1}{U}, {T}: Target player puts the top three cards of his or her library into his or her graveyard.\"').
card_mana_cost('evanescent intellect', ['U']).
card_cmc('evanescent intellect', 1).
card_layout('evanescent intellect', 'normal').

% Found in: THS
card_name('evangel of heliod', 'Evangel of Heliod').
card_type('evangel of heliod', 'Creature — Human Cleric').
card_types('evangel of heliod', ['Creature']).
card_subtypes('evangel of heliod', ['Human', 'Cleric']).
card_colors('evangel of heliod', ['W']).
card_text('evangel of heliod', 'When Evangel of Heliod enters the battlefield, put a number of 1/1 white Soldier creature tokens onto the battlefield equal to your devotion to white. (Each {W} in the mana costs of permanents you control counts toward your devotion to white.)').
card_mana_cost('evangel of heliod', ['4', 'W', 'W']).
card_cmc('evangel of heliod', 6).
card_layout('evangel of heliod', 'normal').
card_power('evangel of heliod', 1).
card_toughness('evangel of heliod', 3).

% Found in: TSP
card_name('evangelize', 'Evangelize').
card_type('evangelize', 'Sorcery').
card_types('evangelize', ['Sorcery']).
card_subtypes('evangelize', []).
card_colors('evangelize', ['W']).
card_text('evangelize', 'Buyback {2}{W}{W} (You may pay an additional {2}{W}{W} as you cast this spell. If you do, put this card into your hand as it resolves.)\nGain control of target creature of an opponent\'s choice he or she controls.').
card_mana_cost('evangelize', ['4', 'W']).
card_cmc('evangelize', 5).
card_layout('evangelize', 'normal').

% Found in: HML
card_name('evaporate', 'Evaporate').
card_type('evaporate', 'Sorcery').
card_types('evaporate', ['Sorcery']).
card_subtypes('evaporate', []).
card_colors('evaporate', ['R']).
card_text('evaporate', 'Evaporate deals 1 damage to each white and/or blue creature.').
card_mana_cost('evaporate', ['2', 'R']).
card_cmc('evaporate', 3).
card_layout('evaporate', 'normal').

% Found in: APC, DDE
card_name('evasive action', 'Evasive Action').
card_type('evasive action', 'Instant').
card_types('evasive action', ['Instant']).
card_subtypes('evasive action', []).
card_colors('evasive action', ['U']).
card_text('evasive action', 'Domain — Counter target spell unless its controller pays {1} for each basic land type among lands you control.').
card_mana_cost('evasive action', ['1', 'U']).
card_cmc('evasive action', 2).
card_layout('evasive action', 'normal').

% Found in: FUT
card_name('even the odds', 'Even the Odds').
card_type('even the odds', 'Instant').
card_types('even the odds', ['Instant']).
card_subtypes('even the odds', []).
card_colors('even the odds', ['W']).
card_text('even the odds', 'Cast Even the Odds only if you control fewer creatures than each opponent.\nPut three 1/1 white Soldier creature tokens onto the battlefield.').
card_mana_cost('even the odds', ['2', 'W']).
card_cmc('even the odds', 3).
card_layout('even the odds', 'normal').

% Found in: MOR
card_name('everbark shaman', 'Everbark Shaman').
card_type('everbark shaman', 'Creature — Treefolk Shaman').
card_types('everbark shaman', ['Creature']).
card_subtypes('everbark shaman', ['Treefolk', 'Shaman']).
card_colors('everbark shaman', ['G']).
card_text('everbark shaman', '{T}, Exile a Treefolk card from your graveyard: Search your library for up to two Forest cards and put them onto the battlefield tapped. Then shuffle your library.').
card_mana_cost('everbark shaman', ['4', 'G']).
card_cmc('everbark shaman', 5).
card_layout('everbark shaman', 'normal').
card_power('everbark shaman', 3).
card_toughness('everbark shaman', 5).

% Found in: BNG
card_name('everflame eidolon', 'Everflame Eidolon').
card_type('everflame eidolon', 'Enchantment Creature — Spirit').
card_types('everflame eidolon', ['Enchantment', 'Creature']).
card_subtypes('everflame eidolon', ['Spirit']).
card_colors('everflame eidolon', ['R']).
card_text('everflame eidolon', 'Bestow {2}{R} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\n{R}: Everflame Eidolon gets +1/+0 until end of turn. If it\'s an Aura, enchanted creature gets +1/+0 until end of turn instead.\nEnchanted creature gets +1/+1.').
card_mana_cost('everflame eidolon', ['1', 'R']).
card_cmc('everflame eidolon', 2).
card_layout('everflame eidolon', 'normal').
card_power('everflame eidolon', 1).
card_toughness('everflame eidolon', 1).

% Found in: ARC, C14, DDF, MM2, WWK, pFNM
card_name('everflowing chalice', 'Everflowing Chalice').
card_type('everflowing chalice', 'Artifact').
card_types('everflowing chalice', ['Artifact']).
card_subtypes('everflowing chalice', []).
card_colors('everflowing chalice', []).
card_text('everflowing chalice', 'Multikicker {2} (You may pay an additional {2} any number of times as you cast this spell.)\nEverflowing Chalice enters the battlefield with a charge counter on it for each time it was kicked.\n{T}: Add {1} to your mana pool for each charge counter on Everflowing Chalice.').
card_mana_cost('everflowing chalice', ['0']).
card_cmc('everflowing chalice', 0).
card_layout('everflowing chalice', 'normal').

% Found in: C14, VIS
card_name('everglades', 'Everglades').
card_type('everglades', 'Land').
card_types('everglades', ['Land']).
card_subtypes('everglades', []).
card_colors('everglades', []).
card_text('everglades', 'Everglades enters the battlefield tapped.\nWhen Everglades enters the battlefield, sacrifice it unless you return an untapped Swamp you control to its owner\'s hand.\n{T}: Add {1}{B} to your mana pool.').
card_layout('everglades', 'normal').

% Found in: ONS
card_name('everglove courier', 'Everglove Courier').
card_type('everglove courier', 'Creature — Elf').
card_types('everglove courier', ['Creature']).
card_subtypes('everglove courier', ['Elf']).
card_colors('everglove courier', ['G']).
card_text('everglove courier', 'You may choose not to untap Everglove Courier during your untap step.\n{2}{G}, {T}: Target Elf creature gets +2/+2 and has trample for as long as Everglove Courier remains tapped.').
card_mana_cost('everglove courier', ['2', 'G']).
card_cmc('everglove courier', 3).
card_layout('everglove courier', 'normal').
card_power('everglove courier', 2).
card_toughness('everglove courier', 1).

% Found in: SHM
card_name('everlasting torment', 'Everlasting Torment').
card_type('everlasting torment', 'Enchantment').
card_types('everlasting torment', ['Enchantment']).
card_subtypes('everlasting torment', []).
card_colors('everlasting torment', ['B', 'R']).
card_text('everlasting torment', 'Players can\'t gain life.\nDamage can\'t be prevented.\nAll damage is dealt as though its source had wither. (A source with wither deals damage to creatures in the form of -1/-1 counters.)').
card_mana_cost('everlasting torment', ['2', 'B/R']).
card_cmc('everlasting torment', 3).
card_layout('everlasting torment', 'normal').

% Found in: SOK
card_name('evermind', 'Evermind').
card_type('evermind', 'Instant — Arcane').
card_types('evermind', ['Instant']).
card_subtypes('evermind', ['Arcane']).
card_colors('evermind', ['U']).
card_text('evermind', '(Nonexistent mana costs can\'t be paid.)\nDraw a card.\nSplice onto Arcane {1}{U} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_layout('evermind', 'normal').

% Found in: AVR, C14
card_name('evernight shade', 'Evernight Shade').
card_type('evernight shade', 'Creature — Shade').
card_types('evernight shade', ['Creature']).
card_subtypes('evernight shade', ['Shade']).
card_colors('evernight shade', ['B']).
card_text('evernight shade', '{B}: Evernight Shade gets +1/+1 until end of turn.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('evernight shade', ['3', 'B']).
card_cmc('evernight shade', 4).
card_layout('evernight shade', 'normal').
card_power('evernight shade', 1).
card_toughness('evernight shade', 1).

% Found in: EVE
card_name('evershrike', 'Evershrike').
card_type('evershrike', 'Creature — Elemental Spirit').
card_types('evershrike', ['Creature']).
card_subtypes('evershrike', ['Elemental', 'Spirit']).
card_colors('evershrike', ['W', 'B']).
card_text('evershrike', 'Flying\nEvershrike gets +2/+2 for each Aura attached to it.\n{X}{W/B}{W/B}: Return Evershrike from your graveyard to the battlefield. You may put an Aura card with converted mana cost X or less from your hand onto the battlefield attached to it. If you don\'t, exile Evershrike.').
card_mana_cost('evershrike', ['3', 'W/B', 'W/B']).
card_cmc('evershrike', 5).
card_layout('evershrike', 'normal').
card_power('evershrike', 2).
card_toughness('evershrike', 2).

% Found in: ARC
card_name('every hope shall vanish', 'Every Hope Shall Vanish').
card_type('every hope shall vanish', 'Scheme').
card_types('every hope shall vanish', ['Scheme']).
card_subtypes('every hope shall vanish', []).
card_colors('every hope shall vanish', []).
card_text('every hope shall vanish', 'When you set this scheme in motion, each opponent reveals his or her hand. Choose a nonland card from each of those hands. Those players discard those cards.').
card_layout('every hope shall vanish', 'scheme').

% Found in: ARC
card_name('every last vestige shall rot', 'Every Last Vestige Shall Rot').
card_type('every last vestige shall rot', 'Scheme').
card_types('every last vestige shall rot', ['Scheme']).
card_subtypes('every last vestige shall rot', []).
card_colors('every last vestige shall rot', []).
card_text('every last vestige shall rot', 'When you set this scheme in motion, you may pay {X}. If you do, put each nonland permanent target player controls with converted mana cost X or less on the bottom of its owner\'s library.').
card_layout('every last vestige shall rot', 'scheme').

% Found in: ARC
card_name('evil comes to fruition', 'Evil Comes to Fruition').
card_type('evil comes to fruition', 'Scheme').
card_types('evil comes to fruition', ['Scheme']).
card_subtypes('evil comes to fruition', []).
card_colors('evil comes to fruition', []).
card_text('evil comes to fruition', 'When you set this scheme in motion, put seven 0/1 green Plant creature tokens onto the battlefield. If you control ten or more lands, put seven 3/3 green Elemental creature tokens onto the battlefield instead.').
card_layout('evil comes to fruition', 'scheme').

% Found in: 5ED, 6ED, LEG, TSB
card_name('evil eye of orms-by-gore', 'Evil Eye of Orms-by-Gore').
card_type('evil eye of orms-by-gore', 'Creature — Eye').
card_types('evil eye of orms-by-gore', ['Creature']).
card_subtypes('evil eye of orms-by-gore', ['Eye']).
card_colors('evil eye of orms-by-gore', ['B']).
card_text('evil eye of orms-by-gore', 'Non-Eye creatures you control can\'t attack.\nEvil Eye of Orms-by-Gore can\'t be blocked except by Walls.').
card_mana_cost('evil eye of orms-by-gore', ['4', 'B']).
card_cmc('evil eye of orms-by-gore', 5).
card_layout('evil eye of orms-by-gore', 'normal').
card_power('evil eye of orms-by-gore', 3).
card_toughness('evil eye of orms-by-gore', 6).

% Found in: TSP
card_name('evil eye of urborg', 'Evil Eye of Urborg').
card_type('evil eye of urborg', 'Creature — Eye').
card_types('evil eye of urborg', ['Creature']).
card_subtypes('evil eye of urborg', ['Eye']).
card_colors('evil eye of urborg', ['B']).
card_text('evil eye of urborg', 'Non-Eye creatures you control can\'t attack.\nWhenever Evil Eye of Urborg becomes blocked by a creature, destroy that creature.').
card_mana_cost('evil eye of urborg', ['4', 'B']).
card_cmc('evil eye of urborg', 5).
card_layout('evil eye of urborg', 'normal').
card_power('evil eye of urborg', 6).
card_toughness('evil eye of urborg', 3).

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, ME3, NPH
card_name('evil presence', 'Evil Presence').
card_type('evil presence', 'Enchantment — Aura').
card_types('evil presence', ['Enchantment']).
card_subtypes('evil presence', ['Aura']).
card_colors('evil presence', ['B']).
card_text('evil presence', 'Enchant land\nEnchanted land is a Swamp.').
card_mana_cost('evil presence', ['B']).
card_cmc('evil presence', 1).
card_layout('evil presence', 'normal').

% Found in: pHHO
card_name('evil presents', 'Evil Presents').
card_type('evil presents', 'Sorcery').
card_types('evil presents', ['Sorcery']).
card_subtypes('evil presents', []).
card_colors('evil presents', ['B']).
card_text('evil presents', 'Put a creature card from your hand into play under target opponent\'s control. That creature attacks each turn if able... and always attacks its controller.').
card_mana_cost('evil presents', ['2', 'B', 'B']).
card_cmc('evil presents', 4).
card_layout('evil presents', 'normal').

% Found in: ISD
card_name('evil twin', 'Evil Twin').
card_type('evil twin', 'Creature — Shapeshifter').
card_types('evil twin', ['Creature']).
card_subtypes('evil twin', ['Shapeshifter']).
card_colors('evil twin', ['U', 'B']).
card_text('evil twin', 'You may have Evil Twin enter the battlefield as a copy of any creature on the battlefield except it gains \"{U}{B}, {T}: Destroy target creature with the same name as this creature.\"').
card_mana_cost('evil twin', ['2', 'U', 'B']).
card_cmc('evil twin', 4).
card_layout('evil twin', 'normal').
card_power('evil twin', 0).
card_toughness('evil twin', 0).

% Found in: CMD, TMP, TPR
card_name('evincar\'s justice', 'Evincar\'s Justice').
card_type('evincar\'s justice', 'Sorcery').
card_types('evincar\'s justice', ['Sorcery']).
card_subtypes('evincar\'s justice', []).
card_colors('evincar\'s justice', ['B']).
card_text('evincar\'s justice', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nEvincar\'s Justice deals 2 damage to each creature and each player.').
card_mana_cost('evincar\'s justice', ['2', 'B', 'B']).
card_cmc('evincar\'s justice', 4).
card_layout('evincar\'s justice', 'normal').

% Found in: ULG
card_name('eviscerator', 'Eviscerator').
card_type('eviscerator', 'Creature — Horror').
card_types('eviscerator', ['Creature']).
card_subtypes('eviscerator', ['Horror']).
card_colors('eviscerator', ['B']).
card_text('eviscerator', 'Protection from white\nWhen Eviscerator enters the battlefield, you lose 5 life.').
card_mana_cost('eviscerator', ['3', 'B', 'B']).
card_cmc('eviscerator', 5).
card_layout('eviscerator', 'normal').
card_power('eviscerator', 5).
card_toughness('eviscerator', 5).

% Found in: PLC
card_name('evolution charm', 'Evolution Charm').
card_type('evolution charm', 'Instant').
card_types('evolution charm', ['Instant']).
card_subtypes('evolution charm', []).
card_colors('evolution charm', ['G']).
card_text('evolution charm', 'Choose one —\n• Search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.\n• Return target creature card from your graveyard to your hand.\n• Target creature gains flying until end of turn.').
card_mana_cost('evolution charm', ['1', 'G']).
card_cmc('evolution charm', 2).
card_layout('evolution charm', 'normal').

% Found in: DIS
card_name('evolution vat', 'Evolution Vat').
card_type('evolution vat', 'Artifact').
card_types('evolution vat', ['Artifact']).
card_subtypes('evolution vat', []).
card_colors('evolution vat', []).
card_text('evolution vat', '{3}, {T}: Tap target creature and put a +1/+1 counter on it. Until end of turn, that creature gains \"{2}{G}{U}: Double the number of +1/+1 counters on this creature.\"').
card_mana_cost('evolution vat', ['3']).
card_cmc('evolution vat', 3).
card_layout('evolution vat', 'normal').

% Found in: ORI
card_name('evolutionary leap', 'Evolutionary Leap').
card_type('evolutionary leap', 'Enchantment').
card_types('evolutionary leap', ['Enchantment']).
card_subtypes('evolutionary leap', []).
card_colors('evolutionary leap', ['G']).
card_text('evolutionary leap', '{G}, Sacrifice a creature: Reveal cards from the top of your library until you reveal a creature card. Put that card into your hand and the rest on the bottom of your library in a random order.').
card_mana_cost('evolutionary leap', ['1', 'G']).
card_cmc('evolutionary leap', 2).
card_layout('evolutionary leap', 'normal').

% Found in: BFZ, C13, C14, CMD, DDH, DDK, DDN, DDO, DDP, DKA, DTK, M13, M15, MM2, ORI, ROE, pFNM, pMEI
card_name('evolving wilds', 'Evolving Wilds').
card_type('evolving wilds', 'Land').
card_types('evolving wilds', ['Land']).
card_subtypes('evolving wilds', []).
card_colors('evolving wilds', []).
card_text('evolving wilds', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_layout('evolving wilds', 'normal').

% Found in: ONS, V15, pJGP
card_name('exalted angel', 'Exalted Angel').
card_type('exalted angel', 'Creature — Angel').
card_types('exalted angel', ['Creature']).
card_subtypes('exalted angel', ['Angel']).
card_colors('exalted angel', ['W']).
card_text('exalted angel', 'Flying\nWhenever Exalted Angel deals damage, you gain that much life.\nMorph {2}{W}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('exalted angel', ['4', 'W', 'W']).
card_cmc('exalted angel', 6).
card_layout('exalted angel', 'normal').
card_power('exalted angel', 4).
card_toughness('exalted angel', 5).

% Found in: EXO, TPR
card_name('exalted dragon', 'Exalted Dragon').
card_type('exalted dragon', 'Creature — Dragon').
card_types('exalted dragon', ['Creature']).
card_subtypes('exalted dragon', ['Dragon']).
card_colors('exalted dragon', ['W']).
card_text('exalted dragon', 'Flying\nExalted Dragon can\'t attack unless you sacrifice a land. (This cost is paid as attackers are declared.)').
card_mana_cost('exalted dragon', ['4', 'W', 'W']).
card_cmc('exalted dragon', 6).
card_layout('exalted dragon', 'normal').
card_power('exalted dragon', 5).
card_toughness('exalted dragon', 5).
card_reserved('exalted dragon').

% Found in: DGM
card_name('exava, rakdos blood witch', 'Exava, Rakdos Blood Witch').
card_type('exava, rakdos blood witch', 'Legendary Creature — Human Cleric').
card_types('exava, rakdos blood witch', ['Creature']).
card_subtypes('exava, rakdos blood witch', ['Human', 'Cleric']).
card_supertypes('exava, rakdos blood witch', ['Legendary']).
card_colors('exava, rakdos blood witch', ['B', 'R']).
card_text('exava, rakdos blood witch', 'First strike, haste\nUnleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)\nEach other creature you control with a +1/+1 counter on it has haste.').
card_mana_cost('exava, rakdos blood witch', ['2', 'B', 'R']).
card_cmc('exava, rakdos blood witch', 4).
card_layout('exava, rakdos blood witch', 'normal').
card_power('exava, rakdos blood witch', 3).
card_toughness('exava, rakdos blood witch', 3).

% Found in: PCY
card_name('excavation', 'Excavation').
card_type('excavation', 'Enchantment').
card_types('excavation', ['Enchantment']).
card_subtypes('excavation', []).
card_colors('excavation', ['U']).
card_text('excavation', '{1}, Sacrifice a land: Draw a card. Any player may activate this ability.').
card_mana_cost('excavation', ['1', 'U']).
card_cmc('excavation', 2).
card_layout('excavation', 'normal').

% Found in: TMP
card_name('excavator', 'Excavator').
card_type('excavator', 'Artifact').
card_types('excavator', ['Artifact']).
card_subtypes('excavator', []).
card_colors('excavator', []).
card_text('excavator', '{T}, Sacrifice a basic land: Target creature gains landwalk of each of the land types of the sacrificed land until end of turn. (It can\'t be blocked as long as defending player controls a land of any of those types.)').
card_mana_cost('excavator', ['2']).
card_cmc('excavator', 2).
card_layout('excavator', 'normal').

% Found in: PCY
card_name('excise', 'Excise').
card_type('excise', 'Instant').
card_types('excise', ['Instant']).
card_subtypes('excise', []).
card_colors('excise', ['W']).
card_text('excise', 'Exile target attacking creature unless its controller pays {X}.').
card_mana_cost('excise', ['X', 'W']).
card_cmc('excise', 1).
card_layout('excise', 'normal').

% Found in: C14, INV
card_name('exclude', 'Exclude').
card_type('exclude', 'Instant').
card_types('exclude', ['Instant']).
card_subtypes('exclude', []).
card_colors('exclude', ['U']).
card_text('exclude', 'Counter target creature spell.\nDraw a card.').
card_mana_cost('exclude', ['2', 'U']).
card_cmc('exclude', 3).
card_layout('exclude', 'normal').

% Found in: NPH
card_name('exclusion ritual', 'Exclusion Ritual').
card_type('exclusion ritual', 'Enchantment').
card_types('exclusion ritual', ['Enchantment']).
card_subtypes('exclusion ritual', []).
card_colors('exclusion ritual', ['W']).
card_text('exclusion ritual', 'Imprint — When Exclusion Ritual enters the battlefield, exile target nonland permanent.\nPlayers can\'t cast spells with the same name as the exiled card.').
card_mana_cost('exclusion ritual', ['4', 'W', 'W']).
card_cmc('exclusion ritual', 6).
card_layout('exclusion ritual', 'normal').

% Found in: ALA, M10, M11
card_name('excommunicate', 'Excommunicate').
card_type('excommunicate', 'Sorcery').
card_types('excommunicate', ['Sorcery']).
card_subtypes('excommunicate', []).
card_colors('excommunicate', ['W']).
card_text('excommunicate', 'Put target creature on top of its owner\'s library.').
card_mana_cost('excommunicate', ['2', 'W']).
card_cmc('excommunicate', 3).
card_layout('excommunicate', 'normal').

% Found in: BNG
card_name('excoriate', 'Excoriate').
card_type('excoriate', 'Sorcery').
card_types('excoriate', ['Sorcery']).
card_subtypes('excoriate', []).
card_colors('excoriate', ['W']).
card_text('excoriate', 'Exile target tapped creature.').
card_mana_cost('excoriate', ['3', 'W']).
card_cmc('excoriate', 4).
card_layout('excoriate', 'normal').

% Found in: RAV
card_name('excruciator', 'Excruciator').
card_type('excruciator', 'Creature — Avatar').
card_types('excruciator', ['Creature']).
card_subtypes('excruciator', ['Avatar']).
card_colors('excruciator', ['R']).
card_text('excruciator', 'Damage that would be dealt by Excruciator can\'t be prevented.').
card_mana_cost('excruciator', ['6', 'R', 'R']).
card_cmc('excruciator', 8).
card_layout('excruciator', 'normal').
card_power('excruciator', 7).
card_toughness('excruciator', 7).

% Found in: 8ED, 9ED, ODY
card_name('execute', 'Execute').
card_type('execute', 'Instant').
card_types('execute', ['Instant']).
card_subtypes('execute', []).
card_colors('execute', ['B']).
card_text('execute', 'Destroy target white creature. It can\'t be regenerated.\nDraw a card.').
card_mana_cost('execute', ['2', 'B']).
card_cmc('execute', 3).
card_layout('execute', 'normal').

% Found in: ALA, MMA
card_name('executioner\'s capsule', 'Executioner\'s Capsule').
card_type('executioner\'s capsule', 'Artifact').
card_types('executioner\'s capsule', ['Artifact']).
card_subtypes('executioner\'s capsule', []).
card_colors('executioner\'s capsule', ['B']).
card_text('executioner\'s capsule', '{1}{B}, {T}, Sacrifice Executioner\'s Capsule: Destroy target nonblack creature.').
card_mana_cost('executioner\'s capsule', ['B']).
card_cmc('executioner\'s capsule', 1).
card_layout('executioner\'s capsule', 'normal').

% Found in: DKA
card_name('executioner\'s hood', 'Executioner\'s Hood').
card_type('executioner\'s hood', 'Artifact — Equipment').
card_types('executioner\'s hood', ['Artifact']).
card_subtypes('executioner\'s hood', ['Equipment']).
card_colors('executioner\'s hood', []).
card_text('executioner\'s hood', 'Equipped creature has intimidate. (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('executioner\'s hood', ['2']).
card_cmc('executioner\'s hood', 2).
card_layout('executioner\'s hood', 'normal').

% Found in: GTC
card_name('executioner\'s swing', 'Executioner\'s Swing').
card_type('executioner\'s swing', 'Instant').
card_types('executioner\'s swing', ['Instant']).
card_subtypes('executioner\'s swing', []).
card_colors('executioner\'s swing', ['W', 'B']).
card_text('executioner\'s swing', 'Target creature that dealt damage this turn gets -5/-5 until end of turn.').
card_mana_cost('executioner\'s swing', ['W', 'B']).
card_cmc('executioner\'s swing', 2).
card_layout('executioner\'s swing', 'normal').

% Found in: BFZ
card_name('exert influence', 'Exert Influence').
card_type('exert influence', 'Sorcery').
card_types('exert influence', ['Sorcery']).
card_subtypes('exert influence', []).
card_colors('exert influence', ['U']).
card_text('exert influence', 'Converge — Gain control of target creature if its power is less than or equal to the number of colors of mana spent to cast Exert Influence.').
card_mana_cost('exert influence', ['4', 'U']).
card_cmc('exert influence', 5).
card_layout('exert influence', 'normal').

% Found in: 9ED, PO2, POR, PTK, S99, USG
card_name('exhaustion', 'Exhaustion').
card_type('exhaustion', 'Sorcery').
card_types('exhaustion', ['Sorcery']).
card_subtypes('exhaustion', []).
card_colors('exhaustion', ['U']).
card_text('exhaustion', 'Creatures and lands target opponent controls don\'t untap during his or her next untap step.').
card_mana_cost('exhaustion', ['2', 'U']).
card_cmc('exhaustion', 3).
card_layout('exhaustion', 'normal').

% Found in: BRB, PD3, USG
card_name('exhume', 'Exhume').
card_type('exhume', 'Sorcery').
card_types('exhume', ['Sorcery']).
card_subtypes('exhume', []).
card_colors('exhume', ['B']).
card_text('exhume', 'Each player puts a creature card from his or her graveyard onto the battlefield.').
card_mana_cost('exhume', ['1', 'B']).
card_cmc('exhume', 2).
card_layout('exhume', 'normal').

% Found in: GPT
card_name('exhumer thrull', 'Exhumer Thrull').
card_type('exhumer thrull', 'Creature — Thrull').
card_types('exhumer thrull', ['Creature']).
card_subtypes('exhumer thrull', ['Thrull']).
card_colors('exhumer thrull', ['B']).
card_text('exhumer thrull', 'Haunt (When this creature dies, exile it haunting target creature.)\nWhen Exhumer Thrull enters the battlefield or the creature it haunts dies, return target creature card from your graveyard to your hand.').
card_mana_cost('exhumer thrull', ['5', 'B']).
card_cmc('exhumer thrull', 6).
card_layout('exhumer thrull', 'normal').
card_power('exhumer thrull', 3).
card_toughness('exhumer thrull', 3).

% Found in: 6ED, ALL, MED, VMA
card_name('exile', 'Exile').
card_type('exile', 'Instant').
card_types('exile', ['Instant']).
card_subtypes('exile', []).
card_colors('exile', ['W']).
card_text('exile', 'Exile target nonwhite attacking creature. You gain life equal to its toughness.').
card_mana_cost('exile', ['2', 'W']).
card_cmc('exile', 3).
card_layout('exile', 'normal').

% Found in: SOK
card_name('exile into darkness', 'Exile into Darkness').
card_type('exile into darkness', 'Sorcery').
card_types('exile into darkness', ['Sorcery']).
card_subtypes('exile into darkness', []).
card_colors('exile into darkness', ['B']).
card_text('exile into darkness', 'Target player sacrifices a creature with converted mana cost 3 or less.\nAt the beginning of your upkeep, if you have more cards in hand than each opponent, you may return Exile into Darkness from your graveyard to your hand.').
card_mana_cost('exile into darkness', ['4', 'B']).
card_cmc('exile into darkness', 5).
card_layout('exile into darkness', 'normal').

% Found in: LRW
card_name('exiled boggart', 'Exiled Boggart').
card_type('exiled boggart', 'Creature — Goblin Rogue').
card_types('exiled boggart', ['Creature']).
card_subtypes('exiled boggart', ['Goblin', 'Rogue']).
card_colors('exiled boggart', ['B']).
card_text('exiled boggart', 'When Exiled Boggart dies, discard a card.').
card_mana_cost('exiled boggart', ['1', 'B']).
card_cmc('exiled boggart', 2).
card_layout('exiled boggart', 'normal').
card_power('exiled boggart', 2).
card_toughness('exiled boggart', 2).

% Found in: SCG
card_name('exiled doomsayer', 'Exiled Doomsayer').
card_type('exiled doomsayer', 'Creature — Human Cleric').
card_types('exiled doomsayer', ['Creature']).
card_subtypes('exiled doomsayer', ['Human', 'Cleric']).
card_colors('exiled doomsayer', ['W']).
card_text('exiled doomsayer', 'All morph costs cost {2} more. (This doesn\'t affect the cost to cast creature spells face down.)').
card_mana_cost('exiled doomsayer', ['1', 'W']).
card_cmc('exiled doomsayer', 2).
card_layout('exiled doomsayer', 'normal').
card_power('exiled doomsayer', 1).
card_toughness('exiled doomsayer', 2).

% Found in: DRK, ME3
card_name('exorcist', 'Exorcist').
card_type('exorcist', 'Creature — Human Cleric').
card_types('exorcist', ['Creature']).
card_subtypes('exorcist', ['Human', 'Cleric']).
card_colors('exorcist', ['W']).
card_text('exorcist', '{1}{W}, {T}: Destroy target black creature.').
card_mana_cost('exorcist', ['W', 'W']).
card_cmc('exorcist', 2).
card_layout('exorcist', 'normal').
card_power('exorcist', 1).
card_toughness('exorcist', 1).
card_reserved('exorcist').

% Found in: JUD
card_name('exoskeletal armor', 'Exoskeletal Armor').
card_type('exoskeletal armor', 'Enchantment — Aura').
card_types('exoskeletal armor', ['Enchantment']).
card_subtypes('exoskeletal armor', ['Aura']).
card_colors('exoskeletal armor', ['G']).
card_text('exoskeletal armor', 'Enchant creature\nEnchanted creature gets +X/+X, where X is the number of creature cards in all graveyards.').
card_mana_cost('exoskeletal armor', ['1', 'G']).
card_cmc('exoskeletal armor', 2).
card_layout('exoskeletal armor', 'normal').

% Found in: DDE, INV
card_name('exotic curse', 'Exotic Curse').
card_type('exotic curse', 'Enchantment — Aura').
card_types('exotic curse', ['Enchantment']).
card_subtypes('exotic curse', ['Aura']).
card_colors('exotic curse', ['B']).
card_text('exotic curse', 'Enchant creature\nDomain — Enchanted creature gets -1/-1 for each basic land type among lands you control.').
card_mana_cost('exotic curse', ['2', 'B']).
card_cmc('exotic curse', 3).
card_layout('exotic curse', 'normal').

% Found in: PLS
card_name('exotic disease', 'Exotic Disease').
card_type('exotic disease', 'Sorcery').
card_types('exotic disease', ['Sorcery']).
card_subtypes('exotic disease', []).
card_colors('exotic disease', ['B']).
card_text('exotic disease', 'Domain — Target player loses X life and you gain X life, where X is the number of basic land types among lands you control.').
card_mana_cost('exotic disease', ['4', 'B']).
card_cmc('exotic disease', 5).
card_layout('exotic disease', 'normal').

% Found in: CON, PC2
card_name('exotic orchard', 'Exotic Orchard').
card_type('exotic orchard', 'Land').
card_types('exotic orchard', ['Land']).
card_subtypes('exotic orchard', []).
card_colors('exotic orchard', []).
card_text('exotic orchard', '{T}: Add to your mana pool one mana of any color that a land an opponent controls could produce.').
card_layout('exotic orchard', 'normal').

% Found in: BFZ
card_name('expedition envoy', 'Expedition Envoy').
card_type('expedition envoy', 'Creature — Human Scout Ally').
card_types('expedition envoy', ['Creature']).
card_subtypes('expedition envoy', ['Human', 'Scout', 'Ally']).
card_colors('expedition envoy', ['W']).
card_text('expedition envoy', '').
card_mana_cost('expedition envoy', ['W']).
card_cmc('expedition envoy', 1).
card_layout('expedition envoy', 'normal').
card_power('expedition envoy', 2).
card_toughness('expedition envoy', 1).

% Found in: MM2, ZEN
card_name('expedition map', 'Expedition Map').
card_type('expedition map', 'Artifact').
card_types('expedition map', ['Artifact']).
card_subtypes('expedition map', []).
card_colors('expedition map', []).
card_text('expedition map', '{2}, {T}, Sacrifice Expedition Map: Search your library for a land card, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('expedition map', ['1']).
card_cmc('expedition map', 1).
card_layout('expedition map', 'normal').

% Found in: ULG
card_name('expendable troops', 'Expendable Troops').
card_type('expendable troops', 'Creature — Human Soldier').
card_types('expendable troops', ['Creature']).
card_subtypes('expendable troops', ['Human', 'Soldier']).
card_colors('expendable troops', ['W']).
card_text('expendable troops', '{T}, Sacrifice Expendable Troops: Expendable Troops deals 2 damage to target attacking or blocking creature.').
card_mana_cost('expendable troops', ['1', 'W']).
card_cmc('expendable troops', 2).
card_layout('expendable troops', 'normal').
card_power('expendable troops', 2).
card_toughness('expendable troops', 1).

% Found in: DIS
card_name('experiment kraj', 'Experiment Kraj').
card_type('experiment kraj', 'Legendary Creature — Ooze Mutant').
card_types('experiment kraj', ['Creature']).
card_subtypes('experiment kraj', ['Ooze', 'Mutant']).
card_supertypes('experiment kraj', ['Legendary']).
card_colors('experiment kraj', ['U', 'G']).
card_text('experiment kraj', 'Experiment Kraj has all activated abilities of each other creature with a +1/+1 counter on it.\n{T}: Put a +1/+1 counter on target creature.').
card_mana_cost('experiment kraj', ['2', 'G', 'G', 'U', 'U']).
card_cmc('experiment kraj', 6).
card_layout('experiment kraj', 'normal').
card_power('experiment kraj', 4).
card_toughness('experiment kraj', 6).

% Found in: GTC, pFNM
card_name('experiment one', 'Experiment One').
card_type('experiment one', 'Creature — Human Ooze').
card_types('experiment one', ['Creature']).
card_subtypes('experiment one', ['Human', 'Ooze']).
card_colors('experiment one', ['G']).
card_text('experiment one', 'Evolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)\nRemove two +1/+1 counters from Experiment One: Regenerate Experiment One.').
card_mana_cost('experiment one', ['G']).
card_cmc('experiment one', 1).
card_layout('experiment one', 'normal').
card_power('experiment one', 1).
card_toughness('experiment one', 1).

% Found in: CON
card_name('exploding borders', 'Exploding Borders').
card_type('exploding borders', 'Sorcery').
card_types('exploding borders', ['Sorcery']).
card_subtypes('exploding borders', []).
card_colors('exploding borders', ['R', 'G']).
card_text('exploding borders', 'Domain — Search your library for a basic land card, put that card onto the battlefield tapped, then shuffle your library. Exploding Borders deals X damage to target player, where X is the number of basic land types among lands you control.').
card_mana_cost('exploding borders', ['2', 'R', 'G']).
card_cmc('exploding borders', 4).
card_layout('exploding borders', 'normal').

% Found in: CNS, USG
card_name('exploration', 'Exploration').
card_type('exploration', 'Enchantment').
card_types('exploration', ['Enchantment']).
card_subtypes('exploration', []).
card_colors('exploration', ['G']).
card_text('exploration', 'You may play an additional land on each of your turns.').
card_mana_cost('exploration', ['G']).
card_cmc('exploration', 1).
card_layout('exploration', 'normal').

% Found in: DDO, WWK
card_name('explore', 'Explore').
card_type('explore', 'Sorcery').
card_types('explore', ['Sorcery']).
card_subtypes('explore', []).
card_colors('explore', ['G']).
card_text('explore', 'You may play an additional land this turn.\nDraw a card.').
card_mana_cost('explore', ['1', 'G']).
card_cmc('explore', 2).
card_layout('explore', 'normal').

% Found in: CNS, DDP, ZEN
card_name('explorer\'s scope', 'Explorer\'s Scope').
card_type('explorer\'s scope', 'Artifact — Equipment').
card_types('explorer\'s scope', ['Artifact']).
card_subtypes('explorer\'s scope', ['Equipment']).
card_colors('explorer\'s scope', []).
card_text('explorer\'s scope', 'Whenever equipped creature attacks, look at the top card of your library. If it\'s a land card, you may put it onto the battlefield tapped.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('explorer\'s scope', ['1']).
card_cmc('explorer\'s scope', 1).
card_layout('explorer\'s scope', 'normal').

% Found in: INV
card_name('explosive growth', 'Explosive Growth').
card_type('explosive growth', 'Instant').
card_types('explosive growth', ['Instant']).
card_subtypes('explosive growth', []).
card_colors('explosive growth', ['G']).
card_text('explosive growth', 'Kicker {5} (You may pay an additional {5} as you cast this spell.)\nTarget creature gets +2/+2 until end of turn. If Explosive Growth was kicked, that creature gets +5/+5 until end of turn instead.').
card_mana_cost('explosive growth', ['G']).
card_cmc('explosive growth', 1).
card_layout('explosive growth', 'normal').

% Found in: RTR
card_name('explosive impact', 'Explosive Impact').
card_type('explosive impact', 'Instant').
card_types('explosive impact', ['Instant']).
card_subtypes('explosive impact', []).
card_colors('explosive impact', ['R']).
card_text('explosive impact', 'Explosive Impact deals 5 damage to target creature or player.').
card_mana_cost('explosive impact', ['5', 'R']).
card_cmc('explosive impact', 6).
card_layout('explosive impact', 'normal').

% Found in: ROE
card_name('explosive revelation', 'Explosive Revelation').
card_type('explosive revelation', 'Sorcery').
card_types('explosive revelation', ['Sorcery']).
card_subtypes('explosive revelation', []).
card_colors('explosive revelation', ['R']).
card_text('explosive revelation', 'Choose target creature or player. Reveal cards from the top of your library until you reveal a nonland card. Explosive Revelation deals damage equal to that card\'s converted mana cost to that creature or player. Put the nonland card into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('explosive revelation', ['3', 'R', 'R']).
card_cmc('explosive revelation', 5).
card_layout('explosive revelation', 'normal').

% Found in: CMD, DDO, DTK, HOP, ONS
card_name('explosive vegetation', 'Explosive Vegetation').
card_type('explosive vegetation', 'Sorcery').
card_types('explosive vegetation', ['Sorcery']).
card_subtypes('explosive vegetation', []).
card_colors('explosive vegetation', ['G']).
card_text('explosive vegetation', 'Search your library for up to two basic land cards and put them onto the battlefield tapped. Then shuffle your library.').
card_mana_cost('explosive vegetation', ['3', 'G']).
card_cmc('explosive vegetation', 4).
card_layout('explosive vegetation', 'normal').

% Found in: USG, VMA
card_name('expunge', 'Expunge').
card_type('expunge', 'Instant').
card_types('expunge', ['Instant']).
card_subtypes('expunge', []).
card_colors('expunge', ['B']).
card_text('expunge', 'Destroy target nonartifact, nonblack creature. It can\'t be regenerated.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('expunge', ['2', 'B']).
card_cmc('expunge', 3).
card_layout('expunge', 'normal').

% Found in: AVR
card_name('exquisite blood', 'Exquisite Blood').
card_type('exquisite blood', 'Enchantment').
card_types('exquisite blood', ['Enchantment']).
card_subtypes('exquisite blood', []).
card_colors('exquisite blood', ['B']).
card_text('exquisite blood', 'Whenever an opponent loses life, you gain that much life.').
card_mana_cost('exquisite blood', ['4', 'B']).
card_cmc('exquisite blood', 5).
card_layout('exquisite blood', 'normal').

% Found in: ORI
card_name('exquisite firecraft', 'Exquisite Firecraft').
card_type('exquisite firecraft', 'Sorcery').
card_types('exquisite firecraft', ['Sorcery']).
card_subtypes('exquisite firecraft', []).
card_colors('exquisite firecraft', ['R']).
card_text('exquisite firecraft', 'Exquisite Firecraft deals 4 damage to target creature or player.\nSpell mastery — If there are two or more instant and/or sorcery cards in your graveyard, Exquisite Firecraft can\'t be countered by spells or abilities.').
card_mana_cost('exquisite firecraft', ['1', 'R', 'R']).
card_cmc('exquisite firecraft', 3).
card_layout('exquisite firecraft', 'normal').

% Found in: SOM
card_name('exsanguinate', 'Exsanguinate').
card_type('exsanguinate', 'Sorcery').
card_types('exsanguinate', ['Sorcery']).
card_subtypes('exsanguinate', []).
card_colors('exsanguinate', ['B']).
card_text('exsanguinate', 'Each opponent loses X life. You gain life equal to the life lost this way.').
card_mana_cost('exsanguinate', ['X', 'B', 'B']).
card_cmc('exsanguinate', 2).
card_layout('exsanguinate', 'normal').

% Found in: TMP
card_name('extinction', 'Extinction').
card_type('extinction', 'Sorcery').
card_types('extinction', ['Sorcery']).
card_subtypes('extinction', []).
card_colors('extinction', ['B']).
card_text('extinction', 'Destroy all creatures of the creature type of your choice.').
card_mana_cost('extinction', ['4', 'B']).
card_cmc('extinction', 5).
card_layout('extinction', 'normal').

% Found in: PO2, PTK, S99
card_name('extinguish', 'Extinguish').
card_type('extinguish', 'Instant').
card_types('extinguish', ['Instant']).
card_subtypes('extinguish', []).
card_colors('extinguish', ['U']).
card_text('extinguish', 'Counter target sorcery spell.').
card_mana_cost('extinguish', ['1', 'U']).
card_cmc('extinguish', 2).
card_layout('extinguish', 'normal').

% Found in: JOU
card_name('extinguish all hope', 'Extinguish All Hope').
card_type('extinguish all hope', 'Sorcery').
card_types('extinguish all hope', ['Sorcery']).
card_subtypes('extinguish all hope', []).
card_colors('extinguish all hope', ['B']).
card_text('extinguish all hope', 'Destroy all nonenchantment creatures.').
card_mana_cost('extinguish all hope', ['4', 'B', 'B']).
card_cmc('extinguish all hope', 6).
card_layout('extinguish all hope', 'normal').

% Found in: MMA, PLC
card_name('extirpate', 'Extirpate').
card_type('extirpate', 'Instant').
card_types('extirpate', ['Instant']).
card_subtypes('extirpate', []).
card_colors('extirpate', ['B']).
card_text('extirpate', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nChoose target card in a graveyard other than a basic land card. Search its owner\'s graveyard, hand, and library for all cards with the same name as that card and exile them. Then that player shuffles his or her library.').
card_mana_cost('extirpate', ['B']).
card_cmc('extirpate', 1).
card_layout('extirpate', 'normal').

% Found in: MMQ
card_name('extortion', 'Extortion').
card_type('extortion', 'Sorcery').
card_types('extortion', ['Sorcery']).
card_subtypes('extortion', []).
card_colors('extortion', ['B']).
card_text('extortion', 'Look at target player\'s hand and choose up to two cards from it. That player discards those cards.').
card_mana_cost('extortion', ['3', 'B', 'B']).
card_cmc('extortion', 5).
card_layout('extortion', 'normal').

% Found in: SCG
card_name('extra arms', 'Extra Arms').
card_type('extra arms', 'Enchantment — Aura').
card_types('extra arms', ['Enchantment']).
card_subtypes('extra arms', ['Aura']).
card_colors('extra arms', ['R']).
card_text('extra arms', 'Enchant creature\nWhenever enchanted creature attacks, it deals 2 damage to target creature or player.').
card_mana_cost('extra arms', ['4', 'R']).
card_cmc('extra arms', 5).
card_layout('extra arms', 'normal').

% Found in: ODY
card_name('extract', 'Extract').
card_type('extract', 'Sorcery').
card_types('extract', ['Sorcery']).
card_subtypes('extract', []).
card_colors('extract', ['U']).
card_text('extract', 'Search target player\'s library for a card and exile it. Then that player shuffles his or her library.').
card_mana_cost('extract', ['U']).
card_cmc('extract', 1).
card_layout('extract', 'normal').

% Found in: CNS
card_name('extract from darkness', 'Extract from Darkness').
card_type('extract from darkness', 'Sorcery').
card_types('extract from darkness', ['Sorcery']).
card_subtypes('extract from darkness', []).
card_colors('extract from darkness', ['U', 'B']).
card_text('extract from darkness', 'Each player puts the top two cards of his or her library into his or her graveyard. Then put a creature card from a graveyard onto the battlefield under your control.').
card_mana_cost('extract from darkness', ['3', 'U', 'B']).
card_cmc('extract from darkness', 5).
card_layout('extract from darkness', 'normal').

% Found in: ARC, CMD, CON
card_name('extractor demon', 'Extractor Demon').
card_type('extractor demon', 'Creature — Demon').
card_types('extractor demon', ['Creature']).
card_subtypes('extractor demon', ['Demon']).
card_colors('extractor demon', ['B']).
card_text('extractor demon', 'Flying\nWhenever another creature leaves the battlefield, you may have target player put the top two cards of his or her library into his or her graveyard.\nUnearth {2}{B} ({2}{B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('extractor demon', ['4', 'B', 'B']).
card_cmc('extractor demon', 6).
card_layout('extractor demon', 'normal').
card_power('extractor demon', 5).
card_toughness('extractor demon', 5).

% Found in: MRD
card_name('extraplanar lens', 'Extraplanar Lens').
card_type('extraplanar lens', 'Artifact').
card_types('extraplanar lens', ['Artifact']).
card_subtypes('extraplanar lens', []).
card_colors('extraplanar lens', []).
card_text('extraplanar lens', 'Imprint — When Extraplanar Lens enters the battlefield, you may exile target land you control.\nWhenever a land with the same name as the exiled card is tapped for mana, its controller adds one mana to his or her mana pool of any type that land produced.').
card_mana_cost('extraplanar lens', ['3']).
card_cmc('extraplanar lens', 3).
card_layout('extraplanar lens', 'normal').

% Found in: MMQ
card_name('extravagant spirit', 'Extravagant Spirit').
card_type('extravagant spirit', 'Creature — Spirit').
card_types('extravagant spirit', ['Creature']).
card_subtypes('extravagant spirit', ['Spirit']).
card_colors('extravagant spirit', ['U']).
card_text('extravagant spirit', 'Flying\nAt the beginning of your upkeep, sacrifice Extravagant Spirit unless you pay {1} for each card in your hand.').
card_mana_cost('extravagant spirit', ['3', 'U']).
card_cmc('extravagant spirit', 4).
card_layout('extravagant spirit', 'normal').
card_power('extravagant spirit', 4).
card_toughness('extravagant spirit', 4).

% Found in: UDS
card_name('extruder', 'Extruder').
card_type('extruder', 'Artifact Creature — Juggernaut').
card_types('extruder', ['Artifact', 'Creature']).
card_subtypes('extruder', ['Juggernaut']).
card_colors('extruder', []).
card_text('extruder', 'Echo {4} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nSacrifice an artifact: Put a +1/+1 counter on target creature.').
card_mana_cost('extruder', ['4']).
card_cmc('extruder', 4).
card_layout('extruder', 'normal').
card_power('extruder', 4).
card_toughness('extruder', 3).

% Found in: ALA
card_name('exuberant firestoker', 'Exuberant Firestoker').
card_type('exuberant firestoker', 'Creature — Human Druid Shaman').
card_types('exuberant firestoker', ['Creature']).
card_subtypes('exuberant firestoker', ['Human', 'Druid', 'Shaman']).
card_colors('exuberant firestoker', ['R']).
card_text('exuberant firestoker', 'At the beginning of your end step, if you control a creature with power 5 or greater, you may have Exuberant Firestoker deal 2 damage to target player.\n{T}: Add {1} to your mana pool.').
card_mana_cost('exuberant firestoker', ['2', 'R']).
card_cmc('exuberant firestoker', 3).
card_layout('exuberant firestoker', 'normal').
card_power('exuberant firestoker', 1).
card_toughness('exuberant firestoker', 1).

% Found in: 3ED, 4ED, 5ED, ARN, ME4
card_name('eye for an eye', 'Eye for an Eye').
card_type('eye for an eye', 'Instant').
card_types('eye for an eye', ['Instant']).
card_subtypes('eye for an eye', []).
card_colors('eye for an eye', ['W']).
card_text('eye for an eye', 'The next time a source of your choice would deal damage to you this turn, instead that source deals that much damage to you and Eye for an Eye deals that much damage to that source\'s controller.').
card_mana_cost('eye for an eye', ['W', 'W']).
card_cmc('eye for an eye', 2).
card_layout('eye for an eye', 'normal').

% Found in: BNG
card_name('eye gouge', 'Eye Gouge').
card_type('eye gouge', 'Instant').
card_types('eye gouge', ['Instant']).
card_subtypes('eye gouge', []).
card_colors('eye gouge', ['B']).
card_text('eye gouge', 'Target creature gets -1/-1 until end of turn. If it\'s a Cyclops, destroy it.').
card_mana_cost('eye gouge', ['B']).
card_cmc('eye gouge', 1).
card_layout('eye gouge', 'normal').

% Found in: C13
card_name('eye of doom', 'Eye of Doom').
card_type('eye of doom', 'Artifact').
card_types('eye of doom', ['Artifact']).
card_subtypes('eye of doom', []).
card_colors('eye of doom', []).
card_text('eye of doom', 'When Eye of Doom enters the battlefield, each player chooses a nonland permanent and puts a doom counter on it.\n{2}, {T}, Sacrifice Eye of Doom: Destroy each permanent with a doom counter on it.').
card_mana_cost('eye of doom', ['4']).
card_cmc('eye of doom', 4).
card_layout('eye of doom', 'normal').

% Found in: CHK
card_name('eye of nowhere', 'Eye of Nowhere').
card_type('eye of nowhere', 'Sorcery — Arcane').
card_types('eye of nowhere', ['Sorcery']).
card_subtypes('eye of nowhere', ['Arcane']).
card_colors('eye of nowhere', ['U']).
card_text('eye of nowhere', 'Return target permanent to its owner\'s hand.').
card_mana_cost('eye of nowhere', ['U', 'U']).
card_cmc('eye of nowhere', 2).
card_layout('eye of nowhere', 'normal').

% Found in: MMQ
card_name('eye of ramos', 'Eye of Ramos').
card_type('eye of ramos', 'Artifact').
card_types('eye of ramos', ['Artifact']).
card_subtypes('eye of ramos', []).
card_colors('eye of ramos', []).
card_text('eye of ramos', '{T}: Add {U} to your mana pool.\nSacrifice Eye of Ramos: Add {U} to your mana pool.').
card_mana_cost('eye of ramos', ['3']).
card_cmc('eye of ramos', 3).
card_layout('eye of ramos', 'normal').

% Found in: VIS
card_name('eye of singularity', 'Eye of Singularity').
card_type('eye of singularity', 'World Enchantment').
card_types('eye of singularity', ['Enchantment']).
card_subtypes('eye of singularity', []).
card_supertypes('eye of singularity', ['World']).
card_colors('eye of singularity', ['W']).
card_text('eye of singularity', 'When Eye of Singularity enters the battlefield, destroy each permanent with the same name as another permanent, except for basic lands. They can\'t be regenerated.\nWhenever a permanent other than a basic land enters the battlefield, destroy all other permanents with that name. They can\'t be regenerated.').
card_mana_cost('eye of singularity', ['3', 'W']).
card_cmc('eye of singularity', 4).
card_layout('eye of singularity', 'normal').
card_reserved('eye of singularity').

% Found in: RAV
card_name('eye of the storm', 'Eye of the Storm').
card_type('eye of the storm', 'Enchantment').
card_types('eye of the storm', ['Enchantment']).
card_subtypes('eye of the storm', []).
card_colors('eye of the storm', ['U']).
card_text('eye of the storm', 'Whenever a player casts an instant or sorcery card, exile it. Then that player copies each instant or sorcery card exiled with Eye of the Storm. For each copy, the player may cast the copy without paying its mana cost.').
card_mana_cost('eye of the storm', ['5', 'U', 'U']).
card_cmc('eye of the storm', 7).
card_layout('eye of the storm', 'normal').

% Found in: MM2, WWK
card_name('eye of ugin', 'Eye of Ugin').
card_type('eye of ugin', 'Legendary Land').
card_types('eye of ugin', ['Land']).
card_subtypes('eye of ugin', []).
card_supertypes('eye of ugin', ['Legendary']).
card_colors('eye of ugin', []).
card_text('eye of ugin', 'Colorless Eldrazi spells you cast cost {2} less to cast.\n{7}, {T}: Search your library for a colorless creature card, reveal it, and put it into your hand. Then shuffle your library.').
card_layout('eye of ugin', 'normal').

% Found in: NMS
card_name('eye of yawgmoth', 'Eye of Yawgmoth').
card_type('eye of yawgmoth', 'Artifact').
card_types('eye of yawgmoth', ['Artifact']).
card_subtypes('eye of yawgmoth', []).
card_colors('eye of yawgmoth', []).
card_text('eye of yawgmoth', '{3}, {T}, Sacrifice a creature: Reveal a number of cards from the top of your library equal to the sacrificed creature\'s power. Put one into your hand and exile the rest.').
card_mana_cost('eye of yawgmoth', ['3']).
card_cmc('eye of yawgmoth', 3).
card_layout('eye of yawgmoth', 'normal').

% Found in: PO2, S99
card_name('eye spy', 'Eye Spy').
card_type('eye spy', 'Sorcery').
card_types('eye spy', ['Sorcery']).
card_subtypes('eye spy', []).
card_colors('eye spy', ['U']).
card_text('eye spy', 'Look at the top card of target player\'s library. You may put that card into his or her graveyard.').
card_mana_cost('eye spy', ['U']).
card_cmc('eye spy', 1).
card_layout('eye spy', 'normal').

% Found in: UNH
card_name('eye to eye', 'Eye to Eye').
card_type('eye to eye', 'Instant').
card_types('eye to eye', ['Instant']).
card_subtypes('eye to eye', []).
card_colors('eye to eye', ['B']).
card_text('eye to eye', 'You and target creature\'s controller have a staring contest. If you win, destroy that creature.').
card_mana_cost('eye to eye', ['2', 'B']).
card_cmc('eye to eye', 3).
card_layout('eye to eye', 'normal').

% Found in: ORI
card_name('eyeblight assassin', 'Eyeblight Assassin').
card_type('eyeblight assassin', 'Creature — Elf Assassin').
card_types('eyeblight assassin', ['Creature']).
card_subtypes('eyeblight assassin', ['Elf', 'Assassin']).
card_colors('eyeblight assassin', ['B']).
card_text('eyeblight assassin', 'When Eyeblight Assassin enters the battlefield, target creature an opponent controls gets -1/-1 until end of turn.').
card_mana_cost('eyeblight assassin', ['2', 'B']).
card_cmc('eyeblight assassin', 3).
card_layout('eyeblight assassin', 'normal').
card_power('eyeblight assassin', 2).
card_toughness('eyeblight assassin', 2).

% Found in: ORI
card_name('eyeblight massacre', 'Eyeblight Massacre').
card_type('eyeblight massacre', 'Sorcery').
card_types('eyeblight massacre', ['Sorcery']).
card_subtypes('eyeblight massacre', []).
card_colors('eyeblight massacre', ['B']).
card_text('eyeblight massacre', 'Non-Elf creatures get -2/-2 until end of turn.').
card_mana_cost('eyeblight massacre', ['2', 'B', 'B']).
card_cmc('eyeblight massacre', 4).
card_layout('eyeblight massacre', 'normal').

% Found in: DPA, LRW
card_name('eyeblight\'s ending', 'Eyeblight\'s Ending').
card_type('eyeblight\'s ending', 'Tribal Instant — Elf').
card_types('eyeblight\'s ending', ['Tribal', 'Instant']).
card_subtypes('eyeblight\'s ending', ['Elf']).
card_colors('eyeblight\'s ending', ['B']).
card_text('eyeblight\'s ending', 'Destroy target non-Elf creature.').
card_mana_cost('eyeblight\'s ending', ['2', 'B']).
card_cmc('eyeblight\'s ending', 3).
card_layout('eyeblight\'s ending', 'normal').

% Found in: BFZ
card_name('eyeless watcher', 'Eyeless Watcher').
card_type('eyeless watcher', 'Creature — Eldrazi Drone').
card_types('eyeless watcher', ['Creature']).
card_subtypes('eyeless watcher', ['Eldrazi', 'Drone']).
card_colors('eyeless watcher', []).
card_text('eyeless watcher', 'Devoid (This card has no color.)\nWhen Eyeless Watcher enters the battlefield, put two 1/1 colorless Eldrazi Scion creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('eyeless watcher', ['3', 'G']).
card_cmc('eyeless watcher', 4).
card_layout('eyeless watcher', 'normal').
card_power('eyeless watcher', 1).
card_toughness('eyeless watcher', 1).

% Found in: RTR
card_name('eyes in the skies', 'Eyes in the Skies').
card_type('eyes in the skies', 'Instant').
card_types('eyes in the skies', ['Instant']).
card_subtypes('eyes in the skies', []).
card_colors('eyes in the skies', ['W']).
card_text('eyes in the skies', 'Put a 1/1 white Bird creature token with flying onto the battlefield, then populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_mana_cost('eyes in the skies', ['3', 'W']).
card_cmc('eyes in the skies', 4).
card_layout('eyes in the skies', 'normal').

% Found in: 5DN
card_name('eyes of the watcher', 'Eyes of the Watcher').
card_type('eyes of the watcher', 'Enchantment').
card_types('eyes of the watcher', ['Enchantment']).
card_subtypes('eyes of the watcher', []).
card_colors('eyes of the watcher', ['U']).
card_text('eyes of the watcher', 'Whenever you cast an instant or sorcery spell, you may pay {1}. If you do, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('eyes of the watcher', ['2', 'U']).
card_cmc('eyes of the watcher', 3).
card_layout('eyes of the watcher', 'normal').

% Found in: LRW
card_name('eyes of the wisent', 'Eyes of the Wisent').
card_type('eyes of the wisent', 'Tribal Enchantment — Elemental').
card_types('eyes of the wisent', ['Tribal', 'Enchantment']).
card_subtypes('eyes of the wisent', ['Elemental']).
card_colors('eyes of the wisent', ['G']).
card_text('eyes of the wisent', 'Whenever an opponent casts a blue spell during your turn, you may put a 4/4 green Elemental creature token onto the battlefield.').
card_mana_cost('eyes of the wisent', ['1', 'G']).
card_cmc('eyes of the wisent', 2).
card_layout('eyes of the wisent', 'normal').

% Found in: SOM
card_name('ezuri\'s archers', 'Ezuri\'s Archers').
card_type('ezuri\'s archers', 'Creature — Elf Archer').
card_types('ezuri\'s archers', ['Creature']).
card_subtypes('ezuri\'s archers', ['Elf', 'Archer']).
card_colors('ezuri\'s archers', ['G']).
card_text('ezuri\'s archers', 'Reach (This creature can block creatures with flying.)\nWhenever Ezuri\'s Archers blocks a creature with flying, Ezuri\'s Archers gets +3/+0 until end of turn.').
card_mana_cost('ezuri\'s archers', ['G']).
card_cmc('ezuri\'s archers', 1).
card_layout('ezuri\'s archers', 'normal').
card_power('ezuri\'s archers', 1).
card_toughness('ezuri\'s archers', 2).

% Found in: SOM
card_name('ezuri\'s brigade', 'Ezuri\'s Brigade').
card_type('ezuri\'s brigade', 'Creature — Elf Warrior').
card_types('ezuri\'s brigade', ['Creature']).
card_subtypes('ezuri\'s brigade', ['Elf', 'Warrior']).
card_colors('ezuri\'s brigade', ['G']).
card_text('ezuri\'s brigade', 'Metalcraft — As long as you control three or more artifacts, Ezuri\'s Brigade gets +4/+4 and has trample.').
card_mana_cost('ezuri\'s brigade', ['2', 'G', 'G']).
card_cmc('ezuri\'s brigade', 4).
card_layout('ezuri\'s brigade', 'normal').
card_power('ezuri\'s brigade', 4).
card_toughness('ezuri\'s brigade', 4).

% Found in: C14, SOM
card_name('ezuri, renegade leader', 'Ezuri, Renegade Leader').
card_type('ezuri, renegade leader', 'Legendary Creature — Elf Warrior').
card_types('ezuri, renegade leader', ['Creature']).
card_subtypes('ezuri, renegade leader', ['Elf', 'Warrior']).
card_supertypes('ezuri, renegade leader', ['Legendary']).
card_colors('ezuri, renegade leader', ['G']).
card_text('ezuri, renegade leader', '{G}: Regenerate another target Elf.\n{2}{G}{G}{G}: Elf creatures you control get +3/+3 and gain trample until end of turn.').
card_mana_cost('ezuri, renegade leader', ['1', 'G', 'G']).
card_cmc('ezuri, renegade leader', 3).
card_layout('ezuri, renegade leader', 'normal').
card_power('ezuri, renegade leader', 2).
card_toughness('ezuri, renegade leader', 2).

