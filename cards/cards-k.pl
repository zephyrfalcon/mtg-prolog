% Card-specific data

% Found in: CM1, CMD
card_name('kaalia of the vast', 'Kaalia of the Vast').
card_type('kaalia of the vast', 'Legendary Creature — Human Cleric').
card_types('kaalia of the vast', ['Creature']).
card_subtypes('kaalia of the vast', ['Human', 'Cleric']).
card_supertypes('kaalia of the vast', ['Legendary']).
card_colors('kaalia of the vast', ['W', 'B', 'R']).
card_text('kaalia of the vast', 'Flying\nWhenever Kaalia of the Vast attacks an opponent, you may put an Angel, Demon, or Dragon creature card from your hand onto the battlefield tapped and attacking that opponent.').
card_mana_cost('kaalia of the vast', ['1', 'W', 'B', 'R']).
card_cmc('kaalia of the vast', 4).
card_layout('kaalia of the vast', 'normal').
card_power('kaalia of the vast', 2).
card_toughness('kaalia of the vast', 2).

% Found in: DDF, ZEN
card_name('kabira crossroads', 'Kabira Crossroads').
card_type('kabira crossroads', 'Land').
card_types('kabira crossroads', ['Land']).
card_subtypes('kabira crossroads', []).
card_colors('kabira crossroads', []).
card_text('kabira crossroads', 'Kabira Crossroads enters the battlefield tapped.\nWhen Kabira Crossroads enters the battlefield, you gain 2 life.\n{T}: Add {W} to your mana pool.').
card_layout('kabira crossroads', 'normal').

% Found in: ZEN
card_name('kabira evangel', 'Kabira Evangel').
card_type('kabira evangel', 'Creature — Human Cleric Ally').
card_types('kabira evangel', ['Creature']).
card_subtypes('kabira evangel', ['Human', 'Cleric', 'Ally']).
card_colors('kabira evangel', ['W']).
card_text('kabira evangel', 'Whenever Kabira Evangel or another Ally enters the battlefield under your control, you may choose a color. If you do, Allies you control gain protection from the chosen color until end of turn.').
card_mana_cost('kabira evangel', ['2', 'W']).
card_cmc('kabira evangel', 3).
card_layout('kabira evangel', 'normal').
card_power('kabira evangel', 2).
card_toughness('kabira evangel', 3).

% Found in: DDG, DDP, ROE
card_name('kabira vindicator', 'Kabira Vindicator').
card_type('kabira vindicator', 'Creature — Human Knight').
card_types('kabira vindicator', ['Creature']).
card_subtypes('kabira vindicator', ['Human', 'Knight']).
card_colors('kabira vindicator', ['W']).
card_text('kabira vindicator', 'Level up {2}{W} ({2}{W}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 2-4\n3/6\nOther creatures you control get +1/+1.\nLEVEL 5+\n4/8\nOther creatures you control get +2/+2.').
card_mana_cost('kabira vindicator', ['3', 'W']).
card_cmc('kabira vindicator', 4).
card_layout('kabira vindicator', 'leveler').
card_power('kabira vindicator', 2).
card_toughness('kabira vindicator', 4).

% Found in: ONS
card_name('kaboom!', 'Kaboom!').
card_type('kaboom!', 'Sorcery').
card_types('kaboom!', ['Sorcery']).
card_subtypes('kaboom!', []).
card_colors('kaboom!', ['R']).
card_text('kaboom!', 'Choose any number of target players. For each of those players, reveal cards from the top of your library until you reveal a nonland card. Kaboom! deals damage equal to that card\'s converted mana cost to that player, then you put the revealed cards on the bottom of your library in any order.').
card_mana_cost('kaboom!', ['4', 'R']).
card_cmc('kaboom!', 5).
card_layout('kaboom!', 'normal').

% Found in: CHK
card_name('kabuto moth', 'Kabuto Moth').
card_type('kabuto moth', 'Creature — Spirit').
card_types('kabuto moth', ['Creature']).
card_subtypes('kabuto moth', ['Spirit']).
card_colors('kabuto moth', ['W']).
card_text('kabuto moth', 'Flying\n{T}: Target creature gets +1/+2 until end of turn.').
card_mana_cost('kabuto moth', ['2', 'W']).
card_cmc('kabuto moth', 3).
card_layout('kabuto moth', 'normal').
card_power('kabuto moth', 1).
card_toughness('kabuto moth', 2).

% Found in: ARC, TSP
card_name('kaervek the merciless', 'Kaervek the Merciless').
card_type('kaervek the merciless', 'Legendary Creature — Human Shaman').
card_types('kaervek the merciless', ['Creature']).
card_subtypes('kaervek the merciless', ['Human', 'Shaman']).
card_supertypes('kaervek the merciless', ['Legendary']).
card_colors('kaervek the merciless', ['B', 'R']).
card_text('kaervek the merciless', 'Whenever an opponent casts a spell, Kaervek the Merciless deals damage to target creature or player equal to that spell\'s converted mana cost.').
card_mana_cost('kaervek the merciless', ['5', 'B', 'R']).
card_cmc('kaervek the merciless', 7).
card_layout('kaervek the merciless', 'normal').
card_power('kaervek the merciless', 5).
card_toughness('kaervek the merciless', 4).

% Found in: MIR
card_name('kaervek\'s hex', 'Kaervek\'s Hex').
card_type('kaervek\'s hex', 'Sorcery').
card_types('kaervek\'s hex', ['Sorcery']).
card_subtypes('kaervek\'s hex', []).
card_colors('kaervek\'s hex', ['B']).
card_text('kaervek\'s hex', 'Kaervek\'s Hex deals 1 damage to each nonblack creature and an additional 1 damage to each green creature.').
card_mana_cost('kaervek\'s hex', ['3', 'B']).
card_cmc('kaervek\'s hex', 4).
card_layout('kaervek\'s hex', 'normal').

% Found in: MIR
card_name('kaervek\'s purge', 'Kaervek\'s Purge').
card_type('kaervek\'s purge', 'Sorcery').
card_types('kaervek\'s purge', ['Sorcery']).
card_subtypes('kaervek\'s purge', []).
card_colors('kaervek\'s purge', ['B', 'R']).
card_text('kaervek\'s purge', 'Destroy target creature with converted mana cost X. If that creature dies this way, Kaervek\'s Purge deals damage equal to the creature\'s power to the creature\'s controller.').
card_mana_cost('kaervek\'s purge', ['X', 'B', 'R']).
card_cmc('kaervek\'s purge', 2).
card_layout('kaervek\'s purge', 'normal').

% Found in: VIS
card_name('kaervek\'s spite', 'Kaervek\'s Spite').
card_type('kaervek\'s spite', 'Instant').
card_types('kaervek\'s spite', ['Instant']).
card_subtypes('kaervek\'s spite', []).
card_colors('kaervek\'s spite', ['B']).
card_text('kaervek\'s spite', 'As an additional cost to cast Kaervek\'s Spite, sacrifice all permanents you control and discard your hand.\nTarget player loses 5 life.').
card_mana_cost('kaervek\'s spite', ['B', 'B', 'B']).
card_cmc('kaervek\'s spite', 3).
card_layout('kaervek\'s spite', 'normal').
card_reserved('kaervek\'s spite').

% Found in: MIR, VMA
card_name('kaervek\'s torch', 'Kaervek\'s Torch').
card_type('kaervek\'s torch', 'Sorcery').
card_types('kaervek\'s torch', ['Sorcery']).
card_subtypes('kaervek\'s torch', []).
card_colors('kaervek\'s torch', ['R']).
card_text('kaervek\'s torch', 'As long as Kaervek\'s Torch is on the stack, spells that target it cost {2} more to cast.\nKaervek\'s Torch deals X damage to target creature or player.').
card_mana_cost('kaervek\'s torch', ['X', 'R']).
card_cmc('kaervek\'s torch', 1).
card_layout('kaervek\'s torch', 'normal').

% Found in: SOK
card_name('kagemaro\'s clutch', 'Kagemaro\'s Clutch').
card_type('kagemaro\'s clutch', 'Enchantment — Aura').
card_types('kagemaro\'s clutch', ['Enchantment']).
card_subtypes('kagemaro\'s clutch', ['Aura']).
card_colors('kagemaro\'s clutch', ['B']).
card_text('kagemaro\'s clutch', 'Enchant creature\nEnchanted creature gets -X/-X, where X is the number of cards in your hand.').
card_mana_cost('kagemaro\'s clutch', ['3', 'B']).
card_cmc('kagemaro\'s clutch', 4).
card_layout('kagemaro\'s clutch', 'normal').

% Found in: SOK
card_name('kagemaro, first to suffer', 'Kagemaro, First to Suffer').
card_type('kagemaro, first to suffer', 'Legendary Creature — Demon Spirit').
card_types('kagemaro, first to suffer', ['Creature']).
card_subtypes('kagemaro, first to suffer', ['Demon', 'Spirit']).
card_supertypes('kagemaro, first to suffer', ['Legendary']).
card_colors('kagemaro, first to suffer', ['B']).
card_text('kagemaro, first to suffer', 'Kagemaro, First to Suffer\'s power and toughness are each equal to the number of cards in your hand.\n{B}, Sacrifice Kagemaro: All creatures get -X/-X until end of turn, where X is the number of cards in your hand.').
card_mana_cost('kagemaro, first to suffer', ['3', 'B', 'B']).
card_cmc('kagemaro, first to suffer', 5).
card_layout('kagemaro, first to suffer', 'normal').
card_power('kagemaro, first to suffer', '*').
card_toughness('kagemaro, first to suffer', '*').

% Found in: SOK
card_name('kaho, minamo historian', 'Kaho, Minamo Historian').
card_type('kaho, minamo historian', 'Legendary Creature — Human Wizard').
card_types('kaho, minamo historian', ['Creature']).
card_subtypes('kaho, minamo historian', ['Human', 'Wizard']).
card_supertypes('kaho, minamo historian', ['Legendary']).
card_colors('kaho, minamo historian', ['U']).
card_text('kaho, minamo historian', 'When Kaho, Minamo Historian enters the battlefield, search your library for up to three instant cards and exile them. Then shuffle your library.\n{X}, {T}: You may cast a card with converted mana cost X exiled with Kaho without paying its mana cost.').
card_mana_cost('kaho, minamo historian', ['2', 'U', 'U']).
card_cmc('kaho, minamo historian', 4).
card_layout('kaho, minamo historian', 'normal').
card_power('kaho, minamo historian', 2).
card_toughness('kaho, minamo historian', 2).

% Found in: BOK
card_name('kaijin of the vanishing touch', 'Kaijin of the Vanishing Touch').
card_type('kaijin of the vanishing touch', 'Creature — Spirit').
card_types('kaijin of the vanishing touch', ['Creature']).
card_subtypes('kaijin of the vanishing touch', ['Spirit']).
card_colors('kaijin of the vanishing touch', ['U']).
card_text('kaijin of the vanishing touch', 'Defender (This creature can\'t attack.)\nWhenever Kaijin of the Vanishing Touch blocks a creature, return that creature to its owner\'s hand at end of combat. (Return it only if it\'s on the battlefield.)').
card_mana_cost('kaijin of the vanishing touch', ['1', 'U']).
card_cmc('kaijin of the vanishing touch', 2).
card_layout('kaijin of the vanishing touch', 'normal').
card_power('kaijin of the vanishing touch', 0).
card_toughness('kaijin of the vanishing touch', 3).

% Found in: BOK
card_name('kaiso, memory of loyalty', 'Kaiso, Memory of Loyalty').
card_type('kaiso, memory of loyalty', 'Legendary Creature — Spirit').
card_types('kaiso, memory of loyalty', ['Creature']).
card_subtypes('kaiso, memory of loyalty', ['Spirit']).
card_supertypes('kaiso, memory of loyalty', ['Legendary']).
card_colors('kaiso, memory of loyalty', ['W']).
card_text('kaiso, memory of loyalty', 'Flying\nRemove a ki counter from Kaiso, Memory of Loyalty: Prevent all damage that would be dealt to target creature this turn.').
card_mana_cost('kaiso, memory of loyalty', ['1', 'W', 'W']).
card_cmc('kaiso, memory of loyalty', 3).
card_layout('kaiso, memory of loyalty', 'flip').
card_power('kaiso, memory of loyalty', 3).
card_toughness('kaiso, memory of loyalty', 4).

% Found in: BFZ
card_name('kalastria healer', 'Kalastria Healer').
card_type('kalastria healer', 'Creature — Vampire Cleric Ally').
card_types('kalastria healer', ['Creature']).
card_subtypes('kalastria healer', ['Vampire', 'Cleric', 'Ally']).
card_colors('kalastria healer', ['B']).
card_text('kalastria healer', 'Rally — Whenever Kalastria Healer or another Ally enters the battlefield under your control, each opponent loses 1 life and you gain 1 life.').
card_mana_cost('kalastria healer', ['1', 'B']).
card_cmc('kalastria healer', 2).
card_layout('kalastria healer', 'normal').
card_power('kalastria healer', 1).
card_toughness('kalastria healer', 2).

% Found in: WWK, pWPN
card_name('kalastria highborn', 'Kalastria Highborn').
card_type('kalastria highborn', 'Creature — Vampire Shaman').
card_types('kalastria highborn', ['Creature']).
card_subtypes('kalastria highborn', ['Vampire', 'Shaman']).
card_colors('kalastria highborn', ['B']).
card_text('kalastria highborn', 'Whenever Kalastria Highborn or another Vampire you control dies, you may pay {B}. If you do, target player loses 2 life and you gain 2 life.').
card_mana_cost('kalastria highborn', ['B', 'B']).
card_cmc('kalastria highborn', 2).
card_layout('kalastria highborn', 'normal').
card_power('kalastria highborn', 2).
card_toughness('kalastria highborn', 2).

% Found in: BFZ
card_name('kalastria nightwatch', 'Kalastria Nightwatch').
card_type('kalastria nightwatch', 'Creature — Vampire Warrior Ally').
card_types('kalastria nightwatch', ['Creature']).
card_subtypes('kalastria nightwatch', ['Vampire', 'Warrior', 'Ally']).
card_colors('kalastria nightwatch', ['B']).
card_text('kalastria nightwatch', 'Whenever you gain life, Kalastria Nightwatch gains flying until end of turn.').
card_mana_cost('kalastria nightwatch', ['4', 'B']).
card_cmc('kalastria nightwatch', 5).
card_layout('kalastria nightwatch', 'normal').
card_power('kalastria nightwatch', 4).
card_toughness('kalastria nightwatch', 5).

% Found in: CON
card_name('kaleidostone', 'Kaleidostone').
card_type('kaleidostone', 'Artifact').
card_types('kaleidostone', ['Artifact']).
card_subtypes('kaleidostone', []).
card_colors('kaleidostone', []).
card_text('kaleidostone', 'When Kaleidostone enters the battlefield, draw a card.\n{5}, {T}, Sacrifice Kaleidostone: Add {W}{U}{B}{R}{G} to your mana pool.').
card_mana_cost('kaleidostone', ['2']).
card_cmc('kaleidostone', 2).
card_layout('kaleidostone', 'normal').

% Found in: ZEN
card_name('kalitas, bloodchief of ghet', 'Kalitas, Bloodchief of Ghet').
card_type('kalitas, bloodchief of ghet', 'Legendary Creature — Vampire Warrior').
card_types('kalitas, bloodchief of ghet', ['Creature']).
card_subtypes('kalitas, bloodchief of ghet', ['Vampire', 'Warrior']).
card_supertypes('kalitas, bloodchief of ghet', ['Legendary']).
card_colors('kalitas, bloodchief of ghet', ['B']).
card_text('kalitas, bloodchief of ghet', '{B}{B}{B}, {T}: Destroy target creature. If that creature dies this way, put a black Vampire creature token onto the battlefield. Its power is equal to that creature\'s power and its toughness is equal to that creature\'s toughness.').
card_mana_cost('kalitas, bloodchief of ghet', ['5', 'B', 'B']).
card_cmc('kalitas, bloodchief of ghet', 7).
card_layout('kalitas, bloodchief of ghet', 'normal').
card_power('kalitas, bloodchief of ghet', 5).
card_toughness('kalitas, bloodchief of ghet', 5).

% Found in: M10
card_name('kalonian behemoth', 'Kalonian Behemoth').
card_type('kalonian behemoth', 'Creature — Beast').
card_types('kalonian behemoth', ['Creature']).
card_subtypes('kalonian behemoth', ['Beast']).
card_colors('kalonian behemoth', ['G']).
card_text('kalonian behemoth', 'Shroud (This creature can\'t be the target of spells or abilities.)').
card_mana_cost('kalonian behemoth', ['5', 'G', 'G']).
card_cmc('kalonian behemoth', 7).
card_layout('kalonian behemoth', 'normal').
card_power('kalonian behemoth', 9).
card_toughness('kalonian behemoth', 9).

% Found in: M14
card_name('kalonian hydra', 'Kalonian Hydra').
card_type('kalonian hydra', 'Creature — Hydra').
card_types('kalonian hydra', ['Creature']).
card_subtypes('kalonian hydra', ['Hydra']).
card_colors('kalonian hydra', ['G']).
card_text('kalonian hydra', 'Trample\nKalonian Hydra enters the battlefield with four +1/+1 counters on it.\nWhenever Kalonian Hydra attacks, double the number of +1/+1 counters on each creature you control.').
card_mana_cost('kalonian hydra', ['3', 'G', 'G']).
card_cmc('kalonian hydra', 5).
card_layout('kalonian hydra', 'normal').
card_power('kalonian hydra', 0).
card_toughness('kalonian hydra', 0).

% Found in: M14
card_name('kalonian tusker', 'Kalonian Tusker').
card_type('kalonian tusker', 'Creature — Beast').
card_types('kalonian tusker', ['Creature']).
card_subtypes('kalonian tusker', ['Beast']).
card_colors('kalonian tusker', ['G']).
card_text('kalonian tusker', '').
card_mana_cost('kalonian tusker', ['G', 'G']).
card_cmc('kalonian tusker', 2).
card_layout('kalonian tusker', 'normal').
card_power('kalonian tusker', 3).
card_toughness('kalonian tusker', 3).

% Found in: M15
card_name('kalonian twingrove', 'Kalonian Twingrove').
card_type('kalonian twingrove', 'Creature — Treefolk Warrior').
card_types('kalonian twingrove', ['Creature']).
card_subtypes('kalonian twingrove', ['Treefolk', 'Warrior']).
card_colors('kalonian twingrove', ['G']).
card_text('kalonian twingrove', 'Kalonian Twingrove\'s power and toughness are each equal to the number of Forests you control.\nWhen Kalonian Twingrove enters the battlefield, put a green Treefolk Warrior creature token onto the battlefield with \"This creature\'s power and toughness are each equal to the number of Forests you control.\"').
card_mana_cost('kalonian twingrove', ['5', 'G']).
card_cmc('kalonian twingrove', 6).
card_layout('kalonian twingrove', 'normal').
card_power('kalonian twingrove', '*').
card_toughness('kalonian twingrove', '*').

% Found in: ODY
card_name('kamahl\'s desire', 'Kamahl\'s Desire').
card_type('kamahl\'s desire', 'Enchantment — Aura').
card_types('kamahl\'s desire', ['Enchantment']).
card_subtypes('kamahl\'s desire', ['Aura']).
card_colors('kamahl\'s desire', ['R']).
card_text('kamahl\'s desire', 'Enchant creature\nEnchanted creature has first strike.\nThreshold — Enchanted creature gets +3/+0 as long as seven or more cards are in your graveyard.').
card_mana_cost('kamahl\'s desire', ['1', 'R']).
card_cmc('kamahl\'s desire', 2).
card_layout('kamahl\'s desire', 'normal').

% Found in: TOR
card_name('kamahl\'s sledge', 'Kamahl\'s Sledge').
card_type('kamahl\'s sledge', 'Sorcery').
card_types('kamahl\'s sledge', ['Sorcery']).
card_subtypes('kamahl\'s sledge', []).
card_colors('kamahl\'s sledge', ['R']).
card_text('kamahl\'s sledge', 'Kamahl\'s Sledge deals 4 damage to target creature.\nThreshold — If seven or more cards are in your graveyard, instead Kamahl\'s Sledge deals 4 damage to that creature and 4 damage to that creature\'s controller.').
card_mana_cost('kamahl\'s sledge', ['5', 'R', 'R']).
card_cmc('kamahl\'s sledge', 7).
card_layout('kamahl\'s sledge', 'normal').

% Found in: ONS
card_name('kamahl\'s summons', 'Kamahl\'s Summons').
card_type('kamahl\'s summons', 'Sorcery').
card_types('kamahl\'s summons', ['Sorcery']).
card_subtypes('kamahl\'s summons', []).
card_colors('kamahl\'s summons', ['G']).
card_text('kamahl\'s summons', 'Each player may reveal any number of creature cards from his or her hand. Then each player puts a 2/2 green Bear creature token onto the battlefield for each card he or she revealed this way.').
card_mana_cost('kamahl\'s summons', ['3', 'G']).
card_cmc('kamahl\'s summons', 4).
card_layout('kamahl\'s summons', 'normal').

% Found in: ARC, ONS
card_name('kamahl, fist of krosa', 'Kamahl, Fist of Krosa').
card_type('kamahl, fist of krosa', 'Legendary Creature — Human Druid').
card_types('kamahl, fist of krosa', ['Creature']).
card_subtypes('kamahl, fist of krosa', ['Human', 'Druid']).
card_supertypes('kamahl, fist of krosa', ['Legendary']).
card_colors('kamahl, fist of krosa', ['G']).
card_text('kamahl, fist of krosa', '{G}: Target land becomes a 1/1 creature until end of turn. It\'s still a land.\n{2}{G}{G}{G}: Creatures you control get +3/+3 and gain trample until end of turn.').
card_mana_cost('kamahl, fist of krosa', ['4', 'G', 'G']).
card_cmc('kamahl, fist of krosa', 6).
card_layout('kamahl, fist of krosa', 'normal').
card_power('kamahl, fist of krosa', 4).
card_toughness('kamahl, fist of krosa', 3).

% Found in: 10E, DDL, DPA, ODY, p15A
card_name('kamahl, pit fighter', 'Kamahl, Pit Fighter').
card_type('kamahl, pit fighter', 'Legendary Creature — Human Barbarian').
card_types('kamahl, pit fighter', ['Creature']).
card_subtypes('kamahl, pit fighter', ['Human', 'Barbarian']).
card_supertypes('kamahl, pit fighter', ['Legendary']).
card_colors('kamahl, pit fighter', ['R']).
card_text('kamahl, pit fighter', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\n{T}: Kamahl, Pit Fighter deals 3 damage to target creature or player.').
card_mana_cost('kamahl, pit fighter', ['4', 'R', 'R']).
card_cmc('kamahl, pit fighter', 6).
card_layout('kamahl, pit fighter', 'normal').
card_power('kamahl, pit fighter', 6).
card_toughness('kamahl, pit fighter', 1).

% Found in: CHK, MM2
card_name('kami of ancient law', 'Kami of Ancient Law').
card_type('kami of ancient law', 'Creature — Spirit').
card_types('kami of ancient law', ['Creature']).
card_subtypes('kami of ancient law', ['Spirit']).
card_colors('kami of ancient law', ['W']).
card_text('kami of ancient law', 'Sacrifice Kami of Ancient Law: Destroy target enchantment.').
card_mana_cost('kami of ancient law', ['1', 'W']).
card_cmc('kami of ancient law', 2).
card_layout('kami of ancient law', 'normal').
card_power('kami of ancient law', 2).
card_toughness('kami of ancient law', 2).

% Found in: SOK
card_name('kami of empty graves', 'Kami of Empty Graves').
card_type('kami of empty graves', 'Creature — Spirit').
card_types('kami of empty graves', ['Creature']).
card_subtypes('kami of empty graves', ['Spirit']).
card_colors('kami of empty graves', ['B']).
card_text('kami of empty graves', 'Soulshift 3 (When this creature dies, you may return target Spirit card with converted mana cost 3 or less from your graveyard to your hand.)').
card_mana_cost('kami of empty graves', ['3', 'B']).
card_cmc('kami of empty graves', 4).
card_layout('kami of empty graves', 'normal').
card_power('kami of empty graves', 4).
card_toughness('kami of empty graves', 1).

% Found in: BOK
card_name('kami of false hope', 'Kami of False Hope').
card_type('kami of false hope', 'Creature — Spirit').
card_types('kami of false hope', ['Creature']).
card_subtypes('kami of false hope', ['Spirit']).
card_colors('kami of false hope', ['W']).
card_text('kami of false hope', 'Sacrifice Kami of False Hope: Prevent all combat damage that would be dealt this turn.').
card_mana_cost('kami of false hope', ['W']).
card_cmc('kami of false hope', 1).
card_layout('kami of false hope', 'normal').
card_power('kami of false hope', 1).
card_toughness('kami of false hope', 1).

% Found in: CHK
card_name('kami of fire\'s roar', 'Kami of Fire\'s Roar').
card_type('kami of fire\'s roar', 'Creature — Spirit').
card_types('kami of fire\'s roar', ['Creature']).
card_subtypes('kami of fire\'s roar', ['Spirit']).
card_colors('kami of fire\'s roar', ['R']).
card_text('kami of fire\'s roar', 'Whenever you cast a Spirit or Arcane spell, target creature can\'t block this turn.').
card_mana_cost('kami of fire\'s roar', ['3', 'R']).
card_cmc('kami of fire\'s roar', 4).
card_layout('kami of fire\'s roar', 'normal').
card_power('kami of fire\'s roar', 2).
card_toughness('kami of fire\'s roar', 3).

% Found in: CHK
card_name('kami of lunacy', 'Kami of Lunacy').
card_type('kami of lunacy', 'Creature — Spirit').
card_types('kami of lunacy', ['Creature']).
card_subtypes('kami of lunacy', ['Spirit']).
card_colors('kami of lunacy', ['B']).
card_text('kami of lunacy', 'Flying\nSoulshift 5 (When this creature dies, you may return target Spirit card with converted mana cost 5 or less from your graveyard to your hand.)').
card_mana_cost('kami of lunacy', ['4', 'B', 'B']).
card_cmc('kami of lunacy', 6).
card_layout('kami of lunacy', 'normal').
card_power('kami of lunacy', 4).
card_toughness('kami of lunacy', 1).

% Found in: 9ED, CHK
card_name('kami of old stone', 'Kami of Old Stone').
card_type('kami of old stone', 'Creature — Spirit').
card_types('kami of old stone', ['Creature']).
card_subtypes('kami of old stone', ['Spirit']).
card_colors('kami of old stone', ['W']).
card_text('kami of old stone', '').
card_mana_cost('kami of old stone', ['3', 'W']).
card_cmc('kami of old stone', 4).
card_layout('kami of old stone', 'normal').
card_power('kami of old stone', 1).
card_toughness('kami of old stone', 7).

% Found in: BOK
card_name('kami of tattered shoji', 'Kami of Tattered Shoji').
card_type('kami of tattered shoji', 'Creature — Spirit').
card_types('kami of tattered shoji', ['Creature']).
card_subtypes('kami of tattered shoji', ['Spirit']).
card_colors('kami of tattered shoji', ['W']).
card_text('kami of tattered shoji', 'Whenever you cast a Spirit or Arcane spell, Kami of Tattered Shoji gains flying until end of turn.').
card_mana_cost('kami of tattered shoji', ['4', 'W']).
card_cmc('kami of tattered shoji', 5).
card_layout('kami of tattered shoji', 'normal').
card_power('kami of tattered shoji', 2).
card_toughness('kami of tattered shoji', 5).

% Found in: SOK
card_name('kami of the crescent moon', 'Kami of the Crescent Moon').
card_type('kami of the crescent moon', 'Legendary Creature — Spirit').
card_types('kami of the crescent moon', ['Creature']).
card_subtypes('kami of the crescent moon', ['Spirit']).
card_supertypes('kami of the crescent moon', ['Legendary']).
card_colors('kami of the crescent moon', ['U']).
card_text('kami of the crescent moon', 'At the beginning of each player\'s draw step, that player draws an additional card.').
card_mana_cost('kami of the crescent moon', ['U', 'U']).
card_cmc('kami of the crescent moon', 2).
card_layout('kami of the crescent moon', 'normal').
card_power('kami of the crescent moon', 1).
card_toughness('kami of the crescent moon', 3).

% Found in: BOK
card_name('kami of the honored dead', 'Kami of the Honored Dead').
card_type('kami of the honored dead', 'Creature — Spirit').
card_types('kami of the honored dead', ['Creature']).
card_subtypes('kami of the honored dead', ['Spirit']).
card_colors('kami of the honored dead', ['W']).
card_text('kami of the honored dead', 'Flying\nWhenever Kami of the Honored Dead is dealt damage, you gain that much life.\nSoulshift 6 (When this creature dies, you may return target Spirit card with converted mana cost 6 or less from your graveyard to your hand.)').
card_mana_cost('kami of the honored dead', ['5', 'W', 'W']).
card_cmc('kami of the honored dead', 7).
card_layout('kami of the honored dead', 'normal').
card_power('kami of the honored dead', 3).
card_toughness('kami of the honored dead', 5).

% Found in: CHK
card_name('kami of the hunt', 'Kami of the Hunt').
card_type('kami of the hunt', 'Creature — Spirit').
card_types('kami of the hunt', ['Creature']).
card_subtypes('kami of the hunt', ['Spirit']).
card_colors('kami of the hunt', ['G']).
card_text('kami of the hunt', 'Whenever you cast a Spirit or Arcane spell, Kami of the Hunt gets +1/+1 until end of turn.').
card_mana_cost('kami of the hunt', ['2', 'G']).
card_cmc('kami of the hunt', 3).
card_layout('kami of the hunt', 'normal').
card_power('kami of the hunt', 2).
card_toughness('kami of the hunt', 2).

% Found in: CHK
card_name('kami of the painted road', 'Kami of the Painted Road').
card_type('kami of the painted road', 'Creature — Spirit').
card_types('kami of the painted road', ['Creature']).
card_subtypes('kami of the painted road', ['Spirit']).
card_colors('kami of the painted road', ['W']).
card_text('kami of the painted road', 'Whenever you cast a Spirit or Arcane spell, Kami of the Painted Road gains protection from the color of your choice until end of turn.').
card_mana_cost('kami of the painted road', ['4', 'W']).
card_cmc('kami of the painted road', 5).
card_layout('kami of the painted road', 'normal').
card_power('kami of the painted road', 3).
card_toughness('kami of the painted road', 3).

% Found in: CHK
card_name('kami of the palace fields', 'Kami of the Palace Fields').
card_type('kami of the palace fields', 'Creature — Spirit').
card_types('kami of the palace fields', ['Creature']).
card_subtypes('kami of the palace fields', ['Spirit']).
card_colors('kami of the palace fields', ['W']).
card_text('kami of the palace fields', 'Flying, first strike\nSoulshift 5 (When this creature dies, you may return target Spirit card with converted mana cost 5 or less from your graveyard to your hand.)').
card_mana_cost('kami of the palace fields', ['5', 'W']).
card_cmc('kami of the palace fields', 6).
card_layout('kami of the palace fields', 'normal').
card_power('kami of the palace fields', 3).
card_toughness('kami of the palace fields', 2).

% Found in: SOK
card_name('kami of the tended garden', 'Kami of the Tended Garden').
card_type('kami of the tended garden', 'Creature — Spirit').
card_types('kami of the tended garden', ['Creature']).
card_subtypes('kami of the tended garden', ['Spirit']).
card_colors('kami of the tended garden', ['G']).
card_text('kami of the tended garden', 'At the beginning of your upkeep, sacrifice Kami of the Tended Garden unless you pay {G}.\nSoulshift 3 (When this creature dies, you may return target Spirit card with converted mana cost 3 or less from your graveyard to your hand.)').
card_mana_cost('kami of the tended garden', ['3', 'G']).
card_cmc('kami of the tended garden', 4).
card_layout('kami of the tended garden', 'normal').
card_power('kami of the tended garden', 4).
card_toughness('kami of the tended garden', 4).

% Found in: CHK
card_name('kami of the waning moon', 'Kami of the Waning Moon').
card_type('kami of the waning moon', 'Creature — Spirit').
card_types('kami of the waning moon', ['Creature']).
card_subtypes('kami of the waning moon', ['Spirit']).
card_colors('kami of the waning moon', ['B']).
card_text('kami of the waning moon', 'Flying\nWhenever you cast a Spirit or Arcane spell, target creature gains fear until end of turn. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('kami of the waning moon', ['2', 'B']).
card_cmc('kami of the waning moon', 3).
card_layout('kami of the waning moon', 'normal').
card_power('kami of the waning moon', 1).
card_toughness('kami of the waning moon', 1).

% Found in: CHK
card_name('kami of twisted reflection', 'Kami of Twisted Reflection').
card_type('kami of twisted reflection', 'Creature — Spirit').
card_types('kami of twisted reflection', ['Creature']).
card_subtypes('kami of twisted reflection', ['Spirit']).
card_colors('kami of twisted reflection', ['U']).
card_text('kami of twisted reflection', 'Sacrifice Kami of Twisted Reflection: Return target creature you control to its owner\'s hand.').
card_mana_cost('kami of twisted reflection', ['1', 'U', 'U']).
card_cmc('kami of twisted reflection', 3).
card_layout('kami of twisted reflection', 'normal').
card_power('kami of twisted reflection', 2).
card_toughness('kami of twisted reflection', 2).

% Found in: INV
card_name('kangee, aerie keeper', 'Kangee, Aerie Keeper').
card_type('kangee, aerie keeper', 'Legendary Creature — Bird Wizard').
card_types('kangee, aerie keeper', ['Creature']).
card_subtypes('kangee, aerie keeper', ['Bird', 'Wizard']).
card_supertypes('kangee, aerie keeper', ['Legendary']).
card_colors('kangee, aerie keeper', ['W', 'U']).
card_text('kangee, aerie keeper', 'Kicker {X}{2} (You may pay an additional {X}{2} as you cast this spell.)\nFlying\nWhen Kangee, Aerie Keeper enters the battlefield, if it was kicked, put X feather counters on it.\nOther Bird creatures get +1/+1 for each feather counter on Kangee, Aerie Keeper.').
card_mana_cost('kangee, aerie keeper', ['2', 'W', 'U']).
card_cmc('kangee, aerie keeper', 4).
card_layout('kangee, aerie keeper', 'normal').
card_power('kangee, aerie keeper', 2).
card_toughness('kangee, aerie keeper', 2).

% Found in: M15
card_name('kapsho kitefins', 'Kapsho Kitefins').
card_type('kapsho kitefins', 'Creature — Fish').
card_types('kapsho kitefins', ['Creature']).
card_subtypes('kapsho kitefins', ['Fish']).
card_colors('kapsho kitefins', ['U']).
card_text('kapsho kitefins', 'Flying\nWhenever Kapsho Kitefins or another creature enters the battlefield under your control, tap target creature an opponent controls.').
card_mana_cost('kapsho kitefins', ['4', 'U', 'U']).
card_cmc('kapsho kitefins', 6).
card_layout('kapsho kitefins', 'normal').
card_power('kapsho kitefins', 3).
card_toughness('kapsho kitefins', 3).

% Found in: CMD, pJGP
card_name('karador, ghost chieftain', 'Karador, Ghost Chieftain').
card_type('karador, ghost chieftain', 'Legendary Creature — Centaur Spirit').
card_types('karador, ghost chieftain', ['Creature']).
card_subtypes('karador, ghost chieftain', ['Centaur', 'Spirit']).
card_supertypes('karador, ghost chieftain', ['Legendary']).
card_colors('karador, ghost chieftain', ['W', 'B', 'G']).
card_text('karador, ghost chieftain', 'Karador, Ghost Chieftain costs {1} less to cast for each creature card in your graveyard.\nDuring each of your turns, you may cast one creature card from your graveyard.').
card_mana_cost('karador, ghost chieftain', ['5', 'B', 'G', 'W']).
card_cmc('karador, ghost chieftain', 8).
card_layout('karador, ghost chieftain', 'normal').
card_power('karador, ghost chieftain', 3).
card_toughness('karador, ghost chieftain', 4).

% Found in: LEG, ME3, pJGP
card_name('karakas', 'Karakas').
card_type('karakas', 'Legendary Land').
card_types('karakas', ['Land']).
card_subtypes('karakas', []).
card_supertypes('karakas', ['Legendary']).
card_colors('karakas', []).
card_text('karakas', '{T}: Add {W} to your mana pool.\n{T}: Return target legendary creature to its owner\'s hand.').
card_layout('karakas', 'normal').

% Found in: THS, pMEI
card_name('karametra\'s acolyte', 'Karametra\'s Acolyte').
card_type('karametra\'s acolyte', 'Creature — Human Druid').
card_types('karametra\'s acolyte', ['Creature']).
card_subtypes('karametra\'s acolyte', ['Human', 'Druid']).
card_colors('karametra\'s acolyte', ['G']).
card_text('karametra\'s acolyte', '{T}: Add an amount of {G} to your mana pool equal to your devotion to green. (Each {G} in the mana costs of permanents you control counts toward your devotion to green.)').
card_mana_cost('karametra\'s acolyte', ['3', 'G']).
card_cmc('karametra\'s acolyte', 4).
card_layout('karametra\'s acolyte', 'normal').
card_power('karametra\'s acolyte', 1).
card_toughness('karametra\'s acolyte', 4).

% Found in: BNG
card_name('karametra\'s favor', 'Karametra\'s Favor').
card_type('karametra\'s favor', 'Enchantment — Aura').
card_types('karametra\'s favor', ['Enchantment']).
card_subtypes('karametra\'s favor', ['Aura']).
card_colors('karametra\'s favor', ['G']).
card_text('karametra\'s favor', 'Enchant creature\nWhen Karametra\'s Favor enters the battlefield, draw a card.\nEnchanted creature has \"{T}: Add one mana of any color to your mana pool.\"').
card_mana_cost('karametra\'s favor', ['1', 'G']).
card_cmc('karametra\'s favor', 2).
card_layout('karametra\'s favor', 'normal').

% Found in: BNG
card_name('karametra, god of harvests', 'Karametra, God of Harvests').
card_type('karametra, god of harvests', 'Legendary Enchantment Creature — God').
card_types('karametra, god of harvests', ['Enchantment', 'Creature']).
card_subtypes('karametra, god of harvests', ['God']).
card_supertypes('karametra, god of harvests', ['Legendary']).
card_colors('karametra, god of harvests', ['W', 'G']).
card_text('karametra, god of harvests', 'Indestructible\nAs long as your devotion to green and white is less than seven, Karametra isn\'t a creature.\nWhenever you cast a creature spell, you may search your library for a Forest or Plains card, put it onto the battlefield tapped, then shuffle your library.').
card_mana_cost('karametra, god of harvests', ['3', 'G', 'W']).
card_cmc('karametra, god of harvests', 5).
card_layout('karametra, god of harvests', 'normal').
card_power('karametra, god of harvests', 6).
card_toughness('karametra, god of harvests', 7).

% Found in: ROE
card_name('kargan dragonlord', 'Kargan Dragonlord').
card_type('kargan dragonlord', 'Creature — Human Warrior').
card_types('kargan dragonlord', ['Creature']).
card_subtypes('kargan dragonlord', ['Human', 'Warrior']).
card_colors('kargan dragonlord', ['R']).
card_text('kargan dragonlord', 'Level up {R} ({R}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 4-7\n4/4\nFlying\nLEVEL 8+\n8/8\nFlying, trample\n{R}: Kargan Dragonlord gets +1/+0 until end of turn.').
card_mana_cost('kargan dragonlord', ['R', 'R']).
card_cmc('kargan dragonlord', 2).
card_layout('kargan dragonlord', 'leveler').
card_power('kargan dragonlord', 2).
card_toughness('kargan dragonlord', 2).

% Found in: 2ED, 3ED, 4ED, 5ED, 8ED, CED, CEI, LEA, LEB
card_name('karma', 'Karma').
card_type('karma', 'Enchantment').
card_types('karma', ['Enchantment']).
card_subtypes('karma', []).
card_colors('karma', ['W']).
card_text('karma', 'At the beginning of each player\'s upkeep, Karma deals damage to that player equal to the number of Swamps he or she controls.').
card_mana_cost('karma', ['2', 'W', 'W']).
card_cmc('karma', 4).
card_layout('karma', 'normal').

% Found in: C13, ULG, VMA, pJGP
card_name('karmic guide', 'Karmic Guide').
card_type('karmic guide', 'Creature — Angel Spirit').
card_types('karmic guide', ['Creature']).
card_subtypes('karmic guide', ['Angel', 'Spirit']).
card_colors('karmic guide', ['W']).
card_text('karmic guide', 'Flying, protection from black\nEcho {3}{W}{W} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Karmic Guide enters the battlefield, return target creature card from your graveyard to the battlefield.').
card_mana_cost('karmic guide', ['3', 'W', 'W']).
card_cmc('karmic guide', 5).
card_layout('karmic guide', 'normal').
card_power('karmic guide', 2).
card_toughness('karmic guide', 2).

% Found in: ODY
card_name('karmic justice', 'Karmic Justice').
card_type('karmic justice', 'Enchantment').
card_types('karmic justice', ['Enchantment']).
card_subtypes('karmic justice', []).
card_colors('karmic justice', ['W']).
card_text('karmic justice', 'Whenever a spell or ability an opponent controls destroys a noncreature permanent you control, you may destroy target permanent that opponent controls.').
card_mana_cost('karmic justice', ['2', 'W']).
card_cmc('karmic justice', 3).
card_layout('karmic justice', 'normal').

% Found in: VAN
card_name('karn', 'Karn').
card_type('karn', 'Vanguard').
card_types('karn', ['Vanguard']).
card_subtypes('karn', []).
card_colors('karn', []).
card_text('karn', 'Each noncreature artifact you control is an artifact creature with power and toughness each equal to its converted mana cost.').
card_layout('karn', 'vanguard').

% Found in: MM2, NPH
card_name('karn liberated', 'Karn Liberated').
card_type('karn liberated', 'Planeswalker — Karn').
card_types('karn liberated', ['Planeswalker']).
card_subtypes('karn liberated', ['Karn']).
card_colors('karn liberated', []).
card_text('karn liberated', '+4: Target player exiles a card from his or her hand.\n−3: Exile target permanent.\n−14: Restart the game, leaving in exile all non-Aura permanent cards exiled with Karn Liberated. Then put those cards onto the battlefield under your control.').
card_mana_cost('karn liberated', ['7']).
card_cmc('karn liberated', 7).
card_layout('karn liberated', 'normal').
card_loyalty('karn liberated', 6).

% Found in: MMQ
card_name('karn\'s touch', 'Karn\'s Touch').
card_type('karn\'s touch', 'Instant').
card_types('karn\'s touch', ['Instant']).
card_subtypes('karn\'s touch', []).
card_colors('karn\'s touch', ['U']).
card_text('karn\'s touch', 'Target noncreature artifact becomes an artifact creature with power and toughness each equal to its converted mana cost until end of turn. (It retains its abilities.)').
card_mana_cost('karn\'s touch', ['U', 'U']).
card_cmc('karn\'s touch', 2).
card_layout('karn\'s touch', 'normal').

% Found in: USG, V10, VMA, pARL
card_name('karn, silver golem', 'Karn, Silver Golem').
card_type('karn, silver golem', 'Legendary Artifact Creature — Golem').
card_types('karn, silver golem', ['Artifact', 'Creature']).
card_subtypes('karn, silver golem', ['Golem']).
card_supertypes('karn, silver golem', ['Legendary']).
card_colors('karn, silver golem', []).
card_text('karn, silver golem', 'Whenever Karn, Silver Golem blocks or becomes blocked, it gets -4/+4 until end of turn.\n{1}: Target noncreature artifact becomes an artifact creature with power and toughness each equal to its converted mana cost until end of turn.').
card_mana_cost('karn, silver golem', ['5']).
card_cmc('karn, silver golem', 5).
card_layout('karn, silver golem', 'normal').
card_power('karn, silver golem', 4).
card_toughness('karn, silver golem', 4).
card_reserved('karn, silver golem').

% Found in: SCG
card_name('karona\'s zealot', 'Karona\'s Zealot').
card_type('karona\'s zealot', 'Creature — Human Cleric').
card_types('karona\'s zealot', ['Creature']).
card_subtypes('karona\'s zealot', ['Human', 'Cleric']).
card_colors('karona\'s zealot', ['W']).
card_text('karona\'s zealot', 'Morph {3}{W}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Karona\'s Zealot is turned face up, all damage that would be dealt to it this turn is dealt to target creature instead.').
card_mana_cost('karona\'s zealot', ['4', 'W']).
card_cmc('karona\'s zealot', 5).
card_layout('karona\'s zealot', 'normal').
card_power('karona\'s zealot', 2).
card_toughness('karona\'s zealot', 5).

% Found in: SCG
card_name('karona, false god', 'Karona, False God').
card_type('karona, false god', 'Legendary Creature — Avatar').
card_types('karona, false god', ['Creature']).
card_subtypes('karona, false god', ['Avatar']).
card_supertypes('karona, false god', ['Legendary']).
card_colors('karona, false god', ['W', 'U', 'B', 'R', 'G']).
card_text('karona, false god', 'Haste\nAt the beginning of each player\'s upkeep, that player untaps Karona, False God and gains control of it.\nWhenever Karona attacks, creatures of the creature type of your choice get +3/+3 until end of turn.').
card_mana_cost('karona, false god', ['1', 'W', 'U', 'B', 'R', 'G']).
card_cmc('karona, false god', 6).
card_layout('karona, false god', 'normal').
card_power('karona, false god', 5).
card_toughness('karona, false god', 5).

% Found in: VAN
card_name('karona, false god avatar', 'Karona, False God Avatar').
card_type('karona, false god avatar', 'Vanguard').
card_types('karona, false god avatar', ['Vanguard']).
card_subtypes('karona, false god avatar', []).
card_colors('karona, false god avatar', []).
card_text('karona, false god avatar', 'At the beginning of your upkeep, exchange control of a permanent you control chosen at random and a permanent target opponent controls chosen at random.').
card_layout('karona, false god avatar', 'vanguard').

% Found in: C14, VIS
card_name('karoo', 'Karoo').
card_type('karoo', 'Land').
card_types('karoo', ['Land']).
card_subtypes('karoo', []).
card_colors('karoo', []).
card_text('karoo', 'Karoo enters the battlefield tapped.\nWhen Karoo enters the battlefield, sacrifice it unless you return an untapped Plains you control to its owner\'s hand.\n{T}: Add {1}{W} to your mana pool.').
card_layout('karoo', 'normal').

% Found in: MIR
card_name('karoo meerkat', 'Karoo Meerkat').
card_type('karoo meerkat', 'Creature — Mongoose').
card_types('karoo meerkat', ['Creature']).
card_subtypes('karoo meerkat', ['Mongoose']).
card_colors('karoo meerkat', ['G']).
card_text('karoo meerkat', 'Protection from blue').
card_mana_cost('karoo meerkat', ['1', 'G']).
card_cmc('karoo meerkat', 2).
card_layout('karoo meerkat', 'normal').
card_power('karoo meerkat', 2).
card_toughness('karoo meerkat', 1).

% Found in: 10E, 5ED, 6ED, 7ED, 9ED, DKM, ICE
card_name('karplusan forest', 'Karplusan Forest').
card_type('karplusan forest', 'Land').
card_types('karplusan forest', ['Land']).
card_subtypes('karplusan forest', []).
card_colors('karplusan forest', []).
card_text('karplusan forest', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Karplusan Forest deals 1 damage to you.').
card_layout('karplusan forest', 'normal').

% Found in: ICE, ME2
card_name('karplusan giant', 'Karplusan Giant').
card_type('karplusan giant', 'Creature — Giant').
card_types('karplusan giant', ['Creature']).
card_subtypes('karplusan giant', ['Giant']).
card_colors('karplusan giant', ['R']).
card_text('karplusan giant', 'Tap an untapped snow land you control: Karplusan Giant gets +1/+1 until end of turn.').
card_mana_cost('karplusan giant', ['6', 'R']).
card_cmc('karplusan giant', 7).
card_layout('karplusan giant', 'normal').
card_power('karplusan giant', 3).
card_toughness('karplusan giant', 3).

% Found in: CSP
card_name('karplusan minotaur', 'Karplusan Minotaur').
card_type('karplusan minotaur', 'Creature — Minotaur Warrior').
card_types('karplusan minotaur', ['Creature']).
card_subtypes('karplusan minotaur', ['Minotaur', 'Warrior']).
card_colors('karplusan minotaur', ['R']).
card_text('karplusan minotaur', 'Cumulative upkeep—Flip a coin. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhenever you win a coin flip, Karplusan Minotaur deals 1 damage to target creature or player.\nWhenever you lose a coin flip, Karplusan Minotaur deals 1 damage to target creature or player of an opponent\'s choice.').
card_mana_cost('karplusan minotaur', ['2', 'R', 'R']).
card_cmc('karplusan minotaur', 4).
card_layout('karplusan minotaur', 'normal').
card_power('karplusan minotaur', 3).
card_toughness('karplusan minotaur', 3).

% Found in: 10E, CSP, MM2
card_name('karplusan strider', 'Karplusan Strider').
card_type('karplusan strider', 'Creature — Yeti').
card_types('karplusan strider', ['Creature']).
card_subtypes('karplusan strider', ['Yeti']).
card_colors('karplusan strider', ['G']).
card_text('karplusan strider', 'Karplusan Strider can\'t be the target of blue or black spells.').
card_mana_cost('karplusan strider', ['3', 'G']).
card_cmc('karplusan strider', 4).
card_layout('karplusan strider', 'normal').
card_power('karplusan strider', 3).
card_toughness('karplusan strider', 4).

% Found in: CSP
card_name('karplusan wolverine', 'Karplusan Wolverine').
card_type('karplusan wolverine', 'Snow Creature — Wolverine Beast').
card_types('karplusan wolverine', ['Creature']).
card_subtypes('karplusan wolverine', ['Wolverine', 'Beast']).
card_supertypes('karplusan wolverine', ['Snow']).
card_colors('karplusan wolverine', ['R']).
card_text('karplusan wolverine', 'Whenever Karplusan Wolverine becomes blocked, you may have it deal 1 damage to target creature or player.').
card_mana_cost('karplusan wolverine', ['R']).
card_cmc('karplusan wolverine', 1).
card_layout('karplusan wolverine', 'normal').
card_power('karplusan wolverine', 1).
card_toughness('karplusan wolverine', 1).

% Found in: 9ED, ICE
card_name('karplusan yeti', 'Karplusan Yeti').
card_type('karplusan yeti', 'Creature — Yeti').
card_types('karplusan yeti', ['Creature']).
card_subtypes('karplusan yeti', ['Yeti']).
card_colors('karplusan yeti', ['R']).
card_text('karplusan yeti', '{T}: Karplusan Yeti deals damage equal to its power to target creature. That creature deals damage equal to its power to Karplusan Yeti.').
card_mana_cost('karplusan yeti', ['3', 'R', 'R']).
card_cmc('karplusan yeti', 5).
card_layout('karplusan yeti', 'normal').
card_power('karplusan yeti', 3).
card_toughness('karplusan yeti', 3).

% Found in: ARB
card_name('karrthus, tyrant of jund', 'Karrthus, Tyrant of Jund').
card_type('karrthus, tyrant of jund', 'Legendary Creature — Dragon').
card_types('karrthus, tyrant of jund', ['Creature']).
card_subtypes('karrthus, tyrant of jund', ['Dragon']).
card_supertypes('karrthus, tyrant of jund', ['Legendary']).
card_colors('karrthus, tyrant of jund', ['B', 'R', 'G']).
card_text('karrthus, tyrant of jund', 'Flying, haste\nWhen Karrthus, Tyrant of Jund enters the battlefield, gain control of all Dragons, then untap all Dragons.\nOther Dragon creatures you control have haste.').
card_mana_cost('karrthus, tyrant of jund', ['4', 'B', 'R', 'G']).
card_cmc('karrthus, tyrant of jund', 7).
card_layout('karrthus, tyrant of jund', 'normal').
card_power('karrthus, tyrant of jund', 7).
card_toughness('karrthus, tyrant of jund', 7).

% Found in: DST
card_name('karstoderm', 'Karstoderm').
card_type('karstoderm', 'Creature — Beast').
card_types('karstoderm', ['Creature']).
card_subtypes('karstoderm', ['Beast']).
card_colors('karstoderm', ['G']).
card_text('karstoderm', 'Karstoderm enters the battlefield with five +1/+1 counters on it.\nWhenever an artifact enters the battlefield, remove a +1/+1 counter from Karstoderm.').
card_mana_cost('karstoderm', ['2', 'G', 'G']).
card_cmc('karstoderm', 4).
card_layout('karstoderm', 'normal').
card_power('karstoderm', 0).
card_toughness('karstoderm', 0).

% Found in: SOK
card_name('kashi-tribe elite', 'Kashi-Tribe Elite').
card_type('kashi-tribe elite', 'Creature — Snake Warrior').
card_types('kashi-tribe elite', ['Creature']).
card_subtypes('kashi-tribe elite', ['Snake', 'Warrior']).
card_colors('kashi-tribe elite', ['G']).
card_text('kashi-tribe elite', 'Legendary Snakes you control have shroud. (They can\'t be the targets of spells or abilities.)\nWhenever Kashi-Tribe Elite deals combat damage to a creature, tap that creature and it doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('kashi-tribe elite', ['1', 'G', 'G']).
card_cmc('kashi-tribe elite', 3).
card_layout('kashi-tribe elite', 'normal').
card_power('kashi-tribe elite', 2).
card_toughness('kashi-tribe elite', 3).

% Found in: CHK
card_name('kashi-tribe reaver', 'Kashi-Tribe Reaver').
card_type('kashi-tribe reaver', 'Creature — Snake Warrior').
card_types('kashi-tribe reaver', ['Creature']).
card_subtypes('kashi-tribe reaver', ['Snake', 'Warrior']).
card_colors('kashi-tribe reaver', ['G']).
card_text('kashi-tribe reaver', 'Whenever Kashi-Tribe Reaver deals combat damage to a creature, tap that creature and it doesn\'t untap during its controller\'s next untap step.\n{1}{G}: Regenerate Kashi-Tribe Reaver.').
card_mana_cost('kashi-tribe reaver', ['3', 'G']).
card_cmc('kashi-tribe reaver', 4).
card_layout('kashi-tribe reaver', 'normal').
card_power('kashi-tribe reaver', 3).
card_toughness('kashi-tribe reaver', 2).

% Found in: CHK
card_name('kashi-tribe warriors', 'Kashi-Tribe Warriors').
card_type('kashi-tribe warriors', 'Creature — Snake Warrior').
card_types('kashi-tribe warriors', ['Creature']).
card_subtypes('kashi-tribe warriors', ['Snake', 'Warrior']).
card_colors('kashi-tribe warriors', ['G']).
card_text('kashi-tribe warriors', 'Whenever Kashi-Tribe Warriors deals combat damage to a creature, tap that creature and it doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('kashi-tribe warriors', ['3', 'G', 'G']).
card_cmc('kashi-tribe warriors', 5).
card_layout('kashi-tribe warriors', 'normal').
card_power('kashi-tribe warriors', 2).
card_toughness('kashi-tribe warriors', 4).

% Found in: LEG
card_name('kasimir the lone wolf', 'Kasimir the Lone Wolf').
card_type('kasimir the lone wolf', 'Legendary Creature — Human Warrior').
card_types('kasimir the lone wolf', ['Creature']).
card_subtypes('kasimir the lone wolf', ['Human', 'Warrior']).
card_supertypes('kasimir the lone wolf', ['Legendary']).
card_colors('kasimir the lone wolf', ['W', 'U']).
card_text('kasimir the lone wolf', '').
card_mana_cost('kasimir the lone wolf', ['4', 'W', 'U']).
card_cmc('kasimir the lone wolf', 6).
card_layout('kasimir the lone wolf', 'normal').
card_power('kasimir the lone wolf', 5).
card_toughness('kasimir the lone wolf', 3).

% Found in: VIS
card_name('katabatic winds', 'Katabatic Winds').
card_type('katabatic winds', 'Enchantment').
card_types('katabatic winds', ['Enchantment']).
card_subtypes('katabatic winds', []).
card_colors('katabatic winds', ['G']).
card_text('katabatic winds', 'Phasing (This phases in or out before you untap during each of your untap steps. While it\'s phased out, it\'s treated as though it doesn\'t exist.)\nCreatures with flying can\'t attack or block, and their activated abilities with {T} in their costs can\'t be activated.').
card_mana_cost('katabatic winds', ['2', 'G']).
card_cmc('katabatic winds', 3).
card_layout('katabatic winds', 'normal').
card_reserved('katabatic winds').

% Found in: MD1, MMA, SOK
card_name('kataki, war\'s wage', 'Kataki, War\'s Wage').
card_type('kataki, war\'s wage', 'Legendary Creature — Spirit').
card_types('kataki, war\'s wage', ['Creature']).
card_subtypes('kataki, war\'s wage', ['Spirit']).
card_supertypes('kataki, war\'s wage', ['Legendary']).
card_colors('kataki, war\'s wage', ['W']).
card_text('kataki, war\'s wage', 'All artifacts have \"At the beginning of your upkeep, sacrifice this artifact unless you pay {1}.\"').
card_mana_cost('kataki, war\'s wage', ['1', 'W']).
card_cmc('kataki, war\'s wage', 2).
card_layout('kataki, war\'s wage', 'normal').
card_power('kataki, war\'s wage', 2).
card_toughness('kataki, war\'s wage', 1).

% Found in: ARB, DDN
card_name('kathari bomber', 'Kathari Bomber').
card_type('kathari bomber', 'Creature — Bird Shaman').
card_types('kathari bomber', ['Creature']).
card_subtypes('kathari bomber', ['Bird', 'Shaman']).
card_colors('kathari bomber', ['B', 'R']).
card_text('kathari bomber', 'Flying\nWhen Kathari Bomber deals combat damage to a player, put two 1/1 red Goblin creature tokens onto the battlefield and sacrifice Kathari Bomber.\nUnearth {3}{B}{R} ({3}{B}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('kathari bomber', ['1', 'B', 'R']).
card_cmc('kathari bomber', 3).
card_layout('kathari bomber', 'normal').
card_power('kathari bomber', 2).
card_toughness('kathari bomber', 2).

% Found in: ARB, PC2
card_name('kathari remnant', 'Kathari Remnant').
card_type('kathari remnant', 'Creature — Bird Skeleton').
card_types('kathari remnant', ['Creature']).
card_subtypes('kathari remnant', ['Bird', 'Skeleton']).
card_colors('kathari remnant', ['U', 'B']).
card_text('kathari remnant', 'Flying\n{B}: Regenerate Kathari Remnant.\nCascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)').
card_mana_cost('kathari remnant', ['2', 'U', 'B']).
card_cmc('kathari remnant', 4).
card_layout('kathari remnant', 'normal').
card_power('kathari remnant', 0).
card_toughness('kathari remnant', 1).

% Found in: ALA
card_name('kathari screecher', 'Kathari Screecher').
card_type('kathari screecher', 'Creature — Bird Soldier').
card_types('kathari screecher', ['Creature']).
card_subtypes('kathari screecher', ['Bird', 'Soldier']).
card_colors('kathari screecher', ['U']).
card_text('kathari screecher', 'Flying\nUnearth {2}{U} ({2}{U}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('kathari screecher', ['2', 'U']).
card_cmc('kathari screecher', 3).
card_layout('kathari screecher', 'normal').
card_power('kathari screecher', 2).
card_toughness('kathari screecher', 2).

% Found in: INV
card_name('kavu aggressor', 'Kavu Aggressor').
card_type('kavu aggressor', 'Creature — Kavu').
card_types('kavu aggressor', ['Creature']).
card_subtypes('kavu aggressor', ['Kavu']).
card_colors('kavu aggressor', ['R']).
card_text('kavu aggressor', 'Kicker {4} (You may pay an additional {4} as you cast this spell.)\nKavu Aggressor can\'t block.\nIf Kavu Aggressor was kicked, it enters the battlefield with a +1/+1 counter on it.').
card_mana_cost('kavu aggressor', ['2', 'R']).
card_cmc('kavu aggressor', 3).
card_layout('kavu aggressor', 'normal').
card_power('kavu aggressor', 3).
card_toughness('kavu aggressor', 2).

% Found in: INV
card_name('kavu chameleon', 'Kavu Chameleon').
card_type('kavu chameleon', 'Creature — Kavu').
card_types('kavu chameleon', ['Creature']).
card_subtypes('kavu chameleon', ['Kavu']).
card_colors('kavu chameleon', ['G']).
card_text('kavu chameleon', 'Kavu Chameleon can\'t be countered.\n{G}: Kavu Chameleon becomes the color of your choice until end of turn.').
card_mana_cost('kavu chameleon', ['3', 'G', 'G']).
card_cmc('kavu chameleon', 5).
card_layout('kavu chameleon', 'normal').
card_power('kavu chameleon', 4).
card_toughness('kavu chameleon', 4).

% Found in: 10E, 9ED, INV
card_name('kavu climber', 'Kavu Climber').
card_type('kavu climber', 'Creature — Kavu').
card_types('kavu climber', ['Creature']).
card_subtypes('kavu climber', ['Kavu']).
card_colors('kavu climber', ['G']).
card_text('kavu climber', 'When Kavu Climber enters the battlefield, draw a card.').
card_mana_cost('kavu climber', ['3', 'G', 'G']).
card_cmc('kavu climber', 5).
card_layout('kavu climber', 'normal').
card_power('kavu climber', 3).
card_toughness('kavu climber', 3).

% Found in: APC
card_name('kavu glider', 'Kavu Glider').
card_type('kavu glider', 'Creature — Kavu').
card_types('kavu glider', ['Creature']).
card_subtypes('kavu glider', ['Kavu']).
card_colors('kavu glider', ['R']).
card_text('kavu glider', '{W}: Kavu Glider gets +0/+1 until end of turn.\n{U}: Kavu Glider gains flying until end of turn.').
card_mana_cost('kavu glider', ['2', 'R']).
card_cmc('kavu glider', 3).
card_layout('kavu glider', 'normal').
card_power('kavu glider', 2).
card_toughness('kavu glider', 1).

% Found in: APC
card_name('kavu howler', 'Kavu Howler').
card_type('kavu howler', 'Creature — Kavu').
card_types('kavu howler', ['Creature']).
card_subtypes('kavu howler', ['Kavu']).
card_colors('kavu howler', ['G']).
card_text('kavu howler', 'When Kavu Howler enters the battlefield, reveal the top four cards of your library. Put all Kavu cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('kavu howler', ['4', 'G', 'G']).
card_cmc('kavu howler', 6).
card_layout('kavu howler', 'normal').
card_power('kavu howler', 4).
card_toughness('kavu howler', 5).

% Found in: INV
card_name('kavu lair', 'Kavu Lair').
card_type('kavu lair', 'Enchantment').
card_types('kavu lair', ['Enchantment']).
card_subtypes('kavu lair', []).
card_colors('kavu lair', ['G']).
card_text('kavu lair', 'Whenever a creature with power 4 or greater enters the battlefield, its controller draws a card.').
card_mana_cost('kavu lair', ['2', 'G']).
card_cmc('kavu lair', 3).
card_layout('kavu lair', 'normal').

% Found in: APC
card_name('kavu mauler', 'Kavu Mauler').
card_type('kavu mauler', 'Creature — Kavu').
card_types('kavu mauler', ['Creature']).
card_subtypes('kavu mauler', ['Kavu']).
card_colors('kavu mauler', ['G']).
card_text('kavu mauler', 'Trample\nWhenever Kavu Mauler attacks, it gets +1/+1 until end of turn for each other attacking Kavu.').
card_mana_cost('kavu mauler', ['4', 'G', 'G']).
card_cmc('kavu mauler', 6).
card_layout('kavu mauler', 'normal').
card_power('kavu mauler', 4).
card_toughness('kavu mauler', 4).

% Found in: INV
card_name('kavu monarch', 'Kavu Monarch').
card_type('kavu monarch', 'Creature — Kavu').
card_types('kavu monarch', ['Creature']).
card_subtypes('kavu monarch', ['Kavu']).
card_colors('kavu monarch', ['R']).
card_text('kavu monarch', 'Kavu creatures have trample.\nWhenever another Kavu enters the battlefield, put a +1/+1 counter on Kavu Monarch.').
card_mana_cost('kavu monarch', ['2', 'R', 'R']).
card_cmc('kavu monarch', 4).
card_layout('kavu monarch', 'normal').
card_power('kavu monarch', 3).
card_toughness('kavu monarch', 3).

% Found in: DDL, PLC
card_name('kavu predator', 'Kavu Predator').
card_type('kavu predator', 'Creature — Kavu').
card_types('kavu predator', ['Creature']).
card_subtypes('kavu predator', ['Kavu']).
card_colors('kavu predator', ['G']).
card_text('kavu predator', 'Trample\nWhenever an opponent gains life, put that many +1/+1 counters on Kavu Predator.').
card_mana_cost('kavu predator', ['1', 'G']).
card_cmc('kavu predator', 2).
card_layout('kavu predator', 'normal').
card_power('kavu predator', 2).
card_toughness('kavu predator', 2).

% Found in: FUT, MM2
card_name('kavu primarch', 'Kavu Primarch').
card_type('kavu primarch', 'Creature — Kavu').
card_types('kavu primarch', ['Creature']).
card_subtypes('kavu primarch', ['Kavu']).
card_colors('kavu primarch', ['G']).
card_text('kavu primarch', 'Kicker {4} (You may pay an additional {4} as you cast this spell.)\nConvoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nIf Kavu Primarch was kicked, it enters the battlefield with four +1/+1 counters on it.').
card_mana_cost('kavu primarch', ['3', 'G']).
card_cmc('kavu primarch', 4).
card_layout('kavu primarch', 'normal').
card_power('kavu primarch', 3).
card_toughness('kavu primarch', 3).

% Found in: PLS
card_name('kavu recluse', 'Kavu Recluse').
card_type('kavu recluse', 'Creature — Kavu').
card_types('kavu recluse', ['Creature']).
card_subtypes('kavu recluse', ['Kavu']).
card_colors('kavu recluse', ['R']).
card_text('kavu recluse', '{T}: Target land becomes a Forest until end of turn.').
card_mana_cost('kavu recluse', ['2', 'R']).
card_cmc('kavu recluse', 3).
card_layout('kavu recluse', 'normal').
card_power('kavu recluse', 2).
card_toughness('kavu recluse', 2).

% Found in: INV
card_name('kavu runner', 'Kavu Runner').
card_type('kavu runner', 'Creature — Kavu').
card_types('kavu runner', ['Creature']).
card_subtypes('kavu runner', ['Kavu']).
card_colors('kavu runner', ['R']).
card_text('kavu runner', 'Kavu Runner has haste as long as no opponent controls a white or blue creature.').
card_mana_cost('kavu runner', ['3', 'R']).
card_cmc('kavu runner', 4).
card_layout('kavu runner', 'normal').
card_power('kavu runner', 3).
card_toughness('kavu runner', 3).

% Found in: INV
card_name('kavu scout', 'Kavu Scout').
card_type('kavu scout', 'Creature — Kavu Scout').
card_types('kavu scout', ['Creature']).
card_subtypes('kavu scout', ['Kavu', 'Scout']).
card_colors('kavu scout', ['R']).
card_text('kavu scout', 'Domain — Kavu Scout gets +1/+0 for each basic land type among lands you control.').
card_mana_cost('kavu scout', ['2', 'R']).
card_cmc('kavu scout', 3).
card_layout('kavu scout', 'normal').
card_power('kavu scout', 0).
card_toughness('kavu scout', 2).

% Found in: INV
card_name('kavu titan', 'Kavu Titan').
card_type('kavu titan', 'Creature — Kavu').
card_types('kavu titan', ['Creature']).
card_subtypes('kavu titan', ['Kavu']).
card_colors('kavu titan', ['G']).
card_text('kavu titan', 'Kicker {2}{G} (You may pay an additional {2}{G} as you cast this spell.)\nIf Kavu Titan was kicked, it enters the battlefield with three +1/+1 counters on it and with trample.').
card_mana_cost('kavu titan', ['1', 'G']).
card_cmc('kavu titan', 2).
card_layout('kavu titan', 'normal').
card_power('kavu titan', 2).
card_toughness('kavu titan', 2).

% Found in: ALL, ME2
card_name('kaysa', 'Kaysa').
card_type('kaysa', 'Legendary Creature — Elf Druid').
card_types('kaysa', ['Creature']).
card_subtypes('kaysa', ['Elf', 'Druid']).
card_supertypes('kaysa', ['Legendary']).
card_colors('kaysa', ['G']).
card_text('kaysa', 'Green creatures you control get +1/+1.').
card_mana_cost('kaysa', ['3', 'G', 'G']).
card_cmc('kaysa', 5).
card_layout('kaysa', 'normal').
card_power('kaysa', 2).
card_toughness('kaysa', 3).
card_reserved('kaysa').

% Found in: ZEN
card_name('kazandu blademaster', 'Kazandu Blademaster').
card_type('kazandu blademaster', 'Creature — Human Soldier Ally').
card_types('kazandu blademaster', ['Creature']).
card_subtypes('kazandu blademaster', ['Human', 'Soldier', 'Ally']).
card_colors('kazandu blademaster', ['W']).
card_text('kazandu blademaster', 'First strike, vigilance\nWhenever Kazandu Blademaster or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Kazandu Blademaster.').
card_mana_cost('kazandu blademaster', ['W', 'W']).
card_cmc('kazandu blademaster', 2).
card_layout('kazandu blademaster', 'normal').
card_power('kazandu blademaster', 1).
card_toughness('kazandu blademaster', 1).

% Found in: ARC, C13, CMD, DDH, DDL, PC2, ZEN
card_name('kazandu refuge', 'Kazandu Refuge').
card_type('kazandu refuge', 'Land').
card_types('kazandu refuge', ['Land']).
card_subtypes('kazandu refuge', []).
card_colors('kazandu refuge', []).
card_text('kazandu refuge', 'Kazandu Refuge enters the battlefield tapped.\nWhen Kazandu Refuge enters the battlefield, you gain 1 life.\n{T}: Add {R} or {G} to your mana pool.').
card_layout('kazandu refuge', 'normal').

% Found in: C13, ROE
card_name('kazandu tuskcaller', 'Kazandu Tuskcaller').
card_type('kazandu tuskcaller', 'Creature — Human Shaman').
card_types('kazandu tuskcaller', ['Creature']).
card_subtypes('kazandu tuskcaller', ['Human', 'Shaman']).
card_colors('kazandu tuskcaller', ['G']).
card_text('kazandu tuskcaller', 'Level up {1}{G} ({1}{G}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 2-5\n1/1\n{T}: Put a 3/3 green Elephant creature token onto the battlefield.\nLEVEL 6+\n1/1\n{T}: Put two 3/3 green Elephant creature tokens onto the battlefield.').
card_mana_cost('kazandu tuskcaller', ['1', 'G']).
card_cmc('kazandu tuskcaller', 2).
card_layout('kazandu tuskcaller', 'leveler').
card_power('kazandu tuskcaller', 1).
card_toughness('kazandu tuskcaller', 1).

% Found in: ZEN
card_name('kazuul warlord', 'Kazuul Warlord').
card_type('kazuul warlord', 'Creature — Minotaur Warrior Ally').
card_types('kazuul warlord', ['Creature']).
card_subtypes('kazuul warlord', ['Minotaur', 'Warrior', 'Ally']).
card_colors('kazuul warlord', ['R']).
card_text('kazuul warlord', 'Whenever Kazuul Warlord or another Ally enters the battlefield under your control, you may put a +1/+1 counter on each Ally creature you control.').
card_mana_cost('kazuul warlord', ['4', 'R']).
card_cmc('kazuul warlord', 5).
card_layout('kazuul warlord', 'normal').
card_power('kazuul warlord', 3).
card_toughness('kazuul warlord', 3).

% Found in: WWK
card_name('kazuul, tyrant of the cliffs', 'Kazuul, Tyrant of the Cliffs').
card_type('kazuul, tyrant of the cliffs', 'Legendary Creature — Ogre Warrior').
card_types('kazuul, tyrant of the cliffs', ['Creature']).
card_subtypes('kazuul, tyrant of the cliffs', ['Ogre', 'Warrior']).
card_supertypes('kazuul, tyrant of the cliffs', ['Legendary']).
card_colors('kazuul, tyrant of the cliffs', ['R']).
card_text('kazuul, tyrant of the cliffs', 'Whenever a creature an opponent controls attacks, if you\'re the defending player, put a 3/3 red Ogre creature token onto the battlefield unless that creature\'s controller pays {3}.').
card_mana_cost('kazuul, tyrant of the cliffs', ['3', 'R', 'R']).
card_cmc('kazuul, tyrant of the cliffs', 5).
card_layout('kazuul, tyrant of the cliffs', 'normal').
card_power('kazuul, tyrant of the cliffs', 5).
card_toughness('kazuul, tyrant of the cliffs', 4).

% Found in: ALA
card_name('kederekt creeper', 'Kederekt Creeper').
card_type('kederekt creeper', 'Creature — Horror').
card_types('kederekt creeper', ['Creature']).
card_subtypes('kederekt creeper', ['Horror']).
card_colors('kederekt creeper', ['U', 'B', 'R']).
card_text('kederekt creeper', 'Menace (This creature can\'t be blocked except by two or more creatures.)\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_mana_cost('kederekt creeper', ['U', 'B', 'R']).
card_cmc('kederekt creeper', 3).
card_layout('kederekt creeper', 'normal').
card_power('kederekt creeper', 2).
card_toughness('kederekt creeper', 3).

% Found in: ALA
card_name('kederekt leviathan', 'Kederekt Leviathan').
card_type('kederekt leviathan', 'Creature — Leviathan').
card_types('kederekt leviathan', ['Creature']).
card_subtypes('kederekt leviathan', ['Leviathan']).
card_colors('kederekt leviathan', ['U']).
card_text('kederekt leviathan', 'When Kederekt Leviathan enters the battlefield, return all other nonland permanents to their owners\' hands.\nUnearth {6}{U} ({6}{U}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('kederekt leviathan', ['6', 'U', 'U']).
card_cmc('kederekt leviathan', 8).
card_layout('kederekt leviathan', 'normal').
card_power('kederekt leviathan', 5).
card_toughness('kederekt leviathan', 5).

% Found in: CON
card_name('kederekt parasite', 'Kederekt Parasite').
card_type('kederekt parasite', 'Creature — Horror').
card_types('kederekt parasite', ['Creature']).
card_subtypes('kederekt parasite', ['Horror']).
card_colors('kederekt parasite', ['B']).
card_text('kederekt parasite', 'Whenever an opponent draws a card, if you control a red permanent, you may have Kederekt Parasite deal 1 damage to that player.').
card_mana_cost('kederekt parasite', ['B']).
card_cmc('kederekt parasite', 1).
card_layout('kederekt parasite', 'normal').
card_power('kederekt parasite', 1).
card_toughness('kederekt parasite', 1).

% Found in: PLC
card_name('keen sense', 'Keen Sense').
card_type('keen sense', 'Enchantment — Aura').
card_types('keen sense', ['Enchantment']).
card_subtypes('keen sense', ['Aura']).
card_colors('keen sense', ['G']).
card_text('keen sense', 'Enchant creature\nWhenever enchanted creature deals damage to an opponent, you may draw a card.').
card_mana_cost('keen sense', ['G']).
card_cmc('keen sense', 1).
card_layout('keen sense', 'normal').

% Found in: POR
card_name('keen-eyed archers', 'Keen-Eyed Archers').
card_type('keen-eyed archers', 'Creature — Elf Archer').
card_types('keen-eyed archers', ['Creature']).
card_subtypes('keen-eyed archers', ['Elf', 'Archer']).
card_colors('keen-eyed archers', ['W']).
card_text('keen-eyed archers', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('keen-eyed archers', ['2', 'W']).
card_cmc('keen-eyed archers', 3).
card_layout('keen-eyed archers', 'normal').
card_power('keen-eyed archers', 2).
card_toughness('keen-eyed archers', 2).

% Found in: LGN, VMA
card_name('keeneye aven', 'Keeneye Aven').
card_type('keeneye aven', 'Creature — Bird Soldier').
card_types('keeneye aven', ['Creature']).
card_subtypes('keeneye aven', ['Bird', 'Soldier']).
card_colors('keeneye aven', ['U']).
card_text('keeneye aven', 'Flying\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('keeneye aven', ['3', 'U']).
card_cmc('keeneye aven', 4).
card_layout('keeneye aven', 'normal').
card_power('keeneye aven', 2).
card_toughness('keeneye aven', 3).

% Found in: RTR
card_name('keening apparition', 'Keening Apparition').
card_type('keening apparition', 'Creature — Spirit').
card_types('keening apparition', ['Creature']).
card_subtypes('keening apparition', ['Spirit']).
card_colors('keening apparition', ['W']).
card_text('keening apparition', 'Sacrifice Keening Apparition: Destroy target enchantment.').
card_mana_cost('keening apparition', ['1', 'W']).
card_cmc('keening apparition', 2).
card_layout('keening apparition', 'normal').
card_power('keening apparition', 2).
card_toughness('keening apparition', 2).

% Found in: DD3_GVL, DDD, RAV
card_name('keening banshee', 'Keening Banshee').
card_type('keening banshee', 'Creature — Spirit').
card_types('keening banshee', ['Creature']).
card_subtypes('keening banshee', ['Spirit']).
card_colors('keening banshee', ['B']).
card_text('keening banshee', 'Flying\nWhen Keening Banshee enters the battlefield, target creature gets -2/-2 until end of turn.').
card_mana_cost('keening banshee', ['2', 'B', 'B']).
card_cmc('keening banshee', 4).
card_layout('keening banshee', 'normal').
card_power('keening banshee', 2).
card_toughness('keening banshee', 2).

% Found in: ROE
card_name('keening stone', 'Keening Stone').
card_type('keening stone', 'Artifact').
card_types('keening stone', ['Artifact']).
card_subtypes('keening stone', []).
card_colors('keening stone', []).
card_text('keening stone', '{5}, {T}: Target player puts the top X cards of his or her library into his or her graveyard, where X is the number of cards in that player\'s graveyard.').
card_mana_cost('keening stone', ['6']).
card_cmc('keening stone', 6).
card_layout('keening stone', 'normal').

% Found in: HOP, JUD
card_name('keep watch', 'Keep Watch').
card_type('keep watch', 'Instant').
card_types('keep watch', ['Instant']).
card_subtypes('keep watch', []).
card_colors('keep watch', ['U']).
card_text('keep watch', 'Draw a card for each attacking creature.').
card_mana_cost('keep watch', ['2', 'U']).
card_cmc('keep watch', 3).
card_layout('keep watch', 'normal').

% Found in: VIS
card_name('keeper of kookus', 'Keeper of Kookus').
card_type('keeper of kookus', 'Creature — Goblin').
card_types('keeper of kookus', ['Creature']).
card_subtypes('keeper of kookus', ['Goblin']).
card_colors('keeper of kookus', ['R']).
card_text('keeper of kookus', '{R}: Keeper of Kookus gains protection from red until end of turn.').
card_mana_cost('keeper of kookus', ['R']).
card_cmc('keeper of kookus', 1).
card_layout('keeper of kookus', 'normal').
card_power('keeper of kookus', 1).
card_toughness('keeper of kookus', 1).

% Found in: ALA
card_name('keeper of progenitus', 'Keeper of Progenitus').
card_type('keeper of progenitus', 'Creature — Elf Druid').
card_types('keeper of progenitus', ['Creature']).
card_subtypes('keeper of progenitus', ['Elf', 'Druid']).
card_colors('keeper of progenitus', ['G']).
card_text('keeper of progenitus', 'Whenever a player taps a Mountain, Forest, or Plains for mana, that player adds one mana to his or her mana pool of any type that land produced.').
card_mana_cost('keeper of progenitus', ['3', 'G']).
card_cmc('keeper of progenitus', 4).
card_layout('keeper of progenitus', 'normal').
card_power('keeper of progenitus', 1).
card_toughness('keeper of progenitus', 3).

% Found in: EXO
card_name('keeper of the beasts', 'Keeper of the Beasts').
card_type('keeper of the beasts', 'Creature — Human Wizard').
card_types('keeper of the beasts', ['Creature']).
card_subtypes('keeper of the beasts', ['Human', 'Wizard']).
card_colors('keeper of the beasts', ['G']).
card_text('keeper of the beasts', '{G}, {T}: Choose target opponent who controlled more creatures than you did as you activated this ability. Put a 2/2 green Beast creature token onto the battlefield.').
card_mana_cost('keeper of the beasts', ['G', 'G']).
card_cmc('keeper of the beasts', 2).
card_layout('keeper of the beasts', 'normal').
card_power('keeper of the beasts', 1).
card_toughness('keeper of the beasts', 2).

% Found in: EXO
card_name('keeper of the dead', 'Keeper of the Dead').
card_type('keeper of the dead', 'Creature — Human Wizard').
card_types('keeper of the dead', ['Creature']).
card_subtypes('keeper of the dead', ['Human', 'Wizard']).
card_colors('keeper of the dead', ['B']).
card_text('keeper of the dead', '{B}, {T}: Choose target opponent who had at least two fewer creature cards in his or her graveyard than you did as you activated this ability. Destroy target nonblack creature he or she controls.').
card_mana_cost('keeper of the dead', ['B', 'B']).
card_cmc('keeper of the dead', 2).
card_layout('keeper of the dead', 'normal').
card_power('keeper of the dead', 1).
card_toughness('keeper of the dead', 2).

% Found in: EXO
card_name('keeper of the flame', 'Keeper of the Flame').
card_type('keeper of the flame', 'Creature — Human Wizard').
card_types('keeper of the flame', ['Creature']).
card_subtypes('keeper of the flame', ['Human', 'Wizard']).
card_colors('keeper of the flame', ['R']).
card_text('keeper of the flame', '{R}, {T}: Choose target opponent who had more life than you did as you activated this ability. Keeper of the Flame deals 2 damage to him or her.').
card_mana_cost('keeper of the flame', ['R', 'R']).
card_cmc('keeper of the flame', 2).
card_layout('keeper of the flame', 'normal').
card_power('keeper of the flame', 1).
card_toughness('keeper of the flame', 2).

% Found in: DTK
card_name('keeper of the lens', 'Keeper of the Lens').
card_type('keeper of the lens', 'Artifact Creature — Golem').
card_types('keeper of the lens', ['Artifact', 'Creature']).
card_subtypes('keeper of the lens', ['Golem']).
card_colors('keeper of the lens', []).
card_text('keeper of the lens', 'You may look at face-down creatures you don\'t control. (You may do this at any time.)').
card_mana_cost('keeper of the lens', ['1']).
card_cmc('keeper of the lens', 1).
card_layout('keeper of the lens', 'normal').
card_power('keeper of the lens', 1).
card_toughness('keeper of the lens', 2).

% Found in: EXO
card_name('keeper of the light', 'Keeper of the Light').
card_type('keeper of the light', 'Creature — Human Wizard').
card_types('keeper of the light', ['Creature']).
card_subtypes('keeper of the light', ['Human', 'Wizard']).
card_colors('keeper of the light', ['W']).
card_text('keeper of the light', '{W}, {T}: Choose target opponent who had more life than you did as you activated this ability. You gain 3 life.').
card_mana_cost('keeper of the light', ['W', 'W']).
card_cmc('keeper of the light', 2).
card_layout('keeper of the light', 'normal').
card_power('keeper of the light', 1).
card_toughness('keeper of the light', 2).

% Found in: EXO
card_name('keeper of the mind', 'Keeper of the Mind').
card_type('keeper of the mind', 'Creature — Human Wizard').
card_types('keeper of the mind', ['Creature']).
card_subtypes('keeper of the mind', ['Human', 'Wizard']).
card_colors('keeper of the mind', ['U']).
card_text('keeper of the mind', '{U}, {T}: Choose target opponent who had at least two more cards in hand than you did as you activated this ability. Draw a card.').
card_mana_cost('keeper of the mind', ['U', 'U']).
card_cmc('keeper of the mind', 2).
card_layout('keeper of the mind', 'normal').
card_power('keeper of the mind', 1).
card_toughness('keeper of the mind', 2).

% Found in: LGN
card_name('keeper of the nine gales', 'Keeper of the Nine Gales').
card_type('keeper of the nine gales', 'Creature — Bird Wizard').
card_types('keeper of the nine gales', ['Creature']).
card_subtypes('keeper of the nine gales', ['Bird', 'Wizard']).
card_colors('keeper of the nine gales', ['U']).
card_text('keeper of the nine gales', 'Flying\n{T}, Tap two untapped Birds you control: Return target permanent to its owner\'s hand.').
card_mana_cost('keeper of the nine gales', ['2', 'U']).
card_cmc('keeper of the nine gales', 3).
card_layout('keeper of the nine gales', 'normal').
card_power('keeper of the nine gales', 1).
card_toughness('keeper of the nine gales', 2).

% Found in: UNH
card_name('keeper of the sacred word', 'Keeper of the Sacred Word').
card_type('keeper of the sacred word', 'Creature — Human Druid').
card_types('keeper of the sacred word', ['Creature']).
card_subtypes('keeper of the sacred word', ['Human', 'Druid']).
card_colors('keeper of the sacred word', ['G']).
card_text('keeper of the sacred word', 'As Keeper of the Sacred Word comes into play, choose a word.\nWhenever an opponent says the chosen word, Keeper of the Sacred Word gets +3/+3 until end of turn.').
card_mana_cost('keeper of the sacred word', ['2', 'G']).
card_cmc('keeper of the sacred word', 3).
card_layout('keeper of the sacred word', 'normal').
card_power('keeper of the sacred word', 2).
card_toughness('keeper of the sacred word', 3).

% Found in: ALL
card_name('keeper of tresserhorn', 'Keeper of Tresserhorn').
card_type('keeper of tresserhorn', 'Creature — Avatar').
card_types('keeper of tresserhorn', ['Creature']).
card_subtypes('keeper of tresserhorn', ['Avatar']).
card_colors('keeper of tresserhorn', ['B']).
card_text('keeper of tresserhorn', 'Whenever Keeper of Tresserhorn attacks and isn\'t blocked, it assigns no combat damage this turn and defending player loses 2 life.').
card_mana_cost('keeper of tresserhorn', ['5', 'B']).
card_cmc('keeper of tresserhorn', 6).
card_layout('keeper of tresserhorn', 'normal').
card_power('keeper of tresserhorn', 6).
card_toughness('keeper of tresserhorn', 6).
card_reserved('keeper of tresserhorn').

% Found in: CHR, LEG
card_name('keepers of the faith', 'Keepers of the Faith').
card_type('keepers of the faith', 'Creature — Human Cleric').
card_types('keepers of the faith', ['Creature']).
card_subtypes('keepers of the faith', ['Human', 'Cleric']).
card_colors('keepers of the faith', ['W']).
card_text('keepers of the faith', '').
card_mana_cost('keepers of the faith', ['1', 'W', 'W']).
card_cmc('keepers of the faith', 3).
card_layout('keepers of the faith', 'normal').
card_power('keepers of the faith', 2).
card_toughness('keepers of the faith', 3).

% Found in: THS
card_name('keepsake gorgon', 'Keepsake Gorgon').
card_type('keepsake gorgon', 'Creature — Gorgon').
card_types('keepsake gorgon', ['Creature']).
card_subtypes('keepsake gorgon', ['Gorgon']).
card_colors('keepsake gorgon', ['B']).
card_text('keepsake gorgon', 'Deathtouch\n{5}{B}{B}: Monstrosity 1. (If this creature isn\'t monstrous, put a +1/+1 counter on it and it becomes monstrous.)\nWhen Keepsake Gorgon becomes monstrous, destroy target non-Gorgon creature an opponent controls.').
card_mana_cost('keepsake gorgon', ['3', 'B', 'B']).
card_cmc('keepsake gorgon', 5).
card_layout('keepsake gorgon', 'normal').
card_power('keepsake gorgon', 2).
card_toughness('keepsake gorgon', 5).

% Found in: CHR, LEG, ME3
card_name('kei takahashi', 'Kei Takahashi').
card_type('kei takahashi', 'Legendary Creature — Human Cleric').
card_types('kei takahashi', ['Creature']).
card_subtypes('kei takahashi', ['Human', 'Cleric']).
card_supertypes('kei takahashi', ['Legendary']).
card_colors('kei takahashi', ['W', 'G']).
card_text('kei takahashi', '{T}: Prevent the next 2 damage that would be dealt to target creature this turn.').
card_mana_cost('kei takahashi', ['2', 'G', 'W']).
card_cmc('kei takahashi', 4).
card_layout('kei takahashi', 'normal').
card_power('kei takahashi', 2).
card_toughness('kei takahashi', 2).

% Found in: CHK, MMA
card_name('keiga, the tide star', 'Keiga, the Tide Star').
card_type('keiga, the tide star', 'Legendary Creature — Dragon Spirit').
card_types('keiga, the tide star', ['Creature']).
card_subtypes('keiga, the tide star', ['Dragon', 'Spirit']).
card_supertypes('keiga, the tide star', ['Legendary']).
card_colors('keiga, the tide star', ['U']).
card_text('keiga, the tide star', 'Flying\nWhen Keiga, the Tide Star dies, gain control of target creature.').
card_mana_cost('keiga, the tide star', ['5', 'U']).
card_cmc('keiga, the tide star', 6).
card_layout('keiga, the tide star', 'normal').
card_power('keiga, the tide star', 5).
card_toughness('keiga, the tide star', 5).

% Found in: PCY
card_name('keldon arsonist', 'Keldon Arsonist').
card_type('keldon arsonist', 'Creature — Human Soldier').
card_types('keldon arsonist', ['Creature']).
card_subtypes('keldon arsonist', ['Human', 'Soldier']).
card_colors('keldon arsonist', ['R']).
card_text('keldon arsonist', '{1}, Sacrifice two lands: Destroy target land.').
card_mana_cost('keldon arsonist', ['2', 'R']).
card_cmc('keldon arsonist', 3).
card_layout('keldon arsonist', 'normal').
card_power('keldon arsonist', 1).
card_toughness('keldon arsonist', 1).

% Found in: PCY
card_name('keldon battlewagon', 'Keldon Battlewagon').
card_type('keldon battlewagon', 'Artifact Creature — Juggernaut').
card_types('keldon battlewagon', ['Artifact', 'Creature']).
card_subtypes('keldon battlewagon', ['Juggernaut']).
card_colors('keldon battlewagon', []).
card_text('keldon battlewagon', 'Trample\nKeldon Battlewagon can\'t block.\nWhen Keldon Battlewagon attacks, sacrifice it at end of combat.\nTap an untapped creature you control: Keldon Battlewagon gets +X/+0 until end of turn, where X is the power of the creature tapped this way.').
card_mana_cost('keldon battlewagon', ['5']).
card_cmc('keldon battlewagon', 5).
card_layout('keldon battlewagon', 'normal').
card_power('keldon battlewagon', 0).
card_toughness('keldon battlewagon', 3).

% Found in: PCY
card_name('keldon berserker', 'Keldon Berserker').
card_type('keldon berserker', 'Creature — Human Soldier Berserker').
card_types('keldon berserker', ['Creature']).
card_subtypes('keldon berserker', ['Human', 'Soldier', 'Berserker']).
card_colors('keldon berserker', ['R']).
card_text('keldon berserker', 'Whenever Keldon Berserker attacks, if you control no untapped lands, it gets +3/+0 until end of turn.').
card_mana_cost('keldon berserker', ['3', 'R']).
card_cmc('keldon berserker', 4).
card_layout('keldon berserker', 'normal').
card_power('keldon berserker', 2).
card_toughness('keldon berserker', 3).

% Found in: HOP, PD2, UDS
card_name('keldon champion', 'Keldon Champion').
card_type('keldon champion', 'Creature — Human Barbarian').
card_types('keldon champion', ['Creature']).
card_subtypes('keldon champion', ['Human', 'Barbarian']).
card_colors('keldon champion', ['R']).
card_text('keldon champion', 'Haste\nEcho {2}{R}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Keldon Champion enters the battlefield, it deals 3 damage to target player.').
card_mana_cost('keldon champion', ['2', 'R', 'R']).
card_cmc('keldon champion', 4).
card_layout('keldon champion', 'normal').
card_power('keldon champion', 3).
card_toughness('keldon champion', 2).

% Found in: PCY
card_name('keldon firebombers', 'Keldon Firebombers').
card_type('keldon firebombers', 'Creature — Human Soldier').
card_types('keldon firebombers', ['Creature']).
card_subtypes('keldon firebombers', ['Human', 'Soldier']).
card_colors('keldon firebombers', ['R']).
card_text('keldon firebombers', 'When Keldon Firebombers enters the battlefield, each player sacrifices all lands he or she controls except for three.').
card_mana_cost('keldon firebombers', ['3', 'R', 'R']).
card_cmc('keldon firebombers', 5).
card_layout('keldon firebombers', 'normal').
card_power('keldon firebombers', 3).
card_toughness('keldon firebombers', 3).

% Found in: TSP
card_name('keldon halberdier', 'Keldon Halberdier').
card_type('keldon halberdier', 'Creature — Human Warrior').
card_types('keldon halberdier', ['Creature']).
card_subtypes('keldon halberdier', ['Human', 'Warrior']).
card_colors('keldon halberdier', ['R']).
card_text('keldon halberdier', 'First strike\nSuspend 4—{R} (Rather than cast this card from your hand, you may pay {R} and exile it with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_mana_cost('keldon halberdier', ['4', 'R']).
card_cmc('keldon halberdier', 5).
card_layout('keldon halberdier', 'normal').
card_power('keldon halberdier', 4).
card_toughness('keldon halberdier', 1).

% Found in: PLS
card_name('keldon mantle', 'Keldon Mantle').
card_type('keldon mantle', 'Enchantment — Aura').
card_types('keldon mantle', ['Enchantment']).
card_subtypes('keldon mantle', ['Aura']).
card_colors('keldon mantle', ['R']).
card_text('keldon mantle', 'Enchant creature\n{B}: Regenerate enchanted creature.\n{R}: Enchanted creature gets +1/+0 until end of turn.\n{G}: Enchanted creature gains trample until end of turn.').
card_mana_cost('keldon mantle', ['1', 'R']).
card_cmc('keldon mantle', 2).
card_layout('keldon mantle', 'normal').

% Found in: PD2, PLC
card_name('keldon marauders', 'Keldon Marauders').
card_type('keldon marauders', 'Creature — Human Warrior').
card_types('keldon marauders', ['Creature']).
card_subtypes('keldon marauders', ['Human', 'Warrior']).
card_colors('keldon marauders', ['R']).
card_text('keldon marauders', 'Vanishing 2 (This permanent enters the battlefield with two time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Keldon Marauders enters the battlefield or leaves the battlefield, it deals 1 damage to target player.').
card_mana_cost('keldon marauders', ['1', 'R']).
card_cmc('keldon marauders', 2).
card_layout('keldon marauders', 'normal').
card_power('keldon marauders', 3).
card_toughness('keldon marauders', 3).

% Found in: DD2, DD3_JVC, FUT
card_name('keldon megaliths', 'Keldon Megaliths').
card_type('keldon megaliths', 'Land').
card_types('keldon megaliths', ['Land']).
card_subtypes('keldon megaliths', []).
card_colors('keldon megaliths', []).
card_text('keldon megaliths', 'Keldon Megaliths enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\nHellbent — {1}{R}, {T}: Keldon Megaliths deals 1 damage to target creature or player. Activate this ability only if you have no cards in hand.').
card_layout('keldon megaliths', 'normal').

% Found in: INV, VMA
card_name('keldon necropolis', 'Keldon Necropolis').
card_type('keldon necropolis', 'Legendary Land').
card_types('keldon necropolis', ['Land']).
card_subtypes('keldon necropolis', []).
card_supertypes('keldon necropolis', ['Legendary']).
card_colors('keldon necropolis', []).
card_text('keldon necropolis', '{T}: Add {1} to your mana pool.\n{4}{R}, {T}, Sacrifice a creature: Keldon Necropolis deals 2 damage to target creature or player.').
card_layout('keldon necropolis', 'normal').

% Found in: PLS
card_name('keldon twilight', 'Keldon Twilight').
card_type('keldon twilight', 'Enchantment').
card_types('keldon twilight', ['Enchantment']).
card_subtypes('keldon twilight', []).
card_colors('keldon twilight', ['B', 'R']).
card_text('keldon twilight', 'At the beginning of each player\'s end step, if no creatures attacked this turn, that player sacrifices a creature he or she controlled since the beginning of the turn.').
card_mana_cost('keldon twilight', ['1', 'B', 'R']).
card_cmc('keldon twilight', 3).
card_layout('keldon twilight', 'normal').

% Found in: UDS
card_name('keldon vandals', 'Keldon Vandals').
card_type('keldon vandals', 'Creature — Human Rogue').
card_types('keldon vandals', ['Creature']).
card_subtypes('keldon vandals', ['Human', 'Rogue']).
card_colors('keldon vandals', ['R']).
card_text('keldon vandals', 'Echo {2}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Keldon Vandals enters the battlefield, destroy target artifact.').
card_mana_cost('keldon vandals', ['2', 'R']).
card_cmc('keldon vandals', 3).
card_layout('keldon vandals', 'normal').
card_power('keldon vandals', 4).
card_toughness('keldon vandals', 1).

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, MED
card_name('keldon warlord', 'Keldon Warlord').
card_type('keldon warlord', 'Creature — Human Barbarian').
card_types('keldon warlord', ['Creature']).
card_subtypes('keldon warlord', ['Human', 'Barbarian']).
card_colors('keldon warlord', ['R']).
card_text('keldon warlord', 'Keldon Warlord\'s power and toughness are each equal to the number of non-Wall creatures you control.').
card_mana_cost('keldon warlord', ['2', 'R', 'R']).
card_cmc('keldon warlord', 4).
card_layout('keldon warlord', 'normal').
card_power('keldon warlord', '*').
card_toughness('keldon warlord', '*').

% Found in: M10
card_name('kelinore bat', 'Kelinore Bat').
card_type('kelinore bat', 'Creature — Bat').
card_types('kelinore bat', ['Creature']).
card_subtypes('kelinore bat', ['Bat']).
card_colors('kelinore bat', ['B']).
card_text('kelinore bat', 'Flying').
card_mana_cost('kelinore bat', ['2', 'B']).
card_cmc('kelinore bat', 3).
card_layout('kelinore bat', 'normal').
card_power('kelinore bat', 2).
card_toughness('kelinore bat', 1).

% Found in: ICE
card_name('kelsinko ranger', 'Kelsinko Ranger').
card_type('kelsinko ranger', 'Creature — Human').
card_types('kelsinko ranger', ['Creature']).
card_subtypes('kelsinko ranger', ['Human']).
card_colors('kelsinko ranger', ['W']).
card_text('kelsinko ranger', '{1}{W}: Target green creature gains first strike until end of turn.').
card_mana_cost('kelsinko ranger', ['W']).
card_cmc('kelsinko ranger', 1).
card_layout('kelsinko ranger', 'normal').
card_power('kelsinko ranger', 1).
card_toughness('kelsinko ranger', 1).

% Found in: MBS
card_name('kemba\'s legion', 'Kemba\'s Legion').
card_type('kemba\'s legion', 'Creature — Cat Soldier').
card_types('kemba\'s legion', ['Creature']).
card_subtypes('kemba\'s legion', ['Cat', 'Soldier']).
card_colors('kemba\'s legion', ['W']).
card_text('kemba\'s legion', 'Vigilance\nKemba\'s Legion can block an additional creature for each Equipment attached to Kemba\'s Legion.').
card_mana_cost('kemba\'s legion', ['5', 'W', 'W']).
card_cmc('kemba\'s legion', 7).
card_layout('kemba\'s legion', 'normal').
card_power('kemba\'s legion', 4).
card_toughness('kemba\'s legion', 6).

% Found in: DDF, SOM
card_name('kemba\'s skyguard', 'Kemba\'s Skyguard').
card_type('kemba\'s skyguard', 'Creature — Cat Knight').
card_types('kemba\'s skyguard', ['Creature']).
card_subtypes('kemba\'s skyguard', ['Cat', 'Knight']).
card_colors('kemba\'s skyguard', ['W']).
card_text('kemba\'s skyguard', 'Flying\nWhen Kemba\'s Skyguard enters the battlefield, you gain 2 life.').
card_mana_cost('kemba\'s skyguard', ['1', 'W', 'W']).
card_cmc('kemba\'s skyguard', 3).
card_layout('kemba\'s skyguard', 'normal').
card_power('kemba\'s skyguard', 2).
card_toughness('kemba\'s skyguard', 2).

% Found in: C14, SOM
card_name('kemba, kha regent', 'Kemba, Kha Regent').
card_type('kemba, kha regent', 'Legendary Creature — Cat Cleric').
card_types('kemba, kha regent', ['Creature']).
card_subtypes('kemba, kha regent', ['Cat', 'Cleric']).
card_supertypes('kemba, kha regent', ['Legendary']).
card_colors('kemba, kha regent', ['W']).
card_text('kemba, kha regent', 'At the beginning of your upkeep, put a 2/2 white Cat creature token onto the battlefield for each Equipment attached to Kemba, Kha Regent.').
card_mana_cost('kemba, kha regent', ['1', 'W', 'W']).
card_cmc('kemba, kha regent', 3).
card_layout('kemba, kha regent', 'normal').
card_power('kemba, kha regent', 2).
card_toughness('kemba, kha regent', 4).

% Found in: SOK
card_name('kemuri-onna', 'Kemuri-Onna').
card_type('kemuri-onna', 'Creature — Spirit').
card_types('kemuri-onna', ['Creature']).
card_subtypes('kemuri-onna', ['Spirit']).
card_colors('kemuri-onna', ['B']).
card_text('kemuri-onna', 'When Kemuri-Onna enters the battlefield, target player discards a card.\nWhenever you cast a Spirit or Arcane spell, you may return Kemuri-Onna to its owner\'s hand.').
card_mana_cost('kemuri-onna', ['4', 'B']).
card_cmc('kemuri-onna', 5).
card_layout('kemuri-onna', 'normal').
card_power('kemuri-onna', 3).
card_toughness('kemuri-onna', 3).

% Found in: BOK
card_name('kentaro, the smiling cat', 'Kentaro, the Smiling Cat').
card_type('kentaro, the smiling cat', 'Legendary Creature — Human Samurai').
card_types('kentaro, the smiling cat', ['Creature']).
card_subtypes('kentaro, the smiling cat', ['Human', 'Samurai']).
card_supertypes('kentaro, the smiling cat', ['Legendary']).
card_colors('kentaro, the smiling cat', ['W']).
card_text('kentaro, the smiling cat', 'Bushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)\nYou may pay {X} rather than pay the mana cost for Samurai spells you cast, where X is that spell\'s converted mana cost.').
card_mana_cost('kentaro, the smiling cat', ['1', 'W']).
card_cmc('kentaro, the smiling cat', 2).
card_layout('kentaro, the smiling cat', 'normal').
card_power('kentaro, the smiling cat', 2).
card_toughness('kentaro, the smiling cat', 1).

% Found in: CHK
card_name('kenzo the hardhearted', 'Kenzo the Hardhearted').
card_type('kenzo the hardhearted', 'Legendary Creature — Human Samurai').
card_types('kenzo the hardhearted', ['Creature']).
card_subtypes('kenzo the hardhearted', ['Human', 'Samurai']).
card_supertypes('kenzo the hardhearted', ['Legendary']).
card_colors('kenzo the hardhearted', ['W']).
card_text('kenzo the hardhearted', 'Double strike; bushido 2 (Whenever this creature blocks or becomes blocked, it gets +2/+2 until end of turn.)').
card_mana_cost('kenzo the hardhearted', ['W']).
card_cmc('kenzo the hardhearted', 1).
card_layout('kenzo the hardhearted', 'flip').
card_power('kenzo the hardhearted', 3).
card_toughness('kenzo the hardhearted', 4).

% Found in: JOU
card_name('keranos, god of storms', 'Keranos, God of Storms').
card_type('keranos, god of storms', 'Legendary Enchantment Creature — God').
card_types('keranos, god of storms', ['Enchantment', 'Creature']).
card_subtypes('keranos, god of storms', ['God']).
card_supertypes('keranos, god of storms', ['Legendary']).
card_colors('keranos, god of storms', ['U', 'R']).
card_text('keranos, god of storms', 'Indestructible\nAs long as your devotion to blue and red is less than seven, Keranos isn\'t a creature.\nReveal the first card you draw on each of your turns. Whenever you reveal a land card this way, draw a card. Whenever you reveal a nonland card this way, Keranos deals 3 damage to target creature or player.').
card_mana_cost('keranos, god of storms', ['3', 'U', 'R']).
card_cmc('keranos, god of storms', 5).
card_layout('keranos, god of storms', 'normal').
card_power('keranos, god of storms', 6).
card_toughness('keranos, god of storms', 5).

% Found in: PC2
card_name('kessig', 'Kessig').
card_type('kessig', 'Plane — Innistrad').
card_types('kessig', ['Plane']).
card_subtypes('kessig', ['Innistrad']).
card_colors('kessig', []).
card_text('kessig', 'Prevent all combat damage that would be dealt by non-Werewolf creatures.\nWhenever you roll {C}, each creature you control gets +2/+2, gains trample, and becomes a Werewolf in addition to its other types until end of turn.').
card_layout('kessig', 'plane').

% Found in: ISD
card_name('kessig cagebreakers', 'Kessig Cagebreakers').
card_type('kessig cagebreakers', 'Creature — Human Rogue').
card_types('kessig cagebreakers', ['Creature']).
card_subtypes('kessig cagebreakers', ['Human', 'Rogue']).
card_colors('kessig cagebreakers', ['G']).
card_text('kessig cagebreakers', 'Whenever Kessig Cagebreakers attacks, put a 2/2 green Wolf creature token onto the battlefield tapped and attacking for each creature card in your graveyard.').
card_mana_cost('kessig cagebreakers', ['4', 'G']).
card_cmc('kessig cagebreakers', 5).
card_layout('kessig cagebreakers', 'normal').
card_power('kessig cagebreakers', 3).
card_toughness('kessig cagebreakers', 4).

% Found in: AVR
card_name('kessig malcontents', 'Kessig Malcontents').
card_type('kessig malcontents', 'Creature — Human Warrior').
card_types('kessig malcontents', ['Creature']).
card_subtypes('kessig malcontents', ['Human', 'Warrior']).
card_colors('kessig malcontents', ['R']).
card_text('kessig malcontents', 'When Kessig Malcontents enters the battlefield, it deals damage to target player equal to the number of Humans you control.').
card_mana_cost('kessig malcontents', ['2', 'R']).
card_cmc('kessig malcontents', 3).
card_layout('kessig malcontents', 'normal').
card_power('kessig malcontents', 3).
card_toughness('kessig malcontents', 1).

% Found in: DKA
card_name('kessig recluse', 'Kessig Recluse').
card_type('kessig recluse', 'Creature — Spider').
card_types('kessig recluse', ['Creature']).
card_subtypes('kessig recluse', ['Spider']).
card_colors('kessig recluse', ['G']).
card_text('kessig recluse', 'Reach (This creature can block creatures with flying.)\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_mana_cost('kessig recluse', ['2', 'G', 'G']).
card_cmc('kessig recluse', 4).
card_layout('kessig recluse', 'normal').
card_power('kessig recluse', 2).
card_toughness('kessig recluse', 3).

% Found in: ISD
card_name('kessig wolf', 'Kessig Wolf').
card_type('kessig wolf', 'Creature — Wolf').
card_types('kessig wolf', ['Creature']).
card_subtypes('kessig wolf', ['Wolf']).
card_colors('kessig wolf', ['R']).
card_text('kessig wolf', '{1}{R}: Kessig Wolf gains first strike until end of turn.').
card_mana_cost('kessig wolf', ['2', 'R']).
card_cmc('kessig wolf', 3).
card_layout('kessig wolf', 'normal').
card_power('kessig wolf', 3).
card_toughness('kessig wolf', 1).

% Found in: ISD, V13
card_name('kessig wolf run', 'Kessig Wolf Run').
card_type('kessig wolf run', 'Land').
card_types('kessig wolf run', ['Land']).
card_subtypes('kessig wolf run', []).
card_colors('kessig wolf run', []).
card_text('kessig wolf run', '{T}: Add {1} to your mana pool.\n{X}{R}{G}, {T}: Target creature gets +X/+0 and gains trample until end of turn.').
card_layout('kessig wolf run', 'normal').

% Found in: GTC
card_name('keymaster rogue', 'Keymaster Rogue').
card_type('keymaster rogue', 'Creature — Human Rogue').
card_types('keymaster rogue', ['Creature']).
card_subtypes('keymaster rogue', ['Human', 'Rogue']).
card_colors('keymaster rogue', ['U']).
card_text('keymaster rogue', 'Keymaster Rogue can\'t be blocked.\nWhen Keymaster Rogue enters the battlefield, return a creature you control to its owner\'s hand.').
card_mana_cost('keymaster rogue', ['3', 'U']).
card_cmc('keymaster rogue', 4).
card_layout('keymaster rogue', 'normal').
card_power('keymaster rogue', 3).
card_toughness('keymaster rogue', 2).

% Found in: TMP, TPR, VMA
card_name('kezzerdrix', 'Kezzerdrix').
card_type('kezzerdrix', 'Creature — Rabbit Beast').
card_types('kezzerdrix', ['Creature']).
card_subtypes('kezzerdrix', ['Rabbit', 'Beast']).
card_colors('kezzerdrix', ['B']).
card_text('kezzerdrix', 'First strike\nAt the beginning of your upkeep, if your opponents control no creatures, Kezzerdrix deals 4 damage to you.').
card_mana_cost('kezzerdrix', ['2', 'B', 'B']).
card_cmc('kezzerdrix', 4).
card_layout('kezzerdrix', 'normal').
card_power('kezzerdrix', 4).
card_toughness('kezzerdrix', 4).

% Found in: ARN, MED
card_name('khabál ghoul', 'Khabál Ghoul').
card_type('khabál ghoul', 'Creature — Zombie').
card_types('khabál ghoul', ['Creature']).
card_subtypes('khabál ghoul', ['Zombie']).
card_colors('khabál ghoul', ['B']).
card_text('khabál ghoul', 'At the beginning of each end step, put a +1/+1 counter on Khabál Ghoul for each creature that died this turn.').
card_mana_cost('khabál ghoul', ['2', 'B']).
card_cmc('khabál ghoul', 3).
card_layout('khabál ghoul', 'normal').
card_power('khabál ghoul', 1).
card_toughness('khabál ghoul', 1).
card_reserved('khabál ghoul').

% Found in: ARC, C13, PC2, WWK
card_name('khalni garden', 'Khalni Garden').
card_type('khalni garden', 'Land').
card_types('khalni garden', ['Land']).
card_subtypes('khalni garden', []).
card_colors('khalni garden', []).
card_text('khalni garden', 'Khalni Garden enters the battlefield tapped.\nWhen Khalni Garden enters the battlefield, put a 0/1 green Plant creature token onto the battlefield.\n{T}: Add {G} to your mana pool.').
card_layout('khalni garden', 'normal').

% Found in: ZEN
card_name('khalni gem', 'Khalni Gem').
card_type('khalni gem', 'Artifact').
card_types('khalni gem', ['Artifact']).
card_subtypes('khalni gem', []).
card_colors('khalni gem', []).
card_text('khalni gem', 'When Khalni Gem enters the battlefield, return two lands you control to their owner\'s hand.\n{T}: Add two mana of any one color to your mana pool.').
card_mana_cost('khalni gem', ['4']).
card_cmc('khalni gem', 4).
card_layout('khalni gem', 'normal').

% Found in: DDP, ZEN
card_name('khalni heart expedition', 'Khalni Heart Expedition').
card_type('khalni heart expedition', 'Enchantment').
card_types('khalni heart expedition', ['Enchantment']).
card_subtypes('khalni heart expedition', []).
card_colors('khalni heart expedition', ['G']).
card_text('khalni heart expedition', 'Landfall — Whenever a land enters the battlefield under your control, you may put a quest counter on Khalni Heart Expedition.\nRemove three quest counters from Khalni Heart Expedition and sacrifice it: Search your library for up to two basic land cards, put them onto the battlefield tapped, then shuffle your library.').
card_mana_cost('khalni heart expedition', ['1', 'G']).
card_cmc('khalni heart expedition', 2).
card_layout('khalni heart expedition', 'normal').

% Found in: ROE
card_name('khalni hydra', 'Khalni Hydra').
card_type('khalni hydra', 'Creature — Hydra').
card_types('khalni hydra', ['Creature']).
card_subtypes('khalni hydra', ['Hydra']).
card_colors('khalni hydra', ['G']).
card_text('khalni hydra', 'Khalni Hydra costs {G} less to cast for each green creature you control.\nTrample').
card_mana_cost('khalni hydra', ['G', 'G', 'G', 'G', 'G', 'G', 'G', 'G']).
card_cmc('khalni hydra', 8).
card_layout('khalni hydra', 'normal').
card_power('khalni hydra', 8).
card_toughness('khalni hydra', 8).

% Found in: PC2
card_name('kharasha foothills', 'Kharasha Foothills').
card_type('kharasha foothills', 'Plane — Mongseng').
card_types('kharasha foothills', ['Plane']).
card_subtypes('kharasha foothills', ['Mongseng']).
card_colors('kharasha foothills', []).
card_text('kharasha foothills', 'Whenever a creature you control attacks a player, for each other opponent, you may put a token that\'s a copy of that creature onto the battlefield tapped and attacking that opponent. Exile those tokens at the beginning of the next end step.\nWhenever you roll {C}, you may sacrifice any number of creatures. If you do, Kharasha Foothills deals that much damage to target creature.').
card_layout('kharasha foothills', 'plane').

% Found in: C13, TSP
card_name('kher keep', 'Kher Keep').
card_type('kher keep', 'Legendary Land').
card_types('kher keep', ['Land']).
card_subtypes('kher keep', []).
card_supertypes('kher keep', ['Legendary']).
card_colors('kher keep', []).
card_text('kher keep', '{T}: Add {1} to your mana pool.\n{1}{R}, {T}: Put a 0/1 red Kobold creature token named Kobolds of Kher Keep onto the battlefield.').
card_layout('kher keep', 'normal').

% Found in: KTK
card_name('kheru bloodsucker', 'Kheru Bloodsucker').
card_type('kheru bloodsucker', 'Creature — Vampire').
card_types('kheru bloodsucker', ['Creature']).
card_subtypes('kheru bloodsucker', ['Vampire']).
card_colors('kheru bloodsucker', ['B']).
card_text('kheru bloodsucker', 'Whenever a creature you control with toughness 4 or greater dies, each opponent loses 2 life and you gain 2 life.\n{2}{B}, Sacrifice another creature: Put a +1/+1 counter on Kheru Bloodsucker.').
card_mana_cost('kheru bloodsucker', ['2', 'B']).
card_cmc('kheru bloodsucker', 3).
card_layout('kheru bloodsucker', 'normal').
card_power('kheru bloodsucker', 2).
card_toughness('kheru bloodsucker', 2).

% Found in: KTK
card_name('kheru dreadmaw', 'Kheru Dreadmaw').
card_type('kheru dreadmaw', 'Creature — Zombie Crocodile').
card_types('kheru dreadmaw', ['Creature']).
card_subtypes('kheru dreadmaw', ['Zombie', 'Crocodile']).
card_colors('kheru dreadmaw', ['B']).
card_text('kheru dreadmaw', 'Defender\n{1}{G}, Sacrifice another creature: You gain life equal to the sacrificed creature\'s toughness.').
card_mana_cost('kheru dreadmaw', ['4', 'B']).
card_cmc('kheru dreadmaw', 5).
card_layout('kheru dreadmaw', 'normal').
card_power('kheru dreadmaw', 4).
card_toughness('kheru dreadmaw', 4).

% Found in: KTK, pPRE
card_name('kheru lich lord', 'Kheru Lich Lord').
card_type('kheru lich lord', 'Creature — Zombie Wizard').
card_types('kheru lich lord', ['Creature']).
card_subtypes('kheru lich lord', ['Zombie', 'Wizard']).
card_colors('kheru lich lord', ['U', 'B', 'G']).
card_text('kheru lich lord', 'At the beginning of your upkeep, you may pay {2}{B}. If you do, return a creature card at random from your graveyard to the battlefield. It gains flying, trample, and haste. Exile that card at the beginning of your next end step. If it would leave the battlefield, exile it instead of putting it anywhere else.').
card_mana_cost('kheru lich lord', ['3', 'B', 'G', 'U']).
card_cmc('kheru lich lord', 6).
card_layout('kheru lich lord', 'normal').
card_power('kheru lich lord', 4).
card_toughness('kheru lich lord', 4).

% Found in: KTK
card_name('kheru spellsnatcher', 'Kheru Spellsnatcher').
card_type('kheru spellsnatcher', 'Creature — Naga Wizard').
card_types('kheru spellsnatcher', ['Creature']).
card_subtypes('kheru spellsnatcher', ['Naga', 'Wizard']).
card_colors('kheru spellsnatcher', ['U']).
card_text('kheru spellsnatcher', 'Morph {4}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Kheru Spellsnatcher is turned face up, counter target spell. If that spell is countered this way, exile it instead of putting it into its owner\'s graveyard. You may cast that card without paying its mana cost for as long as it remains exiled.').
card_mana_cost('kheru spellsnatcher', ['3', 'U']).
card_cmc('kheru spellsnatcher', 4).
card_layout('kheru spellsnatcher', 'normal').
card_power('kheru spellsnatcher', 3).
card_toughness('kheru spellsnatcher', 3).

% Found in: CHK, MM2, MMA, V11
card_name('kiki-jiki, mirror breaker', 'Kiki-Jiki, Mirror Breaker').
card_type('kiki-jiki, mirror breaker', 'Legendary Creature — Goblin Shaman').
card_types('kiki-jiki, mirror breaker', ['Creature']).
card_subtypes('kiki-jiki, mirror breaker', ['Goblin', 'Shaman']).
card_supertypes('kiki-jiki, mirror breaker', ['Legendary']).
card_colors('kiki-jiki, mirror breaker', ['R']).
card_text('kiki-jiki, mirror breaker', 'Haste\n{T}: Put a token that\'s a copy of target nonlegendary creature you control onto the battlefield. That token has haste. Sacrifice it at the beginning of the next end step.').
card_mana_cost('kiki-jiki, mirror breaker', ['2', 'R', 'R', 'R']).
card_cmc('kiki-jiki, mirror breaker', 5).
card_layout('kiki-jiki, mirror breaker', 'normal').
card_power('kiki-jiki, mirror breaker', 2).
card_toughness('kiki-jiki, mirror breaker', 2).

% Found in: SOK
card_name('kiku\'s shadow', 'Kiku\'s Shadow').
card_type('kiku\'s shadow', 'Sorcery').
card_types('kiku\'s shadow', ['Sorcery']).
card_subtypes('kiku\'s shadow', []).
card_colors('kiku\'s shadow', ['B']).
card_text('kiku\'s shadow', 'Target creature deals damage to itself equal to its power.').
card_mana_cost('kiku\'s shadow', ['B', 'B']).
card_cmc('kiku\'s shadow', 2).
card_layout('kiku\'s shadow', 'normal').

% Found in: CHK
card_name('kiku, night\'s flower', 'Kiku, Night\'s Flower').
card_type('kiku, night\'s flower', 'Legendary Creature — Human Assassin').
card_types('kiku, night\'s flower', ['Creature']).
card_subtypes('kiku, night\'s flower', ['Human', 'Assassin']).
card_supertypes('kiku, night\'s flower', ['Legendary']).
card_colors('kiku, night\'s flower', ['B']).
card_text('kiku, night\'s flower', '{2}{B}{B}, {T}: Target creature deals damage to itself equal to its power.').
card_mana_cost('kiku, night\'s flower', ['B', 'B']).
card_cmc('kiku, night\'s flower', 2).
card_layout('kiku, night\'s flower', 'normal').
card_power('kiku, night\'s flower', 1).
card_toughness('kiku, night\'s flower', 1).

% Found in: KTK
card_name('kill shot', 'Kill Shot').
card_type('kill shot', 'Instant').
card_types('kill shot', ['Instant']).
card_subtypes('kill shot', []).
card_colors('kill shot', ['W']).
card_text('kill shot', 'Destroy target attacking creature.').
card_mana_cost('kill shot', ['2', 'W']).
card_cmc('kill shot', 3).
card_layout('kill shot', 'normal').

% Found in: NMS
card_name('kill switch', 'Kill Switch').
card_type('kill switch', 'Artifact').
card_types('kill switch', ['Artifact']).
card_subtypes('kill switch', []).
card_colors('kill switch', []).
card_text('kill switch', '{2}, {T}: Tap all other artifacts. They don\'t untap during their controllers\' untap steps for as long as Kill Switch remains tapped.').
card_mana_cost('kill switch', ['3']).
card_cmc('kill switch', 3).
card_layout('kill switch', 'normal').

% Found in: UNH
card_name('kill! destroy!', 'Kill! Destroy!').
card_type('kill! destroy!', 'Instant').
card_types('kill! destroy!', ['Instant']).
card_subtypes('kill! destroy!', []).
card_colors('kill! destroy!', ['B']).
card_text('kill! destroy!', 'Destroy target nonblack creature.\nGotcha Whenever an opponent says \"Kill\" or \"Destroy,\" you may say \"Gotcha\" If you do, return Kill Destroy from your graveyard to your hand.').
card_mana_cost('kill! destroy!', ['1', 'B', 'B']).
card_cmc('kill! destroy!', 3).
card_layout('kill! destroy!', 'normal').

% Found in: DIS
card_name('kill-suit cultist', 'Kill-Suit Cultist').
card_type('kill-suit cultist', 'Creature — Goblin Berserker').
card_types('kill-suit cultist', ['Creature']).
card_subtypes('kill-suit cultist', ['Goblin', 'Berserker']).
card_colors('kill-suit cultist', ['R']).
card_text('kill-suit cultist', 'Kill-Suit Cultist attacks each turn if able.\n{B}, Sacrifice Kill-Suit Cultist: The next time damage would be dealt to target creature this turn, destroy that creature instead.').
card_mana_cost('kill-suit cultist', ['R']).
card_cmc('kill-suit cultist', 1).
card_layout('kill-suit cultist', 'normal').
card_power('kill-suit cultist', 1).
card_toughness('kill-suit cultist', 1).

% Found in: 4ED, 5ED, LEG, ME3
card_name('killer bees', 'Killer Bees').
card_type('killer bees', 'Creature — Insect').
card_types('killer bees', ['Creature']).
card_subtypes('killer bees', ['Insect']).
card_colors('killer bees', ['G']).
card_text('killer bees', 'Flying\n{G}: Killer Bees gets +1/+1 until end of turn.').
card_mana_cost('killer bees', ['1', 'G', 'G']).
card_cmc('killer bees', 3).
card_layout('killer bees', 'normal').
card_power('killer bees', 0).
card_toughness('killer bees', 1).

% Found in: GPT
card_name('killer instinct', 'Killer Instinct').
card_type('killer instinct', 'Enchantment').
card_types('killer instinct', ['Enchantment']).
card_subtypes('killer instinct', []).
card_colors('killer instinct', ['R', 'G']).
card_text('killer instinct', 'At the beginning of your upkeep, reveal the top card of your library. If it\'s a creature card, put it onto the battlefield. That creature gains haste until end of turn. Sacrifice it at the beginning of the next end step.').
card_mana_cost('killer instinct', ['4', 'R', 'G']).
card_cmc('killer instinct', 6).
card_layout('killer instinct', 'normal').

% Found in: BTD, EXO, TPR, VMA
card_name('killer whale', 'Killer Whale').
card_type('killer whale', 'Creature — Whale').
card_types('killer whale', ['Creature']).
card_subtypes('killer whale', ['Whale']).
card_colors('killer whale', ['U']).
card_text('killer whale', '{U}: Killer Whale gains flying until end of turn.').
card_mana_cost('killer whale', ['3', 'U', 'U']).
card_cmc('killer whale', 5).
card_layout('killer whale', 'normal').
card_power('killer whale', 3).
card_toughness('killer whale', 5).

% Found in: GTC
card_name('killing glare', 'Killing Glare').
card_type('killing glare', 'Instant').
card_types('killing glare', ['Instant']).
card_subtypes('killing glare', []).
card_colors('killing glare', ['B']).
card_text('killing glare', 'Destroy target creature with power X or less.').
card_mana_cost('killing glare', ['X', 'B']).
card_cmc('killing glare', 1).
card_layout('killing glare', 'normal').

% Found in: AVR, pMGD
card_name('killing wave', 'Killing Wave').
card_type('killing wave', 'Sorcery').
card_types('killing wave', ['Sorcery']).
card_subtypes('killing wave', []).
card_colors('killing wave', ['B']).
card_text('killing wave', 'For each creature, its controller sacrifices it unless he or she pays X life.').
card_mana_cost('killing wave', ['X', 'B']).
card_cmc('killing wave', 1).
card_layout('killing wave', 'normal').

% Found in: DDJ, ROE
card_name('kiln fiend', 'Kiln Fiend').
card_type('kiln fiend', 'Creature — Elemental Beast').
card_types('kiln fiend', ['Creature']).
card_subtypes('kiln fiend', ['Elemental', 'Beast']).
card_colors('kiln fiend', ['R']).
card_text('kiln fiend', 'Whenever you cast an instant or sorcery spell, Kiln Fiend gets +3/+0 until end of turn.').
card_mana_cost('kiln fiend', ['1', 'R']).
card_cmc('kiln fiend', 2).
card_layout('kiln fiend', 'normal').
card_power('kiln fiend', 1).
card_toughness('kiln fiend', 2).

% Found in: NPH
card_name('kiln walker', 'Kiln Walker').
card_type('kiln walker', 'Artifact Creature — Construct').
card_types('kiln walker', ['Artifact', 'Creature']).
card_subtypes('kiln walker', ['Construct']).
card_colors('kiln walker', []).
card_text('kiln walker', 'Whenever Kiln Walker attacks, it gets +3/+0 until end of turn.').
card_mana_cost('kiln walker', ['3']).
card_cmc('kiln walker', 3).
card_layout('kiln walker', 'normal').
card_power('kiln walker', 0).
card_toughness('kiln walker', 3).

% Found in: ARC, DDG, LGN
card_name('kilnmouth dragon', 'Kilnmouth Dragon').
card_type('kilnmouth dragon', 'Creature — Dragon').
card_types('kilnmouth dragon', ['Creature']).
card_subtypes('kilnmouth dragon', ['Dragon']).
card_colors('kilnmouth dragon', ['R']).
card_text('kilnmouth dragon', 'Amplify 3 (As this creature enters the battlefield, put three +1/+1 counters on it for each Dragon card you reveal in your hand.)\nFlying\n{T}: Kilnmouth Dragon deals damage equal to the number of +1/+1 counters on it to target creature or player.').
card_mana_cost('kilnmouth dragon', ['5', 'R', 'R']).
card_cmc('kilnmouth dragon', 7).
card_layout('kilnmouth dragon', 'normal').
card_power('kilnmouth dragon', 5).
card_toughness('kilnmouth dragon', 5).

% Found in: PC2
card_name('kilnspire district', 'Kilnspire District').
card_type('kilnspire district', 'Plane — Ravnica').
card_types('kilnspire district', ['Plane']).
card_subtypes('kilnspire district', ['Ravnica']).
card_colors('kilnspire district', []).
card_text('kilnspire district', 'When you planeswalk to Kilnspire District or at the beginning of your precombat main phase, put a charge counter on Kilnspire District, then add {R} to your mana pool for each charge counter on it.\nWhenever you roll {C}, you may pay {X}. If you do, Kilnspire District deals X damage to target creature or player.').
card_layout('kilnspire district', 'plane').

% Found in: KTK
card_name('kin-tree invocation', 'Kin-Tree Invocation').
card_type('kin-tree invocation', 'Sorcery').
card_types('kin-tree invocation', ['Sorcery']).
card_subtypes('kin-tree invocation', []).
card_colors('kin-tree invocation', ['B', 'G']).
card_text('kin-tree invocation', 'Put an X/X black and green Spirit Warrior creature token onto the battlefield, where X is the greatest toughness among creatures you control.').
card_mana_cost('kin-tree invocation', ['B', 'G']).
card_cmc('kin-tree invocation', 2).
card_layout('kin-tree invocation', 'normal').

% Found in: KTK
card_name('kin-tree warden', 'Kin-Tree Warden').
card_type('kin-tree warden', 'Creature — Human Warrior').
card_types('kin-tree warden', ['Creature']).
card_subtypes('kin-tree warden', ['Human', 'Warrior']).
card_colors('kin-tree warden', ['G']).
card_text('kin-tree warden', '{2}: Regenerate Kin-Tree Warden.\nMorph {G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('kin-tree warden', ['G']).
card_cmc('kin-tree warden', 1).
card_layout('kin-tree warden', 'normal').
card_power('kin-tree warden', 1).
card_toughness('kin-tree warden', 1).

% Found in: ISD
card_name('kindercatch', 'Kindercatch').
card_type('kindercatch', 'Creature — Spirit').
card_types('kindercatch', ['Creature']).
card_subtypes('kindercatch', ['Spirit']).
card_colors('kindercatch', ['G']).
card_text('kindercatch', '').
card_mana_cost('kindercatch', ['3', 'G', 'G', 'G']).
card_cmc('kindercatch', 6).
card_layout('kindercatch', 'normal').
card_power('kindercatch', 6).
card_toughness('kindercatch', 6).

% Found in: TMP, TPR, VMA
card_name('kindle', 'Kindle').
card_type('kindle', 'Instant').
card_types('kindle', ['Instant']).
card_subtypes('kindle', []).
card_colors('kindle', ['R']).
card_text('kindle', 'Kindle deals X damage to target creature or player, where X is 2 plus the number of cards named Kindle in all graveyards.').
card_mana_cost('kindle', ['1', 'R']).
card_cmc('kindle', 2).
card_layout('kindle', 'normal').

% Found in: DIS
card_name('kindle the carnage', 'Kindle the Carnage').
card_type('kindle the carnage', 'Sorcery').
card_types('kindle the carnage', ['Sorcery']).
card_subtypes('kindle the carnage', []).
card_colors('kindle the carnage', ['R']).
card_text('kindle the carnage', 'Discard a card at random. If you do, Kindle the Carnage deals damage equal to that card\'s converted mana cost to each creature. You may repeat this process any number of times.').
card_mana_cost('kindle the carnage', ['1', 'R', 'R']).
card_cmc('kindle the carnage', 3).
card_layout('kindle the carnage', 'normal').

% Found in: DTK, M10, M13, MOR
card_name('kindled fury', 'Kindled Fury').
card_type('kindled fury', 'Instant').
card_types('kindled fury', ['Instant']).
card_subtypes('kindled fury', []).
card_colors('kindled fury', ['R']).
card_text('kindled fury', 'Target creature gets +1/+0 and gains first strike until end of turn.').
card_mana_cost('kindled fury', ['R']).
card_cmc('kindled fury', 1).
card_layout('kindled fury', 'normal').

% Found in: 9ED, MGB, VIS
card_name('king cheetah', 'King Cheetah').
card_type('king cheetah', 'Creature — Cat').
card_types('king cheetah', ['Creature']).
card_subtypes('king cheetah', ['Cat']).
card_colors('king cheetah', ['G']).
card_text('king cheetah', 'Flash').
card_mana_cost('king cheetah', ['3', 'G']).
card_cmc('king cheetah', 4).
card_layout('king cheetah', 'normal').
card_power('king cheetah', 3).
card_toughness('king cheetah', 2).

% Found in: ULG
card_name('king crab', 'King Crab').
card_type('king crab', 'Creature — Crab').
card_types('king crab', ['Creature']).
card_subtypes('king crab', ['Crab']).
card_colors('king crab', ['U']).
card_text('king crab', '{1}{U}, {T}: Put target green creature on top of its owner\'s library.').
card_mana_cost('king crab', ['4', 'U', 'U']).
card_cmc('king crab', 6).
card_layout('king crab', 'normal').
card_power('king crab', 4).
card_toughness('king crab', 5).

% Found in: JOU
card_name('king macar, the gold-cursed', 'King Macar, the Gold-Cursed').
card_type('king macar, the gold-cursed', 'Legendary Creature — Human').
card_types('king macar, the gold-cursed', ['Creature']).
card_subtypes('king macar, the gold-cursed', ['Human']).
card_supertypes('king macar, the gold-cursed', ['Legendary']).
card_colors('king macar, the gold-cursed', ['B']).
card_text('king macar, the gold-cursed', 'Inspired — Whenever King Macar, the Gold-Cursed becomes untapped, you may exile target creature. If you do, put a colorless artifact token named Gold onto the battlefield. It has \"Sacrifice this artifact: Add one mana of any color to your mana pool.\"').
card_mana_cost('king macar, the gold-cursed', ['2', 'B', 'B']).
card_cmc('king macar, the gold-cursed', 4).
card_layout('king macar, the gold-cursed', 'normal').
card_power('king macar, the gold-cursed', 2).
card_toughness('king macar, the gold-cursed', 3).

% Found in: ARN
card_name('king suleiman', 'King Suleiman').
card_type('king suleiman', 'Creature — Human').
card_types('king suleiman', ['Creature']).
card_subtypes('king suleiman', ['Human']).
card_colors('king suleiman', ['W']).
card_text('king suleiman', '{T}: Destroy target Djinn or Efreet.').
card_mana_cost('king suleiman', ['1', 'W']).
card_cmc('king suleiman', 2).
card_layout('king suleiman', 'normal').
card_power('king suleiman', 1).
card_toughness('king suleiman', 1).
card_reserved('king suleiman').

% Found in: POR
card_name('king\'s assassin', 'King\'s Assassin').
card_type('king\'s assassin', 'Creature — Human Assassin').
card_types('king\'s assassin', ['Creature']).
card_subtypes('king\'s assassin', ['Human', 'Assassin']).
card_colors('king\'s assassin', ['B']).
card_text('king\'s assassin', '{T}: Destroy target tapped creature. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('king\'s assassin', ['1', 'B', 'B']).
card_cmc('king\'s assassin', 3).
card_layout('king\'s assassin', 'normal').
card_power('king\'s assassin', 1).
card_toughness('king\'s assassin', 1).

% Found in: UDS
card_name('kingfisher', 'Kingfisher').
card_type('kingfisher', 'Creature — Bird').
card_types('kingfisher', ['Creature']).
card_subtypes('kingfisher', ['Bird']).
card_colors('kingfisher', ['U']).
card_text('kingfisher', 'Flying\nWhen Kingfisher dies, draw a card.').
card_mana_cost('kingfisher', ['3', 'U']).
card_cmc('kingfisher', 4).
card_layout('kingfisher', 'normal').
card_power('kingfisher', 2).
card_toughness('kingfisher', 2).

% Found in: GTC
card_name('kingpin\'s pet', 'Kingpin\'s Pet').
card_type('kingpin\'s pet', 'Creature — Thrull').
card_types('kingpin\'s pet', ['Creature']).
card_subtypes('kingpin\'s pet', ['Thrull']).
card_colors('kingpin\'s pet', ['W', 'B']).
card_text('kingpin\'s pet', 'Flying\nExtort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)').
card_mana_cost('kingpin\'s pet', ['1', 'W', 'B']).
card_cmc('kingpin\'s pet', 3).
card_layout('kingpin\'s pet', 'normal').
card_power('kingpin\'s pet', 2).
card_toughness('kingpin\'s pet', 2).

% Found in: LRW
card_name('kinsbaile balloonist', 'Kinsbaile Balloonist').
card_type('kinsbaile balloonist', 'Creature — Kithkin Soldier').
card_types('kinsbaile balloonist', ['Creature']).
card_subtypes('kinsbaile balloonist', ['Kithkin', 'Soldier']).
card_colors('kinsbaile balloonist', ['W']).
card_text('kinsbaile balloonist', 'Flying\nWhenever Kinsbaile Balloonist attacks, you may have target creature gain flying until end of turn.').
card_mana_cost('kinsbaile balloonist', ['3', 'W']).
card_cmc('kinsbaile balloonist', 4).
card_layout('kinsbaile balloonist', 'normal').
card_power('kinsbaile balloonist', 2).
card_toughness('kinsbaile balloonist', 2).

% Found in: MOR
card_name('kinsbaile borderguard', 'Kinsbaile Borderguard').
card_type('kinsbaile borderguard', 'Creature — Kithkin Soldier').
card_types('kinsbaile borderguard', ['Creature']).
card_subtypes('kinsbaile borderguard', ['Kithkin', 'Soldier']).
card_colors('kinsbaile borderguard', ['W']).
card_text('kinsbaile borderguard', 'Kinsbaile Borderguard enters the battlefield with a +1/+1 counter on it for each other Kithkin you control.\nWhen Kinsbaile Borderguard dies, put a 1/1 white Kithkin Soldier creature token onto the battlefield for each counter on it.').
card_mana_cost('kinsbaile borderguard', ['1', 'W', 'W']).
card_cmc('kinsbaile borderguard', 3).
card_layout('kinsbaile borderguard', 'normal').
card_power('kinsbaile borderguard', 1).
card_toughness('kinsbaile borderguard', 1).

% Found in: DDG, MOR
card_name('kinsbaile cavalier', 'Kinsbaile Cavalier').
card_type('kinsbaile cavalier', 'Creature — Kithkin Knight').
card_types('kinsbaile cavalier', ['Creature']).
card_subtypes('kinsbaile cavalier', ['Kithkin', 'Knight']).
card_colors('kinsbaile cavalier', ['W']).
card_text('kinsbaile cavalier', 'Knight creatures you control have double strike.').
card_mana_cost('kinsbaile cavalier', ['3', 'W']).
card_cmc('kinsbaile cavalier', 4).
card_layout('kinsbaile cavalier', 'normal').
card_power('kinsbaile cavalier', 2).
card_toughness('kinsbaile cavalier', 2).

% Found in: DDO, LRW, M15
card_name('kinsbaile skirmisher', 'Kinsbaile Skirmisher').
card_type('kinsbaile skirmisher', 'Creature — Kithkin Soldier').
card_types('kinsbaile skirmisher', ['Creature']).
card_subtypes('kinsbaile skirmisher', ['Kithkin', 'Soldier']).
card_colors('kinsbaile skirmisher', ['W']).
card_text('kinsbaile skirmisher', 'When Kinsbaile Skirmisher enters the battlefield, target creature gets +1/+1 until end of turn.').
card_mana_cost('kinsbaile skirmisher', ['1', 'W']).
card_cmc('kinsbaile skirmisher', 2).
card_layout('kinsbaile skirmisher', 'normal').
card_power('kinsbaile skirmisher', 2).
card_toughness('kinsbaile skirmisher', 2).

% Found in: SHM
card_name('kinscaer harpoonist', 'Kinscaer Harpoonist').
card_type('kinscaer harpoonist', 'Creature — Kithkin Soldier').
card_types('kinscaer harpoonist', ['Creature']).
card_subtypes('kinscaer harpoonist', ['Kithkin', 'Soldier']).
card_colors('kinscaer harpoonist', ['U']).
card_text('kinscaer harpoonist', 'Flying\nWhenever Kinscaer Harpoonist attacks, you may have target creature lose flying until end of turn.').
card_mana_cost('kinscaer harpoonist', ['3', 'U']).
card_cmc('kinscaer harpoonist', 4).
card_layout('kinscaer harpoonist', 'normal').
card_power('kinscaer harpoonist', 2).
card_toughness('kinscaer harpoonist', 2).

% Found in: JOU
card_name('kiora\'s dismissal', 'Kiora\'s Dismissal').
card_type('kiora\'s dismissal', 'Instant').
card_types('kiora\'s dismissal', ['Instant']).
card_subtypes('kiora\'s dismissal', []).
card_colors('kiora\'s dismissal', ['U']).
card_text('kiora\'s dismissal', 'Strive — Kiora\'s Dismissal costs {U} more to cast for each target beyond the first.\nReturn any number of target enchantments to their owners\' hands.').
card_mana_cost('kiora\'s dismissal', ['U']).
card_cmc('kiora\'s dismissal', 1).
card_layout('kiora\'s dismissal', 'normal').

% Found in: BNG, DDO, pMGD
card_name('kiora\'s follower', 'Kiora\'s Follower').
card_type('kiora\'s follower', 'Creature — Merfolk').
card_types('kiora\'s follower', ['Creature']).
card_subtypes('kiora\'s follower', ['Merfolk']).
card_colors('kiora\'s follower', ['U', 'G']).
card_text('kiora\'s follower', '{T}: Untap another target permanent.').
card_mana_cost('kiora\'s follower', ['G', 'U']).
card_cmc('kiora\'s follower', 2).
card_layout('kiora\'s follower', 'normal').
card_power('kiora\'s follower', 2).
card_toughness('kiora\'s follower', 2).

% Found in: BFZ
card_name('kiora, master of the depths', 'Kiora, Master of the Depths').
card_type('kiora, master of the depths', 'Planeswalker — Kiora').
card_types('kiora, master of the depths', ['Planeswalker']).
card_subtypes('kiora, master of the depths', ['Kiora']).
card_colors('kiora, master of the depths', ['U', 'G']).
card_text('kiora, master of the depths', '+1: Untap up to one target creature and up to one target land.\n−2: Reveal the top four cards of your library. You may put a creature card and/or a land card from among them into your hand. Put the rest into your graveyard.\n−8: You get an emblem with \"Whenever a creature enters the battlefield under your control, you may have it fight target creature.\" Then put three 8/8 blue Octopus creature tokens onto the battlefield.').
card_mana_cost('kiora, master of the depths', ['2', 'G', 'U']).
card_cmc('kiora, master of the depths', 4).
card_layout('kiora, master of the depths', 'normal').
card_loyalty('kiora, master of the depths', 4).

% Found in: BNG, DDO
card_name('kiora, the crashing wave', 'Kiora, the Crashing Wave').
card_type('kiora, the crashing wave', 'Planeswalker — Kiora').
card_types('kiora, the crashing wave', ['Planeswalker']).
card_subtypes('kiora, the crashing wave', ['Kiora']).
card_colors('kiora, the crashing wave', ['U', 'G']).
card_text('kiora, the crashing wave', '+1: Until your next turn, prevent all damage that would be dealt to and dealt by target permanent an opponent controls.\n−1: Draw a card. You may play an additional land this turn.\n−5: You get an emblem with \"At the beginning of your end step, put a 9/9 blue Kraken creature token onto the battlefield.\"').
card_mana_cost('kiora, the crashing wave', ['2', 'G', 'U']).
card_cmc('kiora, the crashing wave', 4).
card_layout('kiora, the crashing wave', 'normal').
card_loyalty('kiora, the crashing wave', 2).

% Found in: BOK, MMA
card_name('kira, great glass-spinner', 'Kira, Great Glass-Spinner').
card_type('kira, great glass-spinner', 'Legendary Creature — Spirit').
card_types('kira, great glass-spinner', ['Creature']).
card_subtypes('kira, great glass-spinner', ['Spirit']).
card_supertypes('kira, great glass-spinner', ['Legendary']).
card_colors('kira, great glass-spinner', ['U']).
card_text('kira, great glass-spinner', 'Flying\nCreatures you control have \"Whenever this creature becomes the target of a spell or ability for the first time in a turn, counter that spell or ability.\"').
card_mana_cost('kira, great glass-spinner', ['1', 'U', 'U']).
card_cmc('kira, great glass-spinner', 3).
card_layout('kira, great glass-spinner', 'normal').
card_power('kira, great glass-spinner', 2).
card_toughness('kira, great glass-spinner', 2).

% Found in: 3ED, 9ED, ARN, BTD, DDH, V09, pFNM
card_name('kird ape', 'Kird Ape').
card_type('kird ape', 'Creature — Ape').
card_types('kird ape', ['Creature']).
card_subtypes('kird ape', ['Ape']).
card_colors('kird ape', ['R']).
card_text('kird ape', 'Kird Ape gets +1/+2 as long as you control a Forest.').
card_mana_cost('kird ape', ['R']).
card_cmc('kird ape', 1).
card_layout('kird ape', 'normal').
card_power('kird ape', 1).
card_toughness('kird ape', 1).

% Found in: M15
card_name('kird chieftain', 'Kird Chieftain').
card_type('kird chieftain', 'Creature — Ape').
card_types('kird chieftain', ['Creature']).
card_subtypes('kird chieftain', ['Ape']).
card_colors('kird chieftain', ['R']).
card_text('kird chieftain', 'Kird Chieftain gets +1/+1 as long as you control a Forest.\n{4}{G}: Target creature gets +2/+2 and gains trample until end of turn. (If it would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('kird chieftain', ['3', 'R']).
card_cmc('kird chieftain', 4).
card_layout('kird chieftain', 'normal').
card_power('kird chieftain', 3).
card_toughness('kird chieftain', 3).

% Found in: SOK
card_name('kiri-onna', 'Kiri-Onna').
card_type('kiri-onna', 'Creature — Spirit').
card_types('kiri-onna', ['Creature']).
card_subtypes('kiri-onna', ['Spirit']).
card_colors('kiri-onna', ['U']).
card_text('kiri-onna', 'When Kiri-Onna enters the battlefield, return target creature to its owner\'s hand.\nWhenever you cast a Spirit or Arcane spell, you may return Kiri-Onna to its owner\'s hand.').
card_mana_cost('kiri-onna', ['4', 'U']).
card_cmc('kiri-onna', 5).
card_layout('kiri-onna', 'normal').
card_power('kiri-onna', 2).
card_toughness('kiri-onna', 2).

% Found in: ODY
card_name('kirtar\'s desire', 'Kirtar\'s Desire').
card_type('kirtar\'s desire', 'Enchantment — Aura').
card_types('kirtar\'s desire', ['Enchantment']).
card_subtypes('kirtar\'s desire', ['Aura']).
card_colors('kirtar\'s desire', ['W']).
card_text('kirtar\'s desire', 'Enchant creature\nEnchanted creature can\'t attack.\nThreshold — Enchanted creature can\'t block as long as seven or more cards are in your graveyard.').
card_mana_cost('kirtar\'s desire', ['W']).
card_cmc('kirtar\'s desire', 1).
card_layout('kirtar\'s desire', 'normal').

% Found in: C13, ODY
card_name('kirtar\'s wrath', 'Kirtar\'s Wrath').
card_type('kirtar\'s wrath', 'Sorcery').
card_types('kirtar\'s wrath', ['Sorcery']).
card_subtypes('kirtar\'s wrath', []).
card_colors('kirtar\'s wrath', ['W']).
card_text('kirtar\'s wrath', 'Destroy all creatures. They can\'t be regenerated.\nThreshold — If seven or more cards are in your graveyard, instead destroy all creatures, then put two 1/1 white Spirit creature tokens with flying onto the battlefield. Creatures destroyed this way can\'t be regenerated.').
card_mana_cost('kirtar\'s wrath', ['4', 'W', 'W']).
card_cmc('kirtar\'s wrath', 6).
card_layout('kirtar\'s wrath', 'normal').

% Found in: 4ED, 5ED, 6ED, LEG, ME4
card_name('kismet', 'Kismet').
card_type('kismet', 'Enchantment').
card_types('kismet', ['Enchantment']).
card_subtypes('kismet', []).
card_colors('kismet', ['W']).
card_text('kismet', 'Artifacts, creatures, and lands played by your opponents enter the battlefield tapped.').
card_mana_cost('kismet', ['3', 'W']).
card_cmc('kismet', 4).
card_layout('kismet', 'normal').

% Found in: PO2
card_name('kiss of death', 'Kiss of Death').
card_type('kiss of death', 'Sorcery').
card_types('kiss of death', ['Sorcery']).
card_subtypes('kiss of death', []).
card_colors('kiss of death', ['B']).
card_text('kiss of death', 'Kiss of Death deals 4 damage to target opponent. You gain 4 life.').
card_mana_cost('kiss of death', ['4', 'B', 'B']).
card_cmc('kiss of death', 6).
card_layout('kiss of death', 'normal').

% Found in: ALA
card_name('kiss of the amesha', 'Kiss of the Amesha').
card_type('kiss of the amesha', 'Sorcery').
card_types('kiss of the amesha', ['Sorcery']).
card_subtypes('kiss of the amesha', []).
card_colors('kiss of the amesha', ['W', 'U']).
card_text('kiss of the amesha', 'Target player gains 7 life and draws two cards.').
card_mana_cost('kiss of the amesha', ['4', 'W', 'U']).
card_cmc('kiss of the amesha', 6).
card_layout('kiss of the amesha', 'normal').

% Found in: MMA, SHM, pFNM
card_name('kitchen finks', 'Kitchen Finks').
card_type('kitchen finks', 'Creature — Ouphe').
card_types('kitchen finks', ['Creature']).
card_subtypes('kitchen finks', ['Ouphe']).
card_colors('kitchen finks', ['W', 'G']).
card_text('kitchen finks', 'When Kitchen Finks enters the battlefield, you gain 2 life.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('kitchen finks', ['1', 'G/W', 'G/W']).
card_cmc('kitchen finks', 3).
card_layout('kitchen finks', 'normal').
card_power('kitchen finks', 3).
card_toughness('kitchen finks', 2).

% Found in: M12
card_name('kite shield', 'Kite Shield').
card_type('kite shield', 'Artifact — Equipment').
card_types('kite shield', ['Artifact']).
card_subtypes('kite shield', ['Equipment']).
card_colors('kite shield', []).
card_text('kite shield', 'Equipped creature gets +0/+3.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('kite shield', ['0']).
card_cmc('kite shield', 0).
card_layout('kite shield', 'normal').

% Found in: M13, MM2, WWK
card_name('kitesail', 'Kitesail').
card_type('kitesail', 'Artifact — Equipment').
card_types('kitesail', ['Artifact']).
card_subtypes('kitesail', ['Equipment']).
card_colors('kitesail', []).
card_text('kitesail', 'Equipped creature gets +1/+0 and has flying.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('kitesail', ['2']).
card_cmc('kitesail', 2).
card_layout('kitesail', 'normal').

% Found in: WWK
card_name('kitesail apprentice', 'Kitesail Apprentice').
card_type('kitesail apprentice', 'Creature — Kor Soldier').
card_types('kitesail apprentice', ['Creature']).
card_subtypes('kitesail apprentice', ['Kor', 'Soldier']).
card_colors('kitesail apprentice', ['W']).
card_text('kitesail apprentice', 'As long as Kitesail Apprentice is equipped, it gets +1/+1 and has flying.').
card_mana_cost('kitesail apprentice', ['W']).
card_cmc('kitesail apprentice', 1).
card_layout('kitesail apprentice', 'normal').
card_power('kitesail apprentice', 1).
card_toughness('kitesail apprentice', 1).

% Found in: BFZ
card_name('kitesail scout', 'Kitesail Scout').
card_type('kitesail scout', 'Creature — Kor Scout').
card_types('kitesail scout', ['Creature']).
card_subtypes('kitesail scout', ['Kor', 'Scout']).
card_colors('kitesail scout', ['W']).
card_text('kitesail scout', 'Flying').
card_mana_cost('kitesail scout', ['W']).
card_cmc('kitesail scout', 1).
card_layout('kitesail scout', 'normal').
card_power('kitesail scout', 1).
card_toughness('kitesail scout', 1).

% Found in: WTH
card_name('kithkin armor', 'Kithkin Armor').
card_type('kithkin armor', 'Enchantment — Aura').
card_types('kithkin armor', ['Enchantment']).
card_subtypes('kithkin armor', ['Aura']).
card_colors('kithkin armor', ['W']).
card_text('kithkin armor', 'Enchant creature\nEnchanted creature can\'t be blocked by creatures with power 3 or greater.\nSacrifice Kithkin Armor: The next time a source of your choice would deal damage to enchanted creature this turn, prevent that damage.').
card_mana_cost('kithkin armor', ['W']).
card_cmc('kithkin armor', 1).
card_layout('kithkin armor', 'normal').

% Found in: LRW
card_name('kithkin daggerdare', 'Kithkin Daggerdare').
card_type('kithkin daggerdare', 'Creature — Kithkin Soldier').
card_types('kithkin daggerdare', ['Creature']).
card_subtypes('kithkin daggerdare', ['Kithkin', 'Soldier']).
card_colors('kithkin daggerdare', ['G']).
card_text('kithkin daggerdare', '{G}, {T}: Target attacking creature gets +2/+2 until end of turn.').
card_mana_cost('kithkin daggerdare', ['1', 'G']).
card_cmc('kithkin daggerdare', 2).
card_layout('kithkin daggerdare', 'normal').
card_power('kithkin daggerdare', 1).
card_toughness('kithkin daggerdare', 1).

% Found in: LRW, MMA
card_name('kithkin greatheart', 'Kithkin Greatheart').
card_type('kithkin greatheart', 'Creature — Kithkin Soldier').
card_types('kithkin greatheart', ['Creature']).
card_subtypes('kithkin greatheart', ['Kithkin', 'Soldier']).
card_colors('kithkin greatheart', ['W']).
card_text('kithkin greatheart', 'As long as you control a Giant, Kithkin Greatheart gets +1/+1 and has first strike.').
card_mana_cost('kithkin greatheart', ['1', 'W']).
card_cmc('kithkin greatheart', 2).
card_layout('kithkin greatheart', 'normal').
card_power('kithkin greatheart', 2).
card_toughness('kithkin greatheart', 1).

% Found in: LRW
card_name('kithkin harbinger', 'Kithkin Harbinger').
card_type('kithkin harbinger', 'Creature — Kithkin Wizard').
card_types('kithkin harbinger', ['Creature']).
card_subtypes('kithkin harbinger', ['Kithkin', 'Wizard']).
card_colors('kithkin harbinger', ['W']).
card_text('kithkin harbinger', 'When Kithkin Harbinger enters the battlefield, you may search your library for a Kithkin card, reveal it, then shuffle your library and put that card on top of it.').
card_mana_cost('kithkin harbinger', ['2', 'W']).
card_cmc('kithkin harbinger', 3).
card_layout('kithkin harbinger', 'normal').
card_power('kithkin harbinger', 1).
card_toughness('kithkin harbinger', 3).

% Found in: LRW
card_name('kithkin healer', 'Kithkin Healer').
card_type('kithkin healer', 'Creature — Kithkin Cleric').
card_types('kithkin healer', ['Creature']).
card_subtypes('kithkin healer', ['Kithkin', 'Cleric']).
card_colors('kithkin healer', ['W']).
card_text('kithkin healer', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_mana_cost('kithkin healer', ['2', 'W']).
card_cmc('kithkin healer', 3).
card_layout('kithkin healer', 'normal').
card_power('kithkin healer', 2).
card_toughness('kithkin healer', 2).

% Found in: LRW
card_name('kithkin mourncaller', 'Kithkin Mourncaller').
card_type('kithkin mourncaller', 'Creature — Kithkin Scout').
card_types('kithkin mourncaller', ['Creature']).
card_subtypes('kithkin mourncaller', ['Kithkin', 'Scout']).
card_colors('kithkin mourncaller', ['G']).
card_text('kithkin mourncaller', 'Whenever an attacking Kithkin or Elf is put into your graveyard from the battlefield, you may draw a card.').
card_mana_cost('kithkin mourncaller', ['2', 'G']).
card_cmc('kithkin mourncaller', 3).
card_layout('kithkin mourncaller', 'normal').
card_power('kithkin mourncaller', 2).
card_toughness('kithkin mourncaller', 2).

% Found in: SHM
card_name('kithkin rabble', 'Kithkin Rabble').
card_type('kithkin rabble', 'Creature — Kithkin').
card_types('kithkin rabble', ['Creature']).
card_subtypes('kithkin rabble', ['Kithkin']).
card_colors('kithkin rabble', ['W']).
card_text('kithkin rabble', 'Vigilance\nKithkin Rabble\'s power and toughness are each equal to the number of white permanents you control.').
card_mana_cost('kithkin rabble', ['3', 'W']).
card_cmc('kithkin rabble', 4).
card_layout('kithkin rabble', 'normal').
card_power('kithkin rabble', '*').
card_toughness('kithkin rabble', '*').

% Found in: SHM
card_name('kithkin shielddare', 'Kithkin Shielddare').
card_type('kithkin shielddare', 'Creature — Kithkin Soldier').
card_types('kithkin shielddare', ['Creature']).
card_subtypes('kithkin shielddare', ['Kithkin', 'Soldier']).
card_colors('kithkin shielddare', ['W']).
card_text('kithkin shielddare', '{W}, {T}: Target blocking creature gets +2/+2 until end of turn.').
card_mana_cost('kithkin shielddare', ['1', 'W']).
card_cmc('kithkin shielddare', 2).
card_layout('kithkin shielddare', 'normal').
card_power('kithkin shielddare', 1).
card_toughness('kithkin shielddare', 1).

% Found in: EVE
card_name('kithkin spellduster', 'Kithkin Spellduster').
card_type('kithkin spellduster', 'Creature — Kithkin Wizard').
card_types('kithkin spellduster', ['Creature']).
card_subtypes('kithkin spellduster', ['Kithkin', 'Wizard']).
card_colors('kithkin spellduster', ['W']).
card_text('kithkin spellduster', 'Flying\n{1}{W}, Sacrifice Kithkin Spellduster: Destroy target enchantment.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('kithkin spellduster', ['4', 'W']).
card_cmc('kithkin spellduster', 5).
card_layout('kithkin spellduster', 'normal').
card_power('kithkin spellduster', 2).
card_toughness('kithkin spellduster', 3).

% Found in: EVE
card_name('kithkin zealot', 'Kithkin Zealot').
card_type('kithkin zealot', 'Creature — Kithkin Cleric').
card_types('kithkin zealot', ['Creature']).
card_subtypes('kithkin zealot', ['Kithkin', 'Cleric']).
card_colors('kithkin zealot', ['W']).
card_text('kithkin zealot', 'When Kithkin Zealot enters the battlefield, you gain 1 life for each black and/or red permanent target opponent controls.').
card_mana_cost('kithkin zealot', ['1', 'W']).
card_cmc('kithkin zealot', 2).
card_layout('kithkin zealot', 'normal').
card_power('kithkin zealot', 1).
card_toughness('kithkin zealot', 3).

% Found in: MOR
card_name('kithkin zephyrnaut', 'Kithkin Zephyrnaut').
card_type('kithkin zephyrnaut', 'Creature — Kithkin Soldier').
card_types('kithkin zephyrnaut', ['Creature']).
card_subtypes('kithkin zephyrnaut', ['Kithkin', 'Soldier']).
card_colors('kithkin zephyrnaut', ['W']).
card_text('kithkin zephyrnaut', 'Kinship — At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Kithkin Zephyrnaut, you may reveal it. If you do, Kithkin Zephyrnaut gets +2/+2 and gains flying and vigilance until end of turn.').
card_mana_cost('kithkin zephyrnaut', ['2', 'W']).
card_cmc('kithkin zephyrnaut', 3).
card_layout('kithkin zephyrnaut', 'normal').
card_power('kithkin zephyrnaut', 2).
card_toughness('kithkin zephyrnaut', 2).

% Found in: CHK
card_name('kitsune blademaster', 'Kitsune Blademaster').
card_type('kitsune blademaster', 'Creature — Fox Samurai').
card_types('kitsune blademaster', ['Creature']).
card_subtypes('kitsune blademaster', ['Fox', 'Samurai']).
card_colors('kitsune blademaster', ['W']).
card_text('kitsune blademaster', 'First strike\nBushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_mana_cost('kitsune blademaster', ['2', 'W']).
card_cmc('kitsune blademaster', 3).
card_layout('kitsune blademaster', 'normal').
card_power('kitsune blademaster', 2).
card_toughness('kitsune blademaster', 2).

% Found in: SOK
card_name('kitsune bonesetter', 'Kitsune Bonesetter').
card_type('kitsune bonesetter', 'Creature — Fox Cleric').
card_types('kitsune bonesetter', ['Creature']).
card_subtypes('kitsune bonesetter', ['Fox', 'Cleric']).
card_colors('kitsune bonesetter', ['W']).
card_text('kitsune bonesetter', '{T}: Prevent the next 3 damage that would be dealt to target creature this turn. Activate this ability only if you have more cards in hand than each opponent.').
card_mana_cost('kitsune bonesetter', ['2', 'W']).
card_cmc('kitsune bonesetter', 3).
card_layout('kitsune bonesetter', 'normal').
card_power('kitsune bonesetter', 0).
card_toughness('kitsune bonesetter', 1).

% Found in: SOK
card_name('kitsune dawnblade', 'Kitsune Dawnblade').
card_type('kitsune dawnblade', 'Creature — Fox Samurai').
card_types('kitsune dawnblade', ['Creature']).
card_subtypes('kitsune dawnblade', ['Fox', 'Samurai']).
card_colors('kitsune dawnblade', ['W']).
card_text('kitsune dawnblade', 'Bushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)\nWhen Kitsune Dawnblade enters the battlefield, you may tap target creature.').
card_mana_cost('kitsune dawnblade', ['4', 'W']).
card_cmc('kitsune dawnblade', 5).
card_layout('kitsune dawnblade', 'normal').
card_power('kitsune dawnblade', 2).
card_toughness('kitsune dawnblade', 3).

% Found in: CHK
card_name('kitsune diviner', 'Kitsune Diviner').
card_type('kitsune diviner', 'Creature — Fox Cleric').
card_types('kitsune diviner', ['Creature']).
card_subtypes('kitsune diviner', ['Fox', 'Cleric']).
card_colors('kitsune diviner', ['W']).
card_text('kitsune diviner', '{T}: Tap target Spirit.').
card_mana_cost('kitsune diviner', ['W']).
card_cmc('kitsune diviner', 1).
card_layout('kitsune diviner', 'normal').
card_power('kitsune diviner', 0).
card_toughness('kitsune diviner', 1).

% Found in: CHK
card_name('kitsune healer', 'Kitsune Healer').
card_type('kitsune healer', 'Creature — Fox Cleric').
card_types('kitsune healer', ['Creature']).
card_subtypes('kitsune healer', ['Fox', 'Cleric']).
card_colors('kitsune healer', ['W']).
card_text('kitsune healer', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\n{T}: Prevent all damage that would be dealt to target legendary creature this turn.').
card_mana_cost('kitsune healer', ['3', 'W']).
card_cmc('kitsune healer', 4).
card_layout('kitsune healer', 'normal').
card_power('kitsune healer', 2).
card_toughness('kitsune healer', 2).

% Found in: SOK
card_name('kitsune loreweaver', 'Kitsune Loreweaver').
card_type('kitsune loreweaver', 'Creature — Fox Cleric').
card_types('kitsune loreweaver', ['Creature']).
card_subtypes('kitsune loreweaver', ['Fox', 'Cleric']).
card_colors('kitsune loreweaver', ['W']).
card_text('kitsune loreweaver', '{1}{W}: Kitsune Loreweaver gets +0/+X until end of turn, where X is the number of cards in your hand.').
card_mana_cost('kitsune loreweaver', ['1', 'W']).
card_cmc('kitsune loreweaver', 2).
card_layout('kitsune loreweaver', 'normal').
card_power('kitsune loreweaver', 2).
card_toughness('kitsune loreweaver', 1).

% Found in: CHK
card_name('kitsune mystic', 'Kitsune Mystic').
card_type('kitsune mystic', 'Creature — Fox Wizard').
card_types('kitsune mystic', ['Creature']).
card_subtypes('kitsune mystic', ['Fox', 'Wizard']).
card_colors('kitsune mystic', ['W']).
card_text('kitsune mystic', 'At the beginning of the end step, if Kitsune Mystic is enchanted by two or more Auras, flip it.').
card_mana_cost('kitsune mystic', ['3', 'W']).
card_cmc('kitsune mystic', 4).
card_layout('kitsune mystic', 'flip').
card_power('kitsune mystic', 2).
card_toughness('kitsune mystic', 3).
card_sides('kitsune mystic', 'autumn-tail, kitsune sage').

% Found in: BOK
card_name('kitsune palliator', 'Kitsune Palliator').
card_type('kitsune palliator', 'Creature — Fox Cleric').
card_types('kitsune palliator', ['Creature']).
card_subtypes('kitsune palliator', ['Fox', 'Cleric']).
card_colors('kitsune palliator', ['W']).
card_text('kitsune palliator', '{T}: Prevent the next 1 damage that would be dealt to each creature and each player this turn.').
card_mana_cost('kitsune palliator', ['2', 'W']).
card_cmc('kitsune palliator', 3).
card_layout('kitsune palliator', 'normal').
card_power('kitsune palliator', 0).
card_toughness('kitsune palliator', 2).

% Found in: CHK
card_name('kitsune riftwalker', 'Kitsune Riftwalker').
card_type('kitsune riftwalker', 'Creature — Fox Wizard').
card_types('kitsune riftwalker', ['Creature']).
card_subtypes('kitsune riftwalker', ['Fox', 'Wizard']).
card_colors('kitsune riftwalker', ['W']).
card_text('kitsune riftwalker', 'Protection from Spirits and from Arcane').
card_mana_cost('kitsune riftwalker', ['1', 'W', 'W']).
card_cmc('kitsune riftwalker', 3).
card_layout('kitsune riftwalker', 'normal').
card_power('kitsune riftwalker', 2).
card_toughness('kitsune riftwalker', 1).

% Found in: SOK, pPRE
card_name('kiyomaro, first to stand', 'Kiyomaro, First to Stand').
card_type('kiyomaro, first to stand', 'Legendary Creature — Spirit').
card_types('kiyomaro, first to stand', ['Creature']).
card_subtypes('kiyomaro, first to stand', ['Spirit']).
card_supertypes('kiyomaro, first to stand', ['Legendary']).
card_colors('kiyomaro, first to stand', ['W']).
card_text('kiyomaro, first to stand', 'Kiyomaro, First to Stand\'s power and toughness are each equal to the number of cards in your hand.\nAs long as you have four or more cards in hand, Kiyomaro has vigilance.\nWhenever Kiyomaro deals damage, if you have seven or more cards in hand, you gain 7 life.').
card_mana_cost('kiyomaro, first to stand', ['3', 'W', 'W']).
card_cmc('kiyomaro, first to stand', 5).
card_layout('kiyomaro, first to stand', 'normal').
card_power('kiyomaro, first to stand', '*').
card_toughness('kiyomaro, first to stand', '*').

% Found in: 5ED, 6ED, CST, ICE, ME2
card_name('kjeldoran dead', 'Kjeldoran Dead').
card_type('kjeldoran dead', 'Creature — Skeleton').
card_types('kjeldoran dead', ['Creature']).
card_subtypes('kjeldoran dead', ['Skeleton']).
card_colors('kjeldoran dead', ['B']).
card_text('kjeldoran dead', 'When Kjeldoran Dead enters the battlefield, sacrifice a creature.\n{B}: Regenerate Kjeldoran Dead.').
card_mana_cost('kjeldoran dead', ['B']).
card_cmc('kjeldoran dead', 1).
card_layout('kjeldoran dead', 'normal').
card_power('kjeldoran dead', 3).
card_toughness('kjeldoran dead', 1).

% Found in: CST, ICE, ME2
card_name('kjeldoran elite guard', 'Kjeldoran Elite Guard').
card_type('kjeldoran elite guard', 'Creature — Human Soldier').
card_types('kjeldoran elite guard', ['Creature']).
card_subtypes('kjeldoran elite guard', ['Human', 'Soldier']).
card_colors('kjeldoran elite guard', ['W']).
card_text('kjeldoran elite guard', '{T}: Target creature gets +2/+2 until end of turn. When that creature leaves the battlefield this turn, sacrifice Kjeldoran Elite Guard. Activate this ability only during combat.').
card_mana_cost('kjeldoran elite guard', ['3', 'W']).
card_cmc('kjeldoran elite guard', 4).
card_layout('kjeldoran elite guard', 'normal').
card_power('kjeldoran elite guard', 2).
card_toughness('kjeldoran elite guard', 2).

% Found in: ALL
card_name('kjeldoran escort', 'Kjeldoran Escort').
card_type('kjeldoran escort', 'Creature — Human Soldier').
card_types('kjeldoran escort', ['Creature']).
card_subtypes('kjeldoran escort', ['Human', 'Soldier']).
card_colors('kjeldoran escort', ['W']).
card_text('kjeldoran escort', 'Banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('kjeldoran escort', ['2', 'W', 'W']).
card_cmc('kjeldoran escort', 4).
card_layout('kjeldoran escort', 'normal').
card_power('kjeldoran escort', 2).
card_toughness('kjeldoran escort', 3).

% Found in: ICE, ME3
card_name('kjeldoran frostbeast', 'Kjeldoran Frostbeast').
card_type('kjeldoran frostbeast', 'Creature — Elemental Beast').
card_types('kjeldoran frostbeast', ['Creature']).
card_subtypes('kjeldoran frostbeast', ['Elemental', 'Beast']).
card_colors('kjeldoran frostbeast', ['W', 'G']).
card_text('kjeldoran frostbeast', 'At end of combat, destroy all creatures blocking or blocked by Kjeldoran Frostbeast.').
card_mana_cost('kjeldoran frostbeast', ['3', 'G', 'W']).
card_cmc('kjeldoran frostbeast', 5).
card_layout('kjeldoran frostbeast', 'normal').
card_power('kjeldoran frostbeast', 2).
card_toughness('kjeldoran frostbeast', 4).

% Found in: CSP
card_name('kjeldoran gargoyle', 'Kjeldoran Gargoyle').
card_type('kjeldoran gargoyle', 'Creature — Gargoyle').
card_types('kjeldoran gargoyle', ['Creature']).
card_subtypes('kjeldoran gargoyle', ['Gargoyle']).
card_colors('kjeldoran gargoyle', ['W']).
card_text('kjeldoran gargoyle', 'Flying, first strike\nWhenever Kjeldoran Gargoyle deals damage, you gain that much life.').
card_mana_cost('kjeldoran gargoyle', ['5', 'W']).
card_cmc('kjeldoran gargoyle', 6).
card_layout('kjeldoran gargoyle', 'normal').
card_power('kjeldoran gargoyle', 3).
card_toughness('kjeldoran gargoyle', 3).

% Found in: ICE
card_name('kjeldoran guard', 'Kjeldoran Guard').
card_type('kjeldoran guard', 'Creature — Human Soldier').
card_types('kjeldoran guard', ['Creature']).
card_subtypes('kjeldoran guard', ['Human', 'Soldier']).
card_colors('kjeldoran guard', ['W']).
card_text('kjeldoran guard', '{T}: Target creature gets +1/+1 until end of turn. When that creature leaves the battlefield this turn, sacrifice Kjeldoran Guard. Activate this ability only during combat and only if defending player controls no snow lands.').
card_mana_cost('kjeldoran guard', ['1', 'W']).
card_cmc('kjeldoran guard', 2).
card_layout('kjeldoran guard', 'normal').
card_power('kjeldoran guard', 1).
card_toughness('kjeldoran guard', 1).

% Found in: ALL, CST, ME2
card_name('kjeldoran home guard', 'Kjeldoran Home Guard').
card_type('kjeldoran home guard', 'Creature — Human Soldier').
card_types('kjeldoran home guard', ['Creature']).
card_subtypes('kjeldoran home guard', ['Human', 'Soldier']).
card_colors('kjeldoran home guard', ['W']).
card_text('kjeldoran home guard', 'At end of combat, if Kjeldoran Home Guard attacked or blocked this combat, put a -0/-1 counter on Kjeldoran Home Guard and put a 0/1 white Deserter creature token onto the battlefield.').
card_mana_cost('kjeldoran home guard', ['3', 'W']).
card_cmc('kjeldoran home guard', 4).
card_layout('kjeldoran home guard', 'normal').
card_power('kjeldoran home guard', 1).
card_toughness('kjeldoran home guard', 6).

% Found in: CSP
card_name('kjeldoran javelineer', 'Kjeldoran Javelineer').
card_type('kjeldoran javelineer', 'Creature — Human Soldier').
card_types('kjeldoran javelineer', ['Creature']).
card_subtypes('kjeldoran javelineer', ['Human', 'Soldier']).
card_colors('kjeldoran javelineer', ['W']).
card_text('kjeldoran javelineer', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\n{T}: Kjeldoran Javelineer deals damage equal to the number of age counters on it to target attacking or blocking creature.').
card_mana_cost('kjeldoran javelineer', ['W']).
card_cmc('kjeldoran javelineer', 1).
card_layout('kjeldoran javelineer', 'normal').
card_power('kjeldoran javelineer', 1).
card_toughness('kjeldoran javelineer', 2).

% Found in: ICE
card_name('kjeldoran knight', 'Kjeldoran Knight').
card_type('kjeldoran knight', 'Creature — Human Knight').
card_types('kjeldoran knight', ['Creature']).
card_subtypes('kjeldoran knight', ['Human', 'Knight']).
card_colors('kjeldoran knight', ['W']).
card_text('kjeldoran knight', 'Banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)\n{1}{W}: Kjeldoran Knight gets +1/+0 until end of turn.\n{W}{W}: Kjeldoran Knight gets +0/+2 until end of turn.').
card_mana_cost('kjeldoran knight', ['W', 'W']).
card_cmc('kjeldoran knight', 2).
card_layout('kjeldoran knight', 'normal').
card_power('kjeldoran knight', 1).
card_toughness('kjeldoran knight', 1).
card_reserved('kjeldoran knight').

% Found in: ALL, ME2, VMA
card_name('kjeldoran outpost', 'Kjeldoran Outpost').
card_type('kjeldoran outpost', 'Land').
card_types('kjeldoran outpost', ['Land']).
card_subtypes('kjeldoran outpost', []).
card_colors('kjeldoran outpost', []).
card_text('kjeldoran outpost', 'If Kjeldoran Outpost would enter the battlefield, sacrifice a Plains instead. If you do, put Kjeldoran Outpost onto the battlefield. If you don\'t, put it into its owner\'s graveyard.\n{T}: Add {W} to your mana pool.\n{1}{W}, {T}: Put a 1/1 white Soldier creature token onto the battlefield.').
card_layout('kjeldoran outpost', 'normal').
card_reserved('kjeldoran outpost').

% Found in: CSP
card_name('kjeldoran outrider', 'Kjeldoran Outrider').
card_type('kjeldoran outrider', 'Creature — Human Soldier').
card_types('kjeldoran outrider', ['Creature']).
card_subtypes('kjeldoran outrider', ['Human', 'Soldier']).
card_colors('kjeldoran outrider', ['W']).
card_text('kjeldoran outrider', '{W}: Kjeldoran Outrider gets +0/+1 until end of turn.').
card_mana_cost('kjeldoran outrider', ['1', 'W']).
card_cmc('kjeldoran outrider', 2).
card_layout('kjeldoran outrider', 'normal').
card_power('kjeldoran outrider', 2).
card_toughness('kjeldoran outrider', 2).

% Found in: ICE
card_name('kjeldoran phalanx', 'Kjeldoran Phalanx').
card_type('kjeldoran phalanx', 'Creature — Human Soldier').
card_types('kjeldoran phalanx', ['Creature']).
card_subtypes('kjeldoran phalanx', ['Human', 'Soldier']).
card_colors('kjeldoran phalanx', ['W']).
card_text('kjeldoran phalanx', 'First strike; banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('kjeldoran phalanx', ['5', 'W']).
card_cmc('kjeldoran phalanx', 6).
card_layout('kjeldoran phalanx', 'normal').
card_power('kjeldoran phalanx', 2).
card_toughness('kjeldoran phalanx', 5).
card_reserved('kjeldoran phalanx').

% Found in: ALL, CST
card_name('kjeldoran pride', 'Kjeldoran Pride').
card_type('kjeldoran pride', 'Enchantment — Aura').
card_types('kjeldoran pride', ['Enchantment']).
card_subtypes('kjeldoran pride', ['Aura']).
card_colors('kjeldoran pride', ['W']).
card_text('kjeldoran pride', 'Enchant creature\nEnchanted creature gets +1/+2.\n{2}{U}: Attach Kjeldoran Pride to target creature other than enchanted creature.').
card_mana_cost('kjeldoran pride', ['1', 'W']).
card_cmc('kjeldoran pride', 2).
card_layout('kjeldoran pride', 'normal').

% Found in: 10E, 5ED, 6ED, 7ED, ICE
card_name('kjeldoran royal guard', 'Kjeldoran Royal Guard').
card_type('kjeldoran royal guard', 'Creature — Human Soldier').
card_types('kjeldoran royal guard', ['Creature']).
card_subtypes('kjeldoran royal guard', ['Human', 'Soldier']).
card_colors('kjeldoran royal guard', ['W']).
card_text('kjeldoran royal guard', '{T}: All combat damage that would be dealt to you by unblocked creatures this turn is dealt to Kjeldoran Royal Guard instead.').
card_mana_cost('kjeldoran royal guard', ['3', 'W', 'W']).
card_cmc('kjeldoran royal guard', 5).
card_layout('kjeldoran royal guard', 'normal').
card_power('kjeldoran royal guard', 2).
card_toughness('kjeldoran royal guard', 5).

% Found in: 5ED, ICE, ME2
card_name('kjeldoran skycaptain', 'Kjeldoran Skycaptain').
card_type('kjeldoran skycaptain', 'Creature — Human Soldier').
card_types('kjeldoran skycaptain', ['Creature']).
card_subtypes('kjeldoran skycaptain', ['Human', 'Soldier']).
card_colors('kjeldoran skycaptain', ['W']).
card_text('kjeldoran skycaptain', 'Flying; first strike; banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('kjeldoran skycaptain', ['4', 'W']).
card_cmc('kjeldoran skycaptain', 5).
card_layout('kjeldoran skycaptain', 'normal').
card_power('kjeldoran skycaptain', 2).
card_toughness('kjeldoran skycaptain', 2).

% Found in: ICE
card_name('kjeldoran skyknight', 'Kjeldoran Skyknight').
card_type('kjeldoran skyknight', 'Creature — Human Knight').
card_types('kjeldoran skyknight', ['Creature']).
card_subtypes('kjeldoran skyknight', ['Human', 'Knight']).
card_colors('kjeldoran skyknight', ['W']).
card_text('kjeldoran skyknight', 'Flying; first strike; banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('kjeldoran skyknight', ['2', 'W']).
card_cmc('kjeldoran skyknight', 3).
card_layout('kjeldoran skyknight', 'normal').
card_power('kjeldoran skyknight', 1).
card_toughness('kjeldoran skyknight', 1).

% Found in: CSP
card_name('kjeldoran war cry', 'Kjeldoran War Cry').
card_type('kjeldoran war cry', 'Instant').
card_types('kjeldoran war cry', ['Instant']).
card_subtypes('kjeldoran war cry', []).
card_colors('kjeldoran war cry', ['W']).
card_text('kjeldoran war cry', 'Creatures you control get +X/+X until end of turn, where X is 1 plus the number of cards named Kjeldoran War Cry in all graveyards.').
card_mana_cost('kjeldoran war cry', ['1', 'W']).
card_cmc('kjeldoran war cry', 2).
card_layout('kjeldoran war cry', 'normal').

% Found in: ICE
card_name('kjeldoran warrior', 'Kjeldoran Warrior').
card_type('kjeldoran warrior', 'Creature — Human Warrior').
card_types('kjeldoran warrior', ['Creature']).
card_subtypes('kjeldoran warrior', ['Human', 'Warrior']).
card_colors('kjeldoran warrior', ['W']).
card_text('kjeldoran warrior', 'Banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('kjeldoran warrior', ['W']).
card_cmc('kjeldoran warrior', 1).
card_layout('kjeldoran warrior', 'normal').
card_power('kjeldoran warrior', 1).
card_toughness('kjeldoran warrior', 1).

% Found in: SHM
card_name('knacksaw clique', 'Knacksaw Clique').
card_type('knacksaw clique', 'Creature — Faerie Rogue').
card_types('knacksaw clique', ['Creature']).
card_subtypes('knacksaw clique', ['Faerie', 'Rogue']).
card_colors('knacksaw clique', ['U']).
card_text('knacksaw clique', 'Flying\n{1}{U}, {Q}: Target opponent exiles the top card of his or her library. Until end of turn, you may play that card. ({Q} is the untap symbol.)').
card_mana_cost('knacksaw clique', ['3', 'U']).
card_cmc('knacksaw clique', 4).
card_layout('knacksaw clique', 'normal').
card_power('knacksaw clique', 1).
card_toughness('knacksaw clique', 4).

% Found in: 7ED, POR, S00, S99
card_name('knight errant', 'Knight Errant').
card_type('knight errant', 'Creature — Human Knight').
card_types('knight errant', ['Creature']).
card_subtypes('knight errant', ['Human', 'Knight']).
card_colors('knight errant', ['W']).
card_text('knight errant', '').
card_mana_cost('knight errant', ['1', 'W']).
card_cmc('knight errant', 2).
card_layout('knight errant', 'normal').
card_power('knight errant', 2).
card_toughness('knight errant', 2).

% Found in: DDG, M11, pMEI
card_name('knight exemplar', 'Knight Exemplar').
card_type('knight exemplar', 'Creature — Human Knight').
card_types('knight exemplar', ['Creature']).
card_subtypes('knight exemplar', ['Human', 'Knight']).
card_colors('knight exemplar', ['W']).
card_text('knight exemplar', 'First strike (This creature deals combat damage before creatures without first strike.)\nOther Knight creatures you control get +1/+1 and have indestructible. (Damage and effects that say \"destroy\" don\'t destroy them.)').
card_mana_cost('knight exemplar', ['1', 'W', 'W']).
card_cmc('knight exemplar', 3).
card_layout('knight exemplar', 'normal').
card_power('knight exemplar', 2).
card_toughness('knight exemplar', 2).

% Found in: DDG, DDP, ROE
card_name('knight of cliffhaven', 'Knight of Cliffhaven').
card_type('knight of cliffhaven', 'Creature — Kor Knight').
card_types('knight of cliffhaven', ['Creature']).
card_subtypes('knight of cliffhaven', ['Kor', 'Knight']).
card_colors('knight of cliffhaven', ['W']).
card_text('knight of cliffhaven', 'Level up {3} ({3}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-3\n2/3\nFlying\nLEVEL 4+\n4/4\nFlying, vigilance').
card_mana_cost('knight of cliffhaven', ['1', 'W']).
card_cmc('knight of cliffhaven', 2).
card_layout('knight of cliffhaven', 'leveler').
card_power('knight of cliffhaven', 2).
card_toughness('knight of cliffhaven', 2).

% Found in: TMP
card_name('knight of dawn', 'Knight of Dawn').
card_type('knight of dawn', 'Creature — Human Knight').
card_types('knight of dawn', ['Creature']).
card_subtypes('knight of dawn', ['Human', 'Knight']).
card_colors('knight of dawn', ['W']).
card_text('knight of dawn', 'First strike\n{W}{W}: Knight of Dawn gains protection from the color of your choice until end of turn.').
card_mana_cost('knight of dawn', ['1', 'W', 'W']).
card_cmc('knight of dawn', 3).
card_layout('knight of dawn', 'normal').
card_power('knight of dawn', 2).
card_toughness('knight of dawn', 2).

% Found in: 10E, TMP
card_name('knight of dusk', 'Knight of Dusk').
card_type('knight of dusk', 'Creature — Human Knight').
card_types('knight of dusk', ['Creature']).
card_subtypes('knight of dusk', ['Human', 'Knight']).
card_colors('knight of dusk', ['B']).
card_text('knight of dusk', '{B}{B}: Destroy target creature blocking Knight of Dusk.').
card_mana_cost('knight of dusk', ['1', 'B', 'B']).
card_cmc('knight of dusk', 3).
card_layout('knight of dusk', 'normal').
card_power('knight of dusk', 2).
card_toughness('knight of dusk', 2).

% Found in: M13
card_name('knight of glory', 'Knight of Glory').
card_type('knight of glory', 'Creature — Human Knight').
card_types('knight of glory', ['Creature']).
card_subtypes('knight of glory', ['Human', 'Knight']).
card_colors('knight of glory', ['W']).
card_text('knight of glory', 'Protection from black (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything black.)\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_mana_cost('knight of glory', ['1', 'W']).
card_cmc('knight of glory', 2).
card_layout('knight of glory', 'normal').
card_power('knight of glory', 2).
card_toughness('knight of glory', 1).

% Found in: M13
card_name('knight of infamy', 'Knight of Infamy').
card_type('knight of infamy', 'Creature — Human Knight').
card_types('knight of infamy', ['Creature']).
card_subtypes('knight of infamy', ['Human', 'Knight']).
card_colors('knight of infamy', ['B']).
card_text('knight of infamy', 'Protection from white (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything white.)\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_mana_cost('knight of infamy', ['1', 'B']).
card_cmc('knight of infamy', 2).
card_layout('knight of infamy', 'normal').
card_power('knight of infamy', 2).
card_toughness('knight of infamy', 1).

% Found in: DDG, LRW
card_name('knight of meadowgrain', 'Knight of Meadowgrain').
card_type('knight of meadowgrain', 'Creature — Kithkin Knight').
card_types('knight of meadowgrain', ['Creature']).
card_subtypes('knight of meadowgrain', ['Kithkin', 'Knight']).
card_colors('knight of meadowgrain', ['W']).
card_text('knight of meadowgrain', 'First strike\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('knight of meadowgrain', ['W', 'W']).
card_cmc('knight of meadowgrain', 2).
card_layout('knight of meadowgrain', 'normal').
card_power('knight of meadowgrain', 2).
card_toughness('knight of meadowgrain', 2).

% Found in: ARB, pLPA
card_name('knight of new alara', 'Knight of New Alara').
card_type('knight of new alara', 'Creature — Human Knight').
card_types('knight of new alara', ['Creature']).
card_subtypes('knight of new alara', ['Human', 'Knight']).
card_colors('knight of new alara', ['W', 'G']).
card_text('knight of new alara', 'Each other multicolored creature you control gets +1/+1 for each of its colors.').
card_mana_cost('knight of new alara', ['2', 'G', 'W']).
card_cmc('knight of new alara', 4).
card_layout('knight of new alara', 'normal').
card_power('knight of new alara', 2).
card_toughness('knight of new alara', 2).

% Found in: GTC
card_name('knight of obligation', 'Knight of Obligation').
card_type('knight of obligation', 'Creature — Human Knight').
card_types('knight of obligation', ['Creature']).
card_subtypes('knight of obligation', ['Human', 'Knight']).
card_colors('knight of obligation', ['W']).
card_text('knight of obligation', 'Vigilance\nExtort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)').
card_mana_cost('knight of obligation', ['3', 'W']).
card_cmc('knight of obligation', 4).
card_layout('knight of obligation', 'normal').
card_power('knight of obligation', 2).
card_toughness('knight of obligation', 4).

% Found in: 5ED, ATH, ICE, ME2
card_name('knight of stromgald', 'Knight of Stromgald').
card_type('knight of stromgald', 'Creature — Human Knight').
card_types('knight of stromgald', ['Creature']).
card_subtypes('knight of stromgald', ['Human', 'Knight']).
card_colors('knight of stromgald', ['B']).
card_text('knight of stromgald', 'Protection from white\n{B}: Knight of Stromgald gains first strike until end of turn.\n{B}{B}: Knight of Stromgald gets +1/+0 until end of turn.').
card_mana_cost('knight of stromgald', ['B', 'B']).
card_cmc('knight of stromgald', 2).
card_layout('knight of stromgald', 'normal').
card_power('knight of stromgald', 2).
card_toughness('knight of stromgald', 1).

% Found in: FUT
card_name('knight of sursi', 'Knight of Sursi').
card_type('knight of sursi', 'Creature — Human Knight').
card_types('knight of sursi', ['Creature']).
card_subtypes('knight of sursi', ['Human', 'Knight']).
card_colors('knight of sursi', ['W']).
card_text('knight of sursi', 'Flying; flanking  (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nSuspend 3—{W} (Rather than cast this card from your hand, you may pay {W} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_mana_cost('knight of sursi', ['3', 'W']).
card_cmc('knight of sursi', 4).
card_layout('knight of sursi', 'normal').
card_power('knight of sursi', 2).
card_toughness('knight of sursi', 2).

% Found in: UGL
card_name('knight of the hokey pokey', 'Knight of the Hokey Pokey').
card_type('knight of the hokey pokey', 'Creature — Knight').
card_types('knight of the hokey pokey', ['Creature']).
card_subtypes('knight of the hokey pokey', ['Knight']).
card_colors('knight of the hokey pokey', ['W']).
card_text('knight of the hokey pokey', 'First strike\n{1}{W}, Do the Hokey Pokey (Stand up, wiggle your butt, raise your hands above your head, and shake them wildly as you rotate 360 degrees): Prevent all damage to Knight of the Hokey Pokey from any one source.').
card_mana_cost('knight of the hokey pokey', ['W', 'W']).
card_cmc('knight of the hokey pokey', 2).
card_layout('knight of the hokey pokey', 'normal').
card_power('knight of the hokey pokey', 2).
card_toughness('knight of the hokey pokey', 2).

% Found in: TSP
card_name('knight of the holy nimbus', 'Knight of the Holy Nimbus').
card_type('knight of the holy nimbus', 'Creature — Human Rebel Knight').
card_types('knight of the holy nimbus', ['Creature']).
card_subtypes('knight of the holy nimbus', ['Human', 'Rebel', 'Knight']).
card_colors('knight of the holy nimbus', ['W']).
card_text('knight of the holy nimbus', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nIf Knight of the Holy Nimbus would be destroyed, regenerate it.\n{2}: Knight of the Holy Nimbus can\'t be regenerated this turn. Only any opponent may activate this ability.').
card_mana_cost('knight of the holy nimbus', ['W', 'W']).
card_cmc('knight of the holy nimbus', 2).
card_layout('knight of the holy nimbus', 'normal').
card_power('knight of the holy nimbus', 2).
card_toughness('knight of the holy nimbus', 2).

% Found in: VIS
card_name('knight of the mists', 'Knight of the Mists').
card_type('knight of the mists', 'Creature — Human Knight').
card_types('knight of the mists', ['Creature']).
card_subtypes('knight of the mists', ['Human', 'Knight']).
card_colors('knight of the mists', ['U']).
card_text('knight of the mists', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nWhen Knight of the Mists enters the battlefield, you may pay {U}. If you don\'t, destroy target Knight and it can\'t be regenerated.').
card_mana_cost('knight of the mists', ['2', 'U']).
card_cmc('knight of the mists', 3).
card_layout('knight of the mists', 'normal').
card_power('knight of the mists', 2).
card_toughness('knight of the mists', 2).

% Found in: ORI
card_name('knight of the pilgrim\'s road', 'Knight of the Pilgrim\'s Road').
card_type('knight of the pilgrim\'s road', 'Creature — Human Knight').
card_types('knight of the pilgrim\'s road', ['Creature']).
card_subtypes('knight of the pilgrim\'s road', ['Human', 'Knight']).
card_colors('knight of the pilgrim\'s road', ['W']).
card_text('knight of the pilgrim\'s road', 'Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)').
card_mana_cost('knight of the pilgrim\'s road', ['2', 'W']).
card_cmc('knight of the pilgrim\'s road', 3).
card_layout('knight of the pilgrim\'s road', 'normal').
card_power('knight of the pilgrim\'s road', 3).
card_toughness('knight of the pilgrim\'s road', 2).

% Found in: CON, DDG, MMA
card_name('knight of the reliquary', 'Knight of the Reliquary').
card_type('knight of the reliquary', 'Creature — Human Knight').
card_types('knight of the reliquary', ['Creature']).
card_subtypes('knight of the reliquary', ['Human', 'Knight']).
card_colors('knight of the reliquary', ['W', 'G']).
card_text('knight of the reliquary', 'Knight of the Reliquary gets +1/+1 for each land card in your graveyard.\n{T}, Sacrifice a Forest or Plains: Search your library for a land card, put it onto the battlefield, then shuffle your library.').
card_mana_cost('knight of the reliquary', ['1', 'G', 'W']).
card_cmc('knight of the reliquary', 3).
card_layout('knight of the reliquary', 'normal').
card_power('knight of the reliquary', 2).
card_toughness('knight of the reliquary', 2).

% Found in: ALA
card_name('knight of the skyward eye', 'Knight of the Skyward Eye').
card_type('knight of the skyward eye', 'Creature — Human Knight').
card_types('knight of the skyward eye', ['Creature']).
card_subtypes('knight of the skyward eye', ['Human', 'Knight']).
card_colors('knight of the skyward eye', ['W']).
card_text('knight of the skyward eye', '{3}{G}: Knight of the Skyward Eye gets +3/+3 until end of turn. Activate this ability only once each turn.').
card_mana_cost('knight of the skyward eye', ['1', 'W']).
card_cmc('knight of the skyward eye', 2).
card_layout('knight of the skyward eye', 'normal').
card_power('knight of the skyward eye', 2).
card_toughness('knight of the skyward eye', 2).

% Found in: ALA, DDG, ORI
card_name('knight of the white orchid', 'Knight of the White Orchid').
card_type('knight of the white orchid', 'Creature — Human Knight').
card_types('knight of the white orchid', ['Creature']).
card_subtypes('knight of the white orchid', ['Human', 'Knight']).
card_colors('knight of the white orchid', ['W']).
card_text('knight of the white orchid', 'First strike\nWhen Knight of the White Orchid enters the battlefield, if an opponent controls more lands than you, you may search your library for a Plains card, put it onto the battlefield, then shuffle your library.').
card_mana_cost('knight of the white orchid', ['W', 'W']).
card_cmc('knight of the white orchid', 2).
card_layout('knight of the white orchid', 'normal').
card_power('knight of the white orchid', 2).
card_toughness('knight of the white orchid', 2).

% Found in: VIS
card_name('knight of valor', 'Knight of Valor').
card_type('knight of valor', 'Creature — Human Knight').
card_types('knight of valor', ['Creature']).
card_subtypes('knight of valor', ['Human', 'Knight']).
card_colors('knight of valor', ['W']).
card_text('knight of valor', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{1}{W}: Each creature without flanking blocking Knight of Valor gets -1/-1 until end of turn. Activate this ability only once each turn.').
card_mana_cost('knight of valor', ['2', 'W']).
card_cmc('knight of valor', 3).
card_layout('knight of valor', 'normal').
card_power('knight of valor', 2).
card_toughness('knight of valor', 2).

% Found in: GTC
card_name('knight watch', 'Knight Watch').
card_type('knight watch', 'Sorcery').
card_types('knight watch', ['Sorcery']).
card_subtypes('knight watch', []).
card_colors('knight watch', ['W']).
card_text('knight watch', 'Put two 2/2 white Knight creature tokens with vigilance onto the battlefield.').
card_mana_cost('knight watch', ['4', 'W']).
card_cmc('knight watch', 5).
card_layout('knight watch', 'normal').

% Found in: ALA
card_name('knight-captain of eos', 'Knight-Captain of Eos').
card_type('knight-captain of eos', 'Creature — Human Knight').
card_types('knight-captain of eos', ['Creature']).
card_subtypes('knight-captain of eos', ['Human', 'Knight']).
card_colors('knight-captain of eos', ['W']).
card_text('knight-captain of eos', 'When Knight-Captain of Eos enters the battlefield, put two 1/1 white Soldier creature tokens onto the battlefield.\n{W}, Sacrifice a Soldier: Prevent all combat damage that would be dealt this turn.').
card_mana_cost('knight-captain of eos', ['4', 'W']).
card_cmc('knight-captain of eos', 5).
card_layout('knight-captain of eos', 'normal').
card_power('knight-captain of eos', 2).
card_toughness('knight-captain of eos', 2).

% Found in: 7ED, ULG
card_name('knighthood', 'Knighthood').
card_type('knighthood', 'Enchantment').
card_types('knighthood', ['Enchantment']).
card_subtypes('knighthood', []).
card_colors('knighthood', ['W']).
card_text('knighthood', 'Creatures you control have first strike.').
card_mana_cost('knighthood', ['2', 'W']).
card_cmc('knighthood', 3).
card_layout('knighthood', 'normal').

% Found in: ORI, RTR
card_name('knightly valor', 'Knightly Valor').
card_type('knightly valor', 'Enchantment — Aura').
card_types('knightly valor', ['Enchantment']).
card_subtypes('knightly valor', ['Aura']).
card_colors('knightly valor', ['W']).
card_text('knightly valor', 'Enchant creature\nWhen Knightly Valor enters the battlefield, put a 2/2 white Knight creature token with vigilance onto the battlefield. (Attacking doesn\'t cause it to tap.)\nEnchanted creature gets +2/+2 and has vigilance.').
card_mana_cost('knightly valor', ['4', 'W']).
card_cmc('knightly valor', 5).
card_layout('knightly valor', 'normal').

% Found in: DRK, MED
card_name('knights of thorn', 'Knights of Thorn').
card_type('knights of thorn', 'Creature — Human Knight').
card_types('knights of thorn', ['Creature']).
card_subtypes('knights of thorn', ['Human', 'Knight']).
card_colors('knights of thorn', ['W']).
card_text('knights of thorn', 'Protection from red; banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('knights of thorn', ['3', 'W']).
card_cmc('knights of thorn', 4).
card_layout('knights of thorn', 'normal').
card_power('knights of thorn', 2).
card_toughness('knights of thorn', 2).
card_reserved('knights of thorn').

% Found in: SHM
card_name('knollspine dragon', 'Knollspine Dragon').
card_type('knollspine dragon', 'Creature — Dragon').
card_types('knollspine dragon', ['Creature']).
card_subtypes('knollspine dragon', ['Dragon']).
card_colors('knollspine dragon', ['R']).
card_text('knollspine dragon', 'Flying\nWhen Knollspine Dragon enters the battlefield, you may discard your hand and draw cards equal to the damage dealt to target opponent this turn.').
card_mana_cost('knollspine dragon', ['5', 'R', 'R']).
card_cmc('knollspine dragon', 7).
card_layout('knollspine dragon', 'normal').
card_power('knollspine dragon', 7).
card_toughness('knollspine dragon', 5).

% Found in: SHM
card_name('knollspine invocation', 'Knollspine Invocation').
card_type('knollspine invocation', 'Enchantment').
card_types('knollspine invocation', ['Enchantment']).
card_subtypes('knollspine invocation', []).
card_colors('knollspine invocation', ['R']).
card_text('knollspine invocation', '{X}, Discard a card with converted mana cost X: Knollspine Invocation deals X damage to target creature or player.').
card_mana_cost('knollspine invocation', ['1', 'R', 'R']).
card_cmc('knollspine invocation', 3).
card_layout('knollspine invocation', 'normal').

% Found in: CON
card_name('knotvine mystic', 'Knotvine Mystic').
card_type('knotvine mystic', 'Creature — Elf Druid').
card_types('knotvine mystic', ['Creature']).
card_subtypes('knotvine mystic', ['Elf', 'Druid']).
card_colors('knotvine mystic', ['W', 'R', 'G']).
card_text('knotvine mystic', '{1}, {T}: Add {R}{G}{W} to your mana pool.').
card_mana_cost('knotvine mystic', ['R', 'G', 'W']).
card_cmc('knotvine mystic', 3).
card_layout('knotvine mystic', 'normal').
card_power('knotvine mystic', 2).
card_toughness('knotvine mystic', 2).

% Found in: ARB, DDG
card_name('knotvine paladin', 'Knotvine Paladin').
card_type('knotvine paladin', 'Creature — Human Knight').
card_types('knotvine paladin', ['Creature']).
card_subtypes('knotvine paladin', ['Human', 'Knight']).
card_colors('knotvine paladin', ['W', 'G']).
card_text('knotvine paladin', 'Whenever Knotvine Paladin attacks, it gets +1/+1 until end of turn for each untapped creature you control.').
card_mana_cost('knotvine paladin', ['G', 'W']).
card_cmc('knotvine paladin', 2).
card_layout('knotvine paladin', 'normal').
card_power('knotvine paladin', 2).
card_toughness('knotvine paladin', 2).

% Found in: ARC
card_name('know naught but fire', 'Know Naught but Fire').
card_type('know naught but fire', 'Scheme').
card_types('know naught but fire', ['Scheme']).
card_subtypes('know naught but fire', []).
card_colors('know naught but fire', []).
card_text('know naught but fire', 'When you set this scheme in motion, it deals damage to each opponent equal to the number of cards in that player\'s hand.').
card_layout('know naught but fire', 'scheme').

% Found in: JOU
card_name('knowledge and power', 'Knowledge and Power').
card_type('knowledge and power', 'Enchantment').
card_types('knowledge and power', ['Enchantment']).
card_subtypes('knowledge and power', []).
card_colors('knowledge and power', ['R']).
card_text('knowledge and power', 'Whenever you scry, you may pay {2}. If you do, Knowledge and Power deals 2 damage to target creature or player.').
card_mana_cost('knowledge and power', ['4', 'R']).
card_cmc('knowledge and power', 5).
card_layout('knowledge and power', 'normal').

% Found in: MOR
card_name('knowledge exploitation', 'Knowledge Exploitation').
card_type('knowledge exploitation', 'Tribal Sorcery — Rogue').
card_types('knowledge exploitation', ['Tribal', 'Sorcery']).
card_subtypes('knowledge exploitation', ['Rogue']).
card_colors('knowledge exploitation', ['U']).
card_text('knowledge exploitation', 'Prowl {3}{U} (You may cast this for its prowl cost if you dealt combat damage to a player this turn with a Rogue.)\nSearch target opponent\'s library for an instant or sorcery card. You may cast that card without paying its mana cost. Then that player shuffles his or her library.').
card_mana_cost('knowledge exploitation', ['5', 'U', 'U']).
card_cmc('knowledge exploitation', 7).
card_layout('knowledge exploitation', 'normal').

% Found in: MBS
card_name('knowledge pool', 'Knowledge Pool').
card_type('knowledge pool', 'Artifact').
card_types('knowledge pool', ['Artifact']).
card_subtypes('knowledge pool', []).
card_colors('knowledge pool', []).
card_text('knowledge pool', 'Imprint — When Knowledge Pool enters the battlefield, each player exiles the top three cards of his or her library.\nWhenever a player casts a spell from his or her hand, that player exiles it. If the player does, he or she may cast another nonland card exiled with Knowledge Pool without paying that card\'s mana cost.').
card_mana_cost('knowledge pool', ['6']).
card_cmc('knowledge pool', 6).
card_layout('knowledge pool', 'normal').

% Found in: LEG, ME3
card_name('knowledge vault', 'Knowledge Vault').
card_type('knowledge vault', 'Artifact').
card_types('knowledge vault', ['Artifact']).
card_subtypes('knowledge vault', []).
card_colors('knowledge vault', []).
card_text('knowledge vault', '{2}, {T}: Exile the top card of your library face down.\n{0}: Sacrifice Knowledge Vault. If you do, discard your hand, then put all cards exiled with Knowledge Vault into their owner\'s hand.\nWhen Knowledge Vault leaves the battlefield, put all cards exiled with Knowledge Vault into their owner\'s graveyard.').
card_mana_cost('knowledge vault', ['4']).
card_cmc('knowledge vault', 4).
card_layout('knowledge vault', 'normal').
card_reserved('knowledge vault').

% Found in: LRW
card_name('knucklebone witch', 'Knucklebone Witch').
card_type('knucklebone witch', 'Creature — Goblin Shaman').
card_types('knucklebone witch', ['Creature']).
card_subtypes('knucklebone witch', ['Goblin', 'Shaman']).
card_colors('knucklebone witch', ['B']).
card_text('knucklebone witch', 'Whenever a Goblin you control is put into a graveyard from the battlefield, you may put a +1/+1 counter on Knucklebone Witch.').
card_mana_cost('knucklebone witch', ['B']).
card_cmc('knucklebone witch', 1).
card_layout('knucklebone witch', 'normal').
card_power('knucklebone witch', 1).
card_toughness('knucklebone witch', 1).

% Found in: LEG, ME3
card_name('kobold drill sergeant', 'Kobold Drill Sergeant').
card_type('kobold drill sergeant', 'Creature — Kobold Soldier').
card_types('kobold drill sergeant', ['Creature']).
card_subtypes('kobold drill sergeant', ['Kobold', 'Soldier']).
card_colors('kobold drill sergeant', ['R']).
card_text('kobold drill sergeant', 'Other Kobold creatures you control get +0/+1 and have trample.').
card_mana_cost('kobold drill sergeant', ['1', 'R']).
card_cmc('kobold drill sergeant', 2).
card_layout('kobold drill sergeant', 'normal').
card_power('kobold drill sergeant', 1).
card_toughness('kobold drill sergeant', 2).

% Found in: LEG, ME3
card_name('kobold overlord', 'Kobold Overlord').
card_type('kobold overlord', 'Creature — Kobold').
card_types('kobold overlord', ['Creature']).
card_subtypes('kobold overlord', ['Kobold']).
card_colors('kobold overlord', ['R']).
card_text('kobold overlord', 'First strike\nOther Kobold creatures you control have first strike.').
card_mana_cost('kobold overlord', ['1', 'R']).
card_cmc('kobold overlord', 2).
card_layout('kobold overlord', 'normal').
card_power('kobold overlord', 1).
card_toughness('kobold overlord', 2).
card_reserved('kobold overlord').

% Found in: LEG, ME3, TSB
card_name('kobold taskmaster', 'Kobold Taskmaster').
card_type('kobold taskmaster', 'Creature — Kobold').
card_types('kobold taskmaster', ['Creature']).
card_subtypes('kobold taskmaster', ['Kobold']).
card_colors('kobold taskmaster', ['R']).
card_text('kobold taskmaster', 'Other Kobold creatures you control get +1/+0.').
card_mana_cost('kobold taskmaster', ['1', 'R']).
card_cmc('kobold taskmaster', 2).
card_layout('kobold taskmaster', 'normal').
card_power('kobold taskmaster', 1).
card_toughness('kobold taskmaster', 2).

% Found in: LEG, ME3
card_name('kobolds of kher keep', 'Kobolds of Kher Keep').
card_type('kobolds of kher keep', 'Creature — Kobold').
card_types('kobolds of kher keep', ['Creature']).
card_subtypes('kobolds of kher keep', ['Kobold']).
card_colors('kobolds of kher keep', ['R']).
card_text('kobolds of kher keep', '').
card_mana_cost('kobolds of kher keep', ['0']).
card_cmc('kobolds of kher keep', 0).
card_layout('kobolds of kher keep', 'normal').
card_power('kobolds of kher keep', 0).
card_toughness('kobolds of kher keep', 1).

% Found in: BOK
card_name('kodama of the center tree', 'Kodama of the Center Tree').
card_type('kodama of the center tree', 'Legendary Creature — Spirit').
card_types('kodama of the center tree', ['Creature']).
card_subtypes('kodama of the center tree', ['Spirit']).
card_supertypes('kodama of the center tree', ['Legendary']).
card_colors('kodama of the center tree', ['G']).
card_text('kodama of the center tree', 'Kodama of the Center Tree\'s power and toughness are each equal to the number of Spirits you control.\nKodama of the Center Tree has soulshift X, where X is the number of Spirits you control. (When this creature dies, you may return target Spirit card with converted mana cost X or less from your graveyard to your hand.)').
card_mana_cost('kodama of the center tree', ['4', 'G']).
card_cmc('kodama of the center tree', 5).
card_layout('kodama of the center tree', 'normal').
card_power('kodama of the center tree', '*').
card_toughness('kodama of the center tree', '*').

% Found in: CHK
card_name('kodama of the north tree', 'Kodama of the North Tree').
card_type('kodama of the north tree', 'Legendary Creature — Spirit').
card_types('kodama of the north tree', ['Creature']).
card_subtypes('kodama of the north tree', ['Spirit']).
card_supertypes('kodama of the north tree', ['Legendary']).
card_colors('kodama of the north tree', ['G']).
card_text('kodama of the north tree', 'Trample\nShroud (This creature can\'t be the target of spells or abilities.)').
card_mana_cost('kodama of the north tree', ['2', 'G', 'G', 'G']).
card_cmc('kodama of the north tree', 5).
card_layout('kodama of the north tree', 'normal').
card_power('kodama of the north tree', 6).
card_toughness('kodama of the north tree', 4).

% Found in: CHK
card_name('kodama of the south tree', 'Kodama of the South Tree').
card_type('kodama of the south tree', 'Legendary Creature — Spirit').
card_types('kodama of the south tree', ['Creature']).
card_subtypes('kodama of the south tree', ['Spirit']).
card_supertypes('kodama of the south tree', ['Legendary']).
card_colors('kodama of the south tree', ['G']).
card_text('kodama of the south tree', 'Whenever you cast a Spirit or Arcane spell, each other creature you control gets +1/+1 and gains trample until end of turn.').
card_mana_cost('kodama of the south tree', ['2', 'G', 'G']).
card_cmc('kodama of the south tree', 4).
card_layout('kodama of the south tree', 'normal').
card_power('kodama of the south tree', 4).
card_toughness('kodama of the south tree', 4).

% Found in: CHK
card_name('kodama\'s might', 'Kodama\'s Might').
card_type('kodama\'s might', 'Instant — Arcane').
card_types('kodama\'s might', ['Instant']).
card_subtypes('kodama\'s might', ['Arcane']).
card_colors('kodama\'s might', ['G']).
card_text('kodama\'s might', 'Target creature gets +2/+2 until end of turn.\nSplice onto Arcane {G} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('kodama\'s might', ['G']).
card_cmc('kodama\'s might', 1).
card_layout('kodama\'s might', 'normal').

% Found in: CHK, CMD, MMA
card_name('kodama\'s reach', 'Kodama\'s Reach').
card_type('kodama\'s reach', 'Sorcery — Arcane').
card_types('kodama\'s reach', ['Sorcery']).
card_subtypes('kodama\'s reach', ['Arcane']).
card_colors('kodama\'s reach', ['G']).
card_text('kodama\'s reach', 'Search your library for up to two basic land cards, reveal those cards, and put one onto the battlefield tapped and the other into your hand. Then shuffle your library.').
card_mana_cost('kodama\'s reach', ['2', 'G']).
card_cmc('kodama\'s reach', 3).
card_layout('kodama\'s reach', 'normal').

% Found in: CHK, DRB, MMA
card_name('kokusho, the evening star', 'Kokusho, the Evening Star').
card_type('kokusho, the evening star', 'Legendary Creature — Dragon Spirit').
card_types('kokusho, the evening star', ['Creature']).
card_subtypes('kokusho, the evening star', ['Dragon', 'Spirit']).
card_supertypes('kokusho, the evening star', ['Legendary']).
card_colors('kokusho, the evening star', ['B']).
card_text('kokusho, the evening star', 'Flying\nWhen Kokusho, the Evening Star dies, each opponent loses 5 life. You gain life equal to the life lost this way.').
card_mana_cost('kokusho, the evening star', ['4', 'B', 'B']).
card_cmc('kokusho, the evening star', 6).
card_layout('kokusho, the evening star', 'normal').
card_power('kokusho, the evening star', 5).
card_toughness('kokusho, the evening star', 5).

% Found in: DTK
card_name('kolaghan aspirant', 'Kolaghan Aspirant').
card_type('kolaghan aspirant', 'Creature — Human Warrior').
card_types('kolaghan aspirant', ['Creature']).
card_subtypes('kolaghan aspirant', ['Human', 'Warrior']).
card_colors('kolaghan aspirant', ['R']).
card_text('kolaghan aspirant', 'Whenever Kolaghan Aspirant becomes blocked by a creature, Kolaghan Aspirant deals 1 damage to that creature.').
card_mana_cost('kolaghan aspirant', ['1', 'R']).
card_cmc('kolaghan aspirant', 2).
card_layout('kolaghan aspirant', 'normal').
card_power('kolaghan aspirant', 2).
card_toughness('kolaghan aspirant', 1).

% Found in: DTK
card_name('kolaghan forerunners', 'Kolaghan Forerunners').
card_type('kolaghan forerunners', 'Creature — Human Berserker').
card_types('kolaghan forerunners', ['Creature']).
card_subtypes('kolaghan forerunners', ['Human', 'Berserker']).
card_colors('kolaghan forerunners', ['R']).
card_text('kolaghan forerunners', 'Trample\nKolaghan Forerunners\'s power is equal to the number of creatures you control.\nDash {2}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('kolaghan forerunners', ['2', 'R']).
card_cmc('kolaghan forerunners', 3).
card_layout('kolaghan forerunners', 'normal').
card_power('kolaghan forerunners', '*').
card_toughness('kolaghan forerunners', 3).

% Found in: DTK
card_name('kolaghan monument', 'Kolaghan Monument').
card_type('kolaghan monument', 'Artifact').
card_types('kolaghan monument', ['Artifact']).
card_subtypes('kolaghan monument', []).
card_colors('kolaghan monument', []).
card_text('kolaghan monument', '{T}: Add {B} or {R} to your mana pool.\n{4}{B}{R}: Kolaghan Monument becomes a 4/4 black and red Dragon artifact creature with flying until end of turn.').
card_mana_cost('kolaghan monument', ['3']).
card_cmc('kolaghan monument', 3).
card_layout('kolaghan monument', 'normal').

% Found in: DTK
card_name('kolaghan skirmisher', 'Kolaghan Skirmisher').
card_type('kolaghan skirmisher', 'Creature — Human Warrior').
card_types('kolaghan skirmisher', ['Creature']).
card_subtypes('kolaghan skirmisher', ['Human', 'Warrior']).
card_colors('kolaghan skirmisher', ['B']).
card_text('kolaghan skirmisher', 'Dash {2}{B} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('kolaghan skirmisher', ['1', 'B']).
card_cmc('kolaghan skirmisher', 2).
card_layout('kolaghan skirmisher', 'normal').
card_power('kolaghan skirmisher', 2).
card_toughness('kolaghan skirmisher', 2).

% Found in: DTK
card_name('kolaghan stormsinger', 'Kolaghan Stormsinger').
card_type('kolaghan stormsinger', 'Creature — Human Shaman').
card_types('kolaghan stormsinger', ['Creature']).
card_subtypes('kolaghan stormsinger', ['Human', 'Shaman']).
card_colors('kolaghan stormsinger', ['R']).
card_text('kolaghan stormsinger', 'Haste\nMegamorph {R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Kolaghan Stormsinger is turned face up, target creature gains haste until end of turn.').
card_mana_cost('kolaghan stormsinger', ['R']).
card_cmc('kolaghan stormsinger', 1).
card_layout('kolaghan stormsinger', 'normal').
card_power('kolaghan stormsinger', 1).
card_toughness('kolaghan stormsinger', 1).

% Found in: DTK
card_name('kolaghan\'s command', 'Kolaghan\'s Command').
card_type('kolaghan\'s command', 'Instant').
card_types('kolaghan\'s command', ['Instant']).
card_subtypes('kolaghan\'s command', []).
card_colors('kolaghan\'s command', ['B', 'R']).
card_text('kolaghan\'s command', 'Choose two —\n• Return target creature card from your graveyard to your hand.\n• Target player discards a card.\n• Destroy target artifact.\n• Kolaghan\'s Command deals 2 damage to target creature or player.').
card_mana_cost('kolaghan\'s command', ['1', 'B', 'R']).
card_cmc('kolaghan\'s command', 3).
card_layout('kolaghan\'s command', 'normal').

% Found in: FRF
card_name('kolaghan, the storm\'s fury', 'Kolaghan, the Storm\'s Fury').
card_type('kolaghan, the storm\'s fury', 'Legendary Creature — Dragon').
card_types('kolaghan, the storm\'s fury', ['Creature']).
card_subtypes('kolaghan, the storm\'s fury', ['Dragon']).
card_supertypes('kolaghan, the storm\'s fury', ['Legendary']).
card_colors('kolaghan, the storm\'s fury', ['B', 'R']).
card_text('kolaghan, the storm\'s fury', 'Flying\nWhenever a Dragon you control attacks, creatures you control get +1/+0 until end of turn.\nDash {3}{B}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('kolaghan, the storm\'s fury', ['3', 'B', 'R']).
card_cmc('kolaghan, the storm\'s fury', 5).
card_layout('kolaghan, the storm\'s fury', 'normal').
card_power('kolaghan, the storm\'s fury', 4).
card_toughness('kolaghan, the storm\'s fury', 5).

% Found in: CHK
card_name('konda\'s banner', 'Konda\'s Banner').
card_type('konda\'s banner', 'Legendary Artifact — Equipment').
card_types('konda\'s banner', ['Artifact']).
card_subtypes('konda\'s banner', ['Equipment']).
card_supertypes('konda\'s banner', ['Legendary']).
card_colors('konda\'s banner', []).
card_text('konda\'s banner', 'Konda\'s Banner can be attached only to a legendary creature.\nCreatures that share a color with equipped creature get +1/+1.\nCreatures that share a creature type with equipped creature get +1/+1.\nEquip {2}').
card_mana_cost('konda\'s banner', ['2']).
card_cmc('konda\'s banner', 2).
card_layout('konda\'s banner', 'normal').

% Found in: CHK
card_name('konda\'s hatamoto', 'Konda\'s Hatamoto').
card_type('konda\'s hatamoto', 'Creature — Human Samurai').
card_types('konda\'s hatamoto', ['Creature']).
card_subtypes('konda\'s hatamoto', ['Human', 'Samurai']).
card_colors('konda\'s hatamoto', ['W']).
card_text('konda\'s hatamoto', 'Bushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)\nAs long as you control a legendary Samurai, Konda\'s Hatamoto gets +1/+2 and has vigilance. (Attacking doesn\'t cause this creature to tap.)').
card_mana_cost('konda\'s hatamoto', ['1', 'W']).
card_cmc('konda\'s hatamoto', 2).
card_layout('konda\'s hatamoto', 'normal').
card_power('konda\'s hatamoto', 1).
card_toughness('konda\'s hatamoto', 2).

% Found in: CHK
card_name('konda, lord of eiganjo', 'Konda, Lord of Eiganjo').
card_type('konda, lord of eiganjo', 'Legendary Creature — Human Samurai').
card_types('konda, lord of eiganjo', ['Creature']).
card_subtypes('konda, lord of eiganjo', ['Human', 'Samurai']).
card_supertypes('konda, lord of eiganjo', ['Legendary']).
card_colors('konda, lord of eiganjo', ['W']).
card_text('konda, lord of eiganjo', 'Indestructible\nVigilance (Attacking doesn\'t cause this creature to tap.)\nBushido 5 (Whenever this creature blocks or becomes blocked, it gets +5/+5 until end of turn.)').
card_mana_cost('konda, lord of eiganjo', ['5', 'W', 'W']).
card_cmc('konda, lord of eiganjo', 7).
card_layout('konda, lord of eiganjo', 'normal').
card_power('konda, lord of eiganjo', 3).
card_toughness('konda, lord of eiganjo', 3).

% Found in: PTK
card_name('kongming\'s contraptions', 'Kongming\'s Contraptions').
card_type('kongming\'s contraptions', 'Creature — Human Soldier').
card_types('kongming\'s contraptions', ['Creature']).
card_subtypes('kongming\'s contraptions', ['Human', 'Soldier']).
card_colors('kongming\'s contraptions', ['W']).
card_text('kongming\'s contraptions', '{T}: Kongming\'s Contraptions deals 2 damage to target attacking creature. Activate this ability only during the declare attackers step and only if you\'ve been attacked this step.').
card_mana_cost('kongming\'s contraptions', ['3', 'W']).
card_cmc('kongming\'s contraptions', 4).
card_layout('kongming\'s contraptions', 'normal').
card_power('kongming\'s contraptions', 2).
card_toughness('kongming\'s contraptions', 4).

% Found in: C13, ME3, PTK, VMA
card_name('kongming, \"sleeping dragon\"', 'Kongming, \"Sleeping Dragon\"').
card_type('kongming, \"sleeping dragon\"', 'Legendary Creature — Human Advisor').
card_types('kongming, \"sleeping dragon\"', ['Creature']).
card_subtypes('kongming, \"sleeping dragon\"', ['Human', 'Advisor']).
card_supertypes('kongming, \"sleeping dragon\"', ['Legendary']).
card_colors('kongming, \"sleeping dragon\"', ['W']).
card_text('kongming, \"sleeping dragon\"', 'Other creatures you control get +1/+1.').
card_mana_cost('kongming, \"sleeping dragon\"', ['2', 'W', 'W']).
card_cmc('kongming, \"sleeping dragon\"', 4).
card_layout('kongming, \"sleeping dragon\"', 'normal').
card_power('kongming, \"sleeping dragon\"', 2).
card_toughness('kongming, \"sleeping dragon\"', 2).

% Found in: VIS
card_name('kookus', 'Kookus').
card_type('kookus', 'Creature — Djinn').
card_types('kookus', ['Creature']).
card_subtypes('kookus', ['Djinn']).
card_colors('kookus', ['R']).
card_text('kookus', 'Trample\nAt the beginning of your upkeep, if you don\'t control a creature named Keeper of Kookus, Kookus deals 3 damage to you and attacks this turn if able.\n{R}: Kookus gets +1/+0 until end of turn.').
card_mana_cost('kookus', ['3', 'R', 'R']).
card_cmc('kookus', 5).
card_layout('kookus', 'normal').
card_power('kookus', 3).
card_toughness('kookus', 5).
card_reserved('kookus').

% Found in: DDF, ZEN
card_name('kor aeronaut', 'Kor Aeronaut').
card_type('kor aeronaut', 'Creature — Kor Soldier').
card_types('kor aeronaut', ['Creature']).
card_subtypes('kor aeronaut', ['Kor', 'Soldier']).
card_colors('kor aeronaut', ['W']).
card_text('kor aeronaut', 'Kicker {1}{W} (You may pay an additional {1}{W} as you cast this spell.)\nFlying\nWhen Kor Aeronaut enters the battlefield, if it was kicked, target creature gains flying until end of turn.').
card_mana_cost('kor aeronaut', ['W', 'W']).
card_cmc('kor aeronaut', 2).
card_layout('kor aeronaut', 'normal').
card_power('kor aeronaut', 2).
card_toughness('kor aeronaut', 2).

% Found in: BFZ
card_name('kor bladewhirl', 'Kor Bladewhirl').
card_type('kor bladewhirl', 'Creature — Kor Soldier Ally').
card_types('kor bladewhirl', ['Creature']).
card_subtypes('kor bladewhirl', ['Kor', 'Soldier', 'Ally']).
card_colors('kor bladewhirl', ['W']).
card_text('kor bladewhirl', 'Rally — Whenever Kor Bladewhirl or another Ally enters the battlefield under your control, creatures you control gain first strike until end of turn.').
card_mana_cost('kor bladewhirl', ['1', 'W']).
card_cmc('kor bladewhirl', 2).
card_layout('kor bladewhirl', 'normal').
card_power('kor bladewhirl', 2).
card_toughness('kor bladewhirl', 2).

% Found in: DDI, ZEN
card_name('kor cartographer', 'Kor Cartographer').
card_type('kor cartographer', 'Creature — Kor Scout').
card_types('kor cartographer', ['Creature']).
card_subtypes('kor cartographer', ['Kor', 'Scout']).
card_colors('kor cartographer', ['W']).
card_text('kor cartographer', 'When Kor Cartographer enters the battlefield, you may search your library for a Plains card, put it onto the battlefield tapped, then shuffle your library.').
card_mana_cost('kor cartographer', ['3', 'W']).
card_cmc('kor cartographer', 4).
card_layout('kor cartographer', 'normal').
card_power('kor cartographer', 2).
card_toughness('kor cartographer', 2).

% Found in: BFZ
card_name('kor castigator', 'Kor Castigator').
card_type('kor castigator', 'Creature — Kor Wizard Ally').
card_types('kor castigator', ['Creature']).
card_subtypes('kor castigator', ['Kor', 'Wizard', 'Ally']).
card_colors('kor castigator', ['W']).
card_text('kor castigator', 'Kor Castigator can\'t be blocked by Eldrazi Scions.').
card_mana_cost('kor castigator', ['1', 'W']).
card_cmc('kor castigator', 2).
card_layout('kor castigator', 'normal').
card_power('kor castigator', 3).
card_toughness('kor castigator', 1).

% Found in: CNS, EXO, TPR
card_name('kor chant', 'Kor Chant').
card_type('kor chant', 'Instant').
card_types('kor chant', ['Instant']).
card_subtypes('kor chant', []).
card_colors('kor chant', ['W']).
card_text('kor chant', 'All damage that would be dealt this turn to target creature you control by a source of your choice is dealt to another target creature instead.').
card_mana_cost('kor chant', ['2', 'W']).
card_cmc('kor chant', 3).
card_layout('kor chant', 'normal').

% Found in: PLC
card_name('kor dirge', 'Kor Dirge').
card_type('kor dirge', 'Instant').
card_types('kor dirge', ['Instant']).
card_subtypes('kor dirge', []).
card_colors('kor dirge', ['B']).
card_text('kor dirge', 'All damage that would be dealt this turn to target creature you control by a source of your choice is dealt to another target creature instead.').
card_mana_cost('kor dirge', ['2', 'B']).
card_cmc('kor dirge', 3).
card_layout('kor dirge', 'normal').

% Found in: MM2, ZEN, pWPN
card_name('kor duelist', 'Kor Duelist').
card_type('kor duelist', 'Creature — Kor Soldier').
card_types('kor duelist', ['Creature']).
card_subtypes('kor duelist', ['Kor', 'Soldier']).
card_colors('kor duelist', ['W']).
card_text('kor duelist', 'As long as Kor Duelist is equipped, it has double strike. (It deals both first-strike and regular combat damage.)').
card_mana_cost('kor duelist', ['W']).
card_cmc('kor duelist', 1).
card_layout('kor duelist', 'normal').
card_power('kor duelist', 1).
card_toughness('kor duelist', 1).

% Found in: BFZ
card_name('kor entanglers', 'Kor Entanglers').
card_type('kor entanglers', 'Creature — Kor Soldier Ally').
card_types('kor entanglers', ['Creature']).
card_subtypes('kor entanglers', ['Kor', 'Soldier', 'Ally']).
card_colors('kor entanglers', ['W']).
card_text('kor entanglers', 'Rally — Whenever Kor Entanglers or another Ally enters the battlefield under your control, tap target creature an opponent controls.').
card_mana_cost('kor entanglers', ['4', 'W']).
card_cmc('kor entanglers', 5).
card_layout('kor entanglers', 'normal').
card_power('kor entanglers', 3).
card_toughness('kor entanglers', 4).

% Found in: WWK, pWPN
card_name('kor firewalker', 'Kor Firewalker').
card_type('kor firewalker', 'Creature — Kor Soldier').
card_types('kor firewalker', ['Creature']).
card_subtypes('kor firewalker', ['Kor', 'Soldier']).
card_colors('kor firewalker', ['W']).
card_text('kor firewalker', 'Protection from red\nWhenever a player casts a red spell, you may gain 1 life.').
card_mana_cost('kor firewalker', ['W', 'W']).
card_cmc('kor firewalker', 2).
card_layout('kor firewalker', 'normal').
card_power('kor firewalker', 2).
card_toughness('kor firewalker', 2).

% Found in: NMS
card_name('kor haven', 'Kor Haven').
card_type('kor haven', 'Legendary Land').
card_types('kor haven', ['Land']).
card_subtypes('kor haven', []).
card_supertypes('kor haven', ['Legendary']).
card_colors('kor haven', []).
card_text('kor haven', '{T}: Add {1} to your mana pool.\n{1}{W}, {T}: Prevent all combat damage that would be dealt by target attacking creature this turn.').
card_layout('kor haven', 'normal').

% Found in: DDF, DDN, ZEN
card_name('kor hookmaster', 'Kor Hookmaster').
card_type('kor hookmaster', 'Creature — Kor Soldier').
card_types('kor hookmaster', ['Creature']).
card_subtypes('kor hookmaster', ['Kor', 'Soldier']).
card_colors('kor hookmaster', ['W']).
card_text('kor hookmaster', 'When Kor Hookmaster enters the battlefield, tap target creature an opponent controls. That creature doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('kor hookmaster', ['2', 'W']).
card_cmc('kor hookmaster', 3).
card_layout('kor hookmaster', 'normal').
card_power('kor hookmaster', 2).
card_toughness('kor hookmaster', 2).

% Found in: ROE
card_name('kor line-slinger', 'Kor Line-Slinger').
card_type('kor line-slinger', 'Creature — Kor Scout').
card_types('kor line-slinger', ['Creature']).
card_subtypes('kor line-slinger', ['Kor', 'Scout']).
card_colors('kor line-slinger', ['W']).
card_text('kor line-slinger', '{T}: Tap target creature with power 3 or less.').
card_mana_cost('kor line-slinger', ['1', 'W']).
card_cmc('kor line-slinger', 2).
card_layout('kor line-slinger', 'normal').
card_power('kor line-slinger', 0).
card_toughness('kor line-slinger', 1).

% Found in: ZEN
card_name('kor outfitter', 'Kor Outfitter').
card_type('kor outfitter', 'Creature — Kor Soldier').
card_types('kor outfitter', ['Creature']).
card_subtypes('kor outfitter', ['Kor', 'Soldier']).
card_colors('kor outfitter', ['W']).
card_text('kor outfitter', 'When Kor Outfitter enters the battlefield, you may attach target Equipment you control to target creature you control.').
card_mana_cost('kor outfitter', ['W', 'W']).
card_cmc('kor outfitter', 2).
card_layout('kor outfitter', 'normal').
card_power('kor outfitter', 2).
card_toughness('kor outfitter', 2).

% Found in: C14, HOP, ZEN
card_name('kor sanctifiers', 'Kor Sanctifiers').
card_type('kor sanctifiers', 'Creature — Kor Cleric').
card_types('kor sanctifiers', ['Creature']).
card_subtypes('kor sanctifiers', ['Kor', 'Cleric']).
card_colors('kor sanctifiers', ['W']).
card_text('kor sanctifiers', 'Kicker {W} (You may pay an additional {W} as you cast this spell.)\nWhen Kor Sanctifiers enters the battlefield, if it was kicked, destroy target artifact or enchantment.').
card_mana_cost('kor sanctifiers', ['2', 'W']).
card_cmc('kor sanctifiers', 3).
card_layout('kor sanctifiers', 'normal').
card_power('kor sanctifiers', 2).
card_toughness('kor sanctifiers', 3).

% Found in: DDF, DDO, ZEN, pMEI
card_name('kor skyfisher', 'Kor Skyfisher').
card_type('kor skyfisher', 'Creature — Kor Soldier').
card_types('kor skyfisher', ['Creature']).
card_subtypes('kor skyfisher', ['Kor', 'Soldier']).
card_colors('kor skyfisher', ['W']).
card_text('kor skyfisher', 'Flying\nWhen Kor Skyfisher enters the battlefield, return a permanent you control to its owner\'s hand.').
card_mana_cost('kor skyfisher', ['1', 'W']).
card_cmc('kor skyfisher', 2).
card_layout('kor skyfisher', 'normal').
card_power('kor skyfisher', 2).
card_toughness('kor skyfisher', 3).

% Found in: PC2, ROE
card_name('kor spiritdancer', 'Kor Spiritdancer').
card_type('kor spiritdancer', 'Creature — Kor Wizard').
card_types('kor spiritdancer', ['Creature']).
card_subtypes('kor spiritdancer', ['Kor', 'Wizard']).
card_colors('kor spiritdancer', ['W']).
card_text('kor spiritdancer', 'Kor Spiritdancer gets +2/+2 for each Aura attached to it.\nWhenever you cast an Aura spell, you may draw a card.').
card_mana_cost('kor spiritdancer', ['1', 'W']).
card_cmc('kor spiritdancer', 2).
card_layout('kor spiritdancer', 'normal').
card_power('kor spiritdancer', 0).
card_toughness('kor spiritdancer', 2).

% Found in: FUT, pPRE
card_name('korlash, heir to blackblade', 'Korlash, Heir to Blackblade').
card_type('korlash, heir to blackblade', 'Legendary Creature — Zombie Warrior').
card_types('korlash, heir to blackblade', ['Creature']).
card_subtypes('korlash, heir to blackblade', ['Zombie', 'Warrior']).
card_supertypes('korlash, heir to blackblade', ['Legendary']).
card_colors('korlash, heir to blackblade', ['B']).
card_text('korlash, heir to blackblade', 'Korlash, Heir to Blackblade\'s power and toughness are each equal to the number of Swamps you control.\n{1}{B}: Regenerate Korlash.\nGrandeur — Discard another card named Korlash, Heir to Blackblade: Search your library for up to two Swamp cards, put them onto the battlefield tapped, then shuffle your library.').
card_mana_cost('korlash, heir to blackblade', ['2', 'B', 'B']).
card_cmc('korlash, heir to blackblade', 4).
card_layout('korlash, heir to blackblade', 'normal').
card_power('korlash, heir to blackblade', '*').
card_toughness('korlash, heir to blackblade', '*').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB, ME4
card_name('kormus bell', 'Kormus Bell').
card_type('kormus bell', 'Artifact').
card_types('kormus bell', ['Artifact']).
card_subtypes('kormus bell', []).
card_colors('kormus bell', []).
card_text('kormus bell', 'All Swamps are 1/1 black creatures that are still lands.').
card_mana_cost('kormus bell', ['4']).
card_cmc('kormus bell', 4).
card_layout('kormus bell', 'normal').

% Found in: DGM
card_name('korozda gorgon', 'Korozda Gorgon').
card_type('korozda gorgon', 'Creature — Gorgon').
card_types('korozda gorgon', ['Creature']).
card_subtypes('korozda gorgon', ['Gorgon']).
card_colors('korozda gorgon', ['B', 'G']).
card_text('korozda gorgon', 'Deathtouch\n{2}, Remove a +1/+1 counter from a creature you control: Target creature gets -1/-1 until end of turn.').
card_mana_cost('korozda gorgon', ['3', 'B', 'G']).
card_cmc('korozda gorgon', 5).
card_layout('korozda gorgon', 'normal').
card_power('korozda gorgon', 2).
card_toughness('korozda gorgon', 5).

% Found in: DDJ, RTR
card_name('korozda guildmage', 'Korozda Guildmage').
card_type('korozda guildmage', 'Creature — Elf Shaman').
card_types('korozda guildmage', ['Creature']).
card_subtypes('korozda guildmage', ['Elf', 'Shaman']).
card_colors('korozda guildmage', ['B', 'G']).
card_text('korozda guildmage', '{1}{B}{G}: Target creature gets +1/+1 and gains intimidate until end of turn. (It can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\n{2}{B}{G}, Sacrifice a nontoken creature: Put X 1/1 green Saproling creature tokens onto the battlefield, where X is the sacrificed creature\'s toughness.').
card_mana_cost('korozda guildmage', ['B', 'G']).
card_cmc('korozda guildmage', 2).
card_layout('korozda guildmage', 'normal').
card_power('korozda guildmage', 2).
card_toughness('korozda guildmage', 2).

% Found in: RTR
card_name('korozda monitor', 'Korozda Monitor').
card_type('korozda monitor', 'Creature — Lizard').
card_types('korozda monitor', ['Creature']).
card_subtypes('korozda monitor', ['Lizard']).
card_colors('korozda monitor', ['G']).
card_text('korozda monitor', 'Trample\nScavenge {5}{G}{G} ({5}{G}{G}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_mana_cost('korozda monitor', ['2', 'G', 'G']).
card_cmc('korozda monitor', 4).
card_layout('korozda monitor', 'normal').
card_power('korozda monitor', 3).
card_toughness('korozda monitor', 3).

% Found in: HML
card_name('koskun falls', 'Koskun Falls').
card_type('koskun falls', 'World Enchantment').
card_types('koskun falls', ['Enchantment']).
card_subtypes('koskun falls', []).
card_supertypes('koskun falls', ['World']).
card_colors('koskun falls', ['B']).
card_text('koskun falls', 'At the beginning of your upkeep, sacrifice Koskun Falls unless you tap an untapped creature you control.\nCreatures can\'t attack you unless their controller pays {2} for each creature he or she controls that\'s attacking you.').
card_mana_cost('koskun falls', ['2', 'B', 'B']).
card_cmc('koskun falls', 4).
card_layout('koskun falls', 'normal').
card_reserved('koskun falls').

% Found in: HML
card_name('koskun keep', 'Koskun Keep').
card_type('koskun keep', 'Land').
card_types('koskun keep', ['Land']).
card_subtypes('koskun keep', []).
card_colors('koskun keep', []).
card_text('koskun keep', '{T}: Add {1} to your mana pool.\n{1}, {T}: Add {R} to your mana pool.\n{2}, {T}: Add {B} or {G} to your mana pool.').
card_layout('koskun keep', 'normal').

% Found in: DDI, SOM
card_name('koth of the hammer', 'Koth of the Hammer').
card_type('koth of the hammer', 'Planeswalker — Koth').
card_types('koth of the hammer', ['Planeswalker']).
card_subtypes('koth of the hammer', ['Koth']).
card_colors('koth of the hammer', ['R']).
card_text('koth of the hammer', '+1: Untap target Mountain. It becomes a 4/4 red Elemental creature until end of turn. It\'s still a land.\n−2: Add {R} to your mana pool for each Mountain you control.\n−5: You get an emblem with \"Mountains you control have ‘{T}: This land deals 1 damage to target creature or player.\'\"').
card_mana_cost('koth of the hammer', ['2', 'R', 'R']).
card_cmc('koth of the hammer', 4).
card_layout('koth of the hammer', 'normal').
card_loyalty('koth of the hammer', 3).

% Found in: MBS
card_name('koth\'s courier', 'Koth\'s Courier').
card_type('koth\'s courier', 'Creature — Human Rogue').
card_types('koth\'s courier', ['Creature']).
card_subtypes('koth\'s courier', ['Human', 'Rogue']).
card_colors('koth\'s courier', ['R']).
card_text('koth\'s courier', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('koth\'s courier', ['1', 'R', 'R']).
card_cmc('koth\'s courier', 3).
card_layout('koth\'s courier', 'normal').
card_power('koth\'s courier', 2).
card_toughness('koth\'s courier', 3).

% Found in: ORI
card_name('kothophed, soul hoarder', 'Kothophed, Soul Hoarder').
card_type('kothophed, soul hoarder', 'Legendary Creature — Demon').
card_types('kothophed, soul hoarder', ['Creature']).
card_subtypes('kothophed, soul hoarder', ['Demon']).
card_supertypes('kothophed, soul hoarder', ['Legendary']).
card_colors('kothophed, soul hoarder', ['B']).
card_text('kothophed, soul hoarder', 'Flying\nWhenever a permanent owned by another player is put into a graveyard from the battlefield, you draw a card and you lose 1 life.').
card_mana_cost('kothophed, soul hoarder', ['4', 'B', 'B']).
card_cmc('kothophed, soul hoarder', 6).
card_layout('kothophed, soul hoarder', 'normal').
card_power('kothophed, soul hoarder', 6).
card_toughness('kothophed, soul hoarder', 6).

% Found in: BFZ
card_name('kozilek\'s channeler', 'Kozilek\'s Channeler').
card_type('kozilek\'s channeler', 'Creature — Eldrazi').
card_types('kozilek\'s channeler', ['Creature']).
card_subtypes('kozilek\'s channeler', ['Eldrazi']).
card_colors('kozilek\'s channeler', []).
card_text('kozilek\'s channeler', '{T}: Add {2} to your mana pool.').
card_mana_cost('kozilek\'s channeler', ['5']).
card_cmc('kozilek\'s channeler', 5).
card_layout('kozilek\'s channeler', 'normal').
card_power('kozilek\'s channeler', 4).
card_toughness('kozilek\'s channeler', 4).

% Found in: MM2, ROE
card_name('kozilek\'s predator', 'Kozilek\'s Predator').
card_type('kozilek\'s predator', 'Creature — Eldrazi Drone').
card_types('kozilek\'s predator', ['Creature']).
card_subtypes('kozilek\'s predator', ['Eldrazi', 'Drone']).
card_colors('kozilek\'s predator', ['G']).
card_text('kozilek\'s predator', 'When Kozilek\'s Predator enters the battlefield, put two 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('kozilek\'s predator', ['3', 'G']).
card_cmc('kozilek\'s predator', 4).
card_layout('kozilek\'s predator', 'normal').
card_power('kozilek\'s predator', 3).
card_toughness('kozilek\'s predator', 3).

% Found in: BFZ
card_name('kozilek\'s sentinel', 'Kozilek\'s Sentinel').
card_type('kozilek\'s sentinel', 'Creature — Eldrazi Drone').
card_types('kozilek\'s sentinel', ['Creature']).
card_subtypes('kozilek\'s sentinel', ['Eldrazi', 'Drone']).
card_colors('kozilek\'s sentinel', []).
card_text('kozilek\'s sentinel', 'Devoid (This card has no color.)\nWhenever you cast a colorless spell, Kozilek\'s Sentinel gets +1/+0 until end of turn.').
card_mana_cost('kozilek\'s sentinel', ['1', 'R']).
card_cmc('kozilek\'s sentinel', 2).
card_layout('kozilek\'s sentinel', 'normal').
card_power('kozilek\'s sentinel', 1).
card_toughness('kozilek\'s sentinel', 4).

% Found in: MM2, ROE
card_name('kozilek, butcher of truth', 'Kozilek, Butcher of Truth').
card_type('kozilek, butcher of truth', 'Legendary Creature — Eldrazi').
card_types('kozilek, butcher of truth', ['Creature']).
card_subtypes('kozilek, butcher of truth', ['Eldrazi']).
card_supertypes('kozilek, butcher of truth', ['Legendary']).
card_colors('kozilek, butcher of truth', []).
card_text('kozilek, butcher of truth', 'When you cast Kozilek, Butcher of Truth, draw four cards.\nAnnihilator 4 (Whenever this creature attacks, defending player sacrifices four permanents.)\nWhen Kozilek is put into a graveyard from anywhere, its owner shuffles his or her graveyard into his or her library.').
card_mana_cost('kozilek, butcher of truth', ['10']).
card_cmc('kozilek, butcher of truth', 10).
card_layout('kozilek, butcher of truth', 'normal').
card_power('kozilek, butcher of truth', 12).
card_toughness('kozilek, butcher of truth', 12).

% Found in: BNG
card_name('kragma butcher', 'Kragma Butcher').
card_type('kragma butcher', 'Creature — Minotaur Warrior').
card_types('kragma butcher', ['Creature']).
card_subtypes('kragma butcher', ['Minotaur', 'Warrior']).
card_colors('kragma butcher', ['R']).
card_text('kragma butcher', 'Inspired — Whenever Kragma Butcher becomes untapped, it gets +2/+0 until end of turn.').
card_mana_cost('kragma butcher', ['2', 'R']).
card_cmc('kragma butcher', 3).
card_layout('kragma butcher', 'normal').
card_power('kragma butcher', 2).
card_toughness('kragma butcher', 3).

% Found in: THS
card_name('kragma warcaller', 'Kragma Warcaller').
card_type('kragma warcaller', 'Creature — Minotaur Warrior').
card_types('kragma warcaller', ['Creature']).
card_subtypes('kragma warcaller', ['Minotaur', 'Warrior']).
card_colors('kragma warcaller', ['B', 'R']).
card_text('kragma warcaller', 'Minotaur creatures you control have haste.\nWhenever a Minotaur you control attacks, it gets +2/+0 until end of turn.').
card_mana_cost('kragma warcaller', ['3', 'B', 'R']).
card_cmc('kragma warcaller', 5).
card_layout('kragma warcaller', 'normal').
card_power('kragma warcaller', 2).
card_toughness('kragma warcaller', 3).

% Found in: DDO
card_name('kraken', 'Kraken').
card_type('kraken', ' Creature — Kraken').
card_types('kraken', ['Creature']).
card_subtypes('kraken', ['Kraken']).
card_colors('kraken', []).
card_text('kraken', '').
card_layout('kraken', 'token').
card_power('kraken', 9).
card_toughness('kraken', 9).

% Found in: M13, ZEN
card_name('kraken hatchling', 'Kraken Hatchling').
card_type('kraken hatchling', 'Creature — Kraken').
card_types('kraken hatchling', ['Creature']).
card_subtypes('kraken hatchling', ['Kraken']).
card_colors('kraken hatchling', ['U']).
card_text('kraken hatchling', '').
card_mana_cost('kraken hatchling', ['U']).
card_cmc('kraken hatchling', 1).
card_layout('kraken hatchling', 'normal').
card_power('kraken hatchling', 0).
card_toughness('kraken hatchling', 4).

% Found in: BNG
card_name('kraken of the straits', 'Kraken of the Straits').
card_type('kraken of the straits', 'Creature — Kraken').
card_types('kraken of the straits', ['Creature']).
card_subtypes('kraken of the straits', ['Kraken']).
card_colors('kraken of the straits', ['U']).
card_text('kraken of the straits', 'Creatures with power less than the number of Islands you control can\'t block Kraken of the Straits.').
card_mana_cost('kraken of the straits', ['5', 'U', 'U']).
card_cmc('kraken of the straits', 7).
card_layout('kraken of the straits', 'normal').
card_power('kraken of the straits', 6).
card_toughness('kraken of the straits', 6).

% Found in: 10E, 9ED, DPA, DST, M10, M11, M12
card_name('kraken\'s eye', 'Kraken\'s Eye').
card_type('kraken\'s eye', 'Artifact').
card_types('kraken\'s eye', ['Artifact']).
card_subtypes('kraken\'s eye', []).
card_colors('kraken\'s eye', []).
card_text('kraken\'s eye', 'Whenever a player casts a blue spell, you may gain 1 life.').
card_mana_cost('kraken\'s eye', ['2']).
card_cmc('kraken\'s eye', 2).
card_layout('kraken\'s eye', 'normal').

% Found in: TMP, TPR
card_name('krakilin', 'Krakilin').
card_type('krakilin', 'Creature — Beast').
card_types('krakilin', ['Creature']).
card_subtypes('krakilin', ['Beast']).
card_colors('krakilin', ['G']).
card_text('krakilin', 'Krakilin enters the battlefield with X +1/+1 counters on it.\n{1}{G}: Regenerate Krakilin.').
card_mana_cost('krakilin', ['X', 'G', 'G']).
card_cmc('krakilin', 2).
card_layout('krakilin', 'normal').
card_power('krakilin', 0).
card_toughness('krakilin', 0).

% Found in: DKA
card_name('krallenhorde killer', 'Krallenhorde Killer').
card_type('krallenhorde killer', 'Creature — Werewolf').
card_types('krallenhorde killer', ['Creature']).
card_subtypes('krallenhorde killer', ['Werewolf']).
card_colors('krallenhorde killer', ['G']).
card_text('krallenhorde killer', '{3}{G}: Krallenhorde Killer gets +4/+4 until end of turn. Activate this ability only once each turn.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Krallenhorde Killer.').
card_layout('krallenhorde killer', 'double-faced').
card_power('krallenhorde killer', 2).
card_toughness('krallenhorde killer', 2).

% Found in: ISD
card_name('krallenhorde wantons', 'Krallenhorde Wantons').
card_type('krallenhorde wantons', 'Creature — Werewolf').
card_types('krallenhorde wantons', ['Creature']).
card_subtypes('krallenhorde wantons', ['Werewolf']).
card_colors('krallenhorde wantons', ['G']).
card_text('krallenhorde wantons', 'At the beginning of each upkeep, if a player cast two or more spells last turn, transform Krallenhorde Wantons.').
card_layout('krallenhorde wantons', 'double-faced').
card_power('krallenhorde wantons', 7).
card_toughness('krallenhorde wantons', 7).

% Found in: CON
card_name('kranioceros', 'Kranioceros').
card_type('kranioceros', 'Creature — Beast').
card_types('kranioceros', ['Creature']).
card_subtypes('kranioceros', ['Beast']).
card_colors('kranioceros', ['R']).
card_text('kranioceros', '{1}{W}: Kranioceros gets +0/+3 until end of turn.').
card_mana_cost('kranioceros', ['4', 'R']).
card_cmc('kranioceros', 5).
card_layout('kranioceros', 'normal').
card_power('kranioceros', 5).
card_toughness('kranioceros', 2).

% Found in: MRD
card_name('krark\'s thumb', 'Krark\'s Thumb').
card_type('krark\'s thumb', 'Legendary Artifact').
card_types('krark\'s thumb', ['Artifact']).
card_subtypes('krark\'s thumb', []).
card_supertypes('krark\'s thumb', ['Legendary']).
card_colors('krark\'s thumb', []).
card_text('krark\'s thumb', 'If you would flip a coin, instead flip two coins and ignore one.').
card_mana_cost('krark\'s thumb', ['2']).
card_cmc('krark\'s thumb', 2).
card_layout('krark\'s thumb', 'normal').

% Found in: 5DN
card_name('krark-clan engineers', 'Krark-Clan Engineers').
card_type('krark-clan engineers', 'Creature — Goblin Artificer').
card_types('krark-clan engineers', ['Creature']).
card_subtypes('krark-clan engineers', ['Goblin', 'Artificer']).
card_colors('krark-clan engineers', ['R']).
card_text('krark-clan engineers', '{R}, Sacrifice two artifacts: Destroy target artifact.').
card_mana_cost('krark-clan engineers', ['3', 'R']).
card_cmc('krark-clan engineers', 4).
card_layout('krark-clan engineers', 'normal').
card_power('krark-clan engineers', 2).
card_toughness('krark-clan engineers', 2).

% Found in: MRD
card_name('krark-clan grunt', 'Krark-Clan Grunt').
card_type('krark-clan grunt', 'Creature — Goblin Warrior').
card_types('krark-clan grunt', ['Creature']).
card_subtypes('krark-clan grunt', ['Goblin', 'Warrior']).
card_colors('krark-clan grunt', ['R']).
card_text('krark-clan grunt', 'Sacrifice an artifact: Krark-Clan Grunt gets +1/+0 and gains first strike until end of turn.').
card_mana_cost('krark-clan grunt', ['2', 'R']).
card_cmc('krark-clan grunt', 3).
card_layout('krark-clan grunt', 'normal').
card_power('krark-clan grunt', 2).
card_toughness('krark-clan grunt', 2).

% Found in: 5DN
card_name('krark-clan ironworks', 'Krark-Clan Ironworks').
card_type('krark-clan ironworks', 'Artifact').
card_types('krark-clan ironworks', ['Artifact']).
card_subtypes('krark-clan ironworks', []).
card_colors('krark-clan ironworks', []).
card_text('krark-clan ironworks', 'Sacrifice an artifact: Add {2} to your mana pool.').
card_mana_cost('krark-clan ironworks', ['4']).
card_cmc('krark-clan ironworks', 4).
card_layout('krark-clan ironworks', 'normal').

% Found in: 5DN
card_name('krark-clan ogre', 'Krark-Clan Ogre').
card_type('krark-clan ogre', 'Creature — Ogre').
card_types('krark-clan ogre', ['Creature']).
card_subtypes('krark-clan ogre', ['Ogre']).
card_colors('krark-clan ogre', ['R']).
card_text('krark-clan ogre', '{R}, Sacrifice an artifact: Target creature can\'t block this turn.').
card_mana_cost('krark-clan ogre', ['3', 'R', 'R']).
card_cmc('krark-clan ogre', 5).
card_layout('krark-clan ogre', 'normal').
card_power('krark-clan ogre', 3).
card_toughness('krark-clan ogre', 3).

% Found in: MRD
card_name('krark-clan shaman', 'Krark-Clan Shaman').
card_type('krark-clan shaman', 'Creature — Goblin Shaman').
card_types('krark-clan shaman', ['Creature']).
card_subtypes('krark-clan shaman', ['Goblin', 'Shaman']).
card_colors('krark-clan shaman', ['R']).
card_text('krark-clan shaman', 'Sacrifice an artifact: Krark-Clan Shaman deals 1 damage to each creature without flying.').
card_mana_cost('krark-clan shaman', ['R']).
card_cmc('krark-clan shaman', 1).
card_layout('krark-clan shaman', 'normal').
card_power('krark-clan shaman', 1).
card_toughness('krark-clan shaman', 1).

% Found in: DST
card_name('krark-clan stoker', 'Krark-Clan Stoker').
card_type('krark-clan stoker', 'Creature — Goblin Shaman').
card_types('krark-clan stoker', ['Creature']).
card_subtypes('krark-clan stoker', ['Goblin', 'Shaman']).
card_colors('krark-clan stoker', ['R']).
card_text('krark-clan stoker', '{T}, Sacrifice an artifact: Add {R}{R} to your mana pool.').
card_mana_cost('krark-clan stoker', ['2', 'R']).
card_cmc('krark-clan stoker', 3).
card_layout('krark-clan stoker', 'normal').
card_power('krark-clan stoker', 2).
card_toughness('krark-clan stoker', 2).

% Found in: DGM
card_name('krasis incubation', 'Krasis Incubation').
card_type('krasis incubation', 'Enchantment — Aura').
card_types('krasis incubation', ['Enchantment']).
card_subtypes('krasis incubation', ['Aura']).
card_colors('krasis incubation', ['U', 'G']).
card_text('krasis incubation', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.\n{1}{G}{U}, Return Krasis Incubation to its owner\'s hand: Put two +1/+1 counters on enchanted creature.').
card_mana_cost('krasis incubation', ['2', 'G', 'U']).
card_cmc('krasis incubation', 4).
card_layout('krasis incubation', 'normal').

% Found in: DGM
card_name('kraul warrior', 'Kraul Warrior').
card_type('kraul warrior', 'Creature — Insect Warrior').
card_types('kraul warrior', ['Creature']).
card_subtypes('kraul warrior', ['Insect', 'Warrior']).
card_colors('kraul warrior', ['G']).
card_text('kraul warrior', '{5}{G}: Kraul Warrior gets +3/+3 until end of turn.').
card_mana_cost('kraul warrior', ['1', 'G']).
card_cmc('kraul warrior', 2).
card_layout('kraul warrior', 'normal').
card_power('kraul warrior', 2).
card_toughness('kraul warrior', 2).

% Found in: UGL
card_name('krazy kow', 'Krazy Kow').
card_type('krazy kow', 'Creature — Cow').
card_types('krazy kow', ['Creature']).
card_subtypes('krazy kow', ['Cow']).
card_colors('krazy kow', ['R']).
card_text('krazy kow', 'During your upkeep, roll a six-sided die. On a 1, sacrifice Krazy Kow and it deals 3 damage to each creature and player.').
card_mana_cost('krazy kow', ['3', 'R']).
card_cmc('krazy kow', 4).
card_layout('krazy kow', 'normal').
card_power('krazy kow', 3).
card_toughness('krazy kow', 3).

% Found in: DDN, M13
card_name('krenko\'s command', 'Krenko\'s Command').
card_type('krenko\'s command', 'Sorcery').
card_types('krenko\'s command', ['Sorcery']).
card_subtypes('krenko\'s command', []).
card_colors('krenko\'s command', ['R']).
card_text('krenko\'s command', 'Put two 1/1 red Goblin creature tokens onto the battlefield.').
card_mana_cost('krenko\'s command', ['1', 'R']).
card_cmc('krenko\'s command', 2).
card_layout('krenko\'s command', 'normal').

% Found in: M15
card_name('krenko\'s enforcer', 'Krenko\'s Enforcer').
card_type('krenko\'s enforcer', 'Creature — Goblin Warrior').
card_types('krenko\'s enforcer', ['Creature']).
card_subtypes('krenko\'s enforcer', ['Goblin', 'Warrior']).
card_colors('krenko\'s enforcer', ['R']).
card_text('krenko\'s enforcer', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_mana_cost('krenko\'s enforcer', ['1', 'R', 'R']).
card_cmc('krenko\'s enforcer', 3).
card_layout('krenko\'s enforcer', 'normal').
card_power('krenko\'s enforcer', 2).
card_toughness('krenko\'s enforcer', 2).

% Found in: DDN, M13
card_name('krenko, mob boss', 'Krenko, Mob Boss').
card_type('krenko, mob boss', 'Legendary Creature — Goblin Warrior').
card_types('krenko, mob boss', ['Creature']).
card_subtypes('krenko, mob boss', ['Goblin', 'Warrior']).
card_supertypes('krenko, mob boss', ['Legendary']).
card_colors('krenko, mob boss', ['R']).
card_text('krenko, mob boss', '{T}: Put X 1/1 red Goblin creature tokens onto the battlefield, where X is the number of Goblins you control.').
card_mana_cost('krenko, mob boss', ['2', 'R', 'R']).
card_cmc('krenko, mob boss', 4).
card_layout('krenko, mob boss', 'normal').
card_power('krenko, mob boss', 3).
card_toughness('krenko, mob boss', 3).

% Found in: ALA, V11
card_name('kresh the bloodbraided', 'Kresh the Bloodbraided').
card_type('kresh the bloodbraided', 'Legendary Creature — Human Warrior').
card_types('kresh the bloodbraided', ['Creature']).
card_subtypes('kresh the bloodbraided', ['Human', 'Warrior']).
card_supertypes('kresh the bloodbraided', ['Legendary']).
card_colors('kresh the bloodbraided', ['B', 'R', 'G']).
card_text('kresh the bloodbraided', 'Whenever another creature dies, you may put X +1/+1 counters on Kresh the Bloodbraided, where X is that creature\'s power.').
card_mana_cost('kresh the bloodbraided', ['2', 'B', 'R', 'G']).
card_cmc('kresh the bloodbraided', 5).
card_layout('kresh the bloodbraided', 'normal').
card_power('kresh the bloodbraided', 3).
card_toughness('kresh the bloodbraided', 3).

% Found in: VAN
card_name('kresh the bloodbraided avatar', 'Kresh the Bloodbraided Avatar').
card_type('kresh the bloodbraided avatar', 'Vanguard').
card_types('kresh the bloodbraided avatar', ['Vanguard']).
card_subtypes('kresh the bloodbraided avatar', []).
card_colors('kresh the bloodbraided avatar', []).
card_text('kresh the bloodbraided avatar', 'Whenever a creature you control is devoured, put an X/X green Ooze creature token onto the battlefield, where X is the devoured creature\'s power.').
card_layout('kresh the bloodbraided avatar', 'vanguard').

% Found in: MMQ
card_name('kris mage', 'Kris Mage').
card_type('kris mage', 'Creature — Human Spellshaper').
card_types('kris mage', ['Creature']).
card_subtypes('kris mage', ['Human', 'Spellshaper']).
card_colors('kris mage', ['R']).
card_text('kris mage', '{R}, {T}, Discard a card: Kris Mage deals 1 damage to target creature or player.').
card_mana_cost('kris mage', ['R']).
card_cmc('kris mage', 1).
card_layout('kris mage', 'normal').
card_power('kris mage', 1).
card_toughness('kris mage', 1).

% Found in: PC2
card_name('krond the dawn-clad', 'Krond the Dawn-Clad').
card_type('krond the dawn-clad', 'Legendary Creature — Archon').
card_types('krond the dawn-clad', ['Creature']).
card_subtypes('krond the dawn-clad', ['Archon']).
card_supertypes('krond the dawn-clad', ['Legendary']).
card_colors('krond the dawn-clad', ['W', 'G']).
card_text('krond the dawn-clad', 'Flying, vigilance\nWhenever Krond the Dawn-Clad attacks, if it\'s enchanted, exile target permanent.').
card_mana_cost('krond the dawn-clad', ['G', 'G', 'G', 'W', 'W', 'W']).
card_cmc('krond the dawn-clad', 6).
card_layout('krond the dawn-clad', 'normal').
card_power('krond the dawn-clad', 6).
card_toughness('krond the dawn-clad', 6).

% Found in: HOP
card_name('krosa', 'Krosa').
card_type('krosa', 'Plane — Dominaria').
card_types('krosa', ['Plane']).
card_subtypes('krosa', ['Dominaria']).
card_colors('krosa', []).
card_text('krosa', 'All creatures get +2/+2.\nWhenever you roll {C}, you may add {W}{U}{B}{R}{G} to your mana pool.').
card_layout('krosa', 'plane').

% Found in: ODY
card_name('krosan archer', 'Krosan Archer').
card_type('krosan archer', 'Creature — Centaur Archer').
card_types('krosan archer', ['Creature']).
card_subtypes('krosan archer', ['Centaur', 'Archer']).
card_colors('krosan archer', ['G']).
card_text('krosan archer', 'Reach (This creature can block creatures with flying.)\n{G}, Discard a card: Krosan Archer gets +0/+2 until end of turn.').
card_mana_cost('krosan archer', ['3', 'G']).
card_cmc('krosan archer', 4).
card_layout('krosan archer', 'normal').
card_power('krosan archer', 2).
card_toughness('krosan archer', 3).

% Found in: ODY
card_name('krosan avenger', 'Krosan Avenger').
card_type('krosan avenger', 'Creature — Human Druid').
card_types('krosan avenger', ['Creature']).
card_subtypes('krosan avenger', ['Human', 'Druid']).
card_colors('krosan avenger', ['G']).
card_text('krosan avenger', 'Trample\nThreshold — {1}{G}: Regenerate Krosan Avenger. Activate this ability only if seven or more cards are in your graveyard.').
card_mana_cost('krosan avenger', ['2', 'G']).
card_cmc('krosan avenger', 3).
card_layout('krosan avenger', 'normal').
card_power('krosan avenger', 3).
card_toughness('krosan avenger', 1).

% Found in: ODY
card_name('krosan beast', 'Krosan Beast').
card_type('krosan beast', 'Creature — Squirrel Beast').
card_types('krosan beast', ['Creature']).
card_subtypes('krosan beast', ['Squirrel', 'Beast']).
card_colors('krosan beast', ['G']).
card_text('krosan beast', 'Threshold — Krosan Beast gets +7/+7 as long as seven or more cards are in your graveyard.').
card_mana_cost('krosan beast', ['3', 'G']).
card_cmc('krosan beast', 4).
card_layout('krosan beast', 'normal').
card_power('krosan beast', 1).
card_toughness('krosan beast', 1).

% Found in: LGN, TSB
card_name('krosan cloudscraper', 'Krosan Cloudscraper').
card_type('krosan cloudscraper', 'Creature — Beast Mutant').
card_types('krosan cloudscraper', ['Creature']).
card_subtypes('krosan cloudscraper', ['Beast', 'Mutant']).
card_colors('krosan cloudscraper', ['G']).
card_text('krosan cloudscraper', 'At the beginning of your upkeep, sacrifice Krosan Cloudscraper unless you pay {G}{G}.\nMorph {7}{G}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('krosan cloudscraper', ['7', 'G', 'G', 'G']).
card_cmc('krosan cloudscraper', 10).
card_layout('krosan cloudscraper', 'normal').
card_power('krosan cloudscraper', 13).
card_toughness('krosan cloudscraper', 13).

% Found in: ONS
card_name('krosan colossus', 'Krosan Colossus').
card_type('krosan colossus', 'Creature — Beast').
card_types('krosan colossus', ['Creature']).
card_subtypes('krosan colossus', ['Beast']).
card_colors('krosan colossus', ['G']).
card_text('krosan colossus', 'Morph {6}{G}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('krosan colossus', ['6', 'G', 'G', 'G']).
card_cmc('krosan colossus', 9).
card_layout('krosan colossus', 'normal').
card_power('krosan colossus', 9).
card_toughness('krosan colossus', 9).

% Found in: TOR
card_name('krosan constrictor', 'Krosan Constrictor').
card_type('krosan constrictor', 'Creature — Snake').
card_types('krosan constrictor', ['Creature']).
card_subtypes('krosan constrictor', ['Snake']).
card_colors('krosan constrictor', ['G']).
card_text('krosan constrictor', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)\n{T}: Target black creature gets -2/-0 until end of turn.').
card_mana_cost('krosan constrictor', ['3', 'G']).
card_cmc('krosan constrictor', 4).
card_layout('krosan constrictor', 'normal').
card_power('krosan constrictor', 2).
card_toughness('krosan constrictor', 2).

% Found in: SCG
card_name('krosan drover', 'Krosan Drover').
card_type('krosan drover', 'Creature — Elf').
card_types('krosan drover', ['Creature']).
card_subtypes('krosan drover', ['Elf']).
card_colors('krosan drover', ['G']).
card_text('krosan drover', 'Creature spells you cast with converted mana cost 6 or greater cost {2} less to cast.').
card_mana_cost('krosan drover', ['3', 'G']).
card_cmc('krosan drover', 4).
card_layout('krosan drover', 'normal').
card_power('krosan drover', 2).
card_toughness('krosan drover', 2).

% Found in: C13, MMA, TSP, pFNM
card_name('krosan grip', 'Krosan Grip').
card_type('krosan grip', 'Instant').
card_types('krosan grip', ['Instant']).
card_subtypes('krosan grip', []).
card_colors('krosan grip', ['G']).
card_text('krosan grip', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nDestroy target artifact or enchantment.').
card_mana_cost('krosan grip', ['2', 'G']).
card_cmc('krosan grip', 3).
card_layout('krosan grip', 'normal').

% Found in: ONS
card_name('krosan groundshaker', 'Krosan Groundshaker').
card_type('krosan groundshaker', 'Creature — Beast').
card_types('krosan groundshaker', ['Creature']).
card_subtypes('krosan groundshaker', ['Beast']).
card_colors('krosan groundshaker', ['G']).
card_text('krosan groundshaker', '{G}: Target Beast creature gains trample until end of turn.').
card_mana_cost('krosan groundshaker', ['4', 'G', 'G', 'G']).
card_cmc('krosan groundshaker', 7).
card_layout('krosan groundshaker', 'normal').
card_power('krosan groundshaker', 6).
card_toughness('krosan groundshaker', 6).

% Found in: JUD
card_name('krosan reclamation', 'Krosan Reclamation').
card_type('krosan reclamation', 'Instant').
card_types('krosan reclamation', ['Instant']).
card_subtypes('krosan reclamation', []).
card_colors('krosan reclamation', ['G']).
card_text('krosan reclamation', 'Target player shuffles up to two target cards from his or her graveyard into his or her library.\nFlashback {1}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('krosan reclamation', ['1', 'G']).
card_cmc('krosan reclamation', 2).
card_layout('krosan reclamation', 'normal').

% Found in: TOR
card_name('krosan restorer', 'Krosan Restorer').
card_type('krosan restorer', 'Creature — Human Druid').
card_types('krosan restorer', ['Creature']).
card_subtypes('krosan restorer', ['Human', 'Druid']).
card_colors('krosan restorer', ['G']).
card_text('krosan restorer', '{T}: Untap target land.\nThreshold — {T}: Untap up to three target lands. Activate this ability only if seven or more cards are in your graveyard.').
card_mana_cost('krosan restorer', ['2', 'G']).
card_cmc('krosan restorer', 3).
card_layout('krosan restorer', 'normal').
card_power('krosan restorer', 1).
card_toughness('krosan restorer', 2).

% Found in: ARC, C13, CMD, DD3_GVL, DDD, DDL, ONS, VMA, pFNM
card_name('krosan tusker', 'Krosan Tusker').
card_type('krosan tusker', 'Creature — Boar Beast').
card_types('krosan tusker', ['Creature']).
card_subtypes('krosan tusker', ['Boar', 'Beast']).
card_colors('krosan tusker', ['G']).
card_text('krosan tusker', 'Cycling {2}{G} ({2}{G}, Discard this card: Draw a card.)\nWhen you cycle Krosan Tusker, you may search your library for a basic land card, reveal that card, put it into your hand, then shuffle your library.').
card_mana_cost('krosan tusker', ['5', 'G', 'G']).
card_cmc('krosan tusker', 7).
card_layout('krosan tusker', 'normal').
card_power('krosan tusker', 6).
card_toughness('krosan tusker', 5).

% Found in: ARC, JUD, PC2
card_name('krosan verge', 'Krosan Verge').
card_type('krosan verge', 'Land').
card_types('krosan verge', ['Land']).
card_subtypes('krosan verge', []).
card_colors('krosan verge', []).
card_text('krosan verge', 'Krosan Verge enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{2}, {T}, Sacrifice Krosan Verge: Search your library for a Forest card and a Plains card and put them onto the battlefield tapped. Then shuffle your library.').
card_layout('krosan verge', 'normal').

% Found in: LGN, VMA
card_name('krosan vorine', 'Krosan Vorine').
card_type('krosan vorine', 'Creature — Cat Beast').
card_types('krosan vorine', ['Creature']).
card_subtypes('krosan vorine', ['Cat', 'Beast']).
card_colors('krosan vorine', ['G']).
card_text('krosan vorine', 'Provoke (Whenever this creature attacks, you may have target creature defending player controls untap and block it if able.)\nKrosan Vorine can\'t be blocked by more than one creature.').
card_mana_cost('krosan vorine', ['3', 'G']).
card_cmc('krosan vorine', 4).
card_layout('krosan vorine', 'normal').
card_power('krosan vorine', 3).
card_toughness('krosan vorine', 2).

% Found in: C13, SCG, pFNM
card_name('krosan warchief', 'Krosan Warchief').
card_type('krosan warchief', 'Creature — Beast').
card_types('krosan warchief', ['Creature']).
card_subtypes('krosan warchief', ['Beast']).
card_colors('krosan warchief', ['G']).
card_text('krosan warchief', 'Beast spells you cast cost {1} less to cast.\n{1}{G}: Regenerate target Beast.').
card_mana_cost('krosan warchief', ['2', 'G']).
card_cmc('krosan warchief', 3).
card_layout('krosan warchief', 'normal').
card_power('krosan warchief', 2).
card_toughness('krosan warchief', 2).

% Found in: JUD
card_name('krosan wayfarer', 'Krosan Wayfarer').
card_type('krosan wayfarer', 'Creature — Human Druid').
card_types('krosan wayfarer', ['Creature']).
card_subtypes('krosan wayfarer', ['Human', 'Druid']).
card_colors('krosan wayfarer', ['G']).
card_text('krosan wayfarer', 'Sacrifice Krosan Wayfarer: You may put a land card from your hand onto the battlefield.').
card_mana_cost('krosan wayfarer', ['G']).
card_cmc('krosan wayfarer', 1).
card_layout('krosan wayfarer', 'normal').
card_power('krosan wayfarer', 1).
card_toughness('krosan wayfarer', 1).

% Found in: ICE
card_name('krovikan elementalist', 'Krovikan Elementalist').
card_type('krovikan elementalist', 'Creature — Human Wizard').
card_types('krovikan elementalist', ['Creature']).
card_subtypes('krovikan elementalist', ['Human', 'Wizard']).
card_colors('krovikan elementalist', ['B']).
card_text('krovikan elementalist', '{2}{R}: Target creature gets +1/+0 until end of turn.\n{U}{U}: Target creature you control gains flying until end of turn. Sacrifice it at the beginning of the next end step.').
card_mana_cost('krovikan elementalist', ['B', 'B']).
card_cmc('krovikan elementalist', 2).
card_layout('krovikan elementalist', 'normal').
card_power('krovikan elementalist', 1).
card_toughness('krovikan elementalist', 1).

% Found in: 5ED, ICE, ME2
card_name('krovikan fetish', 'Krovikan Fetish').
card_type('krovikan fetish', 'Enchantment — Aura').
card_types('krovikan fetish', ['Enchantment']).
card_subtypes('krovikan fetish', ['Aura']).
card_colors('krovikan fetish', ['B']).
card_text('krovikan fetish', 'Enchant creature\nWhen Krovikan Fetish enters the battlefield, draw a card at the beginning of the next turn\'s upkeep.\nEnchanted creature gets +1/+1.').
card_mana_cost('krovikan fetish', ['2', 'B']).
card_cmc('krovikan fetish', 3).
card_layout('krovikan fetish', 'normal').

% Found in: ALL, ME2
card_name('krovikan horror', 'Krovikan Horror').
card_type('krovikan horror', 'Creature — Horror Spirit').
card_types('krovikan horror', ['Creature']).
card_subtypes('krovikan horror', ['Horror', 'Spirit']).
card_colors('krovikan horror', ['B']).
card_text('krovikan horror', 'At the beginning of the end step, if Krovikan Horror is in your graveyard with a creature card directly above it, you may return Krovikan Horror to your hand.\n{1}, Sacrifice a creature: Krovikan Horror deals 1 damage to target creature or player.').
card_mana_cost('krovikan horror', ['3', 'B']).
card_cmc('krovikan horror', 4).
card_layout('krovikan horror', 'normal').
card_power('krovikan horror', 2).
card_toughness('krovikan horror', 2).
card_reserved('krovikan horror').

% Found in: CSP, DDM
card_name('krovikan mist', 'Krovikan Mist').
card_type('krovikan mist', 'Creature — Illusion').
card_types('krovikan mist', ['Creature']).
card_subtypes('krovikan mist', ['Illusion']).
card_colors('krovikan mist', ['U']).
card_text('krovikan mist', 'Flying\nKrovikan Mist\'s power and toughness are each equal to the number of Illusions on the battlefield.').
card_mana_cost('krovikan mist', ['1', 'U']).
card_cmc('krovikan mist', 2).
card_layout('krovikan mist', 'normal').
card_power('krovikan mist', '*').
card_toughness('krovikan mist', '*').

% Found in: ALL
card_name('krovikan plague', 'Krovikan Plague').
card_type('krovikan plague', 'Enchantment — Aura').
card_types('krovikan plague', ['Enchantment']).
card_subtypes('krovikan plague', ['Aura']).
card_colors('krovikan plague', ['B']).
card_text('krovikan plague', 'Enchant non-Wall creature you control\nWhen Krovikan Plague enters the battlefield, draw a card at the beginning of the next turn\'s upkeep.\nTap enchanted creature: Krovikan Plague deals 1 damage to target creature or player. Put a -0/-1 counter on enchanted creature. Activate this ability only if enchanted creature is untapped.').
card_mana_cost('krovikan plague', ['2', 'B']).
card_cmc('krovikan plague', 3).
card_layout('krovikan plague', 'normal').

% Found in: CSP
card_name('krovikan rot', 'Krovikan Rot').
card_type('krovikan rot', 'Instant').
card_types('krovikan rot', ['Instant']).
card_subtypes('krovikan rot', []).
card_colors('krovikan rot', ['B']).
card_text('krovikan rot', 'Destroy target creature with power 2 or less.\nRecover {1}{B}{B} (When a creature is put into your graveyard from the battlefield, you may pay {1}{B}{B}. If you do, return this card from your graveyard to your hand. Otherwise, exile this card.)').
card_mana_cost('krovikan rot', ['2', 'B']).
card_cmc('krovikan rot', 3).
card_layout('krovikan rot', 'normal').

% Found in: CSP
card_name('krovikan scoundrel', 'Krovikan Scoundrel').
card_type('krovikan scoundrel', 'Creature — Human Rogue').
card_types('krovikan scoundrel', ['Creature']).
card_subtypes('krovikan scoundrel', ['Human', 'Rogue']).
card_colors('krovikan scoundrel', ['B']).
card_text('krovikan scoundrel', '').
card_mana_cost('krovikan scoundrel', ['1', 'B']).
card_cmc('krovikan scoundrel', 2).
card_layout('krovikan scoundrel', 'normal').
card_power('krovikan scoundrel', 2).
card_toughness('krovikan scoundrel', 1).

% Found in: 5ED, ICE, ME2, VMA
card_name('krovikan sorcerer', 'Krovikan Sorcerer').
card_type('krovikan sorcerer', 'Creature — Human Wizard').
card_types('krovikan sorcerer', ['Creature']).
card_subtypes('krovikan sorcerer', ['Human', 'Wizard']).
card_colors('krovikan sorcerer', ['U']).
card_text('krovikan sorcerer', '{T}, Discard a nonblack card: Draw a card.\n{T}, Discard a black card: Draw two cards, then discard one of them.').
card_mana_cost('krovikan sorcerer', ['2', 'U']).
card_cmc('krovikan sorcerer', 3).
card_layout('krovikan sorcerer', 'normal').
card_power('krovikan sorcerer', 1).
card_toughness('krovikan sorcerer', 1).

% Found in: ICE, ME2
card_name('krovikan vampire', 'Krovikan Vampire').
card_type('krovikan vampire', 'Creature — Vampire').
card_types('krovikan vampire', ['Creature']).
card_subtypes('krovikan vampire', ['Vampire']).
card_colors('krovikan vampire', ['B']).
card_text('krovikan vampire', 'At the beginning of each end step, if a creature dealt damage by Krovikan Vampire this turn died, put that card onto the battlefield under your control. Sacrifice it when you lose control of Krovikan Vampire.').
card_mana_cost('krovikan vampire', ['3', 'B', 'B']).
card_cmc('krovikan vampire', 5).
card_layout('krovikan vampire', 'normal').
card_power('krovikan vampire', 3).
card_toughness('krovikan vampire', 3).

% Found in: CSP
card_name('krovikan whispers', 'Krovikan Whispers').
card_type('krovikan whispers', 'Enchantment — Aura').
card_types('krovikan whispers', ['Enchantment']).
card_subtypes('krovikan whispers', ['Aura']).
card_colors('krovikan whispers', ['U']).
card_text('krovikan whispers', 'Enchant creature\nCumulative upkeep {U} or {B} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nYou control enchanted creature.\nWhen Krovikan Whispers is put into a graveyard from the battlefield, you lose 2 life for each age counter on it.').
card_mana_cost('krovikan whispers', ['3', 'U']).
card_cmc('krovikan whispers', 4).
card_layout('krovikan whispers', 'normal').

% Found in: ISD
card_name('kruin outlaw', 'Kruin Outlaw').
card_type('kruin outlaw', 'Creature — Human Rogue Werewolf').
card_types('kruin outlaw', ['Creature']).
card_subtypes('kruin outlaw', ['Human', 'Rogue', 'Werewolf']).
card_colors('kruin outlaw', ['R']).
card_text('kruin outlaw', 'First strike\nAt the beginning of each upkeep, if no spells were cast last turn, transform Kruin Outlaw.').
card_mana_cost('kruin outlaw', ['1', 'R', 'R']).
card_cmc('kruin outlaw', 3).
card_layout('kruin outlaw', 'double-faced').
card_power('kruin outlaw', 2).
card_toughness('kruin outlaw', 2).
card_sides('kruin outlaw', 'terror of kruin pass').

% Found in: AVR
card_name('kruin striker', 'Kruin Striker').
card_type('kruin striker', 'Creature — Human Warrior').
card_types('kruin striker', ['Creature']).
card_subtypes('kruin striker', ['Human', 'Warrior']).
card_colors('kruin striker', ['R']).
card_text('kruin striker', 'Whenever another creature enters the battlefield under your control, Kruin Striker gets +1/+0 and gains trample until end of turn.').
card_mana_cost('kruin striker', ['1', 'R']).
card_cmc('kruin striker', 2).
card_layout('kruin striker', 'normal').
card_power('kruin striker', 2).
card_toughness('kruin striker', 1).

% Found in: KTK
card_name('krumar bond-kin', 'Krumar Bond-Kin').
card_type('krumar bond-kin', 'Creature — Orc Warrior').
card_types('krumar bond-kin', ['Creature']).
card_subtypes('krumar bond-kin', ['Orc', 'Warrior']).
card_colors('krumar bond-kin', ['B']).
card_text('krumar bond-kin', 'Morph {4}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('krumar bond-kin', ['3', 'B', 'B']).
card_cmc('krumar bond-kin', 5).
card_layout('krumar bond-kin', 'normal').
card_power('krumar bond-kin', 5).
card_toughness('krumar bond-kin', 3).

% Found in: JOU
card_name('kruphix\'s insight', 'Kruphix\'s Insight').
card_type('kruphix\'s insight', 'Sorcery').
card_types('kruphix\'s insight', ['Sorcery']).
card_subtypes('kruphix\'s insight', []).
card_colors('kruphix\'s insight', ['G']).
card_text('kruphix\'s insight', 'Reveal the top six cards of your library. Put up to three enchantment cards from among them into your hand and the rest of the revealed cards into your graveyard.').
card_mana_cost('kruphix\'s insight', ['2', 'G']).
card_cmc('kruphix\'s insight', 3).
card_layout('kruphix\'s insight', 'normal').

% Found in: JOU
card_name('kruphix, god of horizons', 'Kruphix, God of Horizons').
card_type('kruphix, god of horizons', 'Legendary Enchantment Creature — God').
card_types('kruphix, god of horizons', ['Enchantment', 'Creature']).
card_subtypes('kruphix, god of horizons', ['God']).
card_supertypes('kruphix, god of horizons', ['Legendary']).
card_colors('kruphix, god of horizons', ['U', 'G']).
card_text('kruphix, god of horizons', 'Indestructible\nAs long as your devotion to green and blue is less than seven, Kruphix isn\'t a creature.\nYou have no maximum hand size.\nIf unused mana would empty from your mana pool, that mana becomes colorless instead.').
card_mana_cost('kruphix, god of horizons', ['3', 'G', 'U']).
card_cmc('kruphix, god of horizons', 5).
card_layout('kruphix, god of horizons', 'normal').
card_power('kruphix, god of horizons', 4).
card_toughness('kruphix, god of horizons', 7).

% Found in: LEG
card_name('kry shield', 'Kry Shield').
card_type('kry shield', 'Artifact').
card_types('kry shield', ['Artifact']).
card_subtypes('kry shield', []).
card_colors('kry shield', []).
card_text('kry shield', '{2}, {T}: Prevent all damage that would be dealt this turn by target creature you control. That creature gets +0/+X until end of turn, where X is its converted mana cost.').
card_mana_cost('kry shield', ['2']).
card_cmc('kry shield', 2).
card_layout('kry shield', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME4
card_name('kudzu', 'Kudzu').
card_type('kudzu', 'Enchantment — Aura').
card_types('kudzu', ['Enchantment']).
card_subtypes('kudzu', ['Aura']).
card_colors('kudzu', ['G']).
card_text('kudzu', 'Enchant land\nWhen enchanted land becomes tapped, destroy it. That land\'s controller attaches Kudzu to a land of his or her choice.').
card_mana_cost('kudzu', ['1', 'G', 'G']).
card_cmc('kudzu', 3).
card_layout('kudzu', 'normal').
card_reserved('kudzu').

% Found in: MIR
card_name('kukemssa pirates', 'Kukemssa Pirates').
card_type('kukemssa pirates', 'Creature — Human Pirate').
card_types('kukemssa pirates', ['Creature']).
card_subtypes('kukemssa pirates', ['Human', 'Pirate']).
card_colors('kukemssa pirates', ['U']).
card_text('kukemssa pirates', 'Whenever Kukemssa Pirates attacks and isn\'t blocked, you may gain control of target artifact defending player controls. If you do, Kukemssa Pirates assigns no combat damage this turn.').
card_mana_cost('kukemssa pirates', ['3', 'U']).
card_cmc('kukemssa pirates', 4).
card_layout('kukemssa pirates', 'normal').
card_power('kukemssa pirates', 2).
card_toughness('kukemssa pirates', 2).
card_reserved('kukemssa pirates').

% Found in: MIR
card_name('kukemssa serpent', 'Kukemssa Serpent').
card_type('kukemssa serpent', 'Creature — Serpent').
card_types('kukemssa serpent', ['Creature']).
card_subtypes('kukemssa serpent', ['Serpent']).
card_colors('kukemssa serpent', ['U']).
card_text('kukemssa serpent', 'Kukemssa Serpent can\'t attack unless defending player controls an Island.\n{U}, Sacrifice an Island: Target land an opponent controls becomes an Island until end of turn.\nWhen you control no Islands, sacrifice Kukemssa Serpent.').
card_mana_cost('kukemssa serpent', ['3', 'U']).
card_cmc('kukemssa serpent', 4).
card_layout('kukemssa serpent', 'normal').
card_power('kukemssa serpent', 4).
card_toughness('kukemssa serpent', 3).

% Found in: MBS
card_name('kuldotha flamefiend', 'Kuldotha Flamefiend').
card_type('kuldotha flamefiend', 'Creature — Elemental').
card_types('kuldotha flamefiend', ['Creature']).
card_subtypes('kuldotha flamefiend', ['Elemental']).
card_colors('kuldotha flamefiend', ['R']).
card_text('kuldotha flamefiend', 'When Kuldotha Flamefiend enters the battlefield, you may sacrifice an artifact. If you do, Kuldotha Flamefiend deals 4 damage divided as you choose among any number of target creatures and/or players.').
card_mana_cost('kuldotha flamefiend', ['4', 'R', 'R']).
card_cmc('kuldotha flamefiend', 6).
card_layout('kuldotha flamefiend', 'normal').
card_power('kuldotha flamefiend', 4).
card_toughness('kuldotha flamefiend', 4).

% Found in: SOM
card_name('kuldotha forgemaster', 'Kuldotha Forgemaster').
card_type('kuldotha forgemaster', 'Artifact Creature — Construct').
card_types('kuldotha forgemaster', ['Artifact', 'Creature']).
card_subtypes('kuldotha forgemaster', ['Construct']).
card_colors('kuldotha forgemaster', []).
card_text('kuldotha forgemaster', '{T}, Sacrifice three artifacts: Search your library for an artifact card and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('kuldotha forgemaster', ['5']).
card_cmc('kuldotha forgemaster', 5).
card_layout('kuldotha forgemaster', 'normal').
card_power('kuldotha forgemaster', 3).
card_toughness('kuldotha forgemaster', 5).

% Found in: SOM
card_name('kuldotha phoenix', 'Kuldotha Phoenix').
card_type('kuldotha phoenix', 'Creature — Phoenix').
card_types('kuldotha phoenix', ['Creature']).
card_subtypes('kuldotha phoenix', ['Phoenix']).
card_colors('kuldotha phoenix', ['R']).
card_text('kuldotha phoenix', 'Flying, haste\nMetalcraft — {4}: Return Kuldotha Phoenix from your graveyard to the battlefield. Activate this ability only during your upkeep and only if you control three or more artifacts.').
card_mana_cost('kuldotha phoenix', ['2', 'R', 'R', 'R']).
card_cmc('kuldotha phoenix', 5).
card_layout('kuldotha phoenix', 'normal').
card_power('kuldotha phoenix', 4).
card_toughness('kuldotha phoenix', 4).

% Found in: SOM
card_name('kuldotha rebirth', 'Kuldotha Rebirth').
card_type('kuldotha rebirth', 'Sorcery').
card_types('kuldotha rebirth', ['Sorcery']).
card_subtypes('kuldotha rebirth', []).
card_colors('kuldotha rebirth', ['R']).
card_text('kuldotha rebirth', 'As an additional cost to cast Kuldotha Rebirth, sacrifice an artifact.\nPut three 1/1 red Goblin creature tokens onto the battlefield.').
card_mana_cost('kuldotha rebirth', ['R']).
card_cmc('kuldotha rebirth', 1).
card_layout('kuldotha rebirth', 'normal').

% Found in: MBS
card_name('kuldotha ringleader', 'Kuldotha Ringleader').
card_type('kuldotha ringleader', 'Creature — Giant Berserker').
card_types('kuldotha ringleader', ['Creature']).
card_subtypes('kuldotha ringleader', ['Giant', 'Berserker']).
card_colors('kuldotha ringleader', ['R']).
card_text('kuldotha ringleader', 'Battle cry (Whenever this creature attacks, each other attacking creature gets +1/+0 until end of turn.)\nKuldotha Ringleader attacks each turn if able.').
card_mana_cost('kuldotha ringleader', ['4', 'R']).
card_cmc('kuldotha ringleader', 5).
card_layout('kuldotha ringleader', 'normal').
card_power('kuldotha ringleader', 4).
card_toughness('kuldotha ringleader', 4).

% Found in: SHM
card_name('kulrath knight', 'Kulrath Knight').
card_type('kulrath knight', 'Creature — Elemental Knight').
card_types('kulrath knight', ['Creature']).
card_subtypes('kulrath knight', ['Elemental', 'Knight']).
card_colors('kulrath knight', ['B', 'R']).
card_text('kulrath knight', 'Flying\nWither (This deals damage to creatures in the form of -1/-1 counters.)\nCreatures your opponents control with counters on them can\'t attack or block.').
card_mana_cost('kulrath knight', ['3', 'B/R', 'B/R']).
card_cmc('kulrath knight', 5).
card_layout('kulrath knight', 'normal').
card_power('kulrath knight', 3).
card_toughness('kulrath knight', 3).

% Found in: BOK
card_name('kumano\'s blessing', 'Kumano\'s Blessing').
card_type('kumano\'s blessing', 'Enchantment — Aura').
card_types('kumano\'s blessing', ['Enchantment']).
card_subtypes('kumano\'s blessing', ['Aura']).
card_colors('kumano\'s blessing', ['R']).
card_text('kumano\'s blessing', 'Flash\nEnchant creature\nIf a creature dealt damage by enchanted creature this turn would die, exile it instead.').
card_mana_cost('kumano\'s blessing', ['2', 'R']).
card_cmc('kumano\'s blessing', 3).
card_layout('kumano\'s blessing', 'normal').

% Found in: CHK
card_name('kumano\'s pupils', 'Kumano\'s Pupils').
card_type('kumano\'s pupils', 'Creature — Human Shaman').
card_types('kumano\'s pupils', ['Creature']).
card_subtypes('kumano\'s pupils', ['Human', 'Shaman']).
card_colors('kumano\'s pupils', ['R']).
card_text('kumano\'s pupils', 'If a creature dealt damage by Kumano\'s Pupils this turn would die, exile it instead.').
card_mana_cost('kumano\'s pupils', ['4', 'R']).
card_cmc('kumano\'s pupils', 5).
card_layout('kumano\'s pupils', 'normal').
card_power('kumano\'s pupils', 3).
card_toughness('kumano\'s pupils', 3).

% Found in: CHK
card_name('kumano, master yamabushi', 'Kumano, Master Yamabushi').
card_type('kumano, master yamabushi', 'Legendary Creature — Human Shaman').
card_types('kumano, master yamabushi', ['Creature']).
card_subtypes('kumano, master yamabushi', ['Human', 'Shaman']).
card_supertypes('kumano, master yamabushi', ['Legendary']).
card_colors('kumano, master yamabushi', ['R']).
card_text('kumano, master yamabushi', '{1}{R}: Kumano, Master Yamabushi deals 1 damage to target creature or player.\nIf a creature dealt damage by Kumano this turn would die, exile it instead.').
card_mana_cost('kumano, master yamabushi', ['3', 'R', 'R']).
card_cmc('kumano, master yamabushi', 5).
card_layout('kumano, master yamabushi', 'normal').
card_power('kumano, master yamabushi', 4).
card_toughness('kumano, master yamabushi', 4).

% Found in: SOK
card_name('kuon\'s essence', 'Kuon\'s Essence').
card_type('kuon\'s essence', 'Legendary Enchantment').
card_types('kuon\'s essence', ['Enchantment']).
card_subtypes('kuon\'s essence', []).
card_supertypes('kuon\'s essence', ['Legendary']).
card_colors('kuon\'s essence', ['B']).
card_text('kuon\'s essence', 'At the beginning of each player\'s upkeep, that player sacrifices a creature.').
card_mana_cost('kuon\'s essence', ['B', 'B', 'B']).
card_cmc('kuon\'s essence', 3).
card_layout('kuon\'s essence', 'flip').

% Found in: SOK
card_name('kuon, ogre ascendant', 'Kuon, Ogre Ascendant').
card_type('kuon, ogre ascendant', 'Legendary Creature — Ogre Monk').
card_types('kuon, ogre ascendant', ['Creature']).
card_subtypes('kuon, ogre ascendant', ['Ogre', 'Monk']).
card_supertypes('kuon, ogre ascendant', ['Legendary']).
card_colors('kuon, ogre ascendant', ['B']).
card_text('kuon, ogre ascendant', 'At the beginning of the end step, if three or more creatures died this turn, flip Kuon, Ogre Ascendant.').
card_mana_cost('kuon, ogre ascendant', ['B', 'B', 'B']).
card_cmc('kuon, ogre ascendant', 3).
card_layout('kuon, ogre ascendant', 'flip').
card_power('kuon, ogre ascendant', 2).
card_toughness('kuon, ogre ascendant', 4).
card_sides('kuon, ogre ascendant', 'kuon\'s essence').

% Found in: SCG
card_name('kurgadon', 'Kurgadon').
card_type('kurgadon', 'Creature — Beast').
card_types('kurgadon', ['Creature']).
card_subtypes('kurgadon', ['Beast']).
card_colors('kurgadon', ['G']).
card_text('kurgadon', 'Whenever you cast a creature spell with converted mana cost 6 or greater, put three +1/+1 counters on Kurgadon.').
card_mana_cost('kurgadon', ['4', 'G']).
card_cmc('kurgadon', 5).
card_layout('kurgadon', 'normal').
card_power('kurgadon', 3).
card_toughness('kurgadon', 3).

% Found in: M15
card_name('kurkesh, onakke ancient', 'Kurkesh, Onakke Ancient').
card_type('kurkesh, onakke ancient', 'Legendary Creature — Ogre Spirit').
card_types('kurkesh, onakke ancient', ['Creature']).
card_subtypes('kurkesh, onakke ancient', ['Ogre', 'Spirit']).
card_supertypes('kurkesh, onakke ancient', ['Legendary']).
card_colors('kurkesh, onakke ancient', ['R']).
card_text('kurkesh, onakke ancient', 'Whenever you activate an ability of an artifact, if it isn\'t a mana ability, you may pay {R}. If you do, copy that ability. You may choose new targets for the copy.').
card_mana_cost('kurkesh, onakke ancient', ['2', 'R', 'R']).
card_cmc('kurkesh, onakke ancient', 4).
card_layout('kurkesh, onakke ancient', 'normal').
card_power('kurkesh, onakke ancient', 4).
card_toughness('kurkesh, onakke ancient', 3).

% Found in: SOK
card_name('kuro\'s taken', 'Kuro\'s Taken').
card_type('kuro\'s taken', 'Creature — Rat Samurai').
card_types('kuro\'s taken', ['Creature']).
card_subtypes('kuro\'s taken', ['Rat', 'Samurai']).
card_colors('kuro\'s taken', ['B']).
card_text('kuro\'s taken', 'Bushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)\n{1}{B}: Regenerate Kuro\'s Taken.').
card_mana_cost('kuro\'s taken', ['1', 'B']).
card_cmc('kuro\'s taken', 2).
card_layout('kuro\'s taken', 'normal').
card_power('kuro\'s taken', 1).
card_toughness('kuro\'s taken', 1).

% Found in: CHK, DD3_DVD, DDC
card_name('kuro, pitlord', 'Kuro, Pitlord').
card_type('kuro, pitlord', 'Legendary Creature — Demon Spirit').
card_types('kuro, pitlord', ['Creature']).
card_subtypes('kuro, pitlord', ['Demon', 'Spirit']).
card_supertypes('kuro, pitlord', ['Legendary']).
card_colors('kuro, pitlord', ['B']).
card_text('kuro, pitlord', 'At the beginning of your upkeep, sacrifice Kuro, Pitlord unless you pay {B}{B}{B}{B}.\nPay 1 life: Target creature gets -1/-1 until end of turn.').
card_mana_cost('kuro, pitlord', ['6', 'B', 'B', 'B']).
card_cmc('kuro, pitlord', 9).
card_layout('kuro, pitlord', 'normal').
card_power('kuro, pitlord', 9).
card_toughness('kuro, pitlord', 9).

% Found in: CHK
card_name('kusari-gama', 'Kusari-Gama').
card_type('kusari-gama', 'Artifact — Equipment').
card_types('kusari-gama', ['Artifact']).
card_subtypes('kusari-gama', ['Equipment']).
card_colors('kusari-gama', []).
card_text('kusari-gama', 'Equipped creature has \"{2}: This creature gets +1/+0 until end of turn.\"\nWhenever equipped creature deals damage to a blocking creature, Kusari-Gama deals that much damage to each other creature defending player controls.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('kusari-gama', ['3']).
card_cmc('kusari-gama', 3).
card_layout('kusari-gama', 'normal').

% Found in: BOK
card_name('kyoki, sanity\'s eclipse', 'Kyoki, Sanity\'s Eclipse').
card_type('kyoki, sanity\'s eclipse', 'Legendary Creature — Demon Spirit').
card_types('kyoki, sanity\'s eclipse', ['Creature']).
card_subtypes('kyoki, sanity\'s eclipse', ['Demon', 'Spirit']).
card_supertypes('kyoki, sanity\'s eclipse', ['Legendary']).
card_colors('kyoki, sanity\'s eclipse', ['B']).
card_text('kyoki, sanity\'s eclipse', 'Whenever you cast a Spirit or Arcane spell, target opponent exiles a card from his or her hand.').
card_mana_cost('kyoki, sanity\'s eclipse', ['4', 'B', 'B']).
card_cmc('kyoki, sanity\'s eclipse', 6).
card_layout('kyoki, sanity\'s eclipse', 'normal').
card_power('kyoki, sanity\'s eclipse', 6).
card_toughness('kyoki, sanity\'s eclipse', 4).

% Found in: MMQ
card_name('kyren archive', 'Kyren Archive').
card_type('kyren archive', 'Artifact').
card_types('kyren archive', ['Artifact']).
card_subtypes('kyren archive', []).
card_colors('kyren archive', []).
card_text('kyren archive', 'At the beginning of your upkeep, you may exile the top card of your library face down.\n{5}, Discard your hand, Sacrifice Kyren Archive: Put all cards exiled with Kyren Archive into their owner\'s hand.').
card_mana_cost('kyren archive', ['3']).
card_cmc('kyren archive', 3).
card_layout('kyren archive', 'normal').

% Found in: MMQ
card_name('kyren glider', 'Kyren Glider').
card_type('kyren glider', 'Creature — Goblin').
card_types('kyren glider', ['Creature']).
card_subtypes('kyren glider', ['Goblin']).
card_colors('kyren glider', ['R']).
card_text('kyren glider', 'Flying\nKyren Glider can\'t block.').
card_mana_cost('kyren glider', ['1', 'R']).
card_cmc('kyren glider', 2).
card_layout('kyren glider', 'normal').
card_power('kyren glider', 1).
card_toughness('kyren glider', 1).

% Found in: MMQ
card_name('kyren legate', 'Kyren Legate').
card_type('kyren legate', 'Creature — Goblin').
card_types('kyren legate', ['Creature']).
card_subtypes('kyren legate', ['Goblin']).
card_colors('kyren legate', ['R']).
card_text('kyren legate', 'Haste\nIf an opponent controls a Plains and you control a Mountain, you may cast Kyren Legate without paying its mana cost.').
card_mana_cost('kyren legate', ['1', 'R']).
card_cmc('kyren legate', 2).
card_layout('kyren legate', 'normal').
card_power('kyren legate', 1).
card_toughness('kyren legate', 1).

% Found in: MMQ
card_name('kyren negotiations', 'Kyren Negotiations').
card_type('kyren negotiations', 'Enchantment').
card_types('kyren negotiations', ['Enchantment']).
card_subtypes('kyren negotiations', []).
card_colors('kyren negotiations', ['R']).
card_text('kyren negotiations', 'Tap an untapped creature you control: Kyren Negotiations deals 1 damage to target player.').
card_mana_cost('kyren negotiations', ['2', 'R', 'R']).
card_cmc('kyren negotiations', 4).
card_layout('kyren negotiations', 'normal').

% Found in: MMQ
card_name('kyren sniper', 'Kyren Sniper').
card_type('kyren sniper', 'Creature — Goblin').
card_types('kyren sniper', ['Creature']).
card_subtypes('kyren sniper', ['Goblin']).
card_colors('kyren sniper', ['R']).
card_text('kyren sniper', 'At the beginning of your upkeep, you may have Kyren Sniper deal 1 damage to target player.').
card_mana_cost('kyren sniper', ['2', 'R']).
card_cmc('kyren sniper', 3).
card_layout('kyren sniper', 'normal').
card_power('kyren sniper', 1).
card_toughness('kyren sniper', 1).

% Found in: MMQ
card_name('kyren toy', 'Kyren Toy').
card_type('kyren toy', 'Artifact').
card_types('kyren toy', ['Artifact']).
card_subtypes('kyren toy', []).
card_colors('kyren toy', []).
card_text('kyren toy', '{1}, {T}: Put a charge counter on Kyren Toy.\n{T}, Remove X charge counters from Kyren Toy: Add {X}{1} to your mana pool.').
card_mana_cost('kyren toy', ['3']).
card_cmc('kyren toy', 3).
card_layout('kyren toy', 'normal').

% Found in: VIS
card_name('kyscu drake', 'Kyscu Drake').
card_type('kyscu drake', 'Creature — Drake').
card_types('kyscu drake', ['Creature']).
card_subtypes('kyscu drake', ['Drake']).
card_colors('kyscu drake', ['G']).
card_text('kyscu drake', 'Flying\n{G}: Kyscu Drake gets +0/+1 until end of turn. Activate this ability only once each turn.\nSacrifice Kyscu Drake and a creature named Spitting Drake: Search your library for a card named Viashivan Dragon and put that card onto the battlefield. Then shuffle your library.').
card_mana_cost('kyscu drake', ['3', 'G']).
card_cmc('kyscu drake', 4).
card_layout('kyscu drake', 'normal').
card_power('kyscu drake', 2).
card_toughness('kyscu drake', 2).

% Found in: ORI
card_name('kytheon\'s irregulars', 'Kytheon\'s Irregulars').
card_type('kytheon\'s irregulars', 'Creature — Human Soldier').
card_types('kytheon\'s irregulars', ['Creature']).
card_subtypes('kytheon\'s irregulars', ['Human', 'Soldier']).
card_colors('kytheon\'s irregulars', ['W']).
card_text('kytheon\'s irregulars', 'Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)\n{W}{W}: Tap target creature.').
card_mana_cost('kytheon\'s irregulars', ['2', 'W', 'W']).
card_cmc('kytheon\'s irregulars', 4).
card_layout('kytheon\'s irregulars', 'normal').
card_power('kytheon\'s irregulars', 4).
card_toughness('kytheon\'s irregulars', 3).

% Found in: ORI
card_name('kytheon\'s tactics', 'Kytheon\'s Tactics').
card_type('kytheon\'s tactics', 'Sorcery').
card_types('kytheon\'s tactics', ['Sorcery']).
card_subtypes('kytheon\'s tactics', []).
card_colors('kytheon\'s tactics', ['W']).
card_text('kytheon\'s tactics', 'Creatures you control get +2/+1 until end of turn.\nSpell mastery — If there are two or more instant and/or sorcery cards in your graveyard, those creatures also gain vigilance until end of turn. (Attacking doesn\'t cause them to tap.)').
card_mana_cost('kytheon\'s tactics', ['1', 'W', 'W']).
card_cmc('kytheon\'s tactics', 3).
card_layout('kytheon\'s tactics', 'normal').

% Found in: ORI
card_name('kytheon, hero of akros', 'Kytheon, Hero of Akros').
card_type('kytheon, hero of akros', 'Legendary Creature — Human Soldier').
card_types('kytheon, hero of akros', ['Creature']).
card_subtypes('kytheon, hero of akros', ['Human', 'Soldier']).
card_supertypes('kytheon, hero of akros', ['Legendary']).
card_colors('kytheon, hero of akros', ['W']).
card_text('kytheon, hero of akros', 'At end of combat, if Kytheon, Hero of Akros and at least two other creatures attacked this combat, exile Kytheon, then return him to the battlefield transformed under his owner\'s control.\n{2}{W}: Kytheon gains indestructible until end of turn.').
card_mana_cost('kytheon, hero of akros', ['W']).
card_cmc('kytheon, hero of akros', 1).
card_layout('kytheon, hero of akros', 'double-faced').
card_power('kytheon, hero of akros', 2).
card_toughness('kytheon, hero of akros', 1).
card_sides('kytheon, hero of akros', 'gideon, battle-forged').

