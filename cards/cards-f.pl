% Card-specific data

% Found in: PLC
card_name('fa\'adiyah seer', 'Fa\'adiyah Seer').
card_type('fa\'adiyah seer', 'Creature — Human Shaman').
card_types('fa\'adiyah seer', ['Creature']).
card_subtypes('fa\'adiyah seer', ['Human', 'Shaman']).
card_colors('fa\'adiyah seer', ['G']).
card_text('fa\'adiyah seer', '{T}: Draw a card and reveal it. If it isn\'t a land card, discard it.').
card_mana_cost('fa\'adiyah seer', ['1', 'G']).
card_cmc('fa\'adiyah seer', 2).
card_layout('fa\'adiyah seer', 'normal').
card_power('fa\'adiyah seer', 1).
card_toughness('fa\'adiyah seer', 1).

% Found in: EVE
card_name('fable of wolf and owl', 'Fable of Wolf and Owl').
card_type('fable of wolf and owl', 'Enchantment').
card_types('fable of wolf and owl', ['Enchantment']).
card_subtypes('fable of wolf and owl', []).
card_colors('fable of wolf and owl', ['U', 'G']).
card_text('fable of wolf and owl', 'Whenever you cast a green spell, you may put a 2/2 green Wolf creature token onto the battlefield.\nWhenever you cast a blue spell, you may put a 1/1 blue Bird creature token with flying onto the battlefield.').
card_mana_cost('fable of wolf and owl', ['3', 'G/U', 'G/U', 'G/U']).
card_cmc('fable of wolf and owl', 6).
card_layout('fable of wolf and owl', 'normal').

% Found in: THS
card_name('fabled hero', 'Fabled Hero').
card_type('fabled hero', 'Creature — Human Soldier').
card_types('fabled hero', ['Creature']).
card_subtypes('fabled hero', ['Human', 'Soldier']).
card_colors('fabled hero', ['W']).
card_text('fabled hero', 'Double strike\nHeroic — Whenever you cast a spell that targets Fabled Hero, put a +1/+1 counter on Fabled Hero.').
card_mana_cost('fabled hero', ['1', 'W', 'W']).
card_cmc('fabled hero', 3).
card_layout('fabled hero', 'normal').
card_power('fabled hero', 2).
card_toughness('fabled hero', 2).

% Found in: HOP, M10, MRD
card_name('fabricate', 'Fabricate').
card_type('fabricate', 'Sorcery').
card_types('fabricate', ['Sorcery']).
card_subtypes('fabricate', []).
card_colors('fabricate', ['U']).
card_text('fabricate', 'Search your library for an artifact card, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('fabricate', ['2', 'U']).
card_cmc('fabricate', 3).
card_layout('fabricate', 'normal').

% Found in: ODY
card_name('face of fear', 'Face of Fear').
card_type('face of fear', 'Creature — Horror').
card_types('face of fear', ['Creature']).
card_subtypes('face of fear', ['Horror']).
card_colors('face of fear', ['B']).
card_text('face of fear', '{2}{B}, Discard a card: Face of Fear gains fear until end of turn. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('face of fear', ['5', 'B']).
card_cmc('face of fear', 6).
card_layout('face of fear', 'normal').
card_power('face of fear', 3).
card_toughness('face of fear', 4).

% Found in: UNH
card_name('face to face', 'Face to Face').
card_type('face to face', 'Sorcery').
card_types('face to face', ['Sorcery']).
card_subtypes('face to face', []).
card_colors('face to face', ['R']).
card_text('face to face', 'You and target opponent play a best two-out-of-three Rock, Paper, Scissors match. If you win, Face to Face deals 5 damage to that opponent.').
card_mana_cost('face to face', ['1', 'R']).
card_cmc('face to face', 2).
card_layout('face to face', 'normal').

% Found in: PD3, TOR, TSB
card_name('faceless butcher', 'Faceless Butcher').
card_type('faceless butcher', 'Creature — Nightmare Horror').
card_types('faceless butcher', ['Creature']).
card_subtypes('faceless butcher', ['Nightmare', 'Horror']).
card_colors('faceless butcher', ['B']).
card_text('faceless butcher', 'When Faceless Butcher enters the battlefield, exile target creature other than Faceless Butcher.\nWhen Faceless Butcher leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_mana_cost('faceless butcher', ['2', 'B', 'B']).
card_cmc('faceless butcher', 4).
card_layout('faceless butcher', 'normal').
card_power('faceless butcher', 2).
card_toughness('faceless butcher', 3).

% Found in: TSP
card_name('faceless devourer', 'Faceless Devourer').
card_type('faceless devourer', 'Creature — Nightmare Horror').
card_types('faceless devourer', ['Creature']).
card_subtypes('faceless devourer', ['Nightmare', 'Horror']).
card_colors('faceless devourer', ['B']).
card_text('faceless devourer', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhen Faceless Devourer enters the battlefield, exile another target creature with shadow.\nWhen Faceless Devourer leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_mana_cost('faceless devourer', ['2', 'B']).
card_cmc('faceless devourer', 3).
card_layout('faceless devourer', 'normal').
card_power('faceless devourer', 2).
card_toughness('faceless devourer', 1).

% Found in: SCG
card_name('faces of the past', 'Faces of the Past').
card_type('faces of the past', 'Enchantment').
card_types('faces of the past', ['Enchantment']).
card_subtypes('faces of the past', []).
card_colors('faces of the past', ['U']).
card_text('faces of the past', 'Whenever a creature dies, tap all untapped creatures that share a creature type with it or untap all tapped creatures that share a creature type with it.').
card_mana_cost('faces of the past', ['2', 'U']).
card_cmc('faces of the past', 3).
card_layout('faces of the past', 'normal').

% Found in: LRW, MMA
card_name('facevaulter', 'Facevaulter').
card_type('facevaulter', 'Creature — Goblin Warrior').
card_types('facevaulter', ['Creature']).
card_subtypes('facevaulter', ['Goblin', 'Warrior']).
card_colors('facevaulter', ['B']).
card_text('facevaulter', '{B}, Sacrifice a Goblin: Facevaulter gets +2/+2 until end of turn.').
card_mana_cost('facevaulter', ['B']).
card_cmc('facevaulter', 1).
card_layout('facevaulter', 'normal').
card_power('facevaulter', 1).
card_toughness('facevaulter', 1).

% Found in: CMD, CNS, DD2, DD3_JVC, INV, V13, VMA, pFNM
card_name('fact or fiction', 'Fact or Fiction').
card_type('fact or fiction', 'Instant').
card_types('fact or fiction', ['Instant']).
card_subtypes('fact or fiction', []).
card_colors('fact or fiction', ['U']).
card_text('fact or fiction', 'Reveal the top five cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other into your graveyard.').
card_mana_cost('fact or fiction', ['3', 'U']).
card_cmc('fact or fiction', 4).
card_layout('fact or fiction', 'normal').

% Found in: EXO
card_name('fade away', 'Fade Away').
card_type('fade away', 'Sorcery').
card_types('fade away', ['Sorcery']).
card_subtypes('fade away', []).
card_colors('fade away', ['U']).
card_text('fade away', 'For each creature, its controller sacrifices a permanent unless he or she pays {1}.').
card_mana_cost('fade away', ['2', 'U']).
card_cmc('fade away', 3).
card_layout('fade away', 'normal').

% Found in: ONS
card_name('fade from memory', 'Fade from Memory').
card_type('fade from memory', 'Instant').
card_types('fade from memory', ['Instant']).
card_subtypes('fade from memory', []).
card_colors('fade from memory', ['B']).
card_text('fade from memory', 'Exile target card from a graveyard.\nCycling {B} ({B}, Discard this card: Draw a card.)').
card_mana_cost('fade from memory', ['B']).
card_cmc('fade from memory', 1).
card_layout('fade from memory', 'normal').

% Found in: THS
card_name('fade into antiquity', 'Fade into Antiquity').
card_type('fade into antiquity', 'Sorcery').
card_types('fade into antiquity', ['Sorcery']).
card_subtypes('fade into antiquity', []).
card_colors('fade into antiquity', ['G']).
card_text('fade into antiquity', 'Exile target artifact or enchantment.').
card_mana_cost('fade into antiquity', ['2', 'G']).
card_cmc('fade into antiquity', 3).
card_layout('fade into antiquity', 'normal').

% Found in: 10E, C13, ULG, pSUM
card_name('faerie conclave', 'Faerie Conclave').
card_type('faerie conclave', 'Land').
card_types('faerie conclave', ['Land']).
card_subtypes('faerie conclave', []).
card_colors('faerie conclave', []).
card_text('faerie conclave', 'Faerie Conclave enters the battlefield tapped.\n{T}: Add {U} to your mana pool.\n{1}{U}: Faerie Conclave becomes a 2/1 blue Faerie creature with flying until end of turn. It\'s still a land.').
card_layout('faerie conclave', 'normal').

% Found in: LRW
card_name('faerie harbinger', 'Faerie Harbinger').
card_type('faerie harbinger', 'Creature — Faerie Wizard').
card_types('faerie harbinger', ['Creature']).
card_subtypes('faerie harbinger', ['Faerie', 'Wizard']).
card_colors('faerie harbinger', ['U']).
card_text('faerie harbinger', 'Flash\nFlying\nWhen Faerie Harbinger enters the battlefield, you may search your library for a Faerie card, reveal it, then shuffle your library and put that card on top of it.').
card_mana_cost('faerie harbinger', ['3', 'U']).
card_cmc('faerie harbinger', 4).
card_layout('faerie harbinger', 'normal').
card_power('faerie harbinger', 2).
card_toughness('faerie harbinger', 2).

% Found in: DDN, RTR
card_name('faerie impostor', 'Faerie Impostor').
card_type('faerie impostor', 'Creature — Faerie Rogue').
card_types('faerie impostor', ['Creature']).
card_subtypes('faerie impostor', ['Faerie', 'Rogue']).
card_colors('faerie impostor', ['U']).
card_text('faerie impostor', 'Flying\nWhen Faerie Impostor enters the battlefield, sacrifice it unless you return another creature you control to its owner\'s hand.').
card_mana_cost('faerie impostor', ['U']).
card_cmc('faerie impostor', 1).
card_layout('faerie impostor', 'normal').
card_power('faerie impostor', 2).
card_toughness('faerie impostor', 1).

% Found in: DDN, M13
card_name('faerie invaders', 'Faerie Invaders').
card_type('faerie invaders', 'Creature — Faerie Rogue').
card_types('faerie invaders', ['Creature']).
card_subtypes('faerie invaders', ['Faerie', 'Rogue']).
card_colors('faerie invaders', ['U']).
card_text('faerie invaders', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying').
card_mana_cost('faerie invaders', ['4', 'U']).
card_cmc('faerie invaders', 5).
card_layout('faerie invaders', 'normal').
card_power('faerie invaders', 3).
card_toughness('faerie invaders', 3).

% Found in: DD3_GVL, DDD, MMA, SHM
card_name('faerie macabre', 'Faerie Macabre').
card_type('faerie macabre', 'Creature — Faerie Rogue').
card_types('faerie macabre', ['Creature']).
card_subtypes('faerie macabre', ['Faerie', 'Rogue']).
card_colors('faerie macabre', ['B']).
card_text('faerie macabre', 'Flying\nDiscard Faerie Macabre: Exile up to two target cards from graveyards.').
card_mana_cost('faerie macabre', ['1', 'B', 'B']).
card_cmc('faerie macabre', 3).
card_layout('faerie macabre', 'normal').
card_power('faerie macabre', 2).
card_toughness('faerie macabre', 2).

% Found in: CON, DDF, MM2, MMA
card_name('faerie mechanist', 'Faerie Mechanist').
card_type('faerie mechanist', 'Artifact Creature — Faerie Artificer').
card_types('faerie mechanist', ['Artifact', 'Creature']).
card_subtypes('faerie mechanist', ['Faerie', 'Artificer']).
card_colors('faerie mechanist', ['U']).
card_text('faerie mechanist', 'Flying\nWhen Faerie Mechanist enters the battlefield, look at the top three cards of your library. You may reveal an artifact card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_mana_cost('faerie mechanist', ['3', 'U']).
card_cmc('faerie mechanist', 4).
card_layout('faerie mechanist', 'normal').
card_power('faerie mechanist', 2).
card_toughness('faerie mechanist', 2).

% Found in: ORI
card_name('faerie miscreant', 'Faerie Miscreant').
card_type('faerie miscreant', 'Creature — Faerie Rogue').
card_types('faerie miscreant', ['Creature']).
card_subtypes('faerie miscreant', ['Faerie', 'Rogue']).
card_colors('faerie miscreant', ['U']).
card_text('faerie miscreant', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhen Faerie Miscreant enters the battlefield, if you control another creature named Faerie Miscreant, draw a card.').
card_mana_cost('faerie miscreant', ['U']).
card_cmc('faerie miscreant', 1).
card_layout('faerie miscreant', 'normal').
card_power('faerie miscreant', 1).
card_toughness('faerie miscreant', 1).

% Found in: HML, ME3
card_name('faerie noble', 'Faerie Noble').
card_type('faerie noble', 'Creature — Faerie').
card_types('faerie noble', ['Creature']).
card_subtypes('faerie noble', ['Faerie']).
card_colors('faerie noble', ['G']).
card_text('faerie noble', 'Flying\nOther Faerie creatures you control get +0/+1.\n{T}: Other Faerie creatures you control get +1/+0 until end of turn.').
card_mana_cost('faerie noble', ['2', 'G']).
card_cmc('faerie noble', 3).
card_layout('faerie noble', 'normal').
card_power('faerie noble', 1).
card_toughness('faerie noble', 2).
card_reserved('faerie noble').

% Found in: INV
card_name('faerie squadron', 'Faerie Squadron').
card_type('faerie squadron', 'Creature — Faerie').
card_types('faerie squadron', ['Creature']).
card_subtypes('faerie squadron', ['Faerie']).
card_colors('faerie squadron', ['U']).
card_text('faerie squadron', 'Kicker {3}{U} (You may pay an additional {3}{U} as you cast this spell.)\nIf Faerie Squadron was kicked, it enters the battlefield with two +1/+1 counters on it and with flying.').
card_mana_cost('faerie squadron', ['U']).
card_cmc('faerie squadron', 1).
card_layout('faerie squadron', 'normal').
card_power('faerie squadron', 1).
card_toughness('faerie squadron', 1).

% Found in: SHM
card_name('faerie swarm', 'Faerie Swarm').
card_type('faerie swarm', 'Creature — Faerie').
card_types('faerie swarm', ['Creature']).
card_subtypes('faerie swarm', ['Faerie']).
card_colors('faerie swarm', ['U']).
card_text('faerie swarm', 'Flying\nFaerie Swarm\'s power and toughness are each equal to the number of blue permanents you control.').
card_mana_cost('faerie swarm', ['3', 'U']).
card_cmc('faerie swarm', 4).
card_layout('faerie swarm', 'normal').
card_power('faerie swarm', '*').
card_toughness('faerie swarm', '*').

% Found in: LRW
card_name('faerie tauntings', 'Faerie Tauntings').
card_type('faerie tauntings', 'Tribal Enchantment — Faerie').
card_types('faerie tauntings', ['Tribal', 'Enchantment']).
card_subtypes('faerie tauntings', ['Faerie']).
card_colors('faerie tauntings', ['B']).
card_text('faerie tauntings', 'Whenever you cast a spell during an opponent\'s turn, you may have each opponent lose 1 life.').
card_mana_cost('faerie tauntings', ['2', 'B']).
card_cmc('faerie tauntings', 3).
card_layout('faerie tauntings', 'normal').

% Found in: LRW
card_name('faerie trickery', 'Faerie Trickery').
card_type('faerie trickery', 'Tribal Instant — Faerie').
card_types('faerie trickery', ['Tribal', 'Instant']).
card_subtypes('faerie trickery', ['Faerie']).
card_colors('faerie trickery', ['U']).
card_text('faerie trickery', 'Counter target non-Faerie spell. If that spell is countered this way, exile it instead of putting it into its owner\'s graveyard.').
card_mana_cost('faerie trickery', ['1', 'U', 'U']).
card_cmc('faerie trickery', 3).
card_layout('faerie trickery', 'normal').

% Found in: USG
card_name('faith healer', 'Faith Healer').
card_type('faith healer', 'Creature — Human Cleric').
card_types('faith healer', ['Creature']).
card_subtypes('faith healer', ['Human', 'Cleric']).
card_colors('faith healer', ['W']).
card_text('faith healer', 'Sacrifice an enchantment: You gain life equal to the sacrificed enchantment\'s converted mana cost.').
card_mana_cost('faith healer', ['1', 'W']).
card_cmc('faith healer', 2).
card_layout('faith healer', 'normal').
card_power('faith healer', 1).
card_toughness('faith healer', 1).

% Found in: DD3_DVD, DDC, RAV
card_name('faith\'s fetters', 'Faith\'s Fetters').
card_type('faith\'s fetters', 'Enchantment — Aura').
card_types('faith\'s fetters', ['Enchantment']).
card_subtypes('faith\'s fetters', ['Aura']).
card_colors('faith\'s fetters', ['W']).
card_text('faith\'s fetters', 'Enchant permanent\nWhen Faith\'s Fetters enters the battlefield, you gain 4 life.\nEnchanted permanent can\'t attack or block, and its activated abilities can\'t be activated unless they\'re mana abilities.').
card_mana_cost('faith\'s fetters', ['3', 'W']).
card_cmc('faith\'s fetters', 4).
card_layout('faith\'s fetters', 'normal').

% Found in: M13
card_name('faith\'s reward', 'Faith\'s Reward').
card_type('faith\'s reward', 'Instant').
card_types('faith\'s reward', ['Instant']).
card_subtypes('faith\'s reward', []).
card_colors('faith\'s reward', ['W']).
card_text('faith\'s reward', 'Return to the battlefield all permanent cards in your graveyard that were put there from the battlefield this turn.').
card_mana_cost('faith\'s reward', ['3', 'W']).
card_cmc('faith\'s reward', 4).
card_layout('faith\'s reward', 'normal').

% Found in: DKA
card_name('faith\'s shield', 'Faith\'s Shield').
card_type('faith\'s shield', 'Instant').
card_types('faith\'s shield', ['Instant']).
card_subtypes('faith\'s shield', []).
card_colors('faith\'s shield', ['W']).
card_text('faith\'s shield', 'Target permanent you control gains protection from the color of your choice until end of turn.\nFateful hour — If you have 5 or less life, instead you and each permanent you control gain protection from the color of your choice until end of turn.').
card_mana_cost('faith\'s shield', ['W']).
card_cmc('faith\'s shield', 1).
card_layout('faith\'s shield', 'normal').

% Found in: BOK
card_name('faithful squire', 'Faithful Squire').
card_type('faithful squire', 'Creature — Human Soldier').
card_types('faithful squire', ['Creature']).
card_subtypes('faithful squire', ['Human', 'Soldier']).
card_colors('faithful squire', ['W']).
card_text('faithful squire', 'Whenever you cast a Spirit or Arcane spell, you may put a ki counter on Faithful Squire.\nAt the beginning of the end step, if there are two or more ki counters on Faithful Squire, you may flip it.').
card_mana_cost('faithful squire', ['1', 'W', 'W']).
card_cmc('faithful squire', 3).
card_layout('faithful squire', 'flip').
card_power('faithful squire', 2).
card_toughness('faithful squire', 2).
card_sides('faithful squire', 'kaiso, memory of loyalty').

% Found in: C14, DDK, DKA, pMEI
card_name('faithless looting', 'Faithless Looting').
card_type('faithless looting', 'Sorcery').
card_types('faithless looting', ['Sorcery']).
card_subtypes('faithless looting', []).
card_colors('faithless looting', ['R']).
card_text('faithless looting', 'Draw two cards, then discard two cards.\nFlashback {2}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('faithless looting', ['R']).
card_cmc('faithless looting', 1).
card_layout('faithless looting', 'normal').

% Found in: DKA
card_name('falkenrath aristocrat', 'Falkenrath Aristocrat').
card_type('falkenrath aristocrat', 'Creature — Vampire').
card_types('falkenrath aristocrat', ['Creature']).
card_subtypes('falkenrath aristocrat', ['Vampire']).
card_colors('falkenrath aristocrat', ['B', 'R']).
card_text('falkenrath aristocrat', 'Flying, haste\nSacrifice a creature: Falkenrath Aristocrat gains indestructible until end of turn. If the sacrificed creature was a Human, put a +1/+1 counter on Falkenrath Aristocrat.').
card_mana_cost('falkenrath aristocrat', ['2', 'B', 'R']).
card_cmc('falkenrath aristocrat', 4).
card_layout('falkenrath aristocrat', 'normal').
card_power('falkenrath aristocrat', 4).
card_toughness('falkenrath aristocrat', 1).

% Found in: AVR
card_name('falkenrath exterminator', 'Falkenrath Exterminator').
card_type('falkenrath exterminator', 'Creature — Vampire Archer').
card_types('falkenrath exterminator', ['Creature']).
card_subtypes('falkenrath exterminator', ['Vampire', 'Archer']).
card_colors('falkenrath exterminator', ['R']).
card_text('falkenrath exterminator', 'Whenever Falkenrath Exterminator deals combat damage to a player, put a +1/+1 counter on it.\n{2}{R}: Falkenrath Exterminator deals damage to target creature equal to the number of +1/+1 counters on Falkenrath Exterminator.').
card_mana_cost('falkenrath exterminator', ['1', 'R']).
card_cmc('falkenrath exterminator', 2).
card_layout('falkenrath exterminator', 'normal').
card_power('falkenrath exterminator', 1).
card_toughness('falkenrath exterminator', 1).

% Found in: ISD
card_name('falkenrath marauders', 'Falkenrath Marauders').
card_type('falkenrath marauders', 'Creature — Vampire Warrior').
card_types('falkenrath marauders', ['Creature']).
card_subtypes('falkenrath marauders', ['Vampire', 'Warrior']).
card_colors('falkenrath marauders', ['R']).
card_text('falkenrath marauders', 'Flying, haste\nWhenever Falkenrath Marauders deals combat damage to a player, put two +1/+1 counters on it.').
card_mana_cost('falkenrath marauders', ['3', 'R', 'R']).
card_cmc('falkenrath marauders', 5).
card_layout('falkenrath marauders', 'normal').
card_power('falkenrath marauders', 2).
card_toughness('falkenrath marauders', 2).

% Found in: ISD
card_name('falkenrath noble', 'Falkenrath Noble').
card_type('falkenrath noble', 'Creature — Vampire').
card_types('falkenrath noble', ['Creature']).
card_subtypes('falkenrath noble', ['Vampire']).
card_colors('falkenrath noble', ['B']).
card_text('falkenrath noble', 'Flying\nWhenever Falkenrath Noble or another creature dies, target player loses 1 life and you gain 1 life.').
card_mana_cost('falkenrath noble', ['3', 'B']).
card_cmc('falkenrath noble', 4).
card_layout('falkenrath noble', 'normal').
card_power('falkenrath noble', 2).
card_toughness('falkenrath noble', 2).

% Found in: DKA
card_name('falkenrath torturer', 'Falkenrath Torturer').
card_type('falkenrath torturer', 'Creature — Vampire').
card_types('falkenrath torturer', ['Creature']).
card_subtypes('falkenrath torturer', ['Vampire']).
card_colors('falkenrath torturer', ['B']).
card_text('falkenrath torturer', 'Sacrifice a creature: Falkenrath Torturer gains flying until end of turn. If the sacrificed creature was a Human, put a +1/+1 counter on Falkenrath Torturer.').
card_mana_cost('falkenrath torturer', ['2', 'B']).
card_cmc('falkenrath torturer', 3).
card_layout('falkenrath torturer', 'normal').
card_power('falkenrath torturer', 2).
card_toughness('falkenrath torturer', 1).

% Found in: DDH, DIS
card_name('fall', 'Fall').
card_type('fall', 'Sorcery').
card_types('fall', ['Sorcery']).
card_subtypes('fall', []).
card_colors('fall', ['B', 'R']).
card_text('fall', 'Target player reveals two cards at random from his or her hand, then discards each nonland card revealed this way.').
card_mana_cost('fall', ['B', 'R']).
card_cmc('fall', 2).
card_layout('fall', 'split').

% Found in: RTR
card_name('fall of the gavel', 'Fall of the Gavel').
card_type('fall of the gavel', 'Instant').
card_types('fall of the gavel', ['Instant']).
card_subtypes('fall of the gavel', []).
card_colors('fall of the gavel', ['W', 'U']).
card_text('fall of the gavel', 'Counter target spell. You gain 5 life.').
card_mana_cost('fall of the gavel', ['3', 'W', 'U']).
card_cmc('fall of the gavel', 5).
card_layout('fall of the gavel', 'normal').

% Found in: BNG
card_name('fall of the hammer', 'Fall of the Hammer').
card_type('fall of the hammer', 'Instant').
card_types('fall of the hammer', ['Instant']).
card_subtypes('fall of the hammer', []).
card_colors('fall of the hammer', ['R']).
card_text('fall of the hammer', 'Target creature you control deals damage equal to its power to another target creature.').
card_mana_cost('fall of the hammer', ['1', 'R']).
card_cmc('fall of the hammer', 2).
card_layout('fall of the hammer', 'normal').

% Found in: 5ED, 6ED, 7ED, 8ED, BTD, CHR, CMD, DD3_DVD, DDC, LEG
card_name('fallen angel', 'Fallen Angel').
card_type('fallen angel', 'Creature — Angel').
card_types('fallen angel', ['Creature']).
card_subtypes('fallen angel', ['Angel']).
card_colors('fallen angel', ['B']).
card_text('fallen angel', 'Flying\nSacrifice a creature: Fallen Angel gets +2/+1 until end of turn.').
card_mana_cost('fallen angel', ['3', 'B', 'B']).
card_cmc('fallen angel', 5).
card_layout('fallen angel', 'normal').
card_power('fallen angel', 3).
card_toughness('fallen angel', 3).

% Found in: VAN
card_name('fallen angel avatar', 'Fallen Angel Avatar').
card_type('fallen angel avatar', 'Vanguard').
card_types('fallen angel avatar', ['Vanguard']).
card_subtypes('fallen angel avatar', []).
card_colors('fallen angel avatar', []).
card_text('fallen angel avatar', 'Whenever a creature you control dies, target opponent loses 1 life and you gain 1 life.').
card_layout('fallen angel avatar', 'vanguard').

% Found in: VIS, VMA
card_name('fallen askari', 'Fallen Askari').
card_type('fallen askari', 'Creature — Human Knight').
card_types('fallen askari', ['Creature']).
card_subtypes('fallen askari', ['Human', 'Knight']).
card_colors('fallen askari', ['B']).
card_text('fallen askari', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nFallen Askari can\'t block.').
card_mana_cost('fallen askari', ['1', 'B']).
card_cmc('fallen askari', 2).
card_layout('fallen askari', 'normal').
card_power('fallen askari', 2).
card_toughness('fallen askari', 2).

% Found in: ONS
card_name('fallen cleric', 'Fallen Cleric').
card_type('fallen cleric', 'Creature — Zombie Cleric').
card_types('fallen cleric', ['Creature']).
card_subtypes('fallen cleric', ['Zombie', 'Cleric']).
card_colors('fallen cleric', ['B']).
card_text('fallen cleric', 'Protection from Clerics\nMorph {4}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('fallen cleric', ['4', 'B']).
card_cmc('fallen cleric', 5).
card_layout('fallen cleric', 'normal').
card_power('fallen cleric', 4).
card_toughness('fallen cleric', 2).

% Found in: NPH
card_name('fallen ferromancer', 'Fallen Ferromancer').
card_type('fallen ferromancer', 'Creature — Human Shaman').
card_types('fallen ferromancer', ['Creature']).
card_subtypes('fallen ferromancer', ['Human', 'Shaman']).
card_colors('fallen ferromancer', ['R']).
card_text('fallen ferromancer', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\n{1}{R}, {T}: Fallen Ferromancer deals 1 damage to target creature or player.').
card_mana_cost('fallen ferromancer', ['3', 'R']).
card_cmc('fallen ferromancer', 4).
card_layout('fallen ferromancer', 'normal').
card_power('fallen ferromancer', 1).
card_toughness('fallen ferromancer', 1).

% Found in: TSP
card_name('fallen ideal', 'Fallen Ideal').
card_type('fallen ideal', 'Enchantment — Aura').
card_types('fallen ideal', ['Enchantment']).
card_subtypes('fallen ideal', ['Aura']).
card_colors('fallen ideal', ['B']).
card_text('fallen ideal', 'Enchant creature\nEnchanted creature has flying and \"Sacrifice a creature: This creature gets +2/+1 until end of turn.\"\nWhen Fallen Ideal is put into a graveyard from the battlefield, return Fallen Ideal to its owner\'s hand.').
card_mana_cost('fallen ideal', ['2', 'B']).
card_cmc('fallen ideal', 3).
card_layout('fallen ideal', 'normal').

% Found in: LEG
card_name('falling star', 'Falling Star').
card_type('falling star', 'Sorcery').
card_types('falling star', ['Sorcery']).
card_subtypes('falling star', []).
card_colors('falling star', ['R']).
card_text('falling star', 'Flip Falling Star onto the playing area from a height of at least one foot. Falling Star deals 3 damage to each creature it lands on. Tap all creatures dealt damage by Falling Star. If Falling Star doesn\'t turn completely over at least once during the flip, it has no effect.').
card_mana_cost('falling star', ['2', 'R']).
card_cmc('falling star', 3).
card_layout('falling star', 'normal').
card_reserved('falling star').

% Found in: PLS
card_name('falling timber', 'Falling Timber').
card_type('falling timber', 'Instant').
card_types('falling timber', ['Instant']).
card_subtypes('falling timber', []).
card_colors('falling timber', ['G']).
card_text('falling timber', 'Kicker—Sacrifice a land. (You may sacrifice a land in addition to any other costs as you cast this spell.)\nPrevent all combat damage target creature would deal this turn. If Falling Timber was kicked, prevent all combat damage another target creature would deal this turn.').
card_mana_cost('falling timber', ['2', 'G']).
card_cmc('falling timber', 3).
card_layout('falling timber', 'normal').

% Found in: 6ED, MIR
card_name('fallow earth', 'Fallow Earth').
card_type('fallow earth', 'Sorcery').
card_types('fallow earth', ['Sorcery']).
card_subtypes('fallow earth', []).
card_colors('fallow earth', ['G']).
card_text('fallow earth', 'Put target land on top of its owner\'s library.').
card_mana_cost('fallow earth', ['2', 'G']).
card_cmc('fallow earth', 3).
card_layout('fallow earth', 'normal').

% Found in: WTH
card_name('fallow wurm', 'Fallow Wurm').
card_type('fallow wurm', 'Creature — Wurm').
card_types('fallow wurm', ['Creature']).
card_subtypes('fallow wurm', ['Wurm']).
card_colors('fallow wurm', ['G']).
card_text('fallow wurm', 'When Fallow Wurm enters the battlefield, sacrifice it unless you discard a land card.').
card_mana_cost('fallow wurm', ['2', 'G']).
card_cmc('fallow wurm', 3).
card_layout('fallow wurm', 'normal').
card_power('fallow wurm', 4).
card_toughness('fallow wurm', 4).

% Found in: LRW
card_name('fallowsage', 'Fallowsage').
card_type('fallowsage', 'Creature — Merfolk Wizard').
card_types('fallowsage', ['Creature']).
card_subtypes('fallowsage', ['Merfolk', 'Wizard']).
card_colors('fallowsage', ['U']).
card_text('fallowsage', 'Whenever Fallowsage becomes tapped, you may draw a card.').
card_mana_cost('fallowsage', ['3', 'U']).
card_cmc('fallowsage', 4).
card_layout('fallowsage', 'normal').
card_power('fallowsage', 2).
card_toughness('fallowsage', 2).

% Found in: ONS
card_name('false cure', 'False Cure').
card_type('false cure', 'Instant').
card_types('false cure', ['Instant']).
card_subtypes('false cure', []).
card_colors('false cure', ['B']).
card_text('false cure', 'Until end of turn, whenever a player gains life, that player loses 2 life for each 1 life he or she gained.').
card_mana_cost('false cure', ['B', 'B']).
card_cmc('false cure', 2).
card_layout('false cure', 'normal').

% Found in: APC
card_name('false dawn', 'False Dawn').
card_type('false dawn', 'Sorcery').
card_types('false dawn', ['Sorcery']).
card_subtypes('false dawn', []).
card_colors('false dawn', ['W']).
card_text('false dawn', 'Until end of turn, spells and abilities you control that would add colored mana to your mana pool add that much white mana instead. Until end of turn, you may spend white mana as though it were mana of any color.\nDraw a card.').
card_mana_cost('false dawn', ['1', 'W']).
card_cmc('false dawn', 2).
card_layout('false dawn', 'normal').

% Found in: ME3, PTK
card_name('false defeat', 'False Defeat').
card_type('false defeat', 'Sorcery').
card_types('false defeat', ['Sorcery']).
card_subtypes('false defeat', []).
card_colors('false defeat', ['W']).
card_text('false defeat', 'Return target creature card from your graveyard to the battlefield.').
card_mana_cost('false defeat', ['3', 'W']).
card_cmc('false defeat', 4).
card_layout('false defeat', 'normal').

% Found in: ALL, MMQ
card_name('false demise', 'False Demise').
card_type('false demise', 'Enchantment — Aura').
card_types('false demise', ['Enchantment']).
card_subtypes('false demise', ['Aura']).
card_colors('false demise', ['U']).
card_text('false demise', 'Enchant creature\nWhen enchanted creature dies, return that card to the battlefield under your control.').
card_mana_cost('false demise', ['2', 'U']).
card_cmc('false demise', 3).
card_layout('false demise', 'normal').

% Found in: TOR
card_name('false memories', 'False Memories').
card_type('false memories', 'Instant').
card_types('false memories', ['Instant']).
card_subtypes('false memories', []).
card_colors('false memories', ['U']).
card_text('false memories', 'Put the top seven cards of your library into your graveyard. At the beginning of the next end step, exile seven cards from your graveyard.').
card_mana_cost('false memories', ['1', 'U']).
card_cmc('false memories', 2).
card_layout('false memories', 'normal').

% Found in: PTK
card_name('false mourning', 'False Mourning').
card_type('false mourning', 'Sorcery').
card_types('false mourning', ['Sorcery']).
card_subtypes('false mourning', []).
card_colors('false mourning', ['G']).
card_text('false mourning', 'Put target card from your graveyard on top of your library.').
card_mana_cost('false mourning', ['G']).
card_cmc('false mourning', 1).
card_layout('false mourning', 'normal').

% Found in: 2ED, CED, CEI, LEA, LEB
card_name('false orders', 'False Orders').
card_type('false orders', 'Instant').
card_types('false orders', ['Instant']).
card_subtypes('false orders', []).
card_colors('false orders', ['R']).
card_text('false orders', 'Cast False Orders only during the declare blockers step.\nRemove target creature defending player controls from combat. Creatures it was blocking that had become blocked by only that creature this combat become unblocked. You may have it block an attacking creature of your choice.').
card_mana_cost('false orders', ['R']).
card_cmc('false orders', 1).
card_layout('false orders', 'normal').

% Found in: POR, S99
card_name('false peace', 'False Peace').
card_type('false peace', 'Sorcery').
card_types('false peace', ['Sorcery']).
card_subtypes('false peace', []).
card_colors('false peace', ['W']).
card_text('false peace', 'Target player skips all combat phases of his or her next turn.').
card_mana_cost('false peace', ['W']).
card_cmc('false peace', 1).
card_layout('false peace', 'normal').

% Found in: CMD, UDS, pPRE
card_name('false prophet', 'False Prophet').
card_type('false prophet', 'Creature — Human Cleric').
card_types('false prophet', ['Creature']).
card_subtypes('false prophet', ['Human', 'Cleric']).
card_colors('false prophet', ['W']).
card_text('false prophet', 'When False Prophet dies, exile all creatures.').
card_mana_cost('false prophet', ['2', 'W', 'W']).
card_cmc('false prophet', 4).
card_layout('false prophet', 'normal').
card_power('false prophet', 2).
card_toughness('false prophet', 2).

% Found in: ME4, PO2
card_name('false summoning', 'False Summoning').
card_type('false summoning', 'Instant').
card_types('false summoning', ['Instant']).
card_subtypes('false summoning', []).
card_colors('false summoning', ['U']).
card_text('false summoning', 'Counter target creature spell.').
card_mana_cost('false summoning', ['1', 'U']).
card_cmc('false summoning', 2).
card_layout('false summoning', 'normal').

% Found in: USG, VMA
card_name('falter', 'Falter').
card_type('falter', 'Instant').
card_types('falter', ['Instant']).
card_subtypes('falter', []).
card_colors('falter', ['R']).
card_text('falter', 'Creatures without flying can\'t block this turn.').
card_mana_cost('falter', ['1', 'R']).
card_cmc('falter', 2).
card_layout('falter', 'normal').

% Found in: 6ED, 7ED, WTH
card_name('familiar ground', 'Familiar Ground').
card_type('familiar ground', 'Enchantment').
card_types('familiar ground', ['Enchantment']).
card_subtypes('familiar ground', []).
card_colors('familiar ground', ['G']).
card_text('familiar ground', 'Each creature you control can\'t be blocked by more than one creature.').
card_mana_cost('familiar ground', ['2', 'G']).
card_cmc('familiar ground', 3).
card_layout('familiar ground', 'normal').

% Found in: LRW
card_name('familiar\'s ruse', 'Familiar\'s Ruse').
card_type('familiar\'s ruse', 'Instant').
card_types('familiar\'s ruse', ['Instant']).
card_subtypes('familiar\'s ruse', []).
card_colors('familiar\'s ruse', ['U']).
card_text('familiar\'s ruse', 'As an additional cost to cast Familiar\'s Ruse, return a creature you control to its owner\'s hand.\nCounter target spell.').
card_mana_cost('familiar\'s ruse', ['U', 'U']).
card_cmc('familiar\'s ruse', 2).
card_layout('familiar\'s ruse', 'normal').

% Found in: C13, ME3, PTK, VMA
card_name('famine', 'Famine').
card_type('famine', 'Sorcery').
card_types('famine', ['Sorcery']).
card_subtypes('famine', []).
card_colors('famine', ['B']).
card_text('famine', 'Famine deals 3 damage to each creature and each player.').
card_mana_cost('famine', ['3', 'B', 'B']).
card_cmc('famine', 5).
card_layout('famine', 'normal').

% Found in: ODY
card_name('famished ghoul', 'Famished Ghoul').
card_type('famished ghoul', 'Creature — Zombie').
card_types('famished ghoul', ['Creature']).
card_subtypes('famished ghoul', ['Zombie']).
card_colors('famished ghoul', ['B']).
card_text('famished ghoul', '{1}{B}, Sacrifice Famished Ghoul: Exile up to two target cards from a single graveyard.').
card_mana_cost('famished ghoul', ['3', 'B']).
card_cmc('famished ghoul', 4).
card_layout('famished ghoul', 'normal').
card_power('famished ghoul', 3).
card_toughness('famished ghoul', 2).

% Found in: THS
card_name('fanatic of mogis', 'Fanatic of Mogis').
card_type('fanatic of mogis', 'Creature — Minotaur Shaman').
card_types('fanatic of mogis', ['Creature']).
card_subtypes('fanatic of mogis', ['Minotaur', 'Shaman']).
card_colors('fanatic of mogis', ['R']).
card_text('fanatic of mogis', 'When Fanatic of Mogis enters the battlefield, it deals damage to each opponent equal to your devotion to red. (Each {R} in the mana costs of permanents you control counts toward your devotion to red.)').
card_mana_cost('fanatic of mogis', ['3', 'R']).
card_cmc('fanatic of mogis', 4).
card_layout('fanatic of mogis', 'normal').
card_power('fanatic of mogis', 4).
card_toughness('fanatic of mogis', 2).

% Found in: BNG, pFNM
card_name('fanatic of xenagos', 'Fanatic of Xenagos').
card_type('fanatic of xenagos', 'Creature — Centaur Warrior').
card_types('fanatic of xenagos', ['Creature']).
card_subtypes('fanatic of xenagos', ['Centaur', 'Warrior']).
card_colors('fanatic of xenagos', ['R', 'G']).
card_text('fanatic of xenagos', 'Trample\nTribute 1 (As this creature enters the battlefield, an opponent of your choice may place a +1/+1 counter on it.)\nWhen Fanatic of Xenagos enters the battlefield, if tribute wasn\'t paid, it gets +1/+1 and gains haste until end of turn.').
card_mana_cost('fanatic of xenagos', ['1', 'R', 'G']).
card_cmc('fanatic of xenagos', 3).
card_layout('fanatic of xenagos', 'normal').
card_power('fanatic of xenagos', 3).
card_toughness('fanatic of xenagos', 3).

% Found in: NMS
card_name('fanatical devotion', 'Fanatical Devotion').
card_type('fanatical devotion', 'Enchantment').
card_types('fanatical devotion', ['Enchantment']).
card_subtypes('fanatical devotion', []).
card_colors('fanatical devotion', ['W']).
card_text('fanatical devotion', 'Sacrifice a creature: Regenerate target creature.').
card_mana_cost('fanatical devotion', ['2', 'W']).
card_cmc('fanatical devotion', 3).
card_layout('fanatical devotion', 'normal').

% Found in: ICE
card_name('fanatical fever', 'Fanatical Fever').
card_type('fanatical fever', 'Instant').
card_types('fanatical fever', ['Instant']).
card_subtypes('fanatical fever', []).
card_colors('fanatical fever', ['G']).
card_text('fanatical fever', 'Target creature gets +3/+0 and gains trample until end of turn.').
card_mana_cost('fanatical fever', ['2', 'G', 'G']).
card_cmc('fanatical fever', 4).
card_layout('fanatical fever', 'normal').

% Found in: EVE
card_name('fang skulkin', 'Fang Skulkin').
card_type('fang skulkin', 'Artifact Creature — Scarecrow').
card_types('fang skulkin', ['Artifact', 'Creature']).
card_subtypes('fang skulkin', ['Scarecrow']).
card_colors('fang skulkin', []).
card_text('fang skulkin', '{2}: Target black creature gains wither until end of turn. (It deals damage to creatures in the form of -1/-1 counters.)').
card_mana_cost('fang skulkin', ['2']).
card_cmc('fang skulkin', 2).
card_layout('fang skulkin', 'normal').
card_power('fang skulkin', 2).
card_toughness('fang skulkin', 1).

% Found in: DST
card_name('fangren firstborn', 'Fangren Firstborn').
card_type('fangren firstborn', 'Creature — Beast').
card_types('fangren firstborn', ['Creature']).
card_subtypes('fangren firstborn', ['Beast']).
card_colors('fangren firstborn', ['G']).
card_text('fangren firstborn', 'Whenever Fangren Firstborn attacks, put a +1/+1 counter on each attacking creature.').
card_mana_cost('fangren firstborn', ['1', 'G', 'G', 'G']).
card_cmc('fangren firstborn', 4).
card_layout('fangren firstborn', 'normal').
card_power('fangren firstborn', 4).
card_toughness('fangren firstborn', 2).

% Found in: MRD
card_name('fangren hunter', 'Fangren Hunter').
card_type('fangren hunter', 'Creature — Beast').
card_types('fangren hunter', ['Creature']).
card_subtypes('fangren hunter', ['Beast']).
card_colors('fangren hunter', ['G']).
card_text('fangren hunter', 'Trample').
card_mana_cost('fangren hunter', ['3', 'G', 'G']).
card_cmc('fangren hunter', 5).
card_layout('fangren hunter', 'normal').
card_power('fangren hunter', 4).
card_toughness('fangren hunter', 4).

% Found in: MBS
card_name('fangren marauder', 'Fangren Marauder').
card_type('fangren marauder', 'Creature — Beast').
card_types('fangren marauder', ['Creature']).
card_subtypes('fangren marauder', ['Beast']).
card_colors('fangren marauder', ['G']).
card_text('fangren marauder', 'Whenever an artifact is put into a graveyard from the battlefield, you may gain 5 life.').
card_mana_cost('fangren marauder', ['5', 'G']).
card_cmc('fangren marauder', 6).
card_layout('fangren marauder', 'normal').
card_power('fangren marauder', 5).
card_toughness('fangren marauder', 5).

% Found in: 5DN
card_name('fangren pathcutter', 'Fangren Pathcutter').
card_type('fangren pathcutter', 'Creature — Beast').
card_types('fangren pathcutter', ['Creature']).
card_subtypes('fangren pathcutter', ['Beast']).
card_colors('fangren pathcutter', ['G']).
card_text('fangren pathcutter', 'Whenever Fangren Pathcutter attacks, attacking creatures gain trample until end of turn.').
card_mana_cost('fangren pathcutter', ['4', 'G', 'G']).
card_cmc('fangren pathcutter', 6).
card_layout('fangren pathcutter', 'normal').
card_power('fangren pathcutter', 4).
card_toughness('fangren pathcutter', 6).

% Found in: STH, TPR
card_name('fanning the flames', 'Fanning the Flames').
card_type('fanning the flames', 'Sorcery').
card_types('fanning the flames', ['Sorcery']).
card_subtypes('fanning the flames', []).
card_colors('fanning the flames', ['R']).
card_text('fanning the flames', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nFanning the Flames deals X damage to target creature or player.').
card_mana_cost('fanning the flames', ['X', 'R', 'R']).
card_cmc('fanning the flames', 2).
card_layout('fanning the flames', 'normal').

% Found in: DGM
card_name('far', 'Far').
card_type('far', 'Instant').
card_types('far', ['Instant']).
card_subtypes('far', []).
card_colors('far', ['U']).
card_text('far', 'Return target creature to its owner\'s hand.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('far', ['1', 'U']).
card_cmc('far', 2).
card_layout('far', 'split').
card_sides('far', 'away').

% Found in: TOR
card_name('far wanderings', 'Far Wanderings').
card_type('far wanderings', 'Sorcery').
card_types('far wanderings', ['Sorcery']).
card_subtypes('far wanderings', []).
card_colors('far wanderings', ['G']).
card_text('far wanderings', 'Search your library for a basic land card, put that card onto the battlefield tapped, then shuffle your library.\nThreshold — If seven or more cards are in your graveyard, instead search your library for up to three basic land cards, put them onto the battlefield tapped, then shuffle your library.').
card_mana_cost('far wanderings', ['2', 'G']).
card_cmc('far wanderings', 3).
card_layout('far wanderings', 'normal').

% Found in: DKA
card_name('farbog boneflinger', 'Farbog Boneflinger').
card_type('farbog boneflinger', 'Creature — Zombie').
card_types('farbog boneflinger', ['Creature']).
card_subtypes('farbog boneflinger', ['Zombie']).
card_colors('farbog boneflinger', ['B']).
card_text('farbog boneflinger', 'When Farbog Boneflinger enters the battlefield, target creature gets -2/-2 until end of turn.').
card_mana_cost('farbog boneflinger', ['4', 'B']).
card_cmc('farbog boneflinger', 5).
card_layout('farbog boneflinger', 'normal').
card_power('farbog boneflinger', 2).
card_toughness('farbog boneflinger', 2).

% Found in: AVR
card_name('farbog explorer', 'Farbog Explorer').
card_type('farbog explorer', 'Creature — Human Scout').
card_types('farbog explorer', ['Creature']).
card_subtypes('farbog explorer', ['Human', 'Scout']).
card_colors('farbog explorer', ['W']).
card_text('farbog explorer', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('farbog explorer', ['2', 'W']).
card_cmc('farbog explorer', 3).
card_layout('farbog explorer', 'normal').
card_power('farbog explorer', 2).
card_toughness('farbog explorer', 3).

% Found in: UNH
card_name('farewell to arms', 'Farewell to Arms').
card_type('farewell to arms', 'Enchantment').
card_types('farewell to arms', ['Enchantment']).
card_subtypes('farewell to arms', []).
card_colors('farewell to arms', ['B']).
card_text('farewell to arms', 'As Farewell to Arms comes into play, choose a hand attached to an opponent\'s arm.\nWhen the chosen hand isn\'t behind its owner\'s back, sacrifice Farewell to Arms. If you do, that player discards his or her hand . . . of cards. (The lawyers wouldn\'t let us do it the other way.)').
card_mana_cost('farewell to arms', ['1', 'B', 'B']).
card_cmc('farewell to arms', 3).
card_layout('farewell to arms', 'normal').

% Found in: C13, C14, SHM
card_name('farhaven elf', 'Farhaven Elf').
card_type('farhaven elf', 'Creature — Elf Druid').
card_types('farhaven elf', ['Creature']).
card_subtypes('farhaven elf', ['Elf', 'Druid']).
card_colors('farhaven elf', ['G']).
card_text('farhaven elf', 'When Farhaven Elf enters the battlefield, you may search your library for a basic land card and put it onto the battlefield tapped. If you do, shuffle your library.').
card_mana_cost('farhaven elf', ['2', 'G']).
card_cmc('farhaven elf', 3).
card_layout('farhaven elf', 'normal').
card_power('farhaven elf', 1).
card_toughness('farhaven elf', 1).

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB
card_name('farmstead', 'Farmstead').
card_type('farmstead', 'Enchantment — Aura').
card_types('farmstead', ['Enchantment']).
card_subtypes('farmstead', ['Aura']).
card_colors('farmstead', ['W']).
card_text('farmstead', 'Enchant land\nEnchanted land has \"At the beginning of your upkeep, you may pay {W}{W}. If you do, you gain 1 life.\"').
card_mana_cost('farmstead', ['W', 'W', 'W']).
card_cmc('farmstead', 3).
card_layout('farmstead', 'normal').
card_reserved('farmstead').

% Found in: FEM, ME2
card_name('farrel\'s mantle', 'Farrel\'s Mantle').
card_type('farrel\'s mantle', 'Enchantment — Aura').
card_types('farrel\'s mantle', ['Enchantment']).
card_subtypes('farrel\'s mantle', ['Aura']).
card_colors('farrel\'s mantle', ['W']).
card_text('farrel\'s mantle', 'Enchant creature\nWhenever enchanted creature attacks and isn\'t blocked, its controller may have it deal damage equal to its power plus 2 to another target creature. If that player does, the attacking creature assigns no combat damage this turn.').
card_mana_cost('farrel\'s mantle', ['2', 'W']).
card_cmc('farrel\'s mantle', 3).
card_layout('farrel\'s mantle', 'normal').

% Found in: FEM, ME2
card_name('farrel\'s zealot', 'Farrel\'s Zealot').
card_type('farrel\'s zealot', 'Creature — Human').
card_types('farrel\'s zealot', ['Creature']).
card_subtypes('farrel\'s zealot', ['Human']).
card_colors('farrel\'s zealot', ['W']).
card_text('farrel\'s zealot', 'Whenever Farrel\'s Zealot attacks and isn\'t blocked, you may have it deal 3 damage to target creature. If you do, Farrel\'s Zealot assigns no combat damage this turn.').
card_mana_cost('farrel\'s zealot', ['1', 'W', 'W']).
card_cmc('farrel\'s zealot', 3).
card_layout('farrel\'s zealot', 'normal').
card_power('farrel\'s zealot', 2).
card_toughness('farrel\'s zealot', 2).

% Found in: FEM
card_name('farrelite priest', 'Farrelite Priest').
card_type('farrelite priest', 'Creature — Human Cleric').
card_types('farrelite priest', ['Creature']).
card_subtypes('farrelite priest', ['Human', 'Cleric']).
card_colors('farrelite priest', ['W']).
card_text('farrelite priest', '{1}: Add {W} to your mana pool. If this ability has been activated four or more times this turn, sacrifice Farrelite Priest at the beginning of the next end step.').
card_mana_cost('farrelite priest', ['1', 'W', 'W']).
card_cmc('farrelite priest', 3).
card_layout('farrelite priest', 'normal').
card_power('farrelite priest', 1).
card_toughness('farrelite priest', 3).

% Found in: M13, RAV, pFNM
card_name('farseek', 'Farseek').
card_type('farseek', 'Sorcery').
card_types('farseek', ['Sorcery']).
card_subtypes('farseek', []).
card_colors('farseek', ['G']).
card_text('farseek', 'Search your library for a Plains, Island, Swamp, or Mountain card and put it onto the battlefield tapped. Then shuffle your library.').
card_mana_cost('farseek', ['1', 'G']).
card_cmc('farseek', 2).
card_layout('farseek', 'normal').

% Found in: MRD, PC2
card_name('farsight mask', 'Farsight Mask').
card_type('farsight mask', 'Artifact').
card_types('farsight mask', ['Artifact']).
card_subtypes('farsight mask', []).
card_colors('farsight mask', []).
card_text('farsight mask', 'Whenever a source an opponent controls deals damage to you, if Farsight Mask is untapped, you may draw a card.').
card_mana_cost('farsight mask', ['5']).
card_cmc('farsight mask', 5).
card_layout('farsight mask', 'normal').

% Found in: FRF
card_name('fascination', 'Fascination').
card_type('fascination', 'Sorcery').
card_types('fascination', ['Sorcery']).
card_subtypes('fascination', []).
card_colors('fascination', ['U']).
card_text('fascination', 'Choose one —\n• Each player draws X cards.\n• Each player puts the top X cards of his or her library into his or her graveyard.').
card_mana_cost('fascination', ['X', 'U', 'U']).
card_cmc('fascination', 2).
card_layout('fascination', 'normal').

% Found in: UNH
card_name('fascist art director', 'Fascist Art Director').
card_type('fascist art director', 'Creature — Human Horror').
card_types('fascist art director', ['Creature']).
card_subtypes('fascist art director', ['Human', 'Horror']).
card_colors('fascist art director', ['W']).
card_text('fascist art director', '{W}{W}: Fascist Art Director gains protection from the artist of your choice until end of turn..').
card_mana_cost('fascist art director', ['1', 'W', 'W']).
card_cmc('fascist art director', 3).
card_layout('fascist art director', 'normal').
card_power('fascist art director', 2).
card_toughness('fascist art director', 2).

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME4, VMA
card_name('fastbond', 'Fastbond').
card_type('fastbond', 'Enchantment').
card_types('fastbond', ['Enchantment']).
card_subtypes('fastbond', []).
card_colors('fastbond', ['G']).
card_text('fastbond', 'You may play any number of lands on each of your turns.\nWhenever you play a land, if it wasn\'t the first land you played this turn, Fastbond deals 1 damage to you.').
card_mana_cost('fastbond', ['G']).
card_cmc('fastbond', 1).
card_layout('fastbond', 'normal').
card_reserved('fastbond').

% Found in: DRK
card_name('fasting', 'Fasting').
card_type('fasting', 'Enchantment').
card_types('fasting', ['Enchantment']).
card_subtypes('fasting', []).
card_colors('fasting', ['W']).
card_text('fasting', 'At the beginning of your upkeep, put a hunger counter on Fasting. Then destroy Fasting if it has five or more hunger counters on it.\nIf you would begin your draw step, you may skip that step instead. If you do, you gain 2 life.\nWhen you draw a card, destroy Fasting.').
card_mana_cost('fasting', ['W']).
card_cmc('fasting', 1).
card_layout('fasting', 'normal').

% Found in: UNH
card_name('fat ass', 'Fat Ass').
card_type('fat ass', 'Creature — Donkey Shaman').
card_types('fat ass', ['Creature']).
card_subtypes('fat ass', ['Donkey', 'Shaman']).
card_colors('fat ass', ['G']).
card_text('fat ass', 'Fat Ass gets +2/+2 and has trample as long as you\'re eating. (Food is in your mouth and you\'re chewing, licking, sucking, or swallowing it.)').
card_mana_cost('fat ass', ['4', 'G']).
card_cmc('fat ass', 5).
card_layout('fat ass', 'normal').
card_power('fat ass', 2).
card_toughness('fat ass', 3.5).

% Found in: FUT
card_name('fatal attraction', 'Fatal Attraction').
card_type('fatal attraction', 'Enchantment — Aura').
card_types('fatal attraction', ['Enchantment']).
card_subtypes('fatal attraction', ['Aura']).
card_colors('fatal attraction', ['R']).
card_text('fatal attraction', 'Enchant creature\nWhen Fatal Attraction enters the battlefield, it deals 2 damage to enchanted creature.\nAt the beginning of your upkeep, Fatal Attraction deals 4 damage to enchanted creature.').
card_mana_cost('fatal attraction', ['2', 'R']).
card_cmc('fatal attraction', 3).
card_layout('fatal attraction', 'normal').

% Found in: 6ED, WTH
card_name('fatal blow', 'Fatal Blow').
card_type('fatal blow', 'Instant').
card_types('fatal blow', ['Instant']).
card_subtypes('fatal blow', []).
card_colors('fatal blow', ['B']).
card_text('fatal blow', 'Destroy target creature that was dealt damage this turn. It can\'t be regenerated.').
card_mana_cost('fatal blow', ['B']).
card_cmc('fatal blow', 1).
card_layout('fatal blow', 'normal').

% Found in: PLC
card_name('fatal frenzy', 'Fatal Frenzy').
card_type('fatal frenzy', 'Instant').
card_types('fatal frenzy', ['Instant']).
card_subtypes('fatal frenzy', []).
card_colors('fatal frenzy', ['R']).
card_text('fatal frenzy', 'Until end of turn, target creature you control gains trample and gets +X/+0, where X is its power. Sacrifice it at the beginning of the next end step.').
card_mana_cost('fatal frenzy', ['2', 'R']).
card_cmc('fatal frenzy', 3).
card_layout('fatal frenzy', 'normal').

% Found in: DGM
card_name('fatal fumes', 'Fatal Fumes').
card_type('fatal fumes', 'Instant').
card_types('fatal fumes', ['Instant']).
card_subtypes('fatal fumes', []).
card_colors('fatal fumes', ['B']).
card_text('fatal fumes', 'Target creature gets -4/-2 until end of turn.').
card_mana_cost('fatal fumes', ['3', 'B']).
card_cmc('fatal fumes', 4).
card_layout('fatal fumes', 'normal').

% Found in: ALL
card_name('fatal lore', 'Fatal Lore').
card_type('fatal lore', 'Sorcery').
card_types('fatal lore', ['Sorcery']).
card_subtypes('fatal lore', []).
card_colors('fatal lore', ['B']).
card_text('fatal lore', 'An opponent chooses one —\n• You draw three cards.\n• You destroy up to two target creatures that player controls. They can\'t be regenerated. That player draws up to three cards.').
card_mana_cost('fatal lore', ['2', 'B', 'B']).
card_cmc('fatal lore', 4).
card_layout('fatal lore', 'normal').
card_reserved('fatal lore').

% Found in: SCG
card_name('fatal mutation', 'Fatal Mutation').
card_type('fatal mutation', 'Enchantment — Aura').
card_types('fatal mutation', ['Enchantment']).
card_subtypes('fatal mutation', ['Aura']).
card_colors('fatal mutation', ['B']).
card_text('fatal mutation', 'Enchant creature\nWhen enchanted creature is turned face up, destroy it. It can\'t be regenerated.').
card_mana_cost('fatal mutation', ['B']).
card_cmc('fatal mutation', 1).
card_layout('fatal mutation', 'normal').

% Found in: THS
card_name('fate foretold', 'Fate Foretold').
card_type('fate foretold', 'Enchantment — Aura').
card_types('fate foretold', ['Enchantment']).
card_subtypes('fate foretold', ['Aura']).
card_colors('fate foretold', ['U']).
card_text('fate foretold', 'Enchant creature\nWhen Fate Foretold enters the battlefield, draw a card.\nWhen enchanted creature dies, its controller draws a card.').
card_mana_cost('fate foretold', ['1', 'U']).
card_cmc('fate foretold', 2).
card_layout('fate foretold', 'normal').

% Found in: DTK
card_name('fate forgotten', 'Fate Forgotten').
card_type('fate forgotten', 'Instant').
card_types('fate forgotten', ['Instant']).
card_subtypes('fate forgotten', []).
card_colors('fate forgotten', ['W']).
card_text('fate forgotten', 'Exile target artifact or enchantment.').
card_mana_cost('fate forgotten', ['2', 'W']).
card_cmc('fate forgotten', 3).
card_layout('fate forgotten', 'normal').

% Found in: SHM
card_name('fate transfer', 'Fate Transfer').
card_type('fate transfer', 'Instant').
card_types('fate transfer', ['Instant']).
card_subtypes('fate transfer', []).
card_colors('fate transfer', ['U', 'B']).
card_text('fate transfer', 'Move all counters from target creature onto another target creature.').
card_mana_cost('fate transfer', ['1', 'U/B']).
card_cmc('fate transfer', 2).
card_layout('fate transfer', 'normal').

% Found in: BNG
card_name('fate unraveler', 'Fate Unraveler').
card_type('fate unraveler', 'Enchantment Creature — Hag').
card_types('fate unraveler', ['Enchantment', 'Creature']).
card_subtypes('fate unraveler', ['Hag']).
card_colors('fate unraveler', ['B']).
card_text('fate unraveler', 'Whenever an opponent draws a card, Fate Unraveler deals 1 damage to that player.').
card_mana_cost('fate unraveler', ['3', 'B']).
card_cmc('fate unraveler', 4).
card_layout('fate unraveler', 'normal').
card_power('fate unraveler', 3).
card_toughness('fate unraveler', 4).

% Found in: BNG, pMEI
card_name('fated conflagration', 'Fated Conflagration').
card_type('fated conflagration', 'Instant').
card_types('fated conflagration', ['Instant']).
card_subtypes('fated conflagration', []).
card_colors('fated conflagration', ['R']).
card_text('fated conflagration', 'Fated Conflagration deals 5 damage to target creature or planeswalker. If it\'s your turn, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('fated conflagration', ['1', 'R', 'R', 'R']).
card_cmc('fated conflagration', 4).
card_layout('fated conflagration', 'normal').

% Found in: BNG
card_name('fated infatuation', 'Fated Infatuation').
card_type('fated infatuation', 'Instant').
card_types('fated infatuation', ['Instant']).
card_subtypes('fated infatuation', []).
card_colors('fated infatuation', ['U']).
card_text('fated infatuation', 'Put a token onto the battlefield that\'s a copy of target creature you control. If it\'s your turn, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('fated infatuation', ['U', 'U', 'U']).
card_cmc('fated infatuation', 3).
card_layout('fated infatuation', 'normal').

% Found in: BNG, CPK
card_name('fated intervention', 'Fated Intervention').
card_type('fated intervention', 'Instant').
card_types('fated intervention', ['Instant']).
card_subtypes('fated intervention', []).
card_colors('fated intervention', ['G']).
card_text('fated intervention', 'Put two 3/3 green Centaur enchantment creature tokens onto the battlefield. If it\'s your turn, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('fated intervention', ['2', 'G', 'G', 'G']).
card_cmc('fated intervention', 5).
card_layout('fated intervention', 'normal').

% Found in: BNG
card_name('fated retribution', 'Fated Retribution').
card_type('fated retribution', 'Instant').
card_types('fated retribution', ['Instant']).
card_subtypes('fated retribution', []).
card_colors('fated retribution', ['W']).
card_text('fated retribution', 'Destroy all creatures and planeswalkers. If it\'s your turn, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('fated retribution', ['4', 'W', 'W', 'W']).
card_cmc('fated retribution', 7).
card_layout('fated retribution', 'normal').

% Found in: BNG
card_name('fated return', 'Fated Return').
card_type('fated return', 'Instant').
card_types('fated return', ['Instant']).
card_subtypes('fated return', []).
card_colors('fated return', ['B']).
card_text('fated return', 'Put target creature card from a graveyard onto the battlefield under your control. It gains indestructible. If it\'s your turn, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('fated return', ['4', 'B', 'B', 'B']).
card_cmc('fated return', 7).
card_layout('fated return', 'normal').

% Found in: MRD
card_name('fatespinner', 'Fatespinner').
card_type('fatespinner', 'Creature — Human Wizard').
card_types('fatespinner', ['Creature']).
card_subtypes('fatespinner', ['Human', 'Wizard']).
card_colors('fatespinner', ['U']).
card_text('fatespinner', 'At the beginning of each opponent\'s upkeep, that player chooses draw step, main phase, or combat phase. The player skips each instance of the chosen step or phase this turn.').
card_mana_cost('fatespinner', ['1', 'U', 'U']).
card_cmc('fatespinner', 3).
card_layout('fatespinner', 'normal').
card_power('fatespinner', 1).
card_toughness('fatespinner', 2).

% Found in: ALA
card_name('fatestitcher', 'Fatestitcher').
card_type('fatestitcher', 'Creature — Zombie Wizard').
card_types('fatestitcher', ['Creature']).
card_subtypes('fatestitcher', ['Zombie', 'Wizard']).
card_colors('fatestitcher', ['U']).
card_text('fatestitcher', '{T}: You may tap or untap another target permanent.\nUnearth {U} ({U}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('fatestitcher', ['3', 'U']).
card_cmc('fatestitcher', 4).
card_layout('fatestitcher', 'normal').
card_power('fatestitcher', 1).
card_toughness('fatestitcher', 2).

% Found in: BFZ
card_name('fathom feeder', 'Fathom Feeder').
card_type('fathom feeder', 'Creature — Eldrazi Drone').
card_types('fathom feeder', ['Creature']).
card_subtypes('fathom feeder', ['Eldrazi', 'Drone']).
card_colors('fathom feeder', []).
card_text('fathom feeder', 'Devoid (This card has no color.)\nDeathtouch\nIngest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.)\n{3}{U}{B}: Draw a card. Each opponent exiles the top card of his or her library.').
card_mana_cost('fathom feeder', ['U', 'B']).
card_cmc('fathom feeder', 2).
card_layout('fathom feeder', 'normal').
card_power('fathom feeder', 1).
card_toughness('fathom feeder', 1).

% Found in: GTC, pPRE
card_name('fathom mage', 'Fathom Mage').
card_type('fathom mage', 'Creature — Human Wizard').
card_types('fathom mage', ['Creature']).
card_subtypes('fathom mage', ['Human', 'Wizard']).
card_colors('fathom mage', ['U', 'G']).
card_text('fathom mage', 'Evolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)\nWhenever a +1/+1 counter is placed on Fathom Mage, you may draw a card.').
card_mana_cost('fathom mage', ['2', 'G', 'U']).
card_cmc('fathom mage', 4).
card_layout('fathom mage', 'normal').
card_power('fathom mage', 1).
card_toughness('fathom mage', 1).

% Found in: C14, DD2, DD3_JVC, DDN, TSP
card_name('fathom seer', 'Fathom Seer').
card_type('fathom seer', 'Creature — Illusion').
card_types('fathom seer', ['Creature']).
card_subtypes('fathom seer', ['Illusion']).
card_colors('fathom seer', ['U']).
card_text('fathom seer', 'Morph—Return two Islands you control to their owner\'s hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Fathom Seer is turned face up, draw two cards.').
card_mana_cost('fathom seer', ['1', 'U']).
card_cmc('fathom seer', 2).
card_layout('fathom seer', 'normal').
card_power('fathom seer', 1).
card_toughness('fathom seer', 3).

% Found in: LRW
card_name('fathom trawl', 'Fathom Trawl').
card_type('fathom trawl', 'Sorcery').
card_types('fathom trawl', ['Sorcery']).
card_subtypes('fathom trawl', []).
card_colors('fathom trawl', ['U']).
card_text('fathom trawl', 'Reveal cards from the top of your library until you reveal three nonland cards. Put the nonland cards revealed this way into your hand, then put the rest of the revealed cards on the bottom of your library in any order.').
card_mana_cost('fathom trawl', ['3', 'U', 'U']).
card_cmc('fathom trawl', 5).
card_layout('fathom trawl', 'normal').

% Found in: UDS
card_name('fatigue', 'Fatigue').
card_type('fatigue', 'Sorcery').
card_types('fatigue', ['Sorcery']).
card_subtypes('fatigue', []).
card_colors('fatigue', ['U']).
card_text('fatigue', 'Target player skips his or her next draw step.').
card_mana_cost('fatigue', ['1', 'U']).
card_cmc('fatigue', 2).
card_layout('fatigue', 'normal').

% Found in: USG
card_name('fault line', 'Fault Line').
card_type('fault line', 'Instant').
card_types('fault line', ['Instant']).
card_subtypes('fault line', []).
card_colors('fault line', ['R']).
card_text('fault line', 'Fault Line deals X damage to each creature without flying and each player.').
card_mana_cost('fault line', ['X', 'R', 'R']).
card_cmc('fault line', 2).
card_layout('fault line', 'normal').

% Found in: PCY
card_name('fault riders', 'Fault Riders').
card_type('fault riders', 'Creature — Human Soldier').
card_types('fault riders', ['Creature']).
card_subtypes('fault riders', ['Human', 'Soldier']).
card_colors('fault riders', ['R']).
card_text('fault riders', 'Sacrifice a land: Fault Riders gets +2/+0 and gains first strike until end of turn. Activate this ability only once each turn.').
card_mana_cost('fault riders', ['2', 'R']).
card_cmc('fault riders', 3).
card_layout('fault riders', 'normal').
card_power('fault riders', 2).
card_toughness('fault riders', 2).

% Found in: CMD, LRW
card_name('faultgrinder', 'Faultgrinder').
card_type('faultgrinder', 'Creature — Elemental').
card_types('faultgrinder', ['Creature']).
card_subtypes('faultgrinder', ['Elemental']).
card_colors('faultgrinder', ['R']).
card_text('faultgrinder', 'Trample\nWhen Faultgrinder enters the battlefield, destroy target land.\nEvoke {4}{R} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('faultgrinder', ['6', 'R']).
card_cmc('faultgrinder', 7).
card_layout('faultgrinder', 'normal').
card_power('faultgrinder', 4).
card_toughness('faultgrinder', 4).

% Found in: M11
card_name('fauna shaman', 'Fauna Shaman').
card_type('fauna shaman', 'Creature — Elf Shaman').
card_types('fauna shaman', ['Creature']).
card_subtypes('fauna shaman', ['Elf', 'Shaman']).
card_colors('fauna shaman', ['G']).
card_text('fauna shaman', '{G}, {T}, Discard a creature card: Search your library for a creature card, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('fauna shaman', ['1', 'G']).
card_cmc('fauna shaman', 2).
card_layout('fauna shaman', 'normal').
card_power('fauna shaman', 2).
card_toughness('fauna shaman', 2).

% Found in: LRW
card_name('favor of the mighty', 'Favor of the Mighty').
card_type('favor of the mighty', 'Tribal Enchantment — Giant').
card_types('favor of the mighty', ['Tribal', 'Enchantment']).
card_subtypes('favor of the mighty', ['Giant']).
card_colors('favor of the mighty', ['W']).
card_text('favor of the mighty', 'Each creature with the highest converted mana cost has protection from all colors.').
card_mana_cost('favor of the mighty', ['1', 'W']).
card_cmc('favor of the mighty', 2).
card_layout('favor of the mighty', 'normal').

% Found in: EVE
card_name('favor of the overbeing', 'Favor of the Overbeing').
card_type('favor of the overbeing', 'Enchantment — Aura').
card_types('favor of the overbeing', ['Enchantment']).
card_subtypes('favor of the overbeing', ['Aura']).
card_colors('favor of the overbeing', ['U', 'G']).
card_text('favor of the overbeing', 'Enchant creature\nAs long as enchanted creature is green, it gets +1/+1 and has vigilance.\nAs long as enchanted creature is blue, it gets +1/+1 and has flying.').
card_mana_cost('favor of the overbeing', ['1', 'G/U']).
card_cmc('favor of the overbeing', 2).
card_layout('favor of the overbeing', 'normal').

% Found in: DKA
card_name('favor of the woods', 'Favor of the Woods').
card_type('favor of the woods', 'Enchantment — Aura').
card_types('favor of the woods', ['Enchantment']).
card_subtypes('favor of the woods', ['Aura']).
card_colors('favor of the woods', ['G']).
card_text('favor of the woods', 'Enchant creature\nWhenever enchanted creature blocks, you gain 3 life.').
card_mana_cost('favor of the woods', ['2', 'G']).
card_cmc('favor of the woods', 3).
card_layout('favor of the woods', 'normal').

% Found in: MIR
card_name('favorable destiny', 'Favorable Destiny').
card_type('favorable destiny', 'Enchantment — Aura').
card_types('favorable destiny', ['Enchantment']).
card_subtypes('favorable destiny', ['Aura']).
card_colors('favorable destiny', ['W']).
card_text('favorable destiny', 'Enchant creature\nEnchanted creature gets +1/+2 as long as it\'s white.\nEnchanted creature has shroud as long as its controller controls another creature. (It can\'t be the target of spells or abilities.)').
card_mana_cost('favorable destiny', ['1', 'W']).
card_cmc('favorable destiny', 2).
card_layout('favorable destiny', 'normal').

% Found in: AVR, CNS
card_name('favorable winds', 'Favorable Winds').
card_type('favorable winds', 'Enchantment').
card_types('favorable winds', ['Enchantment']).
card_subtypes('favorable winds', []).
card_colors('favorable winds', ['U']).
card_text('favorable winds', 'Creatures you control with flying get +1/+1.').
card_mana_cost('favorable winds', ['1', 'U']).
card_cmc('favorable winds', 2).
card_layout('favorable winds', 'normal').

% Found in: THS
card_name('favored hoplite', 'Favored Hoplite').
card_type('favored hoplite', 'Creature — Human Soldier').
card_types('favored hoplite', ['Creature']).
card_subtypes('favored hoplite', ['Human', 'Soldier']).
card_colors('favored hoplite', ['W']).
card_text('favored hoplite', 'Heroic — Whenever you cast a spell that targets Favored Hoplite, put a +1/+1 counter on Favored Hoplite and prevent all damage that would be dealt to it this turn.').
card_mana_cost('favored hoplite', ['W']).
card_cmc('favored hoplite', 1).
card_layout('favored hoplite', 'normal').
card_power('favored hoplite', 1).
card_toughness('favored hoplite', 2).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, CED, CEI, ICE, LEA, LEB
card_name('fear', 'Fear').
card_type('fear', 'Enchantment — Aura').
card_types('fear', ['Enchantment']).
card_subtypes('fear', ['Aura']).
card_colors('fear', ['B']).
card_text('fear', 'Enchant creature (Target a creature as you cast this. This card enters the battlefield attached to that creature.)\nEnchanted creature has fear. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('fear', ['B', 'B']).
card_cmc('fear', 2).
card_layout('fear', 'normal').

% Found in: FRF
card_name('fearsome awakening', 'Fearsome Awakening').
card_type('fearsome awakening', 'Sorcery').
card_types('fearsome awakening', ['Sorcery']).
card_subtypes('fearsome awakening', []).
card_colors('fearsome awakening', ['B']).
card_text('fearsome awakening', 'Return target creature card from your graveyard to the battlefield. If it\'s a Dragon, put two +1/+1 counters on it.').
card_mana_cost('fearsome awakening', ['4', 'B']).
card_cmc('fearsome awakening', 5).
card_layout('fearsome awakening', 'normal').

% Found in: BNG
card_name('fearsome temper', 'Fearsome Temper').
card_type('fearsome temper', 'Enchantment — Aura').
card_types('fearsome temper', ['Enchantment']).
card_subtypes('fearsome temper', ['Aura']).
card_colors('fearsome temper', ['R']).
card_text('fearsome temper', 'Enchant creature\nEnchanted creature gets +2/+2 and has \"{2}{R}: Target creature can\'t block this creature this turn.\"').
card_mana_cost('fearsome temper', ['2', 'R']).
card_cmc('fearsome temper', 3).
card_layout('fearsome temper', 'normal').

% Found in: ZEN, pMEI
card_name('feast of blood', 'Feast of Blood').
card_type('feast of blood', 'Sorcery').
card_types('feast of blood', ['Sorcery']).
card_subtypes('feast of blood', []).
card_colors('feast of blood', ['B']).
card_text('feast of blood', 'Cast Feast of Blood only if you control two or more Vampires.\nDestroy target creature. You gain 4 life.').
card_mana_cost('feast of blood', ['1', 'B']).
card_cmc('feast of blood', 2).
card_layout('feast of blood', 'normal').

% Found in: JOU
card_name('feast of dreams', 'Feast of Dreams').
card_type('feast of dreams', 'Instant').
card_types('feast of dreams', ['Instant']).
card_subtypes('feast of dreams', []).
card_colors('feast of dreams', ['B']).
card_text('feast of dreams', 'Destroy target enchanted creature or enchantment creature.').
card_mana_cost('feast of dreams', ['1', 'B']).
card_cmc('feast of dreams', 2).
card_layout('feast of dreams', 'normal').

% Found in: CSP
card_name('feast of flesh', 'Feast of Flesh').
card_type('feast of flesh', 'Sorcery').
card_types('feast of flesh', ['Sorcery']).
card_subtypes('feast of flesh', []).
card_colors('feast of flesh', ['B']).
card_text('feast of flesh', 'Feast of Flesh deals X damage to target creature and you gain X life, where X is 1 plus the number of cards named Feast of Flesh in all graveyards.').
card_mana_cost('feast of flesh', ['B']).
card_cmc('feast of flesh', 1).
card_layout('feast of flesh', 'normal').

% Found in: 6ED, ATH, HML
card_name('feast of the unicorn', 'Feast of the Unicorn').
card_type('feast of the unicorn', 'Enchantment — Aura').
card_types('feast of the unicorn', ['Enchantment']).
card_subtypes('feast of the unicorn', ['Aura']).
card_colors('feast of the unicorn', ['B']).
card_text('feast of the unicorn', 'Enchant creature\nEnchanted creature gets +4/+0.').
card_mana_cost('feast of the unicorn', ['3', 'B']).
card_cmc('feast of the unicorn', 4).
card_layout('feast of the unicorn', 'normal').

% Found in: CHK
card_name('feast of worms', 'Feast of Worms').
card_type('feast of worms', 'Sorcery — Arcane').
card_types('feast of worms', ['Sorcery']).
card_subtypes('feast of worms', ['Arcane']).
card_colors('feast of worms', ['G']).
card_text('feast of worms', 'Destroy target land. If that land was legendary, its controller sacrifices another land.').
card_mana_cost('feast of worms', ['3', 'G', 'G']).
card_cmc('feast of worms', 5).
card_layout('feast of worms', 'normal').

% Found in: M15
card_name('feast on the fallen', 'Feast on the Fallen').
card_type('feast on the fallen', 'Enchantment').
card_types('feast on the fallen', ['Enchantment']).
card_subtypes('feast on the fallen', []).
card_colors('feast on the fallen', ['B']).
card_text('feast on the fallen', 'At the beginning of each upkeep, if an opponent lost life last turn, put a +1/+1 counter on target creature you control.').
card_mana_cost('feast on the fallen', ['2', 'B']).
card_cmc('feast on the fallen', 3).
card_layout('feast on the fallen', 'normal').

% Found in: ALL, DDJ, MED
card_name('feast or famine', 'Feast or Famine').
card_type('feast or famine', 'Instant').
card_types('feast or famine', ['Instant']).
card_subtypes('feast or famine', []).
card_colors('feast or famine', ['B']).
card_text('feast or famine', 'Choose one —\n• Put a 2/2 black Zombie creature token onto the battlefield.\n• Destroy target nonartifact, nonblack creature. It can\'t be regenerated.').
card_mana_cost('feast or famine', ['3', 'B']).
card_cmc('feast or famine', 4).
card_layout('feast or famine', 'normal').

% Found in: KTK
card_name('feat of resistance', 'Feat of Resistance').
card_type('feat of resistance', 'Instant').
card_types('feat of resistance', ['Instant']).
card_subtypes('feat of resistance', []).
card_colors('feat of resistance', ['W']).
card_text('feat of resistance', 'Put a +1/+1 counter on target creature you control. It gains protection from the color of your choice until end of turn.').
card_mana_cost('feat of resistance', ['1', 'W']).
card_cmc('feat of resistance', 2).
card_layout('feat of resistance', 'normal').

% Found in: 8ED, BRB, C13, USG
card_name('fecundity', 'Fecundity').
card_type('fecundity', 'Enchantment').
card_types('fecundity', ['Enchantment']).
card_subtypes('fecundity', []).
card_colors('fecundity', ['G']).
card_text('fecundity', 'Whenever a creature dies, that creature\'s controller may draw a card.').
card_mana_cost('fecundity', ['2', 'G']).
card_cmc('fecundity', 3).
card_layout('fecundity', 'normal').

% Found in: TSP
card_name('feebleness', 'Feebleness').
card_type('feebleness', 'Enchantment — Aura').
card_types('feebleness', ['Enchantment']).
card_subtypes('feebleness', ['Aura']).
card_colors('feebleness', ['B']).
card_text('feebleness', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature gets -2/-1.').
card_mana_cost('feebleness', ['1', 'B']).
card_cmc('feebleness', 2).
card_layout('feebleness', 'normal').

% Found in: KTK
card_name('feed the clan', 'Feed the Clan').
card_type('feed the clan', 'Instant').
card_types('feed the clan', ['Instant']).
card_subtypes('feed the clan', []).
card_colors('feed the clan', ['G']).
card_text('feed the clan', 'You gain 5 life.\nFerocious — You gain 10 life instead if you control a creature with power 4 or greater.').
card_mana_cost('feed the clan', ['1', 'G']).
card_cmc('feed the clan', 2).
card_layout('feed the clan', 'normal').

% Found in: ARC
card_name('feed the machine', 'Feed the Machine').
card_type('feed the machine', 'Scheme').
card_types('feed the machine', ['Scheme']).
card_subtypes('feed the machine', []).
card_colors('feed the machine', []).
card_text('feed the machine', 'When you set this scheme in motion, target opponent chooses self or others. If that player chooses self, he or she sacrifices two creatures. If the player chooses others, each of your other opponents sacrifices a creature.').
card_layout('feed the machine', 'scheme').

% Found in: DKA
card_name('feed the pack', 'Feed the Pack').
card_type('feed the pack', 'Enchantment').
card_types('feed the pack', ['Enchantment']).
card_subtypes('feed the pack', []).
card_colors('feed the pack', ['G']).
card_text('feed the pack', 'At the beginning of your end step, you may sacrifice a nontoken creature. If you do, put X 2/2 green Wolf creature tokens onto the battlefield, where X is the sacrificed creature\'s toughness.').
card_mana_cost('feed the pack', ['5', 'G']).
card_cmc('feed the pack', 6).
card_layout('feed the pack', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, ITP, LEA, LEB, RQS
card_name('feedback', 'Feedback').
card_type('feedback', 'Enchantment — Aura').
card_types('feedback', ['Enchantment']).
card_subtypes('feedback', ['Aura']).
card_colors('feedback', ['U']).
card_text('feedback', 'Enchant enchantment\nAt the beginning of the upkeep of enchanted enchantment\'s controller, Feedback deals 1 damage to that player.').
card_mana_cost('feedback', ['2', 'U']).
card_cmc('feedback', 3).
card_layout('feedback', 'normal').

% Found in: 5DN
card_name('feedback bolt', 'Feedback Bolt').
card_type('feedback bolt', 'Instant').
card_types('feedback bolt', ['Instant']).
card_subtypes('feedback bolt', []).
card_colors('feedback bolt', ['R']).
card_text('feedback bolt', 'Feedback Bolt deals damage to target player equal to the number of artifacts you control.').
card_mana_cost('feedback bolt', ['4', 'R']).
card_cmc('feedback bolt', 5).
card_layout('feedback bolt', 'normal').

% Found in: ONS
card_name('feeding frenzy', 'Feeding Frenzy').
card_type('feeding frenzy', 'Instant').
card_types('feeding frenzy', ['Instant']).
card_subtypes('feeding frenzy', []).
card_colors('feeding frenzy', ['B']).
card_text('feeding frenzy', 'Target creature gets -X/-X until end of turn, where X is the number of Zombies on the battlefield.').
card_mana_cost('feeding frenzy', ['2', 'B']).
card_cmc('feeding frenzy', 3).
card_layout('feeding frenzy', 'normal').

% Found in: HOP
card_name('feeding grounds', 'Feeding Grounds').
card_type('feeding grounds', 'Plane — Muraganda').
card_types('feeding grounds', ['Plane']).
card_subtypes('feeding grounds', ['Muraganda']).
card_colors('feeding grounds', []).
card_text('feeding grounds', 'Red spells cost {1} less to cast.\nGreen spells cost {1} less to cast.\nWhenever you roll {C}, put X +1/+1 counters on target creature, where X is that creature\'s converted mana cost.').
card_layout('feeding grounds', 'plane').

% Found in: ISD
card_name('feeling of dread', 'Feeling of Dread').
card_type('feeling of dread', 'Instant').
card_types('feeling of dread', ['Instant']).
card_subtypes('feeling of dread', []).
card_colors('feeling of dread', ['W']).
card_text('feeling of dread', 'Tap up to two target creatures.\nFlashback {1}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('feeling of dread', ['1', 'W']).
card_cmc('feeling of dread', 2).
card_layout('feeling of dread', 'normal').

% Found in: LEG
card_name('feint', 'Feint').
card_type('feint', 'Instant').
card_types('feint', ['Instant']).
card_subtypes('feint', []).
card_colors('feint', ['R']).
card_text('feint', 'Tap all creatures blocking target attacking creature. Prevent all combat damage that would be dealt this turn by that creature and each creature blocking it.').
card_mana_cost('feint', ['R']).
card_cmc('feint', 1).
card_layout('feint', 'normal').

% Found in: C14
card_name('feldon of the third path', 'Feldon of the Third Path').
card_type('feldon of the third path', 'Legendary Creature — Human Artificer').
card_types('feldon of the third path', ['Creature']).
card_subtypes('feldon of the third path', ['Human', 'Artificer']).
card_supertypes('feldon of the third path', ['Legendary']).
card_colors('feldon of the third path', ['R']).
card_text('feldon of the third path', '{2}{R}, {T}: Put a token onto the battlefield that\'s a copy of target creature card in your graveyard, except it\'s an artifact in addition to its other types. It gains haste. Sacrifice it at the beginning of the next end step.').
card_mana_cost('feldon of the third path', ['1', 'R', 'R']).
card_cmc('feldon of the third path', 3).
card_layout('feldon of the third path', 'normal').
card_power('feldon of the third path', 2).
card_toughness('feldon of the third path', 3).

% Found in: 5ED, ATQ, CHR, TSB
card_name('feldon\'s cane', 'Feldon\'s Cane').
card_type('feldon\'s cane', 'Artifact').
card_types('feldon\'s cane', ['Artifact']).
card_subtypes('feldon\'s cane', []).
card_colors('feldon\'s cane', []).
card_text('feldon\'s cane', '{T}, Exile Feldon\'s Cane: Shuffle your graveyard into your library.').
card_mana_cost('feldon\'s cane', ['1']).
card_cmc('feldon\'s cane', 1).
card_layout('feldon\'s cane', 'normal').

% Found in: BNG
card_name('felhide brawler', 'Felhide Brawler').
card_type('felhide brawler', 'Creature — Minotaur').
card_types('felhide brawler', ['Creature']).
card_subtypes('felhide brawler', ['Minotaur']).
card_colors('felhide brawler', ['B']).
card_text('felhide brawler', 'Felhide Brawler can\'t block unless you control another Minotaur.').
card_mana_cost('felhide brawler', ['1', 'B']).
card_cmc('felhide brawler', 2).
card_layout('felhide brawler', 'normal').
card_power('felhide brawler', 2).
card_toughness('felhide brawler', 2).

% Found in: THS
card_name('felhide minotaur', 'Felhide Minotaur').
card_type('felhide minotaur', 'Creature — Minotaur').
card_types('felhide minotaur', ['Creature']).
card_subtypes('felhide minotaur', ['Minotaur']).
card_colors('felhide minotaur', ['B']).
card_text('felhide minotaur', '').
card_mana_cost('felhide minotaur', ['2', 'B']).
card_cmc('felhide minotaur', 3).
card_layout('felhide minotaur', 'normal').
card_power('felhide minotaur', 2).
card_toughness('felhide minotaur', 3).

% Found in: JOU
card_name('felhide petrifier', 'Felhide Petrifier').
card_type('felhide petrifier', 'Creature — Minotaur Warrior').
card_types('felhide petrifier', ['Creature']).
card_subtypes('felhide petrifier', ['Minotaur', 'Warrior']).
card_colors('felhide petrifier', ['B']).
card_text('felhide petrifier', 'Deathtouch\nOther Minotaur creatures you control have deathtouch.').
card_mana_cost('felhide petrifier', ['2', 'B']).
card_cmc('felhide petrifier', 3).
card_layout('felhide petrifier', 'normal').
card_power('felhide petrifier', 2).
card_toughness('felhide petrifier', 3).

% Found in: BNG
card_name('felhide spiritbinder', 'Felhide Spiritbinder').
card_type('felhide spiritbinder', 'Creature — Minotaur Shaman').
card_types('felhide spiritbinder', ['Creature']).
card_subtypes('felhide spiritbinder', ['Minotaur', 'Shaman']).
card_colors('felhide spiritbinder', ['R']).
card_text('felhide spiritbinder', 'Inspired — Whenever Felhide Spiritbinder becomes untapped, you may pay {1}{R}. If you do, put a token onto the battlefield that\'s a copy of another target creature except it\'s an enchantment in addition to its other types. It gains haste. Exile it at the beginning of the next end step.').
card_mana_cost('felhide spiritbinder', ['3', 'R']).
card_cmc('felhide spiritbinder', 4).
card_layout('felhide spiritbinder', 'normal').
card_power('felhide spiritbinder', 3).
card_toughness('felhide spiritbinder', 4).

% Found in: BFZ
card_name('felidar cub', 'Felidar Cub').
card_type('felidar cub', 'Creature — Cat Beast').
card_types('felidar cub', ['Creature']).
card_subtypes('felidar cub', ['Cat', 'Beast']).
card_colors('felidar cub', ['W']).
card_text('felidar cub', 'Sacrifice Felidar Cub: Destroy target enchantment.').
card_mana_cost('felidar cub', ['1', 'W']).
card_cmc('felidar cub', 2).
card_layout('felidar cub', 'normal').
card_power('felidar cub', 2).
card_toughness('felidar cub', 2).

% Found in: BFZ, ZEN
card_name('felidar sovereign', 'Felidar Sovereign').
card_type('felidar sovereign', 'Creature — Cat Beast').
card_types('felidar sovereign', ['Creature']).
card_subtypes('felidar sovereign', ['Cat', 'Beast']).
card_colors('felidar sovereign', ['W']).
card_text('felidar sovereign', 'Vigilance, lifelink\nAt the beginning of your upkeep, if you have 40 or more life, you win the game.').
card_mana_cost('felidar sovereign', ['4', 'W', 'W']).
card_cmc('felidar sovereign', 6).
card_layout('felidar sovereign', 'normal').
card_power('felidar sovereign', 4).
card_toughness('felidar sovereign', 6).

% Found in: PC2
card_name('felidar umbra', 'Felidar Umbra').
card_type('felidar umbra', 'Enchantment — Aura').
card_types('felidar umbra', ['Enchantment']).
card_subtypes('felidar umbra', ['Aura']).
card_colors('felidar umbra', ['W']).
card_text('felidar umbra', 'Enchant creature\nEnchanted creature has lifelink.\n{1}{W}: Attach Felidar Umbra to target creature you control.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_mana_cost('felidar umbra', ['1', 'W']).
card_cmc('felidar umbra', 2).
card_layout('felidar umbra', 'normal').

% Found in: C13
card_name('fell shepherd', 'Fell Shepherd').
card_type('fell shepherd', 'Creature — Avatar').
card_types('fell shepherd', ['Creature']).
card_subtypes('fell shepherd', ['Avatar']).
card_colors('fell shepherd', ['B']).
card_text('fell shepherd', 'Whenever Fell Shepherd deals combat damage to a player, you may return to your hand all creature cards that were put into your graveyard from the battlefield this turn.\n{B}, Sacrifice another creature: Target creature gets -2/-2 until end of turn.').
card_mana_cost('fell shepherd', ['5', 'B', 'B']).
card_cmc('fell shepherd', 7).
card_layout('fell shepherd', 'normal').
card_power('fell shepherd', 8).
card_toughness('fell shepherd', 6).

% Found in: C14
card_name('fell the mighty', 'Fell the Mighty').
card_type('fell the mighty', 'Sorcery').
card_types('fell the mighty', ['Sorcery']).
card_subtypes('fell the mighty', []).
card_colors('fell the mighty', ['W']).
card_text('fell the mighty', 'Destroy all creatures with power greater than target creature\'s power.').
card_mana_cost('fell the mighty', ['4', 'W']).
card_cmc('fell the mighty', 5).
card_layout('fell the mighty', 'normal').

% Found in: 4ED, 5ED, 9ED, CMD, DRK, ME3
card_name('fellwar stone', 'Fellwar Stone').
card_type('fellwar stone', 'Artifact').
card_types('fellwar stone', ['Artifact']).
card_subtypes('fellwar stone', []).
card_colors('fellwar stone', []).
card_text('fellwar stone', '{T}: Add to your mana pool one mana of any color that a land an opponent controls could produce.').
card_mana_cost('fellwar stone', ['2']).
card_cmc('fellwar stone', 2).
card_layout('fellwar stone', 'normal').

% Found in: 10E, 6ED, 7ED, MIR
card_name('femeref archers', 'Femeref Archers').
card_type('femeref archers', 'Creature — Human Archer').
card_types('femeref archers', ['Creature']).
card_subtypes('femeref archers', ['Human', 'Archer']).
card_colors('femeref archers', ['G']).
card_text('femeref archers', '{T}: Femeref Archers deals 4 damage to target attacking creature with flying.').
card_mana_cost('femeref archers', ['2', 'G']).
card_cmc('femeref archers', 3).
card_layout('femeref archers', 'normal').
card_power('femeref archers', 2).
card_toughness('femeref archers', 2).

% Found in: VIS
card_name('femeref enchantress', 'Femeref Enchantress').
card_type('femeref enchantress', 'Creature — Human Druid').
card_types('femeref enchantress', ['Creature']).
card_subtypes('femeref enchantress', ['Human', 'Druid']).
card_colors('femeref enchantress', ['W', 'G']).
card_text('femeref enchantress', 'Whenever an enchantment is put into a graveyard from the battlefield, draw a card.').
card_mana_cost('femeref enchantress', ['G', 'W']).
card_cmc('femeref enchantress', 2).
card_layout('femeref enchantress', 'normal').
card_power('femeref enchantress', 1).
card_toughness('femeref enchantress', 2).
card_reserved('femeref enchantress').

% Found in: MIR
card_name('femeref healer', 'Femeref Healer').
card_type('femeref healer', 'Creature — Human Cleric').
card_types('femeref healer', ['Creature']).
card_subtypes('femeref healer', ['Human', 'Cleric']).
card_colors('femeref healer', ['W']).
card_text('femeref healer', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_mana_cost('femeref healer', ['1', 'W']).
card_cmc('femeref healer', 2).
card_layout('femeref healer', 'normal').
card_power('femeref healer', 1).
card_toughness('femeref healer', 1).

% Found in: MIR
card_name('femeref knight', 'Femeref Knight').
card_type('femeref knight', 'Creature — Human Knight').
card_types('femeref knight', ['Creature']).
card_subtypes('femeref knight', ['Human', 'Knight']).
card_colors('femeref knight', ['W']).
card_text('femeref knight', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{W}: Femeref Knight gains vigilance until end of turn.').
card_mana_cost('femeref knight', ['2', 'W']).
card_cmc('femeref knight', 3).
card_layout('femeref knight', 'normal').
card_power('femeref knight', 2).
card_toughness('femeref knight', 2).

% Found in: MIR
card_name('femeref scouts', 'Femeref Scouts').
card_type('femeref scouts', 'Creature — Human Scout').
card_types('femeref scouts', ['Creature']).
card_subtypes('femeref scouts', ['Human', 'Scout']).
card_colors('femeref scouts', ['W']).
card_text('femeref scouts', '').
card_mana_cost('femeref scouts', ['2', 'W']).
card_cmc('femeref scouts', 3).
card_layout('femeref scouts', 'normal').
card_power('femeref scouts', 1).
card_toughness('femeref scouts', 4).

% Found in: PCY
card_name('fen stalker', 'Fen Stalker').
card_type('fen stalker', 'Creature — Nightstalker').
card_types('fen stalker', ['Creature']).
card_subtypes('fen stalker', ['Nightstalker']).
card_colors('fen stalker', ['B']).
card_text('fen stalker', 'Fen Stalker has fear as long as you control no untapped lands. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('fen stalker', ['3', 'B']).
card_cmc('fen stalker', 4).
card_layout('fen stalker', 'normal').
card_power('fen stalker', 3).
card_toughness('fen stalker', 2).

% Found in: MOR
card_name('fencer clique', 'Fencer Clique').
card_type('fencer clique', 'Creature — Faerie Soldier').
card_types('fencer clique', ['Creature']).
card_subtypes('fencer clique', ['Faerie', 'Soldier']).
card_colors('fencer clique', ['U']).
card_text('fencer clique', 'Flying\n{U}: Put Fencer Clique on top of its owner\'s library.').
card_mana_cost('fencer clique', ['2', 'U', 'U']).
card_cmc('fencer clique', 4).
card_layout('fencer clique', 'normal').
card_power('fencer clique', 3).
card_toughness('fencer clique', 2).

% Found in: GPT
card_name('fencer\'s magemark', 'Fencer\'s Magemark').
card_type('fencer\'s magemark', 'Enchantment — Aura').
card_types('fencer\'s magemark', ['Enchantment']).
card_subtypes('fencer\'s magemark', ['Aura']).
card_colors('fencer\'s magemark', ['R']).
card_text('fencer\'s magemark', 'Enchant creature\nCreatures you control that are enchanted get +1/+1 and have first strike.').
card_mana_cost('fencer\'s magemark', ['2', 'R']).
card_cmc('fencer\'s magemark', 3).
card_layout('fencer\'s magemark', 'normal').

% Found in: DDL, RTR
card_name('fencing ace', 'Fencing Ace').
card_type('fencing ace', 'Creature — Human Soldier').
card_types('fencing ace', ['Creature']).
card_subtypes('fencing ace', ['Human', 'Soldier']).
card_colors('fencing ace', ['W']).
card_text('fencing ace', 'Double strike (This creature deals both first-strike and regular combat damage.)').
card_mana_cost('fencing ace', ['1', 'W']).
card_cmc('fencing ace', 2).
card_layout('fencing ace', 'normal').
card_power('fencing ace', 1).
card_toughness('fencing ace', 1).

% Found in: UDS
card_name('fend off', 'Fend Off').
card_type('fend off', 'Instant').
card_types('fend off', ['Instant']).
card_subtypes('fend off', []).
card_colors('fend off', ['W']).
card_text('fend off', 'Prevent all combat damage that would be dealt by target creature this turn.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('fend off', ['1', 'W']).
card_cmc('fend off', 2).
card_layout('fend off', 'normal').

% Found in: MOR
card_name('fendeep summoner', 'Fendeep Summoner').
card_type('fendeep summoner', 'Creature — Treefolk Shaman').
card_types('fendeep summoner', ['Creature']).
card_subtypes('fendeep summoner', ['Treefolk', 'Shaman']).
card_colors('fendeep summoner', ['B']).
card_text('fendeep summoner', '{T}: Up to two target Swamps each become 3/5 Treefolk Warrior creatures in addition to their other types until end of turn.').
card_mana_cost('fendeep summoner', ['4', 'B']).
card_cmc('fendeep summoner', 5).
card_layout('fendeep summoner', 'normal').
card_power('fendeep summoner', 3).
card_toughness('fendeep summoner', 5).

% Found in: DGM, GPT
card_name('feral animist', 'Feral Animist').
card_type('feral animist', 'Creature — Goblin Shaman').
card_types('feral animist', ['Creature']).
card_subtypes('feral animist', ['Goblin', 'Shaman']).
card_colors('feral animist', ['R', 'G']).
card_text('feral animist', '{3}: Feral Animist gets +X/+0 until end of turn, where X is its power.').
card_mana_cost('feral animist', ['1', 'R', 'G']).
card_cmc('feral animist', 3).
card_layout('feral animist', 'normal').
card_power('feral animist', 2).
card_toughness('feral animist', 1).

% Found in: WWK
card_name('feral contest', 'Feral Contest').
card_type('feral contest', 'Sorcery').
card_types('feral contest', ['Sorcery']).
card_subtypes('feral contest', []).
card_colors('feral contest', ['G']).
card_text('feral contest', 'Put a +1/+1 counter on target creature you control. Another target creature blocks it this turn if able.').
card_mana_cost('feral contest', ['3', 'G']).
card_cmc('feral contest', 4).
card_layout('feral contest', 'normal').

% Found in: CHK
card_name('feral deceiver', 'Feral Deceiver').
card_type('feral deceiver', 'Creature — Spirit').
card_types('feral deceiver', ['Creature']).
card_subtypes('feral deceiver', ['Spirit']).
card_colors('feral deceiver', ['G']).
card_text('feral deceiver', '{1}: Look at the top card of your library.\n{2}: Reveal the top card of your library. If it\'s a land card, Feral Deceiver gets +2/+2 and gains trample until end of turn. Activate this ability only once each turn.').
card_mana_cost('feral deceiver', ['3', 'G']).
card_cmc('feral deceiver', 4).
card_layout('feral deceiver', 'normal').
card_power('feral deceiver', 3).
card_toughness('feral deceiver', 2).

% Found in: ALA, ARC
card_name('feral hydra', 'Feral Hydra').
card_type('feral hydra', 'Creature — Hydra Beast').
card_types('feral hydra', ['Creature']).
card_subtypes('feral hydra', ['Hydra', 'Beast']).
card_colors('feral hydra', ['G']).
card_text('feral hydra', 'Feral Hydra enters the battlefield with X +1/+1 counters on it.\n{3}: Put a +1/+1 counter on Feral Hydra. Any player may activate this ability.').
card_mana_cost('feral hydra', ['X', 'G']).
card_cmc('feral hydra', 1).
card_layout('feral hydra', 'normal').
card_power('feral hydra', 0).
card_toughness('feral hydra', 0).

% Found in: M15
card_name('feral incarnation', 'Feral Incarnation').
card_type('feral incarnation', 'Sorcery').
card_types('feral incarnation', ['Sorcery']).
card_subtypes('feral incarnation', []).
card_colors('feral incarnation', ['G']).
card_text('feral incarnation', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nPut three 3/3 green Beast creature tokens onto the battlefield.').
card_mana_cost('feral incarnation', ['8', 'G']).
card_cmc('feral incarnation', 9).
card_layout('feral incarnation', 'normal').

% Found in: VIS
card_name('feral instinct', 'Feral Instinct').
card_type('feral instinct', 'Instant').
card_types('feral instinct', ['Instant']).
card_subtypes('feral instinct', []).
card_colors('feral instinct', ['G']).
card_text('feral instinct', 'Target creature gets +1/+1 until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('feral instinct', ['1', 'G']).
card_cmc('feral instinct', 2).
card_layout('feral instinct', 'normal').

% Found in: THS
card_name('feral invocation', 'Feral Invocation').
card_type('feral invocation', 'Enchantment — Aura').
card_types('feral invocation', ['Enchantment']).
card_subtypes('feral invocation', ['Aura']).
card_colors('feral invocation', ['G']).
card_text('feral invocation', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature gets +2/+2.').
card_mana_cost('feral invocation', ['2', 'G']).
card_cmc('feral invocation', 3).
card_layout('feral invocation', 'normal').

% Found in: FRF
card_name('feral krushok', 'Feral Krushok').
card_type('feral krushok', 'Creature — Beast').
card_types('feral krushok', ['Creature']).
card_subtypes('feral krushok', ['Beast']).
card_colors('feral krushok', ['G']).
card_text('feral krushok', '').
card_mana_cost('feral krushok', ['4', 'G']).
card_cmc('feral krushok', 5).
card_layout('feral krushok', 'normal').
card_power('feral krushok', 5).
card_toughness('feral krushok', 4).

% Found in: SOK
card_name('feral lightning', 'Feral Lightning').
card_type('feral lightning', 'Sorcery').
card_types('feral lightning', ['Sorcery']).
card_subtypes('feral lightning', []).
card_colors('feral lightning', ['R']).
card_text('feral lightning', 'Put three 3/1 red Elemental creature tokens with haste onto the battlefield. Exile them at the beginning of the next end step.').
card_mana_cost('feral lightning', ['3', 'R', 'R', 'R']).
card_cmc('feral lightning', 6).
card_layout('feral lightning', 'normal').

% Found in: ISD
card_name('feral ridgewolf', 'Feral Ridgewolf').
card_type('feral ridgewolf', 'Creature — Wolf').
card_types('feral ridgewolf', ['Creature']).
card_subtypes('feral ridgewolf', ['Wolf']).
card_colors('feral ridgewolf', ['R']).
card_text('feral ridgewolf', 'Trample\n{1}{R}: Feral Ridgewolf gets +2/+0 until end of turn.').
card_mana_cost('feral ridgewolf', ['2', 'R']).
card_cmc('feral ridgewolf', 3).
card_layout('feral ridgewolf', 'normal').
card_power('feral ridgewolf', 1).
card_toughness('feral ridgewolf', 2).

% Found in: 6ED, BTD, MIR, POR, S99, pPOD
card_name('feral shadow', 'Feral Shadow').
card_type('feral shadow', 'Creature — Nightstalker').
card_types('feral shadow', ['Creature']).
card_subtypes('feral shadow', ['Nightstalker']).
card_colors('feral shadow', ['B']).
card_text('feral shadow', 'Flying').
card_mana_cost('feral shadow', ['2', 'B']).
card_cmc('feral shadow', 3).
card_layout('feral shadow', 'normal').
card_power('feral shadow', 2).
card_toughness('feral shadow', 1).

% Found in: FEM, ME2
card_name('feral thallid', 'Feral Thallid').
card_type('feral thallid', 'Creature — Fungus').
card_types('feral thallid', ['Creature']).
card_subtypes('feral thallid', ['Fungus']).
card_colors('feral thallid', ['G']).
card_text('feral thallid', 'At the beginning of your upkeep, put a spore counter on Feral Thallid.\nRemove three spore counters from Feral Thallid: Regenerate Feral Thallid.').
card_mana_cost('feral thallid', ['3', 'G', 'G', 'G']).
card_cmc('feral thallid', 6).
card_layout('feral thallid', 'normal').
card_power('feral thallid', 6).
card_toughness('feral thallid', 3).

% Found in: LGN, pPRE
card_name('feral throwback', 'Feral Throwback').
card_type('feral throwback', 'Creature — Beast').
card_types('feral throwback', ['Creature']).
card_subtypes('feral throwback', ['Beast']).
card_colors('feral throwback', ['G']).
card_text('feral throwback', 'Amplify 2 (As this creature enters the battlefield, put two +1/+1 counters on it for each Beast card you reveal in your hand.)\nProvoke (Whenever this creature attacks, you may have target creature defending player controls untap and block it if able.)').
card_mana_cost('feral throwback', ['4', 'G', 'G']).
card_cmc('feral throwback', 6).
card_layout('feral throwback', 'normal').
card_power('feral throwback', 3).
card_toughness('feral throwback', 3).

% Found in: 5DN
card_name('ferocious charge', 'Ferocious Charge').
card_type('ferocious charge', 'Instant').
card_types('ferocious charge', ['Instant']).
card_subtypes('ferocious charge', []).
card_colors('ferocious charge', ['G']).
card_text('ferocious charge', 'Target creature gets +4/+4 until end of turn. Scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('ferocious charge', ['2', 'G']).
card_cmc('ferocious charge', 3).
card_layout('ferocious charge', 'normal').

% Found in: MMQ
card_name('ferocity', 'Ferocity').
card_type('ferocity', 'Enchantment — Aura').
card_types('ferocity', ['Enchantment']).
card_subtypes('ferocity', ['Aura']).
card_colors('ferocity', ['G']).
card_text('ferocity', 'Enchant creature\nWhenever enchanted creature blocks or becomes blocked, you may put a +1/+1 counter on it.').
card_mana_cost('ferocity', ['1', 'G']).
card_cmc('ferocity', 2).
card_layout('ferocity', 'normal').

% Found in: 5ED, 7ED, HML
card_name('feroz\'s ban', 'Feroz\'s Ban').
card_type('feroz\'s ban', 'Artifact').
card_types('feroz\'s ban', ['Artifact']).
card_subtypes('feroz\'s ban', []).
card_colors('feroz\'s ban', []).
card_text('feroz\'s ban', 'Creature spells cost {2} more to cast.').
card_mana_cost('feroz\'s ban', ['6']).
card_cmc('feroz\'s ban', 6).
card_layout('feroz\'s ban', 'normal').

% Found in: 5DN
card_name('ferropede', 'Ferropede').
card_type('ferropede', 'Artifact Creature — Insect').
card_types('ferropede', ['Artifact', 'Creature']).
card_subtypes('ferropede', ['Insect']).
card_colors('ferropede', []).
card_text('ferropede', 'Ferropede can\'t be blocked.\nWhenever Ferropede deals combat damage to a player, you may remove a counter from target permanent.').
card_mana_cost('ferropede', ['3']).
card_cmc('ferropede', 3).
card_layout('ferropede', 'normal').
card_power('ferropede', 1).
card_toughness('ferropede', 1).

% Found in: SOM
card_name('ferrovore', 'Ferrovore').
card_type('ferrovore', 'Creature — Beast').
card_types('ferrovore', ['Creature']).
card_subtypes('ferrovore', ['Beast']).
card_colors('ferrovore', ['R']).
card_text('ferrovore', '{R}, Sacrifice an artifact: Ferrovore gets +3/+0 until end of turn.').
card_mana_cost('ferrovore', ['2', 'R']).
card_cmc('ferrovore', 3).
card_layout('ferrovore', 'normal').
card_power('ferrovore', 2).
card_toughness('ferrovore', 2).

% Found in: 8ED, BRB, DDE, HOP, INV, LRW, USG
card_name('fertile ground', 'Fertile Ground').
card_type('fertile ground', 'Enchantment — Aura').
card_types('fertile ground', ['Enchantment']).
card_subtypes('fertile ground', ['Aura']).
card_colors('fertile ground', ['G']).
card_text('fertile ground', 'Enchant land\nWhenever enchanted land is tapped for mana, its controller adds one mana of any color to his or her mana pool (in addition to the mana the land produces).').
card_mana_cost('fertile ground', ['1', 'G']).
card_cmc('fertile ground', 2).
card_layout('fertile ground', 'normal').

% Found in: DIS
card_name('fertile imagination', 'Fertile Imagination').
card_type('fertile imagination', 'Sorcery').
card_types('fertile imagination', ['Sorcery']).
card_subtypes('fertile imagination', []).
card_colors('fertile imagination', ['G']).
card_text('fertile imagination', 'Choose a card type. Target opponent reveals his or her hand. Put two 1/1 green Saproling creature tokens onto the battlefield for each card of the chosen type revealed this way. (Artifact, creature, enchantment, instant, land, planeswalker, sorcery, and tribal are card types.)').
card_mana_cost('fertile imagination', ['2', 'G', 'G']).
card_cmc('fertile imagination', 4).
card_layout('fertile imagination', 'normal').

% Found in: BFZ
card_name('fertile thicket', 'Fertile Thicket').
card_type('fertile thicket', 'Land').
card_types('fertile thicket', ['Land']).
card_subtypes('fertile thicket', []).
card_colors('fertile thicket', []).
card_text('fertile thicket', 'Fertile Thicket enters the battlefield tapped.\nWhen Fertile Thicket enters the battlefield, you may look at the top five cards of your library. If you do, reveal up to one basic land card from among them, then put that card on top of your library and the rest on the bottom in any order.\n{T}: Add {G} to your mana pool.').
card_layout('fertile thicket', 'normal').

% Found in: ARC, CMD, HOP, MOR
card_name('fertilid', 'Fertilid').
card_type('fertilid', 'Creature — Elemental').
card_types('fertilid', ['Creature']).
card_subtypes('fertilid', ['Elemental']).
card_colors('fertilid', ['G']).
card_text('fertilid', 'Fertilid enters the battlefield with two +1/+1 counters on it.\n{1}{G}, Remove a +1/+1 counter from Fertilid: Target player searches his or her library for a basic land card and puts it onto the battlefield tapped. Then that player shuffles his or her library.').
card_mana_cost('fertilid', ['2', 'G']).
card_cmc('fertilid', 3).
card_layout('fertilid', 'normal').
card_power('fertilid', 0).
card_toughness('fertilid', 0).

% Found in: AVR
card_name('fervent cathar', 'Fervent Cathar').
card_type('fervent cathar', 'Creature — Human Knight').
card_types('fervent cathar', ['Creature']).
card_subtypes('fervent cathar', ['Human', 'Knight']).
card_colors('fervent cathar', ['R']).
card_text('fervent cathar', 'Haste\nWhen Fervent Cathar enters the battlefield, target creature can\'t block this turn.').
card_mana_cost('fervent cathar', ['2', 'R']).
card_cmc('fervent cathar', 3).
card_layout('fervent cathar', 'normal').
card_power('fervent cathar', 2).
card_toughness('fervent cathar', 1).

% Found in: APC
card_name('fervent charge', 'Fervent Charge').
card_type('fervent charge', 'Enchantment').
card_types('fervent charge', ['Enchantment']).
card_subtypes('fervent charge', []).
card_colors('fervent charge', ['W', 'B', 'R']).
card_text('fervent charge', 'Whenever a creature you control attacks, it gets +2/+2 until end of turn.').
card_mana_cost('fervent charge', ['1', 'W', 'B', 'R']).
card_cmc('fervent charge', 4).
card_layout('fervent charge', 'normal').

% Found in: ODY
card_name('fervent denial', 'Fervent Denial').
card_type('fervent denial', 'Instant').
card_types('fervent denial', ['Instant']).
card_subtypes('fervent denial', []).
card_colors('fervent denial', ['U']).
card_text('fervent denial', 'Counter target spell.\nFlashback {5}{U}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('fervent denial', ['3', 'U', 'U']).
card_cmc('fervent denial', 5).
card_layout('fervent denial', 'normal').

% Found in: 6ED, 7ED, M13, WTH
card_name('fervor', 'Fervor').
card_type('fervor', 'Enchantment').
card_types('fervor', ['Enchantment']).
card_subtypes('fervor', []).
card_colors('fervor', ['R']).
card_text('fervor', 'Creatures you control have haste. (They can attack and {T} as soon as they come under your control.)').
card_mana_cost('fervor', ['2', 'R']).
card_cmc('fervor', 3).
card_layout('fervor', 'normal').

% Found in: MOR
card_name('festercreep', 'Festercreep').
card_type('festercreep', 'Creature — Elemental').
card_types('festercreep', ['Creature']).
card_subtypes('festercreep', ['Elemental']).
card_colors('festercreep', ['B']).
card_text('festercreep', 'Festercreep enters the battlefield with a +1/+1 counter on it.\n{1}{B}, Remove a +1/+1 counter from Festercreep: All other creatures get -1/-1 until end of turn.').
card_mana_cost('festercreep', ['1', 'B']).
card_cmc('festercreep', 2).
card_layout('festercreep', 'normal').
card_power('festercreep', 0).
card_toughness('festercreep', 0).

% Found in: M15
card_name('festergloom', 'Festergloom').
card_type('festergloom', 'Sorcery').
card_types('festergloom', ['Sorcery']).
card_subtypes('festergloom', []).
card_colors('festergloom', ['B']).
card_text('festergloom', 'Nonblack creatures get -1/-1 until end of turn.').
card_mana_cost('festergloom', ['2', 'B']).
card_cmc('festergloom', 3).
card_layout('festergloom', 'normal').

% Found in: DDM, ISD
card_name('festerhide boar', 'Festerhide Boar').
card_type('festerhide boar', 'Creature — Boar').
card_types('festerhide boar', ['Creature']).
card_subtypes('festerhide boar', ['Boar']).
card_colors('festerhide boar', ['G']).
card_text('festerhide boar', 'Trample\nMorbid — Festerhide Boar enters the battlefield with two +1/+1 counters on it if a creature died this turn.').
card_mana_cost('festerhide boar', ['3', 'G']).
card_cmc('festerhide boar', 4).
card_layout('festerhide boar', 'normal').
card_power('festerhide boar', 3).
card_toughness('festerhide boar', 3).

% Found in: WTH
card_name('festering evil', 'Festering Evil').
card_type('festering evil', 'Enchantment').
card_types('festering evil', ['Enchantment']).
card_subtypes('festering evil', []).
card_colors('festering evil', ['B']).
card_text('festering evil', 'At the beginning of your upkeep, Festering Evil deals 1 damage to each creature and each player.\n{B}{B}, Sacrifice Festering Evil: Festering Evil deals 3 damage to each creature and each player.').
card_mana_cost('festering evil', ['3', 'B', 'B']).
card_cmc('festering evil', 5).
card_layout('festering evil', 'normal').

% Found in: 10E, 9ED, ARC, HOP, MMA, ONS
card_name('festering goblin', 'Festering Goblin').
card_type('festering goblin', 'Creature — Zombie Goblin').
card_types('festering goblin', ['Creature']).
card_subtypes('festering goblin', ['Zombie', 'Goblin']).
card_colors('festering goblin', ['B']).
card_text('festering goblin', 'When Festering Goblin dies, target creature gets -1/-1 until end of turn.').
card_mana_cost('festering goblin', ['B']).
card_cmc('festering goblin', 1).
card_layout('festering goblin', 'normal').
card_power('festering goblin', 1).
card_toughness('festering goblin', 1).

% Found in: FUT
card_name('festering march', 'Festering March').
card_type('festering march', 'Sorcery').
card_types('festering march', ['Sorcery']).
card_subtypes('festering march', []).
card_colors('festering march', ['B']).
card_text('festering march', 'Creatures your opponents control get -1/-1 until end of turn. Exile Festering March with three time counters on it.\nSuspend 3—{2}{B} (Rather than cast this card from your hand, you may pay {2}{B} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_mana_cost('festering march', ['3', 'B', 'B']).
card_cmc('festering march', 5).
card_layout('festering march', 'normal').

% Found in: M14
card_name('festering newt', 'Festering Newt').
card_type('festering newt', 'Creature — Salamander').
card_types('festering newt', ['Creature']).
card_subtypes('festering newt', ['Salamander']).
card_colors('festering newt', ['B']).
card_text('festering newt', 'When Festering Newt dies, target creature an opponent controls gets -1/-1 until end of turn. That creature gets -4/-4 instead if you control a creature named Bogbrew Witch.').
card_mana_cost('festering newt', ['B']).
card_cmc('festering newt', 1).
card_layout('festering newt', 'normal').
card_power('festering newt', 1).
card_toughness('festering newt', 1).

% Found in: UDS
card_name('festering wound', 'Festering Wound').
card_type('festering wound', 'Enchantment — Aura').
card_types('festering wound', ['Enchantment']).
card_subtypes('festering wound', ['Aura']).
card_colors('festering wound', ['B']).
card_text('festering wound', 'Enchant creature\nAt the beginning of your upkeep, you may put an infection counter on Festering Wound.\nAt the beginning of the upkeep of enchanted creature\'s controller, Festering Wound deals X damage to that player, where X is the number of infection counters on Festering Wound.').
card_mana_cost('festering wound', ['1', 'B']).
card_cmc('festering wound', 2).
card_layout('festering wound', 'normal').

% Found in: DRK
card_name('festival', 'Festival').
card_type('festival', 'Instant').
card_types('festival', ['Instant']).
card_subtypes('festival', []).
card_colors('festival', ['W']).
card_text('festival', 'Cast Festival only during an opponent\'s upkeep.\nCreatures can\'t attack this turn.').
card_mana_cost('festival', ['W']).
card_cmc('festival', 1).
card_layout('festival', 'normal').

% Found in: RAV
card_name('festival of the guildpact', 'Festival of the Guildpact').
card_type('festival of the guildpact', 'Instant').
card_types('festival of the guildpact', ['Instant']).
card_subtypes('festival of the guildpact', []).
card_colors('festival of the guildpact', ['W']).
card_text('festival of the guildpact', 'Prevent the next X damage that would be dealt to you this turn.\nDraw a card.').
card_mana_cost('festival of the guildpact', ['X', 'W']).
card_cmc('festival of the guildpact', 1).
card_layout('festival of the guildpact', 'normal').

% Found in: PO2
card_name('festival of trokin', 'Festival of Trokin').
card_type('festival of trokin', 'Sorcery').
card_types('festival of trokin', ['Sorcery']).
card_subtypes('festival of trokin', []).
card_colors('festival of trokin', ['W']).
card_text('festival of trokin', 'You gain 2 life for each creature you control.').
card_mana_cost('festival of trokin', ['W']).
card_cmc('festival of trokin', 1).
card_layout('festival of trokin', 'normal').

% Found in: EVE
card_name('fetid heath', 'Fetid Heath').
card_type('fetid heath', 'Land').
card_types('fetid heath', ['Land']).
card_subtypes('fetid heath', []).
card_colors('fetid heath', []).
card_text('fetid heath', '{T}: Add {1} to your mana pool.\n{W/B}, {T}: Add {W}{W}, {W}{B}, or {B}{B} to your mana pool.').
card_layout('fetid heath', 'normal').

% Found in: MIR
card_name('fetid horror', 'Fetid Horror').
card_type('fetid horror', 'Creature — Shade Horror').
card_types('fetid horror', ['Creature']).
card_subtypes('fetid horror', ['Shade', 'Horror']).
card_colors('fetid horror', ['B']).
card_text('fetid horror', '{B}: Fetid Horror gets +1/+1 until end of turn.').
card_mana_cost('fetid horror', ['3', 'B']).
card_cmc('fetid horror', 4).
card_layout('fetid horror', 'normal').
card_power('fetid horror', 1).
card_toughness('fetid horror', 2).

% Found in: ORI
card_name('fetid imp', 'Fetid Imp').
card_type('fetid imp', 'Creature — Imp').
card_types('fetid imp', ['Creature']).
card_subtypes('fetid imp', ['Imp']).
card_colors('fetid imp', ['B']).
card_text('fetid imp', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{B}: Fetid Imp gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_mana_cost('fetid imp', ['1', 'B']).
card_cmc('fetid imp', 2).
card_layout('fetid imp', 'normal').
card_power('fetid imp', 1).
card_toughness('fetid imp', 2).

% Found in: AVR
card_name('fettergeist', 'Fettergeist').
card_type('fettergeist', 'Creature — Spirit').
card_types('fettergeist', ['Creature']).
card_subtypes('fettergeist', ['Spirit']).
card_colors('fettergeist', ['U']).
card_text('fettergeist', 'Flying\nAt the beginning of your upkeep, sacrifice Fettergeist unless you pay {1} for each other creature you control.').
card_mana_cost('fettergeist', ['2', 'U']).
card_cmc('fettergeist', 3).
card_layout('fettergeist', 'normal').
card_power('fettergeist', 3).
card_toughness('fettergeist', 4).

% Found in: MMA, MOR
card_name('feudkiller\'s verdict', 'Feudkiller\'s Verdict').
card_type('feudkiller\'s verdict', 'Tribal Sorcery — Giant').
card_types('feudkiller\'s verdict', ['Tribal', 'Sorcery']).
card_subtypes('feudkiller\'s verdict', ['Giant']).
card_colors('feudkiller\'s verdict', ['W']).
card_text('feudkiller\'s verdict', 'You gain 10 life. Then if you have more life than an opponent, put a 5/5 white Giant Warrior creature token onto the battlefield.').
card_mana_cost('feudkiller\'s verdict', ['4', 'W', 'W']).
card_cmc('feudkiller\'s verdict', 6).
card_layout('feudkiller\'s verdict', 'normal').

% Found in: ONS
card_name('fever charm', 'Fever Charm').
card_type('fever charm', 'Instant').
card_types('fever charm', ['Instant']).
card_subtypes('fever charm', []).
card_colors('fever charm', ['R']).
card_text('fever charm', 'Choose one —\n• Target creature gains haste until end of turn.\n• Target creature gets +2/+0 until end of turn.\n• Fever Charm deals 3 damage to target Wizard creature.').
card_mana_cost('fever charm', ['R']).
card_cmc('fever charm', 1).
card_layout('fever charm', 'normal').

% Found in: TMP
card_name('fevered convulsions', 'Fevered Convulsions').
card_type('fevered convulsions', 'Enchantment').
card_types('fevered convulsions', ['Enchantment']).
card_subtypes('fevered convulsions', []).
card_colors('fevered convulsions', ['B']).
card_text('fevered convulsions', '{2}{B}{B}: Put a -1/-1 counter on target creature.').
card_mana_cost('fevered convulsions', ['B', 'B']).
card_cmc('fevered convulsions', 2).
card_layout('fevered convulsions', 'normal').

% Found in: ALL, ME3
card_name('fevered strength', 'Fevered Strength').
card_type('fevered strength', 'Instant').
card_types('fevered strength', ['Instant']).
card_subtypes('fevered strength', []).
card_colors('fevered strength', ['B']).
card_text('fevered strength', 'Target creature gets +2/+0 until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('fevered strength', ['2', 'B']).
card_cmc('fevered strength', 3).
card_layout('fevered strength', 'normal').

% Found in: PCY
card_name('fickle efreet', 'Fickle Efreet').
card_type('fickle efreet', 'Creature — Efreet').
card_types('fickle efreet', ['Creature']).
card_subtypes('fickle efreet', ['Efreet']).
card_colors('fickle efreet', ['R']).
card_text('fickle efreet', 'Whenever Fickle Efreet attacks or blocks, flip a coin at end of combat. If you lose the flip, an opponent gains control of Fickle Efreet.').
card_mana_cost('fickle efreet', ['3', 'R']).
card_cmc('fickle efreet', 4).
card_layout('fickle efreet', 'normal').
card_power('fickle efreet', 5).
card_toughness('fickle efreet', 2).

% Found in: SOK
card_name('fiddlehead kami', 'Fiddlehead Kami').
card_type('fiddlehead kami', 'Creature — Spirit').
card_types('fiddlehead kami', ['Creature']).
card_subtypes('fiddlehead kami', ['Spirit']).
card_colors('fiddlehead kami', ['G']).
card_text('fiddlehead kami', 'Whenever you cast a Spirit or Arcane spell, regenerate Fiddlehead Kami.').
card_mana_cost('fiddlehead kami', ['4', 'G']).
card_cmc('fiddlehead kami', 5).
card_layout('fiddlehead kami', 'normal').
card_power('fiddlehead kami', 3).
card_toughness('fiddlehead kami', 3).

% Found in: 10E, CSP
card_name('field marshal', 'Field Marshal').
card_type('field marshal', 'Creature — Human Soldier').
card_types('field marshal', ['Creature']).
card_subtypes('field marshal', ['Human', 'Soldier']).
card_colors('field marshal', ['W']).
card_text('field marshal', 'Other Soldier creatures get +1/+1 and have first strike. (They deal combat damage before creatures without first strike.)').
card_mana_cost('field marshal', ['1', 'W', 'W']).
card_cmc('field marshal', 3).
card_layout('field marshal', 'normal').
card_power('field marshal', 2).
card_toughness('field marshal', 2).

% Found in: LEG
card_name('field of dreams', 'Field of Dreams').
card_type('field of dreams', 'World Enchantment').
card_types('field of dreams', ['Enchantment']).
card_subtypes('field of dreams', []).
card_supertypes('field of dreams', ['World']).
card_colors('field of dreams', ['U']).
card_text('field of dreams', 'Players play with the top card of their libraries revealed.').
card_mana_cost('field of dreams', ['U']).
card_cmc('field of dreams', 1).
card_layout('field of dreams', 'normal').
card_reserved('field of dreams').

% Found in: CHK
card_name('field of reality', 'Field of Reality').
card_type('field of reality', 'Enchantment — Aura').
card_types('field of reality', ['Enchantment']).
card_subtypes('field of reality', ['Aura']).
card_colors('field of reality', ['U']).
card_text('field of reality', 'Enchant creature\nEnchanted creature can\'t be blocked by Spirits.\n{1}{U}: Return Field of Reality to its owner\'s hand.').
card_mana_cost('field of reality', ['2', 'U']).
card_cmc('field of reality', 3).
card_layout('field of reality', 'normal').

% Found in: DDK, TMP, TPR
card_name('field of souls', 'Field of Souls').
card_type('field of souls', 'Enchantment').
card_types('field of souls', ['Enchantment']).
card_subtypes('field of souls', []).
card_colors('field of souls', ['W']).
card_text('field of souls', 'Whenever a nontoken creature is put into your graveyard from the battlefield, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_mana_cost('field of souls', ['2', 'W', 'W']).
card_cmc('field of souls', 4).
card_layout('field of souls', 'normal').

% Found in: UDS
card_name('field surgeon', 'Field Surgeon').
card_type('field surgeon', 'Creature — Human Cleric').
card_types('field surgeon', ['Creature']).
card_subtypes('field surgeon', ['Human', 'Cleric']).
card_colors('field surgeon', ['W']).
card_text('field surgeon', 'Tap an untapped creature you control: Prevent the next 1 damage that would be dealt to target creature this turn.').
card_mana_cost('field surgeon', ['1', 'W']).
card_cmc('field surgeon', 2).
card_layout('field surgeon', 'normal').
card_power('field surgeon', 1).
card_toughness('field surgeon', 1).

% Found in: ARB, ARC
card_name('fieldmist borderpost', 'Fieldmist Borderpost').
card_type('fieldmist borderpost', 'Artifact').
card_types('fieldmist borderpost', ['Artifact']).
card_subtypes('fieldmist borderpost', []).
card_colors('fieldmist borderpost', ['W', 'U']).
card_text('fieldmist borderpost', 'You may pay {1} and return a basic land you control to its owner\'s hand rather than pay Fieldmist Borderpost\'s mana cost.\nFieldmist Borderpost enters the battlefield tapped.\n{T}: Add {W} or {U} to your mana pool.').
card_mana_cost('fieldmist borderpost', ['1', 'W', 'U']).
card_cmc('fieldmist borderpost', 3).
card_layout('fieldmist borderpost', 'normal').

% Found in: HOP
card_name('fields of summer', 'Fields of Summer').
card_type('fields of summer', 'Plane — Moag').
card_types('fields of summer', ['Plane']).
card_subtypes('fields of summer', ['Moag']).
card_colors('fields of summer', []).
card_text('fields of summer', 'Whenever a player casts a spell, that player may gain 2 life.\nWhenever you roll {C}, you may gain 10 life.').
card_layout('fields of summer', 'plane').

% Found in: C13, DDK, ISD
card_name('fiend hunter', 'Fiend Hunter').
card_type('fiend hunter', 'Creature — Human Cleric').
card_types('fiend hunter', ['Creature']).
card_subtypes('fiend hunter', ['Human', 'Cleric']).
card_colors('fiend hunter', ['W']).
card_text('fiend hunter', 'When Fiend Hunter enters the battlefield, you may exile another target creature.\nWhen Fiend Hunter leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_mana_cost('fiend hunter', ['1', 'W', 'W']).
card_cmc('fiend hunter', 3).
card_layout('fiend hunter', 'normal').
card_power('fiend hunter', 1).
card_toughness('fiend hunter', 3).

% Found in: DKA
card_name('fiend of the shadows', 'Fiend of the Shadows').
card_type('fiend of the shadows', 'Creature — Vampire Wizard').
card_types('fiend of the shadows', ['Creature']).
card_subtypes('fiend of the shadows', ['Vampire', 'Wizard']).
card_colors('fiend of the shadows', ['B']).
card_text('fiend of the shadows', 'Flying\nWhenever Fiend of the Shadows deals combat damage to a player, that player exiles a card from his or her hand. You may play that card for as long as it remains exiled.\nSacrifice a Human: Regenerate Fiend of the Shadows.').
card_mana_cost('fiend of the shadows', ['3', 'B', 'B']).
card_cmc('fiend of the shadows', 5).
card_layout('fiend of the shadows', 'normal').
card_power('fiend of the shadows', 3).
card_toughness('fiend of the shadows', 3).

% Found in: M14
card_name('fiendslayer paladin', 'Fiendslayer Paladin').
card_type('fiendslayer paladin', 'Creature — Human Knight').
card_types('fiendslayer paladin', ['Creature']).
card_subtypes('fiendslayer paladin', ['Human', 'Knight']).
card_colors('fiendslayer paladin', ['W']).
card_text('fiendslayer paladin', 'First strike (This creature deals combat damage before creatures without first strike.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)\nFiendslayer Paladin can\'t be the target of black or red spells your opponents control.').
card_mana_cost('fiendslayer paladin', ['1', 'W', 'W']).
card_cmc('fiendslayer paladin', 3).
card_layout('fiendslayer paladin', 'normal').
card_power('fiendslayer paladin', 2).
card_toughness('fiendslayer paladin', 2).

% Found in: ARC, CMD, SCG
card_name('fierce empath', 'Fierce Empath').
card_type('fierce empath', 'Creature — Elf').
card_types('fierce empath', ['Creature']).
card_subtypes('fierce empath', ['Elf']).
card_colors('fierce empath', ['G']).
card_text('fierce empath', 'When Fierce Empath enters the battlefield, you may search your library for a creature card with converted mana cost 6 or greater, reveal it, put it into your hand, then shuffle your library.').
card_mana_cost('fierce empath', ['2', 'G']).
card_cmc('fierce empath', 3).
card_layout('fierce empath', 'normal').
card_power('fierce empath', 1).
card_toughness('fierce empath', 1).

% Found in: FRF, FRF_UGIN
card_name('fierce invocation', 'Fierce Invocation').
card_type('fierce invocation', 'Sorcery').
card_types('fierce invocation', ['Sorcery']).
card_subtypes('fierce invocation', []).
card_colors('fierce invocation', ['R']).
card_text('fierce invocation', 'Manifest the top card of your library, then put two +1/+1 counters on it. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_mana_cost('fierce invocation', ['4', 'R']).
card_cmc('fierce invocation', 5).
card_layout('fierce invocation', 'normal').

% Found in: EVE
card_name('fiery bombardment', 'Fiery Bombardment').
card_type('fiery bombardment', 'Enchantment').
card_types('fiery bombardment', ['Enchantment']).
card_subtypes('fiery bombardment', []).
card_colors('fiery bombardment', ['R']).
card_text('fiery bombardment', 'Chroma — {2}, Sacrifice a creature: Fiery Bombardment deals damage to target creature or player equal to the number of red mana symbols in the sacrificed creature\'s mana cost.').
card_mana_cost('fiery bombardment', ['1', 'R']).
card_cmc('fiery bombardment', 2).
card_layout('fiery bombardment', 'normal').

% Found in: ORI, PC2, RAV
card_name('fiery conclusion', 'Fiery Conclusion').
card_type('fiery conclusion', 'Instant').
card_types('fiery conclusion', ['Instant']).
card_subtypes('fiery conclusion', []).
card_colors('fiery conclusion', ['R']).
card_text('fiery conclusion', 'As an additional cost to cast Fiery Conclusion, sacrifice a creature.\nFiery Conclusion deals 5 damage to target creature.').
card_mana_cost('fiery conclusion', ['1', 'R']).
card_cmc('fiery conclusion', 2).
card_layout('fiery conclusion', 'normal').

% Found in: CON, DDG, DDN, MM2, MMA, PC2
card_name('fiery fall', 'Fiery Fall').
card_type('fiery fall', 'Instant').
card_types('fiery fall', ['Instant']).
card_subtypes('fiery fall', []).
card_colors('fiery fall', ['R']).
card_text('fiery fall', 'Fiery Fall deals 5 damage to target creature.\nBasic landcycling {1}{R} ({1}{R}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('fiery fall', ['5', 'R']).
card_cmc('fiery fall', 6).
card_layout('fiery fall', 'normal').

% Found in: MRD
card_name('fiery gambit', 'Fiery Gambit').
card_type('fiery gambit', 'Sorcery').
card_types('fiery gambit', ['Sorcery']).
card_subtypes('fiery gambit', []).
card_colors('fiery gambit', ['R']).
card_text('fiery gambit', 'Flip a coin until you lose a flip or choose to stop flipping. If you lose a flip, Fiery Gambit has no effect. If you win one or more flips, Fiery Gambit deals 3 damage to target creature. If you win two or more flips, Fiery Gambit deals 6 damage to each opponent. If you win three or more flips, draw nine cards and untap all lands you control.').
card_mana_cost('fiery gambit', ['2', 'R']).
card_cmc('fiery gambit', 3).
card_layout('fiery gambit', 'normal').

% Found in: DDI, M10, M11, M12, ORI
card_name('fiery hellhound', 'Fiery Hellhound').
card_type('fiery hellhound', 'Creature — Elemental Hound').
card_types('fiery hellhound', ['Creature']).
card_subtypes('fiery hellhound', ['Elemental', 'Hound']).
card_colors('fiery hellhound', ['R']).
card_text('fiery hellhound', '{R}: Fiery Hellhound gets +1/+0 until end of turn.').
card_mana_cost('fiery hellhound', ['1', 'R', 'R']).
card_cmc('fiery hellhound', 3).
card_layout('fiery hellhound', 'normal').
card_power('fiery hellhound', 2).
card_toughness('fiery hellhound', 2).

% Found in: ORI
card_name('fiery impulse', 'Fiery Impulse').
card_type('fiery impulse', 'Instant').
card_types('fiery impulse', ['Instant']).
card_subtypes('fiery impulse', []).
card_colors('fiery impulse', ['R']).
card_text('fiery impulse', 'Fiery Impulse deals 2 damage to target creature.\nSpell mastery — If there are two or more instant and/or sorcery cards in your graveyard, Fiery Impulse deals 3 damage to that creature instead.').
card_mana_cost('fiery impulse', ['R']).
card_cmc('fiery impulse', 1).
card_layout('fiery impulse', 'normal').

% Found in: C13, ICE, TSB
card_name('fiery justice', 'Fiery Justice').
card_type('fiery justice', 'Sorcery').
card_types('fiery justice', ['Sorcery']).
card_subtypes('fiery justice', []).
card_colors('fiery justice', ['W', 'R', 'G']).
card_text('fiery justice', 'Fiery Justice deals 5 damage divided as you choose among any number of target creatures and/or players. Target opponent gains 5 life.').
card_mana_cost('fiery justice', ['R', 'G', 'W']).
card_cmc('fiery justice', 3).
card_layout('fiery justice', 'normal').

% Found in: USG
card_name('fiery mantle', 'Fiery Mantle').
card_type('fiery mantle', 'Enchantment — Aura').
card_types('fiery mantle', ['Enchantment']).
card_subtypes('fiery mantle', ['Aura']).
card_colors('fiery mantle', ['R']).
card_text('fiery mantle', 'Enchant creature\n{R}: Enchanted creature gets +1/+0 until end of turn.\nWhen Fiery Mantle is put into a graveyard from the battlefield, return Fiery Mantle to its owner\'s hand.').
card_mana_cost('fiery mantle', ['1', 'R']).
card_cmc('fiery mantle', 2).
card_layout('fiery mantle', 'normal').

% Found in: TOR, TSB, pGTW
card_name('fiery temper', 'Fiery Temper').
card_type('fiery temper', 'Instant').
card_types('fiery temper', ['Instant']).
card_subtypes('fiery temper', []).
card_colors('fiery temper', ['R']).
card_text('fiery temper', 'Fiery Temper deals 3 damage to target creature or player.\nMadness {R} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('fiery temper', ['1', 'R', 'R']).
card_cmc('fiery temper', 3).
card_layout('fiery temper', 'normal').

% Found in: INV
card_name('fight or flight', 'Fight or Flight').
card_type('fight or flight', 'Enchantment').
card_types('fight or flight', ['Enchantment']).
card_subtypes('fight or flight', []).
card_colors('fight or flight', ['W']).
card_text('fight or flight', 'At the beginning of combat on each opponent\'s turn, separate all creatures that player controls into two piles. Only creatures in the pile of his or her choice can attack this turn.').
card_mana_cost('fight or flight', ['3', 'W']).
card_cmc('fight or flight', 4).
card_layout('fight or flight', 'normal').

% Found in: ARB
card_name('fight to the death', 'Fight to the Death').
card_type('fight to the death', 'Instant').
card_types('fight to the death', ['Instant']).
card_subtypes('fight to the death', []).
card_colors('fight to the death', ['W', 'R']).
card_text('fight to the death', 'Destroy all blocking creatures and all blocked creatures.').
card_mana_cost('fight to the death', ['R', 'W']).
card_cmc('fight to the death', 2).
card_layout('fight to the death', 'normal').

% Found in: EXO
card_name('fighting chance', 'Fighting Chance').
card_type('fighting chance', 'Instant').
card_types('fighting chance', ['Instant']).
card_subtypes('fighting chance', []).
card_colors('fighting chance', ['R']).
card_text('fighting chance', 'For each blocking creature, flip a coin. If you win the flip, prevent all combat damage that would be dealt by that creature this turn.').
card_mana_cost('fighting chance', ['R']).
card_cmc('fighting chance', 1).
card_layout('fighting chance', 'normal').

% Found in: 7ED, 8ED, TMP, TPR
card_name('fighting drake', 'Fighting Drake').
card_type('fighting drake', 'Creature — Drake').
card_types('fighting drake', ['Creature']).
card_subtypes('fighting drake', ['Drake']).
card_colors('fighting drake', ['U']).
card_text('fighting drake', 'Flying').
card_mana_cost('fighting drake', ['2', 'U', 'U']).
card_cmc('fighting drake', 4).
card_layout('fighting drake', 'normal').
card_power('fighting drake', 2).
card_toughness('fighting drake', 4).

% Found in: DDL, EVE, MMA, PD2, pLPA
card_name('figure of destiny', 'Figure of Destiny').
card_type('figure of destiny', 'Creature — Kithkin').
card_types('figure of destiny', ['Creature']).
card_subtypes('figure of destiny', ['Kithkin']).
card_colors('figure of destiny', ['W', 'R']).
card_text('figure of destiny', '{R/W}: Figure of Destiny becomes a Kithkin Spirit with base power and toughness 2/2.\n{R/W}{R/W}{R/W}: If Figure of Destiny is a Spirit, it becomes a Kithkin Spirit Warrior with base power and toughness 4/4.\n{R/W}{R/W}{R/W}{R/W}{R/W}{R/W}: If Figure of Destiny is a Warrior, it becomes a Kithkin Spirit Warrior Avatar with base power and toughness 8/8, flying, and first strike.').
card_mana_cost('figure of destiny', ['R/W']).
card_cmc('figure of destiny', 1).
card_layout('figure of destiny', 'normal').
card_power('figure of destiny', 1).
card_toughness('figure of destiny', 1).

% Found in: VAN
card_name('figure of destiny avatar', 'Figure of Destiny Avatar').
card_type('figure of destiny avatar', 'Vanguard').
card_types('figure of destiny avatar', ['Vanguard']).
card_subtypes('figure of destiny avatar', []).
card_colors('figure of destiny avatar', []).
card_text('figure of destiny avatar', '{X}: Put a +1/+1 counter on target creature with fewer than X +1/+1 counters on it.').
card_layout('figure of destiny avatar', 'vanguard').

% Found in: ARB, C13
card_name('filigree angel', 'Filigree Angel').
card_type('filigree angel', 'Artifact Creature — Angel').
card_types('filigree angel', ['Artifact', 'Creature']).
card_subtypes('filigree angel', ['Angel']).
card_colors('filigree angel', ['W', 'U']).
card_text('filigree angel', 'Flying\nWhen Filigree Angel enters the battlefield, you gain 3 life for each artifact you control.').
card_mana_cost('filigree angel', ['5', 'W', 'W', 'U']).
card_cmc('filigree angel', 8).
card_layout('filigree angel', 'normal').
card_power('filigree angel', 4).
card_toughness('filigree angel', 4).

% Found in: CON
card_name('filigree fracture', 'Filigree Fracture').
card_type('filigree fracture', 'Instant').
card_types('filigree fracture', ['Instant']).
card_subtypes('filigree fracture', []).
card_colors('filigree fracture', ['G']).
card_text('filigree fracture', 'Destroy target artifact or enchantment. If that permanent was blue or black, draw a card.').
card_mana_cost('filigree fracture', ['2', 'G']).
card_cmc('filigree fracture', 3).
card_layout('filigree fracture', 'normal').

% Found in: ALA
card_name('filigree sages', 'Filigree Sages').
card_type('filigree sages', 'Artifact Creature — Vedalken Wizard').
card_types('filigree sages', ['Artifact', 'Creature']).
card_subtypes('filigree sages', ['Vedalken', 'Wizard']).
card_colors('filigree sages', ['U']).
card_text('filigree sages', '{2}{U}: Untap target artifact.').
card_mana_cost('filigree sages', ['3', 'U']).
card_cmc('filigree sages', 4).
card_layout('filigree sages', 'normal').
card_power('filigree sages', 2).
card_toughness('filigree sages', 3).

% Found in: 5DN
card_name('fill with fright', 'Fill with Fright').
card_type('fill with fright', 'Sorcery').
card_types('fill with fright', ['Sorcery']).
card_subtypes('fill with fright', []).
card_colors('fill with fright', ['B']).
card_text('fill with fright', 'Target player discards two cards. Scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('fill with fright', ['3', 'B']).
card_cmc('fill with fright', 4).
card_layout('fill with fright', 'normal').

% Found in: JUD
card_name('filth', 'Filth').
card_type('filth', 'Creature — Incarnation').
card_types('filth', ['Creature']).
card_subtypes('filth', ['Incarnation']).
card_colors('filth', ['B']).
card_text('filth', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)\nAs long as Filth is in your graveyard and you control a Swamp, creatures you control have swampwalk.').
card_mana_cost('filth', ['3', 'B']).
card_cmc('filth', 4).
card_layout('filth', 'normal').
card_power('filth', 2).
card_toughness('filth', 2).

% Found in: ODY
card_name('filthy cur', 'Filthy Cur').
card_type('filthy cur', 'Creature — Hound').
card_types('filthy cur', ['Creature']).
card_subtypes('filthy cur', ['Hound']).
card_colors('filthy cur', ['B']).
card_text('filthy cur', 'Whenever Filthy Cur is dealt damage, you lose that much life.').
card_mana_cost('filthy cur', ['1', 'B']).
card_cmc('filthy cur', 2).
card_layout('filthy cur', 'normal').
card_power('filthy cur', 2).
card_toughness('filthy cur', 2).

% Found in: 6ED, 7ED, MIR
card_name('final fortune', 'Final Fortune').
card_type('final fortune', 'Instant').
card_types('final fortune', ['Instant']).
card_subtypes('final fortune', []).
card_colors('final fortune', ['R']).
card_text('final fortune', 'Take an extra turn after this one. At the beginning of that turn\'s end step, you lose the game.').
card_mana_cost('final fortune', ['R', 'R']).
card_cmc('final fortune', 2).
card_layout('final fortune', 'normal').

% Found in: BOK
card_name('final judgment', 'Final Judgment').
card_type('final judgment', 'Sorcery').
card_types('final judgment', ['Sorcery']).
card_subtypes('final judgment', []).
card_colors('final judgment', ['W']).
card_text('final judgment', 'Exile all creatures.').
card_mana_cost('final judgment', ['4', 'W', 'W']).
card_cmc('final judgment', 6).
card_layout('final judgment', 'normal').

% Found in: 9ED, SCG
card_name('final punishment', 'Final Punishment').
card_type('final punishment', 'Sorcery').
card_types('final punishment', ['Sorcery']).
card_subtypes('final punishment', []).
card_colors('final punishment', ['B']).
card_text('final punishment', 'Target player loses life equal to the damage already dealt to him or her this turn.').
card_mana_cost('final punishment', ['3', 'B', 'B']).
card_cmc('final punishment', 5).
card_layout('final punishment', 'normal').

% Found in: LRW
card_name('final revels', 'Final Revels').
card_type('final revels', 'Sorcery').
card_types('final revels', ['Sorcery']).
card_subtypes('final revels', []).
card_colors('final revels', ['B']).
card_text('final revels', 'Choose one —\n• All creatures get +2/+0 until end of turn.\n• All creatures get -0/-2 until end of turn.').
card_mana_cost('final revels', ['4', 'B']).
card_cmc('final revels', 5).
card_layout('final revels', 'normal').

% Found in: POR
card_name('final strike', 'Final Strike').
card_type('final strike', 'Sorcery').
card_types('final strike', ['Sorcery']).
card_subtypes('final strike', []).
card_colors('final strike', ['B']).
card_text('final strike', 'As an additional cost to cast Final Strike, sacrifice a creature.\nFinal Strike deals damage to target opponent equal to the sacrificed creature\'s power.').
card_mana_cost('final strike', ['2', 'B', 'B']).
card_cmc('final strike', 4).
card_layout('final strike', 'normal').

% Found in: MOR
card_name('final-sting faerie', 'Final-Sting Faerie').
card_type('final-sting faerie', 'Creature — Faerie Assassin').
card_types('final-sting faerie', ['Creature']).
card_subtypes('final-sting faerie', ['Faerie', 'Assassin']).
card_colors('final-sting faerie', ['B']).
card_text('final-sting faerie', 'Flying\nWhen Final-Sting Faerie enters the battlefield, destroy target creature that was dealt damage this turn.').
card_mana_cost('final-sting faerie', ['3', 'B']).
card_cmc('final-sting faerie', 4).
card_layout('final-sting faerie', 'normal').
card_power('final-sting faerie', 2).
card_toughness('final-sting faerie', 2).

% Found in: ARB
card_name('finest hour', 'Finest Hour').
card_type('finest hour', 'Enchantment').
card_types('finest hour', ['Enchantment']).
card_subtypes('finest hour', []).
card_colors('finest hour', ['W', 'U', 'G']).
card_text('finest hour', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever a creature you control attacks alone, if it\'s the first combat phase of the turn, untap that creature. After this phase, there is an additional combat phase.').
card_mana_cost('finest hour', ['2', 'G', 'W', 'U']).
card_cmc('finest hour', 5).
card_layout('finest hour', 'normal').

% Found in: APC, CMD, DDJ, pFNM
card_name('fire', 'Fire').
card_type('fire', 'Instant').
card_types('fire', ['Instant']).
card_subtypes('fire', []).
card_colors('fire', ['R']).
card_text('fire', 'Fire deals 2 damage divided as you choose among one or two target creatures and/or players.').
card_mana_cost('fire', ['1', 'R']).
card_cmc('fire', 2).
card_layout('fire', 'split').
card_sides('fire', 'ice').

% Found in: ME3, PTK
card_name('fire ambush', 'Fire Ambush').
card_type('fire ambush', 'Sorcery').
card_types('fire ambush', ['Sorcery']).
card_subtypes('fire ambush', []).
card_colors('fire ambush', ['R']).
card_text('fire ambush', 'Fire Ambush deals 3 damage to target creature or player.').
card_mana_cost('fire ambush', ['1', 'R']).
card_cmc('fire ambush', 2).
card_layout('fire ambush', 'normal').

% Found in: DRK
card_name('fire and brimstone', 'Fire and Brimstone').
card_type('fire and brimstone', 'Instant').
card_types('fire and brimstone', ['Instant']).
card_subtypes('fire and brimstone', []).
card_colors('fire and brimstone', ['W']).
card_text('fire and brimstone', 'Fire and Brimstone deals 4 damage to target player who declared an attacking creature this turn and 4 damage to you.').
card_mana_cost('fire and brimstone', ['3', 'W', 'W']).
card_cmc('fire and brimstone', 5).
card_layout('fire and brimstone', 'normal').

% Found in: BRB, USG
card_name('fire ants', 'Fire Ants').
card_type('fire ants', 'Creature — Insect').
card_types('fire ants', ['Creature']).
card_subtypes('fire ants', ['Insect']).
card_colors('fire ants', ['R']).
card_text('fire ants', '{T}: Fire Ants deals 1 damage to each other creature without flying.').
card_mana_cost('fire ants', ['2', 'R']).
card_cmc('fire ants', 3).
card_layout('fire ants', 'normal').
card_power('fire ants', 2).
card_toughness('fire ants', 1).

% Found in: EVE
card_name('fire at will', 'Fire at Will').
card_type('fire at will', 'Instant').
card_types('fire at will', ['Instant']).
card_subtypes('fire at will', []).
card_colors('fire at will', ['W', 'R']).
card_text('fire at will', 'Fire at Will deals 3 damage divided as you choose among one, two, or three target attacking or blocking creatures.').
card_mana_cost('fire at will', ['R/W', 'R/W', 'R/W']).
card_cmc('fire at will', 3).
card_layout('fire at will', 'normal').

% Found in: PTK
card_name('fire bowman', 'Fire Bowman').
card_type('fire bowman', 'Creature — Human Soldier Archer').
card_types('fire bowman', ['Creature']).
card_subtypes('fire bowman', ['Human', 'Soldier', 'Archer']).
card_colors('fire bowman', ['R']).
card_text('fire bowman', 'Sacrifice Fire Bowman: Fire Bowman deals 1 damage to target creature or player. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('fire bowman', ['R']).
card_cmc('fire bowman', 1).
card_layout('fire bowman', 'normal').
card_power('fire bowman', 1).
card_toughness('fire bowman', 1).

% Found in: ICE, MED
card_name('fire covenant', 'Fire Covenant').
card_type('fire covenant', 'Instant').
card_types('fire covenant', ['Instant']).
card_subtypes('fire covenant', []).
card_colors('fire covenant', ['B', 'R']).
card_text('fire covenant', 'As an additional cost to cast Fire Covenant, pay X life.\nFire Covenant deals X damage divided as you choose among any number of target creatures.').
card_mana_cost('fire covenant', ['1', 'B', 'R']).
card_cmc('fire covenant', 3).
card_layout('fire covenant', 'normal').

% Found in: 6ED, 7ED, C14, MIR
card_name('fire diamond', 'Fire Diamond').
card_type('fire diamond', 'Artifact').
card_types('fire diamond', ['Artifact']).
card_subtypes('fire diamond', []).
card_colors('fire diamond', []).
card_text('fire diamond', 'Fire Diamond enters the battlefield tapped.\n{T}: Add {R} to your mana pool.').
card_mana_cost('fire diamond', ['2']).
card_cmc('fire diamond', 2).
card_layout('fire diamond', 'normal').

% Found in: ME2, POR
card_name('fire dragon', 'Fire Dragon').
card_type('fire dragon', 'Creature — Dragon').
card_types('fire dragon', ['Creature']).
card_subtypes('fire dragon', ['Dragon']).
card_colors('fire dragon', ['R']).
card_text('fire dragon', 'Flying\nWhen Fire Dragon enters the battlefield, it deals damage to target creature equal to the number of Mountains you control.').
card_mana_cost('fire dragon', ['6', 'R', 'R', 'R']).
card_cmc('fire dragon', 9).
card_layout('fire dragon', 'normal').
card_power('fire dragon', 6).
card_toughness('fire dragon', 6).

% Found in: 5ED, CHR, DRK, ME3
card_name('fire drake', 'Fire Drake').
card_type('fire drake', 'Creature — Drake').
card_types('fire drake', ['Creature']).
card_subtypes('fire drake', ['Drake']).
card_colors('fire drake', ['R']).
card_text('fire drake', 'Flying\n{R}: Fire Drake gets +1/+0 until end of turn. Activate this ability only once each turn.').
card_mana_cost('fire drake', ['1', 'R', 'R']).
card_cmc('fire drake', 3).
card_layout('fire drake', 'normal').
card_power('fire drake', 1).
card_toughness('fire drake', 2).

% Found in: 2ED, 3ED, 4ED, 6ED, 7ED, CED, CEI, LEA, LEB, M13, S99
card_name('fire elemental', 'Fire Elemental').
card_type('fire elemental', 'Creature — Elemental').
card_types('fire elemental', ['Creature']).
card_subtypes('fire elemental', ['Elemental']).
card_colors('fire elemental', ['R']).
card_text('fire elemental', '').
card_mana_cost('fire elemental', ['3', 'R', 'R']).
card_cmc('fire elemental', 5).
card_layout('fire elemental', 'normal').
card_power('fire elemental', 5).
card_toughness('fire elemental', 4).

% Found in: ME4, POR
card_name('fire imp', 'Fire Imp').
card_type('fire imp', 'Creature — Imp').
card_types('fire imp', ['Creature']).
card_subtypes('fire imp', ['Imp']).
card_colors('fire imp', ['R']).
card_text('fire imp', 'When Fire Imp enters the battlefield, it deals 2 damage to target creature.').
card_mana_cost('fire imp', ['2', 'R']).
card_cmc('fire imp', 3).
card_layout('fire imp', 'normal').
card_power('fire imp', 2).
card_toughness('fire imp', 1).

% Found in: MOR
card_name('fire juggler', 'Fire Juggler').
card_type('fire juggler', 'Creature — Goblin Shaman').
card_types('fire juggler', ['Creature']).
card_subtypes('fire juggler', ['Goblin', 'Shaman']).
card_colors('fire juggler', ['R']).
card_text('fire juggler', 'Whenever Fire Juggler becomes blocked, clash with an opponent. If you win, Fire Juggler deals 4 damage to each creature blocking it. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('fire juggler', ['2', 'R']).
card_cmc('fire juggler', 3).
card_layout('fire juggler', 'normal').
card_power('fire juggler', 2).
card_toughness('fire juggler', 2).

% Found in: M11, PD2
card_name('fire servant', 'Fire Servant').
card_type('fire servant', 'Creature — Elemental').
card_types('fire servant', ['Creature']).
card_subtypes('fire servant', ['Elemental']).
card_colors('fire servant', ['R']).
card_text('fire servant', 'If a red instant or sorcery spell you control would deal damage, it deals double that damage instead.').
card_mana_cost('fire servant', ['3', 'R', 'R']).
card_cmc('fire servant', 5).
card_layout('fire servant', 'normal').
card_power('fire servant', 4).
card_toughness('fire servant', 3).

% Found in: POR
card_name('fire snake', 'Fire Snake').
card_type('fire snake', 'Creature — Snake').
card_types('fire snake', ['Creature']).
card_subtypes('fire snake', ['Snake']).
card_colors('fire snake', ['R']).
card_text('fire snake', 'When Fire Snake dies, destroy target land.').
card_mana_cost('fire snake', ['4', 'R']).
card_cmc('fire snake', 5).
card_layout('fire snake', 'normal').
card_power('fire snake', 3).
card_toughness('fire snake', 1).

% Found in: LEG, ME3
card_name('fire sprites', 'Fire Sprites').
card_type('fire sprites', 'Creature — Faerie').
card_types('fire sprites', ['Creature']).
card_subtypes('fire sprites', ['Faerie']).
card_colors('fire sprites', ['G']).
card_text('fire sprites', 'Flying\n{G}, {T}: Add {R} to your mana pool.').
card_mana_cost('fire sprites', ['1', 'G']).
card_cmc('fire sprites', 2).
card_layout('fire sprites', 'normal').
card_power('fire sprites', 1).
card_toughness('fire sprites', 1).

% Found in: ME4, POR, S99
card_name('fire tempest', 'Fire Tempest').
card_type('fire tempest', 'Sorcery').
card_types('fire tempest', ['Sorcery']).
card_subtypes('fire tempest', []).
card_colors('fire tempest', ['R']).
card_text('fire tempest', 'Fire Tempest deals 6 damage to each creature and each player.').
card_mana_cost('fire tempest', ['5', 'R', 'R']).
card_cmc('fire tempest', 7).
card_layout('fire tempest', 'normal').

% Found in: TSB, WTH
card_name('fire whip', 'Fire Whip').
card_type('fire whip', 'Enchantment — Aura').
card_types('fire whip', ['Enchantment']).
card_subtypes('fire whip', ['Aura']).
card_colors('fire whip', ['R']).
card_text('fire whip', 'Enchant creature you control\nEnchanted creature has \"{T}: This creature deals 1 damage to target creature or player.\"\nSacrifice Fire Whip: Fire Whip deals 1 damage to target creature or player.').
card_mana_cost('fire whip', ['1', 'R']).
card_cmc('fire whip', 2).
card_layout('fire whip', 'normal').

% Found in: DDG, LRW
card_name('fire-belly changeling', 'Fire-Belly Changeling').
card_type('fire-belly changeling', 'Creature — Shapeshifter').
card_types('fire-belly changeling', ['Creature']).
card_subtypes('fire-belly changeling', ['Shapeshifter']).
card_colors('fire-belly changeling', ['R']).
card_text('fire-belly changeling', 'Changeling (This card is every creature type.)\n{R}: Fire-Belly Changeling gets +1/+0 until end of turn. Activate this ability no more than twice each turn.').
card_mana_cost('fire-belly changeling', ['1', 'R']).
card_cmc('fire-belly changeling', 2).
card_layout('fire-belly changeling', 'normal').
card_power('fire-belly changeling', 1).
card_toughness('fire-belly changeling', 1).

% Found in: ALA, DDH
card_name('fire-field ogre', 'Fire-Field Ogre').
card_type('fire-field ogre', 'Creature — Ogre Mutant').
card_types('fire-field ogre', ['Creature']).
card_subtypes('fire-field ogre', ['Ogre', 'Mutant']).
card_colors('fire-field ogre', ['U', 'B', 'R']).
card_text('fire-field ogre', 'First strike\nUnearth {U}{B}{R} ({U}{B}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('fire-field ogre', ['1', 'U', 'B', 'R']).
card_cmc('fire-field ogre', 4).
card_layout('fire-field ogre', 'normal').
card_power('fire-field ogre', 4).
card_toughness('fire-field ogre', 2).

% Found in: SHM
card_name('fire-lit thicket', 'Fire-Lit Thicket').
card_type('fire-lit thicket', 'Land').
card_types('fire-lit thicket', ['Land']).
card_subtypes('fire-lit thicket', []).
card_colors('fire-lit thicket', []).
card_text('fire-lit thicket', '{T}: Add {1} to your mana pool.\n{R/G}, {T}: Add {R}{R}, {R}{G}, or {G}{G} to your mana pool.').
card_layout('fire-lit thicket', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, ARC, ATH, BTD, C13, CED, CEI, DD2, DD3_JVC, DST, ITP, LEA, LEB, M10, M11, M12, ME4, PD2, RQS, pARL, pMEI, pMPR
card_name('fireball', 'Fireball').
card_type('fireball', 'Sorcery').
card_types('fireball', ['Sorcery']).
card_subtypes('fireball', []).
card_colors('fireball', ['R']).
card_text('fireball', 'Fireball deals X damage divided evenly, rounded down, among any number of target creatures and/or players.\nFireball costs {1} more to cast for each target beyond the first.').
card_mana_cost('fireball', ['X', 'R']).
card_cmc('fireball', 1).
card_layout('fireball', 'normal').

% Found in: DD2, DD3_JVC, PD2, VIS, VMA, pFNM
card_name('fireblast', 'Fireblast').
card_type('fireblast', 'Instant').
card_types('fireblast', ['Instant']).
card_subtypes('fireblast', []).
card_colors('fireblast', ['R']).
card_text('fireblast', 'You may sacrifice two Mountains rather than pay Fireblast\'s mana cost.\nFireblast deals 4 damage to target creature or player.').
card_mana_cost('fireblast', ['4', 'R', 'R']).
card_cmc('fireblast', 6).
card_layout('fireblast', 'normal').

% Found in: DD2, DD3_JVC, ODY, pFNM
card_name('firebolt', 'Firebolt').
card_type('firebolt', 'Sorcery').
card_types('firebolt', ['Sorcery']).
card_subtypes('firebolt', []).
card_colors('firebolt', ['R']).
card_text('firebolt', 'Firebolt deals 2 damage to target creature or player.\nFlashback {4}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('firebolt', ['R']).
card_cmc('firebolt', 1).
card_layout('firebolt', 'normal').

% Found in: INV
card_name('firebrand ranger', 'Firebrand Ranger').
card_type('firebrand ranger', 'Creature — Human Soldier').
card_types('firebrand ranger', ['Creature']).
card_subtypes('firebrand ranger', ['Human', 'Soldier']).
card_colors('firebrand ranger', ['R']).
card_text('firebrand ranger', '{G}, {T}: You may put a basic land card from your hand onto the battlefield.').
card_mana_cost('firebrand ranger', ['1', 'R']).
card_cmc('firebrand ranger', 2).
card_layout('firebrand ranger', 'normal').
card_power('firebrand ranger', 2).
card_toughness('firebrand ranger', 1).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 9ED, CED, CEI, LEA, LEB, M10, M12, MIR
card_name('firebreathing', 'Firebreathing').
card_type('firebreathing', 'Enchantment — Aura').
card_types('firebreathing', ['Enchantment']).
card_subtypes('firebreathing', ['Aura']).
card_colors('firebreathing', ['R']).
card_text('firebreathing', 'Enchant creature\n{R}: Enchanted creature gets +1/+0 until end of turn.').
card_mana_cost('firebreathing', ['R']).
card_cmc('firebreathing', 1).
card_layout('firebreathing', 'normal').

% Found in: JUD
card_name('firecat blitz', 'Firecat Blitz').
card_type('firecat blitz', 'Sorcery').
card_types('firecat blitz', ['Sorcery']).
card_subtypes('firecat blitz', []).
card_colors('firecat blitz', ['R']).
card_text('firecat blitz', 'Put X 1/1 red Elemental Cat creature tokens with haste onto the battlefield. Exile them at the beginning of the next end step.\nFlashback—{R}{R}, Sacrifice X Mountains. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('firecat blitz', ['X', 'R', 'R']).
card_cmc('firecat blitz', 2).
card_layout('firecat blitz', 'normal').

% Found in: THS
card_name('firedrinker satyr', 'Firedrinker Satyr').
card_type('firedrinker satyr', 'Creature — Satyr Shaman').
card_types('firedrinker satyr', ['Creature']).
card_subtypes('firedrinker satyr', ['Satyr', 'Shaman']).
card_colors('firedrinker satyr', ['R']).
card_text('firedrinker satyr', 'Whenever Firedrinker Satyr is dealt damage, it deals that much damage to you.\n{1}{R}: Firedrinker Satyr gets +1/+0 until end of turn and deals 1 damage to you.').
card_mana_cost('firedrinker satyr', ['R']).
card_cmc('firedrinker satyr', 1).
card_layout('firedrinker satyr', 'normal').
card_power('firedrinker satyr', 2).
card_toughness('firedrinker satyr', 1).

% Found in: ORI
card_name('firefiend elemental', 'Firefiend Elemental').
card_type('firefiend elemental', 'Creature — Elemental').
card_types('firefiend elemental', ['Creature']).
card_subtypes('firefiend elemental', ['Elemental']).
card_colors('firefiend elemental', ['R']).
card_text('firefiend elemental', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\nRenown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)').
card_mana_cost('firefiend elemental', ['3', 'R']).
card_cmc('firefiend elemental', 4).
card_layout('firefiend elemental', 'normal').
card_power('firefiend elemental', 3).
card_toughness('firefiend elemental', 2).

% Found in: GTC
card_name('firefist striker', 'Firefist Striker').
card_type('firefist striker', 'Creature — Human Soldier').
card_types('firefist striker', ['Creature']).
card_subtypes('firefist striker', ['Human', 'Soldier']).
card_colors('firefist striker', ['R']).
card_text('firefist striker', 'Battalion — Whenever Firefist Striker and at least two other creatures attack, target creature can\'t block this turn.').
card_mana_cost('firefist striker', ['1', 'R']).
card_cmc('firefist striker', 2).
card_layout('firefist striker', 'normal').
card_power('firefist striker', 2).
card_toughness('firefist striker', 1).

% Found in: TMP
card_name('firefly', 'Firefly').
card_type('firefly', 'Creature — Insect').
card_types('firefly', ['Creature']).
card_subtypes('firefly', ['Insect']).
card_colors('firefly', ['R']).
card_text('firefly', 'Flying\n{R}: Firefly gets +1/+0 until end of turn.').
card_mana_cost('firefly', ['3', 'R']).
card_cmc('firefly', 4).
card_layout('firefly', 'normal').
card_power('firefly', 1).
card_toughness('firefly', 1).

% Found in: PLC
card_name('firefright mage', 'Firefright Mage').
card_type('firefright mage', 'Creature — Goblin Spellshaper').
card_types('firefright mage', ['Creature']).
card_subtypes('firefright mage', ['Goblin', 'Spellshaper']).
card_colors('firefright mage', ['R']).
card_text('firefright mage', '{1}{R}, {T}, Discard a card: Target creature can\'t be blocked this turn except by artifact creatures and/or red creatures.').
card_mana_cost('firefright mage', ['R']).
card_cmc('firefright mage', 1).
card_layout('firefright mage', 'normal').
card_power('firefright mage', 1).
card_toughness('firefright mage', 1).

% Found in: KTK
card_name('firehoof cavalry', 'Firehoof Cavalry').
card_type('firehoof cavalry', 'Creature — Human Berserker').
card_types('firehoof cavalry', ['Creature']).
card_subtypes('firehoof cavalry', ['Human', 'Berserker']).
card_colors('firehoof cavalry', ['W']).
card_text('firehoof cavalry', '{3}{R}: Firehoof Cavalry gets +2/+0 and gains trample until end of turn.').
card_mana_cost('firehoof cavalry', ['W']).
card_cmc('firehoof cavalry', 1).
card_layout('firehoof cavalry', 'normal').
card_power('firehoof cavalry', 1).
card_toughness('firehoof cavalry', 1).

% Found in: DDH, RAV
card_name('firemane angel', 'Firemane Angel').
card_type('firemane angel', 'Creature — Angel').
card_types('firemane angel', ['Creature']).
card_subtypes('firemane angel', ['Angel']).
card_colors('firemane angel', ['W', 'R']).
card_text('firemane angel', 'Flying, first strike\nAt the beginning of your upkeep, if Firemane Angel is in your graveyard or on the battlefield, you may gain 1 life.\n{6}{R}{R}{W}{W}: Return Firemane Angel from your graveyard to the battlefield. Activate this ability only during your upkeep.').
card_mana_cost('firemane angel', ['3', 'R', 'W', 'W']).
card_cmc('firemane angel', 6).
card_layout('firemane angel', 'normal').
card_power('firemane angel', 4).
card_toughness('firemane angel', 3).

% Found in: GTC, pMGD
card_name('firemane avenger', 'Firemane Avenger').
card_type('firemane avenger', 'Creature — Angel').
card_types('firemane avenger', ['Creature']).
card_subtypes('firemane avenger', ['Angel']).
card_colors('firemane avenger', ['W', 'R']).
card_text('firemane avenger', 'Flying\nBattalion — Whenever Firemane Avenger and at least two other creatures attack, Firemane Avenger deals 3 damage to target creature or player and you gain 3 life.').
card_mana_cost('firemane avenger', ['2', 'R', 'W']).
card_cmc('firemane avenger', 4).
card_layout('firemane avenger', 'normal').
card_power('firemane avenger', 3).
card_toughness('firemane avenger', 3).

% Found in: BFZ
card_name('firemantle mage', 'Firemantle Mage').
card_type('firemantle mage', 'Creature — Human Shaman Ally').
card_types('firemantle mage', ['Creature']).
card_subtypes('firemantle mage', ['Human', 'Shaman', 'Ally']).
card_colors('firemantle mage', ['R']).
card_text('firemantle mage', 'Rally — Whenever Firemantle Mage or another Ally enters the battlefield under your control, creatures you control gain menace until end of turn. (A creature with menace can\'t be blocked except by two or more creatures.)').
card_mana_cost('firemantle mage', ['2', 'R']).
card_cmc('firemantle mage', 3).
card_layout('firemantle mage', 'normal').
card_power('firemantle mage', 2).
card_toughness('firemantle mage', 2).

% Found in: TSP
card_name('firemaw kavu', 'Firemaw Kavu').
card_type('firemaw kavu', 'Creature — Kavu').
card_types('firemaw kavu', ['Creature']).
card_subtypes('firemaw kavu', ['Kavu']).
card_colors('firemaw kavu', ['R']).
card_text('firemaw kavu', 'Echo {5}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Firemaw Kavu enters the battlefield, it deals 2 damage to target creature.\nWhen Firemaw Kavu leaves the battlefield, it deals 4 damage to target creature.').
card_mana_cost('firemaw kavu', ['5', 'R']).
card_cmc('firemaw kavu', 6).
card_layout('firemaw kavu', 'normal').
card_power('firemaw kavu', 4).
card_toughness('firemaw kavu', 2).

% Found in: RTR
card_name('firemind\'s foresight', 'Firemind\'s Foresight').
card_type('firemind\'s foresight', 'Instant').
card_types('firemind\'s foresight', ['Instant']).
card_subtypes('firemind\'s foresight', []).
card_colors('firemind\'s foresight', ['U', 'R']).
card_text('firemind\'s foresight', 'Search your library for an instant card with converted mana cost 3, reveal it, and put it into your hand. Then repeat this process for instant cards with converted mana costs 2 and 1. Then shuffle your library.').
card_mana_cost('firemind\'s foresight', ['5', 'U', 'R']).
card_cmc('firemind\'s foresight', 7).
card_layout('firemind\'s foresight', 'normal').

% Found in: DKA
card_name('fires of undeath', 'Fires of Undeath').
card_type('fires of undeath', 'Instant').
card_types('fires of undeath', ['Instant']).
card_subtypes('fires of undeath', []).
card_colors('fires of undeath', ['R']).
card_text('fires of undeath', 'Fires of Undeath deals 2 damage to target creature or player.\nFlashback {5}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('fires of undeath', ['2', 'R']).
card_cmc('fires of undeath', 3).
card_layout('fires of undeath', 'normal').

% Found in: ARC, C13, CNS, DDL, HOP, INV, PC2, VMA
card_name('fires of yavimaya', 'Fires of Yavimaya').
card_type('fires of yavimaya', 'Enchantment').
card_types('fires of yavimaya', ['Enchantment']).
card_subtypes('fires of yavimaya', []).
card_colors('fires of yavimaya', ['R', 'G']).
card_text('fires of yavimaya', 'Creatures you control have haste.\nSacrifice Fires of Yavimaya: Target creature gets +2/+2 until end of turn.').
card_mana_cost('fires of yavimaya', ['1', 'R', 'G']).
card_cmc('fires of yavimaya', 3).
card_layout('fires of yavimaya', 'normal').

% Found in: INV
card_name('firescreamer', 'Firescreamer').
card_type('firescreamer', 'Creature — Kavu').
card_types('firescreamer', ['Creature']).
card_subtypes('firescreamer', ['Kavu']).
card_colors('firescreamer', ['B']).
card_text('firescreamer', '{R}: Firescreamer gets +1/+0 until end of turn.').
card_mana_cost('firescreamer', ['3', 'B']).
card_cmc('firescreamer', 4).
card_layout('firescreamer', 'normal').
card_power('firescreamer', 2).
card_toughness('firescreamer', 2).

% Found in: CNS, M14, MRD
card_name('fireshrieker', 'Fireshrieker').
card_type('fireshrieker', 'Artifact — Equipment').
card_types('fireshrieker', ['Artifact']).
card_subtypes('fireshrieker', ['Equipment']).
card_colors('fireshrieker', []).
card_text('fireshrieker', 'Equipped creature has double strike. (It deals both first-strike and regular combat damage.)\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('fireshrieker', ['3']).
card_cmc('fireshrieker', 3).
card_layout('fireshrieker', 'normal').

% Found in: DD2, DD3_JVC, TMP, pFNM
card_name('fireslinger', 'Fireslinger').
card_type('fireslinger', 'Creature — Human Wizard').
card_types('fireslinger', ['Creature']).
card_subtypes('fireslinger', ['Human', 'Wizard']).
card_colors('fireslinger', ['R']).
card_text('fireslinger', '{T}: Fireslinger deals 1 damage to target creature or player and 1 damage to you.').
card_mana_cost('fireslinger', ['1', 'R']).
card_cmc('fireslinger', 2).
card_layout('fireslinger', 'normal').
card_power('fireslinger', 1).
card_toughness('fireslinger', 1).

% Found in: CMD, SHM, V14
card_name('firespout', 'Firespout').
card_type('firespout', 'Sorcery').
card_types('firespout', ['Sorcery']).
card_subtypes('firespout', []).
card_colors('firespout', ['R', 'G']).
card_text('firespout', 'Firespout deals 3 damage to each creature without flying if {R} was spent to cast Firespout and 3 damage to each creature with flying if {G} was spent to cast it. (Do both if {R}{G} was spent.)').
card_mana_cost('firespout', ['2', 'R/G']).
card_cmc('firespout', 3).
card_layout('firespout', 'normal').

% Found in: WTH
card_name('firestorm', 'Firestorm').
card_type('firestorm', 'Instant').
card_types('firestorm', ['Instant']).
card_subtypes('firestorm', []).
card_colors('firestorm', ['R']).
card_text('firestorm', 'As an additional cost to cast Firestorm, discard X cards.\nFirestorm deals X damage to each of X target creatures and/or players.').
card_mana_cost('firestorm', ['R']).
card_cmc('firestorm', 1).
card_layout('firestorm', 'normal').
card_reserved('firestorm').

% Found in: VIS
card_name('firestorm hellkite', 'Firestorm Hellkite').
card_type('firestorm hellkite', 'Creature — Dragon').
card_types('firestorm hellkite', ['Creature']).
card_subtypes('firestorm hellkite', ['Dragon']).
card_colors('firestorm hellkite', ['U', 'R']).
card_text('firestorm hellkite', 'Flying, trample\nCumulative upkeep {U}{R} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('firestorm hellkite', ['4', 'U', 'R']).
card_cmc('firestorm hellkite', 6).
card_layout('firestorm hellkite', 'normal').
card_power('firestorm hellkite', 6).
card_toughness('firestorm hellkite', 6).
card_reserved('firestorm hellkite').

% Found in: LEG, ME3
card_name('firestorm phoenix', 'Firestorm Phoenix').
card_type('firestorm phoenix', 'Creature — Phoenix').
card_types('firestorm phoenix', ['Creature']).
card_subtypes('firestorm phoenix', ['Phoenix']).
card_colors('firestorm phoenix', ['R']).
card_text('firestorm phoenix', 'Flying\nIf Firestorm Phoenix would die, return Firestorm Phoenix to its owner\'s hand instead. Until that player\'s next turn, that player plays with that card revealed in his or her hand and can\'t play it.').
card_mana_cost('firestorm phoenix', ['4', 'R', 'R']).
card_cmc('firestorm phoenix', 6).
card_layout('firestorm phoenix', 'normal').
card_power('firestorm phoenix', 3).
card_toughness('firestorm phoenix', 2).
card_reserved('firestorm phoenix').

% Found in: TSP
card_name('firewake sliver', 'Firewake Sliver').
card_type('firewake sliver', 'Creature — Sliver').
card_types('firewake sliver', ['Creature']).
card_subtypes('firewake sliver', ['Sliver']).
card_colors('firewake sliver', ['R', 'G']).
card_text('firewake sliver', 'All Sliver creatures have haste.\nAll Slivers have \"{1}, Sacrifice this permanent: Target Sliver creature gets +2/+2 until end of turn.\"').
card_mana_cost('firewake sliver', ['1', 'R', 'G']).
card_cmc('firewake sliver', 3).
card_layout('firewake sliver', 'normal').
card_power('firewake sliver', 1).
card_toughness('firewake sliver', 1).

% Found in: ARB
card_name('firewild borderpost', 'Firewild Borderpost').
card_type('firewild borderpost', 'Artifact').
card_types('firewild borderpost', ['Artifact']).
card_subtypes('firewild borderpost', []).
card_colors('firewild borderpost', ['R', 'G']).
card_text('firewild borderpost', 'You may pay {1} and return a basic land you control to its owner\'s hand rather than pay Firewild Borderpost\'s mana cost.\nFirewild Borderpost enters the battlefield tapped.\n{T}: Add {R} or {G} to your mana pool.').
card_mana_cost('firewild borderpost', ['1', 'R', 'G']).
card_cmc('firewild borderpost', 3).
card_layout('firewild borderpost', 'normal').

% Found in: M13
card_name('firewing phoenix', 'Firewing Phoenix').
card_type('firewing phoenix', 'Creature — Phoenix').
card_types('firewing phoenix', ['Creature']).
card_subtypes('firewing phoenix', ['Phoenix']).
card_colors('firewing phoenix', ['R']).
card_text('firewing phoenix', 'Flying\n{1}{R}{R}{R}: Return Firewing Phoenix from your graveyard to your hand.').
card_mana_cost('firewing phoenix', ['3', 'R']).
card_cmc('firewing phoenix', 4).
card_layout('firewing phoenix', 'normal').
card_power('firewing phoenix', 4).
card_toughness('firewing phoenix', 2).

% Found in: UNH
card_name('first come, first served', 'First Come, First Served').
card_type('first come, first served', 'Enchantment').
card_types('first come, first served', ['Enchantment']).
card_subtypes('first come, first served', []).
card_colors('first come, first served', ['W']).
card_text('first come, first served', 'The attacking or blocking creature with the lowest collector number has first strike. If two or more creatures are tied, they all have first strike.').
card_mana_cost('first come, first served', ['1', 'W']).
card_cmc('first come, first served', 2).
card_layout('first come, first served', 'normal').

% Found in: M15
card_name('first response', 'First Response').
card_type('first response', 'Enchantment').
card_types('first response', ['Enchantment']).
card_subtypes('first response', []).
card_colors('first response', ['W']).
card_text('first response', 'At the beginning of each upkeep, if you lost life last turn, put a 1/1 white Soldier creature token onto the battlefield.').
card_mana_cost('first response', ['3', 'W']).
card_cmc('first response', 4).
card_layout('first response', 'normal').

% Found in: BOK
card_name('first volley', 'First Volley').
card_type('first volley', 'Instant — Arcane').
card_types('first volley', ['Instant']).
card_subtypes('first volley', ['Arcane']).
card_colors('first volley', ['R']).
card_text('first volley', 'First Volley deals 1 damage to target creature and 1 damage to that creature\'s controller.').
card_mana_cost('first volley', ['1', 'R']).
card_cmc('first volley', 2).
card_layout('first volley', 'normal').

% Found in: 9ED, ARN, CHR
card_name('fishliver oil', 'Fishliver Oil').
card_type('fishliver oil', 'Enchantment — Aura').
card_types('fishliver oil', ['Enchantment']).
card_subtypes('fishliver oil', ['Aura']).
card_colors('fishliver oil', ['U']).
card_text('fishliver oil', 'Enchant creature (Target a creature as you cast this. This card enters the battlefield attached to that creature.)\nEnchanted creature has islandwalk. (It can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('fishliver oil', ['1', 'U']).
card_cmc('fishliver oil', 2).
card_layout('fishliver oil', 'normal').

% Found in: 4ED, DRK, MED
card_name('fissure', 'Fissure').
card_type('fissure', 'Instant').
card_types('fissure', ['Instant']).
card_subtypes('fissure', []).
card_colors('fissure', ['R']).
card_text('fissure', 'Destroy target creature or land. It can\'t be regenerated.').
card_mana_cost('fissure', ['3', 'R', 'R']).
card_cmc('fissure', 5).
card_layout('fissure', 'normal').

% Found in: C13, ROE
card_name('fissure vent', 'Fissure Vent').
card_type('fissure vent', 'Sorcery').
card_types('fissure vent', ['Sorcery']).
card_subtypes('fissure vent', []).
card_colors('fissure vent', ['R']).
card_text('fissure vent', 'Choose one or both —\n• Destroy target artifact.\n• Destroy target nonbasic land.').
card_mana_cost('fissure vent', ['3', 'R', 'R']).
card_cmc('fissure vent', 5).
card_layout('fissure vent', 'normal').

% Found in: 5DN
card_name('fist of suns', 'Fist of Suns').
card_type('fist of suns', 'Artifact').
card_types('fist of suns', ['Artifact']).
card_subtypes('fist of suns', []).
card_colors('fist of suns', []).
card_text('fist of suns', 'You may pay {W}{U}{B}{R}{G} rather than pay the mana cost for spells that you cast.').
card_mana_cost('fist of suns', ['3']).
card_cmc('fist of suns', 3).
card_layout('fist of suns', 'normal').

% Found in: LRW
card_name('fistful of force', 'Fistful of Force').
card_type('fistful of force', 'Instant').
card_types('fistful of force', ['Instant']).
card_subtypes('fistful of force', []).
card_colors('fistful of force', ['G']).
card_text('fistful of force', 'Target creature gets +2/+2 until end of turn. Clash with an opponent. If you win, that creature gets an additional +2/+2 and gains trample until end of turn. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('fistful of force', ['1', 'G']).
card_cmc('fistful of force', 2).
card_layout('fistful of force', 'normal').

% Found in: CMD, RAV
card_name('fists of ironwood', 'Fists of Ironwood').
card_type('fists of ironwood', 'Enchantment — Aura').
card_types('fists of ironwood', ['Enchantment']).
card_subtypes('fists of ironwood', ['Aura']).
card_colors('fists of ironwood', ['G']).
card_text('fists of ironwood', 'Enchant creature\nWhen Fists of Ironwood enters the battlefield, put two 1/1 green Saproling creature tokens onto the battlefield.\nEnchanted creature has trample.').
card_mana_cost('fists of ironwood', ['1', 'G']).
card_cmc('fists of ironwood', 2).
card_layout('fists of ironwood', 'normal').

% Found in: 10E, MRD
card_name('fists of the anvil', 'Fists of the Anvil').
card_type('fists of the anvil', 'Instant').
card_types('fists of the anvil', ['Instant']).
card_subtypes('fists of the anvil', []).
card_colors('fists of the anvil', ['R']).
card_text('fists of the anvil', 'Target creature gets +4/+0 until end of turn.').
card_mana_cost('fists of the anvil', ['1', 'R']).
card_cmc('fists of the anvil', 2).
card_layout('fists of the anvil', 'normal').

% Found in: SHM
card_name('fists of the demigod', 'Fists of the Demigod').
card_type('fists of the demigod', 'Enchantment — Aura').
card_types('fists of the demigod', ['Enchantment']).
card_subtypes('fists of the demigod', ['Aura']).
card_colors('fists of the demigod', ['B', 'R']).
card_text('fists of the demigod', 'Enchant creature\nAs long as enchanted creature is black, it gets +1/+1 and has wither. (It deals damage to creatures in the form of -1/-1 counters.)\nAs long as enchanted creature is red, it gets +1/+1 and has first strike.').
card_mana_cost('fists of the demigod', ['1', 'B/R']).
card_cmc('fists of the demigod', 2).
card_layout('fists of the demigod', 'normal').

% Found in: 6ED, WTH
card_name('fit of rage', 'Fit of Rage').
card_type('fit of rage', 'Sorcery').
card_types('fit of rage', ['Sorcery']).
card_subtypes('fit of rage', []).
card_colors('fit of rage', ['R']).
card_text('fit of rage', 'Target creature gets +3/+3 and gains first strike until end of turn.').
card_mana_cost('fit of rage', ['1', 'R']).
card_cmc('fit of rage', 2).
card_layout('fit of rage', 'normal').

% Found in: GTC
card_name('five-alarm fire', 'Five-Alarm Fire').
card_type('five-alarm fire', 'Enchantment').
card_types('five-alarm fire', ['Enchantment']).
card_subtypes('five-alarm fire', []).
card_colors('five-alarm fire', ['R']).
card_text('five-alarm fire', 'Whenever a creature you control deals combat damage, put a blaze counter on Five-Alarm Fire.\nRemove five blaze counters from Five-Alarm Fire: Five-Alarm Fire deals 5 damage to target creature or player.').
card_mana_cost('five-alarm fire', ['1', 'R', 'R']).
card_cmc('five-alarm fire', 3).
card_layout('five-alarm fire', 'normal').

% Found in: UNH
card_name('flaccify', 'Flaccify').
card_type('flaccify', 'Instant').
card_types('flaccify', ['Instant']).
card_subtypes('flaccify', []).
card_colors('flaccify', ['U']).
card_text('flaccify', 'Counter target spell unless its controller pays {3}{½}.').
card_mana_cost('flaccify', ['2', 'U']).
card_cmc('flaccify', 3).
card_layout('flaccify', 'normal').

% Found in: TSP
card_name('flagstones of trokair', 'Flagstones of Trokair').
card_type('flagstones of trokair', 'Legendary Land').
card_types('flagstones of trokair', ['Land']).
card_subtypes('flagstones of trokair', []).
card_supertypes('flagstones of trokair', ['Legendary']).
card_colors('flagstones of trokair', []).
card_text('flagstones of trokair', '{T}: Add {W} to your mana pool.\nWhen Flagstones of Trokair is put into a graveyard from the battlefield, you may search your library for a Plains card and put it onto the battlefield tapped. If you do, shuffle your library.').
card_layout('flagstones of trokair', 'normal').

% Found in: TMP
card_name('flailing drake', 'Flailing Drake').
card_type('flailing drake', 'Creature — Drake').
card_types('flailing drake', ['Creature']).
card_subtypes('flailing drake', ['Drake']).
card_colors('flailing drake', ['G']).
card_text('flailing drake', 'Flying\nWhenever Flailing Drake blocks or becomes blocked by a creature, that creature gets +1/+1 until end of turn.').
card_mana_cost('flailing drake', ['3', 'G']).
card_cmc('flailing drake', 4).
card_layout('flailing drake', 'normal').
card_power('flailing drake', 2).
card_toughness('flailing drake', 3).

% Found in: MMQ
card_name('flailing manticore', 'Flailing Manticore').
card_type('flailing manticore', 'Creature — Manticore').
card_types('flailing manticore', ['Creature']).
card_subtypes('flailing manticore', ['Manticore']).
card_colors('flailing manticore', ['R']).
card_text('flailing manticore', 'Flying, first strike\n{1}: Flailing Manticore gets +1/+1 until end of turn. Any player may activate this ability.\n{1}: Flailing Manticore gets -1/-1 until end of turn. Any player may activate this ability.').
card_mana_cost('flailing manticore', ['3', 'R']).
card_cmc('flailing manticore', 4).
card_layout('flailing manticore', 'normal').
card_power('flailing manticore', 3).
card_toughness('flailing manticore', 3).

% Found in: MMQ
card_name('flailing ogre', 'Flailing Ogre').
card_type('flailing ogre', 'Creature — Ogre').
card_types('flailing ogre', ['Creature']).
card_subtypes('flailing ogre', ['Ogre']).
card_colors('flailing ogre', ['R']).
card_text('flailing ogre', '{1}: Flailing Ogre gets +1/+1 until end of turn. Any player may activate this ability.\n{1}: Flailing Ogre gets -1/-1 until end of turn. Any player may activate this ability.').
card_mana_cost('flailing ogre', ['2', 'R']).
card_cmc('flailing ogre', 3).
card_layout('flailing ogre', 'normal').
card_power('flailing ogre', 3).
card_toughness('flailing ogre', 3).

% Found in: MMQ
card_name('flailing soldier', 'Flailing Soldier').
card_type('flailing soldier', 'Creature — Human Soldier').
card_types('flailing soldier', ['Creature']).
card_subtypes('flailing soldier', ['Human', 'Soldier']).
card_colors('flailing soldier', ['R']).
card_text('flailing soldier', '{1}: Flailing Soldier gets +1/+1 until end of turn. Any player may activate this ability.\n{1}: Flailing Soldier gets -1/-1 until end of turn. Any player may activate this ability.').
card_mana_cost('flailing soldier', ['R']).
card_cmc('flailing soldier', 1).
card_layout('flailing soldier', 'normal').
card_power('flailing soldier', 2).
card_toughness('flailing soldier', 2).

% Found in: ODY
card_name('flame burst', 'Flame Burst').
card_type('flame burst', 'Instant').
card_types('flame burst', ['Instant']).
card_subtypes('flame burst', []).
card_colors('flame burst', ['R']).
card_text('flame burst', 'Flame Burst deals X damage to target creature or player, where X is 2 plus the number of cards named Flame Burst in all graveyards.').
card_mana_cost('flame burst', ['1', 'R']).
card_cmc('flame burst', 2).
card_layout('flame burst', 'normal').

% Found in: MIR
card_name('flame elemental', 'Flame Elemental').
card_type('flame elemental', 'Creature — Elemental').
card_types('flame elemental', ['Creature']).
card_subtypes('flame elemental', ['Elemental']).
card_colors('flame elemental', ['R']).
card_text('flame elemental', '{R}, {T}, Sacrifice Flame Elemental: Flame Elemental deals damage equal to its power to target creature.').
card_mana_cost('flame elemental', ['2', 'R', 'R']).
card_cmc('flame elemental', 4).
card_layout('flame elemental', 'normal').
card_power('flame elemental', 3).
card_toughness('flame elemental', 2).

% Found in: RAV
card_name('flame fusillade', 'Flame Fusillade').
card_type('flame fusillade', 'Sorcery').
card_types('flame fusillade', ['Sorcery']).
card_subtypes('flame fusillade', []).
card_colors('flame fusillade', ['R']).
card_text('flame fusillade', 'Until end of turn, permanents you control gain \"{T}: This permanent deals 1 damage to target creature or player.\"').
card_mana_cost('flame fusillade', ['3', 'R']).
card_cmc('flame fusillade', 4).
card_layout('flame fusillade', 'normal').

% Found in: EVE
card_name('flame jab', 'Flame Jab').
card_type('flame jab', 'Sorcery').
card_types('flame jab', ['Sorcery']).
card_subtypes('flame jab', []).
card_colors('flame jab', ['R']).
card_text('flame jab', 'Flame Jab deals 1 damage to target creature or player.\nRetrace (You may cast this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_mana_cost('flame jab', ['R']).
card_cmc('flame jab', 1).
card_layout('flame jab', 'normal').

% Found in: DD2, DD3_JVC, DDK, SHM, pMPR
card_name('flame javelin', 'Flame Javelin').
card_type('flame javelin', 'Instant').
card_types('flame javelin', ['Instant']).
card_subtypes('flame javelin', []).
card_colors('flame javelin', ['R']).
card_text('flame javelin', '({2/R} can be paid with any two mana or with {R}. This card\'s converted mana cost is 6.)\nFlame Javelin deals 4 damage to target creature or player.').
card_mana_cost('flame javelin', ['2/R', '2/R', '2/R']).
card_cmc('flame javelin', 6).
card_layout('flame javelin', 'normal').

% Found in: UDS
card_name('flame jet', 'Flame Jet').
card_type('flame jet', 'Sorcery').
card_types('flame jet', ['Sorcery']).
card_subtypes('flame jet', []).
card_colors('flame jet', ['R']).
card_text('flame jet', 'Flame Jet deals 3 damage to target player.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('flame jet', ['1', 'R']).
card_cmc('flame jet', 2).
card_layout('flame jet', 'normal').

% Found in: NMS
card_name('flame rift', 'Flame Rift').
card_type('flame rift', 'Sorcery').
card_types('flame rift', ['Sorcery']).
card_subtypes('flame rift', []).
card_colors('flame rift', ['R']).
card_text('flame rift', 'Flame Rift deals 4 damage to each player.').
card_mana_cost('flame rift', ['1', 'R']).
card_cmc('flame rift', 2).
card_layout('flame rift', 'normal').

% Found in: DDK, ROE
card_name('flame slash', 'Flame Slash').
card_type('flame slash', 'Sorcery').
card_types('flame slash', ['Sorcery']).
card_subtypes('flame slash', []).
card_colors('flame slash', ['R']).
card_text('flame slash', 'Flame Slash deals 4 damage to target creature.').
card_mana_cost('flame slash', ['R']).
card_cmc('flame slash', 1).
card_layout('flame slash', 'normal').

% Found in: 5ED, 6ED, ICE, ME2, S00
card_name('flame spirit', 'Flame Spirit').
card_type('flame spirit', 'Creature — Elemental Spirit').
card_types('flame spirit', ['Creature']).
card_subtypes('flame spirit', ['Elemental', 'Spirit']).
card_colors('flame spirit', ['R']).
card_text('flame spirit', '{R}: Flame Spirit gets +1/+0 until end of turn.').
card_mana_cost('flame spirit', ['4', 'R']).
card_cmc('flame spirit', 5).
card_layout('flame spirit', 'normal').
card_power('flame spirit', 2).
card_toughness('flame spirit', 3).

% Found in: 9ED, STH, TPR
card_name('flame wave', 'Flame Wave').
card_type('flame wave', 'Sorcery').
card_types('flame wave', ['Sorcery']).
card_subtypes('flame wave', []).
card_colors('flame wave', ['R']).
card_text('flame wave', 'Flame Wave deals 4 damage to target player and each creature he or she controls.').
card_mana_cost('flame wave', ['3', 'R', 'R', 'R', 'R']).
card_cmc('flame wave', 7).
card_layout('flame wave', 'normal').

% Found in: DIS
card_name('flame-kin war scout', 'Flame-Kin War Scout').
card_type('flame-kin war scout', 'Creature — Elemental Scout').
card_types('flame-kin war scout', ['Creature']).
card_subtypes('flame-kin war scout', ['Elemental', 'Scout']).
card_colors('flame-kin war scout', ['R']).
card_text('flame-kin war scout', 'When another creature enters the battlefield, sacrifice Flame-Kin War Scout. If you do, Flame-Kin War Scout deals 4 damage to that creature.').
card_mana_cost('flame-kin war scout', ['3', 'R']).
card_cmc('flame-kin war scout', 4).
card_layout('flame-kin war scout', 'normal').
card_power('flame-kin war scout', 2).
card_toughness('flame-kin war scout', 4).

% Found in: DDN, RAV
card_name('flame-kin zealot', 'Flame-Kin Zealot').
card_type('flame-kin zealot', 'Creature — Elemental Berserker').
card_types('flame-kin zealot', ['Creature']).
card_subtypes('flame-kin zealot', ['Elemental', 'Berserker']).
card_colors('flame-kin zealot', ['W', 'R']).
card_text('flame-kin zealot', 'When Flame-Kin Zealot enters the battlefield, creatures you control get +1/+1 and gain haste until end of turn.').
card_mana_cost('flame-kin zealot', ['1', 'R', 'R', 'W']).
card_cmc('flame-kin zealot', 4).
card_layout('flame-kin zealot', 'normal').
card_power('flame-kin zealot', 2).
card_toughness('flame-kin zealot', 2).

% Found in: BNG
card_name('flame-wreathed phoenix', 'Flame-Wreathed Phoenix').
card_type('flame-wreathed phoenix', 'Creature — Phoenix').
card_types('flame-wreathed phoenix', ['Creature']).
card_subtypes('flame-wreathed phoenix', ['Phoenix']).
card_colors('flame-wreathed phoenix', ['R']).
card_text('flame-wreathed phoenix', 'Flying\nTribute 2 (As this creature enters the battlefield, an opponent of your choice may place two +1/+1 counters on it.)\nWhen Flame-Wreathed Phoenix enters the battlefield, if tribute wasn\'t paid, it gains haste and \"When this creature dies, return it to its owner\'s hand.\"').
card_mana_cost('flame-wreathed phoenix', ['2', 'R', 'R']).
card_cmc('flame-wreathed phoenix', 4).
card_layout('flame-wreathed phoenix', 'normal').
card_power('flame-wreathed phoenix', 3).
card_toughness('flame-wreathed phoenix', 3).

% Found in: ALA, ARC, M12
card_name('flameblast dragon', 'Flameblast Dragon').
card_type('flameblast dragon', 'Creature — Dragon').
card_types('flameblast dragon', ['Creature']).
card_subtypes('flameblast dragon', ['Dragon']).
card_colors('flameblast dragon', ['R']).
card_text('flameblast dragon', 'Flying\nWhenever Flameblast Dragon attacks, you may pay {X}{R}. If you do, Flameblast Dragon deals X damage to target creature or player.').
card_mana_cost('flameblast dragon', ['4', 'R', 'R']).
card_cmc('flameblast dragon', 6).
card_layout('flameblast dragon', 'normal').
card_power('flameblast dragon', 5).
card_toughness('flameblast dragon', 5).

% Found in: SOM
card_name('flameborn hellion', 'Flameborn Hellion').
card_type('flameborn hellion', 'Creature — Hellion').
card_types('flameborn hellion', ['Creature']).
card_subtypes('flameborn hellion', ['Hellion']).
card_colors('flameborn hellion', ['R']).
card_text('flameborn hellion', 'Haste\nFlameborn Hellion attacks each turn if able.').
card_mana_cost('flameborn hellion', ['5', 'R']).
card_cmc('flameborn hellion', 6).
card_layout('flameborn hellion', 'normal').
card_power('flameborn hellion', 5).
card_toughness('flameborn hellion', 4).

% Found in: NPH
card_name('flameborn viron', 'Flameborn Viron').
card_type('flameborn viron', 'Creature — Insect').
card_types('flameborn viron', ['Creature']).
card_subtypes('flameborn viron', ['Insect']).
card_colors('flameborn viron', ['R']).
card_text('flameborn viron', '').
card_mana_cost('flameborn viron', ['4', 'R', 'R']).
card_cmc('flameborn viron', 6).
card_layout('flameborn viron', 'normal').
card_power('flameborn viron', 6).
card_toughness('flameborn viron', 4).

% Found in: DST
card_name('flamebreak', 'Flamebreak').
card_type('flamebreak', 'Sorcery').
card_types('flamebreak', ['Sorcery']).
card_subtypes('flamebreak', []).
card_colors('flamebreak', ['R']).
card_text('flamebreak', 'Flamebreak deals 3 damage to each creature without flying and each player. Creatures dealt damage this way can\'t be regenerated this turn.').
card_mana_cost('flamebreak', ['R', 'R', 'R']).
card_cmc('flamebreak', 3).
card_layout('flamebreak', 'normal').

% Found in: THS
card_name('flamecast wheel', 'Flamecast Wheel').
card_type('flamecast wheel', 'Artifact').
card_types('flamecast wheel', ['Artifact']).
card_subtypes('flamecast wheel', []).
card_colors('flamecast wheel', []).
card_text('flamecast wheel', '{5}, {T}, Sacrifice Flamecast Wheel: Flamecast Wheel deals 3 damage to target creature.').
card_mana_cost('flamecast wheel', ['1']).
card_cmc('flamecast wheel', 1).
card_layout('flamecast wheel', 'normal').

% Found in: TSP
card_name('flamecore elemental', 'Flamecore Elemental').
card_type('flamecore elemental', 'Creature — Elemental').
card_types('flamecore elemental', ['Creature']).
card_subtypes('flamecore elemental', ['Elemental']).
card_colors('flamecore elemental', ['R']).
card_text('flamecore elemental', 'Echo {2}{R}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_mana_cost('flamecore elemental', ['2', 'R', 'R']).
card_cmc('flamecore elemental', 4).
card_layout('flamecore elemental', 'normal').
card_power('flamecore elemental', 5).
card_toughness('flamecore elemental', 4).

% Found in: LRW
card_name('flamekin bladewhirl', 'Flamekin Bladewhirl').
card_type('flamekin bladewhirl', 'Creature — Elemental Warrior').
card_types('flamekin bladewhirl', ['Creature']).
card_subtypes('flamekin bladewhirl', ['Elemental', 'Warrior']).
card_colors('flamekin bladewhirl', ['R']).
card_text('flamekin bladewhirl', 'As an additional cost to cast Flamekin Bladewhirl, reveal an Elemental card from your hand or pay {3}.').
card_mana_cost('flamekin bladewhirl', ['R']).
card_cmc('flamekin bladewhirl', 1).
card_layout('flamekin bladewhirl', 'normal').
card_power('flamekin bladewhirl', 2).
card_toughness('flamekin bladewhirl', 1).

% Found in: DD2, DD3_JVC, LRW
card_name('flamekin brawler', 'Flamekin Brawler').
card_type('flamekin brawler', 'Creature — Elemental Warrior').
card_types('flamekin brawler', ['Creature']).
card_subtypes('flamekin brawler', ['Elemental', 'Warrior']).
card_colors('flamekin brawler', ['R']).
card_text('flamekin brawler', '{R}: Flamekin Brawler gets +1/+0 until end of turn.').
card_mana_cost('flamekin brawler', ['R']).
card_cmc('flamekin brawler', 1).
card_layout('flamekin brawler', 'normal').
card_power('flamekin brawler', 0).
card_toughness('flamekin brawler', 2).

% Found in: HOP, LRW
card_name('flamekin harbinger', 'Flamekin Harbinger').
card_type('flamekin harbinger', 'Creature — Elemental Shaman').
card_types('flamekin harbinger', ['Creature']).
card_subtypes('flamekin harbinger', ['Elemental', 'Shaman']).
card_colors('flamekin harbinger', ['R']).
card_text('flamekin harbinger', 'When Flamekin Harbinger enters the battlefield, you may search your library for an Elemental card, reveal it, then shuffle your library and put that card on top of it.').
card_mana_cost('flamekin harbinger', ['R']).
card_cmc('flamekin harbinger', 1).
card_layout('flamekin harbinger', 'normal').
card_power('flamekin harbinger', 1).
card_toughness('flamekin harbinger', 1).

% Found in: LRW
card_name('flamekin spitfire', 'Flamekin Spitfire').
card_type('flamekin spitfire', 'Creature — Elemental Shaman').
card_types('flamekin spitfire', ['Creature']).
card_subtypes('flamekin spitfire', ['Elemental', 'Shaman']).
card_colors('flamekin spitfire', ['R']).
card_text('flamekin spitfire', '{3}{R}: Flamekin Spitfire deals 1 damage to target creature or player.').
card_mana_cost('flamekin spitfire', ['1', 'R']).
card_cmc('flamekin spitfire', 2).
card_layout('flamekin spitfire', 'normal').
card_power('flamekin spitfire', 1).
card_toughness('flamekin spitfire', 1).

% Found in: C14
card_name('flamekin village', 'Flamekin Village').
card_type('flamekin village', 'Land').
card_types('flamekin village', ['Land']).
card_subtypes('flamekin village', []).
card_colors('flamekin village', []).
card_text('flamekin village', 'As Flamekin Village enters the battlefield, you may reveal an Elemental card from your hand. If you don\'t, Flamekin Village enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\n{R}, {T}: Target creature gains haste until end of turn.').
card_layout('flamekin village', 'normal').

% Found in: FRF, pMEI
card_name('flamerush rider', 'Flamerush Rider').
card_type('flamerush rider', 'Creature — Human Warrior').
card_types('flamerush rider', ['Creature']).
card_subtypes('flamerush rider', ['Human', 'Warrior']).
card_colors('flamerush rider', ['R']).
card_text('flamerush rider', 'Whenever Flamerush Rider attacks, put a token onto the battlefield tapped and attacking that\'s a copy of another target attacking creature. Exile the token at end of combat.\nDash {2}{R}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('flamerush rider', ['4', 'R']).
card_cmc('flamerush rider', 5).
card_layout('flamerush rider', 'normal').
card_power('flamerush rider', 3).
card_toughness('flamerush rider', 3).

% Found in: BOK, PD2
card_name('flames of the blood hand', 'Flames of the Blood Hand').
card_type('flames of the blood hand', 'Instant').
card_types('flames of the blood hand', ['Instant']).
card_subtypes('flames of the blood hand', []).
card_colors('flames of the blood hand', ['R']).
card_text('flames of the blood hand', 'Flames of the Blood Hand deals 4 damage to target player. The damage can\'t be prevented. If that player would gain life this turn, that player gains no life instead.').
card_mana_cost('flames of the blood hand', ['2', 'R']).
card_cmc('flames of the blood hand', 3).
card_layout('flames of the blood hand', 'normal').

% Found in: M13, M14
card_name('flames of the firebrand', 'Flames of the Firebrand').
card_type('flames of the firebrand', 'Sorcery').
card_types('flames of the firebrand', ['Sorcery']).
card_subtypes('flames of the firebrand', []).
card_colors('flames of the firebrand', ['R']).
card_text('flames of the firebrand', 'Flames of the Firebrand deals 3 damage divided as you choose among one, two, or three target creatures and/or players.').
card_mana_cost('flames of the firebrand', ['2', 'R']).
card_cmc('flames of the firebrand', 3).
card_layout('flames of the firebrand', 'normal').

% Found in: ORI
card_name('flameshadow conjuring', 'Flameshadow Conjuring').
card_type('flameshadow conjuring', 'Enchantment').
card_types('flameshadow conjuring', ['Enchantment']).
card_subtypes('flameshadow conjuring', []).
card_colors('flameshadow conjuring', ['R']).
card_text('flameshadow conjuring', 'Whenever a nontoken creature enters the battlefield under your control, you may pay {R}. If you do, put a token onto the battlefield that\'s a copy of that creature. That token gains haste. Exile it at the beginning of the next end step.').
card_mana_cost('flameshadow conjuring', ['3', 'R']).
card_cmc('flameshadow conjuring', 4).
card_layout('flameshadow conjuring', 'normal').

% Found in: PCY
card_name('flameshot', 'Flameshot').
card_type('flameshot', 'Sorcery').
card_types('flameshot', ['Sorcery']).
card_subtypes('flameshot', []).
card_colors('flameshot', ['R']).
card_text('flameshot', 'You may discard a Mountain card rather than pay Flameshot\'s mana cost.\nFlameshot deals 3 damage divided as you choose among one, two, or three target creatures.').
card_mana_cost('flameshot', ['3', 'R']).
card_cmc('flameshot', 4).
card_layout('flameshot', 'normal').

% Found in: THS
card_name('flamespeaker adept', 'Flamespeaker Adept').
card_type('flamespeaker adept', 'Creature — Human Shaman').
card_types('flamespeaker adept', ['Creature']).
card_subtypes('flamespeaker adept', ['Human', 'Shaman']).
card_colors('flamespeaker adept', ['R']).
card_text('flamespeaker adept', 'Whenever you scry, Flamespeaker Adept gets +2/+0 and gains first strike until end of turn.').
card_mana_cost('flamespeaker adept', ['2', 'R']).
card_cmc('flamespeaker adept', 3).
card_layout('flamespeaker adept', 'normal').
card_power('flamespeaker adept', 2).
card_toughness('flamespeaker adept', 3).

% Found in: JOU
card_name('flamespeaker\'s will', 'Flamespeaker\'s Will').
card_type('flamespeaker\'s will', 'Enchantment — Aura').
card_types('flamespeaker\'s will', ['Enchantment']).
card_subtypes('flamespeaker\'s will', ['Aura']).
card_colors('flamespeaker\'s will', ['R']).
card_text('flamespeaker\'s will', 'Enchant creature you control\nEnchanted creature gets +1/+1.\nWhenever enchanted creature deals combat damage to a player, you may sacrifice Flamespeaker\'s Will. If you do, destroy target artifact.').
card_mana_cost('flamespeaker\'s will', ['R']).
card_cmc('flamespeaker\'s will', 1).
card_layout('flamespeaker\'s will', 'normal').

% Found in: ONS
card_name('flamestick courier', 'Flamestick Courier').
card_type('flamestick courier', 'Creature — Goblin').
card_types('flamestick courier', ['Creature']).
card_subtypes('flamestick courier', ['Goblin']).
card_colors('flamestick courier', ['R']).
card_text('flamestick courier', 'You may choose not to untap Flamestick Courier during your untap step.\n{2}{R}, {T}: Target Goblin creature gets +2/+2 and has haste for as long as Flamestick Courier remains tapped.').
card_mana_cost('flamestick courier', ['2', 'R']).
card_cmc('flamestick courier', 3).
card_layout('flamestick courier', 'normal').
card_power('flamestick courier', 2).
card_toughness('flamestick courier', 1).

% Found in: C14, CMD, DD2, DD3_JVC, HOP, PLS, VMA, pFNM
card_name('flametongue kavu', 'Flametongue Kavu').
card_type('flametongue kavu', 'Creature — Kavu').
card_types('flametongue kavu', ['Creature']).
card_subtypes('flametongue kavu', ['Kavu']).
card_colors('flametongue kavu', ['R']).
card_text('flametongue kavu', 'When Flametongue Kavu enters the battlefield, it deals 4 damage to target creature.').
card_mana_cost('flametongue kavu', ['3', 'R']).
card_cmc('flametongue kavu', 4).
card_layout('flametongue kavu', 'normal').
card_power('flametongue kavu', 4).
card_toughness('flametongue kavu', 2).

% Found in: VAN
card_name('flametongue kavu avatar', 'Flametongue Kavu Avatar').
card_type('flametongue kavu avatar', 'Vanguard').
card_types('flametongue kavu avatar', ['Vanguard']).
card_subtypes('flametongue kavu avatar', []).
card_colors('flametongue kavu avatar', []).
card_text('flametongue kavu avatar', 'Whenever a nontoken creature enters the battlefield under your control, that creature deals X damage to target creature, where X is a number chosen at random from 0 to 4.').
card_layout('flametongue kavu avatar', 'vanguard').

% Found in: FRF
card_name('flamewake phoenix', 'Flamewake Phoenix').
card_type('flamewake phoenix', 'Creature — Phoenix').
card_types('flamewake phoenix', ['Creature']).
card_subtypes('flamewake phoenix', ['Phoenix']).
card_colors('flamewake phoenix', ['R']).
card_text('flamewake phoenix', 'Flying, haste\nFlamewake Phoenix attacks each turn if able.\nFerocious — At the beginning of combat on your turn, if you control a creature with power 4 or greater, you may pay {R}. If you do, return Flamewake Phoenix from your graveyard to the battlefield.').
card_mana_cost('flamewake phoenix', ['1', 'R', 'R']).
card_cmc('flamewake phoenix', 3).
card_layout('flamewake phoenix', 'normal').
card_power('flamewake phoenix', 2).
card_toughness('flamewake phoenix', 2).

% Found in: 10E, DD2, DD3_EVG, DD3_JVC, EVG, LGN
card_name('flamewave invoker', 'Flamewave Invoker').
card_type('flamewave invoker', 'Creature — Goblin Mutant').
card_types('flamewave invoker', ['Creature']).
card_subtypes('flamewave invoker', ['Goblin', 'Mutant']).
card_colors('flamewave invoker', ['R']).
card_text('flamewave invoker', '{7}{R}: Flamewave Invoker deals 5 damage to target player.').
card_mana_cost('flamewave invoker', ['2', 'R']).
card_cmc('flamewave invoker', 3).
card_layout('flamewave invoker', 'normal').
card_power('flamewave invoker', 2).
card_toughness('flamewave invoker', 2).

% Found in: CNS
card_name('flamewright', 'Flamewright').
card_type('flamewright', 'Creature — Human Artificer').
card_types('flamewright', ['Creature']).
card_subtypes('flamewright', ['Human', 'Artificer']).
card_colors('flamewright', ['W', 'R']).
card_text('flamewright', '{1}, {T}: Put a 1/1 colorless Construct artifact creature token with defender onto the battlefield.\n{T}, Sacrifice a creature with defender: Flamewright deals 1 damage to target creature or player.').
card_mana_cost('flamewright', ['R', 'W']).
card_cmc('flamewright', 2).
card_layout('flamewright', 'normal').
card_power('flamewright', 1).
card_toughness('flamewright', 1).

% Found in: TOR
card_name('flaming gambit', 'Flaming Gambit').
card_type('flaming gambit', 'Instant').
card_types('flaming gambit', ['Instant']).
card_subtypes('flaming gambit', []).
card_colors('flaming gambit', ['R']).
card_text('flaming gambit', 'Flaming Gambit deals X damage to target player. That player may choose a creature he or she controls and have Flaming Gambit deal that damage to it instead.\nFlashback {X}{R}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('flaming gambit', ['X', 'R']).
card_cmc('flaming gambit', 1).
card_layout('flaming gambit', 'normal').

% Found in: MMQ
card_name('flaming sword', 'Flaming Sword').
card_type('flaming sword', 'Enchantment — Aura').
card_types('flaming sword', ['Enchantment']).
card_subtypes('flaming sword', ['Aura']).
card_colors('flaming sword', ['R']).
card_text('flaming sword', 'Flash\nEnchant creature\nEnchanted creature gets +1/+0 and has first strike.').
card_mana_cost('flaming sword', ['1', 'R']).
card_cmc('flaming sword', 2).
card_layout('flaming sword', 'normal').

% Found in: PTK
card_name('flanking troops', 'Flanking Troops').
card_type('flanking troops', 'Creature — Human Soldier').
card_types('flanking troops', ['Creature']).
card_subtypes('flanking troops', ['Human', 'Soldier']).
card_colors('flanking troops', ['W']).
card_text('flanking troops', 'Whenever Flanking Troops attacks, you may tap target creature.').
card_mana_cost('flanking troops', ['2', 'W', 'W']).
card_cmc('flanking troops', 4).
card_layout('flanking troops', 'normal').
card_power('flanking troops', 2).
card_toughness('flanking troops', 2).

% Found in: 5ED, ICE, MIR
card_name('flare', 'Flare').
card_type('flare', 'Instant').
card_types('flare', ['Instant']).
card_subtypes('flare', []).
card_colors('flare', ['R']).
card_text('flare', 'Flare deals 1 damage to target creature or player.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('flare', ['2', 'R']).
card_cmc('flare', 3).
card_layout('flare', 'normal').

% Found in: CNS, DIS
card_name('flaring flame-kin', 'Flaring Flame-Kin').
card_type('flaring flame-kin', 'Creature — Elemental Warrior').
card_types('flaring flame-kin', ['Creature']).
card_subtypes('flaring flame-kin', ['Elemental', 'Warrior']).
card_colors('flaring flame-kin', ['R']).
card_text('flaring flame-kin', 'As long as Flaring Flame-Kin is enchanted, it gets +2/+2, has trample, and has \"{R}: Flaring Flame-Kin gets +1/+0 until end of turn.\"').
card_mana_cost('flaring flame-kin', ['2', 'R']).
card_cmc('flaring flame-kin', 3).
card_layout('flaring flame-kin', 'normal').
card_power('flaring flame-kin', 2).
card_toughness('flaring flame-kin', 2).

% Found in: JUD
card_name('flaring pain', 'Flaring Pain').
card_type('flaring pain', 'Instant').
card_types('flaring pain', ['Instant']).
card_subtypes('flaring pain', []).
card_colors('flaring pain', ['R']).
card_text('flaring pain', 'Damage can\'t be prevented this turn.\nFlashback {R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('flaring pain', ['1', 'R']).
card_cmc('flaring pain', 2).
card_layout('flaring pain', 'normal').

% Found in: 6ED, MIR
card_name('flash', 'Flash').
card_type('flash', 'Instant').
card_types('flash', ['Instant']).
card_subtypes('flash', []).
card_colors('flash', ['U']).
card_text('flash', 'You may put a creature card from your hand onto the battlefield. If you do, sacrifice it unless you pay its mana cost reduced by up to {2}.').
card_mana_cost('flash', ['1', 'U']).
card_cmc('flash', 2).
card_layout('flash', 'normal').

% Found in: RAV
card_name('flash conscription', 'Flash Conscription').
card_type('flash conscription', 'Instant').
card_types('flash conscription', ['Instant']).
card_subtypes('flash conscription', []).
card_colors('flash conscription', ['R']).
card_text('flash conscription', 'Untap target creature and gain control of it until end of turn.  That creature gains haste until end of turn. If {W} was spent to cast Flash Conscription, the creature gains \"Whenever this creature deals combat damage, you gain that much life\" until end of turn.').
card_mana_cost('flash conscription', ['5', 'R']).
card_cmc('flash conscription', 6).
card_layout('flash conscription', 'normal').

% Found in: 8ED, LEG
card_name('flash counter', 'Flash Counter').
card_type('flash counter', 'Instant').
card_types('flash counter', ['Instant']).
card_subtypes('flash counter', []).
card_colors('flash counter', ['U']).
card_text('flash counter', 'Counter target instant spell.').
card_mana_cost('flash counter', ['1', 'U']).
card_cmc('flash counter', 2).
card_layout('flash counter', 'normal').

% Found in: CHR, LEG, ME3
card_name('flash flood', 'Flash Flood').
card_type('flash flood', 'Instant').
card_types('flash flood', ['Instant']).
card_subtypes('flash flood', []).
card_colors('flash flood', ['U']).
card_text('flash flood', 'Choose one —\n• Destroy target red permanent.\n• Return target Mountain to its owner\'s hand.').
card_mana_cost('flash flood', ['U']).
card_cmc('flash flood', 1).
card_layout('flash flood', 'normal').

% Found in: DIS
card_name('flash foliage', 'Flash Foliage').
card_type('flash foliage', 'Instant').
card_types('flash foliage', ['Instant']).
card_subtypes('flash foliage', []).
card_colors('flash foliage', ['G']).
card_text('flash foliage', 'Cast Flash Foliage only during combat after blockers are declared.\nPut a 1/1 green Saproling creature token onto the battlefield blocking target creature attacking you.\nDraw a card.').
card_mana_cost('flash foliage', ['2', 'G']).
card_cmc('flash foliage', 3).
card_layout('flash foliage', 'normal').

% Found in: TOR
card_name('flash of defiance', 'Flash of Defiance').
card_type('flash of defiance', 'Sorcery').
card_types('flash of defiance', ['Sorcery']).
card_subtypes('flash of defiance', []).
card_colors('flash of defiance', ['R']).
card_text('flash of defiance', 'Green creatures and white creatures can\'t block this turn.\nFlashback—{1}{R}, Pay 3 life. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('flash of defiance', ['1', 'R']).
card_cmc('flash of defiance', 2).
card_layout('flash of defiance', 'normal').

% Found in: JUD
card_name('flash of insight', 'Flash of Insight').
card_type('flash of insight', 'Instant').
card_types('flash of insight', ['Instant']).
card_subtypes('flash of insight', []).
card_colors('flash of insight', ['U']).
card_text('flash of insight', 'Look at the top X cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.\nFlashback—{1}{U}, Exile X blue cards from your graveyard. (You may cast this card from your graveyard for its flashback cost, then exile it. You can\'t exile Flash of Insight to pay for its own flashback cost.)').
card_mana_cost('flash of insight', ['X', '1', 'U']).
card_cmc('flash of insight', 2).
card_layout('flash of insight', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 8ED, 9ED, CED, CEI, LEA, LEB, POR
card_name('flashfires', 'Flashfires').
card_type('flashfires', 'Sorcery').
card_types('flashfires', ['Sorcery']).
card_subtypes('flashfires', []).
card_colors('flashfires', ['R']).
card_text('flashfires', 'Destroy all Plains.').
card_mana_cost('flashfires', ['3', 'R']).
card_cmc('flashfires', 4).
card_layout('flashfires', 'normal').

% Found in: 10E, CSP, M10, M11, M12, MM2
card_name('flashfreeze', 'Flashfreeze').
card_type('flashfreeze', 'Instant').
card_types('flashfreeze', ['Instant']).
card_subtypes('flashfreeze', []).
card_colors('flashfreeze', ['U']).
card_text('flashfreeze', 'Counter target red or green spell.').
card_mana_cost('flashfreeze', ['1', 'U']).
card_cmc('flashfreeze', 2).
card_layout('flashfreeze', 'normal').

% Found in: DTK
card_name('flatten', 'Flatten').
card_type('flatten', 'Instant').
card_types('flatten', ['Instant']).
card_subtypes('flatten', []).
card_colors('flatten', ['B']).
card_text('flatten', 'Target creature gets -4/-4 until end of turn.').
card_mana_cost('flatten', ['3', 'B']).
card_cmc('flatten', 4).
card_layout('flatten', 'normal').

% Found in: PCY
card_name('flay', 'Flay').
card_type('flay', 'Sorcery').
card_types('flay', ['Sorcery']).
card_subtypes('flay', []).
card_colors('flay', ['B']).
card_text('flay', 'Target player discards a card at random. Then that player discards another card at random unless he or she pays {1}.').
card_mana_cost('flay', ['3', 'B']).
card_cmc('flay', 4).
card_layout('flay', 'normal').

% Found in: MRD
card_name('flayed nim', 'Flayed Nim').
card_type('flayed nim', 'Creature — Skeleton').
card_types('flayed nim', ['Creature']).
card_subtypes('flayed nim', ['Skeleton']).
card_colors('flayed nim', ['B']).
card_text('flayed nim', 'Whenever Flayed Nim deals combat damage to a creature, that creature\'s controller loses that much life.\n{2}{B}: Regenerate Flayed Nim.').
card_mana_cost('flayed nim', ['3', 'B']).
card_cmc('flayed nim', 4).
card_layout('flayed nim', 'normal').
card_power('flayed nim', 2).
card_toughness('flayed nim', 2).

% Found in: MBS, MM2, PC2
card_name('flayer husk', 'Flayer Husk').
card_type('flayer husk', 'Artifact — Equipment').
card_types('flayer husk', ['Artifact']).
card_subtypes('flayer husk', ['Equipment']).
card_colors('flayer husk', []).
card_text('flayer husk', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +1/+1.\nEquip {2}').
card_mana_cost('flayer husk', ['1']).
card_cmc('flayer husk', 1).
card_layout('flayer husk', 'normal').

% Found in: DKA
card_name('flayer of the hatebound', 'Flayer of the Hatebound').
card_type('flayer of the hatebound', 'Creature — Devil').
card_types('flayer of the hatebound', ['Creature']).
card_subtypes('flayer of the hatebound', ['Devil']).
card_colors('flayer of the hatebound', ['R']).
card_text('flayer of the hatebound', 'Undying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)\nWhenever Flayer of the Hatebound or another creature enters the battlefield from your graveyard, that creature deals damage equal to its power to target creature or player.').
card_mana_cost('flayer of the hatebound', ['5', 'R']).
card_cmc('flayer of the hatebound', 6).
card_layout('flayer of the hatebound', 'normal').
card_power('flayer of the hatebound', 4).
card_toughness('flayer of the hatebound', 2).

% Found in: VMA, WTH
card_name('fledgling djinn', 'Fledgling Djinn').
card_type('fledgling djinn', 'Creature — Djinn').
card_types('fledgling djinn', ['Creature']).
card_subtypes('fledgling djinn', ['Djinn']).
card_colors('fledgling djinn', ['B']).
card_text('fledgling djinn', 'Flying\nAt the beginning of your upkeep, Fledgling Djinn deals 1 damage to you.').
card_mana_cost('fledgling djinn', ['1', 'B']).
card_cmc('fledgling djinn', 2).
card_layout('fledgling djinn', 'normal').
card_power('fledgling djinn', 2).
card_toughness('fledgling djinn', 2).

% Found in: JUD
card_name('fledgling dragon', 'Fledgling Dragon').
card_type('fledgling dragon', 'Creature — Dragon').
card_types('fledgling dragon', ['Creature']).
card_subtypes('fledgling dragon', ['Dragon']).
card_colors('fledgling dragon', ['R']).
card_text('fledgling dragon', 'Flying\nThreshold — As long as seven or more cards are in your graveyard, Fledgling Dragon gets +3/+3 and has \"{R}: Fledgling Dragon gets +1/+0 until end of turn.\"').
card_mana_cost('fledgling dragon', ['2', 'R', 'R']).
card_cmc('fledgling dragon', 4).
card_layout('fledgling dragon', 'normal').
card_power('fledgling dragon', 2).
card_toughness('fledgling dragon', 2).

% Found in: WWK
card_name('fledgling griffin', 'Fledgling Griffin').
card_type('fledgling griffin', 'Creature — Griffin').
card_types('fledgling griffin', ['Creature']).
card_subtypes('fledgling griffin', ['Griffin']).
card_colors('fledgling griffin', ['W']).
card_text('fledgling griffin', 'Landfall — Whenever a land enters the battlefield under your control, Fledgling Griffin gains flying until end of turn.').
card_mana_cost('fledgling griffin', ['1', 'W']).
card_cmc('fledgling griffin', 2).
card_layout('fledgling griffin', 'normal').
card_power('fledgling griffin', 2).
card_toughness('fledgling griffin', 2).

% Found in: ODY
card_name('fledgling imp', 'Fledgling Imp').
card_type('fledgling imp', 'Creature — Imp').
card_types('fledgling imp', ['Creature']).
card_subtypes('fledgling imp', ['Imp']).
card_colors('fledgling imp', ['B']).
card_text('fledgling imp', '{B}, Discard a card: Fledgling Imp gains flying until end of turn.').
card_mana_cost('fledgling imp', ['2', 'B']).
card_cmc('fledgling imp', 3).
card_layout('fledgling imp', 'normal').
card_power('fledgling imp', 2).
card_toughness('fledgling imp', 2).

% Found in: DD2, DD3_JVC, TSP
card_name('fledgling mawcor', 'Fledgling Mawcor').
card_type('fledgling mawcor', 'Creature — Beast').
card_types('fledgling mawcor', ['Creature']).
card_subtypes('fledgling mawcor', ['Beast']).
card_colors('fledgling mawcor', ['U']).
card_text('fledgling mawcor', 'Flying\n{T}: Fledgling Mawcor deals 1 damage to target creature or player.\nMorph {U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('fledgling mawcor', ['3', 'U']).
card_cmc('fledgling mawcor', 4).
card_layout('fledgling mawcor', 'normal').
card_power('fledgling mawcor', 2).
card_toughness('fledgling mawcor', 2).

% Found in: UDS
card_name('fledgling osprey', 'Fledgling Osprey').
card_type('fledgling osprey', 'Creature — Bird').
card_types('fledgling osprey', ['Creature']).
card_subtypes('fledgling osprey', ['Bird']).
card_colors('fledgling osprey', ['U']).
card_text('fledgling osprey', 'Fledgling Osprey has flying as long as it\'s enchanted.').
card_mana_cost('fledgling osprey', ['U']).
card_cmc('fledgling osprey', 1).
card_layout('fledgling osprey', 'normal').
card_power('fledgling osprey', 1).
card_toughness('fledgling osprey', 1).

% Found in: THS
card_name('fleecemane lion', 'Fleecemane Lion').
card_type('fleecemane lion', 'Creature — Cat').
card_types('fleecemane lion', ['Creature']).
card_subtypes('fleecemane lion', ['Cat']).
card_colors('fleecemane lion', ['W', 'G']).
card_text('fleecemane lion', '{3}{G}{W}: Monstrosity 1. (If this creature isn\'t monstrous, put a +1/+1 counter on it and it becomes monstrous.)\nAs long as Fleecemane Lion is monstrous, it has hexproof and indestructible.').
card_mana_cost('fleecemane lion', ['G', 'W']).
card_cmc('fleecemane lion', 2).
card_layout('fleecemane lion', 'normal').
card_power('fleecemane lion', 3).
card_toughness('fleecemane lion', 3).

% Found in: POR
card_name('fleet-footed monk', 'Fleet-Footed Monk').
card_type('fleet-footed monk', 'Creature — Human Monk').
card_types('fleet-footed monk', ['Creature']).
card_subtypes('fleet-footed monk', ['Human', 'Monk']).
card_colors('fleet-footed monk', ['W']).
card_text('fleet-footed monk', 'Fleet-Footed Monk can\'t be blocked by creatures with power 2 or greater.').
card_mana_cost('fleet-footed monk', ['1', 'W']).
card_cmc('fleet-footed monk', 2).
card_layout('fleet-footed monk', 'normal').
card_power('fleet-footed monk', 1).
card_toughness('fleet-footed monk', 1).

% Found in: JOU
card_name('fleetfeather cockatrice', 'Fleetfeather Cockatrice').
card_type('fleetfeather cockatrice', 'Creature — Cockatrice').
card_types('fleetfeather cockatrice', ['Creature']).
card_subtypes('fleetfeather cockatrice', ['Cockatrice']).
card_colors('fleetfeather cockatrice', ['U', 'G']).
card_text('fleetfeather cockatrice', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying, deathtouch\n{5}{G}{U}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)').
card_mana_cost('fleetfeather cockatrice', ['3', 'G', 'U']).
card_cmc('fleetfeather cockatrice', 5).
card_layout('fleetfeather cockatrice', 'normal').
card_power('fleetfeather cockatrice', 3).
card_toughness('fleetfeather cockatrice', 3).

% Found in: THS
card_name('fleetfeather sandals', 'Fleetfeather Sandals').
card_type('fleetfeather sandals', 'Artifact — Equipment').
card_types('fleetfeather sandals', ['Artifact']).
card_subtypes('fleetfeather sandals', ['Equipment']).
card_colors('fleetfeather sandals', []).
card_text('fleetfeather sandals', 'Equipped creature has flying and haste.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('fleetfeather sandals', ['2']).
card_cmc('fleetfeather sandals', 2).
card_layout('fleetfeather sandals', 'normal').

% Found in: DDH, PLS
card_name('fleetfoot panther', 'Fleetfoot Panther').
card_type('fleetfoot panther', 'Creature — Cat').
card_types('fleetfoot panther', ['Creature']).
card_subtypes('fleetfoot panther', ['Cat']).
card_colors('fleetfoot panther', ['W', 'G']).
card_text('fleetfoot panther', 'Flash\nWhen Fleetfoot Panther enters the battlefield, return a green or white creature you control to its owner\'s hand.').
card_mana_cost('fleetfoot panther', ['1', 'G', 'W']).
card_cmc('fleetfoot panther', 3).
card_layout('fleetfoot panther', 'normal').
card_power('fleetfoot panther', 3).
card_toughness('fleetfoot panther', 4).

% Found in: ONS
card_name('fleeting aven', 'Fleeting Aven').
card_type('fleeting aven', 'Creature — Bird Wizard').
card_types('fleeting aven', ['Creature']).
card_subtypes('fleeting aven', ['Bird', 'Wizard']).
card_colors('fleeting aven', ['U']).
card_text('fleeting aven', 'Flying\nWhenever a player cycles a card, return Fleeting Aven to its owner\'s hand.').
card_mana_cost('fleeting aven', ['1', 'U', 'U']).
card_cmc('fleeting aven', 3).
card_layout('fleeting aven', 'normal').
card_power('fleeting aven', 2).
card_toughness('fleeting aven', 2).

% Found in: AVR, DDN, ROE
card_name('fleeting distraction', 'Fleeting Distraction').
card_type('fleeting distraction', 'Instant').
card_types('fleeting distraction', ['Instant']).
card_subtypes('fleeting distraction', []).
card_colors('fleeting distraction', ['U']).
card_text('fleeting distraction', 'Target creature gets -1/-0 until end of turn.\nDraw a card.').
card_mana_cost('fleeting distraction', ['U']).
card_cmc('fleeting distraction', 1).
card_layout('fleeting distraction', 'normal').

% Found in: 7ED, 8ED, 9ED, ULG
card_name('fleeting image', 'Fleeting Image').
card_type('fleeting image', 'Creature — Illusion').
card_types('fleeting image', ['Creature']).
card_subtypes('fleeting image', ['Illusion']).
card_colors('fleeting image', ['U']).
card_text('fleeting image', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{1}{U}: Return Fleeting Image to its owner\'s hand.').
card_mana_cost('fleeting image', ['2', 'U']).
card_cmc('fleeting image', 3).
card_layout('fleeting image', 'normal').
card_power('fleeting image', 2).
card_toughness('fleeting image', 1).

% Found in: MBS
card_name('flensermite', 'Flensermite').
card_type('flensermite', 'Creature — Gremlin').
card_types('flensermite', ['Creature']).
card_subtypes('flensermite', ['Gremlin']).
card_colors('flensermite', ['B']).
card_text('flensermite', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('flensermite', ['1', 'B']).
card_cmc('flensermite', 2).
card_layout('flensermite', 'normal').
card_power('flensermite', 1).
card_toughness('flensermite', 1).

% Found in: DGM
card_name('flesh', 'Flesh').
card_type('flesh', 'Sorcery').
card_types('flesh', ['Sorcery']).
card_subtypes('flesh', []).
card_colors('flesh', ['B', 'G']).
card_text('flesh', 'Exile target creature card from a graveyard. Put X +1/+1 counters on target creature, where X is the power of the card you exiled.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('flesh', ['3', 'B', 'G']).
card_cmc('flesh', 5).
card_layout('flesh', 'split').
card_sides('flesh', 'blood').

% Found in: SOM
card_name('flesh allergy', 'Flesh Allergy').
card_type('flesh allergy', 'Sorcery').
card_types('flesh allergy', ['Sorcery']).
card_subtypes('flesh allergy', []).
card_colors('flesh allergy', ['B']).
card_text('flesh allergy', 'As an additional cost to cast Flesh Allergy, sacrifice a creature.\nDestroy target creature. Its controller loses life equal to the number of creatures that died this turn.').
card_mana_cost('flesh allergy', ['2', 'B', 'B']).
card_cmc('flesh allergy', 4).
card_layout('flesh allergy', 'normal').

% Found in: C14
card_name('flesh carver', 'Flesh Carver').
card_type('flesh carver', 'Creature — Human Wizard').
card_types('flesh carver', ['Creature']).
card_subtypes('flesh carver', ['Human', 'Wizard']).
card_colors('flesh carver', ['B']).
card_text('flesh carver', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\n{1}{B}, Sacrifice another creature: Put two +1/+1 counters on Flesh Carver.\nWhen Flesh Carver dies, put an X/X black Horror creature token onto the battlefield, where X is Flesh Carver\'s power.').
card_mana_cost('flesh carver', ['2', 'B']).
card_cmc('flesh carver', 3).
card_layout('flesh carver', 'normal').
card_power('flesh carver', 2).
card_toughness('flesh carver', 2).

% Found in: USG
card_name('flesh reaver', 'Flesh Reaver').
card_type('flesh reaver', 'Creature — Horror').
card_types('flesh reaver', ['Creature']).
card_subtypes('flesh reaver', ['Horror']).
card_colors('flesh reaver', ['B']).
card_text('flesh reaver', 'Whenever Flesh Reaver deals damage to a creature or opponent, Flesh Reaver deals that much damage to you.').
card_mana_cost('flesh reaver', ['1', 'B']).
card_cmc('flesh reaver', 2).
card_layout('flesh reaver', 'normal').
card_power('flesh reaver', 4).
card_toughness('flesh reaver', 4).

% Found in: M15, ORI
card_name('flesh to dust', 'Flesh to Dust').
card_type('flesh to dust', 'Instant').
card_types('flesh to dust', ['Instant']).
card_subtypes('flesh to dust', []).
card_colors('flesh to dust', ['B']).
card_text('flesh to dust', 'Destroy target creature. It can\'t be regenerated.').
card_mana_cost('flesh to dust', ['3', 'B', 'B']).
card_cmc('flesh to dust', 5).
card_layout('flesh to dust', 'normal').

% Found in: MBS
card_name('flesh-eater imp', 'Flesh-Eater Imp').
card_type('flesh-eater imp', 'Creature — Imp').
card_types('flesh-eater imp', ['Creature']).
card_subtypes('flesh-eater imp', ['Imp']).
card_colors('flesh-eater imp', ['B']).
card_text('flesh-eater imp', 'Flying\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nSacrifice a creature: Flesh-Eater Imp gets +1/+1 until end of turn.').
card_mana_cost('flesh-eater imp', ['3', 'B']).
card_cmc('flesh-eater imp', 4).
card_layout('flesh-eater imp', 'normal').
card_power('flesh-eater imp', 2).
card_toughness('flesh-eater imp', 2).

% Found in: ALA, CMD, DD3_GVL, DDD, DDN, ORI
card_name('fleshbag marauder', 'Fleshbag Marauder').
card_type('fleshbag marauder', 'Creature — Zombie Warrior').
card_types('fleshbag marauder', ['Creature']).
card_subtypes('fleshbag marauder', ['Zombie', 'Warrior']).
card_colors('fleshbag marauder', ['B']).
card_text('fleshbag marauder', 'When Fleshbag Marauder enters the battlefield, each player sacrifices a creature.').
card_mana_cost('fleshbag marauder', ['2', 'B']).
card_cmc('fleshbag marauder', 3).
card_layout('fleshbag marauder', 'normal').
card_power('fleshbag marauder', 3).
card_toughness('fleshbag marauder', 1).

% Found in: CON
card_name('fleshformer', 'Fleshformer').
card_type('fleshformer', 'Creature — Human Wizard').
card_types('fleshformer', ['Creature']).
card_subtypes('fleshformer', ['Human', 'Wizard']).
card_colors('fleshformer', ['B']).
card_text('fleshformer', '{W}{U}{B}{R}{G}: Fleshformer gets +2/+2 and gains fear until end of turn. Target creature gets -2/-2 until end of turn. Activate this ability only during your turn. (A creature with fear can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('fleshformer', ['2', 'B']).
card_cmc('fleshformer', 3).
card_layout('fleshformer', 'normal').
card_power('fleshformer', 2).
card_toughness('fleshformer', 2).

% Found in: 5DN
card_name('fleshgrafter', 'Fleshgrafter').
card_type('fleshgrafter', 'Creature — Human Warrior').
card_types('fleshgrafter', ['Creature']).
card_subtypes('fleshgrafter', ['Human', 'Warrior']).
card_colors('fleshgrafter', ['B']).
card_text('fleshgrafter', 'Discard an artifact card: Fleshgrafter gets +2/+2 until end of turn.').
card_mana_cost('fleshgrafter', ['2', 'B']).
card_cmc('fleshgrafter', 3).
card_layout('fleshgrafter', 'normal').
card_power('fleshgrafter', 2).
card_toughness('fleshgrafter', 2).

% Found in: THS
card_name('fleshmad steed', 'Fleshmad Steed').
card_type('fleshmad steed', 'Creature — Horse').
card_types('fleshmad steed', ['Creature']).
card_subtypes('fleshmad steed', ['Horse']).
card_colors('fleshmad steed', ['B']).
card_text('fleshmad steed', 'Whenever another creature dies, tap Fleshmad Steed.').
card_mana_cost('fleshmad steed', ['1', 'B']).
card_cmc('fleshmad steed', 2).
card_layout('fleshmad steed', 'normal').
card_power('fleshmad steed', 2).
card_toughness('fleshmad steed', 2).

% Found in: M14
card_name('fleshpulper giant', 'Fleshpulper Giant').
card_type('fleshpulper giant', 'Creature — Giant').
card_types('fleshpulper giant', ['Creature']).
card_subtypes('fleshpulper giant', ['Giant']).
card_colors('fleshpulper giant', ['R']).
card_text('fleshpulper giant', 'When Fleshpulper Giant enters the battlefield, you may destroy target creature with toughness 2 or less.').
card_mana_cost('fleshpulper giant', ['5', 'R', 'R']).
card_cmc('fleshpulper giant', 7).
card_layout('fleshpulper giant', 'normal').
card_power('fleshpulper giant', 4).
card_toughness('fleshpulper giant', 4).

% Found in: FUT
card_name('fleshwrither', 'Fleshwrither').
card_type('fleshwrither', 'Creature — Horror').
card_types('fleshwrither', ['Creature']).
card_subtypes('fleshwrither', ['Horror']).
card_colors('fleshwrither', ['B']).
card_text('fleshwrither', 'Transfigure {1}{B}{B} ({1}{B}{B}, Sacrifice this creature: Search your library for a creature card with the same converted mana cost as this creature and put that card onto the battlefield. Then shuffle your library. Transfigure only as a sorcery.)').
card_mana_cost('fleshwrither', ['2', 'B', 'B']).
card_cmc('fleshwrither', 4).
card_layout('fleshwrither', 'normal').
card_power('fleshwrither', 3).
card_toughness('fleshwrither', 3).

% Found in: UDS
card_name('flicker', 'Flicker').
card_type('flicker', 'Sorcery').
card_types('flicker', ['Sorcery']).
card_subtypes('flicker', []).
card_colors('flicker', ['W']).
card_text('flicker', 'Exile target nontoken permanent, then return it to the battlefield under its owner\'s control.').
card_mana_cost('flicker', ['1', 'W']).
card_cmc('flicker', 2).
card_layout('flicker', 'normal').

% Found in: C13, RAV
card_name('flickerform', 'Flickerform').
card_type('flickerform', 'Enchantment — Aura').
card_types('flickerform', ['Enchantment']).
card_subtypes('flickerform', ['Aura']).
card_colors('flickerform', ['W']).
card_text('flickerform', 'Enchant creature\n{2}{W}{W}: Exile enchanted creature and all Auras attached to it. At the beginning of the next end step, return that card to the battlefield under its owner\'s control. If you do, return the other cards exiled this way to the battlefield under their owners\' control attached to that creature.').
card_mana_cost('flickerform', ['1', 'W']).
card_cmc('flickerform', 2).
card_layout('flickerform', 'normal').

% Found in: TSP
card_name('flickering spirit', 'Flickering Spirit').
card_type('flickering spirit', 'Creature — Spirit').
card_types('flickering spirit', ['Creature']).
card_subtypes('flickering spirit', ['Spirit']).
card_colors('flickering spirit', ['W']).
card_text('flickering spirit', 'Flying\n{3}{W}: Exile Flickering Spirit, then return it to the battlefield under its owner\'s control.').
card_mana_cost('flickering spirit', ['3', 'W']).
card_cmc('flickering spirit', 4).
card_layout('flickering spirit', 'normal').
card_power('flickering spirit', 2).
card_toughness('flickering spirit', 2).

% Found in: TMP
card_name('flickering ward', 'Flickering Ward').
card_type('flickering ward', 'Enchantment — Aura').
card_types('flickering ward', ['Enchantment']).
card_subtypes('flickering ward', ['Aura']).
card_colors('flickering ward', ['W']).
card_text('flickering ward', 'Enchant creature\nAs Flickering Ward enters the battlefield, choose a color.\nEnchanted creature has protection from the chosen color. This effect doesn\'t remove Flickering Ward.\n{W}: Return Flickering Ward to its owner\'s hand.').
card_mana_cost('flickering ward', ['W']).
card_cmc('flickering ward', 1).
card_layout('flickering ward', 'normal').

% Found in: C13, C14, EVE, MMA
card_name('flickerwisp', 'Flickerwisp').
card_type('flickerwisp', 'Creature — Elemental').
card_types('flickerwisp', ['Creature']).
card_subtypes('flickerwisp', ['Elemental']).
card_colors('flickerwisp', ['W']).
card_text('flickerwisp', 'Flying\nWhen Flickerwisp enters the battlefield, exile another target permanent. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_mana_cost('flickerwisp', ['1', 'W', 'W']).
card_cmc('flickerwisp', 3).
card_layout('flickerwisp', 'normal').
card_power('flickerwisp', 3).
card_toughness('flickerwisp', 1).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, CED, CEI, LEA, LEB, M12, S00
card_name('flight', 'Flight').
card_type('flight', 'Enchantment — Aura').
card_types('flight', ['Enchantment']).
card_subtypes('flight', ['Aura']).
card_colors('flight', ['U']).
card_text('flight', 'Enchant creature\nEnchanted creature has flying.').
card_mana_cost('flight', ['U']).
card_cmc('flight', 1).
card_layout('flight', 'normal').

% Found in: RAV
card_name('flight of fancy', 'Flight of Fancy').
card_type('flight of fancy', 'Enchantment — Aura').
card_types('flight of fancy', ['Enchantment']).
card_subtypes('flight of fancy', ['Aura']).
card_colors('flight of fancy', ['U']).
card_text('flight of fancy', 'Enchant creature\nWhen Flight of Fancy enters the battlefield, draw two cards.\nEnchanted creature has flying.').
card_mana_cost('flight of fancy', ['3', 'U']).
card_cmc('flight of fancy', 4).
card_layout('flight of fancy', 'normal').

% Found in: SOM
card_name('flight spellbomb', 'Flight Spellbomb').
card_type('flight spellbomb', 'Artifact').
card_types('flight spellbomb', ['Artifact']).
card_subtypes('flight spellbomb', []).
card_colors('flight spellbomb', []).
card_text('flight spellbomb', '{T}, Sacrifice Flight Spellbomb: Target creature gains flying until end of turn.\nWhen Flight Spellbomb is put into a graveyard from the battlefield, you may pay {U}. If you do, draw a card.').
card_mana_cost('flight spellbomb', ['1']).
card_cmc('flight spellbomb', 1).
card_layout('flight spellbomb', 'normal').

% Found in: DKA, M11, M12, PC2, STH, pARL, pWPN
card_name('fling', 'Fling').
card_type('fling', 'Instant').
card_types('fling', ['Instant']).
card_subtypes('fling', []).
card_colors('fling', ['R']).
card_text('fling', 'As an additional cost to cast Fling, sacrifice a creature.\nFling deals damage equal to the sacrificed creature\'s power to target creature or player.').
card_mana_cost('fling', ['1', 'R']).
card_cmc('fling', 2).
card_layout('fling', 'normal').

% Found in: NMS
card_name('flint golem', 'Flint Golem').
card_type('flint golem', 'Artifact Creature — Golem').
card_types('flint golem', ['Artifact', 'Creature']).
card_subtypes('flint golem', ['Golem']).
card_colors('flint golem', []).
card_text('flint golem', 'Whenever Flint Golem becomes blocked, defending player puts the top three cards of his or her library into his or her graveyard.').
card_mana_cost('flint golem', ['4']).
card_cmc('flint golem', 4).
card_layout('flint golem', 'normal').
card_power('flint golem', 2).
card_toughness('flint golem', 3).

% Found in: M13
card_name('flinthoof boar', 'Flinthoof Boar').
card_type('flinthoof boar', 'Creature — Boar').
card_types('flinthoof boar', ['Creature']).
card_subtypes('flinthoof boar', ['Boar']).
card_colors('flinthoof boar', ['G']).
card_text('flinthoof boar', 'Flinthoof Boar gets +1/+1 as long as you control a Mountain.\n{R}: Flinthoof Boar gains haste until end of turn. (It can attack and {T} this turn.)').
card_mana_cost('flinthoof boar', ['1', 'G']).
card_cmc('flinthoof boar', 2).
card_layout('flinthoof boar', 'normal').
card_power('flinthoof boar', 2).
card_toughness('flinthoof boar', 2).

% Found in: BNG
card_name('flitterstep eidolon', 'Flitterstep Eidolon').
card_type('flitterstep eidolon', 'Enchantment Creature — Spirit').
card_types('flitterstep eidolon', ['Enchantment', 'Creature']).
card_subtypes('flitterstep eidolon', ['Spirit']).
card_colors('flitterstep eidolon', ['U']).
card_text('flitterstep eidolon', 'Bestow {5}{U} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nFlitterstep Eidolon can\'t be blocked.\nEnchanted creature gets +1/+1 and can\'t be blocked.').
card_mana_cost('flitterstep eidolon', ['1', 'U']).
card_cmc('flitterstep eidolon', 2).
card_layout('flitterstep eidolon', 'normal').
card_power('flitterstep eidolon', 1).
card_toughness('flitterstep eidolon', 1).

% Found in: TOR
card_name('floating shield', 'Floating Shield').
card_type('floating shield', 'Enchantment — Aura').
card_types('floating shield', ['Enchantment']).
card_subtypes('floating shield', ['Aura']).
card_colors('floating shield', ['W']).
card_text('floating shield', 'Enchant creature\nAs Floating Shield enters the battlefield, choose a color.\nEnchanted creature has protection from the chosen color. This effect doesn\'t remove Floating Shield.\nSacrifice Floating Shield: Target creature gains protection from the chosen color until end of turn.').
card_mana_cost('floating shield', ['2', 'W']).
card_cmc('floating shield', 3).
card_layout('floating shield', 'normal').

% Found in: CHK
card_name('floating-dream zubera', 'Floating-Dream Zubera').
card_type('floating-dream zubera', 'Creature — Zubera Spirit').
card_types('floating-dream zubera', ['Creature']).
card_subtypes('floating-dream zubera', ['Zubera', 'Spirit']).
card_colors('floating-dream zubera', ['U']).
card_text('floating-dream zubera', 'When Floating-Dream Zubera dies, draw a card for each Zubera that died this turn.').
card_mana_cost('floating-dream zubera', ['1', 'U']).
card_cmc('floating-dream zubera', 2).
card_layout('floating-dream zubera', 'normal').
card_power('floating-dream zubera', 1).
card_toughness('floating-dream zubera', 2).

% Found in: UGL
card_name('flock of rabid sheep', 'Flock of Rabid Sheep').
card_type('flock of rabid sheep', 'Sorcery').
card_types('flock of rabid sheep', ['Sorcery']).
card_subtypes('flock of rabid sheep', []).
card_colors('flock of rabid sheep', ['G']).
card_text('flock of rabid sheep', 'Flip X coins; an opponent calls heads or tails. For each flip you win, put a Rabid Sheep token into play. Treat these tokens as 2/2 green creatures that count as Sheep.').
card_mana_cost('flock of rabid sheep', ['X', 'G', 'G']).
card_cmc('flock of rabid sheep', 2).
card_layout('flock of rabid sheep', 'normal').

% Found in: 4ED, 5ED, BRB, DRK
card_name('flood', 'Flood').
card_type('flood', 'Enchantment').
card_types('flood', ['Enchantment']).
card_subtypes('flood', []).
card_colors('flood', ['U']).
card_text('flood', '{U}{U}: Tap target creature without flying.').
card_mana_cost('flood', ['U']).
card_cmc('flood', 1).
card_layout('flood', 'normal').

% Found in: DDI, MIR, VMA
card_name('flood plain', 'Flood Plain').
card_type('flood plain', 'Land').
card_types('flood plain', ['Land']).
card_subtypes('flood plain', []).
card_colors('flood plain', []).
card_text('flood plain', 'Flood Plain enters the battlefield tapped.\n{T}, Sacrifice Flood Plain: Search your library for a Plains or Island card and put it onto the battlefield. Then shuffle your library.').
card_layout('flood plain', 'normal').

% Found in: BOK
card_name('floodbringer', 'Floodbringer').
card_type('floodbringer', 'Creature — Moonfolk Wizard').
card_types('floodbringer', ['Creature']).
card_subtypes('floodbringer', ['Moonfolk', 'Wizard']).
card_colors('floodbringer', ['U']).
card_text('floodbringer', 'Flying\n{2}, Return a land you control to its owner\'s hand: Tap target land.').
card_mana_cost('floodbringer', ['1', 'U']).
card_cmc('floodbringer', 2).
card_layout('floodbringer', 'normal').
card_power('floodbringer', 1).
card_toughness('floodbringer', 2).

% Found in: MOR
card_name('floodchaser', 'Floodchaser').
card_type('floodchaser', 'Creature — Elemental').
card_types('floodchaser', ['Creature']).
card_subtypes('floodchaser', ['Elemental']).
card_colors('floodchaser', ['U']).
card_text('floodchaser', 'Floodchaser enters the battlefield with six +1/+1 counters on it.\nFloodchaser can\'t attack unless defending player controls an Island.\n{U}, Remove a +1/+1 counter from Floodchaser: Target land becomes an Island until end of turn.').
card_mana_cost('floodchaser', ['5', 'U']).
card_cmc('floodchaser', 6).
card_layout('floodchaser', 'normal').
card_power('floodchaser', 0).
card_toughness('floodchaser', 0).

% Found in: EVE
card_name('flooded grove', 'Flooded Grove').
card_type('flooded grove', 'Land').
card_types('flooded grove', ['Land']).
card_subtypes('flooded grove', []).
card_colors('flooded grove', []).
card_text('flooded grove', '{T}: Add {1} to your mana pool.\n{G/U}, {T}: Add {G}{G}, {G}{U}, or {U}{U} to your mana pool.').
card_layout('flooded grove', 'normal').

% Found in: VIS
card_name('flooded shoreline', 'Flooded Shoreline').
card_type('flooded shoreline', 'Enchantment').
card_types('flooded shoreline', ['Enchantment']).
card_subtypes('flooded shoreline', []).
card_colors('flooded shoreline', ['U']).
card_text('flooded shoreline', '{U}{U}, Return two Islands you control to their owner\'s hand: Return target creature to its owner\'s hand.').
card_mana_cost('flooded shoreline', ['U', 'U']).
card_cmc('flooded shoreline', 2).
card_layout('flooded shoreline', 'normal').
card_reserved('flooded shoreline').

% Found in: EXP, KTK, ONS, pJGP
card_name('flooded strand', 'Flooded Strand').
card_type('flooded strand', 'Land').
card_types('flooded strand', ['Land']).
card_subtypes('flooded strand', []).
card_colors('flooded strand', []).
card_text('flooded strand', '{T}, Pay 1 life, Sacrifice Flooded Strand: Search your library for a Plains or Island card and put it onto the battlefield. Then shuffle your library.').
card_layout('flooded strand', 'normal').

% Found in: ICE
card_name('flooded woodlands', 'Flooded Woodlands').
card_type('flooded woodlands', 'Enchantment').
card_types('flooded woodlands', ['Enchantment']).
card_subtypes('flooded woodlands', []).
card_colors('flooded woodlands', ['U', 'B']).
card_text('flooded woodlands', 'Green creatures can\'t attack unless their controller sacrifices a land for each green creature he or she controls that\'s attacking. (This cost is paid as attackers are declared.)').
card_mana_cost('flooded woodlands', ['2', 'U', 'B']).
card_cmc('flooded woodlands', 4).
card_layout('flooded woodlands', 'normal').

% Found in: MIR
card_name('floodgate', 'Floodgate').
card_type('floodgate', 'Creature — Wall').
card_types('floodgate', ['Creature']).
card_subtypes('floodgate', ['Wall']).
card_colors('floodgate', ['U']).
card_text('floodgate', 'Defender (This creature can\'t attack.)\nWhen Floodgate has flying, sacrifice it.\nWhen Floodgate leaves the battlefield, it deals damage equal to half the number of Islands you control, rounded down, to each nonblue creature without flying.').
card_mana_cost('floodgate', ['3', 'U']).
card_cmc('floodgate', 4).
card_layout('floodgate', 'normal').
card_power('floodgate', 0).
card_toughness('floodgate', 5).

% Found in: BNG
card_name('floodtide serpent', 'Floodtide Serpent').
card_type('floodtide serpent', 'Creature — Serpent').
card_types('floodtide serpent', ['Creature']).
card_subtypes('floodtide serpent', ['Serpent']).
card_colors('floodtide serpent', ['U']).
card_text('floodtide serpent', 'Floodtide Serpent can\'t attack unless you return an enchantment you control to its owner\'s hand. (This cost is paid as attackers are declared.)').
card_mana_cost('floodtide serpent', ['4', 'U']).
card_cmc('floodtide serpent', 5).
card_layout('floodtide serpent', 'normal').
card_power('floodtide serpent', 4).
card_toughness('floodtide serpent', 4).

% Found in: ALL, ME4
card_name('floodwater dam', 'Floodwater Dam').
card_type('floodwater dam', 'Artifact').
card_types('floodwater dam', ['Artifact']).
card_subtypes('floodwater dam', []).
card_colors('floodwater dam', []).
card_text('floodwater dam', '{X}{X}{1}, {T}: Tap X target lands.').
card_mana_cost('floodwater dam', ['3']).
card_cmc('floodwater dam', 3).
card_layout('floodwater dam', 'normal').
card_reserved('floodwater dam').

% Found in: LEG
card_name('floral spuzzem', 'Floral Spuzzem').
card_type('floral spuzzem', 'Creature — Elemental').
card_types('floral spuzzem', ['Creature']).
card_subtypes('floral spuzzem', ['Elemental']).
card_colors('floral spuzzem', ['G']).
card_text('floral spuzzem', 'Whenever Floral Spuzzem attacks and isn\'t blocked, you may destroy target artifact defending player controls. If you do, Floral Spuzzem assigns no combat damage this turn.').
card_mana_cost('floral spuzzem', ['3', 'G']).
card_cmc('floral spuzzem', 4).
card_layout('floral spuzzem', 'normal').
card_power('floral spuzzem', 2).
card_toughness('floral spuzzem', 2).

% Found in: SHM
card_name('flourishing defenses', 'Flourishing Defenses').
card_type('flourishing defenses', 'Enchantment').
card_types('flourishing defenses', ['Enchantment']).
card_subtypes('flourishing defenses', []).
card_colors('flourishing defenses', ['G']).
card_text('flourishing defenses', 'Whenever a -1/-1 counter is placed on a creature, you may put a 1/1 green Elf Warrior creature token onto the battlefield.').
card_mana_cost('flourishing defenses', ['4', 'G']).
card_cmc('flourishing defenses', 5).
card_layout('flourishing defenses', 'normal').

% Found in: RAV, SHM
card_name('flow of ideas', 'Flow of Ideas').
card_type('flow of ideas', 'Sorcery').
card_types('flow of ideas', ['Sorcery']).
card_subtypes('flow of ideas', []).
card_colors('flow of ideas', ['U']).
card_text('flow of ideas', 'Draw a card for each Island you control.').
card_mana_cost('flow of ideas', ['5', 'U']).
card_cmc('flow of ideas', 6).
card_layout('flow of ideas', 'normal').

% Found in: ICE
card_name('flow of maggots', 'Flow of Maggots').
card_type('flow of maggots', 'Creature — Insect').
card_types('flow of maggots', ['Creature']).
card_subtypes('flow of maggots', ['Insect']).
card_colors('flow of maggots', ['B']).
card_text('flow of maggots', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nFlow of Maggots can\'t be blocked by non-Wall creatures.').
card_mana_cost('flow of maggots', ['2', 'B']).
card_cmc('flow of maggots', 3).
card_layout('flow of maggots', 'normal').
card_power('flow of maggots', 2).
card_toughness('flow of maggots', 2).
card_reserved('flow of maggots').

% Found in: PCY
card_name('flowering field', 'Flowering Field').
card_type('flowering field', 'Enchantment — Aura').
card_types('flowering field', ['Enchantment']).
card_subtypes('flowering field', ['Aura']).
card_colors('flowering field', ['W']).
card_text('flowering field', 'Enchant land\nEnchanted land has \"{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\"').
card_mana_cost('flowering field', ['1', 'W']).
card_cmc('flowering field', 2).
card_layout('flowering field', 'normal').

% Found in: AVR
card_name('flowering lumberknot', 'Flowering Lumberknot').
card_type('flowering lumberknot', 'Creature — Treefolk').
card_types('flowering lumberknot', ['Creature']).
card_subtypes('flowering lumberknot', ['Treefolk']).
card_colors('flowering lumberknot', ['G']).
card_text('flowering lumberknot', 'Flowering Lumberknot can\'t attack or block unless it\'s paired with a creature with soulbond.').
card_mana_cost('flowering lumberknot', ['3', 'G']).
card_cmc('flowering lumberknot', 4).
card_layout('flowering lumberknot', 'normal').
card_power('flowering lumberknot', 5).
card_toughness('flowering lumberknot', 5).

% Found in: NMS
card_name('flowstone armor', 'Flowstone Armor').
card_type('flowstone armor', 'Artifact').
card_types('flowstone armor', ['Artifact']).
card_subtypes('flowstone armor', []).
card_colors('flowstone armor', []).
card_text('flowstone armor', 'You may choose not to untap Flowstone Armor during your untap step.\n{3}, {T}: Target creature gets +1/-1 for as long as Flowstone Armor remains tapped.').
card_mana_cost('flowstone armor', ['3']).
card_cmc('flowstone armor', 3).
card_layout('flowstone armor', 'normal').

% Found in: CNS, STH, TPR
card_name('flowstone blade', 'Flowstone Blade').
card_type('flowstone blade', 'Enchantment — Aura').
card_types('flowstone blade', ['Enchantment']).
card_subtypes('flowstone blade', ['Aura']).
card_colors('flowstone blade', ['R']).
card_text('flowstone blade', 'Enchant creature\n{R}: Enchanted creature gets +1/-1 until end of turn.').
card_mana_cost('flowstone blade', ['R']).
card_cmc('flowstone blade', 1).
card_layout('flowstone blade', 'normal').

% Found in: TSP
card_name('flowstone channeler', 'Flowstone Channeler').
card_type('flowstone channeler', 'Creature — Human Spellshaper').
card_types('flowstone channeler', ['Creature']).
card_subtypes('flowstone channeler', ['Human', 'Spellshaper']).
card_colors('flowstone channeler', ['R']).
card_text('flowstone channeler', '{1}{R}, {T}, Discard a card: Target creature gets +1/-1 and gains haste until end of turn.').
card_mana_cost('flowstone channeler', ['2', 'R']).
card_cmc('flowstone channeler', 3).
card_layout('flowstone channeler', 'normal').
card_power('flowstone channeler', 2).
card_toughness('flowstone channeler', 2).

% Found in: APC
card_name('flowstone charger', 'Flowstone Charger').
card_type('flowstone charger', 'Creature — Beast').
card_types('flowstone charger', ['Creature']).
card_subtypes('flowstone charger', ['Beast']).
card_colors('flowstone charger', ['W', 'R']).
card_text('flowstone charger', 'Whenever Flowstone Charger attacks, it gets +3/-3 until end of turn.').
card_mana_cost('flowstone charger', ['2', 'R', 'W']).
card_cmc('flowstone charger', 4).
card_layout('flowstone charger', 'normal').
card_power('flowstone charger', 2).
card_toughness('flowstone charger', 5).

% Found in: 9ED, NMS
card_name('flowstone crusher', 'Flowstone Crusher').
card_type('flowstone crusher', 'Creature — Beast').
card_types('flowstone crusher', ['Creature']).
card_subtypes('flowstone crusher', ['Beast']).
card_colors('flowstone crusher', ['R']).
card_text('flowstone crusher', '{R}: Flowstone Crusher gets +1/-1 until end of turn.').
card_mana_cost('flowstone crusher', ['3', 'R', 'R']).
card_cmc('flowstone crusher', 5).
card_layout('flowstone crusher', 'normal').
card_power('flowstone crusher', 4).
card_toughness('flowstone crusher', 4).

% Found in: FUT
card_name('flowstone embrace', 'Flowstone Embrace').
card_type('flowstone embrace', 'Enchantment — Aura').
card_types('flowstone embrace', ['Enchantment']).
card_subtypes('flowstone embrace', ['Aura']).
card_colors('flowstone embrace', ['R']).
card_text('flowstone embrace', 'Enchant creature\n{T}: Enchanted creature gets +2/-2 until end of turn.').
card_mana_cost('flowstone embrace', ['1', 'R']).
card_cmc('flowstone embrace', 2).
card_layout('flowstone embrace', 'normal').

% Found in: EXO
card_name('flowstone flood', 'Flowstone Flood').
card_type('flowstone flood', 'Sorcery').
card_types('flowstone flood', ['Sorcery']).
card_subtypes('flowstone flood', []).
card_colors('flowstone flood', ['R']).
card_text('flowstone flood', 'Buyback—Pay 3 life, Discard a card at random. (You may pay 3 life and discard a card at random in addition to any other costs as you cast this spell. If you do, put this card into your hand as it resolves.)\nDestroy target land.').
card_mana_cost('flowstone flood', ['3', 'R']).
card_cmc('flowstone flood', 4).
card_layout('flowstone flood', 'normal').

% Found in: TMP
card_name('flowstone giant', 'Flowstone Giant').
card_type('flowstone giant', 'Creature — Giant').
card_types('flowstone giant', ['Creature']).
card_subtypes('flowstone giant', ['Giant']).
card_colors('flowstone giant', ['R']).
card_text('flowstone giant', '{R}: Flowstone Giant gets +2/-2 until end of turn.').
card_mana_cost('flowstone giant', ['2', 'R', 'R']).
card_cmc('flowstone giant', 4).
card_layout('flowstone giant', 'normal').
card_power('flowstone giant', 3).
card_toughness('flowstone giant', 3).

% Found in: STH, VMA
card_name('flowstone hellion', 'Flowstone Hellion').
card_type('flowstone hellion', 'Creature — Hellion Beast').
card_types('flowstone hellion', ['Creature']).
card_subtypes('flowstone hellion', ['Hellion', 'Beast']).
card_colors('flowstone hellion', ['R']).
card_text('flowstone hellion', 'Haste\n{0}: Flowstone Hellion gets +1/-1 until end of turn.').
card_mana_cost('flowstone hellion', ['4', 'R']).
card_cmc('flowstone hellion', 5).
card_layout('flowstone hellion', 'normal').
card_power('flowstone hellion', 3).
card_toughness('flowstone hellion', 3).

% Found in: STH, TPR
card_name('flowstone mauler', 'Flowstone Mauler').
card_type('flowstone mauler', 'Creature — Beast').
card_types('flowstone mauler', ['Creature']).
card_subtypes('flowstone mauler', ['Beast']).
card_colors('flowstone mauler', ['R']).
card_text('flowstone mauler', 'Trample\n{R}: Flowstone Mauler gets +1/-1 until end of turn.').
card_mana_cost('flowstone mauler', ['4', 'R', 'R']).
card_cmc('flowstone mauler', 6).
card_layout('flowstone mauler', 'normal').
card_power('flowstone mauler', 4).
card_toughness('flowstone mauler', 5).

% Found in: NMS
card_name('flowstone overseer', 'Flowstone Overseer').
card_type('flowstone overseer', 'Creature — Beast').
card_types('flowstone overseer', ['Creature']).
card_subtypes('flowstone overseer', ['Beast']).
card_colors('flowstone overseer', ['R']).
card_text('flowstone overseer', '{R}{R}: Target creature gets +1/-1 until end of turn.').
card_mana_cost('flowstone overseer', ['2', 'R', 'R', 'R']).
card_cmc('flowstone overseer', 5).
card_layout('flowstone overseer', 'normal').
card_power('flowstone overseer', 4).
card_toughness('flowstone overseer', 4).

% Found in: TMP
card_name('flowstone salamander', 'Flowstone Salamander').
card_type('flowstone salamander', 'Creature — Salamander').
card_types('flowstone salamander', ['Creature']).
card_subtypes('flowstone salamander', ['Salamander']).
card_colors('flowstone salamander', ['R']).
card_text('flowstone salamander', '{R}: Flowstone Salamander deals 1 damage to target creature blocking it.').
card_mana_cost('flowstone salamander', ['3', 'R', 'R']).
card_cmc('flowstone salamander', 5).
card_layout('flowstone salamander', 'normal').
card_power('flowstone salamander', 3).
card_toughness('flowstone salamander', 4).

% Found in: TMP, VMA
card_name('flowstone sculpture', 'Flowstone Sculpture').
card_type('flowstone sculpture', 'Artifact Creature — Shapeshifter').
card_types('flowstone sculpture', ['Artifact', 'Creature']).
card_subtypes('flowstone sculpture', ['Shapeshifter']).
card_colors('flowstone sculpture', []).
card_text('flowstone sculpture', '{2}, Discard a card: Put a +1/+1 counter on Flowstone Sculpture or Flowstone Sculpture gains flying, first strike, or trample. (This effect lasts indefinitely.)').
card_mana_cost('flowstone sculpture', ['6']).
card_cmc('flowstone sculpture', 6).
card_layout('flowstone sculpture', 'normal').
card_power('flowstone sculpture', 4).
card_toughness('flowstone sculpture', 4).

% Found in: 9ED, STH
card_name('flowstone shambler', 'Flowstone Shambler').
card_type('flowstone shambler', 'Creature — Beast').
card_types('flowstone shambler', ['Creature']).
card_subtypes('flowstone shambler', ['Beast']).
card_colors('flowstone shambler', ['R']).
card_text('flowstone shambler', '{R}: Flowstone Shambler gets +1/-1 until end of turn.').
card_mana_cost('flowstone shambler', ['2', 'R']).
card_cmc('flowstone shambler', 3).
card_layout('flowstone shambler', 'normal').
card_power('flowstone shambler', 2).
card_toughness('flowstone shambler', 2).

% Found in: 10E, 9ED, NMS
card_name('flowstone slide', 'Flowstone Slide').
card_type('flowstone slide', 'Sorcery').
card_types('flowstone slide', ['Sorcery']).
card_subtypes('flowstone slide', []).
card_colors('flowstone slide', ['R']).
card_text('flowstone slide', 'All creatures get +X/-X until end of turn.').
card_mana_cost('flowstone slide', ['X', '2', 'R', 'R']).
card_cmc('flowstone slide', 4).
card_layout('flowstone slide', 'normal').

% Found in: NMS
card_name('flowstone strike', 'Flowstone Strike').
card_type('flowstone strike', 'Instant').
card_types('flowstone strike', ['Instant']).
card_subtypes('flowstone strike', []).
card_colors('flowstone strike', ['R']).
card_text('flowstone strike', 'Target creature gets +1/-1 and gains haste until end of turn.').
card_mana_cost('flowstone strike', ['1', 'R']).
card_cmc('flowstone strike', 2).
card_layout('flowstone strike', 'normal').

% Found in: NMS
card_name('flowstone surge', 'Flowstone Surge').
card_type('flowstone surge', 'Enchantment').
card_types('flowstone surge', ['Enchantment']).
card_subtypes('flowstone surge', []).
card_colors('flowstone surge', ['R']).
card_text('flowstone surge', 'Creatures you control get +1/-1.').
card_mana_cost('flowstone surge', ['1', 'R']).
card_cmc('flowstone surge', 2).
card_layout('flowstone surge', 'normal').

% Found in: NMS
card_name('flowstone thopter', 'Flowstone Thopter').
card_type('flowstone thopter', 'Artifact Creature — Thopter').
card_types('flowstone thopter', ['Artifact', 'Creature']).
card_subtypes('flowstone thopter', ['Thopter']).
card_colors('flowstone thopter', []).
card_text('flowstone thopter', '{1}: Flowstone Thopter gets +1/-1 and gains flying until end of turn.').
card_mana_cost('flowstone thopter', ['7']).
card_cmc('flowstone thopter', 7).
card_layout('flowstone thopter', 'normal').
card_power('flowstone thopter', 4).
card_toughness('flowstone thopter', 4).

% Found in: NMS
card_name('flowstone wall', 'Flowstone Wall').
card_type('flowstone wall', 'Creature — Wall').
card_types('flowstone wall', ['Creature']).
card_subtypes('flowstone wall', ['Wall']).
card_colors('flowstone wall', ['R']).
card_text('flowstone wall', 'Defender (This creature can\'t attack.)\n{R}: Flowstone Wall gets +1/-1 until end of turn.').
card_mana_cost('flowstone wall', ['2', 'R']).
card_cmc('flowstone wall', 3).
card_layout('flowstone wall', 'normal').
card_power('flowstone wall', 0).
card_toughness('flowstone wall', 6).

% Found in: TMP, TPR
card_name('flowstone wyvern', 'Flowstone Wyvern').
card_type('flowstone wyvern', 'Creature — Drake').
card_types('flowstone wyvern', ['Creature']).
card_subtypes('flowstone wyvern', ['Drake']).
card_colors('flowstone wyvern', ['R']).
card_text('flowstone wyvern', 'Flying\n{R}: Flowstone Wyvern gets +2/-2 until end of turn.').
card_mana_cost('flowstone wyvern', ['3', 'R', 'R']).
card_cmc('flowstone wyvern', 5).
card_layout('flowstone wyvern', 'normal').
card_power('flowstone wyvern', 3).
card_toughness('flowstone wyvern', 3).

% Found in: USG
card_name('fluctuator', 'Fluctuator').
card_type('fluctuator', 'Artifact').
card_types('fluctuator', ['Artifact']).
card_subtypes('fluctuator', []).
card_colors('fluctuator', []).
card_text('fluctuator', 'Cycling abilities you activate cost you up to {2} less to activate.').
card_mana_cost('fluctuator', ['2']).
card_cmc('fluctuator', 2).
card_layout('fluctuator', 'normal').

% Found in: JOU
card_name('flurry of horns', 'Flurry of Horns').
card_type('flurry of horns', 'Sorcery').
card_types('flurry of horns', ['Sorcery']).
card_subtypes('flurry of horns', []).
card_colors('flurry of horns', ['R']).
card_text('flurry of horns', 'Put two 2/3 red Minotaur creature tokens with haste onto the battlefield.').
card_mana_cost('flurry of horns', ['4', 'R']).
card_cmc('flurry of horns', 5).
card_layout('flurry of horns', 'normal').

% Found in: ARB
card_name('flurry of wings', 'Flurry of Wings').
card_type('flurry of wings', 'Instant').
card_types('flurry of wings', ['Instant']).
card_subtypes('flurry of wings', []).
card_colors('flurry of wings', ['W', 'U', 'G']).
card_text('flurry of wings', 'Put X 1/1 white Bird Soldier creature tokens with flying onto the battlefield, where X is the number of attacking creatures.').
card_mana_cost('flurry of wings', ['G', 'W', 'U']).
card_cmc('flurry of wings', 3).
card_layout('flurry of wings', 'normal').

% Found in: CMD, VMA, pJGP
card_name('flusterstorm', 'Flusterstorm').
card_type('flusterstorm', 'Instant').
card_types('flusterstorm', ['Instant']).
card_subtypes('flusterstorm', []).
card_colors('flusterstorm', ['U']).
card_text('flusterstorm', 'Counter target instant or sorcery spell unless its controller pays {1}.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_mana_cost('flusterstorm', ['U']).
card_cmc('flusterstorm', 1).
card_layout('flusterstorm', 'normal').

% Found in: POR, WTH
card_name('flux', 'Flux').
card_type('flux', 'Sorcery').
card_types('flux', ['Sorcery']).
card_subtypes('flux', []).
card_colors('flux', ['U']).
card_text('flux', 'Each player discards any number of cards, then draws that many cards.\nDraw a card.').
card_mana_cost('flux', ['2', 'U']).
card_cmc('flux', 3).
card_layout('flux', 'normal').

% Found in: DGM
card_name('fluxcharger', 'Fluxcharger').
card_type('fluxcharger', 'Creature — Weird').
card_types('fluxcharger', ['Creature']).
card_subtypes('fluxcharger', ['Weird']).
card_colors('fluxcharger', ['U', 'R']).
card_text('fluxcharger', 'Flying\nWhenever you cast an instant or sorcery spell, you may switch Fluxcharger\'s power and toughness until end of turn.').
card_mana_cost('fluxcharger', ['2', 'U', 'R']).
card_cmc('fluxcharger', 4).
card_layout('fluxcharger', 'normal').
card_power('fluxcharger', 1).
card_toughness('fluxcharger', 5).

% Found in: 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, ARN, ME4
card_name('flying carpet', 'Flying Carpet').
card_type('flying carpet', 'Artifact').
card_types('flying carpet', ['Artifact']).
card_subtypes('flying carpet', []).
card_colors('flying carpet', []).
card_text('flying carpet', '{2}, {T}: Target creature gains flying until end of turn.').
card_mana_cost('flying carpet', ['4']).
card_cmc('flying carpet', 4).
card_layout('flying carpet', 'normal').

% Found in: KTK, pPRE
card_name('flying crane technique', 'Flying Crane Technique').
card_type('flying crane technique', 'Instant').
card_types('flying crane technique', ['Instant']).
card_subtypes('flying crane technique', []).
card_colors('flying crane technique', ['W', 'U', 'R']).
card_text('flying crane technique', 'Untap all creatures you control. They gain flying and double strike until end of turn.').
card_mana_cost('flying crane technique', ['3', 'U', 'R', 'W']).
card_cmc('flying crane technique', 6).
card_layout('flying crane technique', 'normal').

% Found in: ARN, TSB
card_name('flying men', 'Flying Men').
card_type('flying men', 'Creature — Human').
card_types('flying men', ['Creature']).
card_subtypes('flying men', ['Human']).
card_colors('flying men', ['U']).
card_text('flying men', 'Flying').
card_mana_cost('flying men', ['U']).
card_cmc('flying men', 1).
card_layout('flying men', 'normal').
card_power('flying men', 1).
card_toughness('flying men', 1).

% Found in: 8ED, UDS
card_name('fodder cannon', 'Fodder Cannon').
card_type('fodder cannon', 'Artifact').
card_types('fodder cannon', ['Artifact']).
card_subtypes('fodder cannon', []).
card_colors('fodder cannon', []).
card_text('fodder cannon', '{4}, {T}, Sacrifice a creature: Fodder Cannon deals 4 damage to target creature.').
card_mana_cost('fodder cannon', ['4']).
card_cmc('fodder cannon', 4).
card_layout('fodder cannon', 'normal').

% Found in: LRW
card_name('fodder launch', 'Fodder Launch').
card_type('fodder launch', 'Tribal Sorcery — Goblin').
card_types('fodder launch', ['Tribal', 'Sorcery']).
card_subtypes('fodder launch', ['Goblin']).
card_colors('fodder launch', ['B']).
card_text('fodder launch', 'As an additional cost to cast Fodder Launch, sacrifice a Goblin.\nTarget creature gets -5/-5 until end of turn. Fodder Launch deals 5 damage to that creature\'s controller.').
card_mana_cost('fodder launch', ['3', 'B']).
card_cmc('fodder launch', 4).
card_layout('fodder launch', 'normal').

% Found in: DTK, pMEI
card_name('foe-razer regent', 'Foe-Razer Regent').
card_type('foe-razer regent', 'Creature — Dragon').
card_types('foe-razer regent', ['Creature']).
card_subtypes('foe-razer regent', ['Dragon']).
card_colors('foe-razer regent', ['G']).
card_text('foe-razer regent', 'Flying\nWhen Foe-Razer Regent enters the battlefield, you may have it fight target creature you don\'t control.\nWhenever a creature you control fights, put two +1/+1 counters on it at the beginning of the next end step.').
card_mana_cost('foe-razer regent', ['5', 'G', 'G']).
card_cmc('foe-razer regent', 7).
card_layout('foe-razer regent', 'normal').
card_power('foe-razer regent', 4).
card_toughness('foe-razer regent', 5).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, ARC, BTD, CED, CEI, LEA, LEB, M10, M11, M12, M13, M14, ME4, MIR
card_name('fog', 'Fog').
card_type('fog', 'Instant').
card_types('fog', ['Instant']).
card_subtypes('fog', []).
card_colors('fog', ['G']).
card_text('fog', 'Prevent all combat damage that would be dealt this turn.').
card_mana_cost('fog', ['G']).
card_cmc('fog', 1).
card_layout('fog', 'normal').

% Found in: C13, C14, CMD, M13, USG
card_name('fog bank', 'Fog Bank').
card_type('fog bank', 'Creature — Wall').
card_types('fog bank', ['Creature']).
card_subtypes('fog bank', ['Wall']).
card_colors('fog bank', ['U']).
card_text('fog bank', 'Defender, flying\nPrevent all combat damage that would be dealt to and dealt by Fog Bank.').
card_mana_cost('fog bank', ['1', 'U']).
card_cmc('fog bank', 2).
card_layout('fog bank', 'normal').
card_power('fog bank', 0).
card_toughness('fog bank', 2).

% Found in: 10E, 6ED, BTD, WTH
card_name('fog elemental', 'Fog Elemental').
card_type('fog elemental', 'Creature — Elemental').
card_types('fog elemental', ['Creature']).
card_subtypes('fog elemental', ['Elemental']).
card_colors('fog elemental', ['U']).
card_text('fog elemental', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhen Fog Elemental attacks or blocks, sacrifice it at end of combat.').
card_mana_cost('fog elemental', ['2', 'U']).
card_cmc('fog elemental', 3).
card_layout('fog elemental', 'normal').
card_power('fog elemental', 4).
card_toughness('fog elemental', 4).

% Found in: ULG
card_name('fog of gnats', 'Fog of Gnats').
card_type('fog of gnats', 'Creature — Insect').
card_types('fog of gnats', ['Creature']).
card_subtypes('fog of gnats', ['Insect']).
card_colors('fog of gnats', ['B']).
card_text('fog of gnats', 'Flying\n{B}: Regenerate Fog of Gnats.').
card_mana_cost('fog of gnats', ['B', 'B']).
card_cmc('fog of gnats', 2).
card_layout('fog of gnats', 'normal').
card_power('fog of gnats', 1).
card_toughness('fog of gnats', 1).

% Found in: NMS
card_name('fog patch', 'Fog Patch').
card_type('fog patch', 'Instant').
card_types('fog patch', ['Instant']).
card_subtypes('fog patch', []).
card_colors('fog patch', ['G']).
card_text('fog patch', 'Cast Fog Patch only during the declare blockers step.\nAttacking creatures become blocked. (This spell works on creatures that can\'t be blocked.)').
card_mana_cost('fog patch', ['1', 'G']).
card_cmc('fog patch', 2).
card_layout('fog patch', 'normal').

% Found in: DDF, PCY
card_name('foil', 'Foil').
card_type('foil', 'Instant').
card_types('foil', ['Instant']).
card_subtypes('foil', []).
card_colors('foil', ['U']).
card_text('foil', 'You may discard an Island card and another card rather than pay Foil\'s mana cost.\nCounter target spell.').
card_mana_cost('foil', ['2', 'U', 'U']).
card_cmc('foil', 4).
card_layout('foil', 'normal').

% Found in: 5DN
card_name('fold into æther', 'Fold into Æther').
card_type('fold into æther', 'Instant').
card_types('fold into æther', ['Instant']).
card_subtypes('fold into æther', []).
card_colors('fold into æther', ['U']).
card_text('fold into æther', 'Counter target spell. If that spell is countered this way, its controller may put a creature card from his or her hand onto the battlefield.').
card_mana_cost('fold into æther', ['2', 'U', 'U']).
card_cmc('fold into æther', 4).
card_layout('fold into æther', 'normal').

% Found in: JUD
card_name('folk medicine', 'Folk Medicine').
card_type('folk medicine', 'Instant').
card_types('folk medicine', ['Instant']).
card_subtypes('folk medicine', []).
card_colors('folk medicine', ['G']).
card_text('folk medicine', 'You gain 1 life for each creature you control.\nFlashback {1}{W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('folk medicine', ['2', 'G']).
card_cmc('folk medicine', 3).
card_layout('folk medicine', 'normal').

% Found in: HML
card_name('folk of an-havva', 'Folk of An-Havva').
card_type('folk of an-havva', 'Creature — Human').
card_types('folk of an-havva', ['Creature']).
card_subtypes('folk of an-havva', ['Human']).
card_colors('folk of an-havva', ['G']).
card_text('folk of an-havva', 'Whenever Folk of An-Havva blocks, it gets +2/+0 until end of turn.').
card_mana_cost('folk of an-havva', ['G']).
card_cmc('folk of an-havva', 1).
card_layout('folk of an-havva', 'normal').
card_power('folk of an-havva', 1).
card_toughness('folk of an-havva', 1).

% Found in: DKM, ICE, ME2
card_name('folk of the pines', 'Folk of the Pines').
card_type('folk of the pines', 'Creature — Dryad').
card_types('folk of the pines', ['Creature']).
card_subtypes('folk of the pines', ['Dryad']).
card_colors('folk of the pines', ['G']).
card_text('folk of the pines', '{1}{G}: Folk of the Pines gets +1/+0 until end of turn.').
card_mana_cost('folk of the pines', ['4', 'G']).
card_cmc('folk of the pines', 5).
card_layout('folk of the pines', 'normal').
card_power('folk of the pines', 2).
card_toughness('folk of the pines', 5).

% Found in: RAV
card_name('followed footsteps', 'Followed Footsteps').
card_type('followed footsteps', 'Enchantment — Aura').
card_types('followed footsteps', ['Enchantment']).
card_subtypes('followed footsteps', ['Aura']).
card_colors('followed footsteps', ['U']).
card_text('followed footsteps', 'Enchant creature\nAt the beginning of your upkeep, put a token that\'s a copy of enchanted creature onto the battlefield.').
card_mana_cost('followed footsteps', ['3', 'U', 'U']).
card_cmc('followed footsteps', 5).
card_layout('followed footsteps', 'normal').

% Found in: FUT
card_name('fomori nomad', 'Fomori Nomad').
card_type('fomori nomad', 'Creature — Nomad Giant').
card_types('fomori nomad', ['Creature']).
card_subtypes('fomori nomad', ['Nomad', 'Giant']).
card_colors('fomori nomad', ['R']).
card_text('fomori nomad', '').
card_mana_cost('fomori nomad', ['4', 'R']).
card_cmc('fomori nomad', 5).
card_layout('fomori nomad', 'normal').
card_power('fomori nomad', 4).
card_toughness('fomori nomad', 4).

% Found in: CPK, JOU
card_name('font of fertility', 'Font of Fertility').
card_type('font of fertility', 'Enchantment').
card_types('font of fertility', ['Enchantment']).
card_subtypes('font of fertility', []).
card_colors('font of fertility', ['G']).
card_text('font of fertility', '{1}{G}, Sacrifice Font of Fertility: Search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.').
card_mana_cost('font of fertility', ['G']).
card_cmc('font of fertility', 1).
card_layout('font of fertility', 'normal').

% Found in: JOU
card_name('font of fortunes', 'Font of Fortunes').
card_type('font of fortunes', 'Enchantment').
card_types('font of fortunes', ['Enchantment']).
card_subtypes('font of fortunes', []).
card_colors('font of fortunes', ['U']).
card_text('font of fortunes', '{1}{U}, Sacrifice Font of Fortunes: Draw two cards.').
card_mana_cost('font of fortunes', ['1', 'U']).
card_cmc('font of fortunes', 2).
card_layout('font of fortunes', 'normal').

% Found in: JOU
card_name('font of ire', 'Font of Ire').
card_type('font of ire', 'Enchantment').
card_types('font of ire', ['Enchantment']).
card_subtypes('font of ire', []).
card_colors('font of ire', ['R']).
card_text('font of ire', '{3}{R}, Sacrifice Font of Ire: Font of Ire deals 5 damage to target player.').
card_mana_cost('font of ire', ['1', 'R']).
card_cmc('font of ire', 2).
card_layout('font of ire', 'normal').

% Found in: CON
card_name('font of mythos', 'Font of Mythos').
card_type('font of mythos', 'Artifact').
card_types('font of mythos', ['Artifact']).
card_subtypes('font of mythos', []).
card_colors('font of mythos', []).
card_text('font of mythos', 'At the beginning of each player\'s draw step, that player draws two additional cards.').
card_mana_cost('font of mythos', ['4']).
card_cmc('font of mythos', 4).
card_layout('font of mythos', 'normal').

% Found in: JOU
card_name('font of return', 'Font of Return').
card_type('font of return', 'Enchantment').
card_types('font of return', ['Enchantment']).
card_subtypes('font of return', []).
card_colors('font of return', ['B']).
card_text('font of return', '{3}{B}, Sacrifice Font of Return: Return up to three target creature cards from your graveyard to your hand.').
card_mana_cost('font of return', ['1', 'B']).
card_cmc('font of return', 2).
card_layout('font of return', 'normal').

% Found in: JOU
card_name('font of vigor', 'Font of Vigor').
card_type('font of vigor', 'Enchantment').
card_types('font of vigor', ['Enchantment']).
card_subtypes('font of vigor', []).
card_colors('font of vigor', ['W']).
card_text('font of vigor', '{2}{W}, Sacrifice Font of Vigor: You gain 7 life.').
card_mana_cost('font of vigor', ['1', 'W']).
card_cmc('font of vigor', 2).
card_layout('font of vigor', 'normal').

% Found in: MMQ
card_name('food chain', 'Food Chain').
card_type('food chain', 'Enchantment').
card_types('food chain', ['Enchantment']).
card_subtypes('food chain', []).
card_colors('food chain', ['G']).
card_text('food chain', 'Exile a creature you control: Add X mana of any one color to your mana pool, where X is the exiled creature\'s converted mana cost plus one. Spend this mana only to cast creature spells.').
card_mana_cost('food chain', ['2', 'G']).
card_cmc('food chain', 3).
card_layout('food chain', 'normal').

% Found in: C14, TSP
card_name('fool\'s demise', 'Fool\'s Demise').
card_type('fool\'s demise', 'Enchantment — Aura').
card_types('fool\'s demise', ['Enchantment']).
card_subtypes('fool\'s demise', ['Aura']).
card_colors('fool\'s demise', ['U']).
card_text('fool\'s demise', 'Enchant creature\nWhen enchanted creature dies, return that card to the battlefield under your control.\nWhen Fool\'s Demise is put into a graveyard from the battlefield, return Fool\'s Demise to its owner\'s hand.').
card_mana_cost('fool\'s demise', ['4', 'U']).
card_cmc('fool\'s demise', 5).
card_layout('fool\'s demise', 'normal').

% Found in: TMP
card_name('fool\'s tome', 'Fool\'s Tome').
card_type('fool\'s tome', 'Artifact').
card_types('fool\'s tome', ['Artifact']).
card_subtypes('fool\'s tome', []).
card_colors('fool\'s tome', []).
card_text('fool\'s tome', '{2}, {T}: Draw a card. Activate this ability only if you have no cards in hand.').
card_mana_cost('fool\'s tome', ['4']).
card_cmc('fool\'s tome', 4).
card_layout('fool\'s tome', 'normal').

% Found in: 9ED, POR, S99
card_name('foot soldiers', 'Foot Soldiers').
card_type('foot soldiers', 'Creature — Human Soldier').
card_types('foot soldiers', ['Creature']).
card_subtypes('foot soldiers', ['Human', 'Soldier']).
card_colors('foot soldiers', ['W']).
card_text('foot soldiers', '').
card_mana_cost('foot soldiers', ['3', 'W']).
card_cmc('foot soldiers', 4).
card_layout('foot soldiers', 'normal').
card_power('foot soldiers', 2).
card_toughness('foot soldiers', 4).

% Found in: CMD, LRW
card_name('footbottom feast', 'Footbottom Feast').
card_type('footbottom feast', 'Instant').
card_types('footbottom feast', ['Instant']).
card_subtypes('footbottom feast', []).
card_colors('footbottom feast', ['B']).
card_text('footbottom feast', 'Put any number of target creature cards from your graveyard on top of your library.\nDraw a card.').
card_mana_cost('footbottom feast', ['2', 'B']).
card_cmc('footbottom feast', 3).
card_layout('footbottom feast', 'normal').

% Found in: ONS
card_name('foothill guide', 'Foothill Guide').
card_type('foothill guide', 'Creature — Human Cleric').
card_types('foothill guide', ['Creature']).
card_subtypes('foothill guide', ['Human', 'Cleric']).
card_colors('foothill guide', ['W']).
card_text('foothill guide', 'Protection from Goblins\nMorph {W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('foothill guide', ['W']).
card_cmc('foothill guide', 1).
card_layout('foothill guide', 'normal').
card_power('foothill guide', 1).
card_toughness('foothill guide', 1).

% Found in: SOK
card_name('footsteps of the goryo', 'Footsteps of the Goryo').
card_type('footsteps of the goryo', 'Sorcery — Arcane').
card_types('footsteps of the goryo', ['Sorcery']).
card_subtypes('footsteps of the goryo', ['Arcane']).
card_colors('footsteps of the goryo', ['B']).
card_text('footsteps of the goryo', 'Return target creature card from your graveyard to the battlefield. Sacrifice that creature at the beginning of the next end step.').
card_mana_cost('footsteps of the goryo', ['2', 'B']).
card_cmc('footsteps of the goryo', 3).
card_layout('footsteps of the goryo', 'normal').

% Found in: 8ED, MIR
card_name('foratog', 'Foratog').
card_type('foratog', 'Creature — Atog').
card_types('foratog', ['Creature']).
card_subtypes('foratog', ['Atog']).
card_colors('foratog', ['G']).
card_text('foratog', '{G}, Sacrifice a Forest: Foratog gets +2/+2 until end of turn.').
card_mana_cost('foratog', ['2', 'G']).
card_cmc('foratog', 3).
card_layout('foratog', 'normal').
card_power('foratog', 1).
card_toughness('foratog', 2).

% Found in: EXO, TPR, pFNM
card_name('forbid', 'Forbid').
card_type('forbid', 'Instant').
card_types('forbid', ['Instant']).
card_subtypes('forbid', []).
card_colors('forbid', ['U']).
card_text('forbid', 'Buyback—Discard two cards. (You may discard two cards in addition to any other costs as you cast this spell. If you do, put this card into your hand as it resolves.)\nCounter target spell.').
card_mana_cost('forbid', ['1', 'U', 'U']).
card_cmc('forbid', 3).
card_layout('forbid', 'normal').

% Found in: ISD, pFNM
card_name('forbidden alchemy', 'Forbidden Alchemy').
card_type('forbidden alchemy', 'Instant').
card_types('forbidden alchemy', ['Instant']).
card_subtypes('forbidden alchemy', []).
card_colors('forbidden alchemy', ['U']).
card_text('forbidden alchemy', 'Look at the top four cards of your library. Put one of them into your hand and the rest into your graveyard.\nFlashback {6}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('forbidden alchemy', ['2', 'U']).
card_cmc('forbidden alchemy', 3).
card_layout('forbidden alchemy', 'normal').

% Found in: 6ED, MIR
card_name('forbidden crypt', 'Forbidden Crypt').
card_type('forbidden crypt', 'Enchantment').
card_types('forbidden crypt', ['Enchantment']).
card_subtypes('forbidden crypt', []).
card_colors('forbidden crypt', ['B']).
card_text('forbidden crypt', 'If you would draw a card, return a card from your graveyard to your hand instead. If you can\'t, you lose the game.\nIf a card would be put into your graveyard from anywhere, exile that card instead.').
card_mana_cost('forbidden crypt', ['3', 'B', 'B']).
card_cmc('forbidden crypt', 5).
card_layout('forbidden crypt', 'normal').

% Found in: ICE, ME2
card_name('forbidden lore', 'Forbidden Lore').
card_type('forbidden lore', 'Enchantment — Aura').
card_types('forbidden lore', ['Enchantment']).
card_subtypes('forbidden lore', ['Aura']).
card_colors('forbidden lore', ['G']).
card_text('forbidden lore', 'Enchant land\nEnchanted land has \"{T}: Target creature gets +2/+1 until end of turn.\"').
card_mana_cost('forbidden lore', ['2', 'G']).
card_cmc('forbidden lore', 3).
card_layout('forbidden lore', 'normal').

% Found in: CHK, V12
card_name('forbidden orchard', 'Forbidden Orchard').
card_type('forbidden orchard', 'Land').
card_types('forbidden orchard', ['Land']).
card_subtypes('forbidden orchard', []).
card_colors('forbidden orchard', []).
card_text('forbidden orchard', '{T}: Add one mana of any color to your mana pool.\nWhenever you tap Forbidden Orchard for mana, target opponent puts a 1/1 colorless Spirit creature token onto the battlefield.').
card_layout('forbidden orchard', 'normal').

% Found in: VIS
card_name('forbidden ritual', 'Forbidden Ritual').
card_type('forbidden ritual', 'Sorcery').
card_types('forbidden ritual', ['Sorcery']).
card_subtypes('forbidden ritual', []).
card_colors('forbidden ritual', ['B']).
card_text('forbidden ritual', 'Sacrifice a nontoken permanent. If you do, target opponent loses 2 life unless he or she sacrifices a permanent or discards a card. You may repeat this process any number of times.').
card_mana_cost('forbidden ritual', ['2', 'B', 'B']).
card_cmc('forbidden ritual', 4).
card_layout('forbidden ritual', 'normal').
card_reserved('forbidden ritual').

% Found in: 10E, ULG
card_name('forbidding watchtower', 'Forbidding Watchtower').
card_type('forbidding watchtower', 'Land').
card_types('forbidding watchtower', ['Land']).
card_subtypes('forbidding watchtower', []).
card_colors('forbidding watchtower', []).
card_text('forbidding watchtower', 'Forbidding Watchtower enters the battlefield tapped.\n{T}: Add {W} to your mana pool.\n{1}{W}: Forbidding Watchtower becomes a 1/5 white Soldier creature until end of turn. It\'s still a land.').
card_layout('forbidding watchtower', 'normal').

% Found in: KTK
card_name('force away', 'Force Away').
card_type('force away', 'Instant').
card_types('force away', ['Instant']).
card_subtypes('force away', []).
card_colors('force away', ['U']).
card_text('force away', 'Return target creature to its owner\'s hand.\nFerocious — If you control a creature with power 4 or greater, you may draw a card. If you do, discard a card.').
card_mana_cost('force away', ['1', 'U']).
card_cmc('force away', 2).
card_layout('force away', 'normal').

% Found in: SCG
card_name('force bubble', 'Force Bubble').
card_type('force bubble', 'Enchantment').
card_types('force bubble', ['Enchantment']).
card_subtypes('force bubble', []).
card_colors('force bubble', ['W']).
card_text('force bubble', 'If damage would be dealt to you, put that many depletion counters on Force Bubble instead.\nWhen there are four or more depletion counters on Force Bubble, sacrifice it.\nAt the beginning of each end step, remove all depletion counters from Force Bubble.').
card_mana_cost('force bubble', ['2', 'W', 'W']).
card_cmc('force bubble', 4).
card_layout('force bubble', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 9ED, BTD, CED, CEI, LEA, LEB, ME4, pREL
card_name('force of nature', 'Force of Nature').
card_type('force of nature', 'Creature — Elemental').
card_types('force of nature', ['Creature']).
card_subtypes('force of nature', ['Elemental']).
card_colors('force of nature', ['G']).
card_text('force of nature', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)\nAt the beginning of your upkeep, Force of Nature deals 8 damage to you unless you pay {G}{G}{G}{G}.').
card_mana_cost('force of nature', ['2', 'G', 'G', 'G', 'G']).
card_cmc('force of nature', 6).
card_layout('force of nature', 'normal').
card_power('force of nature', 8).
card_toughness('force of nature', 8).

% Found in: FUT
card_name('force of savagery', 'Force of Savagery').
card_type('force of savagery', 'Creature — Elemental').
card_types('force of savagery', ['Creature']).
card_subtypes('force of savagery', ['Elemental']).
card_colors('force of savagery', ['G']).
card_text('force of savagery', 'Trample').
card_mana_cost('force of savagery', ['2', 'G']).
card_cmc('force of savagery', 3).
card_layout('force of savagery', 'normal').
card_power('force of savagery', 8).
card_toughness('force of savagery', 0).

% Found in: ALL, MED, VMA, pJGP
card_name('force of will', 'Force of Will').
card_type('force of will', 'Instant').
card_types('force of will', ['Instant']).
card_subtypes('force of will', []).
card_colors('force of will', ['U']).
card_text('force of will', 'You may pay 1 life and exile a blue card from your hand rather than pay Force of Will\'s mana cost.\nCounter target spell.').
card_mana_cost('force of will', ['3', 'U', 'U']).
card_cmc('force of will', 5).
card_layout('force of will', 'normal').

% Found in: 5ED, 7ED, DDJ, LEG, ME3, pFNM
card_name('force spike', 'Force Spike').
card_type('force spike', 'Instant').
card_types('force spike', ['Instant']).
card_subtypes('force spike', []).
card_colors('force spike', ['U']).
card_text('force spike', 'Counter target spell unless its controller pays {1}.').
card_mana_cost('force spike', ['U']).
card_cmc('force spike', 1).
card_layout('force spike', 'normal').

% Found in: ICE
card_name('force void', 'Force Void').
card_type('force void', 'Instant').
card_types('force void', ['Instant']).
card_subtypes('force void', []).
card_colors('force void', ['U']).
card_text('force void', 'Counter target spell unless its controller pays {1}.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('force void', ['2', 'U']).
card_cmc('force void', 3).
card_layout('force void', 'normal').

% Found in: GTC
card_name('forced adaptation', 'Forced Adaptation').
card_type('forced adaptation', 'Enchantment — Aura').
card_types('forced adaptation', ['Enchantment']).
card_subtypes('forced adaptation', ['Aura']).
card_colors('forced adaptation', ['G']).
card_text('forced adaptation', 'Enchant creature\nAt the beginning of your upkeep, put a +1/+1 counter on enchanted creature.').
card_mana_cost('forced adaptation', ['G']).
card_cmc('forced adaptation', 1).
card_layout('forced adaptation', 'normal').

% Found in: LRW
card_name('forced fruition', 'Forced Fruition').
card_type('forced fruition', 'Enchantment').
card_types('forced fruition', ['Enchantment']).
card_subtypes('forced fruition', []).
card_colors('forced fruition', ['U']).
card_text('forced fruition', 'Whenever an opponent casts a spell, that player draws seven cards.').
card_mana_cost('forced fruition', ['4', 'U', 'U']).
card_cmc('forced fruition', 6).
card_layout('forced fruition', 'normal').

% Found in: MMQ
card_name('forced march', 'Forced March').
card_type('forced march', 'Sorcery').
card_types('forced march', ['Sorcery']).
card_subtypes('forced march', []).
card_colors('forced march', ['B']).
card_text('forced march', 'Destroy all creatures with converted mana cost X or less.').
card_mana_cost('forced march', ['X', 'B', 'B', 'B']).
card_cmc('forced march', 3).
card_layout('forced march', 'normal').

% Found in: ME3, PTK
card_name('forced retreat', 'Forced Retreat').
card_type('forced retreat', 'Sorcery').
card_types('forced retreat', ['Sorcery']).
card_subtypes('forced retreat', []).
card_colors('forced retreat', ['U']).
card_text('forced retreat', 'Put target creature on top of its owner\'s library.').
card_mana_cost('forced retreat', ['2', 'U']).
card_cmc('forced retreat', 3).
card_layout('forced retreat', 'normal').

% Found in: NPH
card_name('forced worship', 'Forced Worship').
card_type('forced worship', 'Enchantment — Aura').
card_types('forced worship', ['Enchantment']).
card_subtypes('forced worship', ['Aura']).
card_colors('forced worship', ['W']).
card_text('forced worship', 'Enchant creature\nEnchanted creature can\'t attack.\n{2}{W}: Return Forced Worship to its owner\'s hand.').
card_mana_cost('forced worship', ['1', 'W']).
card_cmc('forced worship', 2).
card_layout('forced worship', 'normal').

% Found in: 2ED, CED, CEI, LEA, LEB, MED
card_name('forcefield', 'Forcefield').
card_type('forcefield', 'Artifact').
card_types('forcefield', ['Artifact']).
card_subtypes('forcefield', []).
card_colors('forcefield', []).
card_text('forcefield', '{1}: The next time an unblocked creature of your choice would deal combat damage to you this turn, prevent all but 1 of that damage.').
card_mana_cost('forcefield', ['3']).
card_cmc('forcefield', 3).
card_layout('forcefield', 'normal').
card_reserved('forcefield').

% Found in: JUD
card_name('forcemage advocate', 'Forcemage Advocate').
card_type('forcemage advocate', 'Creature — Centaur Shaman').
card_types('forcemage advocate', ['Creature']).
card_subtypes('forcemage advocate', ['Centaur', 'Shaman']).
card_colors('forcemage advocate', ['G']).
card_text('forcemage advocate', '{T}: Return target card from an opponent\'s graveyard to his or her hand. Put a +1/+1 counter on target creature.').
card_mana_cost('forcemage advocate', ['1', 'G']).
card_cmc('forcemage advocate', 2).
card_layout('forcemage advocate', 'normal').
card_power('forcemage advocate', 2).
card_toughness('forcemage advocate', 1).

% Found in: BFZ, DDP
card_name('forerunner of slaughter', 'Forerunner of Slaughter').
card_type('forerunner of slaughter', 'Creature — Eldrazi Drone').
card_types('forerunner of slaughter', ['Creature']).
card_subtypes('forerunner of slaughter', ['Eldrazi', 'Drone']).
card_colors('forerunner of slaughter', []).
card_text('forerunner of slaughter', 'Devoid (This card has no color.)\n{1}: Target colorless creature gains haste until end of turn.').
card_mana_cost('forerunner of slaughter', ['B', 'R']).
card_cmc('forerunner of slaughter', 2).
card_layout('forerunner of slaughter', 'normal').
card_power('forerunner of slaughter', 3).
card_toughness('forerunner of slaughter', 2).

% Found in: FUT, M11
card_name('foresee', 'Foresee').
card_type('foresee', 'Sorcery').
card_types('foresee', ['Sorcery']).
card_subtypes('foresee', []).
card_colors('foresee', ['U']).
card_text('foresee', 'Scry 4, then draw two cards. (To scry 4, look at the top four cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('foresee', ['3', 'U']).
card_cmc('foresee', 4).
card_layout('foresee', 'normal').

% Found in: VIS
card_name('foreshadow', 'Foreshadow').
card_type('foreshadow', 'Instant').
card_types('foreshadow', ['Instant']).
card_subtypes('foreshadow', []).
card_colors('foreshadow', ['U']).
card_text('foreshadow', 'Name a card, then target opponent puts the top card of his or her library into his or her graveyard. If that card is the named card, you draw a card.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('foreshadow', ['1', 'U']).
card_cmc('foreshadow', 2).
card_layout('foreshadow', 'normal').

% Found in: ALL
card_name('foresight', 'Foresight').
card_type('foresight', 'Sorcery').
card_types('foresight', ['Sorcery']).
card_subtypes('foresight', []).
card_colors('foresight', ['U']).
card_text('foresight', 'Search your library for three cards, exile them, then shuffle your library.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('foresight', ['1', 'U']).
card_cmc('foresight', 2).
card_layout('foresight', 'normal').

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, ALA, ARC, ATH, AVR, BFZ, BRB, BTD, C13, C14, CED, CEI, CHK, CMD, CST, DD3_EVG, DD3_GVL, DDD, DDE, DDG, DDH, DDJ, DDL, DDM, DDO, DDP, DKM, DPA, DTK, EVG, FRF, H09, HOP, ICE, INV, ISD, ITP, KTK, LEA, LEB, LRW, M10, M11, M12, M13, M14, M15, MBS, ME3, MED, MIR, MMQ, MRD, NPH, ODY, ONS, ORI, PC2, PO2, POR, PTK, RAV, ROE, RQS, RTR, S00, S99, SHM, SOM, THS, TMP, TPR, TSP, UGL, UNH, USG, ZEN, pALP, pARL, pELP, pGRU, pJGP
card_name('forest', 'Forest').
card_type('forest', 'Basic Land — Forest').
card_types('forest', ['Land']).
card_subtypes('forest', ['Forest']).
card_supertypes('forest', ['Basic']).
card_colors('forest', []).
card_text('forest', '').
card_layout('forest', 'normal').

% Found in: PTK
card_name('forest bear', 'Forest Bear').
card_type('forest bear', 'Creature — Bear').
card_types('forest bear', ['Creature']).
card_subtypes('forest bear', ['Bear']).
card_colors('forest bear', ['G']).
card_text('forest bear', '').
card_mana_cost('forest bear', ['1', 'G']).
card_cmc('forest bear', 2).
card_layout('forest bear', 'normal').
card_power('forest bear', 2).
card_toughness('forest bear', 2).

% Found in: LEG
card_name('forethought amulet', 'Forethought Amulet').
card_type('forethought amulet', 'Artifact').
card_types('forethought amulet', ['Artifact']).
card_subtypes('forethought amulet', []).
card_colors('forethought amulet', []).
card_text('forethought amulet', 'At the beginning of your upkeep, sacrifice Forethought Amulet unless you pay {3}.\nIf an instant or sorcery source would deal 3 or more damage to you, it deals 2 damage to you instead.').
card_mana_cost('forethought amulet', ['5']).
card_cmc('forethought amulet', 5).
card_layout('forethought amulet', 'normal').
card_reserved('forethought amulet').

% Found in: MOR
card_name('forfend', 'Forfend').
card_type('forfend', 'Instant').
card_types('forfend', ['Instant']).
card_subtypes('forfend', []).
card_colors('forfend', ['W']).
card_text('forfend', 'Prevent all damage that would be dealt to creatures this turn.').
card_mana_cost('forfend', ['1', 'W']).
card_cmc('forfend', 2).
card_layout('forfend', 'normal').

% Found in: MRD
card_name('forge armor', 'Forge Armor').
card_type('forge armor', 'Instant').
card_types('forge armor', ['Instant']).
card_subtypes('forge armor', []).
card_colors('forge armor', ['R']).
card_text('forge armor', 'As an additional cost to cast Forge Armor, sacrifice an artifact.\nPut X +1/+1 counters on target creature, where X is the sacrificed artifact\'s converted mana cost.').
card_mana_cost('forge armor', ['4', 'R']).
card_cmc('forge armor', 5).
card_layout('forge armor', 'normal').

% Found in: DKA, M15
card_name('forge devil', 'Forge Devil').
card_type('forge devil', 'Creature — Devil').
card_types('forge devil', ['Creature']).
card_subtypes('forge devil', ['Devil']).
card_colors('forge devil', ['R']).
card_text('forge devil', 'When Forge Devil enters the battlefield, it deals 1 damage to target creature and 1 damage to you.').
card_mana_cost('forge devil', ['R']).
card_cmc('forge devil', 1).
card_layout('forge devil', 'normal').
card_power('forge devil', 1).
card_toughness('forge devil', 1).

% Found in: JOU
card_name('forgeborn oreads', 'Forgeborn Oreads').
card_type('forgeborn oreads', 'Enchantment Creature — Nymph').
card_types('forgeborn oreads', ['Enchantment', 'Creature']).
card_subtypes('forgeborn oreads', ['Nymph']).
card_colors('forgeborn oreads', ['R']).
card_text('forgeborn oreads', 'Constellation — Whenever Forgeborn Oreads or another enchantment enters the battlefield under your control, Forgeborn Oreads deals 1 damage to target creature or player.').
card_mana_cost('forgeborn oreads', ['2', 'R', 'R']).
card_cmc('forgeborn oreads', 4).
card_layout('forgeborn oreads', 'normal').
card_power('forgeborn oreads', 4).
card_toughness('forgeborn oreads', 2).

% Found in: BNG, pPRE
card_name('forgestoker dragon', 'Forgestoker Dragon').
card_type('forgestoker dragon', 'Creature — Dragon').
card_types('forgestoker dragon', ['Creature']).
card_subtypes('forgestoker dragon', ['Dragon']).
card_colors('forgestoker dragon', ['R']).
card_text('forgestoker dragon', 'Flying\n{1}{R}: Forgestoker Dragon deals 1 damage to target creature. That creature can\'t block this combat. Activate this ability only if Forgestoker Dragon is attacking.').
card_mana_cost('forgestoker dragon', ['4', 'R', 'R']).
card_cmc('forgestoker dragon', 6).
card_layout('forgestoker dragon', 'normal').
card_power('forgestoker dragon', 5).
card_toughness('forgestoker dragon', 4).

% Found in: 5ED, 6ED, HML
card_name('forget', 'Forget').
card_type('forget', 'Sorcery').
card_types('forget', ['Sorcery']).
card_subtypes('forget', []).
card_colors('forget', ['U']).
card_text('forget', 'Target player discards two cards, then draws as many cards as he or she discarded this way.').
card_mana_cost('forget', ['U', 'U']).
card_cmc('forget', 2).
card_layout('forget', 'normal').

% Found in: ARC, HOP, SCG
card_name('forgotten ancient', 'Forgotten Ancient').
card_type('forgotten ancient', 'Creature — Elemental').
card_types('forgotten ancient', ['Creature']).
card_subtypes('forgotten ancient', ['Elemental']).
card_colors('forgotten ancient', ['G']).
card_text('forgotten ancient', 'Whenever a player casts a spell, you may put a +1/+1 counter on Forgotten Ancient.\nAt the beginning of your upkeep, you may move any number of +1/+1 counters from Forgotten Ancient onto other creatures.').
card_mana_cost('forgotten ancient', ['3', 'G']).
card_cmc('forgotten ancient', 4).
card_layout('forgotten ancient', 'normal').
card_power('forgotten ancient', 0).
card_toughness('forgotten ancient', 3).

% Found in: C13, C14, CMD, DD3_EVG, DDJ, EVG, ONS, VMA
card_name('forgotten cave', 'Forgotten Cave').
card_type('forgotten cave', 'Land').
card_types('forgotten cave', ['Land']).
card_subtypes('forgotten cave', []).
card_colors('forgotten cave', []).
card_text('forgotten cave', 'Forgotten Cave enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\nCycling {R} ({R}, Discard this card: Draw a card.)').
card_layout('forgotten cave', 'normal').

% Found in: PCY
card_name('forgotten harvest', 'Forgotten Harvest').
card_type('forgotten harvest', 'Enchantment').
card_types('forgotten harvest', ['Enchantment']).
card_subtypes('forgotten harvest', []).
card_colors('forgotten harvest', ['G']).
card_text('forgotten harvest', 'At the beginning of your upkeep, you may exile a land card from your graveyard. If you do, put a +1/+1 counter on target creature.').
card_mana_cost('forgotten harvest', ['1', 'G']).
card_cmc('forgotten harvest', 2).
card_layout('forgotten harvest', 'normal').

% Found in: ICE, ME2
card_name('forgotten lore', 'Forgotten Lore').
card_type('forgotten lore', 'Sorcery').
card_types('forgotten lore', ['Sorcery']).
card_subtypes('forgotten lore', []).
card_colors('forgotten lore', ['G']).
card_text('forgotten lore', 'Target opponent chooses a card in your graveyard. You may pay {G}. If you do, repeat this process except that opponent can\'t choose a card already chosen for Forgotten Lore. Then put the last chosen card into your hand.').
card_mana_cost('forgotten lore', ['G']).
card_cmc('forgotten lore', 1).
card_layout('forgotten lore', 'normal').

% Found in: WTH
card_name('foriysian brigade', 'Foriysian Brigade').
card_type('foriysian brigade', 'Creature — Human Soldier').
card_types('foriysian brigade', ['Creature']).
card_subtypes('foriysian brigade', ['Human', 'Soldier']).
card_colors('foriysian brigade', ['W']).
card_text('foriysian brigade', 'Foriysian Brigade can block an additional creature.').
card_mana_cost('foriysian brigade', ['3', 'W']).
card_cmc('foriysian brigade', 4).
card_layout('foriysian brigade', 'normal').
card_power('foriysian brigade', 2).
card_toughness('foriysian brigade', 4).

% Found in: TSP
card_name('foriysian interceptor', 'Foriysian Interceptor').
card_type('foriysian interceptor', 'Creature — Human Soldier').
card_types('foriysian interceptor', ['Creature']).
card_subtypes('foriysian interceptor', ['Human', 'Soldier']).
card_colors('foriysian interceptor', ['W']).
card_text('foriysian interceptor', 'Flash (You may cast this spell any time you could cast an instant.)\nDefender\nForiysian Interceptor can block an additional creature.').
card_mana_cost('foriysian interceptor', ['3', 'W']).
card_cmc('foriysian interceptor', 4).
card_layout('foriysian interceptor', 'normal').
card_power('foriysian interceptor', 0).
card_toughness('foriysian interceptor', 5).

% Found in: TSP
card_name('foriysian totem', 'Foriysian Totem').
card_type('foriysian totem', 'Artifact').
card_types('foriysian totem', ['Artifact']).
card_subtypes('foriysian totem', []).
card_colors('foriysian totem', []).
card_text('foriysian totem', '{T}: Add {R} to your mana pool.\n{4}{R}: Foriysian Totem becomes a 4/4 red Giant artifact creature with trample until end of turn.\nAs long as Foriysian Totem is a creature, it can block an additional creature.').
card_mana_cost('foriysian totem', ['3']).
card_cmc('foriysian totem', 3).
card_layout('foriysian totem', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME4
card_name('fork', 'Fork').
card_type('fork', 'Instant').
card_types('fork', ['Instant']).
card_subtypes('fork', []).
card_colors('fork', ['R']).
card_text('fork', 'Copy target instant or sorcery spell, except that the copy is red. You may choose new targets for the copy.').
card_mana_cost('fork', ['R', 'R']).
card_cmc('fork', 2).
card_layout('fork', 'normal').
card_reserved('fork').

% Found in: DDP, ROE
card_name('forked bolt', 'Forked Bolt').
card_type('forked bolt', 'Sorcery').
card_types('forked bolt', ['Sorcery']).
card_subtypes('forked bolt', []).
card_colors('forked bolt', ['R']).
card_text('forked bolt', 'Forked Bolt deals 2 damage divided as you choose among one or two target creatures and/or players.').
card_mana_cost('forked bolt', ['R']).
card_cmc('forked bolt', 1).
card_layout('forked bolt', 'normal').

% Found in: ME3, POR
card_name('forked lightning', 'Forked Lightning').
card_type('forked lightning', 'Sorcery').
card_types('forked lightning', ['Sorcery']).
card_subtypes('forked lightning', []).
card_colors('forked lightning', ['R']).
card_text('forked lightning', 'Forked Lightning deals 4 damage divided as you choose among one, two, or three target creatures.').
card_mana_cost('forked lightning', ['3', 'R']).
card_cmc('forked lightning', 4).
card_layout('forked lightning', 'normal').

% Found in: BOK
card_name('forked-branch garami', 'Forked-Branch Garami').
card_type('forked-branch garami', 'Creature — Spirit').
card_types('forked-branch garami', ['Creature']).
card_subtypes('forked-branch garami', ['Spirit']).
card_colors('forked-branch garami', ['G']).
card_text('forked-branch garami', 'Soulshift 4, soulshift 4 (When this creature dies, you may return up to two target Spirit cards with converted mana cost 4 or less from your graveyard to your hand.)').
card_mana_cost('forked-branch garami', ['3', 'G', 'G']).
card_cmc('forked-branch garami', 5).
card_layout('forked-branch garami', 'normal').
card_power('forked-branch garami', 4).
card_toughness('forked-branch garami', 4).

% Found in: BNG
card_name('forlorn pseudamma', 'Forlorn Pseudamma').
card_type('forlorn pseudamma', 'Creature — Zombie').
card_types('forlorn pseudamma', ['Creature']).
card_subtypes('forlorn pseudamma', ['Zombie']).
card_colors('forlorn pseudamma', ['B']).
card_text('forlorn pseudamma', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nInspired — Whenever Forlorn Pseudamma becomes untapped, you may pay {2}{B}. If you do, put a 2/2 black Zombie enchantment creature token onto the battlefield.').
card_mana_cost('forlorn pseudamma', ['3', 'B']).
card_cmc('forlorn pseudamma', 4).
card_layout('forlorn pseudamma', 'normal').
card_power('forlorn pseudamma', 2).
card_toughness('forlorn pseudamma', 1).

% Found in: 9ED, DRB, SCG
card_name('form of the dragon', 'Form of the Dragon').
card_type('form of the dragon', 'Enchantment').
card_types('form of the dragon', ['Enchantment']).
card_subtypes('form of the dragon', []).
card_colors('form of the dragon', ['R']).
card_text('form of the dragon', 'At the beginning of your upkeep, Form of the Dragon deals 5 damage to target creature or player.\nAt the beginning of each end step, your life total becomes 5.\nCreatures without flying can\'t attack you.').
card_mana_cost('form of the dragon', ['4', 'R', 'R', 'R']).
card_cmc('form of the dragon', 7).
card_layout('form of the dragon', 'normal').

% Found in: UNH
card_name('form of the squirrel', 'Form of the Squirrel').
card_type('form of the squirrel', 'Enchantment').
card_types('form of the squirrel', ['Enchantment']).
card_subtypes('form of the squirrel', []).
card_colors('form of the squirrel', ['G']).
card_text('form of the squirrel', 'As Form of the Squirrel comes into play, put a 1/1 green Squirrel creature token into play. You lose the game when it leaves play.\nCreatures can\'t attack you.\nYou can\'t be the target of spells or abilities.\nYou can\'t play spells.').
card_mana_cost('form of the squirrel', ['G']).
card_cmc('form of the squirrel', 1).
card_layout('form of the squirrel', 'normal').

% Found in: ICE
card_name('formation', 'Formation').
card_type('formation', 'Instant').
card_types('formation', ['Instant']).
card_subtypes('formation', []).
card_colors('formation', ['W']).
card_text('formation', 'Target creature gains banding until end of turn. (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding a player controls are blocking or being blocked by a creature, that player divides that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('formation', ['1', 'W']).
card_cmc('formation', 2).
card_layout('formation', 'normal').
card_reserved('formation').

% Found in: FRF, FRF_UGIN
card_name('formless nurturing', 'Formless Nurturing').
card_type('formless nurturing', 'Sorcery').
card_types('formless nurturing', ['Sorcery']).
card_subtypes('formless nurturing', []).
card_colors('formless nurturing', ['G']).
card_text('formless nurturing', 'Manifest the top card of your library, then put a +1/+1 counter on it. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_mana_cost('formless nurturing', ['3', 'G']).
card_cmc('formless nurturing', 4).
card_layout('formless nurturing', 'normal').

% Found in: PLS
card_name('forsaken city', 'Forsaken City').
card_type('forsaken city', 'Land').
card_types('forsaken city', ['Land']).
card_subtypes('forsaken city', []).
card_colors('forsaken city', []).
card_text('forsaken city', 'Forsaken City doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may exile a card from your hand. If you do, untap Forsaken City.\n{T}: Add one mana of any color to your mana pool.').
card_layout('forsaken city', 'normal').

% Found in: BNG
card_name('forsaken drifters', 'Forsaken Drifters').
card_type('forsaken drifters', 'Creature — Zombie').
card_types('forsaken drifters', ['Creature']).
card_subtypes('forsaken drifters', ['Zombie']).
card_colors('forsaken drifters', ['B']).
card_text('forsaken drifters', 'When Forsaken Drifters dies, put the top four cards of your library into your graveyard.').
card_mana_cost('forsaken drifters', ['3', 'B']).
card_cmc('forsaken drifters', 4).
card_layout('forsaken drifters', 'normal').
card_power('forsaken drifters', 4).
card_toughness('forsaken drifters', 2).

% Found in: MIR
card_name('forsaken wastes', 'Forsaken Wastes').
card_type('forsaken wastes', 'World Enchantment').
card_types('forsaken wastes', ['Enchantment']).
card_subtypes('forsaken wastes', []).
card_supertypes('forsaken wastes', ['World']).
card_colors('forsaken wastes', ['B']).
card_text('forsaken wastes', 'Players can\'t gain life.\nAt the beginning of each player\'s upkeep, that player loses 1 life.\nWhenever Forsaken Wastes becomes the target of a spell, that spell\'s controller loses 5 life.').
card_mana_cost('forsaken wastes', ['2', 'B']).
card_cmc('forsaken wastes', 3).
card_layout('forsaken wastes', 'normal').
card_reserved('forsaken wastes').

% Found in: 4ED, LEG
card_name('fortified area', 'Fortified Area').
card_type('fortified area', 'Enchantment').
card_types('fortified area', ['Enchantment']).
card_subtypes('fortified area', []).
card_colors('fortified area', ['W']).
card_text('fortified area', 'Wall creatures you control get +1/+0 and have banding. (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('fortified area', ['1', 'W', 'W']).
card_cmc('fortified area', 3).
card_layout('fortified area', 'normal').

% Found in: BFZ
card_name('fortified rampart', 'Fortified Rampart').
card_type('fortified rampart', 'Creature — Wall').
card_types('fortified rampart', ['Creature']).
card_subtypes('fortified rampart', ['Wall']).
card_colors('fortified rampart', ['W']).
card_text('fortified rampart', 'Defender').
card_mana_cost('fortified rampart', ['1', 'W']).
card_cmc('fortified rampart', 2).
card_layout('fortified rampart', 'normal').
card_power('fortified rampart', 0).
card_toughness('fortified rampart', 6).

% Found in: M14, MM2, TSP
card_name('fortify', 'Fortify').
card_type('fortify', 'Instant').
card_types('fortify', ['Instant']).
card_subtypes('fortify', []).
card_colors('fortify', ['W']).
card_text('fortify', 'Choose one —\n• Creatures you control get +2/+0 until end of turn.\n• Creatures you control get +0/+2 until end of turn.').
card_mana_cost('fortify', ['2', 'W']).
card_cmc('fortify', 3).
card_layout('fortify', 'normal').

% Found in: USG
card_name('fortitude', 'Fortitude').
card_type('fortitude', 'Enchantment — Aura').
card_types('fortitude', ['Enchantment']).
card_subtypes('fortitude', ['Aura']).
card_colors('fortitude', ['G']).
card_text('fortitude', 'Enchant creature\nSacrifice a Forest: Regenerate enchanted creature.\nWhen Fortitude is put into a graveyard from the battlefield, return Fortitude to its owner\'s hand.').
card_mana_cost('fortitude', ['1', 'G']).
card_cmc('fortitude', 2).
card_layout('fortitude', 'normal').

% Found in: ISD
card_name('fortress crab', 'Fortress Crab').
card_type('fortress crab', 'Creature — Crab').
card_types('fortress crab', ['Creature']).
card_subtypes('fortress crab', ['Crab']).
card_colors('fortress crab', ['U']).
card_text('fortress crab', '').
card_mana_cost('fortress crab', ['3', 'U']).
card_cmc('fortress crab', 4).
card_layout('fortress crab', 'normal').
card_power('fortress crab', 1).
card_toughness('fortress crab', 6).

% Found in: GTC
card_name('fortress cyclops', 'Fortress Cyclops').
card_type('fortress cyclops', 'Creature — Cyclops Soldier').
card_types('fortress cyclops', ['Creature']).
card_subtypes('fortress cyclops', ['Cyclops', 'Soldier']).
card_colors('fortress cyclops', ['W', 'R']).
card_text('fortress cyclops', 'Whenever Fortress Cyclops attacks, it gets +3/+0 until end of turn.\nWhenever Fortress Cyclops blocks, it gets +0/+3 until end of turn.').
card_mana_cost('fortress cyclops', ['3', 'R', 'W']).
card_cmc('fortress cyclops', 5).
card_layout('fortress cyclops', 'normal').
card_power('fortress cyclops', 3).
card_toughness('fortress cyclops', 3).

% Found in: TSP
card_name('fortune thief', 'Fortune Thief').
card_type('fortune thief', 'Creature — Human Rogue').
card_types('fortune thief', ['Creature']).
card_subtypes('fortune thief', ['Human', 'Rogue']).
card_colors('fortune thief', ['R']).
card_text('fortune thief', 'Damage that would reduce your life total to less than 1 reduces it to 1 instead.\nMorph {R}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('fortune thief', ['4', 'R']).
card_cmc('fortune thief', 5).
card_layout('fortune thief', 'normal').
card_power('fortune thief', 0).
card_toughness('fortune thief', 1).

% Found in: SHM
card_name('fossil find', 'Fossil Find').
card_type('fossil find', 'Sorcery').
card_types('fossil find', ['Sorcery']).
card_subtypes('fossil find', []).
card_colors('fossil find', ['R', 'G']).
card_text('fossil find', 'Return a card at random from your graveyard to your hand, then reorder your graveyard as you choose.').
card_mana_cost('fossil find', ['R/G']).
card_cmc('fossil find', 1).
card_layout('fossil find', 'normal').

% Found in: C13, MMQ
card_name('foster', 'Foster').
card_type('foster', 'Enchantment').
card_types('foster', ['Enchantment']).
card_subtypes('foster', []).
card_colors('foster', ['G']).
card_text('foster', 'Whenever a creature you control dies, you may pay {1}. If you do, reveal cards from the top of your library until you reveal a creature card. Put that card into your hand and the rest into your graveyard.').
card_mana_cost('foster', ['2', 'G', 'G']).
card_cmc('foster', 4).
card_layout('foster', 'normal').

% Found in: DKM, ICE, ME2
card_name('foul familiar', 'Foul Familiar').
card_type('foul familiar', 'Creature — Spirit').
card_types('foul familiar', ['Creature']).
card_subtypes('foul familiar', ['Spirit']).
card_colors('foul familiar', ['B']).
card_text('foul familiar', 'Foul Familiar can\'t block.\n{B}, Pay 1 life: Return Foul Familiar to its owner\'s hand.').
card_mana_cost('foul familiar', ['2', 'B']).
card_cmc('foul familiar', 3).
card_layout('foul familiar', 'normal').
card_power('foul familiar', 3).
card_toughness('foul familiar', 1).

% Found in: 7ED, 9ED, DD3_DVD, DDC, STH
card_name('foul imp', 'Foul Imp').
card_type('foul imp', 'Creature — Imp').
card_types('foul imp', ['Creature']).
card_subtypes('foul imp', ['Imp']).
card_colors('foul imp', ['B']).
card_text('foul imp', 'Flying\nWhen Foul Imp enters the battlefield, you lose 2 life.').
card_mana_cost('foul imp', ['B', 'B']).
card_cmc('foul imp', 2).
card_layout('foul imp', 'normal').
card_power('foul imp', 2).
card_toughness('foul imp', 2).

% Found in: APC
card_name('foul presence', 'Foul Presence').
card_type('foul presence', 'Enchantment — Aura').
card_types('foul presence', ['Enchantment']).
card_subtypes('foul presence', ['Aura']).
card_colors('foul presence', ['B']).
card_text('foul presence', 'Enchant creature\nEnchanted creature gets -1/-1 and has \"{T}: Target creature gets -1/-1 until end of turn.\"').
card_mana_cost('foul presence', ['2', 'B']).
card_cmc('foul presence', 3).
card_layout('foul presence', 'normal').

% Found in: DTK
card_name('foul renewal', 'Foul Renewal').
card_type('foul renewal', 'Instant').
card_types('foul renewal', ['Instant']).
card_subtypes('foul renewal', []).
card_colors('foul renewal', ['B']).
card_text('foul renewal', 'Return target creature card from your graveyard to your hand. Target creature gets -X/-X until end of turn, where X is the toughness of the card returned this way.').
card_mana_cost('foul renewal', ['3', 'B']).
card_cmc('foul renewal', 4).
card_layout('foul renewal', 'normal').

% Found in: ME4, PO2
card_name('foul spirit', 'Foul Spirit').
card_type('foul spirit', 'Creature — Spirit').
card_types('foul spirit', ['Creature']).
card_subtypes('foul spirit', ['Spirit']).
card_colors('foul spirit', ['B']).
card_text('foul spirit', 'Flying\nWhen Foul Spirit enters the battlefield, sacrifice a land.').
card_mana_cost('foul spirit', ['2', 'B']).
card_cmc('foul spirit', 3).
card_layout('foul spirit', 'normal').
card_power('foul spirit', 3).
card_toughness('foul spirit', 2).

% Found in: DTK
card_name('foul-tongue invocation', 'Foul-Tongue Invocation').
card_type('foul-tongue invocation', 'Instant').
card_types('foul-tongue invocation', ['Instant']).
card_subtypes('foul-tongue invocation', []).
card_colors('foul-tongue invocation', ['B']).
card_text('foul-tongue invocation', 'As an additional cost to cast Foul-Tongue Invocation, you may reveal a Dragon card from your hand.\nTarget player sacrifices a creature. If you revealed a Dragon card or controlled a Dragon as you cast Foul-Tongue Invocation, you gain 4 life.').
card_mana_cost('foul-tongue invocation', ['2', 'B']).
card_cmc('foul-tongue invocation', 3).
card_layout('foul-tongue invocation', 'normal').

% Found in: DTK
card_name('foul-tongue shriek', 'Foul-Tongue Shriek').
card_type('foul-tongue shriek', 'Instant').
card_types('foul-tongue shriek', ['Instant']).
card_subtypes('foul-tongue shriek', []).
card_colors('foul-tongue shriek', ['B']).
card_text('foul-tongue shriek', 'Target opponent loses 1 life for each attacking creature you control. You gain that much life.').
card_mana_cost('foul-tongue shriek', ['B']).
card_cmc('foul-tongue shriek', 1).
card_layout('foul-tongue shriek', 'normal').

% Found in: GTC, pPRE
card_name('foundry champion', 'Foundry Champion').
card_type('foundry champion', 'Creature — Elemental Soldier').
card_types('foundry champion', ['Creature']).
card_subtypes('foundry champion', ['Elemental', 'Soldier']).
card_colors('foundry champion', ['W', 'R']).
card_text('foundry champion', 'When Foundry Champion enters the battlefield, it deals damage to target creature or player equal to the number of creatures you control.\n{R}: Foundry Champion gets +1/+0 until end of turn.\n{W}: Foundry Champion gets +0/+1 until end of turn.').
card_mana_cost('foundry champion', ['4', 'R', 'W']).
card_cmc('foundry champion', 6).
card_layout('foundry champion', 'normal').
card_power('foundry champion', 4).
card_toughness('foundry champion', 4).

% Found in: ORI
card_name('foundry of the consuls', 'Foundry of the Consuls').
card_type('foundry of the consuls', 'Land').
card_types('foundry of the consuls', ['Land']).
card_subtypes('foundry of the consuls', []).
card_colors('foundry of the consuls', []).
card_text('foundry of the consuls', '{T}: Add {1} to your mana pool.\n{5}, {T}, Sacrifice Foundry of the Consuls: Put two 1/1 colorless Thopter artifact creature tokens with flying onto the battlefield.').
card_layout('foundry of the consuls', 'normal').

% Found in: GTC, M15
card_name('foundry street denizen', 'Foundry Street Denizen').
card_type('foundry street denizen', 'Creature — Goblin Warrior').
card_types('foundry street denizen', ['Creature']).
card_subtypes('foundry street denizen', ['Goblin', 'Warrior']).
card_colors('foundry street denizen', ['R']).
card_text('foundry street denizen', 'Whenever another red creature enters the battlefield under your control, Foundry Street Denizen gets +1/+0 until end of turn.').
card_mana_cost('foundry street denizen', ['R']).
card_cmc('foundry street denizen', 1).
card_layout('foundry street denizen', 'normal').
card_power('foundry street denizen', 1).
card_toughness('foundry street denizen', 1).

% Found in: MMQ
card_name('fountain of cho', 'Fountain of Cho').
card_type('fountain of cho', 'Land').
card_types('fountain of cho', ['Land']).
card_subtypes('fountain of cho', []).
card_colors('fountain of cho', []).
card_text('fountain of cho', 'Fountain of Cho enters the battlefield tapped.\n{T}: Put a storage counter on Fountain of Cho.\n{T}, Remove any number of storage counters from Fountain of Cho: Add {W} to your mana pool for each storage counter removed this way.').
card_layout('fountain of cho', 'normal').

% Found in: 10E, 5ED, 6ED, CHR, DRK
card_name('fountain of youth', 'Fountain of Youth').
card_type('fountain of youth', 'Artifact').
card_types('fountain of youth', ['Artifact']).
card_subtypes('fountain of youth', []).
card_colors('fountain of youth', []).
card_text('fountain of youth', '{2}, {T}: You gain 1 life.').
card_mana_cost('fountain of youth', ['0']).
card_cmc('fountain of youth', 0).
card_layout('fountain of youth', 'normal').

% Found in: MMQ
card_name('fountain watch', 'Fountain Watch').
card_type('fountain watch', 'Creature — Human Cleric').
card_types('fountain watch', ['Creature']).
card_subtypes('fountain watch', ['Human', 'Cleric']).
card_colors('fountain watch', ['W']).
card_text('fountain watch', 'Artifacts and enchantments you control have shroud. (They can\'t be the targets of spells or abilities.)').
card_mana_cost('fountain watch', ['3', 'W', 'W']).
card_cmc('fountain watch', 5).
card_layout('fountain watch', 'normal').
card_power('fountain watch', 2).
card_toughness('fountain watch', 4).

% Found in: UGL
card_name('fowl play', 'Fowl Play').
card_type('fowl play', 'Enchant Creature').
card_types('fowl play', ['Enchant', 'Creature']).
card_subtypes('fowl play', []).
card_colors('fowl play', ['U']).
card_text('fowl play', 'Enchanted creature loses all abilities and is a 1/1 creature that counts as a Chicken.').
card_mana_cost('fowl play', ['2', 'U']).
card_cmc('fowl play', 3).
card_layout('fowl play', 'normal').

% Found in: 5ED, ICE
card_name('foxfire', 'Foxfire').
card_type('foxfire', 'Instant').
card_types('foxfire', ['Instant']).
card_subtypes('foxfire', []).
card_colors('foxfire', ['G']).
card_text('foxfire', 'Untap target attacking creature. Prevent all combat damage that would be dealt to and dealt by that creature this turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('foxfire', ['2', 'G']).
card_cmc('foxfire', 3).
card_layout('foxfire', 'normal').

% Found in: SHM
card_name('foxfire oak', 'Foxfire Oak').
card_type('foxfire oak', 'Creature — Treefolk Shaman').
card_types('foxfire oak', ['Creature']).
card_subtypes('foxfire oak', ['Treefolk', 'Shaman']).
card_colors('foxfire oak', ['G']).
card_text('foxfire oak', '{R/G}{R/G}{R/G}: Foxfire Oak gets +3/+0 until end of turn.').
card_mana_cost('foxfire oak', ['5', 'G']).
card_cmc('foxfire oak', 6).
card_layout('foxfire oak', 'normal').
card_power('foxfire oak', 3).
card_toughness('foxfire oak', 6).

% Found in: UNH
card_name('fraction jackson', 'Fraction Jackson').
card_type('fraction jackson', 'Creature — Human Hero').
card_types('fraction jackson', ['Creature']).
card_subtypes('fraction jackson', ['Human', 'Hero']).
card_colors('fraction jackson', ['G']).
card_text('fraction jackson', '{G}, {T}: Return target card with a ½ on it from your graveyard to your hand.').
card_mana_cost('fraction jackson', ['2', 'G']).
card_cmc('fraction jackson', 3).
card_layout('fraction jackson', 'normal').
card_power('fraction jackson', 1).
card_toughness('fraction jackson', 1.5).

% Found in: MRD
card_name('fractured loyalty', 'Fractured Loyalty').
card_type('fractured loyalty', 'Enchantment — Aura').
card_types('fractured loyalty', ['Enchantment']).
card_subtypes('fractured loyalty', ['Aura']).
card_colors('fractured loyalty', ['R']).
card_text('fractured loyalty', 'Enchant creature\nWhenever enchanted creature becomes the target of a spell or ability, that spell or ability\'s controller gains control of that creature. (This effect lasts indefinitely.)').
card_mana_cost('fractured loyalty', ['1', 'R']).
card_cmc('fractured loyalty', 2).
card_layout('fractured loyalty', 'normal').

% Found in: PC2
card_name('fractured powerstone', 'Fractured Powerstone').
card_type('fractured powerstone', 'Artifact').
card_types('fractured powerstone', ['Artifact']).
card_subtypes('fractured powerstone', []).
card_colors('fractured powerstone', []).
card_text('fractured powerstone', '{T}: Add {1} to your mana pool.\n{T}: Roll the planar die. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('fractured powerstone', ['2']).
card_cmc('fractured powerstone', 2).
card_layout('fractured powerstone', 'normal').

% Found in: SHM, V14
card_name('fracturing gust', 'Fracturing Gust').
card_type('fracturing gust', 'Instant').
card_types('fracturing gust', ['Instant']).
card_subtypes('fracturing gust', []).
card_colors('fracturing gust', ['W', 'G']).
card_text('fracturing gust', 'Destroy all artifacts and enchantments. You gain 2 life for each permanent destroyed this way.').
card_mana_cost('fracturing gust', ['2', 'G/W', 'G/W', 'G/W']).
card_cmc('fracturing gust', 5).
card_layout('fracturing gust', 'normal').

% Found in: UNH
card_name('framed!', 'Framed!').
card_type('framed!', 'Instant').
card_types('framed!', ['Instant']).
card_subtypes('framed!', []).
card_colors('framed!', ['U']).
card_text('framed!', 'Tap or untap all permanents by the artist of your choice.').
card_mana_cost('framed!', ['1', 'U']).
card_cmc('framed!', 2).
card_layout('framed!', 'normal').

% Found in: DRK
card_name('frankenstein\'s monster', 'Frankenstein\'s Monster').
card_type('frankenstein\'s monster', 'Creature — Zombie').
card_types('frankenstein\'s monster', ['Creature']).
card_subtypes('frankenstein\'s monster', ['Zombie']).
card_colors('frankenstein\'s monster', ['B']).
card_text('frankenstein\'s monster', 'As Frankenstein\'s Monster enters the battlefield, exile X creature cards from your graveyard. If you can\'t, put Frankenstein\'s Monster into its owner\'s graveyard instead of onto the battlefield. For each creature card exiled this way, Frankenstein\'s Monster enters the battlefield with a +2/+0, +1/+1, or +0/+2 counter on it.').
card_mana_cost('frankenstein\'s monster', ['X', 'B', 'B']).
card_cmc('frankenstein\'s monster', 2).
card_layout('frankenstein\'s monster', 'normal').
card_power('frankenstein\'s monster', 0).
card_toughness('frankenstein\'s monster', 1).
card_reserved('frankenstein\'s monster').

% Found in: UNH
card_name('frankie peanuts', 'Frankie Peanuts').
card_type('frankie peanuts', 'Legendary Creature — Elephant Rogue').
card_types('frankie peanuts', ['Creature']).
card_subtypes('frankie peanuts', ['Elephant', 'Rogue']).
card_supertypes('frankie peanuts', ['Legendary']).
card_colors('frankie peanuts', ['W']).
card_text('frankie peanuts', 'At the beginning of your upkeep, you may ask target player a yes-or-no question. If you do, that player answers the question truthfully and abides by that answer if able until end of turn.').
card_mana_cost('frankie peanuts', ['2', 'W', 'W']).
card_cmc('frankie peanuts', 4).
card_layout('frankie peanuts', 'normal').
card_power('frankie peanuts', 2).
card_toughness('frankie peanuts', 3).

% Found in: TOR
card_name('frantic purification', 'Frantic Purification').
card_type('frantic purification', 'Instant').
card_types('frantic purification', ['Instant']).
card_subtypes('frantic purification', []).
card_colors('frantic purification', ['W']).
card_text('frantic purification', 'Destroy target enchantment.\nMadness {W} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('frantic purification', ['2', 'W']).
card_cmc('frantic purification', 3).
card_layout('frantic purification', 'normal').

% Found in: MBS
card_name('frantic salvage', 'Frantic Salvage').
card_type('frantic salvage', 'Instant').
card_types('frantic salvage', ['Instant']).
card_subtypes('frantic salvage', []).
card_colors('frantic salvage', ['W']).
card_text('frantic salvage', 'Put any number of target artifact cards from your graveyard on top of your library.\nDraw a card.').
card_mana_cost('frantic salvage', ['3', 'W']).
card_cmc('frantic salvage', 4).
card_layout('frantic salvage', 'normal').

% Found in: ULG, VMA
card_name('frantic search', 'Frantic Search').
card_type('frantic search', 'Instant').
card_types('frantic search', ['Instant']).
card_subtypes('frantic search', []).
card_colors('frantic search', ['U']).
card_text('frantic search', 'Draw two cards, then discard two cards. Untap up to three lands.').
card_mana_cost('frantic search', ['2', 'U']).
card_cmc('frantic search', 3).
card_layout('frantic search', 'normal').

% Found in: pCEL
card_name('fraternal exaltation', 'Fraternal Exaltation').
card_type('fraternal exaltation', 'Sorcery').
card_types('fraternal exaltation', ['Sorcery']).
card_subtypes('fraternal exaltation', []).
card_colors('fraternal exaltation', ['U']).
card_text('fraternal exaltation', 'Sneak into your parents\' closet to get a deck. Your new brother is joining the game.').
card_mana_cost('fraternal exaltation', ['U', 'U', 'U', 'U']).
card_cmc('fraternal exaltation', 4).
card_layout('fraternal exaltation', 'normal').

% Found in: GPT
card_name('frazzle', 'Frazzle').
card_type('frazzle', 'Instant').
card_types('frazzle', ['Instant']).
card_subtypes('frazzle', []).
card_colors('frazzle', ['U']).
card_text('frazzle', 'Counter target nonblue spell.').
card_mana_cost('frazzle', ['3', 'U']).
card_cmc('frazzle', 4).
card_layout('frazzle', 'normal').

% Found in: UNH
card_name('frazzled editor', 'Frazzled Editor').
card_type('frazzled editor', 'Creature — Human Bureaucrat').
card_types('frazzled editor', ['Creature']).
card_subtypes('frazzled editor', ['Human', 'Bureaucrat']).
card_colors('frazzled editor', ['R']).
card_text('frazzled editor', 'Protection from wordy (Something is wordy if it has four or more lines of text in its text box.)').
card_mana_cost('frazzled editor', ['1', 'R']).
card_cmc('frazzled editor', 2).
card_layout('frazzled editor', 'normal').
card_power('frazzled editor', 2).
card_toughness('frazzled editor', 2).

% Found in: UGL
card_name('free-for-all', 'Free-for-All').
card_type('free-for-all', 'Enchantment').
card_types('free-for-all', ['Enchantment']).
card_subtypes('free-for-all', []).
card_colors('free-for-all', ['U']).
card_text('free-for-all', 'When Free-for-All comes into play, set aside all creatures in play, face down.\nDuring each player\'s upkeep, that player chooses a creature card at random from those set aside in this way and puts that creature into play under his or her control.\nIf Free-for-All leaves play, put each creature still set aside this way into its owner\'s graveyard.').
card_mana_cost('free-for-all', ['3', 'U']).
card_cmc('free-for-all', 4).
card_layout('free-for-all', 'normal').

% Found in: UGL
card_name('free-range chicken', 'Free-Range Chicken').
card_type('free-range chicken', 'Creature — Chicken').
card_types('free-range chicken', ['Creature']).
card_subtypes('free-range chicken', ['Chicken']).
card_colors('free-range chicken', ['G']).
card_text('free-range chicken', '{1}{G}: Roll two six-sided dice. If both die rolls are the same, Free-Range Chicken gets +X/+X until end of turn, where X is the number rolled on each die. Otherwise, if the total rolled is equal to any other total you have rolled this turn for Free-Range Chicken, sacrifice it. (For example, if you roll two 3s, Free-Range Chicken gets +3/+3. If you roll a total of 6 for Free-Range Chicken later in that turn, sacrifice it.)').
card_mana_cost('free-range chicken', ['3', 'G']).
card_cmc('free-range chicken', 4).
card_layout('free-range chicken', 'normal').
card_power('free-range chicken', 3).
card_toughness('free-range chicken', 3).

% Found in: SOK
card_name('freed from the real', 'Freed from the Real').
card_type('freed from the real', 'Enchantment — Aura').
card_types('freed from the real', ['Enchantment']).
card_subtypes('freed from the real', ['Aura']).
card_colors('freed from the real', ['U']).
card_text('freed from the real', 'Enchant creature\n{U}: Tap enchanted creature.\n{U}: Untap enchanted creature.').
card_mana_cost('freed from the real', ['2', 'U']).
card_cmc('freed from the real', 3).
card_layout('freed from the real', 'normal').

% Found in: DDL, DIS
card_name('freewind equenaut', 'Freewind Equenaut').
card_type('freewind equenaut', 'Creature — Human Archer').
card_types('freewind equenaut', ['Creature']).
card_subtypes('freewind equenaut', ['Human', 'Archer']).
card_colors('freewind equenaut', ['W']).
card_text('freewind equenaut', 'Flying\nAs long as Freewind Equenaut is enchanted, it has \"{T}: Freewind Equenaut deals 2 damage to target attacking or blocking creature.\"').
card_mana_cost('freewind equenaut', ['2', 'W']).
card_cmc('freewind equenaut', 3).
card_layout('freewind equenaut', 'normal').
card_power('freewind equenaut', 2).
card_toughness('freewind equenaut', 2).

% Found in: ATH, VIS
card_name('freewind falcon', 'Freewind Falcon').
card_type('freewind falcon', 'Creature — Bird').
card_types('freewind falcon', ['Creature']).
card_subtypes('freewind falcon', ['Bird']).
card_colors('freewind falcon', ['W']).
card_text('freewind falcon', 'Flying, protection from red').
card_mana_cost('freewind falcon', ['1', 'W']).
card_cmc('freewind falcon', 2).
card_layout('freewind falcon', 'normal').
card_power('freewind falcon', 1).
card_toughness('freewind falcon', 1).

% Found in: MIR
card_name('frenetic efreet', 'Frenetic Efreet').
card_type('frenetic efreet', 'Creature — Efreet').
card_types('frenetic efreet', ['Creature']).
card_subtypes('frenetic efreet', ['Efreet']).
card_colors('frenetic efreet', ['U', 'R']).
card_text('frenetic efreet', 'Flying\n{0}: Flip a coin. If you win the flip, Frenetic Efreet phases out. If you lose the flip, sacrifice Frenetic Efreet. (While it\'s phased out, it\'s treated as though it doesn\'t exist. It phases in before you untap during your next untap step.)').
card_mana_cost('frenetic efreet', ['1', 'U', 'R']).
card_cmc('frenetic efreet', 3).
card_layout('frenetic efreet', 'normal').
card_power('frenetic efreet', 2).
card_toughness('frenetic efreet', 1).
card_reserved('frenetic efreet').

% Found in: VAN
card_name('frenetic efreet avatar', 'Frenetic Efreet Avatar').
card_type('frenetic efreet avatar', 'Vanguard').
card_types('frenetic efreet avatar', ['Vanguard']).
card_subtypes('frenetic efreet avatar', []).
card_colors('frenetic efreet avatar', []).
card_text('frenetic efreet avatar', 'Each permanent you control has phasing. (It phases in or out before you untap during each of your untap steps. While it\'s phased out, it\'s treated as though it doesn\'t exist.)\nAt the beginning of your end step, flip a coin. If you win the flip, take an extra turn after this one.').
card_layout('frenetic efreet avatar', 'vanguard').

% Found in: ODY
card_name('frenetic ogre', 'Frenetic Ogre').
card_type('frenetic ogre', 'Creature — Ogre').
card_types('frenetic ogre', ['Creature']).
card_subtypes('frenetic ogre', ['Ogre']).
card_colors('frenetic ogre', ['R']).
card_text('frenetic ogre', '{R}, Discard a card at random: Frenetic Ogre gets +3/+0 until end of turn.').
card_mana_cost('frenetic ogre', ['4', 'R']).
card_cmc('frenetic ogre', 5).
card_layout('frenetic ogre', 'normal').
card_power('frenetic ogre', 2).
card_toughness('frenetic ogre', 3).

% Found in: LGN
card_name('frenetic raptor', 'Frenetic Raptor').
card_type('frenetic raptor', 'Creature — Lizard Beast').
card_types('frenetic raptor', ['Creature']).
card_subtypes('frenetic raptor', ['Lizard', 'Beast']).
card_colors('frenetic raptor', ['R']).
card_text('frenetic raptor', 'Beasts can\'t block.').
card_mana_cost('frenetic raptor', ['5', 'R']).
card_cmc('frenetic raptor', 6).
card_layout('frenetic raptor', 'normal').
card_power('frenetic raptor', 6).
card_toughness('frenetic raptor', 6).

% Found in: PLC
card_name('frenetic sliver', 'Frenetic Sliver').
card_type('frenetic sliver', 'Creature — Sliver').
card_types('frenetic sliver', ['Creature']).
card_subtypes('frenetic sliver', ['Sliver']).
card_colors('frenetic sliver', ['U', 'R']).
card_text('frenetic sliver', 'All Slivers have \"{0}: If this permanent is on the battlefield, flip a coin. If you win the flip, exile this permanent and return it to the battlefield under its owner\'s control at the beginning of the next end step. If you lose the flip, sacrifice it.\"').
card_mana_cost('frenetic sliver', ['1', 'U', 'R']).
card_cmc('frenetic sliver', 3).
card_layout('frenetic sliver', 'normal').
card_power('frenetic sliver', 2).
card_toughness('frenetic sliver', 2).

% Found in: DDN, M15, RAV, pFNM
card_name('frenzied goblin', 'Frenzied Goblin').
card_type('frenzied goblin', 'Creature — Goblin Berserker').
card_types('frenzied goblin', ['Creature']).
card_subtypes('frenzied goblin', ['Goblin', 'Berserker']).
card_colors('frenzied goblin', ['R']).
card_text('frenzied goblin', 'Whenever Frenzied Goblin attacks, you may pay {R}. If you do, target creature can\'t block this turn.').
card_mana_cost('frenzied goblin', ['R']).
card_cmc('frenzied goblin', 1).
card_layout('frenzied goblin', 'normal').
card_power('frenzied goblin', 1).
card_toughness('frenzied goblin', 1).

% Found in: GTC, INV
card_name('frenzied tilling', 'Frenzied Tilling').
card_type('frenzied tilling', 'Sorcery').
card_types('frenzied tilling', ['Sorcery']).
card_subtypes('frenzied tilling', []).
card_colors('frenzied tilling', ['R', 'G']).
card_text('frenzied tilling', 'Destroy target land. Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.').
card_mana_cost('frenzied tilling', ['3', 'R', 'G']).
card_cmc('frenzied tilling', 5).
card_layout('frenzied tilling', 'normal').

% Found in: FUT, H09
card_name('frenzy sliver', 'Frenzy Sliver').
card_type('frenzy sliver', 'Creature — Sliver').
card_types('frenzy sliver', ['Creature']).
card_subtypes('frenzy sliver', ['Sliver']).
card_colors('frenzy sliver', ['B']).
card_text('frenzy sliver', 'All Sliver creatures have frenzy 1. (Whenever a Sliver attacks and isn\'t blocked, it gets +1/+0 until end of turn.)').
card_mana_cost('frenzy sliver', ['1', 'B']).
card_cmc('frenzy sliver', 2).
card_layout('frenzy sliver', 'normal').
card_power('frenzy sliver', 1).
card_toughness('frenzy sliver', 1).

% Found in: C14, NPH
card_name('fresh meat', 'Fresh Meat').
card_type('fresh meat', 'Instant').
card_types('fresh meat', ['Instant']).
card_subtypes('fresh meat', []).
card_colors('fresh meat', ['G']).
card_text('fresh meat', 'Put a 3/3 green Beast creature token onto the battlefield for each creature put into your graveyard from the battlefield this turn.').
card_mana_cost('fresh meat', ['3', 'G']).
card_cmc('fresh meat', 4).
card_layout('fresh meat', 'normal').

% Found in: MMQ
card_name('fresh volunteers', 'Fresh Volunteers').
card_type('fresh volunteers', 'Creature — Human Rebel').
card_types('fresh volunteers', ['Creature']).
card_subtypes('fresh volunteers', ['Human', 'Rebel']).
card_colors('fresh volunteers', ['W']).
card_text('fresh volunteers', '').
card_mana_cost('fresh volunteers', ['1', 'W']).
card_cmc('fresh volunteers', 2).
card_layout('fresh volunteers', 'normal').
card_power('fresh volunteers', 2).
card_toughness('fresh volunteers', 2).

% Found in: ICE
card_name('freyalise supplicant', 'Freyalise Supplicant').
card_type('freyalise supplicant', 'Creature — Human Cleric').
card_types('freyalise supplicant', ['Creature']).
card_subtypes('freyalise supplicant', ['Human', 'Cleric']).
card_colors('freyalise supplicant', ['G']).
card_text('freyalise supplicant', '{T}, Sacrifice a red or white creature: Freyalise Supplicant deals damage to target creature or player equal to half the sacrificed creature\'s power, rounded down.').
card_mana_cost('freyalise supplicant', ['1', 'G']).
card_cmc('freyalise supplicant', 2).
card_layout('freyalise supplicant', 'normal').
card_power('freyalise supplicant', 1).
card_toughness('freyalise supplicant', 1).

% Found in: ICE
card_name('freyalise\'s charm', 'Freyalise\'s Charm').
card_type('freyalise\'s charm', 'Enchantment').
card_types('freyalise\'s charm', ['Enchantment']).
card_subtypes('freyalise\'s charm', []).
card_colors('freyalise\'s charm', ['G']).
card_text('freyalise\'s charm', 'Whenever an opponent casts a black spell, you may pay {G}{G}. If you do, you draw a card.\n{G}{G}: Return Freyalise\'s Charm to its owner\'s hand.').
card_mana_cost('freyalise\'s charm', ['G', 'G']).
card_cmc('freyalise\'s charm', 2).
card_layout('freyalise\'s charm', 'normal').

% Found in: CSP
card_name('freyalise\'s radiance', 'Freyalise\'s Radiance').
card_type('freyalise\'s radiance', 'Enchantment').
card_types('freyalise\'s radiance', ['Enchantment']).
card_subtypes('freyalise\'s radiance', []).
card_colors('freyalise\'s radiance', ['G']).
card_text('freyalise\'s radiance', 'Cumulative upkeep {2} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nSnow permanents don\'t untap during their controllers\' untap steps.').
card_mana_cost('freyalise\'s radiance', ['1', 'G']).
card_cmc('freyalise\'s radiance', 2).
card_layout('freyalise\'s radiance', 'normal').

% Found in: ICE, ME3
card_name('freyalise\'s winds', 'Freyalise\'s Winds').
card_type('freyalise\'s winds', 'Enchantment').
card_types('freyalise\'s winds', ['Enchantment']).
card_subtypes('freyalise\'s winds', []).
card_colors('freyalise\'s winds', ['G']).
card_text('freyalise\'s winds', 'Whenever a permanent becomes tapped, put a wind counter on it.\nIf a permanent with a wind counter on it would untap during its controller\'s untap step, remove all wind counters from it instead.').
card_mana_cost('freyalise\'s winds', ['2', 'G', 'G']).
card_cmc('freyalise\'s winds', 4).
card_layout('freyalise\'s winds', 'normal').

% Found in: C14
card_name('freyalise, llanowar\'s fury', 'Freyalise, Llanowar\'s Fury').
card_type('freyalise, llanowar\'s fury', 'Planeswalker — Freyalise').
card_types('freyalise, llanowar\'s fury', ['Planeswalker']).
card_subtypes('freyalise, llanowar\'s fury', ['Freyalise']).
card_colors('freyalise, llanowar\'s fury', ['G']).
card_text('freyalise, llanowar\'s fury', '+2: Put a 1/1 green Elf Druid creature token onto the battlefield with \"{T}: Add {G} to your mana pool.\"\n−2: Destroy target artifact or enchantment.\n−6: Draw a card for each green creature you control.\nFreyalise, Llanowar\'s Fury can be your commander.').
card_mana_cost('freyalise, llanowar\'s fury', ['3', 'G', 'G']).
card_cmc('freyalise, llanowar\'s fury', 5).
card_layout('freyalise, llanowar\'s fury', 'normal').
card_loyalty('freyalise, llanowar\'s fury', 3).

% Found in: FRF
card_name('friendly fire', 'Friendly Fire').
card_type('friendly fire', 'Instant').
card_types('friendly fire', ['Instant']).
card_subtypes('friendly fire', []).
card_colors('friendly fire', ['R']).
card_text('friendly fire', 'Target creature\'s controller reveals a card at random from his or her hand. Friendly Fire deals damage to that creature and that player equal to the revealed card\'s converted mana cost.').
card_mana_cost('friendly fire', ['3', 'R']).
card_cmc('friendly fire', 4).
card_layout('friendly fire', 'normal').

% Found in: ODY
card_name('frightcrawler', 'Frightcrawler').
card_type('frightcrawler', 'Creature — Horror').
card_types('frightcrawler', ['Creature']).
card_subtypes('frightcrawler', ['Horror']).
card_colors('frightcrawler', ['B']).
card_text('frightcrawler', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nThreshold — As long as seven or more cards are in your graveyard, Frightcrawler gets +2/+2 and can\'t block.').
card_mana_cost('frightcrawler', ['1', 'B']).
card_cmc('frightcrawler', 2).
card_layout('frightcrawler', 'normal').
card_power('frightcrawler', 1).
card_toughness('frightcrawler', 1).

% Found in: ISD
card_name('frightful delusion', 'Frightful Delusion').
card_type('frightful delusion', 'Instant').
card_types('frightful delusion', ['Instant']).
card_subtypes('frightful delusion', []).
card_colors('frightful delusion', ['U']).
card_text('frightful delusion', 'Counter target spell unless its controller pays {1}. That player discards a card.').
card_mana_cost('frightful delusion', ['2', 'U']).
card_cmc('frightful delusion', 3).
card_layout('frightful delusion', 'normal').

% Found in: ONS
card_name('frightshroud courier', 'Frightshroud Courier').
card_type('frightshroud courier', 'Creature — Zombie').
card_types('frightshroud courier', ['Creature']).
card_subtypes('frightshroud courier', ['Zombie']).
card_colors('frightshroud courier', ['B']).
card_text('frightshroud courier', 'You may choose not to untap Frightshroud Courier during your untap step.\n{2}{B}, {T}: Target Zombie creature gets +2/+2 and has fear for as long as Frightshroud Courier remains tapped. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('frightshroud courier', ['2', 'B']).
card_cmc('frightshroud courier', 3).
card_layout('frightshroud courier', 'normal').
card_power('frightshroud courier', 2).
card_toughness('frightshroud courier', 1).

% Found in: GTC
card_name('frilled oculus', 'Frilled Oculus').
card_type('frilled oculus', 'Creature — Homunculus').
card_types('frilled oculus', ['Creature']).
card_subtypes('frilled oculus', ['Homunculus']).
card_colors('frilled oculus', ['U']).
card_text('frilled oculus', '{1}{G}: Frilled Oculus gets +2/+2 until end of turn. Activate this ability only once each turn.').
card_mana_cost('frilled oculus', ['1', 'U']).
card_cmc('frilled oculus', 2).
card_layout('frilled oculus', 'normal').
card_power('frilled oculus', 1).
card_toughness('frilled oculus', 3).

% Found in: TMP
card_name('frog tongue', 'Frog Tongue').
card_type('frog tongue', 'Enchantment — Aura').
card_types('frog tongue', ['Enchantment']).
card_subtypes('frog tongue', ['Aura']).
card_colors('frog tongue', ['G']).
card_text('frog tongue', 'Enchant creature\nWhen Frog Tongue enters the battlefield, draw a card.\nEnchanted creature has reach. (It can block creatures with flying.)').
card_mana_cost('frog tongue', ['G']).
card_cmc('frog tongue', 1).
card_layout('frog tongue', 'normal').

% Found in: DDF, MM2, MMA, MRD
card_name('frogmite', 'Frogmite').
card_type('frogmite', 'Artifact Creature — Frog').
card_types('frogmite', ['Artifact', 'Creature']).
card_subtypes('frogmite', ['Frog']).
card_colors('frogmite', []).
card_text('frogmite', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)').
card_mana_cost('frogmite', ['4']).
card_cmc('frogmite', 4).
card_layout('frogmite', 'normal').
card_power('frogmite', 2).
card_toughness('frogmite', 2).

% Found in: MOR
card_name('frogtosser banneret', 'Frogtosser Banneret').
card_type('frogtosser banneret', 'Creature — Goblin Rogue').
card_types('frogtosser banneret', ['Creature']).
card_subtypes('frogtosser banneret', ['Goblin', 'Rogue']).
card_colors('frogtosser banneret', ['B']).
card_text('frogtosser banneret', 'Haste\nGoblin spells and Rogue spells you cast cost {1} less to cast.').
card_mana_cost('frogtosser banneret', ['1', 'B']).
card_cmc('frogtosser banneret', 2).
card_layout('frogtosser banneret', 'normal').
card_power('frogtosser banneret', 1).
card_toughness('frogtosser banneret', 1).

% Found in: BFZ
card_name('from beyond', 'From Beyond').
card_type('from beyond', 'Enchantment').
card_types('from beyond', ['Enchantment']).
card_subtypes('from beyond', []).
card_colors('from beyond', []).
card_text('from beyond', 'Devoid (This card has no color.)\nAt the beginning of your upkeep, put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"\n{1}{G}, Sacrifice From Beyond: Search your library for an Eldrazi card, reveal it, put it into your hand, then shuffle your library.').
card_mana_cost('from beyond', ['3', 'G']).
card_cmc('from beyond', 4).
card_layout('from beyond', 'normal').

% Found in: C13
card_name('from the ashes', 'From the Ashes').
card_type('from the ashes', 'Sorcery').
card_types('from the ashes', ['Sorcery']).
card_subtypes('from the ashes', []).
card_colors('from the ashes', ['R']).
card_text('from the ashes', 'Destroy all nonbasic lands. For each land destroyed this way, its controller may search his or her library for a basic land card and put it onto the battlefield. Then each player who searched his or her library this way shuffles it.').
card_mana_cost('from the ashes', ['3', 'R']).
card_cmc('from the ashes', 4).
card_layout('from the ashes', 'normal').

% Found in: KTK
card_name('frontier bivouac', 'Frontier Bivouac').
card_type('frontier bivouac', 'Land').
card_types('frontier bivouac', ['Land']).
card_subtypes('frontier bivouac', []).
card_colors('frontier bivouac', []).
card_text('frontier bivouac', 'Frontier Bivouac enters the battlefield tapped.\n{T}: Add {G}, {U}, or {R} to your mana pool.').
card_layout('frontier bivouac', 'normal').

% Found in: DDP, ZEN
card_name('frontier guide', 'Frontier Guide').
card_type('frontier guide', 'Creature — Elf Scout').
card_types('frontier guide', ['Creature']).
card_subtypes('frontier guide', ['Elf', 'Scout']).
card_colors('frontier guide', ['G']).
card_text('frontier guide', '{3}{G}, {T}: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_mana_cost('frontier guide', ['1', 'G']).
card_cmc('frontier guide', 2).
card_layout('frontier guide', 'normal').
card_power('frontier guide', 1).
card_toughness('frontier guide', 1).

% Found in: FRF
card_name('frontier mastodon', 'Frontier Mastodon').
card_type('frontier mastodon', 'Creature — Elephant').
card_types('frontier mastodon', ['Creature']).
card_subtypes('frontier mastodon', ['Elephant']).
card_colors('frontier mastodon', ['G']).
card_text('frontier mastodon', 'Ferocious — Frontier Mastodon enters the battlefield with a +1/+1 counter on it if you control a creature with power 4 or greater.').
card_mana_cost('frontier mastodon', ['2', 'G']).
card_cmc('frontier mastodon', 3).
card_layout('frontier mastodon', 'normal').
card_power('frontier mastodon', 3).
card_toughness('frontier mastodon', 2).

% Found in: FRF
card_name('frontier siege', 'Frontier Siege').
card_type('frontier siege', 'Enchantment').
card_types('frontier siege', ['Enchantment']).
card_subtypes('frontier siege', []).
card_colors('frontier siege', ['G']).
card_text('frontier siege', 'As Frontier Siege enters the battlefield, choose Khans or Dragons.\n• Khans — At the beginning of each of your main phases, add {G}{G} to your mana pool.\n• Dragons — Whenever a creature with flying enters the battlefield under your control, you may have it fight target creature you don\'t control.').
card_mana_cost('frontier siege', ['3', 'G']).
card_cmc('frontier siege', 4).
card_layout('frontier siege', 'normal').

% Found in: GTC
card_name('frontline medic', 'Frontline Medic').
card_type('frontline medic', 'Creature — Human Cleric').
card_types('frontline medic', ['Creature']).
card_subtypes('frontline medic', ['Human', 'Cleric']).
card_colors('frontline medic', ['W']).
card_text('frontline medic', 'Battalion — Whenever Frontline Medic and at least two other creatures attack, creatures you control gain indestructible until end of turn.\nSacrifice Frontline Medic: Counter target spell with {X} in its mana cost unless its controller pays {3}.').
card_mana_cost('frontline medic', ['2', 'W']).
card_cmc('frontline medic', 3).
card_layout('frontline medic', 'normal').
card_power('frontline medic', 3).
card_toughness('frontline medic', 3).

% Found in: CON
card_name('frontline sage', 'Frontline Sage').
card_type('frontline sage', 'Creature — Human Wizard').
card_types('frontline sage', ['Creature']).
card_subtypes('frontline sage', ['Human', 'Wizard']).
card_colors('frontline sage', ['U']).
card_text('frontline sage', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{U}, {T}: Draw a card, then discard a card.').
card_mana_cost('frontline sage', ['2', 'U']).
card_cmc('frontline sage', 3).
card_layout('frontline sage', 'normal').
card_power('frontline sage', 0).
card_toughness('frontline sage', 1).

% Found in: SCG
card_name('frontline strategist', 'Frontline Strategist').
card_type('frontline strategist', 'Creature — Human Soldier').
card_types('frontline strategist', ['Creature']).
card_subtypes('frontline strategist', ['Human', 'Soldier']).
card_colors('frontline strategist', ['W']).
card_text('frontline strategist', 'Morph {W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Frontline Strategist is turned face up, prevent all combat damage non-Soldier creatures would deal this turn.').
card_mana_cost('frontline strategist', ['W']).
card_cmc('frontline strategist', 1).
card_layout('frontline strategist', 'normal').
card_power('frontline strategist', 1).
card_toughness('frontline strategist', 1).

% Found in: M12, M14
card_name('frost breath', 'Frost Breath').
card_type('frost breath', 'Instant').
card_types('frost breath', ['Instant']).
card_subtypes('frost breath', []).
card_colors('frost breath', ['U']).
card_text('frost breath', 'Tap up to two target creatures. Those creatures don\'t untap during their controller\'s next untap step.').
card_mana_cost('frost breath', ['2', 'U']).
card_cmc('frost breath', 3).
card_layout('frost breath', 'normal').

% Found in: LEG, ME3
card_name('frost giant', 'Frost Giant').
card_type('frost giant', 'Creature — Giant').
card_types('frost giant', ['Creature']).
card_subtypes('frost giant', ['Giant']).
card_colors('frost giant', ['R']).
card_text('frost giant', 'Rampage 2 (Whenever this creature becomes blocked, it gets +2/+2 until end of turn for each creature blocking it beyond the first.)').
card_mana_cost('frost giant', ['3', 'R', 'R', 'R']).
card_cmc('frost giant', 6).
card_layout('frost giant', 'normal').
card_power('frost giant', 4).
card_toughness('frost giant', 4).

% Found in: M15
card_name('frost lynx', 'Frost Lynx').
card_type('frost lynx', 'Creature — Elemental Cat').
card_types('frost lynx', ['Creature']).
card_subtypes('frost lynx', ['Elemental', 'Cat']).
card_colors('frost lynx', ['U']).
card_text('frost lynx', 'When Frost Lynx enters the battlefield, tap target creature an opponent controls. That creature doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('frost lynx', ['2', 'U']).
card_cmc('frost lynx', 3).
card_layout('frost lynx', 'normal').
card_power('frost lynx', 2).
card_toughness('frost lynx', 2).

% Found in: CSP
card_name('frost marsh', 'Frost Marsh').
card_type('frost marsh', 'Snow Land').
card_types('frost marsh', ['Land']).
card_subtypes('frost marsh', []).
card_supertypes('frost marsh', ['Snow']).
card_colors('frost marsh', []).
card_text('frost marsh', 'Frost Marsh enters the battlefield tapped.\n{T}: Add {U} or {B} to your mana pool.').
card_layout('frost marsh', 'normal').

% Found in: BOK
card_name('frost ogre', 'Frost Ogre').
card_type('frost ogre', 'Creature — Ogre Warrior').
card_types('frost ogre', ['Creature']).
card_subtypes('frost ogre', ['Ogre', 'Warrior']).
card_colors('frost ogre', ['R']).
card_text('frost ogre', '').
card_mana_cost('frost ogre', ['3', 'R', 'R']).
card_cmc('frost ogre', 5).
card_layout('frost ogre', 'normal').
card_power('frost ogre', 5).
card_toughness('frost ogre', 3).

% Found in: CSP
card_name('frost raptor', 'Frost Raptor').
card_type('frost raptor', 'Snow Creature — Bird').
card_types('frost raptor', ['Creature']).
card_subtypes('frost raptor', ['Bird']).
card_supertypes('frost raptor', ['Snow']).
card_colors('frost raptor', ['U']).
card_text('frost raptor', 'Flying\n{S}{S}: Frost Raptor gains shroud until end of turn. ({S} can be paid with one mana from a snow permanent. A creature with shroud can\'t be the target of spells or abilities.)').
card_mana_cost('frost raptor', ['2', 'U']).
card_cmc('frost raptor', 3).
card_layout('frost raptor', 'normal').
card_power('frost raptor', 2).
card_toughness('frost raptor', 2).

% Found in: C14, M11, M12, pMEI
card_name('frost titan', 'Frost Titan').
card_type('frost titan', 'Creature — Giant').
card_types('frost titan', ['Creature']).
card_subtypes('frost titan', ['Giant']).
card_colors('frost titan', ['U']).
card_text('frost titan', 'Whenever Frost Titan becomes the target of a spell or ability an opponent controls, counter that spell or ability unless its controller pays {2}.\nWhenever Frost Titan enters the battlefield or attacks, tap target permanent. It doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('frost titan', ['4', 'U', 'U']).
card_cmc('frost titan', 6).
card_layout('frost titan', 'normal').
card_power('frost titan', 6).
card_toughness('frost titan', 6).

% Found in: FRF
card_name('frost walker', 'Frost Walker').
card_type('frost walker', 'Creature — Elemental').
card_types('frost walker', ['Creature']).
card_subtypes('frost walker', ['Elemental']).
card_colors('frost walker', ['U']).
card_text('frost walker', 'When Frost Walker becomes the target of a spell or ability, sacrifice it.').
card_mana_cost('frost walker', ['1', 'U']).
card_cmc('frost walker', 2).
card_layout('frost walker', 'normal').
card_power('frost walker', 4).
card_toughness('frost walker', 1).

% Found in: RTR
card_name('frostburn weird', 'Frostburn Weird').
card_type('frostburn weird', 'Creature — Weird').
card_types('frostburn weird', ['Creature']).
card_subtypes('frostburn weird', ['Weird']).
card_colors('frostburn weird', ['U', 'R']).
card_text('frostburn weird', '{U/R}: Frostburn Weird gets +1/-1 until end of turn.').
card_mana_cost('frostburn weird', ['U/R', 'U/R']).
card_cmc('frostburn weird', 2).
card_layout('frostburn weird', 'normal').
card_power('frostburn weird', 1).
card_toughness('frostburn weird', 4).

% Found in: BOK
card_name('frostling', 'Frostling').
card_type('frostling', 'Creature — Spirit').
card_types('frostling', ['Creature']).
card_subtypes('frostling', ['Spirit']).
card_colors('frostling', ['R']).
card_text('frostling', 'Sacrifice Frostling: Frostling deals 1 damage to target creature.').
card_mana_cost('frostling', ['R']).
card_cmc('frostling', 1).
card_layout('frostling', 'normal').
card_power('frostling', 1).
card_toughness('frostling', 1).

% Found in: CSP
card_name('frostweb spider', 'Frostweb Spider').
card_type('frostweb spider', 'Snow Creature — Spider').
card_types('frostweb spider', ['Creature']).
card_subtypes('frostweb spider', ['Spider']).
card_supertypes('frostweb spider', ['Snow']).
card_colors('frostweb spider', ['G']).
card_text('frostweb spider', 'Reach (This creature can block creatures with flying.)\nWhenever Frostweb Spider blocks a creature with flying, put a +1/+1 counter on Frostweb Spider at end of combat.').
card_mana_cost('frostweb spider', ['2', 'G']).
card_cmc('frostweb spider', 3).
card_layout('frostweb spider', 'normal').
card_power('frostweb spider', 1).
card_toughness('frostweb spider', 3).

% Found in: CHK
card_name('frostwielder', 'Frostwielder').
card_type('frostwielder', 'Creature — Human Shaman').
card_types('frostwielder', ['Creature']).
card_subtypes('frostwielder', ['Human', 'Shaman']).
card_colors('frostwielder', ['R']).
card_text('frostwielder', '{T}: Frostwielder deals 1 damage to target creature or player.\nIf a creature dealt damage by Frostwielder this turn would die, exile it instead.').
card_mana_cost('frostwielder', ['2', 'R', 'R']).
card_cmc('frostwielder', 4).
card_layout('frostwielder', 'normal').
card_power('frostwielder', 1).
card_toughness('frostwielder', 2).

% Found in: ROE
card_name('frostwind invoker', 'Frostwind Invoker').
card_type('frostwind invoker', 'Creature — Merfolk Wizard').
card_types('frostwind invoker', ['Creature']).
card_subtypes('frostwind invoker', ['Merfolk', 'Wizard']).
card_colors('frostwind invoker', ['U']).
card_text('frostwind invoker', 'Flying\n{8}: Creatures you control gain flying until end of turn.').
card_mana_cost('frostwind invoker', ['4', 'U']).
card_cmc('frostwind invoker', 5).
card_layout('frostwind invoker', 'normal').
card_power('frostwind invoker', 3).
card_toughness('frostwind invoker', 3).

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB
card_name('frozen shade', 'Frozen Shade').
card_type('frozen shade', 'Creature — Shade').
card_types('frozen shade', ['Creature']).
card_subtypes('frozen shade', ['Shade']).
card_colors('frozen shade', ['B']).
card_text('frozen shade', '{B}: Frozen Shade gets +1/+1 until end of turn.').
card_mana_cost('frozen shade', ['2', 'B']).
card_cmc('frozen shade', 3).
card_layout('frozen shade', 'normal').
card_power('frozen shade', 0).
card_toughness('frozen shade', 1).

% Found in: CSP, SCG
card_name('frozen solid', 'Frozen Solid').
card_type('frozen solid', 'Enchantment — Aura').
card_types('frozen solid', ['Enchantment']).
card_subtypes('frozen solid', ['Aura']).
card_colors('frozen solid', ['U']).
card_text('frozen solid', 'Enchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nWhen enchanted creature is dealt damage, destroy it.').
card_mana_cost('frozen solid', ['1', 'U', 'U']).
card_cmc('frozen solid', 3).
card_layout('frozen solid', 'normal').

% Found in: PLC
card_name('frozen æther', 'Frozen Æther').
card_type('frozen æther', 'Enchantment').
card_types('frozen æther', ['Enchantment']).
card_subtypes('frozen æther', []).
card_colors('frozen æther', ['U']).
card_text('frozen æther', 'Artifacts, creatures, and lands your opponents control enter the battlefield tapped.').
card_mana_cost('frozen æther', ['3', 'U']).
card_cmc('frozen æther', 4).
card_layout('frozen æther', 'normal').

% Found in: FRF
card_name('fruit of the first tree', 'Fruit of the First Tree').
card_type('fruit of the first tree', 'Enchantment — Aura').
card_types('fruit of the first tree', ['Enchantment']).
card_subtypes('fruit of the first tree', ['Aura']).
card_colors('fruit of the first tree', ['G']).
card_text('fruit of the first tree', 'Enchant creature\nWhen enchanted creature dies, you gain X life and draw X cards, where X is its toughness.').
card_mana_cost('fruit of the first tree', ['3', 'G']).
card_cmc('fruit of the first tree', 4).
card_layout('fruit of the first tree', 'normal').

% Found in: pHHO
card_name('fruitcake elemental', 'Fruitcake Elemental').
card_type('fruitcake elemental', 'Creature — Elemental').
card_types('fruitcake elemental', ['Creature']).
card_subtypes('fruitcake elemental', ['Elemental']).
card_colors('fruitcake elemental', ['G']).
card_text('fruitcake elemental', 'Fruitcake Elemental is indestructible.\nAt the end of your turn, Fruitcake Elemental deals 7 damage to you.\n{3}: Target player gains control of Fruitcake Elemental.').
card_mana_cost('fruitcake elemental', ['1', 'G', 'G']).
card_cmc('fruitcake elemental', 3).
card_layout('fruitcake elemental', 'normal').
card_power('fruitcake elemental', 7).
card_toughness('fruitcake elemental', 7).

% Found in: POR
card_name('fruition', 'Fruition').
card_type('fruition', 'Sorcery').
card_types('fruition', ['Sorcery']).
card_subtypes('fruition', []).
card_colors('fruition', ['G']).
card_text('fruition', 'You gain 1 life for each Forest on the battlefield.').
card_mana_cost('fruition', ['G']).
card_cmc('fruition', 1).
card_layout('fruition', 'normal').

% Found in: MBS
card_name('fuel for the cause', 'Fuel for the Cause').
card_type('fuel for the cause', 'Instant').
card_types('fuel for the cause', ['Instant']).
card_subtypes('fuel for the cause', []).
card_colors('fuel for the cause', ['U']).
card_text('fuel for the cause', 'Counter target spell, then proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_mana_cost('fuel for the cause', ['2', 'U', 'U']).
card_cmc('fuel for the cause', 4).
card_layout('fuel for the cause', 'normal').

% Found in: TMP
card_name('fugitive druid', 'Fugitive Druid').
card_type('fugitive druid', 'Creature — Human Druid').
card_types('fugitive druid', ['Creature']).
card_subtypes('fugitive druid', ['Human', 'Druid']).
card_colors('fugitive druid', ['G']).
card_text('fugitive druid', 'Whenever Fugitive Druid becomes the target of an Aura spell, you draw a card.').
card_mana_cost('fugitive druid', ['3', 'G']).
card_cmc('fugitive druid', 4).
card_layout('fugitive druid', 'normal').
card_power('fugitive druid', 3).
card_toughness('fugitive druid', 2).

% Found in: 10E, 8ED, 9ED, LGN, M15
card_name('fugitive wizard', 'Fugitive Wizard').
card_type('fugitive wizard', 'Creature — Human Wizard').
card_types('fugitive wizard', ['Creature']).
card_subtypes('fugitive wizard', ['Human', 'Wizard']).
card_colors('fugitive wizard', ['U']).
card_text('fugitive wizard', '').
card_mana_cost('fugitive wizard', ['U']).
card_cmc('fugitive wizard', 1).
card_layout('fugitive wizard', 'normal').
card_power('fugitive wizard', 1).
card_toughness('fugitive wizard', 1).

% Found in: 7ED, EXO, TPR
card_name('fugue', 'Fugue').
card_type('fugue', 'Sorcery').
card_types('fugue', ['Sorcery']).
card_subtypes('fugue', []).
card_colors('fugue', ['B']).
card_text('fugue', 'Target player discards three cards.').
card_mana_cost('fugue', ['3', 'B', 'B']).
card_cmc('fugue', 5).
card_layout('fugue', 'normal').

% Found in: SOM
card_name('fulgent distraction', 'Fulgent Distraction').
card_type('fulgent distraction', 'Instant').
card_types('fulgent distraction', ['Instant']).
card_subtypes('fulgent distraction', []).
card_colors('fulgent distraction', ['W']).
card_text('fulgent distraction', 'Choose two target creatures. Tap those creatures, then unattach all Equipment from them.').
card_mana_cost('fulgent distraction', ['2', 'W']).
card_cmc('fulgent distraction', 3).
card_layout('fulgent distraction', 'normal').

% Found in: ISD
card_name('full moon\'s rise', 'Full Moon\'s Rise').
card_type('full moon\'s rise', 'Enchantment').
card_types('full moon\'s rise', ['Enchantment']).
card_subtypes('full moon\'s rise', []).
card_colors('full moon\'s rise', ['G']).
card_text('full moon\'s rise', 'Werewolf creatures you control get +1/+0 and have trample.\nSacrifice Full Moon\'s Rise: Regenerate all Werewolf creatures you control.').
card_mana_cost('full moon\'s rise', ['1', 'G']).
card_cmc('full moon\'s rise', 2).
card_layout('full moon\'s rise', 'normal').

% Found in: MM2, SHM
card_name('fulminator mage', 'Fulminator Mage').
card_type('fulminator mage', 'Creature — Elemental Shaman').
card_types('fulminator mage', ['Creature']).
card_subtypes('fulminator mage', ['Elemental', 'Shaman']).
card_colors('fulminator mage', ['B', 'R']).
card_text('fulminator mage', 'Sacrifice Fulminator Mage: Destroy target nonbasic land.').
card_mana_cost('fulminator mage', ['1', 'B/R', 'B/R']).
card_cmc('fulminator mage', 3).
card_layout('fulminator mage', 'normal').
card_power('fulminator mage', 2).
card_toughness('fulminator mage', 2).

% Found in: ICE, ME2
card_name('fumarole', 'Fumarole').
card_type('fumarole', 'Sorcery').
card_types('fumarole', ['Sorcery']).
card_subtypes('fumarole', []).
card_colors('fumarole', ['B', 'R']).
card_text('fumarole', 'As an additional cost to cast Fumarole, pay 3 life.\nDestroy target creature and target land.').
card_mana_cost('fumarole', ['3', 'B', 'R']).
card_cmc('fumarole', 5).
card_layout('fumarole', 'normal').

% Found in: SOM
card_name('fume spitter', 'Fume Spitter').
card_type('fume spitter', 'Creature — Horror').
card_types('fume spitter', ['Creature']).
card_subtypes('fume spitter', ['Horror']).
card_colors('fume spitter', ['B']).
card_text('fume spitter', 'Sacrifice Fume Spitter: Put a -1/-1 counter on target creature.').
card_mana_cost('fume spitter', ['B']).
card_cmc('fume spitter', 1).
card_layout('fume spitter', 'normal').
card_power('fume spitter', 1).
card_toughness('fume spitter', 1).

% Found in: BOK
card_name('fumiko the lowblood', 'Fumiko the Lowblood').
card_type('fumiko the lowblood', 'Legendary Creature — Human Samurai').
card_types('fumiko the lowblood', ['Creature']).
card_subtypes('fumiko the lowblood', ['Human', 'Samurai']).
card_supertypes('fumiko the lowblood', ['Legendary']).
card_colors('fumiko the lowblood', ['R']).
card_text('fumiko the lowblood', 'Fumiko the Lowblood has bushido X, where X is the number of attacking creatures. (Whenever this creature blocks or becomes blocked, it gets +X/+X until end of turn.)\nCreatures your opponents control attack each turn if able.').
card_mana_cost('fumiko the lowblood', ['2', 'R', 'R']).
card_cmc('fumiko the lowblood', 4).
card_layout('fumiko the lowblood', 'normal').
card_power('fumiko the lowblood', 3).
card_toughness('fumiko the lowblood', 2).

% Found in: TSB, VIS
card_name('funeral charm', 'Funeral Charm').
card_type('funeral charm', 'Instant').
card_types('funeral charm', ['Instant']).
card_subtypes('funeral charm', []).
card_colors('funeral charm', ['B']).
card_text('funeral charm', 'Choose one —\n• Target player discards a card.\n• Target creature gets +2/-1 until end of turn.\n• Target creature gains swampwalk until end of turn. (It can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('funeral charm', ['B']).
card_cmc('funeral charm', 1).
card_layout('funeral charm', 'normal').

% Found in: 5ED, HML, ME2
card_name('funeral march', 'Funeral March').
card_type('funeral march', 'Enchantment — Aura').
card_types('funeral march', ['Enchantment']).
card_subtypes('funeral march', ['Aura']).
card_colors('funeral march', ['B']).
card_text('funeral march', 'Enchant creature\nWhen enchanted creature leaves the battlefield, its controller sacrifices a creature.').
card_mana_cost('funeral march', ['1', 'B', 'B']).
card_cmc('funeral march', 3).
card_layout('funeral march', 'normal').

% Found in: JUD
card_name('funeral pyre', 'Funeral Pyre').
card_type('funeral pyre', 'Instant').
card_types('funeral pyre', ['Instant']).
card_subtypes('funeral pyre', []).
card_colors('funeral pyre', ['W']).
card_text('funeral pyre', 'Exile target card from a graveyard. Its owner puts a 1/1 white Spirit creature token with flying onto the battlefield.').
card_mana_cost('funeral pyre', ['W']).
card_cmc('funeral pyre', 1).
card_layout('funeral pyre', 'normal').

% Found in: PLC
card_name('fungal behemoth', 'Fungal Behemoth').
card_type('fungal behemoth', 'Creature — Fungus').
card_types('fungal behemoth', ['Creature']).
card_subtypes('fungal behemoth', ['Fungus']).
card_colors('fungal behemoth', ['G']).
card_text('fungal behemoth', 'Fungal Behemoth\'s power and toughness are each equal to the number of +1/+1 counters on creatures you control.\nSuspend X—{X}{G}{G}. X can\'t be 0. (Rather than cast this card from your hand, you may pay {X}{G}{G} and exile it with X time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)\nWhenever a time counter is removed from Fungal Behemoth while it\'s exiled, you may put a +1/+1 counter on target creature.').
card_mana_cost('fungal behemoth', ['3', 'G']).
card_cmc('fungal behemoth', 4).
card_layout('fungal behemoth', 'normal').
card_power('fungal behemoth', '*').
card_toughness('fungal behemoth', '*').

% Found in: FEM, ME2
card_name('fungal bloom', 'Fungal Bloom').
card_type('fungal bloom', 'Enchantment').
card_types('fungal bloom', ['Enchantment']).
card_subtypes('fungal bloom', []).
card_colors('fungal bloom', ['G']).
card_text('fungal bloom', '{G}{G}: Put a spore counter on target Fungus.').
card_mana_cost('fungal bloom', ['G', 'G']).
card_cmc('fungal bloom', 2).
card_layout('fungal bloom', 'normal').
card_reserved('fungal bloom').

% Found in: CMD, TSP
card_name('fungal reaches', 'Fungal Reaches').
card_type('fungal reaches', 'Land').
card_types('fungal reaches', ['Land']).
card_subtypes('fungal reaches', []).
card_colors('fungal reaches', []).
card_text('fungal reaches', '{T}: Add {1} to your mana pool.\n{1}, {T}: Put a storage counter on Fungal Reaches.\n{1}, Remove X storage counters from Fungal Reaches: Add X mana in any combination of {R} and/or {G} to your mana pool.').
card_layout('fungal reaches', 'normal').

% Found in: APC, pPRE
card_name('fungal shambler', 'Fungal Shambler').
card_type('fungal shambler', 'Creature — Fungus Beast').
card_types('fungal shambler', ['Creature']).
card_subtypes('fungal shambler', ['Fungus', 'Beast']).
card_colors('fungal shambler', ['U', 'B', 'G']).
card_text('fungal shambler', 'Trample\nWhenever Fungal Shambler deals damage to an opponent, you draw a card and that opponent discards a card.').
card_mana_cost('fungal shambler', ['4', 'G', 'U', 'B']).
card_cmc('fungal shambler', 7).
card_layout('fungal shambler', 'normal').
card_power('fungal shambler', 6).
card_toughness('fungal shambler', 4).

% Found in: M13
card_name('fungal sprouting', 'Fungal Sprouting').
card_type('fungal sprouting', 'Sorcery').
card_types('fungal sprouting', ['Sorcery']).
card_subtypes('fungal sprouting', []).
card_colors('fungal sprouting', ['G']).
card_text('fungal sprouting', 'Put X 1/1 green Saproling creature tokens onto the battlefield, where X is the greatest power among creatures you control.').
card_mana_cost('fungal sprouting', ['3', 'G']).
card_cmc('fungal sprouting', 4).
card_layout('fungal sprouting', 'normal').

% Found in: WTH
card_name('fungus elemental', 'Fungus Elemental').
card_type('fungus elemental', 'Creature — Fungus Elemental').
card_types('fungus elemental', ['Creature']).
card_subtypes('fungus elemental', ['Fungus', 'Elemental']).
card_colors('fungus elemental', ['G']).
card_text('fungus elemental', '{G}, Sacrifice a Forest: Put a +2/+2 counter on Fungus Elemental. Activate this ability only if Fungus Elemental entered the battlefield this turn.').
card_mana_cost('fungus elemental', ['3', 'G']).
card_cmc('fungus elemental', 4).
card_layout('fungus elemental', 'normal').
card_power('fungus elemental', 3).
card_toughness('fungus elemental', 3).
card_reserved('fungus elemental').

% Found in: H09, TSP
card_name('fungus sliver', 'Fungus Sliver').
card_type('fungus sliver', 'Creature — Fungus Sliver').
card_types('fungus sliver', ['Creature']).
card_subtypes('fungus sliver', ['Fungus', 'Sliver']).
card_colors('fungus sliver', ['G']).
card_text('fungus sliver', 'All Sliver creatures have \"Whenever this creature is dealt damage, put a +1/+1 counter on it.\" (The damage is dealt before the counter is put on.)').
card_mana_cost('fungus sliver', ['3', 'G']).
card_cmc('fungus sliver', 4).
card_layout('fungus sliver', 'normal').
card_power('fungus sliver', 2).
card_toughness('fungus sliver', 2).

% Found in: 2ED, 3ED, 4ED, 5ED, 8ED, CED, CEI, LEA, LEB
card_name('fungusaur', 'Fungusaur').
card_type('fungusaur', 'Creature — Fungus Lizard').
card_types('fungusaur', ['Creature']).
card_subtypes('fungusaur', ['Fungus', 'Lizard']).
card_colors('fungusaur', ['G']).
card_text('fungusaur', 'Whenever Fungusaur is dealt damage, put a +1/+1 counter on it.').
card_mana_cost('fungusaur', ['3', 'G']).
card_cmc('fungusaur', 4).
card_layout('fungusaur', 'normal').
card_power('fungusaur', 2).
card_toughness('fungusaur', 2).

% Found in: MMQ
card_name('furious assault', 'Furious Assault').
card_type('furious assault', 'Enchantment').
card_types('furious assault', ['Enchantment']).
card_subtypes('furious assault', []).
card_colors('furious assault', ['R']).
card_text('furious assault', 'Whenever you cast a creature spell, Furious Assault deals 1 damage to target player.').
card_mana_cost('furious assault', ['2', 'R']).
card_cmc('furious assault', 3).
card_layout('furious assault', 'normal').

% Found in: GTC
card_name('furious resistance', 'Furious Resistance').
card_type('furious resistance', 'Instant').
card_types('furious resistance', ['Instant']).
card_subtypes('furious resistance', []).
card_colors('furious resistance', ['R']).
card_text('furious resistance', 'Target blocking creature gets +3/+0 and gains first strike until end of turn.').
card_mana_cost('furious resistance', ['R']).
card_cmc('furious resistance', 1).
card_layout('furious resistance', 'normal').

% Found in: EXO, TPR
card_name('furnace brood', 'Furnace Brood').
card_type('furnace brood', 'Creature — Elemental').
card_types('furnace brood', ['Creature']).
card_subtypes('furnace brood', ['Elemental']).
card_colors('furnace brood', ['R']).
card_text('furnace brood', '{R}: Target creature can\'t be regenerated this turn.').
card_mana_cost('furnace brood', ['3', 'R']).
card_cmc('furnace brood', 4).
card_layout('furnace brood', 'normal').
card_power('furnace brood', 3).
card_toughness('furnace brood', 3).

% Found in: C13, SOM
card_name('furnace celebration', 'Furnace Celebration').
card_type('furnace celebration', 'Enchantment').
card_types('furnace celebration', ['Enchantment']).
card_subtypes('furnace celebration', []).
card_colors('furnace celebration', ['R']).
card_text('furnace celebration', 'Whenever you sacrifice another permanent, you may pay {2}. If you do, Furnace Celebration deals 2 damage to target creature or player.').
card_mana_cost('furnace celebration', ['1', 'R', 'R']).
card_cmc('furnace celebration', 3).
card_layout('furnace celebration', 'normal').

% Found in: DST
card_name('furnace dragon', 'Furnace Dragon').
card_type('furnace dragon', 'Creature — Dragon').
card_types('furnace dragon', ['Creature']).
card_subtypes('furnace dragon', ['Dragon']).
card_colors('furnace dragon', ['R']).
card_text('furnace dragon', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nFlying\nWhen Furnace Dragon enters the battlefield, if you cast it from your hand, exile all artifacts.').
card_mana_cost('furnace dragon', ['6', 'R', 'R', 'R']).
card_cmc('furnace dragon', 9).
card_layout('furnace dragon', 'normal').
card_power('furnace dragon', 5).
card_toughness('furnace dragon', 5).

% Found in: PC2
card_name('furnace layer', 'Furnace Layer').
card_type('furnace layer', 'Plane — New Phyrexia').
card_types('furnace layer', ['Plane']).
card_subtypes('furnace layer', ['New Phyrexia']).
card_colors('furnace layer', []).
card_text('furnace layer', 'When you planeswalk to Furnace Layer or at the beginning of your upkeep, select target player at random. That player discards a card. If that player discards a land card this way, he or she loses 3 life.\nWhenever you roll {C}, you may destroy target nonland permanent.').
card_layout('furnace layer', 'plane').

% Found in: 10E, 8ED, 9ED, DPA, HOP, TMP
card_name('furnace of rath', 'Furnace of Rath').
card_type('furnace of rath', 'Enchantment').
card_types('furnace of rath', ['Enchantment']).
card_subtypes('furnace of rath', []).
card_colors('furnace of rath', ['R']).
card_text('furnace of rath', 'If a source would deal damage to a creature or player, it deals double that damage to that creature or player instead.').
card_mana_cost('furnace of rath', ['1', 'R', 'R', 'R']).
card_cmc('furnace of rath', 4).
card_layout('furnace of rath', 'normal').

% Found in: NPH
card_name('furnace scamp', 'Furnace Scamp').
card_type('furnace scamp', 'Creature — Beast').
card_types('furnace scamp', ['Creature']).
card_subtypes('furnace scamp', ['Beast']).
card_colors('furnace scamp', ['R']).
card_text('furnace scamp', 'Whenever Furnace Scamp deals combat damage to a player, you may sacrifice it. If you do, Furnace Scamp deals 3 damage to that player.').
card_mana_cost('furnace scamp', ['R']).
card_cmc('furnace scamp', 1).
card_layout('furnace scamp', 'normal').
card_power('furnace scamp', 1).
card_toughness('furnace scamp', 1).

% Found in: STH
card_name('furnace spirit', 'Furnace Spirit').
card_type('furnace spirit', 'Creature — Spirit').
card_types('furnace spirit', ['Creature']).
card_subtypes('furnace spirit', ['Spirit']).
card_colors('furnace spirit', ['R']).
card_text('furnace spirit', 'Haste\n{R}: Furnace Spirit gets +1/+0 until end of turn.').
card_mana_cost('furnace spirit', ['2', 'R']).
card_cmc('furnace spirit', 3).
card_layout('furnace spirit', 'normal').
card_power('furnace spirit', 1).
card_toughness('furnace spirit', 1).

% Found in: 10E, 5DN, ARC, CMD, DD2, DD3_JVC, M13, M15
card_name('furnace whelp', 'Furnace Whelp').
card_type('furnace whelp', 'Creature — Dragon').
card_types('furnace whelp', ['Creature']).
card_subtypes('furnace whelp', ['Dragon']).
card_colors('furnace whelp', ['R']).
card_text('furnace whelp', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{R}: Furnace Whelp gets +1/+0 until end of turn.').
card_mana_cost('furnace whelp', ['2', 'R', 'R']).
card_cmc('furnace whelp', 4).
card_layout('furnace whelp', 'normal').
card_power('furnace whelp', 2).
card_toughness('furnace whelp', 2).

% Found in: ISD
card_name('furor of the bitten', 'Furor of the Bitten').
card_type('furor of the bitten', 'Enchantment — Aura').
card_types('furor of the bitten', ['Enchantment']).
card_subtypes('furor of the bitten', ['Aura']).
card_colors('furor of the bitten', ['R']).
card_text('furor of the bitten', 'Enchant creature\nEnchanted creature gets +2/+2 and attacks each turn if able.').
card_mana_cost('furor of the bitten', ['R']).
card_cmc('furor of the bitten', 1).
card_layout('furor of the bitten', 'normal').

% Found in: MMA, PLC
card_name('fury charm', 'Fury Charm').
card_type('fury charm', 'Instant').
card_types('fury charm', ['Instant']).
card_subtypes('fury charm', []).
card_colors('fury charm', ['R']).
card_text('fury charm', 'Choose one —\n• Destroy target artifact.\n• Target creature gets +1/+1 and gains trample until end of turn.\n• Remove two time counters from target permanent or suspended card.').
card_mana_cost('fury charm', ['1', 'R']).
card_cmc('fury charm', 2).
card_layout('fury charm', 'normal').

% Found in: CSP, DDN
card_name('fury of the horde', 'Fury of the Horde').
card_type('fury of the horde', 'Sorcery').
card_types('fury of the horde', ['Sorcery']).
card_subtypes('fury of the horde', []).
card_colors('fury of the horde', ['R']).
card_text('fury of the horde', 'You may exile two red cards from your hand rather than pay Fury of the Horde\'s mana cost.\nUntap all creatures that attacked this turn. After this main phase, there is an additional combat phase followed by an additional main phase.').
card_mana_cost('fury of the horde', ['5', 'R', 'R']).
card_cmc('fury of the horde', 7).
card_layout('fury of the horde', 'normal').

% Found in: H09, TSP
card_name('fury sliver', 'Fury Sliver').
card_type('fury sliver', 'Creature — Sliver').
card_types('fury sliver', ['Creature']).
card_subtypes('fury sliver', ['Sliver']).
card_colors('fury sliver', ['R']).
card_text('fury sliver', 'All Sliver creatures have double strike.').
card_mana_cost('fury sliver', ['5', 'R']).
card_cmc('fury sliver', 6).
card_layout('fury sliver', 'normal').
card_power('fury sliver', 3).
card_toughness('fury sliver', 3).

% Found in: M12
card_name('furyborn hellkite', 'Furyborn Hellkite').
card_type('furyborn hellkite', 'Creature — Dragon').
card_types('furyborn hellkite', ['Creature']).
card_subtypes('furyborn hellkite', ['Dragon']).
card_colors('furyborn hellkite', ['R']).
card_text('furyborn hellkite', 'Bloodthirst 6 (If an opponent was dealt damage this turn, this creature enters the battlefield with six +1/+1 counters on it.)\nFlying').
card_mana_cost('furyborn hellkite', ['4', 'R', 'R', 'R']).
card_cmc('furyborn hellkite', 7).
card_layout('furyborn hellkite', 'normal').
card_power('furyborn hellkite', 6).
card_toughness('furyborn hellkite', 6).

% Found in: SHM
card_name('furystoke giant', 'Furystoke Giant').
card_type('furystoke giant', 'Creature — Giant Warrior').
card_types('furystoke giant', ['Creature']).
card_subtypes('furystoke giant', ['Giant', 'Warrior']).
card_colors('furystoke giant', ['R']).
card_text('furystoke giant', 'When Furystoke Giant enters the battlefield, other creatures you control gain \"{T}: This creature deals 2 damage to target creature or player\" until end of turn.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('furystoke giant', ['3', 'R', 'R']).
card_cmc('furystoke giant', 5).
card_layout('furystoke giant', 'normal').
card_power('furystoke giant', 3).
card_toughness('furystoke giant', 3).

% Found in: CON, PC2
card_name('fusion elemental', 'Fusion Elemental').
card_type('fusion elemental', 'Creature — Elemental').
card_types('fusion elemental', ['Creature']).
card_subtypes('fusion elemental', ['Elemental']).
card_colors('fusion elemental', ['W', 'U', 'B', 'R', 'G']).
card_text('fusion elemental', '').
card_mana_cost('fusion elemental', ['W', 'U', 'B', 'R', 'G']).
card_cmc('fusion elemental', 5).
card_layout('fusion elemental', 'normal').
card_power('fusion elemental', 8).
card_toughness('fusion elemental', 8).

% Found in: DDM, ONS, VMA
card_name('future sight', 'Future Sight').
card_type('future sight', 'Enchantment').
card_types('future sight', ['Enchantment']).
card_subtypes('future sight', []).
card_colors('future sight', ['U']).
card_text('future sight', 'Play with the top card of your library revealed.\nYou may play the top card of your library.').
card_mana_cost('future sight', ['2', 'U', 'U', 'U']).
card_cmc('future sight', 5).
card_layout('future sight', 'normal').

% Found in: TMP
card_name('fylamarid', 'Fylamarid').
card_type('fylamarid', 'Creature — Squid Beast').
card_types('fylamarid', ['Creature']).
card_subtypes('fylamarid', ['Squid', 'Beast']).
card_colors('fylamarid', ['U']).
card_text('fylamarid', 'Flying\nFylamarid can\'t be blocked by blue creatures.\n{U}: Target creature becomes blue until end of turn.').
card_mana_cost('fylamarid', ['1', 'U', 'U']).
card_cmc('fylamarid', 3).
card_layout('fylamarid', 'normal').
card_power('fylamarid', 1).
card_toughness('fylamarid', 3).

% Found in: ICE
card_name('fylgja', 'Fylgja').
card_type('fylgja', 'Enchantment — Aura').
card_types('fylgja', ['Enchantment']).
card_subtypes('fylgja', ['Aura']).
card_colors('fylgja', ['W']).
card_text('fylgja', 'Enchant creature\nFylgja enters the battlefield with four healing counters on it.\nRemove a healing counter from Fylgja: Prevent the next 1 damage that would be dealt to enchanted creature this turn.\n{2}{W}: Put a healing counter on Fylgja.').
card_mana_cost('fylgja', ['W']).
card_cmc('fylgja', 1).
card_layout('fylgja', 'normal').

% Found in: ICE
card_name('fyndhorn bow', 'Fyndhorn Bow').
card_type('fyndhorn bow', 'Artifact').
card_types('fyndhorn bow', ['Artifact']).
card_subtypes('fyndhorn bow', []).
card_colors('fyndhorn bow', []).
card_text('fyndhorn bow', '{3}, {T}: Target creature gains first strike until end of turn.').
card_mana_cost('fyndhorn bow', ['2']).
card_cmc('fyndhorn bow', 2).
card_layout('fyndhorn bow', 'normal').

% Found in: 6ED, ICE
card_name('fyndhorn brownie', 'Fyndhorn Brownie').
card_type('fyndhorn brownie', 'Creature — Ouphe').
card_types('fyndhorn brownie', ['Creature']).
card_subtypes('fyndhorn brownie', ['Ouphe']).
card_colors('fyndhorn brownie', ['G']).
card_text('fyndhorn brownie', '{2}{G}, {T}: Untap target creature.').
card_mana_cost('fyndhorn brownie', ['2', 'G']).
card_cmc('fyndhorn brownie', 3).
card_layout('fyndhorn brownie', 'normal').
card_power('fyndhorn brownie', 1).
card_toughness('fyndhorn brownie', 1).

% Found in: ALL
card_name('fyndhorn druid', 'Fyndhorn Druid').
card_type('fyndhorn druid', 'Creature — Elf Druid').
card_types('fyndhorn druid', ['Creature']).
card_subtypes('fyndhorn druid', ['Elf', 'Druid']).
card_colors('fyndhorn druid', ['G']).
card_text('fyndhorn druid', 'When Fyndhorn Druid dies, if it was blocked this turn, you gain 4 life.').
card_mana_cost('fyndhorn druid', ['2', 'G']).
card_cmc('fyndhorn druid', 3).
card_layout('fyndhorn druid', 'normal').
card_power('fyndhorn druid', 2).
card_toughness('fyndhorn druid', 2).

% Found in: 5ED, 6ED, 7ED, 8ED, ICE
card_name('fyndhorn elder', 'Fyndhorn Elder').
card_type('fyndhorn elder', 'Creature — Elf Druid').
card_types('fyndhorn elder', ['Creature']).
card_subtypes('fyndhorn elder', ['Elf', 'Druid']).
card_colors('fyndhorn elder', ['G']).
card_text('fyndhorn elder', '{T}: Add {G}{G} to your mana pool.').
card_mana_cost('fyndhorn elder', ['2', 'G']).
card_cmc('fyndhorn elder', 3).
card_layout('fyndhorn elder', 'normal').
card_power('fyndhorn elder', 1).
card_toughness('fyndhorn elder', 1).

% Found in: DKM, ICE, MED, V13, VMA
card_name('fyndhorn elves', 'Fyndhorn Elves').
card_type('fyndhorn elves', 'Creature — Elf Druid').
card_types('fyndhorn elves', ['Creature']).
card_subtypes('fyndhorn elves', ['Elf', 'Druid']).
card_colors('fyndhorn elves', ['G']).
card_text('fyndhorn elves', '{T}: Add {G} to your mana pool.').
card_mana_cost('fyndhorn elves', ['G']).
card_cmc('fyndhorn elves', 1).
card_layout('fyndhorn elves', 'normal').
card_power('fyndhorn elves', 1).
card_toughness('fyndhorn elves', 1).

% Found in: ICE, ME2
card_name('fyndhorn pollen', 'Fyndhorn Pollen').
card_type('fyndhorn pollen', 'Enchantment').
card_types('fyndhorn pollen', ['Enchantment']).
card_subtypes('fyndhorn pollen', []).
card_colors('fyndhorn pollen', ['G']).
card_text('fyndhorn pollen', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAll creatures get -1/-0.\n{1}{G}: All creatures get -1/-0 until end of turn.').
card_mana_cost('fyndhorn pollen', ['2', 'G']).
card_cmc('fyndhorn pollen', 3).
card_layout('fyndhorn pollen', 'normal').
card_reserved('fyndhorn pollen').

