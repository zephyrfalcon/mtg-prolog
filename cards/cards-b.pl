% Card-specific data

% Found in: UNH
card_name('b-i-n-g-o', 'B-I-N-G-O').
card_type('b-i-n-g-o', 'Creature — Hound').
card_types('b-i-n-g-o', ['Creature']).
card_subtypes('b-i-n-g-o', ['Hound']).
card_colors('b-i-n-g-o', ['G']).
card_text('b-i-n-g-o', 'Trample\nWhenever a player plays a spell, put a chip counter on its converted mana cost.  \nB-I-N-G-O gets +9/+9 for each set of three numbers in a row with chip counters on them.').
card_mana_cost('b-i-n-g-o', ['1', 'G']).
card_cmc('b-i-n-g-o', 2).
card_layout('b-i-n-g-o', 'normal').
card_power('b-i-n-g-o', 1).
card_toughness('b-i-n-g-o', 1).

% Found in: UGL
card_name('b.f.m. (big furry monster)', 'B.F.M. (Big Furry Monster)').
card_type('b.f.m. (big furry monster)', 'Creature — The Biggest, Baddest, Nastiest,').
card_types('b.f.m. (big furry monster)', ['Creature']).
card_subtypes('b.f.m. (big furry monster)', ['The', 'Biggest,', 'Baddest,', 'Nastiest,']).
card_colors('b.f.m. (big furry monster)', []).
card_text('b.f.m. (big furry monster)', 'You must play both B.F.M. cards to put\nleaves play, sacrifice the other.\nB.F.M. can be blocked only by three or').
card_layout('b.f.m. (big furry monster)', 'normal').

% Found in: ISD
card_name('back from the brink', 'Back from the Brink').
card_type('back from the brink', 'Enchantment').
card_types('back from the brink', ['Enchantment']).
card_subtypes('back from the brink', []).
card_colors('back from the brink', ['U']).
card_text('back from the brink', 'Exile a creature card from your graveyard and pay its mana cost: Put a token onto the battlefield that\'s a copy of that card. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('back from the brink', ['4', 'U', 'U']).
card_cmc('back from the brink', 6).
card_layout('back from the brink', 'normal').

% Found in: USG
card_name('back to basics', 'Back to Basics').
card_type('back to basics', 'Enchantment').
card_types('back to basics', ['Enchantment']).
card_subtypes('back to basics', []).
card_colors('back to basics', ['U']).
card_text('back to basics', 'Nonbasic lands don\'t untap during their controllers\' untap steps.').
card_mana_cost('back to basics', ['2', 'U']).
card_cmc('back to basics', 3).
card_layout('back to basics', 'normal').

% Found in: M11, M15
card_name('back to nature', 'Back to Nature').
card_type('back to nature', 'Instant').
card_types('back to nature', ['Instant']).
card_subtypes('back to nature', []).
card_colors('back to nature', ['G']).
card_text('back to nature', 'Destroy all enchantments.').
card_mana_cost('back to nature', ['1', 'G']).
card_cmc('back to nature', 2).
card_layout('back to nature', 'normal').

% Found in: LEG
card_name('backdraft', 'Backdraft').
card_type('backdraft', 'Instant').
card_types('backdraft', ['Instant']).
card_subtypes('backdraft', []).
card_colors('backdraft', ['R']).
card_text('backdraft', 'Choose a player who cast one or more sorcery spells this turn. Backdraft deals damage to that player equal to half the damage dealt by one of those sorcery spells this turn, rounded down.').
card_mana_cost('backdraft', ['1', 'R']).
card_cmc('backdraft', 2).
card_layout('backdraft', 'normal').

% Found in: 4ED, LEG
card_name('backfire', 'Backfire').
card_type('backfire', 'Enchantment — Aura').
card_types('backfire', ['Enchantment']).
card_subtypes('backfire', ['Aura']).
card_colors('backfire', ['U']).
card_text('backfire', 'Enchant creature\nWhenever enchanted creature deals damage to you, Backfire deals that much damage to that creature\'s controller.').
card_mana_cost('backfire', ['U']).
card_cmc('backfire', 1).
card_layout('backfire', 'normal').

% Found in: INV
card_name('backlash', 'Backlash').
card_type('backlash', 'Instant').
card_types('backlash', ['Instant']).
card_subtypes('backlash', []).
card_colors('backlash', ['B', 'R']).
card_text('backlash', 'Tap target untapped creature. That creature deals damage equal to its power to its controller.').
card_mana_cost('backlash', ['1', 'B', 'R']).
card_cmc('backlash', 3).
card_layout('backlash', 'normal').

% Found in: ONS
card_name('backslide', 'Backslide').
card_type('backslide', 'Instant').
card_types('backslide', ['Instant']).
card_subtypes('backslide', []).
card_colors('backslide', ['U']).
card_text('backslide', 'Turn target creature with a morph ability face down.\nCycling {U} ({U}, Discard this card: Draw a card.)').
card_mana_cost('backslide', ['1', 'U']).
card_cmc('backslide', 2).
card_layout('backslide', 'normal').

% Found in: CNS
card_name('backup plan', 'Backup Plan').
card_type('backup plan', 'Conspiracy').
card_types('backup plan', ['Conspiracy']).
card_subtypes('backup plan', []).
card_colors('backup plan', []).
card_text('backup plan', '(Start the game with this conspiracy face up in the command zone.)\nDraw an additional hand of seven cards as the game begins. Before taking mulligans, shuffle all but one of your hands into your library.').
card_layout('backup plan', 'normal').

% Found in: UNH
card_name('bad ass', 'Bad Ass').
card_type('bad ass', 'Creature — Donkey Zombie').
card_types('bad ass', ['Creature']).
card_subtypes('bad ass', ['Donkey', 'Zombie']).
card_colors('bad ass', ['B']).
card_text('bad ass', '{1}{B}, Growl: Regenerate Bad Ass.').
card_mana_cost('bad ass', ['2', 'B', 'B']).
card_cmc('bad ass', 4).
card_layout('bad ass', 'normal').
card_power('bad ass', 3.5).
card_toughness('bad ass', 1).

% Found in: 2ED, 3ED, 4ED, 5ED, C14, CED, CEI, DD3_GVL, DDD, LEA, LEB, TSB
card_name('bad moon', 'Bad Moon').
card_type('bad moon', 'Enchantment').
card_types('bad moon', ['Enchantment']).
card_subtypes('bad moon', []).
card_colors('bad moon', ['B']).
card_text('bad moon', 'Black creatures get +1/+1.').
card_mana_cost('bad moon', ['1', 'B']).
card_cmc('bad moon', 2).
card_layout('bad moon', 'normal').

% Found in: MIR, VMA
card_name('bad river', 'Bad River').
card_type('bad river', 'Land').
card_types('bad river', ['Land']).
card_subtypes('bad river', []).
card_colors('bad river', []).
card_text('bad river', 'Bad River enters the battlefield tapped.\n{T}, Sacrifice Bad River: Search your library for an Island or Swamp card and put it onto the battlefield. Then shuffle your library.').
card_layout('bad river', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME2, ME4, VMA
card_name('badlands', 'Badlands').
card_type('badlands', 'Land — Swamp Mountain').
card_types('badlands', ['Land']).
card_subtypes('badlands', ['Swamp', 'Mountain']).
card_colors('badlands', []).
card_text('badlands', '({T}: Add {B} or {R} to your mana pool.)').
card_layout('badlands', 'normal').
card_reserved('badlands').

% Found in: HML
card_name('baki\'s curse', 'Baki\'s Curse').
card_type('baki\'s curse', 'Sorcery').
card_types('baki\'s curse', ['Sorcery']).
card_subtypes('baki\'s curse', []).
card_colors('baki\'s curse', ['U']).
card_text('baki\'s curse', 'Baki\'s Curse deals 2 damage to each creature for each Aura attached to that creature.').
card_mana_cost('baki\'s curse', ['2', 'U', 'U']).
card_cmc('baki\'s curse', 4).
card_layout('baki\'s curse', 'normal').
card_reserved('baki\'s curse').

% Found in: BOK
card_name('baku altar', 'Baku Altar').
card_type('baku altar', 'Artifact').
card_types('baku altar', ['Artifact']).
card_subtypes('baku altar', []).
card_colors('baku altar', []).
card_text('baku altar', 'Whenever you cast a Spirit or Arcane spell, you may put a ki counter on Baku Altar.\n{2}, {T}, Remove a ki counter from Baku Altar: Put a 1/1 colorless Spirit creature token onto the battlefield.').
card_mana_cost('baku altar', ['2']).
card_cmc('baku altar', 2).
card_layout('baku altar', 'normal').

% Found in: ROE
card_name('bala ged scorpion', 'Bala Ged Scorpion').
card_type('bala ged scorpion', 'Creature — Scorpion').
card_types('bala ged scorpion', ['Creature']).
card_subtypes('bala ged scorpion', ['Scorpion']).
card_colors('bala ged scorpion', ['B']).
card_text('bala ged scorpion', 'When Bala Ged Scorpion enters the battlefield, you may destroy target creature with power 1 or less.').
card_mana_cost('bala ged scorpion', ['3', 'B']).
card_cmc('bala ged scorpion', 4).
card_layout('bala ged scorpion', 'normal').
card_power('bala ged scorpion', 2).
card_toughness('bala ged scorpion', 3).

% Found in: ZEN
card_name('bala ged thief', 'Bala Ged Thief').
card_type('bala ged thief', 'Creature — Human Rogue Ally').
card_types('bala ged thief', ['Creature']).
card_subtypes('bala ged thief', ['Human', 'Rogue', 'Ally']).
card_colors('bala ged thief', ['B']).
card_text('bala ged thief', 'Whenever Bala Ged Thief or another Ally enters the battlefield under your control, target player reveals a number of cards from his or her hand equal to the number of Allies you control. You choose one of them. That player discards that card.').
card_mana_cost('bala ged thief', ['3', 'B']).
card_cmc('bala ged thief', 4).
card_layout('bala ged thief', 'normal').
card_power('bala ged thief', 2).
card_toughness('bala ged thief', 2).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB, ME4, V09, VMA, pJGP
card_name('balance', 'Balance').
card_type('balance', 'Sorcery').
card_types('balance', ['Sorcery']).
card_subtypes('balance', []).
card_colors('balance', ['W']).
card_text('balance', 'Each player chooses a number of lands he or she controls equal to the number of lands controlled by the player who controls the fewest, then sacrifices the rest. Players discard cards and sacrifice creatures the same way.').
card_mana_cost('balance', ['1', 'W']).
card_cmc('balance', 2).
card_layout('balance', 'normal').

% Found in: 8ED, POR, PTK
card_name('balance of power', 'Balance of Power').
card_type('balance of power', 'Sorcery').
card_types('balance of power', ['Sorcery']).
card_subtypes('balance of power', []).
card_colors('balance of power', ['U']).
card_text('balance of power', 'If target opponent has more cards in hand than you, draw cards equal to the difference.').
card_mana_cost('balance of power', ['3', 'U', 'U']).
card_cmc('balance of power', 5).
card_layout('balance of power', 'normal').

% Found in: ODY
card_name('balancing act', 'Balancing Act').
card_type('balancing act', 'Sorcery').
card_types('balancing act', ['Sorcery']).
card_subtypes('balancing act', []).
card_colors('balancing act', ['W']).
card_text('balancing act', 'Each player chooses a number of permanents he or she controls equal to the number of permanents controlled by the player who controls the fewest, then sacrifices the rest. Each player discards cards the same way.').
card_mana_cost('balancing act', ['2', 'W', 'W']).
card_cmc('balancing act', 4).
card_layout('balancing act', 'normal').

% Found in: 6ED, 7ED, 8ED, 9ED, ICE
card_name('balduvian barbarians', 'Balduvian Barbarians').
card_type('balduvian barbarians', 'Creature — Human Barbarian').
card_types('balduvian barbarians', ['Creature']).
card_subtypes('balduvian barbarians', ['Human', 'Barbarian']).
card_colors('balduvian barbarians', ['R']).
card_text('balduvian barbarians', '').
card_mana_cost('balduvian barbarians', ['1', 'R', 'R']).
card_cmc('balduvian barbarians', 3).
card_layout('balduvian barbarians', 'normal').
card_power('balduvian barbarians', 3).
card_toughness('balduvian barbarians', 2).

% Found in: DKM, ICE
card_name('balduvian bears', 'Balduvian Bears').
card_type('balduvian bears', 'Creature — Bear').
card_types('balduvian bears', ['Creature']).
card_subtypes('balduvian bears', ['Bear']).
card_colors('balduvian bears', ['G']).
card_text('balduvian bears', '').
card_mana_cost('balduvian bears', ['1', 'G']).
card_cmc('balduvian bears', 2).
card_layout('balduvian bears', 'normal').
card_power('balduvian bears', 2).
card_toughness('balduvian bears', 2).

% Found in: ICE, ME2
card_name('balduvian conjurer', 'Balduvian Conjurer').
card_type('balduvian conjurer', 'Creature — Human Wizard').
card_types('balduvian conjurer', ['Creature']).
card_subtypes('balduvian conjurer', ['Human', 'Wizard']).
card_colors('balduvian conjurer', ['U']).
card_text('balduvian conjurer', '{T}: Target snow land becomes a 2/2 creature until end of turn. It\'s still a land.').
card_mana_cost('balduvian conjurer', ['1', 'U']).
card_cmc('balduvian conjurer', 2).
card_layout('balduvian conjurer', 'normal').
card_power('balduvian conjurer', 0).
card_toughness('balduvian conjurer', 2).

% Found in: ALL, CST, ME2
card_name('balduvian dead', 'Balduvian Dead').
card_type('balduvian dead', 'Creature — Zombie').
card_types('balduvian dead', ['Creature']).
card_subtypes('balduvian dead', ['Zombie']).
card_colors('balduvian dead', ['B']).
card_text('balduvian dead', '{2}{R}, Exile a creature card from your graveyard: Put a 3/1 black and red Graveborn creature token with haste onto the battlefield. Sacrifice it at the beginning of the next end step.').
card_mana_cost('balduvian dead', ['3', 'B']).
card_cmc('balduvian dead', 4).
card_layout('balduvian dead', 'normal').
card_power('balduvian dead', 2).
card_toughness('balduvian dead', 3).

% Found in: CSP
card_name('balduvian fallen', 'Balduvian Fallen').
card_type('balduvian fallen', 'Creature — Zombie').
card_types('balduvian fallen', ['Creature']).
card_subtypes('balduvian fallen', ['Zombie']).
card_colors('balduvian fallen', ['B']).
card_text('balduvian fallen', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhenever Balduvian Fallen\'s cumulative upkeep is paid, it gets +1/+0 until end of turn for each {B} or {R} spent this way.').
card_mana_cost('balduvian fallen', ['3', 'B']).
card_cmc('balduvian fallen', 4).
card_layout('balduvian fallen', 'normal').
card_power('balduvian fallen', 3).
card_toughness('balduvian fallen', 5).

% Found in: CSP
card_name('balduvian frostwaker', 'Balduvian Frostwaker').
card_type('balduvian frostwaker', 'Creature — Human Wizard').
card_types('balduvian frostwaker', ['Creature']).
card_subtypes('balduvian frostwaker', ['Human', 'Wizard']).
card_colors('balduvian frostwaker', ['U']).
card_text('balduvian frostwaker', '{U}, {T}: Target snow land becomes a 2/2 blue Elemental creature with flying. It\'s still a land.').
card_mana_cost('balduvian frostwaker', ['2', 'U']).
card_cmc('balduvian frostwaker', 3).
card_layout('balduvian frostwaker', 'normal').
card_power('balduvian frostwaker', 1).
card_toughness('balduvian frostwaker', 1).

% Found in: 6ED, ALL, BTD, DKM, MED, pWOR
card_name('balduvian horde', 'Balduvian Horde').
card_type('balduvian horde', 'Creature — Human Barbarian').
card_types('balduvian horde', ['Creature']).
card_subtypes('balduvian horde', ['Human', 'Barbarian']).
card_colors('balduvian horde', ['R']).
card_text('balduvian horde', 'When Balduvian Horde enters the battlefield, sacrifice it unless you discard a card at random.').
card_mana_cost('balduvian horde', ['2', 'R', 'R']).
card_cmc('balduvian horde', 4).
card_layout('balduvian horde', 'normal').
card_power('balduvian horde', 5).
card_toughness('balduvian horde', 5).

% Found in: ICE, ME2
card_name('balduvian hydra', 'Balduvian Hydra').
card_type('balduvian hydra', 'Creature — Hydra').
card_types('balduvian hydra', ['Creature']).
card_subtypes('balduvian hydra', ['Hydra']).
card_colors('balduvian hydra', ['R']).
card_text('balduvian hydra', 'Balduvian Hydra enters the battlefield with X +1/+0 counters on it.\nRemove a +1/+0 counter from Balduvian Hydra: Prevent the next 1 damage that would be dealt to Balduvian Hydra this turn.\n{R}{R}{R}: Put a +1/+0 counter on Balduvian Hydra. Activate this ability only during your upkeep.').
card_mana_cost('balduvian hydra', ['X', 'R', 'R']).
card_cmc('balduvian hydra', 2).
card_layout('balduvian hydra', 'normal').
card_power('balduvian hydra', 0).
card_toughness('balduvian hydra', 1).
card_reserved('balduvian hydra').

% Found in: CSP
card_name('balduvian rage', 'Balduvian Rage').
card_type('balduvian rage', 'Instant').
card_types('balduvian rage', ['Instant']).
card_subtypes('balduvian rage', []).
card_colors('balduvian rage', ['R']).
card_text('balduvian rage', 'Target attacking creature gets +X/+0 until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('balduvian rage', ['X', 'R']).
card_cmc('balduvian rage', 1).
card_layout('balduvian rage', 'normal').

% Found in: ICE
card_name('balduvian shaman', 'Balduvian Shaman').
card_type('balduvian shaman', 'Creature — Human Cleric Shaman').
card_types('balduvian shaman', ['Creature']).
card_subtypes('balduvian shaman', ['Human', 'Cleric', 'Shaman']).
card_colors('balduvian shaman', ['U']).
card_text('balduvian shaman', '{T}: Change the text of target white enchantment you control that doesn\'t have cumulative upkeep by replacing all instances of one color word with another. (For example, you may change \"black creatures can\'t attack\" to \"blue creatures can\'t attack.\") That enchantment gains \"Cumulative upkeep {1}.\" (At the beginning of its controller\'s upkeep, that player puts an age counter on it, then sacrifices it unless he or she pays its upkeep cost for each age counter on it.)').
card_mana_cost('balduvian shaman', ['U']).
card_cmc('balduvian shaman', 1).
card_layout('balduvian shaman', 'normal').
card_power('balduvian shaman', 1).
card_toughness('balduvian shaman', 1).

% Found in: ALL, ME2
card_name('balduvian trading post', 'Balduvian Trading Post').
card_type('balduvian trading post', 'Land').
card_types('balduvian trading post', ['Land']).
card_subtypes('balduvian trading post', []).
card_colors('balduvian trading post', []).
card_text('balduvian trading post', 'If Balduvian Trading Post would enter the battlefield, sacrifice an untapped Mountain instead. If you do, put Balduvian Trading Post onto the battlefield. If you don\'t, put it into its owner\'s graveyard.\n{T}: Add {1}{R} to your mana pool.\n{1}, {T}: Balduvian Trading Post deals 1 damage to target attacking creature.').
card_layout('balduvian trading post', 'normal').
card_reserved('balduvian trading post').

% Found in: ALL
card_name('balduvian war-makers', 'Balduvian War-Makers').
card_type('balduvian war-makers', 'Creature — Human Barbarian').
card_types('balduvian war-makers', ['Creature']).
card_subtypes('balduvian war-makers', ['Human', 'Barbarian']).
card_colors('balduvian war-makers', ['R']).
card_text('balduvian war-makers', 'Haste\nRampage 1 (Whenever this creature becomes blocked, it gets +1/+1 until end of turn for each creature blocking it beyond the first.)').
card_mana_cost('balduvian war-makers', ['4', 'R']).
card_cmc('balduvian war-makers', 5).
card_layout('balduvian war-makers', 'normal').
card_power('balduvian war-makers', 3).
card_toughness('balduvian war-makers', 3).

% Found in: CSP
card_name('balduvian warlord', 'Balduvian Warlord').
card_type('balduvian warlord', 'Creature — Human Barbarian').
card_types('balduvian warlord', ['Creature']).
card_subtypes('balduvian warlord', ['Human', 'Barbarian']).
card_colors('balduvian warlord', ['R']).
card_text('balduvian warlord', '{T}: Remove target blocking creature from combat. Creatures it was blocking that hadn\'t become blocked by another creature this combat become unblocked, then it blocks an attacking creature of your choice. Activate this ability only during the declare blockers step.').
card_mana_cost('balduvian warlord', ['3', 'R']).
card_cmc('balduvian warlord', 4).
card_layout('balduvian warlord', 'normal').
card_power('balduvian warlord', 3).
card_toughness('balduvian warlord', 2).

% Found in: ISD
card_name('balefire dragon', 'Balefire Dragon').
card_type('balefire dragon', 'Creature — Dragon').
card_types('balefire dragon', ['Creature']).
card_subtypes('balefire dragon', ['Dragon']).
card_colors('balefire dragon', ['R']).
card_text('balefire dragon', 'Flying\nWhenever Balefire Dragon deals combat damage to a player, it deals that much damage to each creature that player controls.').
card_mana_cost('balefire dragon', ['5', 'R', 'R']).
card_cmc('balefire dragon', 7).
card_layout('balefire dragon', 'normal').
card_power('balefire dragon', 6).
card_toughness('balefire dragon', 6).

% Found in: EVE, HOP
card_name('balefire liege', 'Balefire Liege').
card_type('balefire liege', 'Creature — Spirit Horror').
card_types('balefire liege', ['Creature']).
card_subtypes('balefire liege', ['Spirit', 'Horror']).
card_colors('balefire liege', ['W', 'R']).
card_text('balefire liege', 'Other red creatures you control get +1/+1.\nOther white creatures you control get +1/+1.\nWhenever you cast a red spell, Balefire Liege deals 3 damage to target player.\nWhenever you cast a white spell, you gain 3 life.').
card_mana_cost('balefire liege', ['2', 'R/W', 'R/W', 'R/W']).
card_cmc('balefire liege', 5).
card_layout('balefire liege', 'normal').
card_power('balefire liege', 2).
card_toughness('balefire liege', 4).

% Found in: THS
card_name('baleful eidolon', 'Baleful Eidolon').
card_type('baleful eidolon', 'Enchantment Creature — Spirit').
card_types('baleful eidolon', ['Enchantment', 'Creature']).
card_subtypes('baleful eidolon', ['Spirit']).
card_colors('baleful eidolon', ['B']).
card_text('baleful eidolon', 'Bestow {4}{B} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nEnchanted creature gets +1/+1 and has deathtouch.').
card_mana_cost('baleful eidolon', ['1', 'B']).
card_cmc('baleful eidolon', 2).
card_layout('baleful eidolon', 'normal').
card_power('baleful eidolon', 1).
card_toughness('baleful eidolon', 1).

% Found in: C13, VMA
card_name('baleful force', 'Baleful Force').
card_type('baleful force', 'Creature — Elemental').
card_types('baleful force', ['Creature']).
card_subtypes('baleful force', ['Elemental']).
card_colors('baleful force', ['B']).
card_text('baleful force', 'At the beginning of each upkeep, you draw a card and you lose 1 life.').
card_mana_cost('baleful force', ['5', 'B', 'B', 'B']).
card_cmc('baleful force', 8).
card_layout('baleful force', 'normal').
card_power('baleful force', 7).
card_toughness('baleful force', 7).

% Found in: 7ED, 9ED, POR
card_name('baleful stare', 'Baleful Stare').
card_type('baleful stare', 'Sorcery').
card_types('baleful stare', ['Sorcery']).
card_subtypes('baleful stare', []).
card_colors('baleful stare', ['U']).
card_text('baleful stare', 'Target opponent reveals his or her hand. You draw a card for each Mountain and red card in it.').
card_mana_cost('baleful stare', ['2', 'U']).
card_cmc('baleful stare', 3).
card_layout('baleful stare', 'normal').

% Found in: C13, PC2, VMA
card_name('baleful strix', 'Baleful Strix').
card_type('baleful strix', 'Artifact Creature — Bird').
card_types('baleful strix', ['Artifact', 'Creature']).
card_subtypes('baleful strix', ['Bird']).
card_colors('baleful strix', ['U', 'B']).
card_text('baleful strix', 'Flying, deathtouch\nWhen Baleful Strix enters the battlefield, draw a card.').
card_mana_cost('baleful strix', ['U', 'B']).
card_cmc('baleful strix', 2).
card_layout('baleful strix', 'normal').
card_power('baleful strix', 1).
card_toughness('baleful strix', 1).

% Found in: 4ED, 5ED, BTD, DRK, M10, MED, PD2, pJGP
card_name('ball lightning', 'Ball Lightning').
card_type('ball lightning', 'Creature — Elemental').
card_types('ball lightning', ['Creature']).
card_subtypes('ball lightning', ['Elemental']).
card_colors('ball lightning', ['R']).
card_text('ball lightning', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)\nHaste (This creature can attack and {T} as soon as it comes under your control.)\nAt the beginning of the end step, sacrifice Ball Lightning.').
card_mana_cost('ball lightning', ['R', 'R', 'R']).
card_cmc('ball lightning', 3).
card_layout('ball lightning', 'normal').
card_power('ball lightning', 6).
card_toughness('ball lightning', 1).

% Found in: 10E, 9ED, MMQ
card_name('ballista squad', 'Ballista Squad').
card_type('ballista squad', 'Creature — Human Rebel').
card_types('ballista squad', ['Creature']).
card_subtypes('ballista squad', ['Human', 'Rebel']).
card_colors('ballista squad', ['W']).
card_text('ballista squad', '{X}{W}, {T}: Ballista Squad deals X damage to target attacking or blocking creature.').
card_mana_cost('ballista squad', ['3', 'W']).
card_cmc('ballista squad', 4).
card_layout('ballista squad', 'normal').
card_power('ballista squad', 2).
card_toughness('ballista squad', 2).

% Found in: MMQ
card_name('balloon peddler', 'Balloon Peddler').
card_type('balloon peddler', 'Creature — Human Spellshaper').
card_types('balloon peddler', ['Creature']).
card_subtypes('balloon peddler', ['Human', 'Spellshaper']).
card_colors('balloon peddler', ['U']).
card_text('balloon peddler', '{U}, {T}, Discard a card: Target creature gains flying until end of turn.').
card_mana_cost('balloon peddler', ['2', 'U']).
card_cmc('balloon peddler', 3).
card_layout('balloon peddler', 'normal').
card_power('balloon peddler', 2).
card_toughness('balloon peddler', 2).

% Found in: SHM
card_name('ballynock cohort', 'Ballynock Cohort').
card_type('ballynock cohort', 'Creature — Kithkin Soldier').
card_types('ballynock cohort', ['Creature']).
card_subtypes('ballynock cohort', ['Kithkin', 'Soldier']).
card_colors('ballynock cohort', ['W']).
card_text('ballynock cohort', 'First strike\nBallynock Cohort gets +1/+1 as long as you control another white creature.').
card_mana_cost('ballynock cohort', ['2', 'W']).
card_cmc('ballynock cohort', 3).
card_layout('ballynock cohort', 'normal').
card_power('ballynock cohort', 2).
card_toughness('ballynock cohort', 2).

% Found in: EVE
card_name('ballynock trapper', 'Ballynock Trapper').
card_type('ballynock trapper', 'Creature — Kithkin Soldier').
card_types('ballynock trapper', ['Creature']).
card_subtypes('ballynock trapper', ['Kithkin', 'Soldier']).
card_colors('ballynock trapper', ['W']).
card_text('ballynock trapper', '{T}: Tap target creature.\nWhenever you cast a white spell, you may untap Ballynock Trapper.').
card_mana_cost('ballynock trapper', ['3', 'W']).
card_cmc('ballynock trapper', 4).
card_layout('ballynock trapper', 'normal').
card_power('ballynock trapper', 2).
card_toughness('ballynock trapper', 2).

% Found in: MOR
card_name('ballyrush banneret', 'Ballyrush Banneret').
card_type('ballyrush banneret', 'Creature — Kithkin Soldier').
card_types('ballyrush banneret', ['Creature']).
card_subtypes('ballyrush banneret', ['Kithkin', 'Soldier']).
card_colors('ballyrush banneret', ['W']).
card_text('ballyrush banneret', 'Kithkin spells and Soldier spells you cast cost {1} less to cast.').
card_mana_cost('ballyrush banneret', ['1', 'W']).
card_cmc('ballyrush banneret', 2).
card_layout('ballyrush banneret', 'normal').
card_power('ballyrush banneret', 2).
card_toughness('ballyrush banneret', 1).

% Found in: FEM
card_name('balm of restoration', 'Balm of Restoration').
card_type('balm of restoration', 'Artifact').
card_types('balm of restoration', ['Artifact']).
card_subtypes('balm of restoration', []).
card_colors('balm of restoration', []).
card_text('balm of restoration', '{1}, {T}, Sacrifice Balm of Restoration: Choose one —\n• You gain 2 life.\n• Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_mana_cost('balm of restoration', ['2']).
card_cmc('balm of restoration', 2).
card_layout('balm of restoration', 'normal').
card_reserved('balm of restoration').

% Found in: ZEN
card_name('baloth cage trap', 'Baloth Cage Trap').
card_type('baloth cage trap', 'Instant — Trap').
card_types('baloth cage trap', ['Instant']).
card_subtypes('baloth cage trap', ['Trap']).
card_colors('baloth cage trap', ['G']).
card_text('baloth cage trap', 'If an opponent had an artifact enter the battlefield under his or her control this turn, you may pay {1}{G} rather than pay Baloth Cage Trap\'s mana cost.\nPut a 4/4 green Beast creature token onto the battlefield.').
card_mana_cost('baloth cage trap', ['3', 'G', 'G']).
card_cmc('baloth cage trap', 5).
card_layout('baloth cage trap', 'normal').

% Found in: C13, CMD, ZEN
card_name('baloth woodcrasher', 'Baloth Woodcrasher').
card_type('baloth woodcrasher', 'Creature — Beast').
card_types('baloth woodcrasher', ['Creature']).
card_subtypes('baloth woodcrasher', ['Beast']).
card_colors('baloth woodcrasher', ['G']).
card_text('baloth woodcrasher', 'Landfall — Whenever a land enters the battlefield under your control, Baloth Woodcrasher gets +4/+4 and gains trample until end of turn.').
card_mana_cost('baloth woodcrasher', ['4', 'G', 'G']).
card_cmc('baloth woodcrasher', 6).
card_layout('baloth woodcrasher', 'normal').
card_power('baloth woodcrasher', 4).
card_toughness('baloth woodcrasher', 4).

% Found in: ODY
card_name('balshan beguiler', 'Balshan Beguiler').
card_type('balshan beguiler', 'Creature — Human Wizard').
card_types('balshan beguiler', ['Creature']).
card_subtypes('balshan beguiler', ['Human', 'Wizard']).
card_colors('balshan beguiler', ['U']).
card_text('balshan beguiler', 'Whenever Balshan Beguiler deals combat damage to a player, that player reveals the top two cards of his or her library. You choose one of those cards and put it into his or her graveyard.').
card_mana_cost('balshan beguiler', ['2', 'U']).
card_cmc('balshan beguiler', 3).
card_layout('balshan beguiler', 'normal').
card_power('balshan beguiler', 1).
card_toughness('balshan beguiler', 1).

% Found in: TOR
card_name('balshan collaborator', 'Balshan Collaborator').
card_type('balshan collaborator', 'Creature — Bird Soldier').
card_types('balshan collaborator', ['Creature']).
card_subtypes('balshan collaborator', ['Bird', 'Soldier']).
card_colors('balshan collaborator', ['U']).
card_text('balshan collaborator', 'Flying\n{B}: Balshan Collaborator gets +1/+1 until end of turn.').
card_mana_cost('balshan collaborator', ['3', 'U']).
card_cmc('balshan collaborator', 4).
card_layout('balshan collaborator', 'normal').
card_power('balshan collaborator', 2).
card_toughness('balshan collaborator', 2).

% Found in: ODY
card_name('balshan griffin', 'Balshan Griffin').
card_type('balshan griffin', 'Creature — Griffin').
card_types('balshan griffin', ['Creature']).
card_subtypes('balshan griffin', ['Griffin']).
card_colors('balshan griffin', ['U']).
card_text('balshan griffin', 'Flying\n{1}{U}, Discard a card: Return Balshan Griffin to its owner\'s hand.').
card_mana_cost('balshan griffin', ['3', 'U', 'U']).
card_cmc('balshan griffin', 5).
card_layout('balshan griffin', 'normal').
card_power('balshan griffin', 3).
card_toughness('balshan griffin', 2).

% Found in: JUD
card_name('balthor the defiled', 'Balthor the Defiled').
card_type('balthor the defiled', 'Legendary Creature — Zombie Dwarf').
card_types('balthor the defiled', ['Creature']).
card_subtypes('balthor the defiled', ['Zombie', 'Dwarf']).
card_supertypes('balthor the defiled', ['Legendary']).
card_colors('balthor the defiled', ['B']).
card_text('balthor the defiled', 'Minion creatures get +1/+1.\n{B}{B}{B}, Exile Balthor the Defiled: Each player returns all black and all red creature cards from his or her graveyard to the battlefield.').
card_mana_cost('balthor the defiled', ['2', 'B', 'B']).
card_cmc('balthor the defiled', 4).
card_layout('balthor the defiled', 'normal').
card_power('balthor the defiled', 2).
card_toughness('balthor the defiled', 2).

% Found in: TOR
card_name('balthor the stout', 'Balthor the Stout').
card_type('balthor the stout', 'Legendary Creature — Dwarf Barbarian').
card_types('balthor the stout', ['Creature']).
card_subtypes('balthor the stout', ['Dwarf', 'Barbarian']).
card_supertypes('balthor the stout', ['Legendary']).
card_colors('balthor the stout', ['R']).
card_text('balthor the stout', 'Other Barbarian creatures get +1/+1.\n{R}: Another target Barbarian creature gets +1/+0 until end of turn.').
card_mana_cost('balthor the stout', ['1', 'R', 'R']).
card_cmc('balthor the stout', 3).
card_layout('balthor the stout', 'normal').
card_power('balthor the stout', 2).
card_toughness('balthor the stout', 2).

% Found in: GTC
card_name('balustrade spy', 'Balustrade Spy').
card_type('balustrade spy', 'Creature — Vampire Rogue').
card_types('balustrade spy', ['Creature']).
card_subtypes('balustrade spy', ['Vampire', 'Rogue']).
card_colors('balustrade spy', ['B']).
card_text('balustrade spy', 'Flying\nWhen Balustrade Spy enters the battlefield, target player reveals cards from the top of his or her library until he or she reveals a land card, then puts those cards into his or her graveyard.').
card_mana_cost('balustrade spy', ['3', 'B']).
card_cmc('balustrade spy', 4).
card_layout('balustrade spy', 'normal').
card_power('balustrade spy', 2).
card_toughness('balustrade spy', 3).

% Found in: ODY
card_name('bamboozle', 'Bamboozle').
card_type('bamboozle', 'Sorcery').
card_types('bamboozle', ['Sorcery']).
card_subtypes('bamboozle', []).
card_colors('bamboozle', ['U']).
card_text('bamboozle', 'Target player reveals the top four cards of his or her library. You choose two of those cards and put them into his or her graveyard. Put the rest on top of his or her library in any order.').
card_mana_cost('bamboozle', ['2', 'U']).
card_cmc('bamboozle', 3).
card_layout('bamboozle', 'normal').

% Found in: 10E, STH, TPR
card_name('bandage', 'Bandage').
card_type('bandage', 'Instant').
card_types('bandage', ['Instant']).
card_subtypes('bandage', []).
card_colors('bandage', ['W']).
card_text('bandage', 'Prevent the next 1 damage that would be dealt to target creature or player this turn.\nDraw a card.').
card_mana_cost('bandage', ['W']).
card_cmc('bandage', 1).
card_layout('bandage', 'normal').

% Found in: DGM
card_name('bane alley blackguard', 'Bane Alley Blackguard').
card_type('bane alley blackguard', 'Creature — Human Rogue').
card_types('bane alley blackguard', ['Creature']).
card_subtypes('bane alley blackguard', ['Human', 'Rogue']).
card_colors('bane alley blackguard', ['B']).
card_text('bane alley blackguard', '').
card_mana_cost('bane alley blackguard', ['1', 'B']).
card_cmc('bane alley blackguard', 2).
card_layout('bane alley blackguard', 'normal').
card_power('bane alley blackguard', 1).
card_toughness('bane alley blackguard', 3).

% Found in: GTC
card_name('bane alley broker', 'Bane Alley Broker').
card_type('bane alley broker', 'Creature — Human Rogue').
card_types('bane alley broker', ['Creature']).
card_subtypes('bane alley broker', ['Human', 'Rogue']).
card_colors('bane alley broker', ['U', 'B']).
card_text('bane alley broker', '{T}: Draw a card, then exile a card from your hand face down.\nYou may look at cards exiled with Bane Alley Broker.\n{U}{B}, {T}: Return a card exiled with Bane Alley Broker to its owner\'s hand.').
card_mana_cost('bane alley broker', ['1', 'U', 'B']).
card_cmc('bane alley broker', 3).
card_layout('bane alley broker', 'normal').
card_power('bane alley broker', 0).
card_toughness('bane alley broker', 3).

% Found in: BFZ
card_name('bane of bala ged', 'Bane of Bala Ged').
card_type('bane of bala ged', 'Creature — Eldrazi').
card_types('bane of bala ged', ['Creature']).
card_subtypes('bane of bala ged', ['Eldrazi']).
card_colors('bane of bala ged', []).
card_text('bane of bala ged', 'Whenever Bane of Bala Ged attacks, defending player exiles two permanents he or she controls.').
card_mana_cost('bane of bala ged', ['7']).
card_cmc('bane of bala ged', 7).
card_layout('bane of bala ged', 'normal').
card_power('bane of bala ged', 7).
card_toughness('bane of bala ged', 5).

% Found in: ISD
card_name('bane of hanweir', 'Bane of Hanweir').
card_type('bane of hanweir', 'Creature — Werewolf').
card_types('bane of hanweir', ['Creature']).
card_subtypes('bane of hanweir', ['Werewolf']).
card_colors('bane of hanweir', ['R']).
card_text('bane of hanweir', 'Bane of Hanweir attacks each turn if able.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Bane of Hanweir.').
card_layout('bane of hanweir', 'double-faced').
card_power('bane of hanweir', 5).
card_toughness('bane of hanweir', 5).

% Found in: C13
card_name('bane of progress', 'Bane of Progress').
card_type('bane of progress', 'Creature — Elemental').
card_types('bane of progress', ['Creature']).
card_subtypes('bane of progress', ['Elemental']).
card_colors('bane of progress', ['G']).
card_text('bane of progress', 'When Bane of Progress enters the battlefield, destroy all artifacts and enchantments. Put a +1/+1 counter on Bane of Progress for each permanent destroyed this way.').
card_mana_cost('bane of progress', ['4', 'G', 'G']).
card_cmc('bane of progress', 6).
card_layout('bane of progress', 'normal').
card_power('bane of progress', 2).
card_toughness('bane of progress', 2).

% Found in: LGN
card_name('bane of the living', 'Bane of the Living').
card_type('bane of the living', 'Creature — Insect').
card_types('bane of the living', ['Creature']).
card_subtypes('bane of the living', ['Insect']).
card_colors('bane of the living', ['B']).
card_text('bane of the living', 'Morph {X}{B}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Bane of the Living is turned face up, all creatures get -X/-X until end of turn.').
card_mana_cost('bane of the living', ['2', 'B', 'B']).
card_cmc('bane of the living', 4).
card_layout('bane of the living', 'normal').
card_power('bane of the living', 4).
card_toughness('bane of the living', 3).

% Found in: CON, DDN, DPA, MM2
card_name('banefire', 'Banefire').
card_type('banefire', 'Sorcery').
card_types('banefire', ['Sorcery']).
card_subtypes('banefire', []).
card_colors('banefire', ['R']).
card_text('banefire', 'Banefire deals X damage to target creature or player.\nIf X is 5 or more, Banefire can\'t be countered by spells or abilities and the damage can\'t be prevented.').
card_mana_cost('banefire', ['X', 'R']).
card_cmc('banefire', 1).
card_layout('banefire', 'normal').

% Found in: ROE
card_name('baneful omen', 'Baneful Omen').
card_type('baneful omen', 'Enchantment').
card_types('baneful omen', ['Enchantment']).
card_subtypes('baneful omen', []).
card_colors('baneful omen', ['B']).
card_text('baneful omen', 'At the beginning of your end step, you may reveal the top card of your library. If you do, each opponent loses life equal to that card\'s converted mana cost.').
card_mana_cost('baneful omen', ['4', 'B', 'B', 'B']).
card_cmc('baneful omen', 7).
card_layout('baneful omen', 'normal').

% Found in: M10, M11, V15
card_name('baneslayer angel', 'Baneslayer Angel').
card_type('baneslayer angel', 'Creature — Angel').
card_types('baneslayer angel', ['Creature']).
card_subtypes('baneslayer angel', ['Angel']).
card_colors('baneslayer angel', ['W']).
card_text('baneslayer angel', 'Flying, first strike, lifelink, protection from Demons and from Dragons').
card_mana_cost('baneslayer angel', ['3', 'W', 'W']).
card_cmc('baneslayer angel', 5).
card_layout('baneslayer angel', 'normal').
card_power('baneslayer angel', 5).
card_toughness('baneslayer angel', 5).

% Found in: ALA
card_name('banewasp affliction', 'Banewasp Affliction').
card_type('banewasp affliction', 'Enchantment — Aura').
card_types('banewasp affliction', ['Enchantment']).
card_subtypes('banewasp affliction', ['Aura']).
card_colors('banewasp affliction', ['B']).
card_text('banewasp affliction', 'Enchant creature\nWhen enchanted creature dies, that creature\'s controller loses life equal to its toughness.').
card_mana_cost('banewasp affliction', ['1', 'B']).
card_cmc('banewasp affliction', 2).
card_layout('banewasp affliction', 'normal').

% Found in: DDO, M14, pFNM
card_name('banisher priest', 'Banisher Priest').
card_type('banisher priest', 'Creature — Human Cleric').
card_types('banisher priest', ['Creature']).
card_subtypes('banisher priest', ['Human', 'Cleric']).
card_colors('banisher priest', ['W']).
card_text('banisher priest', 'When Banisher Priest enters the battlefield, exile target creature an opponent controls until Banisher Priest leaves the battlefield. (That creature returns under its owner\'s control.)').
card_mana_cost('banisher priest', ['1', 'W', 'W']).
card_cmc('banisher priest', 3).
card_layout('banisher priest', 'normal').
card_power('banisher priest', 2).
card_toughness('banisher priest', 2).

% Found in: EVE
card_name('banishing knack', 'Banishing Knack').
card_type('banishing knack', 'Instant').
card_types('banishing knack', ['Instant']).
card_subtypes('banishing knack', []).
card_colors('banishing knack', ['U']).
card_text('banishing knack', 'Until end of turn, target creature gains \"{T}: Return target nonland permanent to its owner\'s hand.\"').
card_mana_cost('banishing knack', ['U']).
card_cmc('banishing knack', 1).
card_layout('banishing knack', 'normal').

% Found in: JOU, pFNM
card_name('banishing light', 'Banishing Light').
card_type('banishing light', 'Enchantment').
card_types('banishing light', ['Enchantment']).
card_subtypes('banishing light', []).
card_colors('banishing light', ['W']).
card_text('banishing light', 'When Banishing Light enters the battlefield, exile target nonland permanent an opponent controls until Banishing Light leaves the battlefield. (That permanent returns under its owner\'s control.)').
card_mana_cost('banishing light', ['2', 'W']).
card_cmc('banishing light', 3).
card_layout('banishing light', 'normal').

% Found in: AVR
card_name('banishing stroke', 'Banishing Stroke').
card_type('banishing stroke', 'Instant').
card_types('banishing stroke', ['Instant']).
card_subtypes('banishing stroke', []).
card_colors('banishing stroke', ['W']).
card_text('banishing stroke', 'Put target artifact, creature, or enchantment on the bottom of its owner\'s library.\nMiracle {W} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_mana_cost('banishing stroke', ['5', 'W']).
card_cmc('banishing stroke', 6).
card_layout('banishing stroke', 'normal').

% Found in: MBS
card_name('banishment decree', 'Banishment Decree').
card_type('banishment decree', 'Instant').
card_types('banishment decree', ['Instant']).
card_subtypes('banishment decree', []).
card_colors('banishment decree', ['W']).
card_text('banishment decree', 'Put target artifact, creature, or enchantment on top of its owner\'s library.').
card_mana_cost('banishment decree', ['3', 'W', 'W']).
card_cmc('banishment decree', 5).
card_layout('banishment decree', 'normal').

% Found in: AVR
card_name('banners raised', 'Banners Raised').
card_type('banners raised', 'Instant').
card_types('banners raised', ['Instant']).
card_subtypes('banners raised', []).
card_colors('banners raised', ['R']).
card_text('banners raised', 'Creatures you control get +1/+0 until end of turn.').
card_mana_cost('banners raised', ['R']).
card_cmc('banners raised', 1).
card_layout('banners raised', 'normal').

% Found in: CHR, DRK, ME3
card_name('banshee', 'Banshee').
card_type('banshee', 'Creature — Spirit').
card_types('banshee', ['Creature']).
card_subtypes('banshee', ['Spirit']).
card_colors('banshee', ['B']).
card_text('banshee', '{X}, {T}: Banshee deals half X damage, rounded down, to target creature or player, and half X damage, rounded up, to you.').
card_mana_cost('banshee', ['2', 'B', 'B']).
card_cmc('banshee', 4).
card_layout('banshee', 'normal').
card_power('banshee', 0).
card_toughness('banshee', 1).

% Found in: MRD
card_name('banshee\'s blade', 'Banshee\'s Blade').
card_type('banshee\'s blade', 'Artifact — Equipment').
card_types('banshee\'s blade', ['Artifact']).
card_subtypes('banshee\'s blade', ['Equipment']).
card_colors('banshee\'s blade', []).
card_text('banshee\'s blade', 'Equipped creature gets +1/+1 for each charge counter on Banshee\'s Blade.\nWhenever equipped creature deals combat damage, put a charge counter on Banshee\'s Blade.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery. This card enters the battlefield unattached and stays on the battlefield if the creature leaves.)').
card_mana_cost('banshee\'s blade', ['2']).
card_cmc('banshee\'s blade', 2).
card_layout('banshee\'s blade', 'normal').

% Found in: HOP
card_name('bant', 'Bant').
card_type('bant', 'Plane — Alara').
card_types('bant', ['Plane']).
card_subtypes('bant', ['Alara']).
card_colors('bant', []).
card_text('bant', 'All creatures have exalted. (Whenever a creature attacks alone, it gets +1/+1 until end of turn for each instance of exalted among permanents its controller controls.)\nWhenever you roll {C}, put a divinity counter on target green, white, or blue creature. That creature has indestructible for as long as it has a divinity counter on it.').
card_layout('bant', 'plane').

% Found in: ALA
card_name('bant battlemage', 'Bant Battlemage').
card_type('bant battlemage', 'Creature — Human Wizard').
card_types('bant battlemage', ['Creature']).
card_subtypes('bant battlemage', ['Human', 'Wizard']).
card_colors('bant battlemage', ['W']).
card_text('bant battlemage', '{G}, {T}: Target creature gains trample until end of turn.\n{U}, {T}: Target creature gains flying until end of turn.').
card_mana_cost('bant battlemage', ['2', 'W']).
card_cmc('bant battlemage', 3).
card_layout('bant battlemage', 'normal').
card_power('bant battlemage', 2).
card_toughness('bant battlemage', 2).

% Found in: ALA
card_name('bant charm', 'Bant Charm').
card_type('bant charm', 'Instant').
card_types('bant charm', ['Instant']).
card_subtypes('bant charm', []).
card_colors('bant charm', ['W', 'U', 'G']).
card_text('bant charm', 'Choose one —\n• Destroy target artifact.\n• Put target creature on the bottom of its owner\'s library.\n• Counter target instant spell.').
card_mana_cost('bant charm', ['G', 'W', 'U']).
card_cmc('bant charm', 3).
card_layout('bant charm', 'normal').

% Found in: ALA, C13
card_name('bant panorama', 'Bant Panorama').
card_type('bant panorama', 'Land').
card_types('bant panorama', ['Land']).
card_subtypes('bant panorama', []).
card_colors('bant panorama', []).
card_text('bant panorama', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Bant Panorama: Search your library for a basic Forest, Plains, or Island card and put it onto the battlefield tapped. Then shuffle your library.').
card_layout('bant panorama', 'normal').

% Found in: ARB
card_name('bant sojourners', 'Bant Sojourners').
card_type('bant sojourners', 'Creature — Human Soldier').
card_types('bant sojourners', ['Creature']).
card_subtypes('bant sojourners', ['Human', 'Soldier']).
card_colors('bant sojourners', ['W', 'U', 'G']).
card_text('bant sojourners', 'When you cycle Bant Sojourners or it dies, you may put a 1/1 white Soldier creature token onto the battlefield.\nCycling {2}{W} ({2}{W}, Discard this card: Draw a card.)').
card_mana_cost('bant sojourners', ['1', 'G', 'W', 'U']).
card_cmc('bant sojourners', 4).
card_layout('bant sojourners', 'normal').
card_power('bant sojourners', 2).
card_toughness('bant sojourners', 4).

% Found in: ARB
card_name('bant sureblade', 'Bant Sureblade').
card_type('bant sureblade', 'Creature — Human Soldier').
card_types('bant sureblade', ['Creature']).
card_subtypes('bant sureblade', ['Human', 'Soldier']).
card_colors('bant sureblade', ['W', 'U', 'G']).
card_text('bant sureblade', 'As long as you control another multicolored permanent, Bant Sureblade gets +1/+1 and has first strike.').
card_mana_cost('bant sureblade', ['G/U', 'W']).
card_cmc('bant sureblade', 2).
card_layout('bant sureblade', 'normal').
card_power('bant sureblade', 2).
card_toughness('bant sureblade', 1).

% Found in: DKA
card_name('bar the door', 'Bar the Door').
card_type('bar the door', 'Instant').
card_types('bar the door', ['Instant']).
card_subtypes('bar the door', []).
card_colors('bar the door', ['W']).
card_text('bar the door', 'Creatures you control get +0/+4 until end of turn.').
card_mana_cost('bar the door', ['2', 'W']).
card_cmc('bar the door', 3).
card_layout('bar the door', 'normal').

% Found in: JUD
card_name('barbarian bully', 'Barbarian Bully').
card_type('barbarian bully', 'Creature — Human Barbarian').
card_types('barbarian bully', ['Creature']).
card_subtypes('barbarian bully', ['Human', 'Barbarian']).
card_colors('barbarian bully', ['R']).
card_text('barbarian bully', 'Discard a card at random: Barbarian Bully gets +2/+2 until end of turn unless a player has Barbarian Bully deal 4 damage to him or her. Activate this ability only once each turn.').
card_mana_cost('barbarian bully', ['2', 'R']).
card_cmc('barbarian bully', 3).
card_layout('barbarian bully', 'normal').
card_power('barbarian bully', 2).
card_toughness('barbarian bully', 2).

% Found in: PTK
card_name('barbarian general', 'Barbarian General').
card_type('barbarian general', 'Creature — Human Barbarian Soldier').
card_types('barbarian general', ['Creature']).
card_subtypes('barbarian general', ['Human', 'Barbarian', 'Soldier']).
card_colors('barbarian general', ['R']).
card_text('barbarian general', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)').
card_mana_cost('barbarian general', ['4', 'R']).
card_cmc('barbarian general', 5).
card_layout('barbarian general', 'normal').
card_power('barbarian general', 3).
card_toughness('barbarian general', 2).

% Found in: ICE
card_name('barbarian guides', 'Barbarian Guides').
card_type('barbarian guides', 'Creature — Human Barbarian').
card_types('barbarian guides', ['Creature']).
card_subtypes('barbarian guides', ['Human', 'Barbarian']).
card_colors('barbarian guides', ['R']).
card_text('barbarian guides', '{2}{R}, {T}: Choose a land type. Target creature you control gains snow landwalk of the chosen type until end of turn. Return that creature to its owner\'s hand at the beginning of the next end step. (It can\'t be blocked as long as defending player controls a snow land of that type.)').
card_mana_cost('barbarian guides', ['2', 'R']).
card_cmc('barbarian guides', 3).
card_layout('barbarian guides', 'normal').
card_power('barbarian guides', 1).
card_toughness('barbarian guides', 2).

% Found in: PTK
card_name('barbarian horde', 'Barbarian Horde').
card_type('barbarian horde', 'Creature — Human Barbarian Soldier').
card_types('barbarian horde', ['Creature']).
card_subtypes('barbarian horde', ['Human', 'Barbarian', 'Soldier']).
card_colors('barbarian horde', ['R']).
card_text('barbarian horde', '').
card_mana_cost('barbarian horde', ['3', 'R']).
card_cmc('barbarian horde', 4).
card_layout('barbarian horde', 'normal').
card_power('barbarian horde', 3).
card_toughness('barbarian horde', 3).

% Found in: ODY
card_name('barbarian lunatic', 'Barbarian Lunatic').
card_type('barbarian lunatic', 'Creature — Human Barbarian').
card_types('barbarian lunatic', ['Creature']).
card_subtypes('barbarian lunatic', ['Human', 'Barbarian']).
card_colors('barbarian lunatic', ['R']).
card_text('barbarian lunatic', '{2}{R}, Sacrifice Barbarian Lunatic: Barbarian Lunatic deals 2 damage to target creature.').
card_mana_cost('barbarian lunatic', ['2', 'R']).
card_cmc('barbarian lunatic', 3).
card_layout('barbarian lunatic', 'normal').
card_power('barbarian lunatic', 2).
card_toughness('barbarian lunatic', 1).

% Found in: TOR
card_name('barbarian outcast', 'Barbarian Outcast').
card_type('barbarian outcast', 'Creature — Human Barbarian Beast').
card_types('barbarian outcast', ['Creature']).
card_subtypes('barbarian outcast', ['Human', 'Barbarian', 'Beast']).
card_colors('barbarian outcast', ['R']).
card_text('barbarian outcast', 'When you control no Swamps, sacrifice Barbarian Outcast.').
card_mana_cost('barbarian outcast', ['1', 'R']).
card_cmc('barbarian outcast', 2).
card_layout('barbarian outcast', 'normal').
card_power('barbarian outcast', 2).
card_toughness('barbarian outcast', 2).

% Found in: RAV
card_name('barbarian riftcutter', 'Barbarian Riftcutter').
card_type('barbarian riftcutter', 'Creature — Human Barbarian').
card_types('barbarian riftcutter', ['Creature']).
card_subtypes('barbarian riftcutter', ['Human', 'Barbarian']).
card_colors('barbarian riftcutter', ['R']).
card_text('barbarian riftcutter', '{R}, Sacrifice Barbarian Riftcutter: Destroy target land.').
card_mana_cost('barbarian riftcutter', ['4', 'R']).
card_cmc('barbarian riftcutter', 5).
card_layout('barbarian riftcutter', 'normal').
card_power('barbarian riftcutter', 3).
card_toughness('barbarian riftcutter', 3).

% Found in: ODY, PD2
card_name('barbarian ring', 'Barbarian Ring').
card_type('barbarian ring', 'Land').
card_types('barbarian ring', ['Land']).
card_subtypes('barbarian ring', []).
card_colors('barbarian ring', []).
card_text('barbarian ring', '{T}: Add {R} to your mana pool. Barbarian Ring deals 1 damage to you.\nThreshold — {R}, {T}, Sacrifice Barbarian Ring: Barbarian Ring deals 2 damage to target creature or player. Activate this ability only if seven or more cards are in your graveyard.').
card_layout('barbarian ring', 'normal').

% Found in: LEG
card_name('barbary apes', 'Barbary Apes').
card_type('barbary apes', 'Creature — Ape').
card_types('barbary apes', ['Creature']).
card_subtypes('barbary apes', ['Ape']).
card_colors('barbary apes', ['G']).
card_text('barbary apes', '').
card_mana_cost('barbary apes', ['1', 'G']).
card_cmc('barbary apes', 2).
card_layout('barbary apes', 'normal').
card_power('barbary apes', 2).
card_toughness('barbary apes', 2).

% Found in: SOM
card_name('barbed battlegear', 'Barbed Battlegear').
card_type('barbed battlegear', 'Artifact — Equipment').
card_types('barbed battlegear', ['Artifact']).
card_subtypes('barbed battlegear', ['Equipment']).
card_colors('barbed battlegear', []).
card_text('barbed battlegear', 'Equipped creature gets +4/-1.\nEquip {2}').
card_mana_cost('barbed battlegear', ['3']).
card_cmc('barbed battlegear', 3).
card_layout('barbed battlegear', 'normal').

% Found in: PCY
card_name('barbed field', 'Barbed Field').
card_type('barbed field', 'Enchantment — Aura').
card_types('barbed field', ['Enchantment']).
card_subtypes('barbed field', ['Aura']).
card_colors('barbed field', ['R']).
card_text('barbed field', 'Enchant land\nEnchanted land has \"{T}: This land deals 1 damage to target creature or player.\"').
card_mana_cost('barbed field', ['2', 'R', 'R']).
card_cmc('barbed field', 4).
card_layout('barbed field', 'normal').

% Found in: MIR
card_name('barbed foliage', 'Barbed Foliage').
card_type('barbed foliage', 'Enchantment').
card_types('barbed foliage', ['Enchantment']).
card_subtypes('barbed foliage', []).
card_colors('barbed foliage', ['G']).
card_text('barbed foliage', 'Whenever a creature attacks you, it loses flanking until end of turn.\nWhenever a creature without flying attacks you, Barbed Foliage deals 1 damage to it.').
card_mana_cost('barbed foliage', ['2', 'G', 'G']).
card_cmc('barbed foliage', 4).
card_layout('barbed foliage', 'normal').

% Found in: DST
card_name('barbed lightning', 'Barbed Lightning').
card_type('barbed lightning', 'Instant').
card_types('barbed lightning', ['Instant']).
card_subtypes('barbed lightning', []).
card_colors('barbed lightning', ['R']).
card_text('barbed lightning', 'Choose one —\n• Barbed Lightning deals 3 damage to target creature.\n• Barbed Lightning deals 3 damage to target player.\nEntwine {2} (Choose both if you pay the entwine cost.)').
card_mana_cost('barbed lightning', ['2', 'R']).
card_cmc('barbed lightning', 3).
card_layout('barbed lightning', 'normal').

% Found in: 5ED, CST, DKM, ICE, ME2
card_name('barbed sextant', 'Barbed Sextant').
card_type('barbed sextant', 'Artifact').
card_types('barbed sextant', ['Artifact']).
card_subtypes('barbed sextant', []).
card_colors('barbed sextant', []).
card_text('barbed sextant', '{1}, {T}, Sacrifice Barbed Sextant: Add one mana of any color to your mana pool. Draw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('barbed sextant', ['1']).
card_cmc('barbed sextant', 1).
card_layout('barbed sextant', 'normal').

% Found in: CNS, TSP
card_name('barbed shocker', 'Barbed Shocker').
card_type('barbed shocker', 'Creature — Insect').
card_types('barbed shocker', ['Creature']).
card_subtypes('barbed shocker', ['Insect']).
card_colors('barbed shocker', ['R']).
card_text('barbed shocker', 'Trample, haste\nWhenever Barbed Shocker deals damage to a player, that player discards all the cards in his or her hand, then draws that many cards.').
card_mana_cost('barbed shocker', ['3', 'R']).
card_cmc('barbed shocker', 4).
card_layout('barbed shocker', 'normal').
card_power('barbed shocker', 2).
card_toughness('barbed shocker', 2).

% Found in: H09, TMP, TPR
card_name('barbed sliver', 'Barbed Sliver').
card_type('barbed sliver', 'Creature — Sliver').
card_types('barbed sliver', ['Creature']).
card_subtypes('barbed sliver', ['Sliver']).
card_colors('barbed sliver', ['R']).
card_text('barbed sliver', 'All Sliver creatures have \"{2}: This creature gets +1/+0 until end of turn.\"').
card_mana_cost('barbed sliver', ['2', 'R']).
card_cmc('barbed sliver', 3).
card_layout('barbed sliver', 'normal').
card_power('barbed sliver', 2).
card_toughness('barbed sliver', 2).

% Found in: MMQ
card_name('barbed wire', 'Barbed Wire').
card_type('barbed wire', 'Artifact').
card_types('barbed wire', ['Artifact']).
card_subtypes('barbed wire', []).
card_colors('barbed wire', []).
card_text('barbed wire', 'At the beginning of each player\'s upkeep, Barbed Wire deals 1 damage to that player.\n{2}: Prevent the next 1 damage that would be dealt by Barbed Wire this turn.').
card_mana_cost('barbed wire', ['3']).
card_cmc('barbed wire', 3).
card_layout('barbed wire', 'normal').

% Found in: MIR
card_name('barbed-back wurm', 'Barbed-Back Wurm').
card_type('barbed-back wurm', 'Creature — Wurm').
card_types('barbed-back wurm', ['Creature']).
card_subtypes('barbed-back wurm', ['Wurm']).
card_colors('barbed-back wurm', ['B']).
card_text('barbed-back wurm', '{B}: Target green creature blocking Barbed-Back Wurm gets -1/-1 until end of turn.').
card_mana_cost('barbed-back wurm', ['4', 'B']).
card_cmc('barbed-back wurm', 5).
card_layout('barbed-back wurm', 'normal').
card_power('barbed-back wurm', 4).
card_toughness('barbed-back wurm', 3).

% Found in: PO2, S99
card_name('barbtooth wurm', 'Barbtooth Wurm').
card_type('barbtooth wurm', 'Creature — Wurm').
card_types('barbtooth wurm', ['Creature']).
card_subtypes('barbtooth wurm', ['Wurm']).
card_colors('barbtooth wurm', ['G']).
card_text('barbtooth wurm', '').
card_mana_cost('barbtooth wurm', ['5', 'G']).
card_cmc('barbtooth wurm', 6).
card_layout('barbtooth wurm', 'normal').
card_power('barbtooth wurm', 6).
card_toughness('barbtooth wurm', 4).

% Found in: PO2, S99
card_name('bargain', 'Bargain').
card_type('bargain', 'Sorcery').
card_types('bargain', ['Sorcery']).
card_subtypes('bargain', []).
card_colors('bargain', ['W']).
card_text('bargain', 'Target opponent draws a card.\nYou gain 7 life.').
card_mana_cost('bargain', ['2', 'W']).
card_cmc('bargain', 3).
card_layout('bargain', 'normal').

% Found in: MMQ
card_name('bargaining table', 'Bargaining Table').
card_type('bargaining table', 'Artifact').
card_types('bargaining table', ['Artifact']).
card_subtypes('bargaining table', []).
card_colors('bargaining table', []).
card_text('bargaining table', '{X}, {T}: Draw a card. X is the number of cards in an opponent\'s hand.').
card_mana_cost('bargaining table', ['5']).
card_cmc('bargaining table', 5).
card_layout('bargaining table', 'normal').

% Found in: WTH
card_name('barishi', 'Barishi').
card_type('barishi', 'Creature — Elemental').
card_types('barishi', ['Creature']).
card_subtypes('barishi', ['Elemental']).
card_colors('barishi', ['G']).
card_text('barishi', 'When Barishi dies, exile Barishi, then shuffle all creature cards from your graveyard into your library.').
card_mana_cost('barishi', ['2', 'G', 'G']).
card_cmc('barishi', 4).
card_layout('barishi', 'normal').
card_power('barishi', 4).
card_toughness('barishi', 3).

% Found in: ONS
card_name('barkhide mauler', 'Barkhide Mauler').
card_type('barkhide mauler', 'Creature — Beast').
card_types('barkhide mauler', ['Creature']).
card_subtypes('barkhide mauler', ['Beast']).
card_colors('barkhide mauler', ['G']).
card_text('barkhide mauler', 'Cycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('barkhide mauler', ['4', 'G']).
card_cmc('barkhide mauler', 5).
card_layout('barkhide mauler', 'normal').
card_power('barkhide mauler', 4).
card_toughness('barkhide mauler', 4).

% Found in: SHM
card_name('barkshell blessing', 'Barkshell Blessing').
card_type('barkshell blessing', 'Instant').
card_types('barkshell blessing', ['Instant']).
card_subtypes('barkshell blessing', []).
card_colors('barkshell blessing', ['W', 'G']).
card_text('barkshell blessing', 'Target creature gets +2/+2 until end of turn.\nConspire (As you cast this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_mana_cost('barkshell blessing', ['G/W']).
card_cmc('barkshell blessing', 1).
card_layout('barkshell blessing', 'normal').

% Found in: LEG, ME3
card_name('barktooth warbeard', 'Barktooth Warbeard').
card_type('barktooth warbeard', 'Legendary Creature — Human Warrior').
card_types('barktooth warbeard', ['Creature']).
card_subtypes('barktooth warbeard', ['Human', 'Warrior']).
card_supertypes('barktooth warbeard', ['Legendary']).
card_colors('barktooth warbeard', ['B', 'R']).
card_text('barktooth warbeard', '').
card_mana_cost('barktooth warbeard', ['4', 'B', 'R', 'R']).
card_cmc('barktooth warbeard', 7).
card_layout('barktooth warbeard', 'normal').
card_power('barktooth warbeard', 6).
card_toughness('barktooth warbeard', 5).

% Found in: 5ED, CHR, DRK, ME3
card_name('barl\'s cage', 'Barl\'s Cage').
card_type('barl\'s cage', 'Artifact').
card_types('barl\'s cage', ['Artifact']).
card_subtypes('barl\'s cage', []).
card_colors('barl\'s cage', []).
card_text('barl\'s cage', '{3}: Target creature doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('barl\'s cage', ['4']).
card_cmc('barl\'s cage', 4).
card_layout('barl\'s cage', 'normal').

% Found in: HML, MED
card_name('baron sengir', 'Baron Sengir').
card_type('baron sengir', 'Legendary Creature — Vampire').
card_types('baron sengir', ['Creature']).
card_subtypes('baron sengir', ['Vampire']).
card_supertypes('baron sengir', ['Legendary']).
card_colors('baron sengir', ['B']).
card_text('baron sengir', 'Flying\nWhenever a creature dealt damage by Baron Sengir this turn dies, put a +2/+2 counter on Baron Sengir.\n{T}: Regenerate another target Vampire.').
card_mana_cost('baron sengir', ['5', 'B', 'B', 'B']).
card_cmc('baron sengir', 8).
card_layout('baron sengir', 'normal').
card_power('baron sengir', 5).
card_toughness('baron sengir', 5).
card_reserved('baron sengir').

% Found in: M11
card_name('barony vampire', 'Barony Vampire').
card_type('barony vampire', 'Creature — Vampire').
card_types('barony vampire', ['Creature']).
card_subtypes('barony vampire', ['Vampire']).
card_colors('barony vampire', ['B']).
card_text('barony vampire', '').
card_mana_cost('barony vampire', ['2', 'B']).
card_cmc('barony vampire', 3).
card_layout('barony vampire', 'normal').
card_power('barony vampire', 3).
card_toughness('barony vampire', 2).

% Found in: KTK
card_name('barrage of boulders', 'Barrage of Boulders').
card_type('barrage of boulders', 'Sorcery').
card_types('barrage of boulders', ['Sorcery']).
card_subtypes('barrage of boulders', []).
card_colors('barrage of boulders', ['R']).
card_text('barrage of boulders', 'Barrage of Boulders deals 1 damage to each creature you don\'t control.\nFerocious — If you control a creature with power 4 or greater, creatures can\'t block this turn.').
card_mana_cost('barrage of boulders', ['2', 'R']).
card_cmc('barrage of boulders', 3).
card_layout('barrage of boulders', 'normal').

% Found in: M14
card_name('barrage of expendables', 'Barrage of Expendables').
card_type('barrage of expendables', 'Enchantment').
card_types('barrage of expendables', ['Enchantment']).
card_subtypes('barrage of expendables', []).
card_colors('barrage of expendables', ['R']).
card_text('barrage of expendables', '{R}, Sacrifice a creature: Barrage of Expendables deals 1 damage to target creature or player.').
card_mana_cost('barrage of expendables', ['R']).
card_cmc('barrage of expendables', 1).
card_layout('barrage of expendables', 'normal').

% Found in: SOM
card_name('barrage ogre', 'Barrage Ogre').
card_type('barrage ogre', 'Creature — Ogre Warrior').
card_types('barrage ogre', ['Creature']).
card_subtypes('barrage ogre', ['Ogre', 'Warrior']).
card_colors('barrage ogre', ['R']).
card_text('barrage ogre', '{T}, Sacrifice an artifact: Barrage Ogre deals 2 damage to target creature or player.').
card_mana_cost('barrage ogre', ['3', 'R', 'R']).
card_cmc('barrage ogre', 5).
card_layout('barrage ogre', 'normal').
card_power('barrage ogre', 3).
card_toughness('barrage ogre', 3).

% Found in: BFZ
card_name('barrage tyrant', 'Barrage Tyrant').
card_type('barrage tyrant', 'Creature — Eldrazi').
card_types('barrage tyrant', ['Creature']).
card_subtypes('barrage tyrant', ['Eldrazi']).
card_colors('barrage tyrant', []).
card_text('barrage tyrant', 'Devoid (This card has no color.)\n{2}{R}, Sacrifice another colorless creature: Barrage Tyrant deals damage equal to the sacrificed creature\'s power to target creature or player.').
card_mana_cost('barrage tyrant', ['4', 'R']).
card_cmc('barrage tyrant', 5).
card_layout('barrage tyrant', 'normal').
card_power('barrage tyrant', 5).
card_toughness('barrage tyrant', 3).

% Found in: SOK
card_name('barrel down sokenzan', 'Barrel Down Sokenzan').
card_type('barrel down sokenzan', 'Instant — Arcane').
card_types('barrel down sokenzan', ['Instant']).
card_subtypes('barrel down sokenzan', ['Arcane']).
card_colors('barrel down sokenzan', ['R']).
card_text('barrel down sokenzan', 'Sweep — Return any number of Mountains you control to their owner\'s hand. Barrel Down Sokenzan deals damage to target creature equal to twice the number of Mountains returned this way.').
card_mana_cost('barrel down sokenzan', ['2', 'R']).
card_cmc('barrel down sokenzan', 3).
card_layout('barrel down sokenzan', 'normal').

% Found in: MIR
card_name('barreling attack', 'Barreling Attack').
card_type('barreling attack', 'Instant').
card_types('barreling attack', ['Instant']).
card_subtypes('barreling attack', []).
card_colors('barreling attack', ['R']).
card_text('barreling attack', 'Target creature gains trample until end of turn. When that creature becomes blocked this turn, it gets +1/+1 until end of turn for each creature blocking it.').
card_mana_cost('barreling attack', ['2', 'R', 'R']).
card_cmc('barreling attack', 4).
card_layout('barreling attack', 'normal').
card_reserved('barreling attack').

% Found in: FUT
card_name('barren glory', 'Barren Glory').
card_type('barren glory', 'Enchantment').
card_types('barren glory', ['Enchantment']).
card_subtypes('barren glory', []).
card_colors('barren glory', ['W']).
card_text('barren glory', 'At the beginning of your upkeep, if you control no permanents other than Barren Glory and have no cards in hand, you win the game.').
card_mana_cost('barren glory', ['4', 'W', 'W']).
card_cmc('barren glory', 6).
card_layout('barren glory', 'normal').

% Found in: ARC, C13, C14, CMD, DD3_DVD, DDC, DDJ, ONS, VMA
card_name('barren moor', 'Barren Moor').
card_type('barren moor', 'Land').
card_types('barren moor', ['Land']).
card_subtypes('barren moor', []).
card_colors('barren moor', []).
card_text('barren moor', 'Barren Moor enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\nCycling {B} ({B}, Discard this card: Draw a card.)').
card_layout('barren moor', 'normal').

% Found in: SHM
card_name('barrenton cragtreads', 'Barrenton Cragtreads').
card_type('barrenton cragtreads', 'Creature — Kithkin Scout').
card_types('barrenton cragtreads', ['Creature']).
card_subtypes('barrenton cragtreads', ['Kithkin', 'Scout']).
card_colors('barrenton cragtreads', ['W', 'U']).
card_text('barrenton cragtreads', 'Barrenton Cragtreads can\'t be blocked by red creatures.').
card_mana_cost('barrenton cragtreads', ['2', 'W/U', 'W/U']).
card_cmc('barrenton cragtreads', 4).
card_layout('barrenton cragtreads', 'normal').
card_power('barrenton cragtreads', 3).
card_toughness('barrenton cragtreads', 3).

% Found in: SHM
card_name('barrenton medic', 'Barrenton Medic').
card_type('barrenton medic', 'Creature — Kithkin Cleric').
card_types('barrenton medic', ['Creature']).
card_subtypes('barrenton medic', ['Kithkin', 'Cleric']).
card_colors('barrenton medic', ['W']).
card_text('barrenton medic', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\nPut a -1/-1 counter on Barrenton Medic: Untap Barrenton Medic.').
card_mana_cost('barrenton medic', ['4', 'W']).
card_cmc('barrenton medic', 5).
card_layout('barrenton medic', 'normal').
card_power('barrenton medic', 0).
card_toughness('barrenton medic', 4).

% Found in: VAN
card_name('barrin', 'Barrin').
card_type('barrin', 'Vanguard').
card_types('barrin', ['Vanguard']).
card_subtypes('barrin', []).
card_colors('barrin', []).
card_text('barrin', 'Sacrifice a permanent: Return target creature to its owner\'s hand.').
card_layout('barrin', 'vanguard').

% Found in: USG
card_name('barrin\'s codex', 'Barrin\'s Codex').
card_type('barrin\'s codex', 'Artifact').
card_types('barrin\'s codex', ['Artifact']).
card_subtypes('barrin\'s codex', []).
card_colors('barrin\'s codex', []).
card_text('barrin\'s codex', 'At the beginning of your upkeep, you may put a page counter on Barrin\'s Codex.\n{4}, {T}, Sacrifice Barrin\'s Codex: Draw X cards, where X is the number of page counters on Barrin\'s Codex.').
card_mana_cost('barrin\'s codex', ['4']).
card_cmc('barrin\'s codex', 4).
card_layout('barrin\'s codex', 'normal').

% Found in: INV
card_name('barrin\'s spite', 'Barrin\'s Spite').
card_type('barrin\'s spite', 'Sorcery').
card_types('barrin\'s spite', ['Sorcery']).
card_subtypes('barrin\'s spite', []).
card_colors('barrin\'s spite', ['U', 'B']).
card_text('barrin\'s spite', 'Choose two target creatures controlled by the same player. Their controller chooses and sacrifices one of them. Return the other to its owner\'s hand.').
card_mana_cost('barrin\'s spite', ['2', 'U', 'B']).
card_cmc('barrin\'s spite', 4).
card_layout('barrin\'s spite', 'normal').

% Found in: INV
card_name('barrin\'s unmaking', 'Barrin\'s Unmaking').
card_type('barrin\'s unmaking', 'Instant').
card_types('barrin\'s unmaking', ['Instant']).
card_subtypes('barrin\'s unmaking', []).
card_colors('barrin\'s unmaking', ['U']).
card_text('barrin\'s unmaking', 'Return target permanent to its owner\'s hand if that permanent shares a color with the most common color among all permanents or a color tied for most common.').
card_mana_cost('barrin\'s unmaking', ['1', 'U']).
card_cmc('barrin\'s unmaking', 2).
card_layout('barrin\'s unmaking', 'normal').

% Found in: USG
card_name('barrin, master wizard', 'Barrin, Master Wizard').
card_type('barrin, master wizard', 'Legendary Creature — Human Wizard').
card_types('barrin, master wizard', ['Creature']).
card_subtypes('barrin, master wizard', ['Human', 'Wizard']).
card_supertypes('barrin, master wizard', ['Legendary']).
card_colors('barrin, master wizard', ['U']).
card_text('barrin, master wizard', '{2}, Sacrifice a permanent: Return target creature to its owner\'s hand.').
card_mana_cost('barrin, master wizard', ['1', 'U', 'U']).
card_cmc('barrin, master wizard', 3).
card_layout('barrin, master wizard', 'normal').
card_power('barrin, master wizard', 1).
card_toughness('barrin, master wizard', 1).
card_reserved('barrin, master wizard').

% Found in: WTH
card_name('barrow ghoul', 'Barrow Ghoul').
card_type('barrow ghoul', 'Creature — Zombie').
card_types('barrow ghoul', ['Creature']).
card_subtypes('barrow ghoul', ['Zombie']).
card_colors('barrow ghoul', ['B']).
card_text('barrow ghoul', 'At the beginning of your upkeep, sacrifice Barrow Ghoul unless you exile the top creature card of your graveyard.').
card_mana_cost('barrow ghoul', ['1', 'B']).
card_cmc('barrow ghoul', 2).
card_layout('barrow ghoul', 'normal').
card_power('barrow ghoul', 4).
card_toughness('barrow ghoul', 4).

% Found in: LEG, ME3
card_name('bartel runeaxe', 'Bartel Runeaxe').
card_type('bartel runeaxe', 'Legendary Creature — Giant Warrior').
card_types('bartel runeaxe', ['Creature']).
card_subtypes('bartel runeaxe', ['Giant', 'Warrior']).
card_supertypes('bartel runeaxe', ['Legendary']).
card_colors('bartel runeaxe', ['B', 'R', 'G']).
card_text('bartel runeaxe', 'Vigilance\nBartel Runeaxe can\'t be the target of Aura spells.').
card_mana_cost('bartel runeaxe', ['3', 'B', 'R', 'G']).
card_cmc('bartel runeaxe', 6).
card_layout('bartel runeaxe', 'normal').
card_power('bartel runeaxe', 6).
card_toughness('bartel runeaxe', 5).
card_reserved('bartel runeaxe').

% Found in: AVR, DD3_DVD, DDC, MRD
card_name('barter in blood', 'Barter in Blood').
card_type('barter in blood', 'Sorcery').
card_types('barter in blood', ['Sorcery']).
card_subtypes('barter in blood', []).
card_colors('barter in blood', ['B']).
card_text('barter in blood', 'Each player sacrifices two creatures.').
card_mana_cost('barter in blood', ['2', 'B', 'B']).
card_cmc('barter in blood', 4).
card_layout('barter in blood', 'normal').

% Found in: FUT
card_name('baru, fist of krosa', 'Baru, Fist of Krosa').
card_type('baru, fist of krosa', 'Legendary Creature — Human Druid').
card_types('baru, fist of krosa', ['Creature']).
card_subtypes('baru, fist of krosa', ['Human', 'Druid']).
card_supertypes('baru, fist of krosa', ['Legendary']).
card_colors('baru, fist of krosa', ['G']).
card_text('baru, fist of krosa', 'Whenever a Forest enters the battlefield, green creatures you control get +1/+1 and gain trample until end of turn.\nGrandeur — Discard another card named Baru, Fist of Krosa: Put an X/X green Wurm creature token onto the battlefield, where X is the number of lands you control.').
card_mana_cost('baru, fist of krosa', ['3', 'G', 'G']).
card_cmc('baru, fist of krosa', 5).
card_layout('baru, fist of krosa', 'normal').
card_power('baru, fist of krosa', 4).
card_toughness('baru, fist of krosa', 4).

% Found in: TSP
card_name('basal sliver', 'Basal Sliver').
card_type('basal sliver', 'Creature — Sliver').
card_types('basal sliver', ['Creature']).
card_subtypes('basal sliver', ['Sliver']).
card_colors('basal sliver', ['B']).
card_text('basal sliver', 'All Slivers have \"Sacrifice this permanent: Add {B}{B} to your mana pool.\"').
card_mana_cost('basal sliver', ['2', 'B']).
card_cmc('basal sliver', 3).
card_layout('basal sliver', 'normal').
card_power('basal sliver', 2).
card_toughness('basal sliver', 2).

% Found in: FEM, MED
card_name('basal thrull', 'Basal Thrull').
card_type('basal thrull', 'Creature — Thrull').
card_types('basal thrull', ['Creature']).
card_subtypes('basal thrull', ['Thrull']).
card_colors('basal thrull', ['B']).
card_text('basal thrull', '{T}, Sacrifice Basal Thrull: Add {B}{B} to your mana pool.').
card_mana_cost('basal thrull', ['B', 'B']).
card_cmc('basal thrull', 2).
card_layout('basal thrull', 'normal').
card_power('basal thrull', 1).
card_toughness('basal thrull', 2).

% Found in: TSP
card_name('basalt gargoyle', 'Basalt Gargoyle').
card_type('basalt gargoyle', 'Creature — Gargoyle').
card_types('basalt gargoyle', ['Creature']).
card_subtypes('basalt gargoyle', ['Gargoyle']).
card_colors('basalt gargoyle', ['R']).
card_text('basalt gargoyle', 'Flying\nEcho {2}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\n{R}: Basalt Gargoyle gets +0/+1 until end of turn.').
card_mana_cost('basalt gargoyle', ['2', 'R']).
card_cmc('basalt gargoyle', 3).
card_layout('basalt gargoyle', 'normal').
card_power('basalt gargoyle', 3).
card_toughness('basalt gargoyle', 2).

% Found in: MIR
card_name('basalt golem', 'Basalt Golem').
card_type('basalt golem', 'Artifact Creature — Golem').
card_types('basalt golem', ['Artifact', 'Creature']).
card_subtypes('basalt golem', ['Golem']).
card_colors('basalt golem', []).
card_text('basalt golem', 'Basalt Golem can\'t be blocked by artifact creatures.\nWhenever Basalt Golem becomes blocked by a creature, that creature\'s controller sacrifices it at end of combat. If the player does, he or she puts a 0/2 colorless Wall artifact creature token with defender onto the battlefield.').
card_mana_cost('basalt golem', ['5']).
card_cmc('basalt golem', 5).
card_layout('basalt golem', 'normal').
card_power('basalt golem', 2).
card_toughness('basalt golem', 4).

% Found in: 2ED, 3ED, C13, CED, CEI, LEA, LEB, ME4
card_name('basalt monolith', 'Basalt Monolith').
card_type('basalt monolith', 'Artifact').
card_types('basalt monolith', ['Artifact']).
card_subtypes('basalt monolith', []).
card_colors('basalt monolith', []).
card_text('basalt monolith', 'Basalt Monolith doesn\'t untap during your untap step.\n{T}: Add {3} to your mana pool.\n{3}: Untap Basalt Monolith.').
card_mana_cost('basalt monolith', ['3']).
card_cmc('basalt monolith', 3).
card_layout('basalt monolith', 'normal').

% Found in: CMD, CNS, VMA
card_name('basandra, battle seraph', 'Basandra, Battle Seraph').
card_type('basandra, battle seraph', 'Legendary Creature — Angel').
card_types('basandra, battle seraph', ['Creature']).
card_subtypes('basandra, battle seraph', ['Angel']).
card_supertypes('basandra, battle seraph', ['Legendary']).
card_colors('basandra, battle seraph', ['W', 'R']).
card_text('basandra, battle seraph', 'Flying\nPlayers can\'t cast spells during combat.\n{R}: Target creature attacks this turn if able.').
card_mana_cost('basandra, battle seraph', ['3', 'R', 'W']).
card_cmc('basandra, battle seraph', 5).
card_layout('basandra, battle seraph', 'normal').
card_power('basandra, battle seraph', 4).
card_toughness('basandra, battle seraph', 4).

% Found in: ODY
card_name('bash to bits', 'Bash to Bits').
card_type('bash to bits', 'Instant').
card_types('bash to bits', ['Instant']).
card_subtypes('bash to bits', []).
card_colors('bash to bits', ['R']).
card_text('bash to bits', 'Destroy target artifact.\nFlashback {4}{R}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('bash to bits', ['3', 'R']).
card_cmc('bash to bits', 4).
card_layout('bash to bits', 'normal').

% Found in: GTC
card_name('basilica guards', 'Basilica Guards').
card_type('basilica guards', 'Creature — Human Soldier').
card_types('basilica guards', ['Creature']).
card_subtypes('basilica guards', ['Human', 'Soldier']).
card_colors('basilica guards', ['W']).
card_text('basilica guards', 'Defender\nExtort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)').
card_mana_cost('basilica guards', ['2', 'W']).
card_cmc('basilica guards', 3).
card_layout('basilica guards', 'normal').
card_power('basilica guards', 1).
card_toughness('basilica guards', 4).

% Found in: GTC
card_name('basilica screecher', 'Basilica Screecher').
card_type('basilica screecher', 'Creature — Bat').
card_types('basilica screecher', ['Creature']).
card_subtypes('basilica screecher', ['Bat']).
card_colors('basilica screecher', ['B']).
card_text('basilica screecher', 'Flying\nExtort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)').
card_mana_cost('basilica screecher', ['1', 'B']).
card_cmc('basilica screecher', 2).
card_layout('basilica screecher', 'normal').
card_power('basilica screecher', 1).
card_toughness('basilica screecher', 2).

% Found in: WWK
card_name('basilisk collar', 'Basilisk Collar').
card_type('basilisk collar', 'Artifact — Equipment').
card_types('basilisk collar', ['Artifact']).
card_subtypes('basilisk collar', ['Equipment']).
card_colors('basilisk collar', []).
card_text('basilisk collar', 'Equipped creature has deathtouch and lifelink.\nEquip {2}').
card_mana_cost('basilisk collar', ['1']).
card_cmc('basilisk collar', 1).
card_layout('basilisk collar', 'normal').

% Found in: DD3_GVL, DDD, TOR, VMA, pFNM
card_name('basking rootwalla', 'Basking Rootwalla').
card_type('basking rootwalla', 'Creature — Lizard').
card_types('basking rootwalla', ['Creature']).
card_subtypes('basking rootwalla', ['Lizard']).
card_colors('basking rootwalla', ['G']).
card_text('basking rootwalla', '{1}{G}: Basking Rootwalla gets +2/+2 until end of turn. Activate this ability only once each turn.\nMadness {0} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('basking rootwalla', ['G']).
card_cmc('basking rootwalla', 1).
card_layout('basking rootwalla', 'normal').
card_power('basking rootwalla', 1).
card_toughness('basking rootwalla', 1).

% Found in: JOU
card_name('bassara tower archer', 'Bassara Tower Archer').
card_type('bassara tower archer', 'Creature — Human Archer').
card_types('bassara tower archer', ['Creature']).
card_subtypes('bassara tower archer', ['Human', 'Archer']).
card_colors('bassara tower archer', ['G']).
card_text('bassara tower archer', 'Hexproof, reach').
card_mana_cost('bassara tower archer', ['G', 'G']).
card_cmc('bassara tower archer', 2).
card_layout('bassara tower archer', 'normal').
card_power('bassara tower archer', 2).
card_toughness('bassara tower archer', 1).

% Found in: FRF
card_name('bathe in dragonfire', 'Bathe in Dragonfire').
card_type('bathe in dragonfire', 'Sorcery').
card_types('bathe in dragonfire', ['Sorcery']).
card_subtypes('bathe in dragonfire', []).
card_colors('bathe in dragonfire', ['R']).
card_text('bathe in dragonfire', 'Bathe in Dragonfire deals 4 damage to target creature.').
card_mana_cost('bathe in dragonfire', ['2', 'R']).
card_cmc('bathe in dragonfire', 3).
card_layout('bathe in dragonfire', 'normal').

% Found in: CMD, RAV
card_name('bathe in light', 'Bathe in Light').
card_type('bathe in light', 'Instant').
card_types('bathe in light', ['Instant']).
card_subtypes('bathe in light', []).
card_colors('bathe in light', ['W']).
card_text('bathe in light', 'Radiance — Choose a color. Target creature and each other creature that shares a color with it gain protection from the chosen color until end of turn.').
card_mana_cost('bathe in light', ['1', 'W']).
card_cmc('bathe in light', 2).
card_layout('bathe in light', 'normal').

% Found in: 5DN
card_name('baton of courage', 'Baton of Courage').
card_type('baton of courage', 'Artifact').
card_types('baton of courage', ['Artifact']).
card_subtypes('baton of courage', []).
card_colors('baton of courage', []).
card_text('baton of courage', 'Flash\nSunburst (This enters the battlefield with a charge counter on it for each color of mana spent to cast it.)\nRemove a charge counter from Baton of Courage: Target creature gets +1/+1 until end of turn.').
card_mana_cost('baton of courage', ['3']).
card_cmc('baton of courage', 3).
card_layout('baton of courage', 'normal').

% Found in: ICE
card_name('baton of morale', 'Baton of Morale').
card_type('baton of morale', 'Artifact').
card_types('baton of morale', ['Artifact']).
card_subtypes('baton of morale', []).
card_colors('baton of morale', []).
card_text('baton of morale', '{2}: Target creature gains banding until end of turn. (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding a player controls are blocking or being blocked by a creature, that player divides that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('baton of morale', ['2']).
card_cmc('baton of morale', 2).
card_layout('baton of morale', 'normal').

% Found in: 5DN
card_name('battered golem', 'Battered Golem').
card_type('battered golem', 'Artifact Creature — Golem').
card_types('battered golem', ['Artifact', 'Creature']).
card_subtypes('battered golem', ['Golem']).
card_colors('battered golem', []).
card_text('battered golem', 'Battered Golem doesn\'t untap during your untap step.\nWhenever an artifact enters the battlefield, you may untap Battered Golem.').
card_mana_cost('battered golem', ['3']).
card_cmc('battered golem', 3).
card_layout('battered golem', 'normal').
card_power('battered golem', 3).
card_toughness('battered golem', 2).

% Found in: RTR
card_name('batterhorn', 'Batterhorn').
card_type('batterhorn', 'Creature — Beast').
card_types('batterhorn', ['Creature']).
card_subtypes('batterhorn', ['Beast']).
card_colors('batterhorn', ['R']).
card_text('batterhorn', 'When Batterhorn enters the battlefield, you may destroy target artifact.').
card_mana_cost('batterhorn', ['4', 'R']).
card_cmc('batterhorn', 5).
card_layout('batterhorn', 'normal').
card_power('batterhorn', 4).
card_toughness('batterhorn', 3).

% Found in: ARC, ONS
card_name('battering craghorn', 'Battering Craghorn').
card_type('battering craghorn', 'Creature — Goat Beast').
card_types('battering craghorn', ['Creature']).
card_subtypes('battering craghorn', ['Goat', 'Beast']).
card_colors('battering craghorn', ['R']).
card_text('battering craghorn', 'First strike\nMorph {1}{R}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('battering craghorn', ['2', 'R', 'R']).
card_cmc('battering craghorn', 4).
card_layout('battering craghorn', 'normal').
card_power('battering craghorn', 3).
card_toughness('battering craghorn', 1).

% Found in: DGM
card_name('battering krasis', 'Battering Krasis').
card_type('battering krasis', 'Creature — Fish Beast').
card_types('battering krasis', ['Creature']).
card_subtypes('battering krasis', ['Fish', 'Beast']).
card_colors('battering krasis', ['G']).
card_text('battering krasis', 'Trample\nEvolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)').
card_mana_cost('battering krasis', ['2', 'G']).
card_cmc('battering krasis', 3).
card_layout('battering krasis', 'normal').
card_power('battering krasis', 2).
card_toughness('battering krasis', 1).

% Found in: 4ED, 5ED, ATQ, ITP, RQS
card_name('battering ram', 'Battering Ram').
card_type('battering ram', 'Artifact Creature — Construct').
card_types('battering ram', ['Artifact', 'Creature']).
card_subtypes('battering ram', ['Construct']).
card_colors('battering ram', []).
card_text('battering ram', 'At the beginning of combat on your turn, Battering Ram gains banding until end of combat. (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s blocking.)\nWhenever Battering Ram becomes blocked by a Wall, destroy that Wall at end of combat.').
card_mana_cost('battering ram', ['2']).
card_cmc('battering ram', 2).
card_layout('battering ram', 'normal').
card_power('battering ram', 1).
card_toughness('battering ram', 1).

% Found in: PLC
card_name('battering sliver', 'Battering Sliver').
card_type('battering sliver', 'Creature — Sliver').
card_types('battering sliver', ['Creature']).
card_subtypes('battering sliver', ['Sliver']).
card_colors('battering sliver', ['R']).
card_text('battering sliver', 'All Sliver creatures have trample.').
card_mana_cost('battering sliver', ['5', 'R']).
card_cmc('battering sliver', 6).
card_layout('battering sliver', 'normal').
card_power('battering sliver', 4).
card_toughness('battering sliver', 4).

% Found in: GPT
card_name('battering wurm', 'Battering Wurm').
card_type('battering wurm', 'Creature — Wurm').
card_types('battering wurm', ['Creature']).
card_subtypes('battering wurm', ['Wurm']).
card_colors('battering wurm', ['G']).
card_text('battering wurm', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)\nCreatures with power less than Battering Wurm\'s power can\'t block it.').
card_mana_cost('battering wurm', ['6', 'G']).
card_cmc('battering wurm', 7).
card_layout('battering wurm', 'normal').
card_power('battering wurm', 4).
card_toughness('battering wurm', 3).

% Found in: NPH, pGPX
card_name('batterskull', 'Batterskull').
card_type('batterskull', 'Artifact — Equipment').
card_types('batterskull', ['Artifact']).
card_subtypes('batterskull', ['Equipment']).
card_colors('batterskull', []).
card_text('batterskull', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +4/+4 and has vigilance and lifelink.\n{3}: Return Batterskull to its owner\'s hand.\nEquip {5}').
card_mana_cost('batterskull', ['5']).
card_cmc('batterskull', 5).
card_layout('batterskull', 'normal').

% Found in: HOP, INV, TSB
card_name('battery', 'Battery').
card_type('battery', 'Sorcery').
card_types('battery', ['Sorcery']).
card_subtypes('battery', []).
card_colors('battery', ['G']).
card_text('battery', 'Put a 3/3 green Elephant creature token onto the battlefield.').
card_mana_cost('battery', ['3', 'G']).
card_cmc('battery', 4).
card_layout('battery', 'split').

% Found in: FRF
card_name('battle brawler', 'Battle Brawler').
card_type('battle brawler', 'Creature — Orc Warrior').
card_types('battle brawler', ['Creature']).
card_subtypes('battle brawler', ['Orc', 'Warrior']).
card_colors('battle brawler', ['B']).
card_text('battle brawler', 'As long as you control a red or white permanent, Battle Brawler gets +1/+0 and has first strike.').
card_mana_cost('battle brawler', ['1', 'B']).
card_cmc('battle brawler', 2).
card_layout('battle brawler', 'normal').
card_power('battle brawler', 2).
card_toughness('battle brawler', 2).

% Found in: ICE
card_name('battle cry', 'Battle Cry').
card_type('battle cry', 'Instant').
card_types('battle cry', ['Instant']).
card_subtypes('battle cry', []).
card_colors('battle cry', ['W']).
card_text('battle cry', 'Untap all white creatures you control.\nWhenever a creature blocks this turn, it gets +0/+1 until end of turn.').
card_mana_cost('battle cry', ['2', 'W']).
card_cmc('battle cry', 3).
card_layout('battle cry', 'normal').

% Found in: ICE
card_name('battle frenzy', 'Battle Frenzy').
card_type('battle frenzy', 'Instant').
card_types('battle frenzy', ['Instant']).
card_subtypes('battle frenzy', []).
card_colors('battle frenzy', ['R']).
card_text('battle frenzy', 'Green creatures you control get +1/+1 until end of turn.\nNongreen creatures you control get +1/+0 until end of turn.').
card_mana_cost('battle frenzy', ['2', 'R']).
card_cmc('battle frenzy', 3).
card_layout('battle frenzy', 'normal').

% Found in: WWK
card_name('battle hurda', 'Battle Hurda').
card_type('battle hurda', 'Creature — Giant').
card_types('battle hurda', ['Creature']).
card_subtypes('battle hurda', ['Giant']).
card_colors('battle hurda', ['W']).
card_text('battle hurda', 'First strike').
card_mana_cost('battle hurda', ['4', 'W']).
card_cmc('battle hurda', 5).
card_layout('battle hurda', 'normal').
card_power('battle hurda', 3).
card_toughness('battle hurda', 3).

% Found in: AVR
card_name('battle hymn', 'Battle Hymn').
card_type('battle hymn', 'Instant').
card_types('battle hymn', ['Instant']).
card_subtypes('battle hymn', []).
card_colors('battle hymn', ['R']).
card_text('battle hymn', 'Add {R} to your mana pool for each creature you control.').
card_mana_cost('battle hymn', ['1', 'R']).
card_cmc('battle hymn', 2).
card_layout('battle hymn', 'normal').

% Found in: DDL, DTK, LRW, M15
card_name('battle mastery', 'Battle Mastery').
card_type('battle mastery', 'Enchantment — Aura').
card_types('battle mastery', ['Enchantment']).
card_subtypes('battle mastery', ['Aura']).
card_colors('battle mastery', ['W']).
card_text('battle mastery', 'Enchant creature\nEnchanted creature has double strike. (It deals both first-strike and regular combat damage.)').
card_mana_cost('battle mastery', ['2', 'W']).
card_cmc('battle mastery', 3).
card_layout('battle mastery', 'normal').

% Found in: 9ED, M13, ODY
card_name('battle of wits', 'Battle of Wits').
card_type('battle of wits', 'Enchantment').
card_types('battle of wits', ['Enchantment']).
card_subtypes('battle of wits', []).
card_colors('battle of wits', ['U']).
card_text('battle of wits', 'At the beginning of your upkeep, if you have 200 or more cards in your library, you win the game.').
card_mana_cost('battle of wits', ['3', 'U', 'U']).
card_cmc('battle of wits', 5).
card_layout('battle of wits', 'normal').

% Found in: MMQ, ROE
card_name('battle rampart', 'Battle Rampart').
card_type('battle rampart', 'Creature — Wall').
card_types('battle rampart', ['Creature']).
card_subtypes('battle rampart', ['Wall']).
card_colors('battle rampart', ['R']).
card_text('battle rampart', 'Defender\n{T}: Target creature gains haste until end of turn.').
card_mana_cost('battle rampart', ['2', 'R']).
card_cmc('battle rampart', 3).
card_layout('battle rampart', 'normal').
card_power('battle rampart', 1).
card_toughness('battle rampart', 3).

% Found in: JUD, VMA
card_name('battle screech', 'Battle Screech').
card_type('battle screech', 'Sorcery').
card_types('battle screech', ['Sorcery']).
card_subtypes('battle screech', []).
card_colors('battle screech', ['W']).
card_text('battle screech', 'Put two 1/1 white Bird creature tokens with flying onto the battlefield.\nFlashback—Tap three untapped white creatures you control. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('battle screech', ['2', 'W', 'W']).
card_cmc('battle screech', 4).
card_layout('battle screech', 'normal').

% Found in: M14
card_name('battle sliver', 'Battle Sliver').
card_type('battle sliver', 'Creature — Sliver').
card_types('battle sliver', ['Creature']).
card_subtypes('battle sliver', ['Sliver']).
card_colors('battle sliver', ['R']).
card_text('battle sliver', 'Sliver creatures you control get +2/+0.').
card_mana_cost('battle sliver', ['4', 'R']).
card_cmc('battle sliver', 5).
card_layout('battle sliver', 'normal').
card_power('battle sliver', 3).
card_toughness('battle sliver', 3).

% Found in: MMQ
card_name('battle squadron', 'Battle Squadron').
card_type('battle squadron', 'Creature — Goblin').
card_types('battle squadron', ['Creature']).
card_subtypes('battle squadron', ['Goblin']).
card_colors('battle squadron', ['R']).
card_text('battle squadron', 'Flying\nBattle Squadron\'s power and toughness are each equal to the number of creatures you control.').
card_mana_cost('battle squadron', ['3', 'R', 'R']).
card_cmc('battle squadron', 5).
card_layout('battle squadron', 'normal').
card_power('battle squadron', '*').
card_toughness('battle squadron', '*').

% Found in: ODY
card_name('battle strain', 'Battle Strain').
card_type('battle strain', 'Enchantment').
card_types('battle strain', ['Enchantment']).
card_subtypes('battle strain', []).
card_colors('battle strain', ['R']).
card_text('battle strain', 'Whenever a creature blocks, Battle Strain deals 1 damage to that creature\'s controller.').
card_mana_cost('battle strain', ['1', 'R']).
card_cmc('battle strain', 2).
card_layout('battle strain', 'normal').

% Found in: CHK
card_name('battle-mad ronin', 'Battle-Mad Ronin').
card_type('battle-mad ronin', 'Creature — Human Samurai').
card_types('battle-mad ronin', ['Creature']).
card_subtypes('battle-mad ronin', ['Human', 'Samurai']).
card_colors('battle-mad ronin', ['R']).
card_text('battle-mad ronin', 'Bushido 2 (Whenever this creature blocks or becomes blocked, it gets +2/+2 until end of turn.)\nBattle-Mad Ronin attacks each turn if able.').
card_mana_cost('battle-mad ronin', ['1', 'R']).
card_cmc('battle-mad ronin', 2).
card_layout('battle-mad ronin', 'normal').
card_power('battle-mad ronin', 1).
card_toughness('battle-mad ronin', 1).

% Found in: ROE
card_name('battle-rattle shaman', 'Battle-Rattle Shaman').
card_type('battle-rattle shaman', 'Creature — Goblin Shaman').
card_types('battle-rattle shaman', ['Creature']).
card_subtypes('battle-rattle shaman', ['Goblin', 'Shaman']).
card_colors('battle-rattle shaman', ['R']).
card_text('battle-rattle shaman', 'At the beginning of combat on your turn, you may have target creature get +2/+0 until end of turn.').
card_mana_cost('battle-rattle shaman', ['3', 'R']).
card_cmc('battle-rattle shaman', 4).
card_layout('battle-rattle shaman', 'normal').
card_power('battle-rattle shaman', 2).
card_toughness('battle-rattle shaman', 2).

% Found in: 10E, 9ED, APC, M15, ORI
card_name('battlefield forge', 'Battlefield Forge').
card_type('battlefield forge', 'Land').
card_types('battlefield forge', ['Land']).
card_subtypes('battlefield forge', []).
card_colors('battlefield forge', []).
card_text('battlefield forge', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {W} to your mana pool. Battlefield Forge deals 1 damage to you.').
card_layout('battlefield forge', 'normal').

% Found in: ONS
card_name('battlefield medic', 'Battlefield Medic').
card_type('battlefield medic', 'Creature — Human Cleric').
card_types('battlefield medic', ['Creature']).
card_subtypes('battlefield medic', ['Human', 'Cleric']).
card_colors('battlefield medic', ['W']).
card_text('battlefield medic', '{T}: Prevent the next X damage that would be dealt to target creature this turn, where X is the number of Clerics on the battlefield.').
card_mana_cost('battlefield medic', ['1', 'W']).
card_cmc('battlefield medic', 2).
card_layout('battlefield medic', 'normal').
card_power('battlefield medic', 1).
card_toughness('battlefield medic', 1).

% Found in: NMS
card_name('battlefield percher', 'Battlefield Percher').
card_type('battlefield percher', 'Creature — Bird').
card_types('battlefield percher', ['Creature']).
card_subtypes('battlefield percher', ['Bird']).
card_colors('battlefield percher', ['B']).
card_text('battlefield percher', 'Flying\nBattlefield Percher can block only creatures with flying.\n{1}{B}: Battlefield Percher gets +1/+1 until end of turn.').
card_mana_cost('battlefield percher', ['3', 'B', 'B']).
card_cmc('battlefield percher', 5).
card_layout('battlefield percher', 'normal').
card_power('battlefield percher', 2).
card_toughness('battlefield percher', 2).

% Found in: JUD
card_name('battlefield scrounger', 'Battlefield Scrounger').
card_type('battlefield scrounger', 'Creature — Centaur').
card_types('battlefield scrounger', ['Creature']).
card_subtypes('battlefield scrounger', ['Centaur']).
card_colors('battlefield scrounger', ['G']).
card_text('battlefield scrounger', 'Threshold — Put three cards from your graveyard on the bottom of your library: Battlefield Scrounger gets +3/+3 until end of turn. Activate this ability only once each turn, and only if seven or more cards are in your graveyard.').
card_mana_cost('battlefield scrounger', ['3', 'G', 'G']).
card_cmc('battlefield scrounger', 5).
card_layout('battlefield scrounger', 'normal').
card_power('battlefield scrounger', 3).
card_toughness('battlefield scrounger', 3).

% Found in: JOU
card_name('battlefield thaumaturge', 'Battlefield Thaumaturge').
card_type('battlefield thaumaturge', 'Creature — Human Wizard').
card_types('battlefield thaumaturge', ['Creature']).
card_subtypes('battlefield thaumaturge', ['Human', 'Wizard']).
card_colors('battlefield thaumaturge', ['U']).
card_text('battlefield thaumaturge', 'Each instant and sorcery spell you cast costs {1} less to cast for each creature it targets.\nHeroic — Whenever you cast a spell that targets Battlefield Thaumaturge, Battlefield Thaumaturge gains hexproof until end of turn.').
card_mana_cost('battlefield thaumaturge', ['1', 'U']).
card_cmc('battlefield thaumaturge', 2).
card_layout('battlefield thaumaturge', 'normal').
card_power('battlefield thaumaturge', 2).
card_toughness('battlefield thaumaturge', 1).

% Found in: M13
card_name('battleflight eagle', 'Battleflight Eagle').
card_type('battleflight eagle', 'Creature — Bird').
card_types('battleflight eagle', ['Creature']).
card_subtypes('battleflight eagle', ['Bird']).
card_colors('battleflight eagle', ['W']).
card_text('battleflight eagle', 'Flying\nWhen Battleflight Eagle enters the battlefield, target creature gets +2/+2 and gains flying until end of turn.').
card_mana_cost('battleflight eagle', ['4', 'W']).
card_cmc('battleflight eagle', 5).
card_layout('battleflight eagle', 'normal').
card_power('battleflight eagle', 2).
card_toughness('battleflight eagle', 2).

% Found in: FRF
card_name('battlefront krushok', 'Battlefront Krushok').
card_type('battlefront krushok', 'Creature — Beast').
card_types('battlefront krushok', ['Creature']).
card_subtypes('battlefront krushok', ['Beast']).
card_colors('battlefront krushok', ['G']).
card_text('battlefront krushok', 'Battlefront Krushok can\'t be blocked by more than one creature.\nEach creature you control with a +1/+1 counter on it can\'t be blocked by more than one creature.').
card_mana_cost('battlefront krushok', ['4', 'G']).
card_cmc('battlefront krushok', 5).
card_layout('battlefront krushok', 'normal').
card_power('battlefront krushok', 3).
card_toughness('battlefront krushok', 4).

% Found in: EVE, HOP
card_name('battlegate mimic', 'Battlegate Mimic').
card_type('battlegate mimic', 'Creature — Shapeshifter').
card_types('battlegate mimic', ['Creature']).
card_subtypes('battlegate mimic', ['Shapeshifter']).
card_colors('battlegate mimic', ['W', 'R']).
card_text('battlegate mimic', 'Whenever you cast a spell that\'s both red and white, Battlegate Mimic has base power and toughness 4/2 until end of turn and gains first strike until end of turn.').
card_mana_cost('battlegate mimic', ['1', 'R/W']).
card_cmc('battlegate mimic', 2).
card_layout('battlegate mimic', 'normal').
card_power('battlegate mimic', 2).
card_toughness('battlegate mimic', 1).

% Found in: ALA, MM2
card_name('battlegrace angel', 'Battlegrace Angel').
card_type('battlegrace angel', 'Creature — Angel').
card_types('battlegrace angel', ['Creature']).
card_subtypes('battlegrace angel', ['Angel']).
card_colors('battlegrace angel', ['W']).
card_text('battlegrace angel', 'Flying\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever a creature you control attacks alone, it gains lifelink until end of turn.').
card_mana_cost('battlegrace angel', ['3', 'W', 'W']).
card_cmc('battlegrace angel', 5).
card_layout('battlegrace angel', 'normal').
card_power('battlegrace angel', 4).
card_toughness('battlegrace angel', 4).

% Found in: ISD
card_name('battleground geist', 'Battleground Geist').
card_type('battleground geist', 'Creature — Spirit').
card_types('battleground geist', ['Creature']).
card_subtypes('battleground geist', ['Spirit']).
card_colors('battleground geist', ['U']).
card_text('battleground geist', 'Flying\nOther Spirit creatures you control get +1/+0.').
card_mana_cost('battleground geist', ['4', 'U']).
card_cmc('battleground geist', 5).
card_layout('battleground geist', 'normal').
card_power('battleground geist', 3).
card_toughness('battleground geist', 3).

% Found in: MRD
card_name('battlegrowth', 'Battlegrowth').
card_type('battlegrowth', 'Instant').
card_types('battlegrowth', ['Instant']).
card_subtypes('battlegrowth', []).
card_colors('battlegrowth', ['G']).
card_text('battlegrowth', 'Put a +1/+1 counter on target creature.').
card_mana_cost('battlegrowth', ['G']).
card_cmc('battlegrowth', 1).
card_layout('battlegrowth', 'normal').

% Found in: MOR
card_name('battletide alchemist', 'Battletide Alchemist').
card_type('battletide alchemist', 'Creature — Kithkin Cleric').
card_types('battletide alchemist', ['Creature']).
card_subtypes('battletide alchemist', ['Kithkin', 'Cleric']).
card_colors('battletide alchemist', ['W']).
card_text('battletide alchemist', 'If a source would deal damage to a player, you may prevent X of that damage, where X is the number of Clerics you control.').
card_mana_cost('battletide alchemist', ['3', 'W', 'W']).
card_cmc('battletide alchemist', 5).
card_layout('battletide alchemist', 'normal').
card_power('battletide alchemist', 3).
card_toughness('battletide alchemist', 4).

% Found in: LRW
card_name('battlewand oak', 'Battlewand Oak').
card_type('battlewand oak', 'Creature — Treefolk Warrior').
card_types('battlewand oak', ['Creature']).
card_subtypes('battlewand oak', ['Treefolk', 'Warrior']).
card_colors('battlewand oak', ['G']).
card_text('battlewand oak', 'Whenever a Forest enters the battlefield under your control, Battlewand Oak gets +2/+2 until end of turn.\nWhenever you cast a Treefolk spell, Battlewand Oak gets +2/+2 until end of turn.').
card_mana_cost('battlewand oak', ['2', 'G']).
card_cmc('battlewand oak', 3).
card_layout('battlewand oak', 'normal').
card_power('battlewand oak', 1).
card_toughness('battlewand oak', 3).

% Found in: JUD
card_name('battlewise aven', 'Battlewise Aven').
card_type('battlewise aven', 'Creature — Bird Soldier').
card_types('battlewise aven', ['Creature']).
card_subtypes('battlewise aven', ['Bird', 'Soldier']).
card_colors('battlewise aven', ['W']).
card_text('battlewise aven', 'Flying\nThreshold — As long as seven or more cards are in your graveyard, Battlewise Aven gets +1/+1 and has first strike.').
card_mana_cost('battlewise aven', ['3', 'W']).
card_cmc('battlewise aven', 4).
card_layout('battlewise aven', 'normal').
card_power('battlewise aven', 2).
card_toughness('battlewise aven', 2).

% Found in: THS
card_name('battlewise hoplite', 'Battlewise Hoplite').
card_type('battlewise hoplite', 'Creature — Human Soldier').
card_types('battlewise hoplite', ['Creature']).
card_subtypes('battlewise hoplite', ['Human', 'Soldier']).
card_colors('battlewise hoplite', ['W', 'U']).
card_text('battlewise hoplite', 'Heroic — Whenever you cast a spell that targets Battlewise Hoplite, put a +1/+1 counter on Battlewise Hoplite, then scry 1. (To scry 1, look at the top card of your library, then you may put that card on the bottom of your library.)').
card_mana_cost('battlewise hoplite', ['W', 'U']).
card_cmc('battlewise hoplite', 2).
card_layout('battlewise hoplite', 'normal').
card_power('battlewise hoplite', 2).
card_toughness('battlewise hoplite', 2).

% Found in: THS
card_name('battlewise valor', 'Battlewise Valor').
card_type('battlewise valor', 'Instant').
card_types('battlewise valor', ['Instant']).
card_subtypes('battlewise valor', []).
card_colors('battlewise valor', ['W']).
card_text('battlewise valor', 'Target creature gets +2/+2 until end of turn. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('battlewise valor', ['1', 'W']).
card_cmc('battlewise valor', 2).
card_layout('battlewise valor', 'normal').

% Found in: ARC, EVE
card_name('batwing brume', 'Batwing Brume').
card_type('batwing brume', 'Instant').
card_types('batwing brume', ['Instant']).
card_subtypes('batwing brume', []).
card_colors('batwing brume', ['W', 'B']).
card_text('batwing brume', 'Prevent all combat damage that would be dealt this turn if {W} was spent to cast Batwing Brume. Each player loses 1 life for each attacking creature he or she controls if {B} was spent to cast Batwing Brume. (Do both if {W}{B} was spent.)').
card_mana_cost('batwing brume', ['1', 'W/B']).
card_cmc('batwing brume', 2).
card_layout('batwing brume', 'normal').

% Found in: MIR
card_name('bay falcon', 'Bay Falcon').
card_type('bay falcon', 'Creature — Bird').
card_types('bay falcon', ['Creature']).
card_subtypes('bay falcon', ['Bird']).
card_colors('bay falcon', ['U']).
card_text('bay falcon', 'Flying, vigilance').
card_mana_cost('bay falcon', ['1', 'U']).
card_cmc('bay falcon', 2).
card_layout('bay falcon', 'normal').
card_power('bay falcon', 1).
card_toughness('bay falcon', 1).

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME3, ME4, VMA
card_name('bayou', 'Bayou').
card_type('bayou', 'Land — Swamp Forest').
card_types('bayou', ['Land']).
card_subtypes('bayou', ['Swamp', 'Forest']).
card_colors('bayou', []).
card_text('bayou', '({T}: Add {B} or {G} to your mana pool.)').
card_layout('bayou', 'normal').
card_reserved('bayou').

% Found in: TMP
card_name('bayou dragonfly', 'Bayou Dragonfly').
card_type('bayou dragonfly', 'Creature — Insect').
card_types('bayou dragonfly', ['Creature']).
card_subtypes('bayou dragonfly', ['Insect']).
card_colors('bayou dragonfly', ['G']).
card_text('bayou dragonfly', 'Flying; swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('bayou dragonfly', ['1', 'G']).
card_cmc('bayou dragonfly', 2).
card_layout('bayou dragonfly', 'normal').
card_power('bayou dragonfly', 1).
card_toughness('bayou dragonfly', 1).

% Found in: RTR
card_name('bazaar krovod', 'Bazaar Krovod').
card_type('bazaar krovod', 'Creature — Beast').
card_types('bazaar krovod', ['Creature']).
card_subtypes('bazaar krovod', ['Beast']).
card_colors('bazaar krovod', ['W']).
card_text('bazaar krovod', 'Whenever Bazaar Krovod attacks, another target attacking creature gets +0/+2 until end of turn. Untap that creature.').
card_mana_cost('bazaar krovod', ['4', 'W']).
card_cmc('bazaar krovod', 5).
card_layout('bazaar krovod', 'normal').
card_power('bazaar krovod', 2).
card_toughness('bazaar krovod', 5).

% Found in: ARN, ME3, VMA
card_name('bazaar of baghdad', 'Bazaar of Baghdad').
card_type('bazaar of baghdad', 'Land').
card_types('bazaar of baghdad', ['Land']).
card_subtypes('bazaar of baghdad', []).
card_colors('bazaar of baghdad', []).
card_text('bazaar of baghdad', '{T}: Draw two cards, then discard three cards.').
card_layout('bazaar of baghdad', 'normal').
card_reserved('bazaar of baghdad').

% Found in: MIR
card_name('bazaar of wonders', 'Bazaar of Wonders').
card_type('bazaar of wonders', 'World Enchantment').
card_types('bazaar of wonders', ['Enchantment']).
card_subtypes('bazaar of wonders', []).
card_supertypes('bazaar of wonders', ['World']).
card_colors('bazaar of wonders', ['U']).
card_text('bazaar of wonders', 'When Bazaar of Wonders enters the battlefield, exile all cards from all graveyards.\nWhenever a player casts a spell, counter it if a card with the same name is in a graveyard or a nontoken permanent with the same name is on the battlefield.').
card_mana_cost('bazaar of wonders', ['3', 'U', 'U']).
card_cmc('bazaar of wonders', 5).
card_layout('bazaar of wonders', 'normal').
card_reserved('bazaar of wonders').

% Found in: WWK
card_name('bazaar trader', 'Bazaar Trader').
card_type('bazaar trader', 'Creature — Goblin').
card_types('bazaar trader', ['Creature']).
card_subtypes('bazaar trader', ['Goblin']).
card_colors('bazaar trader', ['R']).
card_text('bazaar trader', '{T}: Target player gains control of target artifact, creature, or land you control.').
card_mana_cost('bazaar trader', ['1', 'R']).
card_cmc('bazaar trader', 2).
card_layout('bazaar trader', 'normal').
card_power('bazaar trader', 1).
card_toughness('bazaar trader', 1).

% Found in: CON
card_name('beacon behemoth', 'Beacon Behemoth').
card_type('beacon behemoth', 'Creature — Beast').
card_types('beacon behemoth', ['Creature']).
card_subtypes('beacon behemoth', ['Beast']).
card_colors('beacon behemoth', ['G']).
card_text('beacon behemoth', '{1}: Target creature with power 5 or greater gains vigilance until end of turn.').
card_mana_cost('beacon behemoth', ['3', 'G', 'G']).
card_cmc('beacon behemoth', 5).
card_layout('beacon behemoth', 'normal').
card_power('beacon behemoth', 5).
card_toughness('beacon behemoth', 3).

% Found in: DIS
card_name('beacon hawk', 'Beacon Hawk').
card_type('beacon hawk', 'Creature — Bird').
card_types('beacon hawk', ['Creature']).
card_subtypes('beacon hawk', ['Bird']).
card_colors('beacon hawk', ['W']).
card_text('beacon hawk', 'Flying\nWhenever Beacon Hawk deals combat damage to a player, you may untap target creature.\n{W}: Beacon Hawk gets +0/+1 until end of turn.').
card_mana_cost('beacon hawk', ['1', 'W']).
card_cmc('beacon hawk', 2).
card_layout('beacon hawk', 'normal').
card_power('beacon hawk', 1).
card_toughness('beacon hawk', 1).

% Found in: 5DN
card_name('beacon of creation', 'Beacon of Creation').
card_type('beacon of creation', 'Sorcery').
card_types('beacon of creation', ['Sorcery']).
card_subtypes('beacon of creation', []).
card_colors('beacon of creation', ['G']).
card_text('beacon of creation', 'Put a 1/1 green Insect creature token onto the battlefield for each Forest you control. Shuffle Beacon of Creation into its owner\'s library.').
card_mana_cost('beacon of creation', ['3', 'G']).
card_cmc('beacon of creation', 4).
card_layout('beacon of creation', 'normal').

% Found in: LGN
card_name('beacon of destiny', 'Beacon of Destiny').
card_type('beacon of destiny', 'Creature — Human Cleric').
card_types('beacon of destiny', ['Creature']).
card_subtypes('beacon of destiny', ['Human', 'Cleric']).
card_colors('beacon of destiny', ['W']).
card_text('beacon of destiny', '{T}: The next time a source of your choice would deal damage to you this turn, that damage is dealt to Beacon of Destiny instead.').
card_mana_cost('beacon of destiny', ['1', 'W']).
card_cmc('beacon of destiny', 2).
card_layout('beacon of destiny', 'normal').
card_power('beacon of destiny', 1).
card_toughness('beacon of destiny', 3).

% Found in: 10E, 5DN
card_name('beacon of destruction', 'Beacon of Destruction').
card_type('beacon of destruction', 'Instant').
card_types('beacon of destruction', ['Instant']).
card_subtypes('beacon of destruction', []).
card_colors('beacon of destruction', ['R']).
card_text('beacon of destruction', 'Beacon of Destruction deals 5 damage to target creature or player. Shuffle Beacon of Destruction into its owner\'s library.').
card_mana_cost('beacon of destruction', ['3', 'R', 'R']).
card_cmc('beacon of destruction', 5).
card_layout('beacon of destruction', 'normal').

% Found in: 10E, 5DN
card_name('beacon of immortality', 'Beacon of Immortality').
card_type('beacon of immortality', 'Instant').
card_types('beacon of immortality', ['Instant']).
card_subtypes('beacon of immortality', []).
card_colors('beacon of immortality', ['W']).
card_text('beacon of immortality', 'Double target player\'s life total. Shuffle Beacon of Immortality into its owner\'s library.').
card_mana_cost('beacon of immortality', ['5', 'W']).
card_cmc('beacon of immortality', 6).
card_layout('beacon of immortality', 'normal').

% Found in: 5DN
card_name('beacon of tomorrows', 'Beacon of Tomorrows').
card_type('beacon of tomorrows', 'Sorcery').
card_types('beacon of tomorrows', ['Sorcery']).
card_subtypes('beacon of tomorrows', []).
card_colors('beacon of tomorrows', ['U']).
card_text('beacon of tomorrows', 'Target player takes an extra turn after this one. Shuffle Beacon of Tomorrows into its owner\'s library.').
card_mana_cost('beacon of tomorrows', ['6', 'U', 'U']).
card_cmc('beacon of tomorrows', 8).
card_layout('beacon of tomorrows', 'normal').

% Found in: 10E, 5DN, ARC, HOP
card_name('beacon of unrest', 'Beacon of Unrest').
card_type('beacon of unrest', 'Sorcery').
card_types('beacon of unrest', ['Sorcery']).
card_subtypes('beacon of unrest', []).
card_colors('beacon of unrest', ['B']).
card_text('beacon of unrest', 'Put target artifact or creature card from a graveyard onto the battlefield under your control. Shuffle Beacon of Unrest into its owner\'s library.').
card_mana_cost('beacon of unrest', ['3', 'B', 'B']).
card_cmc('beacon of unrest', 5).
card_layout('beacon of unrest', 'normal').

% Found in: PO2
card_name('bear cub', 'Bear Cub').
card_type('bear cub', 'Creature — Bear').
card_types('bear cub', ['Creature']).
card_subtypes('bear cub', ['Bear']).
card_colors('bear cub', ['G']).
card_text('bear cub', '').
card_mana_cost('bear cub', ['1', 'G']).
card_cmc('bear cub', 2).
card_layout('bear cub', 'normal').
card_power('bear cub', 2).
card_toughness('bear cub', 2).

% Found in: ROE
card_name('bear umbra', 'Bear Umbra').
card_type('bear umbra', 'Enchantment — Aura').
card_types('bear umbra', ['Enchantment']).
card_subtypes('bear umbra', ['Aura']).
card_colors('bear umbra', ['G']).
card_text('bear umbra', 'Enchant creature\nEnchanted creature gets +2/+2 and has \"Whenever this creature attacks, untap all lands you control.\"\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_mana_cost('bear umbra', ['2', 'G', 'G']).
card_cmc('bear umbra', 4).
card_layout('bear umbra', 'normal').

% Found in: KTK
card_name('bear\'s companion', 'Bear\'s Companion').
card_type('bear\'s companion', 'Creature — Human Warrior').
card_types('bear\'s companion', ['Creature']).
card_subtypes('bear\'s companion', ['Human', 'Warrior']).
card_colors('bear\'s companion', ['U', 'R', 'G']).
card_text('bear\'s companion', 'When Bear\'s Companion enters the battlefield, put a 4/4 green Bear creature token onto the battlefield.').
card_mana_cost('bear\'s companion', ['2', 'G', 'U', 'R']).
card_cmc('bear\'s companion', 5).
card_layout('bear\'s companion', 'normal').
card_power('bear\'s companion', 2).
card_toughness('bear\'s companion', 2).

% Found in: JOU
card_name('bearer of the heavens', 'Bearer of the Heavens').
card_type('bearer of the heavens', 'Creature — Giant').
card_types('bearer of the heavens', ['Creature']).
card_subtypes('bearer of the heavens', ['Giant']).
card_colors('bearer of the heavens', ['R']).
card_text('bearer of the heavens', 'When Bearer of the Heavens dies, destroy all permanents at the beginning of the next end step.').
card_mana_cost('bearer of the heavens', ['7', 'R']).
card_cmc('bearer of the heavens', 8).
card_layout('bearer of the heavens', 'normal').
card_power('bearer of the heavens', 10).
card_toughness('bearer of the heavens', 10).

% Found in: ODY
card_name('bearscape', 'Bearscape').
card_type('bearscape', 'Enchantment').
card_types('bearscape', ['Enchantment']).
card_subtypes('bearscape', []).
card_colors('bearscape', ['G']).
card_text('bearscape', '{1}{G}, Exile two cards from your graveyard: Put a 2/2 green Bear creature token onto the battlefield.').
card_mana_cost('bearscape', ['1', 'G', 'G']).
card_cmc('bearscape', 3).
card_layout('bearscape', 'normal').

% Found in: DDD
card_name('beast', 'Beast').
card_type('beast', 'Creature — Beast').
card_types('beast', ['Creature']).
card_subtypes('beast', ['Beast']).
card_colors('beast', []).
card_text('beast', '').
card_layout('beast', 'token').
card_power('beast', 4).
card_toughness('beast', 4).

% Found in: DD3_GVL, DDD, ODY
card_name('beast attack', 'Beast Attack').
card_type('beast attack', 'Instant').
card_types('beast attack', ['Instant']).
card_subtypes('beast attack', []).
card_colors('beast attack', ['G']).
card_text('beast attack', 'Put a 4/4 green Beast creature token onto the battlefield.\nFlashback {2}{G}{G}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('beast attack', ['2', 'G', 'G', 'G']).
card_cmc('beast attack', 5).
card_layout('beast attack', 'normal').

% Found in: HOP, ZEN
card_name('beast hunt', 'Beast Hunt').
card_type('beast hunt', 'Sorcery').
card_types('beast hunt', ['Sorcery']).
card_subtypes('beast hunt', []).
card_colors('beast hunt', ['G']).
card_text('beast hunt', 'Reveal the top three cards of your library. Put all creature cards revealed this way into your hand and the rest into your graveyard.').
card_mana_cost('beast hunt', ['3', 'G']).
card_cmc('beast hunt', 4).
card_layout('beast hunt', 'normal').

% Found in: 7ED, 8ED, 9ED, ULG, pPRE
card_name('beast of burden', 'Beast of Burden').
card_type('beast of burden', 'Artifact Creature — Golem').
card_types('beast of burden', ['Artifact', 'Creature']).
card_subtypes('beast of burden', ['Golem']).
card_colors('beast of burden', []).
card_text('beast of burden', 'Beast of Burden\'s power and toughness are each equal to the number of creatures on the battlefield.').
card_mana_cost('beast of burden', ['6']).
card_cmc('beast of burden', 6).
card_layout('beast of burden', 'normal').
card_power('beast of burden', '*').
card_toughness('beast of burden', '*').

% Found in: HML
card_name('beast walkers', 'Beast Walkers').
card_type('beast walkers', 'Creature — Human Beast Soldier').
card_types('beast walkers', ['Creature']).
card_subtypes('beast walkers', ['Human', 'Beast', 'Soldier']).
card_colors('beast walkers', ['W']).
card_text('beast walkers', '{G}: Beast Walkers gains banding until end of turn. (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('beast walkers', ['1', 'W', 'W']).
card_cmc('beast walkers', 3).
card_layout('beast walkers', 'normal').
card_power('beast walkers', 2).
card_toughness('beast walkers', 2).
card_reserved('beast walkers').

% Found in: DDL, NPH, PC2
card_name('beast within', 'Beast Within').
card_type('beast within', 'Instant').
card_types('beast within', ['Instant']).
card_subtypes('beast within', []).
card_colors('beast within', ['G']).
card_text('beast within', 'Destroy target permanent. Its controller puts a 3/3 green Beast creature token onto the battlefield.').
card_mana_cost('beast within', ['2', 'G']).
card_cmc('beast within', 3).
card_layout('beast within', 'normal').

% Found in: DDP, ROE
card_name('beastbreaker of bala ged', 'Beastbreaker of Bala Ged').
card_type('beastbreaker of bala ged', 'Creature — Human Warrior').
card_types('beastbreaker of bala ged', ['Creature']).
card_subtypes('beastbreaker of bala ged', ['Human', 'Warrior']).
card_colors('beastbreaker of bala ged', ['G']).
card_text('beastbreaker of bala ged', 'Level up {2}{G} ({2}{G}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-3\n4/4\nLEVEL 4+\n6/6\nTrample').
card_mana_cost('beastbreaker of bala ged', ['1', 'G']).
card_cmc('beastbreaker of bala ged', 2).
card_layout('beastbreaker of bala ged', 'leveler').
card_power('beastbreaker of bala ged', 2).
card_toughness('beastbreaker of bala ged', 2).

% Found in: BFZ
card_name('beastcaller savant', 'Beastcaller Savant').
card_type('beastcaller savant', 'Creature — Elf Shaman Ally').
card_types('beastcaller savant', ['Creature']).
card_subtypes('beastcaller savant', ['Elf', 'Shaman', 'Ally']).
card_colors('beastcaller savant', ['G']).
card_text('beastcaller savant', 'Haste\n{T}: Add one mana of any color to your mana pool. Spend this mana only to cast a creature spell.').
card_mana_cost('beastcaller savant', ['1', 'G']).
card_cmc('beastcaller savant', 2).
card_layout('beastcaller savant', 'normal').
card_power('beastcaller savant', 1).
card_toughness('beastcaller savant', 1).

% Found in: C14, ZEN
card_name('beastmaster ascension', 'Beastmaster Ascension').
card_type('beastmaster ascension', 'Enchantment').
card_types('beastmaster ascension', ['Enchantment']).
card_subtypes('beastmaster ascension', []).
card_colors('beastmaster ascension', ['G']).
card_text('beastmaster ascension', 'Whenever a creature you control attacks, you may put a quest counter on Beastmaster Ascension.\nAs long as Beastmaster Ascension has seven or more quest counters on it, creatures you control get +5/+5.').
card_mana_cost('beastmaster ascension', ['2', 'G']).
card_cmc('beastmaster ascension', 3).
card_layout('beastmaster ascension', 'normal').

% Found in: GPT
card_name('beastmaster\'s magemark', 'Beastmaster\'s Magemark').
card_type('beastmaster\'s magemark', 'Enchantment — Aura').
card_types('beastmaster\'s magemark', ['Enchantment']).
card_subtypes('beastmaster\'s magemark', ['Aura']).
card_colors('beastmaster\'s magemark', ['G']).
card_text('beastmaster\'s magemark', 'Enchant creature\nCreatures you control that are enchanted get +1/+1.\nWhenever a creature you control that\'s enchanted becomes blocked, it gets +1/+1 until end of turn for each creature blocking it.').
card_mana_cost('beastmaster\'s magemark', ['2', 'G']).
card_cmc('beastmaster\'s magemark', 3).
card_layout('beastmaster\'s magemark', 'normal').

% Found in: CHR, LEG
card_name('beasts of bogardan', 'Beasts of Bogardan').
card_type('beasts of bogardan', 'Creature — Beast').
card_types('beasts of bogardan', ['Creature']).
card_subtypes('beasts of bogardan', ['Beast']).
card_colors('beasts of bogardan', ['R']).
card_text('beasts of bogardan', 'Protection from red\nBeasts of Bogardan gets +1/+1 as long as an opponent controls a nontoken white permanent.').
card_mana_cost('beasts of bogardan', ['4', 'R']).
card_cmc('beasts of bogardan', 5).
card_layout('beasts of bogardan', 'normal').
card_power('beasts of bogardan', 3).
card_toughness('beasts of bogardan', 3).

% Found in: DGM
card_name('beck', 'Beck').
card_type('beck', 'Sorcery').
card_types('beck', ['Sorcery']).
card_subtypes('beck', []).
card_colors('beck', ['U', 'G']).
card_text('beck', 'Whenever a creature enters the battlefield this turn, you may draw a card.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('beck', ['G', 'U']).
card_cmc('beck', 2).
card_layout('beck', 'split').
card_sides('beck', 'call').

% Found in: EVE, GTC
card_name('beckon apparition', 'Beckon Apparition').
card_type('beckon apparition', 'Instant').
card_types('beckon apparition', ['Instant']).
card_subtypes('beckon apparition', []).
card_colors('beckon apparition', ['W', 'B']).
card_text('beckon apparition', 'Exile target card from a graveyard. Put a 1/1 white and black Spirit creature token with flying onto the battlefield.').
card_mana_cost('beckon apparition', ['W/B']).
card_cmc('beckon apparition', 1).
card_layout('beckon apparition', 'normal').

% Found in: KTK
card_name('become immense', 'Become Immense').
card_type('become immense', 'Instant').
card_types('become immense', ['Instant']).
card_subtypes('become immense', []).
card_colors('become immense', ['G']).
card_text('become immense', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nTarget creature gets +6/+6 until end of turn.').
card_mana_cost('become immense', ['5', 'G']).
card_cmc('become immense', 6).
card_layout('become immense', 'normal').

% Found in: 7ED, USG
card_name('bedlam', 'Bedlam').
card_type('bedlam', 'Enchantment').
card_types('bedlam', ['Enchantment']).
card_subtypes('bedlam', []).
card_colors('bedlam', ['R']).
card_text('bedlam', 'Creatures can\'t block.').
card_mana_cost('bedlam', ['2', 'R', 'R']).
card_cmc('bedlam', 4).
card_layout('bedlam', 'normal').

% Found in: ME4, PO2, POR
card_name('bee sting', 'Bee Sting').
card_type('bee sting', 'Sorcery').
card_types('bee sting', ['Sorcery']).
card_subtypes('bee sting', []).
card_colors('bee sting', ['G']).
card_text('bee sting', 'Bee Sting deals 2 damage to target creature or player.').
card_mana_cost('bee sting', ['3', 'G']).
card_cmc('bee sting', 4).
card_layout('bee sting', 'normal').

% Found in: C14, DDN, PC2, VMA
card_name('beetleback chief', 'Beetleback Chief').
card_type('beetleback chief', 'Creature — Goblin Warrior').
card_types('beetleback chief', ['Creature']).
card_subtypes('beetleback chief', ['Goblin', 'Warrior']).
card_colors('beetleback chief', ['R']).
card_text('beetleback chief', 'When Beetleback Chief enters the battlefield, put two 1/1 red Goblin creature tokens onto the battlefield.').
card_mana_cost('beetleback chief', ['2', 'R', 'R']).
card_cmc('beetleback chief', 4).
card_layout('beetleback chief', 'normal').
card_power('beetleback chief', 2).
card_toughness('beetleback chief', 2).

% Found in: DGM
card_name('beetleform mage', 'Beetleform Mage').
card_type('beetleform mage', 'Creature — Human Insect Wizard').
card_types('beetleform mage', ['Creature']).
card_subtypes('beetleform mage', ['Human', 'Insect', 'Wizard']).
card_colors('beetleform mage', ['U', 'G']).
card_text('beetleform mage', '{G}{U}: Beetleform Mage gets +2/+2 and gains flying until end of turn. Activate this ability only once each turn.').
card_mana_cost('beetleform mage', ['1', 'G', 'U']).
card_cmc('beetleform mage', 3).
card_layout('beetleform mage', 'normal').
card_power('beetleform mage', 2).
card_toughness('beetleform mage', 2).

% Found in: 7ED, CHK, USG
card_name('befoul', 'Befoul').
card_type('befoul', 'Sorcery').
card_types('befoul', ['Sorcery']).
card_subtypes('befoul', []).
card_colors('befoul', ['B']).
card_text('befoul', 'Destroy target land or nonblack creature. It can\'t be regenerated.').
card_mana_cost('befoul', ['2', 'B', 'B']).
card_cmc('befoul', 4).
card_layout('befoul', 'normal').

% Found in: DKA
card_name('beguiler of wills', 'Beguiler of Wills').
card_type('beguiler of wills', 'Creature — Human Wizard').
card_types('beguiler of wills', ['Creature']).
card_subtypes('beguiler of wills', ['Human', 'Wizard']).
card_colors('beguiler of wills', ['U']).
card_text('beguiler of wills', '{T}: Gain control of target creature with power less than or equal to the number of creatures you control.').
card_mana_cost('beguiler of wills', ['3', 'U', 'U']).
card_cmc('beguiler of wills', 5).
card_layout('beguiler of wills', 'normal').
card_power('beguiler of wills', 1).
card_toughness('beguiler of wills', 1).

% Found in: ARB, C13, DDH
card_name('behemoth sledge', 'Behemoth Sledge').
card_type('behemoth sledge', 'Artifact — Equipment').
card_types('behemoth sledge', ['Artifact']).
card_subtypes('behemoth sledge', ['Equipment']).
card_colors('behemoth sledge', ['W', 'G']).
card_text('behemoth sledge', 'Equipped creature gets +2/+2 and has lifelink and trample.\nEquip {3}').
card_mana_cost('behemoth sledge', ['1', 'G', 'W']).
card_cmc('behemoth sledge', 3).
card_layout('behemoth sledge', 'normal').

% Found in: ALA
card_name('behemoth\'s herald', 'Behemoth\'s Herald').
card_type('behemoth\'s herald', 'Creature — Elf Shaman').
card_types('behemoth\'s herald', ['Creature']).
card_subtypes('behemoth\'s herald', ['Elf', 'Shaman']).
card_colors('behemoth\'s herald', ['G']).
card_text('behemoth\'s herald', '{2}{G}, {T}, Sacrifice a red creature, a green creature, and a white creature: Search your library for a card named Godsire and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('behemoth\'s herald', ['G']).
card_cmc('behemoth\'s herald', 1).
card_layout('behemoth\'s herald', 'normal').
card_power('behemoth\'s herald', 1).
card_toughness('behemoth\'s herald', 1).

% Found in: ARC
card_name('behold the power of destruction', 'Behold the Power of Destruction').
card_type('behold the power of destruction', 'Scheme').
card_types('behold the power of destruction', ['Scheme']).
card_subtypes('behold the power of destruction', []).
card_colors('behold the power of destruction', []).
card_text('behold the power of destruction', 'When you set this scheme in motion, destroy all nonland permanents target opponent controls.').
card_layout('behold the power of destruction', 'scheme').

% Found in: NMS
card_name('belbe\'s armor', 'Belbe\'s Armor').
card_type('belbe\'s armor', 'Artifact').
card_types('belbe\'s armor', ['Artifact']).
card_subtypes('belbe\'s armor', []).
card_colors('belbe\'s armor', []).
card_text('belbe\'s armor', '{X}, {T}: Target creature gets -X/+X until end of turn.').
card_mana_cost('belbe\'s armor', ['3']).
card_cmc('belbe\'s armor', 3).
card_layout('belbe\'s armor', 'normal').

% Found in: NMS
card_name('belbe\'s percher', 'Belbe\'s Percher').
card_type('belbe\'s percher', 'Creature — Bird').
card_types('belbe\'s percher', ['Creature']).
card_subtypes('belbe\'s percher', ['Bird']).
card_colors('belbe\'s percher', ['B']).
card_text('belbe\'s percher', 'Flying\nBelbe\'s Percher can block only creatures with flying.').
card_mana_cost('belbe\'s percher', ['2', 'B']).
card_cmc('belbe\'s percher', 3).
card_layout('belbe\'s percher', 'normal').
card_power('belbe\'s percher', 2).
card_toughness('belbe\'s percher', 2).

% Found in: NMS
card_name('belbe\'s portal', 'Belbe\'s Portal').
card_type('belbe\'s portal', 'Artifact').
card_types('belbe\'s portal', ['Artifact']).
card_subtypes('belbe\'s portal', []).
card_colors('belbe\'s portal', []).
card_text('belbe\'s portal', 'As Belbe\'s Portal enters the battlefield, choose a creature type.\n{3}, {T}: You may put a creature card of the chosen type from your hand onto the battlefield.').
card_mana_cost('belbe\'s portal', ['5']).
card_cmc('belbe\'s portal', 5).
card_layout('belbe\'s portal', 'normal').

% Found in: GPT
card_name('belfry spirit', 'Belfry Spirit').
card_type('belfry spirit', 'Creature — Spirit').
card_types('belfry spirit', ['Creature']).
card_subtypes('belfry spirit', ['Spirit']).
card_colors('belfry spirit', ['W']).
card_text('belfry spirit', 'Flying\nHaunt (When this creature dies, exile it haunting target creature.)\nWhen Belfry Spirit enters the battlefield or the creature it haunts dies, put two 1/1 black Bat creature tokens with flying onto the battlefield.').
card_mana_cost('belfry spirit', ['3', 'W', 'W']).
card_cmc('belfry spirit', 5).
card_layout('belfry spirit', 'normal').
card_power('belfry spirit', 1).
card_toughness('belfry spirit', 1).

% Found in: EVE
card_name('belligerent hatchling', 'Belligerent Hatchling').
card_type('belligerent hatchling', 'Creature — Elemental').
card_types('belligerent hatchling', ['Creature']).
card_subtypes('belligerent hatchling', ['Elemental']).
card_colors('belligerent hatchling', ['W', 'R']).
card_text('belligerent hatchling', 'First strike\nBelligerent Hatchling enters the battlefield with four -1/-1 counters on it.\nWhenever you cast a red spell, remove a -1/-1 counter from Belligerent Hatchling.\nWhenever you cast a white spell, remove a -1/-1 counter from Belligerent Hatchling.').
card_mana_cost('belligerent hatchling', ['3', 'R/W']).
card_cmc('belligerent hatchling', 4).
card_layout('belligerent hatchling', 'normal').
card_power('belligerent hatchling', 6).
card_toughness('belligerent hatchling', 6).

% Found in: M15
card_name('belligerent sliver', 'Belligerent Sliver').
card_type('belligerent sliver', 'Creature — Sliver').
card_types('belligerent sliver', ['Creature']).
card_subtypes('belligerent sliver', ['Sliver']).
card_colors('belligerent sliver', ['R']).
card_text('belligerent sliver', 'Sliver creatures you control have menace. (They can\'t be blocked except by two or more creatures.)').
card_mana_cost('belligerent sliver', ['2', 'R']).
card_cmc('belligerent sliver', 3).
card_layout('belligerent sliver', 'normal').
card_power('belligerent sliver', 2).
card_toughness('belligerent sliver', 2).

% Found in: BFZ
card_name('belligerent whiptail', 'Belligerent Whiptail').
card_type('belligerent whiptail', 'Creature — Wurm').
card_types('belligerent whiptail', ['Creature']).
card_subtypes('belligerent whiptail', ['Wurm']).
card_colors('belligerent whiptail', ['R']).
card_text('belligerent whiptail', 'Landfall — Whenever a land enters the battlefield under your control, Belligerent Whiptail gains first strike until end of turn.').
card_mana_cost('belligerent whiptail', ['3', 'R']).
card_cmc('belligerent whiptail', 4).
card_layout('belligerent whiptail', 'normal').
card_power('belligerent whiptail', 4).
card_toughness('belligerent whiptail', 2).

% Found in: 7ED, TMP
card_name('bellowing fiend', 'Bellowing Fiend').
card_type('bellowing fiend', 'Creature — Spirit').
card_types('bellowing fiend', ['Creature']).
card_subtypes('bellowing fiend', ['Spirit']).
card_colors('bellowing fiend', ['B']).
card_text('bellowing fiend', 'Flying\nWhenever Bellowing Fiend deals damage to a creature, Bellowing Fiend deals 3 damage to that creature\'s controller and 3 damage to you.').
card_mana_cost('bellowing fiend', ['4', 'B']).
card_cmc('bellowing fiend', 5).
card_layout('bellowing fiend', 'normal').
card_power('bellowing fiend', 3).
card_toughness('bellowing fiend', 3).

% Found in: KTK
card_name('bellowing saddlebrute', 'Bellowing Saddlebrute').
card_type('bellowing saddlebrute', 'Creature — Orc Warrior').
card_types('bellowing saddlebrute', ['Creature']).
card_subtypes('bellowing saddlebrute', ['Orc', 'Warrior']).
card_colors('bellowing saddlebrute', ['B']).
card_text('bellowing saddlebrute', 'Raid — When Bellowing Saddlebrute enters the battlefield, you lose 4 life unless you attacked with a creature this turn.').
card_mana_cost('bellowing saddlebrute', ['3', 'B']).
card_cmc('bellowing saddlebrute', 4).
card_layout('bellowing saddlebrute', 'normal').
card_power('bellowing saddlebrute', 4).
card_toughness('bellowing saddlebrute', 5).

% Found in: SOM
card_name('bellowing tanglewurm', 'Bellowing Tanglewurm').
card_type('bellowing tanglewurm', 'Creature — Wurm').
card_types('bellowing tanglewurm', ['Creature']).
card_subtypes('bellowing tanglewurm', ['Wurm']).
card_colors('bellowing tanglewurm', ['G']).
card_text('bellowing tanglewurm', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nOther green creatures you control have intimidate.').
card_mana_cost('bellowing tanglewurm', ['3', 'G', 'G']).
card_cmc('bellowing tanglewurm', 5).
card_layout('bellowing tanglewurm', 'normal').
card_power('bellowing tanglewurm', 4).
card_toughness('bellowing tanglewurm', 4).

% Found in: ORI, RTR
card_name('bellows lizard', 'Bellows Lizard').
card_type('bellows lizard', 'Creature — Lizard').
card_types('bellows lizard', ['Creature']).
card_subtypes('bellows lizard', ['Lizard']).
card_colors('bellows lizard', ['R']).
card_text('bellows lizard', '{1}{R}: Bellows Lizard gets +1/+0 until end of turn.').
card_mana_cost('bellows lizard', ['R']).
card_cmc('bellows lizard', 1).
card_layout('bellows lizard', 'normal').
card_power('bellows lizard', 1).
card_toughness('bellows lizard', 1).

% Found in: DTK
card_name('belltoll dragon', 'Belltoll Dragon').
card_type('belltoll dragon', 'Creature — Dragon').
card_types('belltoll dragon', ['Creature']).
card_subtypes('belltoll dragon', ['Dragon']).
card_colors('belltoll dragon', ['U']).
card_text('belltoll dragon', 'Flying, hexproof\nMegamorph {5}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Belltoll Dragon is turned face up, put a +1/+1 counter on each other Dragon creature you control.').
card_mana_cost('belltoll dragon', ['5', 'U']).
card_cmc('belltoll dragon', 6).
card_layout('belltoll dragon', 'normal').
card_power('belltoll dragon', 3).
card_toughness('belltoll dragon', 3).

% Found in: M12, RAV
card_name('belltower sphinx', 'Belltower Sphinx').
card_type('belltower sphinx', 'Creature — Sphinx').
card_types('belltower sphinx', ['Creature']).
card_subtypes('belltower sphinx', ['Sphinx']).
card_colors('belltower sphinx', ['U']).
card_text('belltower sphinx', 'Flying\nWhenever a source deals damage to Belltower Sphinx, that source\'s controller puts that many cards from the top of his or her library into his or her graveyard.').
card_mana_cost('belltower sphinx', ['4', 'U']).
card_cmc('belltower sphinx', 5).
card_layout('belltower sphinx', 'normal').
card_power('belltower sphinx', 2).
card_toughness('belltower sphinx', 5).

% Found in: ODY
card_name('beloved chaplain', 'Beloved Chaplain').
card_type('beloved chaplain', 'Creature — Human Cleric').
card_types('beloved chaplain', ['Creature']).
card_subtypes('beloved chaplain', ['Human', 'Cleric']).
card_colors('beloved chaplain', ['W']).
card_text('beloved chaplain', 'Protection from creatures').
card_mana_cost('beloved chaplain', ['1', 'W']).
card_cmc('beloved chaplain', 2).
card_layout('beloved chaplain', 'normal').
card_power('beloved chaplain', 1).
card_toughness('beloved chaplain', 1).

% Found in: CHK
card_name('ben-ben, akki hermit', 'Ben-Ben, Akki Hermit').
card_type('ben-ben, akki hermit', 'Legendary Creature — Goblin Shaman').
card_types('ben-ben, akki hermit', ['Creature']).
card_subtypes('ben-ben, akki hermit', ['Goblin', 'Shaman']).
card_supertypes('ben-ben, akki hermit', ['Legendary']).
card_colors('ben-ben, akki hermit', ['R']).
card_text('ben-ben, akki hermit', '{T}: Ben-Ben, Akki Hermit deals damage to target attacking creature equal to the number of untapped Mountains you control.').
card_mana_cost('ben-ben, akki hermit', ['2', 'R', 'R']).
card_cmc('ben-ben, akki hermit', 4).
card_layout('ben-ben, akki hermit', 'normal').
card_power('ben-ben, akki hermit', 1).
card_toughness('ben-ben, akki hermit', 1).

% Found in: TSP
card_name('benalish cavalry', 'Benalish Cavalry').
card_type('benalish cavalry', 'Creature — Human Knight').
card_types('benalish cavalry', ['Creature']).
card_subtypes('benalish cavalry', ['Human', 'Knight']).
card_colors('benalish cavalry', ['W']).
card_text('benalish cavalry', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)').
card_mana_cost('benalish cavalry', ['1', 'W']).
card_cmc('benalish cavalry', 2).
card_layout('benalish cavalry', 'normal').
card_power('benalish cavalry', 2).
card_toughness('benalish cavalry', 2).

% Found in: PLC
card_name('benalish commander', 'Benalish Commander').
card_type('benalish commander', 'Creature — Human Soldier').
card_types('benalish commander', ['Creature']).
card_subtypes('benalish commander', ['Human', 'Soldier']).
card_colors('benalish commander', ['W']).
card_text('benalish commander', 'Benalish Commander\'s power and toughness are each equal to the number of Soldiers you control.\nSuspend X—{X}{W}{W}. X can\'t be 0. (Rather than cast this card from your hand, you may pay {X}{W}{W} and exile it with X time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)\nWhenever a time counter is removed from Benalish Commander while it\'s exiled, put a 1/1 white Soldier creature token onto the battlefield.').
card_mana_cost('benalish commander', ['3', 'W']).
card_cmc('benalish commander', 4).
card_layout('benalish commander', 'normal').
card_power('benalish commander', '*').
card_toughness('benalish commander', '*').

% Found in: INV
card_name('benalish emissary', 'Benalish Emissary').
card_type('benalish emissary', 'Creature — Human Wizard').
card_types('benalish emissary', ['Creature']).
card_subtypes('benalish emissary', ['Human', 'Wizard']).
card_colors('benalish emissary', ['W']).
card_text('benalish emissary', 'Kicker {1}{G} (You may pay an additional {1}{G} as you cast this spell.)\nWhen Benalish Emissary enters the battlefield, if it was kicked, destroy target land.').
card_mana_cost('benalish emissary', ['2', 'W']).
card_cmc('benalish emissary', 3).
card_layout('benalish emissary', 'normal').
card_power('benalish emissary', 1).
card_toughness('benalish emissary', 4).

% Found in: INV
card_name('benalish heralds', 'Benalish Heralds').
card_type('benalish heralds', 'Creature — Human Soldier').
card_types('benalish heralds', ['Creature']).
card_subtypes('benalish heralds', ['Human', 'Soldier']).
card_colors('benalish heralds', ['W']).
card_text('benalish heralds', '{3}{U}, {T}: Draw a card.').
card_mana_cost('benalish heralds', ['3', 'W']).
card_cmc('benalish heralds', 4).
card_layout('benalish heralds', 'normal').
card_power('benalish heralds', 2).
card_toughness('benalish heralds', 4).

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, MED
card_name('benalish hero', 'Benalish Hero').
card_type('benalish hero', 'Creature — Human Soldier').
card_types('benalish hero', ['Creature']).
card_subtypes('benalish hero', ['Human', 'Soldier']).
card_colors('benalish hero', ['W']).
card_text('benalish hero', 'Banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('benalish hero', ['W']).
card_cmc('benalish hero', 1).
card_layout('benalish hero', 'normal').
card_power('benalish hero', 1).
card_toughness('benalish hero', 1).

% Found in: WTH
card_name('benalish infantry', 'Benalish Infantry').
card_type('benalish infantry', 'Creature — Human Soldier').
card_types('benalish infantry', ['Creature']).
card_subtypes('benalish infantry', ['Human', 'Soldier']).
card_colors('benalish infantry', ['W']).
card_text('benalish infantry', 'Banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('benalish infantry', ['2', 'W']).
card_cmc('benalish infantry', 3).
card_layout('benalish infantry', 'normal').
card_power('benalish infantry', 1).
card_toughness('benalish infantry', 3).

% Found in: 10E, ATH, WTH
card_name('benalish knight', 'Benalish Knight').
card_type('benalish knight', 'Creature — Human Knight').
card_types('benalish knight', ['Creature']).
card_subtypes('benalish knight', ['Human', 'Knight']).
card_colors('benalish knight', ['W']).
card_text('benalish knight', 'Flash (You may cast this spell any time you could cast an instant.)\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_mana_cost('benalish knight', ['2', 'W']).
card_cmc('benalish knight', 3).
card_layout('benalish knight', 'normal').
card_power('benalish knight', 2).
card_toughness('benalish knight', 2).

% Found in: DDG, INV
card_name('benalish lancer', 'Benalish Lancer').
card_type('benalish lancer', 'Creature — Human Knight').
card_types('benalish lancer', ['Creature']).
card_subtypes('benalish lancer', ['Human', 'Knight']).
card_colors('benalish lancer', ['W']).
card_text('benalish lancer', 'Kicker {2}{W} (You may pay an additional {2}{W} as you cast this spell.)\nIf Benalish Lancer was kicked, it enters the battlefield with two +1/+1 counters on it and with first strike.').
card_mana_cost('benalish lancer', ['2', 'W']).
card_cmc('benalish lancer', 3).
card_layout('benalish lancer', 'normal').
card_power('benalish lancer', 2).
card_toughness('benalish lancer', 2).

% Found in: WTH
card_name('benalish missionary', 'Benalish Missionary').
card_type('benalish missionary', 'Creature — Human Cleric').
card_types('benalish missionary', ['Creature']).
card_subtypes('benalish missionary', ['Human', 'Cleric']).
card_colors('benalish missionary', ['W']).
card_text('benalish missionary', '{1}{W}, {T}: Prevent all combat damage that would be dealt by target blocked creature this turn.').
card_mana_cost('benalish missionary', ['W']).
card_cmc('benalish missionary', 1).
card_layout('benalish missionary', 'normal').
card_power('benalish missionary', 1).
card_toughness('benalish missionary', 1).

% Found in: INV, VMA
card_name('benalish trapper', 'Benalish Trapper').
card_type('benalish trapper', 'Creature — Human Soldier').
card_types('benalish trapper', ['Creature']).
card_subtypes('benalish trapper', ['Human', 'Soldier']).
card_colors('benalish trapper', ['W']).
card_text('benalish trapper', '{W}, {T}: Tap target creature.').
card_mana_cost('benalish trapper', ['1', 'W']).
card_cmc('benalish trapper', 2).
card_layout('benalish trapper', 'normal').
card_power('benalish trapper', 1).
card_toughness('benalish trapper', 2).

% Found in: M12
card_name('benalish veteran', 'Benalish Veteran').
card_type('benalish veteran', 'Creature — Human Soldier').
card_types('benalish veteran', ['Creature']).
card_subtypes('benalish veteran', ['Human', 'Soldier']).
card_colors('benalish veteran', ['W']).
card_text('benalish veteran', 'Whenever Benalish Veteran attacks, it gets +1/+1 until end of turn.').
card_mana_cost('benalish veteran', ['2', 'W']).
card_cmc('benalish veteran', 3).
card_layout('benalish veteran', 'normal').
card_power('benalish veteran', 2).
card_toughness('benalish veteran', 2).

% Found in: INV
card_name('bend or break', 'Bend or Break').
card_type('bend or break', 'Sorcery').
card_types('bend or break', ['Sorcery']).
card_subtypes('bend or break', []).
card_colors('bend or break', ['R']).
card_text('bend or break', 'Each player separates all nontoken lands he or she controls into two piles. For each player, one of his or her piles is chosen by one of his or her opponents of his or her choice. Destroy all lands in the chosen piles. Tap all lands in the other piles.').
card_mana_cost('bend or break', ['3', 'R']).
card_cmc('bend or break', 4).
card_layout('bend or break', 'normal').

% Found in: GPT
card_name('benediction of moons', 'Benediction of Moons').
card_type('benediction of moons', 'Sorcery').
card_types('benediction of moons', ['Sorcery']).
card_subtypes('benediction of moons', []).
card_colors('benediction of moons', ['W']).
card_text('benediction of moons', 'You gain 1 life for each player.\nHaunt (When this spell card is put into a graveyard after resolving, exile it haunting target creature.)\nWhen the creature Benediction of Moons haunts dies, you gain 1 life for each player.').
card_mana_cost('benediction of moons', ['W']).
card_cmc('benediction of moons', 1).
card_layout('benediction of moons', 'normal').

% Found in: RAV
card_name('benevolent ancestor', 'Benevolent Ancestor').
card_type('benevolent ancestor', 'Creature — Spirit').
card_types('benevolent ancestor', ['Creature']).
card_subtypes('benevolent ancestor', ['Spirit']).
card_colors('benevolent ancestor', ['W']).
card_text('benevolent ancestor', 'Defender (This creature can\'t attack.)\n{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_mana_cost('benevolent ancestor', ['2', 'W']).
card_cmc('benevolent ancestor', 3).
card_layout('benevolent ancestor', 'normal').
card_power('benevolent ancestor', 0).
card_toughness('benevolent ancestor', 4).

% Found in: JUD, VMA
card_name('benevolent bodyguard', 'Benevolent Bodyguard').
card_type('benevolent bodyguard', 'Creature — Human Cleric').
card_types('benevolent bodyguard', ['Creature']).
card_subtypes('benevolent bodyguard', ['Human', 'Cleric']).
card_colors('benevolent bodyguard', ['W']).
card_text('benevolent bodyguard', 'Sacrifice Benevolent Bodyguard: Target creature you control gains protection from the color of your choice until end of turn.').
card_mana_cost('benevolent bodyguard', ['W']).
card_cmc('benevolent bodyguard', 1).
card_layout('benevolent bodyguard', 'normal').
card_power('benevolent bodyguard', 1).
card_toughness('benevolent bodyguard', 1).

% Found in: C14
card_name('benevolent offering', 'Benevolent Offering').
card_type('benevolent offering', 'Instant').
card_types('benevolent offering', ['Instant']).
card_subtypes('benevolent offering', []).
card_colors('benevolent offering', ['W']).
card_text('benevolent offering', 'Choose an opponent. You and that player each put three 1/1 white Spirit creature tokens with flying onto the battlefield.\nChoose an opponent. You gain 2 life for each creature you control and that player gains 2 life for each creature he or she controls.').
card_mana_cost('benevolent offering', ['3', 'W']).
card_cmc('benevolent offering', 4).
card_layout('benevolent offering', 'normal').

% Found in: MIR
card_name('benevolent unicorn', 'Benevolent Unicorn').
card_type('benevolent unicorn', 'Creature — Unicorn').
card_types('benevolent unicorn', ['Creature']).
card_subtypes('benevolent unicorn', ['Unicorn']).
card_colors('benevolent unicorn', ['W']).
card_text('benevolent unicorn', 'If a spell would deal damage to a creature or player, it deals that much damage minus 1 to that creature or player instead.').
card_mana_cost('benevolent unicorn', ['1', 'W']).
card_cmc('benevolent unicorn', 2).
card_layout('benevolent unicorn', 'normal').
card_power('benevolent unicorn', 1).
card_toughness('benevolent unicorn', 2).

% Found in: 7ED, TMP
card_name('benthic behemoth', 'Benthic Behemoth').
card_type('benthic behemoth', 'Creature — Serpent').
card_types('benthic behemoth', ['Creature']).
card_subtypes('benthic behemoth', ['Serpent']).
card_colors('benthic behemoth', ['U']).
card_text('benthic behemoth', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('benthic behemoth', ['5', 'U', 'U', 'U']).
card_cmc('benthic behemoth', 8).
card_layout('benthic behemoth', 'normal').
card_power('benthic behemoth', 7).
card_toughness('benthic behemoth', 6).

% Found in: MIR
card_name('benthic djinn', 'Benthic Djinn').
card_type('benthic djinn', 'Creature — Djinn').
card_types('benthic djinn', ['Creature']).
card_subtypes('benthic djinn', ['Djinn']).
card_colors('benthic djinn', ['U', 'B']).
card_text('benthic djinn', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nAt the beginning of your upkeep, you lose 2 life.').
card_mana_cost('benthic djinn', ['2', 'U', 'B']).
card_cmc('benthic djinn', 4).
card_layout('benthic djinn', 'normal').
card_power('benthic djinn', 5).
card_toughness('benthic djinn', 3).
card_reserved('benthic djinn').

% Found in: ALL, ME3
card_name('benthic explorers', 'Benthic Explorers').
card_type('benthic explorers', 'Creature — Merfolk Scout').
card_types('benthic explorers', ['Creature']).
card_subtypes('benthic explorers', ['Merfolk', 'Scout']).
card_colors('benthic explorers', ['U']).
card_text('benthic explorers', '{T}, Untap a tapped land an opponent controls: Add one mana of any type that land could produce to your mana pool.').
card_mana_cost('benthic explorers', ['3', 'U']).
card_cmc('benthic explorers', 4).
card_layout('benthic explorers', 'normal').
card_power('benthic explorers', 2).
card_toughness('benthic explorers', 4).

% Found in: THS
card_name('benthic giant', 'Benthic Giant').
card_type('benthic giant', 'Creature — Giant').
card_types('benthic giant', ['Creature']).
card_subtypes('benthic giant', ['Giant']).
card_colors('benthic giant', ['U']).
card_text('benthic giant', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_mana_cost('benthic giant', ['5', 'U']).
card_cmc('benthic giant', 6).
card_layout('benthic giant', 'normal').
card_power('benthic giant', 4).
card_toughness('benthic giant', 5).

% Found in: BFZ
card_name('benthic infiltrator', 'Benthic Infiltrator').
card_type('benthic infiltrator', 'Creature — Eldrazi Drone').
card_types('benthic infiltrator', ['Creature']).
card_subtypes('benthic infiltrator', ['Eldrazi', 'Drone']).
card_colors('benthic infiltrator', []).
card_text('benthic infiltrator', 'Devoid (This card has no color.)\nIngest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.)\nBenthic Infiltrator can\'t be blocked.').
card_mana_cost('benthic infiltrator', ['2', 'U']).
card_cmc('benthic infiltrator', 3).
card_layout('benthic infiltrator', 'normal').
card_power('benthic infiltrator', 1).
card_toughness('benthic infiltrator', 4).

% Found in: LRW
card_name('benthicore', 'Benthicore').
card_type('benthicore', 'Creature — Elemental').
card_types('benthicore', ['Creature']).
card_subtypes('benthicore', ['Elemental']).
card_colors('benthicore', ['U']).
card_text('benthicore', 'When Benthicore enters the battlefield, put two 1/1 blue Merfolk Wizard creature tokens onto the battlefield.\nTap two untapped Merfolk you control: Untap Benthicore. It gains shroud until end of turn. (It can\'t be the target of spells or abilities.)').
card_mana_cost('benthicore', ['6', 'U']).
card_cmc('benthicore', 7).
card_layout('benthicore', 'normal').
card_power('benthicore', 5).
card_toughness('benthicore', 5).

% Found in: EXO
card_name('bequeathal', 'Bequeathal').
card_type('bequeathal', 'Enchantment — Aura').
card_types('bequeathal', ['Enchantment']).
card_subtypes('bequeathal', ['Aura']).
card_colors('bequeathal', ['G']).
card_text('bequeathal', 'Enchant creature\nWhen enchanted creature dies, you draw two cards.').
card_mana_cost('bequeathal', ['G']).
card_cmc('bequeathal', 1).
card_layout('bequeathal', 'normal').

% Found in: 7ED, USG
card_name('bereavement', 'Bereavement').
card_type('bereavement', 'Enchantment').
card_types('bereavement', ['Enchantment']).
card_subtypes('bereavement', []).
card_colors('bereavement', ['B']).
card_text('bereavement', 'Whenever a green creature dies, its controller discards a card.').
card_mana_cost('bereavement', ['1', 'B']).
card_cmc('bereavement', 2).
card_layout('bereavement', 'normal').

% Found in: 2ED, CED, CEI, LEA, LEB, MED, V09, VMA
card_name('berserk', 'Berserk').
card_type('berserk', 'Instant').
card_types('berserk', ['Instant']).
card_subtypes('berserk', []).
card_colors('berserk', ['G']).
card_text('berserk', 'Cast Berserk only before the combat damage step.\nTarget creature gains trample and gets +X/+0 until end of turn, where X is its power. At the beginning of the next end step, destroy that creature if it attacked this turn.').
card_mana_cost('berserk', ['G']).
card_cmc('berserk', 1).
card_layout('berserk', 'normal').

% Found in: LGN
card_name('berserk murlodont', 'Berserk Murlodont').
card_type('berserk murlodont', 'Creature — Beast').
card_types('berserk murlodont', ['Creature']).
card_subtypes('berserk murlodont', ['Beast']).
card_colors('berserk murlodont', ['G']).
card_text('berserk murlodont', 'Whenever a Beast becomes blocked, it gets +1/+1 until end of turn for each creature blocking it.').
card_mana_cost('berserk murlodont', ['4', 'G']).
card_cmc('berserk murlodont', 5).
card_layout('berserk murlodont', 'normal').
card_power('berserk murlodont', 3).
card_toughness('berserk murlodont', 3).

% Found in: M10, M11
card_name('berserkers of blood ridge', 'Berserkers of Blood Ridge').
card_type('berserkers of blood ridge', 'Creature — Human Berserker').
card_types('berserkers of blood ridge', ['Creature']).
card_subtypes('berserkers of blood ridge', ['Human', 'Berserker']).
card_colors('berserkers of blood ridge', ['R']).
card_text('berserkers of blood ridge', 'Berserkers of Blood Ridge attacks each turn if able.').
card_mana_cost('berserkers of blood ridge', ['4', 'R']).
card_cmc('berserkers of blood ridge', 5).
card_layout('berserkers of blood ridge', 'normal').
card_power('berserkers of blood ridge', 4).
card_toughness('berserkers of blood ridge', 4).

% Found in: DTK
card_name('berserkers\' onslaught', 'Berserkers\' Onslaught').
card_type('berserkers\' onslaught', 'Enchantment').
card_types('berserkers\' onslaught', ['Enchantment']).
card_subtypes('berserkers\' onslaught', []).
card_colors('berserkers\' onslaught', ['R']).
card_text('berserkers\' onslaught', 'Attacking creatures you control have double strike.').
card_mana_cost('berserkers\' onslaught', ['3', 'R', 'R']).
card_cmc('berserkers\' onslaught', 5).
card_layout('berserkers\' onslaught', 'normal').

% Found in: HOP, SHM
card_name('beseech the queen', 'Beseech the Queen').
card_type('beseech the queen', 'Sorcery').
card_types('beseech the queen', ['Sorcery']).
card_subtypes('beseech the queen', []).
card_colors('beseech the queen', ['B']).
card_text('beseech the queen', '({2/B} can be paid with any two mana or with {B}. This card\'s converted mana cost is 6.)\nSearch your library for a card with converted mana cost less than or equal to the number of lands you control, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('beseech the queen', ['2/B', '2/B', '2/B']).
card_cmc('beseech the queen', 6).
card_layout('beseech the queen', 'normal').

% Found in: ALL, MED
card_name('bestial fury', 'Bestial Fury').
card_type('bestial fury', 'Enchantment — Aura').
card_types('bestial fury', ['Enchantment']).
card_subtypes('bestial fury', ['Aura']).
card_colors('bestial fury', ['R']).
card_text('bestial fury', 'Enchant creature\nWhen Bestial Fury enters the battlefield, draw a card at the beginning of the next turn\'s upkeep.\nWhenever enchanted creature becomes blocked, it gets +4/+0 and gains trample until end of turn.').
card_mana_cost('bestial fury', ['2', 'R']).
card_cmc('bestial fury', 3).
card_layout('bestial fury', 'normal').

% Found in: CMD, MM2, WWK
card_name('bestial menace', 'Bestial Menace').
card_type('bestial menace', 'Sorcery').
card_types('bestial menace', ['Sorcery']).
card_subtypes('bestial menace', []).
card_colors('bestial menace', ['G']).
card_text('bestial menace', 'Put a 1/1 green Snake creature token, a 2/2 green Wolf creature token, and a 3/3 green Elephant creature token onto the battlefield.').
card_mana_cost('bestial menace', ['3', 'G', 'G']).
card_cmc('bestial menace', 5).
card_layout('bestial menace', 'normal').

% Found in: VIS
card_name('betrayal', 'Betrayal').
card_type('betrayal', 'Enchantment — Aura').
card_types('betrayal', ['Enchantment']).
card_subtypes('betrayal', ['Aura']).
card_colors('betrayal', ['U']).
card_text('betrayal', 'Enchant creature an opponent controls\nWhenever enchanted creature becomes tapped, you draw a card.').
card_mana_cost('betrayal', ['U']).
card_cmc('betrayal', 1).
card_layout('betrayal', 'normal').

% Found in: MRD
card_name('betrayal of flesh', 'Betrayal of Flesh').
card_type('betrayal of flesh', 'Instant').
card_types('betrayal of flesh', ['Instant']).
card_subtypes('betrayal of flesh', []).
card_colors('betrayal of flesh', ['B']).
card_text('betrayal of flesh', 'Choose one —\n• Destroy target creature.\n• Return target creature card from your graveyard to the battlefield.\nEntwine—Sacrifice three lands. (Choose both if you pay the entwine cost.)').
card_mana_cost('betrayal of flesh', ['5', 'B']).
card_cmc('betrayal of flesh', 6).
card_layout('betrayal of flesh', 'normal').

% Found in: WTH
card_name('betrothed of fire', 'Betrothed of Fire').
card_type('betrothed of fire', 'Enchantment — Aura').
card_types('betrothed of fire', ['Enchantment']).
card_subtypes('betrothed of fire', ['Aura']).
card_colors('betrothed of fire', ['R']).
card_text('betrothed of fire', 'Enchant creature\nSacrifice an untapped creature: Enchanted creature gets +2/+0 until end of turn.\nSacrifice enchanted creature: Creatures you control get +2/+0 until end of turn.').
card_mana_cost('betrothed of fire', ['1', 'R']).
card_cmc('betrothed of fire', 2).
card_layout('betrothed of fire', 'normal').

% Found in: TSP
card_name('bewilder', 'Bewilder').
card_type('bewilder', 'Instant').
card_types('bewilder', ['Instant']).
card_subtypes('bewilder', []).
card_colors('bewilder', ['U']).
card_text('bewilder', 'Target creature gets -3/-0 until end of turn.\nDraw a card.').
card_mana_cost('bewilder', ['2', 'U']).
card_cmc('bewilder', 3).
card_layout('bewilder', 'normal').

% Found in: THS, pLPA
card_name('bident of thassa', 'Bident of Thassa').
card_type('bident of thassa', 'Legendary Enchantment Artifact').
card_types('bident of thassa', ['Enchantment', 'Artifact']).
card_subtypes('bident of thassa', []).
card_supertypes('bident of thassa', ['Legendary']).
card_colors('bident of thassa', ['U']).
card_text('bident of thassa', 'Whenever a creature you control deals combat damage to a player, you may draw a card.\n{1}{U}, {T}: Creatures your opponents control attack this turn if able.').
card_mana_cost('bident of thassa', ['2', 'U', 'U']).
card_cmc('bident of thassa', 4).
card_layout('bident of thassa', 'normal').

% Found in: MMQ
card_name('bifurcate', 'Bifurcate').
card_type('bifurcate', 'Sorcery').
card_types('bifurcate', ['Sorcery']).
card_subtypes('bifurcate', []).
card_colors('bifurcate', ['G']).
card_text('bifurcate', 'Search your library for a permanent card with the same name as target nontoken creature and put that card onto the battlefield. Then shuffle your library.').
card_mana_cost('bifurcate', ['3', 'G']).
card_cmc('bifurcate', 4).
card_layout('bifurcate', 'normal').

% Found in: PLC
card_name('big game hunter', 'Big Game Hunter').
card_type('big game hunter', 'Creature — Human Rebel Assassin').
card_types('big game hunter', ['Creature']).
card_subtypes('big game hunter', ['Human', 'Rebel', 'Assassin']).
card_colors('big game hunter', ['B']).
card_text('big game hunter', 'When Big Game Hunter enters the battlefield, destroy target creature with power 4 or greater. It can\'t be regenerated.\nMadness {B} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('big game hunter', ['1', 'B', 'B']).
card_cmc('big game hunter', 3).
card_layout('big game hunter', 'normal').
card_power('big game hunter', 1).
card_toughness('big game hunter', 1).

% Found in: BNG, pFNM
card_name('bile blight', 'Bile Blight').
card_type('bile blight', 'Instant').
card_types('bile blight', ['Instant']).
card_subtypes('bile blight', []).
card_colors('bile blight', ['B']).
card_text('bile blight', 'Target creature and all other creatures with the same name as that creature get -3/-3 until end of turn.').
card_mana_cost('bile blight', ['B', 'B']).
card_cmc('bile blight', 2).
card_layout('bile blight', 'normal').

% Found in: BOK
card_name('bile urchin', 'Bile Urchin').
card_type('bile urchin', 'Creature — Spirit').
card_types('bile urchin', ['Creature']).
card_subtypes('bile urchin', ['Spirit']).
card_colors('bile urchin', ['B']).
card_text('bile urchin', 'Sacrifice Bile Urchin: Target player loses 1 life.').
card_mana_cost('bile urchin', ['B']).
card_cmc('bile urchin', 1).
card_layout('bile urchin', 'normal').
card_power('bile urchin', 1).
card_toughness('bile urchin', 1).

% Found in: INV
card_name('bind', 'Bind').
card_type('bind', 'Instant').
card_types('bind', ['Instant']).
card_subtypes('bind', []).
card_colors('bind', ['G']).
card_text('bind', 'Counter target activated ability. (Mana abilities can\'t be targeted.)\nDraw a card.').
card_mana_cost('bind', ['1', 'G']).
card_cmc('bind', 2).
card_layout('bind', 'normal').

% Found in: MIR
card_name('binding agony', 'Binding Agony').
card_type('binding agony', 'Enchantment — Aura').
card_types('binding agony', ['Enchantment']).
card_subtypes('binding agony', ['Aura']).
card_colors('binding agony', ['B']).
card_text('binding agony', 'Enchant creature\nWhenever enchanted creature is dealt damage, Binding Agony deals that much damage to that creature\'s controller.').
card_mana_cost('binding agony', ['1', 'B']).
card_cmc('binding agony', 2).
card_layout('binding agony', 'normal').

% Found in: 5ED, CST, ICE, ME2
card_name('binding grasp', 'Binding Grasp').
card_type('binding grasp', 'Enchantment — Aura').
card_types('binding grasp', ['Enchantment']).
card_subtypes('binding grasp', ['Aura']).
card_colors('binding grasp', ['U']).
card_text('binding grasp', 'Enchant creature\nAt the beginning of your upkeep, sacrifice Binding Grasp unless you pay {1}{U}.\nYou control enchanted creature.\nEnchanted creature gets +0/+1.').
card_mana_cost('binding grasp', ['3', 'U']).
card_cmc('binding grasp', 4).
card_layout('binding grasp', 'normal').

% Found in: DIS
card_name('biomantic mastery', 'Biomantic Mastery').
card_type('biomantic mastery', 'Sorcery').
card_types('biomantic mastery', ['Sorcery']).
card_subtypes('biomantic mastery', []).
card_colors('biomantic mastery', ['U', 'G']).
card_text('biomantic mastery', '({G/U} can be paid with either {G} or {U}.)\nDraw a card for each creature target player controls, then draw a card for each creature another target player controls.').
card_mana_cost('biomantic mastery', ['4', 'G/U', 'G/U', 'G/U']).
card_cmc('biomantic mastery', 7).
card_layout('biomantic mastery', 'normal').

% Found in: GTC
card_name('biomass mutation', 'Biomass Mutation').
card_type('biomass mutation', 'Instant').
card_types('biomass mutation', ['Instant']).
card_subtypes('biomass mutation', []).
card_colors('biomass mutation', ['U', 'G']).
card_text('biomass mutation', 'Creatures you control have base power and toughness X/X until end of turn.').
card_mana_cost('biomass mutation', ['X', 'G/U', 'G/U']).
card_cmc('biomass mutation', 2).
card_layout('biomass mutation', 'normal').

% Found in: GPT
card_name('bioplasm', 'Bioplasm').
card_type('bioplasm', 'Creature — Ooze').
card_types('bioplasm', ['Creature']).
card_subtypes('bioplasm', ['Ooze']).
card_colors('bioplasm', ['G']).
card_text('bioplasm', 'Whenever Bioplasm attacks, exile the top card of your library. If it\'s a creature card, Bioplasm gets +X/+Y until end of turn, where X is the exiled creature card\'s power and Y is its toughness.').
card_mana_cost('bioplasm', ['3', 'G', 'G']).
card_cmc('bioplasm', 5).
card_layout('bioplasm', 'normal').
card_power('bioplasm', 4).
card_toughness('bioplasm', 4).

% Found in: 9ED, ONS
card_name('biorhythm', 'Biorhythm').
card_type('biorhythm', 'Sorcery').
card_types('biorhythm', ['Sorcery']).
card_subtypes('biorhythm', []).
card_colors('biorhythm', ['G']).
card_text('biorhythm', 'Each player\'s life total becomes the number of creatures he or she controls.').
card_mana_cost('biorhythm', ['6', 'G', 'G']).
card_cmc('biorhythm', 8).
card_layout('biorhythm', 'normal').

% Found in: GTC
card_name('bioshift', 'Bioshift').
card_type('bioshift', 'Instant').
card_types('bioshift', ['Instant']).
card_subtypes('bioshift', []).
card_colors('bioshift', ['U', 'G']).
card_text('bioshift', 'Move any number of +1/+1 counters from target creature onto another target creature with the same controller.').
card_mana_cost('bioshift', ['G/U']).
card_cmc('bioshift', 1).
card_layout('bioshift', 'normal').

% Found in: GTC
card_name('biovisionary', 'Biovisionary').
card_type('biovisionary', 'Creature — Human Wizard').
card_types('biovisionary', ['Creature']).
card_subtypes('biovisionary', ['Human', 'Wizard']).
card_colors('biovisionary', ['U', 'G']).
card_text('biovisionary', 'At the beginning of the end step, if you control four or more creatures named Biovisionary, you win the game.').
card_mana_cost('biovisionary', ['1', 'G', 'U']).
card_cmc('biovisionary', 3).
card_layout('biovisionary', 'normal').
card_power('biovisionary', 2).
card_toughness('biovisionary', 3).

% Found in: ONS
card_name('birchlore rangers', 'Birchlore Rangers').
card_type('birchlore rangers', 'Creature — Elf Druid').
card_types('birchlore rangers', ['Creature']).
card_subtypes('birchlore rangers', ['Elf', 'Druid']).
card_colors('birchlore rangers', ['G']).
card_text('birchlore rangers', 'Tap two untapped Elves you control: Add one mana of any color to your mana pool.\nMorph {G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('birchlore rangers', ['G']).
card_cmc('birchlore rangers', 1).
card_layout('birchlore rangers', 'normal').
card_power('birchlore rangers', 1).
card_toughness('birchlore rangers', 1).

% Found in: 4ED, 5ED, ARN, ME4
card_name('bird maiden', 'Bird Maiden').
card_type('bird maiden', 'Creature — Human Bird').
card_types('bird maiden', ['Creature']).
card_subtypes('bird maiden', ['Human', 'Bird']).
card_colors('bird maiden', ['R']).
card_text('bird maiden', 'Flying').
card_mana_cost('bird maiden', ['2', 'R']).
card_cmc('bird maiden', 3).
card_layout('bird maiden', 'normal').
card_power('bird maiden', 1).
card_toughness('bird maiden', 2).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, CED, CEI, LEA, LEB, M10, M11, M12, RAV, pMEI
card_name('birds of paradise', 'Birds of Paradise').
card_type('birds of paradise', 'Creature — Bird').
card_types('birds of paradise', ['Creature']).
card_subtypes('birds of paradise', ['Bird']).
card_colors('birds of paradise', ['G']).
card_text('birds of paradise', 'Flying\n{T}: Add one mana of any color to your mana pool.').
card_mana_cost('birds of paradise', ['G']).
card_cmc('birds of paradise', 1).
card_layout('birds of paradise', 'normal').
card_power('birds of paradise', 0).
card_toughness('birds of paradise', 1).

% Found in: VAN
card_name('birds of paradise avatar', 'Birds of Paradise Avatar').
card_type('birds of paradise avatar', 'Vanguard').
card_types('birds of paradise avatar', ['Vanguard']).
card_subtypes('birds of paradise avatar', []).
card_colors('birds of paradise avatar', []).
card_text('birds of paradise avatar', 'Lands you control have \"{T}: Add one mana of any color to your mana pool.\"').
card_layout('birds of paradise avatar', 'vanguard').

% Found in: NPH
card_name('birthing pod', 'Birthing Pod').
card_type('birthing pod', 'Artifact').
card_types('birthing pod', ['Artifact']).
card_subtypes('birthing pod', []).
card_colors('birthing pod', ['G']).
card_text('birthing pod', '({G/P} can be paid with either {G} or 2 life.)\n{1}{G/P}, {T}, Sacrifice a creature: Search your library for a creature card with converted mana cost equal to 1 plus the sacrificed creature\'s converted mana cost, put that card onto the battlefield, then shuffle your library. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('birthing pod', ['3', 'G/P']).
card_cmc('birthing pod', 4).
card_layout('birthing pod', 'normal').

% Found in: CNS
card_name('bite of the black rose', 'Bite of the Black Rose').
card_type('bite of the black rose', 'Sorcery').
card_types('bite of the black rose', ['Sorcery']).
card_subtypes('bite of the black rose', []).
card_colors('bite of the black rose', ['B']).
card_text('bite of the black rose', 'Will of the council — Starting with you, each player votes for sickness or psychosis. If sickness gets more votes, creatures your opponents control get -2/-2 until end of turn. If psychosis gets more votes or the vote is tied, each opponent discards two cards.').
card_mana_cost('bite of the black rose', ['3', 'B']).
card_cmc('bite of the black rose', 4).
card_layout('bite of the black rose', 'normal').

% Found in: SHM
card_name('biting tether', 'Biting Tether').
card_type('biting tether', 'Enchantment — Aura').
card_types('biting tether', ['Enchantment']).
card_subtypes('biting tether', ['Aura']).
card_colors('biting tether', ['U']).
card_text('biting tether', 'Enchant creature\nYou control enchanted creature.\nAt the beginning of your upkeep, put a -1/-1 counter on enchanted creature.').
card_mana_cost('biting tether', ['4', 'U']).
card_cmc('biting tether', 5).
card_layout('biting tether', 'normal').

% Found in: C14
card_name('bitter feud', 'Bitter Feud').
card_type('bitter feud', 'Enchantment').
card_types('bitter feud', ['Enchantment']).
card_subtypes('bitter feud', []).
card_colors('bitter feud', ['R']).
card_text('bitter feud', 'As Bitter Feud enters the battlefield, choose two players.\nIf a source controlled by one of the chosen players would deal damage to the other chosen player or a permanent that player controls, that source deals double that damage to that player or permanent instead.').
card_mana_cost('bitter feud', ['4', 'R']).
card_cmc('bitter feud', 5).
card_layout('bitter feud', 'normal').

% Found in: FUT
card_name('bitter ordeal', 'Bitter Ordeal').
card_type('bitter ordeal', 'Sorcery').
card_types('bitter ordeal', ['Sorcery']).
card_subtypes('bitter ordeal', []).
card_colors('bitter ordeal', ['B']).
card_text('bitter ordeal', 'Search target player\'s library for a card and exile it. Then that player shuffles his or her library.\nGravestorm (When you cast this spell, copy it for each permanent put into a graveyard this turn. You may choose new targets for the copies.)').
card_mana_cost('bitter ordeal', ['2', 'B']).
card_cmc('bitter ordeal', 3).
card_layout('bitter ordeal', 'normal').

% Found in: KTK
card_name('bitter revelation', 'Bitter Revelation').
card_type('bitter revelation', 'Sorcery').
card_types('bitter revelation', ['Sorcery']).
card_subtypes('bitter revelation', []).
card_colors('bitter revelation', ['B']).
card_text('bitter revelation', 'Look at the top four cards of your library. Put two of them into your hand and the rest into your graveyard. You lose 2 life.').
card_mana_cost('bitter revelation', ['3', 'B']).
card_cmc('bitter revelation', 4).
card_layout('bitter revelation', 'normal').

% Found in: MM2, MOR, pJGP
card_name('bitterblossom', 'Bitterblossom').
card_type('bitterblossom', 'Tribal Enchantment — Faerie').
card_types('bitterblossom', ['Tribal', 'Enchantment']).
card_subtypes('bitterblossom', ['Faerie']).
card_colors('bitterblossom', ['B']).
card_text('bitterblossom', 'At the beginning of your upkeep, you lose 1 life and put a 1/1 black Faerie Rogue creature token with flying onto the battlefield.').
card_mana_cost('bitterblossom', ['1', 'B']).
card_cmc('bitterblossom', 2).
card_layout('bitterblossom', 'normal').

% Found in: ISD
card_name('bitterheart witch', 'Bitterheart Witch').
card_type('bitterheart witch', 'Creature — Human Shaman').
card_types('bitterheart witch', ['Creature']).
card_subtypes('bitterheart witch', ['Human', 'Shaman']).
card_colors('bitterheart witch', ['B']).
card_text('bitterheart witch', 'Deathtouch\nWhen Bitterheart Witch dies, you may search your library for a Curse card, put it onto the battlefield attached to target player, then shuffle your library.').
card_mana_cost('bitterheart witch', ['4', 'B']).
card_cmc('bitterheart witch', 5).
card_layout('bitterheart witch', 'normal').
card_power('bitterheart witch', 1).
card_toughness('bitterheart witch', 2).

% Found in: ARB, ARC, PC2, pMPR
card_name('bituminous blast', 'Bituminous Blast').
card_type('bituminous blast', 'Instant').
card_types('bituminous blast', ['Instant']).
card_subtypes('bituminous blast', []).
card_colors('bituminous blast', ['B', 'R']).
card_text('bituminous blast', 'Cascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)\nBituminous Blast deals 4 damage to target creature.').
card_mana_cost('bituminous blast', ['3', 'B', 'R']).
card_cmc('bituminous blast', 5).
card_layout('bituminous blast', 'normal').

% Found in: HML
card_name('black carriage', 'Black Carriage').
card_type('black carriage', 'Creature — Horse').
card_types('black carriage', ['Creature']).
card_subtypes('black carriage', ['Horse']).
card_colors('black carriage', ['B']).
card_text('black carriage', 'Trample\nBlack Carriage doesn\'t untap during your untap step.\nSacrifice a creature: Untap Black Carriage. Activate this ability only during your upkeep.').
card_mana_cost('black carriage', ['3', 'B', 'B']).
card_cmc('black carriage', 5).
card_layout('black carriage', 'normal').
card_power('black carriage', 4).
card_toughness('black carriage', 4).
card_reserved('black carriage').

% Found in: DKA, M15
card_name('black cat', 'Black Cat').
card_type('black cat', 'Creature — Zombie Cat').
card_types('black cat', ['Creature']).
card_subtypes('black cat', ['Zombie', 'Cat']).
card_colors('black cat', ['B']).
card_text('black cat', 'When Black Cat dies, target opponent discards a card at random.').
card_mana_cost('black cat', ['1', 'B']).
card_cmc('black cat', 2).
card_layout('black cat', 'normal').
card_power('black cat', 1).
card_toughness('black cat', 1).

% Found in: 2ED, 3ED, 4ED, 5ED, ATH, CED, CEI, LEA, LEB, M10, M11, ME4, MED, pFNM
card_name('black knight', 'Black Knight').
card_type('black knight', 'Creature — Human Knight').
card_types('black knight', ['Creature']).
card_subtypes('black knight', ['Human', 'Knight']).
card_colors('black knight', ['B']).
card_text('black knight', 'First strike (This creature deals combat damage before creatures without first strike.)\nProtection from white (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything white.)').
card_mana_cost('black knight', ['B', 'B']).
card_cmc('black knight', 2).
card_layout('black knight', 'normal').
card_power('black knight', 2).
card_toughness('black knight', 2).

% Found in: 2ED, CED, CEI, LEA, LEB, VMA
card_name('black lotus', 'Black Lotus').
card_type('black lotus', 'Artifact').
card_types('black lotus', ['Artifact']).
card_subtypes('black lotus', []).
card_colors('black lotus', []).
card_text('black lotus', '{T}, Sacrifice Black Lotus: Add three mana of any one color to your mana pool.').
card_mana_cost('black lotus', ['0']).
card_cmc('black lotus', 0).
card_layout('black lotus', 'normal').
card_reserved('black lotus').

% Found in: 4ED, LEG
card_name('black mana battery', 'Black Mana Battery').
card_type('black mana battery', 'Artifact').
card_types('black mana battery', ['Artifact']).
card_subtypes('black mana battery', []).
card_colors('black mana battery', []).
card_text('black mana battery', '{2}, {T}: Put a charge counter on Black Mana Battery.\n{T}, Remove any number of charge counters from Black Mana Battery: Add {B} to your mana pool, then add an additional {B} to your mana pool for each charge counter removed this way.').
card_mana_cost('black mana battery', ['4']).
card_cmc('black mana battery', 4).
card_layout('black mana battery', 'normal').

% Found in: MMQ
card_name('black market', 'Black Market').
card_type('black market', 'Enchantment').
card_types('black market', ['Enchantment']).
card_subtypes('black market', []).
card_colors('black market', ['B']).
card_text('black market', 'Whenever a creature dies, put a charge counter on Black Market.\nAt the beginning of your precombat main phase, add {B} to your mana pool for each charge counter on Black Market.').
card_mana_cost('black market', ['3', 'B', 'B']).
card_cmc('black market', 5).
card_layout('black market', 'normal').

% Found in: BNG
card_name('black oak of odunos', 'Black Oak of Odunos').
card_type('black oak of odunos', 'Creature — Zombie Treefolk').
card_types('black oak of odunos', ['Creature']).
card_subtypes('black oak of odunos', ['Zombie', 'Treefolk']).
card_colors('black oak of odunos', ['B']).
card_text('black oak of odunos', 'Defender\n{B}, Tap another untapped creature you control: Black Oak of Odunos gets +1/+1 until end of turn.').
card_mana_cost('black oak of odunos', ['2', 'B']).
card_cmc('black oak of odunos', 3).
card_layout('black oak of odunos', 'normal').
card_power('black oak of odunos', 0).
card_toughness('black oak of odunos', 5).

% Found in: LRW
card_name('black poplar shaman', 'Black Poplar Shaman').
card_type('black poplar shaman', 'Creature — Treefolk Shaman').
card_types('black poplar shaman', ['Creature']).
card_subtypes('black poplar shaman', ['Treefolk', 'Shaman']).
card_colors('black poplar shaman', ['B']).
card_text('black poplar shaman', '{2}{B}: Regenerate target Treefolk.').
card_mana_cost('black poplar shaman', ['2', 'B']).
card_cmc('black poplar shaman', 3).
card_layout('black poplar shaman', 'normal').
card_power('black poplar shaman', 1).
card_toughness('black poplar shaman', 3).

% Found in: ICE
card_name('black scarab', 'Black Scarab').
card_type('black scarab', 'Enchantment — Aura').
card_types('black scarab', ['Enchantment']).
card_subtypes('black scarab', ['Aura']).
card_colors('black scarab', ['W']).
card_text('black scarab', 'Enchant creature\nEnchanted creature can\'t be blocked by black creatures.\nEnchanted creature gets +2/+2 as long as an opponent controls a black permanent.').
card_mana_cost('black scarab', ['W']).
card_cmc('black scarab', 1).
card_layout('black scarab', 'normal').

% Found in: C14, MBS, pMGD
card_name('black sun\'s zenith', 'Black Sun\'s Zenith').
card_type('black sun\'s zenith', 'Sorcery').
card_types('black sun\'s zenith', ['Sorcery']).
card_subtypes('black sun\'s zenith', []).
card_colors('black sun\'s zenith', ['B']).
card_text('black sun\'s zenith', 'Put X -1/-1 counters on each creature. Shuffle Black Sun\'s Zenith into its owner\'s library.').
card_mana_cost('black sun\'s zenith', ['X', 'B', 'B']).
card_cmc('black sun\'s zenith', 2).
card_layout('black sun\'s zenith', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB, ME3, V10
card_name('black vise', 'Black Vise').
card_type('black vise', 'Artifact').
card_types('black vise', ['Artifact']).
card_subtypes('black vise', []).
card_colors('black vise', []).
card_text('black vise', 'As Black Vise enters the battlefield, choose an opponent.\nAt the beginning of the chosen player\'s upkeep, Black Vise deals X damage to that player, where X is the number of cards in his or her hand minus 4.').
card_mana_cost('black vise', ['1']).
card_cmc('black vise', 1).
card_layout('black vise', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('black ward', 'Black Ward').
card_type('black ward', 'Enchantment — Aura').
card_types('black ward', ['Enchantment']).
card_subtypes('black ward', ['Aura']).
card_colors('black ward', ['W']).
card_text('black ward', 'Enchant creature\nEnchanted creature has protection from black. This effect doesn\'t remove Black Ward.').
card_mana_cost('black ward', ['W']).
card_cmc('black ward', 1).
card_layout('black ward', 'normal').

% Found in: SOM
card_name('blackcleave cliffs', 'Blackcleave Cliffs').
card_type('blackcleave cliffs', 'Land').
card_types('blackcleave cliffs', ['Land']).
card_subtypes('blackcleave cliffs', []).
card_colors('blackcleave cliffs', []).
card_text('blackcleave cliffs', 'Blackcleave Cliffs enters the battlefield tapped unless you control two or fewer other lands.\n{T}: Add {B} or {R} to your mana pool.').
card_layout('blackcleave cliffs', 'normal').

% Found in: SOM
card_name('blackcleave goblin', 'Blackcleave Goblin').
card_type('blackcleave goblin', 'Creature — Goblin Zombie').
card_types('blackcleave goblin', ['Creature']).
card_subtypes('blackcleave goblin', ['Goblin', 'Zombie']).
card_colors('blackcleave goblin', ['B']).
card_text('blackcleave goblin', 'Haste\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('blackcleave goblin', ['3', 'B']).
card_cmc('blackcleave goblin', 4).
card_layout('blackcleave goblin', 'normal').
card_power('blackcleave goblin', 2).
card_toughness('blackcleave goblin', 1).

% Found in: UGL
card_name('blacker lotus', 'Blacker Lotus').
card_type('blacker lotus', 'Artifact').
card_types('blacker lotus', ['Artifact']).
card_subtypes('blacker lotus', []).
card_colors('blacker lotus', []).
card_text('blacker lotus', '{T}: Tear Blacker Lotus into pieces. Add four mana of any one color to your mana pool. Play this ability as a mana source. Remove the pieces from the game afterwards.').
card_mana_cost('blacker lotus', ['0']).
card_cmc('blacker lotus', 0).
card_layout('blacker lotus', 'normal').

% Found in: 9ED, ONS
card_name('blackmail', 'Blackmail').
card_type('blackmail', 'Sorcery').
card_types('blackmail', ['Sorcery']).
card_subtypes('blackmail', []).
card_colors('blackmail', ['B']).
card_text('blackmail', 'Target player reveals three cards from his or her hand and you choose one of them. That player discards that card.').
card_mana_cost('blackmail', ['B']).
card_cmc('blackmail', 1).
card_layout('blackmail', 'normal').

% Found in: ZEN
card_name('blade of the bloodchief', 'Blade of the Bloodchief').
card_type('blade of the bloodchief', 'Artifact — Equipment').
card_types('blade of the bloodchief', ['Artifact']).
card_subtypes('blade of the bloodchief', ['Equipment']).
card_colors('blade of the bloodchief', []).
card_text('blade of the bloodchief', 'Whenever a creature dies, put a +1/+1 counter on equipped creature. If equipped creature is a Vampire, put two +1/+1 counters on it instead.\nEquip {1}').
card_mana_cost('blade of the bloodchief', ['1']).
card_cmc('blade of the bloodchief', 1).
card_layout('blade of the bloodchief', 'normal').

% Found in: FUT
card_name('blade of the sixth pride', 'Blade of the Sixth Pride').
card_type('blade of the sixth pride', 'Creature — Cat Rebel').
card_types('blade of the sixth pride', ['Creature']).
card_subtypes('blade of the sixth pride', ['Cat', 'Rebel']).
card_colors('blade of the sixth pride', ['W']).
card_text('blade of the sixth pride', '').
card_mana_cost('blade of the sixth pride', ['1', 'W']).
card_cmc('blade of the sixth pride', 2).
card_layout('blade of the sixth pride', 'normal').
card_power('blade of the sixth pride', 3).
card_toughness('blade of the sixth pride', 1).

% Found in: LGN
card_name('blade sliver', 'Blade Sliver').
card_type('blade sliver', 'Creature — Sliver').
card_types('blade sliver', ['Creature']).
card_subtypes('blade sliver', ['Sliver']).
card_colors('blade sliver', ['R']).
card_text('blade sliver', 'All Sliver creatures get +1/+0.').
card_mana_cost('blade sliver', ['2', 'R']).
card_cmc('blade sliver', 3).
card_layout('blade sliver', 'normal').
card_power('blade sliver', 2).
card_toughness('blade sliver', 2).

% Found in: NPH
card_name('blade splicer', 'Blade Splicer').
card_type('blade splicer', 'Creature — Human Artificer').
card_types('blade splicer', ['Creature']).
card_subtypes('blade splicer', ['Human', 'Artificer']).
card_colors('blade splicer', ['W']).
card_text('blade splicer', 'When Blade Splicer enters the battlefield, put a 3/3 colorless Golem artifact creature token onto the battlefield.\nGolem creatures you control have first strike.').
card_mana_cost('blade splicer', ['2', 'W']).
card_cmc('blade splicer', 3).
card_layout('blade splicer', 'normal').
card_power('blade splicer', 1).
card_toughness('blade splicer', 1).

% Found in: SOM
card_name('blade-tribe berserkers', 'Blade-Tribe Berserkers').
card_type('blade-tribe berserkers', 'Creature — Human Berserker').
card_types('blade-tribe berserkers', ['Creature']).
card_subtypes('blade-tribe berserkers', ['Human', 'Berserker']).
card_colors('blade-tribe berserkers', ['R']).
card_text('blade-tribe berserkers', 'Metalcraft — When Blade-Tribe Berserkers enters the battlefield, if you control three or more artifacts, Blade-Tribe Berserkers gets +3/+3 and gains haste until end of turn.').
card_mana_cost('blade-tribe berserkers', ['3', 'R']).
card_cmc('blade-tribe berserkers', 4).
card_layout('blade-tribe berserkers', 'normal').
card_power('blade-tribe berserkers', 3).
card_toughness('blade-tribe berserkers', 3).

% Found in: AVR
card_name('bladed bracers', 'Bladed Bracers').
card_type('bladed bracers', 'Artifact — Equipment').
card_types('bladed bracers', ['Artifact']).
card_subtypes('bladed bracers', ['Equipment']).
card_colors('bladed bracers', []).
card_text('bladed bracers', 'Equipped creature gets +1/+1.\nAs long as equipped creature is a Human or an Angel, it has vigilance.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('bladed bracers', ['1']).
card_cmc('bladed bracers', 1).
card_layout('bladed bracers', 'normal').

% Found in: SOM
card_name('bladed pinions', 'Bladed Pinions').
card_type('bladed pinions', 'Artifact — Equipment').
card_types('bladed pinions', ['Artifact']).
card_subtypes('bladed pinions', ['Equipment']).
card_colors('bladed pinions', []).
card_text('bladed pinions', 'Equipped creature has flying and first strike.\nEquip {2}').
card_mana_cost('bladed pinions', ['2']).
card_cmc('bladed pinions', 2).
card_layout('bladed pinions', 'normal').

% Found in: MBS
card_name('bladed sentinel', 'Bladed Sentinel').
card_type('bladed sentinel', 'Artifact Creature — Construct').
card_types('bladed sentinel', ['Artifact', 'Creature']).
card_subtypes('bladed sentinel', ['Construct']).
card_colors('bladed sentinel', []).
card_text('bladed sentinel', '{W}: Bladed Sentinel gains vigilance until end of turn.').
card_mana_cost('bladed sentinel', ['4']).
card_cmc('bladed sentinel', 4).
card_layout('bladed sentinel', 'normal').
card_power('bladed sentinel', 2).
card_toughness('bladed sentinel', 4).

% Found in: BOK
card_name('blademane baku', 'Blademane Baku').
card_type('blademane baku', 'Creature — Spirit').
card_types('blademane baku', ['Creature']).
card_subtypes('blademane baku', ['Spirit']).
card_colors('blademane baku', ['R']).
card_text('blademane baku', 'Whenever you cast a Spirit or Arcane spell, you may put a ki counter on Blademane Baku.\n{1}, Remove X ki counters from Blademane Baku: For each counter removed, Blademane Baku gets +2/+0 until end of turn.').
card_mana_cost('blademane baku', ['1', 'R']).
card_cmc('blademane baku', 2).
card_layout('blademane baku', 'normal').
card_power('blademane baku', 1).
card_toughness('blademane baku', 1).

% Found in: LRW, MM2
card_name('blades of velis vel', 'Blades of Velis Vel').
card_type('blades of velis vel', 'Tribal Instant — Shapeshifter').
card_types('blades of velis vel', ['Tribal', 'Instant']).
card_subtypes('blades of velis vel', ['Shapeshifter']).
card_colors('blades of velis vel', ['R']).
card_text('blades of velis vel', 'Changeling (This card is every creature type.)\nUp to two target creatures each get +2/+0 and gain all creature types until end of turn.').
card_mana_cost('blades of velis vel', ['1', 'R']).
card_cmc('blades of velis vel', 2).
card_layout('blades of velis vel', 'normal').

% Found in: JOU, M13, ZEN
card_name('bladetusk boar', 'Bladetusk Boar').
card_type('bladetusk boar', 'Creature — Boar').
card_types('bladetusk boar', ['Creature']).
card_subtypes('bladetusk boar', ['Boar']).
card_colors('bladetusk boar', ['R']).
card_text('bladetusk boar', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_mana_cost('bladetusk boar', ['3', 'R']).
card_cmc('bladetusk boar', 4).
card_layout('bladetusk boar', 'normal').
card_power('bladetusk boar', 3).
card_toughness('bladetusk boar', 2).

% Found in: CMD, DRB, SCG
card_name('bladewing the risen', 'Bladewing the Risen').
card_type('bladewing the risen', 'Legendary Creature — Zombie Dragon').
card_types('bladewing the risen', ['Creature']).
card_subtypes('bladewing the risen', ['Zombie', 'Dragon']).
card_supertypes('bladewing the risen', ['Legendary']).
card_colors('bladewing the risen', ['B', 'R']).
card_text('bladewing the risen', 'Flying\nWhen Bladewing the Risen enters the battlefield, you may return target Dragon permanent card from your graveyard to the battlefield.\n{B}{R}: Dragon creatures get +1/+1 until end of turn.').
card_mana_cost('bladewing the risen', ['3', 'B', 'B', 'R', 'R']).
card_cmc('bladewing the risen', 7).
card_layout('bladewing the risen', 'normal').
card_power('bladewing the risen', 4).
card_toughness('bladewing the risen', 4).

% Found in: SCG
card_name('bladewing\'s thrall', 'Bladewing\'s Thrall').
card_type('bladewing\'s thrall', 'Creature — Zombie').
card_types('bladewing\'s thrall', ['Creature']).
card_subtypes('bladewing\'s thrall', ['Zombie']).
card_colors('bladewing\'s thrall', ['B']).
card_text('bladewing\'s thrall', 'Bladewing\'s Thrall has flying as long as you control a Dragon.\nWhen a Dragon enters the battlefield, you may return Bladewing\'s Thrall from your graveyard to the battlefield.').
card_mana_cost('bladewing\'s thrall', ['2', 'B', 'B']).
card_cmc('bladewing\'s thrall', 4).
card_layout('bladewing\'s thrall', 'normal').
card_power('bladewing\'s thrall', 3).
card_toughness('bladewing\'s thrall', 3).

% Found in: 10E, 7ED, 8ED, 9ED, DPA, USG
card_name('blanchwood armor', 'Blanchwood Armor').
card_type('blanchwood armor', 'Enchantment — Aura').
card_types('blanchwood armor', ['Enchantment']).
card_subtypes('blanchwood armor', ['Aura']).
card_colors('blanchwood armor', ['G']).
card_text('blanchwood armor', 'Enchant creature (Target a creature as you cast this. This card enters the battlefield attached to that creature.)\nEnchanted creature gets +1/+1 for each Forest you control.').
card_mana_cost('blanchwood armor', ['2', 'G']).
card_cmc('blanchwood armor', 3).
card_layout('blanchwood armor', 'normal').

% Found in: USG
card_name('blanchwood treefolk', 'Blanchwood Treefolk').
card_type('blanchwood treefolk', 'Creature — Treefolk').
card_types('blanchwood treefolk', ['Creature']).
card_subtypes('blanchwood treefolk', ['Treefolk']).
card_colors('blanchwood treefolk', ['G']).
card_text('blanchwood treefolk', '').
card_mana_cost('blanchwood treefolk', ['4', 'G']).
card_cmc('blanchwood treefolk', 5).
card_layout('blanchwood treefolk', 'normal').
card_power('blanchwood treefolk', 4).
card_toughness('blanchwood treefolk', 5).

% Found in: VIS
card_name('blanket of night', 'Blanket of Night').
card_type('blanket of night', 'Enchantment').
card_types('blanket of night', ['Enchantment']).
card_subtypes('blanket of night', []).
card_colors('blanket of night', ['B']).
card_text('blanket of night', 'Each land is a Swamp in addition to its other land types.').
card_mana_cost('blanket of night', ['1', 'B', 'B']).
card_cmc('blanket of night', 3).
card_layout('blanket of night', 'normal').

% Found in: C14, ISD
card_name('blasphemous act', 'Blasphemous Act').
card_type('blasphemous act', 'Sorcery').
card_types('blasphemous act', ['Sorcery']).
card_subtypes('blasphemous act', []).
card_colors('blasphemous act', ['R']).
card_text('blasphemous act', 'Blasphemous Act costs {1} less to cast for each creature on the battlefield.\nBlasphemous Act deals 13 damage to each creature.').
card_mana_cost('blasphemous act', ['8', 'R']).
card_cmc('blasphemous act', 9).
card_layout('blasphemous act', 'normal').

% Found in: UNH
card_name('blast from the past', 'Blast from the Past').
card_type('blast from the past', 'Instant').
card_types('blast from the past', ['Instant']).
card_subtypes('blast from the past', []).
card_colors('blast from the past', ['R']).
card_text('blast from the past', 'Madness {R}, cycling {1}{R}, kicker {2}{R}, flashback {3}{R}, buyback {4}{R}\nBlast from the Past deals 2 damage to target creature or player.\nIf the kicker cost was paid, put a 1/1 red Goblin creature token into play.').
card_mana_cost('blast from the past', ['2', 'R']).
card_cmc('blast from the past', 3).
card_layout('blast from the past', 'normal').

% Found in: DGM
card_name('blast of genius', 'Blast of Genius').
card_type('blast of genius', 'Sorcery').
card_types('blast of genius', ['Sorcery']).
card_subtypes('blast of genius', []).
card_colors('blast of genius', ['U', 'R']).
card_text('blast of genius', 'Choose target creature or player. Draw three cards, then discard a card. Blast of Genius deals damage equal to the discarded card\'s converted mana cost to that creature or player.').
card_mana_cost('blast of genius', ['4', 'U', 'R']).
card_cmc('blast of genius', 6).
card_layout('blast of genius', 'normal').

% Found in: USG
card_name('blasted landscape', 'Blasted Landscape').
card_type('blasted landscape', 'Land').
card_types('blasted landscape', ['Land']).
card_subtypes('blasted landscape', []).
card_colors('blasted landscape', []).
card_text('blasted landscape', '{T}: Add {1} to your mana pool.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_layout('blasted landscape', 'normal').

% Found in: MMQ
card_name('blaster mage', 'Blaster Mage').
card_type('blaster mage', 'Creature — Human Spellshaper').
card_types('blaster mage', ['Creature']).
card_subtypes('blaster mage', ['Human', 'Spellshaper']).
card_colors('blaster mage', ['R']).
card_text('blaster mage', '{R}, {T}, Discard a card: Destroy target Wall.').
card_mana_cost('blaster mage', ['2', 'R']).
card_cmc('blaster mage', 3).
card_layout('blaster mage', 'normal').
card_power('blaster mage', 2).
card_toughness('blaster mage', 2).

% Found in: M15
card_name('blastfire bolt', 'Blastfire Bolt').
card_type('blastfire bolt', 'Instant').
card_types('blastfire bolt', ['Instant']).
card_subtypes('blastfire bolt', []).
card_colors('blastfire bolt', ['R']).
card_text('blastfire bolt', 'Blastfire Bolt deals 5 damage to target creature. Destroy all Equipment attached to that creature.').
card_mana_cost('blastfire bolt', ['5', 'R']).
card_cmc('blastfire bolt', 6).
card_layout('blastfire bolt', 'normal').

% Found in: 5DN
card_name('blasting station', 'Blasting Station').
card_type('blasting station', 'Artifact').
card_types('blasting station', ['Artifact']).
card_subtypes('blasting station', []).
card_colors('blasting station', []).
card_text('blasting station', '{T}, Sacrifice a creature: Blasting Station deals 1 damage to target creature or player.\nWhenever a creature enters the battlefield, you may untap Blasting Station.').
card_mana_cost('blasting station', ['3']).
card_cmc('blasting station', 3).
card_layout('blasting station', 'normal').

% Found in: DD3_GVL, DDD, NMS, VMA, pFNM
card_name('blastoderm', 'Blastoderm').
card_type('blastoderm', 'Creature — Beast').
card_types('blastoderm', ['Creature']).
card_subtypes('blastoderm', ['Beast']).
card_colors('blastoderm', ['G']).
card_text('blastoderm', 'Shroud (This creature can\'t be the target of spells or abilities.)\nFading 3 (This creature enters the battlefield with three fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)').
card_mana_cost('blastoderm', ['2', 'G', 'G']).
card_cmc('blastoderm', 4).
card_layout('blastoderm', 'normal').
card_power('blastoderm', 5).
card_toughness('blastoderm', 5).

% Found in: ONS
card_name('blatant thievery', 'Blatant Thievery').
card_type('blatant thievery', 'Sorcery').
card_types('blatant thievery', ['Sorcery']).
card_subtypes('blatant thievery', []).
card_colors('blatant thievery', ['U']).
card_text('blatant thievery', 'For each opponent, gain control of target permanent that player controls. (This effect lasts indefinitely.)').
card_mana_cost('blatant thievery', ['4', 'U', 'U', 'U']).
card_cmc('blatant thievery', 7).
card_layout('blatant thievery', 'normal').

% Found in: 10E, 6ED, 7ED, 8ED, 9ED, DPA, HOP, PO2, POR, PTK
card_name('blaze', 'Blaze').
card_type('blaze', 'Sorcery').
card_types('blaze', ['Sorcery']).
card_subtypes('blaze', []).
card_colors('blaze', ['R']).
card_text('blaze', 'Blaze deals X damage to target creature or player.').
card_mana_cost('blaze', ['X', 'R']).
card_cmc('blaze', 1).
card_layout('blaze', 'normal').

% Found in: DGM
card_name('blaze commando', 'Blaze Commando').
card_type('blaze commando', 'Creature — Minotaur Soldier').
card_types('blaze commando', ['Creature']).
card_subtypes('blaze commando', ['Minotaur', 'Soldier']).
card_colors('blaze commando', ['W', 'R']).
card_text('blaze commando', 'Whenever an instant or sorcery spell you control deals damage, put two 1/1 red and white Soldier creature tokens with haste onto the battlefield.').
card_mana_cost('blaze commando', ['3', 'R', 'W']).
card_cmc('blaze commando', 5).
card_layout('blaze commando', 'normal').
card_power('blaze commando', 5).
card_toughness('blaze commando', 3).

% Found in: 2ED, CED, CEI, LEA, LEB, ME4
card_name('blaze of glory', 'Blaze of Glory').
card_type('blaze of glory', 'Instant').
card_types('blaze of glory', ['Instant']).
card_subtypes('blaze of glory', []).
card_colors('blaze of glory', ['W']).
card_text('blaze of glory', 'Cast Blaze of Glory only during combat before blockers are declared.\nTarget creature defending player controls can block any number of creatures this turn. It blocks each attacking creature this turn if able.').
card_mana_cost('blaze of glory', ['W']).
card_cmc('blaze of glory', 1).
card_layout('blaze of glory', 'normal').
card_reserved('blaze of glory').

% Found in: SHM
card_name('blazethorn scarecrow', 'Blazethorn Scarecrow').
card_type('blazethorn scarecrow', 'Artifact Creature — Scarecrow').
card_types('blazethorn scarecrow', ['Artifact', 'Creature']).
card_subtypes('blazethorn scarecrow', ['Scarecrow']).
card_colors('blazethorn scarecrow', []).
card_text('blazethorn scarecrow', 'Blazethorn Scarecrow has haste as long as you control a red creature.\nBlazethorn Scarecrow has wither as long as you control a green creature. (It deals damage to creatures in the form of -1/-1 counters.)').
card_mana_cost('blazethorn scarecrow', ['5']).
card_cmc('blazethorn scarecrow', 5).
card_layout('blazethorn scarecrow', 'normal').
card_power('blazethorn scarecrow', 3).
card_toughness('blazethorn scarecrow', 3).

% Found in: PD3, RAV
card_name('blazing archon', 'Blazing Archon').
card_type('blazing archon', 'Creature — Archon').
card_types('blazing archon', ['Creature']).
card_subtypes('blazing archon', ['Archon']).
card_colors('blazing archon', ['W']).
card_text('blazing archon', 'Flying\nCreatures can\'t attack you.').
card_mana_cost('blazing archon', ['6', 'W', 'W', 'W']).
card_cmc('blazing archon', 9).
card_layout('blazing archon', 'normal').
card_power('blazing archon', 5).
card_toughness('blazing archon', 6).

% Found in: TSP
card_name('blazing blade askari', 'Blazing Blade Askari').
card_type('blazing blade askari', 'Creature — Human Knight').
card_types('blazing blade askari', ['Creature']).
card_subtypes('blazing blade askari', ['Human', 'Knight']).
card_colors('blazing blade askari', ['R']).
card_text('blazing blade askari', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{2}: Blazing Blade Askari becomes colorless until end of turn.').
card_mana_cost('blazing blade askari', ['2', 'R']).
card_cmc('blazing blade askari', 3).
card_layout('blazing blade askari', 'normal').
card_power('blazing blade askari', 2).
card_toughness('blazing blade askari', 2).

% Found in: LEG
card_name('blazing effigy', 'Blazing Effigy').
card_type('blazing effigy', 'Creature — Elemental').
card_types('blazing effigy', ['Creature']).
card_subtypes('blazing effigy', ['Elemental']).
card_colors('blazing effigy', ['R']).
card_text('blazing effigy', 'When Blazing Effigy dies, it deals X damage to target creature, where X is 3 plus the amount of damage dealt to Blazing Effigy this turn by other sources named Blazing Effigy.').
card_mana_cost('blazing effigy', ['1', 'R']).
card_cmc('blazing effigy', 2).
card_layout('blazing effigy', 'normal').
card_power('blazing effigy', 0).
card_toughness('blazing effigy', 3).

% Found in: ORI
card_name('blazing hellhound', 'Blazing Hellhound').
card_type('blazing hellhound', 'Creature — Elemental Hound').
card_types('blazing hellhound', ['Creature']).
card_subtypes('blazing hellhound', ['Elemental', 'Hound']).
card_colors('blazing hellhound', ['B', 'R']).
card_text('blazing hellhound', '{1}, Sacrifice another creature: Blazing Hellhound deals 1 damage to target creature or player.').
card_mana_cost('blazing hellhound', ['2', 'B', 'R']).
card_cmc('blazing hellhound', 4).
card_layout('blazing hellhound', 'normal').
card_power('blazing hellhound', 4).
card_toughness('blazing hellhound', 3).

% Found in: DDK, ODY
card_name('blazing salvo', 'Blazing Salvo').
card_type('blazing salvo', 'Instant').
card_types('blazing salvo', ['Instant']).
card_subtypes('blazing salvo', []).
card_colors('blazing salvo', ['R']).
card_text('blazing salvo', 'Blazing Salvo deals 3 damage to target creature unless that creature\'s controller has Blazing Salvo deal 5 damage to him or her.').
card_mana_cost('blazing salvo', ['R']).
card_cmc('blazing salvo', 1).
card_layout('blazing salvo', 'normal').

% Found in: BOK
card_name('blazing shoal', 'Blazing Shoal').
card_type('blazing shoal', 'Instant — Arcane').
card_types('blazing shoal', ['Instant']).
card_subtypes('blazing shoal', ['Arcane']).
card_colors('blazing shoal', ['R']).
card_text('blazing shoal', 'You may exile a red card with converted mana cost X from your hand rather than pay Blazing Shoal\'s mana cost.\nTarget creature gets +X/+0 until end of turn.').
card_mana_cost('blazing shoal', ['X', 'R', 'R']).
card_cmc('blazing shoal', 2).
card_layout('blazing shoal', 'normal').

% Found in: DDH, INV, VMA
card_name('blazing specter', 'Blazing Specter').
card_type('blazing specter', 'Creature — Specter').
card_types('blazing specter', ['Creature']).
card_subtypes('blazing specter', ['Specter']).
card_colors('blazing specter', ['B', 'R']).
card_text('blazing specter', 'Flying, haste\nWhenever Blazing Specter deals combat damage to a player, that player discards a card.').
card_mana_cost('blazing specter', ['2', 'B', 'R']).
card_cmc('blazing specter', 4).
card_layout('blazing specter', 'normal').
card_power('blazing specter', 2).
card_toughness('blazing specter', 2).

% Found in: ISD, ZEN
card_name('blazing torch', 'Blazing Torch').
card_type('blazing torch', 'Artifact — Equipment').
card_types('blazing torch', ['Artifact']).
card_subtypes('blazing torch', ['Equipment']).
card_colors('blazing torch', []).
card_text('blazing torch', 'Equipped creature can\'t be blocked by Vampires or Zombies.\nEquipped creature has \"{T}, Sacrifice Blazing Torch: Blazing Torch deals 2 damage to target creature or player.\"\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('blazing torch', ['1']).
card_cmc('blazing torch', 1).
card_layout('blazing torch', 'normal').

% Found in: SOM
card_name('bleak coven vampires', 'Bleak Coven Vampires').
card_type('bleak coven vampires', 'Creature — Vampire Warrior').
card_types('bleak coven vampires', ['Creature']).
card_subtypes('bleak coven vampires', ['Vampire', 'Warrior']).
card_colors('bleak coven vampires', ['B']).
card_text('bleak coven vampires', 'Metalcraft — When Bleak Coven Vampires enters the battlefield, if you control three or more artifacts, target player loses 4 life and you gain 4 life.').
card_mana_cost('bleak coven vampires', ['3', 'B', 'B']).
card_cmc('bleak coven vampires', 5).
card_layout('bleak coven vampires', 'normal').
card_power('bleak coven vampires', 4).
card_toughness('bleak coven vampires', 3).

% Found in: CHK
card_name('blessed breath', 'Blessed Breath').
card_type('blessed breath', 'Instant — Arcane').
card_types('blessed breath', ['Instant']).
card_subtypes('blessed breath', ['Arcane']).
card_colors('blessed breath', ['W']).
card_text('blessed breath', 'Target creature you control gains protection from the color of your choice until end of turn.\nSplice onto Arcane {W} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('blessed breath', ['W']).
card_cmc('blessed breath', 1).
card_layout('blessed breath', 'normal').

% Found in: 9ED, ODY
card_name('blessed orator', 'Blessed Orator').
card_type('blessed orator', 'Creature — Human Cleric').
card_types('blessed orator', ['Creature']).
card_subtypes('blessed orator', ['Human', 'Cleric']).
card_colors('blessed orator', ['W']).
card_text('blessed orator', 'Other creatures you control get +0/+1.').
card_mana_cost('blessed orator', ['3', 'W']).
card_cmc('blessed orator', 4).
card_layout('blessed orator', 'normal').
card_power('blessed orator', 1).
card_toughness('blessed orator', 4).

% Found in: DTK
card_name('blessed reincarnation', 'Blessed Reincarnation').
card_type('blessed reincarnation', 'Instant').
card_types('blessed reincarnation', ['Instant']).
card_subtypes('blessed reincarnation', []).
card_colors('blessed reincarnation', ['U']).
card_text('blessed reincarnation', 'Exile target creature an opponent controls. That player reveals cards from the top of his or her library until a creature card is revealed. The player puts that card onto the battlefield, then shuffles the rest into his or her library.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('blessed reincarnation', ['3', 'U']).
card_cmc('blessed reincarnation', 4).
card_layout('blessed reincarnation', 'normal').

% Found in: 7ED, 8ED, POR, ULG
card_name('blessed reversal', 'Blessed Reversal').
card_type('blessed reversal', 'Instant').
card_types('blessed reversal', ['Instant']).
card_subtypes('blessed reversal', []).
card_colors('blessed reversal', ['W']).
card_text('blessed reversal', 'You gain 3 life for each creature attacking you.').
card_mana_cost('blessed reversal', ['1', 'W']).
card_cmc('blessed reversal', 2).
card_layout('blessed reversal', 'normal').

% Found in: ORI
card_name('blessed spirits', 'Blessed Spirits').
card_type('blessed spirits', 'Creature — Spirit').
card_types('blessed spirits', ['Creature']).
card_subtypes('blessed spirits', ['Spirit']).
card_colors('blessed spirits', ['W']).
card_text('blessed spirits', 'Flying\nWhenever you cast an enchantment spell, put a +1/+1 counter on Blessed Spirits.').
card_mana_cost('blessed spirits', ['2', 'W']).
card_cmc('blessed spirits', 3).
card_layout('blessed spirits', 'normal').
card_power('blessed spirits', 2).
card_toughness('blessed spirits', 2).

% Found in: PCY
card_name('blessed wind', 'Blessed Wind').
card_type('blessed wind', 'Sorcery').
card_types('blessed wind', ['Sorcery']).
card_subtypes('blessed wind', []).
card_colors('blessed wind', ['W']).
card_text('blessed wind', 'Target player\'s life total becomes 20.').
card_mana_cost('blessed wind', ['7', 'W', 'W']).
card_cmc('blessed wind', 9).
card_layout('blessed wind', 'normal').

% Found in: 5ED, ICE
card_name('blessed wine', 'Blessed Wine').
card_type('blessed wine', 'Instant').
card_types('blessed wine', ['Instant']).
card_subtypes('blessed wine', []).
card_colors('blessed wine', ['W']).
card_text('blessed wine', 'You gain 1 life.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('blessed wine', ['1', 'W']).
card_cmc('blessed wine', 2).
card_layout('blessed wine', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB, M14
card_name('blessing', 'Blessing').
card_type('blessing', 'Enchantment — Aura').
card_types('blessing', ['Enchantment']).
card_subtypes('blessing', ['Aura']).
card_colors('blessing', ['W']).
card_text('blessing', 'Enchant creature\n{W}: Enchanted creature gets +1/+1 until end of turn.').
card_mana_cost('blessing', ['W', 'W']).
card_cmc('blessing', 2).
card_layout('blessing', 'normal').

% Found in: BOK
card_name('blessing of leeches', 'Blessing of Leeches').
card_type('blessing of leeches', 'Enchantment — Aura').
card_types('blessing of leeches', ['Enchantment']).
card_subtypes('blessing of leeches', ['Aura']).
card_colors('blessing of leeches', ['B']).
card_text('blessing of leeches', 'Flash\nEnchant creature\nAt the beginning of your upkeep, you lose 1 life.\n{0}: Regenerate enchanted creature.').
card_mana_cost('blessing of leeches', ['2', 'B']).
card_cmc('blessing of leeches', 3).
card_layout('blessing of leeches', 'normal').

% Found in: DIS
card_name('blessing of the nephilim', 'Blessing of the Nephilim').
card_type('blessing of the nephilim', 'Enchantment — Aura').
card_types('blessing of the nephilim', ['Enchantment']).
card_subtypes('blessing of the nephilim', ['Aura']).
card_colors('blessing of the nephilim', ['W']).
card_text('blessing of the nephilim', 'Enchant creature\nEnchanted creature gets +1/+1 for each of its colors.').
card_mana_cost('blessing of the nephilim', ['W']).
card_cmc('blessing of the nephilim', 1).
card_layout('blessing of the nephilim', 'normal').

% Found in: AVR
card_name('blessings of nature', 'Blessings of Nature').
card_type('blessings of nature', 'Sorcery').
card_types('blessings of nature', ['Sorcery']).
card_subtypes('blessings of nature', []).
card_colors('blessings of nature', ['G']).
card_text('blessings of nature', 'Distribute four +1/+1 counters among any number of target creatures.\nMiracle {G} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_mana_cost('blessings of nature', ['4', 'G']).
card_cmc('blessings of nature', 5).
card_layout('blessings of nature', 'normal').

% Found in: 4ED, 5ED, 6ED, LEG, MED
card_name('blight', 'Blight').
card_type('blight', 'Enchantment — Aura').
card_types('blight', ['Enchantment']).
card_subtypes('blight', ['Aura']).
card_colors('blight', ['B']).
card_text('blight', 'Enchant land\nWhen enchanted land becomes tapped, destroy it.').
card_mana_cost('blight', ['B', 'B']).
card_cmc('blight', 2).
card_layout('blight', 'normal').

% Found in: BFZ
card_name('blight herder', 'Blight Herder').
card_type('blight herder', 'Creature — Eldrazi Processor').
card_types('blight herder', ['Creature']).
card_subtypes('blight herder', ['Eldrazi', 'Processor']).
card_colors('blight herder', []).
card_text('blight herder', 'When you cast Blight Herder, you may put two cards your opponents own from exile into their owners\' graveyards. If you do, put three 1/1 colorless Eldrazi Scion creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('blight herder', ['5']).
card_cmc('blight herder', 5).
card_layout('blight herder', 'normal').
card_power('blight herder', 4).
card_toughness('blight herder', 5).

% Found in: SOM
card_name('blight mamba', 'Blight Mamba').
card_type('blight mamba', 'Creature — Snake').
card_types('blight mamba', ['Creature']).
card_subtypes('blight mamba', ['Snake']).
card_colors('blight mamba', ['G']).
card_text('blight mamba', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\n{1}{G}: Regenerate Blight Mamba.').
card_mana_cost('blight mamba', ['1', 'G']).
card_cmc('blight mamba', 2).
card_layout('blight mamba', 'normal').
card_power('blight mamba', 1).
card_toughness('blight mamba', 1).

% Found in: SHM
card_name('blight sickle', 'Blight Sickle').
card_type('blight sickle', 'Artifact — Equipment').
card_types('blight sickle', ['Artifact']).
card_subtypes('blight sickle', ['Equipment']).
card_colors('blight sickle', []).
card_text('blight sickle', 'Equipped creature gets +1/+0 and has wither. (It deals damage to creatures in the form of -1/-1 counters.)\nEquip {2}').
card_mana_cost('blight sickle', ['2']).
card_cmc('blight sickle', 2).
card_layout('blight sickle', 'normal').

% Found in: M14, ORI
card_name('blightcaster', 'Blightcaster').
card_type('blightcaster', 'Creature — Human Wizard').
card_types('blightcaster', ['Creature']).
card_subtypes('blightcaster', ['Human', 'Wizard']).
card_colors('blightcaster', ['B']).
card_text('blightcaster', 'Whenever you cast an enchantment spell, you may have target creature get -2/-2 until end of turn.').
card_mana_cost('blightcaster', ['3', 'B']).
card_cmc('blightcaster', 4).
card_layout('blightcaster', 'normal').
card_power('blightcaster', 2).
card_toughness('blightcaster', 3).

% Found in: NPH
card_name('blighted agent', 'Blighted Agent').
card_type('blighted agent', 'Creature — Human Rogue').
card_types('blighted agent', ['Creature']).
card_subtypes('blighted agent', ['Human', 'Rogue']).
card_colors('blighted agent', ['U']).
card_text('blighted agent', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nBlighted Agent can\'t be blocked.').
card_mana_cost('blighted agent', ['1', 'U']).
card_cmc('blighted agent', 2).
card_layout('blighted agent', 'normal').
card_power('blighted agent', 1).
card_toughness('blighted agent', 1).

% Found in: BFZ
card_name('blighted cataract', 'Blighted Cataract').
card_type('blighted cataract', 'Land').
card_types('blighted cataract', ['Land']).
card_subtypes('blighted cataract', []).
card_colors('blighted cataract', []).
card_text('blighted cataract', '{T}: Add {1} to your mana pool.\n{5}{U}, {T}, Sacrifice Blighted Cataract: Draw two cards.').
card_layout('blighted cataract', 'normal').

% Found in: BFZ
card_name('blighted fen', 'Blighted Fen').
card_type('blighted fen', 'Land').
card_types('blighted fen', ['Land']).
card_subtypes('blighted fen', []).
card_colors('blighted fen', []).
card_text('blighted fen', '{T}: Add {1} to your mana pool.\n{4}{B}, {T}, Sacrifice Blighted Fen: Target opponent sacrifices a creature.').
card_layout('blighted fen', 'normal').

% Found in: BFZ
card_name('blighted gorge', 'Blighted Gorge').
card_type('blighted gorge', 'Land').
card_types('blighted gorge', ['Land']).
card_subtypes('blighted gorge', []).
card_colors('blighted gorge', []).
card_text('blighted gorge', '{T}: Add {1} to your mana pool.\n{4}{R}, {T}, Sacrifice Blighted Gorge: Blighted Gorge deals 2 damage to target creature or player.').
card_layout('blighted gorge', 'normal').

% Found in: 6ED, MIR
card_name('blighted shaman', 'Blighted Shaman').
card_type('blighted shaman', 'Creature — Human Cleric Shaman').
card_types('blighted shaman', ['Creature']).
card_subtypes('blighted shaman', ['Human', 'Cleric', 'Shaman']).
card_colors('blighted shaman', ['B']).
card_text('blighted shaman', '{T}, Sacrifice a Swamp: Target creature gets +1/+1 until end of turn.\n{T}, Sacrifice a creature: Target creature gets +2/+2 until end of turn.').
card_mana_cost('blighted shaman', ['1', 'B']).
card_cmc('blighted shaman', 2).
card_layout('blighted shaman', 'normal').
card_power('blighted shaman', 1).
card_toughness('blighted shaman', 1).

% Found in: BFZ
card_name('blighted steppe', 'Blighted Steppe').
card_type('blighted steppe', 'Land').
card_types('blighted steppe', ['Land']).
card_subtypes('blighted steppe', []).
card_colors('blighted steppe', []).
card_text('blighted steppe', '{T}: Add {1} to your mana pool.\n{3}{W}, {T}, Sacrifice Blighted Steppe: You gain 2 life for each creature you control.').
card_layout('blighted steppe', 'normal').

% Found in: BFZ
card_name('blighted woodland', 'Blighted Woodland').
card_type('blighted woodland', 'Land').
card_types('blighted woodland', ['Land']).
card_subtypes('blighted woodland', []).
card_colors('blighted woodland', []).
card_text('blighted woodland', '{T}: Add {1} to your mana pool.\n{3}{G}, {T}, Sacrifice Blighted Woodland: Search your library for up to two basic land cards and put them onto the battlefield tapped. Then shuffle your library.').
card_layout('blighted woodland', 'normal').

% Found in: ALA, DDK, pMPR
card_name('blightning', 'Blightning').
card_type('blightning', 'Sorcery').
card_types('blightning', ['Sorcery']).
card_subtypes('blightning', []).
card_colors('blightning', ['B', 'R']).
card_text('blightning', 'Blightning deals 3 damage to target player. That player discards two cards.').
card_mana_cost('blightning', ['1', 'B', 'R']).
card_cmc('blightning', 3).
card_layout('blightning', 'normal').

% Found in: MOR
card_name('blightsoil druid', 'Blightsoil Druid').
card_type('blightsoil druid', 'Creature — Elf Druid').
card_types('blightsoil druid', ['Creature']).
card_subtypes('blightsoil druid', ['Elf', 'Druid']).
card_colors('blightsoil druid', ['B']).
card_text('blightsoil druid', '{T}, Pay 1 life: Add {G} to your mana pool.').
card_mana_cost('blightsoil druid', ['1', 'B']).
card_cmc('blightsoil druid', 2).
card_layout('blightsoil druid', 'normal').
card_power('blightsoil druid', 1).
card_toughness('blightsoil druid', 2).

% Found in: MMA, PLC
card_name('blightspeaker', 'Blightspeaker').
card_type('blightspeaker', 'Creature — Human Rebel Cleric').
card_types('blightspeaker', ['Creature']).
card_subtypes('blightspeaker', ['Human', 'Rebel', 'Cleric']).
card_colors('blightspeaker', ['B']).
card_text('blightspeaker', '{T}: Target player loses 1 life.\n{4}, {T}: Search your library for a Rebel permanent card with converted mana cost 3 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('blightspeaker', ['1', 'B']).
card_cmc('blightspeaker', 2).
card_layout('blightspeaker', 'normal').
card_power('blightspeaker', 1).
card_toughness('blightspeaker', 1).

% Found in: MBS
card_name('blightsteel colossus', 'Blightsteel Colossus').
card_type('blightsteel colossus', 'Artifact Creature — Golem').
card_types('blightsteel colossus', ['Artifact', 'Creature']).
card_subtypes('blightsteel colossus', ['Golem']).
card_colors('blightsteel colossus', []).
card_text('blightsteel colossus', 'Trample, infect, indestructible\nIf Blightsteel Colossus would be put into a graveyard from anywhere, reveal Blightsteel Colossus and shuffle it into its owner\'s library instead.').
card_mana_cost('blightsteel colossus', ['12']).
card_cmc('blightsteel colossus', 12).
card_layout('blightsteel colossus', 'normal').
card_power('blightsteel colossus', 11).
card_toughness('blightsteel colossus', 11).

% Found in: MBS
card_name('blightwidow', 'Blightwidow').
card_type('blightwidow', 'Creature — Spider').
card_types('blightwidow', ['Creature']).
card_subtypes('blightwidow', ['Spider']).
card_colors('blightwidow', ['G']).
card_text('blightwidow', 'Reach (This creature can block creatures with flying.)\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('blightwidow', ['3', 'G']).
card_cmc('blightwidow', 4).
card_layout('blightwidow', 'normal').
card_power('blightwidow', 2).
card_toughness('blightwidow', 4).

% Found in: 5DN
card_name('blind creeper', 'Blind Creeper').
card_type('blind creeper', 'Creature — Zombie Beast').
card_types('blind creeper', ['Creature']).
card_subtypes('blind creeper', ['Zombie', 'Beast']).
card_colors('blind creeper', ['B']).
card_text('blind creeper', 'Whenever a player casts a spell, Blind Creeper gets -1/-1 until end of turn.').
card_mana_cost('blind creeper', ['1', 'B']).
card_cmc('blind creeper', 2).
card_layout('blind creeper', 'normal').
card_power('blind creeper', 3).
card_toughness('blind creeper', 3).

% Found in: MIR
card_name('blind fury', 'Blind Fury').
card_type('blind fury', 'Instant').
card_types('blind fury', ['Instant']).
card_subtypes('blind fury', []).
card_colors('blind fury', ['R']).
card_text('blind fury', 'All creatures lose trample until end of turn. If a creature would deal combat damage to a creature this turn, it deals double that damage to that creature instead.').
card_mana_cost('blind fury', ['2', 'R', 'R']).
card_cmc('blind fury', 4).
card_layout('blind fury', 'normal').

% Found in: GPT
card_name('blind hunter', 'Blind Hunter').
card_type('blind hunter', 'Creature — Bat').
card_types('blind hunter', ['Creature']).
card_subtypes('blind hunter', ['Bat']).
card_colors('blind hunter', ['W', 'B']).
card_text('blind hunter', 'Flying\nHaunt (When this creature dies, exile it haunting target creature.)\nWhen Blind Hunter enters the battlefield or the creature it haunts dies, target player loses 2 life and you gain 2 life.').
card_mana_cost('blind hunter', ['2', 'W', 'B']).
card_cmc('blind hunter', 4).
card_layout('blind hunter', 'normal').
card_power('blind hunter', 2).
card_toughness('blind hunter', 2).

% Found in: GTC
card_name('blind obedience', 'Blind Obedience').
card_type('blind obedience', 'Enchantment').
card_types('blind obedience', ['Enchantment']).
card_subtypes('blind obedience', []).
card_colors('blind obedience', ['W']).
card_text('blind obedience', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)\nArtifacts and creatures your opponents control enter the battlefield tapped.').
card_mana_cost('blind obedience', ['1', 'W']).
card_cmc('blind obedience', 2).
card_layout('blind obedience', 'normal').

% Found in: FUT
card_name('blind phantasm', 'Blind Phantasm').
card_type('blind phantasm', 'Creature — Illusion').
card_types('blind phantasm', ['Creature']).
card_subtypes('blind phantasm', ['Illusion']).
card_colors('blind phantasm', ['U']).
card_text('blind phantasm', '').
card_mana_cost('blind phantasm', ['2', 'U']).
card_cmc('blind phantasm', 3).
card_layout('blind phantasm', 'normal').
card_power('blind phantasm', 2).
card_toughness('blind phantasm', 3).

% Found in: INV
card_name('blind seer', 'Blind Seer').
card_type('blind seer', 'Legendary Creature — Human Wizard').
card_types('blind seer', ['Creature']).
card_subtypes('blind seer', ['Human', 'Wizard']).
card_supertypes('blind seer', ['Legendary']).
card_colors('blind seer', ['U']).
card_text('blind seer', '{1}{U}: Target spell or permanent becomes the color of your choice until end of turn.').
card_mana_cost('blind seer', ['2', 'U', 'U']).
card_cmc('blind seer', 4).
card_layout('blind seer', 'normal').
card_power('blind seer', 3).
card_toughness('blind seer', 3).

% Found in: CHK
card_name('blind with anger', 'Blind with Anger').
card_type('blind with anger', 'Instant — Arcane').
card_types('blind with anger', ['Instant']).
card_subtypes('blind with anger', ['Arcane']).
card_colors('blind with anger', ['R']).
card_text('blind with anger', 'Untap target nonlegendary creature and gain control of it until end of turn. That creature gains haste until end of turn.').
card_mana_cost('blind with anger', ['3', 'R']).
card_cmc('blind with anger', 4).
card_layout('blind with anger', 'normal').

% Found in: NPH
card_name('blind zealot', 'Blind Zealot').
card_type('blind zealot', 'Creature — Human Cleric').
card_types('blind zealot', ['Creature']).
card_subtypes('blind zealot', ['Human', 'Cleric']).
card_colors('blind zealot', ['B']).
card_text('blind zealot', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nWhenever Blind Zealot deals combat damage to a player, you may sacrifice it. If you do, destroy target creature that player controls.').
card_mana_cost('blind zealot', ['1', 'B', 'B']).
card_cmc('blind zealot', 3).
card_layout('blind zealot', 'normal').
card_power('blind zealot', 2).
card_toughness('blind zealot', 2).

% Found in: LRW, MMA
card_name('blind-spot giant', 'Blind-Spot Giant').
card_type('blind-spot giant', 'Creature — Giant Warrior').
card_types('blind-spot giant', ['Creature']).
card_subtypes('blind-spot giant', ['Giant', 'Warrior']).
card_colors('blind-spot giant', ['R']).
card_text('blind-spot giant', 'Blind-Spot Giant can\'t attack or block unless you control another Giant.').
card_mana_cost('blind-spot giant', ['2', 'R']).
card_cmc('blind-spot giant', 3).
card_layout('blind-spot giant', 'normal').
card_power('blind-spot giant', 4).
card_toughness('blind-spot giant', 3).

% Found in: 8ED, 9ED, NMS
card_name('blinding angel', 'Blinding Angel').
card_type('blinding angel', 'Creature — Angel').
card_types('blinding angel', ['Creature']).
card_subtypes('blinding angel', ['Angel']).
card_colors('blinding angel', ['W']).
card_text('blinding angel', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhenever Blinding Angel deals combat damage to a player, that player skips his or her next combat phase.').
card_mana_cost('blinding angel', ['3', 'W', 'W']).
card_cmc('blinding angel', 5).
card_layout('blinding angel', 'normal').
card_power('blinding angel', 2).
card_toughness('blinding angel', 4).

% Found in: DDF, MMA, MRD
card_name('blinding beam', 'Blinding Beam').
card_type('blinding beam', 'Instant').
card_types('blinding beam', ['Instant']).
card_subtypes('blinding beam', []).
card_colors('blinding beam', ['W']).
card_text('blinding beam', 'Choose one —\n• Tap two target creatures.\n• Creatures don\'t untap during target player\'s next untap step.\nEntwine {1} (Choose both if you pay the entwine cost.)').
card_mana_cost('blinding beam', ['2', 'W']).
card_cmc('blinding beam', 3).
card_layout('blinding beam', 'normal').

% Found in: JOU
card_name('blinding flare', 'Blinding Flare').
card_type('blinding flare', 'Sorcery').
card_types('blinding flare', ['Sorcery']).
card_subtypes('blinding flare', []).
card_colors('blinding flare', ['R']).
card_text('blinding flare', 'Strive — Blinding Flare costs {R} more to cast for each target beyond the first.\nAny number of target creatures can\'t block this turn.').
card_mana_cost('blinding flare', ['R']).
card_cmc('blinding flare', 1).
card_layout('blinding flare', 'normal').

% Found in: INV, MIR, POR, S99
card_name('blinding light', 'Blinding Light').
card_type('blinding light', 'Sorcery').
card_types('blinding light', ['Sorcery']).
card_subtypes('blinding light', []).
card_colors('blinding light', ['W']).
card_text('blinding light', 'Tap all nonwhite creatures.').
card_mana_cost('blinding light', ['2', 'W']).
card_cmc('blinding light', 3).
card_layout('blinding light', 'normal').

% Found in: M10, M11
card_name('blinding mage', 'Blinding Mage').
card_type('blinding mage', 'Creature — Human Wizard').
card_types('blinding mage', ['Creature']).
card_subtypes('blinding mage', ['Human', 'Wizard']).
card_colors('blinding mage', ['W']).
card_text('blinding mage', '{W}, {T}: Tap target creature.').
card_mana_cost('blinding mage', ['1', 'W']).
card_cmc('blinding mage', 2).
card_layout('blinding mage', 'normal').
card_power('blinding mage', 1).
card_toughness('blinding mage', 2).

% Found in: BOK
card_name('blinding powder', 'Blinding Powder').
card_type('blinding powder', 'Artifact — Equipment').
card_types('blinding powder', ['Artifact']).
card_subtypes('blinding powder', ['Equipment']).
card_colors('blinding powder', []).
card_text('blinding powder', 'Equipped creature has \"Unattach Blinding Powder: Prevent all combat damage that would be dealt to this creature this turn.\"\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('blinding powder', ['1']).
card_cmc('blinding powder', 1).
card_layout('blinding powder', 'normal').

% Found in: MM2, NPH
card_name('blinding souleater', 'Blinding Souleater').
card_type('blinding souleater', 'Artifact Creature — Cleric').
card_types('blinding souleater', ['Artifact', 'Creature']).
card_subtypes('blinding souleater', ['Cleric']).
card_colors('blinding souleater', []).
card_text('blinding souleater', '{W/P}, {T}: Tap target creature. ({W/P} can be paid with either {W} or 2 life.)').
card_mana_cost('blinding souleater', ['3']).
card_cmc('blinding souleater', 3).
card_layout('blinding souleater', 'normal').
card_power('blinding souleater', 1).
card_toughness('blinding souleater', 3).

% Found in: KTK
card_name('blinding spray', 'Blinding Spray').
card_type('blinding spray', 'Instant').
card_types('blinding spray', ['Instant']).
card_subtypes('blinding spray', []).
card_colors('blinding spray', ['U']).
card_text('blinding spray', 'Creatures your opponents control get -4/-0 until end of turn.\nDraw a card.').
card_mana_cost('blinding spray', ['4', 'U']).
card_cmc('blinding spray', 5).
card_layout('blinding spray', 'normal').

% Found in: 5ED, 9ED, BRB, ICE
card_name('blinking spirit', 'Blinking Spirit').
card_type('blinking spirit', 'Creature — Spirit').
card_types('blinking spirit', ['Creature']).
card_subtypes('blinking spirit', ['Spirit']).
card_colors('blinking spirit', ['W']).
card_text('blinking spirit', '{0}: Return Blinking Spirit to its owner\'s hand.').
card_mana_cost('blinking spirit', ['3', 'W']).
card_cmc('blinking spirit', 4).
card_layout('blinking spirit', 'normal').
card_power('blinking spirit', 2).
card_toughness('blinking spirit', 2).

% Found in: 5DN
card_name('blinkmoth infusion', 'Blinkmoth Infusion').
card_type('blinkmoth infusion', 'Instant').
card_types('blinkmoth infusion', ['Instant']).
card_subtypes('blinkmoth infusion', []).
card_colors('blinkmoth infusion', ['U']).
card_text('blinkmoth infusion', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nUntap all artifacts.').
card_mana_cost('blinkmoth infusion', ['12', 'U', 'U']).
card_cmc('blinkmoth infusion', 14).
card_layout('blinkmoth infusion', 'normal').

% Found in: DST, MM2, MMA
card_name('blinkmoth nexus', 'Blinkmoth Nexus').
card_type('blinkmoth nexus', 'Land').
card_types('blinkmoth nexus', ['Land']).
card_subtypes('blinkmoth nexus', []).
card_colors('blinkmoth nexus', []).
card_text('blinkmoth nexus', '{T}: Add {1} to your mana pool.\n{1}: Blinkmoth Nexus becomes a 1/1 Blinkmoth artifact creature with flying until end of turn. It\'s still a land.\n{1}, {T}: Target Blinkmoth creature gets +1/+1 until end of turn.').
card_layout('blinkmoth nexus', 'normal').

% Found in: MRD
card_name('blinkmoth urn', 'Blinkmoth Urn').
card_type('blinkmoth urn', 'Artifact').
card_types('blinkmoth urn', ['Artifact']).
card_subtypes('blinkmoth urn', []).
card_colors('blinkmoth urn', []).
card_text('blinkmoth urn', 'At the beginning of each player\'s precombat main phase, if Blinkmoth Urn is untapped, that player adds {1} to his or her mana pool for each artifact he or she controls.').
card_mana_cost('blinkmoth urn', ['5']).
card_cmc('blinkmoth urn', 5).
card_layout('blinkmoth urn', 'normal').

% Found in: MRD
card_name('blinkmoth well', 'Blinkmoth Well').
card_type('blinkmoth well', 'Land').
card_types('blinkmoth well', ['Land']).
card_subtypes('blinkmoth well', []).
card_colors('blinkmoth well', []).
card_text('blinkmoth well', '{T}: Add {1} to your mana pool.\n{2}, {T}: Tap target noncreature artifact.').
card_layout('blinkmoth well', 'normal').

% Found in: ALA
card_name('blister beetle', 'Blister Beetle').
card_type('blister beetle', 'Creature — Insect').
card_types('blister beetle', ['Creature']).
card_subtypes('blister beetle', ['Insect']).
card_colors('blister beetle', ['B']).
card_text('blister beetle', 'When Blister Beetle enters the battlefield, target creature gets -1/-1 until end of turn.').
card_mana_cost('blister beetle', ['1', 'B']).
card_cmc('blister beetle', 2).
card_layout('blister beetle', 'normal').
card_power('blister beetle', 1).
card_toughness('blister beetle', 1).

% Found in: RTR
card_name('blistercoil weird', 'Blistercoil Weird').
card_type('blistercoil weird', 'Creature — Weird').
card_types('blistercoil weird', ['Creature']).
card_subtypes('blistercoil weird', ['Weird']).
card_colors('blistercoil weird', ['U', 'R']).
card_text('blistercoil weird', 'Whenever you cast an instant or sorcery spell, Blistercoil Weird gets +1/+1 until end of turn. Untap it.').
card_mana_cost('blistercoil weird', ['U/R']).
card_cmc('blistercoil weird', 1).
card_layout('blistercoil weird', 'normal').
card_power('blistercoil weird', 1).
card_toughness('blistercoil weird', 1).

% Found in: SOM
card_name('blistergrub', 'Blistergrub').
card_type('blistergrub', 'Creature — Horror').
card_types('blistergrub', ['Creature']).
card_subtypes('blistergrub', ['Horror']).
card_colors('blistergrub', ['B']).
card_text('blistergrub', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)\nWhen Blistergrub dies, each opponent loses 2 life.').
card_mana_cost('blistergrub', ['2', 'B']).
card_cmc('blistergrub', 3).
card_layout('blistergrub', 'normal').
card_power('blistergrub', 2).
card_toughness('blistergrub', 2).

% Found in: MIR
card_name('blistering barrier', 'Blistering Barrier').
card_type('blistering barrier', 'Creature — Wall').
card_types('blistering barrier', ['Creature']).
card_subtypes('blistering barrier', ['Wall']).
card_colors('blistering barrier', ['R']).
card_text('blistering barrier', 'Defender (This creature can\'t attack.)').
card_mana_cost('blistering barrier', ['2', 'R']).
card_cmc('blistering barrier', 3).
card_layout('blistering barrier', 'normal').
card_power('blistering barrier', 5).
card_toughness('blistering barrier', 2).

% Found in: SHM
card_name('blistering dieflyn', 'Blistering Dieflyn').
card_type('blistering dieflyn', 'Creature — Imp').
card_types('blistering dieflyn', ['Creature']).
card_subtypes('blistering dieflyn', ['Imp']).
card_colors('blistering dieflyn', ['R']).
card_text('blistering dieflyn', 'Flying\n{B/R}: Blistering Dieflyn gets +1/+0 until end of turn.').
card_mana_cost('blistering dieflyn', ['3', 'R']).
card_cmc('blistering dieflyn', 4).
card_layout('blistering dieflyn', 'normal').
card_power('blistering dieflyn', 0).
card_toughness('blistering dieflyn', 1).

% Found in: ONS
card_name('blistering firecat', 'Blistering Firecat').
card_type('blistering firecat', 'Creature — Elemental Cat').
card_types('blistering firecat', ['Creature']).
card_subtypes('blistering firecat', ['Elemental', 'Cat']).
card_colors('blistering firecat', ['R']).
card_text('blistering firecat', 'Trample, haste\nAt the beginning of the end step, sacrifice Blistering Firecat.\nMorph {R}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('blistering firecat', ['1', 'R', 'R', 'R']).
card_cmc('blistering firecat', 4).
card_layout('blistering firecat', 'normal').
card_power('blistering firecat', 7).
card_toughness('blistering firecat', 1).

% Found in: BFZ
card_name('blisterpod', 'Blisterpod').
card_type('blisterpod', 'Creature — Eldrazi Drone').
card_types('blisterpod', ['Creature']).
card_subtypes('blisterpod', ['Eldrazi', 'Drone']).
card_colors('blisterpod', []).
card_text('blisterpod', 'Devoid (This card has no color.)\nWhen Blisterpod dies, put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('blisterpod', ['G']).
card_cmc('blisterpod', 1).
card_layout('blisterpod', 'normal').
card_power('blisterpod', 1).
card_toughness('blisterpod', 1).

% Found in: MBS
card_name('blisterstick shaman', 'Blisterstick Shaman').
card_type('blisterstick shaman', 'Creature — Goblin Shaman').
card_types('blisterstick shaman', ['Creature']).
card_subtypes('blisterstick shaman', ['Goblin', 'Shaman']).
card_colors('blisterstick shaman', ['R']).
card_text('blisterstick shaman', 'When Blisterstick Shaman enters the battlefield, it deals 1 damage to target creature or player.').
card_mana_cost('blisterstick shaman', ['2', 'R']).
card_cmc('blisterstick shaman', 3).
card_layout('blisterstick shaman', 'normal').
card_power('blisterstick shaman', 2).
card_toughness('blisterstick shaman', 1).

% Found in: ARB
card_name('blitz hellion', 'Blitz Hellion').
card_type('blitz hellion', 'Creature — Hellion').
card_types('blitz hellion', ['Creature']).
card_subtypes('blitz hellion', ['Hellion']).
card_colors('blitz hellion', ['R', 'G']).
card_text('blitz hellion', 'Trample, haste\nAt the beginning of the end step, Blitz Hellion\'s owner shuffles it into his or her library.').
card_mana_cost('blitz hellion', ['3', 'R', 'G']).
card_cmc('blitz hellion', 5).
card_layout('blitz hellion', 'normal').
card_power('blitz hellion', 7).
card_toughness('blitz hellion', 7).

% Found in: ICE
card_name('blizzard', 'Blizzard').
card_type('blizzard', 'Enchantment').
card_types('blizzard', ['Enchantment']).
card_subtypes('blizzard', []).
card_colors('blizzard', ['G']).
card_text('blizzard', 'Cast Blizzard only if you control a snow land.\nCumulative upkeep {2} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nCreatures with flying don\'t untap during their controllers\' untap steps.').
card_mana_cost('blizzard', ['G', 'G']).
card_cmc('blizzard', 2).
card_layout('blizzard', 'normal').
card_reserved('blizzard').

% Found in: BTD, UDS
card_name('blizzard elemental', 'Blizzard Elemental').
card_type('blizzard elemental', 'Creature — Elemental').
card_types('blizzard elemental', ['Creature']).
card_subtypes('blizzard elemental', ['Elemental']).
card_colors('blizzard elemental', ['U']).
card_text('blizzard elemental', 'Flying\n{3}{U}: Untap Blizzard Elemental.').
card_mana_cost('blizzard elemental', ['5', 'U', 'U']).
card_cmc('blizzard elemental', 7).
card_layout('blizzard elemental', 'normal').
card_power('blizzard elemental', 5).
card_toughness('blizzard elemental', 5).

% Found in: CSP
card_name('blizzard specter', 'Blizzard Specter').
card_type('blizzard specter', 'Snow Creature — Specter').
card_types('blizzard specter', ['Creature']).
card_subtypes('blizzard specter', ['Specter']).
card_supertypes('blizzard specter', ['Snow']).
card_colors('blizzard specter', ['U', 'B']).
card_text('blizzard specter', 'Flying\nWhenever Blizzard Specter deals combat damage to a player, choose one —\n• That player returns a permanent he or she controls to its owner\'s hand.\n• That player discards a card.').
card_mana_cost('blizzard specter', ['2', 'U', 'B']).
card_cmc('blizzard specter', 4).
card_layout('blizzard specter', 'normal').
card_power('blizzard specter', 2).
card_toughness('blizzard specter', 3).

% Found in: ULG
card_name('bloated toad', 'Bloated Toad').
card_type('bloated toad', 'Creature — Frog').
card_types('bloated toad', ['Creature']).
card_subtypes('bloated toad', ['Frog']).
card_colors('bloated toad', ['G']).
card_text('bloated toad', 'Protection from blue\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('bloated toad', ['2', 'G']).
card_cmc('bloated toad', 3).
card_layout('bloated toad', 'normal').
card_power('bloated toad', 2).
card_toughness('bloated toad', 2).

% Found in: MMQ
card_name('blockade runner', 'Blockade Runner').
card_type('blockade runner', 'Creature — Merfolk').
card_types('blockade runner', ['Creature']).
card_subtypes('blockade runner', ['Merfolk']).
card_colors('blockade runner', ['U']).
card_text('blockade runner', '{U}: Blockade Runner can\'t be blocked this turn.').
card_mana_cost('blockade runner', ['3', 'U']).
card_cmc('blockade runner', 4).
card_layout('blockade runner', 'normal').
card_power('blockade runner', 2).
card_toughness('blockade runner', 2).

% Found in: RAV
card_name('blockbuster', 'Blockbuster').
card_type('blockbuster', 'Enchantment').
card_types('blockbuster', ['Enchantment']).
card_subtypes('blockbuster', []).
card_colors('blockbuster', ['R']).
card_text('blockbuster', '{1}{R}, Sacrifice Blockbuster: Blockbuster deals 3 damage to each tapped creature and each player.').
card_mana_cost('blockbuster', ['3', 'R', 'R']).
card_cmc('blockbuster', 5).
card_layout('blockbuster', 'normal').

% Found in: DGM
card_name('blood', 'Blood').
card_type('blood', 'Sorcery').
card_types('blood', ['Sorcery']).
card_subtypes('blood', []).
card_colors('blood', ['R', 'G']).
card_text('blood', 'Target creature you control deals damage equal to its power to target creature or player.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('blood', ['R', 'G']).
card_cmc('blood', 2).
card_layout('blood', 'split').

% Found in: AVR
card_name('blood artist', 'Blood Artist').
card_type('blood artist', 'Creature — Vampire').
card_types('blood artist', ['Creature']).
card_subtypes('blood artist', ['Vampire']).
card_colors('blood artist', ['B']).
card_text('blood artist', 'Whenever Blood Artist or another creature dies, target player loses 1 life and you gain 1 life.').
card_mana_cost('blood artist', ['1', 'B']).
card_cmc('blood artist', 2).
card_layout('blood artist', 'normal').
card_power('blood artist', 0).
card_toughness('blood artist', 1).

% Found in: M14
card_name('blood bairn', 'Blood Bairn').
card_type('blood bairn', 'Creature — Vampire').
card_types('blood bairn', ['Creature']).
card_subtypes('blood bairn', ['Vampire']).
card_colors('blood bairn', ['B']).
card_text('blood bairn', 'Sacrifice another creature: Blood Bairn gets +2/+2 until end of turn.').
card_mana_cost('blood bairn', ['2', 'B']).
card_cmc('blood bairn', 3).
card_layout('blood bairn', 'normal').
card_power('blood bairn', 2).
card_toughness('blood bairn', 2).

% Found in: DGM
card_name('blood baron of vizkopa', 'Blood Baron of Vizkopa').
card_type('blood baron of vizkopa', 'Creature — Vampire').
card_types('blood baron of vizkopa', ['Creature']).
card_subtypes('blood baron of vizkopa', ['Vampire']).
card_colors('blood baron of vizkopa', ['W', 'B']).
card_text('blood baron of vizkopa', 'Lifelink, protection from white and from black\nAs long as you have 30 or more life and an opponent has 10 or less life, Blood Baron of Vizkopa gets +6/+6 and has flying.').
card_mana_cost('blood baron of vizkopa', ['3', 'W', 'B']).
card_cmc('blood baron of vizkopa', 5).
card_layout('blood baron of vizkopa', 'normal').
card_power('blood baron of vizkopa', 4).
card_toughness('blood baron of vizkopa', 4).

% Found in: LGN
card_name('blood celebrant', 'Blood Celebrant').
card_type('blood celebrant', 'Creature — Human Cleric').
card_types('blood celebrant', ['Creature']).
card_subtypes('blood celebrant', ['Human', 'Cleric']).
card_colors('blood celebrant', ['B']).
card_text('blood celebrant', '{B}, Pay 1 life: Add one mana of any color to your mana pool.').
card_mana_cost('blood celebrant', ['B']).
card_cmc('blood celebrant', 1).
card_layout('blood celebrant', 'normal').
card_power('blood celebrant', 1).
card_toughness('blood celebrant', 1).

% Found in: SOK
card_name('blood clock', 'Blood Clock').
card_type('blood clock', 'Artifact').
card_types('blood clock', ['Artifact']).
card_subtypes('blood clock', []).
card_colors('blood clock', []).
card_text('blood clock', 'At the beginning of each player\'s upkeep, that player returns a permanent he or she controls to its owner\'s hand unless he or she pays 2 life.').
card_mana_cost('blood clock', ['4']).
card_cmc('blood clock', 4).
card_layout('blood clock', 'normal').

% Found in: DIS, EXP, RTR
card_name('blood crypt', 'Blood Crypt').
card_type('blood crypt', 'Land — Swamp Mountain').
card_types('blood crypt', ['Land']).
card_subtypes('blood crypt', ['Swamp', 'Mountain']).
card_colors('blood crypt', []).
card_text('blood crypt', '({T}: Add {B} or {R} to your mana pool.)\nAs Blood Crypt enters the battlefield, you may pay 2 life. If you don\'t, Blood Crypt enters the battlefield tapped.').
card_layout('blood crypt', 'normal').

% Found in: ALA
card_name('blood cultist', 'Blood Cultist').
card_type('blood cultist', 'Creature — Human Wizard').
card_types('blood cultist', ['Creature']).
card_subtypes('blood cultist', ['Human', 'Wizard']).
card_colors('blood cultist', ['B', 'R']).
card_text('blood cultist', '{T}: Blood Cultist deals 1 damage to target creature.\nWhenever a creature dealt damage by Blood Cultist this turn dies, put a +1/+1 counter on Blood Cultist.').
card_mana_cost('blood cultist', ['1', 'B', 'R']).
card_cmc('blood cultist', 3).
card_layout('blood cultist', 'normal').
card_power('blood cultist', 1).
card_toughness('blood cultist', 1).

% Found in: DKA
card_name('blood feud', 'Blood Feud').
card_type('blood feud', 'Sorcery').
card_types('blood feud', ['Sorcery']).
card_subtypes('blood feud', []).
card_colors('blood feud', ['R']).
card_text('blood feud', 'Target creature fights another target creature. (Each deals damage equal to its power to the other.)').
card_mana_cost('blood feud', ['4', 'R', 'R']).
card_cmc('blood feud', 6).
card_layout('blood feud', 'normal').

% Found in: TMP
card_name('blood frenzy', 'Blood Frenzy').
card_type('blood frenzy', 'Instant').
card_types('blood frenzy', ['Instant']).
card_subtypes('blood frenzy', []).
card_colors('blood frenzy', ['R']).
card_text('blood frenzy', 'Cast Blood Frenzy only before the combat damage step.\nTarget attacking or blocking creature gets +4/+0 until end of turn. Destroy that creature at the beginning of the next end step.').
card_mana_cost('blood frenzy', ['1', 'R']).
card_cmc('blood frenzy', 2).
card_layout('blood frenzy', 'normal').

% Found in: RAV
card_name('blood funnel', 'Blood Funnel').
card_type('blood funnel', 'Enchantment').
card_types('blood funnel', ['Enchantment']).
card_subtypes('blood funnel', []).
card_colors('blood funnel', ['B']).
card_text('blood funnel', 'Noncreature spells you cast cost {2} less to cast.\nWhenever you cast a noncreature spell, counter that spell unless you sacrifice a creature.').
card_mana_cost('blood funnel', ['1', 'B']).
card_cmc('blood funnel', 2).
card_layout('blood funnel', 'normal').

% Found in: M15
card_name('blood host', 'Blood Host').
card_type('blood host', 'Creature — Vampire').
card_types('blood host', ['Creature']).
card_subtypes('blood host', ['Vampire']).
card_colors('blood host', ['B']).
card_text('blood host', '{1}{B}, Sacrifice another creature: Put a +1/+1 counter on Blood Host and you gain 2 life.').
card_mana_cost('blood host', ['3', 'B', 'B']).
card_cmc('blood host', 5).
card_layout('blood host', 'normal').
card_power('blood host', 3).
card_toughness('blood host', 3).

% Found in: MMQ
card_name('blood hound', 'Blood Hound').
card_type('blood hound', 'Creature — Hound').
card_types('blood hound', ['Creature']).
card_subtypes('blood hound', ['Hound']).
card_colors('blood hound', ['R']).
card_text('blood hound', 'Whenever you\'re dealt damage, you may put that many +1/+1 counters on Blood Hound.\nAt the beginning of your end step, remove all +1/+1 counters from Blood Hound.').
card_mana_cost('blood hound', ['2', 'R']).
card_cmc('blood hound', 3).
card_layout('blood hound', 'normal').
card_power('blood hound', 1).
card_toughness('blood hound', 1).

% Found in: PLC, pCMP
card_name('blood knight', 'Blood Knight').
card_type('blood knight', 'Creature — Human Knight').
card_types('blood knight', ['Creature']).
card_subtypes('blood knight', ['Human', 'Knight']).
card_colors('blood knight', ['R']).
card_text('blood knight', 'First strike, protection from white').
card_mana_cost('blood knight', ['R', 'R']).
card_cmc('blood knight', 2).
card_layout('blood knight', 'normal').
card_power('blood knight', 2).
card_toughness('blood knight', 2).

% Found in: 4ED, 5ED, LEG, ME3
card_name('blood lust', 'Blood Lust').
card_type('blood lust', 'Instant').
card_types('blood lust', ['Instant']).
card_subtypes('blood lust', []).
card_colors('blood lust', ['R']).
card_text('blood lust', 'If target creature has toughness 5 or greater, it gets +4/-4 until end of turn. Otherwise, it gets +4/-X until end of turn, where X is its toughness minus 1.').
card_mana_cost('blood lust', ['1', 'R']).
card_cmc('blood lust', 2).
card_layout('blood lust', 'normal').

% Found in: 8ED, 9ED, CHR, DRK, MMA
card_name('blood moon', 'Blood Moon').
card_type('blood moon', 'Enchantment').
card_types('blood moon', ['Enchantment']).
card_subtypes('blood moon', []).
card_colors('blood moon', ['R']).
card_text('blood moon', 'Nonbasic lands are Mountains.').
card_mana_cost('blood moon', ['2', 'R']).
card_cmc('blood moon', 3).
card_layout('blood moon', 'normal').

% Found in: MMQ
card_name('blood oath', 'Blood Oath').
card_type('blood oath', 'Instant').
card_types('blood oath', ['Instant']).
card_subtypes('blood oath', []).
card_colors('blood oath', ['R']).
card_text('blood oath', 'Choose a card type. Target opponent reveals his or her hand. Blood Oath deals 3 damage to that player for each card of the chosen type revealed this way. (Artifact, creature, enchantment, instant, land, planeswalker, sorcery, and tribal are card types.)').
card_mana_cost('blood oath', ['3', 'R']).
card_cmc('blood oath', 4).
card_layout('blood oath', 'normal').

% Found in: CHR, DRK
card_name('blood of the martyr', 'Blood of the Martyr').
card_type('blood of the martyr', 'Instant').
card_types('blood of the martyr', ['Instant']).
card_subtypes('blood of the martyr', []).
card_colors('blood of the martyr', ['W']).
card_text('blood of the martyr', 'Until end of turn, if damage would be dealt to any creature, you may have that damage dealt to you instead.').
card_mana_cost('blood of the martyr', ['W', 'W', 'W']).
card_cmc('blood of the martyr', 3).
card_layout('blood of the martyr', 'normal').

% Found in: DDL, M12, MM2
card_name('blood ogre', 'Blood Ogre').
card_type('blood ogre', 'Creature — Ogre Warrior').
card_types('blood ogre', ['Creature']).
card_subtypes('blood ogre', ['Ogre', 'Warrior']).
card_colors('blood ogre', ['R']).
card_text('blood ogre', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_mana_cost('blood ogre', ['2', 'R']).
card_cmc('blood ogre', 3).
card_layout('blood ogre', 'normal').
card_power('blood ogre', 2).
card_toughness('blood ogre', 2).

% Found in: 6ED, 7ED, TMP
card_name('blood pet', 'Blood Pet').
card_type('blood pet', 'Creature — Thrull').
card_types('blood pet', ['Creature']).
card_subtypes('blood pet', ['Thrull']).
card_colors('blood pet', ['B']).
card_text('blood pet', 'Sacrifice Blood Pet: Add {B} to your mana pool.').
card_mana_cost('blood pet', ['B']).
card_cmc('blood pet', 1).
card_layout('blood pet', 'normal').
card_power('blood pet', 1).
card_toughness('blood pet', 1).

% Found in: M13
card_name('blood reckoning', 'Blood Reckoning').
card_type('blood reckoning', 'Enchantment').
card_types('blood reckoning', ['Enchantment']).
card_subtypes('blood reckoning', []).
card_colors('blood reckoning', ['B']).
card_text('blood reckoning', 'Whenever a creature attacks you or a planeswalker you control, that creature\'s controller loses 1 life.').
card_mana_cost('blood reckoning', ['3', 'B']).
card_cmc('blood reckoning', 4).
card_layout('blood reckoning', 'normal').

% Found in: C13, CHK
card_name('blood rites', 'Blood Rites').
card_type('blood rites', 'Enchantment').
card_types('blood rites', ['Enchantment']).
card_subtypes('blood rites', []).
card_colors('blood rites', ['R']).
card_text('blood rites', '{1}{R}, Sacrifice a creature: Blood Rites deals 2 damage to target creature or player.').
card_mana_cost('blood rites', ['3', 'R', 'R']).
card_cmc('blood rites', 5).
card_layout('blood rites', 'normal').

% Found in: DGM
card_name('blood scrivener', 'Blood Scrivener').
card_type('blood scrivener', 'Creature — Zombie Wizard').
card_types('blood scrivener', ['Creature']).
card_subtypes('blood scrivener', ['Zombie', 'Wizard']).
card_colors('blood scrivener', ['B']).
card_text('blood scrivener', 'If you would draw a card while you have no cards in hand, instead draw two cards and lose 1 life.').
card_mana_cost('blood scrivener', ['1', 'B']).
card_cmc('blood scrivener', 2).
card_layout('blood scrivener', 'normal').
card_power('blood scrivener', 2).
card_toughness('blood scrivener', 1).

% Found in: M12, ZEN
card_name('blood seeker', 'Blood Seeker').
card_type('blood seeker', 'Creature — Vampire Shaman').
card_types('blood seeker', ['Creature']).
card_subtypes('blood seeker', ['Vampire', 'Shaman']).
card_colors('blood seeker', ['B']).
card_text('blood seeker', 'Whenever a creature enters the battlefield under an opponent\'s control, you may have that player lose 1 life.').
card_mana_cost('blood seeker', ['1', 'B']).
card_cmc('blood seeker', 2).
card_layout('blood seeker', 'normal').
card_power('blood seeker', 1).
card_toughness('blood seeker', 1).

% Found in: CHK
card_name('blood speaker', 'Blood Speaker').
card_type('blood speaker', 'Creature — Ogre Shaman').
card_types('blood speaker', ['Creature']).
card_subtypes('blood speaker', ['Ogre', 'Shaman']).
card_colors('blood speaker', ['B']).
card_text('blood speaker', 'At the beginning of your upkeep, you may sacrifice Blood Speaker. If you do, search your library for a Demon card, reveal that card, put it into your hand, then shuffle your library.\nWhenever a Demon enters the battlefield under your control, return Blood Speaker from your graveyard to your hand.').
card_mana_cost('blood speaker', ['3', 'B']).
card_cmc('blood speaker', 4).
card_layout('blood speaker', 'normal').
card_power('blood speaker', 3).
card_toughness('blood speaker', 2).

% Found in: M11
card_name('blood tithe', 'Blood Tithe').
card_type('blood tithe', 'Sorcery').
card_types('blood tithe', ['Sorcery']).
card_subtypes('blood tithe', []).
card_colors('blood tithe', ['B']).
card_text('blood tithe', 'Each opponent loses 3 life. You gain life equal to the life lost this way.').
card_mana_cost('blood tithe', ['3', 'B']).
card_cmc('blood tithe', 4).
card_layout('blood tithe', 'normal').

% Found in: ZEN
card_name('blood tribute', 'Blood Tribute').
card_type('blood tribute', 'Sorcery').
card_types('blood tribute', ['Sorcery']).
card_subtypes('blood tribute', []).
card_colors('blood tribute', ['B']).
card_text('blood tribute', 'Kicker—Tap an untapped Vampire you control. (You may tap a Vampire you control in addition to any other costs as you cast this spell.)\nTarget opponent loses half his or her life, rounded up. If Blood Tribute was kicked, you gain life equal to the life lost this way.').
card_mana_cost('blood tribute', ['4', 'B', 'B']).
card_cmc('blood tribute', 6).
card_layout('blood tribute', 'normal').

% Found in: CON
card_name('blood tyrant', 'Blood Tyrant').
card_type('blood tyrant', 'Creature — Vampire').
card_types('blood tyrant', ['Creature']).
card_subtypes('blood tyrant', ['Vampire']).
card_colors('blood tyrant', ['U', 'B', 'R']).
card_text('blood tyrant', 'Flying, trample\nAt the beginning of your upkeep, each player loses 1 life. Put a +1/+1 counter on Blood Tyrant for each 1 life lost this way.\nWhenever a player loses the game, put five +1/+1 counters on Blood Tyrant.').
card_mana_cost('blood tyrant', ['4', 'U', 'B', 'R']).
card_cmc('blood tyrant', 7).
card_layout('blood tyrant', 'normal').
card_power('blood tyrant', 5).
card_toughness('blood tyrant', 5).

% Found in: USG
card_name('blood vassal', 'Blood Vassal').
card_type('blood vassal', 'Creature — Thrull').
card_types('blood vassal', ['Creature']).
card_subtypes('blood vassal', ['Thrull']).
card_colors('blood vassal', ['B']).
card_text('blood vassal', 'Sacrifice Blood Vassal: Add {B}{B} to your mana pool.').
card_mana_cost('blood vassal', ['2', 'B']).
card_cmc('blood vassal', 3).
card_layout('blood vassal', 'normal').
card_power('blood vassal', 2).
card_toughness('blood vassal', 2).

% Found in: DTK
card_name('blood-chin fanatic', 'Blood-Chin Fanatic').
card_type('blood-chin fanatic', 'Creature — Orc Warrior').
card_types('blood-chin fanatic', ['Creature']).
card_subtypes('blood-chin fanatic', ['Orc', 'Warrior']).
card_colors('blood-chin fanatic', ['B']).
card_text('blood-chin fanatic', '{1}{B}, Sacrifice another Warrior creature: Target player loses X life and you gain X life, where X is the sacrificed creature\'s power.').
card_mana_cost('blood-chin fanatic', ['1', 'B', 'B']).
card_cmc('blood-chin fanatic', 3).
card_layout('blood-chin fanatic', 'normal').
card_power('blood-chin fanatic', 3).
card_toughness('blood-chin fanatic', 3).

% Found in: DTK
card_name('blood-chin rager', 'Blood-Chin Rager').
card_type('blood-chin rager', 'Creature — Human Warrior').
card_types('blood-chin rager', ['Creature']).
card_subtypes('blood-chin rager', ['Human', 'Warrior']).
card_colors('blood-chin rager', ['B']).
card_text('blood-chin rager', 'Whenever Blood-Chin Rager attacks, Warrior creatures you control gain menace until end of turn. (They can\'t be blocked except by two or more creatures.)').
card_mana_cost('blood-chin rager', ['1', 'B']).
card_cmc('blood-chin rager', 2).
card_layout('blood-chin rager', 'normal').
card_power('blood-chin rager', 2).
card_toughness('blood-chin rager', 2).

% Found in: ORI
card_name('blood-cursed knight', 'Blood-Cursed Knight').
card_type('blood-cursed knight', 'Creature — Vampire Knight').
card_types('blood-cursed knight', ['Creature']).
card_subtypes('blood-cursed knight', ['Vampire', 'Knight']).
card_colors('blood-cursed knight', ['W', 'B']).
card_text('blood-cursed knight', 'As long as you control an enchantment, Blood-Cursed Knight gets +1/+1 and has lifelink. (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('blood-cursed knight', ['1', 'W', 'B']).
card_cmc('blood-cursed knight', 3).
card_layout('blood-cursed knight', 'normal').
card_power('blood-cursed knight', 3).
card_toughness('blood-cursed knight', 2).

% Found in: THS
card_name('blood-toll harpy', 'Blood-Toll Harpy').
card_type('blood-toll harpy', 'Creature — Harpy').
card_types('blood-toll harpy', ['Creature']).
card_subtypes('blood-toll harpy', ['Harpy']).
card_colors('blood-toll harpy', ['B']).
card_text('blood-toll harpy', 'Flying\nWhen Blood-Toll Harpy enters the battlefield, each player loses 1 life.').
card_mana_cost('blood-toll harpy', ['2', 'B']).
card_cmc('blood-toll harpy', 3).
card_layout('blood-toll harpy', 'normal').
card_power('blood-toll harpy', 2).
card_toughness('blood-toll harpy', 1).

% Found in: RAV
card_name('bloodbond march', 'Bloodbond March').
card_type('bloodbond march', 'Enchantment').
card_types('bloodbond march', ['Enchantment']).
card_subtypes('bloodbond march', []).
card_colors('bloodbond march', ['B', 'G']).
card_text('bloodbond march', 'Whenever a player casts a creature spell, each player returns all cards with the same name as that spell from his or her graveyard to the battlefield.').
card_mana_cost('bloodbond march', ['2', 'B', 'G']).
card_cmc('bloodbond march', 4).
card_layout('bloodbond march', 'normal').

% Found in: BFZ
card_name('bloodbond vampire', 'Bloodbond Vampire').
card_type('bloodbond vampire', 'Creature — Vampire Shaman Ally').
card_types('bloodbond vampire', ['Creature']).
card_subtypes('bloodbond vampire', ['Vampire', 'Shaman', 'Ally']).
card_colors('bloodbond vampire', ['B']).
card_text('bloodbond vampire', 'Whenever you gain life, put a +1/+1 counter on Bloodbond Vampire.').
card_mana_cost('bloodbond vampire', ['2', 'B', 'B']).
card_cmc('bloodbond vampire', 4).
card_layout('bloodbond vampire', 'normal').
card_power('bloodbond vampire', 3).
card_toughness('bloodbond vampire', 3).

% Found in: ARB, PC2, pFNM
card_name('bloodbraid elf', 'Bloodbraid Elf').
card_type('bloodbraid elf', 'Creature — Elf Berserker').
card_types('bloodbraid elf', ['Creature']).
card_subtypes('bloodbraid elf', ['Elf', 'Berserker']).
card_colors('bloodbraid elf', ['R', 'G']).
card_text('bloodbraid elf', 'Haste\nCascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)').
card_mana_cost('bloodbraid elf', ['2', 'R', 'G']).
card_cmc('bloodbraid elf', 4).
card_layout('bloodbraid elf', 'normal').
card_power('bloodbraid elf', 3).
card_toughness('bloodbraid elf', 2).

% Found in: ZEN
card_name('bloodchief ascension', 'Bloodchief Ascension').
card_type('bloodchief ascension', 'Enchantment').
card_types('bloodchief ascension', ['Enchantment']).
card_subtypes('bloodchief ascension', []).
card_colors('bloodchief ascension', ['B']).
card_text('bloodchief ascension', 'At the beginning of each end step, if an opponent lost 2 or more life this turn, you may put a quest counter on Bloodchief Ascension. (Damage causes loss of life.)\nWhenever a card is put into an opponent\'s graveyard from anywhere, if Bloodchief Ascension has three or more quest counters on it, you may have that player lose 2 life. If you do, you gain 2 life.').
card_mana_cost('bloodchief ascension', ['B']).
card_cmc('bloodchief ascension', 1).
card_layout('bloodchief ascension', 'normal').

% Found in: M11
card_name('bloodcrazed goblin', 'Bloodcrazed Goblin').
card_type('bloodcrazed goblin', 'Creature — Goblin Berserker').
card_types('bloodcrazed goblin', ['Creature']).
card_subtypes('bloodcrazed goblin', ['Goblin', 'Berserker']).
card_colors('bloodcrazed goblin', ['R']).
card_text('bloodcrazed goblin', 'Bloodcrazed Goblin can\'t attack unless an opponent has been dealt damage this turn.').
card_mana_cost('bloodcrazed goblin', ['R']).
card_cmc('bloodcrazed goblin', 1).
card_layout('bloodcrazed goblin', 'normal').
card_power('bloodcrazed goblin', 2).
card_toughness('bloodcrazed goblin', 2).

% Found in: JOU
card_name('bloodcrazed hoplite', 'Bloodcrazed Hoplite').
card_type('bloodcrazed hoplite', 'Creature — Human Soldier').
card_types('bloodcrazed hoplite', ['Creature']).
card_subtypes('bloodcrazed hoplite', ['Human', 'Soldier']).
card_colors('bloodcrazed hoplite', ['B']).
card_text('bloodcrazed hoplite', 'Heroic — Whenever you cast a spell that targets Bloodcrazed Hoplite, put a +1/+1 counter on it.\nWhenever a +1/+1 counter is placed on Bloodcrazed Hoplite, remove a +1/+1 counter from target creature an opponent controls.').
card_mana_cost('bloodcrazed hoplite', ['1', 'B']).
card_cmc('bloodcrazed hoplite', 2).
card_layout('bloodcrazed hoplite', 'normal').
card_power('bloodcrazed hoplite', 2).
card_toughness('bloodcrazed hoplite', 1).

% Found in: ISD, pWPN
card_name('bloodcrazed neonate', 'Bloodcrazed Neonate').
card_type('bloodcrazed neonate', 'Creature — Vampire').
card_types('bloodcrazed neonate', ['Creature']).
card_subtypes('bloodcrazed neonate', ['Vampire']).
card_colors('bloodcrazed neonate', ['R']).
card_text('bloodcrazed neonate', 'Bloodcrazed Neonate attacks each turn if able.\nWhenever Bloodcrazed Neonate deals combat damage to a player, put a +1/+1 counter on it.').
card_mana_cost('bloodcrazed neonate', ['1', 'R']).
card_cmc('bloodcrazed neonate', 2).
card_layout('bloodcrazed neonate', 'normal').
card_power('bloodcrazed neonate', 2).
card_toughness('bloodcrazed neonate', 1).

% Found in: ODY
card_name('bloodcurdler', 'Bloodcurdler').
card_type('bloodcurdler', 'Creature — Horror').
card_types('bloodcurdler', ['Creature']).
card_subtypes('bloodcurdler', ['Horror']).
card_colors('bloodcurdler', ['B']).
card_text('bloodcurdler', 'Flying\nAt the beginning of your upkeep, put the top card of your library into your graveyard.\nThreshold — As long as seven or more cards are in your graveyard, Bloodcurdler gets +1/+1 and has \"At the beginning of your end step, exile two cards from your graveyard.\"').
card_mana_cost('bloodcurdler', ['1', 'B']).
card_cmc('bloodcurdler', 2).
card_layout('bloodcurdler', 'normal').
card_power('bloodcurdler', 1).
card_toughness('bloodcurdler', 1).

% Found in: PO2
card_name('bloodcurdling scream', 'Bloodcurdling Scream').
card_type('bloodcurdling scream', 'Sorcery').
card_types('bloodcurdling scream', ['Sorcery']).
card_subtypes('bloodcurdling scream', []).
card_colors('bloodcurdling scream', ['B']).
card_text('bloodcurdling scream', 'Target creature gets +X/+0 until end of turn.').
card_mana_cost('bloodcurdling scream', ['X', 'B']).
card_cmc('bloodcurdling scream', 1).
card_layout('bloodcurdling scream', 'normal').

% Found in: FRF, KTK
card_name('bloodfell caves', 'Bloodfell Caves').
card_type('bloodfell caves', 'Land').
card_types('bloodfell caves', ['Land']).
card_subtypes('bloodfell caves', []).
card_colors('bloodfell caves', []).
card_text('bloodfell caves', 'Bloodfell Caves enters the battlefield tapped.\nWhen Bloodfell Caves enters the battlefield, you gain 1 life.\n{T}: Add {B} or {R} to your mana pool.').
card_layout('bloodfell caves', 'normal').

% Found in: 10E, 9ED, APC, DDI
card_name('bloodfire colossus', 'Bloodfire Colossus').
card_type('bloodfire colossus', 'Creature — Giant').
card_types('bloodfire colossus', ['Creature']).
card_subtypes('bloodfire colossus', ['Giant']).
card_colors('bloodfire colossus', ['R']).
card_text('bloodfire colossus', '{R}, Sacrifice Bloodfire Colossus: Bloodfire Colossus deals 6 damage to each creature and each player.').
card_mana_cost('bloodfire colossus', ['6', 'R', 'R']).
card_cmc('bloodfire colossus', 8).
card_layout('bloodfire colossus', 'normal').
card_power('bloodfire colossus', 6).
card_toughness('bloodfire colossus', 6).

% Found in: APC
card_name('bloodfire dwarf', 'Bloodfire Dwarf').
card_type('bloodfire dwarf', 'Creature — Dwarf').
card_types('bloodfire dwarf', ['Creature']).
card_subtypes('bloodfire dwarf', ['Dwarf']).
card_colors('bloodfire dwarf', ['R']).
card_text('bloodfire dwarf', '{R}, Sacrifice Bloodfire Dwarf: Bloodfire Dwarf deals 1 damage to each creature without flying.').
card_mana_cost('bloodfire dwarf', ['R']).
card_cmc('bloodfire dwarf', 1).
card_layout('bloodfire dwarf', 'normal').
card_power('bloodfire dwarf', 1).
card_toughness('bloodfire dwarf', 1).

% Found in: FRF
card_name('bloodfire enforcers', 'Bloodfire Enforcers').
card_type('bloodfire enforcers', 'Creature — Human Monk').
card_types('bloodfire enforcers', ['Creature']).
card_subtypes('bloodfire enforcers', ['Human', 'Monk']).
card_colors('bloodfire enforcers', ['R']).
card_text('bloodfire enforcers', 'Bloodfire Enforcers has first strike and trample as long as an instant card and a sorcery card are in your graveyard.').
card_mana_cost('bloodfire enforcers', ['3', 'R']).
card_cmc('bloodfire enforcers', 4).
card_layout('bloodfire enforcers', 'normal').
card_power('bloodfire enforcers', 5).
card_toughness('bloodfire enforcers', 2).

% Found in: KTK
card_name('bloodfire expert', 'Bloodfire Expert').
card_type('bloodfire expert', 'Creature — Efreet Monk').
card_types('bloodfire expert', ['Creature']).
card_subtypes('bloodfire expert', ['Efreet', 'Monk']).
card_colors('bloodfire expert', ['R']).
card_text('bloodfire expert', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_mana_cost('bloodfire expert', ['2', 'R']).
card_cmc('bloodfire expert', 3).
card_layout('bloodfire expert', 'normal').
card_power('bloodfire expert', 3).
card_toughness('bloodfire expert', 1).

% Found in: APC
card_name('bloodfire infusion', 'Bloodfire Infusion').
card_type('bloodfire infusion', 'Enchantment — Aura').
card_types('bloodfire infusion', ['Enchantment']).
card_subtypes('bloodfire infusion', ['Aura']).
card_colors('bloodfire infusion', ['R']).
card_text('bloodfire infusion', 'Enchant creature you control\n{R}, Sacrifice enchanted creature: Bloodfire Infusion deals damage equal to the sacrificed creature\'s power to each creature.').
card_mana_cost('bloodfire infusion', ['2', 'R']).
card_cmc('bloodfire infusion', 3).
card_layout('bloodfire infusion', 'normal').

% Found in: APC, DDI
card_name('bloodfire kavu', 'Bloodfire Kavu').
card_type('bloodfire kavu', 'Creature — Kavu').
card_types('bloodfire kavu', ['Creature']).
card_subtypes('bloodfire kavu', ['Kavu']).
card_colors('bloodfire kavu', ['R']).
card_text('bloodfire kavu', '{R}, Sacrifice Bloodfire Kavu: Bloodfire Kavu deals 2 damage to each creature.').
card_mana_cost('bloodfire kavu', ['2', 'R', 'R']).
card_cmc('bloodfire kavu', 4).
card_layout('bloodfire kavu', 'normal').
card_power('bloodfire kavu', 2).
card_toughness('bloodfire kavu', 2).

% Found in: KTK
card_name('bloodfire mentor', 'Bloodfire Mentor').
card_type('bloodfire mentor', 'Creature — Efreet Shaman').
card_types('bloodfire mentor', ['Creature']).
card_subtypes('bloodfire mentor', ['Efreet', 'Shaman']).
card_colors('bloodfire mentor', ['R']).
card_text('bloodfire mentor', '{2}{U}, {T}: Draw a card, then discard a card.').
card_mana_cost('bloodfire mentor', ['2', 'R']).
card_cmc('bloodfire mentor', 3).
card_layout('bloodfire mentor', 'normal').
card_power('bloodfire mentor', 0).
card_toughness('bloodfire mentor', 5).

% Found in: AVR
card_name('bloodflow connoisseur', 'Bloodflow Connoisseur').
card_type('bloodflow connoisseur', 'Creature — Vampire').
card_types('bloodflow connoisseur', ['Creature']).
card_subtypes('bloodflow connoisseur', ['Vampire']).
card_colors('bloodflow connoisseur', ['B']).
card_text('bloodflow connoisseur', 'Sacrifice a creature: Put a +1/+1 counter on Bloodflow Connoisseur.').
card_mana_cost('bloodflow connoisseur', ['2', 'B']).
card_cmc('bloodflow connoisseur', 3).
card_layout('bloodflow connoisseur', 'normal').
card_power('bloodflow connoisseur', 1).
card_toughness('bloodflow connoisseur', 1).

% Found in: RTR
card_name('bloodfray giant', 'Bloodfray Giant').
card_type('bloodfray giant', 'Creature — Giant').
card_types('bloodfray giant', ['Creature']).
card_subtypes('bloodfray giant', ['Giant']).
card_colors('bloodfray giant', ['R']).
card_text('bloodfray giant', 'Trample\nUnleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_mana_cost('bloodfray giant', ['2', 'R', 'R']).
card_cmc('bloodfray giant', 4).
card_layout('bloodfray giant', 'normal').
card_power('bloodfray giant', 4).
card_toughness('bloodfray giant', 3).

% Found in: ZEN
card_name('bloodghast', 'Bloodghast').
card_type('bloodghast', 'Creature — Vampire Spirit').
card_types('bloodghast', ['Creature']).
card_subtypes('bloodghast', ['Vampire', 'Spirit']).
card_colors('bloodghast', ['B']).
card_text('bloodghast', 'Bloodghast can\'t block.\nBloodghast has haste as long as an opponent has 10 or less life.\nLandfall — Whenever a land enters the battlefield under your control, you may return Bloodghast from your graveyard to the battlefield.').
card_mana_cost('bloodghast', ['B', 'B']).
card_cmc('bloodghast', 2).
card_layout('bloodghast', 'normal').
card_power('bloodghast', 2).
card_toughness('bloodghast', 1).

% Found in: C14, ISD
card_name('bloodgift demon', 'Bloodgift Demon').
card_type('bloodgift demon', 'Creature — Demon').
card_types('bloodgift demon', ['Creature']).
card_subtypes('bloodgift demon', ['Demon']).
card_colors('bloodgift demon', ['B']).
card_text('bloodgift demon', 'Flying\nAt the beginning of your upkeep, target player draws a card and loses 1 life.').
card_mana_cost('bloodgift demon', ['3', 'B', 'B']).
card_cmc('bloodgift demon', 5).
card_layout('bloodgift demon', 'normal').
card_power('bloodgift demon', 5).
card_toughness('bloodgift demon', 4).

% Found in: CON
card_name('bloodhall ooze', 'Bloodhall Ooze').
card_type('bloodhall ooze', 'Creature — Ooze').
card_types('bloodhall ooze', ['Creature']).
card_subtypes('bloodhall ooze', ['Ooze']).
card_colors('bloodhall ooze', ['R']).
card_text('bloodhall ooze', 'At the beginning of your upkeep, if you control a black permanent, you may put a +1/+1 counter on Bloodhall Ooze.\nAt the beginning of your upkeep, if you control a green permanent, you may put a +1/+1 counter on Bloodhall Ooze.').
card_mana_cost('bloodhall ooze', ['R']).
card_cmc('bloodhall ooze', 1).
card_layout('bloodhall ooze', 'normal').
card_power('bloodhall ooze', 1).
card_toughness('bloodhall ooze', 1).

% Found in: PC2
card_name('bloodhill bastion', 'Bloodhill Bastion').
card_type('bloodhill bastion', 'Plane — Equilor').
card_types('bloodhill bastion', ['Plane']).
card_subtypes('bloodhill bastion', ['Equilor']).
card_colors('bloodhill bastion', []).
card_text('bloodhill bastion', 'Whenever a creature enters the battlefield, it gains double strike and haste until end of turn.\nWhenever you roll {C}, exile target nontoken creature you control, then return it to the battlefield under your control.').
card_layout('bloodhill bastion', 'plane').

% Found in: M13
card_name('bloodhunter bat', 'Bloodhunter Bat').
card_type('bloodhunter bat', 'Creature — Bat').
card_types('bloodhunter bat', ['Creature']).
card_subtypes('bloodhunter bat', ['Bat']).
card_colors('bloodhunter bat', ['B']).
card_text('bloodhunter bat', 'Flying\nWhen Bloodhunter Bat enters the battlefield, target player loses 2 life and you gain 2 life.').
card_mana_cost('bloodhunter bat', ['3', 'B']).
card_cmc('bloodhunter bat', 4).
card_layout('bloodhunter bat', 'normal').
card_power('bloodhunter bat', 2).
card_toughness('bloodhunter bat', 2).

% Found in: WWK
card_name('bloodhusk ritualist', 'Bloodhusk Ritualist').
card_type('bloodhusk ritualist', 'Creature — Vampire Shaman').
card_types('bloodhusk ritualist', ['Creature']).
card_subtypes('bloodhusk ritualist', ['Vampire', 'Shaman']).
card_colors('bloodhusk ritualist', ['B']).
card_text('bloodhusk ritualist', 'Multikicker {B} (You may pay an additional {B} any number of times as you cast this spell.)\nWhen Bloodhusk Ritualist enters the battlefield, target opponent discards a card for each time it was kicked.').
card_mana_cost('bloodhusk ritualist', ['2', 'B']).
card_cmc('bloodhusk ritualist', 3).
card_layout('bloodhusk ritualist', 'normal').
card_power('bloodhusk ritualist', 2).
card_toughness('bloodhusk ritualist', 2).

% Found in: EVE
card_name('bloodied ghost', 'Bloodied Ghost').
card_type('bloodied ghost', 'Creature — Spirit').
card_types('bloodied ghost', ['Creature']).
card_subtypes('bloodied ghost', ['Spirit']).
card_colors('bloodied ghost', ['W', 'B']).
card_text('bloodied ghost', 'Flying\nBloodied Ghost enters the battlefield with a -1/-1 counter on it.').
card_mana_cost('bloodied ghost', ['1', 'W/B', 'W/B']).
card_cmc('bloodied ghost', 3).
card_layout('bloodied ghost', 'normal').
card_power('bloodied ghost', 3).
card_toughness('bloodied ghost', 3).

% Found in: UNH
card_name('bloodletter', 'Bloodletter').
card_type('bloodletter', 'Creature — Zombie').
card_types('bloodletter', ['Creature']).
card_subtypes('bloodletter', ['Zombie']).
card_colors('bloodletter', ['B']).
card_text('bloodletter', 'When the names of three or more nonland permanents begin with the same letter, sacrifice Bloodletter. If you do, it deals 2 damage to each creature and each player.').
card_mana_cost('bloodletter', ['2', 'B']).
card_cmc('bloodletter', 3).
card_layout('bloodletter', 'normal').
card_power('bloodletter', 2).
card_toughness('bloodletter', 3).

% Found in: RAV
card_name('bloodletter quill', 'Bloodletter Quill').
card_type('bloodletter quill', 'Artifact').
card_types('bloodletter quill', ['Artifact']).
card_subtypes('bloodletter quill', []).
card_colors('bloodletter quill', []).
card_text('bloodletter quill', '{2}, {T}, Put a blood counter on Bloodletter Quill: Draw a card, then you lose 1 life for each blood counter on Bloodletter Quill.\n{U}{B}: Remove a blood counter from Bloodletter Quill.').
card_mana_cost('bloodletter quill', ['3']).
card_cmc('bloodletter quill', 3).
card_layout('bloodletter quill', 'normal').

% Found in: ISD
card_name('bloodline keeper', 'Bloodline Keeper').
card_type('bloodline keeper', 'Creature — Vampire').
card_types('bloodline keeper', ['Creature']).
card_subtypes('bloodline keeper', ['Vampire']).
card_colors('bloodline keeper', ['B']).
card_text('bloodline keeper', 'Flying\n{T}: Put a 2/2 black Vampire creature token with flying onto the battlefield.\n{B}: Transform Bloodline Keeper. Activate this ability only if you control five or more Vampires.').
card_mana_cost('bloodline keeper', ['2', 'B', 'B']).
card_cmc('bloodline keeper', 4).
card_layout('bloodline keeper', 'double-faced').
card_power('bloodline keeper', 3).
card_toughness('bloodline keeper', 3).
card_sides('bloodline keeper', 'lord of lineage').

% Found in: ONS
card_name('bloodline shaman', 'Bloodline Shaman').
card_type('bloodline shaman', 'Creature — Elf Wizard Shaman').
card_types('bloodline shaman', ['Creature']).
card_subtypes('bloodline shaman', ['Elf', 'Wizard', 'Shaman']).
card_colors('bloodline shaman', ['G']).
card_text('bloodline shaman', '{T}: Choose a creature type. Reveal the top card of your library. If that card is a creature card of the chosen type, put it into your hand. Otherwise, put it into your graveyard.').
card_mana_cost('bloodline shaman', ['1', 'G']).
card_cmc('bloodline shaman', 2).
card_layout('bloodline shaman', 'normal').
card_power('bloodline shaman', 1).
card_toughness('bloodline shaman', 1).

% Found in: M12, pPRE
card_name('bloodlord of vaasgoth', 'Bloodlord of Vaasgoth').
card_type('bloodlord of vaasgoth', 'Creature — Vampire Warrior').
card_types('bloodlord of vaasgoth', ['Creature']).
card_subtypes('bloodlord of vaasgoth', ['Vampire', 'Warrior']).
card_colors('bloodlord of vaasgoth', ['B']).
card_text('bloodlord of vaasgoth', 'Bloodthirst 3 (If an opponent was dealt damage this turn, this creature enters the battlefield with three +1/+1 counters on it.)\nFlying\nWhenever you cast a Vampire creature spell, it gains bloodthirst 3.').
card_mana_cost('bloodlord of vaasgoth', ['3', 'B', 'B']).
card_cmc('bloodlord of vaasgoth', 5).
card_layout('bloodlord of vaasgoth', 'normal').
card_power('bloodlord of vaasgoth', 3).
card_toughness('bloodlord of vaasgoth', 3).

% Found in: DDG, DPA, SHM
card_name('bloodmark mentor', 'Bloodmark Mentor').
card_type('bloodmark mentor', 'Creature — Goblin Warrior').
card_types('bloodmark mentor', ['Creature']).
card_subtypes('bloodmark mentor', ['Goblin', 'Warrior']).
card_colors('bloodmark mentor', ['R']).
card_text('bloodmark mentor', 'Red creatures you control have first strike.').
card_mana_cost('bloodmark mentor', ['1', 'R']).
card_cmc('bloodmark mentor', 2).
card_layout('bloodmark mentor', 'normal').
card_power('bloodmark mentor', 1).
card_toughness('bloodmark mentor', 1).

% Found in: ALA
card_name('bloodpyre elemental', 'Bloodpyre Elemental').
card_type('bloodpyre elemental', 'Creature — Elemental').
card_types('bloodpyre elemental', ['Creature']).
card_subtypes('bloodpyre elemental', ['Elemental']).
card_colors('bloodpyre elemental', ['R']).
card_text('bloodpyre elemental', 'Sacrifice Bloodpyre Elemental: Bloodpyre Elemental deals 4 damage to target creature. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('bloodpyre elemental', ['4', 'R']).
card_cmc('bloodpyre elemental', 5).
card_layout('bloodpyre elemental', 'normal').
card_power('bloodpyre elemental', 4).
card_toughness('bloodpyre elemental', 1).

% Found in: DDK, M12
card_name('bloodrage vampire', 'Bloodrage Vampire').
card_type('bloodrage vampire', 'Creature — Vampire').
card_types('bloodrage vampire', ['Creature']).
card_subtypes('bloodrage vampire', ['Vampire']).
card_colors('bloodrage vampire', ['B']).
card_text('bloodrage vampire', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)').
card_mana_cost('bloodrage vampire', ['2', 'B']).
card_cmc('bloodrage vampire', 3).
card_layout('bloodrage vampire', 'normal').
card_power('bloodrage vampire', 3).
card_toughness('bloodrage vampire', 1).

% Found in: DDP, ROE
card_name('bloodrite invoker', 'Bloodrite Invoker').
card_type('bloodrite invoker', 'Creature — Vampire Shaman').
card_types('bloodrite invoker', ['Creature']).
card_subtypes('bloodrite invoker', ['Vampire', 'Shaman']).
card_colors('bloodrite invoker', ['B']).
card_text('bloodrite invoker', '{8}: Target player loses 3 life and you gain 3 life.').
card_mana_cost('bloodrite invoker', ['2', 'B']).
card_cmc('bloodrite invoker', 3).
card_layout('bloodrite invoker', 'normal').
card_power('bloodrite invoker', 3).
card_toughness('bloodrite invoker', 1).

% Found in: 10E, BTD, WTH
card_name('bloodrock cyclops', 'Bloodrock Cyclops').
card_type('bloodrock cyclops', 'Creature — Cyclops').
card_types('bloodrock cyclops', ['Creature']).
card_subtypes('bloodrock cyclops', ['Cyclops']).
card_colors('bloodrock cyclops', ['R']).
card_text('bloodrock cyclops', 'Bloodrock Cyclops attacks each turn if able.').
card_mana_cost('bloodrock cyclops', ['2', 'R']).
card_cmc('bloodrock cyclops', 3).
card_layout('bloodrock cyclops', 'normal').
card_power('bloodrock cyclops', 3).
card_toughness('bloodrock cyclops', 3).

% Found in: GPT
card_name('bloodscale prowler', 'Bloodscale Prowler').
card_type('bloodscale prowler', 'Creature — Viashino Warrior').
card_types('bloodscale prowler', ['Creature']).
card_subtypes('bloodscale prowler', ['Viashino', 'Warrior']).
card_colors('bloodscale prowler', ['R']).
card_text('bloodscale prowler', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)').
card_mana_cost('bloodscale prowler', ['2', 'R']).
card_cmc('bloodscale prowler', 3).
card_layout('bloodscale prowler', 'normal').
card_power('bloodscale prowler', 3).
card_toughness('bloodscale prowler', 1).

% Found in: MRD
card_name('bloodscent', 'Bloodscent').
card_type('bloodscent', 'Instant').
card_types('bloodscent', ['Instant']).
card_subtypes('bloodscent', []).
card_colors('bloodscent', ['G']).
card_text('bloodscent', 'All creatures able to block target creature this turn do so.').
card_mana_cost('bloodscent', ['3', 'G']).
card_cmc('bloodscent', 4).
card_layout('bloodscent', 'normal').

% Found in: SHM
card_name('bloodshed fever', 'Bloodshed Fever').
card_type('bloodshed fever', 'Enchantment — Aura').
card_types('bloodshed fever', ['Enchantment']).
card_subtypes('bloodshed fever', ['Aura']).
card_colors('bloodshed fever', ['R']).
card_text('bloodshed fever', 'Enchant creature\nEnchanted creature attacks each turn if able.').
card_mana_cost('bloodshed fever', ['R']).
card_cmc('bloodshed fever', 1).
card_layout('bloodshed fever', 'normal').

% Found in: 7ED, 8ED, UDS
card_name('bloodshot cyclops', 'Bloodshot Cyclops').
card_type('bloodshot cyclops', 'Creature — Cyclops Giant').
card_types('bloodshot cyclops', ['Creature']).
card_subtypes('bloodshot cyclops', ['Cyclops', 'Giant']).
card_colors('bloodshot cyclops', ['R']).
card_text('bloodshot cyclops', '{T}, Sacrifice a creature: Bloodshot Cyclops deals damage equal to the sacrificed creature\'s power to target creature or player.').
card_mana_cost('bloodshot cyclops', ['5', 'R']).
card_cmc('bloodshot cyclops', 6).
card_layout('bloodshot cyclops', 'normal').
card_power('bloodshot cyclops', 4).
card_toughness('bloodshot cyclops', 4).

% Found in: FUT, MM2, SOM
card_name('bloodshot trainee', 'Bloodshot Trainee').
card_type('bloodshot trainee', 'Creature — Goblin Warrior').
card_types('bloodshot trainee', ['Creature']).
card_subtypes('bloodshot trainee', ['Goblin', 'Warrior']).
card_colors('bloodshot trainee', ['R']).
card_text('bloodshot trainee', '{T}: Bloodshot Trainee deals 4 damage to target creature. Activate this ability only if Bloodshot Trainee\'s power is 4 or greater.').
card_mana_cost('bloodshot trainee', ['3', 'R']).
card_cmc('bloodshot trainee', 4).
card_layout('bloodshot trainee', 'normal').
card_power('bloodshot trainee', 2).
card_toughness('bloodshot trainee', 3).

% Found in: KTK, pPRE
card_name('bloodsoaked champion', 'Bloodsoaked Champion').
card_type('bloodsoaked champion', 'Creature — Human Warrior').
card_types('bloodsoaked champion', ['Creature']).
card_subtypes('bloodsoaked champion', ['Human', 'Warrior']).
card_colors('bloodsoaked champion', ['B']).
card_text('bloodsoaked champion', 'Bloodsoaked Champion can\'t block.\nRaid — {1}{B}: Return Bloodsoaked Champion from your graveyard to the battlefield. Activate this ability only if you attacked with a creature this turn.').
card_mana_cost('bloodsoaked champion', ['B']).
card_cmc('bloodsoaked champion', 1).
card_layout('bloodsoaked champion', 'normal').
card_power('bloodsoaked champion', 2).
card_toughness('bloodsoaked champion', 1).

% Found in: EXP, KTK, ONS, pJGP
card_name('bloodstained mire', 'Bloodstained Mire').
card_type('bloodstained mire', 'Land').
card_types('bloodstained mire', ['Land']).
card_subtypes('bloodstained mire', []).
card_colors('bloodstained mire', []).
card_text('bloodstained mire', '{T}, Pay 1 life, Sacrifice Bloodstained Mire: Search your library for a Swamp or Mountain card and put it onto the battlefield. Then shuffle your library.').
card_layout('bloodstained mire', 'normal').

% Found in: LGN
card_name('bloodstoke howler', 'Bloodstoke Howler').
card_type('bloodstoke howler', 'Creature — Beast').
card_types('bloodstoke howler', ['Creature']).
card_subtypes('bloodstoke howler', ['Beast']).
card_colors('bloodstoke howler', ['R']).
card_text('bloodstoke howler', 'Morph {6}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Bloodstoke Howler is turned face up, Beast creatures you control get +3/+0 until end of turn.').
card_mana_cost('bloodstoke howler', ['5', 'R']).
card_cmc('bloodstoke howler', 6).
card_layout('bloodstoke howler', 'normal').
card_power('bloodstoke howler', 3).
card_toughness('bloodstoke howler', 4).

% Found in: INV
card_name('bloodstone cameo', 'Bloodstone Cameo').
card_type('bloodstone cameo', 'Artifact').
card_types('bloodstone cameo', ['Artifact']).
card_subtypes('bloodstone cameo', []).
card_colors('bloodstone cameo', []).
card_text('bloodstone cameo', '{T}: Add {B} or {R} to your mana pool.').
card_mana_cost('bloodstone cameo', ['3']).
card_cmc('bloodstone cameo', 3).
card_layout('bloodstone cameo', 'normal').

% Found in: CHK
card_name('bloodthirsty ogre', 'Bloodthirsty Ogre').
card_type('bloodthirsty ogre', 'Creature — Ogre Warrior Shaman').
card_types('bloodthirsty ogre', ['Creature']).
card_subtypes('bloodthirsty ogre', ['Ogre', 'Warrior', 'Shaman']).
card_colors('bloodthirsty ogre', ['B']).
card_text('bloodthirsty ogre', '{T}: Put a devotion counter on Bloodthirsty Ogre.\n{T}: Target creature gets -X/-X until end of turn, where X is the number of devotion counters on Bloodthirsty Ogre. Activate this ability only if you control a Demon.').
card_mana_cost('bloodthirsty ogre', ['2', 'B']).
card_cmc('bloodthirsty ogre', 3).
card_layout('bloodthirsty ogre', 'normal').
card_power('bloodthirsty ogre', 3).
card_toughness('bloodthirsty ogre', 1).

% Found in: ALA
card_name('bloodthorn taunter', 'Bloodthorn Taunter').
card_type('bloodthorn taunter', 'Creature — Human Scout').
card_types('bloodthorn taunter', ['Creature']).
card_subtypes('bloodthorn taunter', ['Human', 'Scout']).
card_colors('bloodthorn taunter', ['R']).
card_text('bloodthorn taunter', 'Haste\n{T}: Target creature with power 5 or greater gains haste until end of turn.').
card_mana_cost('bloodthorn taunter', ['1', 'R']).
card_cmc('bloodthorn taunter', 2).
card_layout('bloodthorn taunter', 'normal').
card_power('bloodthorn taunter', 1).
card_toughness('bloodthorn taunter', 1).

% Found in: DDP, M11, M13, MM2, ROE, pMEI
card_name('bloodthrone vampire', 'Bloodthrone Vampire').
card_type('bloodthrone vampire', 'Creature — Vampire').
card_types('bloodthrone vampire', ['Creature']).
card_subtypes('bloodthrone vampire', ['Vampire']).
card_colors('bloodthrone vampire', ['B']).
card_text('bloodthrone vampire', 'Sacrifice a creature: Bloodthrone Vampire gets +2/+2 until end of turn.').
card_mana_cost('bloodthrone vampire', ['1', 'B']).
card_cmc('bloodthrone vampire', 2).
card_layout('bloodthrone vampire', 'normal').
card_power('bloodthrone vampire', 1).
card_toughness('bloodthrone vampire', 1).

% Found in: EVE
card_name('bloom tender', 'Bloom Tender').
card_type('bloom tender', 'Creature — Elf Druid').
card_types('bloom tender', ['Creature']).
card_subtypes('bloom tender', ['Elf', 'Druid']).
card_colors('bloom tender', ['G']).
card_text('bloom tender', '{T}: For each color among permanents you control, add one mana of that color to your mana pool.').
card_mana_cost('bloom tender', ['1', 'G']).
card_cmc('bloom tender', 2).
card_layout('bloom tender', 'normal').
card_power('bloom tender', 1).
card_toughness('bloom tender', 1).

% Found in: FRF, KTK
card_name('blossoming sands', 'Blossoming Sands').
card_type('blossoming sands', 'Land').
card_types('blossoming sands', ['Land']).
card_subtypes('blossoming sands', []).
card_colors('blossoming sands', []).
card_text('blossoming sands', 'Blossoming Sands enters the battlefield tapped.\nWhen Blossoming Sands enters the battlefield, you gain 1 life.\n{T}: Add {G} or {W} to your mana pool.').
card_layout('blossoming sands', 'normal').

% Found in: WTH
card_name('blossoming wreath', 'Blossoming Wreath').
card_type('blossoming wreath', 'Instant').
card_types('blossoming wreath', ['Instant']).
card_subtypes('blossoming wreath', []).
card_colors('blossoming wreath', ['G']).
card_text('blossoming wreath', 'You gain life equal to the number of creature cards in your graveyard.').
card_mana_cost('blossoming wreath', ['G']).
card_cmc('blossoming wreath', 1).
card_layout('blossoming wreath', 'normal').

% Found in: SHM
card_name('blowfly infestation', 'Blowfly Infestation').
card_type('blowfly infestation', 'Enchantment').
card_types('blowfly infestation', ['Enchantment']).
card_subtypes('blowfly infestation', []).
card_colors('blowfly infestation', ['B']).
card_text('blowfly infestation', 'Whenever a creature dies, if it had a -1/-1 counter on it, put a -1/-1 counter on target creature.').
card_mana_cost('blowfly infestation', ['2', 'B']).
card_cmc('blowfly infestation', 3).
card_layout('blowfly infestation', 'normal').

% Found in: NPH
card_name('bludgeon brawl', 'Bludgeon Brawl').
card_type('bludgeon brawl', 'Enchantment').
card_types('bludgeon brawl', ['Enchantment']).
card_subtypes('bludgeon brawl', []).
card_colors('bludgeon brawl', ['R']).
card_text('bludgeon brawl', 'Each noncreature, non-Equipment artifact is an Equipment with equip {X} and \"Equipped creature gets +X/+0,\" where X is that artifact\'s converted mana cost.').
card_mana_cost('bludgeon brawl', ['2', 'R']).
card_cmc('bludgeon brawl', 3).
card_layout('bludgeon brawl', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB, ME4, pMEI
card_name('blue elemental blast', 'Blue Elemental Blast').
card_type('blue elemental blast', 'Instant').
card_types('blue elemental blast', ['Instant']).
card_subtypes('blue elemental blast', []).
card_colors('blue elemental blast', ['U']).
card_text('blue elemental blast', 'Choose one —\n• Counter target red spell.\n• Destroy target red permanent.').
card_mana_cost('blue elemental blast', ['U']).
card_cmc('blue elemental blast', 1).
card_layout('blue elemental blast', 'normal').

% Found in: 4ED, LEG
card_name('blue mana battery', 'Blue Mana Battery').
card_type('blue mana battery', 'Artifact').
card_types('blue mana battery', ['Artifact']).
card_subtypes('blue mana battery', []).
card_colors('blue mana battery', []).
card_text('blue mana battery', '{2}, {T}: Put a charge counter on Blue Mana Battery.\n{T}, Remove any number of charge counters from Blue Mana Battery: Add {U} to your mana pool, then add an additional {U} to your mana pool for each charge counter removed this way.').
card_mana_cost('blue mana battery', ['4']).
card_cmc('blue mana battery', 4).
card_layout('blue mana battery', 'normal').

% Found in: ICE
card_name('blue scarab', 'Blue Scarab').
card_type('blue scarab', 'Enchantment — Aura').
card_types('blue scarab', ['Enchantment']).
card_subtypes('blue scarab', ['Aura']).
card_colors('blue scarab', ['W']).
card_text('blue scarab', 'Enchant creature\nEnchanted creature can\'t be blocked by blue creatures.\nEnchanted creature gets +2/+2 as long as an opponent controls a blue permanent.').
card_mana_cost('blue scarab', ['W']).
card_cmc('blue scarab', 1).
card_layout('blue scarab', 'normal').

% Found in: C13, MBS
card_name('blue sun\'s zenith', 'Blue Sun\'s Zenith').
card_type('blue sun\'s zenith', 'Instant').
card_types('blue sun\'s zenith', ['Instant']).
card_subtypes('blue sun\'s zenith', []).
card_colors('blue sun\'s zenith', ['U']).
card_text('blue sun\'s zenith', 'Target player draws X cards. Shuffle Blue Sun\'s Zenith into its owner\'s library.').
card_mana_cost('blue sun\'s zenith', ['X', 'U', 'U', 'U']).
card_cmc('blue sun\'s zenith', 3).
card_layout('blue sun\'s zenith', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('blue ward', 'Blue Ward').
card_type('blue ward', 'Enchantment — Aura').
card_types('blue ward', ['Enchantment']).
card_subtypes('blue ward', ['Aura']).
card_colors('blue ward', ['W']).
card_text('blue ward', 'Enchant creature\nEnchanted creature has protection from blue. This effect doesn\'t remove Blue Ward.').
card_mana_cost('blue ward', ['W']).
card_cmc('blue ward', 1).
card_layout('blue ward', 'normal').

% Found in: SOM
card_name('blunt the assault', 'Blunt the Assault').
card_type('blunt the assault', 'Instant').
card_types('blunt the assault', ['Instant']).
card_subtypes('blunt the assault', []).
card_colors('blunt the assault', ['G']).
card_text('blunt the assault', 'You gain 1 life for each creature on the battlefield. Prevent all combat damage that would be dealt this turn.').
card_mana_cost('blunt the assault', ['3', 'G']).
card_cmc('blunt the assault', 4).
card_layout('blunt the assault', 'normal').

% Found in: M14
card_name('blur sliver', 'Blur Sliver').
card_type('blur sliver', 'Creature — Sliver').
card_types('blur sliver', ['Creature']).
card_subtypes('blur sliver', ['Sliver']).
card_colors('blur sliver', ['R']).
card_text('blur sliver', 'Sliver creatures you control have haste. (They can attack and {T} as soon as they come under your control.)').
card_mana_cost('blur sliver', ['2', 'R']).
card_cmc('blur sliver', 3).
card_layout('blur sliver', 'normal').
card_power('blur sliver', 2).
card_toughness('blur sliver', 2).

% Found in: INV
card_name('blurred mongoose', 'Blurred Mongoose').
card_type('blurred mongoose', 'Creature — Mongoose').
card_types('blurred mongoose', ['Creature']).
card_subtypes('blurred mongoose', ['Mongoose']).
card_colors('blurred mongoose', ['G']).
card_text('blurred mongoose', 'Blurred Mongoose can\'t be countered.\nShroud (This creature can\'t be the target of spells or abilities.)').
card_mana_cost('blurred mongoose', ['1', 'G']).
card_cmc('blurred mongoose', 2).
card_layout('blurred mongoose', 'normal').
card_power('blurred mongoose', 2).
card_toughness('blurred mongoose', 1).

% Found in: RTR
card_name('blustersquall', 'Blustersquall').
card_type('blustersquall', 'Instant').
card_types('blustersquall', ['Instant']).
card_subtypes('blustersquall', []).
card_colors('blustersquall', ['U']).
card_text('blustersquall', 'Tap target creature you don\'t control.\nOverload {3}{U} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_mana_cost('blustersquall', ['U']).
card_cmc('blustersquall', 1).
card_layout('blustersquall', 'normal').

% Found in: MMQ
card_name('boa constrictor', 'Boa Constrictor').
card_type('boa constrictor', 'Creature — Snake').
card_types('boa constrictor', ['Creature']).
card_subtypes('boa constrictor', ['Snake']).
card_colors('boa constrictor', ['G']).
card_text('boa constrictor', '{T}: Boa Constrictor gets +3/+3 until end of turn.').
card_mana_cost('boa constrictor', ['4', 'G']).
card_cmc('boa constrictor', 5).
card_layout('boa constrictor', 'normal').
card_power('boa constrictor', 3).
card_toughness('boa constrictor', 3).

% Found in: PC2, ROE
card_name('boar umbra', 'Boar Umbra').
card_type('boar umbra', 'Enchantment — Aura').
card_types('boar umbra', ['Enchantment']).
card_subtypes('boar umbra', ['Aura']).
card_colors('boar umbra', ['G']).
card_text('boar umbra', 'Enchant creature\nEnchanted creature gets +3/+3.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_mana_cost('boar umbra', ['2', 'G']).
card_cmc('boar umbra', 3).
card_layout('boar umbra', 'normal').

% Found in: SHM
card_name('boartusk liege', 'Boartusk Liege').
card_type('boartusk liege', 'Creature — Goblin Knight').
card_types('boartusk liege', ['Creature']).
card_subtypes('boartusk liege', ['Goblin', 'Knight']).
card_colors('boartusk liege', ['R', 'G']).
card_text('boartusk liege', 'Trample\nOther red creatures you control get +1/+1.\nOther green creatures you control get +1/+1.').
card_mana_cost('boartusk liege', ['1', 'R/G', 'R/G', 'R/G']).
card_cmc('boartusk liege', 4).
card_layout('boartusk liege', 'normal').
card_power('boartusk liege', 3).
card_toughness('boartusk liege', 4).

% Found in: DDM, PLC
card_name('body double', 'Body Double').
card_type('body double', 'Creature — Shapeshifter').
card_types('body double', ['Creature']).
card_subtypes('body double', ['Shapeshifter']).
card_colors('body double', ['U']).
card_text('body double', 'You may have Body Double enter the battlefield as a copy of any creature card in a graveyard.').
card_mana_cost('body double', ['4', 'U']).
card_cmc('body double', 5).
card_layout('body double', 'normal').
card_power('body double', 0).
card_toughness('body double', 0).

% Found in: BOK
card_name('body of jukai', 'Body of Jukai').
card_type('body of jukai', 'Creature — Spirit').
card_types('body of jukai', ['Creature']).
card_subtypes('body of jukai', ['Spirit']).
card_colors('body of jukai', ['G']).
card_text('body of jukai', 'Trample\nSoulshift 8 (When this creature dies, you may return target Spirit card with converted mana cost 8 or less from your graveyard to your hand.)').
card_mana_cost('body of jukai', ['7', 'G', 'G']).
card_cmc('body of jukai', 9).
card_layout('body of jukai', 'normal').
card_power('body of jukai', 8).
card_toughness('body of jukai', 5).

% Found in: UDS
card_name('body snatcher', 'Body Snatcher').
card_type('body snatcher', 'Creature — Minion').
card_types('body snatcher', ['Creature']).
card_subtypes('body snatcher', ['Minion']).
card_colors('body snatcher', ['B']).
card_text('body snatcher', 'When Body Snatcher enters the battlefield, exile it unless you discard a creature card.\nWhen Body Snatcher dies, exile Body Snatcher and return target creature card from your graveyard to the battlefield.').
card_mana_cost('body snatcher', ['2', 'B', 'B']).
card_cmc('body snatcher', 4).
card_layout('body snatcher', 'normal').
card_power('body snatcher', 2).
card_toughness('body snatcher', 2).

% Found in: PLS
card_name('bog down', 'Bog Down').
card_type('bog down', 'Sorcery').
card_types('bog down', ['Sorcery']).
card_subtypes('bog down', []).
card_colors('bog down', ['B']).
card_text('bog down', 'Kicker—Sacrifice two lands. (You may sacrifice two lands in addition to any other costs as you cast this spell.)\nTarget player discards two cards. If Bog Down was kicked, that player discards three cards instead.').
card_mana_cost('bog down', ['2', 'B']).
card_cmc('bog down', 3).
card_layout('bog down', 'normal').

% Found in: PCY
card_name('bog elemental', 'Bog Elemental').
card_type('bog elemental', 'Creature — Elemental').
card_types('bog elemental', ['Creature']).
card_subtypes('bog elemental', ['Elemental']).
card_colors('bog elemental', ['B']).
card_text('bog elemental', 'Protection from white\nAt the beginning of your upkeep, sacrifice Bog Elemental unless you sacrifice a land.').
card_mana_cost('bog elemental', ['3', 'B', 'B']).
card_cmc('bog elemental', 5).
card_layout('bog elemental', 'normal').
card_power('bog elemental', 5).
card_toughness('bog elemental', 4).

% Found in: PCY
card_name('bog glider', 'Bog Glider').
card_type('bog glider', 'Creature — Human Mercenary').
card_types('bog glider', ['Creature']).
card_subtypes('bog glider', ['Human', 'Mercenary']).
card_colors('bog glider', ['B']).
card_text('bog glider', 'Flying\n{T}, Sacrifice a land: Search your library for a Mercenary permanent card with converted mana cost 2 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('bog glider', ['2', 'B']).
card_cmc('bog glider', 3).
card_layout('bog glider', 'normal').
card_power('bog glider', 1).
card_toughness('bog glider', 1).

% Found in: APC
card_name('bog gnarr', 'Bog Gnarr').
card_type('bog gnarr', 'Creature — Beast').
card_types('bog gnarr', ['Creature']).
card_subtypes('bog gnarr', ['Beast']).
card_colors('bog gnarr', ['G']).
card_text('bog gnarr', 'Whenever a player casts a black spell, Bog Gnarr gets +2/+2 until end of turn.').
card_mana_cost('bog gnarr', ['4', 'G']).
card_cmc('bog gnarr', 5).
card_layout('bog gnarr', 'normal').
card_power('bog gnarr', 2).
card_toughness('bog gnarr', 2).

% Found in: LRW
card_name('bog hoodlums', 'Bog Hoodlums').
card_type('bog hoodlums', 'Creature — Goblin Warrior').
card_types('bog hoodlums', ['Creature']).
card_subtypes('bog hoodlums', ['Goblin', 'Warrior']).
card_colors('bog hoodlums', ['B']).
card_text('bog hoodlums', 'Bog Hoodlums can\'t block.\nWhen Bog Hoodlums enters the battlefield, clash with an opponent. If you win, put a +1/+1 counter on Bog Hoodlums. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('bog hoodlums', ['5', 'B']).
card_cmc('bog hoodlums', 6).
card_layout('bog hoodlums', 'normal').
card_power('bog hoodlums', 4).
card_toughness('bog hoodlums', 1).

% Found in: 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, DRK, ITP, POR, RQS, S00, S99
card_name('bog imp', 'Bog Imp').
card_type('bog imp', 'Creature — Imp').
card_types('bog imp', ['Creature']).
card_subtypes('bog imp', ['Imp']).
card_colors('bog imp', ['B']).
card_text('bog imp', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_mana_cost('bog imp', ['1', 'B']).
card_cmc('bog imp', 2).
card_layout('bog imp', 'normal').
card_power('bog imp', 1).
card_toughness('bog imp', 1).

% Found in: INV
card_name('bog initiate', 'Bog Initiate').
card_type('bog initiate', 'Creature — Human Wizard').
card_types('bog initiate', ['Creature']).
card_subtypes('bog initiate', ['Human', 'Wizard']).
card_colors('bog initiate', ['B']).
card_text('bog initiate', '{1}: Add {B} to your mana pool.').
card_mana_cost('bog initiate', ['1', 'B']).
card_cmc('bog initiate', 2).
card_layout('bog initiate', 'normal').
card_power('bog initiate', 1).
card_toughness('bog initiate', 1).

% Found in: M11, POR, S99, USG
card_name('bog raiders', 'Bog Raiders').
card_type('bog raiders', 'Creature — Zombie').
card_types('bog raiders', ['Creature']).
card_subtypes('bog raiders', ['Zombie']).
card_colors('bog raiders', ['B']).
card_text('bog raiders', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('bog raiders', ['2', 'B']).
card_cmc('bog raiders', 3).
card_layout('bog raiders', 'normal').
card_power('bog raiders', 2).
card_toughness('bog raiders', 2).

% Found in: 5ED, 6ED, CHR, DRK
card_name('bog rats', 'Bog Rats').
card_type('bog rats', 'Creature — Rat').
card_types('bog rats', ['Creature']).
card_subtypes('bog rats', ['Rat']).
card_colors('bog rats', ['B']).
card_text('bog rats', 'Bog Rats can\'t be blocked by Walls.').
card_mana_cost('bog rats', ['B']).
card_cmc('bog rats', 1).
card_layout('bog rats', 'normal').
card_power('bog rats', 1).
card_toughness('bog rats', 1).

% Found in: PLC
card_name('bog serpent', 'Bog Serpent').
card_type('bog serpent', 'Creature — Serpent').
card_types('bog serpent', ['Creature']).
card_subtypes('bog serpent', ['Serpent']).
card_colors('bog serpent', ['B']).
card_text('bog serpent', 'Bog Serpent can\'t attack unless defending player controls a Swamp.\nWhen you control no Swamps, sacrifice Bog Serpent.').
card_mana_cost('bog serpent', ['5', 'B']).
card_cmc('bog serpent', 6).
card_layout('bog serpent', 'normal').
card_power('bog serpent', 5).
card_toughness('bog serpent', 5).

% Found in: MMQ
card_name('bog smugglers', 'Bog Smugglers').
card_type('bog smugglers', 'Creature — Human Mercenary').
card_types('bog smugglers', ['Creature']).
card_subtypes('bog smugglers', ['Human', 'Mercenary']).
card_colors('bog smugglers', ['B']).
card_text('bog smugglers', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('bog smugglers', ['1', 'B', 'B']).
card_cmc('bog smugglers', 3).
card_layout('bog smugglers', 'normal').
card_power('bog smugglers', 2).
card_toughness('bog smugglers', 2).

% Found in: ZEN
card_name('bog tatters', 'Bog Tatters').
card_type('bog tatters', 'Creature — Wraith').
card_types('bog tatters', ['Creature']).
card_subtypes('bog tatters', ['Wraith']).
card_colors('bog tatters', ['B']).
card_text('bog tatters', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('bog tatters', ['4', 'B']).
card_cmc('bog tatters', 5).
card_layout('bog tatters', 'normal').
card_power('bog tatters', 4).
card_toughness('bog tatters', 2).

% Found in: ARC, MMQ
card_name('bog witch', 'Bog Witch').
card_type('bog witch', 'Creature — Human Spellshaper').
card_types('bog witch', ['Creature']).
card_subtypes('bog witch', ['Human', 'Spellshaper']).
card_colors('bog witch', ['B']).
card_text('bog witch', '{B}, {T}, Discard a card: Add {B}{B}{B} to your mana pool.').
card_mana_cost('bog witch', ['2', 'B']).
card_cmc('bog witch', 3).
card_layout('bog witch', 'normal').
card_power('bog witch', 1).
card_toughness('bog witch', 1).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, CED, CEI, ITP, LEA, LEB, M10, POR, RQS, S99
card_name('bog wraith', 'Bog Wraith').
card_type('bog wraith', 'Creature — Wraith').
card_types('bog wraith', ['Creature']).
card_subtypes('bog wraith', ['Wraith']).
card_colors('bog wraith', ['B']).
card_text('bog wraith', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('bog wraith', ['3', 'B']).
card_cmc('bog wraith', 4).
card_layout('bog wraith', 'normal').
card_power('bog wraith', 3).
card_toughness('bog wraith', 3).

% Found in: ODY
card_name('bog wreckage', 'Bog Wreckage').
card_type('bog wreckage', 'Land').
card_types('bog wreckage', ['Land']).
card_subtypes('bog wreckage', []).
card_colors('bog wreckage', []).
card_text('bog wreckage', 'Bog Wreckage enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\n{T}, Sacrifice Bog Wreckage: Add one mana of any color to your mana pool.').
card_layout('bog wreckage', 'normal').

% Found in: LRW
card_name('bog-strider ash', 'Bog-Strider Ash').
card_type('bog-strider ash', 'Creature — Treefolk Shaman').
card_types('bog-strider ash', ['Creature']).
card_subtypes('bog-strider ash', ['Treefolk', 'Shaman']).
card_colors('bog-strider ash', ['G']).
card_text('bog-strider ash', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)\nWhenever a player casts a Goblin spell, you may pay {G}. If you do, you gain 2 life.').
card_mana_cost('bog-strider ash', ['3', 'G']).
card_cmc('bog-strider ash', 4).
card_layout('bog-strider ash', 'normal').
card_power('bog-strider ash', 2).
card_toughness('bog-strider ash', 4).

% Found in: 10E, HOP, WTH
card_name('bogardan firefiend', 'Bogardan Firefiend').
card_type('bogardan firefiend', 'Creature — Elemental Spirit').
card_types('bogardan firefiend', ['Creature']).
card_subtypes('bogardan firefiend', ['Elemental', 'Spirit']).
card_colors('bogardan firefiend', ['R']).
card_text('bogardan firefiend', 'When Bogardan Firefiend dies, it deals 2 damage to target creature.').
card_mana_cost('bogardan firefiend', ['2', 'R']).
card_cmc('bogardan firefiend', 3).
card_layout('bogardan firefiend', 'normal').
card_power('bogardan firefiend', 2).
card_toughness('bogardan firefiend', 1).

% Found in: C14, DDG, DRB, M10, TSP
card_name('bogardan hellkite', 'Bogardan Hellkite').
card_type('bogardan hellkite', 'Creature — Dragon').
card_types('bogardan hellkite', ['Creature']).
card_subtypes('bogardan hellkite', ['Dragon']).
card_colors('bogardan hellkite', ['R']).
card_text('bogardan hellkite', 'Flash\nFlying\nWhen Bogardan Hellkite enters the battlefield, it deals 5 damage divided as you choose among any number of target creatures and/or players.').
card_mana_cost('bogardan hellkite', ['6', 'R', 'R']).
card_cmc('bogardan hellkite', 8).
card_layout('bogardan hellkite', 'normal').
card_power('bogardan hellkite', 5).
card_toughness('bogardan hellkite', 5).

% Found in: FUT
card_name('bogardan lancer', 'Bogardan Lancer').
card_type('bogardan lancer', 'Creature — Human Knight').
card_types('bogardan lancer', ['Creature']).
card_subtypes('bogardan lancer', ['Human', 'Knight']).
card_colors('bogardan lancer', ['R']).
card_text('bogardan lancer', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)\nFlanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)').
card_mana_cost('bogardan lancer', ['1', 'R']).
card_cmc('bogardan lancer', 2).
card_layout('bogardan lancer', 'normal').
card_power('bogardan lancer', 1).
card_toughness('bogardan lancer', 1).

% Found in: VIS
card_name('bogardan phoenix', 'Bogardan Phoenix').
card_type('bogardan phoenix', 'Creature — Phoenix').
card_types('bogardan phoenix', ['Creature']).
card_subtypes('bogardan phoenix', ['Phoenix']).
card_colors('bogardan phoenix', ['R']).
card_text('bogardan phoenix', 'Flying\nWhen Bogardan Phoenix dies, exile it if it had a death counter on it. Otherwise, return it to the battlefield under your control and put a death counter on it.').
card_mana_cost('bogardan phoenix', ['2', 'R', 'R', 'R']).
card_cmc('bogardan phoenix', 5).
card_layout('bogardan phoenix', 'normal').
card_power('bogardan phoenix', 3).
card_toughness('bogardan phoenix', 3).
card_reserved('bogardan phoenix').

% Found in: DDG, HOP, TSP
card_name('bogardan rager', 'Bogardan Rager').
card_type('bogardan rager', 'Creature — Elemental').
card_types('bogardan rager', ['Creature']).
card_subtypes('bogardan rager', ['Elemental']).
card_colors('bogardan rager', ['R']).
card_text('bogardan rager', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Bogardan Rager enters the battlefield, target creature gets +4/+0 until end of turn.').
card_mana_cost('bogardan rager', ['5', 'R']).
card_cmc('bogardan rager', 6).
card_layout('bogardan rager', 'normal').
card_power('bogardan rager', 3).
card_toughness('bogardan rager', 4).

% Found in: M14
card_name('bogbrew witch', 'Bogbrew Witch').
card_type('bogbrew witch', 'Creature — Human Wizard').
card_types('bogbrew witch', ['Creature']).
card_subtypes('bogbrew witch', ['Human', 'Wizard']).
card_colors('bogbrew witch', ['B']).
card_text('bogbrew witch', '{2}, {T}: Search your library for a card named Festering Newt or Bubbling Cauldron, put it onto the battlefield tapped, then shuffle your library.').
card_mana_cost('bogbrew witch', ['3', 'B']).
card_cmc('bogbrew witch', 4).
card_layout('bogbrew witch', 'normal').
card_power('bogbrew witch', 1).
card_toughness('bogbrew witch', 3).

% Found in: SHM
card_name('boggart arsonists', 'Boggart Arsonists').
card_type('boggart arsonists', 'Creature — Goblin Rogue').
card_types('boggart arsonists', ['Creature']).
card_subtypes('boggart arsonists', ['Goblin', 'Rogue']).
card_colors('boggart arsonists', ['R']).
card_text('boggart arsonists', 'Plainswalk (This creature can\'t be blocked as long as defending player controls a Plains.)\n{2}{R}, Sacrifice Boggart Arsonists: Destroy target Scarecrow or Plains.').
card_mana_cost('boggart arsonists', ['2', 'R']).
card_cmc('boggart arsonists', 3).
card_layout('boggart arsonists', 'normal').
card_power('boggart arsonists', 2).
card_toughness('boggart arsonists', 1).

% Found in: LRW
card_name('boggart birth rite', 'Boggart Birth Rite').
card_type('boggart birth rite', 'Tribal Sorcery — Goblin').
card_types('boggart birth rite', ['Tribal', 'Sorcery']).
card_subtypes('boggart birth rite', ['Goblin']).
card_colors('boggart birth rite', ['B']).
card_text('boggart birth rite', 'Return target Goblin card from your graveyard to your hand.').
card_mana_cost('boggart birth rite', ['B']).
card_cmc('boggart birth rite', 1).
card_layout('boggart birth rite', 'normal').

% Found in: ORI
card_name('boggart brute', 'Boggart Brute').
card_type('boggart brute', 'Creature — Goblin Warrior').
card_types('boggart brute', ['Creature']).
card_subtypes('boggart brute', ['Goblin', 'Warrior']).
card_colors('boggart brute', ['R']).
card_text('boggart brute', 'Menace (This creature can\'t be blocked except by two or more creatures.)').
card_mana_cost('boggart brute', ['2', 'R']).
card_cmc('boggart brute', 3).
card_layout('boggart brute', 'normal').
card_power('boggart brute', 3).
card_toughness('boggart brute', 2).

% Found in: LRW
card_name('boggart forager', 'Boggart Forager').
card_type('boggart forager', 'Creature — Goblin Rogue').
card_types('boggart forager', ['Creature']).
card_subtypes('boggart forager', ['Goblin', 'Rogue']).
card_colors('boggart forager', ['R']).
card_text('boggart forager', '{R}, Sacrifice Boggart Forager: Target player shuffles his or her library.').
card_mana_cost('boggart forager', ['R']).
card_cmc('boggart forager', 1).
card_layout('boggart forager', 'normal').
card_power('boggart forager', 1).
card_toughness('boggart forager', 1).

% Found in: LRW
card_name('boggart harbinger', 'Boggart Harbinger').
card_type('boggart harbinger', 'Creature — Goblin Shaman').
card_types('boggart harbinger', ['Creature']).
card_subtypes('boggart harbinger', ['Goblin', 'Shaman']).
card_colors('boggart harbinger', ['B']).
card_text('boggart harbinger', 'When Boggart Harbinger enters the battlefield, you may search your library for a Goblin card, reveal it, then shuffle your library and put that card on top of it.').
card_mana_cost('boggart harbinger', ['2', 'B']).
card_cmc('boggart harbinger', 3).
card_layout('boggart harbinger', 'normal').
card_power('boggart harbinger', 2).
card_toughness('boggart harbinger', 1).

% Found in: LRW
card_name('boggart loggers', 'Boggart Loggers').
card_type('boggart loggers', 'Creature — Goblin Rogue').
card_types('boggart loggers', ['Creature']).
card_subtypes('boggart loggers', ['Goblin', 'Rogue']).
card_colors('boggart loggers', ['B']).
card_text('boggart loggers', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)\n{2}{B}, Sacrifice Boggart Loggers: Destroy target Treefolk or Forest.').
card_mana_cost('boggart loggers', ['2', 'B']).
card_cmc('boggart loggers', 3).
card_layout('boggart loggers', 'normal').
card_power('boggart loggers', 2).
card_toughness('boggart loggers', 1).

% Found in: LRW
card_name('boggart mob', 'Boggart Mob').
card_type('boggart mob', 'Creature — Goblin Warrior').
card_types('boggart mob', ['Creature']).
card_subtypes('boggart mob', ['Goblin', 'Warrior']).
card_colors('boggart mob', ['B']).
card_text('boggart mob', 'Champion a Goblin (When this enters the battlefield, sacrifice it unless you exile another Goblin you control. When this leaves the battlefield, that card returns to the battlefield.)\nWhenever a Goblin you control deals combat damage to a player, you may put a 1/1 black Goblin Rogue creature token onto the battlefield.').
card_mana_cost('boggart mob', ['3', 'B']).
card_cmc('boggart mob', 4).
card_layout('boggart mob', 'normal').
card_power('boggart mob', 5).
card_toughness('boggart mob', 5).

% Found in: PD2, SHM, pGTW
card_name('boggart ram-gang', 'Boggart Ram-Gang').
card_type('boggart ram-gang', 'Creature — Goblin Warrior').
card_types('boggart ram-gang', ['Creature']).
card_subtypes('boggart ram-gang', ['Goblin', 'Warrior']).
card_colors('boggart ram-gang', ['R', 'G']).
card_text('boggart ram-gang', 'Haste\nWither (This deals damage to creatures in the form of -1/-1 counters.)').
card_mana_cost('boggart ram-gang', ['R/G', 'R/G', 'R/G']).
card_cmc('boggart ram-gang', 3).
card_layout('boggart ram-gang', 'normal').
card_power('boggart ram-gang', 3).
card_toughness('boggart ram-gang', 3).

% Found in: DD3_EVG, EVG, LRW
card_name('boggart shenanigans', 'Boggart Shenanigans').
card_type('boggart shenanigans', 'Tribal Enchantment — Goblin').
card_types('boggart shenanigans', ['Tribal', 'Enchantment']).
card_subtypes('boggart shenanigans', ['Goblin']).
card_colors('boggart shenanigans', ['R']).
card_text('boggart shenanigans', 'Whenever another Goblin you control is put into a graveyard from the battlefield, you may have Boggart Shenanigans deal 1 damage to target player.').
card_mana_cost('boggart shenanigans', ['2', 'R']).
card_cmc('boggart shenanigans', 3).
card_layout('boggart shenanigans', 'normal').

% Found in: LRW
card_name('boggart sprite-chaser', 'Boggart Sprite-Chaser').
card_type('boggart sprite-chaser', 'Creature — Goblin Warrior').
card_types('boggart sprite-chaser', ['Creature']).
card_subtypes('boggart sprite-chaser', ['Goblin', 'Warrior']).
card_colors('boggart sprite-chaser', ['R']).
card_text('boggart sprite-chaser', 'As long as you control a Faerie, Boggart Sprite-Chaser gets +1/+1 and has flying.').
card_mana_cost('boggart sprite-chaser', ['1', 'R']).
card_cmc('boggart sprite-chaser', 2).
card_layout('boggart sprite-chaser', 'normal').
card_power('boggart sprite-chaser', 1).
card_toughness('boggart sprite-chaser', 2).

% Found in: 6ED, 7ED, 8ED, TMP
card_name('boil', 'Boil').
card_type('boil', 'Instant').
card_types('boil', ['Instant']).
card_subtypes('boil', []).
card_colors('boil', ['R']).
card_text('boil', 'Destroy all Islands.').
card_mana_cost('boil', ['3', 'R']).
card_cmc('boil', 4).
card_layout('boil', 'normal').

% Found in: WTH
card_name('boiling blood', 'Boiling Blood').
card_type('boiling blood', 'Instant').
card_types('boiling blood', ['Instant']).
card_subtypes('boiling blood', []).
card_colors('boiling blood', ['R']).
card_text('boiling blood', 'Target creature attacks this turn if able.\nDraw a card.').
card_mana_cost('boiling blood', ['2', 'R']).
card_cmc('boiling blood', 3).
card_layout('boiling blood', 'normal').

% Found in: BFZ
card_name('boiling earth', 'Boiling Earth').
card_type('boiling earth', 'Sorcery').
card_types('boiling earth', ['Sorcery']).
card_subtypes('boiling earth', []).
card_colors('boiling earth', ['R']).
card_text('boiling earth', 'Boiling Earth deals 1 damage to each creature your opponents control.\nAwaken 4—{6}{R} (If you cast this spell for {6}{R}, also put four +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_mana_cost('boiling earth', ['1', 'R']).
card_cmc('boiling earth', 2).
card_layout('boiling earth', 'normal').

% Found in: 9ED, POR
card_name('boiling seas', 'Boiling Seas').
card_type('boiling seas', 'Sorcery').
card_types('boiling seas', ['Sorcery']).
card_subtypes('boiling seas', []).
card_colors('boiling seas', ['R']).
card_text('boiling seas', 'Destroy all Islands.').
card_mana_cost('boiling seas', ['3', 'R']).
card_cmc('boiling seas', 4).
card_layout('boiling seas', 'normal').

% Found in: C13, C14, CMD, WWK
card_name('bojuka bog', 'Bojuka Bog').
card_type('bojuka bog', 'Land').
card_types('bojuka bog', ['Land']).
card_subtypes('bojuka bog', []).
card_colors('bojuka bog', []).
card_text('bojuka bog', 'Bojuka Bog enters the battlefield tapped.\nWhen Bojuka Bog enters the battlefield, exile all cards from target player\'s graveyard.\n{T}: Add {B} to your mana pool.').
card_layout('bojuka bog', 'normal').

% Found in: WWK
card_name('bojuka brigand', 'Bojuka Brigand').
card_type('bojuka brigand', 'Creature — Human Warrior Ally').
card_types('bojuka brigand', ['Creature']).
card_subtypes('bojuka brigand', ['Human', 'Warrior', 'Ally']).
card_colors('bojuka brigand', ['B']).
card_text('bojuka brigand', 'Bojuka Brigand can\'t block.\nWhenever Bojuka Brigand or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Bojuka Brigand.').
card_mana_cost('bojuka brigand', ['1', 'B']).
card_cmc('bojuka brigand', 2).
card_layout('bojuka brigand', 'normal').
card_power('bojuka brigand', 1).
card_toughness('bojuka brigand', 1).

% Found in: NMS
card_name('bola warrior', 'Bola Warrior').
card_type('bola warrior', 'Creature — Human Spellshaper Warrior').
card_types('bola warrior', ['Creature']).
card_subtypes('bola warrior', ['Human', 'Spellshaper', 'Warrior']).
card_colors('bola warrior', ['R']).
card_text('bola warrior', '{R}, {T}, Discard a card: Target creature can\'t block this turn.').
card_mana_cost('bola warrior', ['1', 'R']).
card_cmc('bola warrior', 2).
card_layout('bola warrior', 'normal').
card_power('bola warrior', 1).
card_toughness('bola warrior', 1).

% Found in: ZEN
card_name('bold defense', 'Bold Defense').
card_type('bold defense', 'Instant').
card_types('bold defense', ['Instant']).
card_subtypes('bold defense', []).
card_colors('bold defense', ['W']).
card_text('bold defense', 'Kicker {3}{W} (You may pay an additional {3}{W} as you cast this spell.)\nCreatures you control get +1/+1 until end of turn. If Bold Defense was kicked, instead creatures you control get +2/+2 and gain first strike until end of turn.').
card_mana_cost('bold defense', ['2', 'W']).
card_cmc('bold defense', 3).
card_layout('bold defense', 'normal').

% Found in: MOR
card_name('boldwyr heavyweights', 'Boldwyr Heavyweights').
card_type('boldwyr heavyweights', 'Creature — Giant Warrior').
card_types('boldwyr heavyweights', ['Creature']).
card_subtypes('boldwyr heavyweights', ['Giant', 'Warrior']).
card_colors('boldwyr heavyweights', ['R']).
card_text('boldwyr heavyweights', 'Trample\nWhen Boldwyr Heavyweights enters the battlefield, each opponent may search his or her library for a creature card and put it onto the battlefield. Then each player who searched his or her library this way shuffles it.').
card_mana_cost('boldwyr heavyweights', ['2', 'R', 'R']).
card_cmc('boldwyr heavyweights', 4).
card_layout('boldwyr heavyweights', 'normal').
card_power('boldwyr heavyweights', 8).
card_toughness('boldwyr heavyweights', 8).

% Found in: CNS, FUT, MOR
card_name('boldwyr intimidator', 'Boldwyr Intimidator').
card_type('boldwyr intimidator', 'Creature — Giant Warrior').
card_types('boldwyr intimidator', ['Creature']).
card_subtypes('boldwyr intimidator', ['Giant', 'Warrior']).
card_colors('boldwyr intimidator', ['R']).
card_text('boldwyr intimidator', 'Cowards can\'t block Warriors.\n{R}: Target creature becomes a Coward until end of turn.\n{2}{R}: Target creature becomes a Warrior until end of turn.').
card_mana_cost('boldwyr intimidator', ['5', 'R', 'R']).
card_cmc('boldwyr intimidator', 7).
card_layout('boldwyr intimidator', 'normal').
card_power('boldwyr intimidator', 5).
card_toughness('boldwyr intimidator', 5).

% Found in: BNG
card_name('bolt of keranos', 'Bolt of Keranos').
card_type('bolt of keranos', 'Sorcery').
card_types('bolt of keranos', ['Sorcery']).
card_subtypes('bolt of keranos', []).
card_colors('bolt of keranos', ['R']).
card_text('bolt of keranos', 'Bolt of Keranos deals 3 damage to target creature or player. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('bolt of keranos', ['1', 'R', 'R']).
card_cmc('bolt of keranos', 3).
card_layout('bolt of keranos', 'normal').

% Found in: DTK, pMEI
card_name('boltwing marauder', 'Boltwing Marauder').
card_type('boltwing marauder', 'Creature — Dragon').
card_types('boltwing marauder', ['Creature']).
card_subtypes('boltwing marauder', ['Dragon']).
card_colors('boltwing marauder', ['B', 'R']).
card_text('boltwing marauder', 'Flying\nWhenever another creature enters the battlefield under your control, target creature gets +2/+0 until end of turn.').
card_mana_cost('boltwing marauder', ['3', 'B', 'R']).
card_cmc('boltwing marauder', 5).
card_layout('boltwing marauder', 'normal').
card_power('boltwing marauder', 5).
card_toughness('boltwing marauder', 4).

% Found in: ODY
card_name('bomb squad', 'Bomb Squad').
card_type('bomb squad', 'Creature — Dwarf').
card_types('bomb squad', ['Creature']).
card_subtypes('bomb squad', ['Dwarf']).
card_colors('bomb squad', ['R']).
card_text('bomb squad', '{T}: Put a fuse counter on target creature.\nAt the beginning of your upkeep, put a fuse counter on each creature with a fuse counter on it.\nWhenever a creature has four or more fuse counters on it, remove all fuse counters from it and destroy it. That creature deals 4 damage to its controller.').
card_mana_cost('bomb squad', ['3', 'R']).
card_cmc('bomb squad', 4).
card_layout('bomb squad', 'normal').
card_power('bomb squad', 1).
card_toughness('bomb squad', 1).

% Found in: GTC
card_name('bomber corps', 'Bomber Corps').
card_type('bomber corps', 'Creature — Human Soldier').
card_types('bomber corps', ['Creature']).
card_subtypes('bomber corps', ['Human', 'Soldier']).
card_colors('bomber corps', ['R']).
card_text('bomber corps', 'Battalion — Whenever Bomber Corps and at least two other creatures attack, Bomber Corps deals 1 damage to target creature or player.').
card_mana_cost('bomber corps', ['1', 'R']).
card_cmc('bomber corps', 2).
card_layout('bomber corps', 'normal').
card_power('bomber corps', 1).
card_toughness('bomber corps', 2).

% Found in: M13
card_name('bond beetle', 'Bond Beetle').
card_type('bond beetle', 'Creature — Insect').
card_types('bond beetle', ['Creature']).
card_subtypes('bond beetle', ['Insect']).
card_colors('bond beetle', ['G']).
card_text('bond beetle', 'When Bond Beetle enters the battlefield, put a +1/+1 counter on target creature.').
card_mana_cost('bond beetle', ['G']).
card_cmc('bond beetle', 1).
card_layout('bond beetle', 'normal').
card_power('bond beetle', 0).
card_toughness('bond beetle', 1).

% Found in: DIS
card_name('bond of agony', 'Bond of Agony').
card_type('bond of agony', 'Sorcery').
card_types('bond of agony', ['Sorcery']).
card_subtypes('bond of agony', []).
card_colors('bond of agony', ['B']).
card_text('bond of agony', 'As an additional cost to cast Bond of Agony, pay X life.\nEach other player loses X life.').
card_mana_cost('bond of agony', ['X', 'B']).
card_cmc('bond of agony', 1).
card_layout('bond of agony', 'normal').

% Found in: ORI
card_name('bonded construct', 'Bonded Construct').
card_type('bonded construct', 'Artifact Creature — Construct').
card_types('bonded construct', ['Artifact', 'Creature']).
card_subtypes('bonded construct', ['Construct']).
card_colors('bonded construct', []).
card_text('bonded construct', 'Bonded Construct can\'t attack alone.').
card_mana_cost('bonded construct', ['1']).
card_cmc('bonded construct', 1).
card_layout('bonded construct', 'normal').
card_power('bonded construct', 2).
card_toughness('bonded construct', 1).

% Found in: FUT
card_name('bonded fetch', 'Bonded Fetch').
card_type('bonded fetch', 'Creature — Homunculus').
card_types('bonded fetch', ['Creature']).
card_subtypes('bonded fetch', ['Homunculus']).
card_colors('bonded fetch', ['U']).
card_text('bonded fetch', 'Defender, haste\n{T}: Draw a card, then discard a card.').
card_mana_cost('bonded fetch', ['2', 'U']).
card_cmc('bonded fetch', 3).
card_layout('bonded fetch', 'normal').
card_power('bonded fetch', 0).
card_toughness('bonded fetch', 2).

% Found in: DDL, ISD
card_name('bonds of faith', 'Bonds of Faith').
card_type('bonds of faith', 'Enchantment — Aura').
card_types('bonds of faith', ['Enchantment']).
card_subtypes('bonds of faith', ['Aura']).
card_colors('bonds of faith', ['W']).
card_text('bonds of faith', 'Enchant creature\nEnchanted creature gets +2/+2 as long as it\'s a Human. Otherwise, it can\'t attack or block.').
card_mana_cost('bonds of faith', ['1', 'W']).
card_cmc('bonds of faith', 2).
card_layout('bonds of faith', 'normal').

% Found in: SOM
card_name('bonds of quicksilver', 'Bonds of Quicksilver').
card_type('bonds of quicksilver', 'Enchantment — Aura').
card_types('bonds of quicksilver', ['Enchantment']).
card_subtypes('bonds of quicksilver', ['Aura']).
card_colors('bonds of quicksilver', ['U']).
card_text('bonds of quicksilver', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_mana_cost('bonds of quicksilver', ['3', 'U']).
card_cmc('bonds of quicksilver', 4).
card_layout('bonds of quicksilver', 'normal').

% Found in: WTH
card_name('bone dancer', 'Bone Dancer').
card_type('bone dancer', 'Creature — Zombie').
card_types('bone dancer', ['Creature']).
card_subtypes('bone dancer', ['Zombie']).
card_colors('bone dancer', ['B']).
card_text('bone dancer', 'Whenever Bone Dancer attacks and isn\'t blocked, you may put the top creature card of defending player\'s graveyard onto the battlefield under your control. If you do, Bone Dancer assigns no combat damage this turn.').
card_mana_cost('bone dancer', ['1', 'B', 'B']).
card_cmc('bone dancer', 3).
card_layout('bone dancer', 'normal').
card_power('bone dancer', 2).
card_toughness('bone dancer', 2).
card_reserved('bone dancer').

% Found in: DRK, ME3
card_name('bone flute', 'Bone Flute').
card_type('bone flute', 'Artifact').
card_types('bone flute', ['Artifact']).
card_subtypes('bone flute', []).
card_colors('bone flute', []).
card_text('bone flute', '{2}, {T}: All creatures get -1/-0 until end of turn.').
card_mana_cost('bone flute', ['3']).
card_cmc('bone flute', 3).
card_layout('bone flute', 'normal').

% Found in: BTD, MIR
card_name('bone harvest', 'Bone Harvest').
card_type('bone harvest', 'Instant').
card_types('bone harvest', ['Instant']).
card_subtypes('bone harvest', []).
card_colors('bone harvest', ['B']).
card_text('bone harvest', 'Put any number of target creature cards from your graveyard on top of your library.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('bone harvest', ['2', 'B']).
card_cmc('bone harvest', 3).
card_layout('bone harvest', 'normal').

% Found in: MIR
card_name('bone mask', 'Bone Mask').
card_type('bone mask', 'Artifact').
card_types('bone mask', ['Artifact']).
card_subtypes('bone mask', []).
card_colors('bone mask', []).
card_text('bone mask', '{2}, {T}: The next time a source of your choice would deal damage to you this turn, prevent that damage. Exile cards from the top of your library equal to the damage prevented this way.').
card_mana_cost('bone mask', ['4']).
card_cmc('bone mask', 4).
card_layout('bone mask', 'normal').
card_reserved('bone mask').

% Found in: CON
card_name('bone saw', 'Bone Saw').
card_type('bone saw', 'Artifact — Equipment').
card_types('bone saw', ['Artifact']).
card_subtypes('bone saw', ['Equipment']).
card_colors('bone saw', []).
card_text('bone saw', 'Equipped creature gets +1/+0.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('bone saw', ['0']).
card_cmc('bone saw', 0).
card_layout('bone saw', 'normal').

% Found in: ICE
card_name('bone shaman', 'Bone Shaman').
card_type('bone shaman', 'Creature — Giant Shaman').
card_types('bone shaman', ['Creature']).
card_subtypes('bone shaman', ['Giant', 'Shaman']).
card_colors('bone shaman', ['R']).
card_text('bone shaman', '{B}: Until end of turn, Bone Shaman gains \"Creatures dealt damage by Bone Shaman this turn can\'t be regenerated this turn.\"').
card_mana_cost('bone shaman', ['2', 'R', 'R']).
card_cmc('bone shaman', 4).
card_layout('bone shaman', 'normal').
card_power('bone shaman', 3).
card_toughness('bone shaman', 3).

% Found in: DDE, ULG
card_name('bone shredder', 'Bone Shredder').
card_type('bone shredder', 'Creature — Minion').
card_types('bone shredder', ['Creature']).
card_subtypes('bone shredder', ['Minion']).
card_colors('bone shredder', ['B']).
card_text('bone shredder', 'Flying\nEcho {2}{B} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Bone Shredder enters the battlefield, destroy target nonartifact, nonblack creature.').
card_mana_cost('bone shredder', ['2', 'B']).
card_cmc('bone shredder', 3).
card_layout('bone shredder', 'normal').
card_power('bone shredder', 1).
card_toughness('bone shredder', 1).

% Found in: ALA, AVR, BFZ, DDN, MM2
card_name('bone splinters', 'Bone Splinters').
card_type('bone splinters', 'Sorcery').
card_types('bone splinters', ['Sorcery']).
card_subtypes('bone splinters', []).
card_colors('bone splinters', ['B']).
card_text('bone splinters', 'As an additional cost to cast Bone Splinters, sacrifice a creature.\nDestroy target creature.').
card_mana_cost('bone splinters', ['B']).
card_cmc('bone splinters', 1).
card_layout('bone splinters', 'normal').

% Found in: DKA, ORI
card_name('bone to ash', 'Bone to Ash').
card_type('bone to ash', 'Instant').
card_types('bone to ash', ['Instant']).
card_subtypes('bone to ash', []).
card_colors('bone to ash', ['U']).
card_text('bone to ash', 'Counter target creature spell.\nDraw a card.').
card_mana_cost('bone to ash', ['2', 'U', 'U']).
card_cmc('bone to ash', 4).
card_layout('bone to ash', 'normal').

% Found in: M12
card_name('bonebreaker giant', 'Bonebreaker Giant').
card_type('bonebreaker giant', 'Creature — Giant').
card_types('bonebreaker giant', ['Creature']).
card_subtypes('bonebreaker giant', ['Giant']).
card_colors('bonebreaker giant', ['R']).
card_text('bonebreaker giant', '').
card_mana_cost('bonebreaker giant', ['4', 'R']).
card_cmc('bonebreaker giant', 5).
card_layout('bonebreaker giant', 'normal').
card_power('bonebreaker giant', 4).
card_toughness('bonebreaker giant', 4).

% Found in: C14, MBS
card_name('bonehoard', 'Bonehoard').
card_type('bonehoard', 'Artifact — Equipment').
card_types('bonehoard', ['Artifact']).
card_subtypes('bonehoard', ['Equipment']).
card_colors('bonehoard', []).
card_text('bonehoard', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +X/+X, where X is the number of creature cards in all graveyards.\nEquip {2}').
card_mana_cost('bonehoard', ['4']).
card_cmc('bonehoard', 4).
card_layout('bonehoard', 'normal').

% Found in: ONS
card_name('boneknitter', 'Boneknitter').
card_type('boneknitter', 'Creature — Zombie Cleric').
card_types('boneknitter', ['Creature']).
card_subtypes('boneknitter', ['Zombie', 'Cleric']).
card_colors('boneknitter', ['B']).
card_text('boneknitter', '{1}{B}: Regenerate target Zombie.\nMorph {2}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('boneknitter', ['1', 'B']).
card_cmc('boneknitter', 2).
card_layout('boneknitter', 'normal').
card_power('boneknitter', 1).
card_toughness('boneknitter', 1).

% Found in: M14, pMEI
card_name('bonescythe sliver', 'Bonescythe Sliver').
card_type('bonescythe sliver', 'Creature — Sliver').
card_types('bonescythe sliver', ['Creature']).
card_subtypes('bonescythe sliver', ['Sliver']).
card_colors('bonescythe sliver', ['W']).
card_text('bonescythe sliver', 'Sliver creatures you control have double strike. (They deal both first-strike and regular combat damage.)').
card_mana_cost('bonescythe sliver', ['3', 'W']).
card_cmc('bonescythe sliver', 4).
card_layout('bonescythe sliver', 'normal').
card_power('bonescythe sliver', 2).
card_toughness('bonescythe sliver', 2).

% Found in: TOR
card_name('boneshard slasher', 'Boneshard Slasher').
card_type('boneshard slasher', 'Creature — Horror').
card_types('boneshard slasher', ['Creature']).
card_subtypes('boneshard slasher', ['Horror']).
card_colors('boneshard slasher', ['B']).
card_text('boneshard slasher', 'Flying\nThreshold — As long as seven or more cards are in your graveyard, Boneshard Slasher gets +2/+2 and has \"When Boneshard Slasher becomes the target of a spell or ability, sacrifice it.\"').
card_mana_cost('boneshard slasher', ['1', 'B']).
card_cmc('boneshard slasher', 2).
card_layout('boneshard slasher', 'normal').
card_power('boneshard slasher', 1).
card_toughness('boneshard slasher', 1).

% Found in: MMA, MRD, pARL
card_name('bonesplitter', 'Bonesplitter').
card_type('bonesplitter', 'Artifact — Equipment').
card_types('bonesplitter', ['Artifact']).
card_subtypes('bonesplitter', ['Equipment']).
card_colors('bonesplitter', []).
card_text('bonesplitter', 'Equipped creature gets +2/+0.\nEquip {1}').
card_mana_cost('bonesplitter', ['1']).
card_cmc('bonesplitter', 1).
card_layout('bonesplitter', 'normal').

% Found in: TSP
card_name('bonesplitter sliver', 'Bonesplitter Sliver').
card_type('bonesplitter sliver', 'Creature — Sliver').
card_types('bonesplitter sliver', ['Creature']).
card_subtypes('bonesplitter sliver', ['Sliver']).
card_colors('bonesplitter sliver', ['R']).
card_text('bonesplitter sliver', 'All Sliver creatures get +2/+0.').
card_mana_cost('bonesplitter sliver', ['3', 'R']).
card_cmc('bonesplitter sliver', 4).
card_layout('bonesplitter sliver', 'normal').
card_power('bonesplitter sliver', 2).
card_toughness('bonesplitter sliver', 2).

% Found in: SCG
card_name('bonethorn valesk', 'Bonethorn Valesk').
card_type('bonethorn valesk', 'Creature — Beast').
card_types('bonethorn valesk', ['Creature']).
card_subtypes('bonethorn valesk', ['Beast']).
card_colors('bonethorn valesk', ['R']).
card_text('bonethorn valesk', 'Whenever a permanent is turned face up, Bonethorn Valesk deals 1 damage to target creature or player.').
card_mana_cost('bonethorn valesk', ['4', 'R']).
card_cmc('bonethorn valesk', 5).
card_layout('bonethorn valesk', 'normal').
card_power('bonethorn valesk', 4).
card_toughness('bonethorn valesk', 2).

% Found in: DDJ, ISD, pWPN
card_name('boneyard wurm', 'Boneyard Wurm').
card_type('boneyard wurm', 'Creature — Wurm').
card_types('boneyard wurm', ['Creature']).
card_subtypes('boneyard wurm', ['Wurm']).
card_colors('boneyard wurm', ['G']).
card_text('boneyard wurm', 'Boneyard Wurm\'s power and toughness are each equal to the number of creature cards in your graveyard.').
card_mana_cost('boneyard wurm', ['1', 'G']).
card_cmc('boneyard wurm', 2).
card_layout('boneyard wurm', 'normal').
card_power('boneyard wurm', '*').
card_toughness('boneyard wurm', '*').

% Found in: AVR
card_name('bonfire of the damned', 'Bonfire of the Damned').
card_type('bonfire of the damned', 'Sorcery').
card_types('bonfire of the damned', ['Sorcery']).
card_subtypes('bonfire of the damned', []).
card_colors('bonfire of the damned', ['R']).
card_text('bonfire of the damned', 'Bonfire of the Damned deals X damage to target player and each creature he or she controls.\nMiracle {X}{R} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_mana_cost('bonfire of the damned', ['X', 'X', 'R']).
card_cmc('bonfire of the damned', 1).
card_layout('bonfire of the damned', 'normal').

% Found in: 9ED, TMP
card_name('booby trap', 'Booby Trap').
card_type('booby trap', 'Artifact').
card_types('booby trap', ['Artifact']).
card_subtypes('booby trap', []).
card_colors('booby trap', []).
card_text('booby trap', 'As Booby Trap enters the battlefield, name a card other than a basic land card and choose an opponent.\nThe chosen player reveals each card he or she draws.\nWhen the chosen player draws the named card, sacrifice Booby Trap. If you do, Booby Trap deals 10 damage to that player.').
card_mana_cost('booby trap', ['6']).
card_cmc('booby trap', 6).
card_layout('booby trap', 'normal').

% Found in: JUD
card_name('book burning', 'Book Burning').
card_type('book burning', 'Sorcery').
card_types('book burning', ['Sorcery']).
card_subtypes('book burning', []).
card_colors('book burning', ['R']).
card_text('book burning', 'Any player may have Book Burning deal 6 damage to him or her. If no one does, target player puts the top six cards of his or her library into his or her graveyard.').
card_mana_cost('book burning', ['1', 'R']).
card_cmc('book burning', 2).
card_layout('book burning', 'normal').

% Found in: CHR, DRK, ME4
card_name('book of rass', 'Book of Rass').
card_type('book of rass', 'Artifact').
card_types('book of rass', ['Artifact']).
card_subtypes('book of rass', []).
card_colors('book of rass', []).
card_text('book of rass', '{2}, Pay 2 life: Draw a card.').
card_mana_cost('book of rass', ['6']).
card_cmc('book of rass', 6).
card_layout('book of rass', 'normal').

% Found in: PLC
card_name('boom', 'Boom').
card_type('boom', 'Sorcery').
card_types('boom', ['Sorcery']).
card_subtypes('boom', []).
card_colors('boom', ['R']).
card_text('boom', 'Destroy target land you control and target land you don\'t control.').
card_mana_cost('boom', ['1', 'R']).
card_cmc('boom', 2).
card_layout('boom', 'split').
card_sides('boom', 'bust').

% Found in: 10E, 5ED, 6ED, 7ED, 8ED, 9ED, CHR, DPA, LEG, ME3, MIR, pGTW
card_name('boomerang', 'Boomerang').
card_type('boomerang', 'Instant').
card_types('boomerang', ['Instant']).
card_subtypes('boomerang', []).
card_colors('boomerang', ['U']).
card_text('boomerang', 'Return target permanent to its owner\'s hand.').
card_mana_cost('boomerang', ['U', 'U']).
card_cmc('boomerang', 2).
card_layout('boomerang', 'normal').

% Found in: THS
card_name('boon of erebos', 'Boon of Erebos').
card_type('boon of erebos', 'Instant').
card_types('boon of erebos', ['Instant']).
card_subtypes('boon of erebos', []).
card_colors('boon of erebos', ['B']).
card_text('boon of erebos', 'Target creature gets +2/+0 until end of turn. Regenerate it. You lose 2 life.').
card_mana_cost('boon of erebos', ['B']).
card_cmc('boon of erebos', 1).
card_layout('boon of erebos', 'normal').

% Found in: SHM
card_name('boon reflection', 'Boon Reflection').
card_type('boon reflection', 'Enchantment').
card_types('boon reflection', ['Enchantment']).
card_subtypes('boon reflection', []).
card_colors('boon reflection', ['W']).
card_text('boon reflection', 'If you would gain life, you gain twice that much life instead.').
card_mana_cost('boon reflection', ['4', 'W']).
card_cmc('boon reflection', 5).
card_layout('boon reflection', 'normal').

% Found in: THS
card_name('boon satyr', 'Boon Satyr').
card_type('boon satyr', 'Enchantment Creature — Satyr').
card_types('boon satyr', ['Enchantment', 'Creature']).
card_subtypes('boon satyr', ['Satyr']).
card_colors('boon satyr', ['G']).
card_text('boon satyr', 'Flash\nBestow {3}{G}{G} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nEnchanted creature gets +4/+2.').
card_mana_cost('boon satyr', ['1', 'G', 'G']).
card_cmc('boon satyr', 3).
card_layout('boon satyr', 'normal').
card_power('boon satyr', 4).
card_toughness('boon satyr', 2).

% Found in: M15
card_name('boonweaver giant', 'Boonweaver Giant').
card_type('boonweaver giant', 'Creature — Giant Monk').
card_types('boonweaver giant', ['Creature']).
card_subtypes('boonweaver giant', ['Giant', 'Monk']).
card_colors('boonweaver giant', ['W']).
card_text('boonweaver giant', 'When Boonweaver Giant enters the battlefield, you may search your graveyard, hand, and/or library for an Aura card and put it onto the battlefield attached to Boonweaver Giant. If you search your library this way, shuffle it.').
card_mana_cost('boonweaver giant', ['6', 'W']).
card_cmc('boonweaver giant', 7).
card_layout('boonweaver giant', 'normal').
card_power('boonweaver giant', 4).
card_toughness('boonweaver giant', 4).

% Found in: UNH, pARL
card_name('booster tutor', 'Booster Tutor').
card_type('booster tutor', 'Instant').
card_types('booster tutor', ['Instant']).
card_subtypes('booster tutor', []).
card_colors('booster tutor', ['B']).
card_text('booster tutor', 'Open a sealed Magic booster pack, reveal the cards, and put one of those cards into your hand. (Remove that card from your deck before beginning a new game.)').
card_mana_cost('booster tutor', ['B']).
card_cmc('booster tutor', 1).
card_layout('booster tutor', 'normal').

% Found in: GPT
card_name('borborygmos', 'Borborygmos').
card_type('borborygmos', 'Legendary Creature — Cyclops').
card_types('borborygmos', ['Creature']).
card_subtypes('borborygmos', ['Cyclops']).
card_supertypes('borborygmos', ['Legendary']).
card_colors('borborygmos', ['R', 'G']).
card_text('borborygmos', 'Trample\nWhenever Borborygmos deals combat damage to a player, put a +1/+1 counter on each creature you control.').
card_mana_cost('borborygmos', ['3', 'R', 'R', 'G', 'G']).
card_cmc('borborygmos', 7).
card_layout('borborygmos', 'normal').
card_power('borborygmos', 6).
card_toughness('borborygmos', 7).

% Found in: GTC
card_name('borborygmos enraged', 'Borborygmos Enraged').
card_type('borborygmos enraged', 'Legendary Creature — Cyclops').
card_types('borborygmos enraged', ['Creature']).
card_subtypes('borborygmos enraged', ['Cyclops']).
card_supertypes('borborygmos enraged', ['Legendary']).
card_colors('borborygmos enraged', ['R', 'G']).
card_text('borborygmos enraged', 'Trample\nWhenever Borborygmos Enraged deals combat damage to a player, reveal the top three cards of your library. Put all land cards revealed this way into your hand and the rest into your graveyard.\nDiscard a land card: Borborygmos Enraged deals 3 damage to target creature or player.').
card_mana_cost('borborygmos enraged', ['4', 'R', 'R', 'G', 'G']).
card_cmc('borborygmos enraged', 8).
card_layout('borborygmos enraged', 'normal').
card_power('borborygmos enraged', 7).
card_toughness('borborygmos enraged', 6).

% Found in: POR, S99
card_name('border guard', 'Border Guard').
card_type('border guard', 'Creature — Human Soldier').
card_types('border guard', ['Creature']).
card_subtypes('border guard', ['Human', 'Soldier']).
card_colors('border guard', ['W']).
card_text('border guard', '').
card_mana_cost('border guard', ['2', 'W']).
card_cmc('border guard', 3).
card_layout('border guard', 'normal').
card_power('border guard', 1).
card_toughness('border guard', 4).

% Found in: JUD
card_name('border patrol', 'Border Patrol').
card_type('border patrol', 'Creature — Human Nomad').
card_types('border patrol', ['Creature']).
card_subtypes('border patrol', ['Human', 'Nomad']).
card_colors('border patrol', ['W']).
card_text('border patrol', 'Vigilance').
card_mana_cost('border patrol', ['4', 'W']).
card_cmc('border patrol', 5).
card_layout('border patrol', 'normal').
card_power('border patrol', 1).
card_toughness('border patrol', 6).

% Found in: MOR
card_name('borderland behemoth', 'Borderland Behemoth').
card_type('borderland behemoth', 'Creature — Giant Warrior').
card_types('borderland behemoth', ['Creature']).
card_subtypes('borderland behemoth', ['Giant', 'Warrior']).
card_colors('borderland behemoth', ['R']).
card_text('borderland behemoth', 'Trample\nBorderland Behemoth gets +4/+4 for each other Giant you control.').
card_mana_cost('borderland behemoth', ['5', 'R', 'R']).
card_cmc('borderland behemoth', 7).
card_layout('borderland behemoth', 'normal').
card_power('borderland behemoth', 4).
card_toughness('borderland behemoth', 4).

% Found in: M15
card_name('borderland marauder', 'Borderland Marauder').
card_type('borderland marauder', 'Creature — Human Warrior').
card_types('borderland marauder', ['Creature']).
card_subtypes('borderland marauder', ['Human', 'Warrior']).
card_colors('borderland marauder', ['R']).
card_text('borderland marauder', 'Whenever Borderland Marauder attacks, it gets +2/+0 until end of turn.').
card_mana_cost('borderland marauder', ['1', 'R']).
card_cmc('borderland marauder', 2).
card_layout('borderland marauder', 'normal').
card_power('borderland marauder', 1).
card_toughness('borderland marauder', 2).

% Found in: THS
card_name('borderland minotaur', 'Borderland Minotaur').
card_type('borderland minotaur', 'Creature — Minotaur Warrior').
card_types('borderland minotaur', ['Creature']).
card_subtypes('borderland minotaur', ['Minotaur', 'Warrior']).
card_colors('borderland minotaur', ['R']).
card_text('borderland minotaur', '').
card_mana_cost('borderland minotaur', ['2', 'R', 'R']).
card_cmc('borderland minotaur', 4).
card_layout('borderland minotaur', 'normal').
card_power('borderland minotaur', 4).
card_toughness('borderland minotaur', 3).

% Found in: AVR, M10
card_name('borderland ranger', 'Borderland Ranger').
card_type('borderland ranger', 'Creature — Human Scout').
card_types('borderland ranger', ['Creature']).
card_subtypes('borderland ranger', ['Human', 'Scout']).
card_colors('borderland ranger', ['G']).
card_text('borderland ranger', 'When Borderland Ranger enters the battlefield, you may search your library for a basic land card, reveal it, and put it into your hand. If you do, shuffle your library.').
card_mana_cost('borderland ranger', ['2', 'G']).
card_cmc('borderland ranger', 3).
card_layout('borderland ranger', 'normal').
card_power('borderland ranger', 2).
card_toughness('borderland ranger', 2).

% Found in: CSP
card_name('boreal centaur', 'Boreal Centaur').
card_type('boreal centaur', 'Snow Creature — Centaur Warrior').
card_types('boreal centaur', ['Creature']).
card_subtypes('boreal centaur', ['Centaur', 'Warrior']).
card_supertypes('boreal centaur', ['Snow']).
card_colors('boreal centaur', ['G']).
card_text('boreal centaur', '{S}: Boreal Centaur gets +1/+1 until end of turn. Activate this ability only once each turn. ({S} can be paid with one mana from a snow permanent.)').
card_mana_cost('boreal centaur', ['1', 'G']).
card_cmc('boreal centaur', 2).
card_layout('boreal centaur', 'normal').
card_power('boreal centaur', 2).
card_toughness('boreal centaur', 2).

% Found in: CSP
card_name('boreal druid', 'Boreal Druid').
card_type('boreal druid', 'Snow Creature — Elf Druid').
card_types('boreal druid', ['Creature']).
card_subtypes('boreal druid', ['Elf', 'Druid']).
card_supertypes('boreal druid', ['Snow']).
card_colors('boreal druid', ['G']).
card_text('boreal druid', '{T}: Add {1} to your mana pool.').
card_mana_cost('boreal druid', ['G']).
card_cmc('boreal druid', 1).
card_layout('boreal druid', 'normal').
card_power('boreal druid', 1).
card_toughness('boreal druid', 1).

% Found in: CSP
card_name('boreal griffin', 'Boreal Griffin').
card_type('boreal griffin', 'Snow Creature — Griffin').
card_types('boreal griffin', ['Creature']).
card_subtypes('boreal griffin', ['Griffin']).
card_supertypes('boreal griffin', ['Snow']).
card_colors('boreal griffin', ['W']).
card_text('boreal griffin', 'Flying\n{S}: Boreal Griffin gains first strike until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_mana_cost('boreal griffin', ['3', 'W', 'W']).
card_cmc('boreal griffin', 5).
card_layout('boreal griffin', 'normal').
card_power('boreal griffin', 3).
card_toughness('boreal griffin', 2).

% Found in: CSP
card_name('boreal shelf', 'Boreal Shelf').
card_type('boreal shelf', 'Snow Land').
card_types('boreal shelf', ['Land']).
card_subtypes('boreal shelf', []).
card_supertypes('boreal shelf', ['Snow']).
card_colors('boreal shelf', []).
card_text('boreal shelf', 'Boreal Shelf enters the battlefield tapped.\n{T}: Add {W} or {U} to your mana pool.').
card_layout('boreal shelf', 'normal').

% Found in: LEG, ME3
card_name('boris devilboon', 'Boris Devilboon').
card_type('boris devilboon', 'Legendary Creature — Zombie Wizard').
card_types('boris devilboon', ['Creature']).
card_subtypes('boris devilboon', ['Zombie', 'Wizard']).
card_supertypes('boris devilboon', ['Legendary']).
card_colors('boris devilboon', ['B', 'R']).
card_text('boris devilboon', '{2}{B}{R}, {T}: Put a 1/1 black and red Demon creature token named Minor Demon onto the battlefield.').
card_mana_cost('boris devilboon', ['3', 'B', 'R']).
card_cmc('boris devilboon', 5).
card_layout('boris devilboon', 'normal').
card_power('boris devilboon', 2).
card_toughness('boris devilboon', 2).
card_reserved('boris devilboon').

% Found in: DGM
card_name('boros battleshaper', 'Boros Battleshaper').
card_type('boros battleshaper', 'Creature — Minotaur Soldier').
card_types('boros battleshaper', ['Creature']).
card_subtypes('boros battleshaper', ['Minotaur', 'Soldier']).
card_colors('boros battleshaper', ['W', 'R']).
card_text('boros battleshaper', 'At the beginning of each combat, up to one target creature attacks or blocks this combat if able and up to one target creature can\'t attack or block this combat.').
card_mana_cost('boros battleshaper', ['5', 'R', 'W']).
card_cmc('boros battleshaper', 7).
card_layout('boros battleshaper', 'normal').
card_power('boros battleshaper', 5).
card_toughness('boros battleshaper', 5).

% Found in: C13, GTC
card_name('boros charm', 'Boros Charm').
card_type('boros charm', 'Instant').
card_types('boros charm', ['Instant']).
card_subtypes('boros charm', []).
card_colors('boros charm', ['W', 'R']).
card_text('boros charm', 'Choose one —\n• Boros Charm deals 4 damage to target player.\n• Permanents you control gain indestructible until end of turn.\n• Target creature gains double strike until end of turn.').
card_mana_cost('boros charm', ['R', 'W']).
card_cmc('boros charm', 2).
card_layout('boros charm', 'normal').

% Found in: DGM
card_name('boros cluestone', 'Boros Cluestone').
card_type('boros cluestone', 'Artifact').
card_types('boros cluestone', ['Artifact']).
card_subtypes('boros cluestone', []).
card_colors('boros cluestone', []).
card_text('boros cluestone', '{T}: Add {R} or {W} to your mana pool.\n{R}{W}, {T}, Sacrifice Boros Cluestone: Draw a card.').
card_mana_cost('boros cluestone', ['3']).
card_cmc('boros cluestone', 3).
card_layout('boros cluestone', 'normal').

% Found in: GTC
card_name('boros elite', 'Boros Elite').
card_type('boros elite', 'Creature — Human Soldier').
card_types('boros elite', ['Creature']).
card_subtypes('boros elite', ['Human', 'Soldier']).
card_colors('boros elite', ['W']).
card_text('boros elite', 'Battalion — Whenever Boros Elite and at least two other creatures attack, Boros Elite gets +2/+2 until end of turn.').
card_mana_cost('boros elite', ['W']).
card_cmc('boros elite', 1).
card_layout('boros elite', 'normal').
card_power('boros elite', 1).
card_toughness('boros elite', 1).

% Found in: RAV
card_name('boros fury-shield', 'Boros Fury-Shield').
card_type('boros fury-shield', 'Instant').
card_types('boros fury-shield', ['Instant']).
card_subtypes('boros fury-shield', []).
card_colors('boros fury-shield', ['W']).
card_text('boros fury-shield', 'Prevent all combat damage that would be dealt by target attacking or blocking creature this turn. If {R} was spent to cast Boros Fury-Shield, it deals damage to that creature\'s controller equal to the creature\'s power.').
card_mana_cost('boros fury-shield', ['2', 'W']).
card_cmc('boros fury-shield', 3).
card_layout('boros fury-shield', 'normal').

% Found in: C13, CMD, HOP, MM2, RAV
card_name('boros garrison', 'Boros Garrison').
card_type('boros garrison', 'Land').
card_types('boros garrison', ['Land']).
card_subtypes('boros garrison', []).
card_colors('boros garrison', []).
card_text('boros garrison', 'Boros Garrison enters the battlefield tapped.\nWhen Boros Garrison enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {R}{W} to your mana pool.').
card_layout('boros garrison', 'normal').

% Found in: C13, DDL, DGM, GTC
card_name('boros guildgate', 'Boros Guildgate').
card_type('boros guildgate', 'Land — Gate').
card_types('boros guildgate', ['Land']).
card_subtypes('boros guildgate', ['Gate']).
card_colors('boros guildgate', []).
card_text('boros guildgate', 'Boros Guildgate enters the battlefield tapped.\n{T}: Add {R} or {W} to your mana pool.').
card_layout('boros guildgate', 'normal').

% Found in: CMD, HOP, RAV
card_name('boros guildmage', 'Boros Guildmage').
card_type('boros guildmage', 'Creature — Human Wizard').
card_types('boros guildmage', ['Creature']).
card_subtypes('boros guildmage', ['Human', 'Wizard']).
card_colors('boros guildmage', ['W', 'R']).
card_text('boros guildmage', '{1}{R}: Target creature gains haste until end of turn.\n{1}{W}: Target creature gains first strike until end of turn.').
card_mana_cost('boros guildmage', ['R/W', 'R/W']).
card_cmc('boros guildmage', 2).
card_layout('boros guildmage', 'normal').
card_power('boros guildmage', 2).
card_toughness('boros guildmage', 2).

% Found in: GTC
card_name('boros keyrune', 'Boros Keyrune').
card_type('boros keyrune', 'Artifact').
card_types('boros keyrune', ['Artifact']).
card_subtypes('boros keyrune', []).
card_colors('boros keyrune', []).
card_text('boros keyrune', '{T}: Add {R} or {W} to your mana pool.\n{R}{W}: Boros Keyrune becomes a 1/1 red and white Soldier artifact creature with double strike until end of turn. (It deals both first-strike and regular combat damage.)').
card_mana_cost('boros keyrune', ['3']).
card_cmc('boros keyrune', 3).
card_layout('boros keyrune', 'normal').

% Found in: DGM
card_name('boros mastiff', 'Boros Mastiff').
card_type('boros mastiff', 'Creature — Hound').
card_types('boros mastiff', ['Creature']).
card_subtypes('boros mastiff', ['Hound']).
card_colors('boros mastiff', ['W']).
card_text('boros mastiff', 'Battalion — Whenever Boros Mastiff and at least two other creatures attack, Boros Mastiff gains lifelink until end of turn. (Damage dealt by a creature with lifelink also causes its controller to gain that much life.)').
card_mana_cost('boros mastiff', ['1', 'W']).
card_cmc('boros mastiff', 2).
card_layout('boros mastiff', 'normal').
card_power('boros mastiff', 2).
card_toughness('boros mastiff', 2).

% Found in: GTC
card_name('boros reckoner', 'Boros Reckoner').
card_type('boros reckoner', 'Creature — Minotaur Wizard').
card_types('boros reckoner', ['Creature']).
card_subtypes('boros reckoner', ['Minotaur', 'Wizard']).
card_colors('boros reckoner', ['W', 'R']).
card_text('boros reckoner', 'Whenever Boros Reckoner is dealt damage, it deals that much damage to target creature or player.\n{R/W}: Boros Reckoner gains first strike until end of turn.').
card_mana_cost('boros reckoner', ['R/W', 'R/W', 'R/W']).
card_cmc('boros reckoner', 3).
card_layout('boros reckoner', 'normal').
card_power('boros reckoner', 3).
card_toughness('boros reckoner', 3).

% Found in: RAV
card_name('boros recruit', 'Boros Recruit').
card_type('boros recruit', 'Creature — Goblin Soldier').
card_types('boros recruit', ['Creature']).
card_subtypes('boros recruit', ['Goblin', 'Soldier']).
card_colors('boros recruit', ['W', 'R']).
card_text('boros recruit', '({R/W} can be paid with either {R} or {W}.)\nFirst strike').
card_mana_cost('boros recruit', ['R/W']).
card_cmc('boros recruit', 1).
card_layout('boros recruit', 'normal').
card_power('boros recruit', 1).
card_toughness('boros recruit', 1).

% Found in: CMD, HOP, RAV
card_name('boros signet', 'Boros Signet').
card_type('boros signet', 'Artifact').
card_types('boros signet', ['Artifact']).
card_subtypes('boros signet', []).
card_colors('boros signet', []).
card_text('boros signet', '{1}, {T}: Add {R}{W} to your mana pool.').
card_mana_cost('boros signet', ['2']).
card_cmc('boros signet', 2).
card_layout('boros signet', 'normal').

% Found in: HOP, MM2, RAV
card_name('boros swiftblade', 'Boros Swiftblade').
card_type('boros swiftblade', 'Creature — Human Soldier').
card_types('boros swiftblade', ['Creature']).
card_subtypes('boros swiftblade', ['Human', 'Soldier']).
card_colors('boros swiftblade', ['W', 'R']).
card_text('boros swiftblade', 'Double strike').
card_mana_cost('boros swiftblade', ['R', 'W']).
card_cmc('boros swiftblade', 2).
card_layout('boros swiftblade', 'normal').
card_power('boros swiftblade', 1).
card_toughness('boros swiftblade', 2).

% Found in: C13, ME3, PTK
card_name('borrowing 100,000 arrows', 'Borrowing 100,000 Arrows').
card_type('borrowing 100,000 arrows', 'Sorcery').
card_types('borrowing 100,000 arrows', ['Sorcery']).
card_subtypes('borrowing 100,000 arrows', []).
card_colors('borrowing 100,000 arrows', ['U']).
card_text('borrowing 100,000 arrows', 'Draw a card for each tapped creature target opponent controls.').
card_mana_cost('borrowing 100,000 arrows', ['2', 'U']).
card_cmc('borrowing 100,000 arrows', 3).
card_layout('borrowing 100,000 arrows', 'normal').

% Found in: PTK
card_name('borrowing the east wind', 'Borrowing the East Wind').
card_type('borrowing the east wind', 'Sorcery').
card_types('borrowing the east wind', ['Sorcery']).
card_subtypes('borrowing the east wind', []).
card_colors('borrowing the east wind', ['G']).
card_text('borrowing the east wind', 'Borrowing the East Wind deals X damage to each creature with horsemanship and each player.').
card_mana_cost('borrowing the east wind', ['X', 'G', 'G']).
card_cmc('borrowing the east wind', 2).
card_layout('borrowing the east wind', 'normal').

% Found in: CHK, V12
card_name('boseiju, who shelters all', 'Boseiju, Who Shelters All').
card_type('boseiju, who shelters all', 'Legendary Land').
card_types('boseiju, who shelters all', ['Land']).
card_subtypes('boseiju, who shelters all', []).
card_supertypes('boseiju, who shelters all', ['Legendary']).
card_colors('boseiju, who shelters all', []).
card_text('boseiju, who shelters all', 'Boseiju, Who Shelters All enters the battlefield tapped.\n{T}, Pay 2 life: Add {1} to your mana pool. If that mana is spent on an instant or sorcery spell, that spell can\'t be countered by spells or abilities.').
card_layout('boseiju, who shelters all', 'normal').

% Found in: C14, HOP, MRD
card_name('bosh, iron golem', 'Bosh, Iron Golem').
card_type('bosh, iron golem', 'Legendary Artifact Creature — Golem').
card_types('bosh, iron golem', ['Artifact', 'Creature']).
card_subtypes('bosh, iron golem', ['Golem']).
card_supertypes('bosh, iron golem', ['Legendary']).
card_colors('bosh, iron golem', []).
card_text('bosh, iron golem', 'Trample\n{3}{R}, Sacrifice an artifact: Bosh, Iron Golem deals damage equal to the sacrificed artifact\'s converted mana cost to target creature or player.').
card_mana_cost('bosh, iron golem', ['8']).
card_cmc('bosh, iron golem', 8).
card_layout('bosh, iron golem', 'normal').
card_power('bosh, iron golem', 6).
card_toughness('bosh, iron golem', 7).

% Found in: VAN
card_name('bosh, iron golem avatar', 'Bosh, Iron Golem Avatar').
card_type('bosh, iron golem avatar', 'Vanguard').
card_types('bosh, iron golem avatar', ['Vanguard']).
card_subtypes('bosh, iron golem avatar', []).
card_colors('bosh, iron golem avatar', []).
card_text('bosh, iron golem avatar', '{X}, Sacrifice an artifact with converted mana cost X: Bosh, Iron Golem Avatar deals X damage to target creature or player.').
card_layout('bosh, iron golem avatar', 'vanguard').

% Found in: MOR
card_name('bosk banneret', 'Bosk Banneret').
card_type('bosk banneret', 'Creature — Treefolk Shaman').
card_types('bosk banneret', ['Creature']).
card_subtypes('bosk banneret', ['Treefolk', 'Shaman']).
card_colors('bosk banneret', ['G']).
card_text('bosk banneret', 'Treefolk spells and Shaman spells you cast cost {1} less to cast.').
card_mana_cost('bosk banneret', ['1', 'G']).
card_cmc('bosk banneret', 2).
card_layout('bosk banneret', 'normal').
card_power('bosk banneret', 1).
card_toughness('bosk banneret', 3).

% Found in: UNH
card_name('bosom buddy', 'Bosom Buddy').
card_type('bosom buddy', 'Creature — Elephant Townsfolk').
card_types('bosom buddy', ['Creature']).
card_subtypes('bosom buddy', ['Elephant', 'Townsfolk']).
card_colors('bosom buddy', ['W']).
card_text('bosom buddy', 'Whenever you play a spell, you may gain ½ life for each word in that spell\'s name.').
card_mana_cost('bosom buddy', ['3', 'W']).
card_cmc('bosom buddy', 4).
card_layout('bosom buddy', 'normal').
card_power('bosom buddy', 1).
card_toughness('bosom buddy', 4).

% Found in: 10E, 9ED, C14, DD2, DD3_JVC, MRD, TMP, TPR, pFNM
card_name('bottle gnomes', 'Bottle Gnomes').
card_type('bottle gnomes', 'Artifact Creature — Gnome').
card_types('bottle gnomes', ['Artifact', 'Creature']).
card_subtypes('bottle gnomes', ['Gnome']).
card_colors('bottle gnomes', []).
card_text('bottle gnomes', 'Sacrifice Bottle Gnomes: You gain 3 life.').
card_mana_cost('bottle gnomes', ['3']).
card_cmc('bottle gnomes', 3).
card_layout('bottle gnomes', 'normal').
card_power('bottle gnomes', 1).
card_toughness('bottle gnomes', 3).

% Found in: 3ED, 4ED, 5ED, 6ED, ARN, ME4
card_name('bottle of suleiman', 'Bottle of Suleiman').
card_type('bottle of suleiman', 'Artifact').
card_types('bottle of suleiman', ['Artifact']).
card_subtypes('bottle of suleiman', []).
card_colors('bottle of suleiman', []).
card_text('bottle of suleiman', '{1}, Sacrifice Bottle of Suleiman: Flip a coin. If you win the flip, put a 5/5 colorless Djinn artifact creature token with flying onto the battlefield. If you lose the flip, Bottle of Suleiman deals 5 damage to you.').
card_mana_cost('bottle of suleiman', ['4']).
card_cmc('bottle of suleiman', 4).
card_layout('bottle of suleiman', 'normal').

% Found in: RAV
card_name('bottled cloister', 'Bottled Cloister').
card_type('bottled cloister', 'Artifact').
card_types('bottled cloister', ['Artifact']).
card_subtypes('bottled cloister', []).
card_colors('bottled cloister', []).
card_text('bottled cloister', 'At the beginning of each opponent\'s upkeep, exile all cards from your hand face down.\nAt the beginning of your upkeep, return all cards you own exiled with Bottled Cloister to your hand, then draw a card.').
card_mana_cost('bottled cloister', ['4']).
card_cmc('bottled cloister', 4).
card_layout('bottled cloister', 'normal').

% Found in: STH
card_name('bottomless pit', 'Bottomless Pit').
card_type('bottomless pit', 'Enchantment').
card_types('bottomless pit', ['Enchantment']).
card_subtypes('bottomless pit', []).
card_colors('bottomless pit', ['B']).
card_text('bottomless pit', 'At the beginning of each player\'s upkeep, that player discards a card at random.').
card_mana_cost('bottomless pit', ['1', 'B', 'B']).
card_cmc('bottomless pit', 3).
card_layout('bottomless pit', 'normal').

% Found in: 5ED, FEM
card_name('bottomless vault', 'Bottomless Vault').
card_type('bottomless vault', 'Land').
card_types('bottomless vault', ['Land']).
card_subtypes('bottomless vault', []).
card_colors('bottomless vault', []).
card_text('bottomless vault', 'Bottomless Vault enters the battlefield tapped.\nYou may choose not to untap Bottomless Vault during your untap step.\nAt the beginning of your upkeep, if Bottomless Vault is tapped, put a storage counter on it.\n{T}, Remove any number of storage counters from Bottomless Vault: Add {B} to your mana pool for each storage counter removed this way.').
card_layout('bottomless vault', 'normal').

% Found in: THS
card_name('boulderfall', 'Boulderfall').
card_type('boulderfall', 'Instant').
card_types('boulderfall', ['Instant']).
card_subtypes('boulderfall', []).
card_colors('boulderfall', ['R']).
card_text('boulderfall', 'Boulderfall deals 5 damage divided as you choose among any number of target creatures and/or players.').
card_mana_cost('boulderfall', ['6', 'R', 'R']).
card_cmc('boulderfall', 8).
card_layout('boulderfall', 'normal').

% Found in: ULG
card_name('bouncing beebles', 'Bouncing Beebles').
card_type('bouncing beebles', 'Creature — Beeble').
card_types('bouncing beebles', ['Creature']).
card_subtypes('bouncing beebles', ['Beeble']).
card_colors('bouncing beebles', ['U']).
card_text('bouncing beebles', 'Bouncing Beebles can\'t be blocked as long as defending player controls an artifact.').
card_mana_cost('bouncing beebles', ['2', 'U']).
card_cmc('bouncing beebles', 3).
card_layout('bouncing beebles', 'normal').
card_power('bouncing beebles', 2).
card_toughness('bouncing beebles', 2).

% Found in: DIS
card_name('bound', 'Bound').
card_type('bound', 'Instant').
card_types('bound', ['Instant']).
card_subtypes('bound', []).
card_colors('bound', ['B', 'G']).
card_text('bound', 'Sacrifice a creature. Return up to X cards from your graveyard to your hand, where X is the number of colors that creature was. Exile this card.').
card_mana_cost('bound', ['3', 'B', 'G']).
card_cmc('bound', 5).
card_layout('bound', 'split').
card_sides('bound', 'determined').

% Found in: FUT, MMA
card_name('bound in silence', 'Bound in Silence').
card_type('bound in silence', 'Tribal Enchantment — Rebel Aura').
card_types('bound in silence', ['Tribal', 'Enchantment']).
card_subtypes('bound in silence', ['Rebel', 'Aura']).
card_colors('bound in silence', ['W']).
card_text('bound in silence', 'Enchant creature\nEnchanted creature can\'t attack or block.').
card_mana_cost('bound in silence', ['2', 'W']).
card_cmc('bound in silence', 3).
card_layout('bound in silence', 'normal').

% Found in: ORI
card_name('bounding krasis', 'Bounding Krasis').
card_type('bounding krasis', 'Creature — Fish Lizard').
card_types('bounding krasis', ['Creature']).
card_subtypes('bounding krasis', ['Fish', 'Lizard']).
card_colors('bounding krasis', ['U', 'G']).
card_text('bounding krasis', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Bounding Krasis enters the battlefield, you may tap or untap target creature.').
card_mana_cost('bounding krasis', ['1', 'G', 'U']).
card_cmc('bounding krasis', 3).
card_layout('bounding krasis', 'normal').
card_power('bounding krasis', 3).
card_toughness('bounding krasis', 3).

% Found in: M13
card_name('boundless realms', 'Boundless Realms').
card_type('boundless realms', 'Sorcery').
card_types('boundless realms', ['Sorcery']).
card_subtypes('boundless realms', []).
card_colors('boundless realms', ['G']).
card_text('boundless realms', 'Search your library for up to X basic land cards, where X is the number of lands you control, and put them onto the battlefield tapped. Then shuffle your library.').
card_mana_cost('boundless realms', ['6', 'G']).
card_cmc('boundless realms', 7).
card_layout('boundless realms', 'normal').

% Found in: SOK
card_name('bounteous kirin', 'Bounteous Kirin').
card_type('bounteous kirin', 'Legendary Creature — Kirin Spirit').
card_types('bounteous kirin', ['Creature']).
card_subtypes('bounteous kirin', ['Kirin', 'Spirit']).
card_supertypes('bounteous kirin', ['Legendary']).
card_colors('bounteous kirin', ['G']).
card_text('bounteous kirin', 'Flying\nWhenever you cast a Spirit or Arcane spell, you may gain life equal to that spell\'s converted mana cost.').
card_mana_cost('bounteous kirin', ['5', 'G', 'G']).
card_cmc('bounteous kirin', 7).
card_layout('bounteous kirin', 'normal').
card_power('bounteous kirin', 4).
card_toughness('bounteous kirin', 4).

% Found in: M10, M12, M13
card_name('bountiful harvest', 'Bountiful Harvest').
card_type('bountiful harvest', 'Sorcery').
card_types('bountiful harvest', ['Sorcery']).
card_subtypes('bountiful harvest', []).
card_colors('bountiful harvest', ['G']).
card_text('bountiful harvest', 'You gain 1 life for each land you control.').
card_mana_cost('bountiful harvest', ['4', 'G']).
card_cmc('bountiful harvest', 5).
card_layout('bountiful harvest', 'normal').

% Found in: TMP
card_name('bounty hunter', 'Bounty Hunter').
card_type('bounty hunter', 'Creature — Human Archer Minion').
card_types('bounty hunter', ['Creature']).
card_subtypes('bounty hunter', ['Human', 'Archer', 'Minion']).
card_colors('bounty hunter', ['B']).
card_text('bounty hunter', '{T}: Put a bounty counter on target nonblack creature.\n{T}: Destroy target creature with a bounty counter on it.').
card_mana_cost('bounty hunter', ['2', 'B', 'B']).
card_cmc('bounty hunter', 4).
card_layout('bounty hunter', 'normal').
card_power('bounty hunter', 2).
card_toughness('bounty hunter', 2).

% Found in: ALL, CST, DKM, ME2
card_name('bounty of the hunt', 'Bounty of the Hunt').
card_type('bounty of the hunt', 'Instant').
card_types('bounty of the hunt', ['Instant']).
card_subtypes('bounty of the hunt', []).
card_colors('bounty of the hunt', ['G']).
card_text('bounty of the hunt', 'You may exile a green card from your hand rather than pay Bounty of the Hunt\'s mana cost.\nDistribute three +1/+1 counters among one, two, or three target creatures. For each +1/+1 counter you put on a creature this way, remove a +1/+1 counter from that creature at the beginning of the next cleanup step.').
card_mana_cost('bounty of the hunt', ['3', 'G', 'G']).
card_cmc('bounty of the hunt', 5).
card_layout('bounty of the hunt', 'normal').

% Found in: THS
card_name('bow of nylea', 'Bow of Nylea').
card_type('bow of nylea', 'Legendary Enchantment Artifact').
card_types('bow of nylea', ['Enchantment', 'Artifact']).
card_subtypes('bow of nylea', []).
card_supertypes('bow of nylea', ['Legendary']).
card_colors('bow of nylea', ['G']).
card_text('bow of nylea', 'Attacking creatures you control have deathtouch.\n{1}{G}, {T}: Choose one —\n• Put a +1/+1 counter on target creature.\n• Bow of Nylea deals 2 damage to target creature with flying.\n• You gain 3 life.\n• Put up to four target cards from your graveyard on the bottom of your library in any order.').
card_mana_cost('bow of nylea', ['1', 'G', 'G']).
card_cmc('bow of nylea', 3).
card_layout('bow of nylea', 'normal').

% Found in: AVR
card_name('bower passage', 'Bower Passage').
card_type('bower passage', 'Enchantment').
card_types('bower passage', ['Enchantment']).
card_subtypes('bower passage', []).
card_colors('bower passage', ['G']).
card_text('bower passage', 'Creatures with flying can\'t block creatures you control.').
card_mana_cost('bower passage', ['1', 'G']).
card_cmc('bower passage', 2).
card_layout('bower passage', 'normal').

% Found in: DIS
card_name('brace for impact', 'Brace for Impact').
card_type('brace for impact', 'Instant').
card_types('brace for impact', ['Instant']).
card_subtypes('brace for impact', []).
card_colors('brace for impact', ['W']).
card_text('brace for impact', 'Prevent all damage that would be dealt to target multicolored creature this turn. For each 1 damage prevented this way, put a +1/+1 counter on that creature.').
card_mana_cost('brace for impact', ['4', 'W']).
card_cmc('brace for impact', 5).
card_layout('brace for impact', 'normal').

% Found in: CON, DDH
card_name('brackwater elemental', 'Brackwater Elemental').
card_type('brackwater elemental', 'Creature — Elemental').
card_types('brackwater elemental', ['Creature']).
card_subtypes('brackwater elemental', ['Elemental']).
card_colors('brackwater elemental', ['U']).
card_text('brackwater elemental', 'When Brackwater Elemental attacks or blocks, sacrifice it at the beginning of the next end step.\nUnearth {2}{U} ({2}{U}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('brackwater elemental', ['2', 'U']).
card_cmc('brackwater elemental', 3).
card_layout('brackwater elemental', 'normal').
card_power('brackwater elemental', 4).
card_toughness('brackwater elemental', 4).

% Found in: CNS
card_name('brago\'s favor', 'Brago\'s Favor').
card_type('brago\'s favor', 'Conspiracy').
card_types('brago\'s favor', ['Conspiracy']).
card_subtypes('brago\'s favor', []).
card_colors('brago\'s favor', []).
card_text('brago\'s favor', 'Hidden agenda (Start the game with this conspiracy face down in the command zone and secretly name a card. You may turn this conspiracy face up any time and reveal the chosen name.)\nSpells with the chosen name you cast cost {1} less to cast.').
card_layout('brago\'s favor', 'normal').

% Found in: CNS
card_name('brago\'s representative', 'Brago\'s Representative').
card_type('brago\'s representative', 'Creature — Human Advisor').
card_types('brago\'s representative', ['Creature']).
card_subtypes('brago\'s representative', ['Human', 'Advisor']).
card_colors('brago\'s representative', ['W']).
card_text('brago\'s representative', 'While voting, you get an additional vote. (The votes can be for different choices or for the same choice.)').
card_mana_cost('brago\'s representative', ['2', 'W']).
card_cmc('brago\'s representative', 3).
card_layout('brago\'s representative', 'normal').
card_power('brago\'s representative', 1).
card_toughness('brago\'s representative', 4).

% Found in: CNS, VMA
card_name('brago, king eternal', 'Brago, King Eternal').
card_type('brago, king eternal', 'Legendary Creature — Spirit').
card_types('brago, king eternal', ['Creature']).
card_subtypes('brago, king eternal', ['Spirit']).
card_supertypes('brago, king eternal', ['Legendary']).
card_colors('brago, king eternal', ['W', 'U']).
card_text('brago, king eternal', 'Flying\nWhenever Brago, King Eternal deals combat damage to a player, exile any number of target nonland permanents you control, then return those cards to the battlefield under their owner\'s control.').
card_mana_cost('brago, king eternal', ['2', 'W', 'U']).
card_cmc('brago, king eternal', 4).
card_layout('brago, king eternal', 'normal').
card_power('brago, king eternal', 2).
card_toughness('brago, king eternal', 4).

% Found in: CSP
card_name('braid of fire', 'Braid of Fire').
card_type('braid of fire', 'Enchantment').
card_types('braid of fire', ['Enchantment']).
card_subtypes('braid of fire', []).
card_colors('braid of fire', ['R']).
card_text('braid of fire', 'Cumulative upkeep—Add {R} to your mana pool. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('braid of fire', ['1', 'R']).
card_cmc('braid of fire', 2).
card_layout('braid of fire', 'normal').

% Found in: ODY
card_name('braids, cabal minion', 'Braids, Cabal Minion').
card_type('braids, cabal minion', 'Legendary Creature — Human Minion').
card_types('braids, cabal minion', ['Creature']).
card_subtypes('braids, cabal minion', ['Human', 'Minion']).
card_supertypes('braids, cabal minion', ['Legendary']).
card_colors('braids, cabal minion', ['B']).
card_text('braids, cabal minion', 'At the beginning of each player\'s upkeep, that player sacrifices an artifact, creature, or land.').
card_mana_cost('braids, cabal minion', ['2', 'B', 'B']).
card_cmc('braids, cabal minion', 4).
card_layout('braids, cabal minion', 'normal').
card_power('braids, cabal minion', 2).
card_toughness('braids, cabal minion', 2).

% Found in: PLC
card_name('braids, conjurer adept', 'Braids, Conjurer Adept').
card_type('braids, conjurer adept', 'Legendary Creature — Human Wizard').
card_types('braids, conjurer adept', ['Creature']).
card_subtypes('braids, conjurer adept', ['Human', 'Wizard']).
card_supertypes('braids, conjurer adept', ['Legendary']).
card_colors('braids, conjurer adept', ['U']).
card_text('braids, conjurer adept', 'At the beginning of each player\'s upkeep, that player may put an artifact, creature, or land card from his or her hand onto the battlefield.').
card_mana_cost('braids, conjurer adept', ['2', 'U', 'U']).
card_cmc('braids, conjurer adept', 4).
card_layout('braids, conjurer adept', 'normal').
card_power('braids, conjurer adept', 2).
card_toughness('braids, conjurer adept', 2).

% Found in: VAN
card_name('braids, conjurer adept avatar', 'Braids, Conjurer Adept Avatar').
card_type('braids, conjurer adept avatar', 'Vanguard').
card_types('braids, conjurer adept avatar', ['Vanguard']).
card_subtypes('braids, conjurer adept avatar', []).
card_colors('braids, conjurer adept avatar', []).
card_text('braids, conjurer adept avatar', '{2}: Each player may put a land card from his or her hand onto the battlefield tapped.\n{3}: Each player may put a noncreature artifact card from his or her hand onto the battlefield.\n{4}: Each player may put a creature card from his or her hand onto the battlefield. Activate this ability only any time you could cast a sorcery.').
card_layout('braids, conjurer adept avatar', 'vanguard').

% Found in: UDS
card_name('braidwood cup', 'Braidwood Cup').
card_type('braidwood cup', 'Artifact').
card_types('braidwood cup', ['Artifact']).
card_subtypes('braidwood cup', []).
card_colors('braidwood cup', []).
card_text('braidwood cup', '{T}: You gain 1 life.').
card_mana_cost('braidwood cup', ['3']).
card_cmc('braidwood cup', 3).
card_layout('braidwood cup', 'normal').

% Found in: UDS
card_name('braidwood sextant', 'Braidwood Sextant').
card_type('braidwood sextant', 'Artifact').
card_types('braidwood sextant', ['Artifact']).
card_subtypes('braidwood sextant', []).
card_colors('braidwood sextant', []).
card_text('braidwood sextant', '{2}, {T}, Sacrifice Braidwood Sextant: Search your library for a basic land card, reveal that card, and put it into your hand. Then shuffle your library.').
card_mana_cost('braidwood sextant', ['1']).
card_cmc('braidwood sextant', 1).
card_layout('braidwood sextant', 'normal').

% Found in: SCG, VMA
card_name('brain freeze', 'Brain Freeze').
card_type('brain freeze', 'Instant').
card_types('brain freeze', ['Instant']).
card_subtypes('brain freeze', []).
card_colors('brain freeze', ['U']).
card_text('brain freeze', 'Target player puts the top three cards of his or her library into his or her graveyard.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_mana_cost('brain freeze', ['1', 'U']).
card_cmc('brain freeze', 2).
card_layout('brain freeze', 'normal').

% Found in: PLC
card_name('brain gorgers', 'Brain Gorgers').
card_type('brain gorgers', 'Creature — Zombie').
card_types('brain gorgers', ['Creature']).
card_subtypes('brain gorgers', ['Zombie']).
card_colors('brain gorgers', ['B']).
card_text('brain gorgers', 'When you cast Brain Gorgers, any player may sacrifice a creature. If a player does, counter Brain Gorgers.\nMadness {1}{B} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('brain gorgers', ['3', 'B']).
card_cmc('brain gorgers', 4).
card_layout('brain gorgers', 'normal').
card_power('brain gorgers', 4).
card_toughness('brain gorgers', 2).

% Found in: JOU, pFNM
card_name('brain maggot', 'Brain Maggot').
card_type('brain maggot', 'Enchantment Creature — Insect').
card_types('brain maggot', ['Enchantment', 'Creature']).
card_subtypes('brain maggot', ['Insect']).
card_colors('brain maggot', ['B']).
card_text('brain maggot', 'When Brain Maggot enters the battlefield, target opponent reveals his or her hand and you choose a nonland card from it. Exile that card until Brain Maggot leaves the battlefield.').
card_mana_cost('brain maggot', ['1', 'B']).
card_cmc('brain maggot', 2).
card_layout('brain maggot', 'normal').
card_power('brain maggot', 1).
card_toughness('brain maggot', 1).

% Found in: DIS
card_name('brain pry', 'Brain Pry').
card_type('brain pry', 'Sorcery').
card_types('brain pry', ['Sorcery']).
card_subtypes('brain pry', []).
card_colors('brain pry', ['B']).
card_text('brain pry', 'Name a nonland card. Target player reveals his or her hand. That player discards a card with that name. If he or she can\'t, you draw a card.').
card_mana_cost('brain pry', ['1', 'B']).
card_cmc('brain pry', 2).
card_layout('brain pry', 'normal').

% Found in: DDJ, ISD
card_name('brain weevil', 'Brain Weevil').
card_type('brain weevil', 'Creature — Insect').
card_types('brain weevil', ['Creature']).
card_subtypes('brain weevil', ['Insect']).
card_colors('brain weevil', ['B']).
card_text('brain weevil', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nSacrifice Brain Weevil: Target player discards two cards. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('brain weevil', ['3', 'B']).
card_cmc('brain weevil', 4).
card_layout('brain weevil', 'normal').
card_power('brain weevil', 1).
card_toughness('brain weevil', 1).

% Found in: ARB
card_name('brainbite', 'Brainbite').
card_type('brainbite', 'Sorcery').
card_types('brainbite', ['Sorcery']).
card_subtypes('brainbite', []).
card_colors('brainbite', ['U', 'B']).
card_text('brainbite', 'Target opponent reveals his or her hand. You choose a card from it. That player discards that card.\nDraw a card.').
card_mana_cost('brainbite', ['2', 'U', 'B']).
card_cmc('brainbite', 4).
card_layout('brainbite', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME4
card_name('braingeyser', 'Braingeyser').
card_type('braingeyser', 'Sorcery').
card_types('braingeyser', ['Sorcery']).
card_subtypes('braingeyser', []).
card_colors('braingeyser', ['U']).
card_text('braingeyser', 'Target player draws X cards.').
card_mana_cost('braingeyser', ['X', 'U', 'U']).
card_cmc('braingeyser', 2).
card_layout('braingeyser', 'normal').
card_reserved('braingeyser').

% Found in: RAV
card_name('brainspoil', 'Brainspoil').
card_type('brainspoil', 'Sorcery').
card_types('brainspoil', ['Sorcery']).
card_subtypes('brainspoil', []).
card_colors('brainspoil', ['B']).
card_text('brainspoil', 'Destroy target creature that isn\'t enchanted. It can\'t be regenerated.\nTransmute {1}{B}{B} ({1}{B}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Transmute only as a sorcery.)').
card_mana_cost('brainspoil', ['3', 'B', 'B']).
card_cmc('brainspoil', 5).
card_layout('brainspoil', 'normal').

% Found in: 5ED, BTD, CMD, CNS, CST, DDJ, ICE, ME2, MMQ, VMA, pFNM
card_name('brainstorm', 'Brainstorm').
card_type('brainstorm', 'Instant').
card_types('brainstorm', ['Instant']).
card_subtypes('brainstorm', []).
card_colors('brainstorm', ['U']).
card_text('brainstorm', 'Draw three cards, then put two cards from your hand on top of your library in any order.').
card_mana_cost('brainstorm', ['U']).
card_cmc('brainstorm', 1).
card_layout('brainstorm', 'normal').

% Found in: 4ED, 5ED, DRK
card_name('brainwash', 'Brainwash').
card_type('brainwash', 'Enchantment — Aura').
card_types('brainwash', ['Enchantment']).
card_subtypes('brainwash', ['Aura']).
card_colors('brainwash', ['W']).
card_text('brainwash', 'Enchant creature\nEnchanted creature can\'t attack unless its controller pays {3}.').
card_mana_cost('brainwash', ['W']).
card_cmc('brainwash', 1).
card_layout('brainwash', 'normal').

% Found in: M10
card_name('bramble creeper', 'Bramble Creeper').
card_type('bramble creeper', 'Creature — Elemental').
card_types('bramble creeper', ['Creature']).
card_subtypes('bramble creeper', ['Elemental']).
card_colors('bramble creeper', ['G']).
card_text('bramble creeper', 'Whenever Bramble Creeper attacks, it gets +5/+0 until end of turn.').
card_mana_cost('bramble creeper', ['4', 'G']).
card_cmc('bramble creeper', 5).
card_layout('bramble creeper', 'normal').
card_power('bramble creeper', 0).
card_toughness('bramble creeper', 3).

% Found in: PC2, RAV
card_name('bramble elemental', 'Bramble Elemental').
card_type('bramble elemental', 'Creature — Elemental').
card_types('bramble elemental', ['Creature']).
card_subtypes('bramble elemental', ['Elemental']).
card_colors('bramble elemental', ['G']).
card_text('bramble elemental', 'Whenever an Aura becomes attached to Bramble Elemental, put two 1/1 green Saproling creature tokens onto the battlefield.').
card_mana_cost('bramble elemental', ['3', 'G', 'G']).
card_cmc('bramble elemental', 5).
card_layout('bramble elemental', 'normal').
card_power('bramble elemental', 4).
card_toughness('bramble elemental', 4).

% Found in: ISD, M14
card_name('bramblecrush', 'Bramblecrush').
card_type('bramblecrush', 'Sorcery').
card_types('bramblecrush', ['Sorcery']).
card_subtypes('bramblecrush', []).
card_colors('bramblecrush', ['G']).
card_text('bramblecrush', 'Destroy target noncreature permanent.').
card_mana_cost('bramblecrush', ['2', 'G', 'G']).
card_cmc('bramblecrush', 4).
card_layout('bramblecrush', 'normal').

% Found in: ROE
card_name('bramblesnap', 'Bramblesnap').
card_type('bramblesnap', 'Creature — Elemental').
card_types('bramblesnap', ['Creature']).
card_subtypes('bramblesnap', ['Elemental']).
card_colors('bramblesnap', ['G']).
card_text('bramblesnap', 'Trample\nTap an untapped creature you control: Bramblesnap gets +1/+1 until end of turn.').
card_mana_cost('bramblesnap', ['1', 'G']).
card_cmc('bramblesnap', 2).
card_layout('bramblesnap', 'normal').
card_power('bramblesnap', 1).
card_toughness('bramblesnap', 1).

% Found in: MOR, pCMP
card_name('bramblewood paragon', 'Bramblewood Paragon').
card_type('bramblewood paragon', 'Creature — Elf Warrior').
card_types('bramblewood paragon', ['Creature']).
card_subtypes('bramblewood paragon', ['Elf', 'Warrior']).
card_colors('bramblewood paragon', ['G']).
card_text('bramblewood paragon', 'Each other Warrior creature you control enters the battlefield with an additional +1/+1 counter on it.\nEach creature you control with a +1/+1 counter on it has trample.').
card_mana_cost('bramblewood paragon', ['1', 'G']).
card_cmc('bramblewood paragon', 2).
card_layout('bramblewood paragon', 'normal').
card_power('bramblewood paragon', 2).
card_toughness('bramblewood paragon', 2).

% Found in: ALA, ARC, HOP
card_name('branching bolt', 'Branching Bolt').
card_type('branching bolt', 'Instant').
card_types('branching bolt', ['Instant']).
card_subtypes('branching bolt', []).
card_colors('branching bolt', ['R', 'G']).
card_text('branching bolt', 'Choose one or both —\n• Branching Bolt deals 3 damage to target creature with flying.\n• Branching Bolt deals 3 damage to target creature without flying.').
card_mana_cost('branching bolt', ['1', 'R', 'G']).
card_cmc('branching bolt', 3).
card_layout('branching bolt', 'normal').

% Found in: LGN
card_name('branchsnap lorian', 'Branchsnap Lorian').
card_type('branchsnap lorian', 'Creature — Beast').
card_types('branchsnap lorian', ['Creature']).
card_subtypes('branchsnap lorian', ['Beast']).
card_colors('branchsnap lorian', ['G']).
card_text('branchsnap lorian', 'Trample\nMorph {G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('branchsnap lorian', ['1', 'G', 'G']).
card_cmc('branchsnap lorian', 3).
card_layout('branchsnap lorian', 'normal').
card_power('branchsnap lorian', 4).
card_toughness('branchsnap lorian', 1).

% Found in: USG
card_name('brand', 'Brand').
card_type('brand', 'Instant').
card_types('brand', ['Instant']).
card_subtypes('brand', []).
card_colors('brand', ['R']).
card_text('brand', 'Gain control of all permanents you own. (This effect lasts indefinitely.)\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('brand', ['R']).
card_cmc('brand', 1).
card_layout('brand', 'normal').

% Found in: ICE
card_name('brand of ill omen', 'Brand of Ill Omen').
card_type('brand of ill omen', 'Enchantment — Aura').
card_types('brand of ill omen', ['Enchantment']).
card_subtypes('brand of ill omen', ['Aura']).
card_colors('brand of ill omen', ['R']).
card_text('brand of ill omen', 'Enchant creature\nCumulative upkeep {R} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nEnchanted creature\'s controller can\'t cast creature spells.').
card_mana_cost('brand of ill omen', ['3', 'R']).
card_cmc('brand of ill omen', 4).
card_layout('brand of ill omen', 'normal').
card_reserved('brand of ill omen').

% Found in: PCY
card_name('branded brawlers', 'Branded Brawlers').
card_type('branded brawlers', 'Creature — Human Soldier').
card_types('branded brawlers', ['Creature']).
card_subtypes('branded brawlers', ['Human', 'Soldier']).
card_colors('branded brawlers', ['R']).
card_text('branded brawlers', 'Branded Brawlers can\'t attack if defending player controls an untapped land.\nBranded Brawlers can\'t block if you control an untapped land.').
card_mana_cost('branded brawlers', ['R']).
card_cmc('branded brawlers', 1).
card_layout('branded brawlers', 'normal').
card_power('branded brawlers', 2).
card_toughness('branded brawlers', 2).

% Found in: TSP
card_name('brass gnat', 'Brass Gnat').
card_type('brass gnat', 'Artifact Creature — Insect').
card_types('brass gnat', ['Artifact', 'Creature']).
card_subtypes('brass gnat', ['Insect']).
card_colors('brass gnat', []).
card_text('brass gnat', 'Flying\nBrass Gnat doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may pay {1}. If you do, untap Brass Gnat.').
card_mana_cost('brass gnat', ['1']).
card_cmc('brass gnat', 1).
card_layout('brass gnat', 'normal').
card_power('brass gnat', 1).
card_toughness('brass gnat', 1).

% Found in: 8ED, APC
card_name('brass herald', 'Brass Herald').
card_type('brass herald', 'Artifact Creature — Golem').
card_types('brass herald', ['Artifact', 'Creature']).
card_subtypes('brass herald', ['Golem']).
card_colors('brass herald', []).
card_text('brass herald', 'As Brass Herald enters the battlefield, choose a creature type.\nWhen Brass Herald enters the battlefield, reveal the top four cards of your library. Put all creature cards of the chosen type revealed this way into your hand and the rest on the bottom of your library in any order.\nCreatures of the chosen type get +1/+1.').
card_mana_cost('brass herald', ['6']).
card_cmc('brass herald', 6).
card_layout('brass herald', 'normal').
card_power('brass herald', 2).
card_toughness('brass herald', 2).

% Found in: 3ED, 4ED, ARN, ME4
card_name('brass man', 'Brass Man').
card_type('brass man', 'Artifact Creature — Construct').
card_types('brass man', ['Artifact', 'Creature']).
card_subtypes('brass man', ['Construct']).
card_colors('brass man', []).
card_text('brass man', 'Brass Man doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may pay {1}. If you do, untap Brass Man.').
card_mana_cost('brass man', ['1']).
card_cmc('brass man', 1).
card_layout('brass man', 'normal').
card_power('brass man', 1).
card_toughness('brass man', 3).

% Found in: UDS
card_name('brass secretary', 'Brass Secretary').
card_type('brass secretary', 'Artifact Creature — Construct').
card_types('brass secretary', ['Artifact', 'Creature']).
card_subtypes('brass secretary', ['Construct']).
card_colors('brass secretary', []).
card_text('brass secretary', '{2}, Sacrifice Brass Secretary: Draw a card.').
card_mana_cost('brass secretary', ['3']).
card_cmc('brass secretary', 3).
card_layout('brass secretary', 'normal').
card_power('brass secretary', 2).
card_toughness('brass secretary', 1).

% Found in: MBS
card_name('brass squire', 'Brass Squire').
card_type('brass squire', 'Artifact Creature — Myr').
card_types('brass squire', ['Artifact', 'Creature']).
card_subtypes('brass squire', ['Myr']).
card_colors('brass squire', []).
card_text('brass squire', '{T}: Attach target Equipment you control to target creature you control.').
card_mana_cost('brass squire', ['3']).
card_cmc('brass squire', 3).
card_layout('brass squire', 'normal').
card_power('brass squire', 1).
card_toughness('brass squire', 3).

% Found in: VIS
card_name('brass-talon chimera', 'Brass-Talon Chimera').
card_type('brass-talon chimera', 'Artifact Creature — Chimera').
card_types('brass-talon chimera', ['Artifact', 'Creature']).
card_subtypes('brass-talon chimera', ['Chimera']).
card_colors('brass-talon chimera', []).
card_text('brass-talon chimera', 'First strike\nSacrifice Brass-Talon Chimera: Put a +2/+2 counter on target Chimera creature. It gains first strike. (This effect lasts indefinitely.)').
card_mana_cost('brass-talon chimera', ['4']).
card_cmc('brass-talon chimera', 4).
card_layout('brass-talon chimera', 'normal').
card_power('brass-talon chimera', 2).
card_toughness('brass-talon chimera', 2).

% Found in: 5ED, FEM, ME2
card_name('brassclaw orcs', 'Brassclaw Orcs').
card_type('brassclaw orcs', 'Creature — Orc').
card_types('brassclaw orcs', ['Creature']).
card_subtypes('brassclaw orcs', ['Orc']).
card_colors('brassclaw orcs', ['R']).
card_text('brassclaw orcs', 'Brassclaw Orcs can\'t block creatures with power 2 or greater.').
card_mana_cost('brassclaw orcs', ['2', 'R']).
card_cmc('brassclaw orcs', 3).
card_layout('brassclaw orcs', 'normal').
card_power('brassclaw orcs', 3).
card_toughness('brassclaw orcs', 2).

% Found in: USG
card_name('bravado', 'Bravado').
card_type('bravado', 'Enchantment — Aura').
card_types('bravado', ['Enchantment']).
card_subtypes('bravado', ['Aura']).
card_colors('bravado', ['R']).
card_text('bravado', 'Enchant creature\nEnchanted creature gets +1/+1 for each other creature you control.').
card_mana_cost('bravado', ['1', 'R']).
card_cmc('bravado', 2).
card_layout('bravado', 'normal').

% Found in: C14, M14, ZEN, pMPR
card_name('brave the elements', 'Brave the Elements').
card_type('brave the elements', 'Instant').
card_types('brave the elements', ['Instant']).
card_subtypes('brave the elements', []).
card_colors('brave the elements', ['W']).
card_text('brave the elements', 'Choose a color. White creatures you control gain protection from the chosen color until end of turn.').
card_mana_cost('brave the elements', ['W']).
card_cmc('brave the elements', 1).
card_layout('brave the elements', 'normal').

% Found in: KTK
card_name('brave the sands', 'Brave the Sands').
card_type('brave the sands', 'Enchantment').
card_types('brave the sands', ['Enchantment']).
card_subtypes('brave the sands', []).
card_colors('brave the sands', ['W']).
card_text('brave the sands', 'Creatures you control have vigilance.\nEach creature you control can block an additional creature.').
card_mana_cost('brave the sands', ['1', 'W']).
card_cmc('brave the sands', 2).
card_layout('brave the sands', 'normal').

% Found in: MMQ
card_name('brawl', 'Brawl').
card_type('brawl', 'Instant').
card_types('brawl', ['Instant']).
card_subtypes('brawl', []).
card_colors('brawl', ['R']).
card_text('brawl', 'Until end of turn, all creatures gain \"{T}: This creature deals damage equal to its power to target creature.\"').
card_mana_cost('brawl', ['3', 'R', 'R']).
card_cmc('brawl', 5).
card_layout('brawl', 'normal').

% Found in: M15, ORI
card_name('brawler\'s plate', 'Brawler\'s Plate').
card_type('brawler\'s plate', 'Artifact — Equipment').
card_types('brawler\'s plate', ['Artifact']).
card_subtypes('brawler\'s plate', ['Equipment']).
card_colors('brawler\'s plate', []).
card_text('brawler\'s plate', 'Equipped creature gets +2/+2 and has trample. (It can deal excess combat damage to defending player or planeswalker while attacking.)\nEquip {4} ({4}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('brawler\'s plate', ['3']).
card_cmc('brawler\'s plate', 3).
card_layout('brawler\'s plate', 'normal').

% Found in: CMD, JUD
card_name('brawn', 'Brawn').
card_type('brawn', 'Creature — Incarnation').
card_types('brawn', ['Creature']).
card_subtypes('brawn', ['Incarnation']).
card_colors('brawn', ['G']).
card_text('brawn', 'Trample\nAs long as Brawn is in your graveyard and you control a Forest, creatures you control have trample.').
card_mana_cost('brawn', ['3', 'G']).
card_cmc('brawn', 4).
card_layout('brawn', 'normal').
card_power('brawn', 3).
card_toughness('brawn', 3).

% Found in: USG
card_name('breach', 'Breach').
card_type('breach', 'Instant').
card_types('breach', ['Instant']).
card_subtypes('breach', []).
card_colors('breach', ['B']).
card_text('breach', 'Target creature gets +2/+0 and gains fear until end of turn. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('breach', ['2', 'B']).
card_cmc('breach', 3).
card_layout('breach', 'normal').

% Found in: THS
card_name('breaching hippocamp', 'Breaching Hippocamp').
card_type('breaching hippocamp', 'Creature — Horse Fish').
card_types('breaching hippocamp', ['Creature']).
card_subtypes('breaching hippocamp', ['Horse', 'Fish']).
card_colors('breaching hippocamp', ['U']).
card_text('breaching hippocamp', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Breaching Hippocamp enters the battlefield, untap another target creature you control.').
card_mana_cost('breaching hippocamp', ['3', 'U']).
card_cmc('breaching hippocamp', 4).
card_layout('breaching hippocamp', 'normal').
card_power('breaching hippocamp', 3).
card_toughness('breaching hippocamp', 2).

% Found in: C14
card_name('breaching leviathan', 'Breaching Leviathan').
card_type('breaching leviathan', 'Creature — Leviathan').
card_types('breaching leviathan', ['Creature']).
card_subtypes('breaching leviathan', ['Leviathan']).
card_colors('breaching leviathan', ['U']).
card_text('breaching leviathan', 'When Breaching Leviathan enters the battlefield, if you cast it from your hand, tap all nonblue creatures. Those creatures don\'t untap during their controllers\' next untap steps.').
card_mana_cost('breaching leviathan', ['7', 'U', 'U']).
card_cmc('breaching leviathan', 9).
card_layout('breaching leviathan', 'normal').
card_power('breaching leviathan', 9).
card_toughness('breaching leviathan', 9).

% Found in: SCG
card_name('break asunder', 'Break Asunder').
card_type('break asunder', 'Sorcery').
card_types('break asunder', ['Sorcery']).
card_subtypes('break asunder', []).
card_colors('break asunder', ['G']).
card_text('break asunder', 'Destroy target artifact or enchantment.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('break asunder', ['2', 'G', 'G']).
card_cmc('break asunder', 4).
card_layout('break asunder', 'normal').

% Found in: DKA
card_name('break of day', 'Break of Day').
card_type('break of day', 'Instant').
card_types('break of day', ['Instant']).
card_subtypes('break of day', []).
card_colors('break of day', ['W']).
card_text('break of day', 'Creatures you control get +1/+1 until end of turn.\nFateful hour — If you have 5 or less life, those creatures gain indestructible until end of turn. (Damage and effects that say \"destroy\" don\'t destroy them.)').
card_mana_cost('break of day', ['1', 'W']).
card_cmc('break of day', 2).
card_layout('break of day', 'normal').

% Found in: ONS
card_name('break open', 'Break Open').
card_type('break open', 'Instant').
card_types('break open', ['Instant']).
card_subtypes('break open', []).
card_colors('break open', ['R']).
card_text('break open', 'Turn target face-down creature an opponent controls face up.').
card_mana_cost('break open', ['1', 'R']).
card_cmc('break open', 2).
card_layout('break open', 'normal').

% Found in: FRF
card_name('break through the line', 'Break Through the Line').
card_type('break through the line', 'Enchantment').
card_types('break through the line', ['Enchantment']).
card_subtypes('break through the line', []).
card_colors('break through the line', ['R']).
card_text('break through the line', '{R}: Target creature with power 2 or less gains haste until end of turn and can\'t be blocked this turn.').
card_mana_cost('break through the line', ['1', 'R']).
card_cmc('break through the line', 2).
card_layout('break through the line', 'normal').

% Found in: BFZ
card_name('breaker of armies', 'Breaker of Armies').
card_type('breaker of armies', 'Creature — Eldrazi').
card_types('breaker of armies', ['Creature']).
card_subtypes('breaker of armies', ['Eldrazi']).
card_colors('breaker of armies', []).
card_text('breaker of armies', 'All creatures able to block Breaker of Armies do so.').
card_mana_cost('breaker of armies', ['8']).
card_cmc('breaker of armies', 8).
card_layout('breaker of armies', 'normal').
card_power('breaker of armies', 10).
card_toughness('breaker of armies', 8).

% Found in: DGM, pLPA
card_name('breaking', 'Breaking').
card_type('breaking', 'Sorcery').
card_types('breaking', ['Sorcery']).
card_subtypes('breaking', []).
card_colors('breaking', ['U', 'B']).
card_text('breaking', 'Target player puts the top eight cards of his or her library into his or her graveyard.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('breaking', ['U', 'B']).
card_cmc('breaking', 2).
card_layout('breaking', 'split').
card_sides('breaking', 'entering').

% Found in: DDK, JUD
card_name('breaking point', 'Breaking Point').
card_type('breaking point', 'Sorcery').
card_types('breaking point', ['Sorcery']).
card_subtypes('breaking point', []).
card_colors('breaking point', ['R']).
card_text('breaking point', 'Any player may have Breaking Point deal 6 damage to him or her. If no one does, destroy all creatures. Creatures destroyed this way can\'t be regenerated.').
card_mana_cost('breaking point', ['1', 'R', 'R']).
card_cmc('breaking point', 3).
card_layout('breaking point', 'normal').

% Found in: INV
card_name('breaking wave', 'Breaking Wave').
card_type('breaking wave', 'Sorcery').
card_types('breaking wave', ['Sorcery']).
card_subtypes('breaking wave', []).
card_colors('breaking wave', ['U']).
card_text('breaking wave', 'You may cast Breaking Wave as though it had flash if you pay {2} more to cast it. (You may cast it any time you could cast an instant.)\nSimultaneously untap all tapped creatures and tap all untapped creatures.').
card_mana_cost('breaking wave', ['2', 'U', 'U']).
card_cmc('breaking wave', 4).
card_layout('breaking wave', 'normal').

% Found in: CNS, TOR
card_name('breakthrough', 'Breakthrough').
card_type('breakthrough', 'Sorcery').
card_types('breakthrough', ['Sorcery']).
card_subtypes('breakthrough', []).
card_colors('breakthrough', ['U']).
card_text('breakthrough', 'Draw four cards, then choose X cards in your hand and discard the rest.').
card_mana_cost('breakthrough', ['X', 'U']).
card_cmc('breakthrough', 1).
card_layout('breakthrough', 'normal').

% Found in: ARC, CMD, DDG, INV
card_name('breath of darigaaz', 'Breath of Darigaaz').
card_type('breath of darigaaz', 'Sorcery').
card_types('breath of darigaaz', ['Sorcery']).
card_subtypes('breath of darigaaz', []).
card_colors('breath of darigaaz', ['R']).
card_text('breath of darigaaz', 'Kicker {2} (You may pay an additional {2} as you cast this spell.)\nBreath of Darigaaz deals 1 damage to each creature without flying and each player. If Breath of Darigaaz was kicked, it deals 4 damage to each creature without flying and each player instead.').
card_mana_cost('breath of darigaaz', ['1', 'R']).
card_cmc('breath of darigaaz', 2).
card_layout('breath of darigaaz', 'normal').

% Found in: ICE
card_name('breath of dreams', 'Breath of Dreams').
card_type('breath of dreams', 'Enchantment').
card_types('breath of dreams', ['Enchantment']).
card_subtypes('breath of dreams', []).
card_colors('breath of dreams', ['U']).
card_text('breath of dreams', 'Cumulative upkeep {U} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nGreen creatures have \"Cumulative upkeep {1}.\"').
card_mana_cost('breath of dreams', ['2', 'U', 'U']).
card_cmc('breath of dreams', 4).
card_layout('breath of dreams', 'normal').

% Found in: RAV
card_name('breath of fury', 'Breath of Fury').
card_type('breath of fury', 'Enchantment — Aura').
card_types('breath of fury', ['Enchantment']).
card_subtypes('breath of fury', ['Aura']).
card_colors('breath of fury', ['R']).
card_text('breath of fury', 'Enchant creature you control\nWhen enchanted creature deals combat damage to a player, sacrifice it and attach Breath of Fury to a creature you control. If you do, untap all creatures you control and after this phase, there is an additional combat phase.').
card_mana_cost('breath of fury', ['2', 'R', 'R']).
card_cmc('breath of fury', 4).
card_layout('breath of fury', 'normal').

% Found in: 7ED, PO2, POR, S00, S99, VMA
card_name('breath of life', 'Breath of Life').
card_type('breath of life', 'Sorcery').
card_types('breath of life', ['Sorcery']).
card_subtypes('breath of life', []).
card_colors('breath of life', ['W']).
card_text('breath of life', 'Return target creature card from your graveyard to the battlefield.').
card_mana_cost('breath of life', ['3', 'W']).
card_cmc('breath of life', 4).
card_layout('breath of life', 'normal').

% Found in: ARB, pMEI
card_name('breath of malfegor', 'Breath of Malfegor').
card_type('breath of malfegor', 'Instant').
card_types('breath of malfegor', ['Instant']).
card_subtypes('breath of malfegor', []).
card_colors('breath of malfegor', ['B', 'R']).
card_text('breath of malfegor', 'Breath of Malfegor deals 5 damage to each opponent.').
card_mana_cost('breath of malfegor', ['3', 'B', 'R']).
card_cmc('breath of malfegor', 5).
card_layout('breath of malfegor', 'normal').

% Found in: MIR
card_name('breathstealer', 'Breathstealer').
card_type('breathstealer', 'Creature — Nightstalker').
card_types('breathstealer', ['Creature']).
card_subtypes('breathstealer', ['Nightstalker']).
card_colors('breathstealer', ['B']).
card_text('breathstealer', '{B}: Breathstealer gets +1/-1 until end of turn.').
card_mana_cost('breathstealer', ['2', 'B']).
card_cmc('breathstealer', 3).
card_layout('breathstealer', 'normal').
card_power('breathstealer', 2).
card_toughness('breathstealer', 2).

% Found in: VIS
card_name('breathstealer\'s crypt', 'Breathstealer\'s Crypt').
card_type('breathstealer\'s crypt', 'Enchantment').
card_types('breathstealer\'s crypt', ['Enchantment']).
card_subtypes('breathstealer\'s crypt', []).
card_colors('breathstealer\'s crypt', ['U', 'B']).
card_text('breathstealer\'s crypt', 'If a player would draw a card, instead he or she draws a card and reveals it. If it\'s a creature card, that player discards it unless he or she pays 3 life.').
card_mana_cost('breathstealer\'s crypt', ['2', 'U', 'B']).
card_cmc('breathstealer\'s crypt', 4).
card_layout('breathstealer\'s crypt', 'normal').
card_reserved('breathstealer\'s crypt').

% Found in: DGM
card_name('bred for the hunt', 'Bred for the Hunt').
card_type('bred for the hunt', 'Enchantment').
card_types('bred for the hunt', ['Enchantment']).
card_subtypes('bred for the hunt', []).
card_colors('bred for the hunt', ['U', 'G']).
card_text('bred for the hunt', 'Whenever a creature you control with a +1/+1 counter on it deals combat damage to a player, you may draw a card.').
card_mana_cost('bred for the hunt', ['1', 'G', 'U']).
card_cmc('bred for the hunt', 3).
card_layout('bred for the hunt', 'normal').

% Found in: 5ED, DD3_DVD, DDC, FEM, MED
card_name('breeding pit', 'Breeding Pit').
card_type('breeding pit', 'Enchantment').
card_types('breeding pit', ['Enchantment']).
card_subtypes('breeding pit', []).
card_colors('breeding pit', ['B']).
card_text('breeding pit', 'At the beginning of your upkeep, sacrifice Breeding Pit unless you pay {B}{B}.\nAt the beginning of your end step, put a 0/1 black Thrull creature token onto the battlefield.').
card_mana_cost('breeding pit', ['3', 'B']).
card_cmc('breeding pit', 4).
card_layout('breeding pit', 'normal').

% Found in: DIS, EXP, GTC
card_name('breeding pool', 'Breeding Pool').
card_type('breeding pool', 'Land — Forest Island').
card_types('breeding pool', ['Land']).
card_subtypes('breeding pool', ['Forest', 'Island']).
card_colors('breeding pool', []).
card_text('breeding pool', '({T}: Add {G} or {U} to your mana pool.)\nAs Breeding Pool enters the battlefield, you may pay 2 life. If you don\'t, Breeding Pool enters the battlefield tapped.').
card_layout('breeding pool', 'normal').

% Found in: VIS
card_name('breezekeeper', 'Breezekeeper').
card_type('breezekeeper', 'Creature — Djinn').
card_types('breezekeeper', ['Creature']).
card_subtypes('breezekeeper', ['Djinn']).
card_colors('breezekeeper', ['U']).
card_text('breezekeeper', 'Flying\nPhasing (This phases in or out before you untap during each of your untap steps. While it\'s phased out, it\'s treated as though it doesn\'t exist.)').
card_mana_cost('breezekeeper', ['3', 'U']).
card_cmc('breezekeeper', 4).
card_layout('breezekeeper', 'normal').
card_power('breezekeeper', 4).
card_toughness('breezekeeper', 4).

% Found in: MMQ
card_name('briar patch', 'Briar Patch').
card_type('briar patch', 'Enchantment').
card_types('briar patch', ['Enchantment']).
card_subtypes('briar patch', []).
card_colors('briar patch', ['G']).
card_text('briar patch', 'Whenever a creature attacks you, it gets -1/-0 until end of turn.').
card_mana_cost('briar patch', ['1', 'G', 'G']).
card_cmc('briar patch', 3).
card_layout('briar patch', 'normal').

% Found in: WTH
card_name('briar shield', 'Briar Shield').
card_type('briar shield', 'Enchantment — Aura').
card_types('briar shield', ['Enchantment']).
card_subtypes('briar shield', ['Aura']).
card_colors('briar shield', ['G']).
card_text('briar shield', 'Enchant creature\nEnchanted creature gets +1/+1.\nSacrifice Briar Shield: Enchanted creature gets +3/+3 until end of turn.').
card_mana_cost('briar shield', ['G']).
card_cmc('briar shield', 1).
card_layout('briar shield', 'normal').

% Found in: SHM
card_name('briarberry cohort', 'Briarberry Cohort').
card_type('briarberry cohort', 'Creature — Faerie Soldier').
card_types('briarberry cohort', ['Creature']).
card_subtypes('briarberry cohort', ['Faerie', 'Soldier']).
card_colors('briarberry cohort', ['U']).
card_text('briarberry cohort', 'Flying\nBriarberry Cohort gets +1/+1 as long as you control another blue creature.').
card_mana_cost('briarberry cohort', ['1', 'U']).
card_cmc('briarberry cohort', 2).
card_layout('briarberry cohort', 'normal').
card_power('briarberry cohort', 1).
card_toughness('briarberry cohort', 1).

% Found in: DDH, HOP, LRW
card_name('briarhorn', 'Briarhorn').
card_type('briarhorn', 'Creature — Elemental').
card_types('briarhorn', ['Creature']).
card_subtypes('briarhorn', ['Elemental']).
card_colors('briarhorn', ['G']).
card_text('briarhorn', 'Flash\nWhen Briarhorn enters the battlefield, target creature gets +3/+3 until end of turn.\nEvoke {1}{G} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('briarhorn', ['3', 'G']).
card_cmc('briarhorn', 4).
card_layout('briarhorn', 'normal').
card_power('briarhorn', 3).
card_toughness('briarhorn', 3).

% Found in: SOK
card_name('briarknit kami', 'Briarknit Kami').
card_type('briarknit kami', 'Creature — Spirit').
card_types('briarknit kami', ['Creature']).
card_subtypes('briarknit kami', ['Spirit']).
card_colors('briarknit kami', ['G']).
card_text('briarknit kami', 'Whenever you cast a Spirit or Arcane spell, put a +1/+1 counter on target creature.').
card_mana_cost('briarknit kami', ['3', 'G', 'G']).
card_cmc('briarknit kami', 5).
card_layout('briarknit kami', 'normal').
card_power('briarknit kami', 3).
card_toughness('briarknit kami', 3).

% Found in: DKA, M14
card_name('briarpack alpha', 'Briarpack Alpha').
card_type('briarpack alpha', 'Creature — Wolf').
card_types('briarpack alpha', ['Creature']).
card_subtypes('briarpack alpha', ['Wolf']).
card_colors('briarpack alpha', ['G']).
card_text('briarpack alpha', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Briarpack Alpha enters the battlefield, target creature gets +2/+2 until end of turn.').
card_mana_cost('briarpack alpha', ['3', 'G']).
card_cmc('briarpack alpha', 4).
card_layout('briarpack alpha', 'normal').
card_power('briarpack alpha', 3).
card_toughness('briarpack alpha', 3).

% Found in: FRF_UGIN, KTK
card_name('briber\'s purse', 'Briber\'s Purse').
card_type('briber\'s purse', 'Artifact').
card_types('briber\'s purse', ['Artifact']).
card_subtypes('briber\'s purse', []).
card_colors('briber\'s purse', []).
card_text('briber\'s purse', 'Briber\'s Purse enters the battlefield with X gem counters on it.\n{1}, {T}, Remove a gem counter from Briber\'s Purse: Target creature can\'t attack or block this turn.').
card_mana_cost('briber\'s purse', ['X']).
card_cmc('briber\'s purse', 0).
card_layout('briber\'s purse', 'normal').

% Found in: 8ED, MMQ, pJGP
card_name('bribery', 'Bribery').
card_type('bribery', 'Sorcery').
card_types('bribery', ['Sorcery']).
card_subtypes('bribery', []).
card_colors('bribery', ['U']).
card_text('bribery', 'Search target opponent\'s library for a creature card and put that card onto the battlefield under your control. Then that player shuffles his or her library.').
card_mana_cost('bribery', ['3', 'U', 'U']).
card_cmc('bribery', 5).
card_layout('bribery', 'normal').

% Found in: FUT, MMA
card_name('bridge from below', 'Bridge from Below').
card_type('bridge from below', 'Enchantment').
card_types('bridge from below', ['Enchantment']).
card_subtypes('bridge from below', []).
card_colors('bridge from below', ['B']).
card_text('bridge from below', 'Whenever a nontoken creature is put into your graveyard from the battlefield, if Bridge from Below is in your graveyard, put a 2/2 black Zombie creature token onto the battlefield.\nWhen a creature is put into an opponent\'s graveyard from the battlefield, if Bridge from Below is in your graveyard, exile Bridge from Below.').
card_mana_cost('bridge from below', ['B', 'B', 'B']).
card_cmc('bridge from below', 3).
card_layout('bridge from below', 'normal').

% Found in: RAV
card_name('brightflame', 'Brightflame').
card_type('brightflame', 'Sorcery').
card_types('brightflame', ['Sorcery']).
card_subtypes('brightflame', []).
card_colors('brightflame', ['W', 'R']).
card_text('brightflame', 'Radiance — Brightflame deals X damage to target creature and each other creature that shares a color with it. You gain life equal to the damage dealt this way.').
card_mana_cost('brightflame', ['X', 'R', 'R', 'W', 'W']).
card_cmc('brightflame', 4).
card_layout('brightflame', 'normal').

% Found in: MOR
card_name('brighthearth banneret', 'Brighthearth Banneret').
card_type('brighthearth banneret', 'Creature — Elemental Warrior').
card_types('brighthearth banneret', ['Creature']).
card_subtypes('brighthearth banneret', ['Elemental', 'Warrior']).
card_colors('brighthearth banneret', ['R']).
card_text('brighthearth banneret', 'Elemental spells and Warrior spells you cast cost {1} less to cast.\nReinforce 1—{1}{R} ({1}{R}, Discard this card: Put a +1/+1 counter on target creature.)').
card_mana_cost('brighthearth banneret', ['1', 'R']).
card_cmc('brighthearth banneret', 2).
card_layout('brighthearth banneret', 'normal').
card_power('brighthearth banneret', 1).
card_toughness('brighthearth banneret', 1).

% Found in: ONS
card_name('brightstone ritual', 'Brightstone Ritual').
card_type('brightstone ritual', 'Instant').
card_types('brightstone ritual', ['Instant']).
card_subtypes('brightstone ritual', []).
card_colors('brightstone ritual', ['R']).
card_text('brightstone ritual', 'Add {R} to your mana pool for each Goblin on the battlefield.').
card_mana_cost('brightstone ritual', ['R']).
card_cmc('brightstone ritual', 1).
card_layout('brightstone ritual', 'normal').

% Found in: LRW
card_name('brigid, hero of kinsbaile', 'Brigid, Hero of Kinsbaile').
card_type('brigid, hero of kinsbaile', 'Legendary Creature — Kithkin Archer').
card_types('brigid, hero of kinsbaile', ['Creature']).
card_subtypes('brigid, hero of kinsbaile', ['Kithkin', 'Archer']).
card_supertypes('brigid, hero of kinsbaile', ['Legendary']).
card_colors('brigid, hero of kinsbaile', ['W']).
card_text('brigid, hero of kinsbaile', 'First strike\n{T}: Brigid, Hero of Kinsbaile deals 2 damage to each attacking or blocking creature target player controls.').
card_mana_cost('brigid, hero of kinsbaile', ['2', 'W', 'W']).
card_cmc('brigid, hero of kinsbaile', 4).
card_layout('brigid, hero of kinsbaile', 'normal').
card_power('brigid, hero of kinsbaile', 2).
card_toughness('brigid, hero of kinsbaile', 3).

% Found in: USG, VMA
card_name('brilliant halo', 'Brilliant Halo').
card_type('brilliant halo', 'Enchantment — Aura').
card_types('brilliant halo', ['Enchantment']).
card_subtypes('brilliant halo', ['Aura']).
card_colors('brilliant halo', ['W']).
card_text('brilliant halo', 'Enchant creature\nEnchanted creature gets +1/+2.\nWhen Brilliant Halo is put into a graveyard from the battlefield, return Brilliant Halo to its owner\'s hand.').
card_mana_cost('brilliant halo', ['1', 'W']).
card_cmc('brilliant halo', 2).
card_layout('brilliant halo', 'normal').

% Found in: C13, ME3, PTK
card_name('brilliant plan', 'Brilliant Plan').
card_type('brilliant plan', 'Sorcery').
card_types('brilliant plan', ['Sorcery']).
card_subtypes('brilliant plan', []).
card_colors('brilliant plan', ['U']).
card_text('brilliant plan', 'Draw three cards.').
card_mana_cost('brilliant plan', ['4', 'U']).
card_cmc('brilliant plan', 5).
card_layout('brilliant plan', 'normal').

% Found in: BFZ
card_name('brilliant spectrum', 'Brilliant Spectrum').
card_type('brilliant spectrum', 'Sorcery').
card_types('brilliant spectrum', ['Sorcery']).
card_subtypes('brilliant spectrum', []).
card_colors('brilliant spectrum', ['U']).
card_text('brilliant spectrum', 'Converge — Draw X cards, where X is the number of colors of mana spent to cast Brilliant Spectrum. Then discard two cards.').
card_mana_cost('brilliant spectrum', ['3', 'U']).
card_cmc('brilliant spectrum', 4).
card_layout('brilliant spectrum', 'normal').

% Found in: ALA
card_name('brilliant ultimatum', 'Brilliant Ultimatum').
card_type('brilliant ultimatum', 'Sorcery').
card_types('brilliant ultimatum', ['Sorcery']).
card_subtypes('brilliant ultimatum', []).
card_colors('brilliant ultimatum', ['W', 'U', 'B']).
card_text('brilliant ultimatum', 'Exile the top five cards of your library. An opponent separates those cards into two piles. You may play any number of cards from one of those piles without paying their mana costs.').
card_mana_cost('brilliant ultimatum', ['W', 'W', 'U', 'U', 'U', 'B', 'B']).
card_cmc('brilliant ultimatum', 7).
card_layout('brilliant ultimatum', 'normal').

% Found in: BNG
card_name('brimaz, king of oreskos', 'Brimaz, King of Oreskos').
card_type('brimaz, king of oreskos', 'Legendary Creature — Cat Soldier').
card_types('brimaz, king of oreskos', ['Creature']).
card_subtypes('brimaz, king of oreskos', ['Cat', 'Soldier']).
card_supertypes('brimaz, king of oreskos', ['Legendary']).
card_colors('brimaz, king of oreskos', ['W']).
card_text('brimaz, king of oreskos', 'Vigilance\nWhenever Brimaz, King of Oreskos attacks, put a 1/1 white Cat Soldier creature token with vigilance onto the battlefield attacking.\nWhenever Brimaz blocks a creature, put a 1/1 white Cat Soldier creature token with vigilance onto the battlefield blocking that creature.').
card_mana_cost('brimaz, king of oreskos', ['1', 'W', 'W']).
card_cmc('brimaz, king of oreskos', 3).
card_layout('brimaz, king of oreskos', 'normal').
card_power('brimaz, king of oreskos', 3).
card_toughness('brimaz, king of oreskos', 4).

% Found in: ME2, PO2
card_name('brimstone dragon', 'Brimstone Dragon').
card_type('brimstone dragon', 'Creature — Dragon').
card_types('brimstone dragon', ['Creature']).
card_subtypes('brimstone dragon', ['Dragon']).
card_colors('brimstone dragon', ['R']).
card_text('brimstone dragon', 'Flying, haste').
card_mana_cost('brimstone dragon', ['6', 'R', 'R']).
card_cmc('brimstone dragon', 8).
card_layout('brimstone dragon', 'normal').
card_power('brimstone dragon', 6).
card_toughness('brimstone dragon', 6).

% Found in: ROE
card_name('brimstone mage', 'Brimstone Mage').
card_type('brimstone mage', 'Creature — Human Shaman').
card_types('brimstone mage', ['Creature']).
card_subtypes('brimstone mage', ['Human', 'Shaman']).
card_colors('brimstone mage', ['R']).
card_text('brimstone mage', 'Level up {3}{R} ({3}{R}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-2\n2/3\n{T}: Brimstone Mage deals 1 damage to target creature or player.\nLEVEL 3+\n2/4\n{T}: Brimstone Mage deals 3 damage to target creature or player.').
card_mana_cost('brimstone mage', ['2', 'R']).
card_cmc('brimstone mage', 3).
card_layout('brimstone mage', 'leveler').
card_power('brimstone mage', 2).
card_toughness('brimstone mage', 2).

% Found in: CNS, ISD
card_name('brimstone volley', 'Brimstone Volley').
card_type('brimstone volley', 'Instant').
card_types('brimstone volley', ['Instant']).
card_subtypes('brimstone volley', []).
card_colors('brimstone volley', ['R']).
card_text('brimstone volley', 'Brimstone Volley deals 3 damage to target creature or player.\nMorbid — Brimstone Volley deals 5 damage to that creature or player instead if a creature died this turn.').
card_mana_cost('brimstone volley', ['2', 'R']).
card_cmc('brimstone volley', 3).
card_layout('brimstone volley', 'normal').

% Found in: M11, M12, M14
card_name('brindle boar', 'Brindle Boar').
card_type('brindle boar', 'Creature — Boar').
card_types('brindle boar', ['Creature']).
card_subtypes('brindle boar', ['Boar']).
card_colors('brindle boar', ['G']).
card_text('brindle boar', 'Sacrifice Brindle Boar: You gain 4 life.').
card_mana_cost('brindle boar', ['2', 'G']).
card_cmc('brindle boar', 3).
card_layout('brindle boar', 'normal').
card_power('brindle boar', 2).
card_toughness('brindle boar', 2).

% Found in: PC2, VMA
card_name('brindle shoat', 'Brindle Shoat').
card_type('brindle shoat', 'Creature — Boar').
card_types('brindle shoat', ['Creature']).
card_subtypes('brindle shoat', ['Boar']).
card_colors('brindle shoat', ['G']).
card_text('brindle shoat', 'When Brindle Shoat dies, put a 3/3 green Boar creature token onto the battlefield.').
card_mana_cost('brindle shoat', ['1', 'G']).
card_cmc('brindle shoat', 2).
card_layout('brindle shoat', 'normal').
card_power('brindle shoat', 1).
card_toughness('brindle shoat', 1).

% Found in: C14, DD2, DD3_JVC, TSP
card_name('brine elemental', 'Brine Elemental').
card_type('brine elemental', 'Creature — Elemental').
card_types('brine elemental', ['Creature']).
card_subtypes('brine elemental', ['Elemental']).
card_colors('brine elemental', ['U']).
card_text('brine elemental', 'Morph {5}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Brine Elemental is turned face up, each opponent skips his or her next untap step.').
card_mana_cost('brine elemental', ['4', 'U', 'U']).
card_cmc('brine elemental', 6).
card_layout('brine elemental', 'normal').
card_power('brine elemental', 5).
card_toughness('brine elemental', 4).

% Found in: LEG
card_name('brine hag', 'Brine Hag').
card_type('brine hag', 'Creature — Hag').
card_types('brine hag', ['Creature']).
card_subtypes('brine hag', ['Hag']).
card_colors('brine hag', ['U']).
card_text('brine hag', 'When Brine Hag dies, change the base power and toughness of all creatures that dealt damage to it this turn to 0/2. (This effect lasts indefinitely.)').
card_mana_cost('brine hag', ['2', 'U', 'U']).
card_cmc('brine hag', 4).
card_layout('brine hag', 'normal').
card_power('brine hag', 2).
card_toughness('brine hag', 2).

% Found in: UDS
card_name('brine seer', 'Brine Seer').
card_type('brine seer', 'Creature — Human Wizard').
card_types('brine seer', ['Creature']).
card_subtypes('brine seer', ['Human', 'Wizard']).
card_colors('brine seer', ['U']).
card_text('brine seer', '{2}{U}, {T}: Reveal any number of blue cards in your hand. Counter target spell unless its controller pays {1} for each card revealed this way.').
card_mana_cost('brine seer', ['3', 'U']).
card_cmc('brine seer', 4).
card_layout('brine seer', 'normal').
card_power('brine seer', 1).
card_toughness('brine seer', 1).

% Found in: ICE, ME2
card_name('brine shaman', 'Brine Shaman').
card_type('brine shaman', 'Creature — Human Cleric Shaman').
card_types('brine shaman', ['Creature']).
card_subtypes('brine shaman', ['Human', 'Cleric', 'Shaman']).
card_colors('brine shaman', ['B']).
card_text('brine shaman', '{T}, Sacrifice a creature: Target creature gets +2/+2 until end of turn.\n{1}{U}{U}, Sacrifice a creature: Counter target creature spell.').
card_mana_cost('brine shaman', ['1', 'B']).
card_cmc('brine shaman', 2).
card_layout('brine shaman', 'normal').
card_power('brine shaman', 1).
card_toughness('brine shaman', 1).

% Found in: KTK
card_name('bring low', 'Bring Low').
card_type('bring low', 'Instant').
card_types('bring low', ['Instant']).
card_subtypes('bring low', []).
card_colors('bring low', ['R']).
card_text('bring low', 'Bring Low deals 3 damage to target creature. If that creature has a +1/+1 counter on it, Bring Low deals 5 damage to it instead.').
card_mana_cost('bring low', ['3', 'R']).
card_cmc('bring low', 4).
card_layout('bring low', 'normal').

% Found in: BFZ
card_name('bring to light', 'Bring to Light').
card_type('bring to light', 'Sorcery').
card_types('bring to light', ['Sorcery']).
card_subtypes('bring to light', []).
card_colors('bring to light', ['U', 'G']).
card_text('bring to light', 'Converge — Search your library for a creature, instant, or sorcery card with converted mana cost less than or equal to the number of colors of mana spent to cast Bring to Light, exile that card, then shuffle your library. You may cast that card without paying its mana cost.').
card_mana_cost('bring to light', ['3', 'G', 'U']).
card_cmc('bring to light', 5).
card_layout('bring to light', 'normal').

% Found in: 5DN
card_name('bringer of the black dawn', 'Bringer of the Black Dawn').
card_type('bringer of the black dawn', 'Creature — Bringer').
card_types('bringer of the black dawn', ['Creature']).
card_subtypes('bringer of the black dawn', ['Bringer']).
card_colors('bringer of the black dawn', ['B']).
card_text('bringer of the black dawn', 'You may pay {W}{U}{B}{R}{G} rather than pay Bringer of the Black Dawn\'s mana cost.\nTrample\nAt the beginning of your upkeep, you may pay 2 life. If you do, search your library for a card, then shuffle your library and put that card on top of it.').
card_mana_cost('bringer of the black dawn', ['7', 'B', 'B']).
card_cmc('bringer of the black dawn', 9).
card_layout('bringer of the black dawn', 'normal').
card_power('bringer of the black dawn', 5).
card_toughness('bringer of the black dawn', 5).

% Found in: 5DN
card_name('bringer of the blue dawn', 'Bringer of the Blue Dawn').
card_type('bringer of the blue dawn', 'Creature — Bringer').
card_types('bringer of the blue dawn', ['Creature']).
card_subtypes('bringer of the blue dawn', ['Bringer']).
card_colors('bringer of the blue dawn', ['U']).
card_text('bringer of the blue dawn', 'You may pay {W}{U}{B}{R}{G} rather than pay Bringer of the Blue Dawn\'s mana cost.\nTrample\nAt the beginning of your upkeep, you may draw two cards.').
card_mana_cost('bringer of the blue dawn', ['7', 'U', 'U']).
card_cmc('bringer of the blue dawn', 9).
card_layout('bringer of the blue dawn', 'normal').
card_power('bringer of the blue dawn', 5).
card_toughness('bringer of the blue dawn', 5).

% Found in: 5DN
card_name('bringer of the green dawn', 'Bringer of the Green Dawn').
card_type('bringer of the green dawn', 'Creature — Bringer').
card_types('bringer of the green dawn', ['Creature']).
card_subtypes('bringer of the green dawn', ['Bringer']).
card_colors('bringer of the green dawn', ['G']).
card_text('bringer of the green dawn', 'You may pay {W}{U}{B}{R}{G} rather than pay Bringer of the Green Dawn\'s mana cost.\nTrample\nAt the beginning of your upkeep, you may put a 3/3 green Beast creature token onto the battlefield.').
card_mana_cost('bringer of the green dawn', ['7', 'G', 'G']).
card_cmc('bringer of the green dawn', 9).
card_layout('bringer of the green dawn', 'normal').
card_power('bringer of the green dawn', 5).
card_toughness('bringer of the green dawn', 5).

% Found in: 5DN
card_name('bringer of the red dawn', 'Bringer of the Red Dawn').
card_type('bringer of the red dawn', 'Creature — Bringer').
card_types('bringer of the red dawn', ['Creature']).
card_subtypes('bringer of the red dawn', ['Bringer']).
card_colors('bringer of the red dawn', ['R']).
card_text('bringer of the red dawn', 'You may pay {W}{U}{B}{R}{G} rather than pay Bringer of the Red Dawn\'s mana cost.\nTrample\nAt the beginning of your upkeep, you may untap target creature and gain control of it until end of turn. That creature gains haste until end of turn.').
card_mana_cost('bringer of the red dawn', ['7', 'R', 'R']).
card_cmc('bringer of the red dawn', 9).
card_layout('bringer of the red dawn', 'normal').
card_power('bringer of the red dawn', 5).
card_toughness('bringer of the red dawn', 5).

% Found in: 5DN
card_name('bringer of the white dawn', 'Bringer of the White Dawn').
card_type('bringer of the white dawn', 'Creature — Bringer').
card_types('bringer of the white dawn', ['Creature']).
card_subtypes('bringer of the white dawn', ['Bringer']).
card_colors('bringer of the white dawn', ['W']).
card_text('bringer of the white dawn', 'You may pay {W}{U}{B}{R}{G} rather than pay Bringer of the White Dawn\'s mana cost.\nTrample\nAt the beginning of your upkeep, you may return target artifact card from your graveyard to the battlefield.').
card_mana_cost('bringer of the white dawn', ['7', 'W', 'W']).
card_cmc('bringer of the white dawn', 9).
card_layout('bringer of the white dawn', 'normal').
card_power('bringer of the white dawn', 5).
card_toughness('bringer of the white dawn', 5).

% Found in: M12, WWK
card_name('brink of disaster', 'Brink of Disaster').
card_type('brink of disaster', 'Enchantment — Aura').
card_types('brink of disaster', ['Enchantment']).
card_subtypes('brink of disaster', ['Aura']).
card_colors('brink of disaster', ['B']).
card_text('brink of disaster', 'Enchant creature or land\nWhen enchanted permanent becomes tapped, destroy it.').
card_mana_cost('brink of disaster', ['2', 'B', 'B']).
card_cmc('brink of disaster', 4).
card_layout('brink of disaster', 'normal').

% Found in: ULG
card_name('brink of madness', 'Brink of Madness').
card_type('brink of madness', 'Enchantment').
card_types('brink of madness', ['Enchantment']).
card_subtypes('brink of madness', []).
card_colors('brink of madness', ['B']).
card_text('brink of madness', 'At the beginning of your upkeep, if you have no cards in hand, sacrifice Brink of Madness and target opponent discards his or her hand.').
card_mana_cost('brink of madness', ['2', 'B', 'B']).
card_cmc('brink of madness', 4).
card_layout('brink of madness', 'normal').

% Found in: CMD, LRW, pMEI
card_name('brion stoutarm', 'Brion Stoutarm').
card_type('brion stoutarm', 'Legendary Creature — Giant Warrior').
card_types('brion stoutarm', ['Creature']).
card_subtypes('brion stoutarm', ['Giant', 'Warrior']).
card_supertypes('brion stoutarm', ['Legendary']).
card_colors('brion stoutarm', ['W', 'R']).
card_text('brion stoutarm', 'Lifelink\n{R}, {T}, Sacrifice a creature other than Brion Stoutarm: Brion Stoutarm deals damage equal to the sacrificed creature\'s power to target player.').
card_mana_cost('brion stoutarm', ['2', 'R', 'W']).
card_cmc('brion stoutarm', 4).
card_layout('brion stoutarm', 'normal').
card_power('brion stoutarm', 4).
card_toughness('brion stoutarm', 4).

% Found in: M11
card_name('brittle effigy', 'Brittle Effigy').
card_type('brittle effigy', 'Artifact').
card_types('brittle effigy', ['Artifact']).
card_subtypes('brittle effigy', []).
card_colors('brittle effigy', []).
card_text('brittle effigy', '{4}, {T}, Exile Brittle Effigy: Exile target creature.').
card_mana_cost('brittle effigy', ['1']).
card_cmc('brittle effigy', 1).
card_layout('brittle effigy', 'normal').

% Found in: LRW
card_name('broken ambitions', 'Broken Ambitions').
card_type('broken ambitions', 'Instant').
card_types('broken ambitions', ['Instant']).
card_subtypes('broken ambitions', []).
card_colors('broken ambitions', ['U']).
card_text('broken ambitions', 'Counter target spell unless its controller pays {X}. Clash with an opponent. If you win, that spell\'s controller puts the top four cards of his or her library into his or her graveyard. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('broken ambitions', ['X', 'U']).
card_cmc('broken ambitions', 1).
card_layout('broken ambitions', 'normal').

% Found in: PTK
card_name('broken dam', 'Broken Dam').
card_type('broken dam', 'Sorcery').
card_types('broken dam', ['Sorcery']).
card_subtypes('broken dam', []).
card_colors('broken dam', ['U']).
card_text('broken dam', 'Tap one or two target creatures without horsemanship.').
card_mana_cost('broken dam', ['U']).
card_cmc('broken dam', 1).
card_layout('broken dam', 'normal').

% Found in: BRB, TMP
card_name('broken fall', 'Broken Fall').
card_type('broken fall', 'Enchantment').
card_types('broken fall', ['Enchantment']).
card_subtypes('broken fall', []).
card_colors('broken fall', ['G']).
card_text('broken fall', 'Return Broken Fall to its owner\'s hand: Regenerate target creature.').
card_mana_cost('broken fall', ['2', 'G']).
card_cmc('broken fall', 3).
card_layout('broken fall', 'normal').

% Found in: 5ED, HML, ME2
card_name('broken visage', 'Broken Visage').
card_type('broken visage', 'Instant').
card_types('broken visage', ['Instant']).
card_subtypes('broken visage', []).
card_colors('broken visage', ['B']).
card_text('broken visage', 'Destroy target nonartifact attacking creature. It can\'t be regenerated. Put a black Spirit creature token with that creature\'s power and toughness onto the battlefield. Sacrifice the token at the beginning of the next end step.').
card_mana_cost('broken visage', ['4', 'B']).
card_cmc('broken visage', 5).
card_layout('broken visage', 'normal').

% Found in: LGN
card_name('brontotherium', 'Brontotherium').
card_type('brontotherium', 'Creature — Beast').
card_types('brontotherium', ['Creature']).
card_subtypes('brontotherium', ['Beast']).
card_colors('brontotherium', ['G']).
card_text('brontotherium', 'Trample\nProvoke (Whenever this creature attacks, you may have target creature defending player controls untap and block it if able.)').
card_mana_cost('brontotherium', ['4', 'G', 'G']).
card_cmc('brontotherium', 6).
card_layout('brontotherium', 'normal').
card_power('brontotherium', 5).
card_toughness('brontotherium', 3).

% Found in: DIS
card_name('bronze bombshell', 'Bronze Bombshell').
card_type('bronze bombshell', 'Artifact Creature — Construct').
card_types('bronze bombshell', ['Artifact', 'Creature']).
card_subtypes('bronze bombshell', ['Construct']).
card_colors('bronze bombshell', []).
card_text('bronze bombshell', 'When a player other than Bronze Bombshell\'s owner controls it, that player sacrifices it. If the player does, Bronze Bombshell deals 7 damage to him or her.').
card_mana_cost('bronze bombshell', ['4']).
card_cmc('bronze bombshell', 4).
card_layout('bronze bombshell', 'normal').
card_power('bronze bombshell', 4).
card_toughness('bronze bombshell', 1).

% Found in: UGL
card_name('bronze calendar', 'Bronze Calendar').
card_type('bronze calendar', 'Artifact').
card_types('bronze calendar', ['Artifact']).
card_subtypes('bronze calendar', []).
card_colors('bronze calendar', []).
card_text('bronze calendar', 'Your spells cost {1} less to play as long as you speak in a voice other than your normal voice.\nIf you speak in your normal voice, sacrifice Bronze Calendar.').
card_mana_cost('bronze calendar', ['4']).
card_cmc('bronze calendar', 4).
card_layout('bronze calendar', 'normal').

% Found in: CHR, LEG, ME4
card_name('bronze horse', 'Bronze Horse').
card_type('bronze horse', 'Artifact Creature — Horse').
card_types('bronze horse', ['Artifact', 'Creature']).
card_subtypes('bronze horse', ['Horse']).
card_colors('bronze horse', []).
card_text('bronze horse', 'Trample\nAs long as you control another creature, prevent all damage that would be dealt to Bronze Horse by spells that target it.').
card_mana_cost('bronze horse', ['7']).
card_cmc('bronze horse', 7).
card_layout('bronze horse', 'normal').
card_power('bronze horse', 4).
card_toughness('bronze horse', 4).

% Found in: M15, THS
card_name('bronze sable', 'Bronze Sable').
card_type('bronze sable', 'Artifact Creature — Sable').
card_types('bronze sable', ['Artifact', 'Creature']).
card_subtypes('bronze sable', ['Sable']).
card_colors('bronze sable', []).
card_text('bronze sable', '').
card_mana_cost('bronze sable', ['2']).
card_cmc('bronze sable', 2).
card_layout('bronze sable', 'normal').
card_power('bronze sable', 2).
card_toughness('bronze sable', 1).

% Found in: 4ED, ATQ
card_name('bronze tablet', 'Bronze Tablet').
card_type('bronze tablet', 'Artifact').
card_types('bronze tablet', ['Artifact']).
card_subtypes('bronze tablet', []).
card_colors('bronze tablet', []).
card_text('bronze tablet', 'Remove Bronze Tablet from your deck before playing if you\'re not playing for ante.\nBronze Tablet enters the battlefield tapped.\n{4}, {T}: Exile Bronze Tablet and target nontoken permanent an opponent owns. That player may pay 10 life. If he or she does, put Bronze Tablet into its owner\'s graveyard. Otherwise, that player owns Bronze Tablet and you own the other exiled card.').
card_mana_cost('bronze tablet', ['6']).
card_cmc('bronze tablet', 6).
card_layout('bronze tablet', 'normal').

% Found in: DGM
card_name('bronzebeak moa', 'Bronzebeak Moa').
card_type('bronzebeak moa', 'Creature — Bird').
card_types('bronzebeak moa', ['Creature']).
card_subtypes('bronzebeak moa', ['Bird']).
card_colors('bronzebeak moa', ['W', 'G']).
card_text('bronzebeak moa', 'Whenever another creature enters the battlefield under your control, Bronzebeak Moa gets +3/+3 until end of turn.').
card_mana_cost('bronzebeak moa', ['2', 'G', 'W']).
card_cmc('bronzebeak moa', 4).
card_layout('bronzebeak moa', 'normal').
card_power('bronzebeak moa', 2).
card_toughness('bronzebeak moa', 2).

% Found in: ROE
card_name('brood birthing', 'Brood Birthing').
card_type('brood birthing', 'Sorcery').
card_types('brood birthing', ['Sorcery']).
card_subtypes('brood birthing', []).
card_colors('brood birthing', ['R']).
card_text('brood birthing', 'If you control an Eldrazi Spawn, put three 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\" Otherwise, put one of those tokens onto the battlefield.').
card_mana_cost('brood birthing', ['1', 'R']).
card_cmc('brood birthing', 2).
card_layout('brood birthing', 'normal').

% Found in: BFZ
card_name('brood butcher', 'Brood Butcher').
card_type('brood butcher', 'Creature — Eldrazi Drone').
card_types('brood butcher', ['Creature']).
card_subtypes('brood butcher', ['Eldrazi', 'Drone']).
card_colors('brood butcher', []).
card_text('brood butcher', 'Devoid (This card has no color.)\nWhen Brood Butcher enters the battlefield, put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"\n{B}{G}, Sacrifice a creature: Target creature gets -2/-2 until end of turn.').
card_mana_cost('brood butcher', ['3', 'B', 'G']).
card_cmc('brood butcher', 5).
card_layout('brood butcher', 'normal').
card_power('brood butcher', 3).
card_toughness('brood butcher', 3).

% Found in: M15
card_name('brood keeper', 'Brood Keeper').
card_type('brood keeper', 'Creature — Human Shaman').
card_types('brood keeper', ['Creature']).
card_subtypes('brood keeper', ['Human', 'Shaman']).
card_colors('brood keeper', ['R']).
card_text('brood keeper', 'Whenever an Aura becomes attached to Brood Keeper, put a 2/2 red Dragon creature token with flying onto the battlefield. It has \"{R}: This creature gets +1/+0 until end of turn.\"').
card_mana_cost('brood keeper', ['3', 'R']).
card_cmc('brood keeper', 4).
card_layout('brood keeper', 'normal').
card_power('brood keeper', 2).
card_toughness('brood keeper', 3).

% Found in: BFZ
card_name('brood monitor', 'Brood Monitor').
card_type('brood monitor', 'Creature — Eldrazi Drone').
card_types('brood monitor', ['Creature']).
card_subtypes('brood monitor', ['Eldrazi', 'Drone']).
card_colors('brood monitor', []).
card_text('brood monitor', 'Devoid (This card has no color.)\nWhen Brood Monitor enters the battlefield, put three 1/1 colorless Eldrazi Scion creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('brood monitor', ['4', 'G', 'G']).
card_cmc('brood monitor', 6).
card_layout('brood monitor', 'normal').
card_power('brood monitor', 3).
card_toughness('brood monitor', 3).

% Found in: VIS
card_name('brood of cockroaches', 'Brood of Cockroaches').
card_type('brood of cockroaches', 'Creature — Insect').
card_types('brood of cockroaches', ['Creature']).
card_subtypes('brood of cockroaches', ['Insect']).
card_colors('brood of cockroaches', ['B']).
card_text('brood of cockroaches', 'When Brood of Cockroaches is put into your graveyard from the battlefield, at the beginning of the next end step, you lose 1 life and return Brood of Cockroaches to your hand.').
card_mana_cost('brood of cockroaches', ['1', 'B']).
card_cmc('brood of cockroaches', 2).
card_layout('brood of cockroaches', 'normal').
card_power('brood of cockroaches', 1).
card_toughness('brood of cockroaches', 1).

% Found in: H09, LGN
card_name('brood sliver', 'Brood Sliver').
card_type('brood sliver', 'Creature — Sliver').
card_types('brood sliver', ['Creature']).
card_subtypes('brood sliver', ['Sliver']).
card_colors('brood sliver', ['G']).
card_text('brood sliver', 'Whenever a Sliver deals combat damage to a player, its controller may put a 1/1 colorless Sliver creature token onto the battlefield.').
card_mana_cost('brood sliver', ['4', 'G']).
card_cmc('brood sliver', 5).
card_layout('brood sliver', 'normal').
card_power('brood sliver', 3).
card_toughness('brood sliver', 3).

% Found in: ONS
card_name('broodhatch nantuko', 'Broodhatch Nantuko').
card_type('broodhatch nantuko', 'Creature — Insect Druid').
card_types('broodhatch nantuko', ['Creature']).
card_subtypes('broodhatch nantuko', ['Insect', 'Druid']).
card_colors('broodhatch nantuko', ['G']).
card_text('broodhatch nantuko', 'Whenever Broodhatch Nantuko is dealt damage, you may put that many 1/1 green Insect creature tokens onto the battlefield.\nMorph {2}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('broodhatch nantuko', ['1', 'G']).
card_cmc('broodhatch nantuko', 2).
card_layout('broodhatch nantuko', 'normal').
card_power('broodhatch nantuko', 1).
card_toughness('broodhatch nantuko', 1).

% Found in: BFZ
card_name('broodhunter wurm', 'Broodhunter Wurm').
card_type('broodhunter wurm', 'Creature — Wurm').
card_types('broodhunter wurm', ['Creature']).
card_subtypes('broodhunter wurm', ['Wurm']).
card_colors('broodhunter wurm', ['G']).
card_text('broodhunter wurm', '').
card_mana_cost('broodhunter wurm', ['3', 'G']).
card_cmc('broodhunter wurm', 4).
card_layout('broodhunter wurm', 'normal').
card_power('broodhunter wurm', 4).
card_toughness('broodhunter wurm', 3).

% Found in: C13, CSP
card_name('brooding saurian', 'Brooding Saurian').
card_type('brooding saurian', 'Creature — Lizard').
card_types('brooding saurian', ['Creature']).
card_subtypes('brooding saurian', ['Lizard']).
card_colors('brooding saurian', ['G']).
card_text('brooding saurian', 'At the beginning of each end step, each player gains control of all nontoken permanents he or she owns.').
card_mana_cost('brooding saurian', ['2', 'G', 'G']).
card_cmc('brooding saurian', 4).
card_layout('brooding saurian', 'normal').
card_power('brooding saurian', 4).
card_toughness('brooding saurian', 4).

% Found in: ALA, pMEI
card_name('broodmate dragon', 'Broodmate Dragon').
card_type('broodmate dragon', 'Creature — Dragon').
card_types('broodmate dragon', ['Creature']).
card_subtypes('broodmate dragon', ['Dragon']).
card_colors('broodmate dragon', ['B', 'R', 'G']).
card_text('broodmate dragon', 'Flying\nWhen Broodmate Dragon enters the battlefield, put a 4/4 red Dragon creature token with flying onto the battlefield.').
card_mana_cost('broodmate dragon', ['3', 'B', 'R', 'G']).
card_cmc('broodmate dragon', 6).
card_layout('broodmate dragon', 'normal').
card_power('broodmate dragon', 4).
card_toughness('broodmate dragon', 4).

% Found in: HOP, MRD
card_name('broodstar', 'Broodstar').
card_type('broodstar', 'Creature — Beast').
card_types('broodstar', ['Creature']).
card_subtypes('broodstar', ['Beast']).
card_colors('broodstar', ['U']).
card_text('broodstar', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nFlying\nBroodstar\'s power and toughness are each equal to the number of artifacts you control.').
card_mana_cost('broodstar', ['8', 'U', 'U']).
card_cmc('broodstar', 10).
card_layout('broodstar', 'normal').
card_power('broodstar', '*').
card_toughness('broodstar', '*').

% Found in: ROE
card_name('broodwarden', 'Broodwarden').
card_type('broodwarden', 'Creature — Eldrazi Drone').
card_types('broodwarden', ['Creature']).
card_subtypes('broodwarden', ['Eldrazi', 'Drone']).
card_colors('broodwarden', ['G']).
card_text('broodwarden', 'Eldrazi Spawn creatures you control get +2/+1.').
card_mana_cost('broodwarden', ['3', 'G', 'G']).
card_cmc('broodwarden', 5).
card_layout('broodwarden', 'normal').
card_power('broodwarden', 4).
card_toughness('broodwarden', 4).

% Found in: 4ED, 5ED, DRK, MED
card_name('brothers of fire', 'Brothers of Fire').
card_type('brothers of fire', 'Creature — Human Shaman').
card_types('brothers of fire', ['Creature']).
card_subtypes('brothers of fire', ['Human', 'Shaman']).
card_colors('brothers of fire', ['R']).
card_text('brothers of fire', '{1}{R}{R}: Brothers of Fire deals 1 damage to target creature or player and 1 damage to you.').
card_mana_cost('brothers of fire', ['1', 'R', 'R']).
card_cmc('brothers of fire', 3).
card_layout('brothers of fire', 'normal').
card_power('brothers of fire', 2).
card_toughness('brothers of fire', 2).

% Found in: CHK
card_name('brothers yamazaki', 'Brothers Yamazaki').
card_type('brothers yamazaki', 'Legendary Creature — Human Samurai').
card_types('brothers yamazaki', ['Creature']).
card_subtypes('brothers yamazaki', ['Human', 'Samurai']).
card_supertypes('brothers yamazaki', ['Legendary']).
card_colors('brothers yamazaki', ['R']).
card_text('brothers yamazaki', 'Bushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)\nIf there are exactly two permanents named Brothers Yamazaki on the battlefield, the \"legend rule\" doesn\'t apply to them.\nEach other creature named Brothers Yamazaki gets +2/+2 and has haste.').
card_mana_cost('brothers yamazaki', ['2', 'R']).
card_cmc('brothers yamazaki', 3).
card_layout('brothers yamazaki', 'normal').
card_power('brothers yamazaki', 2).
card_toughness('brothers yamazaki', 1).

% Found in: DDK, HOP, JUD, PD2, TSB, pFNM
card_name('browbeat', 'Browbeat').
card_type('browbeat', 'Sorcery').
card_types('browbeat', ['Sorcery']).
card_subtypes('browbeat', []).
card_colors('browbeat', ['R']).
card_text('browbeat', 'Any player may have Browbeat deal 5 damage to him or her. If no one does, target player draws three cards.').
card_mana_cost('browbeat', ['2', 'R']).
card_cmc('browbeat', 3).
card_layout('browbeat', 'normal').

% Found in: ICE, MRD
card_name('brown ouphe', 'Brown Ouphe').
card_type('brown ouphe', 'Creature — Ouphe').
card_types('brown ouphe', ['Creature']).
card_subtypes('brown ouphe', ['Ouphe']).
card_colors('brown ouphe', ['G']).
card_text('brown ouphe', '{1}{G}, {T}: Counter target activated ability from an artifact source. (Mana abilities can\'t be targeted.)').
card_mana_cost('brown ouphe', ['G']).
card_cmc('brown ouphe', 1).
card_layout('brown ouphe', 'normal').
card_power('brown ouphe', 1).
card_toughness('brown ouphe', 1).

% Found in: 6ED, ALL, CST, ME2
card_name('browse', 'Browse').
card_type('browse', 'Enchantment').
card_types('browse', ['Enchantment']).
card_subtypes('browse', []).
card_colors('browse', ['U']).
card_text('browse', '{2}{U}{U}: Look at the top five cards of your library, put one of them into your hand, and exile the rest.').
card_mana_cost('browse', ['2', 'U', 'U']).
card_cmc('browse', 4).
card_layout('browse', 'normal').

% Found in: AVR
card_name('bruna, light of alabaster', 'Bruna, Light of Alabaster').
card_type('bruna, light of alabaster', 'Legendary Creature — Angel').
card_types('bruna, light of alabaster', ['Creature']).
card_subtypes('bruna, light of alabaster', ['Angel']).
card_supertypes('bruna, light of alabaster', ['Legendary']).
card_colors('bruna, light of alabaster', ['W', 'U']).
card_text('bruna, light of alabaster', 'Flying, vigilance\nWhenever Bruna, Light of Alabaster attacks or blocks, you may attach to it any number of Auras on the battlefield and you may put onto the battlefield attached to it any number of Aura cards that could enchant it from your graveyard and/or hand.').
card_mana_cost('bruna, light of alabaster', ['3', 'W', 'W', 'U']).
card_cmc('bruna, light of alabaster', 6).
card_layout('bruna, light of alabaster', 'normal').
card_power('bruna, light of alabaster', 5).
card_toughness('bruna, light of alabaster', 5).

% Found in: STH
card_name('brush with death', 'Brush with Death').
card_type('brush with death', 'Sorcery').
card_types('brush with death', ['Sorcery']).
card_subtypes('brush with death', []).
card_colors('brush with death', ['B']).
card_text('brush with death', 'Buyback {2}{B}{B} (You may pay an additional {2}{B}{B} as you cast this spell. If you do, put this card into your hand as it resolves.)\nTarget opponent loses 2 life. You gain 2 life.').
card_mana_cost('brush with death', ['2', 'B']).
card_cmc('brush with death', 3).
card_layout('brush with death', 'normal').

% Found in: 10E, 5ED, 6ED, 7ED, 9ED, ATH, ICE
card_name('brushland', 'Brushland').
card_type('brushland', 'Land').
card_types('brushland', ['Land']).
card_subtypes('brushland', []).
card_colors('brushland', []).
card_text('brushland', '{T}: Add {1} to your mana pool.\n{T}: Add {G} or {W} to your mana pool. Brushland deals 1 damage to you.').
card_layout('brushland', 'normal').

% Found in: RTR
card_name('brushstrider', 'Brushstrider').
card_type('brushstrider', 'Creature — Beast').
card_types('brushstrider', ['Creature']).
card_subtypes('brushstrider', ['Beast']).
card_colors('brushstrider', ['G']).
card_text('brushstrider', 'Vigilance').
card_mana_cost('brushstrider', ['1', 'G']).
card_cmc('brushstrider', 2).
card_layout('brushstrider', 'normal').
card_power('brushstrider', 3).
card_toughness('brushstrider', 1).

% Found in: UNH
card_name('brushstroke paintermage', 'Brushstroke Paintermage').
card_type('brushstroke paintermage', 'Creature — Human Wizard').
card_types('brushstroke paintermage', ['Creature']).
card_subtypes('brushstroke paintermage', ['Human', 'Wizard']).
card_colors('brushstroke paintermage', ['U']).
card_text('brushstroke paintermage', '{T}: Target permanent\'s artist becomes the artist of your choice until end of turn.').
card_mana_cost('brushstroke paintermage', ['3', 'U']).
card_cmc('brushstroke paintermage', 4).
card_layout('brushstroke paintermage', 'normal').
card_power('brushstroke paintermage', 2).
card_toughness('brushstroke paintermage', 3).

% Found in: MIR
card_name('brushwagg', 'Brushwagg').
card_type('brushwagg', 'Creature — Brushwagg').
card_types('brushwagg', ['Creature']).
card_subtypes('brushwagg', ['Brushwagg']).
card_colors('brushwagg', ['G']).
card_text('brushwagg', 'Whenever Brushwagg blocks or becomes blocked, it gets -2/+2 until end of turn.').
card_mana_cost('brushwagg', ['1', 'G', 'G']).
card_cmc('brushwagg', 3).
card_layout('brushwagg', 'normal').
card_power('brushwagg', 3).
card_toughness('brushwagg', 2).
card_reserved('brushwagg').

% Found in: CHK
card_name('brutal deceiver', 'Brutal Deceiver').
card_type('brutal deceiver', 'Creature — Spirit').
card_types('brutal deceiver', ['Creature']).
card_subtypes('brutal deceiver', ['Spirit']).
card_colors('brutal deceiver', ['R']).
card_text('brutal deceiver', '{1}: Look at the top card of your library.\n{2}: Reveal the top card of your library. If it\'s a land card, Brutal Deceiver gets +1/+0 and gains first strike until end of turn. Activate this ability only once each turn.').
card_mana_cost('brutal deceiver', ['2', 'R']).
card_cmc('brutal deceiver', 3).
card_layout('brutal deceiver', 'normal').
card_power('brutal deceiver', 2).
card_toughness('brutal deceiver', 2).

% Found in: BFZ
card_name('brutal expulsion', 'Brutal Expulsion').
card_type('brutal expulsion', 'Instant').
card_types('brutal expulsion', ['Instant']).
card_subtypes('brutal expulsion', []).
card_colors('brutal expulsion', []).
card_text('brutal expulsion', 'Devoid (This card has no color.)\nChoose one or both —\n• Return target spell or creature to its owner\'s hand.\n• Brutal Expulsion deals 2 damage to target creature or planeswalker. If that permanent would be put into a graveyard this turn, exile it instead.').
card_mana_cost('brutal expulsion', ['2', 'U', 'R']).
card_cmc('brutal expulsion', 4).
card_layout('brutal expulsion', 'normal').

% Found in: FRF
card_name('brutal hordechief', 'Brutal Hordechief').
card_type('brutal hordechief', 'Creature — Orc Warrior').
card_types('brutal hordechief', ['Creature']).
card_subtypes('brutal hordechief', ['Orc', 'Warrior']).
card_colors('brutal hordechief', ['B']).
card_text('brutal hordechief', 'Whenever a creature you control attacks, defending player loses 1 life and you gain 1 life.\n{3}{R/W}{R/W}: Creatures your opponents control block this turn if able, and you choose how those creatures block.').
card_mana_cost('brutal hordechief', ['3', 'B']).
card_cmc('brutal hordechief', 4).
card_layout('brutal hordechief', 'normal').
card_power('brutal hordechief', 3).
card_toughness('brutal hordechief', 3).

% Found in: PO2
card_name('brutal nightstalker', 'Brutal Nightstalker').
card_type('brutal nightstalker', 'Creature — Nightstalker').
card_types('brutal nightstalker', ['Creature']).
card_subtypes('brutal nightstalker', ['Nightstalker']).
card_colors('brutal nightstalker', ['B']).
card_text('brutal nightstalker', 'When Brutal Nightstalker enters the battlefield, you may have target opponent discard a card.').
card_mana_cost('brutal nightstalker', ['3', 'B', 'B']).
card_cmc('brutal nightstalker', 5).
card_layout('brutal nightstalker', 'normal').
card_power('brutal nightstalker', 3).
card_toughness('brutal nightstalker', 2).

% Found in: PCY
card_name('brutal suppression', 'Brutal Suppression').
card_type('brutal suppression', 'Enchantment').
card_types('brutal suppression', ['Enchantment']).
card_subtypes('brutal suppression', []).
card_colors('brutal suppression', ['R']).
card_text('brutal suppression', 'Activated abilities of nontoken Rebels cost an additional \"Sacrifice a land\" to activate.').
card_mana_cost('brutal suppression', ['R']).
card_cmc('brutal suppression', 1).
card_layout('brutal suppression', 'normal').

% Found in: NPH, PC2
card_name('brutalizer exarch', 'Brutalizer Exarch').
card_type('brutalizer exarch', 'Creature — Cleric').
card_types('brutalizer exarch', ['Creature']).
card_subtypes('brutalizer exarch', ['Cleric']).
card_colors('brutalizer exarch', ['G']).
card_text('brutalizer exarch', 'When Brutalizer Exarch enters the battlefield, choose one —\n• Search your library for a creature card, reveal it, then shuffle your library and put that card on top of it.\n• Put target noncreature permanent on the bottom of its owner\'s library.').
card_mana_cost('brutalizer exarch', ['5', 'G']).
card_cmc('brutalizer exarch', 6).
card_layout('brutalizer exarch', 'normal').
card_power('brutalizer exarch', 3).
card_toughness('brutalizer exarch', 3).

% Found in: MM2, MMA, PLC
card_name('brute force', 'Brute Force').
card_type('brute force', 'Instant').
card_types('brute force', ['Instant']).
card_subtypes('brute force', []).
card_colors('brute force', ['R']).
card_text('brute force', 'Target creature gets +3/+3 until end of turn.').
card_mana_cost('brute force', ['R']).
card_cmc('brute force', 1).
card_layout('brute force', 'normal').

% Found in: WTH
card_name('bubble matrix', 'Bubble Matrix').
card_type('bubble matrix', 'Artifact').
card_types('bubble matrix', ['Artifact']).
card_subtypes('bubble matrix', []).
card_colors('bubble matrix', []).
card_text('bubble matrix', 'Prevent all damage that would be dealt to creatures.').
card_mana_cost('bubble matrix', ['4']).
card_cmc('bubble matrix', 4).
card_layout('bubble matrix', 'normal').
card_reserved('bubble matrix').

% Found in: UDS
card_name('bubbling beebles', 'Bubbling Beebles').
card_type('bubbling beebles', 'Creature — Beeble').
card_types('bubbling beebles', ['Creature']).
card_subtypes('bubbling beebles', ['Beeble']).
card_colors('bubbling beebles', ['U']).
card_text('bubbling beebles', 'Bubbling Beebles can\'t be blocked as long as defending player controls an enchantment.').
card_mana_cost('bubbling beebles', ['4', 'U']).
card_cmc('bubbling beebles', 5).
card_layout('bubbling beebles', 'normal').
card_power('bubbling beebles', 3).
card_toughness('bubbling beebles', 3).

% Found in: M14
card_name('bubbling cauldron', 'Bubbling Cauldron').
card_type('bubbling cauldron', 'Artifact').
card_types('bubbling cauldron', ['Artifact']).
card_subtypes('bubbling cauldron', []).
card_colors('bubbling cauldron', []).
card_text('bubbling cauldron', '{1}, {T}, Sacrifice a creature: You gain 4 life.\n{1}, {T}, Sacrifice a creature named Festering Newt: Each opponent loses 4 life. You gain life equal to the life lost this way.').
card_mana_cost('bubbling cauldron', ['2']).
card_cmc('bubbling cauldron', 2).
card_layout('bubbling cauldron', 'normal').

% Found in: UDS
card_name('bubbling muck', 'Bubbling Muck').
card_type('bubbling muck', 'Sorcery').
card_types('bubbling muck', ['Sorcery']).
card_subtypes('bubbling muck', []).
card_colors('bubbling muck', ['B']).
card_text('bubbling muck', 'Until end of turn, whenever a player taps a Swamp for mana, that player adds {B} to his or her mana pool (in addition to the mana the land produces).').
card_mana_cost('bubbling muck', ['B']).
card_cmc('bubbling muck', 1).
card_layout('bubbling muck', 'normal').

% Found in: CHK
card_name('budoka gardener', 'Budoka Gardener').
card_type('budoka gardener', 'Creature — Human Monk').
card_types('budoka gardener', ['Creature']).
card_subtypes('budoka gardener', ['Human', 'Monk']).
card_colors('budoka gardener', ['G']).
card_text('budoka gardener', '{T}: You may put a land card from your hand onto the battlefield. If you control ten or more lands, flip Budoka Gardener.').
card_mana_cost('budoka gardener', ['1', 'G']).
card_cmc('budoka gardener', 2).
card_layout('budoka gardener', 'flip').
card_power('budoka gardener', 2).
card_toughness('budoka gardener', 1).
card_sides('budoka gardener', 'dokai, weaver of life').

% Found in: BOK, pREL
card_name('budoka pupil', 'Budoka Pupil').
card_type('budoka pupil', 'Creature — Human Monk').
card_types('budoka pupil', ['Creature']).
card_subtypes('budoka pupil', ['Human', 'Monk']).
card_colors('budoka pupil', ['G']).
card_text('budoka pupil', 'Whenever you cast a Spirit or Arcane spell, you may put a ki counter on Budoka Pupil.\nAt the beginning of the end step, if there are two or more ki counters on Budoka Pupil, you may flip it.').
card_mana_cost('budoka pupil', ['1', 'G', 'G']).
card_cmc('budoka pupil', 3).
card_layout('budoka pupil', 'flip').
card_power('budoka pupil', 2).
card_toughness('budoka pupil', 2).
card_sides('budoka pupil', 'ichiga, who topples oaks').

% Found in: MIR
card_name('builder\'s bane', 'Builder\'s Bane').
card_type('builder\'s bane', 'Sorcery').
card_types('builder\'s bane', ['Sorcery']).
card_subtypes('builder\'s bane', []).
card_colors('builder\'s bane', ['R']).
card_text('builder\'s bane', 'Destroy X target artifacts. Builder\'s Bane deals damage to each player equal to the number of artifacts he or she controlled put into a graveyard this way.').
card_mana_cost('builder\'s bane', ['X', 'X', 'R']).
card_cmc('builder\'s bane', 1).
card_layout('builder\'s bane', 'normal').

% Found in: AVR
card_name('builder\'s blessing', 'Builder\'s Blessing').
card_type('builder\'s blessing', 'Enchantment').
card_types('builder\'s blessing', ['Enchantment']).
card_subtypes('builder\'s blessing', []).
card_colors('builder\'s blessing', ['W']).
card_text('builder\'s blessing', 'Untapped creatures you control get +0/+2.').
card_mana_cost('builder\'s blessing', ['3', 'W']).
card_cmc('builder\'s blessing', 4).
card_layout('builder\'s blessing', 'normal').

% Found in: CSP
card_name('bull aurochs', 'Bull Aurochs').
card_type('bull aurochs', 'Creature — Aurochs').
card_types('bull aurochs', ['Creature']).
card_subtypes('bull aurochs', ['Aurochs']).
card_colors('bull aurochs', ['G']).
card_text('bull aurochs', 'Trample\nWhenever Bull Aurochs attacks, it gets +1/+0 until end of turn for each other attacking Aurochs.').
card_mana_cost('bull aurochs', ['1', 'G']).
card_cmc('bull aurochs', 2).
card_layout('bull aurochs', 'normal').
card_power('bull aurochs', 2).
card_toughness('bull aurochs', 1).

% Found in: ALA, HOP
card_name('bull cerodon', 'Bull Cerodon').
card_type('bull cerodon', 'Creature — Beast').
card_types('bull cerodon', ['Creature']).
card_subtypes('bull cerodon', ['Beast']).
card_colors('bull cerodon', ['W', 'R']).
card_text('bull cerodon', 'Vigilance, haste').
card_mana_cost('bull cerodon', ['4', 'R', 'W']).
card_cmc('bull cerodon', 6).
card_layout('bull cerodon', 'normal').
card_power('bull cerodon', 5).
card_toughness('bull cerodon', 5).

% Found in: MGB, VIS
card_name('bull elephant', 'Bull Elephant').
card_type('bull elephant', 'Creature — Elephant').
card_types('bull elephant', ['Creature']).
card_subtypes('bull elephant', ['Elephant']).
card_colors('bull elephant', ['G']).
card_text('bull elephant', 'When Bull Elephant enters the battlefield, sacrifice it unless you return two Forests you control to their owner\'s hand.').
card_mana_cost('bull elephant', ['3', 'G']).
card_cmc('bull elephant', 4).
card_layout('bull elephant', 'normal').
card_power('bull elephant', 4).
card_toughness('bull elephant', 4).

% Found in: 7ED, POR, S99, USG, pPOD
card_name('bull hippo', 'Bull Hippo').
card_type('bull hippo', 'Creature — Hippo').
card_types('bull hippo', ['Creature']).
card_subtypes('bull hippo', ['Hippo']).
card_colors('bull hippo', ['G']).
card_text('bull hippo', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('bull hippo', ['3', 'G']).
card_cmc('bull hippo', 4).
card_layout('bull hippo', 'normal').
card_power('bull hippo', 3).
card_toughness('bull hippo', 3).

% Found in: WWK
card_name('bull rush', 'Bull Rush').
card_type('bull rush', 'Instant').
card_types('bull rush', ['Instant']).
card_subtypes('bull rush', []).
card_colors('bull rush', ['R']).
card_text('bull rush', 'Target creature gets +2/+0 until end of turn.').
card_mana_cost('bull rush', ['R']).
card_cmc('bull rush', 1).
card_layout('bull rush', 'normal').

% Found in: STH
card_name('bullwhip', 'Bullwhip').
card_type('bullwhip', 'Artifact').
card_types('bullwhip', ['Artifact']).
card_subtypes('bullwhip', []).
card_colors('bullwhip', []).
card_text('bullwhip', '{2}, {T}: Bullwhip deals 1 damage to target creature. That creature attacks this turn if able.').
card_mana_cost('bullwhip', ['4']).
card_cmc('bullwhip', 4).
card_layout('bullwhip', 'normal').

% Found in: USG
card_name('bulwark', 'Bulwark').
card_type('bulwark', 'Enchantment').
card_types('bulwark', ['Enchantment']).
card_subtypes('bulwark', []).
card_colors('bulwark', ['R']).
card_text('bulwark', 'At the beginning of your upkeep, Bulwark deals X damage to target opponent, where X is the number of cards in your hand minus the number of cards in that player\'s hand.').
card_mana_cost('bulwark', ['3', 'R', 'R']).
card_cmc('bulwark', 5).
card_layout('bulwark', 'normal').

% Found in: DDK, ISD
card_name('bump in the night', 'Bump in the Night').
card_type('bump in the night', 'Sorcery').
card_types('bump in the night', ['Sorcery']).
card_subtypes('bump in the night', []).
card_colors('bump in the night', ['B']).
card_text('bump in the night', 'Target opponent loses 3 life.\nFlashback {5}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('bump in the night', ['B']).
card_cmc('bump in the night', 1).
card_layout('bump in the night', 'normal').

% Found in: MMQ
card_name('buoyancy', 'Buoyancy').
card_type('buoyancy', 'Enchantment — Aura').
card_types('buoyancy', ['Enchantment']).
card_subtypes('buoyancy', ['Aura']).
card_colors('buoyancy', ['U']).
card_text('buoyancy', 'Flash\nEnchant creature\nEnchanted creature has flying.').
card_mana_cost('buoyancy', ['1', 'U']).
card_cmc('buoyancy', 2).
card_layout('buoyancy', 'normal').

% Found in: DST
card_name('burden of greed', 'Burden of Greed').
card_type('burden of greed', 'Instant').
card_types('burden of greed', ['Instant']).
card_subtypes('burden of greed', []).
card_colors('burden of greed', ['B']).
card_text('burden of greed', 'Target player loses 1 life for each tapped artifact he or she controls.').
card_mana_cost('burden of greed', ['3', 'B']).
card_cmc('burden of greed', 4).
card_layout('burden of greed', 'normal').

% Found in: DKA
card_name('burden of guilt', 'Burden of Guilt').
card_type('burden of guilt', 'Enchantment — Aura').
card_types('burden of guilt', ['Enchantment']).
card_subtypes('burden of guilt', ['Aura']).
card_colors('burden of guilt', ['W']).
card_text('burden of guilt', 'Enchant creature\n{1}: Tap enchanted creature.').
card_mana_cost('burden of guilt', ['W']).
card_cmc('burden of guilt', 1).
card_layout('burden of guilt', 'normal').

% Found in: UGL
card_name('bureaucracy', 'Bureaucracy').
card_type('bureaucracy', 'Enchantment').
card_types('bureaucracy', ['Enchantment']).
card_subtypes('bureaucracy', []).
card_colors('bureaucracy', ['U']).
card_text('bureaucracy', 'Pursuant to subsection 3.1(4) of Richard\'s Rules of Order, during the upkeep of each participant in this game of the Magic: The Gathering® trading card game (hereafter known as \"PLAYER\"), that PLAYER performs all actions in the sequence of previously added actions (hereafter known as \"ACTION QUEUE\"), in the order those actions were added, then adds another action to the end of the ACTION QUEUE. All actions must be simple physical or verbal actions that a player can perform while sitting in a chair, without jeopardizing the health and security of said PLAYER.\nIf any PLAYER does not perform all the prescribed actions in the correct order, sacrifice Bureaucracy and said PLAYER discards his or her complement of cards in hand (hereafter known as \"HAND\").').
card_mana_cost('bureaucracy', ['3', 'U', 'U']).
card_cmc('bureaucracy', 5).
card_layout('bureaucracy', 'normal').

% Found in: STH
card_name('burgeoning', 'Burgeoning').
card_type('burgeoning', 'Enchantment').
card_types('burgeoning', ['Enchantment']).
card_subtypes('burgeoning', []).
card_colors('burgeoning', ['G']).
card_text('burgeoning', 'Whenever an opponent plays a land, you may put a land card from your hand onto the battlefield.').
card_mana_cost('burgeoning', ['G']).
card_cmc('burgeoning', 1).
card_layout('burgeoning', 'normal').

% Found in: CMD, ODY, PD3, WTH
card_name('buried alive', 'Buried Alive').
card_type('buried alive', 'Sorcery').
card_types('buried alive', ['Sorcery']).
card_subtypes('buried alive', []).
card_colors('buried alive', ['B']).
card_text('buried alive', 'Search your library for up to three creature cards and put them into your graveyard. Then shuffle your library.').
card_mana_cost('buried alive', ['2', 'B']).
card_cmc('buried alive', 3).
card_layout('buried alive', 'normal').

% Found in: C14, M12
card_name('buried ruin', 'Buried Ruin').
card_type('buried ruin', 'Land').
card_types('buried ruin', ['Land']).
card_subtypes('buried ruin', []).
card_colors('buried ruin', []).
card_text('buried ruin', '{T}: Add {1} to your mana pool.\n{2}, {T}, Sacrifice Buried Ruin: Return target artifact card from your graveyard to your hand.').
card_layout('buried ruin', 'normal').

% Found in: DGM
card_name('burn', 'Burn').
card_type('burn', 'Instant').
card_types('burn', ['Instant']).
card_subtypes('burn', []).
card_colors('burn', ['R']).
card_text('burn', 'Burn deals 2 damage to target creature or player.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('burn', ['1', 'R']).
card_cmc('burn', 2).
card_layout('burn', 'split').

% Found in: AVR
card_name('burn at the stake', 'Burn at the Stake').
card_type('burn at the stake', 'Sorcery').
card_types('burn at the stake', ['Sorcery']).
card_subtypes('burn at the stake', []).
card_colors('burn at the stake', ['R']).
card_text('burn at the stake', 'As an additional cost to cast Burn at the Stake, tap any number of untapped creatures you control.\nBurn at the Stake deals damage to target creature or player equal to three times the number of creatures tapped this way.').
card_mana_cost('burn at the stake', ['2', 'R', 'R', 'R']).
card_cmc('burn at the stake', 5).
card_layout('burn at the stake', 'normal').

% Found in: KTK
card_name('burn away', 'Burn Away').
card_type('burn away', 'Instant').
card_types('burn away', ['Instant']).
card_subtypes('burn away', []).
card_colors('burn away', ['R']).
card_text('burn away', 'Burn Away deals 6 damage to target creature. When that creature dies this turn, exile all cards from its controller\'s graveyard.').
card_mana_cost('burn away', ['4', 'R']).
card_cmc('burn away', 5).
card_layout('burn away', 'normal').

% Found in: MBS
card_name('burn the impure', 'Burn the Impure').
card_type('burn the impure', 'Instant').
card_types('burn the impure', ['Instant']).
card_subtypes('burn the impure', []).
card_colors('burn the impure', ['R']).
card_text('burn the impure', 'Burn the Impure deals 3 damage to target creature. If that creature has infect, Burn the Impure deals 3 damage to that creature\'s controller.').
card_mana_cost('burn the impure', ['1', 'R']).
card_cmc('burn the impure', 2).
card_layout('burn the impure', 'normal').

% Found in: SHM
card_name('burn trail', 'Burn Trail').
card_type('burn trail', 'Sorcery').
card_types('burn trail', ['Sorcery']).
card_subtypes('burn trail', []).
card_colors('burn trail', ['R']).
card_text('burn trail', 'Burn Trail deals 3 damage to target creature or player.\nConspire (As you cast this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_mana_cost('burn trail', ['3', 'R']).
card_cmc('burn trail', 4).
card_layout('burn trail', 'normal').

% Found in: M15
card_name('burning anger', 'Burning Anger').
card_type('burning anger', 'Enchantment — Aura').
card_types('burning anger', ['Enchantment']).
card_subtypes('burning anger', ['Aura']).
card_colors('burning anger', ['R']).
card_text('burning anger', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals damage equal to its power to target creature or player.\"').
card_mana_cost('burning anger', ['4', 'R']).
card_cmc('burning anger', 5).
card_layout('burning anger', 'normal').

% Found in: UGL
card_name('burning cinder fury of crimson chaos fire', 'Burning Cinder Fury of Crimson Chaos Fire').
card_type('burning cinder fury of crimson chaos fire', 'Enchantment').
card_types('burning cinder fury of crimson chaos fire', ['Enchantment']).
card_subtypes('burning cinder fury of crimson chaos fire', []).
card_colors('burning cinder fury of crimson chaos fire', ['R']).
card_text('burning cinder fury of crimson chaos fire', 'Whenever any player taps a card, that player gives control of that card to an opponent at end of turn.\nIf a player does not tap any nonland cards during his or her turn, Burning Cinder Fury of Crimson Chaos Fire deals 3 damage to that player at end of turn.').
card_mana_cost('burning cinder fury of crimson chaos fire', ['3', 'R']).
card_cmc('burning cinder fury of crimson chaos fire', 4).
card_layout('burning cinder fury of crimson chaos fire', 'normal').

% Found in: POR
card_name('burning cloak', 'Burning Cloak').
card_type('burning cloak', 'Sorcery').
card_types('burning cloak', ['Sorcery']).
card_subtypes('burning cloak', []).
card_colors('burning cloak', ['R']).
card_text('burning cloak', 'Target creature gets +2/+0 until end of turn. Burning Cloak deals 2 damage to that creature.').
card_mana_cost('burning cloak', ['R']).
card_cmc('burning cloak', 1).
card_layout('burning cloak', 'normal').

% Found in: M14
card_name('burning earth', 'Burning Earth').
card_type('burning earth', 'Enchantment').
card_types('burning earth', ['Enchantment']).
card_subtypes('burning earth', []).
card_colors('burning earth', ['R']).
card_text('burning earth', 'Whenever a player taps a nonbasic land for mana, Burning Earth deals 1 damage to that player.').
card_mana_cost('burning earth', ['3', 'R']).
card_cmc('burning earth', 4).
card_layout('burning earth', 'normal').

% Found in: PTK
card_name('burning fields', 'Burning Fields').
card_type('burning fields', 'Sorcery').
card_types('burning fields', ['Sorcery']).
card_subtypes('burning fields', []).
card_colors('burning fields', ['R']).
card_text('burning fields', 'Burning Fields deals 5 damage to target opponent.').
card_mana_cost('burning fields', ['4', 'R']).
card_cmc('burning fields', 5).
card_layout('burning fields', 'normal').

% Found in: M10
card_name('burning inquiry', 'Burning Inquiry').
card_type('burning inquiry', 'Sorcery').
card_types('burning inquiry', ['Sorcery']).
card_subtypes('burning inquiry', []).
card_colors('burning inquiry', ['R']).
card_text('burning inquiry', 'Each player draws three cards, then discards three cards at random.').
card_mana_cost('burning inquiry', ['R']).
card_cmc('burning inquiry', 1).
card_layout('burning inquiry', 'normal').

% Found in: ME3, PTK, V14, VMA
card_name('burning of xinye', 'Burning of Xinye').
card_type('burning of xinye', 'Sorcery').
card_types('burning of xinye', ['Sorcery']).
card_subtypes('burning of xinye', []).
card_colors('burning of xinye', ['R']).
card_text('burning of xinye', 'You destroy four lands you control, then target opponent destroys four lands he or she controls. Then Burning of Xinye deals 4 damage to each creature.').
card_mana_cost('burning of xinye', ['4', 'R', 'R']).
card_cmc('burning of xinye', 6).
card_layout('burning of xinye', 'normal').

% Found in: DKA
card_name('burning oil', 'Burning Oil').
card_type('burning oil', 'Instant').
card_types('burning oil', ['Instant']).
card_subtypes('burning oil', []).
card_colors('burning oil', ['R']).
card_text('burning oil', 'Burning Oil deals 3 damage to target attacking or blocking creature.\nFlashback {3}{W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('burning oil', ['1', 'R']).
card_cmc('burning oil', 2).
card_layout('burning oil', 'normal').

% Found in: MIR
card_name('burning palm efreet', 'Burning Palm Efreet').
card_type('burning palm efreet', 'Creature — Efreet').
card_types('burning palm efreet', ['Creature']).
card_subtypes('burning palm efreet', ['Efreet']).
card_colors('burning palm efreet', ['R']).
card_text('burning palm efreet', '{1}{R}{R}: Burning Palm Efreet deals 2 damage to target creature with flying and that creature loses flying until end of turn.').
card_mana_cost('burning palm efreet', ['2', 'R', 'R']).
card_cmc('burning palm efreet', 4).
card_layout('burning palm efreet', 'normal').
card_power('burning palm efreet', 2).
card_toughness('burning palm efreet', 2).

% Found in: ODY
card_name('burning sands', 'Burning Sands').
card_type('burning sands', 'Enchantment').
card_types('burning sands', ['Enchantment']).
card_subtypes('burning sands', []).
card_colors('burning sands', ['R']).
card_text('burning sands', 'Whenever a creature dies, that creature\'s controller sacrifices a land.').
card_mana_cost('burning sands', ['3', 'R', 'R']).
card_cmc('burning sands', 5).
card_layout('burning sands', 'normal').

% Found in: MIR
card_name('burning shield askari', 'Burning Shield Askari').
card_type('burning shield askari', 'Creature — Human Knight').
card_types('burning shield askari', ['Creature']).
card_subtypes('burning shield askari', ['Human', 'Knight']).
card_colors('burning shield askari', ['R']).
card_text('burning shield askari', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{R}{R}: Burning Shield Askari gains first strike until end of turn.').
card_mana_cost('burning shield askari', ['2', 'R']).
card_cmc('burning shield askari', 3).
card_layout('burning shield askari', 'normal').
card_power('burning shield askari', 2).
card_toughness('burning shield askari', 2).

% Found in: ISD
card_name('burning vengeance', 'Burning Vengeance').
card_type('burning vengeance', 'Enchantment').
card_types('burning vengeance', ['Enchantment']).
card_subtypes('burning vengeance', []).
card_colors('burning vengeance', ['R']).
card_text('burning vengeance', 'Whenever you cast a spell from your graveyard, Burning Vengeance deals 2 damage to target creature or player.').
card_mana_cost('burning vengeance', ['2', 'R']).
card_cmc('burning vengeance', 3).
card_layout('burning vengeance', 'normal').

% Found in: JUD, VMA, pJGP
card_name('burning wish', 'Burning Wish').
card_type('burning wish', 'Sorcery').
card_types('burning wish', ['Sorcery']).
card_subtypes('burning wish', []).
card_colors('burning wish', ['R']).
card_text('burning wish', 'You may choose a sorcery card you own from outside the game, reveal that card, and put it into your hand. Exile Burning Wish.').
card_mana_cost('burning wish', ['1', 'R']).
card_cmc('burning wish', 2).
card_layout('burning wish', 'normal').

% Found in: SOK
card_name('burning-eye zubera', 'Burning-Eye Zubera').
card_type('burning-eye zubera', 'Creature — Zubera Spirit').
card_types('burning-eye zubera', ['Creature']).
card_subtypes('burning-eye zubera', ['Zubera', 'Spirit']).
card_colors('burning-eye zubera', ['R']).
card_text('burning-eye zubera', 'When Burning-Eye Zubera dies, if 4 or more damage was dealt to it this turn, Burning-Eye Zubera deals 3 damage to target creature or player.').
card_mana_cost('burning-eye zubera', ['2', 'R', 'R']).
card_cmc('burning-eye zubera', 4).
card_layout('burning-eye zubera', 'normal').
card_power('burning-eye zubera', 3).
card_toughness('burning-eye zubera', 3).

% Found in: GPT
card_name('burning-tree bloodscale', 'Burning-Tree Bloodscale').
card_type('burning-tree bloodscale', 'Creature — Viashino Berserker').
card_types('burning-tree bloodscale', ['Creature']).
card_subtypes('burning-tree bloodscale', ['Viashino', 'Berserker']).
card_colors('burning-tree bloodscale', ['R', 'G']).
card_text('burning-tree bloodscale', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)\n{2}{R}: Target creature can\'t block Burning-Tree Bloodscale this turn.\n{2}{G}: Target creature blocks Burning-Tree Bloodscale this turn if able.').
card_mana_cost('burning-tree bloodscale', ['2', 'R', 'G']).
card_cmc('burning-tree bloodscale', 4).
card_layout('burning-tree bloodscale', 'normal').
card_power('burning-tree bloodscale', 2).
card_toughness('burning-tree bloodscale', 2).

% Found in: GTC
card_name('burning-tree emissary', 'Burning-Tree Emissary').
card_type('burning-tree emissary', 'Creature — Human Shaman').
card_types('burning-tree emissary', ['Creature']).
card_subtypes('burning-tree emissary', ['Human', 'Shaman']).
card_colors('burning-tree emissary', ['R', 'G']).
card_text('burning-tree emissary', 'When Burning-Tree Emissary enters the battlefield, add {R}{G} to your mana pool.').
card_mana_cost('burning-tree emissary', ['R/G', 'R/G']).
card_cmc('burning-tree emissary', 2).
card_layout('burning-tree emissary', 'normal').
card_power('burning-tree emissary', 2).
card_toughness('burning-tree emissary', 2).

% Found in: GPT
card_name('burning-tree shaman', 'Burning-Tree Shaman').
card_type('burning-tree shaman', 'Creature — Centaur Shaman').
card_types('burning-tree shaman', ['Creature']).
card_subtypes('burning-tree shaman', ['Centaur', 'Shaman']).
card_colors('burning-tree shaman', ['R', 'G']).
card_text('burning-tree shaman', 'Whenever a player activates an ability that isn\'t a mana ability, Burning-Tree Shaman deals 1 damage to that player.').
card_mana_cost('burning-tree shaman', ['1', 'R', 'G']).
card_cmc('burning-tree shaman', 3).
card_layout('burning-tree shaman', 'normal').
card_power('burning-tree shaman', 3).
card_toughness('burning-tree shaman', 4).

% Found in: C14, THS
card_name('burnished hart', 'Burnished Hart').
card_type('burnished hart', 'Artifact Creature — Elk').
card_types('burnished hart', ['Artifact', 'Creature']).
card_subtypes('burnished hart', ['Elk']).
card_colors('burnished hart', []).
card_text('burnished hart', '{3}, Sacrifice Burnished Hart: Search your library for up to two basic land cards, put them onto the battlefield tapped, then shuffle your library.').
card_mana_cost('burnished hart', ['3']).
card_cmc('burnished hart', 3).
card_layout('burnished hart', 'normal').
card_power('burnished hart', 2).
card_toughness('burnished hart', 2).

% Found in: ALL, ME2
card_name('burnout', 'Burnout').
card_type('burnout', 'Instant').
card_types('burnout', ['Instant']).
card_subtypes('burnout', []).
card_colors('burnout', ['R']).
card_text('burnout', 'Counter target instant spell if it\'s blue.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('burnout', ['1', 'R']).
card_cmc('burnout', 2).
card_layout('burnout', 'normal').

% Found in: ICE
card_name('burnt offering', 'Burnt Offering').
card_type('burnt offering', 'Instant').
card_types('burnt offering', ['Instant']).
card_subtypes('burnt offering', []).
card_colors('burnt offering', ['B']).
card_text('burnt offering', 'As an additional cost to cast Burnt Offering, sacrifice a creature.\nAdd X mana in any combination of {B} and/or {R} to your mana pool, where X is the sacrificed creature\'s converted mana cost.').
card_mana_cost('burnt offering', ['B']).
card_cmc('burnt offering', 1).
card_layout('burnt offering', 'normal').

% Found in: CHK
card_name('burr grafter', 'Burr Grafter').
card_type('burr grafter', 'Creature — Spirit').
card_types('burr grafter', ['Creature']).
card_subtypes('burr grafter', ['Spirit']).
card_colors('burr grafter', ['G']).
card_text('burr grafter', 'Sacrifice Burr Grafter: Target creature gets +2/+2 until end of turn.\nSoulshift 3 (When this creature dies, you may return target Spirit card with converted mana cost 3 or less from your graveyard to your hand.)').
card_mana_cost('burr grafter', ['3', 'G']).
card_cmc('burr grafter', 4).
card_layout('burr grafter', 'normal').
card_power('burr grafter', 2).
card_toughness('burr grafter', 2).

% Found in: DDF, MOR
card_name('burrenton bombardier', 'Burrenton Bombardier').
card_type('burrenton bombardier', 'Creature — Kithkin Soldier').
card_types('burrenton bombardier', ['Creature']).
card_subtypes('burrenton bombardier', ['Kithkin', 'Soldier']).
card_colors('burrenton bombardier', ['W']).
card_text('burrenton bombardier', 'Flying\nReinforce 2—{2}{W} ({2}{W}, Discard this card: Put two +1/+1 counters on target creature.)').
card_mana_cost('burrenton bombardier', ['2', 'W']).
card_cmc('burrenton bombardier', 3).
card_layout('burrenton bombardier', 'normal').
card_power('burrenton bombardier', 2).
card_toughness('burrenton bombardier', 2).

% Found in: LRW, MD1
card_name('burrenton forge-tender', 'Burrenton Forge-Tender').
card_type('burrenton forge-tender', 'Creature — Kithkin Wizard').
card_types('burrenton forge-tender', ['Creature']).
card_subtypes('burrenton forge-tender', ['Kithkin', 'Wizard']).
card_colors('burrenton forge-tender', ['W']).
card_text('burrenton forge-tender', 'Protection from red\nSacrifice Burrenton Forge-Tender: Prevent all damage a red source of your choice would deal this turn.').
card_mana_cost('burrenton forge-tender', ['W']).
card_cmc('burrenton forge-tender', 1).
card_layout('burrenton forge-tender', 'normal').
card_power('burrenton forge-tender', 1).
card_toughness('burrenton forge-tender', 1).

% Found in: MOR
card_name('burrenton shield-bearers', 'Burrenton Shield-Bearers').
card_type('burrenton shield-bearers', 'Creature — Kithkin Soldier').
card_types('burrenton shield-bearers', ['Creature']).
card_subtypes('burrenton shield-bearers', ['Kithkin', 'Soldier']).
card_colors('burrenton shield-bearers', ['W']).
card_text('burrenton shield-bearers', 'Whenever Burrenton Shield-Bearers attacks, target creature gets +0/+3 until end of turn.').
card_mana_cost('burrenton shield-bearers', ['4', 'W']).
card_cmc('burrenton shield-bearers', 5).
card_layout('burrenton shield-bearers', 'normal').
card_power('burrenton shield-bearers', 3).
card_toughness('burrenton shield-bearers', 3).

% Found in: 2ED, 3ED, 4ED, 6ED, CED, CEI, LEA, LEB
card_name('burrowing', 'Burrowing').
card_type('burrowing', 'Enchantment — Aura').
card_types('burrowing', ['Enchantment']).
card_subtypes('burrowing', ['Aura']).
card_colors('burrowing', ['R']).
card_text('burrowing', 'Enchant creature\nEnchanted creature has mountainwalk. (It can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('burrowing', ['R']).
card_cmc('burrowing', 1).
card_layout('burrowing', 'normal').

% Found in: MM2, ZEN, pMPR
card_name('burst lightning', 'Burst Lightning').
card_type('burst lightning', 'Instant').
card_types('burst lightning', ['Instant']).
card_subtypes('burst lightning', []).
card_colors('burst lightning', ['R']).
card_text('burst lightning', 'Kicker {4} (You may pay an additional {4} as you cast this spell.)\nBurst Lightning deals 2 damage to target creature or player. If Burst Lightning was kicked, it deals 4 damage to that creature or player instead.').
card_mana_cost('burst lightning', ['R']).
card_cmc('burst lightning', 1).
card_layout('burst lightning', 'normal').

% Found in: ULG
card_name('burst of energy', 'Burst of Energy').
card_type('burst of energy', 'Instant').
card_types('burst of energy', ['Instant']).
card_subtypes('burst of energy', []).
card_colors('burst of energy', ['W']).
card_text('burst of energy', 'Untap target permanent.').
card_mana_cost('burst of energy', ['W']).
card_cmc('burst of energy', 1).
card_layout('burst of energy', 'normal').

% Found in: M10
card_name('burst of speed', 'Burst of Speed').
card_type('burst of speed', 'Sorcery').
card_types('burst of speed', ['Sorcery']).
card_subtypes('burst of speed', []).
card_colors('burst of speed', ['R']).
card_text('burst of speed', 'Creatures you control gain haste until end of turn. (They can attack and {T} even if they just came under your control.)').
card_mana_cost('burst of speed', ['R']).
card_cmc('burst of speed', 1).
card_layout('burst of speed', 'normal').

% Found in: GTC
card_name('burst of strength', 'Burst of Strength').
card_type('burst of strength', 'Instant').
card_types('burst of strength', ['Instant']).
card_subtypes('burst of strength', []).
card_colors('burst of strength', ['G']).
card_text('burst of strength', 'Put a +1/+1 counter on target creature and untap it.').
card_mana_cost('burst of strength', ['G']).
card_cmc('burst of strength', 1).
card_layout('burst of strength', 'normal').

% Found in: UNH
card_name('bursting beebles', 'Bursting Beebles').
card_type('bursting beebles', 'Creature — Beeble').
card_types('bursting beebles', ['Creature']).
card_subtypes('bursting beebles', ['Beeble']).
card_colors('bursting beebles', ['U']).
card_text('bursting beebles', 'Bursting Beebles is unblockable as long as defending player controls two or more nonland permanents that share an artist.').
card_mana_cost('bursting beebles', ['2', 'U']).
card_cmc('bursting beebles', 3).
card_layout('bursting beebles', 'normal').
card_power('bursting beebles', 2).
card_toughness('bursting beebles', 2).

% Found in: CHK
card_name('bushi tenderfoot', 'Bushi Tenderfoot').
card_type('bushi tenderfoot', 'Creature — Human Soldier').
card_types('bushi tenderfoot', ['Creature']).
card_subtypes('bushi tenderfoot', ['Human', 'Soldier']).
card_colors('bushi tenderfoot', ['W']).
card_text('bushi tenderfoot', 'When a creature dealt damage by Bushi Tenderfoot this turn dies, flip Bushi Tenderfoot.').
card_mana_cost('bushi tenderfoot', ['W']).
card_cmc('bushi tenderfoot', 1).
card_layout('bushi tenderfoot', 'flip').
card_power('bushi tenderfoot', 1).
card_toughness('bushi tenderfoot', 1).
card_sides('bushi tenderfoot', 'kenzo the hardhearted').

% Found in: PLC
card_name('bust', 'Bust').
card_type('bust', 'Sorcery').
card_types('bust', ['Sorcery']).
card_subtypes('bust', []).
card_colors('bust', ['R']).
card_text('bust', 'Destroy all lands.').
card_mana_cost('bust', ['5', 'R']).
card_cmc('bust', 6).
card_layout('bust', 'split').

% Found in: AVR
card_name('butcher ghoul', 'Butcher Ghoul').
card_type('butcher ghoul', 'Creature — Zombie').
card_types('butcher ghoul', ['Creature']).
card_subtypes('butcher ghoul', ['Zombie']).
card_colors('butcher ghoul', ['B']).
card_text('butcher ghoul', 'Undying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('butcher ghoul', ['1', 'B']).
card_cmc('butcher ghoul', 2).
card_layout('butcher ghoul', 'normal').
card_power('butcher ghoul', 1).
card_toughness('butcher ghoul', 1).

% Found in: C14, CMD, DDK, DDP, WWK
card_name('butcher of malakir', 'Butcher of Malakir').
card_type('butcher of malakir', 'Creature — Vampire Warrior').
card_types('butcher of malakir', ['Creature']).
card_subtypes('butcher of malakir', ['Vampire', 'Warrior']).
card_colors('butcher of malakir', ['B']).
card_text('butcher of malakir', 'Flying\nWhenever Butcher of Malakir or another creature you control dies, each opponent sacrifices a creature.').
card_mana_cost('butcher of malakir', ['5', 'B', 'B']).
card_cmc('butcher of malakir', 7).
card_layout('butcher of malakir', 'normal').
card_power('butcher of malakir', 5).
card_toughness('butcher of malakir', 4).

% Found in: KTK, pPRE
card_name('butcher of the horde', 'Butcher of the Horde').
card_type('butcher of the horde', 'Creature — Demon').
card_types('butcher of the horde', ['Creature']).
card_subtypes('butcher of the horde', ['Demon']).
card_colors('butcher of the horde', ['W', 'B', 'R']).
card_text('butcher of the horde', 'Flying\nSacrifice another creature: Butcher of the Horde gains your choice of vigilance, lifelink, or haste until end of turn.').
card_mana_cost('butcher of the horde', ['1', 'R', 'W', 'B']).
card_cmc('butcher of the horde', 4).
card_layout('butcher of the horde', 'normal').
card_power('butcher of the horde', 5).
card_toughness('butcher of the horde', 4).

% Found in: ONS
card_name('butcher orgg', 'Butcher Orgg').
card_type('butcher orgg', 'Creature — Orgg').
card_types('butcher orgg', ['Creature']).
card_subtypes('butcher orgg', ['Orgg']).
card_colors('butcher orgg', ['R']).
card_text('butcher orgg', 'You may assign Butcher Orgg\'s combat damage divided as you choose among defending player and/or any number of creatures he or she controls.').
card_mana_cost('butcher orgg', ['4', 'R', 'R', 'R']).
card_cmc('butcher orgg', 7).
card_layout('butcher orgg', 'normal').
card_power('butcher orgg', 6).
card_toughness('butcher orgg', 6).

% Found in: ISD
card_name('butcher\'s cleaver', 'Butcher\'s Cleaver').
card_type('butcher\'s cleaver', 'Artifact — Equipment').
card_types('butcher\'s cleaver', ['Artifact']).
card_subtypes('butcher\'s cleaver', ['Equipment']).
card_colors('butcher\'s cleaver', []).
card_text('butcher\'s cleaver', 'Equipped creature gets +3/+0.\nAs long as equipped creature is a Human, it has lifelink.\nEquip {3}').
card_mana_cost('butcher\'s cleaver', ['3']).
card_cmc('butcher\'s cleaver', 3).
card_layout('butcher\'s cleaver', 'normal').

% Found in: DTK
card_name('butcher\'s glee', 'Butcher\'s Glee').
card_type('butcher\'s glee', 'Instant').
card_types('butcher\'s glee', ['Instant']).
card_subtypes('butcher\'s glee', []).
card_colors('butcher\'s glee', ['B']).
card_text('butcher\'s glee', 'Target creature gets +3/+0 and gains lifelink until end of turn. Regenerate it. (Damage dealt by a creature with lifelink also causes its controller to gain that much life.)').
card_mana_cost('butcher\'s glee', ['2', 'B']).
card_cmc('butcher\'s glee', 3).
card_layout('butcher\'s glee', 'normal').

% Found in: WTH
card_name('bösium strip', 'Bösium Strip').
card_type('bösium strip', 'Artifact').
card_types('bösium strip', ['Artifact']).
card_subtypes('bösium strip', []).
card_colors('bösium strip', []).
card_text('bösium strip', '{3}, {T}: Until end of turn, if the top card of your graveyard is an instant or sorcery card, you may cast that card. If a card cast this way would be put into a graveyard this turn, exile it instead.').
card_mana_cost('bösium strip', ['3']).
card_cmc('bösium strip', 3).
card_layout('bösium strip', 'normal').
card_reserved('bösium strip').

