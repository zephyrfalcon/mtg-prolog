% Card-specific data

% Found in: ARC
card_name('i bask in your silent awe', 'I Bask in Your Silent Awe').
card_type('i bask in your silent awe', 'Ongoing Scheme').
card_types('i bask in your silent awe', ['Scheme']).
card_subtypes('i bask in your silent awe', []).
card_supertypes('i bask in your silent awe', ['Ongoing']).
card_colors('i bask in your silent awe', []).
card_text('i bask in your silent awe', '(An ongoing scheme remains face up until it\'s abandoned.)\nEach opponent can\'t cast more than one spell each turn.\nAt the beginning of your upkeep, if no opponent cast a spell since your last turn ended, abandon this scheme.').
card_layout('i bask in your silent awe', 'scheme').

% Found in: ARC
card_name('i call on the ancient magics', 'I Call on the Ancient Magics').
card_type('i call on the ancient magics', 'Scheme').
card_types('i call on the ancient magics', ['Scheme']).
card_subtypes('i call on the ancient magics', []).
card_colors('i call on the ancient magics', []).
card_text('i call on the ancient magics', 'When you set this scheme in motion, each other player searches his or her library for a card, reveals it, and puts it into his or her hand. Then you search your library for two cards and put them into your hand. Each player shuffles his or her library.').
card_layout('i call on the ancient magics', 'scheme').

% Found in: ARC
card_name('i delight in your convulsions', 'I Delight in Your Convulsions').
card_type('i delight in your convulsions', 'Scheme').
card_types('i delight in your convulsions', ['Scheme']).
card_subtypes('i delight in your convulsions', []).
card_colors('i delight in your convulsions', []).
card_text('i delight in your convulsions', 'When you set this scheme in motion, each opponent loses 3 life. You gain life equal to the life lost this way.').
card_layout('i delight in your convulsions', 'scheme').

% Found in: ARC
card_name('i know all, i see all', 'I Know All, I See All').
card_type('i know all, i see all', 'Ongoing Scheme').
card_types('i know all, i see all', ['Scheme']).
card_subtypes('i know all, i see all', []).
card_supertypes('i know all, i see all', ['Ongoing']).
card_colors('i know all, i see all', []).
card_text('i know all, i see all', '(An ongoing scheme remains face up until it\'s abandoned.)\nUntap all permanents you control during each opponent\'s untap step.\nAt the beginning of each end step, if three or more cards were put into your graveyard this turn from anywhere, abandon this scheme.').
card_layout('i know all, i see all', 'scheme').

% Found in: UGL
card_name('i\'m rubber, you\'re glue', 'I\'m Rubber, You\'re Glue').
card_type('i\'m rubber, you\'re glue', 'Enchantment').
card_types('i\'m rubber, you\'re glue', ['Enchantment']).
card_subtypes('i\'m rubber, you\'re glue', []).
card_colors('i\'m rubber, you\'re glue', ['W']).
card_text('i\'m rubber, you\'re glue', 'Speak only in rhyming sentences. If you do not, sacrifice I\'m Rubber, You\'re Glue.\nSay \"I\'m rubber, you\'re glue. Everything bounces off me and sticks to you\": Target spell or ability, which targets only you, targets another player of your choice instead. (The new target must be legal.)').
card_mana_cost('i\'m rubber, you\'re glue', ['W', 'W']).
card_cmc('i\'m rubber, you\'re glue', 2).
card_layout('i\'m rubber, you\'re glue', 'normal').

% Found in: DD3_EVG, EVG, TSP
card_name('ib halfheart, goblin tactician', 'Ib Halfheart, Goblin Tactician').
card_type('ib halfheart, goblin tactician', 'Legendary Creature — Goblin Advisor').
card_types('ib halfheart, goblin tactician', ['Creature']).
card_subtypes('ib halfheart, goblin tactician', ['Goblin', 'Advisor']).
card_supertypes('ib halfheart, goblin tactician', ['Legendary']).
card_colors('ib halfheart, goblin tactician', ['R']).
card_text('ib halfheart, goblin tactician', 'Whenever another Goblin you control becomes blocked, sacrifice it. If you do, it deals 4 damage to each creature blocking it.\nSacrifice two Mountains: Put two 1/1 red Goblin creature tokens onto the battlefield.').
card_mana_cost('ib halfheart, goblin tactician', ['3', 'R']).
card_cmc('ib halfheart, goblin tactician', 4).
card_layout('ib halfheart, goblin tactician', 'normal').
card_power('ib halfheart, goblin tactician', 3).
card_toughness('ib halfheart, goblin tactician', 2).

% Found in: TSP
card_name('icatian crier', 'Icatian Crier').
card_type('icatian crier', 'Creature — Human Spellshaper').
card_types('icatian crier', ['Creature']).
card_subtypes('icatian crier', ['Human', 'Spellshaper']).
card_colors('icatian crier', ['W']).
card_text('icatian crier', '{1}{W}, {T}, Discard a card: Put two 1/1 white Citizen creature tokens onto the battlefield.').
card_mana_cost('icatian crier', ['2', 'W']).
card_cmc('icatian crier', 3).
card_layout('icatian crier', 'normal').
card_power('icatian crier', 1).
card_toughness('icatian crier', 1).

% Found in: FEM
card_name('icatian infantry', 'Icatian Infantry').
card_type('icatian infantry', 'Creature — Human Soldier').
card_types('icatian infantry', ['Creature']).
card_subtypes('icatian infantry', ['Human', 'Soldier']).
card_colors('icatian infantry', ['W']).
card_text('icatian infantry', '{1}: Icatian Infantry gains first strike until end of turn.\n{1}: Icatian Infantry gains banding until end of turn. (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('icatian infantry', ['W']).
card_cmc('icatian infantry', 1).
card_layout('icatian infantry', 'normal').
card_power('icatian infantry', 1).
card_toughness('icatian infantry', 1).

% Found in: ATH, DDO, FEM, ME2, TSB, pGTW
card_name('icatian javelineers', 'Icatian Javelineers').
card_type('icatian javelineers', 'Creature — Human Soldier').
card_types('icatian javelineers', ['Creature']).
card_subtypes('icatian javelineers', ['Human', 'Soldier']).
card_colors('icatian javelineers', ['W']).
card_text('icatian javelineers', 'Icatian Javelineers enters the battlefield with a javelin counter on it.\n{T}, Remove a javelin counter from Icatian Javelineers: Icatian Javelineers deals 1 damage to target creature or player.').
card_mana_cost('icatian javelineers', ['W']).
card_cmc('icatian javelineers', 1).
card_layout('icatian javelineers', 'normal').
card_power('icatian javelineers', 1).
card_toughness('icatian javelineers', 1).

% Found in: FEM, MED
card_name('icatian lieutenant', 'Icatian Lieutenant').
card_type('icatian lieutenant', 'Creature — Human Soldier').
card_types('icatian lieutenant', ['Creature']).
card_subtypes('icatian lieutenant', ['Human', 'Soldier']).
card_colors('icatian lieutenant', ['W']).
card_text('icatian lieutenant', '{1}{W}: Target Soldier creature gets +1/+0 until end of turn.').
card_mana_cost('icatian lieutenant', ['W', 'W']).
card_cmc('icatian lieutenant', 2).
card_layout('icatian lieutenant', 'normal').
card_power('icatian lieutenant', 1).
card_toughness('icatian lieutenant', 2).
card_reserved('icatian lieutenant').

% Found in: FEM
card_name('icatian moneychanger', 'Icatian Moneychanger').
card_type('icatian moneychanger', 'Creature — Human').
card_types('icatian moneychanger', ['Creature']).
card_subtypes('icatian moneychanger', ['Human']).
card_colors('icatian moneychanger', ['W']).
card_text('icatian moneychanger', 'Icatian Moneychanger enters the battlefield with three credit counters on it.\nWhen Icatian Moneychanger enters the battlefield, it deals 3 damage to you.\nAt the beginning of your upkeep, put a credit counter on Icatian Moneychanger.\nSacrifice Icatian Moneychanger: You gain 1 life for each credit counter on Icatian Moneychanger. Activate this ability only during your upkeep.').
card_mana_cost('icatian moneychanger', ['W']).
card_cmc('icatian moneychanger', 1).
card_layout('icatian moneychanger', 'normal').
card_power('icatian moneychanger', 0).
card_toughness('icatian moneychanger', 2).

% Found in: 5ED, FEM, ME2
card_name('icatian phalanx', 'Icatian Phalanx').
card_type('icatian phalanx', 'Creature — Human Soldier').
card_types('icatian phalanx', ['Creature']).
card_subtypes('icatian phalanx', ['Human', 'Soldier']).
card_colors('icatian phalanx', ['W']).
card_text('icatian phalanx', 'Banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('icatian phalanx', ['4', 'W']).
card_cmc('icatian phalanx', 5).
card_layout('icatian phalanx', 'normal').
card_power('icatian phalanx', 2).
card_toughness('icatian phalanx', 4).

% Found in: 10E, DD3_DVD, DDC, FEM
card_name('icatian priest', 'Icatian Priest').
card_type('icatian priest', 'Creature — Human Cleric').
card_types('icatian priest', ['Creature']).
card_subtypes('icatian priest', ['Human', 'Cleric']).
card_colors('icatian priest', ['W']).
card_text('icatian priest', '{1}{W}{W}: Target creature gets +1/+1 until end of turn.').
card_mana_cost('icatian priest', ['W']).
card_cmc('icatian priest', 1).
card_layout('icatian priest', 'normal').
card_power('icatian priest', 1).
card_toughness('icatian priest', 1).

% Found in: 5ED, FEM, ME2
card_name('icatian scout', 'Icatian Scout').
card_type('icatian scout', 'Creature — Human Soldier Scout').
card_types('icatian scout', ['Creature']).
card_subtypes('icatian scout', ['Human', 'Soldier', 'Scout']).
card_colors('icatian scout', ['W']).
card_text('icatian scout', '{1}, {T}: Target creature gains first strike until end of turn.').
card_mana_cost('icatian scout', ['W']).
card_cmc('icatian scout', 1).
card_layout('icatian scout', 'normal').
card_power('icatian scout', 1).
card_toughness('icatian scout', 1).

% Found in: FEM
card_name('icatian skirmishers', 'Icatian Skirmishers').
card_type('icatian skirmishers', 'Creature — Human Soldier').
card_types('icatian skirmishers', ['Creature']).
card_subtypes('icatian skirmishers', ['Human', 'Soldier']).
card_colors('icatian skirmishers', ['W']).
card_text('icatian skirmishers', 'First strike; banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)\nWhenever Icatian Skirmishers attacks, all creatures banded with it gain first strike until end of turn.').
card_mana_cost('icatian skirmishers', ['3', 'W']).
card_cmc('icatian skirmishers', 4).
card_layout('icatian skirmishers', 'normal').
card_power('icatian skirmishers', 1).
card_toughness('icatian skirmishers', 1).
card_reserved('icatian skirmishers').

% Found in: 5ED, FEM
card_name('icatian store', 'Icatian Store').
card_type('icatian store', 'Land').
card_types('icatian store', ['Land']).
card_subtypes('icatian store', []).
card_colors('icatian store', []).
card_text('icatian store', 'Icatian Store enters the battlefield tapped.\nYou may choose not to untap Icatian Store during your untap step.\nAt the beginning of your upkeep, if Icatian Store is tapped, put a storage counter on it.\n{T}, Remove any number of storage counters from Icatian Store: Add {W} to your mana pool for each storage counter removed this way.').
card_layout('icatian store', 'normal').

% Found in: 5ED, 6ED, FEM, MED
card_name('icatian town', 'Icatian Town').
card_type('icatian town', 'Sorcery').
card_types('icatian town', ['Sorcery']).
card_subtypes('icatian town', []).
card_colors('icatian town', ['W']).
card_text('icatian town', 'Put four 1/1 white Citizen creature tokens onto the battlefield.').
card_mana_cost('icatian town', ['5', 'W']).
card_cmc('icatian town', 6).
card_layout('icatian town', 'normal').

% Found in: APC, CMD, DDJ, pFNM
card_name('ice', 'Ice').
card_type('ice', 'Instant').
card_types('ice', ['Instant']).
card_subtypes('ice', []).
card_colors('ice', ['U']).
card_text('ice', 'Tap target permanent.\nDraw a card.').
card_mana_cost('ice', ['1', 'U']).
card_cmc('ice', 2).
card_layout('ice', 'split').

% Found in: M10, M11, M12
card_name('ice cage', 'Ice Cage').
card_type('ice cage', 'Enchantment — Aura').
card_types('ice cage', ['Enchantment']).
card_subtypes('ice cage', ['Aura']).
card_colors('ice cage', ['U']).
card_text('ice cage', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.\nWhen enchanted creature becomes the target of a spell or ability, destroy Ice Cage.').
card_mana_cost('ice cage', ['1', 'U']).
card_cmc('ice cage', 2).
card_layout('ice cage', 'normal').

% Found in: ICE, ME4
card_name('ice cauldron', 'Ice Cauldron').
card_type('ice cauldron', 'Artifact').
card_types('ice cauldron', ['Artifact']).
card_subtypes('ice cauldron', []).
card_colors('ice cauldron', []).
card_text('ice cauldron', '{X}, {T}: Put a charge counter on Ice Cauldron and exile a nonland card from your hand. You may cast that card for as long as it remains exiled. Note the type and amount of mana spent to pay this activation cost. Activate this ability only if there are no charge counters on Ice Cauldron.\n{T}, Remove a charge counter from Ice Cauldron: Add Ice Cauldron\'s last noted type and amount of mana to your mana pool. Spend this mana only to cast the last card exiled with Ice Cauldron.').
card_mana_cost('ice cauldron', ['4']).
card_cmc('ice cauldron', 4).
card_layout('ice cauldron', 'normal').
card_reserved('ice cauldron').

% Found in: APC
card_name('ice cave', 'Ice Cave').
card_type('ice cave', 'Enchantment').
card_types('ice cave', ['Enchantment']).
card_subtypes('ice cave', []).
card_colors('ice cave', ['U']).
card_text('ice cave', 'Whenever a player casts a spell, any other player may pay that spell\'s mana cost. If a player does, counter the spell. (Mana cost includes color.)').
card_mana_cost('ice cave', ['3', 'U', 'U']).
card_cmc('ice cave', 5).
card_layout('ice cave', 'normal').

% Found in: 5ED, ICE, ME2
card_name('ice floe', 'Ice Floe').
card_type('ice floe', 'Land').
card_types('ice floe', ['Land']).
card_subtypes('ice floe', []).
card_colors('ice floe', []).
card_text('ice floe', 'You may choose not to untap Ice Floe during your untap step.\n{T}: Tap target creature without flying that\'s attacking you. It doesn\'t untap during its controller\'s untap step for as long as Ice Floe remains tapped.').
card_layout('ice floe', 'normal').

% Found in: 2ED, CED, CEI, LEA, LEB, MED
card_name('ice storm', 'Ice Storm').
card_type('ice storm', 'Sorcery').
card_types('ice storm', ['Sorcery']).
card_subtypes('ice storm', []).
card_colors('ice storm', ['G']).
card_text('ice storm', 'Destroy target land.').
card_mana_cost('ice storm', ['2', 'G']).
card_cmc('ice storm', 3).
card_layout('ice storm', 'normal').

% Found in: CST, ICE, ME2
card_name('iceberg', 'Iceberg').
card_type('iceberg', 'Enchantment').
card_types('iceberg', ['Enchantment']).
card_subtypes('iceberg', []).
card_colors('iceberg', ['U']).
card_text('iceberg', 'Iceberg enters the battlefield with X ice counters on it.\n{3}: Put an ice counter on Iceberg.\nRemove an ice counter from Iceberg: Add {1} to your mana pool.').
card_mana_cost('iceberg', ['X', 'U', 'U']).
card_cmc('iceberg', 2).
card_layout('iceberg', 'normal').

% Found in: CSP
card_name('icefall', 'Icefall').
card_type('icefall', 'Sorcery').
card_types('icefall', ['Sorcery']).
card_subtypes('icefall', []).
card_colors('icefall', ['R']).
card_text('icefall', 'Destroy target artifact or land.\nRecover {R}{R} (When a creature is put into your graveyard from the battlefield, you may pay {R}{R}. If you do, return this card from your graveyard to your hand. Otherwise, exile this card.)').
card_mana_cost('icefall', ['2', 'R', 'R']).
card_cmc('icefall', 4).
card_layout('icefall', 'normal').

% Found in: DTK
card_name('icefall regent', 'Icefall Regent').
card_type('icefall regent', 'Creature — Dragon').
card_types('icefall regent', ['Creature']).
card_subtypes('icefall regent', ['Dragon']).
card_colors('icefall regent', ['U']).
card_text('icefall regent', 'Flying\nWhen Icefall Regent enters the battlefield, tap target creature an opponent controls. That creature doesn\'t untap during its controller\'s untap step for as long as you control Icefall Regent. \nSpells your opponents cast that target Icefall Regent cost {2} more to cast.').
card_mana_cost('icefall regent', ['3', 'U', 'U']).
card_cmc('icefall regent', 5).
card_layout('icefall regent', 'normal').
card_power('icefall regent', 4).
card_toughness('icefall regent', 3).

% Found in: KTK
card_name('icefeather aven', 'Icefeather Aven').
card_type('icefeather aven', 'Creature — Bird Shaman').
card_types('icefeather aven', ['Creature']).
card_subtypes('icefeather aven', ['Bird', 'Shaman']).
card_colors('icefeather aven', ['U', 'G']).
card_text('icefeather aven', 'Flying\nMorph {1}{G}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Icefeather Aven is turned face up, you may return another target creature to its owner\'s hand.').
card_mana_cost('icefeather aven', ['G', 'U']).
card_cmc('icefeather aven', 2).
card_layout('icefeather aven', 'normal').
card_power('icefeather aven', 2).
card_toughness('icefeather aven', 2).

% Found in: ICE, ME2
card_name('icequake', 'Icequake').
card_type('icequake', 'Sorcery').
card_types('icequake', ['Sorcery']).
card_subtypes('icequake', []).
card_colors('icequake', ['B']).
card_text('icequake', 'Destroy target land. If that land was a snow land, Icequake deals 1 damage to that land\'s controller.').
card_mana_cost('icequake', ['1', 'B', 'B']).
card_cmc('icequake', 3).
card_layout('icequake', 'normal').

% Found in: BOK, pREL
card_name('ichiga, who topples oaks', 'Ichiga, Who Topples Oaks').
card_type('ichiga, who topples oaks', 'Legendary Creature — Spirit').
card_types('ichiga, who topples oaks', ['Creature']).
card_subtypes('ichiga, who topples oaks', ['Spirit']).
card_supertypes('ichiga, who topples oaks', ['Legendary']).
card_colors('ichiga, who topples oaks', ['G']).
card_text('ichiga, who topples oaks', 'Trample\nRemove a ki counter from Ichiga, Who Topples Oaks: Target creature gets +2/+2 until end of turn.').
card_mana_cost('ichiga, who topples oaks', ['1', 'G', 'G']).
card_cmc('ichiga, who topples oaks', 3).
card_layout('ichiga, who topples oaks', 'flip').
card_power('ichiga, who topples oaks', 4).
card_toughness('ichiga, who topples oaks', 3).

% Found in: LEG
card_name('ichneumon druid', 'Ichneumon Druid').
card_type('ichneumon druid', 'Creature — Human Druid').
card_types('ichneumon druid', ['Creature']).
card_subtypes('ichneumon druid', ['Human', 'Druid']).
card_colors('ichneumon druid', ['G']).
card_text('ichneumon druid', 'Whenever an opponent casts an instant spell other than the first instant spell that player casts each turn, Ichneumon Druid deals 4 damage to him or her.').
card_mana_cost('ichneumon druid', ['1', 'G', 'G']).
card_cmc('ichneumon druid', 3).
card_layout('ichneumon druid', 'normal').
card_power('ichneumon druid', 1).
card_toughness('ichneumon druid', 1).

% Found in: NPH
card_name('ichor explosion', 'Ichor Explosion').
card_type('ichor explosion', 'Sorcery').
card_types('ichor explosion', ['Sorcery']).
card_subtypes('ichor explosion', []).
card_colors('ichor explosion', ['B']).
card_text('ichor explosion', 'As an additional cost to cast Ichor Explosion, sacrifice a creature.\nAll creatures get -X/-X until end of turn, where X is the sacrificed creature\'s power.').
card_mana_cost('ichor explosion', ['5', 'B', 'B']).
card_cmc('ichor explosion', 7).
card_layout('ichor explosion', 'normal').

% Found in: SOM
card_name('ichor rats', 'Ichor Rats').
card_type('ichor rats', 'Creature — Rat').
card_types('ichor rats', ['Creature']).
card_subtypes('ichor rats', ['Rat']).
card_colors('ichor rats', ['B']).
card_text('ichor rats', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhen Ichor Rats enters the battlefield, each player gets a poison counter.').
card_mana_cost('ichor rats', ['1', 'B', 'B']).
card_cmc('ichor rats', 3).
card_layout('ichor rats', 'normal').
card_power('ichor rats', 2).
card_toughness('ichor rats', 1).

% Found in: DD3_GVL, DDD, FUT
card_name('ichor slick', 'Ichor Slick').
card_type('ichor slick', 'Sorcery').
card_types('ichor slick', ['Sorcery']).
card_subtypes('ichor slick', []).
card_colors('ichor slick', ['B']).
card_text('ichor slick', 'Target creature gets -3/-3 until end of turn.\nCycling {2} ({2}, Discard this card: Draw a card.)\nMadness {3}{B} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('ichor slick', ['2', 'B']).
card_cmc('ichor slick', 3).
card_layout('ichor slick', 'normal').

% Found in: C14, MBS
card_name('ichor wellspring', 'Ichor Wellspring').
card_type('ichor wellspring', 'Artifact').
card_types('ichor wellspring', ['Artifact']).
card_subtypes('ichor wellspring', []).
card_colors('ichor wellspring', []).
card_text('ichor wellspring', 'When Ichor Wellspring enters the battlefield or is put into a graveyard from the battlefield, draw a card.').
card_mana_cost('ichor wellspring', ['2']).
card_cmc('ichor wellspring', 2).
card_layout('ichor wellspring', 'normal').

% Found in: SOM
card_name('ichorclaw myr', 'Ichorclaw Myr').
card_type('ichorclaw myr', 'Artifact Creature — Myr').
card_types('ichorclaw myr', ['Artifact', 'Creature']).
card_subtypes('ichorclaw myr', ['Myr']).
card_colors('ichorclaw myr', []).
card_text('ichorclaw myr', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhenever Ichorclaw Myr becomes blocked, it gets +2/+2 until end of turn.').
card_mana_cost('ichorclaw myr', ['2']).
card_cmc('ichorclaw myr', 2).
card_layout('ichorclaw myr', 'normal').
card_power('ichorclaw myr', 1).
card_toughness('ichorclaw myr', 1).

% Found in: TOR, VMA
card_name('ichorid', 'Ichorid').
card_type('ichorid', 'Creature — Horror').
card_types('ichorid', ['Creature']).
card_subtypes('ichorid', ['Horror']).
card_colors('ichorid', ['B']).
card_text('ichorid', 'Haste\nAt the beginning of the end step, sacrifice Ichorid.\nAt the beginning of your upkeep, if Ichorid is in your graveyard, you may exile a black creature card other than Ichorid from your graveyard. If you do, return Ichorid to the battlefield.').
card_mana_cost('ichorid', ['3', 'B']).
card_cmc('ichorid', 4).
card_layout('ichorid', 'normal').
card_power('ichorid', 3).
card_toughness('ichorid', 1).

% Found in: KTK, pPRE
card_name('icy blast', 'Icy Blast').
card_type('icy blast', 'Instant').
card_types('icy blast', ['Instant']).
card_subtypes('icy blast', []).
card_colors('icy blast', ['U']).
card_text('icy blast', 'Tap X target creatures.\nFerocious — If you control a creature with power 4 or greater, those creatures don\'t untap during their controllers\' next untap steps.').
card_mana_cost('icy blast', ['X', 'U']).
card_cmc('icy blast', 1).
card_layout('icy blast', 'normal').

% Found in: 10E, 2ED, 9ED, CED, CEI, DDH, DKM, ICE, LEA, LEB, ME4, MRD, pFNM
card_name('icy manipulator', 'Icy Manipulator').
card_type('icy manipulator', 'Artifact').
card_types('icy manipulator', ['Artifact']).
card_subtypes('icy manipulator', []).
card_colors('icy manipulator', []).
card_text('icy manipulator', '{1}, {T}: Tap target artifact, creature, or land.').
card_mana_cost('icy manipulator', ['4']).
card_cmc('icy manipulator', 4).
card_layout('icy manipulator', 'normal').

% Found in: ICE, ME2
card_name('icy prison', 'Icy Prison').
card_type('icy prison', 'Enchantment').
card_types('icy prison', ['Enchantment']).
card_subtypes('icy prison', []).
card_colors('icy prison', ['U']).
card_text('icy prison', 'When Icy Prison enters the battlefield, exile target creature.\nAt the beginning of your upkeep, sacrifice Icy Prison unless any player pays {3}.\nWhen Icy Prison leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_mana_cost('icy prison', ['U', 'U']).
card_cmc('icy prison', 2).
card_layout('icy prison', 'normal').

% Found in: SOK
card_name('ideas unbound', 'Ideas Unbound').
card_type('ideas unbound', 'Sorcery — Arcane').
card_types('ideas unbound', ['Sorcery']).
card_subtypes('ideas unbound', ['Arcane']).
card_colors('ideas unbound', ['U']).
card_text('ideas unbound', 'Draw three cards. Discard three cards at the beginning of the next end step.').
card_mana_cost('ideas unbound', ['U', 'U']).
card_cmc('ideas unbound', 2).
card_layout('ideas unbound', 'normal').

% Found in: ARB
card_name('identity crisis', 'Identity Crisis').
card_type('identity crisis', 'Sorcery').
card_types('identity crisis', ['Sorcery']).
card_subtypes('identity crisis', []).
card_colors('identity crisis', ['W', 'B']).
card_text('identity crisis', 'Exile all cards from target player\'s hand and graveyard.').
card_mana_cost('identity crisis', ['2', 'W', 'W', 'B', 'B']).
card_cmc('identity crisis', 6).
card_layout('identity crisis', 'normal').

% Found in: EVE
card_name('idle thoughts', 'Idle Thoughts').
card_type('idle thoughts', 'Enchantment').
card_types('idle thoughts', ['Enchantment']).
card_subtypes('idle thoughts', []).
card_colors('idle thoughts', ['U']).
card_text('idle thoughts', '{2}: Draw a card if you have no cards in hand.').
card_mana_cost('idle thoughts', ['3', 'U']).
card_cmc('idle thoughts', 4).
card_layout('idle thoughts', 'normal').

% Found in: MOR
card_name('idyllic tutor', 'Idyllic Tutor').
card_type('idyllic tutor', 'Sorcery').
card_types('idyllic tutor', ['Sorcery']).
card_subtypes('idyllic tutor', []).
card_colors('idyllic tutor', ['W']).
card_text('idyllic tutor', 'Search your library for an enchantment card, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('idyllic tutor', ['2', 'W']).
card_cmc('idyllic tutor', 3).
card_layout('idyllic tutor', 'normal').

% Found in: ARN, MED
card_name('ifh-bíff efreet', 'Ifh-Bíff Efreet').
card_type('ifh-bíff efreet', 'Creature — Efreet').
card_types('ifh-bíff efreet', ['Creature']).
card_subtypes('ifh-bíff efreet', ['Efreet']).
card_colors('ifh-bíff efreet', ['G']).
card_text('ifh-bíff efreet', 'Flying\n{G}: Ifh-Bíff Efreet deals 1 damage to each creature with flying and each player. Any player may activate this ability.').
card_mana_cost('ifh-bíff efreet', ['2', 'G', 'G']).
card_cmc('ifh-bíff efreet', 4).
card_layout('ifh-bíff efreet', 'normal').
card_power('ifh-bíff efreet', 3).
card_toughness('ifh-bíff efreet', 3).
card_reserved('ifh-bíff efreet').

% Found in: MIR
card_name('igneous golem', 'Igneous Golem').
card_type('igneous golem', 'Artifact Creature — Golem').
card_types('igneous golem', ['Artifact', 'Creature']).
card_subtypes('igneous golem', ['Golem']).
card_colors('igneous golem', []).
card_text('igneous golem', '{2}: Igneous Golem gains trample until end of turn.').
card_mana_cost('igneous golem', ['5']).
card_cmc('igneous golem', 5).
card_layout('igneous golem', 'normal').
card_power('igneous golem', 3).
card_toughness('igneous golem', 4).

% Found in: ARB, DDH
card_name('igneous pouncer', 'Igneous Pouncer').
card_type('igneous pouncer', 'Creature — Elemental').
card_types('igneous pouncer', ['Creature']).
card_subtypes('igneous pouncer', ['Elemental']).
card_colors('igneous pouncer', ['B', 'R']).
card_text('igneous pouncer', 'Haste\nSwampcycling {2}, mountaincycling {2} ({2}, Discard this card: Search your library for a Swamp or Mountain card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('igneous pouncer', ['4', 'B', 'R']).
card_cmc('igneous pouncer', 6).
card_layout('igneous pouncer', 'normal').
card_power('igneous pouncer', 5).
card_toughness('igneous pouncer', 1).

% Found in: CON, M10
card_name('ignite disorder', 'Ignite Disorder').
card_type('ignite disorder', 'Instant').
card_types('ignite disorder', ['Instant']).
card_subtypes('ignite disorder', []).
card_colors('ignite disorder', ['R']).
card_text('ignite disorder', 'Ignite Disorder deals 3 damage divided as you choose among one, two, or three target white and/or blue creatures.').
card_mana_cost('ignite disorder', ['1', 'R']).
card_cmc('ignite disorder', 2).
card_layout('ignite disorder', 'normal').

% Found in: TSP
card_name('ignite memories', 'Ignite Memories').
card_type('ignite memories', 'Sorcery').
card_types('ignite memories', ['Sorcery']).
card_subtypes('ignite memories', []).
card_colors('ignite memories', ['R']).
card_text('ignite memories', 'Target player reveals a card at random from his or her hand. Ignite Memories deals damage to that player equal to that card\'s converted mana cost.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_mana_cost('ignite memories', ['4', 'R']).
card_cmc('ignite memories', 5).
card_layout('ignite memories', 'normal').

% Found in: ARC
card_name('ignite the cloneforge!', 'Ignite the Cloneforge!').
card_type('ignite the cloneforge!', 'Scheme').
card_types('ignite the cloneforge!', ['Scheme']).
card_subtypes('ignite the cloneforge!', []).
card_colors('ignite the cloneforge!', []).
card_text('ignite the cloneforge!', 'When you set this scheme in motion, put a token onto the battlefield that\'s a copy of target permanent an opponent controls.').
card_layout('ignite the cloneforge!', 'scheme').

% Found in: CNS
card_name('ignition team', 'Ignition Team').
card_type('ignition team', 'Creature — Goblin Warrior').
card_types('ignition team', ['Creature']).
card_subtypes('ignition team', ['Goblin', 'Warrior']).
card_colors('ignition team', ['R']).
card_text('ignition team', 'Ignition Team enters the battlefield with X +1/+1 counters on it, where X is the number of tapped lands on the battlefield.\n{2}{R}, Remove a +1/+1 counter from Ignition Team: Target land becomes a 4/4 red Elemental creature until end of turn. It\'s still a land.').
card_mana_cost('ignition team', ['5', 'R', 'R']).
card_cmc('ignition team', 7).
card_layout('ignition team', 'normal').
card_power('ignition team', 0).
card_toughness('ignition team', 0).

% Found in: MMQ
card_name('ignoble soldier', 'Ignoble Soldier').
card_type('ignoble soldier', 'Creature — Human Soldier').
card_types('ignoble soldier', ['Creature']).
card_subtypes('ignoble soldier', ['Human', 'Soldier']).
card_colors('ignoble soldier', ['W']).
card_text('ignoble soldier', 'Whenever Ignoble Soldier becomes blocked, prevent all combat damage that would be dealt by it this turn.').
card_mana_cost('ignoble soldier', ['2', 'W']).
card_cmc('ignoble soldier', 3).
card_layout('ignoble soldier', 'normal').
card_power('ignoble soldier', 3).
card_toughness('ignoble soldier', 1).

% Found in: DIS
card_name('ignorant bliss', 'Ignorant Bliss').
card_type('ignorant bliss', 'Instant').
card_types('ignorant bliss', ['Instant']).
card_subtypes('ignorant bliss', []).
card_colors('ignorant bliss', ['R']).
card_text('ignorant bliss', 'Exile all cards from your hand face down. At the beginning of the next end step, return those cards to your hand, then draw a card.').
card_mana_cost('ignorant bliss', ['1', 'R']).
card_cmc('ignorant bliss', 2).
card_layout('ignorant bliss', 'normal').

% Found in: ATH, HML, ME2
card_name('ihsan\'s shade', 'Ihsan\'s Shade').
card_type('ihsan\'s shade', 'Legendary Creature — Shade Knight').
card_types('ihsan\'s shade', ['Creature']).
card_subtypes('ihsan\'s shade', ['Shade', 'Knight']).
card_supertypes('ihsan\'s shade', ['Legendary']).
card_colors('ihsan\'s shade', ['B']).
card_text('ihsan\'s shade', 'Protection from white').
card_mana_cost('ihsan\'s shade', ['3', 'B', 'B', 'B']).
card_cmc('ihsan\'s shade', 6).
card_layout('ihsan\'s shade', 'normal').
card_power('ihsan\'s shade', 5).
card_toughness('ihsan\'s shade', 5).

% Found in: SOK
card_name('iizuka the ruthless', 'Iizuka the Ruthless').
card_type('iizuka the ruthless', 'Legendary Creature — Human Samurai').
card_types('iizuka the ruthless', ['Creature']).
card_subtypes('iizuka the ruthless', ['Human', 'Samurai']).
card_supertypes('iizuka the ruthless', ['Legendary']).
card_colors('iizuka the ruthless', ['R']).
card_text('iizuka the ruthless', 'Bushido 2 (Whenever this creature blocks or becomes blocked, it gets +2/+2 until end of turn.)\n{2}{R}, Sacrifice a Samurai: Samurai creatures you control gain double strike until end of turn.').
card_mana_cost('iizuka the ruthless', ['3', 'R', 'R']).
card_cmc('iizuka the ruthless', 5).
card_layout('iizuka the ruthless', 'normal').
card_power('iizuka the ruthless', 3).
card_toughness('iizuka the ruthless', 3).

% Found in: ROE
card_name('ikiral outrider', 'Ikiral Outrider').
card_type('ikiral outrider', 'Creature — Human Soldier').
card_types('ikiral outrider', ['Creature']).
card_subtypes('ikiral outrider', ['Human', 'Soldier']).
card_colors('ikiral outrider', ['W']).
card_text('ikiral outrider', 'Level up {4} ({4}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-3\n2/6\nVigilance\nLEVEL 4+\n3/10\nVigilance').
card_mana_cost('ikiral outrider', ['1', 'W']).
card_cmc('ikiral outrider', 2).
card_layout('ikiral outrider', 'leveler').
card_power('ikiral outrider', 1).
card_toughness('ikiral outrider', 2).

% Found in: CNS, USG
card_name('ill-gotten gains', 'Ill-Gotten Gains').
card_type('ill-gotten gains', 'Sorcery').
card_types('ill-gotten gains', ['Sorcery']).
card_subtypes('ill-gotten gains', []).
card_colors('ill-gotten gains', ['B']).
card_text('ill-gotten gains', 'Exile Ill-Gotten Gains. Each player discards his or her hand, then returns up to three cards from his or her graveyard to his or her hand.').
card_mana_cost('ill-gotten gains', ['2', 'B', 'B']).
card_cmc('ill-gotten gains', 4).
card_layout('ill-gotten gains', 'normal').

% Found in: THS
card_name('ill-tempered cyclops', 'Ill-Tempered Cyclops').
card_type('ill-tempered cyclops', 'Creature — Cyclops').
card_types('ill-tempered cyclops', ['Creature']).
card_subtypes('ill-tempered cyclops', ['Cyclops']).
card_colors('ill-tempered cyclops', ['R']).
card_text('ill-tempered cyclops', 'Trample\n{5}{R}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)').
card_mana_cost('ill-tempered cyclops', ['3', 'R']).
card_cmc('ill-tempered cyclops', 4).
card_layout('ill-tempered cyclops', 'normal').
card_power('ill-tempered cyclops', 3).
card_toughness('ill-tempered cyclops', 3).

% Found in: 6ED, MIR
card_name('illicit auction', 'Illicit Auction').
card_type('illicit auction', 'Sorcery').
card_types('illicit auction', ['Sorcery']).
card_subtypes('illicit auction', []).
card_colors('illicit auction', ['R']).
card_text('illicit auction', 'Each player may bid life for control of target creature. You start the bidding with a bid of 0. In turn order, each player may top the high bid. The bidding ends if the high bid stands. The high bidder loses life equal to the high bid and gains control of the creature. (This effect lasts indefinitely.)').
card_mana_cost('illicit auction', ['3', 'R', 'R']).
card_cmc('illicit auction', 5).
card_layout('illicit auction', 'normal').

% Found in: GTC
card_name('illness in the ranks', 'Illness in the Ranks').
card_type('illness in the ranks', 'Enchantment').
card_types('illness in the ranks', ['Enchantment']).
card_subtypes('illness in the ranks', []).
card_colors('illness in the ranks', ['B']).
card_text('illness in the ranks', 'Creature tokens get -1/-1.').
card_mana_cost('illness in the ranks', ['B']).
card_cmc('illness in the ranks', 1).
card_layout('illness in the ranks', 'normal').

% Found in: APC
card_name('illuminate', 'Illuminate').
card_type('illuminate', 'Sorcery').
card_types('illuminate', ['Sorcery']).
card_subtypes('illuminate', []).
card_colors('illuminate', ['R']).
card_text('illuminate', 'Kicker {2}{R} and/or {3}{U} (You may pay an additional {2}{R} and/or {3}{U} as you cast this spell.)\nIlluminate deals X damage to target creature. If Illuminate was kicked with its {2}{R} kicker, it deals X damage to that creature\'s controller. If Illuminate was kicked with its {3}{U} kicker, you draw X cards.').
card_mana_cost('illuminate', ['X', 'R']).
card_cmc('illuminate', 1).
card_layout('illuminate', 'normal').

% Found in: SHM
card_name('illuminated folio', 'Illuminated Folio').
card_type('illuminated folio', 'Artifact').
card_types('illuminated folio', ['Artifact']).
card_subtypes('illuminated folio', []).
card_colors('illuminated folio', []).
card_text('illuminated folio', '{1}, {T}, Reveal two cards from your hand that share a color: Draw a card.').
card_mana_cost('illuminated folio', ['5']).
card_cmc('illuminated folio', 5).
card_layout('illuminated folio', 'normal').

% Found in: UDS
card_name('illuminated wings', 'Illuminated Wings').
card_type('illuminated wings', 'Enchantment — Aura').
card_types('illuminated wings', ['Enchantment']).
card_subtypes('illuminated wings', ['Aura']).
card_colors('illuminated wings', ['U']).
card_text('illuminated wings', 'Enchant creature\nEnchanted creature has flying.\n{2}, Sacrifice Illuminated Wings: Draw a card.').
card_mana_cost('illuminated wings', ['1', 'U']).
card_cmc('illuminated wings', 2).
card_layout('illuminated wings', 'normal').

% Found in: MIR
card_name('illumination', 'Illumination').
card_type('illumination', 'Instant').
card_types('illumination', ['Instant']).
card_subtypes('illumination', []).
card_colors('illumination', ['W']).
card_text('illumination', 'Counter target artifact or enchantment spell. Its controller gains life equal to its converted mana cost.').
card_mana_cost('illumination', ['W', 'W']).
card_cmc('illumination', 2).
card_layout('illumination', 'normal').

% Found in: APC
card_name('illusion', 'Illusion').
card_type('illusion', 'Instant').
card_types('illusion', ['Instant']).
card_subtypes('illusion', []).
card_colors('illusion', ['U']).
card_text('illusion', 'Target spell or permanent becomes the color of your choice until end of turn.').
card_mana_cost('illusion', ['U']).
card_cmc('illusion', 1).
card_layout('illusion', 'split').
card_sides('illusion', 'reality').

% Found in: M14
card_name('illusionary armor', 'Illusionary Armor').
card_type('illusionary armor', 'Enchantment — Aura').
card_types('illusionary armor', ['Enchantment']).
card_subtypes('illusionary armor', ['Aura']).
card_colors('illusionary armor', ['U']).
card_text('illusionary armor', 'Enchant creature\nEnchanted creature gets +4/+4.\nWhen enchanted creature becomes the target of a spell or ability, sacrifice Illusionary Armor.').
card_mana_cost('illusionary armor', ['4', 'U']).
card_cmc('illusionary armor', 5).
card_layout('illusionary armor', 'normal').

% Found in: ICE, MED
card_name('illusionary forces', 'Illusionary Forces').
card_type('illusionary forces', 'Creature — Illusion').
card_types('illusionary forces', ['Creature']).
card_subtypes('illusionary forces', ['Illusion']).
card_colors('illusionary forces', ['U']).
card_text('illusionary forces', 'Flying\nCumulative upkeep {U} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('illusionary forces', ['3', 'U']).
card_cmc('illusionary forces', 4).
card_layout('illusionary forces', 'normal').
card_power('illusionary forces', 4).
card_toughness('illusionary forces', 4).

% Found in: 2ED, CED, CEI, LEA, LEB, ME3
card_name('illusionary mask', 'Illusionary Mask').
card_type('illusionary mask', 'Artifact').
card_types('illusionary mask', ['Artifact']).
card_subtypes('illusionary mask', []).
card_colors('illusionary mask', []).
card_text('illusionary mask', '{X}: You may choose a creature card in your hand whose mana cost could be paid by some amount of, or all of, the mana you spent on {X}. If you do, you may cast that card face down as a 2/2 creature spell without paying its mana cost. If the creature that spell becomes as it resolves has not been turned face up and would assign or deal damage, be dealt damage, or become tapped, instead it\'s turned face up and assigns or deals damage, is dealt damage, or becomes tapped. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('illusionary mask', ['2']).
card_cmc('illusionary mask', 2).
card_layout('illusionary mask', 'normal').
card_reserved('illusionary mask').

% Found in: ICE
card_name('illusionary presence', 'Illusionary Presence').
card_type('illusionary presence', 'Creature — Illusion').
card_types('illusionary presence', ['Creature']).
card_subtypes('illusionary presence', ['Illusion']).
card_colors('illusionary presence', ['U']).
card_text('illusionary presence', 'Cumulative upkeep {U} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAt the beginning of your upkeep, choose a land type. Illusionary Presence gains landwalk of the chosen type until end of turn. (It can\'t be blocked as long as defending player controls a land of that type.)').
card_mana_cost('illusionary presence', ['1', 'U', 'U']).
card_cmc('illusionary presence', 3).
card_layout('illusionary presence', 'normal').
card_power('illusionary presence', 2).
card_toughness('illusionary presence', 2).
card_reserved('illusionary presence').

% Found in: M10
card_name('illusionary servant', 'Illusionary Servant').
card_type('illusionary servant', 'Creature — Illusion').
card_types('illusionary servant', ['Creature']).
card_subtypes('illusionary servant', ['Illusion']).
card_colors('illusionary servant', ['U']).
card_text('illusionary servant', 'Flying\nWhen Illusionary Servant becomes the target of a spell or ability, sacrifice it.').
card_mana_cost('illusionary servant', ['1', 'U', 'U']).
card_cmc('illusionary servant', 3).
card_layout('illusionary servant', 'normal').
card_power('illusionary servant', 3).
card_toughness('illusionary servant', 4).

% Found in: ICE
card_name('illusionary terrain', 'Illusionary Terrain').
card_type('illusionary terrain', 'Enchantment').
card_types('illusionary terrain', ['Enchantment']).
card_subtypes('illusionary terrain', []).
card_colors('illusionary terrain', ['U']).
card_text('illusionary terrain', 'Cumulative upkeep {2} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAs Illusionary Terrain enters the battlefield, choose two basic land types.\nBasic lands of the first chosen type are the second chosen type.').
card_mana_cost('illusionary terrain', ['U', 'U']).
card_cmc('illusionary terrain', 2).
card_layout('illusionary terrain', 'normal').

% Found in: ICE, MED
card_name('illusionary wall', 'Illusionary Wall').
card_type('illusionary wall', 'Creature — Illusion Wall').
card_types('illusionary wall', ['Creature']).
card_subtypes('illusionary wall', ['Illusion', 'Wall']).
card_colors('illusionary wall', ['U']).
card_text('illusionary wall', 'Defender, flying, first strike\nCumulative upkeep {U} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('illusionary wall', ['4', 'U']).
card_cmc('illusionary wall', 5).
card_layout('illusionary wall', 'normal').
card_power('illusionary wall', 7).
card_toughness('illusionary wall', 4).

% Found in: GTC
card_name('illusionist\'s bracers', 'Illusionist\'s Bracers').
card_type('illusionist\'s bracers', 'Artifact — Equipment').
card_types('illusionist\'s bracers', ['Artifact']).
card_subtypes('illusionist\'s bracers', ['Equipment']).
card_colors('illusionist\'s bracers', []).
card_text('illusionist\'s bracers', 'Whenever an ability of equipped creature is activated, if it isn\'t a mana ability, copy that ability. You may choose new targets for the copy.\nEquip {3}').
card_mana_cost('illusionist\'s bracers', ['2']).
card_cmc('illusionist\'s bracers', 2).
card_layout('illusionist\'s bracers', 'normal').

% Found in: C13
card_name('illusionist\'s gambit', 'Illusionist\'s Gambit').
card_type('illusionist\'s gambit', 'Instant').
card_types('illusionist\'s gambit', ['Instant']).
card_subtypes('illusionist\'s gambit', []).
card_colors('illusionist\'s gambit', ['U']).
card_text('illusionist\'s gambit', 'Cast Illusionist\'s Gambit only during the declare blockers step on an opponent\'s turn.\nRemove all attacking creatures from combat and untap them. After this phase, there is an additional combat phase. Each of those creatures attacks that combat if able. They can\'t attack you or a planeswalker you control that combat.').
card_mana_cost('illusionist\'s gambit', ['2', 'U', 'U']).
card_cmc('illusionist\'s gambit', 4).
card_layout('illusionist\'s gambit', 'normal').

% Found in: ICE, MED
card_name('illusions of grandeur', 'Illusions of Grandeur').
card_type('illusions of grandeur', 'Enchantment').
card_types('illusions of grandeur', ['Enchantment']).
card_subtypes('illusions of grandeur', []).
card_colors('illusions of grandeur', ['U']).
card_text('illusions of grandeur', 'Cumulative upkeep {2} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhen Illusions of Grandeur enters the battlefield, you gain 20 life.\nWhen Illusions of Grandeur leaves the battlefield, you lose 20 life.').
card_mana_cost('illusions of grandeur', ['3', 'U']).
card_cmc('illusions of grandeur', 4).
card_layout('illusions of grandeur', 'normal').
card_reserved('illusions of grandeur').

% Found in: M15, PC2
card_name('illusory angel', 'Illusory Angel').
card_type('illusory angel', 'Creature — Angel Illusion').
card_types('illusory angel', ['Creature']).
card_subtypes('illusory angel', ['Angel', 'Illusion']).
card_colors('illusory angel', ['U']).
card_text('illusory angel', 'Flying\nCast Illusory Angel only if you\'ve cast another spell this turn.').
card_mana_cost('illusory angel', ['2', 'U']).
card_cmc('illusory angel', 3).
card_layout('illusory angel', 'normal').
card_power('illusory angel', 4).
card_toughness('illusory angel', 4).

% Found in: ARB
card_name('illusory demon', 'Illusory Demon').
card_type('illusory demon', 'Creature — Demon Illusion').
card_types('illusory demon', ['Creature']).
card_subtypes('illusory demon', ['Demon', 'Illusion']).
card_colors('illusory demon', ['U', 'B']).
card_text('illusory demon', 'Flying\nWhen you cast a spell, sacrifice Illusory Demon.').
card_mana_cost('illusory demon', ['1', 'U', 'B']).
card_cmc('illusory demon', 3).
card_layout('illusory demon', 'normal').
card_power('illusory demon', 4).
card_toughness('illusory demon', 3).

% Found in: DTK
card_name('illusory gains', 'Illusory Gains').
card_type('illusory gains', 'Enchantment — Aura').
card_types('illusory gains', ['Enchantment']).
card_subtypes('illusory gains', ['Aura']).
card_colors('illusory gains', ['U']).
card_text('illusory gains', 'Enchant creature\nYou control enchanted creature.\nWhenever a creature enters the battlefield under an opponent\'s control, attach Illusory Gains to that creature.').
card_mana_cost('illusory gains', ['3', 'U', 'U']).
card_cmc('illusory gains', 5).
card_layout('illusory gains', 'normal').

% Found in: ONS
card_name('imagecrafter', 'Imagecrafter').
card_type('imagecrafter', 'Creature — Human Wizard').
card_types('imagecrafter', ['Creature']).
card_subtypes('imagecrafter', ['Human', 'Wizard']).
card_colors('imagecrafter', ['U']).
card_text('imagecrafter', '{T}: Choose a creature type other than Wall. Target creature becomes that type until end of turn.').
card_mana_cost('imagecrafter', ['U']).
card_cmc('imagecrafter', 1).
card_layout('imagecrafter', 'normal').
card_power('imagecrafter', 1).
card_toughness('imagecrafter', 1).

% Found in: 9ED, USG
card_name('imaginary pet', 'Imaginary Pet').
card_type('imaginary pet', 'Creature — Illusion').
card_types('imaginary pet', ['Creature']).
card_subtypes('imaginary pet', ['Illusion']).
card_colors('imaginary pet', ['U']).
card_text('imaginary pet', 'At the beginning of your upkeep, if you have a card in hand, return Imaginary Pet to its owner\'s hand.').
card_mana_cost('imaginary pet', ['1', 'U']).
card_cmc('imaginary pet', 2).
card_layout('imaginary pet', 'normal').
card_power('imaginary pet', 4).
card_toughness('imaginary pet', 4).

% Found in: CHK
card_name('imi statue', 'Imi Statue').
card_type('imi statue', 'Artifact').
card_types('imi statue', ['Artifact']).
card_subtypes('imi statue', []).
card_colors('imi statue', []).
card_text('imi statue', 'Players can\'t untap more than one artifact during their untap steps.').
card_mana_cost('imi statue', ['3']).
card_cmc('imi statue', 3).
card_layout('imi statue', 'normal').

% Found in: C14, DPA, LRW
card_name('immaculate magistrate', 'Immaculate Magistrate').
card_type('immaculate magistrate', 'Creature — Elf Shaman').
card_types('immaculate magistrate', ['Creature']).
card_subtypes('immaculate magistrate', ['Elf', 'Shaman']).
card_colors('immaculate magistrate', ['G']).
card_text('immaculate magistrate', '{T}: Put a +1/+1 counter on target creature for each Elf you control.').
card_mana_cost('immaculate magistrate', ['3', 'G']).
card_cmc('immaculate magistrate', 4).
card_layout('immaculate magistrate', 'normal').
card_power('immaculate magistrate', 2).
card_toughness('immaculate magistrate', 2).

% Found in: CNS
card_name('immediate action', 'Immediate Action').
card_type('immediate action', 'Conspiracy').
card_types('immediate action', ['Conspiracy']).
card_subtypes('immediate action', []).
card_colors('immediate action', []).
card_text('immediate action', 'Hidden agenda (Start the game with this conspiracy face down in the command zone and secretly name a card. You may turn this conspiracy face up any time and reveal the chosen name.)\nCreatures you control with the chosen name have haste.').
card_layout('immediate action', 'normal').

% Found in: HOP
card_name('immersturm', 'Immersturm').
card_type('immersturm', 'Plane — Valla').
card_types('immersturm', ['Plane']).
card_subtypes('immersturm', ['Valla']).
card_colors('immersturm', []).
card_text('immersturm', 'Whenever a creature enters the battlefield, that creature\'s controller may have it deal damage equal to its power to target creature or player of his or her choice.\nWhenever you roll {C}, exile target creature, then return it to the battlefield under its owner\'s control.').
card_layout('immersturm', 'plane').

% Found in: DKA
card_name('immerwolf', 'Immerwolf').
card_type('immerwolf', 'Creature — Wolf').
card_types('immerwolf', ['Creature']).
card_subtypes('immerwolf', ['Wolf']).
card_colors('immerwolf', ['R', 'G']).
card_text('immerwolf', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nEach other creature you control that\'s a Wolf or a Werewolf gets +1/+1.\nNon-Human Werewolves you control can\'t transform.').
card_mana_cost('immerwolf', ['1', 'R', 'G']).
card_cmc('immerwolf', 3).
card_layout('immerwolf', 'normal').
card_power('immerwolf', 2).
card_toughness('immerwolf', 2).

% Found in: ODY
card_name('immobilizing ink', 'Immobilizing Ink').
card_type('immobilizing ink', 'Enchantment — Aura').
card_types('immobilizing ink', ['Enchantment']).
card_subtypes('immobilizing ink', ['Aura']).
card_colors('immobilizing ink', ['U']).
card_text('immobilizing ink', 'Enchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nEnchanted creature has \"{1}, Discard a card: Untap this creature.\"').
card_mana_cost('immobilizing ink', ['1', 'U']).
card_cmc('immobilizing ink', 2).
card_layout('immobilizing ink', 'normal').

% Found in: NPH
card_name('immolating souleater', 'Immolating Souleater').
card_type('immolating souleater', 'Artifact Creature — Hound').
card_types('immolating souleater', ['Artifact', 'Creature']).
card_subtypes('immolating souleater', ['Hound']).
card_colors('immolating souleater', []).
card_text('immolating souleater', '{R/P}: Immolating Souleater gets +1/+0 until end of turn. ({R/P} can be paid with either {R} or 2 life.)').
card_mana_cost('immolating souleater', ['2']).
card_cmc('immolating souleater', 2).
card_layout('immolating souleater', 'normal').
card_power('immolating souleater', 1).
card_toughness('immolating souleater', 1).

% Found in: 4ED, LEG, ME3
card_name('immolation', 'Immolation').
card_type('immolation', 'Enchantment — Aura').
card_types('immolation', ['Enchantment']).
card_subtypes('immolation', ['Aura']).
card_colors('immolation', ['R']).
card_text('immolation', 'Enchant creature\nEnchanted creature gets +2/-2.').
card_mana_cost('immolation', ['R']).
card_cmc('immolation', 1).
card_layout('immolation', 'normal').

% Found in: ALA
card_name('immortal coil', 'Immortal Coil').
card_type('immortal coil', 'Artifact').
card_types('immortal coil', ['Artifact']).
card_subtypes('immortal coil', []).
card_colors('immortal coil', ['B']).
card_text('immortal coil', '{T}, Exile two cards from your graveyard: Draw a card.\nIf damage would be dealt to you, prevent that damage. Exile a card from your graveyard for each 1 damage prevented this way.\nWhen there are no cards in your graveyard, you lose the game.').
card_mana_cost('immortal coil', ['2', 'B', 'B']).
card_cmc('immortal coil', 4).
card_layout('immortal coil', 'normal').

% Found in: GTC
card_name('immortal servitude', 'Immortal Servitude').
card_type('immortal servitude', 'Sorcery').
card_types('immortal servitude', ['Sorcery']).
card_subtypes('immortal servitude', []).
card_colors('immortal servitude', ['W', 'B']).
card_text('immortal servitude', 'Return each creature card with converted mana cost X from your graveyard to the battlefield.').
card_mana_cost('immortal servitude', ['X', 'W/B', 'W/B', 'W/B']).
card_cmc('immortal servitude', 3).
card_layout('immortal servitude', 'normal').

% Found in: PLC
card_name('imp\'s mischief', 'Imp\'s Mischief').
card_type('imp\'s mischief', 'Instant').
card_types('imp\'s mischief', ['Instant']).
card_subtypes('imp\'s mischief', []).
card_colors('imp\'s mischief', ['B']).
card_text('imp\'s mischief', 'Change the target of target spell with a single target. You lose life equal to that spell\'s converted mana cost.').
card_mana_cost('imp\'s mischief', ['1', 'B']).
card_cmc('imp\'s mischief', 2).
card_layout('imp\'s mischief', 'normal').

% Found in: C14
card_name('impact resonance', 'Impact Resonance').
card_type('impact resonance', 'Instant').
card_types('impact resonance', ['Instant']).
card_subtypes('impact resonance', []).
card_colors('impact resonance', ['R']).
card_text('impact resonance', 'Impact Resonance deals X damage divided as you choose among any number of target creatures, where X is the greatest amount of damage dealt by a source to a permanent or player this turn.').
card_mana_cost('impact resonance', ['1', 'R']).
card_cmc('impact resonance', 2).
card_layout('impact resonance', 'normal').

% Found in: DTK
card_name('impact tremors', 'Impact Tremors').
card_type('impact tremors', 'Enchantment').
card_types('impact tremors', ['Enchantment']).
card_subtypes('impact tremors', []).
card_colors('impact tremors', ['R']).
card_text('impact tremors', 'Whenever a creature enters the battlefield under your control, Impact Tremors deals 1 damage to each opponent.').
card_mana_cost('impact tremors', ['1', 'R']).
card_cmc('impact tremors', 2).
card_layout('impact tremors', 'normal').

% Found in: NPH
card_name('impaler shrike', 'Impaler Shrike').
card_type('impaler shrike', 'Creature — Bird').
card_types('impaler shrike', ['Creature']).
card_subtypes('impaler shrike', ['Bird']).
card_colors('impaler shrike', ['U']).
card_text('impaler shrike', 'Flying\nWhenever Impaler Shrike deals combat damage to a player, you may sacrifice it. If you do, draw three cards.').
card_mana_cost('impaler shrike', ['2', 'U', 'U']).
card_cmc('impaler shrike', 4).
card_layout('impaler shrike', 'normal').
card_power('impaler shrike', 3).
card_toughness('impaler shrike', 1).

% Found in: 7ED, UDS
card_name('impatience', 'Impatience').
card_type('impatience', 'Enchantment').
card_types('impatience', ['Enchantment']).
card_subtypes('impatience', []).
card_colors('impatience', ['R']).
card_text('impatience', 'At the beginning of each player\'s end step, if that player didn\'t cast a spell this turn, Impatience deals 2 damage to him or her.').
card_mana_cost('impatience', ['2', 'R']).
card_cmc('impatience', 3).
card_layout('impatience', 'normal').

% Found in: EVE
card_name('impelled giant', 'Impelled Giant').
card_type('impelled giant', 'Creature — Giant Warrior').
card_types('impelled giant', ['Creature']).
card_subtypes('impelled giant', ['Giant', 'Warrior']).
card_colors('impelled giant', ['R']).
card_text('impelled giant', 'Trample\nTap an untapped red creature you control other than Impelled Giant: Impelled Giant gets +X/+0 until end of turn, where X is the power of the creature tapped this way.').
card_mana_cost('impelled giant', ['4', 'R', 'R']).
card_cmc('impelled giant', 6).
card_layout('impelled giant', 'normal').
card_power('impelled giant', 3).
card_toughness('impelled giant', 3).

% Found in: ULG
card_name('impending disaster', 'Impending Disaster').
card_type('impending disaster', 'Enchantment').
card_types('impending disaster', ['Enchantment']).
card_subtypes('impending disaster', []).
card_colors('impending disaster', ['R']).
card_text('impending disaster', 'At the beginning of your upkeep, if there are seven or more lands on the battlefield, sacrifice Impending Disaster and destroy all lands.').
card_mana_cost('impending disaster', ['1', 'R']).
card_cmc('impending disaster', 2).
card_layout('impending disaster', 'normal').

% Found in: PTK
card_name('imperial edict', 'Imperial Edict').
card_type('imperial edict', 'Sorcery').
card_types('imperial edict', ['Sorcery']).
card_subtypes('imperial edict', []).
card_colors('imperial edict', ['B']).
card_text('imperial edict', 'Target opponent chooses a creature he or she controls. Destroy it.').
card_mana_cost('imperial edict', ['1', 'B']).
card_cmc('imperial edict', 2).
card_layout('imperial edict', 'normal').

% Found in: ARC, LGN
card_name('imperial hellkite', 'Imperial Hellkite').
card_type('imperial hellkite', 'Creature — Dragon').
card_types('imperial hellkite', ['Creature']).
card_subtypes('imperial hellkite', ['Dragon']).
card_colors('imperial hellkite', ['R']).
card_text('imperial hellkite', 'Flying\nMorph {6}{R}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Imperial Hellkite is turned face up, you may search your library for a Dragon card, reveal it, and put it into your hand. If you do, shuffle your library.').
card_mana_cost('imperial hellkite', ['5', 'R', 'R']).
card_cmc('imperial hellkite', 7).
card_layout('imperial hellkite', 'normal').
card_power('imperial hellkite', 6).
card_toughness('imperial hellkite', 6).

% Found in: FUT
card_name('imperial mask', 'Imperial Mask').
card_type('imperial mask', 'Enchantment').
card_types('imperial mask', ['Enchantment']).
card_subtypes('imperial mask', []).
card_colors('imperial mask', ['W']).
card_text('imperial mask', 'When Imperial Mask enters the battlefield, if it\'s not a token, each of your teammates puts a token that\'s a copy of Imperial Mask onto the battlefield.\nYou have hexproof. (You can\'t be the target of spells or abilities your opponents control.)').
card_mana_cost('imperial mask', ['4', 'W']).
card_cmc('imperial mask', 5).
card_layout('imperial mask', 'normal').

% Found in: ME2, PTK, pJGP
card_name('imperial recruiter', 'Imperial Recruiter').
card_type('imperial recruiter', 'Creature — Human Advisor').
card_types('imperial recruiter', ['Creature']).
card_subtypes('imperial recruiter', ['Human', 'Advisor']).
card_colors('imperial recruiter', ['R']).
card_text('imperial recruiter', 'When Imperial Recruiter enters the battlefield, search your library for a creature card with power 2 or less, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('imperial recruiter', ['2', 'R']).
card_cmc('imperial recruiter', 3).
card_layout('imperial recruiter', 'normal').
card_power('imperial recruiter', 1).
card_toughness('imperial recruiter', 1).

% Found in: ME2, PTK
card_name('imperial seal', 'Imperial Seal').
card_type('imperial seal', 'Sorcery').
card_types('imperial seal', ['Sorcery']).
card_subtypes('imperial seal', []).
card_colors('imperial seal', ['B']).
card_text('imperial seal', 'Search your library for a card, then shuffle your library and put that card on top of it. You lose 2 life.').
card_mana_cost('imperial seal', ['B']).
card_cmc('imperial seal', 1).
card_layout('imperial seal', 'normal').

% Found in: FUT, MMA
card_name('imperiosaur', 'Imperiosaur').
card_type('imperiosaur', 'Creature — Lizard').
card_types('imperiosaur', ['Creature']).
card_subtypes('imperiosaur', ['Lizard']).
card_colors('imperiosaur', ['G']).
card_text('imperiosaur', 'Spend only mana produced by basic lands to cast Imperiosaur.').
card_mana_cost('imperiosaur', ['2', 'G', 'G']).
card_cmc('imperiosaur', 4).
card_layout('imperiosaur', 'normal').
card_power('imperiosaur', 5).
card_toughness('imperiosaur', 5).

% Found in: C14, DD3_EVG, DPA, EVG, LRW, pCMP
card_name('imperious perfect', 'Imperious Perfect').
card_type('imperious perfect', 'Creature — Elf Warrior').
card_types('imperious perfect', ['Creature']).
card_subtypes('imperious perfect', ['Elf', 'Warrior']).
card_colors('imperious perfect', ['G']).
card_text('imperious perfect', 'Other Elf creatures you control get +1/+1.\n{G}, {T}: Put a 1/1 green Elf Warrior creature token onto the battlefield.').
card_mana_cost('imperious perfect', ['2', 'G']).
card_cmc('imperious perfect', 3).
card_layout('imperious perfect', 'normal').
card_power('imperious perfect', 2).
card_toughness('imperious perfect', 2).

% Found in: BNG
card_name('impetuous sunchaser', 'Impetuous Sunchaser').
card_type('impetuous sunchaser', 'Creature — Human Soldier').
card_types('impetuous sunchaser', ['Creature']).
card_subtypes('impetuous sunchaser', ['Human', 'Soldier']).
card_colors('impetuous sunchaser', ['R']).
card_text('impetuous sunchaser', 'Flying, haste\nImpetuous Sunchaser attacks each turn if able.').
card_mana_cost('impetuous sunchaser', ['1', 'R']).
card_cmc('impetuous sunchaser', 2).
card_layout('impetuous sunchaser', 'normal').
card_power('impetuous sunchaser', 1).
card_toughness('impetuous sunchaser', 1).

% Found in: FEM
card_name('implements of sacrifice', 'Implements of Sacrifice').
card_type('implements of sacrifice', 'Artifact').
card_types('implements of sacrifice', ['Artifact']).
card_subtypes('implements of sacrifice', []).
card_colors('implements of sacrifice', []).
card_text('implements of sacrifice', '{1}, {T}, Sacrifice Implements of Sacrifice: Add two mana of any one color to your mana pool.').
card_mana_cost('implements of sacrifice', ['2']).
card_cmc('implements of sacrifice', 2).
card_layout('implements of sacrifice', 'normal').
card_reserved('implements of sacrifice').

% Found in: PLS
card_name('implode', 'Implode').
card_type('implode', 'Sorcery').
card_types('implode', ['Sorcery']).
card_subtypes('implode', []).
card_colors('implode', ['R']).
card_text('implode', 'Destroy target land.\nDraw a card.').
card_mana_cost('implode', ['4', 'R']).
card_cmc('implode', 5).
card_layout('implode', 'normal').

% Found in: M14
card_name('imposing sovereign', 'Imposing Sovereign').
card_type('imposing sovereign', 'Creature — Human').
card_types('imposing sovereign', ['Creature']).
card_subtypes('imposing sovereign', ['Human']).
card_colors('imposing sovereign', ['W']).
card_text('imposing sovereign', 'Creatures your opponents control enter the battlefield tapped.').
card_mana_cost('imposing sovereign', ['1', 'W']).
card_cmc('imposing sovereign', 2).
card_layout('imposing sovereign', 'normal').
card_power('imposing sovereign', 2).
card_toughness('imposing sovereign', 1).

% Found in: 5ED, ICE
card_name('imposing visage', 'Imposing Visage').
card_type('imposing visage', 'Enchantment — Aura').
card_types('imposing visage', ['Enchantment']).
card_subtypes('imposing visage', ['Aura']).
card_colors('imposing visage', ['R']).
card_text('imposing visage', 'Enchant creature\nEnchanted creature has menace. (It can\'t be blocked except by two or more creatures.)').
card_mana_cost('imposing visage', ['R']).
card_cmc('imposing visage', 1).
card_layout('imposing visage', 'normal').

% Found in: LEG
card_name('imprison', 'Imprison').
card_type('imprison', 'Enchantment — Aura').
card_types('imprison', ['Enchantment']).
card_subtypes('imprison', ['Aura']).
card_colors('imprison', ['B']).
card_text('imprison', 'Enchant creature\nWhenever a player activates an ability of enchanted creature with {T} in its activation cost that isn\'t a mana ability, you may pay {1}. If you do, counter that ability. If you don\'t, destroy Imprison.\nWhenever enchanted creature attacks or blocks, you may pay {1}. If you do, tap the creature, remove it from combat, and creatures it was blocking that had become blocked by only that creature this combat become unblocked. If you don\'t, destroy Imprison.').
card_mana_cost('imprison', ['B']).
card_cmc('imprison', 1).
card_layout('imprison', 'normal').
card_reserved('imprison').

% Found in: SHM
card_name('impromptu raid', 'Impromptu Raid').
card_type('impromptu raid', 'Enchantment').
card_types('impromptu raid', ['Enchantment']).
card_subtypes('impromptu raid', []).
card_colors('impromptu raid', ['R', 'G']).
card_text('impromptu raid', '{2}{R/G}: Reveal the top card of your library. If it isn\'t a creature card, put it into your graveyard. Otherwise, put that card onto the battlefield. That creature gains haste. Sacrifice it at the beginning of the next end step.').
card_mana_cost('impromptu raid', ['3', 'R/G']).
card_cmc('impromptu raid', 4).
card_layout('impromptu raid', 'normal').

% Found in: ONS
card_name('improvised armor', 'Improvised Armor').
card_type('improvised armor', 'Enchantment — Aura').
card_types('improvised armor', ['Enchantment']).
card_subtypes('improvised armor', ['Aura']).
card_colors('improvised armor', ['W']).
card_text('improvised armor', 'Enchant creature\nEnchanted creature gets +2/+5.\nCycling {3} ({3}, Discard this card: Draw a card.)').
card_mana_cost('improvised armor', ['3', 'W']).
card_cmc('improvised armor', 4).
card_layout('improvised armor', 'normal').

% Found in: TMP
card_name('imps\' taunt', 'Imps\' Taunt').
card_type('imps\' taunt', 'Instant').
card_types('imps\' taunt', ['Instant']).
card_subtypes('imps\' taunt', []).
card_colors('imps\' taunt', ['B']).
card_text('imps\' taunt', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nTarget creature attacks this turn if able.').
card_mana_cost('imps\' taunt', ['1', 'B']).
card_cmc('imps\' taunt', 2).
card_layout('imps\' taunt', 'normal').

% Found in: BTD, DDN, V13, VIS, pFNM
card_name('impulse', 'Impulse').
card_type('impulse', 'Instant').
card_types('impulse', ['Instant']).
card_subtypes('impulse', []).
card_colors('impulse', ['U']).
card_text('impulse', 'Look at the top four cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('impulse', ['1', 'U']).
card_cmc('impulse', 2).
card_layout('impulse', 'normal').

% Found in: ODY
card_name('impulsive maneuvers', 'Impulsive Maneuvers').
card_type('impulsive maneuvers', 'Enchantment').
card_types('impulsive maneuvers', ['Enchantment']).
card_subtypes('impulsive maneuvers', []).
card_colors('impulsive maneuvers', ['R']).
card_text('impulsive maneuvers', 'Whenever a creature attacks, flip a coin. If you win the flip, the next time that creature would deal combat damage this turn, it deals double that damage instead. If you lose the flip, the next time that creature would deal combat damage this turn, prevent that damage.').
card_mana_cost('impulsive maneuvers', ['2', 'R', 'R']).
card_cmc('impulsive maneuvers', 4).
card_layout('impulsive maneuvers', 'normal').

% Found in: M15, pLPA
card_name('in garruk\'s wake', 'In Garruk\'s Wake').
card_type('in garruk\'s wake', 'Sorcery').
card_types('in garruk\'s wake', ['Sorcery']).
card_subtypes('in garruk\'s wake', []).
card_colors('in garruk\'s wake', ['B']).
card_text('in garruk\'s wake', 'Destroy all creatures you don\'t control and all planeswalkers you don\'t control.').
card_mana_cost('in garruk\'s wake', ['7', 'B', 'B']).
card_cmc('in garruk\'s wake', 9).
card_layout('in garruk\'s wake', 'normal').

% Found in: LEG, ME4
card_name('in the eye of chaos', 'In the Eye of Chaos').
card_type('in the eye of chaos', 'World Enchantment').
card_types('in the eye of chaos', ['Enchantment']).
card_subtypes('in the eye of chaos', []).
card_supertypes('in the eye of chaos', ['World']).
card_colors('in the eye of chaos', ['U']).
card_text('in the eye of chaos', 'Whenever a player casts an instant spell, counter it unless that player pays {X}, where X is its converted mana cost.').
card_mana_cost('in the eye of chaos', ['2', 'U']).
card_cmc('in the eye of chaos', 3).
card_layout('in the eye of chaos', 'normal').
card_reserved('in the eye of chaos').

% Found in: BOK
card_name('in the web of war', 'In the Web of War').
card_type('in the web of war', 'Enchantment').
card_types('in the web of war', ['Enchantment']).
card_subtypes('in the web of war', []).
card_colors('in the web of war', ['R']).
card_text('in the web of war', 'Whenever a creature enters the battlefield under your control, it gets +2/+0 and gains haste until end of turn.').
card_mana_cost('in the web of war', ['3', 'R', 'R']).
card_cmc('in the web of war', 5).
card_layout('in the web of war', 'normal').

% Found in: RTR
card_name('inaction injunction', 'Inaction Injunction').
card_type('inaction injunction', 'Sorcery').
card_types('inaction injunction', ['Sorcery']).
card_subtypes('inaction injunction', []).
card_colors('inaction injunction', ['U']).
card_text('inaction injunction', 'Detain target creature an opponent controls. (Until your next turn, that creature can\'t attack or block and its activated abilities can\'t be activated.)\nDraw a card.').
card_mana_cost('inaction injunction', ['1', 'U']).
card_cmc('inaction injunction', 2).
card_layout('inaction injunction', 'normal').

% Found in: SOK
card_name('iname as one', 'Iname as One').
card_type('iname as one', 'Legendary Creature — Spirit').
card_types('iname as one', ['Creature']).
card_subtypes('iname as one', ['Spirit']).
card_supertypes('iname as one', ['Legendary']).
card_colors('iname as one', ['B', 'G']).
card_text('iname as one', 'When Iname as One enters the battlefield, if you cast it from your hand, you may search your library for a Spirit permanent card, put it onto the battlefield, then shuffle your library.\nWhen Iname as One dies, you may exile it. If you do, return target Spirit permanent card from your graveyard to the battlefield.').
card_mana_cost('iname as one', ['8', 'B', 'B', 'G', 'G']).
card_cmc('iname as one', 12).
card_layout('iname as one', 'normal').
card_power('iname as one', 8).
card_toughness('iname as one', 8).

% Found in: CHK
card_name('iname, death aspect', 'Iname, Death Aspect').
card_type('iname, death aspect', 'Legendary Creature — Spirit').
card_types('iname, death aspect', ['Creature']).
card_subtypes('iname, death aspect', ['Spirit']).
card_supertypes('iname, death aspect', ['Legendary']).
card_colors('iname, death aspect', ['B']).
card_text('iname, death aspect', 'When Iname, Death Aspect enters the battlefield, you may search your library for any number of Spirit cards and put them into your graveyard. If you do, shuffle your library.').
card_mana_cost('iname, death aspect', ['4', 'B', 'B']).
card_cmc('iname, death aspect', 6).
card_layout('iname, death aspect', 'normal').
card_power('iname, death aspect', 4).
card_toughness('iname, death aspect', 4).

% Found in: CHK
card_name('iname, life aspect', 'Iname, Life Aspect').
card_type('iname, life aspect', 'Legendary Creature — Spirit').
card_types('iname, life aspect', ['Creature']).
card_subtypes('iname, life aspect', ['Spirit']).
card_supertypes('iname, life aspect', ['Legendary']).
card_colors('iname, life aspect', ['G']).
card_text('iname, life aspect', 'When Iname, Life Aspect dies, you may exile it. If you do, return any number of target Spirit cards from your graveyard to your hand.').
card_mana_cost('iname, life aspect', ['4', 'G', 'G']).
card_cmc('iname, life aspect', 6).
card_layout('iname, life aspect', 'normal').
card_power('iname, life aspect', 4).
card_toughness('iname, life aspect', 4).

% Found in: LRW, MM2
card_name('incandescent soulstoke', 'Incandescent Soulstoke').
card_type('incandescent soulstoke', 'Creature — Elemental Shaman').
card_types('incandescent soulstoke', ['Creature']).
card_subtypes('incandescent soulstoke', ['Elemental', 'Shaman']).
card_colors('incandescent soulstoke', ['R']).
card_text('incandescent soulstoke', 'Other Elemental creatures you control get +1/+1.\n{1}{R}, {T}: You may put an Elemental creature card from your hand onto the battlefield. That creature gains haste until end of turn. Sacrifice it at the beginning of the next end step.').
card_mana_cost('incandescent soulstoke', ['2', 'R']).
card_cmc('incandescent soulstoke', 3).
card_layout('incandescent soulstoke', 'normal').
card_power('incandescent soulstoke', 2).
card_toughness('incandescent soulstoke', 2).

% Found in: UDS
card_name('incendiary', 'Incendiary').
card_type('incendiary', 'Enchantment — Aura').
card_types('incendiary', ['Enchantment']).
card_subtypes('incendiary', ['Aura']).
card_colors('incendiary', ['R']).
card_text('incendiary', 'Enchant creature\nAt the beginning of your upkeep, you may put a fuse counter on Incendiary.\nWhen enchanted creature dies, Incendiary deals X damage to target creature or player, where X is the number of fuse counters on Incendiary.').
card_mana_cost('incendiary', ['R']).
card_cmc('incendiary', 1).
card_layout('incendiary', 'normal').

% Found in: C13, LRW
card_name('incendiary command', 'Incendiary Command').
card_type('incendiary command', 'Sorcery').
card_types('incendiary command', ['Sorcery']).
card_subtypes('incendiary command', []).
card_colors('incendiary command', ['R']).
card_text('incendiary command', 'Choose two —\n• Incendiary Command deals 4 damage to target player.\n• Incendiary Command deals 2 damage to each creature.\n• Destroy target nonbasic land.\n• Each player discards all the cards in his or her hand, then draws that many cards.').
card_mana_cost('incendiary command', ['3', 'R', 'R']).
card_cmc('incendiary command', 5).
card_layout('incendiary command', 'normal').

% Found in: 10E, 5ED, CST, DD2, DD3_JVC, DKM, DPA, ICE, M12, ME2, MIR, pLGM, pMPR
card_name('incinerate', 'Incinerate').
card_type('incinerate', 'Instant').
card_types('incinerate', ['Instant']).
card_subtypes('incinerate', []).
card_colors('incinerate', ['R']).
card_text('incinerate', 'Incinerate deals 3 damage to target creature or player. A creature dealt damage this way can\'t be regenerated this turn.').
card_mana_cost('incinerate', ['1', 'R']).
card_cmc('incinerate', 2).
card_layout('incinerate', 'normal').

% Found in: M11
card_name('incite', 'Incite').
card_type('incite', 'Instant').
card_types('incite', ['Instant']).
card_subtypes('incite', []).
card_colors('incite', ['R']).
card_text('incite', 'Target creature becomes red until end of turn and attacks this turn if able.').
card_mana_cost('incite', ['R']).
card_cmc('incite', 1).
card_layout('incite', 'normal').

% Found in: RAV
card_name('incite hysteria', 'Incite Hysteria').
card_type('incite hysteria', 'Sorcery').
card_types('incite hysteria', ['Sorcery']).
card_subtypes('incite hysteria', []).
card_colors('incite hysteria', ['R']).
card_text('incite hysteria', 'Radiance — Until end of turn, target creature and each other creature that shares a color with it gain \"This creature can\'t block.\"').
card_mana_cost('incite hysteria', ['2', 'R']).
card_cmc('incite hysteria', 3).
card_layout('incite hysteria', 'normal').

% Found in: C14
card_name('incite rebellion', 'Incite Rebellion').
card_type('incite rebellion', 'Sorcery').
card_types('incite rebellion', ['Sorcery']).
card_subtypes('incite rebellion', []).
card_colors('incite rebellion', ['R']).
card_text('incite rebellion', 'For each player, Incite Rebellion deals damage to that player and each creature that player controls equal to the number of creatures he or she controls.').
card_mana_cost('incite rebellion', ['4', 'R', 'R']).
card_cmc('incite rebellion', 6).
card_layout('incite rebellion', 'normal').

% Found in: MRD
card_name('incite war', 'Incite War').
card_type('incite war', 'Instant').
card_types('incite war', ['Instant']).
card_subtypes('incite war', []).
card_colors('incite war', ['R']).
card_text('incite war', 'Choose one —\n• Creatures target player controls attack this turn if able.\n• Creatures you control gain first strike until end of turn.\nEntwine {2} (Choose both if you pay the entwine cost.)').
card_mana_cost('incite war', ['2', 'R']).
card_cmc('incite war', 3).
card_layout('incite war', 'normal').

% Found in: UGL
card_name('incoming!', 'Incoming!').
card_type('incoming!', 'Sorcery').
card_types('incoming!', ['Sorcery']).
card_subtypes('incoming!', []).
card_colors('incoming!', ['G']).
card_text('incoming!', 'Each player searches his or her library for any number of artifacts, creatures, enchantments, and lands and puts those cards into play. Each player shuffles his or her library afterwards.').
card_mana_cost('incoming!', ['4', 'G', 'G', 'G', 'G']).
card_cmc('incoming!', 8).
card_layout('incoming!', 'normal').

% Found in: DKA
card_name('increasing ambition', 'Increasing Ambition').
card_type('increasing ambition', 'Sorcery').
card_types('increasing ambition', ['Sorcery']).
card_subtypes('increasing ambition', []).
card_colors('increasing ambition', ['B']).
card_text('increasing ambition', 'Search your library for a card and put that card into your hand. If Increasing Ambition was cast from a graveyard, instead search your library for two cards and put those cards into your hand. Then shuffle your library.\nFlashback {7}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('increasing ambition', ['4', 'B']).
card_cmc('increasing ambition', 5).
card_layout('increasing ambition', 'normal').

% Found in: DKA
card_name('increasing confusion', 'Increasing Confusion').
card_type('increasing confusion', 'Sorcery').
card_types('increasing confusion', ['Sorcery']).
card_subtypes('increasing confusion', []).
card_colors('increasing confusion', ['U']).
card_text('increasing confusion', 'Target player puts the top X cards of his or her library into his or her graveyard. If Increasing Confusion was cast from a graveyard, that player puts twice that many cards into his or her graveyard instead.\nFlashback {X}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('increasing confusion', ['X', 'U']).
card_cmc('increasing confusion', 1).
card_layout('increasing confusion', 'normal').

% Found in: DKA
card_name('increasing devotion', 'Increasing Devotion').
card_type('increasing devotion', 'Sorcery').
card_types('increasing devotion', ['Sorcery']).
card_subtypes('increasing devotion', []).
card_colors('increasing devotion', ['W']).
card_text('increasing devotion', 'Put five 1/1 white Human creature tokens onto the battlefield. If Increasing Devotion was cast from a graveyard, put ten of those tokens onto the battlefield instead.\nFlashback {7}{W}{W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('increasing devotion', ['3', 'W', 'W']).
card_cmc('increasing devotion', 5).
card_layout('increasing devotion', 'normal').

% Found in: DKA
card_name('increasing savagery', 'Increasing Savagery').
card_type('increasing savagery', 'Sorcery').
card_types('increasing savagery', ['Sorcery']).
card_subtypes('increasing savagery', []).
card_colors('increasing savagery', ['G']).
card_text('increasing savagery', 'Put five +1/+1 counters on target creature. If Increasing Savagery was cast from a graveyard, put ten +1/+1 counters on that creature instead.\nFlashback {5}{G}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('increasing savagery', ['2', 'G', 'G']).
card_cmc('increasing savagery', 4).
card_layout('increasing savagery', 'normal').

% Found in: DKA
card_name('increasing vengeance', 'Increasing Vengeance').
card_type('increasing vengeance', 'Instant').
card_types('increasing vengeance', ['Instant']).
card_subtypes('increasing vengeance', []).
card_colors('increasing vengeance', ['R']).
card_text('increasing vengeance', 'Copy target instant or sorcery spell you control. If Increasing Vengeance was cast from a graveyard, copy that spell twice instead. You may choose new targets for the copies.\nFlashback {3}{R}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('increasing vengeance', ['R', 'R']).
card_cmc('increasing vengeance', 2).
card_layout('increasing vengeance', 'normal').

% Found in: ARC, HOP, SHM
card_name('incremental blight', 'Incremental Blight').
card_type('incremental blight', 'Sorcery').
card_types('incremental blight', ['Sorcery']).
card_subtypes('incremental blight', []).
card_colors('incremental blight', ['B']).
card_text('incremental blight', 'Put a -1/-1 counter on target creature, two -1/-1 counters on another target creature, and three -1/-1 counters on a third target creature.').
card_mana_cost('incremental blight', ['3', 'B', 'B']).
card_cmc('incremental blight', 5).
card_layout('incremental blight', 'normal').

% Found in: KTK, LRW, MMA
card_name('incremental growth', 'Incremental Growth').
card_type('incremental growth', 'Sorcery').
card_types('incremental growth', ['Sorcery']).
card_subtypes('incremental growth', []).
card_colors('incremental growth', ['G']).
card_text('incremental growth', 'Put a +1/+1 counter on target creature, two +1/+1 counters on another target creature, and three +1/+1 counters on a third target creature.').
card_mana_cost('incremental growth', ['3', 'G', 'G']).
card_cmc('incremental growth', 5).
card_layout('incremental growth', 'normal').

% Found in: BFZ
card_name('incubator drone', 'Incubator Drone').
card_type('incubator drone', 'Creature — Eldrazi Drone').
card_types('incubator drone', ['Creature']).
card_subtypes('incubator drone', ['Eldrazi', 'Drone']).
card_colors('incubator drone', []).
card_text('incubator drone', 'Devoid (This card has no color.)\nWhen Incubator Drone enters the battlefield, put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('incubator drone', ['3', 'U']).
card_cmc('incubator drone', 4).
card_layout('incubator drone', 'normal').
card_power('incubator drone', 2).
card_toughness('incubator drone', 3).

% Found in: ALA
card_name('incurable ogre', 'Incurable Ogre').
card_type('incurable ogre', 'Creature — Ogre Mutant').
card_types('incurable ogre', ['Creature']).
card_subtypes('incurable ogre', ['Ogre', 'Mutant']).
card_colors('incurable ogre', ['R']).
card_text('incurable ogre', '').
card_mana_cost('incurable ogre', ['3', 'R']).
card_cmc('incurable ogre', 4).
card_layout('incurable ogre', 'normal').
card_power('incurable ogre', 5).
card_toughness('incurable ogre', 1).

% Found in: GTC
card_name('incursion specialist', 'Incursion Specialist').
card_type('incursion specialist', 'Creature — Human Wizard').
card_types('incursion specialist', ['Creature']).
card_subtypes('incursion specialist', ['Human', 'Wizard']).
card_colors('incursion specialist', ['U']).
card_text('incursion specialist', 'Whenever you cast your second spell each turn, Incursion Specialist gets +2/+0 until end of turn and can\'t be blocked this turn.').
card_mana_cost('incursion specialist', ['1', 'U']).
card_cmc('incursion specialist', 2).
card_layout('incursion specialist', 'normal').
card_power('incursion specialist', 1).
card_toughness('incursion specialist', 3).

% Found in: BOK
card_name('indebted samurai', 'Indebted Samurai').
card_type('indebted samurai', 'Creature — Human Samurai').
card_types('indebted samurai', ['Creature']).
card_subtypes('indebted samurai', ['Human', 'Samurai']).
card_colors('indebted samurai', ['W']).
card_text('indebted samurai', 'Bushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)\nWhenever a Samurai you control dies, you may put a +1/+1 counter on Indebted Samurai.').
card_mana_cost('indebted samurai', ['3', 'W']).
card_cmc('indebted samurai', 4).
card_layout('indebted samurai', 'normal').
card_power('indebted samurai', 2).
card_toughness('indebted samurai', 3).

% Found in: MMQ
card_name('indentured djinn', 'Indentured Djinn').
card_type('indentured djinn', 'Creature — Djinn').
card_types('indentured djinn', ['Creature']).
card_subtypes('indentured djinn', ['Djinn']).
card_colors('indentured djinn', ['U']).
card_text('indentured djinn', 'Flying\nWhen Indentured Djinn enters the battlefield, each other player may draw up to three cards.').
card_mana_cost('indentured djinn', ['1', 'U', 'U']).
card_cmc('indentured djinn', 3).
card_layout('indentured djinn', 'normal').
card_power('indentured djinn', 4).
card_toughness('indentured djinn', 4).

% Found in: RAV
card_name('indentured oaf', 'Indentured Oaf').
card_type('indentured oaf', 'Creature — Ogre Warrior').
card_types('indentured oaf', ['Creature']).
card_subtypes('indentured oaf', ['Ogre', 'Warrior']).
card_colors('indentured oaf', ['R']).
card_text('indentured oaf', 'Prevent all damage that Indentured Oaf would deal to red creatures.').
card_mana_cost('indentured oaf', ['3', 'R']).
card_cmc('indentured oaf', 4).
card_layout('indentured oaf', 'normal').
card_power('indentured oaf', 4).
card_toughness('indentured oaf', 3).

% Found in: PTK
card_name('independent troops', 'Independent Troops').
card_type('independent troops', 'Creature — Human Soldier').
card_types('independent troops', ['Creature']).
card_subtypes('independent troops', ['Human', 'Soldier']).
card_colors('independent troops', ['R']).
card_text('independent troops', '').
card_mana_cost('independent troops', ['1', 'R']).
card_cmc('independent troops', 2).
card_layout('independent troops', 'normal').
card_power('independent troops', 2).
card_toughness('independent troops', 1).

% Found in: M10, M14
card_name('indestructibility', 'Indestructibility').
card_type('indestructibility', 'Enchantment — Aura').
card_types('indestructibility', ['Enchantment']).
card_subtypes('indestructibility', ['Aura']).
card_colors('indestructibility', ['W']).
card_text('indestructibility', 'Enchant permanent\nEnchanted permanent has indestructible. (Effects that say \"destroy\" don\'t destroy that permanent. A creature with indestructible can\'t be destroyed by damage.)').
card_mana_cost('indestructibility', ['3', 'W']).
card_cmc('indestructibility', 4).
card_layout('indestructibility', 'normal').

% Found in: CHR, LEG
card_name('indestructible aura', 'Indestructible Aura').
card_type('indestructible aura', 'Instant').
card_types('indestructible aura', ['Instant']).
card_subtypes('indestructible aura', []).
card_colors('indestructible aura', ['W']).
card_text('indestructible aura', 'Prevent all damage that would be dealt to target creature this turn.').
card_mana_cost('indestructible aura', ['W']).
card_cmc('indestructible aura', 1).
card_layout('indestructible aura', 'normal').

% Found in: 8ED, 9ED, APC, M13
card_name('index', 'Index').
card_type('index', 'Sorcery').
card_types('index', ['Sorcery']).
card_subtypes('index', []).
card_colors('index', ['U']).
card_text('index', 'Look at the top five cards of your library, then put them back in any order.').
card_mana_cost('index', ['U']).
card_cmc('index', 1).
card_layout('index', 'normal').

% Found in: EVE
card_name('indigo faerie', 'Indigo Faerie').
card_type('indigo faerie', 'Creature — Faerie Wizard').
card_types('indigo faerie', ['Creature']).
card_subtypes('indigo faerie', ['Faerie', 'Wizard']).
card_colors('indigo faerie', ['U']).
card_text('indigo faerie', 'Flying\n{U}: Target permanent becomes blue in addition to its other colors until end of turn.').
card_mana_cost('indigo faerie', ['1', 'U']).
card_cmc('indigo faerie', 2).
card_layout('indigo faerie', 'normal').
card_power('indigo faerie', 1).
card_toughness('indigo faerie', 1).

% Found in: MOR
card_name('indomitable ancients', 'Indomitable Ancients').
card_type('indomitable ancients', 'Creature — Treefolk Warrior').
card_types('indomitable ancients', ['Creature']).
card_subtypes('indomitable ancients', ['Treefolk', 'Warrior']).
card_colors('indomitable ancients', ['W']).
card_text('indomitable ancients', '').
card_mana_cost('indomitable ancients', ['2', 'W', 'W']).
card_cmc('indomitable ancients', 4).
card_layout('indomitable ancients', 'normal').
card_power('indomitable ancients', 2).
card_toughness('indomitable ancients', 10).

% Found in: MM2, SOM
card_name('indomitable archangel', 'Indomitable Archangel').
card_type('indomitable archangel', 'Creature — Angel').
card_types('indomitable archangel', ['Creature']).
card_subtypes('indomitable archangel', ['Angel']).
card_colors('indomitable archangel', ['W']).
card_text('indomitable archangel', 'Flying\nMetalcraft — Artifacts you control have shroud as long as you control three or more artifacts. (An artifact with shroud can\'t be the target of spells or abilities.)').
card_mana_cost('indomitable archangel', ['2', 'W', 'W']).
card_cmc('indomitable archangel', 4).
card_layout('indomitable archangel', 'normal').
card_power('indomitable archangel', 4).
card_toughness('indomitable archangel', 4).

% Found in: CHK
card_name('indomitable will', 'Indomitable Will').
card_type('indomitable will', 'Enchantment — Aura').
card_types('indomitable will', ['Enchantment']).
card_subtypes('indomitable will', ['Aura']).
card_colors('indomitable will', ['W']).
card_text('indomitable will', 'Flash\nEnchant creature\nEnchanted creature gets +1/+2.').
card_mana_cost('indomitable will', ['1', 'W']).
card_cmc('indomitable will', 2).
card_layout('indomitable will', 'normal').

% Found in: DD3_GVL, DDD, DIS
card_name('indrik stomphowler', 'Indrik Stomphowler').
card_type('indrik stomphowler', 'Creature — Beast').
card_types('indrik stomphowler', ['Creature']).
card_subtypes('indrik stomphowler', ['Beast']).
card_colors('indrik stomphowler', ['G']).
card_text('indrik stomphowler', 'When Indrik Stomphowler enters the battlefield, destroy target artifact or enchantment.').
card_mana_cost('indrik stomphowler', ['4', 'G']).
card_cmc('indrik stomphowler', 5).
card_layout('indrik stomphowler', 'normal').
card_power('indrik stomphowler', 4).
card_toughness('indrik stomphowler', 4).

% Found in: PC2
card_name('indrik umbra', 'Indrik Umbra').
card_type('indrik umbra', 'Enchantment — Aura').
card_types('indrik umbra', ['Enchantment']).
card_subtypes('indrik umbra', ['Aura']).
card_colors('indrik umbra', ['W', 'G']).
card_text('indrik umbra', 'Enchant creature\nEnchanted creature gets +4/+4 and has first strike, and all creatures able to block it do so.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_mana_cost('indrik umbra', ['4', 'G', 'W']).
card_cmc('indrik umbra', 6).
card_layout('indrik umbra', 'normal').

% Found in: DDP, ROE
card_name('induce despair', 'Induce Despair').
card_type('induce despair', 'Instant').
card_types('induce despair', ['Instant']).
card_subtypes('induce despair', []).
card_colors('induce despair', ['B']).
card_text('induce despair', 'As an additional cost to cast Induce Despair, reveal a creature card from your hand.\nTarget creature gets -X/-X until end of turn, where X is the revealed card\'s converted mana cost.').
card_mana_cost('induce despair', ['2', 'B']).
card_cmc('induce despair', 3).
card_layout('induce despair', 'normal').

% Found in: RAV
card_name('induce paranoia', 'Induce Paranoia').
card_type('induce paranoia', 'Instant').
card_types('induce paranoia', ['Instant']).
card_subtypes('induce paranoia', []).
card_colors('induce paranoia', ['U']).
card_text('induce paranoia', 'Counter target spell. If {B} was spent to cast Induce Paranoia, that spell\'s controller puts the top X cards of his or her library into his or her graveyard, where X is the spell\'s converted mana cost.').
card_mana_cost('induce paranoia', ['2', 'U', 'U']).
card_cmc('induce paranoia', 4).
card_layout('induce paranoia', 'normal').

% Found in: M15, pPRE
card_name('indulgent tormentor', 'Indulgent Tormentor').
card_type('indulgent tormentor', 'Creature — Demon').
card_types('indulgent tormentor', ['Creature']).
card_subtypes('indulgent tormentor', ['Demon']).
card_colors('indulgent tormentor', ['B']).
card_text('indulgent tormentor', 'Flying\nAt the beginning of your upkeep, draw a card unless target opponent sacrifices a creature or pays 3 life.').
card_mana_cost('indulgent tormentor', ['3', 'B', 'B']).
card_cmc('indulgent tormentor', 5).
card_layout('indulgent tormentor', 'normal').
card_power('indulgent tormentor', 5).
card_toughness('indulgent tormentor', 3).

% Found in: MRD
card_name('inertia bubble', 'Inertia Bubble').
card_type('inertia bubble', 'Enchantment — Aura').
card_types('inertia bubble', ['Enchantment']).
card_subtypes('inertia bubble', ['Aura']).
card_colors('inertia bubble', ['U']).
card_text('inertia bubble', 'Enchant artifact\nEnchanted artifact doesn\'t untap during its controller\'s untap step.').
card_mana_cost('inertia bubble', ['1', 'U']).
card_cmc('inertia bubble', 2).
card_layout('inertia bubble', 'normal').

% Found in: SHM
card_name('inescapable brute', 'Inescapable Brute').
card_type('inescapable brute', 'Creature — Giant Warrior').
card_types('inescapable brute', ['Creature']).
card_subtypes('inescapable brute', ['Giant', 'Warrior']).
card_colors('inescapable brute', ['R']).
card_text('inescapable brute', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nInescapable Brute must be blocked if able.').
card_mana_cost('inescapable brute', ['5', 'R']).
card_cmc('inescapable brute', 6).
card_layout('inescapable brute', 'normal').
card_power('inescapable brute', 3).
card_toughness('inescapable brute', 3).

% Found in: MM2, SOM
card_name('inexorable tide', 'Inexorable Tide').
card_type('inexorable tide', 'Enchantment').
card_types('inexorable tide', ['Enchantment']).
card_subtypes('inexorable tide', []).
card_colors('inexorable tide', ['U']).
card_text('inexorable tide', 'Whenever you cast a spell, proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_mana_cost('inexorable tide', ['3', 'U', 'U']).
card_cmc('inexorable tide', 5).
card_layout('inexorable tide', 'normal').

% Found in: 6ED, 9ED, ATH, BRB, DDF, DDN, M11, VIS
card_name('infantry veteran', 'Infantry Veteran').
card_type('infantry veteran', 'Creature — Human Soldier').
card_types('infantry veteran', ['Creature']).
card_subtypes('infantry veteran', ['Human', 'Soldier']).
card_colors('infantry veteran', ['W']).
card_text('infantry veteran', '{T}: Target attacking creature gets +1/+1 until end of turn.').
card_mana_cost('infantry veteran', ['W']).
card_cmc('infantry veteran', 1).
card_layout('infantry veteran', 'normal').
card_power('infantry veteran', 1).
card_toughness('infantry veteran', 1).

% Found in: ODY
card_name('infected vermin', 'Infected Vermin').
card_type('infected vermin', 'Creature — Rat').
card_types('infected vermin', ['Creature']).
card_subtypes('infected vermin', ['Rat']).
card_colors('infected vermin', ['B']).
card_text('infected vermin', '{2}{B}: Infected Vermin deals 1 damage to each creature and each player.\nThreshold — {3}{B}: Infected Vermin deals 3 damage to each creature and each player. Activate this ability only if seven or more cards are in your graveyard.').
card_mana_cost('infected vermin', ['2', 'B']).
card_cmc('infected vermin', 3).
card_layout('infected vermin', 'normal').
card_power('infected vermin', 1).
card_toughness('infected vermin', 1).

% Found in: ORI
card_name('infectious bloodlust', 'Infectious Bloodlust').
card_type('infectious bloodlust', 'Enchantment — Aura').
card_types('infectious bloodlust', ['Enchantment']).
card_subtypes('infectious bloodlust', ['Aura']).
card_colors('infectious bloodlust', ['R']).
card_text('infectious bloodlust', 'Enchant creature\nEnchanted creature gets +2/+1, has haste, and attacks each turn if able.\nWhen enchanted creature dies, you may search your library for a card named Infectious Bloodlust, reveal it, put it into your hand, then shuffle your library.').
card_mana_cost('infectious bloodlust', ['1', 'R']).
card_cmc('infectious bloodlust', 2).
card_layout('infectious bloodlust', 'normal').

% Found in: ARC, CNS, CON
card_name('infectious horror', 'Infectious Horror').
card_type('infectious horror', 'Creature — Zombie Horror').
card_types('infectious horror', ['Creature']).
card_subtypes('infectious horror', ['Zombie', 'Horror']).
card_colors('infectious horror', ['B']).
card_text('infectious horror', 'Whenever Infectious Horror attacks, each opponent loses 2 life.').
card_mana_cost('infectious horror', ['3', 'B']).
card_cmc('infectious horror', 4).
card_layout('infectious horror', 'normal').
card_power('infectious horror', 2).
card_toughness('infectious horror', 2).

% Found in: RAV
card_name('infectious host', 'Infectious Host').
card_type('infectious host', 'Creature — Zombie').
card_types('infectious host', ['Creature']).
card_subtypes('infectious host', ['Zombie']).
card_colors('infectious host', ['B']).
card_text('infectious host', 'When Infectious Host dies, target player loses 2 life.').
card_mana_cost('infectious host', ['2', 'B']).
card_cmc('infectious host', 3).
card_layout('infectious host', 'normal').
card_power('infectious host', 1).
card_toughness('infectious host', 1).

% Found in: JUD
card_name('infectious rage', 'Infectious Rage').
card_type('infectious rage', 'Enchantment — Aura').
card_types('infectious rage', ['Enchantment']).
card_subtypes('infectious rage', ['Aura']).
card_colors('infectious rage', ['R']).
card_text('infectious rage', 'Enchant creature\nEnchanted creature gets +2/-1.\nWhen enchanted creature dies, choose a creature at random Infectious Rage can enchant. Return Infectious Rage to the battlefield attached to that creature.').
card_mana_cost('infectious rage', ['1', 'R']).
card_cmc('infectious rage', 2).
card_layout('infectious rage', 'normal').

% Found in: LGN
card_name('infernal caretaker', 'Infernal Caretaker').
card_type('infernal caretaker', 'Creature — Human Cleric').
card_types('infernal caretaker', ['Creature']).
card_subtypes('infernal caretaker', ['Human', 'Cleric']).
card_colors('infernal caretaker', ['B']).
card_text('infernal caretaker', 'Morph {3}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Infernal Caretaker is turned face up, return all Zombie cards from all graveyards to their owners\' hands.').
card_mana_cost('infernal caretaker', ['3', 'B']).
card_cmc('infernal caretaker', 4).
card_layout('infernal caretaker', 'normal').
card_power('infernal caretaker', 2).
card_toughness('infernal caretaker', 2).

% Found in: 6ED, 7ED, MIR
card_name('infernal contract', 'Infernal Contract').
card_type('infernal contract', 'Sorcery').
card_types('infernal contract', ['Sorcery']).
card_subtypes('infernal contract', []).
card_colors('infernal contract', ['B']).
card_text('infernal contract', 'Draw four cards. You lose half your life, rounded up.').
card_mana_cost('infernal contract', ['B', 'B', 'B']).
card_cmc('infernal contract', 3).
card_layout('infernal contract', 'normal').

% Found in: ICE, ME2
card_name('infernal darkness', 'Infernal Darkness').
card_type('infernal darkness', 'Enchantment').
card_types('infernal darkness', ['Enchantment']).
card_subtypes('infernal darkness', []).
card_colors('infernal darkness', ['B']).
card_text('infernal darkness', 'Cumulative upkeep—Pay {B} and 1 life. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nIf a land is tapped for mana, it produces {B} instead of any other type.').
card_mana_cost('infernal darkness', ['2', 'B', 'B']).
card_cmc('infernal darkness', 4).
card_layout('infernal darkness', 'normal').

% Found in: ICE
card_name('infernal denizen', 'Infernal Denizen').
card_type('infernal denizen', 'Creature — Demon').
card_types('infernal denizen', ['Creature']).
card_subtypes('infernal denizen', ['Demon']).
card_colors('infernal denizen', ['B']).
card_text('infernal denizen', 'At the beginning of your upkeep, sacrifice two Swamps. If you can\'t, tap Infernal Denizen, and an opponent may gain control of a creature you control of his or her choice for as long as Infernal Denizen remains on the battlefield.\n{T}: Gain control of target creature for as long as Infernal Denizen remains on the battlefield.').
card_mana_cost('infernal denizen', ['7', 'B']).
card_cmc('infernal denizen', 8).
card_layout('infernal denizen', 'normal').
card_power('infernal denizen', 5).
card_toughness('infernal denizen', 7).
card_reserved('infernal denizen').

% Found in: PCY
card_name('infernal genesis', 'Infernal Genesis').
card_type('infernal genesis', 'Enchantment').
card_types('infernal genesis', ['Enchantment']).
card_subtypes('infernal genesis', []).
card_colors('infernal genesis', ['B']).
card_text('infernal genesis', 'At the beginning of each player\'s upkeep, that player puts the top card of his or her library into his or her graveyard. Then he or she puts X 1/1 black Minion creature tokens onto the battlefield, where X is that card\'s converted mana cost.').
card_mana_cost('infernal genesis', ['4', 'B', 'B']).
card_cmc('infernal genesis', 6).
card_layout('infernal genesis', 'normal').

% Found in: VIS
card_name('infernal harvest', 'Infernal Harvest').
card_type('infernal harvest', 'Sorcery').
card_types('infernal harvest', ['Sorcery']).
card_subtypes('infernal harvest', []).
card_colors('infernal harvest', ['B']).
card_text('infernal harvest', 'As an additional cost to cast Infernal Harvest, return X Swamps you control to their owner\'s hand.\nInfernal Harvest deals X damage divided as you choose among any number of target creatures.').
card_mana_cost('infernal harvest', ['1', 'B']).
card_cmc('infernal harvest', 2).
card_layout('infernal harvest', 'normal').

% Found in: SOK
card_name('infernal kirin', 'Infernal Kirin').
card_type('infernal kirin', 'Legendary Creature — Kirin Spirit').
card_types('infernal kirin', ['Creature']).
card_subtypes('infernal kirin', ['Kirin', 'Spirit']).
card_supertypes('infernal kirin', ['Legendary']).
card_colors('infernal kirin', ['B']).
card_text('infernal kirin', 'Flying\nWhenever you cast a Spirit or Arcane spell, target player reveals his or her hand and discards all cards with that spell\'s converted mana cost.').
card_mana_cost('infernal kirin', ['2', 'B', 'B']).
card_cmc('infernal kirin', 4).
card_layout('infernal kirin', 'normal').
card_power('infernal kirin', 3).
card_toughness('infernal kirin', 3).

% Found in: LEG
card_name('infernal medusa', 'Infernal Medusa').
card_type('infernal medusa', 'Creature — Gorgon').
card_types('infernal medusa', ['Creature']).
card_subtypes('infernal medusa', ['Gorgon']).
card_colors('infernal medusa', ['B']).
card_text('infernal medusa', 'Whenever Infernal Medusa blocks a creature, destroy that creature at end of combat.\nWhenever Infernal Medusa becomes blocked by a non-Wall creature, destroy that creature at end of combat.').
card_mana_cost('infernal medusa', ['3', 'B', 'B']).
card_cmc('infernal medusa', 5).
card_layout('infernal medusa', 'normal').
card_power('infernal medusa', 2).
card_toughness('infernal medusa', 4).

% Found in: C14
card_name('infernal offering', 'Infernal Offering').
card_type('infernal offering', 'Sorcery').
card_types('infernal offering', ['Sorcery']).
card_subtypes('infernal offering', []).
card_colors('infernal offering', ['B']).
card_text('infernal offering', 'Choose an opponent. You and that player each sacrifice a creature. Each player who sacrificed a creature this way draws two cards.\nChoose an opponent. Return a creature card from your graveyard to the battlefield, then that player returns a creature card from his or her graveyard to the battlefield.').
card_mana_cost('infernal offering', ['4', 'B']).
card_cmc('infernal offering', 5).
card_layout('infernal offering', 'normal').

% Found in: ISD
card_name('infernal plunge', 'Infernal Plunge').
card_type('infernal plunge', 'Sorcery').
card_types('infernal plunge', ['Sorcery']).
card_subtypes('infernal plunge', []).
card_colors('infernal plunge', ['R']).
card_text('infernal plunge', 'As an additional cost to cast Infernal Plunge, sacrifice a creature.\nAdd {R}{R}{R} to your mana pool.').
card_mana_cost('infernal plunge', ['R']).
card_cmc('infernal plunge', 1).
card_layout('infernal plunge', 'normal').

% Found in: ORI
card_name('infernal scarring', 'Infernal Scarring').
card_type('infernal scarring', 'Enchantment — Aura').
card_types('infernal scarring', ['Enchantment']).
card_subtypes('infernal scarring', ['Aura']).
card_colors('infernal scarring', ['B']).
card_text('infernal scarring', 'Enchant creature\nEnchanted creature gets +2/+0 and has \"When this creature dies, draw a card.\"').
card_mana_cost('infernal scarring', ['1', 'B']).
card_cmc('infernal scarring', 2).
card_layout('infernal scarring', 'normal').

% Found in: UGL
card_name('infernal spawn of evil', 'Infernal Spawn of Evil').
card_type('infernal spawn of evil', 'Creature — Demon Beast').
card_types('infernal spawn of evil', ['Creature']).
card_subtypes('infernal spawn of evil', ['Demon', 'Beast']).
card_colors('infernal spawn of evil', ['B']).
card_text('infernal spawn of evil', 'Flying, first strike\n{1}{B}, Reveal Infernal Spawn of Evil from your hand, Say \"It\'s coming\": Infernal Spawn of Evil deals 1 damage to target opponent. Use this ability only during your upkeep and only once each upkeep.').
card_mana_cost('infernal spawn of evil', ['6', 'B', 'B', 'B']).
card_cmc('infernal spawn of evil', 9).
card_layout('infernal spawn of evil', 'normal').
card_power('infernal spawn of evil', 7).
card_toughness('infernal spawn of evil', 7).

% Found in: UNH
card_name('infernal spawn of infernal spawn of evil', 'Infernal Spawn of Infernal Spawn of Evil').
card_type('infernal spawn of infernal spawn of evil', 'Creature — Demon Child').
card_types('infernal spawn of infernal spawn of evil', ['Creature']).
card_subtypes('infernal spawn of infernal spawn of evil', ['Demon', 'Child']).
card_colors('infernal spawn of infernal spawn of evil', ['B']).
card_text('infernal spawn of infernal spawn of evil', 'Flying, first strike, trample\nIf you say \"I\'m coming, too\" as you search your library, you may pay {1}{B} and reveal Infernal Spawn of Infernal Spawn of Evil from your library to have it deal 2 damage to a player of your choice. Do this no more than once each turn.').
card_mana_cost('infernal spawn of infernal spawn of evil', ['8', 'B', 'B']).
card_cmc('infernal spawn of infernal spawn of evil', 10).
card_layout('infernal spawn of infernal spawn of evil', 'normal').
card_power('infernal spawn of infernal spawn of evil', 8).
card_toughness('infernal spawn of infernal spawn of evil', 8).

% Found in: WTH
card_name('infernal tribute', 'Infernal Tribute').
card_type('infernal tribute', 'Enchantment').
card_types('infernal tribute', ['Enchantment']).
card_subtypes('infernal tribute', []).
card_colors('infernal tribute', ['B']).
card_text('infernal tribute', '{2}, Sacrifice a nontoken permanent: Draw a card.').
card_mana_cost('infernal tribute', ['B', 'B', 'B']).
card_cmc('infernal tribute', 3).
card_layout('infernal tribute', 'normal').
card_reserved('infernal tribute').

% Found in: DIS
card_name('infernal tutor', 'Infernal Tutor').
card_type('infernal tutor', 'Sorcery').
card_types('infernal tutor', ['Sorcery']).
card_subtypes('infernal tutor', []).
card_colors('infernal tutor', ['B']).
card_text('infernal tutor', 'Reveal a card from your hand. Search your library for a card with the same name as that card, reveal it, put it into your hand, then shuffle your library.\nHellbent — If you have no cards in hand, instead search your library for a card, put it into your hand, then shuffle your library.').
card_mana_cost('infernal tutor', ['1', 'B']).
card_cmc('infernal tutor', 2).
card_layout('infernal tutor', 'normal').

% Found in: 4ED, 5ED, 6ED, 7ED, 8ED, DRK
card_name('inferno', 'Inferno').
card_type('inferno', 'Instant').
card_types('inferno', ['Instant']).
card_subtypes('inferno', []).
card_colors('inferno', ['R']).
card_text('inferno', 'Inferno deals 6 damage to each creature and each player.').
card_mana_cost('inferno', ['5', 'R', 'R']).
card_cmc('inferno', 7).
card_layout('inferno', 'normal').

% Found in: M10
card_name('inferno elemental', 'Inferno Elemental').
card_type('inferno elemental', 'Creature — Elemental').
card_types('inferno elemental', ['Creature']).
card_subtypes('inferno elemental', ['Elemental']).
card_colors('inferno elemental', ['R']).
card_text('inferno elemental', 'Whenever Inferno Elemental blocks or becomes blocked by a creature, Inferno Elemental deals 3 damage to that creature.').
card_mana_cost('inferno elemental', ['4', 'R', 'R']).
card_cmc('inferno elemental', 6).
card_layout('inferno elemental', 'normal').
card_power('inferno elemental', 4).
card_toughness('inferno elemental', 4).

% Found in: M15
card_name('inferno fist', 'Inferno Fist').
card_type('inferno fist', 'Enchantment — Aura').
card_types('inferno fist', ['Enchantment']).
card_subtypes('inferno fist', ['Aura']).
card_colors('inferno fist', ['R']).
card_text('inferno fist', 'Enchant creature you control\nEnchanted creature gets +2/+0.\n{R}, Sacrifice Inferno Fist: Inferno Fist deals 2 damage to target creature or player.').
card_mana_cost('inferno fist', ['1', 'R']).
card_cmc('inferno fist', 2).
card_layout('inferno fist', 'normal').

% Found in: C13, M11, M12, pMEI
card_name('inferno titan', 'Inferno Titan').
card_type('inferno titan', 'Creature — Giant').
card_types('inferno titan', ['Creature']).
card_subtypes('inferno titan', ['Giant']).
card_colors('inferno titan', ['R']).
card_text('inferno titan', '{R}: Inferno Titan gets +1/+0 until end of turn.\nWhenever Inferno Titan enters the battlefield or attacks, it deals 3 damage divided as you choose among one, two, or three target creatures and/or players.').
card_mana_cost('inferno titan', ['4', 'R', 'R']).
card_cmc('inferno titan', 6).
card_layout('inferno titan', 'normal').
card_power('inferno titan', 6).
card_toughness('inferno titan', 6).

% Found in: ARC, DDN, ZEN
card_name('inferno trap', 'Inferno Trap').
card_type('inferno trap', 'Instant — Trap').
card_types('inferno trap', ['Instant']).
card_subtypes('inferno trap', ['Trap']).
card_colors('inferno trap', ['R']).
card_text('inferno trap', 'If you\'ve been dealt damage by two or more creatures this turn, you may pay {R} rather than pay Inferno Trap\'s mana cost.\nInferno Trap deals 4 damage to target creature.').
card_mana_cost('inferno trap', ['3', 'R']).
card_cmc('inferno trap', 4).
card_layout('inferno trap', 'normal').

% Found in: ALA, ARC, C13, ONS, pMPR
card_name('infest', 'Infest').
card_type('infest', 'Sorcery').
card_types('infest', ['Sorcery']).
card_subtypes('infest', []).
card_colors('infest', ['B']).
card_text('infest', 'All creatures get -2/-2 until end of turn.').
card_mana_cost('infest', ['1', 'B', 'B']).
card_cmc('infest', 3).
card_layout('infest', 'normal').

% Found in: DST
card_name('infested roothold', 'Infested Roothold').
card_type('infested roothold', 'Creature — Wall').
card_types('infested roothold', ['Creature']).
card_subtypes('infested roothold', ['Wall']).
card_colors('infested roothold', ['G']).
card_text('infested roothold', 'Defender (This creature can\'t attack.)\nProtection from artifacts\nWhenever an opponent casts an artifact spell, you may put a 1/1 green Insect creature token onto the battlefield.').
card_mana_cost('infested roothold', ['4', 'G']).
card_cmc('infested roothold', 5).
card_layout('infested roothold', 'normal').
card_power('infested roothold', 0).
card_toughness('infested roothold', 3).

% Found in: NMS
card_name('infiltrate', 'Infiltrate').
card_type('infiltrate', 'Instant').
card_types('infiltrate', ['Instant']).
card_subtypes('infiltrate', []).
card_colors('infiltrate', ['U']).
card_text('infiltrate', 'Target creature can\'t be blocked this turn.').
card_mana_cost('infiltrate', ['U']).
card_cmc('infiltrate', 1).
card_layout('infiltrate', 'normal').

% Found in: SOM
card_name('infiltration lens', 'Infiltration Lens').
card_type('infiltration lens', 'Artifact — Equipment').
card_types('infiltration lens', ['Artifact']).
card_subtypes('infiltration lens', ['Equipment']).
card_colors('infiltration lens', []).
card_text('infiltration lens', 'Whenever equipped creature becomes blocked by a creature, you may draw two cards.\nEquip {1}').
card_mana_cost('infiltration lens', ['1']).
card_cmc('infiltration lens', 1).
card_layout('infiltration lens', 'normal').

% Found in: FUT
card_name('infiltrator il-kor', 'Infiltrator il-Kor').
card_type('infiltrator il-kor', 'Creature — Kor Rogue').
card_types('infiltrator il-kor', ['Creature']).
card_subtypes('infiltrator il-kor', ['Kor', 'Rogue']).
card_colors('infiltrator il-kor', ['U']).
card_text('infiltrator il-kor', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nSuspend 2—{1}{U} (Rather than cast this card from your hand, you may pay {1}{U} and exile it with two time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_mana_cost('infiltrator il-kor', ['4', 'U']).
card_cmc('infiltrator il-kor', 5).
card_layout('infiltrator il-kor', 'normal').
card_power('infiltrator il-kor', 3).
card_toughness('infiltrator il-kor', 1).

% Found in: GPT
card_name('infiltrator\'s magemark', 'Infiltrator\'s Magemark').
card_type('infiltrator\'s magemark', 'Enchantment — Aura').
card_types('infiltrator\'s magemark', ['Enchantment']).
card_subtypes('infiltrator\'s magemark', ['Aura']).
card_colors('infiltrator\'s magemark', ['U']).
card_text('infiltrator\'s magemark', 'Enchant creature\nCreatures you control that are enchanted get +1/+1 and can\'t be blocked except by creatures with defender.').
card_mana_cost('infiltrator\'s magemark', ['2', 'U']).
card_cmc('infiltrator\'s magemark', 3).
card_layout('infiltrator\'s magemark', 'normal').

% Found in: LEG
card_name('infinite authority', 'Infinite Authority').
card_type('infinite authority', 'Enchantment — Aura').
card_types('infinite authority', ['Enchantment']).
card_subtypes('infinite authority', ['Aura']).
card_colors('infinite authority', ['W']).
card_text('infinite authority', 'Enchant creature\nWhenever enchanted creature blocks or becomes blocked by a creature with toughness 3 or less, destroy the other creature at end of combat. At the beginning of the next end step, if that creature was destroyed this way, put a +1/+1 counter on the first creature.').
card_mana_cost('infinite authority', ['W', 'W', 'W']).
card_cmc('infinite authority', 3).
card_layout('infinite authority', 'normal').
card_reserved('infinite authority').

% Found in: 5ED, ICE
card_name('infinite hourglass', 'Infinite Hourglass').
card_type('infinite hourglass', 'Artifact').
card_types('infinite hourglass', ['Artifact']).
card_subtypes('infinite hourglass', []).
card_colors('infinite hourglass', []).
card_text('infinite hourglass', 'At the beginning of your upkeep, put a time counter on Infinite Hourglass.\nAll creatures get +1/+0 for each time counter on Infinite Hourglass.\n{3}: Remove a time counter from Infinite Hourglass. Any player may activate this ability but only during any upkeep step.').
card_mana_cost('infinite hourglass', ['4']).
card_cmc('infinite hourglass', 4).
card_layout('infinite hourglass', 'normal').

% Found in: ORI
card_name('infinite obliteration', 'Infinite Obliteration').
card_type('infinite obliteration', 'Sorcery').
card_types('infinite obliteration', ['Sorcery']).
card_subtypes('infinite obliteration', []).
card_colors('infinite obliteration', ['B']).
card_text('infinite obliteration', 'Name a creature card. Search target opponent\'s graveyard, hand, and library for any number of cards with that name and exile them. Then that player shuffles his or her library.').
card_mana_cost('infinite obliteration', ['1', 'B', 'B']).
card_cmc('infinite obliteration', 3).
card_layout('infinite obliteration', 'normal').

% Found in: AVR, C14
card_name('infinite reflection', 'Infinite Reflection').
card_type('infinite reflection', 'Enchantment — Aura').
card_types('infinite reflection', ['Enchantment']).
card_subtypes('infinite reflection', ['Aura']).
card_colors('infinite reflection', ['U']).
card_text('infinite reflection', 'Enchant creature\nWhen Infinite Reflection enters the battlefield attached to a creature, each other nontoken creature you control becomes a copy of that creature.\nNontoken creatures you control enter the battlefield as a copy of enchanted creature.').
card_mana_cost('infinite reflection', ['5', 'U']).
card_cmc('infinite reflection', 6).
card_layout('infinite reflection', 'normal').

% Found in: DST, PCY
card_name('inflame', 'Inflame').
card_type('inflame', 'Instant').
card_types('inflame', ['Instant']).
card_subtypes('inflame', []).
card_colors('inflame', ['R']).
card_text('inflame', 'Inflame deals 2 damage to each creature dealt damage this turn.').
card_mana_cost('inflame', ['R']).
card_cmc('inflame', 1).
card_layout('inflame', 'normal').

% Found in: ONS
card_name('information dealer', 'Information Dealer').
card_type('information dealer', 'Creature — Human Wizard').
card_types('information dealer', ['Creature']).
card_subtypes('information dealer', ['Human', 'Wizard']).
card_colors('information dealer', ['U']).
card_text('information dealer', '{T}: Look at the top X cards of your library, where X is the number of Wizards on the battlefield, then put them back in any order.').
card_mana_cost('information dealer', ['1', 'U']).
card_cmc('information dealer', 2).
card_layout('information dealer', 'normal').
card_power('information dealer', 1).
card_toughness('information dealer', 1).

% Found in: ICE, ME3
card_name('infuse', 'Infuse').
card_type('infuse', 'Instant').
card_types('infuse', ['Instant']).
card_subtypes('infuse', []).
card_colors('infuse', ['U']).
card_text('infuse', 'Untap target artifact, creature, or land.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('infuse', ['2', 'U']).
card_cmc('infuse', 3).
card_layout('infuse', 'normal').

% Found in: BFZ
card_name('infuse with the elements', 'Infuse with the Elements').
card_type('infuse with the elements', 'Instant').
card_types('infuse with the elements', ['Instant']).
card_subtypes('infuse with the elements', []).
card_colors('infuse with the elements', ['G']).
card_text('infuse with the elements', 'Converge — Put X +1/+1 counters on target creature, where X is the number of colors of mana spent to cast Infuse with the Elements. That creature gains trample until end of turn.').
card_mana_cost('infuse with the elements', ['3', 'G']).
card_cmc('infuse with the elements', 4).
card_layout('infuse with the elements', 'normal').

% Found in: 5DN
card_name('infused arrows', 'Infused Arrows').
card_type('infused arrows', 'Artifact').
card_types('infused arrows', ['Artifact']).
card_subtypes('infused arrows', []).
card_colors('infused arrows', []).
card_text('infused arrows', 'Sunburst (This enters the battlefield with a charge counter on it for each color of mana spent to cast it.)\n{T}, Remove X charge counters from Infused Arrows: Target creature gets -X/-X until end of turn.').
card_mana_cost('infused arrows', ['4']).
card_cmc('infused arrows', 4).
card_layout('infused arrows', 'normal').

% Found in: POR, S99
card_name('ingenious thief', 'Ingenious Thief').
card_type('ingenious thief', 'Creature — Human Rogue').
card_types('ingenious thief', ['Creature']).
card_subtypes('ingenious thief', ['Human', 'Rogue']).
card_colors('ingenious thief', ['U']).
card_text('ingenious thief', 'Flying\nWhen Ingenious Thief enters the battlefield, look at target player\'s hand.').
card_mana_cost('ingenious thief', ['1', 'U']).
card_cmc('ingenious thief', 2).
card_layout('ingenious thief', 'normal').
card_power('ingenious thief', 1).
card_toughness('ingenious thief', 1).

% Found in: C14, DD2, DD3_JVC, LRW
card_name('ingot chewer', 'Ingot Chewer').
card_type('ingot chewer', 'Creature — Elemental').
card_types('ingot chewer', ['Creature']).
card_subtypes('ingot chewer', ['Elemental']).
card_colors('ingot chewer', ['R']).
card_text('ingot chewer', 'When Ingot Chewer enters the battlefield, destroy target artifact.\nEvoke {R} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('ingot chewer', ['4', 'R']).
card_cmc('ingot chewer', 5).
card_layout('ingot chewer', 'normal').
card_power('ingot chewer', 3).
card_toughness('ingot chewer', 3).

% Found in: ALL, ME2
card_name('inheritance', 'Inheritance').
card_type('inheritance', 'Enchantment').
card_types('inheritance', ['Enchantment']).
card_subtypes('inheritance', []).
card_colors('inheritance', ['W']).
card_text('inheritance', 'Whenever a creature dies, you may pay {3}. If you do, draw a card.').
card_mana_cost('inheritance', ['W']).
card_cmc('inheritance', 1).
card_layout('inheritance', 'normal').

% Found in: CHK
card_name('initiate of blood', 'Initiate of Blood').
card_type('initiate of blood', 'Creature — Ogre Shaman').
card_types('initiate of blood', ['Creature']).
card_subtypes('initiate of blood', ['Ogre', 'Shaman']).
card_colors('initiate of blood', ['R']).
card_text('initiate of blood', '{T}: Initiate of Blood deals 1 damage to target creature that was dealt damage this turn. When that creature dies this turn, flip Initiate of Blood.').
card_mana_cost('initiate of blood', ['3', 'R']).
card_cmc('initiate of blood', 4).
card_layout('initiate of blood', 'flip').
card_power('initiate of blood', 2).
card_toughness('initiate of blood', 2).
card_sides('initiate of blood', 'goka the unjust').

% Found in: 5ED, FEM
card_name('initiates of the ebon hand', 'Initiates of the Ebon Hand').
card_type('initiates of the ebon hand', 'Creature — Cleric').
card_types('initiates of the ebon hand', ['Creature']).
card_subtypes('initiates of the ebon hand', ['Cleric']).
card_colors('initiates of the ebon hand', ['B']).
card_text('initiates of the ebon hand', '{1}: Add {B} to your mana pool. If this ability has been activated four or more times this turn, sacrifice Initiates of the Ebon Hand at the beginning of the next end step.').
card_mana_cost('initiates of the ebon hand', ['B']).
card_cmc('initiates of the ebon hand', 1).
card_layout('initiates of the ebon hand', 'normal').
card_power('initiates of the ebon hand', 1).
card_toughness('initiates of the ebon hand', 1).

% Found in: MOR
card_name('ink dissolver', 'Ink Dissolver').
card_type('ink dissolver', 'Creature — Merfolk Wizard').
card_types('ink dissolver', ['Creature']).
card_subtypes('ink dissolver', ['Merfolk', 'Wizard']).
card_colors('ink dissolver', ['U']).
card_text('ink dissolver', 'Kinship — At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Ink Dissolver, you may reveal it. If you do, each opponent puts the top three cards of his or her library into his or her graveyard.').
card_mana_cost('ink dissolver', ['1', 'U']).
card_cmc('ink dissolver', 2).
card_layout('ink dissolver', 'normal').
card_power('ink dissolver', 2).
card_toughness('ink dissolver', 1).

% Found in: BOK, PC2, V13, pPRE
card_name('ink-eyes, servant of oni', 'Ink-Eyes, Servant of Oni').
card_type('ink-eyes, servant of oni', 'Legendary Creature — Rat Ninja').
card_types('ink-eyes, servant of oni', ['Creature']).
card_subtypes('ink-eyes, servant of oni', ['Rat', 'Ninja']).
card_supertypes('ink-eyes, servant of oni', ['Legendary']).
card_colors('ink-eyes, servant of oni', ['B']).
card_text('ink-eyes, servant of oni', 'Ninjutsu {3}{B}{B} ({3}{B}{B}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Ink-Eyes, Servant of Oni deals combat damage to a player, you may put target creature card from that player\'s graveyard onto the battlefield under your control.\n{1}{B}: Regenerate Ink-Eyes.').
card_mana_cost('ink-eyes, servant of oni', ['4', 'B', 'B']).
card_cmc('ink-eyes, servant of oni', 6).
card_layout('ink-eyes, servant of oni', 'normal').
card_power('ink-eyes, servant of oni', 5).
card_toughness('ink-eyes, servant of oni', 4).

% Found in: VAN
card_name('ink-eyes, servant of oni avatar', 'Ink-Eyes, Servant of Oni Avatar').
card_type('ink-eyes, servant of oni avatar', 'Vanguard').
card_types('ink-eyes, servant of oni avatar', ['Vanguard']).
card_subtypes('ink-eyes, servant of oni avatar', []).
card_colors('ink-eyes, servant of oni avatar', []).
card_text('ink-eyes, servant of oni avatar', 'At the beginning of the game, look at target opponent\'s hand and choose a nonland card from it. That player discards that card.\n{X}, Pay X life: Put target creature card with converted mana cost X from an opponent\'s graveyard onto the battlefield under your control.').
card_layout('ink-eyes, servant of oni avatar', 'vanguard').

% Found in: GPT
card_name('ink-treader nephilim', 'Ink-Treader Nephilim').
card_type('ink-treader nephilim', 'Creature — Nephilim').
card_types('ink-treader nephilim', ['Creature']).
card_subtypes('ink-treader nephilim', ['Nephilim']).
card_colors('ink-treader nephilim', ['W', 'U', 'R', 'G']).
card_text('ink-treader nephilim', 'Whenever a player casts an instant or sorcery spell, if that spell targets only Ink-Treader Nephilim, copy the spell for each other creature that spell could target. Each copy targets a different one of those creatures.').
card_mana_cost('ink-treader nephilim', ['R', 'G', 'W', 'U']).
card_cmc('ink-treader nephilim', 4).
card_layout('ink-treader nephilim', 'normal').
card_power('ink-treader nephilim', 3).
card_toughness('ink-treader nephilim', 3).

% Found in: LRW
card_name('inkfathom divers', 'Inkfathom Divers').
card_type('inkfathom divers', 'Creature — Merfolk Soldier').
card_types('inkfathom divers', ['Creature']).
card_subtypes('inkfathom divers', ['Merfolk', 'Soldier']).
card_colors('inkfathom divers', ['U']).
card_text('inkfathom divers', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nWhen Inkfathom Divers enters the battlefield, look at the top four cards of your library, then put them back in any order.').
card_mana_cost('inkfathom divers', ['3', 'U', 'U']).
card_cmc('inkfathom divers', 5).
card_layout('inkfathom divers', 'normal').
card_power('inkfathom divers', 3).
card_toughness('inkfathom divers', 3).

% Found in: SHM
card_name('inkfathom infiltrator', 'Inkfathom Infiltrator').
card_type('inkfathom infiltrator', 'Creature — Merfolk Rogue').
card_types('inkfathom infiltrator', ['Creature']).
card_subtypes('inkfathom infiltrator', ['Merfolk', 'Rogue']).
card_colors('inkfathom infiltrator', ['U', 'B']).
card_text('inkfathom infiltrator', 'Inkfathom Infiltrator can\'t block and can\'t be blocked.').
card_mana_cost('inkfathom infiltrator', ['U/B', 'U/B']).
card_cmc('inkfathom infiltrator', 2).
card_layout('inkfathom infiltrator', 'normal').
card_power('inkfathom infiltrator', 2).
card_toughness('inkfathom infiltrator', 1).

% Found in: PC2, SHM
card_name('inkfathom witch', 'Inkfathom Witch').
card_type('inkfathom witch', 'Creature — Merfolk Wizard').
card_types('inkfathom witch', ['Creature']).
card_subtypes('inkfathom witch', ['Merfolk', 'Wizard']).
card_colors('inkfathom witch', ['U', 'B']).
card_text('inkfathom witch', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\n{2}{U}{B}: Each unblocked creature has base power and toughness 4/1 until end of turn.').
card_mana_cost('inkfathom witch', ['1', 'U/B']).
card_cmc('inkfathom witch', 2).
card_layout('inkfathom witch', 'normal').
card_power('inkfathom witch', 1).
card_toughness('inkfathom witch', 1).

% Found in: MBS
card_name('inkmoth nexus', 'Inkmoth Nexus').
card_type('inkmoth nexus', 'Land').
card_types('inkmoth nexus', ['Land']).
card_subtypes('inkmoth nexus', []).
card_colors('inkmoth nexus', []).
card_text('inkmoth nexus', '{T}: Add {1} to your mana pool.\n{1}: Inkmoth Nexus becomes a 1/1 Blinkmoth artifact creature with flying and infect until end of turn. It\'s still a land. (It deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_layout('inkmoth nexus', 'normal').

% Found in: CON, DDO, PD3
card_name('inkwell leviathan', 'Inkwell Leviathan').
card_type('inkwell leviathan', 'Artifact Creature — Leviathan').
card_types('inkwell leviathan', ['Artifact', 'Creature']).
card_subtypes('inkwell leviathan', ['Leviathan']).
card_colors('inkwell leviathan', ['U']).
card_text('inkwell leviathan', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nTrample\nShroud (This creature can\'t be the target of spells or abilities.)').
card_mana_cost('inkwell leviathan', ['7', 'U', 'U']).
card_cmc('inkwell leviathan', 9).
card_layout('inkwell leviathan', 'normal').
card_power('inkwell leviathan', 7).
card_toughness('inkwell leviathan', 11).

% Found in: SOK
card_name('inner calm, outer strength', 'Inner Calm, Outer Strength').
card_type('inner calm, outer strength', 'Instant — Arcane').
card_types('inner calm, outer strength', ['Instant']).
card_subtypes('inner calm, outer strength', ['Arcane']).
card_colors('inner calm, outer strength', ['G']).
card_text('inner calm, outer strength', 'Target creature gets +X/+X until end of turn, where X is the number of cards in your hand.').
card_mana_cost('inner calm, outer strength', ['2', 'G']).
card_cmc('inner calm, outer strength', 3).
card_layout('inner calm, outer strength', 'normal').

% Found in: SOK
card_name('inner fire', 'Inner Fire').
card_type('inner fire', 'Sorcery').
card_types('inner fire', ['Sorcery']).
card_subtypes('inner fire', []).
card_colors('inner fire', ['R']).
card_text('inner fire', 'Add {R} to your mana pool for each card in your hand.').
card_mana_cost('inner fire', ['3', 'R']).
card_cmc('inner fire', 4).
card_layout('inner fire', 'normal').

% Found in: WTH
card_name('inner sanctum', 'Inner Sanctum').
card_type('inner sanctum', 'Enchantment').
card_types('inner sanctum', ['Enchantment']).
card_subtypes('inner sanctum', []).
card_colors('inner sanctum', ['W']).
card_text('inner sanctum', 'Cumulative upkeep—Pay 2 life. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nPrevent all damage that would be dealt to creatures you control.').
card_mana_cost('inner sanctum', ['1', 'W', 'W']).
card_cmc('inner sanctum', 3).
card_layout('inner sanctum', 'normal').
card_reserved('inner sanctum').

% Found in: SOK
card_name('inner-chamber guard', 'Inner-Chamber Guard').
card_type('inner-chamber guard', 'Creature — Human Samurai').
card_types('inner-chamber guard', ['Creature']).
card_subtypes('inner-chamber guard', ['Human', 'Samurai']).
card_colors('inner-chamber guard', ['W']).
card_text('inner-chamber guard', 'Bushido 2 (Whenever this creature blocks or becomes blocked, it gets +2/+2 until end of turn.)').
card_mana_cost('inner-chamber guard', ['1', 'W']).
card_cmc('inner-chamber guard', 2).
card_layout('inner-chamber guard', 'normal').
card_power('inner-chamber guard', 0).
card_toughness('inner-chamber guard', 2).

% Found in: DD2, DD3_JVC, LRW
card_name('inner-flame acolyte', 'Inner-Flame Acolyte').
card_type('inner-flame acolyte', 'Creature — Elemental Shaman').
card_types('inner-flame acolyte', ['Creature']).
card_subtypes('inner-flame acolyte', ['Elemental', 'Shaman']).
card_colors('inner-flame acolyte', ['R']).
card_text('inner-flame acolyte', 'When Inner-Flame Acolyte enters the battlefield, target creature gets +2/+0 and gains haste until end of turn.\nEvoke {R} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('inner-flame acolyte', ['1', 'R', 'R']).
card_cmc('inner-flame acolyte', 3).
card_layout('inner-flame acolyte', 'normal').
card_power('inner-flame acolyte', 2).
card_toughness('inner-flame acolyte', 2).

% Found in: LRW, MM2
card_name('inner-flame igniter', 'Inner-Flame Igniter').
card_type('inner-flame igniter', 'Creature — Elemental Warrior').
card_types('inner-flame igniter', ['Creature']).
card_subtypes('inner-flame igniter', ['Elemental', 'Warrior']).
card_colors('inner-flame igniter', ['R']).
card_text('inner-flame igniter', '{2}{R}: Creatures you control get +1/+0 until end of turn. If this is the third time this ability has resolved this turn, creatures you control gain first strike until end of turn.').
card_mana_cost('inner-flame igniter', ['2', 'R']).
card_cmc('inner-flame igniter', 3).
card_layout('inner-flame igniter', 'normal').
card_power('inner-flame igniter', 2).
card_toughness('inner-flame igniter', 2).

% Found in: CHK
card_name('innocence kami', 'Innocence Kami').
card_type('innocence kami', 'Creature — Spirit').
card_types('innocence kami', ['Creature']).
card_subtypes('innocence kami', ['Spirit']).
card_colors('innocence kami', ['W']).
card_text('innocence kami', '{W}, {T}: Tap target creature.\nWhenever you cast a Spirit or Arcane spell, untap Innocence Kami.').
card_mana_cost('innocence kami', ['3', 'W', 'W']).
card_cmc('innocence kami', 5).
card_layout('innocence kami', 'normal').
card_power('innocence kami', 2).
card_toughness('innocence kami', 3).

% Found in: HOP, ODY
card_name('innocent blood', 'Innocent Blood').
card_type('innocent blood', 'Sorcery').
card_types('innocent blood', ['Sorcery']).
card_subtypes('innocent blood', []).
card_colors('innocent blood', ['B']).
card_text('innocent blood', 'Each player sacrifices a creature.').
card_mana_cost('innocent blood', ['B']).
card_cmc('innocent blood', 1).
card_layout('innocent blood', 'normal').

% Found in: DRK
card_name('inquisition', 'Inquisition').
card_type('inquisition', 'Sorcery').
card_types('inquisition', ['Sorcery']).
card_subtypes('inquisition', []).
card_colors('inquisition', ['B']).
card_text('inquisition', 'Target player reveals his or her hand. Inquisition deals damage to that player equal to the number of white cards in his or her hand.').
card_mana_cost('inquisition', ['2', 'B']).
card_cmc('inquisition', 3).
card_layout('inquisition', 'normal').

% Found in: MD1, ROE
card_name('inquisition of kozilek', 'Inquisition of Kozilek').
card_type('inquisition of kozilek', 'Sorcery').
card_types('inquisition of kozilek', ['Sorcery']).
card_subtypes('inquisition of kozilek', []).
card_colors('inquisition of kozilek', ['B']).
card_text('inquisition of kozilek', 'Target player reveals his or her hand. You choose a nonland card from it with converted mana cost 3 or less. That player discards that card.').
card_mana_cost('inquisition of kozilek', ['B']).
card_cmc('inquisition of kozilek', 1).
card_layout('inquisition of kozilek', 'normal').

% Found in: NPH
card_name('inquisitor exarch', 'Inquisitor Exarch').
card_type('inquisitor exarch', 'Creature — Cleric').
card_types('inquisitor exarch', ['Creature']).
card_subtypes('inquisitor exarch', ['Cleric']).
card_colors('inquisitor exarch', ['W']).
card_text('inquisitor exarch', 'When Inquisitor Exarch enters the battlefield, choose one —\n• You gain 2 life.\n• Target opponent loses 2 life.').
card_mana_cost('inquisitor exarch', ['W', 'W']).
card_cmc('inquisitor exarch', 2).
card_layout('inquisitor exarch', 'normal').
card_power('inquisitor exarch', 2).
card_toughness('inquisitor exarch', 2).

% Found in: ISD
card_name('inquisitor\'s flail', 'Inquisitor\'s Flail').
card_type('inquisitor\'s flail', 'Artifact — Equipment').
card_types('inquisitor\'s flail', ['Artifact']).
card_subtypes('inquisitor\'s flail', ['Equipment']).
card_colors('inquisitor\'s flail', []).
card_text('inquisitor\'s flail', 'If equipped creature would deal combat damage, it deals double that damage instead.\nIf another creature would deal combat damage to equipped creature, it deals double that damage to equipped creature instead.\nEquip {2}').
card_mana_cost('inquisitor\'s flail', ['2']).
card_cmc('inquisitor\'s flail', 2).
card_layout('inquisitor\'s flail', 'normal').

% Found in: SHM
card_name('inquisitor\'s snare', 'Inquisitor\'s Snare').
card_type('inquisitor\'s snare', 'Instant').
card_types('inquisitor\'s snare', ['Instant']).
card_subtypes('inquisitor\'s snare', []).
card_colors('inquisitor\'s snare', ['W']).
card_text('inquisitor\'s snare', 'Prevent all damage target attacking or blocking creature would deal this turn. If that creature is black or red, destroy it.').
card_mana_cost('inquisitor\'s snare', ['1', 'W']).
card_cmc('inquisitor\'s snare', 2).
card_layout('inquisitor\'s snare', 'normal').

% Found in: THS
card_name('insatiable harpy', 'Insatiable Harpy').
card_type('insatiable harpy', 'Creature — Harpy').
card_types('insatiable harpy', ['Creature']).
card_subtypes('insatiable harpy', ['Harpy']).
card_colors('insatiable harpy', ['B']).
card_text('insatiable harpy', 'Flying, lifelink').
card_mana_cost('insatiable harpy', ['2', 'B', 'B']).
card_cmc('insatiable harpy', 4).
card_layout('insatiable harpy', 'normal').
card_power('insatiable harpy', 2).
card_toughness('insatiable harpy', 2).

% Found in: NPH
card_name('insatiable souleater', 'Insatiable Souleater').
card_type('insatiable souleater', 'Artifact Creature — Beast').
card_types('insatiable souleater', ['Artifact', 'Creature']).
card_subtypes('insatiable souleater', ['Beast']).
card_colors('insatiable souleater', []).
card_text('insatiable souleater', '{G/P}: Insatiable Souleater gains trample until end of turn. ({G/P} can be paid with either {G} or 2 life.)').
card_mana_cost('insatiable souleater', ['4']).
card_cmc('insatiable souleater', 4).
card_layout('insatiable souleater', 'normal').
card_power('insatiable souleater', 5).
card_toughness('insatiable souleater', 1).

% Found in: ISD
card_name('insectile aberration', 'Insectile Aberration').
card_type('insectile aberration', 'Creature — Human Insect').
card_types('insectile aberration', ['Creature']).
card_subtypes('insectile aberration', ['Human', 'Insect']).
card_colors('insectile aberration', ['U']).
card_text('insectile aberration', 'Flying').
card_layout('insectile aberration', 'double-faced').
card_power('insectile aberration', 3).
card_toughness('insectile aberration', 2).

% Found in: EVE
card_name('inside out', 'Inside Out').
card_type('inside out', 'Instant').
card_types('inside out', ['Instant']).
card_subtypes('inside out', []).
card_colors('inside out', ['U', 'R']).
card_text('inside out', 'Switch target creature\'s power and toughness until end of turn.\nDraw a card.').
card_mana_cost('inside out', ['1', 'U/R']).
card_cmc('inside out', 2).
card_layout('inside out', 'normal').

% Found in: ALL, CST
card_name('insidious bookworms', 'Insidious Bookworms').
card_type('insidious bookworms', 'Creature — Worm').
card_types('insidious bookworms', ['Creature']).
card_subtypes('insidious bookworms', ['Worm']).
card_colors('insidious bookworms', ['B']).
card_text('insidious bookworms', 'When Insidious Bookworms dies, you may pay {1}{B}. If you do, target player discards a card at random.').
card_mana_cost('insidious bookworms', ['B']).
card_cmc('insidious bookworms', 1).
card_layout('insidious bookworms', 'normal').
card_power('insidious bookworms', 1).
card_toughness('insidious bookworms', 1).

% Found in: TOR
card_name('insidious dreams', 'Insidious Dreams').
card_type('insidious dreams', 'Instant').
card_types('insidious dreams', ['Instant']).
card_subtypes('insidious dreams', []).
card_colors('insidious dreams', ['B']).
card_text('insidious dreams', 'As an additional cost to cast Insidious Dreams, discard X cards.\nSearch your library for X cards. Then shuffle your library and put those cards on top of it in any order.').
card_mana_cost('insidious dreams', ['3', 'B']).
card_cmc('insidious dreams', 4).
card_layout('insidious dreams', 'normal').

% Found in: 6ED, TMP
card_name('insight', 'Insight').
card_type('insight', 'Enchantment').
card_types('insight', ['Enchantment']).
card_subtypes('insight', []).
card_colors('insight', ['U']).
card_text('insight', 'Whenever an opponent casts a green spell, you draw a card.').
card_mana_cost('insight', ['2', 'U']).
card_cmc('insight', 3).
card_layout('insight', 'normal').

% Found in: TOR
card_name('insist', 'Insist').
card_type('insist', 'Sorcery').
card_types('insist', ['Sorcery']).
card_subtypes('insist', []).
card_colors('insist', ['G']).
card_text('insist', 'The next creature spell you cast this turn can\'t be countered by spells or abilities.\nDraw a card.').
card_mana_cost('insist', ['G']).
card_cmc('insist', 1).
card_layout('insist', 'normal').

% Found in: PLS
card_name('insolence', 'Insolence').
card_type('insolence', 'Enchantment — Aura').
card_types('insolence', ['Enchantment']).
card_subtypes('insolence', ['Aura']).
card_colors('insolence', ['R']).
card_text('insolence', 'Enchant creature\nWhenever enchanted creature becomes tapped, Insolence deals 2 damage to that creature\'s controller.').
card_mana_cost('insolence', ['2', 'R']).
card_cmc('insolence', 3).
card_layout('insolence', 'normal').

% Found in: 6ED, 7ED, 8ED, RTR, S00, VIS
card_name('inspiration', 'Inspiration').
card_type('inspiration', 'Instant').
card_types('inspiration', ['Instant']).
card_subtypes('inspiration', []).
card_colors('inspiration', ['U']).
card_text('inspiration', 'Target player draws two cards.').
card_mana_cost('inspiration', ['3', 'U']).
card_cmc('inspiration', 4).
card_layout('inspiration', 'normal').

% Found in: BFZ, M11, M15
card_name('inspired charge', 'Inspired Charge').
card_type('inspired charge', 'Instant').
card_types('inspired charge', ['Instant']).
card_subtypes('inspired charge', []).
card_colors('inspired charge', ['W']).
card_text('inspired charge', 'Creatures you control get +2/+1 until end of turn.').
card_mana_cost('inspired charge', ['2', 'W', 'W']).
card_cmc('inspired charge', 4).
card_layout('inspired charge', 'normal').

% Found in: MOR
card_name('inspired sprite', 'Inspired Sprite').
card_type('inspired sprite', 'Creature — Faerie Wizard').
card_types('inspired sprite', ['Creature']).
card_subtypes('inspired sprite', ['Faerie', 'Wizard']).
card_colors('inspired sprite', ['U']).
card_text('inspired sprite', 'Flash\nFlying\nWhenever you cast a Wizard spell, you may untap Inspired Sprite.\n{T}: Draw a card, then discard a card.').
card_mana_cost('inspired sprite', ['3', 'U']).
card_cmc('inspired sprite', 4).
card_layout('inspired sprite', 'normal').
card_power('inspired sprite', 2).
card_toughness('inspired sprite', 2).

% Found in: DTK
card_name('inspiring call', 'Inspiring Call').
card_type('inspiring call', 'Instant').
card_types('inspiring call', ['Instant']).
card_subtypes('inspiring call', []).
card_colors('inspiring call', ['G']).
card_text('inspiring call', 'Draw a card for each creature you control with a +1/+1 counter on it. Those creatures gain indestructible until end of turn. (Damage and effects that say \"destroy\" don\'t destroy them.)').
card_mana_cost('inspiring call', ['2', 'G']).
card_cmc('inspiring call', 3).
card_layout('inspiring call', 'normal').

% Found in: 9ED, ONS
card_name('inspirit', 'Inspirit').
card_type('inspirit', 'Instant').
card_types('inspirit', ['Instant']).
card_subtypes('inspirit', []).
card_colors('inspirit', ['W']).
card_text('inspirit', 'Untap target creature. It gets +2/+4 until end of turn.').
card_mana_cost('inspirit', ['2', 'W']).
card_cmc('inspirit', 3).
card_layout('inspirit', 'normal').

% Found in: MMQ
card_name('instigator', 'Instigator').
card_type('instigator', 'Creature — Human Spellshaper').
card_types('instigator', ['Creature']).
card_subtypes('instigator', ['Human', 'Spellshaper']).
card_colors('instigator', ['B']).
card_text('instigator', '{1}{B}{B}, {T}, Discard a card: Creatures target player controls attack this turn if able.').
card_mana_cost('instigator', ['1', 'B']).
card_cmc('instigator', 2).
card_layout('instigator', 'normal').
card_power('instigator', 1).
card_toughness('instigator', 1).

% Found in: ISD
card_name('instigator gang', 'Instigator Gang').
card_type('instigator gang', 'Creature — Human Werewolf').
card_types('instigator gang', ['Creature']).
card_subtypes('instigator gang', ['Human', 'Werewolf']).
card_colors('instigator gang', ['R']).
card_text('instigator gang', 'Attacking creatures you control get +1/+0.\nAt the beginning of each upkeep, if no spells were cast last turn, transform Instigator Gang.').
card_mana_cost('instigator gang', ['3', 'R']).
card_cmc('instigator gang', 4).
card_layout('instigator gang', 'double-faced').
card_power('instigator gang', 2).
card_toughness('instigator gang', 3).
card_sides('instigator gang', 'wildblood pack').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, ME4
card_name('instill energy', 'Instill Energy').
card_type('instill energy', 'Enchantment — Aura').
card_types('instill energy', ['Enchantment']).
card_subtypes('instill energy', ['Aura']).
card_colors('instill energy', ['G']).
card_text('instill energy', 'Enchant creature\nEnchanted creature can attack as though it had haste.\n{0}: Untap enchanted creature. Activate this ability only during your turn and only once each turn.').
card_mana_cost('instill energy', ['G']).
card_cmc('instill energy', 1).
card_layout('instill energy', 'normal').

% Found in: RAV
card_name('instill furor', 'Instill Furor').
card_type('instill furor', 'Enchantment — Aura').
card_types('instill furor', ['Enchantment']).
card_subtypes('instill furor', ['Aura']).
card_colors('instill furor', ['R']).
card_text('instill furor', 'Enchant creature\nEnchanted creature has \"At the beginning of your end step, sacrifice this creature unless it attacked this turn.\"').
card_mana_cost('instill furor', ['1', 'R']).
card_cmc('instill furor', 2).
card_layout('instill furor', 'normal').

% Found in: MM2, SOM
card_name('instill infection', 'Instill Infection').
card_type('instill infection', 'Instant').
card_types('instill infection', ['Instant']).
card_subtypes('instill infection', []).
card_colors('instill infection', ['B']).
card_text('instill infection', 'Put a -1/-1 counter on target creature.\nDraw a card.').
card_mana_cost('instill infection', ['3', 'B']).
card_cmc('instill infection', 4).
card_layout('instill infection', 'normal').

% Found in: MMQ
card_name('insubordination', 'Insubordination').
card_type('insubordination', 'Enchantment — Aura').
card_types('insubordination', ['Enchantment']).
card_subtypes('insubordination', ['Aura']).
card_colors('insubordination', ['B']).
card_text('insubordination', 'Enchant creature\nAt the beginning of the end step of enchanted creature\'s controller, Insubordination deals 2 damage to that player unless that creature attacked this turn.').
card_mana_cost('insubordination', ['B', 'B']).
card_cmc('insubordination', 2).
card_layout('insubordination', 'normal').

% Found in: CMD, HOP, ONS
card_name('insurrection', 'Insurrection').
card_type('insurrection', 'Sorcery').
card_types('insurrection', ['Sorcery']).
card_subtypes('insurrection', []).
card_colors('insurrection', ['R']).
card_text('insurrection', 'Untap all creatures and gain control of them until end of turn. They gain haste until end of turn.').
card_mana_cost('insurrection', ['5', 'R', 'R', 'R']).
card_cmc('insurrection', 8).
card_layout('insurrection', 'normal').

% Found in: CNS, ISD, MD1
card_name('intangible virtue', 'Intangible Virtue').
card_type('intangible virtue', 'Enchantment').
card_types('intangible virtue', ['Enchantment']).
card_subtypes('intangible virtue', []).
card_colors('intangible virtue', ['W']).
card_text('intangible virtue', 'Creature tokens you control get +1/+1 and have vigilance.').
card_mana_cost('intangible virtue', ['1', 'W']).
card_cmc('intangible virtue', 2).
card_layout('intangible virtue', 'normal').

% Found in: C14
card_name('intellectual offering', 'Intellectual Offering').
card_type('intellectual offering', 'Instant').
card_types('intellectual offering', ['Instant']).
card_subtypes('intellectual offering', []).
card_colors('intellectual offering', ['U']).
card_text('intellectual offering', 'Choose an opponent. You and that player each draw three cards.\nChoose an opponent. Untap all nonland permanents you control and all nonland permanents that player controls.').
card_mana_cost('intellectual offering', ['4', 'U']).
card_cmc('intellectual offering', 5).
card_layout('intellectual offering', 'normal').

% Found in: TMP
card_name('interdict', 'Interdict').
card_type('interdict', 'Instant').
card_types('interdict', ['Instant']).
card_subtypes('interdict', []).
card_colors('interdict', ['U']).
card_text('interdict', 'Counter target activated ability from an artifact, creature, enchantment, or land. That permanent\'s activated abilities can\'t be activated this turn. (Mana abilities can\'t be targeted.)\nDraw a card.').
card_mana_cost('interdict', ['1', 'U']).
card_cmc('interdict', 2).
card_layout('interdict', 'normal').

% Found in: PC2
card_name('interplanar tunnel', 'Interplanar Tunnel').
card_type('interplanar tunnel', 'Phenomenon').
card_types('interplanar tunnel', ['Phenomenon']).
card_subtypes('interplanar tunnel', []).
card_colors('interplanar tunnel', []).
card_text('interplanar tunnel', 'When you encounter Interplanar Tunnel, reveal cards from the top of your planar deck until you reveal five plane cards. Put a plane card from among them on top of your planar deck, then put the rest of the revealed cards on the bottom in a random order. (Then planeswalk away from this phenomenon.)').
card_layout('interplanar tunnel', 'phenomenon').

% Found in: JOU
card_name('interpret the signs', 'Interpret the Signs').
card_type('interpret the signs', 'Sorcery').
card_types('interpret the signs', ['Sorcery']).
card_subtypes('interpret the signs', []).
card_colors('interpret the signs', ['U']).
card_text('interpret the signs', 'Scry 3, then reveal the top card of your library. Draw cards equal to that card\'s converted mana cost. (To scry 3, look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('interpret the signs', ['5', 'U']).
card_cmc('interpret the signs', 6).
card_layout('interpret the signs', 'normal').

% Found in: ULG
card_name('intervene', 'Intervene').
card_type('intervene', 'Instant').
card_types('intervene', ['Instant']).
card_subtypes('intervene', []).
card_colors('intervene', ['U']).
card_text('intervene', 'Counter target spell that targets a creature.').
card_mana_cost('intervene', ['U']).
card_cmc('intervene', 1).
card_layout('intervene', 'normal').

% Found in: FUT
card_name('intervention pact', 'Intervention Pact').
card_type('intervention pact', 'Instant').
card_types('intervention pact', ['Instant']).
card_subtypes('intervention pact', []).
card_colors('intervention pact', ['W']).
card_text('intervention pact', 'The next time a source of your choice would deal damage to you this turn, prevent that damage. You gain life equal to the damage prevented this way.\nAt the beginning of your next upkeep, pay {1}{W}{W}. If you don\'t, you lose the game.').
card_mana_cost('intervention pact', ['0']).
card_cmc('intervention pact', 0).
card_layout('intervention pact', 'normal').

% Found in: CMD, PLC
card_name('intet, the dreamer', 'Intet, the Dreamer').
card_type('intet, the dreamer', 'Legendary Creature — Dragon').
card_types('intet, the dreamer', ['Creature']).
card_subtypes('intet, the dreamer', ['Dragon']).
card_supertypes('intet, the dreamer', ['Legendary']).
card_colors('intet, the dreamer', ['U', 'R', 'G']).
card_text('intet, the dreamer', 'Flying\nWhenever Intet, the Dreamer deals combat damage to a player, you may pay {2}{U}. If you do, exile the top card of your library face down. You may look at that card for as long as it remains exiled. You may play that card without paying its mana cost for as long as Intet remains on the battlefield.').
card_mana_cost('intet, the dreamer', ['3', 'U', 'R', 'G']).
card_cmc('intet, the dreamer', 6).
card_layout('intet, the dreamer', 'normal').
card_power('intet, the dreamer', 6).
card_toughness('intet, the dreamer', 6).

% Found in: MMQ
card_name('intimidation', 'Intimidation').
card_type('intimidation', 'Enchantment').
card_types('intimidation', ['Enchantment']).
card_subtypes('intimidation', []).
card_colors('intimidation', ['B']).
card_text('intimidation', 'Creatures you control have fear. (They can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('intimidation', ['2', 'B', 'B', 'B']).
card_cmc('intimidation', 5).
card_layout('intimidation', 'normal').

% Found in: ARB
card_name('intimidation bolt', 'Intimidation Bolt').
card_type('intimidation bolt', 'Instant').
card_types('intimidation bolt', ['Instant']).
card_subtypes('intimidation bolt', []).
card_colors('intimidation bolt', ['W', 'R']).
card_text('intimidation bolt', 'Intimidation Bolt deals 3 damage to target creature. Other creatures can\'t attack this turn.').
card_mana_cost('intimidation bolt', ['1', 'R', 'W']).
card_cmc('intimidation bolt', 3).
card_layout('intimidation bolt', 'normal').

% Found in: SHM
card_name('intimidator initiate', 'Intimidator Initiate').
card_type('intimidator initiate', 'Creature — Goblin Shaman').
card_types('intimidator initiate', ['Creature']).
card_subtypes('intimidator initiate', ['Goblin', 'Shaman']).
card_colors('intimidator initiate', ['R']).
card_text('intimidator initiate', 'Whenever a player casts a red spell, you may pay {1}. If you do, target creature can\'t block this turn.').
card_mana_cost('intimidator initiate', ['R']).
card_cmc('intimidator initiate', 1).
card_layout('intimidator initiate', 'normal').
card_power('intimidator initiate', 1).
card_toughness('intimidator initiate', 1).

% Found in: MBS
card_name('into the core', 'Into the Core').
card_type('into the core', 'Instant').
card_types('into the core', ['Instant']).
card_subtypes('into the core', []).
card_colors('into the core', ['R']).
card_text('into the core', 'Exile two target artifacts.').
card_mana_cost('into the core', ['2', 'R', 'R']).
card_cmc('into the core', 4).
card_layout('into the core', 'normal').

% Found in: ARC
card_name('into the earthen maw', 'Into the Earthen Maw').
card_type('into the earthen maw', 'Scheme').
card_types('into the earthen maw', ['Scheme']).
card_subtypes('into the earthen maw', []).
card_colors('into the earthen maw', []).
card_text('into the earthen maw', 'When you set this scheme in motion, exile up to one target creature with flying, up to one target creature without flying, and all cards from up to one target opponent\'s graveyard.').
card_layout('into the earthen maw', 'scheme').

% Found in: SOK
card_name('into the fray', 'Into the Fray').
card_type('into the fray', 'Instant — Arcane').
card_types('into the fray', ['Instant']).
card_subtypes('into the fray', ['Arcane']).
card_colors('into the fray', ['R']).
card_text('into the fray', 'Target creature attacks this turn if able.\nSplice onto Arcane {R} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('into the fray', ['R']).
card_cmc('into the fray', 1).
card_layout('into the fray', 'normal').

% Found in: ISD
card_name('into the maw of hell', 'Into the Maw of Hell').
card_type('into the maw of hell', 'Sorcery').
card_types('into the maw of hell', ['Sorcery']).
card_subtypes('into the maw of hell', []).
card_colors('into the maw of hell', ['R']).
card_text('into the maw of hell', 'Destroy target land. Into the Maw of Hell deals 13 damage to target creature.').
card_mana_cost('into the maw of hell', ['4', 'R', 'R']).
card_cmc('into the maw of hell', 6).
card_layout('into the maw of hell', 'normal').

% Found in: CSP
card_name('into the north', 'Into the North').
card_type('into the north', 'Sorcery').
card_types('into the north', ['Sorcery']).
card_subtypes('into the north', []).
card_colors('into the north', ['G']).
card_text('into the north', 'Search your library for a snow land card and put it onto the battlefield tapped. Then shuffle your library.').
card_mana_cost('into the north', ['1', 'G']).
card_cmc('into the north', 2).
card_layout('into the north', 'normal').

% Found in: C14, DDM, ZEN
card_name('into the roil', 'Into the Roil').
card_type('into the roil', 'Instant').
card_types('into the roil', ['Instant']).
card_subtypes('into the roil', []).
card_colors('into the roil', ['U']).
card_text('into the roil', 'Kicker {1}{U} (You may pay an additional {1}{U} as you cast this spell.)\nReturn target nonland permanent to its owner\'s hand. If Into the Roil was kicked, draw a card.').
card_mana_cost('into the roil', ['1', 'U']).
card_cmc('into the roil', 2).
card_layout('into the roil', 'normal').

% Found in: AVR, M15, ORI
card_name('into the void', 'Into the Void').
card_type('into the void', 'Sorcery').
card_types('into the void', ['Sorcery']).
card_subtypes('into the void', []).
card_colors('into the void', ['U']).
card_text('into the void', 'Return up to two target creatures to their owners\' hands.').
card_mana_cost('into the void', ['3', 'U']).
card_cmc('into the void', 4).
card_layout('into the void', 'normal').

% Found in: M14
card_name('into the wilds', 'Into the Wilds').
card_type('into the wilds', 'Enchantment').
card_types('into the wilds', ['Enchantment']).
card_subtypes('into the wilds', []).
card_colors('into the wilds', ['G']).
card_text('into the wilds', 'At the beginning of your upkeep, look at the top card of your library. If it\'s a land card, you may put it onto the battlefield.').
card_mana_cost('into the wilds', ['3', 'G']).
card_cmc('into the wilds', 4).
card_layout('into the wilds', 'normal').

% Found in: 5DN
card_name('into thin air', 'Into Thin Air').
card_type('into thin air', 'Instant').
card_types('into thin air', ['Instant']).
card_subtypes('into thin air', []).
card_colors('into thin air', ['U']).
card_text('into thin air', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nReturn target artifact to its owner\'s hand.').
card_mana_cost('into thin air', ['5', 'U']).
card_cmc('into thin air', 6).
card_layout('into thin air', 'normal').

% Found in: 7ED, 8ED, M13, USG
card_name('intrepid hero', 'Intrepid Hero').
card_type('intrepid hero', 'Creature — Human Soldier').
card_types('intrepid hero', ['Creature']).
card_subtypes('intrepid hero', ['Human', 'Soldier']).
card_colors('intrepid hero', ['W']).
card_text('intrepid hero', '{T}: Destroy target creature with power 4 or greater.').
card_mana_cost('intrepid hero', ['2', 'W']).
card_cmc('intrepid hero', 3).
card_layout('intrepid hero', 'normal').
card_power('intrepid hero', 1).
card_toughness('intrepid hero', 1).

% Found in: ARC
card_name('introductions are in order', 'Introductions Are in Order').
card_type('introductions are in order', 'Scheme').
card_types('introductions are in order', ['Scheme']).
card_subtypes('introductions are in order', []).
card_colors('introductions are in order', []).
card_text('introductions are in order', 'When you set this scheme in motion, choose one —\n• Search your library for a creature card, reveal it, put it into your hand, then shuffle your library.\n• You may put a creature card from your hand onto the battlefield.').
card_layout('introductions are in order', 'scheme').

% Found in: 8ED, STH
card_name('intruder alarm', 'Intruder Alarm').
card_type('intruder alarm', 'Enchantment').
card_types('intruder alarm', ['Enchantment']).
card_subtypes('intruder alarm', []).
card_colors('intruder alarm', ['U']).
card_text('intruder alarm', 'Creatures don\'t untap during their controllers\' untap steps.\nWhenever a creature enters the battlefield, untap all creatures.').
card_mana_cost('intruder alarm', ['2', 'U']).
card_cmc('intruder alarm', 3).
card_layout('intruder alarm', 'normal').

% Found in: TMP, TPR, pJGP
card_name('intuition', 'Intuition').
card_type('intuition', 'Instant').
card_types('intuition', ['Instant']).
card_subtypes('intuition', []).
card_colors('intuition', ['U']).
card_text('intuition', 'Search your library for three cards and reveal them. Target opponent chooses one. Put that card into your hand and the rest into your graveyard. Then shuffle your library.').
card_mana_cost('intuition', ['2', 'U']).
card_cmc('intuition', 3).
card_layout('intuition', 'normal').
card_reserved('intuition').

% Found in: EVE
card_name('inundate', 'Inundate').
card_type('inundate', 'Sorcery').
card_types('inundate', ['Sorcery']).
card_subtypes('inundate', []).
card_colors('inundate', ['U']).
card_text('inundate', 'Return all nonblue creatures to their owners\' hands.').
card_mana_cost('inundate', ['3', 'U', 'U', 'U']).
card_cmc('inundate', 6).
card_layout('inundate', 'normal').

% Found in: NPH
card_name('invader parasite', 'Invader Parasite').
card_type('invader parasite', 'Creature — Insect').
card_types('invader parasite', ['Creature']).
card_subtypes('invader parasite', ['Insect']).
card_colors('invader parasite', ['R']).
card_text('invader parasite', 'Imprint — When Invader Parasite enters the battlefield, exile target land.\nWhenever a land with the same name as the exiled card enters the battlefield under an opponent\'s control, Invader Parasite deals 2 damage to that player.').
card_mana_cost('invader parasite', ['3', 'R', 'R']).
card_cmc('invader parasite', 5).
card_layout('invader parasite', 'normal').
card_power('invader parasite', 3).
card_toughness('invader parasite', 2).

% Found in: STH
card_name('invasion plans', 'Invasion Plans').
card_type('invasion plans', 'Enchantment').
card_types('invasion plans', ['Enchantment']).
card_subtypes('invasion plans', []).
card_colors('invasion plans', ['R']).
card_text('invasion plans', 'All creatures block each turn if able.\nThe attacking player chooses how each creature blocks each turn.').
card_mana_cost('invasion plans', ['2', 'R']).
card_cmc('invasion plans', 3).
card_layout('invasion plans', 'normal').

% Found in: M15
card_name('invasive species', 'Invasive Species').
card_type('invasive species', 'Creature — Insect').
card_types('invasive species', ['Creature']).
card_subtypes('invasive species', ['Insect']).
card_colors('invasive species', ['G']).
card_text('invasive species', 'When Invasive Species enters the battlefield, return another permanent you control to its owner\'s hand.').
card_mana_cost('invasive species', ['2', 'G']).
card_cmc('invasive species', 3).
card_layout('invasive species', 'normal').
card_power('invasive species', 3).
card_toughness('invasive species', 3).

% Found in: EVE
card_name('invert the skies', 'Invert the Skies').
card_type('invert the skies', 'Instant').
card_types('invert the skies', ['Instant']).
card_subtypes('invert the skies', []).
card_colors('invert the skies', ['U', 'G']).
card_text('invert the skies', 'Creatures your opponents control lose flying until end of turn if {G} was spent to cast Invert the Skies, and creatures you control gain flying until end of turn if {U} was spent to cast it. (Do both if {G}{U} was spent.)').
card_mana_cost('invert the skies', ['3', 'G/U']).
card_cmc('invert the skies', 4).
card_layout('invert the skies', 'normal').

% Found in: CMD, DD3_GVL, DDD, MMQ
card_name('invigorate', 'Invigorate').
card_type('invigorate', 'Instant').
card_types('invigorate', ['Instant']).
card_subtypes('invigorate', []).
card_colors('invigorate', ['G']).
card_text('invigorate', 'If you control a Forest, rather than pay Invigorate\'s mana cost, you may have an opponent gain 3 life.\nTarget creature gets +4/+4 until end of turn.').
card_mana_cost('invigorate', ['2', 'G']).
card_cmc('invigorate', 3).
card_layout('invigorate', 'normal').

% Found in: ONS
card_name('invigorating boon', 'Invigorating Boon').
card_type('invigorating boon', 'Enchantment').
card_types('invigorating boon', ['Enchantment']).
card_subtypes('invigorating boon', []).
card_colors('invigorating boon', ['G']).
card_text('invigorating boon', 'Whenever a player cycles a card, you may put a +1/+1 counter on target creature.').
card_mana_cost('invigorating boon', ['1', 'G']).
card_cmc('invigorating boon', 2).
card_layout('invigorating boon', 'normal').

% Found in: TOR
card_name('invigorating falls', 'Invigorating Falls').
card_type('invigorating falls', 'Sorcery').
card_types('invigorating falls', ['Sorcery']).
card_subtypes('invigorating falls', []).
card_colors('invigorating falls', ['G']).
card_text('invigorating falls', 'You gain life equal to the number of creature cards in all graveyards.').
card_mana_cost('invigorating falls', ['2', 'G', 'G']).
card_cmc('invigorating falls', 4).
card_layout('invigorating falls', 'normal').

% Found in: ALA
card_name('invincible hymn', 'Invincible Hymn').
card_type('invincible hymn', 'Sorcery').
card_types('invincible hymn', ['Sorcery']).
card_subtypes('invincible hymn', []).
card_colors('invincible hymn', ['W']).
card_text('invincible hymn', 'Count the number of cards in your library. Your life total becomes that number.').
card_mana_cost('invincible hymn', ['6', 'W', 'W']).
card_cmc('invincible hymn', 8).
card_layout('invincible hymn', 'normal').

% Found in: MMQ
card_name('inviolability', 'Inviolability').
card_type('inviolability', 'Enchantment — Aura').
card_types('inviolability', ['Enchantment']).
card_subtypes('inviolability', ['Aura']).
card_colors('inviolability', ['W']).
card_text('inviolability', 'Enchant creature\nPrevent all damage that would be dealt to enchanted creature.').
card_mana_cost('inviolability', ['1', 'W']).
card_cmc('inviolability', 2).
card_layout('inviolability', 'normal').

% Found in: 2ED, 8ED, CED, CEI, LEA, LEB, M15
card_name('invisibility', 'Invisibility').
card_type('invisibility', 'Enchantment — Aura').
card_types('invisibility', ['Enchantment']).
card_subtypes('invisibility', ['Aura']).
card_colors('invisibility', ['U']).
card_text('invisibility', 'Enchant creature\nEnchanted creature can\'t be blocked except by Walls.').
card_mana_cost('invisibility', ['U', 'U']).
card_cmc('invisibility', 2).
card_layout('invisibility', 'normal').

% Found in: ISD
card_name('invisible stalker', 'Invisible Stalker').
card_type('invisible stalker', 'Creature — Human Rogue').
card_types('invisible stalker', ['Creature']).
card_subtypes('invisible stalker', ['Human', 'Rogue']).
card_colors('invisible stalker', ['U']).
card_text('invisible stalker', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nInvisible Stalker can\'t be blocked.').
card_mana_cost('invisible stalker', ['1', 'U']).
card_cmc('invisible stalker', 2).
card_layout('invisible stalker', 'normal').
card_power('invisible stalker', 1).
card_toughness('invisible stalker', 1).

% Found in: LEG
card_name('invoke prejudice', 'Invoke Prejudice').
card_type('invoke prejudice', 'Enchantment').
card_types('invoke prejudice', ['Enchantment']).
card_subtypes('invoke prejudice', []).
card_colors('invoke prejudice', ['U']).
card_text('invoke prejudice', 'Whenever an opponent casts a creature spell that doesn\'t share a color with a creature you control, counter that spell unless that player pays {X}, where X is its converted mana cost.').
card_mana_cost('invoke prejudice', ['U', 'U', 'U', 'U']).
card_cmc('invoke prejudice', 4).
card_layout('invoke prejudice', 'normal').
card_reserved('invoke prejudice').

% Found in: DDJ, GPT
card_name('invoke the firemind', 'Invoke the Firemind').
card_type('invoke the firemind', 'Sorcery').
card_types('invoke the firemind', ['Sorcery']).
card_subtypes('invoke the firemind', []).
card_colors('invoke the firemind', ['U', 'R']).
card_text('invoke the firemind', 'Choose one —\n• Draw X cards.\n• Invoke the Firemind deals X damage to target creature or player.').
card_mana_cost('invoke the firemind', ['X', 'U', 'U', 'R']).
card_cmc('invoke the firemind', 3).
card_layout('invoke the firemind', 'normal').

% Found in: TMP
card_name('invulnerability', 'Invulnerability').
card_type('invulnerability', 'Instant').
card_types('invulnerability', ['Instant']).
card_subtypes('invulnerability', []).
card_colors('invulnerability', ['W']).
card_text('invulnerability', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nThe next time a source of your choice would deal damage to you this turn, prevent that damage.').
card_mana_cost('invulnerability', ['1', 'W']).
card_cmc('invulnerability', 2).
card_layout('invulnerability', 'normal').

% Found in: 5DN
card_name('ion storm', 'Ion Storm').
card_type('ion storm', 'Enchantment').
card_types('ion storm', ['Enchantment']).
card_subtypes('ion storm', []).
card_colors('ion storm', ['R']).
card_text('ion storm', '{1}{R}, Remove a +1/+1 counter or a charge counter from a permanent you control: Ion Storm deals 2 damage to target creature or player.').
card_mana_cost('ion storm', ['2', 'R']).
card_cmc('ion storm', 3).
card_layout('ion storm', 'normal').

% Found in: WWK
card_name('iona\'s judgment', 'Iona\'s Judgment').
card_type('iona\'s judgment', 'Sorcery').
card_types('iona\'s judgment', ['Sorcery']).
card_subtypes('iona\'s judgment', []).
card_colors('iona\'s judgment', ['W']).
card_text('iona\'s judgment', 'Exile target creature or enchantment.').
card_mana_cost('iona\'s judgment', ['4', 'W']).
card_cmc('iona\'s judgment', 5).
card_layout('iona\'s judgment', 'normal').

% Found in: MM2, V15, ZEN
card_name('iona, shield of emeria', 'Iona, Shield of Emeria').
card_type('iona, shield of emeria', 'Legendary Creature — Angel').
card_types('iona, shield of emeria', ['Creature']).
card_subtypes('iona, shield of emeria', ['Angel']).
card_supertypes('iona, shield of emeria', ['Legendary']).
card_colors('iona, shield of emeria', ['W']).
card_text('iona, shield of emeria', 'Flying\nAs Iona, Shield of Emeria enters the battlefield, choose a color.\nYour opponents can\'t cast spells of the chosen color.').
card_mana_cost('iona, shield of emeria', ['6', 'W', 'W', 'W']).
card_cmc('iona, shield of emeria', 9).
card_layout('iona, shield of emeria', 'normal').
card_power('iona, shield of emeria', 7).
card_toughness('iona, shield of emeria', 7).

% Found in: ZEN
card_name('ior ruin expedition', 'Ior Ruin Expedition').
card_type('ior ruin expedition', 'Enchantment').
card_types('ior ruin expedition', ['Enchantment']).
card_subtypes('ior ruin expedition', []).
card_colors('ior ruin expedition', ['U']).
card_text('ior ruin expedition', 'Landfall — Whenever a land enters the battlefield under your control, you may put a quest counter on Ior Ruin Expedition.\nRemove three quest counters from Ior Ruin Expedition and sacrifice it: Draw two cards.').
card_mana_cost('ior ruin expedition', ['1', 'U']).
card_cmc('ior ruin expedition', 2).
card_layout('ior ruin expedition', 'normal').

% Found in: BOK
card_name('ire of kaminari', 'Ire of Kaminari').
card_type('ire of kaminari', 'Instant — Arcane').
card_types('ire of kaminari', ['Instant']).
card_subtypes('ire of kaminari', ['Arcane']).
card_colors('ire of kaminari', ['R']).
card_text('ire of kaminari', 'Ire of Kaminari deals damage to target creature or player equal to the number of Arcane cards in your graveyard.').
card_mana_cost('ire of kaminari', ['3', 'R']).
card_cmc('ire of kaminari', 4).
card_layout('ire of kaminari', 'normal').

% Found in: DTK
card_name('ire shaman', 'Ire Shaman').
card_type('ire shaman', 'Creature — Orc Shaman').
card_types('ire shaman', ['Creature']).
card_subtypes('ire shaman', ['Orc', 'Shaman']).
card_colors('ire shaman', ['R']).
card_text('ire shaman', 'Menace (This creature can\'t be blocked except by two or more creatures.)\nMegamorph {R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Ire Shaman is turned face up, exile the top card of your library. Until end of turn, you may play that card.').
card_mana_cost('ire shaman', ['1', 'R']).
card_cmc('ire shaman', 2).
card_layout('ire shaman', 'normal').
card_power('ire shaman', 2).
card_toughness('ire shaman', 1).

% Found in: ODY, V15
card_name('iridescent angel', 'Iridescent Angel').
card_type('iridescent angel', 'Creature — Angel').
card_types('iridescent angel', ['Creature']).
card_subtypes('iridescent angel', ['Angel']).
card_colors('iridescent angel', ['W', 'U']).
card_text('iridescent angel', 'Flying, protection from all colors').
card_mana_cost('iridescent angel', ['5', 'W', 'U']).
card_cmc('iridescent angel', 7).
card_layout('iridescent angel', 'normal').
card_power('iridescent angel', 4).
card_toughness('iridescent angel', 4).

% Found in: UDS
card_name('iridescent drake', 'Iridescent Drake').
card_type('iridescent drake', 'Creature — Drake').
card_types('iridescent drake', ['Creature']).
card_subtypes('iridescent drake', ['Drake']).
card_colors('iridescent drake', ['U']).
card_text('iridescent drake', 'Flying\nWhen Iridescent Drake enters the battlefield, put target Aura card from a graveyard onto the battlefield under your control attached to Iridescent Drake.').
card_mana_cost('iridescent drake', ['3', 'U']).
card_cmc('iridescent drake', 4).
card_layout('iridescent drake', 'normal').
card_power('iridescent drake', 2).
card_toughness('iridescent drake', 2).

% Found in: HML
card_name('irini sengir', 'Irini Sengir').
card_type('irini sengir', 'Legendary Creature — Vampire Dwarf').
card_types('irini sengir', ['Creature']).
card_subtypes('irini sengir', ['Vampire', 'Dwarf']).
card_supertypes('irini sengir', ['Legendary']).
card_colors('irini sengir', ['B']).
card_text('irini sengir', 'Green enchantment spells and white enchantment spells cost {2} more to cast.').
card_mana_cost('irini sengir', ['2', 'B', 'B']).
card_cmc('irini sengir', 4).
card_layout('irini sengir', 'normal').
card_power('irini sengir', 2).
card_toughness('irini sengir', 2).

% Found in: ORI
card_name('iroas\'s champion', 'Iroas\'s Champion').
card_type('iroas\'s champion', 'Creature — Human Soldier').
card_types('iroas\'s champion', ['Creature']).
card_subtypes('iroas\'s champion', ['Human', 'Soldier']).
card_colors('iroas\'s champion', ['W', 'R']).
card_text('iroas\'s champion', 'Double strike (This creature deals both first-strike and regular combat damage.)').
card_mana_cost('iroas\'s champion', ['1', 'R', 'W']).
card_cmc('iroas\'s champion', 3).
card_layout('iroas\'s champion', 'normal').
card_power('iroas\'s champion', 2).
card_toughness('iroas\'s champion', 2).

% Found in: JOU
card_name('iroas, god of victory', 'Iroas, God of Victory').
card_type('iroas, god of victory', 'Legendary Enchantment Creature — God').
card_types('iroas, god of victory', ['Enchantment', 'Creature']).
card_subtypes('iroas, god of victory', ['God']).
card_supertypes('iroas, god of victory', ['Legendary']).
card_colors('iroas, god of victory', ['W', 'R']).
card_text('iroas, god of victory', 'Indestructible\nAs long as your devotion to red and white is less than seven, Iroas isn\'t a creature.\nCreatures you control have menace. (They can\'t be blocked except by two or more creatures.)\nPrevent all damage that would be dealt to attacking creatures you control.').
card_mana_cost('iroas, god of victory', ['2', 'R', 'W']).
card_cmc('iroas, god of victory', 4).
card_layout('iroas, god of victory', 'normal').
card_power('iroas, god of victory', 7).
card_toughness('iroas, god of victory', 4).

% Found in: MMQ
card_name('iron lance', 'Iron Lance').
card_type('iron lance', 'Artifact').
card_types('iron lance', ['Artifact']).
card_subtypes('iron lance', []).
card_colors('iron lance', []).
card_text('iron lance', '{3}, {T}: Target creature gains first strike until end of turn.').
card_mana_cost('iron lance', ['2']).
card_cmc('iron lance', 2).
card_layout('iron lance', 'normal').

% Found in: ULG
card_name('iron maiden', 'Iron Maiden').
card_type('iron maiden', 'Artifact').
card_types('iron maiden', ['Artifact']).
card_subtypes('iron maiden', []).
card_colors('iron maiden', []).
card_text('iron maiden', 'At the beginning of each opponent\'s upkeep, Iron Maiden deals X damage to that player, where X is the number of cards in his or her hand minus 4.').
card_mana_cost('iron maiden', ['3']).
card_cmc('iron maiden', 3).
card_layout('iron maiden', 'normal').

% Found in: HOP, MRD, SOM
card_name('iron myr', 'Iron Myr').
card_type('iron myr', 'Artifact Creature — Myr').
card_types('iron myr', ['Artifact', 'Creature']).
card_subtypes('iron myr', ['Myr']).
card_colors('iron myr', []).
card_text('iron myr', '{T}: Add {R} to your mana pool.').
card_mana_cost('iron myr', ['2']).
card_cmc('iron myr', 2).
card_layout('iron myr', 'normal').
card_power('iron myr', 1).
card_toughness('iron myr', 1).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, CED, CEI, LEA, LEB
card_name('iron star', 'Iron Star').
card_type('iron star', 'Artifact').
card_types('iron star', ['Artifact']).
card_subtypes('iron star', []).
card_colors('iron star', []).
card_text('iron star', 'Whenever a player casts a red spell, you may pay {1}. If you do, you gain 1 life.').
card_mana_cost('iron star', ['1']).
card_cmc('iron star', 1).
card_layout('iron star', 'normal').

% Found in: MIR
card_name('iron tusk elephant', 'Iron Tusk Elephant').
card_type('iron tusk elephant', 'Creature — Elephant').
card_types('iron tusk elephant', ['Creature']).
card_subtypes('iron tusk elephant', ['Elephant']).
card_colors('iron tusk elephant', ['W']).
card_text('iron tusk elephant', 'Trample').
card_mana_cost('iron tusk elephant', ['4', 'W']).
card_cmc('iron tusk elephant', 5).
card_layout('iron tusk elephant', 'normal').
card_power('iron tusk elephant', 3).
card_toughness('iron tusk elephant', 3).

% Found in: ULG
card_name('iron will', 'Iron Will').
card_type('iron will', 'Instant').
card_types('iron will', ['Instant']).
card_subtypes('iron will', []).
card_colors('iron will', ['W']).
card_text('iron will', 'Target creature gets +0/+4 until end of turn.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('iron will', ['W']).
card_cmc('iron will', 1).
card_layout('iron will', 'normal').

% Found in: 5DN
card_name('iron-barb hellion', 'Iron-Barb Hellion').
card_type('iron-barb hellion', 'Creature — Hellion Beast').
card_types('iron-barb hellion', ['Creature']).
card_subtypes('iron-barb hellion', ['Hellion', 'Beast']).
card_colors('iron-barb hellion', ['R']).
card_text('iron-barb hellion', 'Haste\nIron-Barb Hellion can\'t block.').
card_mana_cost('iron-barb hellion', ['5', 'R']).
card_cmc('iron-barb hellion', 6).
card_layout('iron-barb hellion', 'normal').
card_power('iron-barb hellion', 5).
card_toughness('iron-barb hellion', 4).

% Found in: VIS
card_name('iron-heart chimera', 'Iron-Heart Chimera').
card_type('iron-heart chimera', 'Artifact Creature — Chimera').
card_types('iron-heart chimera', ['Artifact', 'Creature']).
card_subtypes('iron-heart chimera', ['Chimera']).
card_colors('iron-heart chimera', []).
card_text('iron-heart chimera', 'Vigilance\nSacrifice Iron-Heart Chimera: Put a +2/+2 counter on target Chimera creature. It gains vigilance. (This effect lasts indefinitely.)').
card_mana_cost('iron-heart chimera', ['4']).
card_cmc('iron-heart chimera', 4).
card_layout('iron-heart chimera', 'normal').
card_power('iron-heart chimera', 2).
card_toughness('iron-heart chimera', 2).

% Found in: TSP
card_name('ironclaw buzzardiers', 'Ironclaw Buzzardiers').
card_type('ironclaw buzzardiers', 'Creature — Orc Scout').
card_types('ironclaw buzzardiers', ['Creature']).
card_subtypes('ironclaw buzzardiers', ['Orc', 'Scout']).
card_colors('ironclaw buzzardiers', ['R']).
card_text('ironclaw buzzardiers', 'Ironclaw Buzzardiers can\'t block creatures with power 2 or greater.\n{R}: Ironclaw Buzzardiers gains flying until end of turn.').
card_mana_cost('ironclaw buzzardiers', ['2', 'R']).
card_cmc('ironclaw buzzardiers', 3).
card_layout('ironclaw buzzardiers', 'normal').
card_power('ironclaw buzzardiers', 2).
card_toughness('ironclaw buzzardiers', 2).

% Found in: 5ED, HML
card_name('ironclaw curse', 'Ironclaw Curse').
card_type('ironclaw curse', 'Enchantment — Aura').
card_types('ironclaw curse', ['Enchantment']).
card_subtypes('ironclaw curse', ['Aura']).
card_colors('ironclaw curse', ['R']).
card_text('ironclaw curse', 'Enchant creature\nEnchanted creature gets -0/-1.\nEnchanted creature can\'t block creatures with power equal to or greater than the enchanted creature\'s toughness.').
card_mana_cost('ironclaw curse', ['R']).
card_cmc('ironclaw curse', 1).
card_layout('ironclaw curse', 'normal').

% Found in: 2ED, 4ED, 5ED, CED, CEI, ITP, LEA, LEB, ME2, RQS
card_name('ironclaw orcs', 'Ironclaw Orcs').
card_type('ironclaw orcs', 'Creature — Orc').
card_types('ironclaw orcs', ['Creature']).
card_subtypes('ironclaw orcs', ['Orc']).
card_colors('ironclaw orcs', ['R']).
card_text('ironclaw orcs', 'Ironclaw Orcs can\'t block creatures with power 2 or greater.').
card_mana_cost('ironclaw orcs', ['1', 'R']).
card_cmc('ironclaw orcs', 2).
card_layout('ironclaw orcs', 'normal').
card_power('ironclaw orcs', 2).
card_toughness('ironclaw orcs', 2).

% Found in: ISD
card_name('ironfang', 'Ironfang').
card_type('ironfang', 'Creature — Werewolf').
card_types('ironfang', ['Creature']).
card_subtypes('ironfang', ['Werewolf']).
card_colors('ironfang', ['R']).
card_text('ironfang', 'First strike\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Ironfang.').
card_layout('ironfang', 'double-faced').
card_power('ironfang', 3).
card_toughness('ironfang', 1).

% Found in: ONS
card_name('ironfist crusher', 'Ironfist Crusher').
card_type('ironfist crusher', 'Creature — Human Soldier').
card_types('ironfist crusher', ['Creature']).
card_subtypes('ironfist crusher', ['Human', 'Soldier']).
card_colors('ironfist crusher', ['W']).
card_text('ironfist crusher', 'Ironfist Crusher can block any number of creatures.\nMorph {3}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('ironfist crusher', ['4', 'W']).
card_cmc('ironfist crusher', 5).
card_layout('ironfist crusher', 'normal').
card_power('ironfist crusher', 2).
card_toughness('ironfist crusher', 4).

% Found in: ME4, PO2
card_name('ironhoof ox', 'Ironhoof Ox').
card_type('ironhoof ox', 'Creature — Ox').
card_types('ironhoof ox', ['Creature']).
card_subtypes('ironhoof ox', ['Ox']).
card_colors('ironhoof ox', ['G']).
card_text('ironhoof ox', 'Ironhoof Ox can\'t be blocked by more than one creature.').
card_mana_cost('ironhoof ox', ['3', 'G', 'G']).
card_cmc('ironhoof ox', 5).
card_layout('ironhoof ox', 'normal').
card_power('ironhoof ox', 4).
card_toughness('ironhoof ox', 4).

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB
card_name('ironroot treefolk', 'Ironroot Treefolk').
card_type('ironroot treefolk', 'Creature — Treefolk').
card_types('ironroot treefolk', ['Creature']).
card_subtypes('ironroot treefolk', ['Treefolk']).
card_colors('ironroot treefolk', ['G']).
card_text('ironroot treefolk', '').
card_mana_cost('ironroot treefolk', ['4', 'G']).
card_cmc('ironroot treefolk', 5).
card_layout('ironroot treefolk', 'normal').
card_power('ironroot treefolk', 3).
card_toughness('ironroot treefolk', 5).

% Found in: JUD
card_name('ironshell beetle', 'Ironshell Beetle').
card_type('ironshell beetle', 'Creature — Insect').
card_types('ironshell beetle', ['Creature']).
card_subtypes('ironshell beetle', ['Insect']).
card_colors('ironshell beetle', ['G']).
card_text('ironshell beetle', 'When Ironshell Beetle enters the battlefield, put a +1/+1 counter on target creature.').
card_mana_cost('ironshell beetle', ['1', 'G']).
card_cmc('ironshell beetle', 2).
card_layout('ironshell beetle', 'normal').
card_power('ironshell beetle', 1).
card_toughness('ironshell beetle', 1).

% Found in: MRD
card_name('irradiate', 'Irradiate').
card_type('irradiate', 'Instant').
card_types('irradiate', ['Instant']).
card_subtypes('irradiate', []).
card_colors('irradiate', ['B']).
card_text('irradiate', 'Target creature gets -1/-1 until end of turn for each artifact you control.').
card_mana_cost('irradiate', ['3', 'B']).
card_cmc('irradiate', 4).
card_layout('irradiate', 'normal').

% Found in: ROE
card_name('irresistible prey', 'Irresistible Prey').
card_type('irresistible prey', 'Sorcery').
card_types('irresistible prey', ['Sorcery']).
card_subtypes('irresistible prey', []).
card_colors('irresistible prey', ['G']).
card_text('irresistible prey', 'Target creature must be blocked this turn if able.\nDraw a card.').
card_mana_cost('irresistible prey', ['G']).
card_cmc('irresistible prey', 1).
card_layout('irresistible prey', 'normal').

% Found in: INV
card_name('irrigation ditch', 'Irrigation Ditch').
card_type('irrigation ditch', 'Land').
card_types('irrigation ditch', ['Land']).
card_subtypes('irrigation ditch', []).
card_colors('irrigation ditch', []).
card_text('irrigation ditch', 'Irrigation Ditch enters the battlefield tapped.\n{T}: Add {W} to your mana pool.\n{T}, Sacrifice Irrigation Ditch: Add {G}{U} to your mana pool.').
card_layout('irrigation ditch', 'normal').

% Found in: CHK
card_name('isamaru, hound of konda', 'Isamaru, Hound of Konda').
card_type('isamaru, hound of konda', 'Legendary Creature — Hound').
card_types('isamaru, hound of konda', ['Creature']).
card_subtypes('isamaru, hound of konda', ['Hound']).
card_supertypes('isamaru, hound of konda', ['Legendary']).
card_colors('isamaru, hound of konda', ['W']).
card_text('isamaru, hound of konda', '').
card_mana_cost('isamaru, hound of konda', ['W']).
card_cmc('isamaru, hound of konda', 1).
card_layout('isamaru, hound of konda', 'normal').
card_power('isamaru, hound of konda', 2).
card_toughness('isamaru, hound of konda', 2).

% Found in: BOK
card_name('isao, enlightened bushi', 'Isao, Enlightened Bushi').
card_type('isao, enlightened bushi', 'Legendary Creature — Human Samurai').
card_types('isao, enlightened bushi', ['Creature']).
card_subtypes('isao, enlightened bushi', ['Human', 'Samurai']).
card_supertypes('isao, enlightened bushi', ['Legendary']).
card_colors('isao, enlightened bushi', ['G']).
card_text('isao, enlightened bushi', 'Isao, Enlightened Bushi can\'t be countered.\nBushido 2 (Whenever this creature blocks or becomes blocked, it gets +2/+2 until end of turn.)\n{2}: Regenerate target Samurai.').
card_mana_cost('isao, enlightened bushi', ['2', 'G']).
card_cmc('isao, enlightened bushi', 3).
card_layout('isao, enlightened bushi', 'normal').
card_power('isao, enlightened bushi', 2).
card_toughness('isao, enlightened bushi', 1).

% Found in: BOK
card_name('ishi-ishi, akki crackshot', 'Ishi-Ishi, Akki Crackshot').
card_type('ishi-ishi, akki crackshot', 'Legendary Creature — Goblin Warrior').
card_types('ishi-ishi, akki crackshot', ['Creature']).
card_subtypes('ishi-ishi, akki crackshot', ['Goblin', 'Warrior']).
card_supertypes('ishi-ishi, akki crackshot', ['Legendary']).
card_colors('ishi-ishi, akki crackshot', ['R']).
card_text('ishi-ishi, akki crackshot', 'Whenever an opponent casts a Spirit or Arcane spell, Ishi-Ishi, Akki Crackshot deals 2 damage to that player.').
card_mana_cost('ishi-ishi, akki crackshot', ['1', 'R']).
card_cmc('ishi-ishi, akki crackshot', 2).
card_layout('ishi-ishi, akki crackshot', 'normal').
card_power('ishi-ishi, akki crackshot', 1).
card_toughness('ishi-ishi, akki crackshot', 1).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, ALA, ARC, AVR, BFZ, BRB, BTD, C13, C14, CED, CEI, CHK, CMD, CST, DD2, DD3_JVC, DDE, DDF, DDH, DDI, DDJ, DDM, DDN, DDO, DPA, DTK, FRF, H09, HOP, ICE, INV, ISD, ITP, KTK, LEA, LEB, LRW, M10, M11, M12, M13, M14, M15, MBS, ME3, MED, MIR, MMQ, MRD, NPH, ODY, ONS, ORI, PC2, PO2, POR, PTK, RAV, ROE, RQS, RTR, S00, S99, SHM, SOM, THS, TMP, TPR, TSP, UGL, UNH, USG, ZEN, pALP, pARL, pELP, pGRU, pJGP
card_name('island', 'Island').
card_type('island', 'Basic Land — Island').
card_types('island', ['Land']).
card_subtypes('island', ['Island']).
card_supertypes('island', ['Basic']).
card_colors('island', []).
card_text('island', '').
card_layout('island', 'normal').

% Found in: 3ED, 4ED, ARN
card_name('island fish jasconius', 'Island Fish Jasconius').
card_type('island fish jasconius', 'Creature — Fish').
card_types('island fish jasconius', ['Creature']).
card_subtypes('island fish jasconius', ['Fish']).
card_colors('island fish jasconius', ['U']).
card_text('island fish jasconius', 'Island Fish Jasconius doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may pay {U}{U}{U}. If you do, untap Island Fish Jasconius.\nIsland Fish Jasconius can\'t attack unless defending player controls an Island.\nWhen you control no Islands, sacrifice Island Fish Jasconius.').
card_mana_cost('island fish jasconius', ['4', 'U', 'U', 'U']).
card_cmc('island fish jasconius', 7).
card_layout('island fish jasconius', 'normal').
card_power('island fish jasconius', 6).
card_toughness('island fish jasconius', 8).

% Found in: ARN, MED
card_name('island of wak-wak', 'Island of Wak-Wak').
card_type('island of wak-wak', 'Land').
card_types('island of wak-wak', ['Land']).
card_subtypes('island of wak-wak', []).
card_colors('island of wak-wak', []).
card_text('island of wak-wak', '{T}: Target creature with flying has base power 0 until end of turn.').
card_layout('island of wak-wak', 'normal').
card_reserved('island of wak-wak').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, ME4
card_name('island sanctuary', 'Island Sanctuary').
card_type('island sanctuary', 'Enchantment').
card_types('island sanctuary', ['Enchantment']).
card_subtypes('island sanctuary', []).
card_colors('island sanctuary', ['W']).
card_text('island sanctuary', 'If you would draw a card during your draw step, instead you may skip that draw. If you do, until your next turn, you can\'t be attacked except by creatures with flying and/or islandwalk.').
card_mana_cost('island sanctuary', ['1', 'W']).
card_cmc('island sanctuary', 2).
card_layout('island sanctuary', 'normal').

% Found in: HOP
card_name('isle of vesuva', 'Isle of Vesuva').
card_type('isle of vesuva', 'Plane — Dominaria').
card_types('isle of vesuva', ['Plane']).
card_subtypes('isle of vesuva', ['Dominaria']).
card_colors('isle of vesuva', []).
card_text('isle of vesuva', 'Whenever a nontoken creature enters the battlefield, its controller puts a token onto the battlefield that\'s a copy of that creature.\nWhenever you roll {C}, destroy target creature and all other creatures with the same name as that creature.').
card_layout('isle of vesuva', 'plane').

% Found in: SHM
card_name('isleback spawn', 'Isleback Spawn').
card_type('isleback spawn', 'Creature — Kraken').
card_types('isleback spawn', ['Creature']).
card_subtypes('isleback spawn', ['Kraken']).
card_colors('isleback spawn', ['U']).
card_text('isleback spawn', 'Shroud (This creature can\'t be the target of spells or abilities.)\nIsleback Spawn gets +4/+8 as long as a library has twenty or fewer cards in it.').
card_mana_cost('isleback spawn', ['5', 'U', 'U']).
card_cmc('isleback spawn', 7).
card_layout('isleback spawn', 'normal').
card_power('isleback spawn', 4).
card_toughness('isleback spawn', 8).

% Found in: DDJ, MRD, V10, pFNM
card_name('isochron scepter', 'Isochron Scepter').
card_type('isochron scepter', 'Artifact').
card_types('isochron scepter', ['Artifact']).
card_subtypes('isochron scepter', []).
card_colors('isochron scepter', []).
card_text('isochron scepter', 'Imprint — When Isochron Scepter enters the battlefield, you may exile an instant card with converted mana cost 2 or less from your hand.\n{2}, {T}: You may copy the exiled card. If you do, you may cast the copy without paying its mana cost.').
card_mana_cost('isochron scepter', ['2']).
card_cmc('isochron scepter', 2).
card_layout('isochron scepter', 'normal').

% Found in: ISD, MD1
card_name('isolated chapel', 'Isolated Chapel').
card_type('isolated chapel', 'Land').
card_types('isolated chapel', ['Land']).
card_subtypes('isolated chapel', []).
card_colors('isolated chapel', []).
card_text('isolated chapel', 'Isolated Chapel enters the battlefield tapped unless you control a Plains or a Swamp.\n{T}: Add {W} or {B} to your mana pool.').
card_layout('isolated chapel', 'normal').

% Found in: NPH
card_name('isolation cell', 'Isolation Cell').
card_type('isolation cell', 'Artifact').
card_types('isolation cell', ['Artifact']).
card_subtypes('isolation cell', []).
card_colors('isolation cell', []).
card_text('isolation cell', 'Whenever an opponent casts a creature spell, that player loses 2 life unless he or she pays {2}.').
card_mana_cost('isolation cell', ['4']).
card_cmc('isolation cell', 4).
card_layout('isolation cell', 'normal').

% Found in: DIS
card_name('isperia the inscrutable', 'Isperia the Inscrutable').
card_type('isperia the inscrutable', 'Legendary Creature — Sphinx').
card_types('isperia the inscrutable', ['Creature']).
card_subtypes('isperia the inscrutable', ['Sphinx']).
card_supertypes('isperia the inscrutable', ['Legendary']).
card_colors('isperia the inscrutable', ['W', 'U']).
card_text('isperia the inscrutable', 'Flying\nWhenever Isperia the Inscrutable deals combat damage to a player, name a card. That player reveals his or her hand. If he or she reveals the named card, search your library for a creature card with flying, reveal it, put it into your hand, then shuffle your library.').
card_mana_cost('isperia the inscrutable', ['1', 'W', 'W', 'U', 'U']).
card_cmc('isperia the inscrutable', 5).
card_layout('isperia the inscrutable', 'normal').
card_power('isperia the inscrutable', 3).
card_toughness('isperia the inscrutable', 6).

% Found in: RTR
card_name('isperia\'s skywatch', 'Isperia\'s Skywatch').
card_type('isperia\'s skywatch', 'Creature — Vedalken Knight').
card_types('isperia\'s skywatch', ['Creature']).
card_subtypes('isperia\'s skywatch', ['Vedalken', 'Knight']).
card_colors('isperia\'s skywatch', ['U']).
card_text('isperia\'s skywatch', 'Flying\nWhen Isperia\'s Skywatch enters the battlefield, detain target creature an opponent controls. (Until your next turn, that creature can\'t attack or block and its activated abilities can\'t be activated.)').
card_mana_cost('isperia\'s skywatch', ['5', 'U']).
card_cmc('isperia\'s skywatch', 6).
card_layout('isperia\'s skywatch', 'normal').
card_power('isperia\'s skywatch', 3).
card_toughness('isperia\'s skywatch', 3).

% Found in: RTR
card_name('isperia, supreme judge', 'Isperia, Supreme Judge').
card_type('isperia, supreme judge', 'Legendary Creature — Sphinx').
card_types('isperia, supreme judge', ['Creature']).
card_subtypes('isperia, supreme judge', ['Sphinx']).
card_supertypes('isperia, supreme judge', ['Legendary']).
card_colors('isperia, supreme judge', ['W', 'U']).
card_text('isperia, supreme judge', 'Flying\nWhenever a creature attacks you or a planeswalker you control, you may draw a card.').
card_mana_cost('isperia, supreme judge', ['2', 'W', 'W', 'U', 'U']).
card_cmc('isperia, supreme judge', 6).
card_layout('isperia, supreme judge', 'normal').
card_power('isperia, supreme judge', 6).
card_toughness('isperia, supreme judge', 4).

% Found in: DDP, ROE
card_name('it that betrays', 'It That Betrays').
card_type('it that betrays', 'Creature — Eldrazi').
card_types('it that betrays', ['Creature']).
card_subtypes('it that betrays', ['Eldrazi']).
card_colors('it that betrays', []).
card_text('it that betrays', 'Annihilator 2 (Whenever this creature attacks, defending player sacrifices two permanents.)\nWhenever an opponent sacrifices a nontoken permanent, put that card onto the battlefield under your control.').
card_mana_cost('it that betrays', ['12']).
card_cmc('it that betrays', 12).
card_layout('it that betrays', 'normal').
card_power('it that betrays', 11).
card_toughness('it that betrays', 11).

% Found in: CNS
card_name('iterative analysis', 'Iterative Analysis').
card_type('iterative analysis', 'Conspiracy').
card_types('iterative analysis', ['Conspiracy']).
card_subtypes('iterative analysis', []).
card_colors('iterative analysis', []).
card_text('iterative analysis', 'Hidden agenda (Start the game with this conspiracy face down in the command zone and secretly name a card. You may turn this conspiracy face up any time and reveal the chosen name.)\nWhenever you cast an instant or sorcery spell with the chosen name, you may draw a card.').
card_layout('iterative analysis', 'normal').

% Found in: TSP
card_name('ith, high arcanist', 'Ith, High Arcanist').
card_type('ith, high arcanist', 'Legendary Creature — Human Wizard').
card_types('ith, high arcanist', ['Creature']).
card_subtypes('ith, high arcanist', ['Human', 'Wizard']).
card_supertypes('ith, high arcanist', ['Legendary']).
card_colors('ith, high arcanist', ['W', 'U']).
card_text('ith, high arcanist', 'Vigilance\n{T}: Untap target attacking creature. Prevent all combat damage that would be dealt to and dealt by that creature this turn.\nSuspend 4—{W}{U} (Rather than cast this card from your hand, you may pay {W}{U} and exile it with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_mana_cost('ith, high arcanist', ['5', 'W', 'U']).
card_cmc('ith, high arcanist', 7).
card_layout('ith, high arcanist', 'normal').
card_power('ith, high arcanist', 3).
card_toughness('ith, high arcanist', 5).

% Found in: MIR
card_name('ivory charm', 'Ivory Charm').
card_type('ivory charm', 'Instant').
card_types('ivory charm', ['Instant']).
card_subtypes('ivory charm', []).
card_colors('ivory charm', ['W']).
card_text('ivory charm', 'Choose one —\n• All creatures get -2/-0 until end of turn.\n• Tap target creature.\n• Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_mana_cost('ivory charm', ['W']).
card_cmc('ivory charm', 1).
card_layout('ivory charm', 'normal').

% Found in: SOK
card_name('ivory crane netsuke', 'Ivory Crane Netsuke').
card_type('ivory crane netsuke', 'Artifact').
card_types('ivory crane netsuke', ['Artifact']).
card_subtypes('ivory crane netsuke', []).
card_colors('ivory crane netsuke', []).
card_text('ivory crane netsuke', 'At the beginning of your upkeep, if you have seven or more cards in hand, you gain 4 life.').
card_mana_cost('ivory crane netsuke', ['2']).
card_cmc('ivory crane netsuke', 2).
card_layout('ivory crane netsuke', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, CED, CEI, LEA, LEB
card_name('ivory cup', 'Ivory Cup').
card_type('ivory cup', 'Artifact').
card_types('ivory cup', ['Artifact']).
card_subtypes('ivory cup', []).
card_colors('ivory cup', []).
card_text('ivory cup', 'Whenever a player casts a white spell, you may pay {1}. If you do, you gain 1 life.').
card_mana_cost('ivory cup', ['1']).
card_cmc('ivory cup', 1).
card_layout('ivory cup', 'normal').

% Found in: ALL, ME2
card_name('ivory gargoyle', 'Ivory Gargoyle').
card_type('ivory gargoyle', 'Creature — Gargoyle').
card_types('ivory gargoyle', ['Creature']).
card_subtypes('ivory gargoyle', ['Gargoyle']).
card_colors('ivory gargoyle', ['W']).
card_text('ivory gargoyle', 'Flying\nWhen Ivory Gargoyle dies, return it to the battlefield under its owner\'s control at the beginning of the next end step and you skip your next draw step.\n{4}{W}: Exile Ivory Gargoyle.').
card_mana_cost('ivory gargoyle', ['4', 'W']).
card_cmc('ivory gargoyle', 5).
card_layout('ivory gargoyle', 'normal').
card_power('ivory gargoyle', 2).
card_toughness('ivory gargoyle', 2).
card_reserved('ivory gargoyle').

% Found in: MMA, TSP
card_name('ivory giant', 'Ivory Giant').
card_type('ivory giant', 'Creature — Giant').
card_types('ivory giant', ['Creature']).
card_subtypes('ivory giant', ['Giant']).
card_colors('ivory giant', ['W']).
card_text('ivory giant', 'When Ivory Giant enters the battlefield, tap all nonwhite creatures.\nSuspend 5—{W} (Rather than cast this card from your hand, you may pay {W} and exile it with five time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_mana_cost('ivory giant', ['5', 'W', 'W']).
card_cmc('ivory giant', 7).
card_layout('ivory giant', 'normal').
card_power('ivory giant', 3).
card_toughness('ivory giant', 4).

% Found in: 5ED, CHR, LEG, ME3
card_name('ivory guardians', 'Ivory Guardians').
card_type('ivory guardians', 'Creature — Giant Cleric').
card_types('ivory guardians', ['Creature']).
card_subtypes('ivory guardians', ['Giant', 'Cleric']).
card_colors('ivory guardians', ['W']).
card_text('ivory guardians', 'Protection from red\nCreatures named Ivory Guardians get +1/+1 as long as an opponent controls a nontoken red permanent.').
card_mana_cost('ivory guardians', ['4', 'W', 'W']).
card_cmc('ivory guardians', 6).
card_layout('ivory guardians', 'normal').
card_power('ivory guardians', 3).
card_toughness('ivory guardians', 3).

% Found in: 8ED, 9ED, MMQ
card_name('ivory mask', 'Ivory Mask').
card_type('ivory mask', 'Enchantment').
card_types('ivory mask', ['Enchantment']).
card_subtypes('ivory mask', []).
card_colors('ivory mask', ['W']).
card_text('ivory mask', 'You have shroud. (You can\'t be the target of spells or abilities.)').
card_mana_cost('ivory mask', ['2', 'W', 'W']).
card_cmc('ivory mask', 4).
card_layout('ivory mask', 'normal').

% Found in: 3ED, 4ED, ATQ, MED, V10, VMA
card_name('ivory tower', 'Ivory Tower').
card_type('ivory tower', 'Artifact').
card_types('ivory tower', ['Artifact']).
card_subtypes('ivory tower', []).
card_colors('ivory tower', []).
card_text('ivory tower', 'At the beginning of your upkeep, you gain X life, where X is the number of cards in your hand minus 4.').
card_mana_cost('ivory tower', ['1']).
card_cmc('ivory tower', 1).
card_layout('ivory tower', 'normal').

% Found in: KTK, pMEI, pPRE
card_name('ivorytusk fortress', 'Ivorytusk Fortress').
card_type('ivorytusk fortress', 'Creature — Elephant').
card_types('ivorytusk fortress', ['Creature']).
card_subtypes('ivorytusk fortress', ['Elephant']).
card_colors('ivorytusk fortress', ['W', 'B', 'G']).
card_text('ivorytusk fortress', 'Untap each creature you control with a +1/+1 counter on it during each other player\'s untap step.').
card_mana_cost('ivorytusk fortress', ['2', 'W', 'B', 'G']).
card_cmc('ivorytusk fortress', 5).
card_layout('ivorytusk fortress', 'normal').
card_power('ivorytusk fortress', 5).
card_toughness('ivorytusk fortress', 7).

% Found in: RAV
card_name('ivy dancer', 'Ivy Dancer').
card_type('ivy dancer', 'Creature — Dryad Shaman').
card_types('ivy dancer', ['Creature']).
card_subtypes('ivy dancer', ['Dryad', 'Shaman']).
card_colors('ivy dancer', ['G']).
card_text('ivy dancer', '{T}: Target creature gains forestwalk until end of turn. (It can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('ivy dancer', ['2', 'G']).
card_cmc('ivy dancer', 3).
card_layout('ivy dancer', 'normal').
card_power('ivy dancer', 1).
card_toughness('ivy dancer', 2).

% Found in: HOP, ODY
card_name('ivy elemental', 'Ivy Elemental').
card_type('ivy elemental', 'Creature — Elemental').
card_types('ivy elemental', ['Creature']).
card_subtypes('ivy elemental', ['Elemental']).
card_colors('ivy elemental', ['G']).
card_text('ivy elemental', 'Ivy Elemental enters the battlefield with X +1/+1 counters on it.').
card_mana_cost('ivy elemental', ['X', 'G']).
card_cmc('ivy elemental', 1).
card_layout('ivy elemental', 'normal').
card_power('ivy elemental', 0).
card_toughness('ivy elemental', 0).

% Found in: GTC
card_name('ivy lane denizen', 'Ivy Lane Denizen').
card_type('ivy lane denizen', 'Creature — Elf Warrior').
card_types('ivy lane denizen', ['Creature']).
card_subtypes('ivy lane denizen', ['Elf', 'Warrior']).
card_colors('ivy lane denizen', ['G']).
card_text('ivy lane denizen', 'Whenever another green creature enters the battlefield under your control, put a +1/+1 counter on target creature.').
card_mana_cost('ivy lane denizen', ['3', 'G']).
card_cmc('ivy lane denizen', 4).
card_layout('ivy lane denizen', 'normal').
card_power('ivy lane denizen', 2).
card_toughness('ivy lane denizen', 3).

% Found in: UDS
card_name('ivy seer', 'Ivy Seer').
card_type('ivy seer', 'Creature — Elf Wizard').
card_types('ivy seer', ['Creature']).
card_subtypes('ivy seer', ['Elf', 'Wizard']).
card_colors('ivy seer', ['G']).
card_text('ivy seer', '{2}{G}, {T}: Reveal any number of green cards in your hand. Target creature gets +X/+X until end of turn, where X is the number of cards revealed this way.').
card_mana_cost('ivy seer', ['3', 'G']).
card_cmc('ivy seer', 4).
card_layout('ivy seer', 'normal').
card_power('ivy seer', 1).
card_toughness('ivy seer', 1).

% Found in: BOK
card_name('iwamori of the open fist', 'Iwamori of the Open Fist').
card_type('iwamori of the open fist', 'Legendary Creature — Human Monk').
card_types('iwamori of the open fist', ['Creature']).
card_subtypes('iwamori of the open fist', ['Human', 'Monk']).
card_supertypes('iwamori of the open fist', ['Legendary']).
card_colors('iwamori of the open fist', ['G']).
card_text('iwamori of the open fist', 'Trample\nWhen Iwamori of the Open Fist enters the battlefield, each opponent may put a legendary creature card from his or her hand onto the battlefield.').
card_mana_cost('iwamori of the open fist', ['2', 'G', 'G']).
card_cmc('iwamori of the open fist', 4).
card_layout('iwamori of the open fist', 'normal').
card_power('iwamori of the open fist', 5).
card_toughness('iwamori of the open fist', 5).

% Found in: ONS
card_name('ixidor\'s will', 'Ixidor\'s Will').
card_type('ixidor\'s will', 'Instant').
card_types('ixidor\'s will', ['Instant']).
card_subtypes('ixidor\'s will', []).
card_colors('ixidor\'s will', ['U']).
card_text('ixidor\'s will', 'Counter target spell unless its controller pays {2} for each Wizard on the battlefield.').
card_mana_cost('ixidor\'s will', ['2', 'U']).
card_cmc('ixidor\'s will', 3).
card_layout('ixidor\'s will', 'normal').

% Found in: ONS
card_name('ixidor, reality sculptor', 'Ixidor, Reality Sculptor').
card_type('ixidor, reality sculptor', 'Legendary Creature — Human Wizard').
card_types('ixidor, reality sculptor', ['Creature']).
card_subtypes('ixidor, reality sculptor', ['Human', 'Wizard']).
card_supertypes('ixidor, reality sculptor', ['Legendary']).
card_colors('ixidor, reality sculptor', ['U']).
card_text('ixidor, reality sculptor', 'Face-down creatures get +1/+1.\n{2}{U}: Turn target face-down creature face up.').
card_mana_cost('ixidor, reality sculptor', ['3', 'U', 'U']).
card_cmc('ixidor, reality sculptor', 5).
card_layout('ixidor, reality sculptor', 'normal').
card_power('ixidor, reality sculptor', 3).
card_toughness('ixidor, reality sculptor', 4).

% Found in: C14, TSP
card_name('ixidron', 'Ixidron').
card_type('ixidron', 'Creature — Illusion').
card_types('ixidron', ['Creature']).
card_subtypes('ixidron', ['Illusion']).
card_colors('ixidron', ['U']).
card_text('ixidron', 'As Ixidron enters the battlefield, turn all other nontoken creatures face down. (They\'re 2/2 creatures.)\nIxidron\'s power and toughness are each equal to the number of face-down creatures on the battlefield.').
card_mana_cost('ixidron', ['3', 'U', 'U']).
card_cmc('ixidron', 5).
card_layout('ixidron', 'normal').
card_power('ixidron', '*').
card_toughness('ixidron', '*').

% Found in: C13, CMD, DDJ, GPT, MM2
card_name('izzet boilerworks', 'Izzet Boilerworks').
card_type('izzet boilerworks', 'Land').
card_types('izzet boilerworks', ['Land']).
card_subtypes('izzet boilerworks', []).
card_colors('izzet boilerworks', []).
card_text('izzet boilerworks', 'Izzet Boilerworks enters the battlefield tapped.\nWhen Izzet Boilerworks enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {U}{R} to your mana pool.').
card_layout('izzet boilerworks', 'normal').

% Found in: DDJ, RTR, pFNM
card_name('izzet charm', 'Izzet Charm').
card_type('izzet charm', 'Instant').
card_types('izzet charm', ['Instant']).
card_subtypes('izzet charm', []).
card_colors('izzet charm', ['U', 'R']).
card_text('izzet charm', 'Choose one —\n• Counter target noncreature spell unless its controller pays {2}.\n• Izzet Charm deals 2 damage to target creature.\n• Draw two cards, then discard two cards.').
card_mana_cost('izzet charm', ['U', 'R']).
card_cmc('izzet charm', 2).
card_layout('izzet charm', 'normal').

% Found in: CMD, DDJ, GPT
card_name('izzet chronarch', 'Izzet Chronarch').
card_type('izzet chronarch', 'Creature — Human Wizard').
card_types('izzet chronarch', ['Creature']).
card_subtypes('izzet chronarch', ['Human', 'Wizard']).
card_colors('izzet chronarch', ['U', 'R']).
card_text('izzet chronarch', 'When Izzet Chronarch enters the battlefield, return target instant or sorcery card from your graveyard to your hand.').
card_mana_cost('izzet chronarch', ['3', 'U', 'R']).
card_cmc('izzet chronarch', 5).
card_layout('izzet chronarch', 'normal').
card_power('izzet chronarch', 2).
card_toughness('izzet chronarch', 2).

% Found in: DGM
card_name('izzet cluestone', 'Izzet Cluestone').
card_type('izzet cluestone', 'Artifact').
card_types('izzet cluestone', ['Artifact']).
card_subtypes('izzet cluestone', []).
card_colors('izzet cluestone', []).
card_text('izzet cluestone', '{T}: Add {U} or {R} to your mana pool.\n{U}{R}, {T}, Sacrifice Izzet Cluestone: Draw a card.').
card_mana_cost('izzet cluestone', ['3']).
card_cmc('izzet cluestone', 3).
card_layout('izzet cluestone', 'normal').

% Found in: C13, DGM, RTR
card_name('izzet guildgate', 'Izzet Guildgate').
card_type('izzet guildgate', 'Land — Gate').
card_types('izzet guildgate', ['Land']).
card_subtypes('izzet guildgate', ['Gate']).
card_colors('izzet guildgate', []).
card_text('izzet guildgate', 'Izzet Guildgate enters the battlefield tapped.\n{T}: Add {U} or {R} to your mana pool.').
card_layout('izzet guildgate', 'normal').

% Found in: DDJ, GPT
card_name('izzet guildmage', 'Izzet Guildmage').
card_type('izzet guildmage', 'Creature — Human Wizard').
card_types('izzet guildmage', ['Creature']).
card_subtypes('izzet guildmage', ['Human', 'Wizard']).
card_colors('izzet guildmage', ['U', 'R']).
card_text('izzet guildmage', '({U/R} can be paid with either {U} or {R}.)\n{2}{U}: Copy target instant spell you control with converted mana cost 2 or less. You may choose new targets for the copy.\n{2}{R}: Copy target sorcery spell you control with converted mana cost 2 or less. You may choose new targets for the copy.').
card_mana_cost('izzet guildmage', ['U/R', 'U/R']).
card_cmc('izzet guildmage', 2).
card_layout('izzet guildmage', 'normal').
card_power('izzet guildmage', 2).
card_toughness('izzet guildmage', 2).

% Found in: RTR
card_name('izzet keyrune', 'Izzet Keyrune').
card_type('izzet keyrune', 'Artifact').
card_types('izzet keyrune', ['Artifact']).
card_subtypes('izzet keyrune', []).
card_colors('izzet keyrune', []).
card_text('izzet keyrune', '{T}: Add {U} or {R} to your mana pool.\n{U}{R}: Until end of turn, Izzet Keyrune becomes a 2/1 blue and red Elemental artifact creature.\nWhenever Izzet Keyrune deals combat damage to a player, you may draw a card. If you do, discard a card.').
card_mana_cost('izzet keyrune', ['3']).
card_cmc('izzet keyrune', 3).
card_layout('izzet keyrune', 'normal').

% Found in: CMD, DDJ, GPT
card_name('izzet signet', 'Izzet Signet').
card_type('izzet signet', 'Artifact').
card_types('izzet signet', ['Artifact']).
card_subtypes('izzet signet', []).
card_colors('izzet signet', []).
card_text('izzet signet', '{1}, {T}: Add {U}{R} to your mana pool.').
card_mana_cost('izzet signet', ['2']).
card_cmc('izzet signet', 2).
card_layout('izzet signet', 'normal').

% Found in: RTR
card_name('izzet staticaster', 'Izzet Staticaster').
card_type('izzet staticaster', 'Creature — Human Wizard').
card_types('izzet staticaster', ['Creature']).
card_subtypes('izzet staticaster', ['Human', 'Wizard']).
card_colors('izzet staticaster', ['U', 'R']).
card_text('izzet staticaster', 'Flash (You may cast this spell any time you could cast an instant.)\nHaste\n{T}: Izzet Staticaster deals 1 damage to target creature and each other creature with the same name as that creature.').
card_mana_cost('izzet staticaster', ['1', 'U', 'R']).
card_cmc('izzet staticaster', 3).
card_layout('izzet staticaster', 'normal').
card_power('izzet staticaster', 0).
card_toughness('izzet staticaster', 3).

% Found in: HOP
card_name('izzet steam maze', 'Izzet Steam Maze').
card_type('izzet steam maze', 'Plane — Ravnica').
card_types('izzet steam maze', ['Plane']).
card_subtypes('izzet steam maze', ['Ravnica']).
card_colors('izzet steam maze', []).
card_text('izzet steam maze', 'Whenever a player casts an instant or sorcery spell, that player copies it. The player may choose new targets for the copy.\nWhenever you roll {C}, instant and sorcery spells you cast this turn cost {3} less to cast.').
card_layout('izzet steam maze', 'plane').

