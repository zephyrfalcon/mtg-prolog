% Card-specific data

% Found in: BFZ
card_name('zada, hedron grinder', 'Zada, Hedron Grinder').
card_type('zada, hedron grinder', 'Legendary Creature — Goblin Ally').
card_types('zada, hedron grinder', ['Creature']).
card_subtypes('zada, hedron grinder', ['Goblin', 'Ally']).
card_supertypes('zada, hedron grinder', ['Legendary']).
card_colors('zada, hedron grinder', ['R']).
card_text('zada, hedron grinder', 'Whenever you cast an instant or sorcery spell that targets only Zada, Hedron Grinder, copy that spell for each other creature you control that the spell could target. Each copy targets a different one of those creatures.').
card_mana_cost('zada, hedron grinder', ['3', 'R']).
card_cmc('zada, hedron grinder', 4).
card_layout('zada, hedron grinder', 'normal').
card_power('zada, hedron grinder', 3).
card_toughness('zada, hedron grinder', 3).

% Found in: GTC, pMGD
card_name('zameck guildmage', 'Zameck Guildmage').
card_type('zameck guildmage', 'Creature — Elf Wizard').
card_types('zameck guildmage', ['Creature']).
card_subtypes('zameck guildmage', ['Elf', 'Wizard']).
card_colors('zameck guildmage', ['U', 'G']).
card_text('zameck guildmage', '{G}{U}: This turn, each creature you control enters the battlefield with an additional +1/+1 counter on it.\n{G}{U}, Remove a +1/+1 counter from a creature you control: Draw a card.').
card_mana_cost('zameck guildmage', ['G', 'U']).
card_cmc('zameck guildmage', 2).
card_layout('zameck guildmage', 'normal').
card_power('zameck guildmage', 2).
card_toughness('zameck guildmage', 2).

% Found in: INV
card_name('zanam djinn', 'Zanam Djinn').
card_type('zanam djinn', 'Creature — Djinn').
card_types('zanam djinn', ['Creature']).
card_subtypes('zanam djinn', ['Djinn']).
card_colors('zanam djinn', ['U']).
card_text('zanam djinn', 'Flying\nZanam Djinn gets -2/-2 as long as blue is the most common color among all permanents or is tied for most common.').
card_mana_cost('zanam djinn', ['5', 'U']).
card_cmc('zanam djinn', 6).
card_layout('zanam djinn', 'normal').
card_power('zanam djinn', 5).
card_toughness('zanam djinn', 6).

% Found in: RTR
card_name('zanikev locust', 'Zanikev Locust').
card_type('zanikev locust', 'Creature — Insect').
card_types('zanikev locust', ['Creature']).
card_subtypes('zanikev locust', ['Insect']).
card_colors('zanikev locust', ['B']).
card_text('zanikev locust', 'Flying\nScavenge {2}{B}{B} ({2}{B}{B}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_mana_cost('zanikev locust', ['5', 'B']).
card_cmc('zanikev locust', 6).
card_layout('zanikev locust', 'normal').
card_power('zanikev locust', 3).
card_toughness('zanikev locust', 3).

% Found in: INV
card_name('zap', 'Zap').
card_type('zap', 'Instant').
card_types('zap', ['Instant']).
card_subtypes('zap', []).
card_colors('zap', ['R']).
card_text('zap', 'Zap deals 1 damage to target creature or player.\nDraw a card.').
card_mana_cost('zap', ['2', 'R']).
card_cmc('zap', 3).
card_layout('zap', 'normal').

% Found in: GTC
card_name('zarichi tiger', 'Zarichi Tiger').
card_type('zarichi tiger', 'Creature — Cat').
card_types('zarichi tiger', ['Creature']).
card_subtypes('zarichi tiger', ['Cat']).
card_colors('zarichi tiger', ['W']).
card_text('zarichi tiger', '{1}{W}, {T}: You gain 2 life.').
card_mana_cost('zarichi tiger', ['3', 'W']).
card_cmc('zarichi tiger', 4).
card_layout('zarichi tiger', 'normal').
card_power('zarichi tiger', 2).
card_toughness('zarichi tiger', 3).

% Found in: TSP
card_name('zealot il-vec', 'Zealot il-Vec').
card_type('zealot il-vec', 'Creature — Human Rebel').
card_types('zealot il-vec', ['Creature']).
card_subtypes('zealot il-vec', ['Human', 'Rebel']).
card_colors('zealot il-vec', ['W']).
card_text('zealot il-vec', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhenever Zealot il-Vec attacks and isn\'t blocked, you may have it deal 1 damage to target creature. If you do, prevent all combat damage Zealot il-Vec would deal this turn.').
card_mana_cost('zealot il-vec', ['2', 'W']).
card_cmc('zealot il-vec', 3).
card_layout('zealot il-vec', 'normal').
card_power('zealot il-vec', 1).
card_toughness('zealot il-vec', 1).

% Found in: EXO
card_name('zealots en-dal', 'Zealots en-Dal').
card_type('zealots en-dal', 'Creature — Human Soldier').
card_types('zealots en-dal', ['Creature']).
card_subtypes('zealots en-dal', ['Human', 'Soldier']).
card_colors('zealots en-dal', ['W']).
card_text('zealots en-dal', 'At the beginning of your upkeep, if all nonland permanents you control are white, you gain 1 life.').
card_mana_cost('zealots en-dal', ['3', 'W']).
card_cmc('zealots en-dal', 4).
card_layout('zealots en-dal', 'normal').
card_power('zealots en-dal', 2).
card_toughness('zealots en-dal', 4).

% Found in: AVR
card_name('zealous conscripts', 'Zealous Conscripts').
card_type('zealous conscripts', 'Creature — Human Warrior').
card_types('zealous conscripts', ['Creature']).
card_subtypes('zealous conscripts', ['Human', 'Warrior']).
card_colors('zealous conscripts', ['R']).
card_text('zealous conscripts', 'Haste\nWhen Zealous Conscripts enters the battlefield, gain control of target permanent until end of turn. Untap that permanent. It gains haste until end of turn.').
card_mana_cost('zealous conscripts', ['4', 'R']).
card_cmc('zealous conscripts', 5).
card_layout('zealous conscripts', 'normal').
card_power('zealous conscripts', 3).
card_toughness('zealous conscripts', 3).

% Found in: SHM
card_name('zealous guardian', 'Zealous Guardian').
card_type('zealous guardian', 'Creature — Kithkin Soldier').
card_types('zealous guardian', ['Creature']).
card_subtypes('zealous guardian', ['Kithkin', 'Soldier']).
card_colors('zealous guardian', ['W', 'U']).
card_text('zealous guardian', 'Flash').
card_mana_cost('zealous guardian', ['W/U']).
card_cmc('zealous guardian', 1).
card_layout('zealous guardian', 'normal').
card_power('zealous guardian', 1).
card_toughness('zealous guardian', 1).

% Found in: 9ED, SCG
card_name('zealous inquisitor', 'Zealous Inquisitor').
card_type('zealous inquisitor', 'Creature — Human Cleric').
card_types('zealous inquisitor', ['Creature']).
card_subtypes('zealous inquisitor', ['Human', 'Cleric']).
card_colors('zealous inquisitor', ['W']).
card_text('zealous inquisitor', '{1}{W}: The next 1 damage that would be dealt to Zealous Inquisitor this turn is dealt to target creature instead.').
card_mana_cost('zealous inquisitor', ['2', 'W']).
card_cmc('zealous inquisitor', 3).
card_layout('zealous inquisitor', 'normal').
card_power('zealous inquisitor', 2).
card_toughness('zealous inquisitor', 2).

% Found in: ARB, DDK, MD1
card_name('zealous persecution', 'Zealous Persecution').
card_type('zealous persecution', 'Instant').
card_types('zealous persecution', ['Instant']).
card_subtypes('zealous persecution', []).
card_colors('zealous persecution', ['W', 'B']).
card_text('zealous persecution', 'Until end of turn, creatures you control get +1/+1 and creatures your opponents control get -1/-1.').
card_mana_cost('zealous persecution', ['W', 'B']).
card_cmc('zealous persecution', 2).
card_layout('zealous persecution', 'normal').

% Found in: AVR
card_name('zealous strike', 'Zealous Strike').
card_type('zealous strike', 'Instant').
card_types('zealous strike', ['Instant']).
card_subtypes('zealous strike', []).
card_colors('zealous strike', ['W']).
card_text('zealous strike', 'Target creature gets +2/+2 and gains first strike until end of turn.').
card_mana_cost('zealous strike', ['1', 'W']).
card_cmc('zealous strike', 2).
card_layout('zealous strike', 'normal').

% Found in: MIR
card_name('zebra unicorn', 'Zebra Unicorn').
card_type('zebra unicorn', 'Creature — Unicorn').
card_types('zebra unicorn', ['Creature']).
card_subtypes('zebra unicorn', ['Unicorn']).
card_colors('zebra unicorn', ['W', 'G']).
card_text('zebra unicorn', 'Whenever Zebra Unicorn deals damage, you gain that much life.').
card_mana_cost('zebra unicorn', ['2', 'G', 'W']).
card_cmc('zebra unicorn', 4).
card_layout('zebra unicorn', 'normal').
card_power('zebra unicorn', 2).
card_toughness('zebra unicorn', 2).

% Found in: CMD
card_name('zedruu the greathearted', 'Zedruu the Greathearted').
card_type('zedruu the greathearted', 'Legendary Creature — Minotaur Monk').
card_types('zedruu the greathearted', ['Creature']).
card_subtypes('zedruu the greathearted', ['Minotaur', 'Monk']).
card_supertypes('zedruu the greathearted', ['Legendary']).
card_colors('zedruu the greathearted', ['W', 'U', 'R']).
card_text('zedruu the greathearted', 'At the beginning of your upkeep, you gain X life and draw X cards, where X is the number of permanents you own that your opponents control.\n{R}{W}{U}: Target opponent gains control of target permanent you control.').
card_mana_cost('zedruu the greathearted', ['1', 'R', 'W', 'U']).
card_cmc('zedruu the greathearted', 4).
card_layout('zedruu the greathearted', 'normal').
card_power('zedruu the greathearted', 2).
card_toughness('zedruu the greathearted', 4).

% Found in: ZEN
card_name('zektar shrine expedition', 'Zektar Shrine Expedition').
card_type('zektar shrine expedition', 'Enchantment').
card_types('zektar shrine expedition', ['Enchantment']).
card_subtypes('zektar shrine expedition', []).
card_colors('zektar shrine expedition', ['R']).
card_text('zektar shrine expedition', 'Landfall — Whenever a land enters the battlefield under your control, you may put a quest counter on Zektar Shrine Expedition.\nRemove three quest counters from Zektar Shrine Expedition and sacrifice it: Put a 7/1 red Elemental creature token with trample and haste onto the battlefield. Exile it at the beginning of the next end step.').
card_mana_cost('zektar shrine expedition', ['1', 'R']).
card_cmc('zektar shrine expedition', 2).
card_layout('zektar shrine expedition', 'normal').

% Found in: FEM
card_name('zelyon sword', 'Zelyon Sword').
card_type('zelyon sword', 'Artifact').
card_types('zelyon sword', ['Artifact']).
card_subtypes('zelyon sword', []).
card_colors('zelyon sword', []).
card_text('zelyon sword', 'You may choose not to untap Zelyon Sword during your untap step.\n{3}, {T}: Target creature gets +2/+0 for as long as Zelyon Sword remains tapped.').
card_mana_cost('zelyon sword', ['3']).
card_cmc('zelyon sword', 3).
card_layout('zelyon sword', 'normal').
card_reserved('zelyon sword').

% Found in: ZEN
card_name('zendikar farguide', 'Zendikar Farguide').
card_type('zendikar farguide', 'Creature — Elemental').
card_types('zendikar farguide', ['Creature']).
card_subtypes('zendikar farguide', ['Elemental']).
card_colors('zendikar farguide', ['G']).
card_text('zendikar farguide', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('zendikar farguide', ['4', 'G']).
card_cmc('zendikar farguide', 5).
card_layout('zendikar farguide', 'normal').
card_power('zendikar farguide', 3).
card_toughness('zendikar farguide', 3).

% Found in: ORI
card_name('zendikar incarnate', 'Zendikar Incarnate').
card_type('zendikar incarnate', 'Creature — Elemental').
card_types('zendikar incarnate', ['Creature']).
card_subtypes('zendikar incarnate', ['Elemental']).
card_colors('zendikar incarnate', ['R', 'G']).
card_text('zendikar incarnate', 'Zendikar Incarnate\'s power is equal to the number of lands you control.').
card_mana_cost('zendikar incarnate', ['2', 'R', 'G']).
card_cmc('zendikar incarnate', 4).
card_layout('zendikar incarnate', 'normal').
card_power('zendikar incarnate', '*').
card_toughness('zendikar incarnate', 4).

% Found in: ORI
card_name('zendikar\'s roil', 'Zendikar\'s Roil').
card_type('zendikar\'s roil', 'Enchantment').
card_types('zendikar\'s roil', ['Enchantment']).
card_subtypes('zendikar\'s roil', []).
card_colors('zendikar\'s roil', ['G']).
card_text('zendikar\'s roil', 'Whenever a land enters the battlefield under your control, put a 2/2 green Elemental creature token onto the battlefield.').
card_mana_cost('zendikar\'s roil', ['3', 'G', 'G']).
card_cmc('zendikar\'s roil', 5).
card_layout('zendikar\'s roil', 'normal').

% Found in: USG
card_name('zephid', 'Zephid').
card_type('zephid', 'Creature — Illusion').
card_types('zephid', ['Creature']).
card_subtypes('zephid', ['Illusion']).
card_colors('zephid', ['U']).
card_text('zephid', 'Flying\nShroud (This creature can\'t be the target of spells or abilities.)').
card_mana_cost('zephid', ['4', 'U', 'U']).
card_cmc('zephid', 6).
card_layout('zephid', 'normal').
card_power('zephid', 3).
card_toughness('zephid', 4).
card_reserved('zephid').

% Found in: USG
card_name('zephid\'s embrace', 'Zephid\'s Embrace').
card_type('zephid\'s embrace', 'Enchantment — Aura').
card_types('zephid\'s embrace', ['Enchantment']).
card_subtypes('zephid\'s embrace', ['Aura']).
card_colors('zephid\'s embrace', ['U']).
card_text('zephid\'s embrace', 'Enchant creature\nEnchanted creature gets +2/+2 and has flying and shroud. (It can\'t be the target of spells or abilities.)').
card_mana_cost('zephid\'s embrace', ['2', 'U', 'U']).
card_cmc('zephid\'s embrace', 4).
card_layout('zephid\'s embrace', 'normal').

% Found in: M14
card_name('zephyr charge', 'Zephyr Charge').
card_type('zephyr charge', 'Enchantment').
card_types('zephyr charge', ['Enchantment']).
card_subtypes('zephyr charge', []).
card_colors('zephyr charge', ['U']).
card_text('zephyr charge', '{1}{U}: Target creature gains flying until end of turn.').
card_mana_cost('zephyr charge', ['1', 'U']).
card_cmc('zephyr charge', 2).
card_layout('zephyr charge', 'normal').

% Found in: 4ED, 5ED, ITP, LEG, RQS
card_name('zephyr falcon', 'Zephyr Falcon').
card_type('zephyr falcon', 'Creature — Bird').
card_types('zephyr falcon', ['Creature']).
card_subtypes('zephyr falcon', ['Bird']).
card_colors('zephyr falcon', ['U']).
card_text('zephyr falcon', 'Flying, vigilance').
card_mana_cost('zephyr falcon', ['1', 'U']).
card_cmc('zephyr falcon', 2).
card_layout('zephyr falcon', 'normal').
card_power('zephyr falcon', 1).
card_toughness('zephyr falcon', 1).

% Found in: LRW
card_name('zephyr net', 'Zephyr Net').
card_type('zephyr net', 'Enchantment — Aura').
card_types('zephyr net', ['Enchantment']).
card_subtypes('zephyr net', ['Aura']).
card_colors('zephyr net', ['U']).
card_text('zephyr net', 'Enchant creature\nEnchanted creature has defender and flying.').
card_mana_cost('zephyr net', ['1', 'U']).
card_cmc('zephyr net', 2).
card_layout('zephyr net', 'normal').

% Found in: DTK
card_name('zephyr scribe', 'Zephyr Scribe').
card_type('zephyr scribe', 'Creature — Human Monk').
card_types('zephyr scribe', ['Creature']).
card_subtypes('zephyr scribe', ['Human', 'Monk']).
card_colors('zephyr scribe', ['U']).
card_text('zephyr scribe', '{U}, {T}: Draw a card, then discard a card.\nWhenever you cast a noncreature spell, untap Zephyr Scribe.').
card_mana_cost('zephyr scribe', ['2', 'U']).
card_cmc('zephyr scribe', 3).
card_layout('zephyr scribe', 'normal').
card_power('zephyr scribe', 2).
card_toughness('zephyr scribe', 1).

% Found in: RAV
card_name('zephyr spirit', 'Zephyr Spirit').
card_type('zephyr spirit', 'Creature — Spirit').
card_types('zephyr spirit', ['Creature']).
card_subtypes('zephyr spirit', ['Spirit']).
card_colors('zephyr spirit', ['U']).
card_text('zephyr spirit', 'When Zephyr Spirit blocks, return it to its owner\'s hand.').
card_mana_cost('zephyr spirit', ['5', 'U']).
card_cmc('zephyr spirit', 6).
card_layout('zephyr spirit', 'normal').
card_power('zephyr spirit', 0).
card_toughness('zephyr spirit', 6).

% Found in: M10
card_name('zephyr sprite', 'Zephyr Sprite').
card_type('zephyr sprite', 'Creature — Faerie').
card_types('zephyr sprite', ['Creature']).
card_subtypes('zephyr sprite', ['Faerie']).
card_colors('zephyr sprite', ['U']).
card_text('zephyr sprite', 'Flying').
card_mana_cost('zephyr sprite', ['U']).
card_cmc('zephyr sprite', 1).
card_layout('zephyr sprite', 'normal').
card_power('zephyr sprite', 1).
card_toughness('zephyr sprite', 1).

% Found in: PCY
card_name('zerapa minotaur', 'Zerapa Minotaur').
card_type('zerapa minotaur', 'Creature — Minotaur').
card_types('zerapa minotaur', ['Creature']).
card_subtypes('zerapa minotaur', ['Minotaur']).
card_colors('zerapa minotaur', ['R']).
card_text('zerapa minotaur', 'First strike\n{2}: Zerapa Minotaur loses first strike until end of turn. Any player may activate this ability.').
card_mana_cost('zerapa minotaur', ['2', 'R', 'R']).
card_cmc('zerapa minotaur', 4).
card_layout('zerapa minotaur', 'normal').
card_power('zerapa minotaur', 3).
card_toughness('zerapa minotaur', 3).

% Found in: DDG, MIR, TSB
card_name('zhalfirin commander', 'Zhalfirin Commander').
card_type('zhalfirin commander', 'Creature — Human Knight').
card_types('zhalfirin commander', ['Creature']).
card_subtypes('zhalfirin commander', ['Human', 'Knight']).
card_colors('zhalfirin commander', ['W']).
card_text('zhalfirin commander', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{1}{W}{W}: Target Knight creature gets +1/+1 until end of turn.').
card_mana_cost('zhalfirin commander', ['2', 'W']).
card_cmc('zhalfirin commander', 3).
card_layout('zhalfirin commander', 'normal').
card_power('zhalfirin commander', 2).
card_toughness('zhalfirin commander', 2).

% Found in: VIS, VMA
card_name('zhalfirin crusader', 'Zhalfirin Crusader').
card_type('zhalfirin crusader', 'Creature — Human Knight').
card_types('zhalfirin crusader', ['Creature']).
card_subtypes('zhalfirin crusader', ['Human', 'Knight']).
card_colors('zhalfirin crusader', ['W']).
card_text('zhalfirin crusader', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{1}{W}: The next 1 damage that would be dealt to Zhalfirin Crusader this turn is dealt to target creature or player instead.').
card_mana_cost('zhalfirin crusader', ['1', 'W', 'W']).
card_cmc('zhalfirin crusader', 3).
card_layout('zhalfirin crusader', 'normal').
card_power('zhalfirin crusader', 2).
card_toughness('zhalfirin crusader', 2).
card_reserved('zhalfirin crusader').

% Found in: MIR
card_name('zhalfirin knight', 'Zhalfirin Knight').
card_type('zhalfirin knight', 'Creature — Human Knight').
card_types('zhalfirin knight', ['Creature']).
card_subtypes('zhalfirin knight', ['Human', 'Knight']).
card_colors('zhalfirin knight', ['W']).
card_text('zhalfirin knight', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{W}{W}: Zhalfirin Knight gains first strike until end of turn.').
card_mana_cost('zhalfirin knight', ['2', 'W']).
card_cmc('zhalfirin knight', 3).
card_layout('zhalfirin knight', 'normal').
card_power('zhalfirin knight', 2).
card_toughness('zhalfirin knight', 2).

% Found in: ME3, PTK
card_name('zhang fei, fierce warrior', 'Zhang Fei, Fierce Warrior').
card_type('zhang fei, fierce warrior', 'Legendary Creature — Human Soldier Warrior').
card_types('zhang fei, fierce warrior', ['Creature']).
card_subtypes('zhang fei, fierce warrior', ['Human', 'Soldier', 'Warrior']).
card_supertypes('zhang fei, fierce warrior', ['Legendary']).
card_colors('zhang fei, fierce warrior', ['W']).
card_text('zhang fei, fierce warrior', 'Vigilance; horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)').
card_mana_cost('zhang fei, fierce warrior', ['4', 'W', 'W']).
card_cmc('zhang fei, fierce warrior', 6).
card_layout('zhang fei, fierce warrior', 'normal').
card_power('zhang fei, fierce warrior', 4).
card_toughness('zhang fei, fierce warrior', 4).

% Found in: PTK
card_name('zhang he, wei general', 'Zhang He, Wei General').
card_type('zhang he, wei general', 'Legendary Creature — Human Soldier').
card_types('zhang he, wei general', ['Creature']).
card_subtypes('zhang he, wei general', ['Human', 'Soldier']).
card_supertypes('zhang he, wei general', ['Legendary']).
card_colors('zhang he, wei general', ['B']).
card_text('zhang he, wei general', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)\nWhenever Zhang He, Wei General attacks, each other creature you control gets +1/+0 until end of turn.').
card_mana_cost('zhang he, wei general', ['3', 'B', 'B']).
card_cmc('zhang he, wei general', 5).
card_layout('zhang he, wei general', 'normal').
card_power('zhang he, wei general', 4).
card_toughness('zhang he, wei general', 2).

% Found in: PTK
card_name('zhang liao, hero of hefei', 'Zhang Liao, Hero of Hefei').
card_type('zhang liao, hero of hefei', 'Legendary Creature — Human Soldier').
card_types('zhang liao, hero of hefei', ['Creature']).
card_subtypes('zhang liao, hero of hefei', ['Human', 'Soldier']).
card_supertypes('zhang liao, hero of hefei', ['Legendary']).
card_colors('zhang liao, hero of hefei', ['B']).
card_text('zhang liao, hero of hefei', 'Whenever Zhang Liao, Hero of Hefei deals damage to an opponent, that opponent discards a card.').
card_mana_cost('zhang liao, hero of hefei', ['4', 'B', 'B']).
card_cmc('zhang liao, hero of hefei', 6).
card_layout('zhang liao, hero of hefei', 'normal').
card_power('zhang liao, hero of hefei', 3).
card_toughness('zhang liao, hero of hefei', 3).

% Found in: PTK
card_name('zhao zilong, tiger general', 'Zhao Zilong, Tiger General').
card_type('zhao zilong, tiger general', 'Legendary Creature — Human Soldier Warrior').
card_types('zhao zilong, tiger general', ['Creature']).
card_subtypes('zhao zilong, tiger general', ['Human', 'Soldier', 'Warrior']).
card_supertypes('zhao zilong, tiger general', ['Legendary']).
card_colors('zhao zilong, tiger general', ['W']).
card_text('zhao zilong, tiger general', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)\nWhenever Zhao Zilong, Tiger General blocks, it gets +1/+1 until end of turn.').
card_mana_cost('zhao zilong, tiger general', ['3', 'W', 'W']).
card_cmc('zhao zilong, tiger general', 5).
card_layout('zhao zilong, tiger general', 'normal').
card_power('zhao zilong, tiger general', 3).
card_toughness('zhao zilong, tiger general', 3).

% Found in: PTK
card_name('zhou yu, chief commander', 'Zhou Yu, Chief Commander').
card_type('zhou yu, chief commander', 'Legendary Creature — Human Soldier').
card_types('zhou yu, chief commander', ['Creature']).
card_subtypes('zhou yu, chief commander', ['Human', 'Soldier']).
card_supertypes('zhou yu, chief commander', ['Legendary']).
card_colors('zhou yu, chief commander', ['U']).
card_text('zhou yu, chief commander', 'Zhou Yu, Chief Commander can\'t attack unless defending player controls an Island.').
card_mana_cost('zhou yu, chief commander', ['5', 'U', 'U']).
card_cmc('zhou yu, chief commander', 7).
card_layout('zhou yu, chief commander', 'normal').
card_power('zhou yu, chief commander', 8).
card_toughness('zhou yu, chief commander', 8).

% Found in: PTK
card_name('zhuge jin, wu strategist', 'Zhuge Jin, Wu Strategist').
card_type('zhuge jin, wu strategist', 'Legendary Creature — Human Advisor').
card_types('zhuge jin, wu strategist', ['Creature']).
card_subtypes('zhuge jin, wu strategist', ['Human', 'Advisor']).
card_supertypes('zhuge jin, wu strategist', ['Legendary']).
card_colors('zhuge jin, wu strategist', ['U']).
card_text('zhuge jin, wu strategist', '{T}: Target creature can\'t be blocked this turn. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('zhuge jin, wu strategist', ['1', 'U', 'U']).
card_cmc('zhuge jin, wu strategist', 3).
card_layout('zhuge jin, wu strategist', 'normal').
card_power('zhuge jin, wu strategist', 1).
card_toughness('zhuge jin, wu strategist', 1).

% Found in: DGM
card_name('zhur-taa ancient', 'Zhur-Taa Ancient').
card_type('zhur-taa ancient', 'Creature — Beast').
card_types('zhur-taa ancient', ['Creature']).
card_subtypes('zhur-taa ancient', ['Beast']).
card_colors('zhur-taa ancient', ['R', 'G']).
card_text('zhur-taa ancient', 'Whenever a player taps a land for mana, that player adds one mana to his or her mana pool of any type that land produced.').
card_mana_cost('zhur-taa ancient', ['3', 'R', 'G']).
card_cmc('zhur-taa ancient', 5).
card_layout('zhur-taa ancient', 'normal').
card_power('zhur-taa ancient', 7).
card_toughness('zhur-taa ancient', 5).

% Found in: DDL, DGM
card_name('zhur-taa druid', 'Zhur-Taa Druid').
card_type('zhur-taa druid', 'Creature — Human Druid').
card_types('zhur-taa druid', ['Creature']).
card_subtypes('zhur-taa druid', ['Human', 'Druid']).
card_colors('zhur-taa druid', ['R', 'G']).
card_text('zhur-taa druid', '{T}: Add {G} to your mana pool.\nWhenever you tap Zhur-Taa Druid for mana, it deals 1 damage to each opponent.').
card_mana_cost('zhur-taa druid', ['R', 'G']).
card_cmc('zhur-taa druid', 2).
card_layout('zhur-taa druid', 'normal').
card_power('zhur-taa druid', 1).
card_toughness('zhur-taa druid', 1).

% Found in: GTC
card_name('zhur-taa swine', 'Zhur-Taa Swine').
card_type('zhur-taa swine', 'Creature — Boar').
card_types('zhur-taa swine', ['Creature']).
card_subtypes('zhur-taa swine', ['Boar']).
card_colors('zhur-taa swine', ['R', 'G']).
card_text('zhur-taa swine', 'Bloodrush — {1}{R}{G}, Discard Zhur-Taa Swine: Target attacking creature gets +5/+4 until end of turn.').
card_mana_cost('zhur-taa swine', ['3', 'R', 'G']).
card_cmc('zhur-taa swine', 5).
card_layout('zhur-taa swine', 'normal').
card_power('zhur-taa swine', 5).
card_toughness('zhur-taa swine', 4).

% Found in: MIR
card_name('zirilan of the claw', 'Zirilan of the Claw').
card_type('zirilan of the claw', 'Legendary Creature — Viashino Shaman').
card_types('zirilan of the claw', ['Creature']).
card_subtypes('zirilan of the claw', ['Viashino', 'Shaman']).
card_supertypes('zirilan of the claw', ['Legendary']).
card_colors('zirilan of the claw', ['R']).
card_text('zirilan of the claw', '{1}{R}{R}, {T}: Search your library for a Dragon permanent card and put that card onto the battlefield. Then shuffle your library. That Dragon gains haste until end of turn. Exile it at the beginning of the next end step.').
card_mana_cost('zirilan of the claw', ['3', 'R', 'R']).
card_cmc('zirilan of the claw', 5).
card_layout('zirilan of the claw', 'normal').
card_power('zirilan of the claw', 3).
card_toughness('zirilan of the claw', 4).
card_reserved('zirilan of the claw').

% Found in: CHK
card_name('zo-zu the punisher', 'Zo-Zu the Punisher').
card_type('zo-zu the punisher', 'Legendary Creature — Goblin Warrior').
card_types('zo-zu the punisher', ['Creature']).
card_subtypes('zo-zu the punisher', ['Goblin', 'Warrior']).
card_supertypes('zo-zu the punisher', ['Legendary']).
card_colors('zo-zu the punisher', ['R']).
card_text('zo-zu the punisher', 'Whenever a land enters the battlefield, Zo-Zu the Punisher deals 2 damage to that land\'s controller.').
card_mana_cost('zo-zu the punisher', ['1', 'R', 'R']).
card_cmc('zo-zu the punisher', 3).
card_layout('zo-zu the punisher', 'normal').
card_power('zo-zu the punisher', 2).
card_toughness('zo-zu the punisher', 2).

% Found in: PTK
card_name('zodiac dog', 'Zodiac Dog').
card_type('zodiac dog', 'Creature — Hound').
card_types('zodiac dog', ['Creature']).
card_subtypes('zodiac dog', ['Hound']).
card_colors('zodiac dog', ['R']).
card_text('zodiac dog', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('zodiac dog', ['2', 'R']).
card_cmc('zodiac dog', 3).
card_layout('zodiac dog', 'normal').
card_power('zodiac dog', 2).
card_toughness('zodiac dog', 2).

% Found in: ME3, PTK
card_name('zodiac dragon', 'Zodiac Dragon').
card_type('zodiac dragon', 'Creature — Dragon').
card_types('zodiac dragon', ['Creature']).
card_subtypes('zodiac dragon', ['Dragon']).
card_colors('zodiac dragon', ['R']).
card_text('zodiac dragon', 'When Zodiac Dragon is put into your graveyard from the battlefield, you may return it to your hand.').
card_mana_cost('zodiac dragon', ['7', 'R', 'R']).
card_cmc('zodiac dragon', 9).
card_layout('zodiac dragon', 'normal').
card_power('zodiac dragon', 8).
card_toughness('zodiac dragon', 8).

% Found in: PTK
card_name('zodiac goat', 'Zodiac Goat').
card_type('zodiac goat', 'Creature — Goat').
card_types('zodiac goat', ['Creature']).
card_subtypes('zodiac goat', ['Goat']).
card_colors('zodiac goat', ['R']).
card_text('zodiac goat', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('zodiac goat', ['R']).
card_cmc('zodiac goat', 1).
card_layout('zodiac goat', 'normal').
card_power('zodiac goat', 1).
card_toughness('zodiac goat', 1).

% Found in: PTK
card_name('zodiac horse', 'Zodiac Horse').
card_type('zodiac horse', 'Creature — Horse').
card_types('zodiac horse', ['Creature']).
card_subtypes('zodiac horse', ['Horse']).
card_colors('zodiac horse', ['G']).
card_text('zodiac horse', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('zodiac horse', ['3', 'G']).
card_cmc('zodiac horse', 4).
card_layout('zodiac horse', 'normal').
card_power('zodiac horse', 3).
card_toughness('zodiac horse', 3).

% Found in: 9ED, PTK
card_name('zodiac monkey', 'Zodiac Monkey').
card_type('zodiac monkey', 'Creature — Ape').
card_types('zodiac monkey', ['Creature']).
card_subtypes('zodiac monkey', ['Ape']).
card_colors('zodiac monkey', ['G']).
card_text('zodiac monkey', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('zodiac monkey', ['1', 'G']).
card_cmc('zodiac monkey', 2).
card_layout('zodiac monkey', 'normal').
card_power('zodiac monkey', 2).
card_toughness('zodiac monkey', 1).

% Found in: PTK
card_name('zodiac ox', 'Zodiac Ox').
card_type('zodiac ox', 'Creature — Ox').
card_types('zodiac ox', ['Creature']).
card_subtypes('zodiac ox', ['Ox']).
card_colors('zodiac ox', ['G']).
card_text('zodiac ox', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('zodiac ox', ['3', 'G']).
card_cmc('zodiac ox', 4).
card_layout('zodiac ox', 'normal').
card_power('zodiac ox', 3).
card_toughness('zodiac ox', 3).

% Found in: PTK
card_name('zodiac pig', 'Zodiac Pig').
card_type('zodiac pig', 'Creature — Boar').
card_types('zodiac pig', ['Creature']).
card_subtypes('zodiac pig', ['Boar']).
card_colors('zodiac pig', ['B']).
card_text('zodiac pig', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('zodiac pig', ['3', 'B']).
card_cmc('zodiac pig', 4).
card_layout('zodiac pig', 'normal').
card_power('zodiac pig', 3).
card_toughness('zodiac pig', 3).

% Found in: PTK
card_name('zodiac rabbit', 'Zodiac Rabbit').
card_type('zodiac rabbit', 'Creature — Rabbit').
card_types('zodiac rabbit', ['Creature']).
card_subtypes('zodiac rabbit', ['Rabbit']).
card_colors('zodiac rabbit', ['G']).
card_text('zodiac rabbit', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('zodiac rabbit', ['G']).
card_cmc('zodiac rabbit', 1).
card_layout('zodiac rabbit', 'normal').
card_power('zodiac rabbit', 1).
card_toughness('zodiac rabbit', 1).

% Found in: PTK
card_name('zodiac rat', 'Zodiac Rat').
card_type('zodiac rat', 'Creature — Rat').
card_types('zodiac rat', ['Creature']).
card_subtypes('zodiac rat', ['Rat']).
card_colors('zodiac rat', ['B']).
card_text('zodiac rat', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('zodiac rat', ['B']).
card_cmc('zodiac rat', 1).
card_layout('zodiac rat', 'normal').
card_power('zodiac rat', 1).
card_toughness('zodiac rat', 1).

% Found in: PTK
card_name('zodiac rooster', 'Zodiac Rooster').
card_type('zodiac rooster', 'Creature — Bird').
card_types('zodiac rooster', ['Creature']).
card_subtypes('zodiac rooster', ['Bird']).
card_colors('zodiac rooster', ['G']).
card_text('zodiac rooster', 'Plainswalk (This creature can\'t be blocked as long as defending player controls a Plains.)').
card_mana_cost('zodiac rooster', ['1', 'G']).
card_cmc('zodiac rooster', 2).
card_layout('zodiac rooster', 'normal').
card_power('zodiac rooster', 2).
card_toughness('zodiac rooster', 1).

% Found in: PTK
card_name('zodiac snake', 'Zodiac Snake').
card_type('zodiac snake', 'Creature — Snake').
card_types('zodiac snake', ['Creature']).
card_subtypes('zodiac snake', ['Snake']).
card_colors('zodiac snake', ['B']).
card_text('zodiac snake', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('zodiac snake', ['2', 'B']).
card_cmc('zodiac snake', 3).
card_layout('zodiac snake', 'normal').
card_power('zodiac snake', 2).
card_toughness('zodiac snake', 2).

% Found in: PTK
card_name('zodiac tiger', 'Zodiac Tiger').
card_type('zodiac tiger', 'Creature — Cat').
card_types('zodiac tiger', ['Creature']).
card_subtypes('zodiac tiger', ['Cat']).
card_colors('zodiac tiger', ['G']).
card_text('zodiac tiger', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('zodiac tiger', ['2', 'G', 'G']).
card_cmc('zodiac tiger', 4).
card_layout('zodiac tiger', 'normal').
card_power('zodiac tiger', 3).
card_toughness('zodiac tiger', 4).

% Found in: C14, CMD, FUT, pGTW
card_name('zoetic cavern', 'Zoetic Cavern').
card_type('zoetic cavern', 'Land').
card_types('zoetic cavern', ['Land']).
card_subtypes('zoetic cavern', []).
card_colors('zoetic cavern', []).
card_text('zoetic cavern', '{T}: Add {1} to your mana pool.\nMorph {2} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_layout('zoetic cavern', 'normal').

% Found in: M15, ROE
card_name('zof shade', 'Zof Shade').
card_type('zof shade', 'Creature — Shade').
card_types('zof shade', ['Creature']).
card_subtypes('zof shade', ['Shade']).
card_colors('zof shade', ['B']).
card_text('zof shade', '{2}{B}: Zof Shade gets +2/+2 until end of turn.').
card_mana_cost('zof shade', ['3', 'B']).
card_cmc('zof shade', 4).
card_layout('zof shade', 'normal').
card_power('zof shade', 2).
card_toughness('zof shade', 2).

% Found in: DKA, pMGD
card_name('zombie apocalypse', 'Zombie Apocalypse').
card_type('zombie apocalypse', 'Sorcery').
card_types('zombie apocalypse', ['Sorcery']).
card_subtypes('zombie apocalypse', []).
card_colors('zombie apocalypse', ['B']).
card_text('zombie apocalypse', 'Return all Zombie creature cards from your graveyard to the battlefield tapped, then destroy all Humans.').
card_mana_cost('zombie apocalypse', ['3', 'B', 'B', 'B']).
card_cmc('zombie apocalypse', 6).
card_layout('zombie apocalypse', 'normal').

% Found in: ODY
card_name('zombie assassin', 'Zombie Assassin').
card_type('zombie assassin', 'Creature — Zombie Assassin').
card_types('zombie assassin', ['Creature']).
card_subtypes('zombie assassin', ['Zombie', 'Assassin']).
card_colors('zombie assassin', ['B']).
card_text('zombie assassin', '{T}, Exile two cards from your graveyard and Zombie Assassin: Destroy target nonblack creature. It can\'t be regenerated.').
card_mana_cost('zombie assassin', ['4', 'B']).
card_cmc('zombie assassin', 5).
card_layout('zombie assassin', 'normal').
card_power('zombie assassin', 3).
card_toughness('zombie assassin', 2).

% Found in: APC
card_name('zombie boa', 'Zombie Boa').
card_type('zombie boa', 'Creature — Zombie Snake').
card_types('zombie boa', ['Creature']).
card_subtypes('zombie boa', ['Zombie', 'Snake']).
card_colors('zombie boa', ['B']).
card_text('zombie boa', '{1}{B}: Choose a color. Whenever Zombie Boa becomes blocked by a creature of that color this turn, destroy that creature. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('zombie boa', ['4', 'B']).
card_cmc('zombie boa', 5).
card_layout('zombie boa', 'normal').
card_power('zombie boa', 3).
card_toughness('zombie boa', 3).

% Found in: LGN
card_name('zombie brute', 'Zombie Brute').
card_type('zombie brute', 'Creature — Zombie').
card_types('zombie brute', ['Creature']).
card_subtypes('zombie brute', ['Zombie']).
card_colors('zombie brute', ['B']).
card_text('zombie brute', 'Amplify 1 (As this creature enters the battlefield, put a +1/+1 counter on it for each Zombie card you reveal in your hand.)\nTrample').
card_mana_cost('zombie brute', ['6', 'B']).
card_cmc('zombie brute', 7).
card_layout('zombie brute', 'normal').
card_power('zombie brute', 5).
card_toughness('zombie brute', 4).

% Found in: ODY
card_name('zombie cannibal', 'Zombie Cannibal').
card_type('zombie cannibal', 'Creature — Zombie').
card_types('zombie cannibal', ['Creature']).
card_subtypes('zombie cannibal', ['Zombie']).
card_colors('zombie cannibal', ['B']).
card_text('zombie cannibal', 'Whenever Zombie Cannibal deals combat damage to a player, you may exile target card from that player\'s graveyard.').
card_mana_cost('zombie cannibal', ['B']).
card_cmc('zombie cannibal', 1).
card_layout('zombie cannibal', 'normal').
card_power('zombie cannibal', 1).
card_toughness('zombie cannibal', 1).

% Found in: SCG
card_name('zombie cutthroat', 'Zombie Cutthroat').
card_type('zombie cutthroat', 'Creature — Zombie').
card_types('zombie cutthroat', ['Creature']).
card_subtypes('zombie cutthroat', ['Zombie']).
card_colors('zombie cutthroat', ['B']).
card_text('zombie cutthroat', 'Morph—Pay 5 life. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('zombie cutthroat', ['3', 'B', 'B']).
card_cmc('zombie cutthroat', 5).
card_layout('zombie cutthroat', 'normal').
card_power('zombie cutthroat', 3).
card_toughness('zombie cutthroat', 4).

% Found in: UNH
card_name('zombie fanboy', 'Zombie Fanboy').
card_type('zombie fanboy', 'Creature — Zombie Gamer').
card_types('zombie fanboy', ['Creature']).
card_subtypes('zombie fanboy', ['Zombie', 'Gamer']).
card_colors('zombie fanboy', ['B']).
card_text('zombie fanboy', 'As Zombie Fanboy comes into play, choose an artist.\nWhenever a permanent by the chosen artist is put into a graveyard, put two +1/+1 counters on Zombie Fanboy.').
card_mana_cost('zombie fanboy', ['2', 'B']).
card_cmc('zombie fanboy', 3).
card_layout('zombie fanboy', 'normal').
card_power('zombie fanboy', 1).
card_toughness('zombie fanboy', 1).

% Found in: CNS, M10, M12, M13
card_name('zombie goliath', 'Zombie Goliath').
card_type('zombie goliath', 'Creature — Zombie Giant').
card_types('zombie goliath', ['Creature']).
card_subtypes('zombie goliath', ['Zombie', 'Giant']).
card_colors('zombie goliath', ['B']).
card_text('zombie goliath', '').
card_mana_cost('zombie goliath', ['4', 'B']).
card_cmc('zombie goliath', 5).
card_layout('zombie goliath', 'normal').
card_power('zombie goliath', 4).
card_toughness('zombie goliath', 3).

% Found in: ARC, M12, ODY, PD3
card_name('zombie infestation', 'Zombie Infestation').
card_type('zombie infestation', 'Enchantment').
card_types('zombie infestation', ['Enchantment']).
card_subtypes('zombie infestation', []).
card_colors('zombie infestation', ['B']).
card_text('zombie infestation', 'Discard two cards: Put a 2/2 black Zombie creature token onto the battlefield.').
card_mana_cost('zombie infestation', ['1', 'B']).
card_cmc('zombie infestation', 2).
card_layout('zombie infestation', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, CED, CEI, LEA, LEB, ME4
card_name('zombie master', 'Zombie Master').
card_type('zombie master', 'Creature — Zombie').
card_types('zombie master', ['Creature']).
card_subtypes('zombie master', ['Zombie']).
card_colors('zombie master', ['B']).
card_text('zombie master', 'Other Zombie creatures have swampwalk. (They can\'t be blocked as long as defending player controls a Swamp.)\nOther Zombies have \"{B}: Regenerate this permanent.\"').
card_mana_cost('zombie master', ['1', 'B', 'B']).
card_cmc('zombie master', 3).
card_layout('zombie master', 'normal').
card_power('zombie master', 2).
card_toughness('zombie master', 3).

% Found in: MIR
card_name('zombie mob', 'Zombie Mob').
card_type('zombie mob', 'Creature — Zombie').
card_types('zombie mob', ['Creature']).
card_subtypes('zombie mob', ['Zombie']).
card_colors('zombie mob', ['B']).
card_text('zombie mob', 'Zombie Mob enters the battlefield with a +1/+1 counter on it for each creature card in your graveyard.\nWhen Zombie Mob enters the battlefield, exile all creature cards from your graveyard.').
card_mana_cost('zombie mob', ['2', 'B', 'B']).
card_cmc('zombie mob', 4).
card_layout('zombie mob', 'normal').
card_power('zombie mob', 2).
card_toughness('zombie mob', 0).

% Found in: CSP
card_name('zombie musher', 'Zombie Musher').
card_type('zombie musher', 'Snow Creature — Zombie').
card_types('zombie musher', ['Creature']).
card_subtypes('zombie musher', ['Zombie']).
card_supertypes('zombie musher', ['Snow']).
card_colors('zombie musher', ['B']).
card_text('zombie musher', 'Snow landwalk (This creature can\'t be blocked as long as defending player controls a snow land.)\n{S}: Regenerate Zombie Musher. ({S} can be paid with one mana from a snow permanent.)').
card_mana_cost('zombie musher', ['3', 'B']).
card_cmc('zombie musher', 4).
card_layout('zombie musher', 'normal').
card_power('zombie musher', 2).
card_toughness('zombie musher', 3).

% Found in: CON
card_name('zombie outlander', 'Zombie Outlander').
card_type('zombie outlander', 'Creature — Zombie Scout').
card_types('zombie outlander', ['Creature']).
card_subtypes('zombie outlander', ['Zombie', 'Scout']).
card_colors('zombie outlander', ['U', 'B']).
card_text('zombie outlander', 'Protection from green').
card_mana_cost('zombie outlander', ['U', 'B']).
card_cmc('zombie outlander', 2).
card_layout('zombie outlander', 'normal').
card_power('zombie outlander', 2).
card_toughness('zombie outlander', 2).

% Found in: WTH
card_name('zombie scavengers', 'Zombie Scavengers').
card_type('zombie scavengers', 'Creature — Zombie').
card_types('zombie scavengers', ['Creature']).
card_subtypes('zombie scavengers', ['Zombie']).
card_colors('zombie scavengers', ['B']).
card_text('zombie scavengers', 'Exile the top creature card of your graveyard: Regenerate Zombie Scavengers.').
card_mana_cost('zombie scavengers', ['2', 'B']).
card_cmc('zombie scavengers', 3).
card_layout('zombie scavengers', 'normal').
card_power('zombie scavengers', 3).
card_toughness('zombie scavengers', 1).

% Found in: UGL
card_name('zombie token card', 'Zombie token card').
card_type('zombie token card', '').
card_types('zombie token card', []).
card_subtypes('zombie token card', []).
card_colors('zombie token card', []).
card_text('zombie token card', '').
card_layout('zombie token card', 'token').

% Found in: TOR
card_name('zombie trailblazer', 'Zombie Trailblazer').
card_type('zombie trailblazer', 'Creature — Zombie Scout').
card_types('zombie trailblazer', ['Creature']).
card_subtypes('zombie trailblazer', ['Zombie', 'Scout']).
card_colors('zombie trailblazer', ['B']).
card_text('zombie trailblazer', 'Tap an untapped Zombie you control: Target land becomes a Swamp until end of turn.\nTap an untapped Zombie you control: Target creature gains swampwalk until end of turn. (It can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('zombie trailblazer', ['B', 'B', 'B']).
card_cmc('zombie trailblazer', 3).
card_layout('zombie trailblazer', 'normal').
card_power('zombie trailblazer', 2).
card_toughness('zombie trailblazer', 2).

% Found in: 8ED, 9ED, ARC, ODY, pMPR
card_name('zombify', 'Zombify').
card_type('zombify', 'Sorcery').
card_types('zombify', ['Sorcery']).
card_subtypes('zombify', []).
card_colors('zombify', ['B']).
card_text('zombify', 'Return target creature card from your graveyard to the battlefield.').
card_mana_cost('zombify', ['3', 'B']).
card_cmc('zombify', 4).
card_layout('zombify', 'normal').

% Found in: ODY
card_name('zoologist', 'Zoologist').
card_type('zoologist', 'Creature — Human Druid').
card_types('zoologist', ['Creature']).
card_subtypes('zoologist', ['Human', 'Druid']).
card_colors('zoologist', ['G']).
card_text('zoologist', '{3}{G}, {T}: Reveal the top card of your library. If it\'s a creature card, put it onto the battlefield. Otherwise, put it into your graveyard.').
card_mana_cost('zoologist', ['3', 'G']).
card_cmc('zoologist', 4).
card_layout('zoologist', 'normal').
card_power('zoologist', 1).
card_toughness('zoologist', 2).

% Found in: MIR
card_name('zuberi, golden feather', 'Zuberi, Golden Feather').
card_type('zuberi, golden feather', 'Legendary Creature — Griffin').
card_types('zuberi, golden feather', ['Creature']).
card_subtypes('zuberi, golden feather', ['Griffin']).
card_supertypes('zuberi, golden feather', ['Legendary']).
card_colors('zuberi, golden feather', ['W']).
card_text('zuberi, golden feather', 'Flying\nOther Griffin creatures get +1/+1.').
card_mana_cost('zuberi, golden feather', ['4', 'W']).
card_cmc('zuberi, golden feather', 5).
card_layout('zuberi, golden feather', 'normal').
card_power('zuberi, golden feather', 3).
card_toughness('zuberi, golden feather', 3).
card_reserved('zuberi, golden feather').

% Found in: BFZ
card_name('zulaport cutthroat', 'Zulaport Cutthroat').
card_type('zulaport cutthroat', 'Creature — Human Rogue Ally').
card_types('zulaport cutthroat', ['Creature']).
card_subtypes('zulaport cutthroat', ['Human', 'Rogue', 'Ally']).
card_colors('zulaport cutthroat', ['B']).
card_text('zulaport cutthroat', 'Whenever Zulaport Cutthroat or another creature you control dies, each opponent loses 1 life and you gain 1 life.').
card_mana_cost('zulaport cutthroat', ['1', 'B']).
card_cmc('zulaport cutthroat', 2).
card_layout('zulaport cutthroat', 'normal').
card_power('zulaport cutthroat', 1).
card_toughness('zulaport cutthroat', 1).

% Found in: ROE
card_name('zulaport enforcer', 'Zulaport Enforcer').
card_type('zulaport enforcer', 'Creature — Human Warrior').
card_types('zulaport enforcer', ['Creature']).
card_subtypes('zulaport enforcer', ['Human', 'Warrior']).
card_colors('zulaport enforcer', ['B']).
card_text('zulaport enforcer', 'Level up {4} ({4}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-2\n3/3\nLEVEL 3+\n5/5\nZulaport Enforcer can\'t be blocked except by black creatures.').
card_mana_cost('zulaport enforcer', ['B']).
card_cmc('zulaport enforcer', 1).
card_layout('zulaport enforcer', 'leveler').
card_power('zulaport enforcer', 1).
card_toughness('zulaport enforcer', 1).

% Found in: PTK
card_name('zuo ci, the mocking sage', 'Zuo Ci, the Mocking Sage').
card_type('zuo ci, the mocking sage', 'Legendary Creature — Human Advisor').
card_types('zuo ci, the mocking sage', ['Creature']).
card_subtypes('zuo ci, the mocking sage', ['Human', 'Advisor']).
card_supertypes('zuo ci, the mocking sage', ['Legendary']).
card_colors('zuo ci, the mocking sage', ['G']).
card_text('zuo ci, the mocking sage', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nZuo Ci, the Mocking Sage can\'t be blocked by creatures with horsemanship.').
card_mana_cost('zuo ci, the mocking sage', ['1', 'G', 'G']).
card_cmc('zuo ci, the mocking sage', 3).
card_layout('zuo ci, the mocking sage', 'normal').
card_power('zuo ci, the mocking sage', 1).
card_toughness('zuo ci, the mocking sage', 2).

% Found in: CSP
card_name('zur the enchanter', 'Zur the Enchanter').
card_type('zur the enchanter', 'Legendary Creature — Human Wizard').
card_types('zur the enchanter', ['Creature']).
card_subtypes('zur the enchanter', ['Human', 'Wizard']).
card_supertypes('zur the enchanter', ['Legendary']).
card_colors('zur the enchanter', ['W', 'U', 'B']).
card_text('zur the enchanter', 'Flying\nWhenever Zur the Enchanter attacks, you may search your library for an enchantment card with converted mana cost 3 or less and put it onto the battlefield. If you do, shuffle your library.').
card_mana_cost('zur the enchanter', ['1', 'W', 'U', 'B']).
card_cmc('zur the enchanter', 4).
card_layout('zur the enchanter', 'normal').
card_power('zur the enchanter', 1).
card_toughness('zur the enchanter', 4).

% Found in: 5ED, 6ED, 8ED, 9ED, ICE
card_name('zur\'s weirding', 'Zur\'s Weirding').
card_type('zur\'s weirding', 'Enchantment').
card_types('zur\'s weirding', ['Enchantment']).
card_subtypes('zur\'s weirding', []).
card_colors('zur\'s weirding', ['U']).
card_text('zur\'s weirding', 'Players play with their hands revealed.\nIf a player would draw a card, he or she reveals it instead. Then any other player may pay 2 life. If a player does, put that card into its owner\'s graveyard. Otherwise, that player draws a card.').
card_mana_cost('zur\'s weirding', ['3', 'U']).
card_cmc('zur\'s weirding', 4).
card_layout('zur\'s weirding', 'normal').

% Found in: ICE
card_name('zuran enchanter', 'Zuran Enchanter').
card_type('zuran enchanter', 'Creature — Human Wizard').
card_types('zuran enchanter', ['Creature']).
card_subtypes('zuran enchanter', ['Human', 'Wizard']).
card_colors('zuran enchanter', ['U']).
card_text('zuran enchanter', '{2}{B}, {T}: Target player discards a card. Activate this ability only during your turn.').
card_mana_cost('zuran enchanter', ['1', 'U']).
card_cmc('zuran enchanter', 2).
card_layout('zuran enchanter', 'normal').
card_power('zuran enchanter', 1).
card_toughness('zuran enchanter', 1).

% Found in: ICE, MED, V10
card_name('zuran orb', 'Zuran Orb').
card_type('zuran orb', 'Artifact').
card_types('zuran orb', ['Artifact']).
card_subtypes('zuran orb', []).
card_colors('zuran orb', []).
card_text('zuran orb', 'Sacrifice a land: You gain 2 life.').
card_mana_cost('zuran orb', ['0']).
card_cmc('zuran orb', 0).
card_layout('zuran orb', 'normal').

% Found in: CST, ICE, ME2
card_name('zuran spellcaster', 'Zuran Spellcaster').
card_type('zuran spellcaster', 'Creature — Human Wizard').
card_types('zuran spellcaster', ['Creature']).
card_subtypes('zuran spellcaster', ['Human', 'Wizard']).
card_colors('zuran spellcaster', ['U']).
card_text('zuran spellcaster', '{T}: Zuran Spellcaster deals 1 damage to target creature or player.').
card_mana_cost('zuran spellcaster', ['2', 'U']).
card_cmc('zuran spellcaster', 3).
card_layout('zuran spellcaster', 'normal').
card_power('zuran spellcaster', 1).
card_toughness('zuran spellcaster', 1).

% Found in: DTK
card_name('zurgo bellstriker', 'Zurgo Bellstriker').
card_type('zurgo bellstriker', 'Legendary Creature — Orc Warrior').
card_types('zurgo bellstriker', ['Creature']).
card_subtypes('zurgo bellstriker', ['Orc', 'Warrior']).
card_supertypes('zurgo bellstriker', ['Legendary']).
card_colors('zurgo bellstriker', ['R']).
card_text('zurgo bellstriker', 'Zurgo Bellstriker can\'t block creatures with power 2 or greater.\nDash {1}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('zurgo bellstriker', ['R']).
card_cmc('zurgo bellstriker', 1).
card_layout('zurgo bellstriker', 'normal').
card_power('zurgo bellstriker', 2).
card_toughness('zurgo bellstriker', 2).

% Found in: DDN, KTK, pPRE
card_name('zurgo helmsmasher', 'Zurgo Helmsmasher').
card_type('zurgo helmsmasher', 'Legendary Creature — Orc Warrior').
card_types('zurgo helmsmasher', ['Creature']).
card_subtypes('zurgo helmsmasher', ['Orc', 'Warrior']).
card_supertypes('zurgo helmsmasher', ['Legendary']).
card_colors('zurgo helmsmasher', ['W', 'B', 'R']).
card_text('zurgo helmsmasher', 'Haste\nZurgo Helmsmasher attacks each combat if able.\nZurgo Helmsmasher has indestructible as long as it\'s your turn.\nWhenever a creature dealt damage by Zurgo Helmsmasher this turn dies, put a +1/+1 counter on Zurgo Helmsmasher.').
card_mana_cost('zurgo helmsmasher', ['2', 'R', 'W', 'B']).
card_cmc('zurgo helmsmasher', 5).
card_layout('zurgo helmsmasher', 'normal').
card_power('zurgo helmsmasher', 7).
card_toughness('zurgo helmsmasher', 2).

% Found in: UNH
card_name('zzzyxas\'s abyss', 'Zzzyxas\'s Abyss').
card_type('zzzyxas\'s abyss', 'Enchantment').
card_types('zzzyxas\'s abyss', ['Enchantment']).
card_subtypes('zzzyxas\'s abyss', []).
card_colors('zzzyxas\'s abyss', ['B']).
card_text('zzzyxas\'s abyss', 'At the beginning of your upkeep, destroy all nonland permanents with the first name alphabetically among nonland permanents in play.').
card_mana_cost('zzzyxas\'s abyss', ['1', 'B', 'B']).
card_cmc('zzzyxas\'s abyss', 3).
card_layout('zzzyxas\'s abyss', 'normal').

