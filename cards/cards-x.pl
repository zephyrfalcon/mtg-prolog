% Card-specific data

% Found in: VAN
card_name('xantcha', 'Xantcha').
card_type('xantcha', 'Vanguard').
card_types('xantcha', ['Vanguard']).
card_subtypes('xantcha', []).
card_colors('xantcha', []).
card_text('xantcha', 'Sacrifice a permanent: Regenerate target creature.').
card_layout('xantcha', 'vanguard').

% Found in: WTH
card_name('xanthic statue', 'Xanthic Statue').
card_type('xanthic statue', 'Artifact').
card_types('xanthic statue', ['Artifact']).
card_subtypes('xanthic statue', []).
card_colors('xanthic statue', []).
card_text('xanthic statue', '{5}: Until end of turn, Xanthic Statue becomes an 8/8 Golem artifact creature with trample.').
card_mana_cost('xanthic statue', ['8']).
card_cmc('xanthic statue', 8).
card_layout('xanthic statue', 'normal').
card_reserved('xanthic statue').

% Found in: SCG
card_name('xantid swarm', 'Xantid Swarm').
card_type('xantid swarm', 'Creature — Insect').
card_types('xantid swarm', ['Creature']).
card_subtypes('xantid swarm', ['Insect']).
card_colors('xantid swarm', ['G']).
card_text('xantid swarm', 'Flying\nWhenever Xantid Swarm attacks, defending player can\'t cast spells this turn.').
card_mana_cost('xantid swarm', ['G']).
card_cmc('xantid swarm', 1).
card_layout('xantid swarm', 'normal').
card_power('xantid swarm', 0).
card_toughness('xantid swarm', 1).

% Found in: C14, M10
card_name('xathrid demon', 'Xathrid Demon').
card_type('xathrid demon', 'Creature — Demon').
card_types('xathrid demon', ['Creature']).
card_subtypes('xathrid demon', ['Demon']).
card_colors('xathrid demon', ['B']).
card_text('xathrid demon', 'Flying, trample\nAt the beginning of your upkeep, sacrifice a creature other than Xathrid Demon, then each opponent loses life equal to the sacrificed creature\'s power. If you can\'t sacrifice a creature, tap Xathrid Demon and you lose 7 life.').
card_mana_cost('xathrid demon', ['3', 'B', 'B', 'B']).
card_cmc('xathrid demon', 6).
card_layout('xathrid demon', 'normal').
card_power('xathrid demon', 7).
card_toughness('xathrid demon', 7).

% Found in: M13, pPRE
card_name('xathrid gorgon', 'Xathrid Gorgon').
card_type('xathrid gorgon', 'Creature — Gorgon').
card_types('xathrid gorgon', ['Creature']).
card_subtypes('xathrid gorgon', ['Gorgon']).
card_colors('xathrid gorgon', ['B']).
card_text('xathrid gorgon', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\n{2}{B}, {T}: Put a petrification counter on target creature. It gains defender and becomes a colorless artifact in addition to its other types. Its activated abilities can\'t be activated. (A creature with defender can\'t attack.)').
card_mana_cost('xathrid gorgon', ['5', 'B']).
card_cmc('xathrid gorgon', 6).
card_layout('xathrid gorgon', 'normal').
card_power('xathrid gorgon', 3).
card_toughness('xathrid gorgon', 6).

% Found in: M14, pMEI
card_name('xathrid necromancer', 'Xathrid Necromancer').
card_type('xathrid necromancer', 'Creature — Human Wizard').
card_types('xathrid necromancer', ['Creature']).
card_subtypes('xathrid necromancer', ['Human', 'Wizard']).
card_colors('xathrid necromancer', ['B']).
card_text('xathrid necromancer', 'Whenever Xathrid Necromancer or another Human creature you control dies, put a 2/2 black Zombie creature token onto the battlefield tapped.').
card_mana_cost('xathrid necromancer', ['2', 'B']).
card_cmc('xathrid necromancer', 3).
card_layout('xathrid necromancer', 'normal').
card_power('xathrid necromancer', 2).
card_toughness('xathrid necromancer', 2).

% Found in: M15
card_name('xathrid slyblade', 'Xathrid Slyblade').
card_type('xathrid slyblade', 'Creature — Human Assassin').
card_types('xathrid slyblade', ['Creature']).
card_subtypes('xathrid slyblade', ['Human', 'Assassin']).
card_colors('xathrid slyblade', ['B']).
card_text('xathrid slyblade', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\n{3}{B}: Until end of turn, Xathrid Slyblade loses hexproof and gains first strike and deathtouch. (It deals combat damage before creatures without first strike. Any amount of damage it deals to a creature is enough to destroy it.)').
card_mana_cost('xathrid slyblade', ['2', 'B']).
card_cmc('xathrid slyblade', 3).
card_layout('xathrid slyblade', 'normal').
card_power('xathrid slyblade', 2).
card_toughness('xathrid slyblade', 1).

% Found in: BNG
card_name('xenagos, god of revels', 'Xenagos, God of Revels').
card_type('xenagos, god of revels', 'Legendary Enchantment Creature — God').
card_types('xenagos, god of revels', ['Enchantment', 'Creature']).
card_subtypes('xenagos, god of revels', ['God']).
card_supertypes('xenagos, god of revels', ['Legendary']).
card_colors('xenagos, god of revels', ['R', 'G']).
card_text('xenagos, god of revels', 'Indestructible\nAs long as your devotion to red and green is less than seven, Xenagos isn\'t a creature.\nAt the beginning of combat on your turn, another target creature you control gains haste and gets +X/+X until end of turn, where X is that creature\'s power.').
card_mana_cost('xenagos, god of revels', ['3', 'R', 'G']).
card_cmc('xenagos, god of revels', 5).
card_layout('xenagos, god of revels', 'normal').
card_power('xenagos, god of revels', 6).
card_toughness('xenagos, god of revels', 5).

% Found in: THS
card_name('xenagos, the reveler', 'Xenagos, the Reveler').
card_type('xenagos, the reveler', 'Planeswalker — Xenagos').
card_types('xenagos, the reveler', ['Planeswalker']).
card_subtypes('xenagos, the reveler', ['Xenagos']).
card_colors('xenagos, the reveler', ['R', 'G']).
card_text('xenagos, the reveler', '+1: Add X mana in any combination of {R} and/or {G} to your mana pool, where X is the number of creatures you control.\n0: Put a 2/2 red and green Satyr creature token with haste onto the battlefield.\n−6: Exile the top seven cards of your library. You may put any number of creature and/or land cards from among them onto the battlefield.').
card_mana_cost('xenagos, the reveler', ['2', 'R', 'G']).
card_cmc('xenagos, the reveler', 4).
card_layout('xenagos, the reveler', 'normal').
card_loyalty('xenagos, the reveler', 3).

% Found in: 4ED, 5ED, ATQ, ME4
card_name('xenic poltergeist', 'Xenic Poltergeist').
card_type('xenic poltergeist', 'Creature — Spirit').
card_types('xenic poltergeist', ['Creature']).
card_subtypes('xenic poltergeist', ['Spirit']).
card_colors('xenic poltergeist', ['B']).
card_text('xenic poltergeist', '{T}: Until your next upkeep, target noncreature artifact becomes an artifact creature with power and toughness each equal to its converted mana cost.').
card_mana_cost('xenic poltergeist', ['1', 'B', 'B']).
card_cmc('xenic poltergeist', 3).
card_layout('xenic poltergeist', 'normal').
card_power('xenic poltergeist', 1).
card_toughness('xenic poltergeist', 1).

% Found in: NPH
card_name('xenograft', 'Xenograft').
card_type('xenograft', 'Enchantment').
card_types('xenograft', ['Enchantment']).
card_subtypes('xenograft', []).
card_colors('xenograft', ['U']).
card_text('xenograft', 'As Xenograft enters the battlefield, choose a creature type.\nEach creature you control is the chosen type in addition to its other types.').
card_mana_cost('xenograft', ['4', 'U']).
card_cmc('xenograft', 5).
card_layout('xenograft', 'normal').

% Found in: ME3, PTK, pJGP
card_name('xiahou dun, the one-eyed', 'Xiahou Dun, the One-Eyed').
card_type('xiahou dun, the one-eyed', 'Legendary Creature — Human Soldier').
card_types('xiahou dun, the one-eyed', ['Creature']).
card_subtypes('xiahou dun, the one-eyed', ['Human', 'Soldier']).
card_supertypes('xiahou dun, the one-eyed', ['Legendary']).
card_colors('xiahou dun, the one-eyed', ['B']).
card_text('xiahou dun, the one-eyed', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)\nSacrifice Xiahou Dun, the One-Eyed: Return target black card from your graveyard to your hand. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('xiahou dun, the one-eyed', ['2', 'B', 'B']).
card_cmc('xiahou dun, the one-eyed', 4).
card_layout('xiahou dun, the one-eyed', 'normal').
card_power('xiahou dun, the one-eyed', 3).
card_toughness('xiahou dun, the one-eyed', 2).

% Found in: CHR, LEG, ME3
card_name('xira arien', 'Xira Arien').
card_type('xira arien', 'Legendary Creature — Insect Wizard').
card_types('xira arien', ['Creature']).
card_subtypes('xira arien', ['Insect', 'Wizard']).
card_supertypes('xira arien', ['Legendary']).
card_colors('xira arien', ['B', 'R', 'G']).
card_text('xira arien', 'Flying\n{B}{R}{G}, {T}: Target player draws a card.').
card_mana_cost('xira arien', ['B', 'R', 'G']).
card_cmc('xira arien', 3).
card_layout('xira arien', 'normal').
card_power('xira arien', 1).
card_toughness('xira arien', 2).

% Found in: PTK
card_name('xun yu, wei advisor', 'Xun Yu, Wei Advisor').
card_type('xun yu, wei advisor', 'Legendary Creature — Human Advisor').
card_types('xun yu, wei advisor', ['Creature']).
card_subtypes('xun yu, wei advisor', ['Human', 'Advisor']).
card_supertypes('xun yu, wei advisor', ['Legendary']).
card_colors('xun yu, wei advisor', ['B']).
card_text('xun yu, wei advisor', '{T}: Target creature you control gets +2/+0 until end of turn. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('xun yu, wei advisor', ['1', 'B', 'B']).
card_cmc('xun yu, wei advisor', 3).
card_layout('xun yu, wei advisor', 'normal').
card_power('xun yu, wei advisor', 1).
card_toughness('xun yu, wei advisor', 1).

