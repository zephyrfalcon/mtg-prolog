% Card-specific data

% Found in: CHK
card_name('yamabushi\'s flame', 'Yamabushi\'s Flame').
card_type('yamabushi\'s flame', 'Instant').
card_types('yamabushi\'s flame', ['Instant']).
card_subtypes('yamabushi\'s flame', []).
card_colors('yamabushi\'s flame', ['R']).
card_text('yamabushi\'s flame', 'Yamabushi\'s Flame deals 3 damage to target creature or player. If a creature dealt damage this way would die this turn, exile it instead.').
card_mana_cost('yamabushi\'s flame', ['2', 'R']).
card_cmc('yamabushi\'s flame', 3).
card_layout('yamabushi\'s flame', 'normal').

% Found in: CHK
card_name('yamabushi\'s storm', 'Yamabushi\'s Storm').
card_type('yamabushi\'s storm', 'Sorcery').
card_types('yamabushi\'s storm', ['Sorcery']).
card_subtypes('yamabushi\'s storm', []).
card_colors('yamabushi\'s storm', ['R']).
card_text('yamabushi\'s storm', 'Yamabushi\'s Storm deals 1 damage to each creature. If a creature dealt damage this way would die this turn, exile it instead.').
card_mana_cost('yamabushi\'s storm', ['1', 'R']).
card_cmc('yamabushi\'s storm', 2).
card_layout('yamabushi\'s storm', 'normal').

% Found in: MIR
card_name('yare', 'Yare').
card_type('yare', 'Instant').
card_types('yare', ['Instant']).
card_subtypes('yare', []).
card_colors('yare', ['W']).
card_text('yare', 'Target creature defending player controls gets +3/+0 until end of turn. That creature can block up to two additional creatures this turn.').
card_mana_cost('yare', ['2', 'W']).
card_cmc('yare', 3).
card_layout('yare', 'normal').
card_reserved('yare').

% Found in: FRF
card_name('yasova dragonclaw', 'Yasova Dragonclaw').
card_type('yasova dragonclaw', 'Legendary Creature — Human Warrior').
card_types('yasova dragonclaw', ['Creature']).
card_subtypes('yasova dragonclaw', ['Human', 'Warrior']).
card_supertypes('yasova dragonclaw', ['Legendary']).
card_colors('yasova dragonclaw', ['G']).
card_text('yasova dragonclaw', 'Trample\nAt the beginning of combat on your turn, you may pay {1}{U/R}{U/R}. If you do, gain control of target creature an opponent controls with power less than Yasova Dragonclaw\'s power until end of turn, untap that creature, and it gains haste until end of turn.').
card_mana_cost('yasova dragonclaw', ['2', 'G']).
card_cmc('yasova dragonclaw', 3).
card_layout('yasova dragonclaw', 'normal').
card_power('yasova dragonclaw', 4).
card_toughness('yasova dragonclaw', 2).

% Found in: ALL, DKM, ME2
card_name('yavimaya ancients', 'Yavimaya Ancients').
card_type('yavimaya ancients', 'Creature — Treefolk').
card_types('yavimaya ancients', ['Creature']).
card_subtypes('yavimaya ancients', ['Treefolk']).
card_colors('yavimaya ancients', ['G']).
card_text('yavimaya ancients', '{G}: Yavimaya Ancients gets +1/-2 until end of turn.').
card_mana_cost('yavimaya ancients', ['3', 'G', 'G']).
card_cmc('yavimaya ancients', 5).
card_layout('yavimaya ancients', 'normal').
card_power('yavimaya ancients', 2).
card_toughness('yavimaya ancients', 7).

% Found in: ALL, DKM, MED
card_name('yavimaya ants', 'Yavimaya Ants').
card_type('yavimaya ants', 'Creature — Insect').
card_types('yavimaya ants', ['Creature']).
card_subtypes('yavimaya ants', ['Insect']).
card_colors('yavimaya ants', ['G']).
card_text('yavimaya ants', 'Trample, haste\nCumulative upkeep {G}{G} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('yavimaya ants', ['2', 'G', 'G']).
card_cmc('yavimaya ants', 4).
card_layout('yavimaya ants', 'normal').
card_power('yavimaya ants', 5).
card_toughness('yavimaya ants', 1).

% Found in: INV
card_name('yavimaya barbarian', 'Yavimaya Barbarian').
card_type('yavimaya barbarian', 'Creature — Elf Barbarian').
card_types('yavimaya barbarian', ['Creature']).
card_subtypes('yavimaya barbarian', ['Elf', 'Barbarian']).
card_colors('yavimaya barbarian', ['R', 'G']).
card_text('yavimaya barbarian', 'Protection from blue').
card_mana_cost('yavimaya barbarian', ['R', 'G']).
card_cmc('yavimaya barbarian', 2).
card_layout('yavimaya barbarian', 'normal').
card_power('yavimaya barbarian', 2).
card_toughness('yavimaya barbarian', 2).

% Found in: 10E, 9ED, APC, M15, ORI
card_name('yavimaya coast', 'Yavimaya Coast').
card_type('yavimaya coast', 'Land').
card_types('yavimaya coast', ['Land']).
card_subtypes('yavimaya coast', []).
card_colors('yavimaya coast', []).
card_text('yavimaya coast', '{T}: Add {1} to your mana pool.\n{T}: Add {G} or {U} to your mana pool. Yavimaya Coast deals 1 damage to you.').
card_layout('yavimaya coast', 'normal').

% Found in: ARC, TSP
card_name('yavimaya dryad', 'Yavimaya Dryad').
card_type('yavimaya dryad', 'Creature — Dryad').
card_types('yavimaya dryad', ['Creature']).
card_subtypes('yavimaya dryad', ['Dryad']).
card_colors('yavimaya dryad', ['G']).
card_text('yavimaya dryad', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)\nWhen Yavimaya Dryad enters the battlefield, you may search your library for a Forest card and put it onto the battlefield tapped under target player\'s control. If you do, shuffle your library.').
card_mana_cost('yavimaya dryad', ['1', 'G', 'G']).
card_cmc('yavimaya dryad', 3).
card_layout('yavimaya dryad', 'normal').
card_power('yavimaya dryad', 2).
card_toughness('yavimaya dryad', 1).

% Found in: CMD, DDE, UDS, VMA
card_name('yavimaya elder', 'Yavimaya Elder').
card_type('yavimaya elder', 'Creature — Human Druid').
card_types('yavimaya elder', ['Creature']).
card_subtypes('yavimaya elder', ['Human', 'Druid']).
card_colors('yavimaya elder', ['G']).
card_text('yavimaya elder', 'When Yavimaya Elder dies, you may search your library for up to two basic land cards, reveal them, and put them into your hand. If you do, shuffle your library.\n{2}, Sacrifice Yavimaya Elder: Draw a card.').
card_mana_cost('yavimaya elder', ['1', 'G', 'G']).
card_cmc('yavimaya elder', 3).
card_layout('yavimaya elder', 'normal').
card_power('yavimaya elder', 2).
card_toughness('yavimaya elder', 1).

% Found in: 10E, 7ED, 8ED, 9ED, UDS
card_name('yavimaya enchantress', 'Yavimaya Enchantress').
card_type('yavimaya enchantress', 'Creature — Human Druid').
card_types('yavimaya enchantress', ['Creature']).
card_subtypes('yavimaya enchantress', ['Human', 'Druid']).
card_colors('yavimaya enchantress', ['G']).
card_text('yavimaya enchantress', 'Yavimaya Enchantress gets +1/+1 for each enchantment on the battlefield.').
card_mana_cost('yavimaya enchantress', ['2', 'G']).
card_cmc('yavimaya enchantress', 3).
card_layout('yavimaya enchantress', 'normal').
card_power('yavimaya enchantress', 2).
card_toughness('yavimaya enchantress', 2).

% Found in: ICE
card_name('yavimaya gnats', 'Yavimaya Gnats').
card_type('yavimaya gnats', 'Creature — Insect').
card_types('yavimaya gnats', ['Creature']).
card_subtypes('yavimaya gnats', ['Insect']).
card_colors('yavimaya gnats', ['G']).
card_text('yavimaya gnats', 'Flying\n{G}: Regenerate Yavimaya Gnats.').
card_mana_cost('yavimaya gnats', ['2', 'G']).
card_cmc('yavimaya gnats', 3).
card_layout('yavimaya gnats', 'normal').
card_power('yavimaya gnats', 0).
card_toughness('yavimaya gnats', 1).

% Found in: ULG
card_name('yavimaya granger', 'Yavimaya Granger').
card_type('yavimaya granger', 'Creature — Elf').
card_types('yavimaya granger', ['Creature']).
card_subtypes('yavimaya granger', ['Elf']).
card_colors('yavimaya granger', ['G']).
card_text('yavimaya granger', 'Echo {2}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Yavimaya Granger enters the battlefield, you may search your library for a basic land card, put that card onto the battlefield tapped, then shuffle your library.').
card_mana_cost('yavimaya granger', ['2', 'G']).
card_cmc('yavimaya granger', 3).
card_layout('yavimaya granger', 'normal').
card_power('yavimaya granger', 2).
card_toughness('yavimaya granger', 2).

% Found in: UDS, VMA
card_name('yavimaya hollow', 'Yavimaya Hollow').
card_type('yavimaya hollow', 'Legendary Land').
card_types('yavimaya hollow', ['Land']).
card_subtypes('yavimaya hollow', []).
card_supertypes('yavimaya hollow', ['Legendary']).
card_colors('yavimaya hollow', []).
card_text('yavimaya hollow', '{T}: Add {1} to your mana pool.\n{G}, {T}: Regenerate target creature.').
card_layout('yavimaya hollow', 'normal').
card_reserved('yavimaya hollow').

% Found in: INV
card_name('yavimaya kavu', 'Yavimaya Kavu').
card_type('yavimaya kavu', 'Creature — Kavu').
card_types('yavimaya kavu', ['Creature']).
card_subtypes('yavimaya kavu', ['Kavu']).
card_colors('yavimaya kavu', ['R', 'G']).
card_text('yavimaya kavu', 'Yavimaya Kavu\'s power is equal to the number of red creatures on the battlefield.\nYavimaya Kavu\'s toughness is equal to the number of green creatures on the battlefield.').
card_mana_cost('yavimaya kavu', ['2', 'R', 'G']).
card_cmc('yavimaya kavu', 4).
card_layout('yavimaya kavu', 'normal').
card_power('yavimaya kavu', '*').
card_toughness('yavimaya kavu', '*').

% Found in: ULG
card_name('yavimaya scion', 'Yavimaya Scion').
card_type('yavimaya scion', 'Creature — Treefolk').
card_types('yavimaya scion', ['Creature']).
card_subtypes('yavimaya scion', ['Treefolk']).
card_colors('yavimaya scion', ['G']).
card_text('yavimaya scion', 'Protection from artifacts').
card_mana_cost('yavimaya scion', ['4', 'G']).
card_cmc('yavimaya scion', 5).
card_layout('yavimaya scion', 'normal').
card_power('yavimaya scion', 4).
card_toughness('yavimaya scion', 4).

% Found in: BTD, M11, ULG
card_name('yavimaya wurm', 'Yavimaya Wurm').
card_type('yavimaya wurm', 'Creature — Wurm').
card_types('yavimaya wurm', ['Creature']).
card_subtypes('yavimaya wurm', ['Wurm']).
card_colors('yavimaya wurm', ['G']).
card_text('yavimaya wurm', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('yavimaya wurm', ['4', 'G', 'G']).
card_cmc('yavimaya wurm', 6).
card_layout('yavimaya wurm', 'normal').
card_power('yavimaya wurm', 6).
card_toughness('yavimaya wurm', 4).

% Found in: APC
card_name('yavimaya\'s embrace', 'Yavimaya\'s Embrace').
card_type('yavimaya\'s embrace', 'Enchantment — Aura').
card_types('yavimaya\'s embrace', ['Enchantment']).
card_subtypes('yavimaya\'s embrace', ['Aura']).
card_colors('yavimaya\'s embrace', ['U', 'G']).
card_text('yavimaya\'s embrace', 'Enchant creature\nYou control enchanted creature.\nEnchanted creature gets +2/+2 and has trample.').
card_mana_cost('yavimaya\'s embrace', ['5', 'G', 'U', 'U']).
card_cmc('yavimaya\'s embrace', 8).
card_layout('yavimaya\'s embrace', 'normal').

% Found in: 9ED, ATQ, CHR
card_name('yawgmoth demon', 'Yawgmoth Demon').
card_type('yawgmoth demon', 'Creature — Demon').
card_types('yawgmoth demon', ['Creature']).
card_subtypes('yawgmoth demon', ['Demon']).
card_colors('yawgmoth demon', ['B']).
card_text('yawgmoth demon', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nFirst strike (This creature deals combat damage before creatures without first strike.)\nAt the beginning of your upkeep, you may sacrifice an artifact. If you don\'t, tap Yawgmoth Demon and it deals 2 damage to you.').
card_mana_cost('yawgmoth demon', ['4', 'B', 'B']).
card_cmc('yawgmoth demon', 6).
card_layout('yawgmoth demon', 'normal').
card_power('yawgmoth demon', 6).
card_toughness('yawgmoth demon', 6).

% Found in: INV
card_name('yawgmoth\'s agenda', 'Yawgmoth\'s Agenda').
card_type('yawgmoth\'s agenda', 'Enchantment').
card_types('yawgmoth\'s agenda', ['Enchantment']).
card_subtypes('yawgmoth\'s agenda', []).
card_colors('yawgmoth\'s agenda', ['B']).
card_text('yawgmoth\'s agenda', 'You can\'t cast more than one spell each turn.\nYou may play cards from your graveyard.\nIf a card would be put into your graveyard from anywhere, exile it instead.').
card_mana_cost('yawgmoth\'s agenda', ['3', 'B', 'B']).
card_cmc('yawgmoth\'s agenda', 5).
card_layout('yawgmoth\'s agenda', 'normal').

% Found in: UDS, VMA
card_name('yawgmoth\'s bargain', 'Yawgmoth\'s Bargain').
card_type('yawgmoth\'s bargain', 'Enchantment').
card_types('yawgmoth\'s bargain', ['Enchantment']).
card_subtypes('yawgmoth\'s bargain', []).
card_colors('yawgmoth\'s bargain', ['B']).
card_text('yawgmoth\'s bargain', 'Skip your draw step.\nPay 1 life: Draw a card.').
card_mana_cost('yawgmoth\'s bargain', ['4', 'B', 'B']).
card_cmc('yawgmoth\'s bargain', 6).
card_layout('yawgmoth\'s bargain', 'normal').
card_reserved('yawgmoth\'s bargain').

% Found in: 7ED, USG
card_name('yawgmoth\'s edict', 'Yawgmoth\'s Edict').
card_type('yawgmoth\'s edict', 'Enchantment').
card_types('yawgmoth\'s edict', ['Enchantment']).
card_subtypes('yawgmoth\'s edict', []).
card_colors('yawgmoth\'s edict', ['B']).
card_text('yawgmoth\'s edict', 'Whenever an opponent casts a white spell, that player loses 1 life and you gain 1 life.').
card_mana_cost('yawgmoth\'s edict', ['1', 'B']).
card_cmc('yawgmoth\'s edict', 2).
card_layout('yawgmoth\'s edict', 'normal').

% Found in: USG, VMA, pJGP
card_name('yawgmoth\'s will', 'Yawgmoth\'s Will').
card_type('yawgmoth\'s will', 'Sorcery').
card_types('yawgmoth\'s will', ['Sorcery']).
card_subtypes('yawgmoth\'s will', []).
card_colors('yawgmoth\'s will', ['B']).
card_text('yawgmoth\'s will', 'Until end of turn, you may play cards from your graveyard.\nIf a card would be put into your graveyard from anywhere this turn, exile that card instead.').
card_mana_cost('yawgmoth\'s will', ['2', 'B']).
card_cmc('yawgmoth\'s will', 3).
card_layout('yawgmoth\'s will', 'normal').
card_reserved('yawgmoth\'s will').

% Found in: M10
card_name('yawning fissure', 'Yawning Fissure').
card_type('yawning fissure', 'Sorcery').
card_types('yawning fissure', ['Sorcery']).
card_subtypes('yawning fissure', []).
card_colors('yawning fissure', ['R']).
card_text('yawning fissure', 'Each opponent sacrifices a land.').
card_mana_cost('yawning fissure', ['4', 'R']).
card_cmc('yawning fissure', 5).
card_layout('yawning fissure', 'normal').

% Found in: ARN, MED
card_name('ydwen efreet', 'Ydwen Efreet').
card_type('ydwen efreet', 'Creature — Efreet').
card_types('ydwen efreet', ['Creature']).
card_subtypes('ydwen efreet', ['Efreet']).
card_colors('ydwen efreet', ['R']).
card_text('ydwen efreet', 'Whenever Ydwen Efreet blocks, flip a coin. If you lose the flip, remove Ydwen Efreet from combat and it can\'t block this turn. Creatures it was blocking that had become blocked by only Ydwen Efreet this combat become unblocked.').
card_mana_cost('ydwen efreet', ['R', 'R', 'R']).
card_cmc('ydwen efreet', 3).
card_layout('ydwen efreet', 'normal').
card_power('ydwen efreet', 3).
card_toughness('ydwen efreet', 6).

% Found in: PTK
card_name('yellow scarves cavalry', 'Yellow Scarves Cavalry').
card_type('yellow scarves cavalry', 'Creature — Human Soldier').
card_types('yellow scarves cavalry', ['Creature']).
card_subtypes('yellow scarves cavalry', ['Human', 'Soldier']).
card_colors('yellow scarves cavalry', ['R']).
card_text('yellow scarves cavalry', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)\nYellow Scarves Cavalry can\'t block.').
card_mana_cost('yellow scarves cavalry', ['1', 'R']).
card_cmc('yellow scarves cavalry', 2).
card_layout('yellow scarves cavalry', 'normal').
card_power('yellow scarves cavalry', 1).
card_toughness('yellow scarves cavalry', 1).

% Found in: PTK
card_name('yellow scarves general', 'Yellow Scarves General').
card_type('yellow scarves general', 'Creature — Human Soldier').
card_types('yellow scarves general', ['Creature']).
card_subtypes('yellow scarves general', ['Human', 'Soldier']).
card_colors('yellow scarves general', ['R']).
card_text('yellow scarves general', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)\nYellow Scarves General can\'t block.').
card_mana_cost('yellow scarves general', ['3', 'R']).
card_cmc('yellow scarves general', 4).
card_layout('yellow scarves general', 'normal').
card_power('yellow scarves general', 2).
card_toughness('yellow scarves general', 2).

% Found in: PTK
card_name('yellow scarves troops', 'Yellow Scarves Troops').
card_type('yellow scarves troops', 'Creature — Human Soldier').
card_types('yellow scarves troops', ['Creature']).
card_subtypes('yellow scarves troops', ['Human', 'Soldier']).
card_colors('yellow scarves troops', ['R']).
card_text('yellow scarves troops', 'Yellow Scarves Troops can\'t block.').
card_mana_cost('yellow scarves troops', ['1', 'R']).
card_cmc('yellow scarves troops', 2).
card_layout('yellow scarves troops', 'normal').
card_power('yellow scarves troops', 2).
card_toughness('yellow scarves troops', 2).

% Found in: UNH
card_name('yet another æther vortex', 'Yet Another Æther Vortex').
card_type('yet another æther vortex', 'Enchantment').
card_types('yet another æther vortex', ['Enchantment']).
card_subtypes('yet another æther vortex', []).
card_colors('yet another æther vortex', ['R']).
card_text('yet another æther vortex', 'All creatures have haste.\nPlayers play with the top card of their libraries revealed.\nNoninstant, nonsorcery cards on top of a library are in play under their owner\'s control in addition to being in that library.').
card_mana_cost('yet another æther vortex', ['3', 'R', 'R']).
card_cmc('yet another æther vortex', 5).
card_layout('yet another æther vortex', 'normal').

% Found in: M13, ORI
card_name('yeva\'s forcemage', 'Yeva\'s Forcemage').
card_type('yeva\'s forcemage', 'Creature — Elf Shaman').
card_types('yeva\'s forcemage', ['Creature']).
card_subtypes('yeva\'s forcemage', ['Elf', 'Shaman']).
card_colors('yeva\'s forcemage', ['G']).
card_text('yeva\'s forcemage', 'When Yeva\'s Forcemage enters the battlefield, target creature gets +2/+2 until end of turn.').
card_mana_cost('yeva\'s forcemage', ['2', 'G']).
card_cmc('yeva\'s forcemage', 3).
card_layout('yeva\'s forcemage', 'normal').
card_power('yeva\'s forcemage', 2).
card_toughness('yeva\'s forcemage', 2).

% Found in: M13
card_name('yeva, nature\'s herald', 'Yeva, Nature\'s Herald').
card_type('yeva, nature\'s herald', 'Legendary Creature — Elf Shaman').
card_types('yeva, nature\'s herald', ['Creature']).
card_subtypes('yeva, nature\'s herald', ['Elf', 'Shaman']).
card_supertypes('yeva, nature\'s herald', ['Legendary']).
card_colors('yeva, nature\'s herald', ['G']).
card_text('yeva, nature\'s herald', 'Flash (You may cast this spell any time you could cast an instant.)\nYou may cast green creature cards as though they had flash.').
card_mana_cost('yeva, nature\'s herald', ['2', 'G', 'G']).
card_cmc('yeva, nature\'s herald', 4).
card_layout('yeva, nature\'s herald', 'normal').
card_power('yeva, nature\'s herald', 4).
card_toughness('yeva, nature\'s herald', 4).

% Found in: AVR
card_name('yew spirit', 'Yew Spirit').
card_type('yew spirit', 'Creature — Spirit Treefolk').
card_types('yew spirit', ['Creature']).
card_subtypes('yew spirit', ['Spirit', 'Treefolk']).
card_colors('yew spirit', ['G']).
card_text('yew spirit', '{2}{G}{G}: Yew Spirit gets +X/+X until end of turn, where X is its power.').
card_mana_cost('yew spirit', ['4', 'G']).
card_cmc('yew spirit', 5).
card_layout('yew spirit', 'normal').
card_power('yew spirit', 3).
card_toughness('yew spirit', 3).

% Found in: M15
card_name('yisan, the wanderer bard', 'Yisan, the Wanderer Bard').
card_type('yisan, the wanderer bard', 'Legendary Creature — Human Rogue').
card_types('yisan, the wanderer bard', ['Creature']).
card_subtypes('yisan, the wanderer bard', ['Human', 'Rogue']).
card_supertypes('yisan, the wanderer bard', ['Legendary']).
card_colors('yisan, the wanderer bard', ['G']).
card_text('yisan, the wanderer bard', '{2}{G}, {T}, Put a verse counter on Yisan, the Wanderer Bard: Search your library for a creature card with converted mana cost equal to the number of verse counters on Yisan, put it onto the battlefield, then shuffle your library.').
card_mana_cost('yisan, the wanderer bard', ['2', 'G']).
card_cmc('yisan, the wanderer bard', 3).
card_layout('yisan, the wanderer bard', 'normal').
card_power('yisan, the wanderer bard', 2).
card_toughness('yisan, the wanderer bard', 3).

% Found in: FUT, pGTW
card_name('yixlid jailer', 'Yixlid Jailer').
card_type('yixlid jailer', 'Creature — Zombie Wizard').
card_types('yixlid jailer', ['Creature']).
card_subtypes('yixlid jailer', ['Zombie', 'Wizard']).
card_colors('yixlid jailer', ['B']).
card_text('yixlid jailer', 'Cards in graveyards lose all abilities.').
card_mana_cost('yixlid jailer', ['1', 'B']).
card_cmc('yixlid jailer', 2).
card_layout('yixlid jailer', 'normal').
card_power('yixlid jailer', 2).
card_toughness('yixlid jailer', 1).

% Found in: CON, DDJ
card_name('yoke of the damned', 'Yoke of the Damned').
card_type('yoke of the damned', 'Enchantment — Aura').
card_types('yoke of the damned', ['Enchantment']).
card_subtypes('yoke of the damned', ['Aura']).
card_colors('yoke of the damned', ['B']).
card_text('yoke of the damned', 'Enchant creature\nWhen a creature dies, destroy enchanted creature.').
card_mana_cost('yoke of the damned', ['1', 'B']).
card_cmc('yoke of the damned', 2).
card_layout('yoke of the damned', 'normal').

% Found in: ORI, THS
card_name('yoked ox', 'Yoked Ox').
card_type('yoked ox', 'Creature — Ox').
card_types('yoked ox', ['Creature']).
card_subtypes('yoked ox', ['Ox']).
card_colors('yoked ox', ['W']).
card_text('yoked ox', '').
card_mana_cost('yoked ox', ['W']).
card_cmc('yoked ox', 1).
card_layout('yoked ox', 'normal').
card_power('yoked ox', 0).
card_toughness('yoked ox', 4).

% Found in: ALA
card_name('yoked plowbeast', 'Yoked Plowbeast').
card_type('yoked plowbeast', 'Creature — Beast').
card_types('yoked plowbeast', ['Creature']).
card_subtypes('yoked plowbeast', ['Beast']).
card_colors('yoked plowbeast', ['W']).
card_text('yoked plowbeast', 'Cycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('yoked plowbeast', ['5', 'W', 'W']).
card_cmc('yoked plowbeast', 7).
card_layout('yoked plowbeast', 'normal').
card_power('yoked plowbeast', 5).
card_toughness('yoked plowbeast', 5).

% Found in: BOK
card_name('yomiji, who bars the way', 'Yomiji, Who Bars the Way').
card_type('yomiji, who bars the way', 'Legendary Creature — Spirit').
card_types('yomiji, who bars the way', ['Creature']).
card_subtypes('yomiji, who bars the way', ['Spirit']).
card_supertypes('yomiji, who bars the way', ['Legendary']).
card_colors('yomiji, who bars the way', ['W']).
card_text('yomiji, who bars the way', 'Whenever a legendary permanent other than Yomiji, Who Bars the Way is put into a graveyard from the battlefield, return that card to its owner\'s hand.').
card_mana_cost('yomiji, who bars the way', ['5', 'W', 'W']).
card_cmc('yomiji, who bars the way', 7).
card_layout('yomiji, who bars the way', 'normal').
card_power('yomiji, who bars the way', 4).
card_toughness('yomiji, who bars the way', 4).

% Found in: GPT
card_name('yore-tiller nephilim', 'Yore-Tiller Nephilim').
card_type('yore-tiller nephilim', 'Creature — Nephilim').
card_types('yore-tiller nephilim', ['Creature']).
card_subtypes('yore-tiller nephilim', ['Nephilim']).
card_colors('yore-tiller nephilim', ['W', 'U', 'B', 'R']).
card_text('yore-tiller nephilim', 'Whenever Yore-Tiller Nephilim attacks, return target creature card from your graveyard to the battlefield tapped and attacking.').
card_mana_cost('yore-tiller nephilim', ['W', 'U', 'B', 'R']).
card_cmc('yore-tiller nephilim', 4).
card_layout('yore-tiller nephilim', 'normal').
card_power('yore-tiller nephilim', 2).
card_toughness('yore-tiller nephilim', 2).

% Found in: CHK, MMA
card_name('yosei, the morning star', 'Yosei, the Morning Star').
card_type('yosei, the morning star', 'Legendary Creature — Dragon Spirit').
card_types('yosei, the morning star', ['Creature']).
card_subtypes('yosei, the morning star', ['Dragon', 'Spirit']).
card_supertypes('yosei, the morning star', ['Legendary']).
card_colors('yosei, the morning star', ['W']).
card_text('yosei, the morning star', 'Flying\nWhen Yosei, the Morning Star dies, target player skips his or her next untap step. Tap up to five target permanents that player controls.').
card_mana_cost('yosei, the morning star', ['4', 'W', 'W']).
card_cmc('yosei, the morning star', 6).
card_layout('yosei, the morning star', 'normal').
card_power('yosei, the morning star', 5).
card_toughness('yosei, the morning star', 5).

% Found in: 4ED, ATQ, ME4, MRD
card_name('yotian soldier', 'Yotian Soldier').
card_type('yotian soldier', 'Artifact Creature — Soldier').
card_types('yotian soldier', ['Artifact', 'Creature']).
card_subtypes('yotian soldier', ['Soldier']).
card_colors('yotian soldier', []).
card_text('yotian soldier', 'Vigilance').
card_mana_cost('yotian soldier', ['3']).
card_cmc('yotian soldier', 3).
card_layout('yotian soldier', 'normal').
card_power('yotian soldier', 1).
card_toughness('yotian soldier', 4).

% Found in: M14
card_name('young pyromancer', 'Young Pyromancer').
card_type('young pyromancer', 'Creature — Human Shaman').
card_types('young pyromancer', ['Creature']).
card_subtypes('young pyromancer', ['Human', 'Shaman']).
card_colors('young pyromancer', ['R']).
card_text('young pyromancer', 'Whenever you cast an instant or sorcery spell, put a 1/1 red Elemental creature token onto the battlefield.').
card_mana_cost('young pyromancer', ['1', 'R']).
card_cmc('young pyromancer', 2).
card_layout('young pyromancer', 'normal').
card_power('young pyromancer', 2).
card_toughness('young pyromancer', 1).

% Found in: ME3, PTK
card_name('young wei recruits', 'Young Wei Recruits').
card_type('young wei recruits', 'Creature — Human Soldier').
card_types('young wei recruits', ['Creature']).
card_subtypes('young wei recruits', ['Human', 'Soldier']).
card_colors('young wei recruits', ['B']).
card_text('young wei recruits', 'Young Wei Recruits can\'t block.').
card_mana_cost('young wei recruits', ['1', 'B']).
card_cmc('young wei recruits', 2).
card_layout('young wei recruits', 'normal').
card_power('young wei recruits', 2).
card_toughness('young wei recruits', 2).

% Found in: DKA
card_name('young wolf', 'Young Wolf').
card_type('young wolf', 'Creature — Wolf').
card_types('young wolf', ['Creature']).
card_subtypes('young wolf', ['Wolf']).
card_colors('young wolf', ['G']).
card_text('young wolf', 'Undying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('young wolf', ['G']).
card_cmc('young wolf', 1).
card_layout('young wolf', 'normal').
card_power('young wolf', 1).
card_toughness('young wolf', 1).

% Found in: ARC
card_name('your fate is thrice sealed', 'Your Fate Is Thrice Sealed').
card_type('your fate is thrice sealed', 'Scheme').
card_types('your fate is thrice sealed', ['Scheme']).
card_subtypes('your fate is thrice sealed', []).
card_colors('your fate is thrice sealed', []).
card_text('your fate is thrice sealed', 'When you set this scheme in motion, reveal the top three cards of your library. Put all land cards revealed this way onto the battlefield and the rest into your hand.').
card_layout('your fate is thrice sealed', 'scheme').

% Found in: ARC
card_name('your puny minds cannot fathom', 'Your Puny Minds Cannot Fathom').
card_type('your puny minds cannot fathom', 'Scheme').
card_types('your puny minds cannot fathom', ['Scheme']).
card_subtypes('your puny minds cannot fathom', []).
card_colors('your puny minds cannot fathom', []).
card_text('your puny minds cannot fathom', 'When you set this scheme in motion, draw four cards. You have no maximum hand size until your next turn.').
card_layout('your puny minds cannot fathom', 'scheme').

% Found in: ARC
card_name('your will is not your own', 'Your Will Is Not Your Own').
card_type('your will is not your own', 'Scheme').
card_types('your will is not your own', ['Scheme']).
card_subtypes('your will is not your own', []).
card_colors('your will is not your own', []).
card_text('your will is not your own', 'When you set this scheme in motion, gain control of target creature an opponent controls until end of turn. Untap that creature. It gets +3/+3 and gains trample and haste until end of turn.').
card_layout('your will is not your own', 'scheme').

% Found in: 10E, ATH, STH, TPR
card_name('youthful knight', 'Youthful Knight').
card_type('youthful knight', 'Creature — Human Knight').
card_types('youthful knight', ['Creature']).
card_subtypes('youthful knight', ['Human', 'Knight']).
card_colors('youthful knight', ['W']).
card_text('youthful knight', 'First strike (This creature deals combat damage before creatures without first strike.)').
card_mana_cost('youthful knight', ['1', 'W']).
card_cmc('youthful knight', 2).
card_layout('youthful knight', 'normal').
card_power('youthful knight', 2).
card_toughness('youthful knight', 1).

% Found in: DTK
card_name('youthful scholar', 'Youthful Scholar').
card_type('youthful scholar', 'Creature — Human Wizard').
card_types('youthful scholar', ['Creature']).
card_subtypes('youthful scholar', ['Human', 'Wizard']).
card_colors('youthful scholar', ['U']).
card_text('youthful scholar', 'When Youthful Scholar dies, draw two cards.').
card_mana_cost('youthful scholar', ['3', 'U']).
card_cmc('youthful scholar', 4).
card_layout('youthful scholar', 'normal').
card_power('youthful scholar', 2).
card_toughness('youthful scholar', 2).

% Found in: PTK
card_name('yuan shao\'s infantry', 'Yuan Shao\'s Infantry').
card_type('yuan shao\'s infantry', 'Creature — Human Soldier').
card_types('yuan shao\'s infantry', ['Creature']).
card_subtypes('yuan shao\'s infantry', ['Human', 'Soldier']).
card_colors('yuan shao\'s infantry', ['R']).
card_text('yuan shao\'s infantry', 'Whenever Yuan Shao\'s Infantry attacks alone, Yuan Shao\'s Infantry can\'t be blocked this combat.').
card_mana_cost('yuan shao\'s infantry', ['3', 'R']).
card_cmc('yuan shao\'s infantry', 4).
card_layout('yuan shao\'s infantry', 'normal').
card_power('yuan shao\'s infantry', 2).
card_toughness('yuan shao\'s infantry', 2).

% Found in: PTK
card_name('yuan shao, the indecisive', 'Yuan Shao, the Indecisive').
card_type('yuan shao, the indecisive', 'Legendary Creature — Human Soldier').
card_types('yuan shao, the indecisive', ['Creature']).
card_subtypes('yuan shao, the indecisive', ['Human', 'Soldier']).
card_supertypes('yuan shao, the indecisive', ['Legendary']).
card_colors('yuan shao, the indecisive', ['R']).
card_text('yuan shao, the indecisive', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)\nEach creature you control can\'t be blocked by more than one creature.').
card_mana_cost('yuan shao, the indecisive', ['4', 'R']).
card_cmc('yuan shao, the indecisive', 5).
card_layout('yuan shao, the indecisive', 'normal').
card_power('yuan shao, the indecisive', 2).
card_toughness('yuan shao, the indecisive', 3).

% Found in: SOK
card_name('yuki-onna', 'Yuki-Onna').
card_type('yuki-onna', 'Creature — Spirit').
card_types('yuki-onna', ['Creature']).
card_subtypes('yuki-onna', ['Spirit']).
card_colors('yuki-onna', ['R']).
card_text('yuki-onna', 'When Yuki-Onna enters the battlefield, destroy target artifact.\nWhenever you cast a Spirit or Arcane spell, you may return Yuki-Onna to its owner\'s hand.').
card_mana_cost('yuki-onna', ['3', 'R']).
card_cmc('yuki-onna', 4).
card_layout('yuki-onna', 'normal').
card_power('yuki-onna', 3).
card_toughness('yuki-onna', 1).

% Found in: BOK
card_name('yukora, the prisoner', 'Yukora, the Prisoner').
card_type('yukora, the prisoner', 'Legendary Creature — Demon Spirit').
card_types('yukora, the prisoner', ['Creature']).
card_subtypes('yukora, the prisoner', ['Demon', 'Spirit']).
card_supertypes('yukora, the prisoner', ['Legendary']).
card_colors('yukora, the prisoner', ['B']).
card_text('yukora, the prisoner', 'When Yukora, the Prisoner leaves the battlefield, sacrifice all non-Ogre creatures you control.').
card_mana_cost('yukora, the prisoner', ['2', 'B', 'B']).
card_cmc('yukora, the prisoner', 4).
card_layout('yukora, the prisoner', 'normal').
card_power('yukora, the prisoner', 5).
card_toughness('yukora, the prisoner', 5).

% Found in: pHHO
card_name('yule ooze', 'Yule Ooze').
card_type('yule ooze', 'Creature — Ooze').
card_types('yule ooze', ['Creature']).
card_subtypes('yule ooze', ['Ooze']).
card_colors('yule ooze', ['R', 'G']).
card_text('yule ooze', 'At the beginning of your upkeep, destroy another nonland permanent chosen at random, then put a number of +1/+1 counters on Yule Ooze equal to that permanent\'s converted mana cost.\n{R}{G}, Eat some food: Regenerate Yule Ooze.').
card_mana_cost('yule ooze', ['2', 'R', 'G']).
card_cmc('yule ooze', 4).
card_layout('yule ooze', 'normal').
card_power('yule ooze', 1).
card_toughness('yule ooze', 1).

