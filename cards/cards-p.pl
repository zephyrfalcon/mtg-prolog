% Card-specific data

% Found in: 10E, 6ED, 7ED, 8ED, 9ED, ATH, BRB, DD3_DVD, DDC, DTK, M10, M11, M12, M13, M14, MIR, ONS, TMP, TPR, USG
card_name('pacifism', 'Pacifism').
card_type('pacifism', 'Enchantment — Aura').
card_types('pacifism', ['Enchantment']).
card_subtypes('pacifism', ['Aura']).
card_colors('pacifism', ['W']).
card_text('pacifism', 'Enchant creature\nEnchanted creature can\'t attack or block.').
card_mana_cost('pacifism', ['1', 'W']).
card_cmc('pacifism', 2).
card_layout('pacifism', 'normal').

% Found in: NMS
card_name('pack hunt', 'Pack Hunt').
card_type('pack hunt', 'Sorcery').
card_types('pack hunt', ['Sorcery']).
card_subtypes('pack hunt', []).
card_colors('pack hunt', ['G']).
card_text('pack hunt', 'Search your library for up to three cards with the same name as target creature, reveal them, and put them into your hand. Then shuffle your library.').
card_mana_cost('pack hunt', ['3', 'G']).
card_cmc('pack hunt', 4).
card_layout('pack hunt', 'normal').

% Found in: RTR
card_name('pack rat', 'Pack Rat').
card_type('pack rat', 'Creature — Rat').
card_types('pack rat', ['Creature']).
card_subtypes('pack rat', ['Rat']).
card_colors('pack rat', ['B']).
card_text('pack rat', 'Pack Rat\'s power and toughness are each equal to the number of Rats you control.\n{2}{B}, Discard a card: Put a token onto the battlefield that\'s a copy of Pack Rat.').
card_mana_cost('pack rat', ['1', 'B']).
card_cmc('pack rat', 2).
card_layout('pack rat', 'normal').
card_power('pack rat', '*').
card_toughness('pack rat', '*').

% Found in: MOR
card_name('pack\'s disdain', 'Pack\'s Disdain').
card_type('pack\'s disdain', 'Instant').
card_types('pack\'s disdain', ['Instant']).
card_subtypes('pack\'s disdain', []).
card_colors('pack\'s disdain', ['B']).
card_text('pack\'s disdain', 'Choose a creature type. Target creature gets -1/-1 until end of turn for each permanent of the chosen type you control.').
card_mana_cost('pack\'s disdain', ['1', 'B']).
card_cmc('pack\'s disdain', 2).
card_layout('pack\'s disdain', 'normal').

% Found in: FUT, MMA
card_name('pact of negation', 'Pact of Negation').
card_type('pact of negation', 'Instant').
card_types('pact of negation', ['Instant']).
card_subtypes('pact of negation', []).
card_colors('pact of negation', ['U']).
card_text('pact of negation', 'Counter target spell.\nAt the beginning of your next upkeep, pay {3}{U}{U}. If you don\'t, you lose the game.').
card_mana_cost('pact of negation', ['0']).
card_cmc('pact of negation', 0).
card_layout('pact of negation', 'normal').

% Found in: FUT
card_name('pact of the titan', 'Pact of the Titan').
card_type('pact of the titan', 'Instant').
card_types('pact of the titan', ['Instant']).
card_subtypes('pact of the titan', []).
card_colors('pact of the titan', ['R']).
card_text('pact of the titan', 'Put a 4/4 red Giant creature token onto the battlefield.\nAt the beginning of your next upkeep, pay {4}{R}. If you don\'t, you lose the game.').
card_mana_cost('pact of the titan', ['0']).
card_cmc('pact of the titan', 0).
card_layout('pact of the titan', 'normal').

% Found in: DDH, INV
card_name('pain', 'Pain').
card_type('pain', 'Sorcery').
card_types('pain', ['Sorcery']).
card_subtypes('pain', []).
card_colors('pain', ['B']).
card_text('pain', 'Target player discards a card.').
card_mana_cost('pain', ['B']).
card_cmc('pain', 1).
card_layout('pain', 'split').
card_sides('pain', 'suffering').

% Found in: CHK
card_name('pain kami', 'Pain Kami').
card_type('pain kami', 'Creature — Spirit').
card_types('pain kami', ['Creature']).
card_subtypes('pain kami', ['Spirit']).
card_colors('pain kami', ['R']).
card_text('pain kami', '{X}{R}, Sacrifice Pain Kami: Pain Kami deals X damage to target creature.').
card_mana_cost('pain kami', ['2', 'R']).
card_cmc('pain kami', 3).
card_layout('pain kami', 'normal').
card_power('pain kami', 2).
card_toughness('pain kami', 2).

% Found in: DIS
card_name('pain magnification', 'Pain Magnification').
card_type('pain magnification', 'Enchantment').
card_types('pain magnification', ['Enchantment']).
card_subtypes('pain magnification', []).
card_colors('pain magnification', ['B', 'R']).
card_text('pain magnification', 'Whenever an opponent is dealt 3 or more damage by a single source, that player discards a card.').
card_mana_cost('pain magnification', ['1', 'B', 'R']).
card_cmc('pain magnification', 3).
card_layout('pain magnification', 'normal').

% Found in: BNG, pMGD
card_name('pain seer', 'Pain Seer').
card_type('pain seer', 'Creature — Human Wizard').
card_types('pain seer', ['Creature']).
card_subtypes('pain seer', ['Human', 'Wizard']).
card_colors('pain seer', ['B']).
card_text('pain seer', 'Inspired — Whenever Pain Seer becomes untapped, reveal the top card of your library and put that card into your hand. You lose life equal to that card\'s converted mana cost.').
card_mana_cost('pain seer', ['1', 'B']).
card_cmc('pain seer', 2).
card_layout('pain seer', 'normal').
card_power('pain seer', 2).
card_toughness('pain seer', 2).

% Found in: SOK
card_name('pain\'s reward', 'Pain\'s Reward').
card_type('pain\'s reward', 'Sorcery').
card_types('pain\'s reward', ['Sorcery']).
card_subtypes('pain\'s reward', []).
card_colors('pain\'s reward', ['B']).
card_text('pain\'s reward', 'Each player may bid life. You start the bidding with a bid of any number. In turn order, each player may top the high bid. The bidding ends if the high bid stands. The high bidder loses life equal to the high bid and draws four cards.').
card_mana_cost('pain\'s reward', ['2', 'B']).
card_cmc('pain\'s reward', 3).
card_layout('pain\'s reward', 'normal').

% Found in: ODY
card_name('painbringer', 'Painbringer').
card_type('painbringer', 'Creature — Human Minion').
card_types('painbringer', ['Creature']).
card_subtypes('painbringer', ['Human', 'Minion']).
card_colors('painbringer', ['B']).
card_text('painbringer', '{T}, Exile any number of cards from your graveyard: Target creature gets -X/-X until end of turn, where X is the number of cards exiled this way.').
card_mana_cost('painbringer', ['2', 'B', 'B']).
card_cmc('painbringer', 4).
card_layout('painbringer', 'normal').
card_power('painbringer', 1).
card_toughness('painbringer', 1).

% Found in: 6ED, MIR
card_name('painful memories', 'Painful Memories').
card_type('painful memories', 'Sorcery').
card_types('painful memories', ['Sorcery']).
card_subtypes('painful memories', []).
card_colors('painful memories', ['B']).
card_text('painful memories', 'Look at target opponent\'s hand and choose a card from it. Put that card on top of that player\'s library.').
card_mana_cost('painful memories', ['1', 'B']).
card_cmc('painful memories', 2).
card_layout('painful memories', 'normal').

% Found in: SOM
card_name('painful quandary', 'Painful Quandary').
card_type('painful quandary', 'Enchantment').
card_types('painful quandary', ['Enchantment']).
card_subtypes('painful quandary', []).
card_colors('painful quandary', ['B']).
card_text('painful quandary', 'Whenever an opponent casts a spell, that player loses 5 life unless he or she discards a card.').
card_mana_cost('painful quandary', ['3', 'B', 'B']).
card_cmc('painful quandary', 5).
card_layout('painful quandary', 'normal').

% Found in: BFZ
card_name('painful truths', 'Painful Truths').
card_type('painful truths', 'Sorcery').
card_types('painful truths', ['Sorcery']).
card_subtypes('painful truths', []).
card_colors('painful truths', ['B']).
card_text('painful truths', 'Converge — You draw X cards and you lose X life, where X is the number of colors of mana spent to cast Painful Truths.').
card_mana_cost('painful truths', ['2', 'B']).
card_cmc('painful truths', 3).
card_layout('painful truths', 'normal').

% Found in: SOM
card_name('painsmith', 'Painsmith').
card_type('painsmith', 'Creature — Human Artificer').
card_types('painsmith', ['Creature']).
card_subtypes('painsmith', ['Human', 'Artificer']).
card_colors('painsmith', ['B']).
card_text('painsmith', 'Whenever you cast an artifact spell, you may have target creature get +2/+0 and gain deathtouch until end of turn.').
card_mana_cost('painsmith', ['1', 'B']).
card_cmc('painsmith', 2).
card_layout('painsmith', 'normal').
card_power('painsmith', 2).
card_toughness('painsmith', 1).

% Found in: SHM
card_name('painter\'s servant', 'Painter\'s Servant').
card_type('painter\'s servant', 'Artifact Creature — Scarecrow').
card_types('painter\'s servant', ['Artifact', 'Creature']).
card_subtypes('painter\'s servant', ['Scarecrow']).
card_colors('painter\'s servant', []).
card_text('painter\'s servant', 'As Painter\'s Servant enters the battlefield, choose a color.\nAll cards that aren\'t on the battlefield, spells, and permanents are the chosen color in addition to their other colors.').
card_mana_cost('painter\'s servant', ['2']).
card_cmc('painter\'s servant', 2).
card_layout('painter\'s servant', 'normal').
card_power('painter\'s servant', 1).
card_toughness('painter\'s servant', 3).

% Found in: CHK
card_name('painwracker oni', 'Painwracker Oni').
card_type('painwracker oni', 'Creature — Demon Spirit').
card_types('painwracker oni', ['Creature']).
card_subtypes('painwracker oni', ['Demon', 'Spirit']).
card_colors('painwracker oni', ['B']).
card_text('painwracker oni', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nAt the beginning of your upkeep, sacrifice a creature if you don\'t control an Ogre.').
card_mana_cost('painwracker oni', ['3', 'B', 'B']).
card_cmc('painwracker oni', 5).
card_layout('painwracker oni', 'normal').
card_power('painwracker oni', 5).
card_toughness('painwracker oni', 4).

% Found in: DTK
card_name('palace familiar', 'Palace Familiar').
card_type('palace familiar', 'Creature — Bird').
card_types('palace familiar', ['Creature']).
card_subtypes('palace familiar', ['Bird']).
card_colors('palace familiar', ['U']).
card_text('palace familiar', 'Flying\nWhen Palace Familiar dies, draw a card.').
card_mana_cost('palace familiar', ['1', 'U']).
card_cmc('palace familiar', 2).
card_layout('palace familiar', 'normal').
card_power('palace familiar', 1).
card_toughness('palace familiar', 1).

% Found in: M10, M11
card_name('palace guard', 'Palace Guard').
card_type('palace guard', 'Creature — Human Soldier').
card_types('palace guard', ['Creature']).
card_subtypes('palace guard', ['Human', 'Soldier']).
card_colors('palace guard', ['W']).
card_text('palace guard', 'Palace Guard can block any number of creatures.').
card_mana_cost('palace guard', ['2', 'W']).
card_cmc('palace guard', 3).
card_layout('palace guard', 'normal').
card_power('palace guard', 1).
card_toughness('palace guard', 4).

% Found in: FRF
card_name('palace siege', 'Palace Siege').
card_type('palace siege', 'Enchantment').
card_types('palace siege', ['Enchantment']).
card_subtypes('palace siege', []).
card_colors('palace siege', ['B']).
card_text('palace siege', 'As Palace Siege enters the battlefield, choose Khans or Dragons.\n• Khans — At the beginning of your upkeep, return target creature card from your graveyard to your hand.\n• Dragons — At the beginning of your upkeep, each opponent loses 2 life and you gain 2 life.').
card_mana_cost('palace siege', ['3', 'B', 'B']).
card_cmc('palace siege', 5).
card_layout('palace siege', 'normal').

% Found in: 10E, 9ED, EXO, TPR
card_name('paladin en-vec', 'Paladin en-Vec').
card_type('paladin en-vec', 'Creature — Human Knight').
card_types('paladin en-vec', ['Creature']).
card_subtypes('paladin en-vec', ['Human', 'Knight']).
card_colors('paladin en-vec', ['W']).
card_text('paladin en-vec', 'First strike, protection from black and from red (This creature deals combat damage before creatures without first strike. It can\'t be blocked, targeted, dealt damage, or enchanted by anything black or red.)').
card_mana_cost('paladin en-vec', ['1', 'W', 'W']).
card_cmc('paladin en-vec', 3).
card_layout('paladin en-vec', 'normal').
card_power('paladin en-vec', 2).
card_toughness('paladin en-vec', 2).

% Found in: DDG, DIS
card_name('paladin of prahv', 'Paladin of Prahv').
card_type('paladin of prahv', 'Creature — Human Knight').
card_types('paladin of prahv', ['Creature']).
card_subtypes('paladin of prahv', ['Human', 'Knight']).
card_colors('paladin of prahv', ['W']).
card_text('paladin of prahv', 'Whenever Paladin of Prahv deals damage, you gain that much life.\nForecast — {1}{W}, Reveal Paladin of Prahv from your hand: Whenever target creature deals damage this turn, you gain that much life. (Activate this ability only during your upkeep and only once each turn.)').
card_mana_cost('paladin of prahv', ['4', 'W', 'W']).
card_cmc('paladin of prahv', 6).
card_layout('paladin of prahv', 'normal').
card_power('paladin of prahv', 3).
card_toughness('paladin of prahv', 4).

% Found in: ICE
card_name('pale bears', 'Pale Bears').
card_type('pale bears', 'Creature — Bear').
card_types('pale bears', ['Creature']).
card_subtypes('pale bears', ['Bear']).
card_colors('pale bears', ['G']).
card_text('pale bears', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('pale bears', ['2', 'G']).
card_cmc('pale bears', 3).
card_layout('pale bears', 'normal').
card_power('pale bears', 2).
card_toughness('pale bears', 2).

% Found in: NMS
card_name('pale moon', 'Pale Moon').
card_type('pale moon', 'Instant').
card_types('pale moon', ['Instant']).
card_subtypes('pale moon', []).
card_colors('pale moon', ['U']).
card_text('pale moon', 'Until end of turn, if a player taps a nonbasic land for mana, it produces colorless mana instead of any other type.').
card_mana_cost('pale moon', ['1', 'U']).
card_cmc('pale moon', 2).
card_layout('pale moon', 'normal').

% Found in: ARB, ARC
card_name('pale recluse', 'Pale Recluse').
card_type('pale recluse', 'Creature — Spider').
card_types('pale recluse', ['Creature']).
card_subtypes('pale recluse', ['Spider']).
card_colors('pale recluse', ['W', 'G']).
card_text('pale recluse', 'Reach (This creature can block creatures with flying.)\nForestcycling {2}, plainscycling {2} ({2}, Discard this card: Search your library for a Forest or Plains card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('pale recluse', ['4', 'G', 'W']).
card_cmc('pale recluse', 6).
card_layout('pale recluse', 'normal').
card_power('pale recluse', 4).
card_toughness('pale recluse', 5).

% Found in: SHM
card_name('pale wayfarer', 'Pale Wayfarer').
card_type('pale wayfarer', 'Creature — Spirit Giant').
card_types('pale wayfarer', ['Creature']).
card_subtypes('pale wayfarer', ['Spirit', 'Giant']).
card_colors('pale wayfarer', ['W']).
card_text('pale wayfarer', '{2}{W}{W}, {Q}: Target creature gains protection from the color of its controller\'s choice until end of turn. ({Q} is the untap symbol.)').
card_mana_cost('pale wayfarer', ['5', 'W', 'W']).
card_cmc('pale wayfarer', 7).
card_layout('pale wayfarer', 'normal').
card_power('pale wayfarer', 4).
card_toughness('pale wayfarer', 4).

% Found in: CON
card_name('paleoloth', 'Paleoloth').
card_type('paleoloth', 'Creature — Beast').
card_types('paleoloth', ['Creature']).
card_subtypes('paleoloth', ['Beast']).
card_colors('paleoloth', ['G']).
card_text('paleoloth', 'Whenever another creature with power 5 or greater enters the battlefield under your control, you may return target creature card from your graveyard to your hand.').
card_mana_cost('paleoloth', ['4', 'G', 'G']).
card_cmc('paleoloth', 6).
card_layout('paleoloth', 'normal').
card_power('paleoloth', 5).
card_toughness('paleoloth', 5).

% Found in: CNS
card_name('paliano, the high city', 'Paliano, the High City').
card_type('paliano, the high city', 'Legendary Land').
card_types('paliano, the high city', ['Land']).
card_subtypes('paliano, the high city', []).
card_supertypes('paliano, the high city', ['Legendary']).
card_colors('paliano, the high city', []).
card_text('paliano, the high city', 'Reveal Paliano, the High City as you draft it. The player to your right chooses a color, you choose another color, then the player to your left chooses a third color.\n{T}: Add one mana to your mana pool of any color chosen as you drafted cards named Paliano, the High City.').
card_layout('paliano, the high city', 'normal').

% Found in: ULG, VMA
card_name('palinchron', 'Palinchron').
card_type('palinchron', 'Creature — Illusion').
card_types('palinchron', ['Creature']).
card_subtypes('palinchron', ['Illusion']).
card_colors('palinchron', ['U']).
card_text('palinchron', 'Flying\nWhen Palinchron enters the battlefield, untap up to seven lands.\n{2}{U}{U}: Return Palinchron to its owner\'s hand.').
card_mana_cost('palinchron', ['5', 'U', 'U']).
card_cmc('palinchron', 7).
card_layout('palinchron', 'normal').
card_power('palinchron', 4).
card_toughness('palinchron', 5).
card_reserved('palinchron').

% Found in: RTR
card_name('palisade giant', 'Palisade Giant').
card_type('palisade giant', 'Creature — Giant Soldier').
card_types('palisade giant', ['Creature']).
card_subtypes('palisade giant', ['Giant', 'Soldier']).
card_colors('palisade giant', ['W']).
card_text('palisade giant', 'All damage that would be dealt to you or another permanent you control is dealt to Palisade Giant instead.').
card_mana_cost('palisade giant', ['4', 'W', 'W']).
card_cmc('palisade giant', 6).
card_layout('palisade giant', 'normal').
card_power('palisade giant', 2).
card_toughness('palisade giant', 7).

% Found in: CHR, LEG, ME3
card_name('palladia-mors', 'Palladia-Mors').
card_type('palladia-mors', 'Legendary Creature — Elder Dragon').
card_types('palladia-mors', ['Creature']).
card_subtypes('palladia-mors', ['Elder', 'Dragon']).
card_supertypes('palladia-mors', ['Legendary']).
card_colors('palladia-mors', ['W', 'R', 'G']).
card_text('palladia-mors', 'Flying, trample\nAt the beginning of your upkeep, sacrifice Palladia-Mors unless you pay {R}{G}{W}.').
card_mana_cost('palladia-mors', ['2', 'R', 'R', 'G', 'G', 'W', 'W']).
card_cmc('palladia-mors', 8).
card_layout('palladia-mors', 'normal').
card_power('palladia-mors', 7).
card_toughness('palladia-mors', 7).

% Found in: C14, SOM
card_name('palladium myr', 'Palladium Myr').
card_type('palladium myr', 'Artifact Creature — Myr').
card_types('palladium myr', ['Artifact', 'Creature']).
card_subtypes('palladium myr', ['Myr']).
card_colors('palladium myr', []).
card_text('palladium myr', '{T}: Add {2} to your mana pool.').
card_mana_cost('palladium myr', ['3']).
card_cmc('palladium myr', 3).
card_layout('palladium myr', 'normal').
card_power('palladium myr', 2).
card_toughness('palladium myr', 2).

% Found in: DIS
card_name('palliation accord', 'Palliation Accord').
card_type('palliation accord', 'Enchantment').
card_types('palliation accord', ['Enchantment']).
card_subtypes('palliation accord', []).
card_colors('palliation accord', ['W', 'U']).
card_text('palliation accord', 'Whenever a creature an opponent controls becomes tapped, put a shield counter on Palliation Accord.\nRemove a shield counter from Palliation Accord: Prevent the next 1 damage that would be dealt to you this turn.').
card_mana_cost('palliation accord', ['3', 'W', 'U']).
card_cmc('palliation accord', 5).
card_layout('palliation accord', 'normal').

% Found in: MMA, PLC
card_name('pallid mycoderm', 'Pallid Mycoderm').
card_type('pallid mycoderm', 'Creature — Fungus').
card_types('pallid mycoderm', ['Creature']).
card_subtypes('pallid mycoderm', ['Fungus']).
card_colors('pallid mycoderm', ['W']).
card_text('pallid mycoderm', 'At the beginning of your upkeep, put a spore counter on Pallid Mycoderm.\nRemove three spore counters from Pallid Mycoderm: Put a 1/1 green Saproling creature token onto the battlefield.\nSacrifice a Saproling: Each creature you control that\'s a Fungus or a Saproling gets +1/+1 until end of turn.').
card_mana_cost('pallid mycoderm', ['3', 'W']).
card_cmc('pallid mycoderm', 4).
card_layout('pallid mycoderm', 'normal').
card_power('pallid mycoderm', 2).
card_toughness('pallid mycoderm', 4).

% Found in: TMP
card_name('pallimud', 'Pallimud').
card_type('pallimud', 'Creature — Beast').
card_types('pallimud', ['Creature']).
card_subtypes('pallimud', ['Beast']).
card_colors('pallimud', ['R']).
card_text('pallimud', 'As Pallimud enters the battlefield, choose an opponent.\nPallimud\'s power is equal to the number of tapped lands the chosen player controls.').
card_mana_cost('pallimud', ['2', 'R']).
card_cmc('pallimud', 3).
card_layout('pallimud', 'normal').
card_power('pallimud', '*').
card_toughness('pallimud', 3).

% Found in: MMQ
card_name('panacea', 'Panacea').
card_type('panacea', 'Artifact').
card_types('panacea', ['Artifact']).
card_subtypes('panacea', []).
card_colors('panacea', []).
card_text('panacea', '{X}{X}, {T}: Prevent the next X damage that would be dealt to target creature or player this turn.').
card_mana_cost('panacea', ['4']).
card_cmc('panacea', 4).
card_layout('panacea', 'normal').

% Found in: EXO, TPR, TSB
card_name('pandemonium', 'Pandemonium').
card_type('pandemonium', 'Enchantment').
card_types('pandemonium', ['Enchantment']).
card_subtypes('pandemonium', []).
card_colors('pandemonium', ['R']).
card_text('pandemonium', 'Whenever a creature enters the battlefield, that creature\'s controller may have it deal damage equal to its power to target creature or player of his or her choice.').
card_mana_cost('pandemonium', ['3', 'R']).
card_cmc('pandemonium', 4).
card_layout('pandemonium', 'normal').

% Found in: PTK
card_name('pang tong, \"young phoenix\"', 'Pang Tong, \"Young Phoenix\"').
card_type('pang tong, \"young phoenix\"', 'Legendary Creature — Human Advisor').
card_types('pang tong, \"young phoenix\"', ['Creature']).
card_subtypes('pang tong, \"young phoenix\"', ['Human', 'Advisor']).
card_supertypes('pang tong, \"young phoenix\"', ['Legendary']).
card_colors('pang tong, \"young phoenix\"', ['W']).
card_text('pang tong, \"young phoenix\"', '{T}: Target creature gets +0/+2 until end of turn. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('pang tong, \"young phoenix\"', ['1', 'W', 'W']).
card_cmc('pang tong, \"young phoenix\"', 3).
card_layout('pang tong, \"young phoenix\"', 'normal').
card_power('pang tong, \"young phoenix\"', 1).
card_toughness('pang tong, \"young phoenix\"', 2).

% Found in: CSP
card_name('panglacial wurm', 'Panglacial Wurm').
card_type('panglacial wurm', 'Creature — Wurm').
card_types('panglacial wurm', ['Creature']).
card_subtypes('panglacial wurm', ['Wurm']).
card_colors('panglacial wurm', ['G']).
card_text('panglacial wurm', 'Trample\nWhile you\'re searching your library, you may cast Panglacial Wurm from your library.').
card_mana_cost('panglacial wurm', ['5', 'G', 'G']).
card_cmc('panglacial wurm', 7).
card_layout('panglacial wurm', 'normal').
card_power('panglacial wurm', 9).
card_toughness('panglacial wurm', 5).

% Found in: MMQ
card_name('pangosaur', 'Pangosaur').
card_type('pangosaur', 'Creature — Lizard').
card_types('pangosaur', ['Creature']).
card_subtypes('pangosaur', ['Lizard']).
card_colors('pangosaur', ['G']).
card_text('pangosaur', 'Whenever a player plays a land, return Pangosaur to its owner\'s hand.').
card_mana_cost('pangosaur', ['2', 'G', 'G']).
card_cmc('pangosaur', 4).
card_layout('pangosaur', 'normal').
card_power('pangosaur', 6).
card_toughness('pangosaur', 6).

% Found in: 5ED, ICE, ME2
card_name('panic', 'Panic').
card_type('panic', 'Instant').
card_types('panic', ['Instant']).
card_subtypes('panic', []).
card_colors('panic', ['R']).
card_text('panic', 'Cast Panic only during combat before blockers are declared.\nTarget creature can\'t block this turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('panic', ['R']).
card_cmc('panic', 1).
card_layout('panic', 'normal').

% Found in: 8ED, 9ED, M10, PCY
card_name('panic attack', 'Panic Attack').
card_type('panic attack', 'Sorcery').
card_types('panic attack', ['Sorcery']).
card_subtypes('panic attack', []).
card_colors('panic attack', ['R']).
card_text('panic attack', 'Up to three target creatures can\'t block this turn.').
card_mana_cost('panic attack', ['2', 'R']).
card_cmc('panic attack', 3).
card_layout('panic attack', 'normal').

% Found in: C14, SOM
card_name('panic spellbomb', 'Panic Spellbomb').
card_type('panic spellbomb', 'Artifact').
card_types('panic spellbomb', ['Artifact']).
card_subtypes('panic spellbomb', []).
card_colors('panic spellbomb', []).
card_text('panic spellbomb', '{T}, Sacrifice Panic Spellbomb: Target creature can\'t block this turn.\nWhen Panic Spellbomb is put into a graveyard from the battlefield, you may pay {R}. If you do, draw a card.').
card_mana_cost('panic spellbomb', ['1']).
card_cmc('panic spellbomb', 1).
card_layout('panic spellbomb', 'normal').

% Found in: DST
card_name('panoptic mirror', 'Panoptic Mirror').
card_type('panoptic mirror', 'Artifact').
card_types('panoptic mirror', ['Artifact']).
card_subtypes('panoptic mirror', []).
card_colors('panoptic mirror', []).
card_text('panoptic mirror', 'Imprint — {X}, {T}: You may exile an instant or sorcery card with converted mana cost X from your hand.\nAt the beginning of your upkeep, you may copy a card exiled with Panoptic Mirror. If you do, you may cast the copy without paying its mana cost.').
card_mana_cost('panoptic mirror', ['5']).
card_cmc('panoptic mirror', 5).
card_layout('panoptic mirror', 'normal').

% Found in: HOP
card_name('panopticon', 'Panopticon').
card_type('panopticon', 'Plane — Mirrodin').
card_types('panopticon', ['Plane']).
card_subtypes('panopticon', ['Mirrodin']).
card_colors('panopticon', []).
card_text('panopticon', 'When you planeswalk to Panopticon, draw a card.\nAt the beginning of your draw step, draw an additional card.\nWhenever you roll {C}, draw a card.').
card_layout('panopticon', 'plane').

% Found in: 6ED, POR, VIS
card_name('panther warriors', 'Panther Warriors').
card_type('panther warriors', 'Creature — Cat Warrior').
card_types('panther warriors', ['Creature']).
card_subtypes('panther warriors', ['Cat', 'Warrior']).
card_colors('panther warriors', ['G']).
card_text('panther warriors', '').
card_mana_cost('panther warriors', ['4', 'G']).
card_cmc('panther warriors', 5).
card_layout('panther warriors', 'normal').
card_power('panther warriors', 6).
card_toughness('panther warriors', 3).

% Found in: UGL
card_name('paper tiger', 'Paper Tiger').
card_type('paper tiger', 'Artifact Creature').
card_types('paper tiger', ['Artifact', 'Creature']).
card_subtypes('paper tiger', []).
card_colors('paper tiger', []).
card_text('paper tiger', 'Rock Lobsters cannot attack or block.').
card_mana_cost('paper tiger', ['4']).
card_cmc('paper tiger', 4).
card_layout('paper tiger', 'normal').
card_power('paper tiger', 4).
card_toughness('paper tiger', 3).

% Found in: LRW
card_name('paperfin rascal', 'Paperfin Rascal').
card_type('paperfin rascal', 'Creature — Merfolk Rogue').
card_types('paperfin rascal', ['Creature']).
card_subtypes('paperfin rascal', ['Merfolk', 'Rogue']).
card_colors('paperfin rascal', ['U']).
card_text('paperfin rascal', 'When Paperfin Rascal enters the battlefield, clash with an opponent. If you win, put a +1/+1 counter on Paperfin Rascal. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('paperfin rascal', ['2', 'U']).
card_cmc('paperfin rascal', 3).
card_layout('paperfin rascal', 'normal').
card_power('paperfin rascal', 2).
card_toughness('paperfin rascal', 2).

% Found in: WTH
card_name('paradigm shift', 'Paradigm Shift').
card_type('paradigm shift', 'Sorcery').
card_types('paradigm shift', ['Sorcery']).
card_subtypes('paradigm shift', []).
card_colors('paradigm shift', ['U']).
card_text('paradigm shift', 'Exile all cards from your library. Then shuffle your graveyard into your library.').
card_mana_cost('paradigm shift', ['1', 'U']).
card_cmc('paradigm shift', 2).
card_layout('paradigm shift', 'normal').
card_reserved('paradigm shift').

% Found in: 5DN, MMA
card_name('paradise mantle', 'Paradise Mantle').
card_type('paradise mantle', 'Artifact — Equipment').
card_types('paradise mantle', ['Artifact']).
card_subtypes('paradise mantle', ['Equipment']).
card_colors('paradise mantle', []).
card_text('paradise mantle', 'Equipped creature has \"{T}: Add one mana of any color to your mana pool.\"\nEquip {1}').
card_mana_cost('paradise mantle', ['0']).
card_cmc('paradise mantle', 0).
card_layout('paradise mantle', 'normal').

% Found in: TSP
card_name('paradise plume', 'Paradise Plume').
card_type('paradise plume', 'Artifact').
card_types('paradise plume', ['Artifact']).
card_subtypes('paradise plume', []).
card_colors('paradise plume', []).
card_text('paradise plume', 'As Paradise Plume enters the battlefield, choose a color.\nWhenever a player casts a spell of the chosen color, you may gain 1 life.\n{T}: Add one mana of the chosen color to your mana pool.').
card_mana_cost('paradise plume', ['4']).
card_cmc('paradise plume', 4).
card_layout('paradise plume', 'normal').

% Found in: TSP
card_name('paradox haze', 'Paradox Haze').
card_type('paradox haze', 'Enchantment — Aura').
card_types('paradox haze', ['Enchantment']).
card_subtypes('paradox haze', ['Aura']).
card_colors('paradox haze', ['U']).
card_text('paradox haze', 'Enchant player\nAt the beginning of enchanted player\'s first upkeep each turn, that player gets an additional upkeep step after this step.').
card_mana_cost('paradox haze', ['2', 'U']).
card_cmc('paradox haze', 3).
card_layout('paradox haze', 'normal').

% Found in: M15
card_name('paragon of eternal wilds', 'Paragon of Eternal Wilds').
card_type('paragon of eternal wilds', 'Creature — Human Druid').
card_types('paragon of eternal wilds', ['Creature']).
card_subtypes('paragon of eternal wilds', ['Human', 'Druid']).
card_colors('paragon of eternal wilds', ['G']).
card_text('paragon of eternal wilds', 'Other green creatures you control get +1/+1.\n{G}, {T}: Another target green creature you control gains trample until end of turn. (If it would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('paragon of eternal wilds', ['3', 'G']).
card_cmc('paragon of eternal wilds', 4).
card_layout('paragon of eternal wilds', 'normal').
card_power('paragon of eternal wilds', 2).
card_toughness('paragon of eternal wilds', 2).

% Found in: M15
card_name('paragon of fierce defiance', 'Paragon of Fierce Defiance').
card_type('paragon of fierce defiance', 'Creature — Human Warrior').
card_types('paragon of fierce defiance', ['Creature']).
card_subtypes('paragon of fierce defiance', ['Human', 'Warrior']).
card_colors('paragon of fierce defiance', ['R']).
card_text('paragon of fierce defiance', 'Other red creatures you control get +1/+1.\n{R}, {T}: Another target red creature you control gains haste until end of turn. (It can attack and {T} this turn.)').
card_mana_cost('paragon of fierce defiance', ['3', 'R']).
card_cmc('paragon of fierce defiance', 4).
card_layout('paragon of fierce defiance', 'normal').
card_power('paragon of fierce defiance', 2).
card_toughness('paragon of fierce defiance', 2).

% Found in: M15
card_name('paragon of gathering mists', 'Paragon of Gathering Mists').
card_type('paragon of gathering mists', 'Creature — Human Wizard').
card_types('paragon of gathering mists', ['Creature']).
card_subtypes('paragon of gathering mists', ['Human', 'Wizard']).
card_colors('paragon of gathering mists', ['U']).
card_text('paragon of gathering mists', 'Other blue creatures you control get +1/+1.\n{U}, {T}: Another target blue creature you control gains flying until end of turn.').
card_mana_cost('paragon of gathering mists', ['3', 'U']).
card_cmc('paragon of gathering mists', 4).
card_layout('paragon of gathering mists', 'normal').
card_power('paragon of gathering mists', 2).
card_toughness('paragon of gathering mists', 2).

% Found in: M15
card_name('paragon of new dawns', 'Paragon of New Dawns').
card_type('paragon of new dawns', 'Creature — Human Soldier').
card_types('paragon of new dawns', ['Creature']).
card_subtypes('paragon of new dawns', ['Human', 'Soldier']).
card_colors('paragon of new dawns', ['W']).
card_text('paragon of new dawns', 'Other white creatures you control get +1/+1.\n{W}, {T}: Another target white creature you control gains vigilance until end of turn. (Attacking doesn\'t cause it to tap.)').
card_mana_cost('paragon of new dawns', ['3', 'W']).
card_cmc('paragon of new dawns', 4).
card_layout('paragon of new dawns', 'normal').
card_power('paragon of new dawns', 2).
card_toughness('paragon of new dawns', 2).

% Found in: M15
card_name('paragon of open graves', 'Paragon of Open Graves').
card_type('paragon of open graves', 'Creature — Skeleton Warrior').
card_types('paragon of open graves', ['Creature']).
card_subtypes('paragon of open graves', ['Skeleton', 'Warrior']).
card_colors('paragon of open graves', ['B']).
card_text('paragon of open graves', 'Other black creatures you control get +1/+1.\n{2}{B}, {T}: Another target black creature you control gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_mana_cost('paragon of open graves', ['3', 'B']).
card_cmc('paragon of open graves', 4).
card_layout('paragon of open graves', 'normal').
card_power('paragon of open graves', 2).
card_toughness('paragon of open graves', 2).

% Found in: CON
card_name('paragon of the amesha', 'Paragon of the Amesha').
card_type('paragon of the amesha', 'Creature — Human Knight').
card_types('paragon of the amesha', ['Creature']).
card_subtypes('paragon of the amesha', ['Human', 'Knight']).
card_colors('paragon of the amesha', ['W']).
card_text('paragon of the amesha', 'First strike\n{W}{U}{B}{R}{G}: Until end of turn, Paragon of the Amesha becomes an Angel, gets +3/+3, and gains flying and lifelink.').
card_mana_cost('paragon of the amesha', ['2', 'W']).
card_cmc('paragon of the amesha', 3).
card_layout('paragon of the amesha', 'normal').
card_power('paragon of the amesha', 2).
card_toughness('paragon of the amesha', 2).

% Found in: NMS
card_name('parallax dementia', 'Parallax Dementia').
card_type('parallax dementia', 'Enchantment — Aura').
card_types('parallax dementia', ['Enchantment']).
card_subtypes('parallax dementia', ['Aura']).
card_colors('parallax dementia', ['B']).
card_text('parallax dementia', 'Enchant creature\nFading 1 (This enchantment enters the battlefield with one fade counter on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nEnchanted creature gets +3/+2.\nWhen Parallax Dementia leaves the battlefield, destroy enchanted creature. That creature can\'t be regenerated.').
card_mana_cost('parallax dementia', ['1', 'B']).
card_cmc('parallax dementia', 2).
card_layout('parallax dementia', 'normal').

% Found in: NMS
card_name('parallax inhibitor', 'Parallax Inhibitor').
card_type('parallax inhibitor', 'Artifact').
card_types('parallax inhibitor', ['Artifact']).
card_subtypes('parallax inhibitor', []).
card_colors('parallax inhibitor', []).
card_text('parallax inhibitor', '{1}, {T}, Sacrifice Parallax Inhibitor: Put a fade counter on each permanent with fading you control.').
card_mana_cost('parallax inhibitor', ['2']).
card_cmc('parallax inhibitor', 2).
card_layout('parallax inhibitor', 'normal').

% Found in: NMS
card_name('parallax nexus', 'Parallax Nexus').
card_type('parallax nexus', 'Enchantment').
card_types('parallax nexus', ['Enchantment']).
card_subtypes('parallax nexus', []).
card_colors('parallax nexus', ['B']).
card_text('parallax nexus', 'Fading 5 (This enchantment enters the battlefield with five fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nRemove a fade counter from Parallax Nexus: Target opponent exiles a card from his or her hand. Activate this ability only any time you could cast a sorcery.\nWhen Parallax Nexus leaves the battlefield, each player returns to his or her hand all cards he or she owns exiled with Parallax Nexus.').
card_mana_cost('parallax nexus', ['2', 'B']).
card_cmc('parallax nexus', 3).
card_layout('parallax nexus', 'normal').

% Found in: NMS
card_name('parallax tide', 'Parallax Tide').
card_type('parallax tide', 'Enchantment').
card_types('parallax tide', ['Enchantment']).
card_subtypes('parallax tide', []).
card_colors('parallax tide', ['U']).
card_text('parallax tide', 'Fading 5 (This enchantment enters the battlefield with five fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nRemove a fade counter from Parallax Tide: Exile target land.\nWhen Parallax Tide leaves the battlefield, each player returns to the battlefield all cards he or she owns exiled with Parallax Tide.').
card_mana_cost('parallax tide', ['2', 'U', 'U']).
card_cmc('parallax tide', 4).
card_layout('parallax tide', 'normal').

% Found in: NMS, VMA
card_name('parallax wave', 'Parallax Wave').
card_type('parallax wave', 'Enchantment').
card_types('parallax wave', ['Enchantment']).
card_subtypes('parallax wave', []).
card_colors('parallax wave', ['W']).
card_text('parallax wave', 'Fading 5 (This enchantment enters the battlefield with five fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nRemove a fade counter from Parallax Wave: Exile target creature.\nWhen Parallax Wave leaves the battlefield, each player returns to the battlefield all cards he or she owns exiled with Parallax Wave.').
card_mana_cost('parallax wave', ['2', 'W', 'W']).
card_cmc('parallax wave', 4).
card_layout('parallax wave', 'normal').

% Found in: GPT
card_name('parallectric feedback', 'Parallectric Feedback').
card_type('parallectric feedback', 'Instant').
card_types('parallectric feedback', ['Instant']).
card_subtypes('parallectric feedback', []).
card_colors('parallectric feedback', ['R']).
card_text('parallectric feedback', 'Parallectric Feedback deals damage to target spell\'s controller equal to that spell\'s converted mana cost.').
card_mana_cost('parallectric feedback', ['3', 'R']).
card_cmc('parallectric feedback', 4).
card_layout('parallectric feedback', 'normal').

% Found in: TOR
card_name('parallel evolution', 'Parallel Evolution').
card_type('parallel evolution', 'Sorcery').
card_types('parallel evolution', ['Sorcery']).
card_subtypes('parallel evolution', []).
card_colors('parallel evolution', ['G']).
card_text('parallel evolution', 'For each creature token on the battlefield, its controller puts a token that\'s a copy of that creature onto the battlefield.\nFlashback {4}{G}{G}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('parallel evolution', ['3', 'G', 'G']).
card_cmc('parallel evolution', 5).
card_layout('parallel evolution', 'normal').

% Found in: ISD
card_name('parallel lives', 'Parallel Lives').
card_type('parallel lives', 'Enchantment').
card_types('parallel lives', ['Enchantment']).
card_subtypes('parallel lives', []).
card_colors('parallel lives', ['G']).
card_text('parallel lives', 'If an effect would put one or more tokens onto the battlefield under your control, it puts twice that many of those tokens onto the battlefield instead.').
card_mana_cost('parallel lives', ['3', 'G']).
card_cmc('parallel lives', 4).
card_layout('parallel lives', 'normal').

% Found in: SCG
card_name('parallel thoughts', 'Parallel Thoughts').
card_type('parallel thoughts', 'Enchantment').
card_types('parallel thoughts', ['Enchantment']).
card_subtypes('parallel thoughts', []).
card_colors('parallel thoughts', ['U']).
card_text('parallel thoughts', 'When Parallel Thoughts enters the battlefield, search your library for seven cards, exile them in a face-down pile, and shuffle that pile. Then shuffle your library.\nIf you would draw a card, you may instead put the top card of the pile you exiled into your hand.').
card_mana_cost('parallel thoughts', ['3', 'U', 'U']).
card_cmc('parallel thoughts', 5).
card_layout('parallel thoughts', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, MED, VMA
card_name('paralyze', 'Paralyze').
card_type('paralyze', 'Enchantment — Aura').
card_types('paralyze', ['Enchantment']).
card_subtypes('paralyze', ['Aura']).
card_colors('paralyze', ['B']).
card_text('paralyze', 'Enchant creature\nWhen Paralyze enters the battlefield, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nAt the beginning of the upkeep of enchanted creature\'s controller, that player may pay {4}. If he or she does, untap the creature.').
card_mana_cost('paralyze', ['B']).
card_cmc('paralyze', 1).
card_layout('paralyze', 'normal').

% Found in: RTR, ZEN
card_name('paralyzing grasp', 'Paralyzing Grasp').
card_type('paralyzing grasp', 'Enchantment — Aura').
card_types('paralyzing grasp', ['Enchantment']).
card_subtypes('paralyzing grasp', ['Aura']).
card_colors('paralyzing grasp', ['U']).
card_text('paralyzing grasp', 'Enchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_mana_cost('paralyzing grasp', ['2', 'U']).
card_cmc('paralyzing grasp', 3).
card_layout('paralyzing grasp', 'normal').

% Found in: GTC
card_name('paranoid delusions', 'Paranoid Delusions').
card_type('paranoid delusions', 'Sorcery').
card_types('paranoid delusions', ['Sorcery']).
card_subtypes('paranoid delusions', []).
card_colors('paranoid delusions', ['U', 'B']).
card_text('paranoid delusions', 'Target player puts the top three cards of his or her library into his or her graveyard.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_mana_cost('paranoid delusions', ['U', 'B']).
card_cmc('paranoid delusions', 2).
card_layout('paranoid delusions', 'normal').

% Found in: VIS
card_name('parapet', 'Parapet').
card_type('parapet', 'Enchantment').
card_types('parapet', ['Enchantment']).
card_subtypes('parapet', []).
card_colors('parapet', ['W']).
card_text('parapet', 'You may cast Parapet as though it had flash. If you cast it any time a sorcery couldn\'t have been cast, the controller of the permanent it becomes sacrifices it at the beginning of the next cleanup step.\nCreatures you control get +0/+1.').
card_mana_cost('parapet', ['1', 'W']).
card_cmc('parapet', 2).
card_layout('parapet', 'normal').

% Found in: SHM
card_name('parapet watchers', 'Parapet Watchers').
card_type('parapet watchers', 'Creature — Kithkin Soldier').
card_types('parapet watchers', ['Creature']).
card_subtypes('parapet watchers', ['Kithkin', 'Soldier']).
card_colors('parapet watchers', ['U']).
card_text('parapet watchers', '{W/U}: Parapet Watchers gets +0/+1 until end of turn.').
card_mana_cost('parapet watchers', ['2', 'U']).
card_cmc('parapet watchers', 3).
card_layout('parapet watchers', 'normal').
card_power('parapet watchers', 2).
card_toughness('parapet watchers', 2).

% Found in: ISD
card_name('paraselene', 'Paraselene').
card_type('paraselene', 'Sorcery').
card_types('paraselene', ['Sorcery']).
card_subtypes('paraselene', []).
card_colors('paraselene', ['W']).
card_text('paraselene', 'Destroy all enchantments. You gain 1 life for each enchantment destroyed this way.').
card_mana_cost('paraselene', ['2', 'W']).
card_cmc('paraselene', 3).
card_layout('paraselene', 'normal').

% Found in: USG
card_name('parasitic bond', 'Parasitic Bond').
card_type('parasitic bond', 'Enchantment — Aura').
card_types('parasitic bond', ['Enchantment']).
card_subtypes('parasitic bond', ['Aura']).
card_colors('parasitic bond', ['B']).
card_text('parasitic bond', 'Enchant creature\nAt the beginning of the upkeep of enchanted creature\'s controller, Parasitic Bond deals 2 damage to that player.').
card_mana_cost('parasitic bond', ['3', 'B']).
card_cmc('parasitic bond', 4).
card_layout('parasitic bond', 'normal').

% Found in: NPH
card_name('parasitic implant', 'Parasitic Implant').
card_type('parasitic implant', 'Enchantment — Aura').
card_types('parasitic implant', ['Enchantment']).
card_subtypes('parasitic implant', ['Aura']).
card_colors('parasitic implant', ['B']).
card_text('parasitic implant', 'Enchant creature\nAt the beginning of your upkeep, enchanted creature\'s controller sacrifices it and you put a 1/1 colorless Myr artifact creature token onto the battlefield.').
card_mana_cost('parasitic implant', ['3', 'B']).
card_cmc('parasitic implant', 4).
card_layout('parasitic implant', 'normal').

% Found in: CON
card_name('parasitic strix', 'Parasitic Strix').
card_type('parasitic strix', 'Artifact Creature — Bird').
card_types('parasitic strix', ['Artifact', 'Creature']).
card_subtypes('parasitic strix', ['Bird']).
card_colors('parasitic strix', ['U']).
card_text('parasitic strix', 'Flying\nWhen Parasitic Strix enters the battlefield, if you control a black permanent, target player loses 2 life and you gain 2 life.').
card_mana_cost('parasitic strix', ['2', 'U']).
card_cmc('parasitic strix', 3).
card_layout('parasitic strix', 'normal').
card_power('parasitic strix', 2).
card_toughness('parasitic strix', 2).

% Found in: ULG
card_name('parch', 'Parch').
card_type('parch', 'Instant').
card_types('parch', ['Instant']).
card_subtypes('parch', []).
card_colors('parch', ['R']).
card_text('parch', 'Choose one —\n• Parch deals 2 damage to target creature or player.\n• Parch deals 4 damage to target blue creature.').
card_mana_cost('parch', ['1', 'R']).
card_cmc('parch', 2).
card_layout('parch', 'normal').

% Found in: TOR
card_name('pardic arsonist', 'Pardic Arsonist').
card_type('pardic arsonist', 'Creature — Human Barbarian').
card_types('pardic arsonist', ['Creature']).
card_subtypes('pardic arsonist', ['Human', 'Barbarian']).
card_colors('pardic arsonist', ['R']).
card_text('pardic arsonist', 'Threshold — As long as seven or more cards are in your graveyard, Pardic Arsonist has \"When Pardic Arsonist enters the battlefield, it deals 3 damage to target creature or player.\"').
card_mana_cost('pardic arsonist', ['2', 'R', 'R']).
card_cmc('pardic arsonist', 4).
card_layout('pardic arsonist', 'normal').
card_power('pardic arsonist', 3).
card_toughness('pardic arsonist', 3).

% Found in: TOR
card_name('pardic collaborator', 'Pardic Collaborator').
card_type('pardic collaborator', 'Creature — Human Barbarian').
card_types('pardic collaborator', ['Creature']).
card_subtypes('pardic collaborator', ['Human', 'Barbarian']).
card_colors('pardic collaborator', ['R']).
card_text('pardic collaborator', 'First strike\n{B}: Pardic Collaborator gets +1/+1 until end of turn.').
card_mana_cost('pardic collaborator', ['3', 'R']).
card_cmc('pardic collaborator', 4).
card_layout('pardic collaborator', 'normal').
card_power('pardic collaborator', 2).
card_toughness('pardic collaborator', 2).

% Found in: MMA, TSP
card_name('pardic dragon', 'Pardic Dragon').
card_type('pardic dragon', 'Creature — Dragon').
card_types('pardic dragon', ['Creature']).
card_subtypes('pardic dragon', ['Dragon']).
card_colors('pardic dragon', ['R']).
card_text('pardic dragon', 'Flying\n{R}: Pardic Dragon gets +1/+0 until end of turn.\nSuspend 2—{R}{R}\nWhenever an opponent casts a spell, if Pardic Dragon is suspended, that player may put a time counter on Pardic Dragon.').
card_mana_cost('pardic dragon', ['4', 'R', 'R']).
card_cmc('pardic dragon', 6).
card_layout('pardic dragon', 'normal').
card_power('pardic dragon', 4).
card_toughness('pardic dragon', 4).

% Found in: ODY
card_name('pardic firecat', 'Pardic Firecat').
card_type('pardic firecat', 'Creature — Elemental Cat').
card_types('pardic firecat', ['Creature']).
card_subtypes('pardic firecat', ['Elemental', 'Cat']).
card_colors('pardic firecat', ['R']).
card_text('pardic firecat', 'Haste\nIf Pardic Firecat is in a graveyard, effects from spells named Flame Burst count it as a card named Flame Burst.').
card_mana_cost('pardic firecat', ['3', 'R']).
card_cmc('pardic firecat', 4).
card_layout('pardic firecat', 'normal').
card_power('pardic firecat', 2).
card_toughness('pardic firecat', 3).

% Found in: TOR
card_name('pardic lancer', 'Pardic Lancer').
card_type('pardic lancer', 'Creature — Human Barbarian').
card_types('pardic lancer', ['Creature']).
card_subtypes('pardic lancer', ['Human', 'Barbarian']).
card_colors('pardic lancer', ['R']).
card_text('pardic lancer', 'Discard a card at random: Pardic Lancer gets +1/+0 and gains first strike until end of turn.').
card_mana_cost('pardic lancer', ['4', 'R']).
card_cmc('pardic lancer', 5).
card_layout('pardic lancer', 'normal').
card_power('pardic lancer', 3).
card_toughness('pardic lancer', 2).

% Found in: ODY
card_name('pardic miner', 'Pardic Miner').
card_type('pardic miner', 'Creature — Dwarf').
card_types('pardic miner', ['Creature']).
card_subtypes('pardic miner', ['Dwarf']).
card_colors('pardic miner', ['R']).
card_text('pardic miner', 'Sacrifice Pardic Miner: Target player can\'t play lands this turn.').
card_mana_cost('pardic miner', ['1', 'R']).
card_cmc('pardic miner', 2).
card_layout('pardic miner', 'normal').
card_power('pardic miner', 1).
card_toughness('pardic miner', 1).

% Found in: ODY
card_name('pardic swordsmith', 'Pardic Swordsmith').
card_type('pardic swordsmith', 'Creature — Dwarf').
card_types('pardic swordsmith', ['Creature']).
card_subtypes('pardic swordsmith', ['Dwarf']).
card_colors('pardic swordsmith', ['R']).
card_text('pardic swordsmith', '{R}, Discard a card at random: Pardic Swordsmith gets +2/+0 until end of turn.').
card_mana_cost('pardic swordsmith', ['2', 'R']).
card_cmc('pardic swordsmith', 3).
card_layout('pardic swordsmith', 'normal').
card_power('pardic swordsmith', 1).
card_toughness('pardic swordsmith', 1).

% Found in: 10E, 7ED, USG
card_name('pariah', 'Pariah').
card_type('pariah', 'Enchantment — Aura').
card_types('pariah', ['Enchantment']).
card_subtypes('pariah', ['Aura']).
card_colors('pariah', ['W']).
card_text('pariah', 'Enchant creature (Target a creature as you cast this. This card enters the battlefield attached to that creature.)\nAll damage that would be dealt to you is dealt to enchanted creature instead.').
card_mana_cost('pariah', ['2', 'W']).
card_cmc('pariah', 3).
card_layout('pariah', 'normal').

% Found in: RAV
card_name('pariah\'s shield', 'Pariah\'s Shield').
card_type('pariah\'s shield', 'Artifact — Equipment').
card_types('pariah\'s shield', ['Artifact']).
card_subtypes('pariah\'s shield', ['Equipment']).
card_colors('pariah\'s shield', []).
card_text('pariah\'s shield', 'All damage that would be dealt to you is dealt to equipped creature instead.\nEquip {3}').
card_mana_cost('pariah\'s shield', ['5']).
card_cmc('pariah\'s shield', 5).
card_layout('pariah\'s shield', 'normal').

% Found in: EXO
card_name('paroxysm', 'Paroxysm').
card_type('paroxysm', 'Enchantment — Aura').
card_types('paroxysm', ['Enchantment']).
card_subtypes('paroxysm', ['Aura']).
card_colors('paroxysm', ['R']).
card_text('paroxysm', 'Enchant creature\nAt the beginning of the upkeep of enchanted creature\'s controller, that player reveals the top card of his or her library. If that card is a land card, destroy that creature. Otherwise, it gets +3/+3 until end of turn.').
card_mana_cost('paroxysm', ['1', 'R']).
card_cmc('paroxysm', 2).
card_layout('paroxysm', 'normal').

% Found in: CHK
card_name('part the veil', 'Part the Veil').
card_type('part the veil', 'Instant — Arcane').
card_types('part the veil', ['Instant']).
card_subtypes('part the veil', ['Arcane']).
card_colors('part the veil', ['U']).
card_text('part the veil', 'Return all creatures you control to their owner\'s hand.').
card_mana_cost('part the veil', ['3', 'U']).
card_cmc('part the veil', 4).
card_layout('part the veil', 'normal').

% Found in: BFZ
card_name('part the waterveil', 'Part the Waterveil').
card_type('part the waterveil', 'Sorcery').
card_types('part the waterveil', ['Sorcery']).
card_subtypes('part the waterveil', []).
card_colors('part the waterveil', ['U']).
card_text('part the waterveil', 'Take an extra turn after this one. Exile Part the Waterveil.\nAwaken 6—{6}{U}{U}{U} (If you cast this spell for {6}{U}{U}{U}, also put six +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_mana_cost('part the waterveil', ['4', 'U', 'U']).
card_cmc('part the waterveil', 6).
card_layout('part the waterveil', 'normal').

% Found in: LEG
card_name('part water', 'Part Water').
card_type('part water', 'Sorcery').
card_types('part water', ['Sorcery']).
card_subtypes('part water', []).
card_colors('part water', ['U']).
card_text('part water', 'X target creatures gain islandwalk until end of turn. (They can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('part water', ['X', 'X', 'U']).
card_cmc('part water', 1).
card_layout('part water', 'normal').

% Found in: ISD
card_name('past in flames', 'Past in Flames').
card_type('past in flames', 'Sorcery').
card_types('past in flames', ['Sorcery']).
card_subtypes('past in flames', []).
card_colors('past in flames', ['R']).
card_text('past in flames', 'Each instant and sorcery card in your graveyard gains flashback until end of turn. The flashback cost is equal to its mana cost.\nFlashback {4}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('past in flames', ['3', 'R']).
card_cmc('past in flames', 4).
card_layout('past in flames', 'normal').

% Found in: 6ED, 7ED, 8ED, MIR
card_name('patagia golem', 'Patagia Golem').
card_type('patagia golem', 'Artifact Creature — Golem').
card_types('patagia golem', ['Artifact', 'Creature']).
card_subtypes('patagia golem', ['Golem']).
card_colors('patagia golem', []).
card_text('patagia golem', '{3}: Patagia Golem gains flying until end of turn.').
card_mana_cost('patagia golem', ['4']).
card_cmc('patagia golem', 4).
card_layout('patagia golem', 'normal').
card_power('patagia golem', 2).
card_toughness('patagia golem', 3).

% Found in: DIS
card_name('patagia viper', 'Patagia Viper').
card_type('patagia viper', 'Creature — Snake').
card_types('patagia viper', ['Creature']).
card_subtypes('patagia viper', ['Snake']).
card_colors('patagia viper', ['G']).
card_text('patagia viper', 'Flying\nWhen Patagia Viper enters the battlefield, put two 1/1 green and blue Snake creature tokens onto the battlefield.\nWhen Patagia Viper enters the battlefield, sacrifice it unless {U} was spent to cast it.').
card_mana_cost('patagia viper', ['3', 'G']).
card_cmc('patagia viper', 4).
card_layout('patagia viper', 'normal').
card_power('patagia viper', 2).
card_toughness('patagia viper', 1).

% Found in: ODY, TMP, TPR
card_name('patchwork gnomes', 'Patchwork Gnomes').
card_type('patchwork gnomes', 'Artifact Creature — Gnome').
card_types('patchwork gnomes', ['Artifact', 'Creature']).
card_subtypes('patchwork gnomes', ['Gnome']).
card_colors('patchwork gnomes', []).
card_text('patchwork gnomes', 'Discard a card: Regenerate Patchwork Gnomes.').
card_mana_cost('patchwork gnomes', ['3']).
card_cmc('patchwork gnomes', 3).
card_layout('patchwork gnomes', 'normal').
card_power('patchwork gnomes', 2).
card_toughness('patchwork gnomes', 1).

% Found in: SOK
card_name('path of anger\'s flame', 'Path of Anger\'s Flame').
card_type('path of anger\'s flame', 'Instant — Arcane').
card_types('path of anger\'s flame', ['Instant']).
card_subtypes('path of anger\'s flame', ['Arcane']).
card_colors('path of anger\'s flame', ['R']).
card_text('path of anger\'s flame', 'Creatures you control get +2/+0 until end of turn.').
card_mana_cost('path of anger\'s flame', ['2', 'R']).
card_cmc('path of anger\'s flame', 3).
card_layout('path of anger\'s flame', 'normal').

% Found in: M14
card_name('path of bravery', 'Path of Bravery').
card_type('path of bravery', 'Enchantment').
card_types('path of bravery', ['Enchantment']).
card_subtypes('path of bravery', []).
card_colors('path of bravery', ['W']).
card_text('path of bravery', 'As long as your life total is greater than or equal to your starting life total, creatures you control get +1/+1.\nWhenever one or more creatures you control attack, you gain life equal to the number of attacking creatures.').
card_mana_cost('path of bravery', ['2', 'W']).
card_cmc('path of bravery', 3).
card_layout('path of bravery', 'normal').

% Found in: PO2, POR, S99, USG
card_name('path of peace', 'Path of Peace').
card_type('path of peace', 'Sorcery').
card_types('path of peace', ['Sorcery']).
card_subtypes('path of peace', []).
card_colors('path of peace', ['W']).
card_text('path of peace', 'Destroy target creature. Its owner gains 4 life.').
card_mana_cost('path of peace', ['3', 'W']).
card_cmc('path of peace', 4).
card_layout('path of peace', 'normal').

% Found in: ARC, CMD, CON, DDI, MD1, MMA, pWPN
card_name('path to exile', 'Path to Exile').
card_type('path to exile', 'Instant').
card_types('path to exile', ['Instant']).
card_subtypes('path to exile', []).
card_colors('path to exile', ['W']).
card_text('path to exile', 'Exile target creature. Its controller may search his or her library for a basic land card, put that card onto the battlefield tapped, then shuffle his or her library.').
card_mana_cost('path to exile', ['W']).
card_cmc('path to exile', 1).
card_layout('path to exile', 'normal').

% Found in: AVR
card_name('pathbreaker wurm', 'Pathbreaker Wurm').
card_type('pathbreaker wurm', 'Creature — Wurm').
card_types('pathbreaker wurm', ['Creature']).
card_subtypes('pathbreaker wurm', ['Wurm']).
card_colors('pathbreaker wurm', ['G']).
card_text('pathbreaker wurm', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Pathbreaker Wurm is paired with another creature, both creatures have trample.').
card_mana_cost('pathbreaker wurm', ['4', 'G', 'G']).
card_cmc('pathbreaker wurm', 6).
card_layout('pathbreaker wurm', 'normal').
card_power('pathbreaker wurm', 6).
card_toughness('pathbreaker wurm', 4).

% Found in: ROE, pWPN
card_name('pathrazer of ulamog', 'Pathrazer of Ulamog').
card_type('pathrazer of ulamog', 'Creature — Eldrazi').
card_types('pathrazer of ulamog', ['Creature']).
card_subtypes('pathrazer of ulamog', ['Eldrazi']).
card_colors('pathrazer of ulamog', []).
card_text('pathrazer of ulamog', 'Annihilator 3 (Whenever this creature attacks, defending player sacrifices three permanents.)\nPathrazer of Ulamog can\'t be blocked except by three or more creatures.').
card_mana_cost('pathrazer of ulamog', ['11']).
card_cmc('pathrazer of ulamog', 11).
card_layout('pathrazer of ulamog', 'normal').
card_power('pathrazer of ulamog', 9).
card_toughness('pathrazer of ulamog', 9).

% Found in: BFZ
card_name('pathway arrows', 'Pathway Arrows').
card_type('pathway arrows', 'Artifact — Equipment').
card_types('pathway arrows', ['Artifact']).
card_subtypes('pathway arrows', ['Equipment']).
card_colors('pathway arrows', []).
card_text('pathway arrows', 'Equipped creature has \"{2}, {T}: This creature deals 1 damage to target creature. If a colorless creature is dealt damage this way, tap it.\"\nEquip {2}').
card_mana_cost('pathway arrows', ['1']).
card_cmc('pathway arrows', 1).
card_layout('pathway arrows', 'normal').

% Found in: ONS
card_name('patriarch\'s bidding', 'Patriarch\'s Bidding').
card_type('patriarch\'s bidding', 'Sorcery').
card_types('patriarch\'s bidding', ['Sorcery']).
card_subtypes('patriarch\'s bidding', []).
card_colors('patriarch\'s bidding', ['B']).
card_text('patriarch\'s bidding', 'Each player chooses a creature type. Each player returns all creature cards of a type chosen this way from his or her graveyard to the battlefield.').
card_mana_cost('patriarch\'s bidding', ['3', 'B', 'B']).
card_cmc('patriarch\'s bidding', 5).
card_layout('patriarch\'s bidding', 'normal').

% Found in: ODY
card_name('patriarch\'s desire', 'Patriarch\'s Desire').
card_type('patriarch\'s desire', 'Enchantment — Aura').
card_types('patriarch\'s desire', ['Enchantment']).
card_subtypes('patriarch\'s desire', ['Aura']).
card_colors('patriarch\'s desire', ['B']).
card_text('patriarch\'s desire', 'Enchant creature\nEnchanted creature gets +2/-2.\nThreshold — Enchanted creature gets an additional +2/-2 as long as seven or more cards are in your graveyard.').
card_mana_cost('patriarch\'s desire', ['3', 'B']).
card_cmc('patriarch\'s desire', 4).
card_layout('patriarch\'s desire', 'normal').

% Found in: FUT
card_name('patrician\'s scorn', 'Patrician\'s Scorn').
card_type('patrician\'s scorn', 'Instant').
card_types('patrician\'s scorn', ['Instant']).
card_subtypes('patrician\'s scorn', []).
card_colors('patrician\'s scorn', ['W']).
card_text('patrician\'s scorn', 'If you\'ve cast another white spell this turn, you may cast Patrician\'s Scorn without paying its mana cost.\nDestroy all enchantments.').
card_mana_cost('patrician\'s scorn', ['3', 'W']).
card_cmc('patrician\'s scorn', 4).
card_layout('patrician\'s scorn', 'normal').

% Found in: ODY
card_name('patrol hound', 'Patrol Hound').
card_type('patrol hound', 'Creature — Hound').
card_types('patrol hound', ['Creature']).
card_subtypes('patrol hound', ['Hound']).
card_colors('patrol hound', ['W']).
card_text('patrol hound', 'Discard a card: Patrol Hound gains first strike until end of turn.').
card_mana_cost('patrol hound', ['1', 'W']).
card_cmc('patrol hound', 2).
card_layout('patrol hound', 'normal').
card_power('patrol hound', 2).
card_toughness('patrol hound', 2).

% Found in: EVE
card_name('patrol signaler', 'Patrol Signaler').
card_type('patrol signaler', 'Creature — Kithkin Soldier').
card_types('patrol signaler', ['Creature']).
card_subtypes('patrol signaler', ['Kithkin', 'Soldier']).
card_colors('patrol signaler', ['W']).
card_text('patrol signaler', '{1}{W}, {Q}: Put a 1/1 white Kithkin Soldier creature token onto the battlefield. ({Q} is the untap symbol.)').
card_mana_cost('patrol signaler', ['1', 'W']).
card_cmc('patrol signaler', 2).
card_layout('patrol signaler', 'normal').
card_power('patrol signaler', 1).
card_toughness('patrol signaler', 1).

% Found in: BOK
card_name('patron of the akki', 'Patron of the Akki').
card_type('patron of the akki', 'Legendary Creature — Spirit').
card_types('patron of the akki', ['Creature']).
card_subtypes('patron of the akki', ['Spirit']).
card_supertypes('patron of the akki', ['Legendary']).
card_colors('patron of the akki', ['R']).
card_text('patron of the akki', 'Goblin offering (You may cast this card any time you could cast an instant by sacrificing a Goblin and paying the difference in mana costs between this and the sacrificed Goblin. Mana cost includes color.)\nWhenever Patron of the Akki attacks, creatures you control get +2/+0 until end of turn.').
card_mana_cost('patron of the akki', ['4', 'R', 'R']).
card_cmc('patron of the akki', 6).
card_layout('patron of the akki', 'normal').
card_power('patron of the akki', 5).
card_toughness('patron of the akki', 5).

% Found in: BOK
card_name('patron of the kitsune', 'Patron of the Kitsune').
card_type('patron of the kitsune', 'Legendary Creature — Spirit').
card_types('patron of the kitsune', ['Creature']).
card_subtypes('patron of the kitsune', ['Spirit']).
card_supertypes('patron of the kitsune', ['Legendary']).
card_colors('patron of the kitsune', ['W']).
card_text('patron of the kitsune', 'Fox offering (You may cast this card any time you could cast an instant by sacrificing a Fox and paying the difference in mana costs between this and the sacrificed Fox. Mana cost includes color.)\nWhenever a creature attacks, you may gain 1 life.').
card_mana_cost('patron of the kitsune', ['4', 'W', 'W']).
card_cmc('patron of the kitsune', 6).
card_layout('patron of the kitsune', 'normal').
card_power('patron of the kitsune', 5).
card_toughness('patron of the kitsune', 6).

% Found in: BOK
card_name('patron of the moon', 'Patron of the Moon').
card_type('patron of the moon', 'Legendary Creature — Spirit').
card_types('patron of the moon', ['Creature']).
card_subtypes('patron of the moon', ['Spirit']).
card_supertypes('patron of the moon', ['Legendary']).
card_colors('patron of the moon', ['U']).
card_text('patron of the moon', 'Moonfolk offering (You may cast this card any time you could cast an instant by sacrificing a Moonfolk and paying the difference in mana costs between this and the sacrificed Moonfolk. Mana cost includes color.)\nFlying\n{1}: Put up to two land cards from your hand onto the battlefield tapped.').
card_mana_cost('patron of the moon', ['5', 'U', 'U']).
card_cmc('patron of the moon', 7).
card_layout('patron of the moon', 'normal').
card_power('patron of the moon', 5).
card_toughness('patron of the moon', 4).

% Found in: BOK, CMD
card_name('patron of the nezumi', 'Patron of the Nezumi').
card_type('patron of the nezumi', 'Legendary Creature — Spirit').
card_types('patron of the nezumi', ['Creature']).
card_subtypes('patron of the nezumi', ['Spirit']).
card_supertypes('patron of the nezumi', ['Legendary']).
card_colors('patron of the nezumi', ['B']).
card_text('patron of the nezumi', 'Rat offering (You may cast this card any time you could cast an instant by sacrificing a Rat and paying the difference in mana costs between this and the sacrificed Rat. Mana cost includes color.)\nWhenever a permanent is put into an opponent\'s graveyard, that player loses 1 life.').
card_mana_cost('patron of the nezumi', ['5', 'B', 'B']).
card_cmc('patron of the nezumi', 7).
card_layout('patron of the nezumi', 'normal').
card_power('patron of the nezumi', 6).
card_toughness('patron of the nezumi', 6).

% Found in: BOK
card_name('patron of the orochi', 'Patron of the Orochi').
card_type('patron of the orochi', 'Legendary Creature — Spirit').
card_types('patron of the orochi', ['Creature']).
card_subtypes('patron of the orochi', ['Spirit']).
card_supertypes('patron of the orochi', ['Legendary']).
card_colors('patron of the orochi', ['G']).
card_text('patron of the orochi', 'Snake offering (You may cast this card any time you could cast an instant by sacrificing a Snake and paying the difference in mana costs between this and the sacrificed Snake. Mana cost includes color.)\n{T}: Untap all Forests and all green creatures. Activate this ability only once each turn.').
card_mana_cost('patron of the orochi', ['6', 'G', 'G']).
card_cmc('patron of the orochi', 8).
card_layout('patron of the orochi', 'normal').
card_power('patron of the orochi', 7).
card_toughness('patron of the orochi', 7).

% Found in: ORI
card_name('patron of the valiant', 'Patron of the Valiant').
card_type('patron of the valiant', 'Creature — Angel').
card_types('patron of the valiant', ['Creature']).
card_subtypes('patron of the valiant', ['Angel']).
card_colors('patron of the valiant', ['W']).
card_text('patron of the valiant', 'Flying\nWhen Patron of the Valiant enters the battlefield, put a +1/+1 counter on each creature you control with a +1/+1 counter on it.').
card_mana_cost('patron of the valiant', ['3', 'W', 'W']).
card_cmc('patron of the valiant', 5).
card_layout('patron of the valiant', 'normal').
card_power('patron of the valiant', 4).
card_toughness('patron of the valiant', 4).

% Found in: LGN
card_name('patron of the wild', 'Patron of the Wild').
card_type('patron of the wild', 'Creature — Elf').
card_types('patron of the wild', ['Creature']).
card_subtypes('patron of the wild', ['Elf']).
card_colors('patron of the wild', ['G']).
card_text('patron of the wild', 'Morph {2}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Patron of the Wild is turned face up, target creature gets +3/+3 until end of turn.').
card_mana_cost('patron of the wild', ['G']).
card_cmc('patron of the wild', 1).
card_layout('patron of the wild', 'normal').
card_power('patron of the wild', 1).
card_toughness('patron of the wild', 1).

% Found in: ODY
card_name('patron wizard', 'Patron Wizard').
card_type('patron wizard', 'Creature — Human Wizard').
card_types('patron wizard', ['Creature']).
card_subtypes('patron wizard', ['Human', 'Wizard']).
card_colors('patron wizard', ['U']).
card_text('patron wizard', 'Tap an untapped Wizard you control: Counter target spell unless its controller pays {1}.').
card_mana_cost('patron wizard', ['U', 'U', 'U']).
card_cmc('patron wizard', 3).
card_layout('patron wizard', 'normal').
card_power('patron wizard', 2).
card_toughness('patron wizard', 2).

% Found in: UDS
card_name('pattern of rebirth', 'Pattern of Rebirth').
card_type('pattern of rebirth', 'Enchantment — Aura').
card_types('pattern of rebirth', ['Enchantment']).
card_subtypes('pattern of rebirth', ['Aura']).
card_colors('pattern of rebirth', ['G']).
card_text('pattern of rebirth', 'Enchant creature\nWhen enchanted creature dies, that creature\'s controller may search his or her library for a creature card and put that card onto the battlefield. If that player does, he or she shuffles his or her library.').
card_mana_cost('pattern of rebirth', ['3', 'G']).
card_cmc('pattern of rebirth', 4).
card_layout('pattern of rebirth', 'normal').

% Found in: MIR
card_name('paupers\' cage', 'Paupers\' Cage').
card_type('paupers\' cage', 'Artifact').
card_types('paupers\' cage', ['Artifact']).
card_subtypes('paupers\' cage', []).
card_colors('paupers\' cage', []).
card_text('paupers\' cage', 'At the beginning of each opponent\'s upkeep, if that player has two or fewer cards in hand, Paupers\' Cage deals 2 damage to him or her.').
card_mana_cost('paupers\' cage', ['3']).
card_cmc('paupers\' cage', 3).
card_layout('paupers\' cage', 'normal').
card_reserved('paupers\' cage').

% Found in: LEG, ME3
card_name('pavel maliki', 'Pavel Maliki').
card_type('pavel maliki', 'Legendary Creature — Human').
card_types('pavel maliki', ['Creature']).
card_subtypes('pavel maliki', ['Human']).
card_supertypes('pavel maliki', ['Legendary']).
card_colors('pavel maliki', ['B', 'R']).
card_text('pavel maliki', '{B}{R}: Pavel Maliki gets +1/+0 until end of turn.').
card_mana_cost('pavel maliki', ['4', 'B', 'R']).
card_cmc('pavel maliki', 6).
card_layout('pavel maliki', 'normal').
card_power('pavel maliki', 5).
card_toughness('pavel maliki', 3).

% Found in: DDP, ROE
card_name('pawn of ulamog', 'Pawn of Ulamog').
card_type('pawn of ulamog', 'Creature — Vampire Shaman').
card_types('pawn of ulamog', ['Creature']).
card_subtypes('pawn of ulamog', ['Vampire', 'Shaman']).
card_colors('pawn of ulamog', ['B']).
card_text('pawn of ulamog', 'Whenever Pawn of Ulamog or another nontoken creature you control dies, you may put a 0/1 colorless Eldrazi Spawn creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('pawn of ulamog', ['1', 'B', 'B']).
card_cmc('pawn of ulamog', 3).
card_layout('pawn of ulamog', 'normal').
card_power('pawn of ulamog', 2).
card_toughness('pawn of ulamog', 2).

% Found in: DDL, M14, TOR
card_name('pay no heed', 'Pay No Heed').
card_type('pay no heed', 'Instant').
card_types('pay no heed', ['Instant']).
card_subtypes('pay no heed', []).
card_colors('pay no heed', ['W']).
card_text('pay no heed', 'Prevent all damage a source of your choice would deal this turn.').
card_mana_cost('pay no heed', ['W']).
card_cmc('pay no heed', 1).
card_layout('pay no heed', 'normal').

% Found in: ULG
card_name('peace and quiet', 'Peace and Quiet').
card_type('peace and quiet', 'Instant').
card_types('peace and quiet', ['Instant']).
card_subtypes('peace and quiet', []).
card_colors('peace and quiet', ['W']).
card_text('peace and quiet', 'Destroy two target enchantments.').
card_mana_cost('peace and quiet', ['1', 'W']).
card_cmc('peace and quiet', 2).
card_layout('peace and quiet', 'normal').

% Found in: 9ED, EXO
card_name('peace of mind', 'Peace of Mind').
card_type('peace of mind', 'Enchantment').
card_types('peace of mind', ['Enchantment']).
card_subtypes('peace of mind', []).
card_colors('peace of mind', ['W']).
card_text('peace of mind', '{W}, Discard a card: You gain 3 life.').
card_mana_cost('peace of mind', ['1', 'W']).
card_cmc('peace of mind', 2).
card_layout('peace of mind', 'normal').

% Found in: CNS, MBS
card_name('peace strider', 'Peace Strider').
card_type('peace strider', 'Artifact Creature — Construct').
card_types('peace strider', ['Artifact', 'Creature']).
card_subtypes('peace strider', ['Construct']).
card_colors('peace strider', []).
card_text('peace strider', 'When Peace Strider enters the battlefield, you gain 3 life.').
card_mana_cost('peace strider', ['4']).
card_cmc('peace strider', 4).
card_layout('peace strider', 'normal').
card_power('peace strider', 3).
card_toughness('peace strider', 3).

% Found in: MGB, VIS
card_name('peace talks', 'Peace Talks').
card_type('peace talks', 'Sorcery').
card_types('peace talks', ['Sorcery']).
card_subtypes('peace talks', []).
card_colors('peace talks', ['W']).
card_text('peace talks', 'This turn and next turn, creatures can\'t attack, and players and permanents can\'t be the targets of spells or activated abilities.').
card_mana_cost('peace talks', ['1', 'W']).
card_cmc('peace talks', 2).
card_layout('peace talks', 'normal').

% Found in: WTH
card_name('peacekeeper', 'Peacekeeper').
card_type('peacekeeper', 'Creature — Human').
card_types('peacekeeper', ['Creature']).
card_subtypes('peacekeeper', ['Human']).
card_colors('peacekeeper', ['W']).
card_text('peacekeeper', 'At the beginning of your upkeep, sacrifice Peacekeeper unless you pay {1}{W}.\nCreatures can\'t attack.').
card_mana_cost('peacekeeper', ['2', 'W']).
card_cmc('peacekeeper', 3).
card_layout('peacekeeper', 'normal').
card_power('peacekeeper', 1).
card_toughness('peacekeeper', 1).
card_reserved('peacekeeper').

% Found in: VAN
card_name('peacekeeper avatar', 'Peacekeeper Avatar').
card_type('peacekeeper avatar', 'Vanguard').
card_types('peacekeeper avatar', ['Vanguard']).
card_subtypes('peacekeeper avatar', []).
card_colors('peacekeeper avatar', []).
card_text('peacekeeper avatar', '{3}: For each opponent who controls a creature, put a token onto the battlefield that\'s a copy of a card named Arrest and attach it to a creature that player controls chosen at random.').
card_layout('peacekeeper avatar', 'vanguard').

% Found in: 8ED, ME3, PTK
card_name('peach garden oath', 'Peach Garden Oath').
card_type('peach garden oath', 'Sorcery').
card_types('peach garden oath', ['Sorcery']).
card_subtypes('peach garden oath', []).
card_colors('peach garden oath', ['W']).
card_text('peach garden oath', 'You gain 2 life for each creature you control.').
card_mana_cost('peach garden oath', ['W']).
card_cmc('peach garden oath', 1).
card_layout('peach garden oath', 'normal').

% Found in: THS
card_name('peak eruption', 'Peak Eruption').
card_type('peak eruption', 'Sorcery').
card_types('peak eruption', ['Sorcery']).
card_subtypes('peak eruption', []).
card_colors('peak eruption', ['R']).
card_text('peak eruption', 'Destroy target Mountain. Peak Eruption deals 3 damage to that land\'s controller.').
card_mana_cost('peak eruption', ['2', 'R']).
card_cmc('peak eruption', 3).
card_layout('peak eruption', 'normal').

% Found in: 6ED, MIR
card_name('pearl dragon', 'Pearl Dragon').
card_type('pearl dragon', 'Creature — Dragon').
card_types('pearl dragon', ['Creature']).
card_subtypes('pearl dragon', ['Dragon']).
card_colors('pearl dragon', ['W']).
card_text('pearl dragon', 'Flying\n{1}{W}: Pearl Dragon gets +0/+1 until end of turn.').
card_mana_cost('pearl dragon', ['4', 'W', 'W']).
card_cmc('pearl dragon', 6).
card_layout('pearl dragon', 'normal').
card_power('pearl dragon', 4).
card_toughness('pearl dragon', 4).

% Found in: KTK
card_name('pearl lake ancient', 'Pearl Lake Ancient').
card_type('pearl lake ancient', 'Creature — Leviathan').
card_types('pearl lake ancient', ['Creature']).
card_subtypes('pearl lake ancient', ['Leviathan']).
card_colors('pearl lake ancient', ['U']).
card_text('pearl lake ancient', 'Flash\nPearl Lake Ancient can\'t be countered.\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nReturn three lands you control to their owner\'s hand: Return Pearl Lake Ancient to its owner\'s hand.').
card_mana_cost('pearl lake ancient', ['5', 'U', 'U']).
card_cmc('pearl lake ancient', 7).
card_layout('pearl lake ancient', 'normal').
card_power('pearl lake ancient', 6).
card_toughness('pearl lake ancient', 7).

% Found in: C14, TMP
card_name('pearl medallion', 'Pearl Medallion').
card_type('pearl medallion', 'Artifact').
card_types('pearl medallion', ['Artifact']).
card_subtypes('pearl medallion', []).
card_colors('pearl medallion', []).
card_text('pearl medallion', 'White spells you cast cost {1} less to cast.').
card_mana_cost('pearl medallion', ['2']).
card_cmc('pearl medallion', 2).
card_layout('pearl medallion', 'normal').

% Found in: MRD
card_name('pearl shard', 'Pearl Shard').
card_type('pearl shard', 'Artifact').
card_types('pearl shard', ['Artifact']).
card_subtypes('pearl shard', []).
card_colors('pearl shard', []).
card_text('pearl shard', '{3}, {T} or {W}, {T}: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_mana_cost('pearl shard', ['3']).
card_cmc('pearl shard', 3).
card_layout('pearl shard', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, ITP, LEA, LEB, RQS
card_name('pearled unicorn', 'Pearled Unicorn').
card_type('pearled unicorn', 'Creature — Unicorn').
card_types('pearled unicorn', ['Creature']).
card_subtypes('pearled unicorn', ['Unicorn']).
card_colors('pearled unicorn', ['W']).
card_text('pearled unicorn', '').
card_mana_cost('pearled unicorn', ['2', 'W']).
card_cmc('pearled unicorn', 3).
card_layout('pearled unicorn', 'normal').
card_power('pearled unicorn', 2).
card_toughness('pearled unicorn', 2).

% Found in: ONS
card_name('pearlspear courier', 'Pearlspear Courier').
card_type('pearlspear courier', 'Creature — Human Soldier').
card_types('pearlspear courier', ['Creature']).
card_subtypes('pearlspear courier', ['Human', 'Soldier']).
card_colors('pearlspear courier', ['W']).
card_text('pearlspear courier', 'You may choose not to untap Pearlspear Courier during your untap step.\n{2}{W}, {T}: Target Soldier creature gets +2/+2 and has vigilance for as long as Pearlspear Courier remains tapped.').
card_mana_cost('pearlspear courier', ['2', 'W']).
card_cmc('pearlspear courier', 3).
card_layout('pearlspear courier', 'normal').
card_power('pearlspear courier', 2).
card_toughness('pearlspear courier', 1).

% Found in: MMQ
card_name('peat bog', 'Peat Bog').
card_type('peat bog', 'Land').
card_types('peat bog', ['Land']).
card_subtypes('peat bog', []).
card_colors('peat bog', []).
card_text('peat bog', 'Peat Bog enters the battlefield tapped with two depletion counters on it.\n{T}, Remove a depletion counter from Peat Bog: Add {B}{B} to your mana pool. If there are no depletion counters on Peat Bog, sacrifice it.').
card_layout('peat bog', 'normal').

% Found in: ODY
card_name('pedantic learning', 'Pedantic Learning').
card_type('pedantic learning', 'Enchantment').
card_types('pedantic learning', ['Enchantment']).
card_subtypes('pedantic learning', []).
card_colors('pedantic learning', ['U']).
card_text('pedantic learning', 'Whenever a land card is put into your graveyard from your library, you may pay {1}. If you do, draw a card.').
card_mana_cost('pedantic learning', ['U', 'U']).
card_cmc('pedantic learning', 2).
card_layout('pedantic learning', 'normal').

% Found in: 10E, ODY
card_name('peek', 'Peek').
card_type('peek', 'Instant').
card_types('peek', ['Instant']).
card_subtypes('peek', []).
card_colors('peek', ['U']).
card_text('peek', 'Look at target player\'s hand.\nDraw a card.').
card_mana_cost('peek', ['U']).
card_cmc('peek', 1).
card_layout('peek', 'normal').

% Found in: AVR, DDO, M15, RAV
card_name('peel from reality', 'Peel from Reality').
card_type('peel from reality', 'Instant').
card_types('peel from reality', ['Instant']).
card_subtypes('peel from reality', []).
card_colors('peel from reality', ['U']).
card_text('peel from reality', 'Return target creature you control and target creature you don\'t control to their owners\' hands.').
card_mana_cost('peel from reality', ['1', 'U']).
card_cmc('peel from reality', 2).
card_layout('peel from reality', 'normal').

% Found in: ONS
card_name('peer pressure', 'Peer Pressure').
card_type('peer pressure', 'Sorcery').
card_types('peer pressure', ['Sorcery']).
card_subtypes('peer pressure', []).
card_colors('peer pressure', ['U']).
card_text('peer pressure', 'Choose a creature type. If you control more creatures of that type than each other player, you gain control of all creatures of that type. (This effect lasts indefinitely.)').
card_mana_cost('peer pressure', ['3', 'U']).
card_cmc('peer pressure', 4).
card_layout('peer pressure', 'normal').

% Found in: CHK, MMA
card_name('peer through depths', 'Peer Through Depths').
card_type('peer through depths', 'Instant — Arcane').
card_types('peer through depths', ['Instant']).
card_subtypes('peer through depths', ['Arcane']).
card_colors('peer through depths', ['U']).
card_text('peer through depths', 'Look at the top five cards of your library. You may reveal an instant or sorcery card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_mana_cost('peer through depths', ['1', 'U']).
card_cmc('peer through depths', 2).
card_layout('peer through depths', 'normal').

% Found in: 9ED, ATH, USG
card_name('pegasus charger', 'Pegasus Charger').
card_type('pegasus charger', 'Creature — Pegasus').
card_types('pegasus charger', ['Creature']).
card_subtypes('pegasus charger', ['Pegasus']).
card_colors('pegasus charger', ['W']).
card_text('pegasus charger', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_mana_cost('pegasus charger', ['2', 'W']).
card_cmc('pegasus charger', 3).
card_layout('pegasus charger', 'normal').
card_power('pegasus charger', 2).
card_toughness('pegasus charger', 1).

% Found in: TMP
card_name('pegasus refuge', 'Pegasus Refuge').
card_type('pegasus refuge', 'Enchantment').
card_types('pegasus refuge', ['Enchantment']).
card_subtypes('pegasus refuge', []).
card_colors('pegasus refuge', ['W']).
card_text('pegasus refuge', '{2}, Discard a card: Put a 1/1 white Pegasus creature token with flying onto the battlefield.').
card_mana_cost('pegasus refuge', ['3', 'W']).
card_cmc('pegasus refuge', 4).
card_layout('pegasus refuge', 'normal').

% Found in: ATH, EXO, TPR
card_name('pegasus stampede', 'Pegasus Stampede').
card_type('pegasus stampede', 'Sorcery').
card_types('pegasus stampede', ['Sorcery']).
card_subtypes('pegasus stampede', []).
card_colors('pegasus stampede', ['W']).
card_text('pegasus stampede', 'Buyback—Sacrifice a land. (You may sacrifice a land in addition to any other costs as you cast this spell. If you do, put this card into your hand as it resolves.)\nPut a 1/1 white Pegasus creature token with flying onto the battlefield.').
card_mana_cost('pegasus stampede', ['1', 'W']).
card_cmc('pegasus stampede', 2).
card_layout('pegasus stampede', 'normal').

% Found in: UGL
card_name('pegasus token card', 'Pegasus token card').
card_type('pegasus token card', '').
card_types('pegasus token card', []).
card_subtypes('pegasus token card', []).
card_colors('pegasus token card', []).
card_text('pegasus token card', '').
card_layout('pegasus token card', 'token').

% Found in: CNS, MM2, ROE
card_name('pelakka wurm', 'Pelakka Wurm').
card_type('pelakka wurm', 'Creature — Wurm').
card_types('pelakka wurm', ['Creature']).
card_subtypes('pelakka wurm', ['Wurm']).
card_colors('pelakka wurm', ['G']).
card_text('pelakka wurm', 'Trample\nWhen Pelakka Wurm enters the battlefield, you gain 7 life.\nWhen Pelakka Wurm dies, draw a card.').
card_mana_cost('pelakka wurm', ['4', 'G', 'G', 'G']).
card_cmc('pelakka wurm', 7).
card_layout('pelakka wurm', 'normal').
card_power('pelakka wurm', 7).
card_toughness('pelakka wurm', 7).

% Found in: SCG
card_name('pemmin\'s aura', 'Pemmin\'s Aura').
card_type('pemmin\'s aura', 'Enchantment — Aura').
card_types('pemmin\'s aura', ['Enchantment']).
card_subtypes('pemmin\'s aura', ['Aura']).
card_colors('pemmin\'s aura', ['U']).
card_text('pemmin\'s aura', 'Enchant creature\n{U}: Untap enchanted creature.\n{U}: Enchanted creature gains flying until end of turn.\n{U}: Enchanted creature gains shroud until end of turn. (It can\'t be the target of spells or abilities.)\n{1}: Enchanted creature gets +1/-1 or -1/+1 until end of turn.').
card_mana_cost('pemmin\'s aura', ['1', 'U', 'U']).
card_cmc('pemmin\'s aura', 3).
card_layout('pemmin\'s aura', 'normal').

% Found in: EXO
card_name('penance', 'Penance').
card_type('penance', 'Enchantment').
card_types('penance', ['Enchantment']).
card_subtypes('penance', []).
card_colors('penance', ['W']).
card_text('penance', 'Put a card from your hand on top of your library: The next time a black or red source of your choice would deal damage this turn, prevent that damage.').
card_mana_cost('penance', ['2', 'W']).
card_cmc('penance', 3).
card_layout('penance', 'normal').

% Found in: ATH, LEG, TSB, pFNM
card_name('pendelhaven', 'Pendelhaven').
card_type('pendelhaven', 'Legendary Land').
card_types('pendelhaven', ['Land']).
card_subtypes('pendelhaven', []).
card_supertypes('pendelhaven', ['Legendary']).
card_colors('pendelhaven', []).
card_text('pendelhaven', '{T}: Add {G} to your mana pool.\n{T}: Target 1/1 creature gets +1/+2 until end of turn.').
card_layout('pendelhaven', 'normal').

% Found in: TSP
card_name('pendelhaven elder', 'Pendelhaven Elder').
card_type('pendelhaven elder', 'Creature — Elf Shaman').
card_types('pendelhaven elder', ['Creature']).
card_subtypes('pendelhaven elder', ['Elf', 'Shaman']).
card_colors('pendelhaven elder', ['G']).
card_text('pendelhaven elder', '{T}: Each 1/1 creature you control gets +1/+2 until end of turn.').
card_mana_cost('pendelhaven elder', ['1', 'G']).
card_cmc('pendelhaven elder', 2).
card_layout('pendelhaven elder', 'normal').
card_power('pendelhaven elder', 1).
card_toughness('pendelhaven elder', 1).

% Found in: USG
card_name('pendrell drake', 'Pendrell Drake').
card_type('pendrell drake', 'Creature — Drake').
card_types('pendrell drake', ['Creature']).
card_subtypes('pendrell drake', ['Drake']).
card_colors('pendrell drake', ['U']).
card_text('pendrell drake', 'Flying\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('pendrell drake', ['3', 'U']).
card_cmc('pendrell drake', 4).
card_layout('pendrell drake', 'normal').
card_power('pendrell drake', 2).
card_toughness('pendrell drake', 3).

% Found in: USG
card_name('pendrell flux', 'Pendrell Flux').
card_type('pendrell flux', 'Enchantment — Aura').
card_types('pendrell flux', ['Enchantment']).
card_subtypes('pendrell flux', ['Aura']).
card_colors('pendrell flux', ['U']).
card_text('pendrell flux', 'Enchant creature\nEnchanted creature has \"At the beginning of your upkeep, sacrifice this creature unless you pay its mana cost.\"').
card_mana_cost('pendrell flux', ['1', 'U']).
card_cmc('pendrell flux', 2).
card_layout('pendrell flux', 'normal').

% Found in: WTH
card_name('pendrell mists', 'Pendrell Mists').
card_type('pendrell mists', 'Enchantment').
card_types('pendrell mists', ['Enchantment']).
card_subtypes('pendrell mists', []).
card_colors('pendrell mists', ['U']).
card_text('pendrell mists', 'All creatures have \"At the beginning of your upkeep, sacrifice this creature unless you pay {1}.\"').
card_mana_cost('pendrell mists', ['3', 'U']).
card_cmc('pendrell mists', 4).
card_layout('pendrell mists', 'normal').
card_reserved('pendrell mists').

% Found in: ROE
card_name('pennon blade', 'Pennon Blade').
card_type('pennon blade', 'Artifact — Equipment').
card_types('pennon blade', ['Artifact']).
card_subtypes('pennon blade', ['Equipment']).
card_colors('pennon blade', []).
card_text('pennon blade', 'Equipped creature gets +1/+1 for each creature you control.\nEquip {4}').
card_mana_cost('pennon blade', ['3']).
card_cmc('pennon blade', 3).
card_layout('pennon blade', 'normal').

% Found in: JOU
card_name('pensive minotaur', 'Pensive Minotaur').
card_type('pensive minotaur', 'Creature — Minotaur Warrior').
card_types('pensive minotaur', ['Creature']).
card_subtypes('pensive minotaur', ['Minotaur', 'Warrior']).
card_colors('pensive minotaur', ['R']).
card_text('pensive minotaur', '').
card_mana_cost('pensive minotaur', ['2', 'R']).
card_cmc('pensive minotaur', 3).
card_layout('pensive minotaur', 'normal').
card_power('pensive minotaur', 2).
card_toughness('pensive minotaur', 3).

% Found in: 5DN, HOP
card_name('pentad prism', 'Pentad Prism').
card_type('pentad prism', 'Artifact').
card_types('pentad prism', ['Artifact']).
card_subtypes('pentad prism', []).
card_colors('pentad prism', []).
card_text('pentad prism', 'Sunburst (This enters the battlefield with a charge counter on it for each color of mana spent to cast it.)\nRemove a charge counter from Pentad Prism: Add one mana of any color to your mana pool.').
card_mana_cost('pentad prism', ['2']).
card_cmc('pentad prism', 2).
card_layout('pentad prism', 'normal').

% Found in: 5ED, 6ED, ICE, ME4
card_name('pentagram of the ages', 'Pentagram of the Ages').
card_type('pentagram of the ages', 'Artifact').
card_types('pentagram of the ages', ['Artifact']).
card_subtypes('pentagram of the ages', []).
card_colors('pentagram of the ages', []).
card_text('pentagram of the ages', '{4}, {T}: The next time a source of your choice would deal damage to you this turn, prevent that damage.').
card_mana_cost('pentagram of the ages', ['4']).
card_cmc('pentagram of the ages', 4).
card_layout('pentagram of the ages', 'normal').

% Found in: TSP
card_name('pentarch paladin', 'Pentarch Paladin').
card_type('pentarch paladin', 'Creature — Human Knight').
card_types('pentarch paladin', ['Creature']).
card_subtypes('pentarch paladin', ['Human', 'Knight']).
card_colors('pentarch paladin', ['W']).
card_text('pentarch paladin', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nAs Pentarch Paladin enters the battlefield, choose a color.\n{W}{W}, {T}: Destroy target permanent of the chosen color.').
card_mana_cost('pentarch paladin', ['2', 'W', 'W', 'W']).
card_cmc('pentarch paladin', 5).
card_layout('pentarch paladin', 'normal').
card_power('pentarch paladin', 3).
card_toughness('pentarch paladin', 3).

% Found in: TSP
card_name('pentarch ward', 'Pentarch Ward').
card_type('pentarch ward', 'Enchantment — Aura').
card_types('pentarch ward', ['Enchantment']).
card_subtypes('pentarch ward', ['Aura']).
card_colors('pentarch ward', ['W']).
card_text('pentarch ward', 'Enchant creature\nAs Pentarch Ward enters the battlefield, choose a color.\nWhen Pentarch Ward enters the battlefield, draw a card.\nEnchanted creature has protection from the chosen color. This effect doesn\'t remove Pentarch Ward.').
card_mana_cost('pentarch ward', ['2', 'W']).
card_cmc('pentarch ward', 3).
card_layout('pentarch ward', 'normal').

% Found in: C14, DDF, HOP, M12, MRD
card_name('pentavus', 'Pentavus').
card_type('pentavus', 'Artifact Creature — Construct').
card_types('pentavus', ['Artifact', 'Creature']).
card_subtypes('pentavus', ['Construct']).
card_colors('pentavus', []).
card_text('pentavus', 'Pentavus enters the battlefield with five +1/+1 counters on it.\n{1}, Remove a +1/+1 counter from Pentavus: Put a 1/1 colorless Pentavite artifact creature token with flying onto the battlefield.\n{1}, Sacrifice a Pentavite: Put a +1/+1 counter on Pentavus.').
card_mana_cost('pentavus', ['7']).
card_cmc('pentavus', 7).
card_layout('pentavus', 'normal').
card_power('pentavus', 0).
card_toughness('pentavus', 0).

% Found in: APC
card_name('penumbra bobcat', 'Penumbra Bobcat').
card_type('penumbra bobcat', 'Creature — Cat').
card_types('penumbra bobcat', ['Creature']).
card_subtypes('penumbra bobcat', ['Cat']).
card_colors('penumbra bobcat', ['G']).
card_text('penumbra bobcat', 'When Penumbra Bobcat dies, put a 2/1 black Cat creature token onto the battlefield.').
card_mana_cost('penumbra bobcat', ['2', 'G']).
card_cmc('penumbra bobcat', 3).
card_layout('penumbra bobcat', 'normal').
card_power('penumbra bobcat', 2).
card_toughness('penumbra bobcat', 1).

% Found in: APC
card_name('penumbra kavu', 'Penumbra Kavu').
card_type('penumbra kavu', 'Creature — Kavu').
card_types('penumbra kavu', ['Creature']).
card_subtypes('penumbra kavu', ['Kavu']).
card_colors('penumbra kavu', ['G']).
card_text('penumbra kavu', 'When Penumbra Kavu dies, put a 3/3 black Kavu creature token onto the battlefield.').
card_mana_cost('penumbra kavu', ['4', 'G']).
card_cmc('penumbra kavu', 5).
card_layout('penumbra kavu', 'normal').
card_power('penumbra kavu', 3).
card_toughness('penumbra kavu', 3).

% Found in: CMD, MMA, PC2, TSP
card_name('penumbra spider', 'Penumbra Spider').
card_type('penumbra spider', 'Creature — Spider').
card_types('penumbra spider', ['Creature']).
card_subtypes('penumbra spider', ['Spider']).
card_colors('penumbra spider', ['G']).
card_text('penumbra spider', 'Reach\nWhen Penumbra Spider dies, put a 2/4 black Spider creature token with reach onto the battlefield.').
card_mana_cost('penumbra spider', ['2', 'G', 'G']).
card_cmc('penumbra spider', 4).
card_layout('penumbra spider', 'normal').
card_power('penumbra spider', 2).
card_toughness('penumbra spider', 4).

% Found in: APC, VMA
card_name('penumbra wurm', 'Penumbra Wurm').
card_type('penumbra wurm', 'Creature — Wurm').
card_types('penumbra wurm', ['Creature']).
card_subtypes('penumbra wurm', ['Wurm']).
card_colors('penumbra wurm', ['G']).
card_text('penumbra wurm', 'Trample\nWhen Penumbra Wurm dies, put a 6/6 black Wurm creature token with trample onto the battlefield.').
card_mana_cost('penumbra wurm', ['5', 'G', 'G']).
card_cmc('penumbra wurm', 7).
card_layout('penumbra wurm', 'normal').
card_power('penumbra wurm', 6).
card_toughness('penumbra wurm', 6).

% Found in: DRK
card_name('people of the woods', 'People of the Woods').
card_type('people of the woods', 'Creature — Human').
card_types('people of the woods', ['Creature']).
card_subtypes('people of the woods', ['Human']).
card_colors('people of the woods', ['G']).
card_text('people of the woods', 'People of the Woods\'s toughness is equal to the number of Forests you control.').
card_mana_cost('people of the woods', ['G', 'G']).
card_cmc('people of the woods', 2).
card_layout('people of the woods', 'normal').
card_power('people of the woods', 1).
card_toughness('people of the woods', '*').

% Found in: LRW, MMA
card_name('peppersmoke', 'Peppersmoke').
card_type('peppersmoke', 'Tribal Instant — Faerie').
card_types('peppersmoke', ['Tribal', 'Instant']).
card_subtypes('peppersmoke', ['Faerie']).
card_colors('peppersmoke', ['B']).
card_text('peppersmoke', 'Target creature gets -1/-1 until end of turn. If you control a Faerie, draw a card.').
card_mana_cost('peppersmoke', ['B']).
card_cmc('peppersmoke', 1).
card_layout('peppersmoke', 'normal').

% Found in: BNG
card_name('peregrination', 'Peregrination').
card_type('peregrination', 'Sorcery').
card_types('peregrination', ['Sorcery']).
card_subtypes('peregrination', []).
card_colors('peregrination', ['G']).
card_text('peregrination', 'Search your library for up to two basic land cards, reveal those cards, and put one onto the battlefield tapped and the other into your hand. Shuffle your library, then scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('peregrination', ['3', 'G']).
card_cmc('peregrination', 4).
card_layout('peregrination', 'normal').

% Found in: PC2, USG
card_name('peregrine drake', 'Peregrine Drake').
card_type('peregrine drake', 'Creature — Drake').
card_types('peregrine drake', ['Creature']).
card_subtypes('peregrine drake', ['Drake']).
card_colors('peregrine drake', ['U']).
card_text('peregrine drake', 'Flying\nWhen Peregrine Drake enters the battlefield, untap up to five lands.').
card_mana_cost('peregrine drake', ['4', 'U']).
card_cmc('peregrine drake', 5).
card_layout('peregrine drake', 'normal').
card_power('peregrine drake', 2).
card_toughness('peregrine drake', 3).

% Found in: M12
card_name('peregrine griffin', 'Peregrine Griffin').
card_type('peregrine griffin', 'Creature — Griffin').
card_types('peregrine griffin', ['Creature']).
card_subtypes('peregrine griffin', ['Griffin']).
card_colors('peregrine griffin', ['W']).
card_text('peregrine griffin', 'Flying\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_mana_cost('peregrine griffin', ['4', 'W']).
card_cmc('peregrine griffin', 5).
card_layout('peregrine griffin', 'normal').
card_power('peregrine griffin', 2).
card_toughness('peregrine griffin', 4).

% Found in: RAV
card_name('peregrine mask', 'Peregrine Mask').
card_type('peregrine mask', 'Artifact — Equipment').
card_types('peregrine mask', ['Artifact']).
card_subtypes('peregrine mask', ['Equipment']).
card_colors('peregrine mask', []).
card_text('peregrine mask', 'Equipped creature has defender, flying, and first strike.\nEquip {2}').
card_mana_cost('peregrine mask', ['1']).
card_cmc('peregrine mask', 1).
card_layout('peregrine mask', 'normal').

% Found in: RAV
card_name('perilous forays', 'Perilous Forays').
card_type('perilous forays', 'Enchantment').
card_types('perilous forays', ['Enchantment']).
card_subtypes('perilous forays', []).
card_colors('perilous forays', ['G']).
card_text('perilous forays', '{1}, Sacrifice a creature: Search your library for a land card with a basic land type and put it onto the battlefield tapped. Then shuffle your library.').
card_mana_cost('perilous forays', ['3', 'G', 'G']).
card_cmc('perilous forays', 5).
card_layout('perilous forays', 'normal').

% Found in: SOM
card_name('perilous myr', 'Perilous Myr').
card_type('perilous myr', 'Artifact Creature — Myr').
card_types('perilous myr', ['Artifact', 'Creature']).
card_subtypes('perilous myr', ['Myr']).
card_colors('perilous myr', []).
card_text('perilous myr', 'When Perilous Myr dies, it deals 2 damage to target creature or player.').
card_mana_cost('perilous myr', ['2']).
card_cmc('perilous myr', 2).
card_layout('perilous myr', 'normal').
card_power('perilous myr', 1).
card_toughness('perilous myr', 1).

% Found in: CMD, CSP, MMA
card_name('perilous research', 'Perilous Research').
card_type('perilous research', 'Instant').
card_types('perilous research', ['Instant']).
card_subtypes('perilous research', []).
card_colors('perilous research', ['U']).
card_text('perilous research', 'Draw two cards, then sacrifice a permanent.').
card_mana_cost('perilous research', ['1', 'U']).
card_cmc('perilous research', 2).
card_layout('perilous research', 'normal').

% Found in: RTR
card_name('perilous shadow', 'Perilous Shadow').
card_type('perilous shadow', 'Creature — Insect Shade').
card_types('perilous shadow', ['Creature']).
card_subtypes('perilous shadow', ['Insect', 'Shade']).
card_colors('perilous shadow', ['B']).
card_text('perilous shadow', '{1}{B}: Perilous Shadow gets +2/+2 until end of turn.').
card_mana_cost('perilous shadow', ['2', 'B', 'B']).
card_cmc('perilous shadow', 4).
card_layout('perilous shadow', 'normal').
card_power('perilous shadow', 0).
card_toughness('perilous shadow', 4).

% Found in: M15
card_name('perilous vault', 'Perilous Vault').
card_type('perilous vault', 'Artifact').
card_types('perilous vault', ['Artifact']).
card_subtypes('perilous vault', []).
card_colors('perilous vault', []).
card_text('perilous vault', '{5}, {T}, Exile Perilous Vault: Exile all nonland permanents.').
card_mana_cost('perilous vault', ['4']).
card_cmc('perilous vault', 4).
card_layout('perilous vault', 'normal').

% Found in: WWK
card_name('perimeter captain', 'Perimeter Captain').
card_type('perimeter captain', 'Creature — Human Soldier').
card_types('perimeter captain', ['Creature']).
card_subtypes('perimeter captain', ['Human', 'Soldier']).
card_colors('perimeter captain', ['W']).
card_text('perimeter captain', 'Defender\nWhenever a creature you control with defender blocks, you may gain 2 life.').
card_mana_cost('perimeter captain', ['W']).
card_cmc('perimeter captain', 1).
card_layout('perimeter captain', 'normal').
card_power('perimeter captain', 0).
card_toughness('perimeter captain', 4).

% Found in: 6ED, TMP
card_name('perish', 'Perish').
card_type('perish', 'Sorcery').
card_types('perish', ['Sorcery']).
card_subtypes('perish', []).
card_colors('perish', ['B']).
card_text('perish', 'Destroy all green creatures. They can\'t be regenerated.').
card_mana_cost('perish', ['2', 'B']).
card_cmc('perish', 3).
card_layout('perish', 'normal').

% Found in: ROE
card_name('perish the thought', 'Perish the Thought').
card_type('perish the thought', 'Sorcery').
card_types('perish the thought', ['Sorcery']).
card_subtypes('perish the thought', []).
card_colors('perish the thought', ['B']).
card_text('perish the thought', 'Target opponent reveals his or her hand. You choose a card from it. That player shuffles that card into his or her library.').
card_mana_cost('perish the thought', ['2', 'B']).
card_cmc('perish the thought', 3).
card_layout('perish the thought', 'normal').

% Found in: WWK
card_name('permafrost trap', 'Permafrost Trap').
card_type('permafrost trap', 'Instant — Trap').
card_types('permafrost trap', ['Instant']).
card_subtypes('permafrost trap', ['Trap']).
card_colors('permafrost trap', ['U']).
card_text('permafrost trap', 'If an opponent had a green creature enter the battlefield under his or her control this turn, you may pay {U} rather than pay Permafrost Trap\'s mana cost.\nTap up to two target creatures. Those creatures don\'t untap during their controller\'s next untap step.').
card_mana_cost('permafrost trap', ['2', 'U', 'U']).
card_cmc('permafrost trap', 4).
card_layout('permafrost trap', 'normal').

% Found in: APC, CNS, pJGP
card_name('pernicious deed', 'Pernicious Deed').
card_type('pernicious deed', 'Enchantment').
card_types('pernicious deed', ['Enchantment']).
card_subtypes('pernicious deed', []).
card_colors('pernicious deed', ['B', 'G']).
card_text('pernicious deed', '{X}, Sacrifice Pernicious Deed: Destroy each artifact, creature, and enchantment with converted mana cost X or less.').
card_mana_cost('pernicious deed', ['1', 'B', 'G']).
card_cmc('pernicious deed', 3).
card_layout('pernicious deed', 'normal').

% Found in: RAV
card_name('perplex', 'Perplex').
card_type('perplex', 'Instant').
card_types('perplex', ['Instant']).
card_subtypes('perplex', []).
card_colors('perplex', ['U', 'B']).
card_text('perplex', 'Counter target spell unless its controller discards his or her hand.\nTransmute {1}{U}{B} ({1}{U}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Transmute only as a sorcery.)').
card_mana_cost('perplex', ['1', 'U', 'B']).
card_cmc('perplex', 3).
card_layout('perplex', 'normal').

% Found in: BNG
card_name('perplexing chimera', 'Perplexing Chimera').
card_type('perplexing chimera', 'Enchantment Creature — Chimera').
card_types('perplexing chimera', ['Enchantment', 'Creature']).
card_subtypes('perplexing chimera', ['Chimera']).
card_colors('perplexing chimera', ['U']).
card_text('perplexing chimera', 'Whenever an opponent casts a spell, you may exchange control of Perplexing Chimera and that spell. If you do, you may choose new targets for the spell. (If the spell becomes a permanent, you control that permanent.)').
card_mana_cost('perplexing chimera', ['4', 'U']).
card_cmc('perplexing chimera', 5).
card_layout('perplexing chimera', 'normal').
card_power('perplexing chimera', 3).
card_toughness('perplexing chimera', 3).

% Found in: 7ED, 8ED, 9ED, USG
card_name('persecute', 'Persecute').
card_type('persecute', 'Sorcery').
card_types('persecute', ['Sorcery']).
card_subtypes('persecute', []).
card_colors('persecute', ['B']).
card_text('persecute', 'Choose a color. Target player reveals his or her hand and discards all cards of that color.').
card_mana_cost('persecute', ['2', 'B', 'B']).
card_cmc('persecute', 4).
card_layout('persecute', 'normal').

% Found in: UNH
card_name('persecute artist', 'Persecute Artist').
card_type('persecute artist', 'Sorcery').
card_types('persecute artist', ['Sorcery']).
card_subtypes('persecute artist', []).
card_colors('persecute artist', ['B']).
card_text('persecute artist', 'Choose an artist other than Rebecca Guay. Target player reveals his or her hand and discards all nonland cards by the chosen artist.').
card_mana_cost('persecute artist', ['1', 'B', 'B']).
card_cmc('persecute artist', 3).
card_layout('persecute artist', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, ME4
card_name('personal incarnation', 'Personal Incarnation').
card_type('personal incarnation', 'Creature — Avatar Incarnation').
card_types('personal incarnation', ['Creature']).
card_subtypes('personal incarnation', ['Avatar', 'Incarnation']).
card_colors('personal incarnation', ['W']).
card_text('personal incarnation', '{0}: The next 1 damage that would be dealt to Personal Incarnation this turn is dealt to its owner instead. Any player may activate this ability, but only if he or she owns Personal Incarnation.\nWhen Personal Incarnation dies, its owner loses half his or her life, rounded up.').
card_mana_cost('personal incarnation', ['3', 'W', 'W', 'W']).
card_cmc('personal incarnation', 6).
card_layout('personal incarnation', 'normal').
card_power('personal incarnation', 6).
card_toughness('personal incarnation', 6).

% Found in: M12
card_name('personal sanctuary', 'Personal Sanctuary').
card_type('personal sanctuary', 'Enchantment').
card_types('personal sanctuary', ['Enchantment']).
card_subtypes('personal sanctuary', []).
card_colors('personal sanctuary', ['W']).
card_text('personal sanctuary', 'During your turn, prevent all damage that would be dealt to you.').
card_mana_cost('personal sanctuary', ['2', 'W']).
card_cmc('personal sanctuary', 3).
card_layout('personal sanctuary', 'normal').

% Found in: ME2, POR
card_name('personal tutor', 'Personal Tutor').
card_type('personal tutor', 'Sorcery').
card_types('personal tutor', ['Sorcery']).
card_subtypes('personal tutor', []).
card_colors('personal tutor', ['U']).
card_text('personal tutor', 'Search your library for a sorcery card and reveal that card. Shuffle your library, then put the card on top of it.').
card_mana_cost('personal tutor', ['U']).
card_cmc('personal tutor', 1).
card_layout('personal tutor', 'normal').

% Found in: 10E, ODY
card_name('persuasion', 'Persuasion').
card_type('persuasion', 'Enchantment — Aura').
card_types('persuasion', ['Enchantment']).
card_subtypes('persuasion', ['Aura']).
card_colors('persuasion', ['U']).
card_text('persuasion', 'Enchant creature (Target a creature as you cast this. This card enters the battlefield attached to that creature.)\nYou control enchanted creature.').
card_mana_cost('persuasion', ['3', 'U', 'U']).
card_cmc('persuasion', 5).
card_layout('persuasion', 'normal').

% Found in: LRW, MMA
card_name('pestermite', 'Pestermite').
card_type('pestermite', 'Creature — Faerie Rogue').
card_types('pestermite', ['Creature']).
card_subtypes('pestermite', ['Faerie', 'Rogue']).
card_colors('pestermite', ['U']).
card_text('pestermite', 'Flash\nFlying\nWhen Pestermite enters the battlefield, you may tap or untap target permanent.').
card_mana_cost('pestermite', ['2', 'U']).
card_cmc('pestermite', 3).
card_layout('pestermite', 'normal').
card_power('pestermite', 2).
card_toughness('pestermite', 1).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, BRB, CED, CEI, LEA, LEB, USG
card_name('pestilence', 'Pestilence').
card_type('pestilence', 'Enchantment').
card_types('pestilence', ['Enchantment']).
card_subtypes('pestilence', []).
card_colors('pestilence', ['B']).
card_text('pestilence', 'At the beginning of the end step, if no creatures are on the battlefield, sacrifice Pestilence.\n{B}: Pestilence deals 1 damage to each creature and each player.').
card_mana_cost('pestilence', ['2', 'B', 'B']).
card_cmc('pestilence', 4).
card_layout('pestilence', 'normal').

% Found in: C14, ROE
card_name('pestilence demon', 'Pestilence Demon').
card_type('pestilence demon', 'Creature — Demon').
card_types('pestilence demon', ['Creature']).
card_subtypes('pestilence demon', ['Demon']).
card_colors('pestilence demon', ['B']).
card_text('pestilence demon', 'Flying\n{B}: Pestilence Demon deals 1 damage to each creature and each player.').
card_mana_cost('pestilence demon', ['5', 'B', 'B', 'B']).
card_cmc('pestilence demon', 8).
card_layout('pestilence demon', 'normal').
card_power('pestilence demon', 7).
card_toughness('pestilence demon', 6).

% Found in: ICE
card_name('pestilence rats', 'Pestilence Rats').
card_type('pestilence rats', 'Creature — Rat').
card_types('pestilence rats', ['Creature']).
card_subtypes('pestilence rats', ['Rat']).
card_colors('pestilence rats', ['B']).
card_text('pestilence rats', 'Pestilence Rats\'s power is equal to the number of other Rats on the battlefield. (For example, as long as there are two other Rats on the battlefield, Pestilence Rats\'s power and toughness are 2/3.)').
card_mana_cost('pestilence rats', ['2', 'B']).
card_cmc('pestilence rats', 3).
card_layout('pestilence rats', 'normal').
card_power('pestilence rats', '*').
card_toughness('pestilence rats', 3).

% Found in: CON
card_name('pestilent kathari', 'Pestilent Kathari').
card_type('pestilent kathari', 'Creature — Bird Warrior').
card_types('pestilent kathari', ['Creature']).
card_subtypes('pestilent kathari', ['Bird', 'Warrior']).
card_colors('pestilent kathari', ['B']).
card_text('pestilent kathari', 'Flying\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\n{2}{R}: Pestilent Kathari gains first strike until end of turn.').
card_mana_cost('pestilent kathari', ['2', 'B']).
card_cmc('pestilent kathari', 3).
card_layout('pestilent kathari', 'normal').
card_power('pestilent kathari', 1).
card_toughness('pestilent kathari', 1).

% Found in: NPH
card_name('pestilent souleater', 'Pestilent Souleater').
card_type('pestilent souleater', 'Artifact Creature — Insect').
card_types('pestilent souleater', ['Artifact', 'Creature']).
card_subtypes('pestilent souleater', ['Insect']).
card_colors('pestilent souleater', []).
card_text('pestilent souleater', '{B/P}: Pestilent Souleater gains infect until end of turn. ({B/P} can be paid with either {B} or 2 life. A creature with infect deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('pestilent souleater', ['5']).
card_cmc('pestilent souleater', 5).
card_layout('pestilent souleater', 'normal').
card_power('pestilent souleater', 3).
card_toughness('pestilent souleater', 3).

% Found in: BOK
card_name('petalmane baku', 'Petalmane Baku').
card_type('petalmane baku', 'Creature — Spirit').
card_types('petalmane baku', ['Creature']).
card_subtypes('petalmane baku', ['Spirit']).
card_colors('petalmane baku', ['G']).
card_text('petalmane baku', 'Whenever you cast a Spirit or Arcane spell, you may put a ki counter on Petalmane Baku.\n{1}, Remove X ki counters from Petalmane Baku: Add X mana of any one color to your mana pool.').
card_mana_cost('petalmane baku', ['1', 'G']).
card_cmc('petalmane baku', 2).
card_layout('petalmane baku', 'normal').
card_power('petalmane baku', 1).
card_toughness('petalmane baku', 2).

% Found in: CHK, MMA
card_name('petals of insight', 'Petals of Insight').
card_type('petals of insight', 'Sorcery — Arcane').
card_types('petals of insight', ['Sorcery']).
card_subtypes('petals of insight', ['Arcane']).
card_colors('petals of insight', ['U']).
card_text('petals of insight', 'Look at the top three cards of your library. You may put those cards on the bottom of your library in any order. If you do, return Petals of Insight to its owner\'s hand. Otherwise, draw three cards.').
card_mana_cost('petals of insight', ['4', 'U']).
card_cmc('petals of insight', 5).
card_layout('petals of insight', 'normal').

% Found in: CHR, LEG, MED
card_name('petra sphinx', 'Petra Sphinx').
card_type('petra sphinx', 'Creature — Sphinx').
card_types('petra sphinx', ['Creature']).
card_subtypes('petra sphinx', ['Sphinx']).
card_colors('petra sphinx', ['W']).
card_text('petra sphinx', '{T}: Target player names a card, then reveals the top card of his or her library. If that card is the named card, that player puts it into his or her hand. If it isn\'t, the player puts it into his or her graveyard.').
card_mana_cost('petra sphinx', ['2', 'W', 'W', 'W']).
card_cmc('petra sphinx', 5).
card_layout('petra sphinx', 'normal').
card_power('petra sphinx', 3).
card_toughness('petra sphinx', 4).

% Found in: TOR
card_name('petradon', 'Petradon').
card_type('petradon', 'Creature — Nightmare Beast').
card_types('petradon', ['Creature']).
card_subtypes('petradon', ['Nightmare', 'Beast']).
card_colors('petradon', ['R']).
card_text('petradon', 'When Petradon enters the battlefield, exile two target lands.\nWhen Petradon leaves the battlefield, return the exiled cards to the battlefield under their owners\' control.\n{R}: Petradon gets +1/+0 until end of turn.').
card_mana_cost('petradon', ['6', 'R', 'R']).
card_cmc('petradon', 8).
card_layout('petradon', 'normal').
card_power('petradon', 5).
card_toughness('petradon', 6).

% Found in: GPT
card_name('petrahydrox', 'Petrahydrox').
card_type('petrahydrox', 'Creature — Weird').
card_types('petrahydrox', ['Creature']).
card_subtypes('petrahydrox', ['Weird']).
card_colors('petrahydrox', ['U', 'R']).
card_text('petrahydrox', '({U/R} can be paid with either {U} or {R}.)\nWhen Petrahydrox becomes the target of a spell or ability, return Petrahydrox to its owner\'s hand.').
card_mana_cost('petrahydrox', ['3', 'U/R']).
card_cmc('petrahydrox', 4).
card_layout('petrahydrox', 'normal').
card_power('petrahydrox', 3).
card_toughness('petrahydrox', 3).

% Found in: TOR
card_name('petravark', 'Petravark').
card_type('petravark', 'Creature — Nightmare Beast').
card_types('petravark', ['Creature']).
card_subtypes('petravark', ['Nightmare', 'Beast']).
card_colors('petravark', ['R']).
card_text('petravark', 'When Petravark enters the battlefield, exile target land.\nWhen Petravark leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_mana_cost('petravark', ['3', 'R']).
card_cmc('petravark', 4).
card_layout('petravark', 'normal').
card_power('petravark', 2).
card_toughness('petravark', 2).

% Found in: ODY
card_name('petrified field', 'Petrified Field').
card_type('petrified field', 'Land').
card_types('petrified field', ['Land']).
card_subtypes('petrified field', []).
card_colors('petrified field', []).
card_text('petrified field', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Petrified Field: Return target land card from your graveyard to your hand.').
card_layout('petrified field', 'normal').

% Found in: FUT
card_name('petrified plating', 'Petrified Plating').
card_type('petrified plating', 'Enchantment — Aura').
card_types('petrified plating', ['Enchantment']).
card_subtypes('petrified plating', ['Aura']).
card_colors('petrified plating', ['G']).
card_text('petrified plating', 'Enchant creature\nEnchanted creature gets +2/+2.\nSuspend 2—{G} (Rather than cast this card from your hand, you may pay {G} and exile it with two time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_mana_cost('petrified plating', ['2', 'G']).
card_cmc('petrified plating', 3).
card_layout('petrified plating', 'normal').

% Found in: GPT
card_name('petrified wood-kin', 'Petrified Wood-Kin').
card_type('petrified wood-kin', 'Creature — Elemental Warrior').
card_types('petrified wood-kin', ['Creature']).
card_subtypes('petrified wood-kin', ['Elemental', 'Warrior']).
card_colors('petrified wood-kin', ['G']).
card_text('petrified wood-kin', 'Petrified Wood-Kin can\'t be countered.\nBloodthirst X (This creature enters the battlefield with X +1/+1 counters on it, where X is the damage dealt to your opponents this turn.)\nProtection from instants').
card_mana_cost('petrified wood-kin', ['6', 'G']).
card_cmc('petrified wood-kin', 7).
card_layout('petrified wood-kin', 'normal').
card_power('petrified wood-kin', 3).
card_toughness('petrified wood-kin', 3).

% Found in: MRD
card_name('pewter golem', 'Pewter Golem').
card_type('pewter golem', 'Artifact Creature — Golem').
card_types('pewter golem', ['Artifact', 'Creature']).
card_subtypes('pewter golem', ['Golem']).
card_colors('pewter golem', []).
card_text('pewter golem', '{1}{B}: Regenerate Pewter Golem.').
card_mana_cost('pewter golem', ['5']).
card_cmc('pewter golem', 5).
card_layout('pewter golem', 'normal').
card_power('pewter golem', 4).
card_toughness('pewter golem', 2).

% Found in: 10E, CNS, LGN
card_name('phage the untouchable', 'Phage the Untouchable').
card_type('phage the untouchable', 'Legendary Creature — Avatar Minion').
card_types('phage the untouchable', ['Creature']).
card_subtypes('phage the untouchable', ['Avatar', 'Minion']).
card_supertypes('phage the untouchable', ['Legendary']).
card_colors('phage the untouchable', ['B']).
card_text('phage the untouchable', 'When Phage the Untouchable enters the battlefield, if you didn\'t cast it from your hand, you lose the game.\nWhenever Phage deals combat damage to a creature, destroy that creature. It can\'t be regenerated.\nWhenever Phage deals combat damage to a player, that player loses the game.').
card_mana_cost('phage the untouchable', ['3', 'B', 'B', 'B', 'B']).
card_cmc('phage the untouchable', 7).
card_layout('phage the untouchable', 'normal').
card_power('phage the untouchable', 4).
card_toughness('phage the untouchable', 4).

% Found in: VAN
card_name('phage the untouchable avatar', 'Phage the Untouchable Avatar').
card_type('phage the untouchable avatar', 'Vanguard').
card_types('phage the untouchable avatar', ['Vanguard']).
card_subtypes('phage the untouchable avatar', []).
card_colors('phage the untouchable avatar', []).
card_text('phage the untouchable avatar', 'Pay 1 life: Until end of turn, whenever a creature deals combat damage to you, destroy that creature.\nPay 1 life: Until end of turn, whenever target creature you control deals combat damage to a creature, destroy the damaged creature.').
card_layout('phage the untouchable avatar', 'vanguard').

% Found in: JOU
card_name('phalanx formation', 'Phalanx Formation').
card_type('phalanx formation', 'Instant').
card_types('phalanx formation', ['Instant']).
card_subtypes('phalanx formation', []).
card_colors('phalanx formation', ['W']).
card_text('phalanx formation', 'Strive — Phalanx Formation costs {1}{W} more to cast for each target beyond the first.\nAny number of target creatures each gain double strike until end of turn. (They deal both first-strike and regular combat damage.)').
card_mana_cost('phalanx formation', ['2', 'W']).
card_cmc('phalanx formation', 3).
card_layout('phalanx formation', 'normal').

% Found in: THS, pMGD
card_name('phalanx leader', 'Phalanx Leader').
card_type('phalanx leader', 'Creature — Human Soldier').
card_types('phalanx leader', ['Creature']).
card_subtypes('phalanx leader', ['Human', 'Soldier']).
card_colors('phalanx leader', ['W']).
card_text('phalanx leader', 'Heroic — Whenever you cast a spell that targets Phalanx Leader, put a +1/+1 counter on each creature you control.').
card_mana_cost('phalanx leader', ['W', 'W']).
card_cmc('phalanx leader', 2).
card_layout('phalanx leader', 'normal').
card_power('phalanx leader', 1).
card_toughness('phalanx leader', 1).

% Found in: PLC
card_name('phantasmagorian', 'Phantasmagorian').
card_type('phantasmagorian', 'Creature — Horror').
card_types('phantasmagorian', ['Creature']).
card_subtypes('phantasmagorian', ['Horror']).
card_colors('phantasmagorian', ['B']).
card_text('phantasmagorian', 'When you cast Phantasmagorian, any player may discard three cards. If a player does, counter Phantasmagorian.\nDiscard three cards: Return Phantasmagorian from your graveyard to your hand.').
card_mana_cost('phantasmagorian', ['5', 'B', 'B']).
card_cmc('phantasmagorian', 7).
card_layout('phantasmagorian', 'normal').
card_power('phantasmagorian', 6).
card_toughness('phantasmagorian', 6).

% Found in: ROE
card_name('phantasmal abomination', 'Phantasmal Abomination').
card_type('phantasmal abomination', 'Creature — Illusion').
card_types('phantasmal abomination', ['Creature']).
card_subtypes('phantasmal abomination', ['Illusion']).
card_colors('phantasmal abomination', ['U']).
card_text('phantasmal abomination', 'Defender\nWhen Phantasmal Abomination becomes the target of a spell or ability, sacrifice it.').
card_mana_cost('phantasmal abomination', ['1', 'U', 'U']).
card_cmc('phantasmal abomination', 3).
card_layout('phantasmal abomination', 'normal').
card_power('phantasmal abomination', 5).
card_toughness('phantasmal abomination', 5).

% Found in: DDM, M12
card_name('phantasmal bear', 'Phantasmal Bear').
card_type('phantasmal bear', 'Creature — Bear Illusion').
card_types('phantasmal bear', ['Creature']).
card_subtypes('phantasmal bear', ['Bear', 'Illusion']).
card_colors('phantasmal bear', ['U']).
card_text('phantasmal bear', 'When Phantasmal Bear becomes the target of a spell or ability, sacrifice it.').
card_mana_cost('phantasmal bear', ['U']).
card_cmc('phantasmal bear', 1).
card_layout('phantasmal bear', 'normal').
card_power('phantasmal bear', 2).
card_toughness('phantasmal bear', 2).

% Found in: DDM, M12
card_name('phantasmal dragon', 'Phantasmal Dragon').
card_type('phantasmal dragon', 'Creature — Dragon Illusion').
card_types('phantasmal dragon', ['Creature']).
card_subtypes('phantasmal dragon', ['Dragon', 'Illusion']).
card_colors('phantasmal dragon', ['U']).
card_text('phantasmal dragon', 'Flying\nWhen Phantasmal Dragon becomes the target of a spell or ability, sacrifice it.').
card_mana_cost('phantasmal dragon', ['2', 'U', 'U']).
card_cmc('phantasmal dragon', 4).
card_layout('phantasmal dragon', 'normal').
card_power('phantasmal dragon', 5).
card_toughness('phantasmal dragon', 5).

% Found in: ALL, CST, DKM, ME2
card_name('phantasmal fiend', 'Phantasmal Fiend').
card_type('phantasmal fiend', 'Creature — Illusion').
card_types('phantasmal fiend', ['Creature']).
card_subtypes('phantasmal fiend', ['Illusion']).
card_colors('phantasmal fiend', ['B']).
card_text('phantasmal fiend', '{B}: Phantasmal Fiend gets +1/-1 until end of turn.\n{1}{U}: Switch Phantasmal Fiend\'s power and toughness until end of turn.').
card_mana_cost('phantasmal fiend', ['3', 'B']).
card_cmc('phantasmal fiend', 4).
card_layout('phantasmal fiend', 'normal').
card_power('phantasmal fiend', 1).
card_toughness('phantasmal fiend', 5).

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, ME4
card_name('phantasmal forces', 'Phantasmal Forces').
card_type('phantasmal forces', 'Creature — Illusion').
card_types('phantasmal forces', ['Creature']).
card_subtypes('phantasmal forces', ['Illusion']).
card_colors('phantasmal forces', ['U']).
card_text('phantasmal forces', 'Flying\nAt the beginning of your upkeep, sacrifice Phantasmal Forces unless you pay {U}.').
card_mana_cost('phantasmal forces', ['3', 'U']).
card_cmc('phantasmal forces', 4).
card_layout('phantasmal forces', 'normal').
card_power('phantasmal forces', 4).
card_toughness('phantasmal forces', 1).

% Found in: M12
card_name('phantasmal image', 'Phantasmal Image').
card_type('phantasmal image', 'Creature — Illusion').
card_types('phantasmal image', ['Creature']).
card_subtypes('phantasmal image', ['Illusion']).
card_colors('phantasmal image', ['U']).
card_text('phantasmal image', 'You may have Phantasmal Image enter the battlefield as a copy of any creature on the battlefield, except it\'s an Illusion in addition to its other types and it gains \"When this creature becomes the target of a spell or ability, sacrifice it.\"').
card_mana_cost('phantasmal image', ['1', 'U']).
card_cmc('phantasmal image', 2).
card_layout('phantasmal image', 'normal').
card_power('phantasmal image', 0).
card_toughness('phantasmal image', 0).

% Found in: ICE, ME2
card_name('phantasmal mount', 'Phantasmal Mount').
card_type('phantasmal mount', 'Creature — Illusion Horse').
card_types('phantasmal mount', ['Creature']).
card_subtypes('phantasmal mount', ['Illusion', 'Horse']).
card_colors('phantasmal mount', ['U']).
card_text('phantasmal mount', 'Flying\n{T}: Target creature you control with toughness 2 or less gets +1/+1 and gains flying until end of turn. When Phantasmal Mount leaves the battlefield this turn, sacrifice that creature. When the creature leaves the battlefield this turn, sacrifice Phantasmal Mount.').
card_mana_cost('phantasmal mount', ['1', 'U']).
card_cmc('phantasmal mount', 2).
card_layout('phantasmal mount', 'normal').
card_power('phantasmal mount', 1).
card_toughness('phantasmal mount', 1).

% Found in: ALL
card_name('phantasmal sphere', 'Phantasmal Sphere').
card_type('phantasmal sphere', 'Creature — Illusion').
card_types('phantasmal sphere', ['Creature']).
card_subtypes('phantasmal sphere', ['Illusion']).
card_colors('phantasmal sphere', ['U']).
card_text('phantasmal sphere', 'Flying\nAt the beginning of your upkeep, put a +1/+1 counter on Phantasmal Sphere, then sacrifice Phantasmal Sphere unless you pay {1} for each +1/+1 counter on it.\nWhen Phantasmal Sphere leaves the battlefield, target opponent puts an X/X blue Orb creature token with flying onto the battlefield, where X is the number of +1/+1 counters on Phantasmal Sphere.').
card_mana_cost('phantasmal sphere', ['1', 'U']).
card_cmc('phantasmal sphere', 2).
card_layout('phantasmal sphere', 'normal').
card_power('phantasmal sphere', 0).
card_toughness('phantasmal sphere', 1).
card_reserved('phantasmal sphere').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, CED, CEI, INV, LEA, LEB, ME4
card_name('phantasmal terrain', 'Phantasmal Terrain').
card_type('phantasmal terrain', 'Enchantment — Aura').
card_types('phantasmal terrain', ['Enchantment']).
card_subtypes('phantasmal terrain', ['Aura']).
card_colors('phantasmal terrain', ['U']).
card_text('phantasmal terrain', 'Enchant land\nAs Phantasmal Terrain enters the battlefield, choose a basic land type.\nEnchanted land is the chosen type.').
card_mana_cost('phantasmal terrain', ['U', 'U']).
card_cmc('phantasmal terrain', 2).
card_layout('phantasmal terrain', 'normal').

% Found in: ODY
card_name('phantatog', 'Phantatog').
card_type('phantatog', 'Creature — Atog').
card_types('phantatog', ['Creature']).
card_subtypes('phantatog', ['Atog']).
card_colors('phantatog', ['W', 'U']).
card_text('phantatog', 'Sacrifice an enchantment: Phantatog gets +1/+1 until end of turn.\nDiscard a card: Phantatog gets +1/+1 until end of turn.').
card_mana_cost('phantatog', ['1', 'W', 'U']).
card_cmc('phantatog', 3).
card_layout('phantatog', 'normal').
card_power('phantatog', 1).
card_toughness('phantatog', 2).

% Found in: M11
card_name('phantom beast', 'Phantom Beast').
card_type('phantom beast', 'Creature — Illusion Beast').
card_types('phantom beast', ['Creature']).
card_subtypes('phantom beast', ['Illusion', 'Beast']).
card_colors('phantom beast', ['U']).
card_text('phantom beast', 'When Phantom Beast becomes the target of a spell or ability, sacrifice it.').
card_mana_cost('phantom beast', ['3', 'U']).
card_cmc('phantom beast', 4).
card_layout('phantom beast', 'normal').
card_power('phantom beast', 4).
card_toughness('phantom beast', 5).

% Found in: JUD
card_name('phantom centaur', 'Phantom Centaur').
card_type('phantom centaur', 'Creature — Centaur Spirit').
card_types('phantom centaur', ['Creature']).
card_subtypes('phantom centaur', ['Centaur', 'Spirit']).
card_colors('phantom centaur', ['G']).
card_text('phantom centaur', 'Protection from black\nPhantom Centaur enters the battlefield with three +1/+1 counters on it.\nIf damage would be dealt to Phantom Centaur, prevent that damage. Remove a +1/+1 counter from Phantom Centaur.').
card_mana_cost('phantom centaur', ['2', 'G', 'G']).
card_cmc('phantom centaur', 4).
card_layout('phantom centaur', 'normal').
card_power('phantom centaur', 2).
card_toughness('phantom centaur', 0).

% Found in: JUD
card_name('phantom flock', 'Phantom Flock').
card_type('phantom flock', 'Creature — Bird Soldier Spirit').
card_types('phantom flock', ['Creature']).
card_subtypes('phantom flock', ['Bird', 'Soldier', 'Spirit']).
card_colors('phantom flock', ['W']).
card_text('phantom flock', 'Flying\nPhantom Flock enters the battlefield with three +1/+1 counters on it.\nIf damage would be dealt to Phantom Flock, prevent that damage. Remove a +1/+1 counter from Phantom Flock.').
card_mana_cost('phantom flock', ['3', 'W', 'W']).
card_cmc('phantom flock', 5).
card_layout('phantom flock', 'normal').
card_power('phantom flock', 0).
card_toughness('phantom flock', 0).

% Found in: DDK, RTR
card_name('phantom general', 'Phantom General').
card_type('phantom general', 'Creature — Spirit Soldier').
card_types('phantom general', ['Creature']).
card_subtypes('phantom general', ['Spirit', 'Soldier']).
card_colors('phantom general', ['W']).
card_text('phantom general', 'Creature tokens you control get +1/+1.').
card_mana_cost('phantom general', ['3', 'W']).
card_cmc('phantom general', 4).
card_layout('phantom general', 'normal').
card_power('phantom general', 2).
card_toughness('phantom general', 3).

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, ITP, LEA, LEB, MED
card_name('phantom monster', 'Phantom Monster').
card_type('phantom monster', 'Creature — Illusion').
card_types('phantom monster', ['Creature']).
card_subtypes('phantom monster', ['Illusion']).
card_colors('phantom monster', ['U']).
card_text('phantom monster', 'Flying').
card_mana_cost('phantom monster', ['3', 'U']).
card_cmc('phantom monster', 4).
card_layout('phantom monster', 'normal').
card_power('phantom monster', 3).
card_toughness('phantom monster', 3).

% Found in: C13, JUD
card_name('phantom nantuko', 'Phantom Nantuko').
card_type('phantom nantuko', 'Creature — Insect Spirit').
card_types('phantom nantuko', ['Creature']).
card_subtypes('phantom nantuko', ['Insect', 'Spirit']).
card_colors('phantom nantuko', ['G']).
card_text('phantom nantuko', 'Trample\nPhantom Nantuko enters the battlefield with two +1/+1 counters on it.\nIf damage would be dealt to Phantom Nantuko, prevent that damage. Remove a +1/+1 counter from Phantom Nantuko.\n{T}: Put a +1/+1 counter on Phantom Nantuko.').
card_mana_cost('phantom nantuko', ['2', 'G']).
card_cmc('phantom nantuko', 3).
card_layout('phantom nantuko', 'normal').
card_power('phantom nantuko', 0).
card_toughness('phantom nantuko', 0).

% Found in: JUD
card_name('phantom nishoba', 'Phantom Nishoba').
card_type('phantom nishoba', 'Creature — Cat Beast Spirit').
card_types('phantom nishoba', ['Creature']).
card_subtypes('phantom nishoba', ['Cat', 'Beast', 'Spirit']).
card_colors('phantom nishoba', ['W', 'G']).
card_text('phantom nishoba', 'Trample\nPhantom Nishoba enters the battlefield with seven +1/+1 counters on it.\nWhenever Phantom Nishoba deals damage, you gain that much life.\nIf damage would be dealt to Phantom Nishoba, prevent that damage. Remove a +1/+1 counter from Phantom Nishoba.').
card_mana_cost('phantom nishoba', ['5', 'G', 'W']).
card_cmc('phantom nishoba', 7).
card_layout('phantom nishoba', 'normal').
card_power('phantom nishoba', 0).
card_toughness('phantom nishoba', 0).

% Found in: JUD, VMA
card_name('phantom nomad', 'Phantom Nomad').
card_type('phantom nomad', 'Creature — Spirit Nomad').
card_types('phantom nomad', ['Creature']).
card_subtypes('phantom nomad', ['Spirit', 'Nomad']).
card_colors('phantom nomad', ['W']).
card_text('phantom nomad', 'Phantom Nomad enters the battlefield with two +1/+1 counters on it.\nIf damage would be dealt to Phantom Nomad, prevent that damage. Remove a +1/+1 counter from Phantom Nomad.').
card_mana_cost('phantom nomad', ['1', 'W']).
card_cmc('phantom nomad', 2).
card_layout('phantom nomad', 'normal').
card_power('phantom nomad', 0).
card_toughness('phantom nomad', 0).

% Found in: JUD
card_name('phantom tiger', 'Phantom Tiger').
card_type('phantom tiger', 'Creature — Cat Spirit').
card_types('phantom tiger', ['Creature']).
card_subtypes('phantom tiger', ['Cat', 'Spirit']).
card_colors('phantom tiger', ['G']).
card_text('phantom tiger', 'Phantom Tiger enters the battlefield with two +1/+1 counters on it.\nIf damage would be dealt to Phantom Tiger, prevent that damage. Remove a +1/+1 counter from Phantom Tiger.').
card_mana_cost('phantom tiger', ['2', 'G']).
card_cmc('phantom tiger', 3).
card_layout('phantom tiger', 'normal').
card_power('phantom tiger', 1).
card_toughness('phantom tiger', 0).

% Found in: 10E, 6ED, 7ED, 8ED, 9ED, DPA, M10, M14, POR, S99, WTH
card_name('phantom warrior', 'Phantom Warrior').
card_type('phantom warrior', 'Creature — Illusion Warrior').
card_types('phantom warrior', ['Creature']).
card_subtypes('phantom warrior', ['Illusion', 'Warrior']).
card_colors('phantom warrior', ['U']).
card_text('phantom warrior', 'Phantom Warrior can\'t be blocked.').
card_mana_cost('phantom warrior', ['1', 'U', 'U']).
card_cmc('phantom warrior', 3).
card_layout('phantom warrior', 'normal').
card_power('phantom warrior', 2).
card_toughness('phantom warrior', 2).

% Found in: ODY
card_name('phantom whelp', 'Phantom Whelp').
card_type('phantom whelp', 'Creature — Illusion Hound').
card_types('phantom whelp', ['Creature']).
card_subtypes('phantom whelp', ['Illusion', 'Hound']).
card_colors('phantom whelp', ['U']).
card_text('phantom whelp', 'When Phantom Whelp attacks or blocks, return it to its owner\'s hand at end of combat. (Return it only if it\'s on the battlefield.)').
card_mana_cost('phantom whelp', ['1', 'U']).
card_cmc('phantom whelp', 2).
card_layout('phantom whelp', 'normal').
card_power('phantom whelp', 2).
card_toughness('phantom whelp', 2).

% Found in: BOK, WTH
card_name('phantom wings', 'Phantom Wings').
card_type('phantom wings', 'Enchantment — Aura').
card_types('phantom wings', ['Enchantment']).
card_subtypes('phantom wings', ['Aura']).
card_colors('phantom wings', ['U']).
card_text('phantom wings', 'Enchant creature\nEnchanted creature has flying.\nSacrifice Phantom Wings: Return enchanted creature to its owner\'s hand.').
card_mana_cost('phantom wings', ['1', 'U']).
card_cmc('phantom wings', 2).
card_layout('phantom wings', 'normal').

% Found in: TSP
card_name('phantom wurm', 'Phantom Wurm').
card_type('phantom wurm', 'Creature — Wurm Spirit').
card_types('phantom wurm', ['Creature']).
card_subtypes('phantom wurm', ['Wurm', 'Spirit']).
card_colors('phantom wurm', ['G']).
card_text('phantom wurm', 'Phantom Wurm enters the battlefield with four +1/+1 counters on it.\nIf damage would be dealt to Phantom Wurm, prevent that damage. Remove a +1/+1 counter from Phantom Wurm.').
card_mana_cost('phantom wurm', ['4', 'G', 'G']).
card_cmc('phantom wurm', 6).
card_layout('phantom wurm', 'normal').
card_power('phantom wurm', 2).
card_toughness('phantom wurm', 0).

% Found in: BNG
card_name('pharagax giant', 'Pharagax Giant').
card_type('pharagax giant', 'Creature — Giant').
card_types('pharagax giant', ['Creature']).
card_subtypes('pharagax giant', ['Giant']).
card_colors('pharagax giant', ['R']).
card_text('pharagax giant', 'Tribute 2 (As this creature enters the battlefield, an opponent of your choice may place two +1/+1 counters on it.)\nWhen Pharagax Giant enters the battlefield, if tribute wasn\'t paid, Pharagax Giant deals 5 damage to each opponent.').
card_mana_cost('pharagax giant', ['4', 'R']).
card_cmc('pharagax giant', 5).
card_layout('pharagax giant', 'normal').
card_power('pharagax giant', 3).
card_toughness('pharagax giant', 3).

% Found in: JOU
card_name('pharika\'s chosen', 'Pharika\'s Chosen').
card_type('pharika\'s chosen', 'Creature — Snake').
card_types('pharika\'s chosen', ['Creature']).
card_subtypes('pharika\'s chosen', ['Snake']).
card_colors('pharika\'s chosen', ['B']).
card_text('pharika\'s chosen', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_mana_cost('pharika\'s chosen', ['B']).
card_cmc('pharika\'s chosen', 1).
card_layout('pharika\'s chosen', 'normal').
card_power('pharika\'s chosen', 1).
card_toughness('pharika\'s chosen', 1).

% Found in: THS
card_name('pharika\'s cure', 'Pharika\'s Cure').
card_type('pharika\'s cure', 'Instant').
card_types('pharika\'s cure', ['Instant']).
card_subtypes('pharika\'s cure', []).
card_colors('pharika\'s cure', ['B']).
card_text('pharika\'s cure', 'Pharika\'s Cure deals 2 damage to target creature and you gain 2 life.').
card_mana_cost('pharika\'s cure', ['B', 'B']).
card_cmc('pharika\'s cure', 2).
card_layout('pharika\'s cure', 'normal').

% Found in: ORI
card_name('pharika\'s disciple', 'Pharika\'s Disciple').
card_type('pharika\'s disciple', 'Creature — Centaur Warrior').
card_types('pharika\'s disciple', ['Creature']).
card_subtypes('pharika\'s disciple', ['Centaur', 'Warrior']).
card_colors('pharika\'s disciple', ['G']).
card_text('pharika\'s disciple', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nRenown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)').
card_mana_cost('pharika\'s disciple', ['3', 'G']).
card_cmc('pharika\'s disciple', 4).
card_layout('pharika\'s disciple', 'normal').
card_power('pharika\'s disciple', 2).
card_toughness('pharika\'s disciple', 3).

% Found in: THS
card_name('pharika\'s mender', 'Pharika\'s Mender').
card_type('pharika\'s mender', 'Creature — Gorgon').
card_types('pharika\'s mender', ['Creature']).
card_subtypes('pharika\'s mender', ['Gorgon']).
card_colors('pharika\'s mender', ['B', 'G']).
card_text('pharika\'s mender', 'When Pharika\'s Mender enters the battlefield, you may return target creature or enchantment card from your graveyard to your hand.').
card_mana_cost('pharika\'s mender', ['3', 'B', 'G']).
card_cmc('pharika\'s mender', 5).
card_layout('pharika\'s mender', 'normal').
card_power('pharika\'s mender', 4).
card_toughness('pharika\'s mender', 3).

% Found in: JOU
card_name('pharika, god of affliction', 'Pharika, God of Affliction').
card_type('pharika, god of affliction', 'Legendary Enchantment Creature — God').
card_types('pharika, god of affliction', ['Enchantment', 'Creature']).
card_subtypes('pharika, god of affliction', ['God']).
card_supertypes('pharika, god of affliction', ['Legendary']).
card_colors('pharika, god of affliction', ['B', 'G']).
card_text('pharika, god of affliction', 'Indestructible\nAs long as your devotion to black and green is less than seven, Pharika isn\'t a creature.\n{B}{G}: Exile target creature card from a graveyard. Its owner puts a 1/1 black and green Snake enchantment creature token with deathtouch onto the battlefield.').
card_mana_cost('pharika, god of affliction', ['1', 'B', 'G']).
card_cmc('pharika, god of affliction', 3).
card_layout('pharika, god of affliction', 'normal').
card_power('pharika, god of affliction', 5).
card_toughness('pharika, god of affliction', 5).

% Found in: ALL, MED
card_name('phelddagrif', 'Phelddagrif').
card_type('phelddagrif', 'Legendary Creature — Phelddagrif').
card_types('phelddagrif', ['Creature']).
card_subtypes('phelddagrif', ['Phelddagrif']).
card_supertypes('phelddagrif', ['Legendary']).
card_colors('phelddagrif', ['W', 'U', 'G']).
card_text('phelddagrif', '{G}: Phelddagrif gains trample until end of turn. Target opponent puts a 1/1 green Hippo creature token onto the battlefield.\n{W}: Phelddagrif gains flying until end of turn. Target opponent gains 2 life.\n{U}: Return Phelddagrif to its owner\'s hand. Target opponent may draw a card.').
card_mana_cost('phelddagrif', ['1', 'G', 'W', 'U']).
card_cmc('phelddagrif', 4).
card_layout('phelddagrif', 'normal').
card_power('phelddagrif', 4).
card_toughness('phelddagrif', 4).
card_reserved('phelddagrif').

% Found in: BNG
card_name('phenax, god of deception', 'Phenax, God of Deception').
card_type('phenax, god of deception', 'Legendary Enchantment Creature — God').
card_types('phenax, god of deception', ['Enchantment', 'Creature']).
card_subtypes('phenax, god of deception', ['God']).
card_supertypes('phenax, god of deception', ['Legendary']).
card_colors('phenax, god of deception', ['U', 'B']).
card_text('phenax, god of deception', 'Indestructible\nAs long as your devotion to blue and black is less than seven, Phenax isn\'t a creature.\nCreatures you control have \"{T}: Target player puts the top X cards of his or her library into his or her graveyard, where X is this creature\'s toughness.\"').
card_mana_cost('phenax, god of deception', ['3', 'U', 'B']).
card_cmc('phenax, god of deception', 5).
card_layout('phenax, god of deception', 'normal').
card_power('phenax, god of deception', 4).
card_toughness('phenax, god of deception', 7).

% Found in: THS
card_name('pheres-band centaurs', 'Pheres-Band Centaurs').
card_type('pheres-band centaurs', 'Creature — Centaur Warrior').
card_types('pheres-band centaurs', ['Creature']).
card_subtypes('pheres-band centaurs', ['Centaur', 'Warrior']).
card_colors('pheres-band centaurs', ['G']).
card_text('pheres-band centaurs', '').
card_mana_cost('pheres-band centaurs', ['4', 'G']).
card_cmc('pheres-band centaurs', 5).
card_layout('pheres-band centaurs', 'normal').
card_power('pheres-band centaurs', 3).
card_toughness('pheres-band centaurs', 7).

% Found in: BNG
card_name('pheres-band raiders', 'Pheres-Band Raiders').
card_type('pheres-band raiders', 'Creature — Centaur Warrior').
card_types('pheres-band raiders', ['Creature']).
card_subtypes('pheres-band raiders', ['Centaur', 'Warrior']).
card_colors('pheres-band raiders', ['G']).
card_text('pheres-band raiders', 'Inspired — Whenever Pheres-Band Raiders becomes untapped, you may pay {2}{G}. If you do, put a 3/3 green Centaur enchantment creature token onto the battlefield.').
card_mana_cost('pheres-band raiders', ['5', 'G']).
card_cmc('pheres-band raiders', 6).
card_layout('pheres-band raiders', 'normal').
card_power('pheres-band raiders', 5).
card_toughness('pheres-band raiders', 5).

% Found in: JOU
card_name('pheres-band thunderhoof', 'Pheres-Band Thunderhoof').
card_type('pheres-band thunderhoof', 'Creature — Centaur Warrior').
card_types('pheres-band thunderhoof', ['Creature']).
card_subtypes('pheres-band thunderhoof', ['Centaur', 'Warrior']).
card_colors('pheres-band thunderhoof', ['G']).
card_text('pheres-band thunderhoof', 'Heroic — Whenever you cast a spell that targets Pheres-Band Thunderhoof, put two +1/+1 counters on Pheres-Band Thunderhoof.').
card_mana_cost('pheres-band thunderhoof', ['4', 'G']).
card_cmc('pheres-band thunderhoof', 5).
card_layout('pheres-band thunderhoof', 'normal').
card_power('pheres-band thunderhoof', 3).
card_toughness('pheres-band thunderhoof', 4).

% Found in: BNG
card_name('pheres-band tromper', 'Pheres-Band Tromper').
card_type('pheres-band tromper', 'Creature — Centaur Warrior').
card_types('pheres-band tromper', ['Creature']).
card_subtypes('pheres-band tromper', ['Centaur', 'Warrior']).
card_colors('pheres-band tromper', ['G']).
card_text('pheres-band tromper', 'Inspired — Whenever Pheres-Band Tromper becomes untapped, put a +1/+1 counter on it.').
card_mana_cost('pheres-band tromper', ['3', 'G']).
card_cmc('pheres-band tromper', 4).
card_layout('pheres-band tromper', 'normal').
card_power('pheres-band tromper', 3).
card_toughness('pheres-band tromper', 3).

% Found in: JOU
card_name('pheres-band warchief', 'Pheres-Band Warchief').
card_type('pheres-band warchief', 'Creature — Centaur Warrior').
card_types('pheres-band warchief', ['Creature']).
card_subtypes('pheres-band warchief', ['Centaur', 'Warrior']).
card_colors('pheres-band warchief', ['G']).
card_text('pheres-band warchief', 'Vigilance, trample\nOther Centaur creatures you control get +1/+1 and have vigilance and trample.').
card_mana_cost('pheres-band warchief', ['3', 'G']).
card_cmc('pheres-band warchief', 4).
card_layout('pheres-band warchief', 'normal').
card_power('pheres-band warchief', 3).
card_toughness('pheres-band warchief', 3).

% Found in: CSP
card_name('phobian phantasm', 'Phobian Phantasm').
card_type('phobian phantasm', 'Creature — Illusion').
card_types('phobian phantasm', ['Creature']).
card_subtypes('phobian phantasm', ['Illusion']).
card_colors('phobian phantasm', ['B']).
card_text('phobian phantasm', 'Flying; fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nCumulative upkeep {B} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('phobian phantasm', ['1', 'B', 'B']).
card_cmc('phobian phantasm', 3).
card_layout('phobian phantasm', 'normal').
card_power('phobian phantasm', 3).
card_toughness('phobian phantasm', 3).

% Found in: EVE, FUT
card_name('phosphorescent feast', 'Phosphorescent Feast').
card_type('phosphorescent feast', 'Sorcery').
card_types('phosphorescent feast', ['Sorcery']).
card_subtypes('phosphorescent feast', []).
card_colors('phosphorescent feast', ['G']).
card_text('phosphorescent feast', 'Chroma — Reveal any number of cards in your hand. You gain 2 life for each green mana symbol in those cards\' mana costs.').
card_mana_cost('phosphorescent feast', ['2', 'G', 'G', 'G']).
card_cmc('phosphorescent feast', 5).
card_layout('phosphorescent feast', 'normal').

% Found in: C13, MMA, TSP
card_name('phthisis', 'Phthisis').
card_type('phthisis', 'Sorcery').
card_types('phthisis', ['Sorcery']).
card_subtypes('phthisis', []).
card_colors('phthisis', ['B']).
card_text('phthisis', 'Destroy target creature. Its controller loses life equal to its power plus its toughness.\nSuspend 5—{1}{B} (Rather than cast this card from your hand, you may pay {1}{B} and exile it with five time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_mana_cost('phthisis', ['3', 'B', 'B', 'B', 'B']).
card_cmc('phthisis', 7).
card_layout('phthisis', 'normal').

% Found in: M11, M13
card_name('phylactery lich', 'Phylactery Lich').
card_type('phylactery lich', 'Creature — Zombie').
card_types('phylactery lich', ['Creature']).
card_subtypes('phylactery lich', ['Zombie']).
card_colors('phylactery lich', ['B']).
card_text('phylactery lich', 'Indestructible\nAs Phylactery Lich enters the battlefield, put a phylactery counter on an artifact you control.\nWhen you control no permanents with phylactery counters on them, sacrifice Phylactery Lich.').
card_mana_cost('phylactery lich', ['B', 'B', 'B']).
card_cmc('phylactery lich', 3).
card_layout('phylactery lich', 'normal').
card_power('phylactery lich', 5).
card_toughness('phylactery lich', 5).

% Found in: MBS
card_name('phyresis', 'Phyresis').
card_type('phyresis', 'Enchantment — Aura').
card_types('phyresis', ['Enchantment']).
card_subtypes('phyresis', ['Aura']).
card_colors('phyresis', ['B']).
card_text('phyresis', 'Enchant creature\nEnchanted creature has infect. (It deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('phyresis', ['1', 'B']).
card_cmc('phyresis', 2).
card_layout('phyresis', 'normal').

% Found in: C14, NPH
card_name('phyrexia\'s core', 'Phyrexia\'s Core').
card_type('phyrexia\'s core', 'Land').
card_types('phyrexia\'s core', ['Land']).
card_subtypes('phyrexia\'s core', []).
card_colors('phyrexia\'s core', []).
card_text('phyrexia\'s core', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice an artifact: You gain 1 life.').
card_layout('phyrexia\'s core', 'normal').

% Found in: INV
card_name('phyrexian altar', 'Phyrexian Altar').
card_type('phyrexian altar', 'Artifact').
card_types('phyrexian altar', ['Artifact']).
card_subtypes('phyrexian altar', []).
card_colors('phyrexian altar', []).
card_text('phyrexian altar', 'Sacrifice a creature: Add one mana of any color to your mana pool.').
card_mana_cost('phyrexian altar', ['3']).
card_cmc('phyrexian altar', 3).
card_layout('phyrexian altar', 'normal').

% Found in: 8ED, 9ED, APC, DDE, HOP
card_name('phyrexian arena', 'Phyrexian Arena').
card_type('phyrexian arena', 'Enchantment').
card_types('phyrexian arena', ['Enchantment']).
card_subtypes('phyrexian arena', []).
card_colors('phyrexian arena', ['B']).
card_text('phyrexian arena', 'At the beginning of your upkeep, you draw a card and you lose 1 life.').
card_mana_cost('phyrexian arena', ['1', 'B', 'B']).
card_cmc('phyrexian arena', 3).
card_layout('phyrexian arena', 'normal').

% Found in: DDE, INV
card_name('phyrexian battleflies', 'Phyrexian Battleflies').
card_type('phyrexian battleflies', 'Creature — Insect').
card_types('phyrexian battleflies', ['Creature']).
card_subtypes('phyrexian battleflies', ['Insect']).
card_colors('phyrexian battleflies', ['B']).
card_text('phyrexian battleflies', 'Flying\n{B}: Phyrexian Battleflies gets +1/+0 until end of turn. Activate this ability no more than twice each turn.').
card_mana_cost('phyrexian battleflies', ['B']).
card_cmc('phyrexian battleflies', 1).
card_layout('phyrexian battleflies', 'normal').
card_power('phyrexian battleflies', 0).
card_toughness('phyrexian battleflies', 1).

% Found in: PLS
card_name('phyrexian bloodstock', 'Phyrexian Bloodstock').
card_type('phyrexian bloodstock', 'Creature — Zombie').
card_types('phyrexian bloodstock', ['Creature']).
card_subtypes('phyrexian bloodstock', ['Zombie']).
card_colors('phyrexian bloodstock', ['B']).
card_text('phyrexian bloodstock', 'When Phyrexian Bloodstock leaves the battlefield, destroy target white creature. It can\'t be regenerated.').
card_mana_cost('phyrexian bloodstock', ['4', 'B']).
card_cmc('phyrexian bloodstock', 5).
card_layout('phyrexian bloodstock', 'normal').
card_power('phyrexian bloodstock', 3).
card_toughness('phyrexian bloodstock', 3).

% Found in: ALL, MED
card_name('phyrexian boon', 'Phyrexian Boon').
card_type('phyrexian boon', 'Enchantment — Aura').
card_types('phyrexian boon', ['Enchantment']).
card_subtypes('phyrexian boon', ['Aura']).
card_colors('phyrexian boon', ['B']).
card_text('phyrexian boon', 'Enchant creature\nEnchanted creature gets +2/+1 as long as it\'s black. Otherwise, it gets -1/-2.').
card_mana_cost('phyrexian boon', ['2', 'B']).
card_cmc('phyrexian boon', 3).
card_layout('phyrexian boon', 'normal').

% Found in: DDE, ULG
card_name('phyrexian broodlings', 'Phyrexian Broodlings').
card_type('phyrexian broodlings', 'Creature — Minion').
card_types('phyrexian broodlings', ['Creature']).
card_subtypes('phyrexian broodlings', ['Minion']).
card_colors('phyrexian broodlings', ['B']).
card_text('phyrexian broodlings', '{1}, Sacrifice a creature: Put a +1/+1 counter on Phyrexian Broodlings.').
card_mana_cost('phyrexian broodlings', ['1', 'B', 'B']).
card_cmc('phyrexian broodlings', 3).
card_layout('phyrexian broodlings', 'normal').
card_power('phyrexian broodlings', 2).
card_toughness('phyrexian broodlings', 2).

% Found in: 7ED, 8ED, DDE, USG
card_name('phyrexian colossus', 'Phyrexian Colossus').
card_type('phyrexian colossus', 'Artifact Creature — Golem').
card_types('phyrexian colossus', ['Artifact', 'Creature']).
card_subtypes('phyrexian colossus', ['Golem']).
card_colors('phyrexian colossus', []).
card_text('phyrexian colossus', 'Phyrexian Colossus doesn\'t untap during your untap step.\nPay 8 life: Untap Phyrexian Colossus.\nPhyrexian Colossus can\'t be blocked except by three or more creatures.').
card_mana_cost('phyrexian colossus', ['7']).
card_cmc('phyrexian colossus', 7).
card_layout('phyrexian colossus', 'normal').
card_power('phyrexian colossus', 8).
card_toughness('phyrexian colossus', 8).

% Found in: MBS
card_name('phyrexian crusader', 'Phyrexian Crusader').
card_type('phyrexian crusader', 'Creature — Zombie Knight').
card_types('phyrexian crusader', ['Creature']).
card_subtypes('phyrexian crusader', ['Zombie', 'Knight']).
card_colors('phyrexian crusader', ['B']).
card_text('phyrexian crusader', 'First strike, protection from red and from white\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('phyrexian crusader', ['1', 'B', 'B']).
card_cmc('phyrexian crusader', 3).
card_layout('phyrexian crusader', 'normal').
card_power('phyrexian crusader', 2).
card_toughness('phyrexian crusader', 2).

% Found in: DDE, ULG
card_name('phyrexian debaser', 'Phyrexian Debaser').
card_type('phyrexian debaser', 'Creature — Carrier').
card_types('phyrexian debaser', ['Creature']).
card_subtypes('phyrexian debaser', ['Carrier']).
card_colors('phyrexian debaser', ['B']).
card_text('phyrexian debaser', 'Flying\n{T}, Sacrifice Phyrexian Debaser: Target creature gets -2/-2 until end of turn.').
card_mana_cost('phyrexian debaser', ['3', 'B']).
card_cmc('phyrexian debaser', 4).
card_layout('phyrexian debaser', 'normal').
card_power('phyrexian debaser', 2).
card_toughness('phyrexian debaser', 2).

% Found in: DDE, ULG, VMA
card_name('phyrexian defiler', 'Phyrexian Defiler').
card_type('phyrexian defiler', 'Creature — Carrier').
card_types('phyrexian defiler', ['Creature']).
card_subtypes('phyrexian defiler', ['Carrier']).
card_colors('phyrexian defiler', ['B']).
card_text('phyrexian defiler', '{T}, Sacrifice Phyrexian Defiler: Target creature gets -3/-3 until end of turn.').
card_mana_cost('phyrexian defiler', ['2', 'B', 'B']).
card_cmc('phyrexian defiler', 4).
card_layout('phyrexian defiler', 'normal').
card_power('phyrexian defiler', 3).
card_toughness('phyrexian defiler', 3).

% Found in: C13, INV
card_name('phyrexian delver', 'Phyrexian Delver').
card_type('phyrexian delver', 'Creature — Zombie').
card_types('phyrexian delver', ['Creature']).
card_subtypes('phyrexian delver', ['Zombie']).
card_colors('phyrexian delver', ['B']).
card_text('phyrexian delver', 'When Phyrexian Delver enters the battlefield, return target creature card from your graveyard to the battlefield. You lose life equal to that card\'s converted mana cost.').
card_mana_cost('phyrexian delver', ['3', 'B', 'B']).
card_cmc('phyrexian delver', 5).
card_layout('phyrexian delver', 'normal').
card_power('phyrexian delver', 3).
card_toughness('phyrexian delver', 2).

% Found in: DDE, ULG
card_name('phyrexian denouncer', 'Phyrexian Denouncer').
card_type('phyrexian denouncer', 'Creature — Carrier').
card_types('phyrexian denouncer', ['Creature']).
card_subtypes('phyrexian denouncer', ['Carrier']).
card_colors('phyrexian denouncer', ['B']).
card_text('phyrexian denouncer', '{T}, Sacrifice Phyrexian Denouncer: Target creature gets -1/-1 until end of turn.').
card_mana_cost('phyrexian denouncer', ['1', 'B']).
card_cmc('phyrexian denouncer', 2).
card_layout('phyrexian denouncer', 'normal').
card_power('phyrexian denouncer', 1).
card_toughness('phyrexian denouncer', 1).

% Found in: ALL, ME2
card_name('phyrexian devourer', 'Phyrexian Devourer').
card_type('phyrexian devourer', 'Artifact Creature — Construct').
card_types('phyrexian devourer', ['Artifact', 'Creature']).
card_subtypes('phyrexian devourer', ['Construct']).
card_colors('phyrexian devourer', []).
card_text('phyrexian devourer', 'When Phyrexian Devourer\'s power is 7 or greater, sacrifice it.\nExile the top card of your library: Put X +1/+1 counters on Phyrexian Devourer, where X is the exiled card\'s converted mana cost.').
card_mana_cost('phyrexian devourer', ['6']).
card_cmc('phyrexian devourer', 6).
card_layout('phyrexian devourer', 'normal').
card_power('phyrexian devourer', 1).
card_toughness('phyrexian devourer', 1).
card_reserved('phyrexian devourer').

% Found in: MBS
card_name('phyrexian digester', 'Phyrexian Digester').
card_type('phyrexian digester', 'Artifact Creature — Construct').
card_types('phyrexian digester', ['Artifact', 'Creature']).
card_subtypes('phyrexian digester', ['Construct']).
card_colors('phyrexian digester', []).
card_text('phyrexian digester', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('phyrexian digester', ['3']).
card_cmc('phyrexian digester', 3).
card_layout('phyrexian digester', 'normal').
card_power('phyrexian digester', 2).
card_toughness('phyrexian digester', 1).

% Found in: MIR, pJGP
card_name('phyrexian dreadnought', 'Phyrexian Dreadnought').
card_type('phyrexian dreadnought', 'Artifact Creature — Dreadnought').
card_types('phyrexian dreadnought', ['Artifact', 'Creature']).
card_subtypes('phyrexian dreadnought', ['Dreadnought']).
card_colors('phyrexian dreadnought', []).
card_text('phyrexian dreadnought', 'Trample\nWhen Phyrexian Dreadnought enters the battlefield, sacrifice it unless you sacrifice any number of creatures with total power 12 or greater.').
card_mana_cost('phyrexian dreadnought', ['1']).
card_cmc('phyrexian dreadnought', 1).
card_layout('phyrexian dreadnought', 'normal').
card_power('phyrexian dreadnought', 12).
card_toughness('phyrexian dreadnought', 12).
card_reserved('phyrexian dreadnought').

% Found in: NMS
card_name('phyrexian driver', 'Phyrexian Driver').
card_type('phyrexian driver', 'Creature — Zombie Mercenary').
card_types('phyrexian driver', ['Creature']).
card_subtypes('phyrexian driver', ['Zombie', 'Mercenary']).
card_colors('phyrexian driver', ['B']).
card_text('phyrexian driver', 'When Phyrexian Driver enters the battlefield, other Mercenary creatures get +1/+1 until end of turn.').
card_mana_cost('phyrexian driver', ['2', 'B']).
card_cmc('phyrexian driver', 3).
card_layout('phyrexian driver', 'normal').
card_power('phyrexian driver', 1).
card_toughness('phyrexian driver', 1).

% Found in: CSP
card_name('phyrexian etchings', 'Phyrexian Etchings').
card_type('phyrexian etchings', 'Enchantment').
card_types('phyrexian etchings', ['Enchantment']).
card_subtypes('phyrexian etchings', []).
card_colors('phyrexian etchings', ['B']).
card_text('phyrexian etchings', 'Cumulative upkeep {B} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAt the beginning of your end step, draw a card for each age counter on Phyrexian Etchings.\nWhen Phyrexian Etchings is put into a graveyard from the battlefield, you lose 2 life for each age counter on it.').
card_mana_cost('phyrexian etchings', ['B', 'B', 'B']).
card_cmc('phyrexian etchings', 3).
card_layout('phyrexian etchings', 'normal').

% Found in: WTH
card_name('phyrexian furnace', 'Phyrexian Furnace').
card_type('phyrexian furnace', 'Artifact').
card_types('phyrexian furnace', ['Artifact']).
card_subtypes('phyrexian furnace', []).
card_colors('phyrexian furnace', []).
card_text('phyrexian furnace', '{T}: Exile the bottom card of target player\'s graveyard.\n{1}, Sacrifice Phyrexian Furnace: Exile target card from a graveyard. Draw a card.').
card_mana_cost('phyrexian furnace', ['1']).
card_cmc('phyrexian furnace', 1).
card_layout('phyrexian furnace', 'normal').

% Found in: 9ED, APC, C13, C14, DDE
card_name('phyrexian gargantua', 'Phyrexian Gargantua').
card_type('phyrexian gargantua', 'Creature — Horror').
card_types('phyrexian gargantua', ['Creature']).
card_subtypes('phyrexian gargantua', ['Horror']).
card_colors('phyrexian gargantua', ['B']).
card_text('phyrexian gargantua', 'When Phyrexian Gargantua enters the battlefield, you draw two cards and you lose 2 life.').
card_mana_cost('phyrexian gargantua', ['4', 'B', 'B']).
card_cmc('phyrexian gargantua', 6).
card_layout('phyrexian gargantua', 'normal').
card_power('phyrexian gargantua', 4).
card_toughness('phyrexian gargantua', 4).

% Found in: BRB, DDE, HOP, USG
card_name('phyrexian ghoul', 'Phyrexian Ghoul').
card_type('phyrexian ghoul', 'Creature — Zombie').
card_types('phyrexian ghoul', ['Creature']).
card_subtypes('phyrexian ghoul', ['Zombie']).
card_colors('phyrexian ghoul', ['B']).
card_text('phyrexian ghoul', 'Sacrifice a creature: Phyrexian Ghoul gets +2/+2 until end of turn.').
card_mana_cost('phyrexian ghoul', ['2', 'B']).
card_cmc('phyrexian ghoul', 3).
card_layout('phyrexian ghoul', 'normal').
card_power('phyrexian ghoul', 2).
card_toughness('phyrexian ghoul', 2).

% Found in: ATQ
card_name('phyrexian gremlins', 'Phyrexian Gremlins').
card_type('phyrexian gremlins', 'Creature — Gremlin').
card_types('phyrexian gremlins', ['Creature']).
card_subtypes('phyrexian gremlins', ['Gremlin']).
card_colors('phyrexian gremlins', ['B']).
card_text('phyrexian gremlins', 'You may choose not to untap Phyrexian Gremlins during your untap step.\n{T}: Tap target artifact. It doesn\'t untap during its controller\'s untap step for as long as Phyrexian Gremlins remains tapped.').
card_mana_cost('phyrexian gremlins', ['2', 'B']).
card_cmc('phyrexian gremlins', 3).
card_layout('phyrexian gremlins', 'normal').
card_power('phyrexian gremlins', 1).
card_toughness('phyrexian gremlins', 1).

% Found in: TMP
card_name('phyrexian grimoire', 'Phyrexian Grimoire').
card_type('phyrexian grimoire', 'Artifact').
card_types('phyrexian grimoire', ['Artifact']).
card_subtypes('phyrexian grimoire', []).
card_colors('phyrexian grimoire', []).
card_text('phyrexian grimoire', '{4}, {T}: Target opponent chooses one of the top two cards of your graveyard. Exile that card and put the other one into your hand.').
card_mana_cost('phyrexian grimoire', ['3']).
card_cmc('phyrexian grimoire', 3).
card_layout('phyrexian grimoire', 'normal').

% Found in: 7ED, 8ED, 9ED, DDE, M13, NPH, TMP, TPR
card_name('phyrexian hulk', 'Phyrexian Hulk').
card_type('phyrexian hulk', 'Artifact Creature — Golem').
card_types('phyrexian hulk', ['Artifact', 'Creature']).
card_subtypes('phyrexian hulk', ['Golem']).
card_colors('phyrexian hulk', []).
card_text('phyrexian hulk', '').
card_mana_cost('phyrexian hulk', ['6']).
card_cmc('phyrexian hulk', 6).
card_layout('phyrexian hulk', 'normal').
card_power('phyrexian hulk', 5).
card_toughness('phyrexian hulk', 4).

% Found in: MBS
card_name('phyrexian hydra', 'Phyrexian Hydra').
card_type('phyrexian hydra', 'Creature — Hydra').
card_types('phyrexian hydra', ['Creature']).
card_subtypes('phyrexian hydra', ['Hydra']).
card_colors('phyrexian hydra', ['G']).
card_text('phyrexian hydra', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nIf damage would be dealt to Phyrexian Hydra, prevent that damage. Put a -1/-1 counter on Phyrexian Hydra for each 1 damage prevented this way.').
card_mana_cost('phyrexian hydra', ['3', 'G', 'G']).
card_cmc('phyrexian hydra', 5).
card_layout('phyrexian hydra', 'normal').
card_power('phyrexian hydra', 7).
card_toughness('phyrexian hydra', 7).

% Found in: INV
card_name('phyrexian infiltrator', 'Phyrexian Infiltrator').
card_type('phyrexian infiltrator', 'Creature — Minion').
card_types('phyrexian infiltrator', ['Creature']).
card_subtypes('phyrexian infiltrator', ['Minion']).
card_colors('phyrexian infiltrator', ['B']).
card_text('phyrexian infiltrator', '{2}{U}{U}: Exchange control of Phyrexian Infiltrator and target creature. (This effect lasts indefinitely.)').
card_mana_cost('phyrexian infiltrator', ['2', 'B']).
card_cmc('phyrexian infiltrator', 3).
card_layout('phyrexian infiltrator', 'normal').
card_power('phyrexian infiltrator', 2).
card_toughness('phyrexian infiltrator', 2).

% Found in: C14, NPH
card_name('phyrexian ingester', 'Phyrexian Ingester').
card_type('phyrexian ingester', 'Creature — Beast').
card_types('phyrexian ingester', ['Creature']).
card_subtypes('phyrexian ingester', ['Beast']).
card_colors('phyrexian ingester', ['U']).
card_text('phyrexian ingester', 'Imprint — When Phyrexian Ingester enters the battlefield, you may exile target nontoken creature.\nPhyrexian Ingester gets +X/+Y, where X is the exiled creature card\'s power and Y is its toughness.').
card_mana_cost('phyrexian ingester', ['6', 'U']).
card_cmc('phyrexian ingester', 7).
card_layout('phyrexian ingester', 'normal').
card_power('phyrexian ingester', 3).
card_toughness('phyrexian ingester', 3).

% Found in: CSP
card_name('phyrexian ironfoot', 'Phyrexian Ironfoot').
card_type('phyrexian ironfoot', 'Snow Artifact Creature — Construct').
card_types('phyrexian ironfoot', ['Artifact', 'Creature']).
card_subtypes('phyrexian ironfoot', ['Construct']).
card_supertypes('phyrexian ironfoot', ['Snow']).
card_colors('phyrexian ironfoot', []).
card_text('phyrexian ironfoot', 'Phyrexian Ironfoot doesn\'t untap during your untap step.\n{1}{S}: Untap Phyrexian Ironfoot. ({S} can be paid with one mana from a snow permanent.)').
card_mana_cost('phyrexian ironfoot', ['3']).
card_cmc('phyrexian ironfoot', 3).
card_layout('phyrexian ironfoot', 'normal').
card_power('phyrexian ironfoot', 3).
card_toughness('phyrexian ironfoot', 4).

% Found in: MBS
card_name('phyrexian juggernaut', 'Phyrexian Juggernaut').
card_type('phyrexian juggernaut', 'Artifact Creature — Juggernaut').
card_types('phyrexian juggernaut', ['Artifact', 'Creature']).
card_subtypes('phyrexian juggernaut', ['Juggernaut']).
card_colors('phyrexian juggernaut', []).
card_text('phyrexian juggernaut', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nPhyrexian Juggernaut attacks each turn if able.').
card_mana_cost('phyrexian juggernaut', ['6']).
card_cmc('phyrexian juggernaut', 6).
card_layout('phyrexian juggernaut', 'normal').
card_power('phyrexian juggernaut', 5).
card_toughness('phyrexian juggernaut', 5).

% Found in: INV
card_name('phyrexian lens', 'Phyrexian Lens').
card_type('phyrexian lens', 'Artifact').
card_types('phyrexian lens', ['Artifact']).
card_subtypes('phyrexian lens', []).
card_colors('phyrexian lens', []).
card_text('phyrexian lens', '{T}, Pay 1 life: Add one mana of any color to your mana pool.').
card_mana_cost('phyrexian lens', ['3']).
card_cmc('phyrexian lens', 3).
card_layout('phyrexian lens', 'normal').

% Found in: UNH
card_name('phyrexian librarian', 'Phyrexian Librarian').
card_type('phyrexian librarian', 'Creature — Horror').
card_types('phyrexian librarian', ['Creature']).
card_subtypes('phyrexian librarian', ['Horror']).
card_colors('phyrexian librarian', ['B']).
card_text('phyrexian librarian', 'Flying, trample\nAt the beginning of your upkeep, remove the top card of your library from the game face up and balance it on your body.\nWhen a balanced card falls or touches another balanced card, sacrifice Phyrexian Librarian.').
card_mana_cost('phyrexian librarian', ['3', 'B']).
card_cmc('phyrexian librarian', 4).
card_layout('phyrexian librarian', 'normal').
card_power('phyrexian librarian', 3).
card_toughness('phyrexian librarian', 3).

% Found in: VIS
card_name('phyrexian marauder', 'Phyrexian Marauder').
card_type('phyrexian marauder', 'Artifact Creature — Construct').
card_types('phyrexian marauder', ['Artifact', 'Creature']).
card_subtypes('phyrexian marauder', ['Construct']).
card_colors('phyrexian marauder', []).
card_text('phyrexian marauder', 'Phyrexian Marauder enters the battlefield with X +1/+1 counters on it.\nPhyrexian Marauder can\'t block.\nPhyrexian Marauder can\'t attack unless you pay {1} for each +1/+1 counter on it.').
card_mana_cost('phyrexian marauder', ['X']).
card_cmc('phyrexian marauder', 0).
card_layout('phyrexian marauder', 'normal').
card_power('phyrexian marauder', 0).
card_toughness('phyrexian marauder', 0).
card_reserved('phyrexian marauder').

% Found in: NPH, pLPA
card_name('phyrexian metamorph', 'Phyrexian Metamorph').
card_type('phyrexian metamorph', 'Artifact Creature — Shapeshifter').
card_types('phyrexian metamorph', ['Artifact', 'Creature']).
card_subtypes('phyrexian metamorph', ['Shapeshifter']).
card_colors('phyrexian metamorph', ['U']).
card_text('phyrexian metamorph', '({U/P} can be paid with either {U} or 2 life.)\nYou may have Phyrexian Metamorph enter the battlefield as a copy of any artifact or creature on the battlefield, except it\'s an artifact in addition to its other types.').
card_mana_cost('phyrexian metamorph', ['3', 'U/P']).
card_cmc('phyrexian metamorph', 4).
card_layout('phyrexian metamorph', 'normal').
card_power('phyrexian metamorph', 0).
card_toughness('phyrexian metamorph', 0).

% Found in: UDS
card_name('phyrexian monitor', 'Phyrexian Monitor').
card_type('phyrexian monitor', 'Creature — Skeleton').
card_types('phyrexian monitor', ['Creature']).
card_subtypes('phyrexian monitor', ['Skeleton']).
card_colors('phyrexian monitor', ['B']).
card_text('phyrexian monitor', '{B}: Regenerate Phyrexian Monitor.').
card_mana_cost('phyrexian monitor', ['3', 'B']).
card_cmc('phyrexian monitor', 4).
card_layout('phyrexian monitor', 'normal').
card_power('phyrexian monitor', 2).
card_toughness('phyrexian monitor', 2).

% Found in: DDE, UDS, pJGP
card_name('phyrexian negator', 'Phyrexian Negator').
card_type('phyrexian negator', 'Creature — Horror').
card_types('phyrexian negator', ['Creature']).
card_subtypes('phyrexian negator', ['Horror']).
card_colors('phyrexian negator', ['B']).
card_text('phyrexian negator', 'Trample\nWhenever Phyrexian Negator is dealt damage, sacrifice that many permanents.').
card_mana_cost('phyrexian negator', ['2', 'B']).
card_cmc('phyrexian negator', 3).
card_layout('phyrexian negator', 'normal').
card_power('phyrexian negator', 5).
card_toughness('phyrexian negator', 5).
card_reserved('phyrexian negator').

% Found in: NPH
card_name('phyrexian obliterator', 'Phyrexian Obliterator').
card_type('phyrexian obliterator', 'Creature — Horror').
card_types('phyrexian obliterator', ['Creature']).
card_subtypes('phyrexian obliterator', ['Horror']).
card_colors('phyrexian obliterator', ['B']).
card_text('phyrexian obliterator', 'Trample\nWhenever a source deals damage to Phyrexian Obliterator, that source\'s controller sacrifices that many permanents.').
card_mana_cost('phyrexian obliterator', ['B', 'B', 'B', 'B']).
card_cmc('phyrexian obliterator', 4).
card_layout('phyrexian obliterator', 'normal').
card_power('phyrexian obliterator', 5).
card_toughness('phyrexian obliterator', 5).

% Found in: 8ED, DDE, ULG
card_name('phyrexian plaguelord', 'Phyrexian Plaguelord').
card_type('phyrexian plaguelord', 'Creature — Carrier').
card_types('phyrexian plaguelord', ['Creature']).
card_subtypes('phyrexian plaguelord', ['Carrier']).
card_colors('phyrexian plaguelord', ['B']).
card_text('phyrexian plaguelord', '{T}, Sacrifice Phyrexian Plaguelord: Target creature gets -4/-4 until end of turn.\nSacrifice a creature: Target creature gets -1/-1 until end of turn.').
card_mana_cost('phyrexian plaguelord', ['3', 'B', 'B']).
card_cmc('phyrexian plaguelord', 5).
card_layout('phyrexian plaguelord', 'normal').
card_power('phyrexian plaguelord', 4).
card_toughness('phyrexian plaguelord', 4).

% Found in: ALL, ME2
card_name('phyrexian portal', 'Phyrexian Portal').
card_type('phyrexian portal', 'Artifact').
card_types('phyrexian portal', ['Artifact']).
card_subtypes('phyrexian portal', []).
card_colors('phyrexian portal', []).
card_text('phyrexian portal', '{3}: If your library has ten or more cards in it, target opponent looks at the top ten cards of your library and separates them into two face-down piles. Exile one of those piles. Search the other pile for a card, put it into your hand, then shuffle the rest of that pile into your library.').
card_mana_cost('phyrexian portal', ['3']).
card_cmc('phyrexian portal', 3).
card_layout('phyrexian portal', 'normal').
card_reserved('phyrexian portal').

% Found in: DDE, USG
card_name('phyrexian processor', 'Phyrexian Processor').
card_type('phyrexian processor', 'Artifact').
card_types('phyrexian processor', ['Artifact']).
card_subtypes('phyrexian processor', []).
card_colors('phyrexian processor', []).
card_text('phyrexian processor', 'As Phyrexian Processor enters the battlefield, pay any amount of life.\n{4}, {T}: Put an X/X black Minion creature token onto the battlefield, where X is the life paid as Phyrexian Processor entered the battlefield.').
card_mana_cost('phyrexian processor', ['4']).
card_cmc('phyrexian processor', 4).
card_layout('phyrexian processor', 'normal').

% Found in: NMS
card_name('phyrexian prowler', 'Phyrexian Prowler').
card_type('phyrexian prowler', 'Creature — Zombie Mercenary').
card_types('phyrexian prowler', ['Creature']).
card_subtypes('phyrexian prowler', ['Zombie', 'Mercenary']).
card_colors('phyrexian prowler', ['B']).
card_text('phyrexian prowler', 'Fading 3 (This creature enters the battlefield with three fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nRemove a fade counter from Phyrexian Prowler: Phyrexian Prowler gets +1/+1 until end of turn.').
card_mana_cost('phyrexian prowler', ['3', 'B']).
card_cmc('phyrexian prowler', 4).
card_layout('phyrexian prowler', 'normal').
card_power('phyrexian prowler', 3).
card_toughness('phyrexian prowler', 3).

% Found in: MIR
card_name('phyrexian purge', 'Phyrexian Purge').
card_type('phyrexian purge', 'Sorcery').
card_types('phyrexian purge', ['Sorcery']).
card_subtypes('phyrexian purge', []).
card_colors('phyrexian purge', ['B', 'R']).
card_text('phyrexian purge', 'Destroy any number of target creatures.\nPhyrexian Purge costs 3 life more to cast for each target.').
card_mana_cost('phyrexian purge', ['2', 'B', 'R']).
card_cmc('phyrexian purge', 4).
card_layout('phyrexian purge', 'normal').
card_reserved('phyrexian purge').

% Found in: 10E, APC, DD3_GVL, DDD, MBS, pMEI
card_name('phyrexian rager', 'Phyrexian Rager').
card_type('phyrexian rager', 'Creature — Horror').
card_types('phyrexian rager', ['Creature']).
card_subtypes('phyrexian rager', ['Horror']).
card_colors('phyrexian rager', ['B']).
card_text('phyrexian rager', 'When Phyrexian Rager enters the battlefield, you draw a card and you lose 1 life.').
card_mana_cost('phyrexian rager', ['2', 'B']).
card_cmc('phyrexian rager', 3).
card_layout('phyrexian rager', 'normal').
card_power('phyrexian rager', 2).
card_toughness('phyrexian rager', 2).

% Found in: INV
card_name('phyrexian reaper', 'Phyrexian Reaper').
card_type('phyrexian reaper', 'Creature — Zombie').
card_types('phyrexian reaper', ['Creature']).
card_subtypes('phyrexian reaper', ['Zombie']).
card_colors('phyrexian reaper', ['B']).
card_text('phyrexian reaper', 'Whenever Phyrexian Reaper becomes blocked by a green creature, destroy that creature. It can\'t be regenerated.').
card_mana_cost('phyrexian reaper', ['4', 'B']).
card_cmc('phyrexian reaper', 5).
card_layout('phyrexian reaper', 'normal').
card_power('phyrexian reaper', 3).
card_toughness('phyrexian reaper', 3).

% Found in: MBS
card_name('phyrexian rebirth', 'Phyrexian Rebirth').
card_type('phyrexian rebirth', 'Sorcery').
card_types('phyrexian rebirth', ['Sorcery']).
card_subtypes('phyrexian rebirth', []).
card_colors('phyrexian rebirth', ['W']).
card_text('phyrexian rebirth', 'Destroy all creatures, then put an X/X colorless Horror artifact creature token onto the battlefield, where X is the number of creatures destroyed this way.').
card_mana_cost('phyrexian rebirth', ['4', 'W', 'W']).
card_cmc('phyrexian rebirth', 6).
card_layout('phyrexian rebirth', 'normal').

% Found in: C13, ULG
card_name('phyrexian reclamation', 'Phyrexian Reclamation').
card_type('phyrexian reclamation', 'Enchantment').
card_types('phyrexian reclamation', ['Enchantment']).
card_subtypes('phyrexian reclamation', []).
card_colors('phyrexian reclamation', ['B']).
card_text('phyrexian reclamation', '{1}{B}, Pay 2 life: Return target creature card from your graveyard to your hand.').
card_mana_cost('phyrexian reclamation', ['B']).
card_cmc('phyrexian reclamation', 1).
card_layout('phyrexian reclamation', 'normal').

% Found in: M15, MBS
card_name('phyrexian revoker', 'Phyrexian Revoker').
card_type('phyrexian revoker', 'Artifact Creature — Horror').
card_types('phyrexian revoker', ['Artifact', 'Creature']).
card_subtypes('phyrexian revoker', ['Horror']).
card_colors('phyrexian revoker', []).
card_text('phyrexian revoker', 'As Phyrexian Revoker enters the battlefield, name a nonland card.\nActivated abilities of sources with the chosen name can\'t be activated.').
card_mana_cost('phyrexian revoker', ['2']).
card_cmc('phyrexian revoker', 2).
card_layout('phyrexian revoker', 'normal').
card_power('phyrexian revoker', 2).
card_toughness('phyrexian revoker', 1).

% Found in: PLS
card_name('phyrexian scuta', 'Phyrexian Scuta').
card_type('phyrexian scuta', 'Creature — Zombie').
card_types('phyrexian scuta', ['Creature']).
card_subtypes('phyrexian scuta', ['Zombie']).
card_colors('phyrexian scuta', ['B']).
card_text('phyrexian scuta', 'Kicker—Pay 3 life. (You may pay 3 life in addition to any other costs as you cast this spell.)\nIf Phyrexian Scuta was kicked, it enters the battlefield with two +1/+1 counters on it.').
card_mana_cost('phyrexian scuta', ['3', 'B']).
card_cmc('phyrexian scuta', 4).
card_layout('phyrexian scuta', 'normal').
card_power('phyrexian scuta', 3).
card_toughness('phyrexian scuta', 3).

% Found in: INV
card_name('phyrexian slayer', 'Phyrexian Slayer').
card_type('phyrexian slayer', 'Creature — Minion').
card_types('phyrexian slayer', ['Creature']).
card_subtypes('phyrexian slayer', ['Minion']).
card_colors('phyrexian slayer', ['B']).
card_text('phyrexian slayer', 'Flying\nWhenever Phyrexian Slayer becomes blocked by a white creature, destroy that creature. It can\'t be regenerated.').
card_mana_cost('phyrexian slayer', ['3', 'B']).
card_cmc('phyrexian slayer', 4).
card_layout('phyrexian slayer', 'normal').
card_power('phyrexian slayer', 2).
card_toughness('phyrexian slayer', 2).

% Found in: CSP
card_name('phyrexian snowcrusher', 'Phyrexian Snowcrusher').
card_type('phyrexian snowcrusher', 'Snow Artifact Creature — Juggernaut').
card_types('phyrexian snowcrusher', ['Artifact', 'Creature']).
card_subtypes('phyrexian snowcrusher', ['Juggernaut']).
card_supertypes('phyrexian snowcrusher', ['Snow']).
card_colors('phyrexian snowcrusher', []).
card_text('phyrexian snowcrusher', 'Phyrexian Snowcrusher attacks each turn if able.\n{1}{S}: Phyrexian Snowcrusher gets +1/+0 until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_mana_cost('phyrexian snowcrusher', ['6']).
card_cmc('phyrexian snowcrusher', 6).
card_layout('phyrexian snowcrusher', 'normal').
card_power('phyrexian snowcrusher', 6).
card_toughness('phyrexian snowcrusher', 5).

% Found in: CSP
card_name('phyrexian soulgorger', 'Phyrexian Soulgorger').
card_type('phyrexian soulgorger', 'Snow Artifact Creature — Construct').
card_types('phyrexian soulgorger', ['Artifact', 'Creature']).
card_subtypes('phyrexian soulgorger', ['Construct']).
card_supertypes('phyrexian soulgorger', ['Snow']).
card_colors('phyrexian soulgorger', []).
card_text('phyrexian soulgorger', 'Cumulative upkeep—Sacrifice a creature. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('phyrexian soulgorger', ['3']).
card_cmc('phyrexian soulgorger', 3).
card_layout('phyrexian soulgorger', 'normal').
card_power('phyrexian soulgorger', 8).
card_toughness('phyrexian soulgorger', 8).

% Found in: TMP
card_name('phyrexian splicer', 'Phyrexian Splicer').
card_type('phyrexian splicer', 'Artifact').
card_types('phyrexian splicer', ['Artifact']).
card_subtypes('phyrexian splicer', []).
card_colors('phyrexian splicer', []).
card_text('phyrexian splicer', '{2}, {T}, Choose flying, first strike, trample, or shadow: Until end of turn, target creature with the chosen ability loses it and another target creature gains it.').
card_mana_cost('phyrexian splicer', ['2']).
card_cmc('phyrexian splicer', 2).
card_layout('phyrexian splicer', 'normal').

% Found in: NPH
card_name('phyrexian swarmlord', 'Phyrexian Swarmlord').
card_type('phyrexian swarmlord', 'Creature — Insect Horror').
card_types('phyrexian swarmlord', ['Creature']).
card_subtypes('phyrexian swarmlord', ['Insect', 'Horror']).
card_colors('phyrexian swarmlord', ['G']).
card_text('phyrexian swarmlord', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nAt the beginning of your upkeep, put a 1/1 green Insect creature token with infect onto the battlefield for each poison counter your opponents have.').
card_mana_cost('phyrexian swarmlord', ['4', 'G', 'G']).
card_cmc('phyrexian swarmlord', 6).
card_layout('phyrexian swarmlord', 'normal').
card_power('phyrexian swarmlord', 4).
card_toughness('phyrexian swarmlord', 4).

% Found in: DDE, TSP
card_name('phyrexian totem', 'Phyrexian Totem').
card_type('phyrexian totem', 'Artifact').
card_types('phyrexian totem', ['Artifact']).
card_subtypes('phyrexian totem', []).
card_colors('phyrexian totem', []).
card_text('phyrexian totem', '{T}: Add {B} to your mana pool.\n{2}{B}: Phyrexian Totem becomes a 5/5 black Horror artifact creature with trample until end of turn.\nWhenever Phyrexian Totem is dealt damage, if it\'s a creature, sacrifice that many permanents.').
card_mana_cost('phyrexian totem', ['3']).
card_cmc('phyrexian totem', 3).
card_layout('phyrexian totem', 'normal').

% Found in: USG
card_name('phyrexian tower', 'Phyrexian Tower').
card_type('phyrexian tower', 'Legendary Land').
card_types('phyrexian tower', ['Land']).
card_subtypes('phyrexian tower', []).
card_supertypes('phyrexian tower', ['Legendary']).
card_colors('phyrexian tower', []).
card_text('phyrexian tower', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice a creature: Add {B}{B} to your mana pool.').
card_layout('phyrexian tower', 'normal').

% Found in: MIR
card_name('phyrexian tribute', 'Phyrexian Tribute').
card_type('phyrexian tribute', 'Sorcery').
card_types('phyrexian tribute', ['Sorcery']).
card_subtypes('phyrexian tribute', []).
card_colors('phyrexian tribute', ['B']).
card_text('phyrexian tribute', 'As an additional cost to cast Phyrexian Tribute, sacrifice two creatures.\nDestroy target artifact.').
card_mana_cost('phyrexian tribute', ['2', 'B']).
card_cmc('phyrexian tribute', 3).
card_layout('phyrexian tribute', 'normal').
card_reserved('phyrexian tribute').

% Found in: PLS
card_name('phyrexian tyranny', 'Phyrexian Tyranny').
card_type('phyrexian tyranny', 'Enchantment').
card_types('phyrexian tyranny', ['Enchantment']).
card_subtypes('phyrexian tyranny', []).
card_colors('phyrexian tyranny', ['U', 'B', 'R']).
card_text('phyrexian tyranny', 'Whenever a player draws a card, that player loses 2 life unless he or she pays {2}.').
card_mana_cost('phyrexian tyranny', ['U', 'B', 'R']).
card_cmc('phyrexian tyranny', 3).
card_layout('phyrexian tyranny', 'normal').

% Found in: NPH
card_name('phyrexian unlife', 'Phyrexian Unlife').
card_type('phyrexian unlife', 'Enchantment').
card_types('phyrexian unlife', ['Enchantment']).
card_subtypes('phyrexian unlife', []).
card_colors('phyrexian unlife', ['W']).
card_text('phyrexian unlife', 'You don\'t lose the game for having 0 or less life.\nAs long as you have 0 or less life, all damage is dealt to you as though its source had infect. (Damage is dealt to you in the form of poison counters.)').
card_mana_cost('phyrexian unlife', ['2', 'W']).
card_cmc('phyrexian unlife', 3).
card_layout('phyrexian unlife', 'normal').

% Found in: MBS
card_name('phyrexian vatmother', 'Phyrexian Vatmother').
card_type('phyrexian vatmother', 'Creature — Horror').
card_types('phyrexian vatmother', ['Creature']).
card_subtypes('phyrexian vatmother', ['Horror']).
card_colors('phyrexian vatmother', ['B']).
card_text('phyrexian vatmother', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nAt the beginning of your upkeep, you get a poison counter.').
card_mana_cost('phyrexian vatmother', ['2', 'B', 'B']).
card_cmc('phyrexian vatmother', 4).
card_layout('phyrexian vatmother', 'normal').
card_power('phyrexian vatmother', 4).
card_toughness('phyrexian vatmother', 5).

% Found in: 10E, 6ED, DDE, MIR
card_name('phyrexian vault', 'Phyrexian Vault').
card_type('phyrexian vault', 'Artifact').
card_types('phyrexian vault', ['Artifact']).
card_subtypes('phyrexian vault', []).
card_colors('phyrexian vault', []).
card_text('phyrexian vault', '{2}, {T}, Sacrifice a creature: Draw a card.').
card_mana_cost('phyrexian vault', ['3']).
card_cmc('phyrexian vault', 3).
card_layout('phyrexian vault', 'normal').

% Found in: VIS
card_name('phyrexian walker', 'Phyrexian Walker').
card_type('phyrexian walker', 'Artifact Creature — Construct').
card_types('phyrexian walker', ['Artifact', 'Creature']).
card_subtypes('phyrexian walker', ['Construct']).
card_colors('phyrexian walker', []).
card_text('phyrexian walker', '').
card_mana_cost('phyrexian walker', ['0']).
card_cmc('phyrexian walker', 0).
card_layout('phyrexian walker', 'normal').
card_power('phyrexian walker', 0).
card_toughness('phyrexian walker', 3).

% Found in: ALL, DKM, MED
card_name('phyrexian war beast', 'Phyrexian War Beast').
card_type('phyrexian war beast', 'Artifact Creature — Beast').
card_types('phyrexian war beast', ['Artifact', 'Creature']).
card_subtypes('phyrexian war beast', ['Beast']).
card_colors('phyrexian war beast', []).
card_text('phyrexian war beast', 'When Phyrexian War Beast leaves the battlefield, sacrifice a land and Phyrexian War Beast deals 1 damage to you.').
card_mana_cost('phyrexian war beast', ['3']).
card_cmc('phyrexian war beast', 3).
card_layout('phyrexian war beast', 'normal').
card_power('phyrexian war beast', 3).
card_toughness('phyrexian war beast', 4).

% Found in: DGM
card_name('phytoburst', 'Phytoburst').
card_type('phytoburst', 'Sorcery').
card_types('phytoburst', ['Sorcery']).
card_subtypes('phytoburst', []).
card_colors('phytoburst', ['G']).
card_text('phytoburst', 'Target creature gets +5/+5 until end of turn.').
card_mana_cost('phytoburst', ['1', 'G']).
card_cmc('phytoburst', 2).
card_layout('phytoburst', 'normal').

% Found in: RAV
card_name('phytohydra', 'Phytohydra').
card_type('phytohydra', 'Creature — Plant Hydra').
card_types('phytohydra', ['Creature']).
card_subtypes('phytohydra', ['Plant', 'Hydra']).
card_colors('phytohydra', ['W', 'G']).
card_text('phytohydra', 'If damage would be dealt to Phytohydra, put that many +1/+1 counters on it instead.').
card_mana_cost('phytohydra', ['2', 'G', 'W', 'W']).
card_cmc('phytohydra', 5).
card_layout('phytohydra', 'normal').
card_power('phytohydra', 1).
card_toughness('phytohydra', 1).

% Found in: M15, pPRE
card_name('phytotitan', 'Phytotitan').
card_type('phytotitan', 'Creature — Plant Elemental').
card_types('phytotitan', ['Creature']).
card_subtypes('phytotitan', ['Plant', 'Elemental']).
card_colors('phytotitan', ['G']).
card_text('phytotitan', 'When Phytotitan dies, return it to the battlefield tapped under its owner\'s control at the beginning of his or her next upkeep.').
card_mana_cost('phytotitan', ['4', 'G', 'G']).
card_cmc('phytotitan', 6).
card_layout('phytotitan', 'normal').
card_power('phytotitan', 7).
card_toughness('phytotitan', 2).

% Found in: ORI
card_name('pia and kiran nalaar', 'Pia and Kiran Nalaar').
card_type('pia and kiran nalaar', 'Legendary Creature — Human Artificer').
card_types('pia and kiran nalaar', ['Creature']).
card_subtypes('pia and kiran nalaar', ['Human', 'Artificer']).
card_supertypes('pia and kiran nalaar', ['Legendary']).
card_colors('pia and kiran nalaar', ['R']).
card_text('pia and kiran nalaar', 'When Pia and Kiran Nalaar enters the battlefield, put two 1/1 colorless Thopter artifact creature tokens with flying onto the battlefield.\n{2}{R}, Sacrifice an artifact: Pia and Kiran Nalaar deals 2 damage to target creature or player.').
card_mana_cost('pia and kiran nalaar', ['2', 'R', 'R']).
card_cmc('pia and kiran nalaar', 4).
card_layout('pia and kiran nalaar', 'normal').
card_power('pia and kiran nalaar', 2).
card_toughness('pia and kiran nalaar', 2).

% Found in: ODY, VMA
card_name('pianna, nomad captain', 'Pianna, Nomad Captain').
card_type('pianna, nomad captain', 'Legendary Creature — Human Nomad').
card_types('pianna, nomad captain', ['Creature']).
card_subtypes('pianna, nomad captain', ['Human', 'Nomad']).
card_supertypes('pianna, nomad captain', ['Legendary']).
card_colors('pianna, nomad captain', ['W']).
card_text('pianna, nomad captain', 'Whenever Pianna, Nomad Captain attacks, attacking creatures get +1/+1 until end of turn.').
card_mana_cost('pianna, nomad captain', ['1', 'W', 'W']).
card_cmc('pianna, nomad captain', 3).
card_layout('pianna, nomad captain', 'normal').
card_power('pianna, nomad captain', 2).
card_toughness('pianna, nomad captain', 2).

% Found in: MBS
card_name('pierce strider', 'Pierce Strider').
card_type('pierce strider', 'Artifact Creature — Construct').
card_types('pierce strider', ['Artifact', 'Creature']).
card_subtypes('pierce strider', ['Construct']).
card_colors('pierce strider', []).
card_text('pierce strider', 'When Pierce Strider enters the battlefield, target opponent loses 3 life.').
card_mana_cost('pierce strider', ['4']).
card_cmc('pierce strider', 4).
card_layout('pierce strider', 'normal').
card_power('pierce strider', 3).
card_toughness('pierce strider', 3).

% Found in: 4ED, ARN
card_name('piety', 'Piety').
card_type('piety', 'Instant').
card_types('piety', ['Instant']).
card_subtypes('piety', []).
card_colors('piety', ['W']).
card_text('piety', 'Blocking creatures get +0/+3 until end of turn.').
card_mana_cost('piety', ['2', 'W']).
card_cmc('piety', 3).
card_layout('piety', 'normal').

% Found in: ONS
card_name('piety charm', 'Piety Charm').
card_type('piety charm', 'Instant').
card_types('piety charm', ['Instant']).
card_subtypes('piety charm', []).
card_colors('piety charm', ['W']).
card_text('piety charm', 'Choose one —\n• Destroy target Aura attached to a creature.\n• Target Soldier creature gets +2/+2 until end of turn.\n• Creatures you control gain vigilance until end of turn.').
card_mana_cost('piety charm', ['W']).
card_cmc('piety charm', 1).
card_layout('piety charm', 'normal').

% Found in: 4ED, 5ED, DRK
card_name('pikemen', 'Pikemen').
card_type('pikemen', 'Creature — Human Soldier').
card_types('pikemen', ['Creature']).
card_subtypes('pikemen', ['Human', 'Soldier']).
card_colors('pikemen', ['W']).
card_text('pikemen', 'First strike; banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('pikemen', ['1', 'W']).
card_cmc('pikemen', 2).
card_layout('pikemen', 'normal').
card_power('pikemen', 1).
card_toughness('pikemen', 1).

% Found in: DGM
card_name('pilfered plans', 'Pilfered Plans').
card_type('pilfered plans', 'Sorcery').
card_types('pilfered plans', ['Sorcery']).
card_subtypes('pilfered plans', []).
card_colors('pilfered plans', ['U', 'B']).
card_text('pilfered plans', 'Target player puts the top two cards of his or her library into his or her graveyard. Draw two cards.').
card_mana_cost('pilfered plans', ['1', 'U', 'B']).
card_cmc('pilfered plans', 3).
card_layout('pilfered plans', 'normal').

% Found in: ODY
card_name('pilgrim of justice', 'Pilgrim of Justice').
card_type('pilgrim of justice', 'Creature — Human Cleric').
card_types('pilgrim of justice', ['Creature']).
card_subtypes('pilgrim of justice', ['Human', 'Cleric']).
card_colors('pilgrim of justice', ['W']).
card_text('pilgrim of justice', 'Protection from red\n{W}, Sacrifice Pilgrim of Justice: The next time a red source of your choice would deal damage this turn, prevent that damage.').
card_mana_cost('pilgrim of justice', ['2', 'W']).
card_cmc('pilgrim of justice', 3).
card_layout('pilgrim of justice', 'normal').
card_power('pilgrim of justice', 1).
card_toughness('pilgrim of justice', 3).

% Found in: FRF
card_name('pilgrim of the fires', 'Pilgrim of the Fires').
card_type('pilgrim of the fires', 'Artifact Creature — Golem').
card_types('pilgrim of the fires', ['Artifact', 'Creature']).
card_subtypes('pilgrim of the fires', ['Golem']).
card_colors('pilgrim of the fires', []).
card_text('pilgrim of the fires', 'First strike, trample').
card_mana_cost('pilgrim of the fires', ['7']).
card_cmc('pilgrim of the fires', 7).
card_layout('pilgrim of the fires', 'normal').
card_power('pilgrim of the fires', 6).
card_toughness('pilgrim of the fires', 4).

% Found in: ODY
card_name('pilgrim of virtue', 'Pilgrim of Virtue').
card_type('pilgrim of virtue', 'Creature — Human Cleric').
card_types('pilgrim of virtue', ['Creature']).
card_subtypes('pilgrim of virtue', ['Human', 'Cleric']).
card_colors('pilgrim of virtue', ['W']).
card_text('pilgrim of virtue', 'Protection from black\n{W}, Sacrifice Pilgrim of Virtue: The next time a black source of your choice would deal damage this turn, prevent that damage.').
card_mana_cost('pilgrim of virtue', ['2', 'W']).
card_cmc('pilgrim of virtue', 3).
card_layout('pilgrim of virtue', 'normal').
card_power('pilgrim of virtue', 1).
card_toughness('pilgrim of virtue', 3).

% Found in: BFZ, C13, C14, DDI, WWK
card_name('pilgrim\'s eye', 'Pilgrim\'s Eye').
card_type('pilgrim\'s eye', 'Artifact Creature — Thopter').
card_types('pilgrim\'s eye', ['Artifact', 'Creature']).
card_subtypes('pilgrim\'s eye', ['Thopter']).
card_colors('pilgrim\'s eye', []).
card_text('pilgrim\'s eye', 'Flying\nWhen Pilgrim\'s Eye enters the battlefield, you may search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.').
card_mana_cost('pilgrim\'s eye', ['3']).
card_cmc('pilgrim\'s eye', 3).
card_layout('pilgrim\'s eye', 'normal').
card_power('pilgrim\'s eye', 1).
card_toughness('pilgrim\'s eye', 1).

% Found in: SHM
card_name('pili-pala', 'Pili-Pala').
card_type('pili-pala', 'Artifact Creature — Scarecrow').
card_types('pili-pala', ['Artifact', 'Creature']).
card_subtypes('pili-pala', ['Scarecrow']).
card_colors('pili-pala', []).
card_text('pili-pala', 'Flying\n{2}, {Q}: Add one mana of any color to your mana pool. ({Q} is the untap symbol.)').
card_mana_cost('pili-pala', ['2']).
card_cmc('pili-pala', 2).
card_layout('pili-pala', 'normal').
card_power('pili-pala', 1).
card_toughness('pili-pala', 1).

% Found in: 6ED, 7ED, ALL, DKM, ME2, PD2, pARL
card_name('pillage', 'Pillage').
card_type('pillage', 'Sorcery').
card_types('pillage', ['Sorcery']).
card_subtypes('pillage', []).
card_colors('pillage', ['R']).
card_text('pillage', 'Destroy target artifact or land. It can\'t be regenerated.').
card_mana_cost('pillage', ['1', 'R', 'R']).
card_cmc('pillage', 3).
card_layout('pillage', 'normal').

% Found in: POR, VMA
card_name('pillaging horde', 'Pillaging Horde').
card_type('pillaging horde', 'Creature — Human Barbarian').
card_types('pillaging horde', ['Creature']).
card_subtypes('pillaging horde', ['Human', 'Barbarian']).
card_colors('pillaging horde', ['R']).
card_text('pillaging horde', 'When Pillaging Horde enters the battlefield, sacrifice it unless you discard a card at random.').
card_mana_cost('pillaging horde', ['2', 'R', 'R']).
card_cmc('pillaging horde', 4).
card_layout('pillaging horde', 'normal').
card_power('pillaging horde', 5).
card_toughness('pillaging horde', 5).

% Found in: AVR, pFNM
card_name('pillar of flame', 'Pillar of Flame').
card_type('pillar of flame', 'Sorcery').
card_types('pillar of flame', ['Sorcery']).
card_subtypes('pillar of flame', []).
card_colors('pillar of flame', ['R']).
card_text('pillar of flame', 'Pillar of Flame deals 2 damage to target creature or player. If a creature dealt damage this way would die this turn, exile it instead.').
card_mana_cost('pillar of flame', ['R']).
card_cmc('pillar of flame', 1).
card_layout('pillar of flame', 'normal').

% Found in: M15
card_name('pillar of light', 'Pillar of Light').
card_type('pillar of light', 'Instant').
card_types('pillar of light', ['Instant']).
card_subtypes('pillar of light', []).
card_colors('pillar of light', ['W']).
card_text('pillar of light', 'Exile target creature with toughness 4 or greater.').
card_mana_cost('pillar of light', ['2', 'W']).
card_cmc('pillar of light', 3).
card_layout('pillar of light', 'normal').

% Found in: DIS
card_name('pillar of the paruns', 'Pillar of the Paruns').
card_type('pillar of the paruns', 'Land').
card_types('pillar of the paruns', ['Land']).
card_subtypes('pillar of the paruns', []).
card_colors('pillar of the paruns', []).
card_text('pillar of the paruns', '{T}: Add one mana of any color to your mana pool. Spend this mana only to cast a multicolored spell.').
card_layout('pillar of the paruns', 'normal').

% Found in: BNG
card_name('pillar of war', 'Pillar of War').
card_type('pillar of war', 'Artifact Creature — Golem').
card_types('pillar of war', ['Artifact', 'Creature']).
card_subtypes('pillar of war', ['Golem']).
card_colors('pillar of war', []).
card_text('pillar of war', 'Defender\nAs long as Pillar of War is enchanted, it can attack as though it didn\'t have defender.').
card_mana_cost('pillar of war', ['3']).
card_cmc('pillar of war', 3).
card_layout('pillar of war', 'normal').
card_power('pillar of war', 3).
card_toughness('pillar of war', 3).

% Found in: VIS
card_name('pillar tombs of aku', 'Pillar Tombs of Aku').
card_type('pillar tombs of aku', 'World Enchantment').
card_types('pillar tombs of aku', ['Enchantment']).
card_subtypes('pillar tombs of aku', []).
card_supertypes('pillar tombs of aku', ['World']).
card_colors('pillar tombs of aku', ['B']).
card_text('pillar tombs of aku', 'At the beginning of each player\'s upkeep, that player may sacrifice a creature. If that player doesn\'t, he or she loses 5 life and you sacrifice Pillar Tombs of Aku.').
card_mana_cost('pillar tombs of aku', ['2', 'B', 'B']).
card_cmc('pillar tombs of aku', 4).
card_layout('pillar tombs of aku', 'normal').
card_reserved('pillar tombs of aku').

% Found in: CNS, M13, M14, ZEN
card_name('pillarfield ox', 'Pillarfield Ox').
card_type('pillarfield ox', 'Creature — Ox').
card_types('pillarfield ox', ['Creature']).
card_subtypes('pillarfield ox', ['Ox']).
card_colors('pillarfield ox', ['W']).
card_text('pillarfield ox', '').
card_mana_cost('pillarfield ox', ['3', 'W']).
card_cmc('pillarfield ox', 4).
card_layout('pillarfield ox', 'normal').
card_power('pillarfield ox', 2).
card_toughness('pillarfield ox', 4).

% Found in: GPT, MM2
card_name('pillory of the sleepless', 'Pillory of the Sleepless').
card_type('pillory of the sleepless', 'Enchantment — Aura').
card_types('pillory of the sleepless', ['Enchantment']).
card_subtypes('pillory of the sleepless', ['Aura']).
card_colors('pillory of the sleepless', ['W', 'B']).
card_text('pillory of the sleepless', 'Enchant creature\nEnchanted creature can\'t attack or block.\nEnchanted creature has \"At the beginning of your upkeep, you lose 1 life.\"').
card_mana_cost('pillory of the sleepless', ['1', 'W', 'B']).
card_cmc('pillory of the sleepless', 3).
card_layout('pillory of the sleepless', 'normal').

% Found in: JOU
card_name('pin to the earth', 'Pin to the Earth').
card_type('pin to the earth', 'Enchantment — Aura').
card_types('pin to the earth', ['Enchantment']).
card_subtypes('pin to the earth', ['Aura']).
card_colors('pin to the earth', ['U']).
card_text('pin to the earth', 'Enchant creature\nEnchanted creature gets -6/-0.').
card_mana_cost('pin to the earth', ['1', 'U']).
card_cmc('pin to the earth', 2).
card_layout('pin to the earth', 'normal').

% Found in: INV
card_name('pincer spider', 'Pincer Spider').
card_type('pincer spider', 'Creature — Spider').
card_types('pincer spider', ['Creature']).
card_subtypes('pincer spider', ['Spider']).
card_colors('pincer spider', ['G']).
card_text('pincer spider', 'Kicker {3} (You may pay an additional {3} as you cast this spell.)\nReach (This creature can block creatures with flying.)\nIf Pincer Spider was kicked, it enters the battlefield with a +1/+1 counter on it.').
card_mana_cost('pincer spider', ['2', 'G']).
card_cmc('pincer spider', 3).
card_layout('pincer spider', 'normal').
card_power('pincer spider', 2).
card_toughness('pincer spider', 3).

% Found in: 10E, BRB, TMP
card_name('pincher beetles', 'Pincher Beetles').
card_type('pincher beetles', 'Creature — Insect').
card_types('pincher beetles', ['Creature']).
card_subtypes('pincher beetles', ['Insect']).
card_colors('pincher beetles', ['G']).
card_text('pincher beetles', 'Shroud (This creature can\'t be the target of spells or abilities.)').
card_mana_cost('pincher beetles', ['2', 'G']).
card_cmc('pincher beetles', 3).
card_layout('pincher beetles', 'normal').
card_power('pincher beetles', 3).
card_toughness('pincher beetles', 1).

% Found in: TMP, TPR, VMA
card_name('pine barrens', 'Pine Barrens').
card_type('pine barrens', 'Land').
card_types('pine barrens', ['Land']).
card_subtypes('pine barrens', []).
card_colors('pine barrens', []).
card_text('pine barrens', 'Pine Barrens enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add {B} or {G} to your mana pool. Pine Barrens deals 1 damage to you.').
card_layout('pine barrens', 'normal').

% Found in: KTK
card_name('pine walker', 'Pine Walker').
card_type('pine walker', 'Creature — Elemental').
card_types('pine walker', ['Creature']).
card_subtypes('pine walker', ['Elemental']).
card_colors('pine walker', ['G']).
card_text('pine walker', 'Morph {4}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhenever Pine Walker or another creature you control is turned face up, untap that creature.').
card_mana_cost('pine walker', ['3', 'G', 'G']).
card_cmc('pine walker', 5).
card_layout('pine walker', 'normal').
card_power('pine walker', 5).
card_toughness('pine walker', 5).

% Found in: CHK
card_name('pinecrest ridge', 'Pinecrest Ridge').
card_type('pinecrest ridge', 'Land').
card_types('pinecrest ridge', ['Land']).
card_subtypes('pinecrest ridge', []).
card_colors('pinecrest ridge', []).
card_text('pinecrest ridge', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Pinecrest Ridge doesn\'t untap during your next untap step.').
card_layout('pinecrest ridge', 'normal').

% Found in: DTK
card_name('pinion feast', 'Pinion Feast').
card_type('pinion feast', 'Instant').
card_types('pinion feast', ['Instant']).
card_subtypes('pinion feast', []).
card_colors('pinion feast', ['G']).
card_text('pinion feast', 'Destroy target creature with flying. Bolster 2. (Choose a creature with the least toughness among creatures you control and put two +1/+1 counters on it.)').
card_mana_cost('pinion feast', ['4', 'G']).
card_cmc('pinion feast', 5).
card_layout('pinion feast', 'normal').

% Found in: BNG
card_name('pinnacle of rage', 'Pinnacle of Rage').
card_type('pinnacle of rage', 'Sorcery').
card_types('pinnacle of rage', ['Sorcery']).
card_subtypes('pinnacle of rage', []).
card_colors('pinnacle of rage', ['R']).
card_text('pinnacle of rage', 'Pinnacle of Rage deals 3 damage to each of two target creatures and/or players.').
card_mana_cost('pinnacle of rage', ['4', 'R', 'R']).
card_cmc('pinnacle of rage', 6).
card_layout('pinnacle of rage', 'normal').

% Found in: ONS
card_name('pinpoint avalanche', 'Pinpoint Avalanche').
card_type('pinpoint avalanche', 'Instant').
card_types('pinpoint avalanche', ['Instant']).
card_subtypes('pinpoint avalanche', []).
card_colors('pinpoint avalanche', ['R']).
card_text('pinpoint avalanche', 'Pinpoint Avalanche deals 4 damage to target creature. The damage can\'t be prevented.').
card_mana_cost('pinpoint avalanche', ['3', 'R', 'R']).
card_cmc('pinpoint avalanche', 5).
card_layout('pinpoint avalanche', 'normal').

% Found in: CHK
card_name('pious kitsune', 'Pious Kitsune').
card_type('pious kitsune', 'Creature — Fox Cleric').
card_types('pious kitsune', ['Creature']).
card_subtypes('pious kitsune', ['Fox', 'Cleric']).
card_colors('pious kitsune', ['W']).
card_text('pious kitsune', 'At the beginning of your upkeep, put a devotion counter on Pious Kitsune. Then if a creature named Eight-and-a-Half-Tails is on the battlefield, you gain 1 life for each devotion counter on Pious Kitsune.\n{T}, Remove a devotion counter from Pious Kitsune: You gain 1 life.').
card_mana_cost('pious kitsune', ['2', 'W']).
card_cmc('pious kitsune', 3).
card_layout('pious kitsune', 'normal').
card_power('pious kitsune', 1).
card_toughness('pious kitsune', 2).

% Found in: MMQ
card_name('pious warrior', 'Pious Warrior').
card_type('pious warrior', 'Creature — Human Rebel Warrior').
card_types('pious warrior', ['Creature']).
card_subtypes('pious warrior', ['Human', 'Rebel', 'Warrior']).
card_colors('pious warrior', ['W']).
card_text('pious warrior', 'Whenever Pious Warrior is dealt combat damage, you gain that much life.').
card_mana_cost('pious warrior', ['3', 'W']).
card_cmc('pious warrior', 4).
card_layout('pious warrior', 'normal').
card_power('pious warrior', 2).
card_toughness('pious warrior', 3).

% Found in: ODY
card_name('piper\'s melody', 'Piper\'s Melody').
card_type('piper\'s melody', 'Sorcery').
card_types('piper\'s melody', ['Sorcery']).
card_subtypes('piper\'s melody', []).
card_colors('piper\'s melody', ['G']).
card_text('piper\'s melody', 'Shuffle any number of target creature cards from your graveyard into your library.').
card_mana_cost('piper\'s melody', ['G']).
card_cmc('piper\'s melody', 1).
card_layout('piper\'s melody', 'normal').

% Found in: PO2, S99
card_name('piracy', 'Piracy').
card_type('piracy', 'Sorcery').
card_types('piracy', ['Sorcery']).
card_subtypes('piracy', []).
card_colors('piracy', ['U']).
card_text('piracy', 'Until end of turn, you may tap lands you don\'t control for mana. Spend this mana only to cast spells.').
card_mana_cost('piracy', ['U', 'U']).
card_cmc('piracy', 2).
card_layout('piracy', 'normal').

% Found in: PLC
card_name('piracy charm', 'Piracy Charm').
card_type('piracy charm', 'Instant').
card_types('piracy charm', ['Instant']).
card_subtypes('piracy charm', []).
card_colors('piracy charm', ['U']).
card_text('piracy charm', 'Choose one —\n• Target creature gains islandwalk until end of turn. (It can\'t be blocked as long as defending player controls an Island.)\n• Target creature gets +2/-1 until end of turn.\n• Target player discards a card.').
card_mana_cost('piracy charm', ['U']).
card_cmc('piracy charm', 1).
card_layout('piracy charm', 'normal').

% Found in: ZEN
card_name('piranha marsh', 'Piranha Marsh').
card_type('piranha marsh', 'Land').
card_types('piranha marsh', ['Land']).
card_subtypes('piranha marsh', []).
card_colors('piranha marsh', []).
card_text('piranha marsh', 'Piranha Marsh enters the battlefield tapped.\nWhen Piranha Marsh enters the battlefield, target player loses 1 life.\n{T}: Add {B} to your mana pool.').
card_layout('piranha marsh', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, TSB
card_name('pirate ship', 'Pirate Ship').
card_type('pirate ship', 'Creature — Human Pirate').
card_types('pirate ship', ['Creature']).
card_subtypes('pirate ship', ['Human', 'Pirate']).
card_colors('pirate ship', ['U']).
card_text('pirate ship', 'Pirate Ship can\'t attack unless defending player controls an Island.\n{T}: Pirate Ship deals 1 damage to target creature or player.\nWhen you control no Islands, sacrifice Pirate Ship.').
card_mana_cost('pirate ship', ['4', 'U']).
card_cmc('pirate ship', 5).
card_layout('pirate ship', 'normal').
card_power('pirate ship', 4).
card_toughness('pirate ship', 3).

% Found in: MBS
card_name('piston sledge', 'Piston Sledge').
card_type('piston sledge', 'Artifact — Equipment').
card_types('piston sledge', ['Artifact']).
card_subtypes('piston sledge', ['Equipment']).
card_colors('piston sledge', []).
card_text('piston sledge', 'When Piston Sledge enters the battlefield, attach it to target creature you control.\nEquipped creature gets +3/+1.\nEquip—Sacrifice an artifact.').
card_mana_cost('piston sledge', ['3']).
card_cmc('piston sledge', 3).
card_layout('piston sledge', 'normal').

% Found in: MBS
card_name('pistus strike', 'Pistus Strike').
card_type('pistus strike', 'Instant').
card_types('pistus strike', ['Instant']).
card_subtypes('pistus strike', []).
card_colors('pistus strike', ['G']).
card_text('pistus strike', 'Destroy target creature with flying. Its controller gets a poison counter.').
card_mana_cost('pistus strike', ['2', 'G']).
card_cmc('pistus strike', 3).
card_layout('pistus strike', 'normal').

% Found in: GTC
card_name('pit fight', 'Pit Fight').
card_type('pit fight', 'Instant').
card_types('pit fight', ['Instant']).
card_subtypes('pit fight', []).
card_colors('pit fight', ['R', 'G']).
card_text('pit fight', 'Target creature you control fights another target creature. (Each deals damage equal to its power to the other.)').
card_mana_cost('pit fight', ['1', 'R/G']).
card_cmc('pit fight', 2).
card_layout('pit fight', 'normal').

% Found in: TMP
card_name('pit imp', 'Pit Imp').
card_type('pit imp', 'Creature — Imp').
card_types('pit imp', ['Creature']).
card_subtypes('pit imp', ['Imp']).
card_colors('pit imp', ['B']).
card_text('pit imp', 'Flying\n{B}: Pit Imp gets +1/+0 until end of turn. Activate this ability no more than twice each turn.').
card_mana_cost('pit imp', ['B']).
card_cmc('pit imp', 1).
card_layout('pit imp', 'normal').
card_power('pit imp', 0).
card_toughness('pit imp', 1).

% Found in: TSP
card_name('pit keeper', 'Pit Keeper').
card_type('pit keeper', 'Creature — Human Wizard').
card_types('pit keeper', ['Creature']).
card_subtypes('pit keeper', ['Human', 'Wizard']).
card_colors('pit keeper', ['B']).
card_text('pit keeper', 'When Pit Keeper enters the battlefield, if you have four or more creature cards in your graveyard, you may return target creature card from your graveyard to your hand.').
card_mana_cost('pit keeper', ['1', 'B']).
card_cmc('pit keeper', 2).
card_layout('pit keeper', 'normal').
card_power('pit keeper', 2).
card_toughness('pit keeper', 1).

% Found in: PCY
card_name('pit raptor', 'Pit Raptor').
card_type('pit raptor', 'Creature — Bird Mercenary').
card_types('pit raptor', ['Creature']).
card_subtypes('pit raptor', ['Bird', 'Mercenary']).
card_colors('pit raptor', ['B']).
card_text('pit raptor', 'Flying, first strike\nAt the beginning of your upkeep, sacrifice Pit Raptor unless you pay {2}{B}{B}.').
card_mana_cost('pit raptor', ['2', 'B', 'B']).
card_cmc('pit raptor', 4).
card_layout('pit raptor', 'normal').
card_power('pit raptor', 4).
card_toughness('pit raptor', 3).

% Found in: 4ED, 5ED, LEG
card_name('pit scorpion', 'Pit Scorpion').
card_type('pit scorpion', 'Creature — Scorpion').
card_types('pit scorpion', ['Creature']).
card_subtypes('pit scorpion', ['Scorpion']).
card_colors('pit scorpion', ['B']).
card_text('pit scorpion', 'Whenever Pit Scorpion deals damage to a player, that player gets a poison counter. (A player with ten or more poison counters loses the game.)').
card_mana_cost('pit scorpion', ['2', 'B']).
card_cmc('pit scorpion', 3).
card_layout('pit scorpion', 'normal').
card_power('pit scorpion', 1).
card_toughness('pit scorpion', 1).

% Found in: EXO
card_name('pit spawn', 'Pit Spawn').
card_type('pit spawn', 'Creature — Demon').
card_types('pit spawn', ['Creature']).
card_subtypes('pit spawn', ['Demon']).
card_colors('pit spawn', ['B']).
card_text('pit spawn', 'First strike\nAt the beginning of your upkeep, sacrifice Pit Spawn unless you pay {B}{B}.\nWhenever Pit Spawn deals damage to a creature, exile that creature.').
card_mana_cost('pit spawn', ['4', 'B', 'B', 'B']).
card_cmc('pit spawn', 7).
card_layout('pit spawn', 'normal').
card_power('pit spawn', 6).
card_toughness('pit spawn', 4).

% Found in: 7ED, ICE, USG
card_name('pit trap', 'Pit Trap').
card_type('pit trap', 'Artifact').
card_types('pit trap', ['Artifact']).
card_subtypes('pit trap', []).
card_colors('pit trap', []).
card_text('pit trap', '{2}, {T}, Sacrifice Pit Trap: Destroy target attacking creature without flying. It can\'t be regenerated.').
card_mana_cost('pit trap', ['2']).
card_cmc('pit trap', 2).
card_layout('pit trap', 'normal').

% Found in: CNS, ISD, M14
card_name('pitchburn devils', 'Pitchburn Devils').
card_type('pitchburn devils', 'Creature — Devil').
card_types('pitchburn devils', ['Creature']).
card_subtypes('pitchburn devils', ['Devil']).
card_colors('pitchburn devils', ['R']).
card_text('pitchburn devils', 'When Pitchburn Devils dies, it deals 3 damage to target creature or player.').
card_mana_cost('pitchburn devils', ['4', 'R']).
card_cmc('pitchburn devils', 5).
card_layout('pitchburn devils', 'normal').
card_power('pitchburn devils', 3).
card_toughness('pitchburn devils', 3).

% Found in: TOR
card_name('pitchstone wall', 'Pitchstone Wall').
card_type('pitchstone wall', 'Creature — Wall').
card_types('pitchstone wall', ['Creature']).
card_subtypes('pitchstone wall', ['Wall']).
card_colors('pitchstone wall', ['R']).
card_text('pitchstone wall', 'Defender (This creature can\'t attack.)\nWhenever you discard a card, you may sacrifice Pitchstone Wall. If you do, return the discarded card from your graveyard to your hand.').
card_mana_cost('pitchstone wall', ['2', 'R']).
card_cmc('pitchstone wall', 3).
card_layout('pitchstone wall', 'normal').
card_power('pitchstone wall', 2).
card_toughness('pitchstone wall', 5).

% Found in: ZEN
card_name('pitfall trap', 'Pitfall Trap').
card_type('pitfall trap', 'Instant — Trap').
card_types('pitfall trap', ['Instant']).
card_subtypes('pitfall trap', ['Trap']).
card_colors('pitfall trap', ['W']).
card_text('pitfall trap', 'If exactly one creature is attacking, you may pay {W} rather than pay Pitfall Trap\'s mana cost.\nDestroy target attacking creature without flying.').
card_mana_cost('pitfall trap', ['2', 'W']).
card_cmc('pitfall trap', 3).
card_layout('pitfall trap', 'normal').

% Found in: NPH
card_name('pith driller', 'Pith Driller').
card_type('pith driller', 'Artifact Creature — Horror').
card_types('pith driller', ['Artifact', 'Creature']).
card_subtypes('pith driller', ['Horror']).
card_colors('pith driller', ['B']).
card_text('pith driller', '({B/P} can be paid with either {B} or 2 life.)\nWhen Pith Driller enters the battlefield, put a -1/-1 counter on target creature.').
card_mana_cost('pith driller', ['4', 'B/P']).
card_cmc('pith driller', 5).
card_layout('pith driller', 'normal').
card_power('pith driller', 2).
card_toughness('pith driller', 4).

% Found in: 10E, M10, RTR, SOK
card_name('pithing needle', 'Pithing Needle').
card_type('pithing needle', 'Artifact').
card_types('pithing needle', ['Artifact']).
card_subtypes('pithing needle', []).
card_colors('pithing needle', []).
card_text('pithing needle', 'As Pithing Needle enters the battlefield, name a card.\nActivated abilities of sources with the chosen name can\'t be activated unless they\'re mana abilities.').
card_mana_cost('pithing needle', ['1']).
card_cmc('pithing needle', 1).
card_layout('pithing needle', 'normal').

% Found in: DTK
card_name('pitiless horde', 'Pitiless Horde').
card_type('pitiless horde', 'Creature — Orc Berserker').
card_types('pitiless horde', ['Creature']).
card_subtypes('pitiless horde', ['Orc', 'Berserker']).
card_colors('pitiless horde', ['B']).
card_text('pitiless horde', 'At the beginning of your upkeep, you lose 2 life.\nDash {2}{B}{B} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('pitiless horde', ['2', 'B']).
card_cmc('pitiless horde', 3).
card_layout('pitiless horde', 'normal').
card_power('pitiless horde', 5).
card_toughness('pitiless horde', 3).

% Found in: LEG
card_name('pixie queen', 'Pixie Queen').
card_type('pixie queen', 'Creature — Faerie').
card_types('pixie queen', ['Creature']).
card_subtypes('pixie queen', ['Faerie']).
card_colors('pixie queen', ['G']).
card_text('pixie queen', 'Flying\n{G}{G}{G}, {T}: Target creature gains flying until end of turn.').
card_mana_cost('pixie queen', ['2', 'G', 'G']).
card_cmc('pixie queen', 4).
card_layout('pixie queen', 'normal').
card_power('pixie queen', 1).
card_toughness('pixie queen', 1).
card_reserved('pixie queen').

% Found in: 10E, 9ED, TOR
card_name('plagiarize', 'Plagiarize').
card_type('plagiarize', 'Instant').
card_types('plagiarize', ['Instant']).
card_subtypes('plagiarize', []).
card_colors('plagiarize', ['U']).
card_text('plagiarize', 'Until end of turn, if target player would draw a card, instead that player skips that draw and you draw a card.').
card_mana_cost('plagiarize', ['3', 'U']).
card_cmc('plagiarize', 4).
card_layout('plagiarize', 'normal').

% Found in: 10E, 7ED, 8ED, 9ED, ULG
card_name('plague beetle', 'Plague Beetle').
card_type('plague beetle', 'Creature — Insect').
card_types('plague beetle', ['Creature']).
card_subtypes('plague beetle', ['Insect']).
card_colors('plague beetle', ['B']).
card_text('plague beetle', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('plague beetle', ['B']).
card_cmc('plague beetle', 1).
card_layout('plague beetle', 'normal').
card_power('plague beetle', 1).
card_toughness('plague beetle', 1).

% Found in: C13, RAV
card_name('plague boiler', 'Plague Boiler').
card_type('plague boiler', 'Artifact').
card_types('plague boiler', ['Artifact']).
card_subtypes('plague boiler', []).
card_colors('plague boiler', []).
card_text('plague boiler', 'At the beginning of your upkeep, put a plague counter on Plague Boiler.\n{1}{B}{G}: Put a plague counter on Plague Boiler or remove a plague counter from it.\nWhen Plague Boiler has three or more plague counters on it, sacrifice it. If you do, destroy all nonland permanents.').
card_mana_cost('plague boiler', ['3']).
card_cmc('plague boiler', 3).
card_layout('plague boiler', 'normal').

% Found in: UDS
card_name('plague dogs', 'Plague Dogs').
card_type('plague dogs', 'Creature — Zombie Hound').
card_types('plague dogs', ['Creature']).
card_subtypes('plague dogs', ['Zombie', 'Hound']).
card_colors('plague dogs', ['B']).
card_text('plague dogs', 'When Plague Dogs dies, all creatures get -1/-1 until end of turn.\n{2}, Sacrifice Plague Dogs: Draw a card.').
card_mana_cost('plague dogs', ['4', 'B']).
card_cmc('plague dogs', 5).
card_layout('plague dogs', 'normal').
card_power('plague dogs', 3).
card_toughness('plague dogs', 3).

% Found in: PCY
card_name('plague fiend', 'Plague Fiend').
card_type('plague fiend', 'Creature — Insect').
card_types('plague fiend', ['Creature']).
card_subtypes('plague fiend', ['Insect']).
card_colors('plague fiend', ['B']).
card_text('plague fiend', 'Whenever Plague Fiend deals combat damage to a creature, destroy that creature unless its controller pays {2}.').
card_mana_cost('plague fiend', ['1', 'B']).
card_cmc('plague fiend', 2).
card_layout('plague fiend', 'normal').
card_power('plague fiend', 1).
card_toughness('plague fiend', 1).

% Found in: MBS, pWPN
card_name('plague myr', 'Plague Myr').
card_type('plague myr', 'Artifact Creature — Myr').
card_types('plague myr', ['Artifact', 'Creature']).
card_subtypes('plague myr', ['Myr']).
card_colors('plague myr', []).
card_text('plague myr', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\n{T}: Add {1} to your mana pool.').
card_mana_cost('plague myr', ['2']).
card_cmc('plague myr', 2).
card_layout('plague myr', 'normal').
card_power('plague myr', 1).
card_toughness('plague myr', 1).

% Found in: SHM
card_name('plague of vermin', 'Plague of Vermin').
card_type('plague of vermin', 'Sorcery').
card_types('plague of vermin', ['Sorcery']).
card_subtypes('plague of vermin', []).
card_colors('plague of vermin', ['B']).
card_text('plague of vermin', 'Starting with you, each player may pay any amount of life. Repeat this process until no one pays life. Each player puts a 1/1 black Rat creature token onto the battlefield for each 1 life he or she paid this way.').
card_mana_cost('plague of vermin', ['6', 'B']).
card_cmc('plague of vermin', 7).
card_layout('plague of vermin', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB
card_name('plague rats', 'Plague Rats').
card_type('plague rats', 'Creature — Rat').
card_types('plague rats', ['Creature']).
card_subtypes('plague rats', ['Rat']).
card_colors('plague rats', ['B']).
card_text('plague rats', 'Plague Rats\'s power and toughness are each equal to the number of creatures named Plague Rats on the battlefield.').
card_mana_cost('plague rats', ['2', 'B']).
card_cmc('plague rats', 3).
card_layout('plague rats', 'normal').
card_power('plague rats', '*').
card_toughness('plague rats', '*').

% Found in: TSP
card_name('plague sliver', 'Plague Sliver').
card_type('plague sliver', 'Creature — Sliver').
card_types('plague sliver', ['Creature']).
card_subtypes('plague sliver', ['Sliver']).
card_colors('plague sliver', ['B']).
card_text('plague sliver', 'All Slivers have \"At the beginning of your upkeep, this permanent deals 1 damage to you.\"').
card_mana_cost('plague sliver', ['2', 'B', 'B']).
card_cmc('plague sliver', 4).
card_layout('plague sliver', 'normal').
card_power('plague sliver', 5).
card_toughness('plague sliver', 5).

% Found in: INV
card_name('plague spitter', 'Plague Spitter').
card_type('plague spitter', 'Creature — Horror').
card_types('plague spitter', ['Creature']).
card_subtypes('plague spitter', ['Horror']).
card_colors('plague spitter', ['B']).
card_text('plague spitter', 'At the beginning of your upkeep, Plague Spitter deals 1 damage to each creature and each player.\nWhen Plague Spitter dies, Plague Spitter deals 1 damage to each creature and each player.').
card_mana_cost('plague spitter', ['2', 'B']).
card_cmc('plague spitter', 3).
card_layout('plague spitter', 'normal').
card_power('plague spitter', 2).
card_toughness('plague spitter', 2).

% Found in: INV
card_name('plague spores', 'Plague Spores').
card_type('plague spores', 'Sorcery').
card_types('plague spores', ['Sorcery']).
card_subtypes('plague spores', []).
card_colors('plague spores', ['B', 'R']).
card_text('plague spores', 'Destroy target nonblack creature and target land. They can\'t be regenerated.').
card_mana_cost('plague spores', ['4', 'B', 'R']).
card_cmc('plague spores', 6).
card_layout('plague spores', 'normal').

% Found in: SOM, pWPN
card_name('plague stinger', 'Plague Stinger').
card_type('plague stinger', 'Creature — Insect Horror').
card_types('plague stinger', ['Creature']).
card_subtypes('plague stinger', ['Insect', 'Horror']).
card_colors('plague stinger', ['B']).
card_text('plague stinger', 'Flying\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('plague stinger', ['1', 'B']).
card_cmc('plague stinger', 2).
card_layout('plague stinger', 'normal').
card_power('plague stinger', 1).
card_toughness('plague stinger', 1).

% Found in: 10E, 8ED, 9ED, PCY
card_name('plague wind', 'Plague Wind').
card_type('plague wind', 'Sorcery').
card_types('plague wind', ['Sorcery']).
card_subtypes('plague wind', []).
card_colors('plague wind', ['B']).
card_text('plague wind', 'Destroy all creatures you don\'t control. They can\'t be regenerated.').
card_mana_cost('plague wind', ['7', 'B', 'B']).
card_cmc('plague wind', 9).
card_layout('plague wind', 'normal').

% Found in: NMS
card_name('plague witch', 'Plague Witch').
card_type('plague witch', 'Creature — Elf Spellshaper').
card_types('plague witch', ['Creature']).
card_subtypes('plague witch', ['Elf', 'Spellshaper']).
card_colors('plague witch', ['B']).
card_text('plague witch', '{B}, {T}, Discard a card: Target creature gets -1/-1 until end of turn.').
card_mana_cost('plague witch', ['1', 'B']).
card_cmc('plague witch', 2).
card_layout('plague witch', 'normal').
card_power('plague witch', 1).
card_toughness('plague witch', 1).

% Found in: EXO
card_name('plaguebearer', 'Plaguebearer').
card_type('plaguebearer', 'Creature — Zombie').
card_types('plaguebearer', ['Creature']).
card_subtypes('plaguebearer', ['Zombie']).
card_colors('plaguebearer', ['B']).
card_text('plaguebearer', '{X}{X}{B}: Destroy target nonblack creature with converted mana cost X.').
card_mana_cost('plaguebearer', ['1', 'B']).
card_cmc('plaguebearer', 2).
card_layout('plaguebearer', 'normal').
card_power('plaguebearer', 1).
card_toughness('plaguebearer', 1).

% Found in: CNS, DDJ, GPT, MM2
card_name('plagued rusalka', 'Plagued Rusalka').
card_type('plagued rusalka', 'Creature — Spirit').
card_types('plagued rusalka', ['Creature']).
card_subtypes('plagued rusalka', ['Spirit']).
card_colors('plagued rusalka', ['B']).
card_text('plagued rusalka', '{B}, Sacrifice a creature: Target creature gets -1/-1 until end of turn.').
card_mana_cost('plagued rusalka', ['B']).
card_cmc('plagued rusalka', 1).
card_layout('plagued rusalka', 'normal').
card_power('plagued rusalka', 1).
card_toughness('plagued rusalka', 1).

% Found in: MBS
card_name('plaguemaw beast', 'Plaguemaw Beast').
card_type('plaguemaw beast', 'Creature — Beast').
card_types('plaguemaw beast', ['Creature']).
card_subtypes('plaguemaw beast', ['Beast']).
card_colors('plaguemaw beast', ['G']).
card_text('plaguemaw beast', '{T}, Sacrifice a creature: Proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_mana_cost('plaguemaw beast', ['3', 'G', 'G']).
card_cmc('plaguemaw beast', 5).
card_layout('plaguemaw beast', 'normal').
card_power('plaguemaw beast', 4).
card_toughness('plaguemaw beast', 3).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, ALA, ARC, ATH, AVR, BFZ, BRB, C13, C14, CED, CEI, CHK, CMD, CST, DD3_DVD, DDC, DDE, DDF, DDG, DDH, DDI, DDK, DDL, DDN, DDO, DDP, DTK, FRF, H09, HOP, ICE, INV, ISD, ITP, KTK, LEA, LEB, LRW, M10, M11, M12, M13, M14, M15, MBS, MD1, ME3, MED, MIR, MMQ, MRD, NPH, ODY, ONS, ORI, PC2, PO2, POR, PTK, RAV, ROE, RQS, RTR, S00, S99, SHM, SOM, THS, TMP, TPR, TSP, UGL, UNH, USG, ZEN, pALP, pARL, pELP, pGRU, pJGP, pPRE
card_name('plains', 'Plains').
card_type('plains', 'Basic Land — Plains').
card_types('plains', ['Land']).
card_subtypes('plains', ['Plains']).
card_supertypes('plains', ['Basic']).
card_colors('plains', []).
card_text('plains', '').
card_layout('plains', 'normal').

% Found in: USG
card_name('planar birth', 'Planar Birth').
card_type('planar birth', 'Sorcery').
card_types('planar birth', ['Sorcery']).
card_subtypes('planar birth', []).
card_colors('planar birth', ['W']).
card_text('planar birth', 'Return all basic land cards from all graveyards to the battlefield tapped under their owners\' control.').
card_mana_cost('planar birth', ['1', 'W']).
card_cmc('planar birth', 2).
card_layout('planar birth', 'normal').

% Found in: JUD
card_name('planar chaos', 'Planar Chaos').
card_type('planar chaos', 'Enchantment').
card_types('planar chaos', ['Enchantment']).
card_subtypes('planar chaos', []).
card_colors('planar chaos', ['R']).
card_text('planar chaos', 'At the beginning of your upkeep, flip a coin. If you lose the flip, sacrifice Planar Chaos.\nWhenever a player casts a spell, that player flips a coin. If he or she loses the flip, counter that spell.').
card_mana_cost('planar chaos', ['2', 'R']).
card_cmc('planar chaos', 3).
card_layout('planar chaos', 'normal').

% Found in: M10, M13, M14
card_name('planar cleansing', 'Planar Cleansing').
card_type('planar cleansing', 'Sorcery').
card_types('planar cleansing', ['Sorcery']).
card_subtypes('planar cleansing', []).
card_colors('planar cleansing', ['W']).
card_text('planar cleansing', 'Destroy all nonland permanents.').
card_mana_cost('planar cleansing', ['3', 'W', 'W', 'W']).
card_cmc('planar cleansing', 6).
card_layout('planar cleansing', 'normal').

% Found in: ULG
card_name('planar collapse', 'Planar Collapse').
card_type('planar collapse', 'Enchantment').
card_types('planar collapse', ['Enchantment']).
card_subtypes('planar collapse', []).
card_colors('planar collapse', ['W']).
card_text('planar collapse', 'At the beginning of your upkeep, if there are four or more creatures on the battlefield, sacrifice Planar Collapse and destroy all creatures. They can\'t be regenerated.').
card_mana_cost('planar collapse', ['1', 'W']).
card_cmc('planar collapse', 2).
card_layout('planar collapse', 'normal').

% Found in: APC
card_name('planar despair', 'Planar Despair').
card_type('planar despair', 'Sorcery').
card_types('planar despair', ['Sorcery']).
card_subtypes('planar despair', []).
card_colors('planar despair', ['B']).
card_text('planar despair', 'Domain — All creatures get -1/-1 until end of turn for each basic land type among lands you control.').
card_mana_cost('planar despair', ['3', 'B', 'B']).
card_cmc('planar despair', 5).
card_layout('planar despair', 'normal').

% Found in: LEG, ME4
card_name('planar gate', 'Planar Gate').
card_type('planar gate', 'Artifact').
card_types('planar gate', ['Artifact']).
card_subtypes('planar gate', []).
card_colors('planar gate', []).
card_text('planar gate', 'Creature spells you cast cost up to {2} less to cast.').
card_mana_cost('planar gate', ['6']).
card_cmc('planar gate', 6).
card_layout('planar gate', 'normal').
card_reserved('planar gate').

% Found in: LGN
card_name('planar guide', 'Planar Guide').
card_type('planar guide', 'Creature — Human Cleric').
card_types('planar guide', ['Creature']).
card_subtypes('planar guide', ['Human', 'Cleric']).
card_colors('planar guide', ['W']).
card_text('planar guide', '{3}{W}, Exile Planar Guide: Exile all creatures. At the beginning of the next end step, return those cards to the battlefield under their owners\' control.').
card_mana_cost('planar guide', ['W']).
card_cmc('planar guide', 1).
card_layout('planar guide', 'normal').
card_power('planar guide', 1).
card_toughness('planar guide', 1).

% Found in: BFZ
card_name('planar outburst', 'Planar Outburst').
card_type('planar outburst', 'Sorcery').
card_types('planar outburst', ['Sorcery']).
card_subtypes('planar outburst', []).
card_colors('planar outburst', ['W']).
card_text('planar outburst', 'Destroy all nonland creatures.\nAwaken 4—{5}{W}{W}{W} (If you cast this spell for {5}{W}{W}{W}, also put four +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_mana_cost('planar outburst', ['3', 'W', 'W']).
card_cmc('planar outburst', 5).
card_layout('planar outburst', 'normal').

% Found in: PLS
card_name('planar overlay', 'Planar Overlay').
card_type('planar overlay', 'Sorcery').
card_types('planar overlay', ['Sorcery']).
card_subtypes('planar overlay', []).
card_colors('planar overlay', ['U']).
card_text('planar overlay', 'Each player chooses a land he or she controls of each basic land type. Return those lands to their owners\' hands.').
card_mana_cost('planar overlay', ['2', 'U']).
card_cmc('planar overlay', 3).
card_layout('planar overlay', 'normal').

% Found in: 8ED, INV
card_name('planar portal', 'Planar Portal').
card_type('planar portal', 'Artifact').
card_types('planar portal', ['Artifact']).
card_subtypes('planar portal', []).
card_colors('planar portal', []).
card_text('planar portal', '{6}, {T}: Search your library for a card and put that card into your hand. Then shuffle your library.').
card_mana_cost('planar portal', ['6']).
card_cmc('planar portal', 6).
card_layout('planar portal', 'normal').

% Found in: USG
card_name('planar void', 'Planar Void').
card_type('planar void', 'Enchantment').
card_types('planar void', ['Enchantment']).
card_subtypes('planar void', []).
card_colors('planar void', ['B']).
card_text('planar void', 'Whenever another card is put into a graveyard from anywhere, exile that card.').
card_mana_cost('planar void', ['B']).
card_cmc('planar void', 1).
card_layout('planar void', 'normal').

% Found in: PLS
card_name('planeswalker\'s favor', 'Planeswalker\'s Favor').
card_type('planeswalker\'s favor', 'Enchantment').
card_types('planeswalker\'s favor', ['Enchantment']).
card_subtypes('planeswalker\'s favor', []).
card_colors('planeswalker\'s favor', ['G']).
card_text('planeswalker\'s favor', '{3}{G}: Target opponent reveals a card at random from his or her hand. Target creature gets +X/+X until end of turn, where X is the revealed card\'s converted mana cost.').
card_mana_cost('planeswalker\'s favor', ['2', 'G']).
card_cmc('planeswalker\'s favor', 3).
card_layout('planeswalker\'s favor', 'normal').

% Found in: PLS
card_name('planeswalker\'s fury', 'Planeswalker\'s Fury').
card_type('planeswalker\'s fury', 'Enchantment').
card_types('planeswalker\'s fury', ['Enchantment']).
card_subtypes('planeswalker\'s fury', []).
card_colors('planeswalker\'s fury', ['R']).
card_text('planeswalker\'s fury', '{3}{R}: Target opponent reveals a card at random from his or her hand. Planeswalker\'s Fury deals damage equal to that card\'s converted mana cost to that player. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('planeswalker\'s fury', ['2', 'R']).
card_cmc('planeswalker\'s fury', 3).
card_layout('planeswalker\'s fury', 'normal').

% Found in: PLS
card_name('planeswalker\'s mirth', 'Planeswalker\'s Mirth').
card_type('planeswalker\'s mirth', 'Enchantment').
card_types('planeswalker\'s mirth', ['Enchantment']).
card_subtypes('planeswalker\'s mirth', []).
card_colors('planeswalker\'s mirth', ['W']).
card_text('planeswalker\'s mirth', '{3}{W}: Target opponent reveals a card at random from his or her hand. You gain life equal to that card\'s converted mana cost.').
card_mana_cost('planeswalker\'s mirth', ['2', 'W']).
card_cmc('planeswalker\'s mirth', 3).
card_layout('planeswalker\'s mirth', 'normal').

% Found in: PLS
card_name('planeswalker\'s mischief', 'Planeswalker\'s Mischief').
card_type('planeswalker\'s mischief', 'Enchantment').
card_types('planeswalker\'s mischief', ['Enchantment']).
card_subtypes('planeswalker\'s mischief', []).
card_colors('planeswalker\'s mischief', ['U']).
card_text('planeswalker\'s mischief', '{3}{U}: Target opponent reveals a card at random from his or her hand. If it\'s an instant or sorcery card, exile it. You may cast it without paying its mana cost for as long as it remains exiled. (If it has X in its mana cost, X is 0.) At the beginning of the next end step, if you haven\'t cast it, return it to its owner\'s hand. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('planeswalker\'s mischief', ['2', 'U']).
card_cmc('planeswalker\'s mischief', 3).
card_layout('planeswalker\'s mischief', 'normal').

% Found in: PLS
card_name('planeswalker\'s scorn', 'Planeswalker\'s Scorn').
card_type('planeswalker\'s scorn', 'Enchantment').
card_types('planeswalker\'s scorn', ['Enchantment']).
card_subtypes('planeswalker\'s scorn', []).
card_colors('planeswalker\'s scorn', ['B']).
card_text('planeswalker\'s scorn', '{3}{B}: Target opponent reveals a card at random from his or her hand. Target creature gets -X/-X until end of turn, where X is the revealed card\'s converted mana cost. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('planeswalker\'s scorn', ['2', 'B']).
card_cmc('planeswalker\'s scorn', 3).
card_layout('planeswalker\'s scorn', 'normal').

% Found in: PC2
card_name('planewide disaster', 'Planewide Disaster').
card_type('planewide disaster', 'Phenomenon').
card_types('planewide disaster', ['Phenomenon']).
card_subtypes('planewide disaster', []).
card_colors('planewide disaster', []).
card_text('planewide disaster', 'When you encounter Planewide Disaster, destroy all creatures. (Then planeswalk away from this phenomenon.)').
card_layout('planewide disaster', 'phenomenon').

% Found in: DDP
card_name('plant', 'Plant').
card_type('plant', ' Creature — Plant').
card_types('plant', ['Creature']).
card_subtypes('plant', ['Plant']).
card_colors('plant', []).
card_text('plant', '').
card_layout('plant', 'token').
card_power('plant', 0).
card_toughness('plant', 1).

% Found in: POR
card_name('plant elemental', 'Plant Elemental').
card_type('plant elemental', 'Creature — Plant Elemental').
card_types('plant elemental', ['Creature']).
card_subtypes('plant elemental', ['Plant', 'Elemental']).
card_colors('plant elemental', ['G']).
card_text('plant elemental', 'When Plant Elemental enters the battlefield, sacrifice it unless you sacrifice a Forest.').
card_mana_cost('plant elemental', ['1', 'G']).
card_cmc('plant elemental', 2).
card_layout('plant elemental', 'normal').
card_power('plant elemental', 3).
card_toughness('plant elemental', 4).

% Found in: DDO, DGM
card_name('plasm capture', 'Plasm Capture').
card_type('plasm capture', 'Instant').
card_types('plasm capture', ['Instant']).
card_subtypes('plasm capture', []).
card_colors('plasm capture', ['U', 'G']).
card_text('plasm capture', 'Counter target spell. At the beginning of your next precombat main phase, add X mana in any combination of colors to your mana pool, where X is that spell\'s converted mana cost.').
card_mana_cost('plasm capture', ['G', 'G', 'U', 'U']).
card_cmc('plasm capture', 4).
card_layout('plasm capture', 'normal').

% Found in: 5DN
card_name('plasma elemental', 'Plasma Elemental').
card_type('plasma elemental', 'Creature — Elemental').
card_types('plasma elemental', ['Creature']).
card_subtypes('plasma elemental', ['Elemental']).
card_colors('plasma elemental', ['U']).
card_text('plasma elemental', 'Plasma Elemental can\'t be blocked.').
card_mana_cost('plasma elemental', ['5', 'U']).
card_cmc('plasma elemental', 6).
card_layout('plasma elemental', 'normal').
card_power('plasma elemental', 4).
card_toughness('plasma elemental', 1).

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME3, ME4, VMA
card_name('plateau', 'Plateau').
card_type('plateau', 'Land — Mountain Plains').
card_types('plateau', ['Land']).
card_subtypes('plateau', ['Mountain', 'Plains']).
card_colors('plateau', []).
card_text('plateau', '({T}: Add {R} or {W} to your mana pool.)').
card_layout('plateau', 'normal').
card_reserved('plateau').

% Found in: BFZ
card_name('plated crusher', 'Plated Crusher').
card_type('plated crusher', 'Creature — Beast').
card_types('plated crusher', ['Creature']).
card_subtypes('plated crusher', ['Beast']).
card_colors('plated crusher', ['G']).
card_text('plated crusher', 'Trample, hexproof').
card_mana_cost('plated crusher', ['4', 'G', 'G', 'G']).
card_cmc('plated crusher', 7).
card_layout('plated crusher', 'normal').
card_power('plated crusher', 7).
card_toughness('plated crusher', 6).

% Found in: DDI, ZEN
card_name('plated geopede', 'Plated Geopede').
card_type('plated geopede', 'Creature — Insect').
card_types('plated geopede', ['Creature']).
card_subtypes('plated geopede', ['Insect']).
card_colors('plated geopede', ['R']).
card_text('plated geopede', 'First strike\nLandfall — Whenever a land enters the battlefield under your control, Plated Geopede gets +2/+2 until end of turn.').
card_mana_cost('plated geopede', ['1', 'R']).
card_cmc('plated geopede', 2).
card_layout('plated geopede', 'normal').
card_power('plated geopede', 1).
card_toughness('plated geopede', 1).

% Found in: TSP
card_name('plated pegasus', 'Plated Pegasus').
card_type('plated pegasus', 'Creature — Pegasus').
card_types('plated pegasus', ['Creature']).
card_subtypes('plated pegasus', ['Pegasus']).
card_colors('plated pegasus', ['W']).
card_text('plated pegasus', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nIf a spell would deal damage to a creature or player, prevent 1 damage that spell would deal to that creature or player.').
card_mana_cost('plated pegasus', ['2', 'W']).
card_cmc('plated pegasus', 3).
card_layout('plated pegasus', 'normal').
card_power('plated pegasus', 1).
card_toughness('plated pegasus', 2).

% Found in: BRB, EXO
card_name('plated rootwalla', 'Plated Rootwalla').
card_type('plated rootwalla', 'Creature — Lizard').
card_types('plated rootwalla', ['Creature']).
card_subtypes('plated rootwalla', ['Lizard']).
card_colors('plated rootwalla', ['G']).
card_text('plated rootwalla', '{2}{G}: Plated Rootwalla gets +3/+3 until end of turn. Activate this ability only once each turn.').
card_mana_cost('plated rootwalla', ['4', 'G']).
card_cmc('plated rootwalla', 5).
card_layout('plated rootwalla', 'normal').
card_power('plated rootwalla', 3).
card_toughness('plated rootwalla', 3).

% Found in: CNS, SOM
card_name('plated seastrider', 'Plated Seastrider').
card_type('plated seastrider', 'Creature — Beast').
card_types('plated seastrider', ['Creature']).
card_subtypes('plated seastrider', ['Beast']).
card_colors('plated seastrider', ['U']).
card_text('plated seastrider', '').
card_mana_cost('plated seastrider', ['U', 'U']).
card_cmc('plated seastrider', 2).
card_layout('plated seastrider', 'normal').
card_power('plated seastrider', 1).
card_toughness('plated seastrider', 4).

% Found in: DD3_GVL, DDD, MRD
card_name('plated slagwurm', 'Plated Slagwurm').
card_type('plated slagwurm', 'Creature — Wurm').
card_types('plated slagwurm', ['Creature']).
card_subtypes('plated slagwurm', ['Wurm']).
card_colors('plated slagwurm', ['G']).
card_text('plated slagwurm', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_mana_cost('plated slagwurm', ['4', 'G', 'G', 'G']).
card_cmc('plated slagwurm', 7).
card_layout('plated slagwurm', 'normal').
card_power('plated slagwurm', 8).
card_toughness('plated slagwurm', 8).

% Found in: LGN
card_name('plated sliver', 'Plated Sliver').
card_type('plated sliver', 'Creature — Sliver').
card_types('plated sliver', ['Creature']).
card_subtypes('plated sliver', ['Sliver']).
card_colors('plated sliver', ['W']).
card_text('plated sliver', 'All Sliver creatures get +0/+1.').
card_mana_cost('plated sliver', ['W']).
card_cmc('plated sliver', 1).
card_layout('plated sliver', 'normal').
card_power('plated sliver', 1).
card_toughness('plated sliver', 1).

% Found in: BTD, UDS
card_name('plated spider', 'Plated Spider').
card_type('plated spider', 'Creature — Spider').
card_types('plated spider', ['Creature']).
card_subtypes('plated spider', ['Spider']).
card_colors('plated spider', ['G']).
card_text('plated spider', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('plated spider', ['4', 'G']).
card_cmc('plated spider', 5).
card_layout('plated spider', 'normal').
card_power('plated spider', 4).
card_toughness('plated spider', 4).

% Found in: PO2
card_name('plated wurm', 'Plated Wurm').
card_type('plated wurm', 'Creature — Wurm').
card_types('plated wurm', ['Creature']).
card_subtypes('plated wurm', ['Wurm']).
card_colors('plated wurm', ['G']).
card_text('plated wurm', '').
card_mana_cost('plated wurm', ['4', 'G']).
card_cmc('plated wurm', 5).
card_layout('plated wurm', 'normal').
card_power('plated wurm', 4).
card_toughness('plated wurm', 5).

% Found in: 10E, M10, M11, MRD, V15
card_name('platinum angel', 'Platinum Angel').
card_type('platinum angel', 'Artifact Creature — Angel').
card_types('platinum angel', ['Artifact', 'Creature']).
card_subtypes('platinum angel', ['Angel']).
card_colors('platinum angel', []).
card_text('platinum angel', 'Flying\nYou can\'t lose the game and your opponents can\'t win the game.').
card_mana_cost('platinum angel', ['7']).
card_cmc('platinum angel', 7).
card_layout('platinum angel', 'normal').
card_power('platinum angel', 4).
card_toughness('platinum angel', 4).

% Found in: VAN
card_name('platinum angel avatar', 'Platinum Angel Avatar').
card_type('platinum angel avatar', 'Vanguard').
card_types('platinum angel avatar', ['Vanguard']).
card_subtypes('platinum angel avatar', []).
card_colors('platinum angel avatar', []).
card_text('platinum angel avatar', 'If you control an artifact, a creature, an enchantment, and a land, you can\'t lose the game and your opponents can\'t win the game.').
card_layout('platinum angel avatar', 'vanguard').

% Found in: SOM
card_name('platinum emperion', 'Platinum Emperion').
card_type('platinum emperion', 'Artifact Creature — Golem').
card_types('platinum emperion', ['Artifact', 'Creature']).
card_subtypes('platinum emperion', ['Golem']).
card_colors('platinum emperion', []).
card_text('platinum emperion', 'Your life total can\'t change. (You can\'t gain or lose life. You can\'t pay any amount of life except 0.)').
card_mana_cost('platinum emperion', ['8']).
card_cmc('platinum emperion', 8).
card_layout('platinum emperion', 'normal').
card_power('platinum emperion', 8).
card_toughness('platinum emperion', 8).

% Found in: DIS, MM2
card_name('plaxcaster frogling', 'Plaxcaster Frogling').
card_type('plaxcaster frogling', 'Creature — Frog Mutant').
card_types('plaxcaster frogling', ['Creature']).
card_subtypes('plaxcaster frogling', ['Frog', 'Mutant']).
card_colors('plaxcaster frogling', ['U', 'G']).
card_text('plaxcaster frogling', 'Graft 3 (This creature enters the battlefield with three +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\n{2}: Target creature with a +1/+1 counter on it gains shroud until end of turn. (It can\'t be the target of spells or abilities.)').
card_mana_cost('plaxcaster frogling', ['1', 'G', 'U']).
card_cmc('plaxcaster frogling', 3).
card_layout('plaxcaster frogling', 'normal').
card_power('plaxcaster frogling', 0).
card_toughness('plaxcaster frogling', 0).

% Found in: DIS
card_name('plaxmanta', 'Plaxmanta').
card_type('plaxmanta', 'Creature — Beast').
card_types('plaxmanta', ['Creature']).
card_subtypes('plaxmanta', ['Beast']).
card_colors('plaxmanta', ['U']).
card_text('plaxmanta', 'Flash\nWhen Plaxmanta enters the battlefield, creatures you control gain shroud until end of turn. (They can\'t be the targets of spells or abilities.)\nWhen Plaxmanta enters the battlefield, sacrifice it unless {G} was spent to cast it.').
card_mana_cost('plaxmanta', ['1', 'U']).
card_cmc('plaxmanta', 2).
card_layout('plaxmanta', 'normal').
card_power('plaxmanta', 2).
card_toughness('plaxmanta', 2).

% Found in: BNG
card_name('plea for guidance', 'Plea for Guidance').
card_type('plea for guidance', 'Sorcery').
card_types('plea for guidance', ['Sorcery']).
card_subtypes('plea for guidance', []).
card_colors('plea for guidance', ['W']).
card_text('plea for guidance', 'Search your library for up to two enchantment cards, reveal them, and put them into your hand. Then shuffle your library.').
card_mana_cost('plea for guidance', ['5', 'W']).
card_cmc('plea for guidance', 6).
card_layout('plea for guidance', 'normal').

% Found in: CNS, VMA
card_name('plea for power', 'Plea for Power').
card_type('plea for power', 'Sorcery').
card_types('plea for power', ['Sorcery']).
card_subtypes('plea for power', []).
card_colors('plea for power', ['U']).
card_text('plea for power', 'Will of the council — Starting with you, each player votes for time or knowledge. If time gets more votes, take an extra turn after this one. If knowledge gets more votes or the vote is tied, draw three cards.').
card_mana_cost('plea for power', ['3', 'U']).
card_cmc('plea for power', 4).
card_layout('plea for power', 'normal').

% Found in: INV
card_name('pledge of loyalty', 'Pledge of Loyalty').
card_type('pledge of loyalty', 'Enchantment — Aura').
card_types('pledge of loyalty', ['Enchantment']).
card_subtypes('pledge of loyalty', ['Aura']).
card_colors('pledge of loyalty', ['W']).
card_text('pledge of loyalty', 'Enchant creature\nEnchanted creature has protection from the colors of permanents you control. This effect doesn\'t remove Pledge of Loyalty.').
card_mana_cost('pledge of loyalty', ['1', 'W']).
card_cmc('pledge of loyalty', 2).
card_layout('pledge of loyalty', 'normal').

% Found in: DDG, LRW
card_name('plover knights', 'Plover Knights').
card_type('plover knights', 'Creature — Kithkin Knight').
card_types('plover knights', ['Creature']).
card_subtypes('plover knights', ['Kithkin', 'Knight']).
card_colors('plover knights', ['W']).
card_text('plover knights', 'Flying, first strike').
card_mana_cost('plover knights', ['3', 'W', 'W']).
card_cmc('plover knights', 5).
card_layout('plover knights', 'normal').
card_power('plover knights', 3).
card_toughness('plover knights', 3).

% Found in: SOK
card_name('plow through reito', 'Plow Through Reito').
card_type('plow through reito', 'Instant — Arcane').
card_types('plow through reito', ['Instant']).
card_subtypes('plow through reito', ['Arcane']).
card_colors('plow through reito', ['W']).
card_text('plow through reito', 'Sweep — Return any number of Plains you control to their owner\'s hand. Target creature gets +1/+1 until end of turn for each Plains returned this way.').
card_mana_cost('plow through reito', ['1', 'W']).
card_cmc('plow through reito', 2).
card_layout('plow through reito', 'normal').

% Found in: 8ED, UDS
card_name('plow under', 'Plow Under').
card_type('plow under', 'Sorcery').
card_types('plow under', ['Sorcery']).
card_subtypes('plow under', []).
card_colors('plow under', ['G']).
card_text('plow under', 'Put two target lands on top of their owners\' libraries.').
card_mana_cost('plow under', ['3', 'G', 'G']).
card_cmc('plow under', 5).
card_layout('plow under', 'normal').

% Found in: DIS
card_name('plumes of peace', 'Plumes of Peace').
card_type('plumes of peace', 'Enchantment — Aura').
card_types('plumes of peace', ['Enchantment']).
card_subtypes('plumes of peace', ['Aura']).
card_colors('plumes of peace', ['W', 'U']).
card_text('plumes of peace', 'Enchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nForecast — {W}{U}, Reveal Plumes of Peace from your hand: Tap target creature. (Activate this ability only during your upkeep and only once each turn.)').
card_mana_cost('plumes of peace', ['1', 'W', 'U']).
card_cmc('plumes of peace', 3).
card_layout('plumes of peace', 'normal').

% Found in: CMD, MMA, SHM
card_name('plumeveil', 'Plumeveil').
card_type('plumeveil', 'Creature — Elemental').
card_types('plumeveil', ['Creature']).
card_subtypes('plumeveil', ['Elemental']).
card_colors('plumeveil', ['W', 'U']).
card_text('plumeveil', 'Flash\nDefender, flying').
card_mana_cost('plumeveil', ['W/U', 'W/U', 'W/U']).
card_cmc('plumeveil', 3).
card_layout('plumeveil', 'normal').
card_power('plumeveil', 4).
card_toughness('plumeveil', 4).

% Found in: ARC, BFZ, CNS, M11, M12, M13, M14, M15, MM2, ORI
card_name('plummet', 'Plummet').
card_type('plummet', 'Instant').
card_types('plummet', ['Instant']).
card_subtypes('plummet', []).
card_colors('plummet', ['G']).
card_text('plummet', 'Destroy target creature with flying.').
card_mana_cost('plummet', ['1', 'G']).
card_cmc('plummet', 2).
card_layout('plummet', 'normal').

% Found in: TSP
card_name('plunder', 'Plunder').
card_type('plunder', 'Sorcery').
card_types('plunder', ['Sorcery']).
card_subtypes('plunder', []).
card_colors('plunder', ['R']).
card_text('plunder', 'Destroy target artifact or land.\nSuspend 4—{1}{R} (Rather than cast this card from your hand, you may pay {1}{R} and exile it with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_mana_cost('plunder', ['4', 'R']).
card_cmc('plunder', 5).
card_layout('plunder', 'normal').

% Found in: 5DN
card_name('plunge into darkness', 'Plunge into Darkness').
card_type('plunge into darkness', 'Instant').
card_types('plunge into darkness', ['Instant']).
card_subtypes('plunge into darkness', []).
card_colors('plunge into darkness', ['B']).
card_text('plunge into darkness', 'Choose one —\n• Sacrifice any number of creatures, then you gain 3 life for each sacrificed creature.\n• Pay X life, then look at the top X cards of your library. Put one of those cards into your hand and exile the rest.\nEntwine {B} (Choose both if you pay the entwine cost.)').
card_mana_cost('plunge into darkness', ['1', 'B']).
card_cmc('plunge into darkness', 2).
card_layout('plunge into darkness', 'normal').

% Found in: UNH
card_name('pointy finger of doom', 'Pointy Finger of Doom').
card_type('pointy finger of doom', 'Artifact').
card_types('pointy finger of doom', ['Artifact']).
card_subtypes('pointy finger of doom', []).
card_colors('pointy finger of doom', []).
card_text('pointy finger of doom', '{3}, {T}: Spin Pointy Finger of Doom in the middle of the table so that it rotates completely at least once, then destroy the closest permanent the finger points to.').
card_mana_cost('pointy finger of doom', ['4']).
card_cmc('pointy finger of doom', 4).
card_layout('pointy finger of doom', 'normal').

% Found in: PTK
card_name('poison arrow', 'Poison Arrow').
card_type('poison arrow', 'Sorcery').
card_types('poison arrow', ['Sorcery']).
card_subtypes('poison arrow', []).
card_colors('poison arrow', ['B']).
card_text('poison arrow', 'Destroy target nonblack creature. You gain 3 life.').
card_mana_cost('poison arrow', ['4', 'B', 'B']).
card_cmc('poison arrow', 6).
card_layout('poison arrow', 'normal').

% Found in: SHM
card_name('poison the well', 'Poison the Well').
card_type('poison the well', 'Sorcery').
card_types('poison the well', ['Sorcery']).
card_subtypes('poison the well', []).
card_colors('poison the well', ['B', 'R']).
card_text('poison the well', 'Destroy target land. Poison the Well deals 2 damage to that land\'s controller.').
card_mana_cost('poison the well', ['2', 'B/R', 'B/R']).
card_cmc('poison the well', 4).
card_layout('poison the well', 'normal').

% Found in: GPT
card_name('poisonbelly ogre', 'Poisonbelly Ogre').
card_type('poisonbelly ogre', 'Creature — Ogre Warrior').
card_types('poisonbelly ogre', ['Creature']).
card_subtypes('poisonbelly ogre', ['Ogre', 'Warrior']).
card_colors('poisonbelly ogre', ['B']).
card_text('poisonbelly ogre', 'Whenever another creature enters the battlefield, its controller loses 1 life.').
card_mana_cost('poisonbelly ogre', ['4', 'B']).
card_cmc('poisonbelly ogre', 5).
card_layout('poisonbelly ogre', 'normal').
card_power('poisonbelly ogre', 3).
card_toughness('poisonbelly ogre', 3).

% Found in: ICE, MED
card_name('polar kraken', 'Polar Kraken').
card_type('polar kraken', 'Creature — Kraken').
card_types('polar kraken', ['Creature']).
card_subtypes('polar kraken', ['Kraken']).
card_colors('polar kraken', ['U']).
card_text('polar kraken', 'Trample\nPolar Kraken enters the battlefield tapped.\nCumulative upkeep—Sacrifice a land. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('polar kraken', ['8', 'U', 'U', 'U']).
card_cmc('polar kraken', 11).
card_layout('polar kraken', 'normal').
card_power('polar kraken', 11).
card_toughness('polar kraken', 11).
card_reserved('polar kraken').

% Found in: THS
card_name('polis crusher', 'Polis Crusher').
card_type('polis crusher', 'Creature — Cyclops').
card_types('polis crusher', ['Creature']).
card_subtypes('polis crusher', ['Cyclops']).
card_colors('polis crusher', ['R', 'G']).
card_text('polis crusher', 'Trample, protection from enchantments\n{4}{R}{G}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)\nWhenever Polis Crusher deals combat damage to a player, if Polis Crusher is monstrous, destroy target enchantment that player controls.').
card_mana_cost('polis crusher', ['2', 'R', 'G']).
card_cmc('polis crusher', 4).
card_layout('polis crusher', 'normal').
card_power('polis crusher', 4).
card_toughness('polis crusher', 4).

% Found in: MIR
card_name('political trickery', 'Political Trickery').
card_type('political trickery', 'Sorcery').
card_types('political trickery', ['Sorcery']).
card_subtypes('political trickery', []).
card_colors('political trickery', ['U']).
card_text('political trickery', 'Exchange control of target land you control and target land an opponent controls. (This effect lasts indefinitely.)').
card_mana_cost('political trickery', ['2', 'U']).
card_cmc('political trickery', 3).
card_layout('political trickery', 'normal').
card_reserved('political trickery').

% Found in: CMD, LRW
card_name('pollen lullaby', 'Pollen Lullaby').
card_type('pollen lullaby', 'Instant').
card_types('pollen lullaby', ['Instant']).
card_subtypes('pollen lullaby', []).
card_colors('pollen lullaby', ['W']).
card_text('pollen lullaby', 'Prevent all combat damage that would be dealt this turn. Clash with an opponent. If you win, creatures that player controls don\'t untap during the player\'s next untap step. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('pollen lullaby', ['1', 'W']).
card_cmc('pollen lullaby', 2).
card_layout('pollen lullaby', 'normal').

% Found in: PLS
card_name('pollen remedy', 'Pollen Remedy').
card_type('pollen remedy', 'Instant').
card_types('pollen remedy', ['Instant']).
card_subtypes('pollen remedy', []).
card_colors('pollen remedy', ['W']).
card_text('pollen remedy', 'Kicker—Sacrifice a land. (You may sacrifice a land in addition to any other costs as you cast this spell.)\nPrevent the next 3 damage that would be dealt this turn to any number of target creatures and/or players, divided as you choose. If Pollen Remedy was kicked, prevent the next 6 damage this way instead.').
card_mana_cost('pollen remedy', ['W']).
card_cmc('pollen remedy', 1).
card_layout('pollen remedy', 'normal').

% Found in: PC2, RAV
card_name('pollenbright wings', 'Pollenbright Wings').
card_type('pollenbright wings', 'Enchantment — Aura').
card_types('pollenbright wings', ['Enchantment']).
card_subtypes('pollenbright wings', ['Aura']).
card_colors('pollenbright wings', ['W', 'G']).
card_text('pollenbright wings', 'Enchant creature\nEnchanted creature has flying.\nWhenever enchanted creature deals combat damage to a player, put that many 1/1 green Saproling creature tokens onto the battlefield.').
card_mana_cost('pollenbright wings', ['4', 'G', 'W']).
card_cmc('pollenbright wings', 6).
card_layout('pollenbright wings', 'normal').

% Found in: SHM
card_name('polluted bonds', 'Polluted Bonds').
card_type('polluted bonds', 'Enchantment').
card_types('polluted bonds', ['Enchantment']).
card_subtypes('polluted bonds', []).
card_colors('polluted bonds', ['B']).
card_text('polluted bonds', 'Whenever a land enters the battlefield under an opponent\'s control, that player loses 2 life and you gain 2 life.').
card_mana_cost('polluted bonds', ['3', 'B', 'B']).
card_cmc('polluted bonds', 5).
card_layout('polluted bonds', 'normal').

% Found in: AVR
card_name('polluted dead', 'Polluted Dead').
card_type('polluted dead', 'Creature — Zombie').
card_types('polluted dead', ['Creature']).
card_subtypes('polluted dead', ['Zombie']).
card_colors('polluted dead', ['B']).
card_text('polluted dead', 'When Polluted Dead dies, destroy target land.').
card_mana_cost('polluted dead', ['4', 'B']).
card_cmc('polluted dead', 5).
card_layout('polluted dead', 'normal').
card_power('polluted dead', 3).
card_toughness('polluted dead', 3).

% Found in: EXP, KTK, ONS, pJGP
card_name('polluted delta', 'Polluted Delta').
card_type('polluted delta', 'Land').
card_types('polluted delta', ['Land']).
card_subtypes('polluted delta', []).
card_colors('polluted delta', []).
card_text('polluted delta', '{T}, Pay 1 life, Sacrifice Polluted Delta: Search your library for an Island or Swamp card and put it onto the battlefield. Then shuffle your library.').
card_layout('polluted delta', 'normal').

% Found in: ATH, BRB, BTD, C14, DD3_GVL, DDD, PD3, USG
card_name('polluted mire', 'Polluted Mire').
card_type('polluted mire', 'Land').
card_types('polluted mire', ['Land']).
card_subtypes('polluted mire', []).
card_colors('polluted mire', []).
card_text('polluted mire', 'Polluted Mire enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_layout('polluted mire', 'normal').

% Found in: DDL, THS
card_name('polukranos, world eater', 'Polukranos, World Eater').
card_type('polukranos, world eater', 'Legendary Creature — Hydra').
card_types('polukranos, world eater', ['Creature']).
card_subtypes('polukranos, world eater', ['Hydra']).
card_supertypes('polukranos, world eater', ['Legendary']).
card_colors('polukranos, world eater', ['G']).
card_text('polukranos, world eater', '{X}{X}{G}: Monstrosity X. (If this creature isn\'t monstrous, put X +1/+1 counters on it and it becomes monstrous.)\nWhen Polukranos, World Eater becomes monstrous, it deals X damage divided as you choose among any number of target creatures your opponents control. Each of those creatures deals damage equal to its power to Polukranos.').
card_mana_cost('polukranos, world eater', ['2', 'G', 'G']).
card_cmc('polukranos, world eater', 4).
card_layout('polukranos, world eater', 'normal').
card_power('polukranos, world eater', 5).
card_toughness('polukranos, world eater', 5).

% Found in: 6ED, 9ED, M10, MIR
card_name('polymorph', 'Polymorph').
card_type('polymorph', 'Sorcery').
card_types('polymorph', ['Sorcery']).
card_subtypes('polymorph', []).
card_colors('polymorph', ['U']).
card_text('polymorph', 'Destroy target creature. It can\'t be regenerated. Its controller reveals cards from the top of his or her library until he or she reveals a creature card. The player puts that card onto the battlefield, then shuffles all other cards revealed this way into his or her library.').
card_mana_cost('polymorph', ['3', 'U']).
card_cmc('polymorph', 4).
card_layout('polymorph', 'normal').

% Found in: M15
card_name('polymorphist\'s jest', 'Polymorphist\'s Jest').
card_type('polymorphist\'s jest', 'Instant').
card_types('polymorphist\'s jest', ['Instant']).
card_subtypes('polymorphist\'s jest', []).
card_colors('polymorphist\'s jest', ['U']).
card_text('polymorphist\'s jest', 'Until end of turn, each creature target player controls loses all abilities and becomes a blue Frog with base power and toughness 1/1.').
card_mana_cost('polymorphist\'s jest', ['1', 'U', 'U']).
card_cmc('polymorphist\'s jest', 3).
card_layout('polymorphist\'s jest', 'normal').

% Found in: JOU
card_name('polymorphous rush', 'Polymorphous Rush').
card_type('polymorphous rush', 'Instant').
card_types('polymorphous rush', ['Instant']).
card_subtypes('polymorphous rush', []).
card_colors('polymorphous rush', ['U']).
card_text('polymorphous rush', 'Strive — Polymorphous Rush costs {1}{U} more to cast for each target beyond the first.\nChoose a creature on the battlefield. Any number of target creatures you control each become a copy of that creature until end of turn.').
card_mana_cost('polymorphous rush', ['2', 'U']).
card_cmc('polymorphous rush', 3).
card_layout('polymorphous rush', 'normal').

% Found in: LRW, M10, M12, pMPR
card_name('ponder', 'Ponder').
card_type('ponder', 'Sorcery').
card_types('ponder', ['Sorcery']).
card_subtypes('ponder', []).
card_colors('ponder', ['U']).
card_text('ponder', 'Look at the top three cards of your library, then put them back in any order. You may shuffle your library.\nDraw a card.').
card_mana_cost('ponder', ['U']).
card_cmc('ponder', 1).
card_layout('ponder', 'normal').

% Found in: C14, PLC
card_name('pongify', 'Pongify').
card_type('pongify', 'Instant').
card_types('pongify', ['Instant']).
card_subtypes('pongify', []).
card_colors('pongify', ['U']).
card_text('pongify', 'Destroy target creature. It can\'t be regenerated. Its controller puts a 3/3 green Ape creature token onto the battlefield.').
card_mana_cost('pongify', ['U']).
card_cmc('pongify', 1).
card_layout('pongify', 'normal').

% Found in: C14, DGM
card_name('pontiff of blight', 'Pontiff of Blight').
card_type('pontiff of blight', 'Creature — Zombie Cleric').
card_types('pontiff of blight', ['Creature']).
card_subtypes('pontiff of blight', ['Zombie', 'Cleric']).
card_colors('pontiff of blight', ['B']).
card_text('pontiff of blight', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)\nOther creatures you control have extort. (If a creature has multiple instances of extort, each triggers separately.)').
card_mana_cost('pontiff of blight', ['4', 'B', 'B']).
card_cmc('pontiff of blight', 6).
card_layout('pontiff of blight', 'normal').
card_power('pontiff of blight', 2).
card_toughness('pontiff of blight', 7).

% Found in: KTK
card_name('ponyback brigade', 'Ponyback Brigade').
card_type('ponyback brigade', 'Creature — Goblin Warrior').
card_types('ponyback brigade', ['Creature']).
card_subtypes('ponyback brigade', ['Goblin', 'Warrior']).
card_colors('ponyback brigade', ['W', 'B', 'R']).
card_text('ponyback brigade', 'When Ponyback Brigade enters the battlefield or is turned face up, put three 1/1 red Goblin creature tokens onto the battlefield.\nMorph {2}{R}{W}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('ponyback brigade', ['3', 'R', 'W', 'B']).
card_cmc('ponyback brigade', 6).
card_layout('ponyback brigade', 'normal').
card_power('ponyback brigade', 2).
card_toughness('ponyback brigade', 2).

% Found in: FUT
card_name('pooling venom', 'Pooling Venom').
card_type('pooling venom', 'Enchantment — Aura').
card_types('pooling venom', ['Enchantment']).
card_subtypes('pooling venom', ['Aura']).
card_colors('pooling venom', ['B']).
card_text('pooling venom', 'Enchant land\nWhenever enchanted land becomes tapped, its controller loses 2 life.\n{3}{B}: Destroy enchanted land.').
card_mana_cost('pooling venom', ['1', 'B']).
card_cmc('pooling venom', 2).
card_layout('pooling venom', 'normal').

% Found in: HOP
card_name('pools of becoming', 'Pools of Becoming').
card_type('pools of becoming', 'Plane — Bolas’s Meditation Realm').
card_types('pools of becoming', ['Plane']).
card_subtypes('pools of becoming', ['Bolas’s Meditation Realm']).
card_colors('pools of becoming', []).
card_text('pools of becoming', 'At the beginning of your end step, put the cards in your hand on the bottom of your library in any order, then draw that many cards.\nWhenever you roll {C}, reveal the top three cards of your planar deck. Each of the revealed cards\' {C} abilities triggers. Then put the revealed cards on the bottom of your planar deck in any order.').
card_layout('pools of becoming', 'plane').

% Found in: NPH
card_name('porcelain legionnaire', 'Porcelain Legionnaire').
card_type('porcelain legionnaire', 'Artifact Creature — Soldier').
card_types('porcelain legionnaire', ['Artifact', 'Creature']).
card_subtypes('porcelain legionnaire', ['Soldier']).
card_colors('porcelain legionnaire', ['W']).
card_text('porcelain legionnaire', '({W/P} can be paid with either {W} or 2 life.)\nFirst strike').
card_mana_cost('porcelain legionnaire', ['2', 'W/P']).
card_cmc('porcelain legionnaire', 3).
card_layout('porcelain legionnaire', 'normal').
card_power('porcelain legionnaire', 3).
card_toughness('porcelain legionnaire', 1).

% Found in: PLC
card_name('porphyry nodes', 'Porphyry Nodes').
card_type('porphyry nodes', 'Enchantment').
card_types('porphyry nodes', ['Enchantment']).
card_subtypes('porphyry nodes', []).
card_colors('porphyry nodes', ['W']).
card_text('porphyry nodes', 'At the beginning of your upkeep, destroy the creature with the least power. It can\'t be regenerated. If two or more creatures are tied for least power, you choose one of them.\nWhen there are no creatures on the battlefield, sacrifice Porphyry Nodes.').
card_mana_cost('porphyry nodes', ['W']).
card_cmc('porphyry nodes', 1).
card_layout('porphyry nodes', 'normal').

% Found in: MMQ
card_name('port inspector', 'Port Inspector').
card_type('port inspector', 'Creature — Human').
card_types('port inspector', ['Creature']).
card_subtypes('port inspector', ['Human']).
card_colors('port inspector', ['U']).
card_text('port inspector', 'Whenever Port Inspector becomes blocked, you may look at defending player\'s hand.').
card_mana_cost('port inspector', ['1', 'U']).
card_cmc('port inspector', 2).
card_layout('port inspector', 'normal').
card_power('port inspector', 1).
card_toughness('port inspector', 2).

% Found in: STH
card_name('portcullis', 'Portcullis').
card_type('portcullis', 'Artifact').
card_types('portcullis', ['Artifact']).
card_subtypes('portcullis', []).
card_colors('portcullis', []).
card_text('portcullis', 'Whenever a creature enters the battlefield, if there are two or more other creatures on the battlefield, exile that creature. Return that card to the battlefield under its owner\'s control when Portcullis leaves the battlefield.').
card_mana_cost('portcullis', ['4']).
card_cmc('portcullis', 4).
card_layout('portcullis', 'normal').

% Found in: 5ED, CST, ICE, ME2
card_name('portent', 'Portent').
card_type('portent', 'Sorcery').
card_types('portent', ['Sorcery']).
card_subtypes('portent', []).
card_colors('portent', ['U']).
card_text('portent', 'Look at the top three cards of target player\'s library, then put them back in any order. You may have that player shuffle his or her library.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('portent', ['U']).
card_cmc('portent', 1).
card_layout('portent', 'normal').

% Found in: THS
card_name('portent of betrayal', 'Portent of Betrayal').
card_type('portent of betrayal', 'Sorcery').
card_types('portent of betrayal', ['Sorcery']).
card_subtypes('portent of betrayal', []).
card_colors('portent of betrayal', ['R']).
card_text('portent of betrayal', 'Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('portent of betrayal', ['3', 'R']).
card_cmc('portent of betrayal', 4).
card_layout('portent of betrayal', 'normal').

% Found in: TOR
card_name('possessed aven', 'Possessed Aven').
card_type('possessed aven', 'Creature — Bird Soldier Horror').
card_types('possessed aven', ['Creature']).
card_subtypes('possessed aven', ['Bird', 'Soldier', 'Horror']).
card_colors('possessed aven', ['U']).
card_text('possessed aven', 'Flying\nThreshold — As long as seven or more cards are in your graveyard, Possessed Aven gets +1/+1, is black, and has \"{2}{B}, {T}: Destroy target blue creature.\"').
card_mana_cost('possessed aven', ['2', 'U', 'U']).
card_cmc('possessed aven', 4).
card_layout('possessed aven', 'normal').
card_power('possessed aven', 3).
card_toughness('possessed aven', 3).

% Found in: TOR
card_name('possessed barbarian', 'Possessed Barbarian').
card_type('possessed barbarian', 'Creature — Human Barbarian Horror').
card_types('possessed barbarian', ['Creature']).
card_subtypes('possessed barbarian', ['Human', 'Barbarian', 'Horror']).
card_colors('possessed barbarian', ['R']).
card_text('possessed barbarian', 'First strike\nThreshold — As long as seven or more cards are in your graveyard, Possessed Barbarian gets +1/+1, is black, and has \"{2}{B}, {T}: Destroy target red creature.\"').
card_mana_cost('possessed barbarian', ['2', 'R', 'R']).
card_cmc('possessed barbarian', 4).
card_layout('possessed barbarian', 'normal').
card_power('possessed barbarian', 3).
card_toughness('possessed barbarian', 3).

% Found in: TOR
card_name('possessed centaur', 'Possessed Centaur').
card_type('possessed centaur', 'Creature — Centaur Horror').
card_types('possessed centaur', ['Creature']).
card_subtypes('possessed centaur', ['Centaur', 'Horror']).
card_colors('possessed centaur', ['G']).
card_text('possessed centaur', 'Trample\nThreshold — As long as seven or more cards are in your graveyard, Possessed Centaur gets +1/+1, is black, and has \"{2}{B}, {T}: Destroy target green creature.\"').
card_mana_cost('possessed centaur', ['2', 'G', 'G']).
card_cmc('possessed centaur', 4).
card_layout('possessed centaur', 'normal').
card_power('possessed centaur', 3).
card_toughness('possessed centaur', 3).

% Found in: TOR
card_name('possessed nomad', 'Possessed Nomad').
card_type('possessed nomad', 'Creature — Human Nomad Horror').
card_types('possessed nomad', ['Creature']).
card_subtypes('possessed nomad', ['Human', 'Nomad', 'Horror']).
card_colors('possessed nomad', ['W']).
card_text('possessed nomad', 'Vigilance\nThreshold — As long as seven or more cards are in your graveyard, Possessed Nomad gets +1/+1, is black, and has \"{2}{B}, {T}: Destroy target white creature.\"').
card_mana_cost('possessed nomad', ['2', 'W', 'W']).
card_cmc('possessed nomad', 4).
card_layout('possessed nomad', 'normal').
card_power('possessed nomad', 3).
card_toughness('possessed nomad', 3).

% Found in: 5DN
card_name('possessed portal', 'Possessed Portal').
card_type('possessed portal', 'Artifact').
card_types('possessed portal', ['Artifact']).
card_subtypes('possessed portal', []).
card_colors('possessed portal', []).
card_text('possessed portal', 'If a player would draw a card, that player skips that draw instead.\nAt the beginning of each end step, each player sacrifices a permanent unless he or she discards a card.').
card_mana_cost('possessed portal', ['8']).
card_cmc('possessed portal', 8).
card_layout('possessed portal', 'normal').

% Found in: ORI
card_name('possessed skaab', 'Possessed Skaab').
card_type('possessed skaab', 'Creature — Zombie').
card_types('possessed skaab', ['Creature']).
card_subtypes('possessed skaab', ['Zombie']).
card_colors('possessed skaab', ['U', 'B']).
card_text('possessed skaab', 'When Possessed Skaab enters the battlefield, return target instant, sorcery, or creature card from your graveyard to your hand.\nIf Possessed Skaab would die, exile it instead.').
card_mana_cost('possessed skaab', ['3', 'U', 'B']).
card_cmc('possessed skaab', 5).
card_layout('possessed skaab', 'normal').
card_power('possessed skaab', 3).
card_toughness('possessed skaab', 2).

% Found in: DGM
card_name('possibility storm', 'Possibility Storm').
card_type('possibility storm', 'Enchantment').
card_types('possibility storm', ['Enchantment']).
card_subtypes('possibility storm', []).
card_colors('possibility storm', ['R']).
card_text('possibility storm', 'Whenever a player casts a spell from his or her hand, that player exiles it, then exiles cards from the top of his or her library until he or she exiles a card that shares a card type with it. That player may cast that card without paying its mana cost. Then he or she puts all cards exiled with Possibility Storm on the bottom of his or her library in a random order.').
card_mana_cost('possibility storm', ['3', 'R', 'R']).
card_cmc('possibility storm', 5).
card_layout('possibility storm', 'normal').

% Found in: NPH
card_name('postmortem lunge', 'Postmortem Lunge').
card_type('postmortem lunge', 'Sorcery').
card_types('postmortem lunge', ['Sorcery']).
card_subtypes('postmortem lunge', []).
card_colors('postmortem lunge', ['B']).
card_text('postmortem lunge', '({B/P} can be paid with either {B} or 2 life.)\nReturn target creature card with converted mana cost X from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step.').
card_mana_cost('postmortem lunge', ['X', 'B/P']).
card_cmc('postmortem lunge', 1).
card_layout('postmortem lunge', 'normal').

% Found in: PLC
card_name('poultice sliver', 'Poultice Sliver').
card_type('poultice sliver', 'Creature — Sliver').
card_types('poultice sliver', ['Creature']).
card_subtypes('poultice sliver', ['Sliver']).
card_colors('poultice sliver', ['W']).
card_text('poultice sliver', 'All Slivers have \"{2}, {T}: Regenerate target Sliver.\"').
card_mana_cost('poultice sliver', ['2', 'W']).
card_cmc('poultice sliver', 3).
card_layout('poultice sliver', 'normal').
card_power('poultice sliver', 2).
card_toughness('poultice sliver', 2).

% Found in: UGL
card_name('poultrygeist', 'Poultrygeist').
card_type('poultrygeist', 'Creature — Chicken').
card_types('poultrygeist', ['Creature']).
card_subtypes('poultrygeist', ['Chicken']).
card_colors('poultrygeist', ['B']).
card_text('poultrygeist', 'Flying\nWhenever a creature is put into any graveyard from play, you may roll a six-sided die. On a 1, sacrifice Poultrygeist. Otherwise, put a +1/+1 counter on Poultrygeist.').
card_mana_cost('poultrygeist', ['2', 'B']).
card_cmc('poultrygeist', 3).
card_layout('poultrygeist', 'normal').
card_power('poultrygeist', 1).
card_toughness('poultrygeist', 1).

% Found in: USG, pARL
card_name('pouncing jaguar', 'Pouncing Jaguar').
card_type('pouncing jaguar', 'Creature — Cat').
card_types('pouncing jaguar', ['Creature']).
card_subtypes('pouncing jaguar', ['Cat']).
card_colors('pouncing jaguar', ['G']).
card_text('pouncing jaguar', 'Echo {G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_mana_cost('pouncing jaguar', ['G']).
card_cmc('pouncing jaguar', 1).
card_layout('pouncing jaguar', 'normal').
card_power('pouncing jaguar', 2).
card_toughness('pouncing jaguar', 2).

% Found in: INV
card_name('pouncing kavu', 'Pouncing Kavu').
card_type('pouncing kavu', 'Creature — Kavu').
card_types('pouncing kavu', ['Creature']).
card_subtypes('pouncing kavu', ['Kavu']).
card_colors('pouncing kavu', ['R']).
card_text('pouncing kavu', 'Kicker {2}{R} (You may pay an additional {2}{R} as you cast this spell.)\nFirst strike\nIf Pouncing Kavu was kicked, it enters the battlefield with two +1/+1 counters on it and with haste.').
card_mana_cost('pouncing kavu', ['1', 'R']).
card_cmc('pouncing kavu', 2).
card_layout('pouncing kavu', 'normal').
card_power('pouncing kavu', 1).
card_toughness('pouncing kavu', 1).

% Found in: PLC
card_name('pouncing wurm', 'Pouncing Wurm').
card_type('pouncing wurm', 'Creature — Wurm').
card_types('pouncing wurm', ['Creature']).
card_subtypes('pouncing wurm', ['Wurm']).
card_colors('pouncing wurm', ['G']).
card_text('pouncing wurm', 'Kicker {2}{G} (You may pay an additional {2}{G} as you cast this spell.)\nIf Pouncing Wurm was kicked, it enters the battlefield with three +1/+1 counters on it and with haste.').
card_mana_cost('pouncing wurm', ['3', 'G']).
card_cmc('pouncing wurm', 4).
card_layout('pouncing wurm', 'normal').
card_power('pouncing wurm', 3).
card_toughness('pouncing wurm', 3).

% Found in: UDS, pMPR
card_name('powder keg', 'Powder Keg').
card_type('powder keg', 'Artifact').
card_types('powder keg', ['Artifact']).
card_subtypes('powder keg', []).
card_colors('powder keg', []).
card_text('powder keg', 'At the beginning of your upkeep, you may put a fuse counter on Powder Keg.\n{T}, Sacrifice Powder Keg: Destroy each artifact and creature with converted mana cost equal to the number of fuse counters on Powder Keg.').
card_mana_cost('powder keg', ['2']).
card_cmc('powder keg', 2).
card_layout('powder keg', 'normal').
card_reserved('powder keg').

% Found in: DDE, INV
card_name('power armor', 'Power Armor').
card_type('power armor', 'Artifact').
card_types('power armor', ['Artifact']).
card_subtypes('power armor', []).
card_colors('power armor', []).
card_text('power armor', 'Domain — {3}, {T}: Target creature gets +1/+1 until end of turn for each basic land type among lands you control.').
card_mana_cost('power armor', ['4']).
card_cmc('power armor', 4).
card_layout('power armor', 'normal').

% Found in: ATQ, ME4
card_name('power artifact', 'Power Artifact').
card_type('power artifact', 'Enchantment — Aura').
card_types('power artifact', ['Enchantment']).
card_subtypes('power artifact', ['Aura']).
card_colors('power artifact', ['U']).
card_text('power artifact', 'Enchant artifact\nEnchanted artifact\'s activated abilities cost {2} less to activate. This effect can\'t reduce the amount of mana an ability costs to activate to less than one mana.').
card_mana_cost('power artifact', ['U', 'U']).
card_cmc('power artifact', 2).
card_layout('power artifact', 'normal').
card_reserved('power artifact').

% Found in: MRD
card_name('power conduit', 'Power Conduit').
card_type('power conduit', 'Artifact').
card_types('power conduit', ['Artifact']).
card_subtypes('power conduit', []).
card_colors('power conduit', []).
card_text('power conduit', '{T}, Remove a counter from a permanent you control: Choose one —\n• Put a charge counter on target artifact.\n• Put a +1/+1 counter on target creature.').
card_mana_cost('power conduit', ['2']).
card_cmc('power conduit', 2).
card_layout('power conduit', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('power leak', 'Power Leak').
card_type('power leak', 'Enchantment — Aura').
card_types('power leak', ['Enchantment']).
card_subtypes('power leak', ['Aura']).
card_colors('power leak', ['U']).
card_text('power leak', 'Enchant enchantment\nAt the beginning of the upkeep of enchanted enchantment\'s controller, that player may pay any amount of mana. Power Leak deals 2 damage to that player. Prevent X of that damage, where X is the amount of mana that player paid this way.').
card_mana_cost('power leak', ['1', 'U']).
card_cmc('power leak', 2).
card_layout('power leak', 'normal').

% Found in: MMQ
card_name('power matrix', 'Power Matrix').
card_type('power matrix', 'Artifact').
card_types('power matrix', ['Artifact']).
card_subtypes('power matrix', []).
card_colors('power matrix', []).
card_text('power matrix', '{T}: Target creature gets +1/+1 and gains flying, first strike, and trample until end of turn.').
card_mana_cost('power matrix', ['4']).
card_cmc('power matrix', 4).
card_layout('power matrix', 'normal').

% Found in: CNS, SHM
card_name('power of fire', 'Power of Fire').
card_type('power of fire', 'Enchantment — Aura').
card_types('power of fire', ['Enchantment']).
card_subtypes('power of fire', ['Aura']).
card_colors('power of fire', ['R']).
card_text('power of fire', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals 1 damage to target creature or player.\"').
card_mana_cost('power of fire', ['1', 'R']).
card_cmc('power of fire', 2).
card_layout('power of fire', 'normal').

% Found in: CNS
card_name('power play', 'Power Play').
card_type('power play', 'Conspiracy').
card_types('power play', ['Conspiracy']).
card_subtypes('power play', []).
card_colors('power play', []).
card_text('power play', '(Start the game with this conspiracy face up in the command zone.)\nYou are the starting player. If multiple players would be the starting player, one of those players is chosen at random.').
card_layout('power play', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, BTD, CED, CEI, ICE, ITP, LEA, LEB, MIR, RQS, TMP, USG, VMA
card_name('power sink', 'Power Sink').
card_type('power sink', 'Instant').
card_types('power sink', ['Instant']).
card_subtypes('power sink', []).
card_colors('power sink', ['U']).
card_text('power sink', 'Counter target spell unless its controller pays {X}. If he or she doesn\'t, that player taps all lands with mana abilities he or she controls and empties his or her mana pool.').
card_mana_cost('power sink', ['X', 'U']).
card_cmc('power sink', 1).
card_layout('power sink', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('power surge', 'Power Surge').
card_type('power surge', 'Enchantment').
card_types('power surge', ['Enchantment']).
card_subtypes('power surge', []).
card_colors('power surge', ['R']).
card_text('power surge', 'At the beginning of each player\'s upkeep, Power Surge deals X damage to that player, where X is the number of untapped lands he or she controlled at the beginning of this turn.').
card_mana_cost('power surge', ['R', 'R']).
card_cmc('power surge', 2).
card_layout('power surge', 'normal').

% Found in: USG
card_name('power taint', 'Power Taint').
card_type('power taint', 'Enchantment — Aura').
card_types('power taint', ['Enchantment']).
card_subtypes('power taint', ['Aura']).
card_colors('power taint', ['U']).
card_text('power taint', 'Enchant enchantment\nAt the beginning of the upkeep of enchanted enchantment\'s controller, that player loses 2 life unless he or she pays {2}.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('power taint', ['1', 'U']).
card_cmc('power taint', 2).
card_layout('power taint', 'normal').

% Found in: ATQ
card_name('powerleech', 'Powerleech').
card_type('powerleech', 'Enchantment').
card_types('powerleech', ['Enchantment']).
card_subtypes('powerleech', []).
card_colors('powerleech', ['G']).
card_text('powerleech', 'Whenever an artifact an opponent controls becomes tapped or an opponent activates an artifact\'s ability without {T} in its activation cost, you gain 1 life.').
card_mana_cost('powerleech', ['G', 'G']).
card_cmc('powerleech', 2).
card_layout('powerleech', 'normal').
card_reserved('powerleech').

% Found in: APC
card_name('powerstone minefield', 'Powerstone Minefield').
card_type('powerstone minefield', 'Enchantment').
card_types('powerstone minefield', ['Enchantment']).
card_subtypes('powerstone minefield', []).
card_colors('powerstone minefield', ['W', 'R']).
card_text('powerstone minefield', 'Whenever a creature attacks or blocks, Powerstone Minefield deals 2 damage to it.').
card_mana_cost('powerstone minefield', ['2', 'R', 'W']).
card_cmc('powerstone minefield', 4).
card_layout('powerstone minefield', 'normal').

% Found in: 5ED, ICE, MED
card_name('pox', 'Pox').
card_type('pox', 'Sorcery').
card_types('pox', ['Sorcery']).
card_subtypes('pox', []).
card_colors('pox', ['B']).
card_text('pox', 'Each player loses a third of his or her life, then discards a third of the cards in his or her hand, then sacrifices a third of the creatures he or she controls, then sacrifices a third of the lands he or she controls. Round up each time.').
card_mana_cost('pox', ['B', 'B', 'B']).
card_cmc('pox', 3).
card_layout('pox', 'normal').

% Found in: 4ED, 5ED, 6ED, LEG
card_name('pradesh gypsies', 'Pradesh Gypsies').
card_type('pradesh gypsies', 'Creature — Human Nomad').
card_types('pradesh gypsies', ['Creature']).
card_subtypes('pradesh gypsies', ['Human', 'Nomad']).
card_colors('pradesh gypsies', ['G']).
card_text('pradesh gypsies', '{1}{G}, {T}: Target creature gets -2/-0 until end of turn.').
card_mana_cost('pradesh gypsies', ['2', 'G']).
card_cmc('pradesh gypsies', 3).
card_layout('pradesh gypsies', 'normal').
card_power('pradesh gypsies', 1).
card_toughness('pradesh gypsies', 1).

% Found in: C14, MBS
card_name('praetor\'s counsel', 'Praetor\'s Counsel').
card_type('praetor\'s counsel', 'Sorcery').
card_types('praetor\'s counsel', ['Sorcery']).
card_subtypes('praetor\'s counsel', []).
card_colors('praetor\'s counsel', ['G']).
card_text('praetor\'s counsel', 'Return all cards from your graveyard to your hand. Exile Praetor\'s Counsel. You have no maximum hand size for the rest of the game.').
card_mana_cost('praetor\'s counsel', ['5', 'G', 'G', 'G']).
card_cmc('praetor\'s counsel', 8).
card_layout('praetor\'s counsel', 'normal').

% Found in: NPH
card_name('praetor\'s grasp', 'Praetor\'s Grasp').
card_type('praetor\'s grasp', 'Sorcery').
card_types('praetor\'s grasp', ['Sorcery']).
card_subtypes('praetor\'s grasp', []).
card_colors('praetor\'s grasp', ['B']).
card_text('praetor\'s grasp', 'Search target opponent\'s library for a card and exile it face down. Then that player shuffles his or her library. You may look at and play that card for as long as it remains exiled.').
card_mana_cost('praetor\'s grasp', ['1', 'B', 'B']).
card_cmc('praetor\'s grasp', 3).
card_layout('praetor\'s grasp', 'normal').

% Found in: PC2
card_name('prahv', 'Prahv').
card_type('prahv', 'Plane — Ravnica').
card_types('prahv', ['Plane']).
card_subtypes('prahv', ['Ravnica']).
card_colors('prahv', []).
card_text('prahv', 'If you cast a spell this turn, you can\'t attack with creatures.\nIf you attacked with creatures this turn, you can\'t cast spells.\nWhenever you roll {C}, you gain life equal to the number of cards in your hand.').
card_layout('prahv', 'plane').

% Found in: DIS
card_name('prahv, spires of order', 'Prahv, Spires of Order').
card_type('prahv, spires of order', 'Land').
card_types('prahv, spires of order', ['Land']).
card_subtypes('prahv, spires of order', []).
card_colors('prahv, spires of order', []).
card_text('prahv, spires of order', '{T}: Add {1} to your mana pool.\n{4}{W}{U}, {T}: Prevent all damage a source of your choice would deal this turn.').
card_layout('prahv, spires of order', 'normal').

% Found in: BFZ, EXP
card_name('prairie stream', 'Prairie Stream').
card_type('prairie stream', 'Land — Plains Island').
card_types('prairie stream', ['Land']).
card_subtypes('prairie stream', ['Plains', 'Island']).
card_colors('prairie stream', []).
card_text('prairie stream', '({T}: Add {W} or {U} to your mana pool.)\nPrairie Stream enters the battlefield tapped unless you control two or more basic lands.').
card_layout('prairie stream', 'normal').

% Found in: DRK, MED
card_name('preacher', 'Preacher').
card_type('preacher', 'Creature — Human Cleric').
card_types('preacher', ['Creature']).
card_subtypes('preacher', ['Human', 'Cleric']).
card_colors('preacher', ['W']).
card_text('preacher', 'You may choose not to untap Preacher during your untap step.\n{T}: For as long as Preacher remains tapped, gain control of target creature of an opponent\'s choice he or she controls.').
card_mana_cost('preacher', ['1', 'W', 'W']).
card_cmc('preacher', 3).
card_layout('preacher', 'normal').
card_power('preacher', 1).
card_toughness('preacher', 1).
card_reserved('preacher').

% Found in: DDO, RTR
card_name('precinct captain', 'Precinct Captain').
card_type('precinct captain', 'Creature — Human Soldier').
card_types('precinct captain', ['Creature']).
card_subtypes('precinct captain', ['Human', 'Soldier']).
card_colors('precinct captain', ['W']).
card_text('precinct captain', 'First strike\nWhenever Precinct Captain deals combat damage to a player, put a 1/1 white Soldier creature token onto the battlefield.').
card_mana_cost('precinct captain', ['W', 'W']).
card_cmc('precinct captain', 2).
card_layout('precinct captain', 'normal').
card_power('precinct captain', 2).
card_toughness('precinct captain', 2).

% Found in: TMP
card_name('precognition', 'Precognition').
card_type('precognition', 'Enchantment').
card_types('precognition', ['Enchantment']).
card_subtypes('precognition', []).
card_colors('precognition', ['U']).
card_text('precognition', 'At the beginning of your upkeep, you may look at the top card of target opponent\'s library. If you do, you may put that card on the bottom of that player\'s library.').
card_mana_cost('precognition', ['4', 'U']).
card_cmc('precognition', 5).
card_layout('precognition', 'normal').

% Found in: MM2, SOM
card_name('precursor golem', 'Precursor Golem').
card_type('precursor golem', 'Artifact Creature — Golem').
card_types('precursor golem', ['Artifact', 'Creature']).
card_subtypes('precursor golem', ['Golem']).
card_colors('precursor golem', []).
card_text('precursor golem', 'When Precursor Golem enters the battlefield, put two 3/3 colorless Golem artifact creature tokens onto the battlefield.\nWhenever a player casts an instant or sorcery spell that targets only a single Golem, that player copies that spell for each other Golem that spell could target. Each copy targets a different one of those Golems.').
card_mana_cost('precursor golem', ['5']).
card_cmc('precursor golem', 5).
card_layout('precursor golem', 'normal').
card_power('precursor golem', 3).
card_toughness('precursor golem', 3).

% Found in: ALA
card_name('predator dragon', 'Predator Dragon').
card_type('predator dragon', 'Creature — Dragon').
card_types('predator dragon', ['Creature']).
card_subtypes('predator dragon', ['Dragon']).
card_colors('predator dragon', ['R']).
card_text('predator dragon', 'Flying, haste\nDevour 2 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with twice that many +1/+1 counters on it.)').
card_mana_cost('predator dragon', ['3', 'R', 'R', 'R']).
card_cmc('predator dragon', 6).
card_layout('predator dragon', 'normal').
card_power('predator dragon', 4).
card_toughness('predator dragon', 4).

% Found in: DKA
card_name('predator ooze', 'Predator Ooze').
card_type('predator ooze', 'Creature — Ooze').
card_types('predator ooze', ['Creature']).
card_subtypes('predator ooze', ['Ooze']).
card_colors('predator ooze', ['G']).
card_text('predator ooze', 'Indestructible\nWhenever Predator Ooze attacks, put a +1/+1 counter on it.\nWhenever a creature dealt damage by Predator Ooze this turn dies, put a +1/+1 counter on Predator Ooze.').
card_mana_cost('predator ooze', ['G', 'G', 'G']).
card_cmc('predator ooze', 3).
card_layout('predator ooze', 'normal').
card_power('predator ooze', 1).
card_toughness('predator ooze', 1).

% Found in: AVR
card_name('predator\'s gambit', 'Predator\'s Gambit').
card_type('predator\'s gambit', 'Enchantment — Aura').
card_types('predator\'s gambit', ['Enchantment']).
card_subtypes('predator\'s gambit', ['Aura']).
card_colors('predator\'s gambit', ['B']).
card_text('predator\'s gambit', 'Enchant creature\nEnchanted creature gets +2/+1.\nEnchanted creature has intimidate as long as its controller controls no other creatures. (It can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_mana_cost('predator\'s gambit', ['B']).
card_cmc('predator\'s gambit', 1).
card_layout('predator\'s gambit', 'normal').

% Found in: CNS
card_name('predator\'s howl', 'Predator\'s Howl').
card_type('predator\'s howl', 'Instant').
card_types('predator\'s howl', ['Instant']).
card_subtypes('predator\'s howl', []).
card_colors('predator\'s howl', ['G']).
card_text('predator\'s howl', 'Put a 2/2 green Wolf creature token onto the battlefield.\nMorbid — Put three 2/2 green Wolf creature tokens onto the battlefield instead if a creature died this turn.').
card_mana_cost('predator\'s howl', ['3', 'G']).
card_cmc('predator\'s howl', 4).
card_layout('predator\'s howl', 'normal').

% Found in: GTC
card_name('predator\'s rapport', 'Predator\'s Rapport').
card_type('predator\'s rapport', 'Instant').
card_types('predator\'s rapport', ['Instant']).
card_subtypes('predator\'s rapport', []).
card_colors('predator\'s rapport', ['G']).
card_text('predator\'s rapport', 'Choose target creature you control. You gain life equal to that creature\'s power plus its toughness.').
card_mana_cost('predator\'s rapport', ['2', 'G']).
card_cmc('predator\'s rapport', 3).
card_layout('predator\'s rapport', 'normal').

% Found in: MRD
card_name('predator\'s strike', 'Predator\'s Strike').
card_type('predator\'s strike', 'Instant').
card_types('predator\'s strike', ['Instant']).
card_subtypes('predator\'s strike', []).
card_colors('predator\'s strike', ['G']).
card_text('predator\'s strike', 'Target creature gets +3/+3 and gains trample until end of turn.').
card_mana_cost('predator\'s strike', ['1', 'G']).
card_cmc('predator\'s strike', 2).
card_layout('predator\'s strike', 'normal').

% Found in: C14, NMS, VMA
card_name('predator, flagship', 'Predator, Flagship').
card_type('predator, flagship', 'Legendary Artifact').
card_types('predator, flagship', ['Artifact']).
card_subtypes('predator, flagship', []).
card_supertypes('predator, flagship', ['Legendary']).
card_colors('predator, flagship', []).
card_text('predator, flagship', '{2}: Target creature gains flying until end of turn.\n{5}, {T}: Destroy target creature with flying.').
card_mana_cost('predator, flagship', ['5']).
card_cmc('predator, flagship', 5).
card_layout('predator, flagship', 'normal').

% Found in: ARB
card_name('predatory advantage', 'Predatory Advantage').
card_type('predatory advantage', 'Enchantment').
card_types('predatory advantage', ['Enchantment']).
card_subtypes('predatory advantage', []).
card_colors('predatory advantage', ['R', 'G']).
card_text('predatory advantage', 'At the beginning of each opponent\'s end step, if that player didn\'t cast a creature spell this turn, put a 2/2 green Lizard creature token onto the battlefield.').
card_mana_cost('predatory advantage', ['3', 'R', 'G']).
card_cmc('predatory advantage', 5).
card_layout('predatory advantage', 'normal').

% Found in: GPT
card_name('predatory focus', 'Predatory Focus').
card_type('predatory focus', 'Sorcery').
card_types('predatory focus', ['Sorcery']).
card_subtypes('predatory focus', []).
card_colors('predatory focus', ['G']).
card_text('predatory focus', 'You may have creatures you control assign their combat damage this turn as though they weren\'t blocked.').
card_mana_cost('predatory focus', ['3', 'G', 'G']).
card_cmc('predatory focus', 5).
card_layout('predatory focus', 'normal').

% Found in: EXO
card_name('predatory hunger', 'Predatory Hunger').
card_type('predatory hunger', 'Enchantment — Aura').
card_types('predatory hunger', ['Enchantment']).
card_subtypes('predatory hunger', ['Aura']).
card_colors('predatory hunger', ['G']).
card_text('predatory hunger', 'Enchant creature\nWhenever an opponent casts a creature spell, put a +1/+1 counter on enchanted creature.').
card_mana_cost('predatory hunger', ['G']).
card_cmc('predatory hunger', 1).
card_layout('predatory hunger', 'normal').

% Found in: PO2, VMA
card_name('predatory nightstalker', 'Predatory Nightstalker').
card_type('predatory nightstalker', 'Creature — Nightstalker').
card_types('predatory nightstalker', ['Creature']).
card_subtypes('predatory nightstalker', ['Nightstalker']).
card_colors('predatory nightstalker', ['B']).
card_text('predatory nightstalker', 'When Predatory Nightstalker enters the battlefield, you may have target opponent sacrifice a creature.').
card_mana_cost('predatory nightstalker', ['3', 'B', 'B']).
card_cmc('predatory nightstalker', 5).
card_layout('predatory nightstalker', 'normal').
card_power('predatory nightstalker', 3).
card_toughness('predatory nightstalker', 2).

% Found in: M13
card_name('predatory rampage', 'Predatory Rampage').
card_type('predatory rampage', 'Sorcery').
card_types('predatory rampage', ['Sorcery']).
card_subtypes('predatory rampage', []).
card_colors('predatory rampage', ['G']).
card_text('predatory rampage', 'Creatures you control get +3/+3 until end of turn. Each creature your opponents control blocks this turn if able.').
card_mana_cost('predatory rampage', ['3', 'G', 'G']).
card_cmc('predatory rampage', 5).
card_layout('predatory rampage', 'normal').

% Found in: M14
card_name('predatory sliver', 'Predatory Sliver').
card_type('predatory sliver', 'Creature — Sliver').
card_types('predatory sliver', ['Creature']).
card_subtypes('predatory sliver', ['Sliver']).
card_colors('predatory sliver', ['G']).
card_text('predatory sliver', 'Sliver creatures you control get +1/+1.').
card_mana_cost('predatory sliver', ['1', 'G']).
card_cmc('predatory sliver', 2).
card_layout('predatory sliver', 'normal').
card_power('predatory sliver', 1).
card_toughness('predatory sliver', 1).

% Found in: PC2, ZEN
card_name('predatory urge', 'Predatory Urge').
card_type('predatory urge', 'Enchantment — Aura').
card_types('predatory urge', ['Enchantment']).
card_subtypes('predatory urge', ['Aura']).
card_colors('predatory urge', ['G']).
card_text('predatory urge', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals damage equal to its power to target creature. That creature deals damage equal to its power to this creature.\"').
card_mana_cost('predatory urge', ['3', 'G']).
card_cmc('predatory urge', 4).
card_layout('predatory urge', 'normal').

% Found in: ODY
card_name('predict', 'Predict').
card_type('predict', 'Instant').
card_types('predict', ['Instant']).
card_subtypes('predict', []).
card_colors('predict', ['U']).
card_text('predict', 'Name a card, then target player puts the top card of his or her library into his or her graveyard. If that card is the named card, you draw two cards. Otherwise, you draw a card.').
card_mana_cost('predict', ['1', 'U']).
card_cmc('predict', 2).
card_layout('predict', 'normal').

% Found in: M15, MOR
card_name('preeminent captain', 'Preeminent Captain').
card_type('preeminent captain', 'Creature — Kithkin Soldier').
card_types('preeminent captain', ['Creature']).
card_subtypes('preeminent captain', ['Kithkin', 'Soldier']).
card_colors('preeminent captain', ['W']).
card_text('preeminent captain', 'First strike (This creature deals combat damage before creatures without first strike.)\nWhenever Preeminent Captain attacks, you may put a Soldier creature card from your hand onto the battlefield tapped and attacking.').
card_mana_cost('preeminent captain', ['2', 'W']).
card_cmc('preeminent captain', 3).
card_layout('preeminent captain', 'normal').
card_power('preeminent captain', 2).
card_toughness('preeminent captain', 2).

% Found in: PTK
card_name('preemptive strike', 'Preemptive Strike').
card_type('preemptive strike', 'Instant').
card_types('preemptive strike', ['Instant']).
card_subtypes('preemptive strike', []).
card_colors('preemptive strike', ['U']).
card_text('preemptive strike', 'Counter target creature spell.').
card_mana_cost('preemptive strike', ['1', 'U']).
card_cmc('preemptive strike', 2).
card_layout('preemptive strike', 'normal').

% Found in: MIR
card_name('preferred selection', 'Preferred Selection').
card_type('preferred selection', 'Enchantment').
card_types('preferred selection', ['Enchantment']).
card_subtypes('preferred selection', []).
card_colors('preferred selection', ['G']).
card_text('preferred selection', 'At the beginning of your upkeep, look at the top two cards of your library. You may sacrifice Preferred Selection and pay {2}{G}{G}. If you do, put one of those cards into your hand. If you don\'t, put one of those cards on the bottom of your library.').
card_mana_cost('preferred selection', ['2', 'G', 'G']).
card_cmc('preferred selection', 4).
card_layout('preferred selection', 'normal').
card_reserved('preferred selection').

% Found in: TSP
card_name('premature burial', 'Premature Burial').
card_type('premature burial', 'Sorcery').
card_types('premature burial', ['Sorcery']).
card_subtypes('premature burial', []).
card_colors('premature burial', ['B']).
card_text('premature burial', 'Destroy target nonblack creature that entered the battlefield since your last turn ended.').
card_mana_cost('premature burial', ['1', 'B']).
card_cmc('premature burial', 2).
card_layout('premature burial', 'normal').

% Found in: DDI, M11
card_name('preordain', 'Preordain').
card_type('preordain', 'Sorcery').
card_types('preordain', ['Sorcery']).
card_subtypes('preordain', []).
card_colors('preordain', ['U']).
card_text('preordain', 'Scry 2, then draw a card. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('preordain', ['U']).
card_cmc('preordain', 1).
card_layout('preordain', 'normal').

% Found in: THS
card_name('prescient chimera', 'Prescient Chimera').
card_type('prescient chimera', 'Creature — Chimera').
card_types('prescient chimera', ['Creature']).
card_subtypes('prescient chimera', ['Chimera']).
card_colors('prescient chimera', ['U']).
card_text('prescient chimera', 'Flying\nWhenever you cast an instant or sorcery spell, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('prescient chimera', ['3', 'U', 'U']).
card_cmc('prescient chimera', 5).
card_layout('prescient chimera', 'normal').
card_power('prescient chimera', 3).
card_toughness('prescient chimera', 4).

% Found in: C13, SHM
card_name('presence of gond', 'Presence of Gond').
card_type('presence of gond', 'Enchantment — Aura').
card_types('presence of gond', ['Enchantment']).
card_subtypes('presence of gond', ['Aura']).
card_colors('presence of gond', ['G']).
card_text('presence of gond', 'Enchant creature\nEnchanted creature has \"{T}: Put a 1/1 green Elf Warrior creature token onto the battlefield.\"').
card_mana_cost('presence of gond', ['2', 'G']).
card_cmc('presence of gond', 3).
card_layout('presence of gond', 'normal').

% Found in: LEG, USG
card_name('presence of the master', 'Presence of the Master').
card_type('presence of the master', 'Enchantment').
card_types('presence of the master', ['Enchantment']).
card_subtypes('presence of the master', []).
card_colors('presence of the master', ['W']).
card_text('presence of the master', 'Whenever a player casts an enchantment spell, counter it.').
card_mana_cost('presence of the master', ['3', 'W']).
card_cmc('presence of the master', 4).
card_layout('presence of the master', 'normal').

% Found in: SOK
card_name('presence of the wise', 'Presence of the Wise').
card_type('presence of the wise', 'Sorcery').
card_types('presence of the wise', ['Sorcery']).
card_subtypes('presence of the wise', []).
card_colors('presence of the wise', ['W']).
card_text('presence of the wise', 'You gain 2 life for each card in your hand.').
card_mana_cost('presence of the wise', ['2', 'W', 'W']).
card_cmc('presence of the wise', 4).
card_layout('presence of the wise', 'normal').

% Found in: DTK
card_name('press the advantage', 'Press the Advantage').
card_type('press the advantage', 'Instant').
card_types('press the advantage', ['Instant']).
card_subtypes('press the advantage', []).
card_colors('press the advantage', ['G']).
card_text('press the advantage', 'Up to two target creatures each get +2/+2 and gain trample until end of turn.').
card_mana_cost('press the advantage', ['2', 'G', 'G']).
card_cmc('press the advantage', 4).
card_layout('press the advantage', 'normal').

% Found in: FRF
card_name('pressure point', 'Pressure Point').
card_type('pressure point', 'Instant').
card_types('pressure point', ['Instant']).
card_subtypes('pressure point', []).
card_colors('pressure point', ['W']).
card_text('pressure point', 'Tap target creature.\nDraw a card.').
card_mana_cost('pressure point', ['1', 'W']).
card_cmc('pressure point', 2).
card_layout('pressure point', 'normal').

% Found in: MMQ
card_name('pretender\'s claim', 'Pretender\'s Claim').
card_type('pretender\'s claim', 'Enchantment — Aura').
card_types('pretender\'s claim', ['Enchantment']).
card_subtypes('pretender\'s claim', ['Aura']).
card_colors('pretender\'s claim', ['B']).
card_text('pretender\'s claim', 'Enchant creature\nWhenever enchanted creature becomes blocked, tap all lands defending player controls.').
card_mana_cost('pretender\'s claim', ['1', 'B']).
card_cmc('pretender\'s claim', 2).
card_layout('pretender\'s claim', 'normal').

% Found in: DDL, ISD, M13
card_name('prey upon', 'Prey Upon').
card_type('prey upon', 'Sorcery').
card_types('prey upon', ['Sorcery']).
card_subtypes('prey upon', []).
card_colors('prey upon', ['G']).
card_text('prey upon', 'Target creature you control fights target creature you don\'t control. (Each deals damage equal to its power to the other.)').
card_mana_cost('prey upon', ['G']).
card_cmc('prey upon', 1).
card_layout('prey upon', 'normal').

% Found in: ROE
card_name('prey\'s vengeance', 'Prey\'s Vengeance').
card_type('prey\'s vengeance', 'Instant').
card_types('prey\'s vengeance', ['Instant']).
card_subtypes('prey\'s vengeance', []).
card_colors('prey\'s vengeance', ['G']).
card_text('prey\'s vengeance', 'Target creature gets +2/+2 until end of turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('prey\'s vengeance', ['G']).
card_cmc('prey\'s vengeance', 1).
card_layout('prey\'s vengeance', 'normal').

% Found in: PC2
card_name('preyseizer dragon', 'Preyseizer Dragon').
card_type('preyseizer dragon', 'Creature — Dragon').
card_types('preyseizer dragon', ['Creature']).
card_subtypes('preyseizer dragon', ['Dragon']).
card_colors('preyseizer dragon', ['R']).
card_text('preyseizer dragon', 'Flying\nDevour 2 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with twice that many +1/+1 counters on it.)\nWhenever Preyseizer Dragon attacks, it deals damage to target creature or player equal to the number of +1/+1 counters on Preyseizer Dragon.').
card_mana_cost('preyseizer dragon', ['4', 'R', 'R']).
card_cmc('preyseizer dragon', 6).
card_layout('preyseizer dragon', 'normal').
card_power('preyseizer dragon', 4).
card_toughness('preyseizer dragon', 4).

% Found in: ODY
card_name('price of glory', 'Price of Glory').
card_type('price of glory', 'Enchantment').
card_types('price of glory', ['Enchantment']).
card_subtypes('price of glory', []).
card_colors('price of glory', ['R']).
card_text('price of glory', 'Whenever a player taps a land for mana, if it\'s not that player\'s turn, destroy that land.').
card_mana_cost('price of glory', ['2', 'R']).
card_cmc('price of glory', 3).
card_layout('price of glory', 'normal').

% Found in: C13
card_name('price of knowledge', 'Price of Knowledge').
card_type('price of knowledge', 'Enchantment').
card_types('price of knowledge', ['Enchantment']).
card_subtypes('price of knowledge', []).
card_colors('price of knowledge', ['B']).
card_text('price of knowledge', 'Players have no maximum hand size.\nAt the beginning of each opponent\'s upkeep, Price of Knowledge deals damage to that player equal to the number of cards in that player\'s hand.').
card_mana_cost('price of knowledge', ['6', 'B']).
card_cmc('price of knowledge', 7).
card_layout('price of knowledge', 'normal').

% Found in: EXO, PD2
card_name('price of progress', 'Price of Progress').
card_type('price of progress', 'Instant').
card_types('price of progress', ['Instant']).
card_subtypes('price of progress', []).
card_colors('price of progress', ['R']).
card_text('price of progress', 'Price of Progress deals damage to each player equal to twice the number of nonbasic lands that player controls.').
card_mana_cost('price of progress', ['1', 'R']).
card_cmc('price of progress', 2).
card_layout('price of progress', 'normal').

% Found in: ORI
card_name('prickleboar', 'Prickleboar').
card_type('prickleboar', 'Creature — Boar').
card_types('prickleboar', ['Creature']).
card_subtypes('prickleboar', ['Boar']).
card_colors('prickleboar', ['R']).
card_text('prickleboar', 'As long as it\'s your turn, Prickleboar gets +2/+0 and has first strike. (It deals combat damage before creatures without first strike.)').
card_mana_cost('prickleboar', ['4', 'R']).
card_cmc('prickleboar', 5).
card_layout('prickleboar', 'normal').
card_power('prickleboar', 3).
card_toughness('prickleboar', 3).

% Found in: MOR
card_name('prickly boggart', 'Prickly Boggart').
card_type('prickly boggart', 'Creature — Goblin Rogue').
card_types('prickly boggart', ['Creature']).
card_subtypes('prickly boggart', ['Goblin', 'Rogue']).
card_colors('prickly boggart', ['B']).
card_text('prickly boggart', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('prickly boggart', ['B']).
card_cmc('prickly boggart', 1).
card_layout('prickly boggart', 'normal').
card_power('prickly boggart', 1).
card_toughness('prickly boggart', 1).

% Found in: CNS, M12
card_name('pride guardian', 'Pride Guardian').
card_type('pride guardian', 'Creature — Cat Monk').
card_types('pride guardian', ['Creature']).
card_subtypes('pride guardian', ['Cat', 'Monk']).
card_colors('pride guardian', ['W']).
card_text('pride guardian', 'Defender\nWhenever Pride Guardian blocks, you gain 3 life.').
card_mana_cost('pride guardian', ['W']).
card_cmc('pride guardian', 1).
card_layout('pride guardian', 'normal').
card_power('pride guardian', 0).
card_toughness('pride guardian', 3).

% Found in: 7ED, DDH, S99
card_name('pride of lions', 'Pride of Lions').
card_type('pride of lions', 'Creature — Cat').
card_types('pride of lions', ['Creature']).
card_subtypes('pride of lions', ['Cat']).
card_colors('pride of lions', ['G']).
card_text('pride of lions', 'You may have Pride of Lions assign its combat damage as though it weren\'t blocked.').
card_mana_cost('pride of lions', ['3', 'G', 'G']).
card_cmc('pride of lions', 5).
card_layout('pride of lions', 'normal').
card_power('pride of lions', 4).
card_toughness('pride of lions', 4).

% Found in: DIS
card_name('pride of the clouds', 'Pride of the Clouds').
card_type('pride of the clouds', 'Creature — Elemental Cat').
card_types('pride of the clouds', ['Creature']).
card_subtypes('pride of the clouds', ['Elemental', 'Cat']).
card_colors('pride of the clouds', ['W', 'U']).
card_text('pride of the clouds', 'Flying\nPride of the Clouds gets +1/+1 for each other creature with flying on the battlefield.\nForecast — {2}{W}{U}, Reveal Pride of the Clouds from your hand: Put a 1/1 white and blue Bird creature token with flying onto the battlefield. (Activate this ability only during your upkeep and only once each turn.)').
card_mana_cost('pride of the clouds', ['W', 'U']).
card_cmc('pride of the clouds', 2).
card_layout('pride of the clouds', 'normal').
card_power('pride of the clouds', 1).
card_toughness('pride of the clouds', 1).

% Found in: DDE, USG
card_name('priest of gix', 'Priest of Gix').
card_type('priest of gix', 'Creature — Human Cleric Minion').
card_types('priest of gix', ['Creature']).
card_subtypes('priest of gix', ['Human', 'Cleric', 'Minion']).
card_colors('priest of gix', ['B']).
card_text('priest of gix', 'When Priest of Gix enters the battlefield, add {B}{B}{B} to your mana pool.').
card_mana_cost('priest of gix', ['2', 'B']).
card_cmc('priest of gix', 3).
card_layout('priest of gix', 'normal').
card_power('priest of gix', 2).
card_toughness('priest of gix', 1).

% Found in: THS
card_name('priest of iroas', 'Priest of Iroas').
card_type('priest of iroas', 'Creature — Human Cleric').
card_types('priest of iroas', ['Creature']).
card_subtypes('priest of iroas', ['Human', 'Cleric']).
card_colors('priest of iroas', ['R']).
card_text('priest of iroas', '{3}{W}, Sacrifice Priest of Iroas: Destroy target enchantment.').
card_mana_cost('priest of iroas', ['R']).
card_cmc('priest of iroas', 1).
card_layout('priest of iroas', 'normal').
card_power('priest of iroas', 1).
card_toughness('priest of iroas', 1).

% Found in: ORI
card_name('priest of the blood rite', 'Priest of the Blood Rite').
card_type('priest of the blood rite', 'Creature — Human Cleric').
card_types('priest of the blood rite', ['Creature']).
card_subtypes('priest of the blood rite', ['Human', 'Cleric']).
card_colors('priest of the blood rite', ['B']).
card_text('priest of the blood rite', 'When Priest of the Blood Rite enters the battlefield, put a 5/5 black Demon creature token with flying onto the battlefield.\nAt the beginning of your upkeep, you lose 2 life.').
card_mana_cost('priest of the blood rite', ['3', 'B', 'B']).
card_cmc('priest of the blood rite', 5).
card_layout('priest of the blood rite', 'normal').
card_power('priest of the blood rite', 2).
card_toughness('priest of the blood rite', 2).

% Found in: C14, USG, pFNM
card_name('priest of titania', 'Priest of Titania').
card_type('priest of titania', 'Creature — Elf Druid').
card_types('priest of titania', ['Creature']).
card_subtypes('priest of titania', ['Elf', 'Druid']).
card_colors('priest of titania', ['G']).
card_text('priest of titania', '{T}: Add {G} to your mana pool for each Elf on the battlefield.').
card_mana_cost('priest of titania', ['1', 'G']).
card_cmc('priest of titania', 2).
card_layout('priest of titania', 'normal').
card_power('priest of titania', 1).
card_toughness('priest of titania', 1).

% Found in: NPH, pMGD
card_name('priest of urabrask', 'Priest of Urabrask').
card_type('priest of urabrask', 'Creature — Human Cleric').
card_types('priest of urabrask', ['Creature']).
card_subtypes('priest of urabrask', ['Human', 'Cleric']).
card_colors('priest of urabrask', ['R']).
card_text('priest of urabrask', 'When Priest of Urabrask enters the battlefield, add {R}{R}{R} to your mana pool.').
card_mana_cost('priest of urabrask', ['2', 'R']).
card_cmc('priest of urabrask', 3).
card_layout('priest of urabrask', 'normal').
card_power('priest of urabrask', 2).
card_toughness('priest of urabrask', 1).

% Found in: ATQ
card_name('priest of yawgmoth', 'Priest of Yawgmoth').
card_type('priest of yawgmoth', 'Creature — Human Cleric').
card_types('priest of yawgmoth', ['Creature']).
card_subtypes('priest of yawgmoth', ['Human', 'Cleric']).
card_colors('priest of yawgmoth', ['B']).
card_text('priest of yawgmoth', '{T}, Sacrifice an artifact: Add to your mana pool an amount of {B} equal to the sacrificed artifact\'s converted mana cost.').
card_mana_cost('priest of yawgmoth', ['1', 'B']).
card_cmc('priest of yawgmoth', 2).
card_layout('priest of yawgmoth', 'normal').
card_power('priest of yawgmoth', 1).
card_toughness('priest of yawgmoth', 2).

% Found in: MBS
card_name('priests of norn', 'Priests of Norn').
card_type('priests of norn', 'Creature — Cleric').
card_types('priests of norn', ['Creature']).
card_subtypes('priests of norn', ['Cleric']).
card_colors('priests of norn', ['W']).
card_text('priests of norn', 'Vigilance\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('priests of norn', ['2', 'W']).
card_cmc('priests of norn', 3).
card_layout('priests of norn', 'normal').
card_power('priests of norn', 1).
card_toughness('priests of norn', 4).

% Found in: ZEN
card_name('primal bellow', 'Primal Bellow').
card_type('primal bellow', 'Instant').
card_types('primal bellow', ['Instant']).
card_subtypes('primal bellow', []).
card_colors('primal bellow', ['G']).
card_text('primal bellow', 'Target creature gets +1/+1 until end of turn for each Forest you control.').
card_mana_cost('primal bellow', ['G']).
card_cmc('primal bellow', 1).
card_layout('primal bellow', 'normal').

% Found in: MOR
card_name('primal beyond', 'Primal Beyond').
card_type('primal beyond', 'Land').
card_types('primal beyond', ['Land']).
card_subtypes('primal beyond', []).
card_colors('primal beyond', []).
card_text('primal beyond', 'As Primal Beyond enters the battlefield, you may reveal an Elemental card from your hand. If you don\'t, Primal Beyond enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add one mana of any color to your mana pool. Spend this mana only to cast an Elemental spell or activate an ability of an Elemental.').
card_layout('primal beyond', 'normal').

% Found in: ONS
card_name('primal boost', 'Primal Boost').
card_type('primal boost', 'Instant').
card_types('primal boost', ['Instant']).
card_subtypes('primal boost', []).
card_colors('primal boost', ['G']).
card_text('primal boost', 'Target creature gets +4/+4 until end of turn.\nCycling {2}{G} ({2}{G}, Discard this card: Draw a card.)\nWhen you cycle Primal Boost, you may have target creature get +1/+1 until end of turn.').
card_mana_cost('primal boost', ['2', 'G']).
card_cmc('primal boost', 3).
card_layout('primal boost', 'normal').

% Found in: 3ED, 4ED, 5ED, 6ED, ATQ, M13, ME4
card_name('primal clay', 'Primal Clay').
card_type('primal clay', 'Artifact Creature — Shapeshifter').
card_types('primal clay', ['Artifact', 'Creature']).
card_subtypes('primal clay', ['Shapeshifter']).
card_colors('primal clay', []).
card_text('primal clay', 'As Primal Clay enters the battlefield, it becomes your choice of a 3/3 artifact creature, a 2/2 artifact creature with flying, or a 1/6 Wall artifact creature with defender in addition to its other types. (A creature with defender can\'t attack.)').
card_mana_cost('primal clay', ['4']).
card_cmc('primal clay', 4).
card_layout('primal clay', 'normal').
card_power('primal clay', '*').
card_toughness('primal clay', '*').

% Found in: M11
card_name('primal cocoon', 'Primal Cocoon').
card_type('primal cocoon', 'Enchantment — Aura').
card_types('primal cocoon', ['Enchantment']).
card_subtypes('primal cocoon', ['Aura']).
card_colors('primal cocoon', ['G']).
card_text('primal cocoon', 'Enchant creature\nAt the beginning of your upkeep, put a +1/+1 counter on enchanted creature.\nWhen enchanted creature attacks or blocks, sacrifice Primal Cocoon.').
card_mana_cost('primal cocoon', ['G']).
card_cmc('primal cocoon', 1).
card_layout('primal cocoon', 'normal').

% Found in: ARC, DDP, LRW
card_name('primal command', 'Primal Command').
card_type('primal command', 'Sorcery').
card_types('primal command', ['Sorcery']).
card_subtypes('primal command', []).
card_colors('primal command', ['G']).
card_text('primal command', 'Choose two —\n• Target player gains 7 life.\n• Put target noncreature permanent on top of its owner\'s library.\n• Target player shuffles his or her graveyard into his or her library.\n• Search your library for a creature card, reveal it, put it into your hand, then shuffle your library.').
card_mana_cost('primal command', ['3', 'G', 'G']).
card_cmc('primal command', 5).
card_layout('primal command', 'normal').

% Found in: TSP
card_name('primal forcemage', 'Primal Forcemage').
card_type('primal forcemage', 'Creature — Elf Shaman').
card_types('primal forcemage', ['Creature']).
card_subtypes('primal forcemage', ['Elf', 'Shaman']).
card_colors('primal forcemage', ['G']).
card_text('primal forcemage', 'Whenever another creature enters the battlefield under your control, that creature gets +3/+3 until end of turn.').
card_mana_cost('primal forcemage', ['2', 'G']).
card_cmc('primal forcemage', 3).
card_layout('primal forcemage', 'normal').
card_power('primal forcemage', 2).
card_toughness('primal forcemage', 2).

% Found in: ODY
card_name('primal frenzy', 'Primal Frenzy').
card_type('primal frenzy', 'Enchantment — Aura').
card_types('primal frenzy', ['Enchantment']).
card_subtypes('primal frenzy', ['Aura']).
card_colors('primal frenzy', ['G']).
card_text('primal frenzy', 'Enchant creature\nEnchanted creature has trample.').
card_mana_cost('primal frenzy', ['G']).
card_cmc('primal frenzy', 1).
card_layout('primal frenzy', 'normal').

% Found in: PLS
card_name('primal growth', 'Primal Growth').
card_type('primal growth', 'Sorcery').
card_types('primal growth', ['Sorcery']).
card_subtypes('primal growth', []).
card_colors('primal growth', ['G']).
card_text('primal growth', 'Kicker—Sacrifice a creature. (You may sacrifice a creature in addition to any other costs as you cast this spell.)\nSearch your library for a basic land card, put that card onto the battlefield, then shuffle your library. If Primal Growth was kicked, instead search your library for up to two basic land cards, put them onto the battlefield, then shuffle your library.').
card_mana_cost('primal growth', ['2', 'G']).
card_cmc('primal growth', 3).
card_layout('primal growth', 'normal').

% Found in: M13
card_name('primal huntbeast', 'Primal Huntbeast').
card_type('primal huntbeast', 'Creature — Beast').
card_types('primal huntbeast', ['Creature']).
card_subtypes('primal huntbeast', ['Beast']).
card_colors('primal huntbeast', ['G']).
card_text('primal huntbeast', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_mana_cost('primal huntbeast', ['3', 'G']).
card_cmc('primal huntbeast', 4).
card_layout('primal huntbeast', 'normal').
card_power('primal huntbeast', 3).
card_toughness('primal huntbeast', 3).

% Found in: 5ED, HML, MED
card_name('primal order', 'Primal Order').
card_type('primal order', 'Enchantment').
card_types('primal order', ['Enchantment']).
card_subtypes('primal order', []).
card_colors('primal order', ['G']).
card_text('primal order', 'At the beginning of each player\'s upkeep, Primal Order deals damage to that player equal to the number of nonbasic lands he or she controls.').
card_mana_cost('primal order', ['2', 'G', 'G']).
card_cmc('primal order', 4).
card_layout('primal order', 'normal').

% Found in: DDI, PC2, PLC
card_name('primal plasma', 'Primal Plasma').
card_type('primal plasma', 'Creature — Elemental Shapeshifter').
card_types('primal plasma', ['Creature']).
card_subtypes('primal plasma', ['Elemental', 'Shapeshifter']).
card_colors('primal plasma', ['U']).
card_text('primal plasma', 'As Primal Plasma enters the battlefield, it becomes your choice of a 3/3 creature, a 2/2 creature with flying, or a 1/6 creature with defender.').
card_mana_cost('primal plasma', ['3', 'U']).
card_cmc('primal plasma', 4).
card_layout('primal plasma', 'normal').
card_power('primal plasma', '*').
card_toughness('primal plasma', '*').

% Found in: 10E, STH
card_name('primal rage', 'Primal Rage').
card_type('primal rage', 'Enchantment').
card_types('primal rage', ['Enchantment']).
card_subtypes('primal rage', []).
card_colors('primal rage', ['G']).
card_text('primal rage', 'Creatures you control have trample. (If a creature you control would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('primal rage', ['1', 'G']).
card_cmc('primal rage', 2).
card_layout('primal rage', 'normal').

% Found in: AVR
card_name('primal surge', 'Primal Surge').
card_type('primal surge', 'Sorcery').
card_types('primal surge', ['Sorcery']).
card_subtypes('primal surge', []).
card_colors('primal surge', ['G']).
card_text('primal surge', 'Exile the top card of your library. If it\'s a permanent card, you may put it onto the battlefield. If you do, repeat this process.').
card_mana_cost('primal surge', ['8', 'G', 'G']).
card_cmc('primal surge', 10).
card_layout('primal surge', 'normal').

% Found in: C13
card_name('primal vigor', 'Primal Vigor').
card_type('primal vigor', 'Enchantment').
card_types('primal vigor', ['Enchantment']).
card_subtypes('primal vigor', []).
card_colors('primal vigor', ['G']).
card_text('primal vigor', 'If one or more tokens would be put onto the battlefield, twice that many of those tokens are put onto the battlefield instead.\nIf one or more +1/+1 counters would be placed on a creature, twice that many +1/+1 counters are placed on that creature instead.').
card_mana_cost('primal vigor', ['4', 'G']).
card_cmc('primal vigor', 5).
card_layout('primal vigor', 'normal').

% Found in: GTC
card_name('primal visitation', 'Primal Visitation').
card_type('primal visitation', 'Enchantment — Aura').
card_types('primal visitation', ['Enchantment']).
card_subtypes('primal visitation', ['Aura']).
card_colors('primal visitation', ['R', 'G']).
card_text('primal visitation', 'Enchant creature\nEnchanted creature gets +3/+3 and has haste.').
card_mana_cost('primal visitation', ['3', 'R', 'G']).
card_cmc('primal visitation', 5).
card_layout('primal visitation', 'normal').

% Found in: LGN
card_name('primal whisperer', 'Primal Whisperer').
card_type('primal whisperer', 'Creature — Elf Soldier').
card_types('primal whisperer', ['Creature']).
card_subtypes('primal whisperer', ['Elf', 'Soldier']).
card_colors('primal whisperer', ['G']).
card_text('primal whisperer', 'Primal Whisperer gets +2/+2 for each face-down creature on the battlefield.\nMorph {3}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('primal whisperer', ['4', 'G']).
card_cmc('primal whisperer', 5).
card_layout('primal whisperer', 'normal').
card_power('primal whisperer', 2).
card_toughness('primal whisperer', 2).

% Found in: EVE
card_name('primalcrux', 'Primalcrux').
card_type('primalcrux', 'Creature — Elemental').
card_types('primalcrux', ['Creature']).
card_subtypes('primalcrux', ['Elemental']).
card_colors('primalcrux', ['G']).
card_text('primalcrux', 'Trample\nChroma — Primalcrux\'s power and toughness are each equal to the number of green mana symbols in the mana costs of permanents you control.').
card_mana_cost('primalcrux', ['G', 'G', 'G', 'G', 'G', 'G']).
card_cmc('primalcrux', 6).
card_layout('primalcrux', 'normal').
card_power('primalcrux', '*').
card_toughness('primalcrux', '*').

% Found in: GTC
card_name('prime speaker zegana', 'Prime Speaker Zegana').
card_type('prime speaker zegana', 'Legendary Creature — Merfolk Wizard').
card_types('prime speaker zegana', ['Creature']).
card_subtypes('prime speaker zegana', ['Merfolk', 'Wizard']).
card_supertypes('prime speaker zegana', ['Legendary']).
card_colors('prime speaker zegana', ['U', 'G']).
card_text('prime speaker zegana', 'Prime Speaker Zegana enters the battlefield with X +1/+1 counters on it, where X is the greatest power among other creatures you control.\nWhen Prime Speaker Zegana enters the battlefield, draw cards equal to its power.').
card_mana_cost('prime speaker zegana', ['2', 'G', 'G', 'U', 'U']).
card_cmc('prime speaker zegana', 6).
card_layout('prime speaker zegana', 'normal').
card_power('prime speaker zegana', 1).
card_toughness('prime speaker zegana', 1).

% Found in: M14
card_name('primeval bounty', 'Primeval Bounty').
card_type('primeval bounty', 'Enchantment').
card_types('primeval bounty', ['Enchantment']).
card_subtypes('primeval bounty', []).
card_colors('primeval bounty', ['G']).
card_text('primeval bounty', 'Whenever you cast a creature spell, put a 3/3 green Beast creature token onto the battlefield.\nWhenever you cast a noncreature spell, put three +1/+1 counters on target creature you control.\nWhenever a land enters the battlefield under your control, you gain 3 life.').
card_mana_cost('primeval bounty', ['5', 'G']).
card_cmc('primeval bounty', 6).
card_layout('primeval bounty', 'normal').

% Found in: 8ED, POR
card_name('primeval force', 'Primeval Force').
card_type('primeval force', 'Creature — Elemental').
card_types('primeval force', ['Creature']).
card_subtypes('primeval force', ['Elemental']).
card_colors('primeval force', ['G']).
card_text('primeval force', 'When Primeval Force enters the battlefield, sacrifice it unless you sacrifice three Forests.').
card_mana_cost('primeval force', ['2', 'G', 'G', 'G']).
card_cmc('primeval force', 5).
card_layout('primeval force', 'normal').
card_power('primeval force', 8).
card_toughness('primeval force', 8).

% Found in: GPT
card_name('primeval light', 'Primeval Light').
card_type('primeval light', 'Sorcery').
card_types('primeval light', ['Sorcery']).
card_subtypes('primeval light', []).
card_colors('primeval light', ['G']).
card_text('primeval light', 'Destroy all enchantments target player controls.').
card_mana_cost('primeval light', ['3', 'G']).
card_cmc('primeval light', 4).
card_layout('primeval light', 'normal').

% Found in: 8ED, MMQ
card_name('primeval shambler', 'Primeval Shambler').
card_type('primeval shambler', 'Creature — Horror Mercenary').
card_types('primeval shambler', ['Creature']).
card_subtypes('primeval shambler', ['Horror', 'Mercenary']).
card_colors('primeval shambler', ['B']).
card_text('primeval shambler', '{B}: Primeval Shambler gets +1/+1 until end of turn.').
card_mana_cost('primeval shambler', ['4', 'B']).
card_cmc('primeval shambler', 5).
card_layout('primeval shambler', 'normal').
card_power('primeval shambler', 3).
card_toughness('primeval shambler', 3).

% Found in: M11, M12, MM2, pGPX
card_name('primeval titan', 'Primeval Titan').
card_type('primeval titan', 'Creature — Giant').
card_types('primeval titan', ['Creature']).
card_subtypes('primeval titan', ['Giant']).
card_colors('primeval titan', ['G']).
card_text('primeval titan', 'Trample\nWhenever Primeval Titan enters the battlefield or attacks, you may search your library for up to two land cards, put them onto the battlefield tapped, then shuffle your library.').
card_mana_cost('primeval titan', ['4', 'G', 'G']).
card_cmc('primeval titan', 6).
card_layout('primeval titan', 'normal').
card_power('primeval titan', 6).
card_toughness('primeval titan', 6).

% Found in: SCG
card_name('primitive etchings', 'Primitive Etchings').
card_type('primitive etchings', 'Enchantment').
card_types('primitive etchings', ['Enchantment']).
card_subtypes('primitive etchings', []).
card_colors('primitive etchings', ['G']).
card_text('primitive etchings', 'Reveal the first card you draw each turn. Whenever you reveal a creature card this way, draw a card.').
card_mana_cost('primitive etchings', ['2', 'G', 'G']).
card_cmc('primitive etchings', 4).
card_layout('primitive etchings', 'normal').

% Found in: ALL, ME4
card_name('primitive justice', 'Primitive Justice').
card_type('primitive justice', 'Sorcery').
card_types('primitive justice', ['Sorcery']).
card_subtypes('primitive justice', []).
card_colors('primitive justice', ['R']).
card_text('primitive justice', 'As an additional cost to cast Primitive Justice, you may pay {1}{R} and/or {1}{G} any number of times.\nDestroy target artifact. For each additional {1}{R} you paid, destroy another target artifact. For each additional {1}{G} you paid, destroy another target artifact, and you gain 1 life.').
card_mana_cost('primitive justice', ['1', 'R']).
card_cmc('primitive justice', 2).
card_layout('primitive justice', 'normal').

% Found in: LGN
card_name('primoc escapee', 'Primoc Escapee').
card_type('primoc escapee', 'Creature — Bird Beast').
card_types('primoc escapee', ['Creature']).
card_subtypes('primoc escapee', ['Bird', 'Beast']).
card_colors('primoc escapee', ['U']).
card_text('primoc escapee', 'Flying\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('primoc escapee', ['6', 'U']).
card_cmc('primoc escapee', 7).
card_layout('primoc escapee', 'normal').
card_power('primoc escapee', 4).
card_toughness('primoc escapee', 4).

% Found in: M12, M13, pMEI
card_name('primordial hydra', 'Primordial Hydra').
card_type('primordial hydra', 'Creature — Hydra').
card_types('primordial hydra', ['Creature']).
card_subtypes('primordial hydra', ['Hydra']).
card_colors('primordial hydra', ['G']).
card_text('primordial hydra', 'Primordial Hydra enters the battlefield with X +1/+1 counters on it.\nAt the beginning of your upkeep, double the number of +1/+1 counters on Primordial Hydra.\nPrimordial Hydra has trample as long as it has ten or more +1/+1 counters on it.').
card_mana_cost('primordial hydra', ['X', 'G', 'G']).
card_cmc('primordial hydra', 2).
card_layout('primordial hydra', 'normal').
card_power('primordial hydra', 0).
card_toughness('primordial hydra', 0).

% Found in: 5ED, CHR, LEG
card_name('primordial ooze', 'Primordial Ooze').
card_type('primordial ooze', 'Creature — Ooze').
card_types('primordial ooze', ['Creature']).
card_subtypes('primordial ooze', ['Ooze']).
card_colors('primordial ooze', ['R']).
card_text('primordial ooze', 'Primordial Ooze attacks each turn if able.\nAt the beginning of your upkeep, put a +1/+1 counter on Primordial Ooze. Then you may pay {X}, where X is the number of +1/+1 counters on it. If you don\'t, tap Primordial Ooze and it deals X damage to you.').
card_mana_cost('primordial ooze', ['R']).
card_cmc('primordial ooze', 1).
card_layout('primordial ooze', 'normal').
card_power('primordial ooze', 1).
card_toughness('primordial ooze', 1).

% Found in: C14, RAV
card_name('primordial sage', 'Primordial Sage').
card_type('primordial sage', 'Creature — Spirit').
card_types('primordial sage', ['Creature']).
card_subtypes('primordial sage', ['Spirit']).
card_colors('primordial sage', ['G']).
card_text('primordial sage', 'Whenever you cast a creature spell, you may draw a card.').
card_mana_cost('primordial sage', ['4', 'G', 'G']).
card_cmc('primordial sage', 6).
card_layout('primordial sage', 'normal').
card_power('primordial sage', 4).
card_toughness('primordial sage', 5).

% Found in: ALA
card_name('prince of thralls', 'Prince of Thralls').
card_type('prince of thralls', 'Creature — Demon').
card_types('prince of thralls', ['Creature']).
card_subtypes('prince of thralls', ['Demon']).
card_colors('prince of thralls', ['U', 'B', 'R']).
card_text('prince of thralls', 'Whenever a permanent an opponent controls is put into a graveyard, put that card onto the battlefield under your control unless that opponent pays 3 life.').
card_mana_cost('prince of thralls', ['4', 'U', 'B', 'B', 'R']).
card_cmc('prince of thralls', 8).
card_layout('prince of thralls', 'normal').
card_power('prince of thralls', 7).
card_toughness('prince of thralls', 7).

% Found in: LEG, ME3
card_name('princess lucrezia', 'Princess Lucrezia').
card_type('princess lucrezia', 'Legendary Creature — Human Wizard').
card_types('princess lucrezia', ['Creature']).
card_subtypes('princess lucrezia', ['Human', 'Wizard']).
card_supertypes('princess lucrezia', ['Legendary']).
card_colors('princess lucrezia', ['U', 'B']).
card_text('princess lucrezia', '{T}: Add {U} to your mana pool.').
card_mana_cost('princess lucrezia', ['3', 'U', 'U', 'B']).
card_cmc('princess lucrezia', 6).
card_layout('princess lucrezia', 'normal').
card_power('princess lucrezia', 5).
card_toughness('princess lucrezia', 4).

% Found in: BFZ
card_name('prism array', 'Prism Array').
card_type('prism array', 'Enchantment').
card_types('prism array', ['Enchantment']).
card_subtypes('prism array', []).
card_colors('prism array', ['U']).
card_text('prism array', 'Converge — Prism Array enters the battlefield with a crystal counter on it for each color of mana spent to cast it.\nRemove a crystal counter from Prism Array: Tap target creature.\n{W}{U}{B}{R}{G}: Scry 3. (Look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('prism array', ['4', 'U']).
card_cmc('prism array', 5).
card_layout('prism array', 'normal').

% Found in: ORI
card_name('prism ring', 'Prism Ring').
card_type('prism ring', 'Artifact').
card_types('prism ring', ['Artifact']).
card_subtypes('prism ring', []).
card_colors('prism ring', []).
card_text('prism ring', 'As Prism Ring enters the battlefield, choose a color.\nWhenever you cast a spell of the chosen color, you gain 1 life.').
card_mana_cost('prism ring', ['1']).
card_cmc('prism ring', 1).
card_layout('prism ring', 'normal').

% Found in: MIR
card_name('prismatic boon', 'Prismatic Boon').
card_type('prismatic boon', 'Instant').
card_types('prismatic boon', ['Instant']).
card_subtypes('prismatic boon', []).
card_colors('prismatic boon', ['W', 'U']).
card_text('prismatic boon', 'Choose a color. X target creatures gain protection from the chosen color until end of turn.').
card_mana_cost('prismatic boon', ['X', 'W', 'U']).
card_cmc('prismatic boon', 2).
card_layout('prismatic boon', 'normal').

% Found in: MIR
card_name('prismatic circle', 'Prismatic Circle').
card_type('prismatic circle', 'Enchantment').
card_types('prismatic circle', ['Enchantment']).
card_subtypes('prismatic circle', []).
card_colors('prismatic circle', ['W']).
card_text('prismatic circle', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAs Prismatic Circle enters the battlefield, choose a color.\n{1}: The next time a source of your choice of the chosen color would deal damage to you this turn, prevent that damage.').
card_mana_cost('prismatic circle', ['2', 'W']).
card_cmc('prismatic circle', 3).
card_layout('prismatic circle', 'normal').

% Found in: MIR
card_name('prismatic lace', 'Prismatic Lace').
card_type('prismatic lace', 'Instant').
card_types('prismatic lace', ['Instant']).
card_subtypes('prismatic lace', []).
card_colors('prismatic lace', ['U']).
card_text('prismatic lace', 'Target permanent becomes the color or colors of your choice. (This effect lasts indefinitely.)').
card_mana_cost('prismatic lace', ['U']).
card_cmc('prismatic lace', 1).
card_layout('prismatic lace', 'normal').
card_reserved('prismatic lace').

% Found in: TSP
card_name('prismatic lens', 'Prismatic Lens').
card_type('prismatic lens', 'Artifact').
card_types('prismatic lens', ['Artifact']).
card_subtypes('prismatic lens', []).
card_colors('prismatic lens', []).
card_text('prismatic lens', '{T}: Add {1} to your mana pool.\n{1}, {T}: Add one mana of any color to your mana pool.').
card_mana_cost('prismatic lens', ['2']).
card_cmc('prismatic lens', 2).
card_layout('prismatic lens', 'normal').

% Found in: SHM
card_name('prismatic omen', 'Prismatic Omen').
card_type('prismatic omen', 'Enchantment').
card_types('prismatic omen', ['Enchantment']).
card_subtypes('prismatic omen', []).
card_colors('prismatic omen', ['G']).
card_text('prismatic omen', 'Lands you control are every basic land type in addition to their other types.').
card_mana_cost('prismatic omen', ['1', 'G']).
card_cmc('prismatic omen', 2).
card_layout('prismatic omen', 'normal').

% Found in: JUD
card_name('prismatic strands', 'Prismatic Strands').
card_type('prismatic strands', 'Instant').
card_types('prismatic strands', ['Instant']).
card_subtypes('prismatic strands', []).
card_colors('prismatic strands', ['W']).
card_text('prismatic strands', 'Prevent all damage that sources of the color of your choice would deal this turn.\nFlashback—Tap an untapped white creature you control. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('prismatic strands', ['2', 'W']).
card_cmc('prismatic strands', 3).
card_layout('prismatic strands', 'normal').

% Found in: 5ED, ICE
card_name('prismatic ward', 'Prismatic Ward').
card_type('prismatic ward', 'Enchantment — Aura').
card_types('prismatic ward', ['Enchantment']).
card_subtypes('prismatic ward', ['Aura']).
card_colors('prismatic ward', ['W']).
card_text('prismatic ward', 'Enchant creature\nAs Prismatic Ward enters the battlefield, choose a color.\nPrevent all damage that would be dealt to enchanted creature by sources of the chosen color.').
card_mana_cost('prismatic ward', ['1', 'W']).
card_cmc('prismatic ward', 2).
card_layout('prismatic ward', 'normal').

% Found in: UGL
card_name('prismatic wardrobe', 'Prismatic Wardrobe').
card_type('prismatic wardrobe', 'Sorcery').
card_types('prismatic wardrobe', ['Sorcery']).
card_subtypes('prismatic wardrobe', []).
card_colors('prismatic wardrobe', ['W']).
card_text('prismatic wardrobe', 'Destroy target card that does not share a color with clothing worn by its controller. You cannot choose an artifact or land card.').
card_mana_cost('prismatic wardrobe', ['W']).
card_cmc('prismatic wardrobe', 1).
card_layout('prismatic wardrobe', 'normal').

% Found in: SHM
card_name('prismwake merrow', 'Prismwake Merrow').
card_type('prismwake merrow', 'Creature — Merfolk Wizard').
card_types('prismwake merrow', ['Creature']).
card_subtypes('prismwake merrow', ['Merfolk', 'Wizard']).
card_colors('prismwake merrow', ['U']).
card_text('prismwake merrow', 'Flash\nWhen Prismwake Merrow enters the battlefield, target permanent becomes the color or colors of your choice until end of turn.').
card_mana_cost('prismwake merrow', ['2', 'U']).
card_cmc('prismwake merrow', 3).
card_layout('prismwake merrow', 'normal').
card_power('prismwake merrow', 2).
card_toughness('prismwake merrow', 1).

% Found in: INV
card_name('prison barricade', 'Prison Barricade').
card_type('prison barricade', 'Creature — Wall').
card_types('prison barricade', ['Creature']).
card_subtypes('prison barricade', ['Wall']).
card_colors('prison barricade', ['W']).
card_text('prison barricade', 'Defender (This creature can\'t attack.)\nKicker {1}{W} (You may pay an additional {1}{W} as you cast this spell.)\nIf Prison Barricade was kicked, it enters the battlefield with a +1/+1 counter on it and with \"Prison Barricade can attack as though it didn\'t have defender.\"').
card_mana_cost('prison barricade', ['1', 'W']).
card_cmc('prison barricade', 2).
card_layout('prison barricade', 'normal').
card_power('prison barricade', 1).
card_toughness('prison barricade', 3).

% Found in: CMD, HOP, SHM
card_name('prison term', 'Prison Term').
card_type('prison term', 'Enchantment — Aura').
card_types('prison term', ['Enchantment']).
card_subtypes('prison term', ['Aura']).
card_colors('prison term', ['W']).
card_text('prison term', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.\nWhenever a creature enters the battlefield under an opponent\'s control, you may attach Prison Term to that creature.').
card_mana_cost('prison term', ['1', 'W', 'W']).
card_cmc('prison term', 3).
card_layout('prison term', 'normal').

% Found in: CNS, DST
card_name('pristine angel', 'Pristine Angel').
card_type('pristine angel', 'Creature — Angel').
card_types('pristine angel', ['Creature']).
card_subtypes('pristine angel', ['Angel']).
card_colors('pristine angel', ['W']).
card_text('pristine angel', 'Flying\nAs long as Pristine Angel is untapped, it has protection from artifacts and from all colors.\nWhenever you cast a spell, you may untap Pristine Angel.').
card_mana_cost('pristine angel', ['4', 'W', 'W']).
card_cmc('pristine angel', 6).
card_layout('pristine angel', 'normal').
card_power('pristine angel', 4).
card_toughness('pristine angel', 4).

% Found in: DTK, pMEI
card_name('pristine skywise', 'Pristine Skywise').
card_type('pristine skywise', 'Creature — Dragon').
card_types('pristine skywise', ['Creature']).
card_subtypes('pristine skywise', ['Dragon']).
card_colors('pristine skywise', ['W', 'U']).
card_text('pristine skywise', 'Flying\nWhenever you cast a noncreature spell, untap Pristine Skywise. It gains protection from the color of your choice until end of turn.').
card_mana_cost('pristine skywise', ['4', 'W', 'U']).
card_cmc('pristine skywise', 6).
card_layout('pristine skywise', 'normal').
card_power('pristine skywise', 6).
card_toughness('pristine skywise', 4).

% Found in: C13, C14, NPH, pMGD
card_name('pristine talisman', 'Pristine Talisman').
card_type('pristine talisman', 'Artifact').
card_types('pristine talisman', ['Artifact']).
card_subtypes('pristine talisman', []).
card_colors('pristine talisman', []).
card_text('pristine talisman', '{T}: Add {1} to your mana pool. You gain 1 life.').
card_mana_cost('pristine talisman', ['3']).
card_cmc('pristine talisman', 3).
card_layout('pristine talisman', 'normal').

% Found in: UDS
card_name('private research', 'Private Research').
card_type('private research', 'Enchantment — Aura').
card_types('private research', ['Enchantment']).
card_subtypes('private research', ['Aura']).
card_colors('private research', ['U']).
card_text('private research', 'Enchant creature\nAt the beginning of your upkeep, you may put a page counter on Private Research.\nWhen enchanted creature dies, draw a card for each page counter on Private Research.').
card_mana_cost('private research', ['U']).
card_cmc('private research', 1).
card_layout('private research', 'normal').

% Found in: RAV
card_name('privileged position', 'Privileged Position').
card_type('privileged position', 'Enchantment').
card_types('privileged position', ['Enchantment']).
card_subtypes('privileged position', []).
card_colors('privileged position', ['W', 'G']).
card_text('privileged position', '({G/W} can be paid with either {G} or {W}.)\nOther permanents you control have hexproof. (They can\'t be the targets of spells or abilities your opponents control.)').
card_mana_cost('privileged position', ['2', 'G/W', 'G/W', 'G/W']).
card_cmc('privileged position', 5).
card_layout('privileged position', 'normal').

% Found in: M13
card_name('prized elephant', 'Prized Elephant').
card_type('prized elephant', 'Creature — Elephant').
card_types('prized elephant', ['Creature']).
card_subtypes('prized elephant', ['Elephant']).
card_colors('prized elephant', ['W']).
card_text('prized elephant', 'Prized Elephant gets +1/+1 as long as you control a Forest.\n{G}: Prized Elephant gains trample until end of turn. (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('prized elephant', ['3', 'W']).
card_cmc('prized elephant', 4).
card_layout('prized elephant', 'normal').
card_power('prized elephant', 3).
card_toughness('prized elephant', 3).

% Found in: M10, M11, ORI
card_name('prized unicorn', 'Prized Unicorn').
card_type('prized unicorn', 'Creature — Unicorn').
card_types('prized unicorn', ['Creature']).
card_subtypes('prized unicorn', ['Unicorn']).
card_colors('prized unicorn', ['G']).
card_text('prized unicorn', 'All creatures able to block Prized Unicorn do so.').
card_mana_cost('prized unicorn', ['3', 'G']).
card_cmc('prized unicorn', 4).
card_layout('prized unicorn', 'normal').
card_power('prized unicorn', 2).
card_toughness('prized unicorn', 2).

% Found in: INV
card_name('probe', 'Probe').
card_type('probe', 'Sorcery').
card_types('probe', ['Sorcery']).
card_subtypes('probe', []).
card_colors('probe', ['U']).
card_text('probe', 'Kicker {1}{B} (You may pay an additional {1}{B} as you cast this spell.)\nDraw three cards, then discard two cards.\nIf Probe was kicked, target player discards two cards.').
card_mana_cost('probe', ['2', 'U']).
card_cmc('probe', 3).
card_layout('probe', 'normal').

% Found in: BFZ
card_name('processor assault', 'Processor Assault').
card_type('processor assault', 'Sorcery').
card_types('processor assault', ['Sorcery']).
card_subtypes('processor assault', []).
card_colors('processor assault', []).
card_text('processor assault', 'Devoid (This card has no color.)\nAs an additional cost to cast Processor Assault, put a card an opponent owns from exile into that player\'s graveyard.\nProcessor Assault deals 5 damage to target creature.').
card_mana_cost('processor assault', ['1', 'R']).
card_cmc('processor assault', 2).
card_layout('processor assault', 'normal').

% Found in: DIS
card_name('proclamation of rebirth', 'Proclamation of Rebirth').
card_type('proclamation of rebirth', 'Sorcery').
card_types('proclamation of rebirth', ['Sorcery']).
card_subtypes('proclamation of rebirth', []).
card_colors('proclamation of rebirth', ['W']).
card_text('proclamation of rebirth', 'Return up to three target creature cards with converted mana cost 1 or less from your graveyard to the battlefield.\nForecast — {5}{W}, Reveal Proclamation of Rebirth from your hand: Return target creature card with converted mana cost 1 or less from your graveyard to the battlefield. (Activate this ability only during your upkeep and only once each turn.)').
card_mana_cost('proclamation of rebirth', ['2', 'W']).
card_cmc('proclamation of rebirth', 3).
card_layout('proclamation of rebirth', 'normal').

% Found in: 10E, DPA, M10, M11, PLC
card_name('prodigal pyromancer', 'Prodigal Pyromancer').
card_type('prodigal pyromancer', 'Creature — Human Wizard').
card_types('prodigal pyromancer', ['Creature']).
card_subtypes('prodigal pyromancer', ['Human', 'Wizard']).
card_colors('prodigal pyromancer', ['R']).
card_text('prodigal pyromancer', '{T}: Prodigal Pyromancer deals 1 damage to target creature or player.').
card_mana_cost('prodigal pyromancer', ['2', 'R']).
card_cmc('prodigal pyromancer', 3).
card_layout('prodigal pyromancer', 'normal').
card_power('prodigal pyromancer', 1).
card_toughness('prodigal pyromancer', 1).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, BRB, CED, CEI, LEA, LEB, ME4, S00, TSB, pFNM
card_name('prodigal sorcerer', 'Prodigal Sorcerer').
card_type('prodigal sorcerer', 'Creature — Human Wizard').
card_types('prodigal sorcerer', ['Creature']).
card_subtypes('prodigal sorcerer', ['Human', 'Wizard']).
card_colors('prodigal sorcerer', ['U']).
card_text('prodigal sorcerer', '{T}: Prodigal Sorcerer deals 1 damage to target creature or player.').
card_mana_cost('prodigal sorcerer', ['2', 'U']).
card_cmc('prodigal sorcerer', 3).
card_layout('prodigal sorcerer', 'normal').
card_power('prodigal sorcerer', 1).
card_toughness('prodigal sorcerer', 1).

% Found in: VAN
card_name('prodigal sorcerer avatar', 'Prodigal Sorcerer Avatar').
card_type('prodigal sorcerer avatar', 'Vanguard').
card_types('prodigal sorcerer avatar', ['Vanguard']).
card_subtypes('prodigal sorcerer avatar', []).
card_colors('prodigal sorcerer avatar', []).
card_text('prodigal sorcerer avatar', 'At the beginning of your upkeep, look at the top card of your library. You may put that card into your graveyard.').
card_layout('prodigal sorcerer avatar', 'vanguard').

% Found in: C14, DDH, HOP, LRW, MM2
card_name('profane command', 'Profane Command').
card_type('profane command', 'Sorcery').
card_types('profane command', ['Sorcery']).
card_subtypes('profane command', []).
card_colors('profane command', ['B']).
card_text('profane command', 'Choose two —\n• Target player loses X life. \n• Return target creature card with converted mana cost X or less from your graveyard to the battlefield.\n• Target creature gets -X/-X until end of turn.\n• Up to X target creatures gain fear until end of turn. (They can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('profane command', ['X', 'B', 'B']).
card_cmc('profane command', 2).
card_layout('profane command', 'normal').

% Found in: M15
card_name('profane memento', 'Profane Memento').
card_type('profane memento', 'Artifact').
card_types('profane memento', ['Artifact']).
card_subtypes('profane memento', []).
card_colors('profane memento', []).
card_text('profane memento', 'Whenever a creature card is put into an opponent\'s graveyard from anywhere, you gain 1 life.').
card_mana_cost('profane memento', ['1']).
card_cmc('profane memento', 1).
card_layout('profane memento', 'normal').

% Found in: ONS
card_name('profane prayers', 'Profane Prayers').
card_type('profane prayers', 'Sorcery').
card_types('profane prayers', ['Sorcery']).
card_subtypes('profane prayers', []).
card_colors('profane prayers', ['B']).
card_text('profane prayers', 'Profane Prayers deals X damage to target creature or player and you gain X life, where X is the number of Clerics on the battlefield.').
card_mana_cost('profane prayers', ['2', 'B', 'B']).
card_cmc('profane prayers', 4).
card_layout('profane prayers', 'normal').

% Found in: DTK
card_name('profaner of the dead', 'Profaner of the Dead').
card_type('profaner of the dead', 'Creature — Naga Wizard').
card_types('profaner of the dead', ['Creature']).
card_subtypes('profaner of the dead', ['Naga', 'Wizard']).
card_colors('profaner of the dead', ['U']).
card_text('profaner of the dead', 'Exploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Profaner of the Dead exploits a creature, return to their owners\' hands all creatures your opponents control with toughness less than the exploited creature\'s toughness.').
card_mana_cost('profaner of the dead', ['3', 'U']).
card_cmc('profaner of the dead', 4).
card_layout('profaner of the dead', 'normal').
card_power('profaner of the dead', 3).
card_toughness('profaner of the dead', 3).

% Found in: DGM
card_name('profit', 'Profit').
card_type('profit', 'Instant').
card_types('profit', ['Instant']).
card_subtypes('profit', []).
card_colors('profit', ['W']).
card_text('profit', 'Creatures you control get +1/+1 until end of turn.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('profit', ['1', 'W']).
card_cmc('profit', 2).
card_layout('profit', 'split').
card_sides('profit', 'loss').

% Found in: DTK
card_name('profound journey', 'Profound Journey').
card_type('profound journey', 'Sorcery').
card_types('profound journey', ['Sorcery']).
card_subtypes('profound journey', []).
card_colors('profound journey', ['W']).
card_text('profound journey', 'Return target permanent card from your graveyard to the battlefield.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('profound journey', ['5', 'W', 'W']).
card_cmc('profound journey', 7).
card_layout('profound journey', 'normal').

% Found in: DGM
card_name('progenitor mimic', 'Progenitor Mimic').
card_type('progenitor mimic', 'Creature — Shapeshifter').
card_types('progenitor mimic', ['Creature']).
card_subtypes('progenitor mimic', ['Shapeshifter']).
card_colors('progenitor mimic', ['U', 'G']).
card_text('progenitor mimic', 'You may have Progenitor Mimic enter the battlefield as a copy of any creature on the battlefield except it gains \"At the beginning of your upkeep, if this creature isn\'t a token, put a token onto the battlefield that\'s a copy of this creature.\"').
card_mana_cost('progenitor mimic', ['4', 'G', 'U']).
card_cmc('progenitor mimic', 6).
card_layout('progenitor mimic', 'normal').
card_power('progenitor mimic', 0).
card_toughness('progenitor mimic', 0).

% Found in: CON, MMA, V11
card_name('progenitus', 'Progenitus').
card_type('progenitus', 'Legendary Creature — Hydra Avatar').
card_types('progenitus', ['Creature']).
card_subtypes('progenitus', ['Hydra', 'Avatar']).
card_supertypes('progenitus', ['Legendary']).
card_colors('progenitus', ['W', 'U', 'B', 'R', 'G']).
card_text('progenitus', 'Protection from everything\nIf Progenitus would be put into a graveyard from anywhere, reveal Progenitus and shuffle it into its owner\'s library instead.').
card_mana_cost('progenitus', ['W', 'W', 'U', 'U', 'B', 'B', 'R', 'R', 'G', 'G']).
card_cmc('progenitus', 10).
card_layout('progenitus', 'normal').
card_power('progenitus', 10).
card_toughness('progenitus', 10).

% Found in: CPK, THS
card_name('prognostic sphinx', 'Prognostic Sphinx').
card_type('prognostic sphinx', 'Creature — Sphinx').
card_types('prognostic sphinx', ['Creature']).
card_subtypes('prognostic sphinx', ['Sphinx']).
card_colors('prognostic sphinx', ['U']).
card_text('prognostic sphinx', 'Flying\nDiscard a card: Prognostic Sphinx gains hexproof until end of turn. Tap it.\nWhenever Prognostic Sphinx attacks, scry 3. (Look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('prognostic sphinx', ['3', 'U', 'U']).
card_cmc('prognostic sphinx', 5).
card_layout('prognostic sphinx', 'normal').
card_power('prognostic sphinx', 3).
card_toughness('prognostic sphinx', 5).

% Found in: DDM, INV
card_name('prohibit', 'Prohibit').
card_type('prohibit', 'Instant').
card_types('prohibit', ['Instant']).
card_subtypes('prohibit', []).
card_colors('prohibit', ['U']).
card_text('prohibit', 'Kicker {2} (You may pay an additional {2} as you cast this spell.)\nCounter target spell if its converted mana cost is 2 or less. If Prohibit was kicked, counter that spell if its converted mana cost is 4 or less instead.').
card_mana_cost('prohibit', ['1', 'U']).
card_cmc('prohibit', 2).
card_layout('prohibit', 'normal').

% Found in: SOK
card_name('promise of bunrei', 'Promise of Bunrei').
card_type('promise of bunrei', 'Enchantment').
card_types('promise of bunrei', ['Enchantment']).
card_subtypes('promise of bunrei', []).
card_colors('promise of bunrei', ['W']).
card_text('promise of bunrei', 'When a creature you control dies, sacrifice Promise of Bunrei. If you do, put four 1/1 colorless Spirit creature tokens onto the battlefield.').
card_mana_cost('promise of bunrei', ['2', 'W']).
card_cmc('promise of bunrei', 3).
card_layout('promise of bunrei', 'normal').

% Found in: C14, DD3_DVD, DDC, MRD
card_name('promise of power', 'Promise of Power').
card_type('promise of power', 'Sorcery').
card_types('promise of power', ['Sorcery']).
card_subtypes('promise of power', []).
card_colors('promise of power', ['B']).
card_text('promise of power', 'Choose one —\n• You draw five cards and you lose 5 life.\n• Put an X/X black Demon creature token with flying onto the battlefield, where X is the number of cards in your hand.\nEntwine {4} (Choose both if you pay the entwine cost.)').
card_mana_cost('promise of power', ['2', 'B', 'B', 'B']).
card_cmc('promise of power', 5).
card_layout('promise of power', 'normal').

% Found in: SOK
card_name('promised kannushi', 'Promised Kannushi').
card_type('promised kannushi', 'Creature — Human Druid').
card_types('promised kannushi', ['Creature']).
card_subtypes('promised kannushi', ['Human', 'Druid']).
card_colors('promised kannushi', ['G']).
card_text('promised kannushi', 'Soulshift 7 (When this creature dies, you may return target Spirit card with converted mana cost 7 or less from your graveyard to your hand.)').
card_mana_cost('promised kannushi', ['G']).
card_cmc('promised kannushi', 1).
card_layout('promised kannushi', 'normal').
card_power('promised kannushi', 1).
card_toughness('promised kannushi', 1).

% Found in: C13, CMD, TMP
card_name('propaganda', 'Propaganda').
card_type('propaganda', 'Enchantment').
card_types('propaganda', ['Enchantment']).
card_subtypes('propaganda', []).
card_colors('propaganda', ['U']).
card_text('propaganda', 'Creatures can\'t attack you unless their controller pays {2} for each creature he or she controls that\'s attacking you.').
card_mana_cost('propaganda', ['2', 'U']).
card_cmc('propaganda', 3).
card_layout('propaganda', 'normal').

% Found in: DIS
card_name('proper burial', 'Proper Burial').
card_type('proper burial', 'Enchantment').
card_types('proper burial', ['Enchantment']).
card_subtypes('proper burial', []).
card_colors('proper burial', ['W']).
card_text('proper burial', 'Whenever a creature you control dies, you gain life equal to that creature\'s toughness.').
card_mana_cost('proper burial', ['3', 'W']).
card_cmc('proper burial', 4).
card_layout('proper burial', 'normal').

% Found in: HML
card_name('prophecy', 'Prophecy').
card_type('prophecy', 'Sorcery').
card_types('prophecy', ['Sorcery']).
card_subtypes('prophecy', []).
card_colors('prophecy', ['W']).
card_text('prophecy', 'Reveal the top card of target opponent\'s library. If it\'s a land, you gain 1 life. Then that player shuffles his or her library.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('prophecy', ['W']).
card_cmc('prophecy', 1).
card_layout('prophecy', 'normal').

% Found in: CPK, THS
card_name('prophet of kruphix', 'Prophet of Kruphix').
card_type('prophet of kruphix', 'Creature — Human Wizard').
card_types('prophet of kruphix', ['Creature']).
card_subtypes('prophet of kruphix', ['Human', 'Wizard']).
card_colors('prophet of kruphix', ['U', 'G']).
card_text('prophet of kruphix', 'Untap all creatures and lands you control during each other player\'s untap step.\nYou may cast creature cards as though they had flash.').
card_mana_cost('prophet of kruphix', ['3', 'G', 'U']).
card_cmc('prophet of kruphix', 5).
card_layout('prophet of kruphix', 'normal').
card_power('prophet of kruphix', 2).
card_toughness('prophet of kruphix', 3).

% Found in: APC, CMD, DDJ, VMA
card_name('prophetic bolt', 'Prophetic Bolt').
card_type('prophetic bolt', 'Instant').
card_types('prophetic bolt', ['Instant']).
card_subtypes('prophetic bolt', []).
card_colors('prophetic bolt', ['U', 'R']).
card_text('prophetic bolt', 'Prophetic Bolt deals 4 damage to target creature or player. Look at the top four cards of your library. Put one of those cards into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('prophetic bolt', ['3', 'U', 'R']).
card_cmc('prophetic bolt', 5).
card_layout('prophetic bolt', 'normal').

% Found in: JOU
card_name('prophetic flamespeaker', 'Prophetic Flamespeaker').
card_type('prophetic flamespeaker', 'Creature — Human Shaman').
card_types('prophetic flamespeaker', ['Creature']).
card_subtypes('prophetic flamespeaker', ['Human', 'Shaman']).
card_colors('prophetic flamespeaker', ['R']).
card_text('prophetic flamespeaker', 'Double strike, trample\nWhenever Prophetic Flamespeaker deals combat damage to a player, exile the top card of your library. You may play it this turn.').
card_mana_cost('prophetic flamespeaker', ['1', 'R', 'R']).
card_cmc('prophetic flamespeaker', 3).
card_layout('prophetic flamespeaker', 'normal').
card_power('prophetic flamespeaker', 1).
card_toughness('prophetic flamespeaker', 3).

% Found in: CMD, GTC, ROE
card_name('prophetic prism', 'Prophetic Prism').
card_type('prophetic prism', 'Artifact').
card_types('prophetic prism', ['Artifact']).
card_subtypes('prophetic prism', []).
card_colors('prophetic prism', []).
card_text('prophetic prism', 'When Prophetic Prism enters the battlefield, draw a card.\n{1}, {T}: Add one mana of any color to your mana pool.').
card_mana_cost('prophetic prism', ['2']).
card_cmc('prophetic prism', 2).
card_layout('prophetic prism', 'normal').

% Found in: pCEL
card_name('proposal', 'Proposal').
card_type('proposal', 'Sorcery').
card_types('proposal', ['Sorcery']).
card_subtypes('proposal', []).
card_colors('proposal', ['W']).
card_text('proposal', 'Allows Richard to propose to Lily. If the proposal is accepted both players win; mix the cards in play, both libraries, and both graveyards as a shared deck.').
card_mana_cost('proposal', ['W', 'W', 'W', 'W']).
card_cmc('proposal', 4).
card_layout('proposal', 'normal').

% Found in: 6ED, C13, POR, VIS
card_name('prosperity', 'Prosperity').
card_type('prosperity', 'Sorcery').
card_types('prosperity', ['Sorcery']).
card_subtypes('prosperity', []).
card_colors('prosperity', ['U']).
card_text('prosperity', 'Each player draws X cards.').
card_mana_cost('prosperity', ['X', 'U']).
card_cmc('prosperity', 1).
card_layout('prosperity', 'normal').

% Found in: C13
card_name('prossh, skyraider of kher', 'Prossh, Skyraider of Kher').
card_type('prossh, skyraider of kher', 'Legendary Creature — Dragon').
card_types('prossh, skyraider of kher', ['Creature']).
card_subtypes('prossh, skyraider of kher', ['Dragon']).
card_supertypes('prossh, skyraider of kher', ['Legendary']).
card_colors('prossh, skyraider of kher', ['B', 'R', 'G']).
card_text('prossh, skyraider of kher', 'Flying\nWhen you cast Prossh, Skyraider of Kher, put X 0/1 red Kobold creature tokens named Kobolds of Kher Keep onto the battlefield, where X is the amount of mana spent to cast Prossh.\nSacrifice another creature: Prossh gets +1/+0 until end of turn.').
card_mana_cost('prossh, skyraider of kher', ['3', 'B', 'R', 'G']).
card_cmc('prossh, skyraider of kher', 6).
card_layout('prossh, skyraider of kher', 'normal').
card_power('prossh, skyraider of kher', 5).
card_toughness('prossh, skyraider of kher', 5).

% Found in: DIS
card_name('protean hulk', 'Protean Hulk').
card_type('protean hulk', 'Creature — Beast').
card_types('protean hulk', ['Creature']).
card_subtypes('protean hulk', ['Beast']).
card_colors('protean hulk', ['G']).
card_text('protean hulk', 'When Protean Hulk dies, search your library for any number of creature cards with total converted mana cost 6 or less and put them onto the battlefield. Then shuffle your library.').
card_mana_cost('protean hulk', ['5', 'G', 'G']).
card_cmc('protean hulk', 7).
card_layout('protean hulk', 'normal').
card_power('protean hulk', 6).
card_toughness('protean hulk', 6).

% Found in: M10, M11
card_name('protean hydra', 'Protean Hydra').
card_type('protean hydra', 'Creature — Hydra').
card_types('protean hydra', ['Creature']).
card_subtypes('protean hydra', ['Hydra']).
card_colors('protean hydra', ['G']).
card_text('protean hydra', 'Protean Hydra enters the battlefield with X +1/+1 counters on it.\nIf damage would be dealt to Protean Hydra, prevent that damage and remove that many +1/+1 counters from it.\nWhenever a +1/+1 counter is removed from Protean Hydra, put two +1/+1 counters on it at the beginning of the next end step.').
card_mana_cost('protean hydra', ['X', 'G']).
card_cmc('protean hydra', 1).
card_layout('protean hydra', 'normal').
card_power('protean hydra', 0).
card_toughness('protean hydra', 0).

% Found in: DGM
card_name('protect', 'Protect').
card_type('protect', 'Instant').
card_types('protect', ['Instant']).
card_subtypes('protect', []).
card_colors('protect', ['W']).
card_text('protect', 'Target creature gets +2/+4 until end of turn.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('protect', ['2', 'W']).
card_cmc('protect', 3).
card_layout('protect', 'split').
card_sides('protect', 'serve').

% Found in: LRW
card_name('protective bubble', 'Protective Bubble').
card_type('protective bubble', 'Enchantment — Aura').
card_types('protective bubble', ['Enchantment']).
card_subtypes('protective bubble', ['Aura']).
card_colors('protective bubble', ['U']).
card_text('protective bubble', 'Enchant creature\nEnchanted creature can\'t be blocked and has shroud. (It can\'t be the target of spells or abilities.)').
card_mana_cost('protective bubble', ['3', 'U']).
card_cmc('protective bubble', 4).
card_layout('protective bubble', 'normal').

% Found in: INV
card_name('protective sphere', 'Protective Sphere').
card_type('protective sphere', 'Enchantment').
card_types('protective sphere', ['Enchantment']).
card_subtypes('protective sphere', []).
card_colors('protective sphere', ['W']).
card_text('protective sphere', '{1}, Pay 1 life: Prevent all damage that would be dealt to you this turn by a source of your choice that shares a color with the mana spent on this activation cost. (Colorless mana prevents no damage.)').
card_mana_cost('protective sphere', ['2', 'W']).
card_cmc('protective sphere', 3).
card_layout('protective sphere', 'normal').

% Found in: SCG
card_name('proteus machine', 'Proteus Machine').
card_type('proteus machine', 'Artifact Creature — Shapeshifter').
card_types('proteus machine', ['Artifact', 'Creature']).
card_subtypes('proteus machine', ['Shapeshifter']).
card_colors('proteus machine', []).
card_text('proteus machine', 'Morph {0} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Proteus Machine is turned face up, it becomes the creature type of your choice. (This effect lasts indefinitely.)').
card_mana_cost('proteus machine', ['3']).
card_cmc('proteus machine', 3).
card_layout('proteus machine', 'normal').
card_power('proteus machine', 2).
card_toughness('proteus machine', 2).

% Found in: MRD
card_name('proteus staff', 'Proteus Staff').
card_type('proteus staff', 'Artifact').
card_types('proteus staff', ['Artifact']).
card_subtypes('proteus staff', []).
card_colors('proteus staff', []).
card_text('proteus staff', '{2}{U}, {T}: Put target creature on the bottom of its owner\'s library. That creature\'s controller reveals cards from the top of his or her library until he or she reveals a creature card. The player puts that card onto the battlefield and the rest on the bottom of his or her library in any order. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('proteus staff', ['3']).
card_cmc('proteus staff', 3).
card_layout('proteus staff', 'normal').

% Found in: ALA
card_name('protomatter powder', 'Protomatter Powder').
card_type('protomatter powder', 'Artifact').
card_types('protomatter powder', ['Artifact']).
card_subtypes('protomatter powder', []).
card_colors('protomatter powder', ['U']).
card_text('protomatter powder', '{4}{W}, {T}, Sacrifice Protomatter Powder: Return target artifact card from your graveyard to the battlefield.').
card_mana_cost('protomatter powder', ['2', 'U']).
card_cmc('protomatter powder', 3).
card_layout('protomatter powder', 'normal').

% Found in: SOM
card_name('prototype portal', 'Prototype Portal').
card_type('prototype portal', 'Artifact').
card_types('prototype portal', ['Artifact']).
card_subtypes('prototype portal', []).
card_colors('prototype portal', []).
card_text('prototype portal', 'Imprint — When Prototype Portal enters the battlefield, you may exile an artifact card from your hand.\n{X}, {T}: Put a token that\'s a copy of the exiled card onto the battlefield. X is the converted mana cost of that card.').
card_mana_cost('prototype portal', ['4']).
card_cmc('prototype portal', 4).
card_layout('prototype portal', 'normal').

% Found in: CNS, STH, TPR, VMA
card_name('provoke', 'Provoke').
card_type('provoke', 'Instant').
card_types('provoke', ['Instant']).
card_subtypes('provoke', []).
card_colors('provoke', ['G']).
card_text('provoke', 'Untap target creature you don\'t control. That creature blocks this turn if able.\nDraw a card.').
card_mana_cost('provoke', ['1', 'G']).
card_cmc('provoke', 2).
card_layout('provoke', 'normal').

% Found in: LRW
card_name('prowess of the fair', 'Prowess of the Fair').
card_type('prowess of the fair', 'Tribal Enchantment — Elf').
card_types('prowess of the fair', ['Tribal', 'Enchantment']).
card_subtypes('prowess of the fair', ['Elf']).
card_colors('prowess of the fair', ['B']).
card_text('prowess of the fair', 'Whenever another nontoken Elf is put into your graveyard from the battlefield, you may put a 1/1 green Elf Warrior creature token onto the battlefield.').
card_mana_cost('prowess of the fair', ['1', 'B']).
card_cmc('prowess of the fair', 2).
card_layout('prowess of the fair', 'normal').

% Found in: THS
card_name('prowler\'s helm', 'Prowler\'s Helm').
card_type('prowler\'s helm', 'Artifact — Equipment').
card_types('prowler\'s helm', ['Artifact']).
card_subtypes('prowler\'s helm', ['Equipment']).
card_colors('prowler\'s helm', []).
card_text('prowler\'s helm', 'Equipped creature can\'t be blocked except by Walls.\nEquip {2}').
card_mana_cost('prowler\'s helm', ['2']).
card_cmc('prowler\'s helm', 2).
card_layout('prowler\'s helm', 'normal').

% Found in: ME4, PO2
card_name('prowling nightstalker', 'Prowling Nightstalker').
card_type('prowling nightstalker', 'Creature — Nightstalker').
card_types('prowling nightstalker', ['Creature']).
card_subtypes('prowling nightstalker', ['Nightstalker']).
card_colors('prowling nightstalker', ['B']).
card_text('prowling nightstalker', 'Prowling Nightstalker can\'t be blocked except by black creatures.').
card_mana_cost('prowling nightstalker', ['3', 'B']).
card_cmc('prowling nightstalker', 4).
card_layout('prowling nightstalker', 'normal').
card_power('prowling nightstalker', 2).
card_toughness('prowling nightstalker', 2).

% Found in: ONS
card_name('prowling pangolin', 'Prowling Pangolin').
card_type('prowling pangolin', 'Creature — Beast').
card_types('prowling pangolin', ['Creature']).
card_subtypes('prowling pangolin', ['Beast']).
card_colors('prowling pangolin', ['B']).
card_text('prowling pangolin', 'When Prowling Pangolin enters the battlefield, any player may sacrifice two creatures. If a player does, sacrifice Prowling Pangolin.').
card_mana_cost('prowling pangolin', ['3', 'B', 'B']).
card_cmc('prowling pangolin', 5).
card_layout('prowling pangolin', 'normal').
card_power('prowling pangolin', 6).
card_toughness('prowling pangolin', 5).

% Found in: 2ED, CED, CEI, LEA, LEB, TSB, pMPR
card_name('psionic blast', 'Psionic Blast').
card_type('psionic blast', 'Instant').
card_types('psionic blast', ['Instant']).
card_subtypes('psionic blast', []).
card_colors('psionic blast', ['U']).
card_text('psionic blast', 'Psionic Blast deals 4 damage to target creature or player and 2 damage to you.').
card_mana_cost('psionic blast', ['2', 'U']).
card_cmc('psionic blast', 3).
card_layout('psionic blast', 'normal').

% Found in: 4ED, LEG
card_name('psionic entity', 'Psionic Entity').
card_type('psionic entity', 'Creature — Illusion').
card_types('psionic entity', ['Creature']).
card_subtypes('psionic entity', ['Illusion']).
card_colors('psionic entity', ['U']).
card_text('psionic entity', '{T}: Psionic Entity deals 2 damage to target creature or player and 3 damage to itself.').
card_mana_cost('psionic entity', ['4', 'U']).
card_cmc('psionic entity', 5).
card_layout('psionic entity', 'normal').
card_power('psionic entity', 2).
card_toughness('psionic entity', 2).

% Found in: ODY
card_name('psionic gift', 'Psionic Gift').
card_type('psionic gift', 'Enchantment — Aura').
card_types('psionic gift', ['Enchantment']).
card_subtypes('psionic gift', ['Aura']).
card_colors('psionic gift', ['U']).
card_text('psionic gift', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals 1 damage to target creature or player.\"').
card_mana_cost('psionic gift', ['1', 'U']).
card_cmc('psionic gift', 2).
card_layout('psionic gift', 'normal').

% Found in: TSP
card_name('psionic sliver', 'Psionic Sliver').
card_type('psionic sliver', 'Creature — Sliver').
card_types('psionic sliver', ['Creature']).
card_subtypes('psionic sliver', ['Sliver']).
card_colors('psionic sliver', ['U']).
card_text('psionic sliver', 'All Sliver creatures have \"{T}: This creature deals 2 damage to target creature or player and 3 damage to itself.\"').
card_mana_cost('psionic sliver', ['4', 'U']).
card_cmc('psionic sliver', 5).
card_layout('psionic sliver', 'normal').
card_power('psionic sliver', 2).
card_toughness('psionic sliver', 2).

% Found in: ODY, VMA, pMPR
card_name('psychatog', 'Psychatog').
card_type('psychatog', 'Creature — Atog').
card_types('psychatog', ['Creature']).
card_subtypes('psychatog', ['Atog']).
card_colors('psychatog', ['U', 'B']).
card_text('psychatog', 'Discard a card: Psychatog gets +1/+1 until end of turn.\nExile two cards from your graveyard: Psychatog gets +1/+1 until end of turn.').
card_mana_cost('psychatog', ['1', 'U', 'B']).
card_cmc('psychatog', 3).
card_layout('psychatog', 'normal').
card_power('psychatog', 1).
card_toughness('psychatog', 2).

% Found in: DRK
card_name('psychic allergy', 'Psychic Allergy').
card_type('psychic allergy', 'Enchantment').
card_types('psychic allergy', ['Enchantment']).
card_subtypes('psychic allergy', []).
card_colors('psychic allergy', ['U']).
card_text('psychic allergy', 'As Psychic Allergy enters the battlefield, choose a color.\nAt the beginning of each opponent\'s upkeep, Psychic Allergy deals X damage to that player, where X is the number of nontoken permanents of the chosen color he or she controls.\nAt the beginning of your upkeep, destroy Psychic Allergy unless you sacrifice two Islands.').
card_mana_cost('psychic allergy', ['3', 'U', 'U']).
card_cmc('psychic allergy', 5).
card_layout('psychic allergy', 'normal').
card_reserved('psychic allergy').

% Found in: NPH
card_name('psychic barrier', 'Psychic Barrier').
card_type('psychic barrier', 'Instant').
card_types('psychic barrier', ['Instant']).
card_subtypes('psychic barrier', []).
card_colors('psychic barrier', ['U']).
card_text('psychic barrier', 'Counter target creature spell. Its controller loses 1 life.').
card_mana_cost('psychic barrier', ['U', 'U']).
card_cmc('psychic barrier', 2).
card_layout('psychic barrier', 'normal').

% Found in: INV
card_name('psychic battle', 'Psychic Battle').
card_type('psychic battle', 'Enchantment').
card_types('psychic battle', ['Enchantment']).
card_subtypes('psychic battle', []).
card_colors('psychic battle', ['U']).
card_text('psychic battle', 'Whenever a player chooses one or more targets, each player reveals the top card of his or her library. The player who reveals the card with the highest converted mana cost may change the target or targets. If two or more cards are tied for highest cost, the target or targets remain unchanged. Changing targets this way doesn\'t trigger abilities of permanents named Psychic Battle.').
card_mana_cost('psychic battle', ['3', 'U', 'U']).
card_cmc('psychic battle', 5).
card_layout('psychic battle', 'normal').

% Found in: RAV
card_name('psychic drain', 'Psychic Drain').
card_type('psychic drain', 'Sorcery').
card_types('psychic drain', ['Sorcery']).
card_subtypes('psychic drain', []).
card_colors('psychic drain', ['U', 'B']).
card_text('psychic drain', 'Target player puts the top X cards of his or her library into his or her graveyard and you gain X life.').
card_mana_cost('psychic drain', ['X', 'U', 'B']).
card_cmc('psychic drain', 2).
card_layout('psychic drain', 'normal').

% Found in: THS
card_name('psychic intrusion', 'Psychic Intrusion').
card_type('psychic intrusion', 'Sorcery').
card_types('psychic intrusion', ['Sorcery']).
card_subtypes('psychic intrusion', []).
card_colors('psychic intrusion', ['U', 'B']).
card_text('psychic intrusion', 'Target opponent reveals his or her hand. You choose a nonland card from that player\'s graveyard or hand and exile it. You may cast that card for as long as it remains exiled, and you may spend mana as though it were mana of any color to cast that spell.').
card_mana_cost('psychic intrusion', ['3', 'U', 'B']).
card_cmc('psychic intrusion', 5).
card_layout('psychic intrusion', 'normal').

% Found in: MRD
card_name('psychic membrane', 'Psychic Membrane').
card_type('psychic membrane', 'Creature — Wall').
card_types('psychic membrane', ['Creature']).
card_subtypes('psychic membrane', ['Wall']).
card_colors('psychic membrane', ['U']).
card_text('psychic membrane', 'Defender (This creature can\'t attack.)\nWhenever Psychic Membrane blocks, you may draw a card.').
card_mana_cost('psychic membrane', ['2', 'U']).
card_cmc('psychic membrane', 3).
card_layout('psychic membrane', 'normal').
card_power('psychic membrane', 0).
card_toughness('psychic membrane', 3).

% Found in: SOM
card_name('psychic miasma', 'Psychic Miasma').
card_type('psychic miasma', 'Sorcery').
card_types('psychic miasma', ['Sorcery']).
card_subtypes('psychic miasma', []).
card_colors('psychic miasma', ['B']).
card_text('psychic miasma', 'Target player discards a card. If a land card is discarded this way, return Psychic Miasma to its owner\'s hand.').
card_mana_cost('psychic miasma', ['1', 'B']).
card_cmc('psychic miasma', 2).
card_layout('psychic miasma', 'normal').

% Found in: UGL
card_name('psychic network', 'Psychic Network').
card_type('psychic network', 'Enchantment').
card_types('psychic network', ['Enchantment']).
card_subtypes('psychic network', []).
card_colors('psychic network', ['U']).
card_text('psychic network', 'Each player reveals the top card of his or her library to all other players by continuously holding it against his or her forehead. This does not allow a player to look at his or her own card. (That card still counts as the top card of your library. Whenever you draw a card, draw that one and replace it with the next card of your library.)').
card_mana_cost('psychic network', ['U']).
card_cmc('psychic network', 1).
card_layout('psychic network', 'normal').

% Found in: DST
card_name('psychic overload', 'Psychic Overload').
card_type('psychic overload', 'Enchantment — Aura').
card_types('psychic overload', ['Enchantment']).
card_subtypes('psychic overload', ['Aura']).
card_colors('psychic overload', ['U']).
card_text('psychic overload', 'Enchant permanent\nWhen Psychic Overload enters the battlefield, tap enchanted permanent.\nEnchanted permanent doesn\'t untap during its controller\'s untap step.\nEnchanted permanent has \"Discard two artifact cards: Untap this permanent.\"').
card_mana_cost('psychic overload', ['3', 'U']).
card_cmc('psychic overload', 4).
card_layout('psychic overload', 'normal').

% Found in: DIS
card_name('psychic possession', 'Psychic Possession').
card_type('psychic possession', 'Enchantment — Aura').
card_types('psychic possession', ['Enchantment']).
card_subtypes('psychic possession', ['Aura']).
card_colors('psychic possession', ['U']).
card_text('psychic possession', 'Enchant opponent\nSkip your draw step.\nWhenever enchanted opponent draws a card, you may draw a card.').
card_mana_cost('psychic possession', ['2', 'U', 'U']).
card_cmc('psychic possession', 4).
card_layout('psychic possession', 'normal').

% Found in: CHK
card_name('psychic puppetry', 'Psychic Puppetry').
card_type('psychic puppetry', 'Instant — Arcane').
card_types('psychic puppetry', ['Instant']).
card_subtypes('psychic puppetry', ['Arcane']).
card_colors('psychic puppetry', ['U']).
card_text('psychic puppetry', 'You may tap or untap target permanent.\nSplice onto Arcane {U} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('psychic puppetry', ['1', 'U']).
card_cmc('psychic puppetry', 2).
card_layout('psychic puppetry', 'normal').

% Found in: LEG, MED
card_name('psychic purge', 'Psychic Purge').
card_type('psychic purge', 'Sorcery').
card_types('psychic purge', ['Sorcery']).
card_subtypes('psychic purge', []).
card_colors('psychic purge', ['U']).
card_text('psychic purge', 'Psychic Purge deals 1 damage to target creature or player.\nWhen a spell or ability an opponent controls causes you to discard Psychic Purge, that player loses 5 life.').
card_mana_cost('psychic purge', ['U']).
card_cmc('psychic purge', 1).
card_layout('psychic purge', 'normal').

% Found in: ORI
card_name('psychic rebuttal', 'Psychic Rebuttal').
card_type('psychic rebuttal', 'Instant').
card_types('psychic rebuttal', ['Instant']).
card_subtypes('psychic rebuttal', []).
card_colors('psychic rebuttal', ['U']).
card_text('psychic rebuttal', 'Counter target instant or sorcery spell that targets you.\nSpell mastery — If there are two or more instant and/or sorcery cards in your graveyard, you may copy the spell countered this way. You may choose new targets for the copy.').
card_mana_cost('psychic rebuttal', ['1', 'U']).
card_cmc('psychic rebuttal', 2).
card_layout('psychic rebuttal', 'normal').

% Found in: BOK
card_name('psychic spear', 'Psychic Spear').
card_type('psychic spear', 'Sorcery').
card_types('psychic spear', ['Sorcery']).
card_subtypes('psychic spear', []).
card_colors('psychic spear', ['B']).
card_text('psychic spear', 'Target player reveals his or her hand. You choose a Spirit or Arcane card from it. That player discards that card.').
card_mana_cost('psychic spear', ['B']).
card_cmc('psychic spear', 1).
card_layout('psychic spear', 'normal').

% Found in: RTR
card_name('psychic spiral', 'Psychic Spiral').
card_type('psychic spiral', 'Instant').
card_types('psychic spiral', ['Instant']).
card_subtypes('psychic spiral', []).
card_colors('psychic spiral', ['U']).
card_text('psychic spiral', 'Shuffle all cards from your graveyard into your library. Target player puts that many cards from the top of his or her library into his or her graveyard.').
card_mana_cost('psychic spiral', ['4', 'U']).
card_cmc('psychic spiral', 5).
card_layout('psychic spiral', 'normal').

% Found in: GTC
card_name('psychic strike', 'Psychic Strike').
card_type('psychic strike', 'Instant').
card_types('psychic strike', ['Instant']).
card_subtypes('psychic strike', []).
card_colors('psychic strike', ['U', 'B']).
card_text('psychic strike', 'Counter target spell. Its controller puts the top two cards of his or her library into his or her graveyard.').
card_mana_cost('psychic strike', ['1', 'U', 'B']).
card_cmc('psychic strike', 3).
card_layout('psychic strike', 'normal').

% Found in: NPH
card_name('psychic surgery', 'Psychic Surgery').
card_type('psychic surgery', 'Enchantment').
card_types('psychic surgery', ['Enchantment']).
card_subtypes('psychic surgery', []).
card_colors('psychic surgery', ['U']).
card_text('psychic surgery', 'Whenever an opponent shuffles his or her library, you may look at the top two cards of that library. You may exile one of those cards. Then put the rest on top of that library in any order.').
card_mana_cost('psychic surgery', ['1', 'U']).
card_cmc('psychic surgery', 2).
card_layout('psychic surgery', 'normal').

% Found in: PCY
card_name('psychic theft', 'Psychic Theft').
card_type('psychic theft', 'Sorcery').
card_types('psychic theft', ['Sorcery']).
card_subtypes('psychic theft', []).
card_colors('psychic theft', ['U']).
card_text('psychic theft', 'Target player reveals his or her hand. You choose an instant or sorcery card from it and exile that card. You may cast that card for as long as it remains exiled. At the beginning of the next end step, if you haven\'t cast the card, return it to its owner\'s hand.').
card_mana_cost('psychic theft', ['1', 'U']).
card_cmc('psychic theft', 2).
card_layout('psychic theft', 'normal').

% Found in: ONS
card_name('psychic trance', 'Psychic Trance').
card_type('psychic trance', 'Instant').
card_types('psychic trance', ['Instant']).
card_subtypes('psychic trance', []).
card_colors('psychic trance', ['U']).
card_text('psychic trance', 'Until end of turn, Wizards you control gain \"{T}: Counter target spell.\"').
card_mana_cost('psychic trance', ['2', 'U', 'U']).
card_cmc('psychic trance', 4).
card_layout('psychic trance', 'normal').

% Found in: 6ED, MIR, S99
card_name('psychic transfer', 'Psychic Transfer').
card_type('psychic transfer', 'Sorcery').
card_types('psychic transfer', ['Sorcery']).
card_subtypes('psychic transfer', []).
card_colors('psychic transfer', ['U']).
card_text('psychic transfer', 'If the difference between your life total and target player\'s life total is 5 or less, exchange life totals with that player.').
card_mana_cost('psychic transfer', ['4', 'U']).
card_cmc('psychic transfer', 5).
card_layout('psychic transfer', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, CED, CEI, LEA, LEB, MED
card_name('psychic venom', 'Psychic Venom').
card_type('psychic venom', 'Enchantment — Aura').
card_types('psychic venom', ['Enchantment']).
card_subtypes('psychic venom', ['Aura']).
card_colors('psychic venom', ['U']).
card_text('psychic venom', 'Enchant land\nWhenever enchanted land becomes tapped, Psychic Venom deals 2 damage to that land\'s controller.').
card_mana_cost('psychic venom', ['1', 'U']).
card_cmc('psychic venom', 2).
card_layout('psychic venom', 'normal').

% Found in: WTH
card_name('psychic vortex', 'Psychic Vortex').
card_type('psychic vortex', 'Enchantment').
card_types('psychic vortex', ['Enchantment']).
card_subtypes('psychic vortex', []).
card_colors('psychic vortex', ['U']).
card_text('psychic vortex', 'Cumulative upkeep—Draw a card. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAt the beginning of your end step, sacrifice a land and discard your hand.').
card_mana_cost('psychic vortex', ['2', 'U', 'U']).
card_cmc('psychic vortex', 4).
card_layout('psychic vortex', 'normal').
card_reserved('psychic vortex').

% Found in: MRD
card_name('psychogenic probe', 'Psychogenic Probe').
card_type('psychogenic probe', 'Artifact').
card_types('psychogenic probe', ['Artifact']).
card_subtypes('psychogenic probe', []).
card_colors('psychogenic probe', []).
card_text('psychogenic probe', 'Whenever a spell or ability causes a player to shuffle his or her library, Psychogenic Probe deals 2 damage to him or her.').
card_mana_cost('psychogenic probe', ['2']).
card_cmc('psychogenic probe', 2).
card_layout('psychogenic probe', 'normal').

% Found in: MBS
card_name('psychosis crawler', 'Psychosis Crawler').
card_type('psychosis crawler', 'Artifact Creature — Horror').
card_types('psychosis crawler', ['Artifact', 'Creature']).
card_subtypes('psychosis crawler', ['Horror']).
card_colors('psychosis crawler', []).
card_text('psychosis crawler', 'Psychosis Crawler\'s power and toughness are each equal to the number of cards in your hand.\nWhenever you draw a card, each opponent loses 1 life.').
card_mana_cost('psychosis crawler', ['5']).
card_cmc('psychosis crawler', 5).
card_layout('psychosis crawler', 'normal').
card_power('psychosis crawler', '*').
card_toughness('psychosis crawler', '*').

% Found in: TSP
card_name('psychotic episode', 'Psychotic Episode').
card_type('psychotic episode', 'Sorcery').
card_types('psychotic episode', ['Sorcery']).
card_subtypes('psychotic episode', []).
card_colors('psychotic episode', ['B']).
card_text('psychotic episode', 'Target player reveals his or her hand and the top card of his or her library. You choose a card revealed this way. That player puts the chosen card on the bottom of his or her library.\nMadness {1}{B} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('psychotic episode', ['1', 'B', 'B']).
card_cmc('psychotic episode', 3).
card_layout('psychotic episode', 'normal').

% Found in: DIS
card_name('psychotic fury', 'Psychotic Fury').
card_type('psychotic fury', 'Instant').
card_types('psychotic fury', ['Instant']).
card_subtypes('psychotic fury', []).
card_colors('psychotic fury', ['R']).
card_text('psychotic fury', 'Target multicolored creature gains double strike until end of turn.\nDraw a card.').
card_mana_cost('psychotic fury', ['1', 'R']).
card_cmc('psychotic fury', 2).
card_layout('psychotic fury', 'normal').

% Found in: TOR
card_name('psychotic haze', 'Psychotic Haze').
card_type('psychotic haze', 'Instant').
card_types('psychotic haze', ['Instant']).
card_subtypes('psychotic haze', []).
card_colors('psychotic haze', ['B']).
card_text('psychotic haze', 'Psychotic Haze deals 1 damage to each creature and each player.\nMadness {1}{B} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('psychotic haze', ['2', 'B', 'B']).
card_cmc('psychotic haze', 4).
card_layout('psychotic haze', 'normal').

% Found in: PLC
card_name('psychotrope thallid', 'Psychotrope Thallid').
card_type('psychotrope thallid', 'Creature — Fungus').
card_types('psychotrope thallid', ['Creature']).
card_subtypes('psychotrope thallid', ['Fungus']).
card_colors('psychotrope thallid', ['G']).
card_text('psychotrope thallid', 'At the beginning of your upkeep, put a spore counter on Psychotrope Thallid.\nRemove three spore counters from Psychotrope Thallid: Put a 1/1 green Saproling creature token onto the battlefield.\n{1}, Sacrifice a Saproling: Draw a card.').
card_mana_cost('psychotrope thallid', ['2', 'G']).
card_cmc('psychotrope thallid', 3).
card_layout('psychotrope thallid', 'normal').
card_power('psychotrope thallid', 1).
card_toughness('psychotrope thallid', 1).

% Found in: DST
card_name('pteron ghost', 'Pteron Ghost').
card_type('pteron ghost', 'Creature — Spirit').
card_types('pteron ghost', ['Creature']).
card_subtypes('pteron ghost', ['Spirit']).
card_colors('pteron ghost', ['W']).
card_text('pteron ghost', 'Flying\nSacrifice Pteron Ghost: Regenerate target artifact.').
card_mana_cost('pteron ghost', ['1', 'W']).
card_cmc('pteron ghost', 2).
card_layout('pteron ghost', 'normal').
card_power('pteron ghost', 1).
card_toughness('pteron ghost', 1).

% Found in: M13
card_name('public execution', 'Public Execution').
card_type('public execution', 'Instant').
card_types('public execution', ['Instant']).
card_subtypes('public execution', []).
card_colors('public execution', ['B']).
card_text('public execution', 'Destroy target creature an opponent controls. Each other creature that player controls gets -2/-0 until end of turn.').
card_mana_cost('public execution', ['5', 'B']).
card_cmc('public execution', 6).
card_layout('public execution', 'normal').

% Found in: SHM
card_name('puca\'s mischief', 'Puca\'s Mischief').
card_type('puca\'s mischief', 'Enchantment').
card_types('puca\'s mischief', ['Enchantment']).
card_subtypes('puca\'s mischief', []).
card_colors('puca\'s mischief', ['U']).
card_text('puca\'s mischief', 'At the beginning of your upkeep, you may exchange control of target nonland permanent you control and target nonland permanent an opponent controls with an equal or lesser converted mana cost.').
card_mana_cost('puca\'s mischief', ['3', 'U']).
card_cmc('puca\'s mischief', 4).
card_layout('puca\'s mischief', 'normal').

% Found in: MMQ
card_name('puffer extract', 'Puffer Extract').
card_type('puffer extract', 'Artifact').
card_types('puffer extract', ['Artifact']).
card_subtypes('puffer extract', []).
card_colors('puffer extract', []).
card_text('puffer extract', '{X}, {T}: Target creature you control gets +X/+X until end of turn. Destroy it at the beginning of the next end step.').
card_mana_cost('puffer extract', ['5']).
card_cmc('puffer extract', 5).
card_layout('puffer extract', 'normal').

% Found in: TSP
card_name('pull from eternity', 'Pull from Eternity').
card_type('pull from eternity', 'Instant').
card_types('pull from eternity', ['Instant']).
card_subtypes('pull from eternity', []).
card_colors('pull from eternity', ['W']).
card_text('pull from eternity', 'Put target face-up exiled card into its owner\'s graveyard.').
card_mana_cost('pull from eternity', ['W']).
card_cmc('pull from eternity', 1).
card_layout('pull from eternity', 'normal').

% Found in: JOU
card_name('pull from the deep', 'Pull from the Deep').
card_type('pull from the deep', 'Sorcery').
card_types('pull from the deep', ['Sorcery']).
card_subtypes('pull from the deep', []).
card_colors('pull from the deep', ['U']).
card_text('pull from the deep', 'Return up to one target instant card and up to one target sorcery card from your graveyard to your hand. Exile Pull from the Deep.').
card_mana_cost('pull from the deep', ['2', 'U', 'U']).
card_cmc('pull from the deep', 4).
card_layout('pull from the deep', 'normal').

% Found in: CHK
card_name('pull under', 'Pull Under').
card_type('pull under', 'Instant — Arcane').
card_types('pull under', ['Instant']).
card_subtypes('pull under', ['Arcane']).
card_colors('pull under', ['B']).
card_text('pull under', 'Target creature gets -5/-5 until end of turn.').
card_mana_cost('pull under', ['5', 'B']).
card_cmc('pull under', 6).
card_layout('pull under', 'normal').

% Found in: MOR
card_name('pulling teeth', 'Pulling Teeth').
card_type('pulling teeth', 'Sorcery').
card_types('pulling teeth', ['Sorcery']).
card_subtypes('pulling teeth', []).
card_colors('pulling teeth', ['B']).
card_text('pulling teeth', 'Clash with an opponent. If you win, target player discards two cards. Otherwise, that player discards a card. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('pulling teeth', ['1', 'B']).
card_cmc('pulling teeth', 2).
card_layout('pulling teeth', 'normal').

% Found in: TSP
card_name('pulmonic sliver', 'Pulmonic Sliver').
card_type('pulmonic sliver', 'Creature — Sliver').
card_types('pulmonic sliver', ['Creature']).
card_subtypes('pulmonic sliver', ['Sliver']).
card_colors('pulmonic sliver', ['W']).
card_text('pulmonic sliver', 'All Sliver creatures have flying.\nAll Slivers have \"If this permanent would be put into a graveyard, you may put it on top of its owner\'s library instead.\"').
card_mana_cost('pulmonic sliver', ['3', 'W', 'W']).
card_cmc('pulmonic sliver', 5).
card_layout('pulmonic sliver', 'normal').
card_power('pulmonic sliver', 3).
card_toughness('pulmonic sliver', 3).

% Found in: ODY
card_name('pulsating illusion', 'Pulsating Illusion').
card_type('pulsating illusion', 'Creature — Illusion').
card_types('pulsating illusion', ['Creature']).
card_subtypes('pulsating illusion', ['Illusion']).
card_colors('pulsating illusion', ['U']).
card_text('pulsating illusion', 'Flying\nDiscard a card: Pulsating Illusion gets +4/+4 until end of turn. Activate this ability only once each turn.').
card_mana_cost('pulsating illusion', ['4', 'U']).
card_cmc('pulsating illusion', 5).
card_layout('pulsating illusion', 'normal').
card_power('pulsating illusion', 0).
card_toughness('pulsating illusion', 1).

% Found in: INV
card_name('pulse of llanowar', 'Pulse of Llanowar').
card_type('pulse of llanowar', 'Enchantment').
card_types('pulse of llanowar', ['Enchantment']).
card_subtypes('pulse of llanowar', []).
card_colors('pulse of llanowar', ['G']).
card_text('pulse of llanowar', 'If a basic land you control is tapped for mana, it produces mana of a color of your choice instead of any other type.').
card_mana_cost('pulse of llanowar', ['3', 'G']).
card_cmc('pulse of llanowar', 4).
card_layout('pulse of llanowar', 'normal').

% Found in: DST
card_name('pulse of the dross', 'Pulse of the Dross').
card_type('pulse of the dross', 'Sorcery').
card_types('pulse of the dross', ['Sorcery']).
card_subtypes('pulse of the dross', []).
card_colors('pulse of the dross', ['B']).
card_text('pulse of the dross', 'Target player reveals three cards from his or her hand and you choose one of them. That player discards that card. Then if that player has more cards in hand than you, return Pulse of the Dross to its owner\'s hand.').
card_mana_cost('pulse of the dross', ['1', 'B', 'B']).
card_cmc('pulse of the dross', 3).
card_layout('pulse of the dross', 'normal').

% Found in: DST
card_name('pulse of the fields', 'Pulse of the Fields').
card_type('pulse of the fields', 'Instant').
card_types('pulse of the fields', ['Instant']).
card_subtypes('pulse of the fields', []).
card_colors('pulse of the fields', ['W']).
card_text('pulse of the fields', 'You gain 4 life. Then if an opponent has more life than you, return Pulse of the Fields to its owner\'s hand.').
card_mana_cost('pulse of the fields', ['1', 'W', 'W']).
card_cmc('pulse of the fields', 3).
card_layout('pulse of the fields', 'normal').

% Found in: DST
card_name('pulse of the forge', 'Pulse of the Forge').
card_type('pulse of the forge', 'Instant').
card_types('pulse of the forge', ['Instant']).
card_subtypes('pulse of the forge', []).
card_colors('pulse of the forge', ['R']).
card_text('pulse of the forge', 'Pulse of the Forge deals 4 damage to target player. Then if that player has more life than you, return Pulse of the Forge to its owner\'s hand.').
card_mana_cost('pulse of the forge', ['1', 'R', 'R']).
card_cmc('pulse of the forge', 3).
card_layout('pulse of the forge', 'normal').

% Found in: DST
card_name('pulse of the grid', 'Pulse of the Grid').
card_type('pulse of the grid', 'Instant').
card_types('pulse of the grid', ['Instant']).
card_subtypes('pulse of the grid', []).
card_colors('pulse of the grid', ['U']).
card_text('pulse of the grid', 'Draw two cards, then discard a card. Then if an opponent has more cards in hand than you, return Pulse of the Grid to its owner\'s hand.').
card_mana_cost('pulse of the grid', ['1', 'U', 'U']).
card_cmc('pulse of the grid', 3).
card_layout('pulse of the grid', 'normal').

% Found in: DST
card_name('pulse of the tangle', 'Pulse of the Tangle').
card_type('pulse of the tangle', 'Sorcery').
card_types('pulse of the tangle', ['Sorcery']).
card_subtypes('pulse of the tangle', []).
card_colors('pulse of the tangle', ['G']).
card_text('pulse of the tangle', 'Put a 3/3 green Beast creature token onto the battlefield. Then if an opponent controls more creatures than you, return Pulse of the Tangle to its owner\'s hand.').
card_mana_cost('pulse of the tangle', ['1', 'G', 'G']).
card_cmc('pulse of the tangle', 3).
card_layout('pulse of the tangle', 'normal').

% Found in: DDM, WWK
card_name('pulse tracker', 'Pulse Tracker').
card_type('pulse tracker', 'Creature — Vampire Rogue').
card_types('pulse tracker', ['Creature']).
card_subtypes('pulse tracker', ['Vampire', 'Rogue']).
card_colors('pulse tracker', ['B']).
card_text('pulse tracker', 'Whenever Pulse Tracker attacks, each opponent loses 1 life.').
card_mana_cost('pulse tracker', ['B']).
card_cmc('pulse tracker', 1).
card_layout('pulse tracker', 'normal').
card_power('pulse tracker', 1).
card_toughness('pulse tracker', 1).

% Found in: JUD
card_name('pulsemage advocate', 'Pulsemage Advocate').
card_type('pulsemage advocate', 'Creature — Human Cleric').
card_types('pulsemage advocate', ['Creature']).
card_subtypes('pulsemage advocate', ['Human', 'Cleric']).
card_colors('pulsemage advocate', ['W']).
card_text('pulsemage advocate', '{T}: Return three target cards from an opponent\'s graveyard to his or her hand. Return target creature card from your graveyard to the battlefield.').
card_mana_cost('pulsemage advocate', ['2', 'W']).
card_cmc('pulsemage advocate', 3).
card_layout('pulsemage advocate', 'normal').
card_power('pulsemage advocate', 1).
card_toughness('pulsemage advocate', 3).

% Found in: MMQ
card_name('pulverize', 'Pulverize').
card_type('pulverize', 'Sorcery').
card_types('pulverize', ['Sorcery']).
card_subtypes('pulverize', []).
card_colors('pulverize', ['R']).
card_text('pulverize', 'You may sacrifice two Mountains rather than pay Pulverize\'s mana cost.\nDestroy all artifacts.').
card_mana_cost('pulverize', ['4', 'R', 'R']).
card_cmc('pulverize', 6).
card_layout('pulverize', 'normal').

% Found in: UNH
card_name('punctuate', 'Punctuate').
card_type('punctuate', 'Instant').
card_types('punctuate', ['Instant']).
card_subtypes('punctuate', []).
card_colors('punctuate', ['R']).
card_text('punctuate', 'Punctuate deals damage to target creature equal to half the number of punctuation marks in that creature\'s text box. (The punctuation marks are  ? , ; : - ( ) / \" \' & .)').
card_mana_cost('punctuate', ['3', 'R']).
card_cmc('punctuate', 4).
card_layout('punctuate', 'normal').

% Found in: EVE
card_name('puncture blast', 'Puncture Blast').
card_type('puncture blast', 'Instant').
card_types('puncture blast', ['Instant']).
card_subtypes('puncture blast', []).
card_colors('puncture blast', ['R']).
card_text('puncture blast', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nPuncture Blast deals 3 damage to target creature or player.').
card_mana_cost('puncture blast', ['2', 'R']).
card_cmc('puncture blast', 3).
card_layout('puncture blast', 'normal').

% Found in: SHM
card_name('puncture bolt', 'Puncture Bolt').
card_type('puncture bolt', 'Instant').
card_types('puncture bolt', ['Instant']).
card_subtypes('puncture bolt', []).
card_colors('puncture bolt', ['R']).
card_text('puncture bolt', 'Puncture Bolt deals 1 damage to target creature. Put a -1/-1 counter on that creature.').
card_mana_cost('puncture bolt', ['1', 'R']).
card_cmc('puncture bolt', 2).
card_layout('puncture bolt', 'normal').

% Found in: ROE
card_name('puncturing light', 'Puncturing Light').
card_type('puncturing light', 'Instant').
card_types('puncturing light', ['Instant']).
card_subtypes('puncturing light', []).
card_colors('puncturing light', ['W']).
card_text('puncturing light', 'Destroy target attacking or blocking creature with power 3 or less.').
card_mana_cost('puncturing light', ['1', 'W']).
card_cmc('puncturing light', 2).
card_layout('puncturing light', 'normal').

% Found in: ALA
card_name('punish ignorance', 'Punish Ignorance').
card_type('punish ignorance', 'Instant').
card_types('punish ignorance', ['Instant']).
card_subtypes('punish ignorance', []).
card_colors('punish ignorance', ['W', 'U', 'B']).
card_text('punish ignorance', 'Counter target spell. Its controller loses 3 life and you gain 3 life.').
card_mana_cost('punish ignorance', ['W', 'U', 'U', 'B']).
card_cmc('punish ignorance', 4).
card_layout('punish ignorance', 'normal').

% Found in: DGM
card_name('punish the enemy', 'Punish the Enemy').
card_type('punish the enemy', 'Instant').
card_types('punish the enemy', ['Instant']).
card_subtypes('punish the enemy', []).
card_colors('punish the enemy', ['R']).
card_text('punish the enemy', 'Punish the Enemy deals 3 damage to target player and 3 damage to target creature.').
card_mana_cost('punish the enemy', ['4', 'R']).
card_cmc('punish the enemy', 5).
card_layout('punish the enemy', 'normal').

% Found in: CMD, DDG, ZEN
card_name('punishing fire', 'Punishing Fire').
card_type('punishing fire', 'Instant').
card_types('punishing fire', ['Instant']).
card_subtypes('punishing fire', []).
card_colors('punishing fire', ['R']).
card_text('punishing fire', 'Punishing Fire deals 2 damage to target creature or player.\nWhenever an opponent gains life, you may pay {R}. If you do, return Punishing Fire from your graveyard to your hand.').
card_mana_cost('punishing fire', ['1', 'R']).
card_cmc('punishing fire', 2).
card_layout('punishing fire', 'normal').

% Found in: DIS
card_name('punishment', 'Punishment').
card_type('punishment', 'Sorcery').
card_types('punishment', ['Sorcery']).
card_subtypes('punishment', []).
card_colors('punishment', ['B', 'G']).
card_text('punishment', 'Destroy each artifact, creature, and enchantment with converted mana cost X.').
card_mana_cost('punishment', ['X', 'B', 'G']).
card_cmc('punishment', 2).
card_layout('punishment', 'split').

% Found in: ALA
card_name('puppet conjurer', 'Puppet Conjurer').
card_type('puppet conjurer', 'Artifact Creature — Human Wizard').
card_types('puppet conjurer', ['Artifact', 'Creature']).
card_subtypes('puppet conjurer', ['Human', 'Wizard']).
card_colors('puppet conjurer', ['B']).
card_text('puppet conjurer', '{U}, {T}: Put a 0/1 blue Homunculus artifact creature token onto the battlefield.\nAt the beginning of your upkeep, sacrifice a Homunculus.').
card_mana_cost('puppet conjurer', ['1', 'B']).
card_cmc('puppet conjurer', 2).
card_layout('puppet conjurer', 'normal').
card_power('puppet conjurer', 1).
card_toughness('puppet conjurer', 2).

% Found in: CHR, LEG
card_name('puppet master', 'Puppet Master').
card_type('puppet master', 'Enchantment — Aura').
card_types('puppet master', ['Enchantment']).
card_subtypes('puppet master', ['Aura']).
card_colors('puppet master', ['U']).
card_text('puppet master', 'Enchant creature\nWhen enchanted creature dies, return that card to its owner\'s hand. If that card is returned to its owner\'s hand this way, you may pay {U}{U}{U}. If you do, return Puppet Master to its owner\'s hand.').
card_mana_cost('puppet master', ['U', 'U', 'U']).
card_cmc('puppet master', 3).
card_layout('puppet master', 'normal').

% Found in: DDE, TMP
card_name('puppet strings', 'Puppet Strings').
card_type('puppet strings', 'Artifact').
card_types('puppet strings', ['Artifact']).
card_subtypes('puppet strings', []).
card_colors('puppet strings', []).
card_text('puppet strings', '{2}, {T}: You may tap or untap target creature.').
card_mana_cost('puppet strings', ['3']).
card_cmc('puppet strings', 3).
card_layout('puppet strings', 'normal').

% Found in: MMQ
card_name('puppet\'s verdict', 'Puppet\'s Verdict').
card_type('puppet\'s verdict', 'Instant').
card_types('puppet\'s verdict', ['Instant']).
card_subtypes('puppet\'s verdict', []).
card_colors('puppet\'s verdict', ['R']).
card_text('puppet\'s verdict', 'Flip a coin. If you win the flip, destroy all creatures with power 2 or less. If you lose the flip, destroy all creatures with power 3 or greater.').
card_mana_cost('puppet\'s verdict', ['1', 'R', 'R']).
card_cmc('puppet\'s verdict', 3).
card_layout('puppet\'s verdict', 'normal').

% Found in: 10E, 8ED, 9ED, ODY
card_name('puppeteer', 'Puppeteer').
card_type('puppeteer', 'Creature — Human Wizard').
card_types('puppeteer', ['Creature']).
card_subtypes('puppeteer', ['Human', 'Wizard']).
card_colors('puppeteer', ['U']).
card_text('puppeteer', '{U}, {T}: You may tap or untap target creature.').
card_mana_cost('puppeteer', ['2', 'U']).
card_cmc('puppeteer', 3).
card_layout('puppeteer', 'normal').
card_power('puppeteer', 1).
card_toughness('puppeteer', 2).

% Found in: MM2, SHM
card_name('puppeteer clique', 'Puppeteer Clique').
card_type('puppeteer clique', 'Creature — Faerie Wizard').
card_types('puppeteer clique', ['Creature']).
card_subtypes('puppeteer clique', ['Faerie', 'Wizard']).
card_colors('puppeteer clique', ['B']).
card_text('puppeteer clique', 'Flying\nWhen Puppeteer Clique enters the battlefield, put target creature card from an opponent\'s graveyard onto the battlefield under your control. It gains haste. At the beginning of your next end step, exile it.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('puppeteer clique', ['3', 'B', 'B']).
card_cmc('puppeteer clique', 5).
card_layout('puppeteer clique', 'normal').
card_power('puppeteer clique', 3).
card_toughness('puppeteer clique', 2).

% Found in: DIS
card_name('pure', 'Pure').
card_type('pure', 'Sorcery').
card_types('pure', ['Sorcery']).
card_subtypes('pure', []).
card_colors('pure', ['R', 'G']).
card_text('pure', 'Destroy target multicolored permanent.').
card_mana_cost('pure', ['1', 'R', 'G']).
card_cmc('pure', 3).
card_layout('pure', 'split').
card_sides('pure', 'simple').

% Found in: SOK
card_name('pure intentions', 'Pure Intentions').
card_type('pure intentions', 'Instant — Arcane').
card_types('pure intentions', ['Instant']).
card_subtypes('pure intentions', ['Arcane']).
card_colors('pure intentions', ['W']).
card_text('pure intentions', 'Whenever a spell or ability an opponent controls causes you to discard cards this turn, return those cards from your graveyard to your hand.\nWhen a spell or ability an opponent controls causes you to discard Pure Intentions, return Pure Intentions from your graveyard to your hand at the beginning of the next end step.').
card_mana_cost('pure intentions', ['W']).
card_cmc('pure intentions', 1).
card_layout('pure intentions', 'normal').

% Found in: INV
card_name('pure reflection', 'Pure Reflection').
card_type('pure reflection', 'Enchantment').
card_types('pure reflection', ['Enchantment']).
card_subtypes('pure reflection', []).
card_colors('pure reflection', ['W']).
card_text('pure reflection', 'Whenever a player casts a creature spell, destroy all Reflections. Then that player puts an X/X white Reflection creature token onto the battlefield, where X is the converted mana cost of that spell.').
card_mana_cost('pure reflection', ['2', 'W']).
card_cmc('pure reflection', 3).
card_layout('pure reflection', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('purelace', 'Purelace').
card_type('purelace', 'Instant').
card_types('purelace', ['Instant']).
card_subtypes('purelace', []).
card_colors('purelace', ['W']).
card_text('purelace', 'Target spell or permanent becomes white. (Mana symbols on that permanent remain unchanged.)').
card_mana_cost('purelace', ['W']).
card_cmc('purelace', 1).
card_layout('purelace', 'normal').

% Found in: SHM
card_name('puresight merrow', 'Puresight Merrow').
card_type('puresight merrow', 'Creature — Merfolk Wizard').
card_types('puresight merrow', ['Creature']).
card_subtypes('puresight merrow', ['Merfolk', 'Wizard']).
card_colors('puresight merrow', ['W', 'U']).
card_text('puresight merrow', '{W/U}, {Q}: Look at the top card of your library. You may exile that card. ({Q} is the untap symbol.)').
card_mana_cost('puresight merrow', ['W/U', 'W/U']).
card_cmc('puresight merrow', 2).
card_layout('puresight merrow', 'normal').
card_power('puresight merrow', 2).
card_toughness('puresight merrow', 2).

% Found in: NPH
card_name('puresteel paladin', 'Puresteel Paladin').
card_type('puresteel paladin', 'Creature — Human Knight').
card_types('puresteel paladin', ['Creature']).
card_subtypes('puresteel paladin', ['Human', 'Knight']).
card_colors('puresteel paladin', ['W']).
card_text('puresteel paladin', 'Whenever an Equipment enters the battlefield under your control, you may draw a card.\nMetalcraft — Equipment you control have equip {0} as long as you control three or more artifacts.').
card_mana_cost('puresteel paladin', ['W', 'W']).
card_cmc('puresteel paladin', 2).
card_layout('puresteel paladin', 'normal').
card_power('puresteel paladin', 2).
card_toughness('puresteel paladin', 2).

% Found in: MIR
card_name('purgatory', 'Purgatory').
card_type('purgatory', 'Enchantment').
card_types('purgatory', ['Enchantment']).
card_subtypes('purgatory', []).
card_colors('purgatory', ['W', 'B']).
card_text('purgatory', 'Whenever a nontoken creature is put into your graveyard from the battlefield, exile that card.\nAt the beginning of your upkeep, you may pay {4} and 2 life. If you do, return a card exiled with Purgatory to the battlefield.').
card_mana_cost('purgatory', ['2', 'W', 'B']).
card_cmc('purgatory', 4).
card_layout('purgatory', 'normal').
card_reserved('purgatory').

% Found in: DST
card_name('purge', 'Purge').
card_type('purge', 'Instant').
card_types('purge', ['Instant']).
card_subtypes('purge', []).
card_colors('purge', ['W']).
card_text('purge', 'Destroy target artifact creature or black creature. It can\'t be regenerated.').
card_mana_cost('purge', ['1', 'W']).
card_cmc('purge', 2).
card_layout('purge', 'normal').

% Found in: GTC
card_name('purge the profane', 'Purge the Profane').
card_type('purge the profane', 'Sorcery').
card_types('purge the profane', ['Sorcery']).
card_subtypes('purge the profane', []).
card_colors('purge the profane', ['W', 'B']).
card_text('purge the profane', 'Target opponent discards two cards and you gain 2 life.').
card_mana_cost('purge the profane', ['2', 'W', 'B']).
card_cmc('purge the profane', 4).
card_layout('purge the profane', 'normal').

% Found in: USG
card_name('purging scythe', 'Purging Scythe').
card_type('purging scythe', 'Artifact').
card_types('purging scythe', ['Artifact']).
card_subtypes('purging scythe', []).
card_colors('purging scythe', []).
card_text('purging scythe', 'At the beginning of your upkeep, Purging Scythe deals 2 damage to the creature with the least toughness. If two or more creatures are tied for least toughness, you choose one of them.').
card_mana_cost('purging scythe', ['5']).
card_cmc('purging scythe', 5).
card_layout('purging scythe', 'normal').

% Found in: 7ED, ULG
card_name('purify', 'Purify').
card_type('purify', 'Sorcery').
card_types('purify', ['Sorcery']).
card_subtypes('purify', []).
card_colors('purify', ['W']).
card_text('purify', 'Destroy all artifacts and enchantments.').
card_mana_cost('purify', ['3', 'W', 'W']).
card_cmc('purify', 5).
card_layout('purify', 'normal').

% Found in: ISD
card_name('purify the grave', 'Purify the Grave').
card_type('purify the grave', 'Instant').
card_types('purify the grave', ['Instant']).
card_subtypes('purify the grave', []).
card_colors('purify the grave', ['W']).
card_text('purify the grave', 'Exile target card from a graveyard.\nFlashback {W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('purify the grave', ['W']).
card_cmc('purify the grave', 1).
card_layout('purify the grave', 'normal').

% Found in: LRW
card_name('purity', 'Purity').
card_type('purity', 'Creature — Elemental Incarnation').
card_types('purity', ['Creature']).
card_subtypes('purity', ['Elemental', 'Incarnation']).
card_colors('purity', ['W']).
card_text('purity', 'Flying\nIf noncombat damage would be dealt to you, prevent that damage. You gain life equal to the damage prevented this way.\nWhen Purity is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_mana_cost('purity', ['3', 'W', 'W', 'W']).
card_cmc('purity', 6).
card_layout('purity', 'normal').
card_power('purity', 6).
card_toughness('purity', 6).

% Found in: THS
card_name('purphoros\'s emissary', 'Purphoros\'s Emissary').
card_type('purphoros\'s emissary', 'Enchantment Creature — Ox').
card_types('purphoros\'s emissary', ['Enchantment', 'Creature']).
card_subtypes('purphoros\'s emissary', ['Ox']).
card_colors('purphoros\'s emissary', ['R']).
card_text('purphoros\'s emissary', 'Bestow {6}{R} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nMenace (This creature can\'t be blocked except by two or more creatures.)\nEnchanted creature gets +3/+3 and has menace.').
card_mana_cost('purphoros\'s emissary', ['3', 'R']).
card_cmc('purphoros\'s emissary', 4).
card_layout('purphoros\'s emissary', 'normal').
card_power('purphoros\'s emissary', 3).
card_toughness('purphoros\'s emissary', 3).

% Found in: THS
card_name('purphoros, god of the forge', 'Purphoros, God of the Forge').
card_type('purphoros, god of the forge', 'Legendary Enchantment Creature — God').
card_types('purphoros, god of the forge', ['Enchantment', 'Creature']).
card_subtypes('purphoros, god of the forge', ['God']).
card_supertypes('purphoros, god of the forge', ['Legendary']).
card_colors('purphoros, god of the forge', ['R']).
card_text('purphoros, god of the forge', 'Indestructible\nAs long as your devotion to red is less than five, Purphoros isn\'t a creature.\nWhenever another creature enters the battlefield under your control, Purphoros deals 2 damage to each opponent.\n{2}{R}: Creatures you control get +1/+0 until end of turn.').
card_mana_cost('purphoros, god of the forge', ['3', 'R']).
card_cmc('purphoros, god of the forge', 4).
card_layout('purphoros, god of the forge', 'normal').
card_power('purphoros, god of the forge', 6).
card_toughness('purphoros, god of the forge', 5).

% Found in: MIR
card_name('purraj of urborg', 'Purraj of Urborg').
card_type('purraj of urborg', 'Legendary Creature — Cat Warrior').
card_types('purraj of urborg', ['Creature']).
card_subtypes('purraj of urborg', ['Cat', 'Warrior']).
card_supertypes('purraj of urborg', ['Legendary']).
card_colors('purraj of urborg', ['B']).
card_text('purraj of urborg', 'Purraj of Urborg has first strike as long as it\'s attacking.\nWhenever a player casts a black spell, you may pay {B}. If you do, put a +1/+1 counter on Purraj of Urborg.').
card_mana_cost('purraj of urborg', ['3', 'B', 'B']).
card_cmc('purraj of urborg', 5).
card_layout('purraj of urborg', 'normal').
card_power('purraj of urborg', 2).
card_toughness('purraj of urborg', 3).
card_reserved('purraj of urborg').

% Found in: RTR
card_name('pursuit of flight', 'Pursuit of Flight').
card_type('pursuit of flight', 'Enchantment — Aura').
card_types('pursuit of flight', ['Enchantment']).
card_subtypes('pursuit of flight', ['Aura']).
card_colors('pursuit of flight', ['R']).
card_text('pursuit of flight', 'Enchant creature\nEnchanted creature gets +2/+2 and has \"{U}: This creature gains flying until end of turn.\"').
card_mana_cost('pursuit of flight', ['1', 'R']).
card_cmc('pursuit of flight', 2).
card_layout('pursuit of flight', 'normal').

% Found in: STH
card_name('pursuit of knowledge', 'Pursuit of Knowledge').
card_type('pursuit of knowledge', 'Enchantment').
card_types('pursuit of knowledge', ['Enchantment']).
card_subtypes('pursuit of knowledge', []).
card_colors('pursuit of knowledge', ['W']).
card_text('pursuit of knowledge', 'If you would draw a card, you may put a study counter on Pursuit of Knowledge instead.\nRemove three study counters from Pursuit of Knowledge, Sacrifice Pursuit of Knowledge: Draw seven cards.').
card_mana_cost('pursuit of knowledge', ['3', 'W']).
card_cmc('pursuit of knowledge', 4).
card_layout('pursuit of knowledge', 'normal').

% Found in: BOK
card_name('pus kami', 'Pus Kami').
card_type('pus kami', 'Creature — Spirit').
card_types('pus kami', ['Creature']).
card_subtypes('pus kami', ['Spirit']).
card_colors('pus kami', ['B']).
card_text('pus kami', '{B}, Sacrifice Pus Kami: Destroy target nonblack creature.\nSoulshift 6 (When this creature dies, you may return target Spirit card with converted mana cost 6 or less from your graveyard to your hand.)').
card_mana_cost('pus kami', ['5', 'B', 'B']).
card_cmc('pus kami', 7).
card_layout('pus kami', 'normal').
card_power('pus kami', 3).
card_toughness('pus kami', 3).

% Found in: SHM
card_name('put away', 'Put Away').
card_type('put away', 'Instant').
card_types('put away', ['Instant']).
card_subtypes('put away', []).
card_colors('put away', ['U']).
card_text('put away', 'Counter target spell. You may shuffle up to one target card from your graveyard into your library.').
card_mana_cost('put away', ['2', 'U', 'U']).
card_cmc('put away', 4).
card_layout('put away', 'normal').

% Found in: MMQ
card_name('putrefaction', 'Putrefaction').
card_type('putrefaction', 'Enchantment').
card_types('putrefaction', ['Enchantment']).
card_subtypes('putrefaction', []).
card_colors('putrefaction', ['B']).
card_text('putrefaction', 'Whenever a player casts a green or white spell, that player discards a card.').
card_mana_cost('putrefaction', ['4', 'B']).
card_cmc('putrefaction', 5).
card_layout('putrefaction', 'normal').

% Found in: SOM
card_name('putrefax', 'Putrefax').
card_type('putrefax', 'Creature — Horror').
card_types('putrefax', ['Creature']).
card_subtypes('putrefax', ['Horror']).
card_colors('putrefax', ['G']).
card_text('putrefax', 'Trample, haste\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nAt the beginning of the end step, sacrifice Putrefax.').
card_mana_cost('putrefax', ['3', 'G', 'G']).
card_cmc('putrefax', 5).
card_layout('putrefax', 'normal').
card_power('putrefax', 5).
card_toughness('putrefax', 3).

% Found in: DDJ, DGM, RAV, pMPR
card_name('putrefy', 'Putrefy').
card_type('putrefy', 'Instant').
card_types('putrefy', ['Instant']).
card_subtypes('putrefy', []).
card_colors('putrefy', ['B', 'G']).
card_text('putrefy', 'Destroy target artifact or creature. It can\'t be regenerated.').
card_mana_cost('putrefy', ['1', 'B', 'G']).
card_cmc('putrefy', 3).
card_layout('putrefy', 'normal').

% Found in: FUT
card_name('putrid cyclops', 'Putrid Cyclops').
card_type('putrid cyclops', 'Creature — Zombie Cyclops').
card_types('putrid cyclops', ['Creature']).
card_subtypes('putrid cyclops', ['Zombie', 'Cyclops']).
card_colors('putrid cyclops', ['B']).
card_text('putrid cyclops', 'When Putrid Cyclops enters the battlefield, scry 1, then reveal the top card of your library. Putrid Cyclops gets -X/-X until end of turn, where X is that card\'s converted mana cost. (To scry 1, look at the top card of your library, then you may put that card on the bottom of your library.)').
card_mana_cost('putrid cyclops', ['2', 'B']).
card_cmc('putrid cyclops', 3).
card_layout('putrid cyclops', 'normal').
card_power('putrid cyclops', 3).
card_toughness('putrid cyclops', 3).

% Found in: PD3, TOR, VMA
card_name('putrid imp', 'Putrid Imp').
card_type('putrid imp', 'Creature — Zombie Imp').
card_types('putrid imp', ['Creature']).
card_subtypes('putrid imp', ['Zombie', 'Imp']).
card_colors('putrid imp', ['B']).
card_text('putrid imp', 'Discard a card: Putrid Imp gains flying until end of turn.\nThreshold — As long as seven or more cards are in your graveyard, Putrid Imp gets +1/+1 and can\'t block.').
card_mana_cost('putrid imp', ['B']).
card_cmc('putrid imp', 1).
card_layout('putrid imp', 'normal').
card_power('putrid imp', 1).
card_toughness('putrid imp', 1).

% Found in: ARB, DDJ, DDM
card_name('putrid leech', 'Putrid Leech').
card_type('putrid leech', 'Creature — Zombie Leech').
card_types('putrid leech', ['Creature']).
card_subtypes('putrid leech', ['Zombie', 'Leech']).
card_colors('putrid leech', ['B', 'G']).
card_text('putrid leech', 'Pay 2 life: Putrid Leech gets +2/+2 until end of turn. Activate this ability only once each turn.').
card_mana_cost('putrid leech', ['B', 'G']).
card_cmc('putrid leech', 2).
card_layout('putrid leech', 'normal').
card_power('putrid leech', 2).
card_toughness('putrid leech', 2).

% Found in: SCG
card_name('putrid raptor', 'Putrid Raptor').
card_type('putrid raptor', 'Creature — Zombie Lizard Beast').
card_types('putrid raptor', ['Creature']).
card_subtypes('putrid raptor', ['Zombie', 'Lizard', 'Beast']).
card_colors('putrid raptor', ['B']).
card_text('putrid raptor', 'Morph—Discard a Zombie card. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('putrid raptor', ['4', 'B', 'B']).
card_cmc('putrid raptor', 6).
card_layout('putrid raptor', 'normal').
card_power('putrid raptor', 4).
card_toughness('putrid raptor', 4).

% Found in: APC
card_name('putrid warrior', 'Putrid Warrior').
card_type('putrid warrior', 'Creature — Zombie Soldier Warrior').
card_types('putrid warrior', ['Creature']).
card_subtypes('putrid warrior', ['Zombie', 'Soldier', 'Warrior']).
card_colors('putrid warrior', ['W', 'B']).
card_text('putrid warrior', 'Whenever Putrid Warrior deals damage, choose one —\n• Each player loses 1 life.\n• Each player gains 1 life.').
card_mana_cost('putrid warrior', ['W', 'B']).
card_cmc('putrid warrior', 2).
card_layout('putrid warrior', 'normal').
card_power('putrid warrior', 2).
card_toughness('putrid warrior', 2).

% Found in: ICE
card_name('pygmy allosaurus', 'Pygmy Allosaurus').
card_type('pygmy allosaurus', 'Creature — Lizard').
card_types('pygmy allosaurus', ['Creature']).
card_subtypes('pygmy allosaurus', ['Lizard']).
card_colors('pygmy allosaurus', ['G']).
card_text('pygmy allosaurus', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('pygmy allosaurus', ['2', 'G']).
card_cmc('pygmy allosaurus', 3).
card_layout('pygmy allosaurus', 'normal').
card_power('pygmy allosaurus', 2).
card_toughness('pygmy allosaurus', 2).

% Found in: UNH
card_name('pygmy giant', 'Pygmy Giant').
card_type('pygmy giant', 'Creature — Giant').
card_types('pygmy giant', ['Creature']).
card_subtypes('pygmy giant', ['Giant']).
card_colors('pygmy giant', ['R']).
card_text('pygmy giant', '{R}, {T}, Sacrifice a creature: Pygmy Giant deals X damage to target creature, where X is a number in the sacrificed creature\'s text box.').
card_mana_cost('pygmy giant', ['1', 'R', 'R']).
card_cmc('pygmy giant', 3).
card_layout('pygmy giant', 'normal').
card_power('pygmy giant', 0).
card_toughness('pygmy giant', 2).

% Found in: VIS
card_name('pygmy hippo', 'Pygmy Hippo').
card_type('pygmy hippo', 'Creature — Hippo').
card_types('pygmy hippo', ['Creature']).
card_subtypes('pygmy hippo', ['Hippo']).
card_colors('pygmy hippo', ['U', 'G']).
card_text('pygmy hippo', 'Whenever Pygmy Hippo attacks and isn\'t blocked, you may have defending player activate a mana ability of each land he or she controls and empty his or her mana pool. If you do, Pygmy Hippo assigns no combat damage this turn and at the beginning of your postcombat main phase, you add {X} to your mana pool, where X is the amount of mana emptied from defending player\'s mana pool this way.').
card_mana_cost('pygmy hippo', ['G', 'U']).
card_cmc('pygmy hippo', 2).
card_layout('pygmy hippo', 'normal').
card_power('pygmy hippo', 2).
card_toughness('pygmy hippo', 2).
card_reserved('pygmy hippo').

% Found in: PLS
card_name('pygmy kavu', 'Pygmy Kavu').
card_type('pygmy kavu', 'Creature — Kavu').
card_types('pygmy kavu', ['Creature']).
card_subtypes('pygmy kavu', ['Kavu']).
card_colors('pygmy kavu', ['G']).
card_text('pygmy kavu', 'When Pygmy Kavu enters the battlefield, draw a card for each black creature your opponents control.').
card_mana_cost('pygmy kavu', ['3', 'G']).
card_cmc('pygmy kavu', 4).
card_layout('pygmy kavu', 'normal').
card_power('pygmy kavu', 1).
card_toughness('pygmy kavu', 2).

% Found in: 7ED, DDI, ULG
card_name('pygmy pyrosaur', 'Pygmy Pyrosaur').
card_type('pygmy pyrosaur', 'Creature — Lizard').
card_types('pygmy pyrosaur', ['Creature']).
card_subtypes('pygmy pyrosaur', ['Lizard']).
card_colors('pygmy pyrosaur', ['R']).
card_text('pygmy pyrosaur', 'Pygmy Pyrosaur can\'t block.\n{R}: Pygmy Pyrosaur gets +1/+0 until end of turn.').
card_mana_cost('pygmy pyrosaur', ['1', 'R']).
card_cmc('pygmy pyrosaur', 2).
card_layout('pygmy pyrosaur', 'normal').
card_power('pygmy pyrosaur', 1).
card_toughness('pygmy pyrosaur', 1).

% Found in: PCY
card_name('pygmy razorback', 'Pygmy Razorback').
card_type('pygmy razorback', 'Creature — Boar').
card_types('pygmy razorback', ['Creature']).
card_subtypes('pygmy razorback', ['Boar']).
card_colors('pygmy razorback', ['G']).
card_text('pygmy razorback', 'Trample').
card_mana_cost('pygmy razorback', ['1', 'G']).
card_cmc('pygmy razorback', 2).
card_layout('pygmy razorback', 'normal').
card_power('pygmy razorback', 2).
card_toughness('pygmy razorback', 1).

% Found in: EXO
card_name('pygmy troll', 'Pygmy Troll').
card_type('pygmy troll', 'Creature — Troll').
card_types('pygmy troll', ['Creature']).
card_subtypes('pygmy troll', ['Troll']).
card_colors('pygmy troll', ['G']).
card_text('pygmy troll', 'Whenever Pygmy Troll becomes blocked by a creature, Pygmy Troll gets +1/+1 until end of turn.\n{G}: Regenerate Pygmy Troll.').
card_mana_cost('pygmy troll', ['1', 'G']).
card_cmc('pygmy troll', 2).
card_layout('pygmy troll', 'normal').
card_power('pygmy troll', 1).
card_toughness('pygmy troll', 1).

% Found in: ICE
card_name('pyknite', 'Pyknite').
card_type('pyknite', 'Creature — Ouphe').
card_types('pyknite', ['Creature']).
card_subtypes('pyknite', ['Ouphe']).
card_colors('pyknite', ['G']).
card_text('pyknite', 'When Pyknite enters the battlefield, draw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('pyknite', ['2', 'G']).
card_cmc('pyknite', 3).
card_layout('pyknite', 'normal').
card_power('pyknite', 1).
card_toughness('pyknite', 1).

% Found in: ARN
card_name('pyramids', 'Pyramids').
card_type('pyramids', 'Artifact').
card_types('pyramids', ['Artifact']).
card_subtypes('pyramids', []).
card_colors('pyramids', []).
card_text('pyramids', '{2}: Choose one —\n• Destroy target Aura attached to a land.\n• The next time target land would be destroyed this turn, remove all damage marked on it instead.').
card_mana_cost('pyramids', ['6']).
card_cmc('pyramids', 6).
card_layout('pyramids', 'normal').
card_reserved('pyramids').

% Found in: DD2, DD3_JVC, SHM
card_name('pyre charger', 'Pyre Charger').
card_type('pyre charger', 'Creature — Elemental Warrior').
card_types('pyre charger', ['Creature']).
card_subtypes('pyre charger', ['Elemental', 'Warrior']).
card_colors('pyre charger', ['R']).
card_text('pyre charger', 'Haste\n{R}: Pyre Charger gets +1/+0 until end of turn.').
card_mana_cost('pyre charger', ['R', 'R']).
card_cmc('pyre charger', 2).
card_layout('pyre charger', 'normal').
card_power('pyre charger', 1).
card_toughness('pyre charger', 1).

% Found in: INV
card_name('pyre zombie', 'Pyre Zombie').
card_type('pyre zombie', 'Creature — Zombie').
card_types('pyre zombie', ['Creature']).
card_subtypes('pyre zombie', ['Zombie']).
card_colors('pyre zombie', ['B', 'R']).
card_text('pyre zombie', 'At the beginning of your upkeep, if Pyre Zombie is in your graveyard, you may pay {1}{B}{B}. If you do, return Pyre Zombie to your hand.\n{1}{R}{R}, Sacrifice Pyre Zombie: Pyre Zombie deals 2 damage to target creature or player.').
card_mana_cost('pyre zombie', ['1', 'B', 'R']).
card_cmc('pyre zombie', 3).
card_layout('pyre zombie', 'normal').
card_power('pyre zombie', 2).
card_toughness('pyre zombie', 1).

% Found in: DKA
card_name('pyreheart wolf', 'Pyreheart Wolf').
card_type('pyreheart wolf', 'Creature — Wolf').
card_types('pyreheart wolf', ['Creature']).
card_subtypes('pyreheart wolf', ['Wolf']).
card_colors('pyreheart wolf', ['R']).
card_text('pyreheart wolf', 'Whenever Pyreheart Wolf attacks, creatures you control gain menace until end of turn. (They can\'t be blocked except by two or more creatures.)\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('pyreheart wolf', ['2', 'R']).
card_cmc('pyreheart wolf', 3).
card_layout('pyreheart wolf', 'normal').
card_power('pyreheart wolf', 1).
card_toughness('pyreheart wolf', 1).

% Found in: M11
card_name('pyretic ritual', 'Pyretic Ritual').
card_type('pyretic ritual', 'Instant').
card_types('pyretic ritual', ['Instant']).
card_subtypes('pyretic ritual', []).
card_colors('pyretic ritual', ['R']).
card_text('pyretic ritual', 'Add {R}{R}{R} to your mana pool.').
card_mana_cost('pyretic ritual', ['1', 'R']).
card_cmc('pyretic ritual', 2).
card_layout('pyretic ritual', 'normal').

% Found in: DGM
card_name('pyrewild shaman', 'Pyrewild Shaman').
card_type('pyrewild shaman', 'Creature — Goblin Shaman').
card_types('pyrewild shaman', ['Creature']).
card_subtypes('pyrewild shaman', ['Goblin', 'Shaman']).
card_colors('pyrewild shaman', ['R']).
card_text('pyrewild shaman', 'Bloodrush — {1}{R}, Discard Pyrewild Shaman: Target attacking creature gets +3/+1 until end of turn.\nWhenever one or more creatures you control deal combat damage to a player, if Pyrewild Shaman is in your graveyard, you may pay {3}. If you do, return Pyrewild Shaman to your hand.').
card_mana_cost('pyrewild shaman', ['2', 'R']).
card_cmc('pyrewild shaman', 3).
card_layout('pyrewild shaman', 'normal').
card_power('pyrewild shaman', 3).
card_toughness('pyrewild shaman', 1).

% Found in: MIR
card_name('pyric salamander', 'Pyric Salamander').
card_type('pyric salamander', 'Creature — Salamander').
card_types('pyric salamander', ['Creature']).
card_subtypes('pyric salamander', ['Salamander']).
card_colors('pyric salamander', ['R']).
card_text('pyric salamander', '{R}: Pyric Salamander gets +1/+0 until end of turn. Sacrifice Pyric Salamander at the beginning of the next end step.').
card_mana_cost('pyric salamander', ['1', 'R']).
card_cmc('pyric salamander', 2).
card_layout('pyric salamander', 'normal').
card_power('pyric salamander', 1).
card_toughness('pyric salamander', 1).

% Found in: MMA, MRD
card_name('pyrite spellbomb', 'Pyrite Spellbomb').
card_type('pyrite spellbomb', 'Artifact').
card_types('pyrite spellbomb', ['Artifact']).
card_subtypes('pyrite spellbomb', []).
card_colors('pyrite spellbomb', []).
card_text('pyrite spellbomb', '{R}, Sacrifice Pyrite Spellbomb: Pyrite Spellbomb deals 2 damage to target creature or player.\n{1}, Sacrifice Pyrite Spellbomb: Draw a card.').
card_mana_cost('pyrite spellbomb', ['1']).
card_cmc('pyrite spellbomb', 1).
card_layout('pyrite spellbomb', 'normal').

% Found in: 5ED, ICE, MED
card_name('pyroblast', 'Pyroblast').
card_type('pyroblast', 'Instant').
card_types('pyroblast', ['Instant']).
card_subtypes('pyroblast', []).
card_colors('pyroblast', ['R']).
card_text('pyroblast', 'Choose one —\n• Counter target spell if it\'s blue.\n• Destroy target permanent if it\'s blue.').
card_mana_cost('pyroblast', ['R']).
card_cmc('pyroblast', 1).
card_layout('pyroblast', 'normal').

% Found in: 10E, 7ED, 8ED, 9ED, DDK, DDL, DKM, ICE, M10, M11, POR, pMPR
card_name('pyroclasm', 'Pyroclasm').
card_type('pyroclasm', 'Sorcery').
card_types('pyroclasm', ['Sorcery']).
card_subtypes('pyroclasm', []).
card_colors('pyroclasm', ['R']).
card_text('pyroclasm', 'Pyroclasm deals 2 damage to each creature.').
card_mana_cost('pyroclasm', ['1', 'R']).
card_cmc('pyroclasm', 2).
card_layout('pyroclasm', 'normal').

% Found in: MOR
card_name('pyroclast consul', 'Pyroclast Consul').
card_type('pyroclast consul', 'Creature — Elemental Shaman').
card_types('pyroclast consul', ['Creature']).
card_subtypes('pyroclast consul', ['Elemental', 'Shaman']).
card_colors('pyroclast consul', ['R']).
card_text('pyroclast consul', 'Kinship — At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Pyroclast Consul, you may reveal it. If you do, Pyroclast Consul deals 2 damage to each creature.').
card_mana_cost('pyroclast consul', ['3', 'R', 'R']).
card_cmc('pyroclast consul', 5).
card_layout('pyroclast consul', 'normal').
card_power('pyroclast consul', 3).
card_toughness('pyroclast consul', 3).

% Found in: RTR
card_name('pyroconvergence', 'Pyroconvergence').
card_type('pyroconvergence', 'Enchantment').
card_types('pyroconvergence', ['Enchantment']).
card_subtypes('pyroconvergence', []).
card_colors('pyroconvergence', ['R']).
card_text('pyroconvergence', 'Whenever you cast a multicolored spell, Pyroconvergence deals 2 damage to target creature or player.').
card_mana_cost('pyroconvergence', ['4', 'R']).
card_cmc('pyroconvergence', 5).
card_layout('pyroconvergence', 'normal').

% Found in: CMD, PLC
card_name('pyrohemia', 'Pyrohemia').
card_type('pyrohemia', 'Enchantment').
card_types('pyrohemia', ['Enchantment']).
card_subtypes('pyrohemia', []).
card_colors('pyrohemia', ['R']).
card_text('pyrohemia', 'At the beginning of the end step, if no creatures are on the battlefield, sacrifice Pyrohemia.\n{R}: Pyrohemia deals 1 damage to each creature and each player.').
card_mana_cost('pyrohemia', ['2', 'R', 'R']).
card_cmc('pyrohemia', 4).
card_layout('pyrohemia', 'normal').

% Found in: ALL, ATH, DDL, ME2
card_name('pyrokinesis', 'Pyrokinesis').
card_type('pyrokinesis', 'Instant').
card_types('pyrokinesis', ['Instant']).
card_subtypes('pyrokinesis', []).
card_colors('pyrokinesis', ['R']).
card_text('pyrokinesis', 'You may exile a red card from your hand rather than pay Pyrokinesis\'s mana cost.\nPyrokinesis deals 4 damage divided as you choose among any number of target creatures.').
card_mana_cost('pyrokinesis', ['4', 'R', 'R']).
card_cmc('pyrokinesis', 6).
card_layout('pyrokinesis', 'normal').

% Found in: ZEN
card_name('pyromancer ascension', 'Pyromancer Ascension').
card_type('pyromancer ascension', 'Enchantment').
card_types('pyromancer ascension', ['Enchantment']).
card_subtypes('pyromancer ascension', []).
card_colors('pyromancer ascension', ['R']).
card_text('pyromancer ascension', 'Whenever you cast an instant or sorcery spell that has the same name as a card in your graveyard, you may put a quest counter on Pyromancer Ascension.\nWhenever you cast an instant or sorcery spell while Pyromancer Ascension has two or more quest counters on it, you may copy that spell. You may choose new targets for the copy.').
card_mana_cost('pyromancer ascension', ['1', 'R']).
card_cmc('pyromancer ascension', 2).
card_layout('pyromancer ascension', 'normal').

% Found in: M14
card_name('pyromancer\'s gauntlet', 'Pyromancer\'s Gauntlet').
card_type('pyromancer\'s gauntlet', 'Artifact').
card_types('pyromancer\'s gauntlet', ['Artifact']).
card_subtypes('pyromancer\'s gauntlet', []).
card_colors('pyromancer\'s gauntlet', []).
card_text('pyromancer\'s gauntlet', 'If a red instant or sorcery spell you control or a red planeswalker you control would deal damage to a permanent or player, it deals that much damage plus 2 to that permanent or player instead.').
card_mana_cost('pyromancer\'s gauntlet', ['5']).
card_cmc('pyromancer\'s gauntlet', 5).
card_layout('pyromancer\'s gauntlet', 'normal').

% Found in: ORI
card_name('pyromancer\'s goggles', 'Pyromancer\'s Goggles').
card_type('pyromancer\'s goggles', 'Legendary Artifact').
card_types('pyromancer\'s goggles', ['Artifact']).
card_subtypes('pyromancer\'s goggles', []).
card_supertypes('pyromancer\'s goggles', ['Legendary']).
card_colors('pyromancer\'s goggles', []).
card_text('pyromancer\'s goggles', '{T}: Add {R} to your mana pool. When that mana is spent to cast a red instant or sorcery spell, copy that spell and you may choose new targets for the copy.').
card_mana_cost('pyromancer\'s goggles', ['5']).
card_cmc('pyromancer\'s goggles', 5).
card_layout('pyromancer\'s goggles', 'normal').

% Found in: FUT, MMA
card_name('pyromancer\'s swath', 'Pyromancer\'s Swath').
card_type('pyromancer\'s swath', 'Enchantment').
card_types('pyromancer\'s swath', ['Enchantment']).
card_subtypes('pyromancer\'s swath', []).
card_colors('pyromancer\'s swath', ['R']).
card_text('pyromancer\'s swath', 'If an instant or sorcery source you control would deal damage to a creature or player, it deals that much damage plus 2 to that creature or player instead.\nAt the beginning of each end step, discard your hand.').
card_mana_cost('pyromancer\'s swath', ['2', 'R']).
card_cmc('pyromancer\'s swath', 3).
card_layout('pyromancer\'s swath', 'normal').

% Found in: ULG
card_name('pyromancy', 'Pyromancy').
card_type('pyromancy', 'Enchantment').
card_types('pyromancy', ['Enchantment']).
card_subtypes('pyromancy', []).
card_colors('pyromancy', ['R']).
card_text('pyromancy', '{3}, Discard a card at random: Pyromancy deals damage to target creature or player equal to the converted mana cost of the discarded card.').
card_mana_cost('pyromancy', ['2', 'R', 'R']).
card_cmc('pyromancy', 4).
card_layout('pyromancy', 'normal').

% Found in: TOR
card_name('pyromania', 'Pyromania').
card_type('pyromania', 'Enchantment').
card_types('pyromania', ['Enchantment']).
card_subtypes('pyromania', []).
card_colors('pyromania', ['R']).
card_text('pyromania', '{1}{R}, Discard a card at random: Pyromania deals 1 damage to target creature or player.\n{1}{R}, Sacrifice Pyromania: Pyromania deals 1 damage to target creature or player.').
card_mana_cost('pyromania', ['2', 'R']).
card_cmc('pyromania', 3).
card_layout('pyromania', 'normal').

% Found in: DDJ, GPT
card_name('pyromatics', 'Pyromatics').
card_type('pyromatics', 'Instant').
card_types('pyromatics', ['Instant']).
card_subtypes('pyromatics', []).
card_colors('pyromatics', ['R']).
card_text('pyromatics', 'Replicate {1}{R} (When you cast this spell, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)\nPyromatics deals 1 damage to target creature or player.').
card_mana_cost('pyromatics', ['1', 'R']).
card_cmc('pyromatics', 2).
card_layout('pyromatics', 'normal').

% Found in: SCG
card_name('pyrostatic pillar', 'Pyrostatic Pillar').
card_type('pyrostatic pillar', 'Enchantment').
card_types('pyrostatic pillar', ['Enchantment']).
card_subtypes('pyrostatic pillar', []).
card_colors('pyrostatic pillar', ['R']).
card_text('pyrostatic pillar', 'Whenever a player casts a spell with converted mana cost 3 or less, Pyrostatic Pillar deals 2 damage to that player.').
card_mana_cost('pyrostatic pillar', ['1', 'R']).
card_cmc('pyrostatic pillar', 2).
card_layout('pyrostatic pillar', 'normal').

% Found in: 4ED, 5ED, 6ED, 7ED, 8ED, ATH, FRF, HOP, ITP, LEG, RQS
card_name('pyrotechnics', 'Pyrotechnics').
card_type('pyrotechnics', 'Sorcery').
card_types('pyrotechnics', ['Sorcery']).
card_subtypes('pyrotechnics', []).
card_colors('pyrotechnics', ['R']).
card_text('pyrotechnics', 'Pyrotechnics deals 4 damage divided as you choose among any number of target creatures and/or players.').
card_mana_cost('pyrotechnics', ['4', 'R']).
card_cmc('pyrotechnics', 5).
card_layout('pyrotechnics', 'normal').

% Found in: EVE
card_name('pyrrhic revival', 'Pyrrhic Revival').
card_type('pyrrhic revival', 'Sorcery').
card_types('pyrrhic revival', ['Sorcery']).
card_subtypes('pyrrhic revival', []).
card_colors('pyrrhic revival', ['W', 'B']).
card_text('pyrrhic revival', 'Each player returns each creature card from his or her graveyard to the battlefield with an additional -1/-1 counter on it.').
card_mana_cost('pyrrhic revival', ['3', 'W/B', 'W/B', 'W/B']).
card_cmc('pyrrhic revival', 6).
card_layout('pyrrhic revival', 'normal').

% Found in: 6ED, POR, S00, VIS
card_name('python', 'Python').
card_type('python', 'Creature — Snake').
card_types('python', ['Creature']).
card_subtypes('python', ['Snake']).
card_colors('python', ['B']).
card_text('python', '').
card_mana_cost('python', ['1', 'B', 'B']).
card_cmc('python', 3).
card_layout('python', 'normal').
card_power('python', 3).
card_toughness('python', 2).

% Found in: THS
card_name('pyxis of pandemonium', 'Pyxis of Pandemonium').
card_type('pyxis of pandemonium', 'Artifact').
card_types('pyxis of pandemonium', ['Artifact']).
card_subtypes('pyxis of pandemonium', []).
card_colors('pyxis of pandemonium', []).
card_text('pyxis of pandemonium', '{T}: Each player exiles the top card of his or her library face down.\n{7}, {T}, Sacrifice Pyxis of Pandemonium: Each player turns face up all cards he or she owns exiled with Pyxis of Pandemonium, then puts all permanent cards among them onto the battlefield.').
card_mana_cost('pyxis of pandemonium', ['1']).
card_cmc('pyxis of pandemonium', 1).
card_layout('pyxis of pandemonium', 'normal').

