% Card-specific data

% Found in: UNH
card_name('r&d\'s secret lair', 'R&D\'s Secret Lair').
card_type('r&d\'s secret lair', 'Legendary Land').
card_types('r&d\'s secret lair', ['Land']).
card_subtypes('r&d\'s secret lair', []).
card_supertypes('r&d\'s secret lair', ['Legendary']).
card_colors('r&d\'s secret lair', []).
card_text('r&d\'s secret lair', 'Play cards as written. Ignore all errata.\n{T}: Add {1} to your mana pool.').
card_layout('r&d\'s secret lair', 'normal').

% Found in: GPT
card_name('rabble-rouser', 'Rabble-Rouser').
card_type('rabble-rouser', 'Creature — Goblin Shaman').
card_types('rabble-rouser', ['Creature']).
card_subtypes('rabble-rouser', ['Goblin', 'Shaman']).
card_colors('rabble-rouser', ['R']).
card_text('rabble-rouser', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)\n{R}, {T}: Attacking creatures get +X/+0 until end of turn, where X is Rabble-Rouser\'s power.').
card_mana_cost('rabble-rouser', ['3', 'R']).
card_cmc('rabble-rouser', 4).
card_layout('rabble-rouser', 'normal').
card_power('rabble-rouser', 1).
card_toughness('rabble-rouser', 1).

% Found in: ORI
card_name('rabid bloodsucker', 'Rabid Bloodsucker').
card_type('rabid bloodsucker', 'Creature — Vampire').
card_types('rabid bloodsucker', ['Creature']).
card_subtypes('rabid bloodsucker', ['Vampire']).
card_colors('rabid bloodsucker', ['B']).
card_text('rabid bloodsucker', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhen Rabid Bloodsucker enters the battlefield, each player loses 2 life.').
card_mana_cost('rabid bloodsucker', ['4', 'B']).
card_cmc('rabid bloodsucker', 5).
card_layout('rabid bloodsucker', 'normal').
card_power('rabid bloodsucker', 3).
card_toughness('rabid bloodsucker', 2).

% Found in: ODY
card_name('rabid elephant', 'Rabid Elephant').
card_type('rabid elephant', 'Creature — Elephant').
card_types('rabid elephant', ['Creature']).
card_subtypes('rabid elephant', ['Elephant']).
card_colors('rabid elephant', ['G']).
card_text('rabid elephant', 'Whenever Rabid Elephant becomes blocked, it gets +2/+2 until end of turn for each creature blocking it.').
card_mana_cost('rabid elephant', ['4', 'G']).
card_cmc('rabid elephant', 5).
card_layout('rabid elephant', 'normal').
card_power('rabid elephant', 3).
card_toughness('rabid elephant', 4).

% Found in: STH
card_name('rabid rats', 'Rabid Rats').
card_type('rabid rats', 'Creature — Rat').
card_types('rabid rats', ['Creature']).
card_subtypes('rabid rats', ['Rat']).
card_colors('rabid rats', ['B']).
card_text('rabid rats', '{T}: Target blocking creature gets -1/-1 until end of turn.').
card_mana_cost('rabid rats', ['1', 'B']).
card_cmc('rabid rats', 2).
card_layout('rabid rats', 'normal').
card_power('rabid rats', 1).
card_toughness('rabid rats', 1).

% Found in: EXO
card_name('rabid wolverines', 'Rabid Wolverines').
card_type('rabid wolverines', 'Creature — Wolverine').
card_types('rabid wolverines', ['Creature']).
card_subtypes('rabid wolverines', ['Wolverine']).
card_colors('rabid wolverines', ['G']).
card_text('rabid wolverines', 'Whenever Rabid Wolverines becomes blocked by a creature, Rabid Wolverines gets +1/+1 until end of turn.').
card_mana_cost('rabid wolverines', ['3', 'G', 'G']).
card_cmc('rabid wolverines', 5).
card_layout('rabid wolverines', 'normal').
card_power('rabid wolverines', 4).
card_toughness('rabid wolverines', 4).

% Found in: 5ED, CHR, LEG, MED
card_name('rabid wombat', 'Rabid Wombat').
card_type('rabid wombat', 'Creature — Wombat').
card_types('rabid wombat', ['Creature']).
card_subtypes('rabid wombat', ['Wombat']).
card_colors('rabid wombat', ['G']).
card_text('rabid wombat', 'Vigilance\nRabid Wombat gets +2/+2 for each Aura attached to it.').
card_mana_cost('rabid wombat', ['2', 'G', 'G']).
card_cmc('rabid wombat', 4).
card_layout('rabid wombat', 'normal').
card_power('rabid wombat', 0).
card_toughness('rabid wombat', 1).

% Found in: RTR
card_name('racecourse fury', 'Racecourse Fury').
card_type('racecourse fury', 'Enchantment — Aura').
card_types('racecourse fury', ['Enchantment']).
card_subtypes('racecourse fury', ['Aura']).
card_colors('racecourse fury', ['R']).
card_text('racecourse fury', 'Enchant land\nEnchanted land has \"{T}: Target creature gains haste until end of turn.\"').
card_mana_cost('racecourse fury', ['R']).
card_cmc('racecourse fury', 1).
card_layout('racecourse fury', 'normal').

% Found in: ULG
card_name('rack and ruin', 'Rack and Ruin').
card_type('rack and ruin', 'Instant').
card_types('rack and ruin', ['Instant']).
card_subtypes('rack and ruin', []).
card_colors('rack and ruin', ['R']).
card_text('rack and ruin', 'Destroy two target artifacts.').
card_mana_cost('rack and ruin', ['2', 'R']).
card_cmc('rack and ruin', 3).
card_layout('rack and ruin', 'normal').

% Found in: NMS
card_name('rackling', 'Rackling').
card_type('rackling', 'Artifact Creature — Construct').
card_types('rackling', ['Artifact', 'Creature']).
card_subtypes('rackling', ['Construct']).
card_colors('rackling', []).
card_text('rackling', 'At the beginning of each opponent\'s upkeep, Rackling deals X damage to that player, where X is 3 minus the number of cards in his or her hand.').
card_mana_cost('rackling', ['4']).
card_cmc('rackling', 4).
card_layout('rackling', 'normal').
card_power('rackling', 2).
card_toughness('rackling', 2).

% Found in: PLC
card_name('radha, heir to keld', 'Radha, Heir to Keld').
card_type('radha, heir to keld', 'Legendary Creature — Elf Warrior').
card_types('radha, heir to keld', ['Creature']).
card_subtypes('radha, heir to keld', ['Elf', 'Warrior']).
card_supertypes('radha, heir to keld', ['Legendary']).
card_colors('radha, heir to keld', ['R', 'G']).
card_text('radha, heir to keld', 'Whenever Radha, Heir to Keld attacks, you may add {R}{R} to your mana pool.\n{T}: Add {G} to your mana pool.').
card_mana_cost('radha, heir to keld', ['R', 'G']).
card_cmc('radha, heir to keld', 2).
card_layout('radha, heir to keld', 'normal').
card_power('radha, heir to keld', 2).
card_toughness('radha, heir to keld', 2).

% Found in: MIR
card_name('radiant essence', 'Radiant Essence').
card_type('radiant essence', 'Creature — Spirit').
card_types('radiant essence', ['Creature']).
card_subtypes('radiant essence', ['Spirit']).
card_colors('radiant essence', ['W', 'G']).
card_text('radiant essence', 'Radiant Essence gets +1/+2 as long as an opponent controls a black permanent.').
card_mana_cost('radiant essence', ['1', 'G', 'W']).
card_cmc('radiant essence', 3).
card_layout('radiant essence', 'normal').
card_power('radiant essence', 2).
card_toughness('radiant essence', 3).

% Found in: BFZ
card_name('radiant flames', 'Radiant Flames').
card_type('radiant flames', 'Sorcery').
card_types('radiant flames', ['Sorcery']).
card_subtypes('radiant flames', []).
card_colors('radiant flames', ['R']).
card_text('radiant flames', 'Converge — Radiant Flames deals X damage to each creature, where X is the number of colors of mana spent to cast Radiant Flames.').
card_mana_cost('radiant flames', ['2', 'R']).
card_cmc('radiant flames', 3).
card_layout('radiant flames', 'normal').

% Found in: M15
card_name('radiant fountain', 'Radiant Fountain').
card_type('radiant fountain', 'Land').
card_types('radiant fountain', ['Land']).
card_subtypes('radiant fountain', []).
card_colors('radiant fountain', []).
card_text('radiant fountain', 'When Radiant Fountain enters the battlefield, you gain 2 life.\n{T}: Add {1} to your mana pool.').
card_layout('radiant fountain', 'normal').

% Found in: PLS
card_name('radiant kavu', 'Radiant Kavu').
card_type('radiant kavu', 'Creature — Kavu').
card_types('radiant kavu', ['Creature']).
card_subtypes('radiant kavu', ['Kavu']).
card_colors('radiant kavu', ['W', 'R', 'G']).
card_text('radiant kavu', '{R}{G}{W}: Prevent all combat damage blue creatures and black creatures would deal this turn.').
card_mana_cost('radiant kavu', ['R', 'G', 'W']).
card_cmc('radiant kavu', 3).
card_layout('radiant kavu', 'normal').
card_power('radiant kavu', 3).
card_toughness('radiant kavu', 3).

% Found in: DTK
card_name('radiant purge', 'Radiant Purge').
card_type('radiant purge', 'Instant').
card_types('radiant purge', ['Instant']).
card_subtypes('radiant purge', []).
card_colors('radiant purge', ['W']).
card_text('radiant purge', 'Exile target multicolored creature or multicolored enchantment.').
card_mana_cost('radiant purge', ['1', 'W']).
card_cmc('radiant purge', 2).
card_layout('radiant purge', 'normal').

% Found in: ULG
card_name('radiant\'s dragoons', 'Radiant\'s Dragoons').
card_type('radiant\'s dragoons', 'Creature — Human Soldier').
card_types('radiant\'s dragoons', ['Creature']).
card_subtypes('radiant\'s dragoons', ['Human', 'Soldier']).
card_colors('radiant\'s dragoons', ['W']).
card_text('radiant\'s dragoons', 'Echo {3}{W} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Radiant\'s Dragoons enters the battlefield, you gain 5 life.').
card_mana_cost('radiant\'s dragoons', ['3', 'W']).
card_cmc('radiant\'s dragoons', 4).
card_layout('radiant\'s dragoons', 'normal').
card_power('radiant\'s dragoons', 2).
card_toughness('radiant\'s dragoons', 5).

% Found in: ULG, VMA
card_name('radiant\'s judgment', 'Radiant\'s Judgment').
card_type('radiant\'s judgment', 'Instant').
card_types('radiant\'s judgment', ['Instant']).
card_subtypes('radiant\'s judgment', []).
card_colors('radiant\'s judgment', ['W']).
card_text('radiant\'s judgment', 'Destroy target creature with power 4 or greater.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('radiant\'s judgment', ['2', 'W']).
card_cmc('radiant\'s judgment', 3).
card_layout('radiant\'s judgment', 'normal').

% Found in: ULG, VMA
card_name('radiant, archangel', 'Radiant, Archangel').
card_type('radiant, archangel', 'Legendary Creature — Angel').
card_types('radiant, archangel', ['Creature']).
card_subtypes('radiant, archangel', ['Angel']).
card_supertypes('radiant, archangel', ['Legendary']).
card_colors('radiant, archangel', ['W']).
card_text('radiant, archangel', 'Flying, vigilance\nRadiant, Archangel gets +1/+1 for each other creature with flying on the battlefield.').
card_mana_cost('radiant, archangel', ['3', 'W', 'W']).
card_cmc('radiant, archangel', 5).
card_layout('radiant, archangel', 'normal').
card_power('radiant, archangel', 3).
card_toughness('radiant, archangel', 3).
card_reserved('radiant, archangel').

% Found in: TOR
card_name('radiate', 'Radiate').
card_type('radiate', 'Instant').
card_types('radiate', ['Instant']).
card_subtypes('radiate', []).
card_colors('radiate', ['R']).
card_text('radiate', 'Choose target instant or sorcery spell that targets only a single permanent or player. Copy that spell for each other permanent or player the spell could target. Each copy targets a different one of those permanents and players.').
card_mana_cost('radiate', ['3', 'R', 'R']).
card_cmc('radiate', 5).
card_layout('radiate', 'normal').

% Found in: 4ED, 5ED, 6ED, LEG, ME4
card_name('radjan spirit', 'Radjan Spirit').
card_type('radjan spirit', 'Creature — Spirit').
card_types('radjan spirit', ['Creature']).
card_subtypes('radjan spirit', ['Spirit']).
card_colors('radjan spirit', ['G']).
card_text('radjan spirit', '{T}: Target creature loses flying until end of turn.').
card_mana_cost('radjan spirit', ['3', 'G']).
card_cmc('radjan spirit', 4).
card_layout('radjan spirit', 'normal').
card_power('radjan spirit', 3).
card_toughness('radjan spirit', 2).

% Found in: ALA, V11
card_name('rafiq of the many', 'Rafiq of the Many').
card_type('rafiq of the many', 'Legendary Creature — Human Knight').
card_types('rafiq of the many', ['Creature']).
card_subtypes('rafiq of the many', ['Human', 'Knight']).
card_supertypes('rafiq of the many', ['Legendary']).
card_colors('rafiq of the many', ['W', 'U', 'G']).
card_text('rafiq of the many', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever a creature you control attacks alone, it gains double strike until end of turn.').
card_mana_cost('rafiq of the many', ['1', 'G', 'W', 'U']).
card_cmc('rafiq of the many', 4).
card_layout('rafiq of the many', 'normal').
card_power('rafiq of the many', 3).
card_toughness('rafiq of the many', 3).

% Found in: CHK
card_name('rag dealer', 'Rag Dealer').
card_type('rag dealer', 'Creature — Human Rogue').
card_types('rag dealer', ['Creature']).
card_subtypes('rag dealer', ['Human', 'Rogue']).
card_colors('rag dealer', ['B']).
card_text('rag dealer', '{2}{B}, {T}: Exile up to three target cards from a single graveyard.').
card_mana_cost('rag dealer', ['B']).
card_cmc('rag dealer', 1).
card_layout('rag dealer', 'normal').
card_power('rag dealer', 1).
card_toughness('rag dealer', 1).

% Found in: 4ED, 5ED, 6ED, 7ED, DRK
card_name('rag man', 'Rag Man').
card_type('rag man', 'Creature — Human Minion').
card_types('rag man', ['Creature']).
card_subtypes('rag man', ['Human', 'Minion']).
card_colors('rag man', ['B']).
card_text('rag man', '{B}{B}{B}, {T}: Target opponent reveals his or her hand and discards a creature card at random. Activate this ability only during your turn.').
card_mana_cost('rag man', ['2', 'B', 'B']).
card_cmc('rag man', 4).
card_layout('rag man', 'normal').
card_power('rag man', 2).
card_toughness('rag man', 1).

% Found in: DIS
card_name('ragamuffyn', 'Ragamuffyn').
card_type('ragamuffyn', 'Creature — Zombie Cleric').
card_types('ragamuffyn', ['Creature']).
card_subtypes('ragamuffyn', ['Zombie', 'Cleric']).
card_colors('ragamuffyn', ['B']).
card_text('ragamuffyn', 'Hellbent — {T}, Sacrifice a creature or land: Draw a card. Activate this ability only if you have no cards in hand.').
card_mana_cost('ragamuffyn', ['2', 'B']).
card_cmc('ragamuffyn', 3).
card_layout('ragamuffyn', 'normal').
card_power('ragamuffyn', 2).
card_toughness('ragamuffyn', 2).

% Found in: NPH
card_name('rage extractor', 'Rage Extractor').
card_type('rage extractor', 'Artifact').
card_types('rage extractor', ['Artifact']).
card_subtypes('rage extractor', []).
card_colors('rage extractor', ['R']).
card_text('rage extractor', '({R/P} can be paid with either {R} or 2 life.)\nWhenever you cast a spell with {P} in its mana cost, Rage Extractor deals damage equal to that spell\'s converted mana cost to target creature or player.').
card_mana_cost('rage extractor', ['4', 'R/P']).
card_cmc('rage extractor', 5).
card_layout('rage extractor', 'normal').

% Found in: MOR
card_name('rage forger', 'Rage Forger').
card_type('rage forger', 'Creature — Elemental Shaman').
card_types('rage forger', ['Creature']).
card_subtypes('rage forger', ['Elemental', 'Shaman']).
card_colors('rage forger', ['R']).
card_text('rage forger', 'When Rage Forger enters the battlefield, put a +1/+1 counter on each other Shaman creature you control.\nWhenever a creature you control with a +1/+1 counter on it attacks, you may have that creature deal 1 damage to target player.').
card_mana_cost('rage forger', ['2', 'R']).
card_cmc('rage forger', 3).
card_layout('rage forger', 'normal').
card_power('rage forger', 2).
card_toughness('rage forger', 2).

% Found in: ROE
card_name('rage nimbus', 'Rage Nimbus').
card_type('rage nimbus', 'Creature — Elemental').
card_types('rage nimbus', ['Creature']).
card_subtypes('rage nimbus', ['Elemental']).
card_colors('rage nimbus', ['R']).
card_text('rage nimbus', 'Defender, flying\n{1}{R}: Target creature attacks this turn if able.').
card_mana_cost('rage nimbus', ['2', 'R']).
card_cmc('rage nimbus', 3).
card_layout('rage nimbus', 'normal').
card_power('rage nimbus', 5).
card_toughness('rage nimbus', 3).

% Found in: THS
card_name('rage of purphoros', 'Rage of Purphoros').
card_type('rage of purphoros', 'Sorcery').
card_types('rage of purphoros', ['Sorcery']).
card_subtypes('rage of purphoros', []).
card_colors('rage of purphoros', ['R']).
card_text('rage of purphoros', 'Rage of Purphoros deals 4 damage to target creature. It can\'t be regenerated this turn. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('rage of purphoros', ['4', 'R']).
card_cmc('rage of purphoros', 5).
card_layout('rage of purphoros', 'normal').

% Found in: DPA, SHM
card_name('rage reflection', 'Rage Reflection').
card_type('rage reflection', 'Enchantment').
card_types('rage reflection', ['Enchantment']).
card_subtypes('rage reflection', []).
card_colors('rage reflection', ['R']).
card_text('rage reflection', 'Creatures you control have double strike.').
card_mana_cost('rage reflection', ['4', 'R', 'R']).
card_cmc('rage reflection', 6).
card_layout('rage reflection', 'normal').

% Found in: ISD
card_name('rage thrower', 'Rage Thrower').
card_type('rage thrower', 'Creature — Human Shaman').
card_types('rage thrower', ['Creature']).
card_subtypes('rage thrower', ['Human', 'Shaman']).
card_colors('rage thrower', ['R']).
card_text('rage thrower', 'Whenever another creature dies, Rage Thrower deals 2 damage to target player.').
card_mana_cost('rage thrower', ['5', 'R']).
card_cmc('rage thrower', 6).
card_layout('rage thrower', 'normal').
card_power('rage thrower', 4).
card_toughness('rage thrower', 2).

% Found in: 10E, INV
card_name('rage weaver', 'Rage Weaver').
card_type('rage weaver', 'Creature — Human Wizard').
card_types('rage weaver', ['Creature']).
card_subtypes('rage weaver', ['Human', 'Wizard']).
card_colors('rage weaver', ['R']).
card_text('rage weaver', '{2}: Target black or green creature gains haste until end of turn. (It can attack and {T} this turn.)').
card_mana_cost('rage weaver', ['1', 'R']).
card_cmc('rage weaver', 2).
card_layout('rage weaver', 'normal').
card_power('rage weaver', 2).
card_toughness('rage weaver', 1).

% Found in: THS
card_name('rageblood shaman', 'Rageblood Shaman').
card_type('rageblood shaman', 'Creature — Minotaur Shaman').
card_types('rageblood shaman', ['Creature']).
card_subtypes('rageblood shaman', ['Minotaur', 'Shaman']).
card_colors('rageblood shaman', ['R']).
card_text('rageblood shaman', 'Trample\nOther Minotaur creatures you control get +1/+1 and have trample.').
card_mana_cost('rageblood shaman', ['1', 'R', 'R']).
card_cmc('rageblood shaman', 3).
card_layout('rageblood shaman', 'normal').
card_power('rageblood shaman', 2).
card_toughness('rageblood shaman', 3).

% Found in: FRF
card_name('rageform', 'Rageform').
card_type('rageform', 'Enchantment').
card_types('rageform', ['Enchantment']).
card_subtypes('rageform', []).
card_colors('rageform', ['R']).
card_text('rageform', 'When Rageform enters the battlefield, it becomes an Aura with enchant creature. Manifest the top card of your library and attach Rageform to it. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)\nEnchanted creature has double strike. (It deals both first-strike and regular combat damage.)').
card_mana_cost('rageform', ['2', 'R', 'R']).
card_cmc('rageform', 4).
card_layout('rageform', 'normal').

% Found in: BNG
card_name('ragemonger', 'Ragemonger').
card_type('ragemonger', 'Creature — Minotaur Shaman').
card_types('ragemonger', ['Creature']).
card_subtypes('ragemonger', ['Minotaur', 'Shaman']).
card_colors('ragemonger', ['B', 'R']).
card_text('ragemonger', 'Minotaur spells you cast cost {B}{R} less to cast. This effect reduces only the amount of colored mana you pay. (For example, if you cast a Minotaur spell with mana cost {2}{R}, it costs {2} to cast.)').
card_mana_cost('ragemonger', ['1', 'B', 'R']).
card_cmc('ragemonger', 3).
card_layout('ragemonger', 'normal').
card_power('ragemonger', 2).
card_toughness('ragemonger', 3).

% Found in: CHK
card_name('ragged veins', 'Ragged Veins').
card_type('ragged veins', 'Enchantment — Aura').
card_types('ragged veins', ['Enchantment']).
card_subtypes('ragged veins', ['Aura']).
card_colors('ragged veins', ['B']).
card_text('ragged veins', 'Flash\nEnchant creature\nWhenever enchanted creature is dealt damage, its controller loses that much life.').
card_mana_cost('ragged veins', ['1', 'B']).
card_cmc('ragged veins', 2).
card_layout('ragged veins', 'normal').

% Found in: LEG
card_name('raging bull', 'Raging Bull').
card_type('raging bull', 'Creature — Ox').
card_types('raging bull', ['Creature']).
card_subtypes('raging bull', ['Ox']).
card_colors('raging bull', ['R']).
card_text('raging bull', '').
card_mana_cost('raging bull', ['2', 'R']).
card_cmc('raging bull', 3).
card_layout('raging bull', 'normal').
card_power('raging bull', 2).
card_toughness('raging bull', 2).

% Found in: POR
card_name('raging cougar', 'Raging Cougar').
card_type('raging cougar', 'Creature — Cat').
card_types('raging cougar', ['Creature']).
card_subtypes('raging cougar', ['Cat']).
card_colors('raging cougar', ['R']).
card_text('raging cougar', 'Haste').
card_mana_cost('raging cougar', ['2', 'R']).
card_cmc('raging cougar', 3).
card_layout('raging cougar', 'normal').
card_power('raging cougar', 2).
card_toughness('raging cougar', 2).

% Found in: 10E, 6ED, 7ED, 8ED, 9ED, ATH, BRB, BTD, DD3_EVG, EVG, EXO, M10, PO2, POR, S99
card_name('raging goblin', 'Raging Goblin').
card_type('raging goblin', 'Creature — Goblin Berserker').
card_types('raging goblin', ['Creature']).
card_subtypes('raging goblin', ['Goblin', 'Berserker']).
card_colors('raging goblin', ['R']).
card_text('raging goblin', 'Haste (This creature can attack and {T} as soon as it comes under your control.)').
card_mana_cost('raging goblin', ['R']).
card_cmc('raging goblin', 1).
card_layout('raging goblin', 'normal').
card_power('raging goblin', 1).
card_toughness('raging goblin', 1).

% Found in: VIS
card_name('raging gorilla', 'Raging Gorilla').
card_type('raging gorilla', 'Creature — Ape').
card_types('raging gorilla', ['Creature']).
card_subtypes('raging gorilla', ['Ape']).
card_colors('raging gorilla', ['R']).
card_text('raging gorilla', 'Whenever Raging Gorilla blocks or becomes blocked, it gets +2/-2 until end of turn.').
card_mana_cost('raging gorilla', ['2', 'R']).
card_cmc('raging gorilla', 3).
card_layout('raging gorilla', 'normal').
card_power('raging gorilla', 2).
card_toughness('raging gorilla', 3).

% Found in: INV, pPRE
card_name('raging kavu', 'Raging Kavu').
card_type('raging kavu', 'Creature — Kavu').
card_types('raging kavu', ['Creature']).
card_subtypes('raging kavu', ['Kavu']).
card_colors('raging kavu', ['R', 'G']).
card_text('raging kavu', 'Flash\nHaste').
card_mana_cost('raging kavu', ['1', 'R', 'G']).
card_cmc('raging kavu', 3).
card_layout('raging kavu', 'normal').
card_power('raging kavu', 3).
card_toughness('raging kavu', 1).

% Found in: ME3, POR
card_name('raging minotaur', 'Raging Minotaur').
card_type('raging minotaur', 'Creature — Minotaur Berserker').
card_types('raging minotaur', ['Creature']).
card_subtypes('raging minotaur', ['Minotaur', 'Berserker']).
card_colors('raging minotaur', ['R']).
card_text('raging minotaur', 'Haste').
card_mana_cost('raging minotaur', ['2', 'R', 'R']).
card_cmc('raging minotaur', 4).
card_layout('raging minotaur', 'normal').
card_power('raging minotaur', 3).
card_toughness('raging minotaur', 3).

% Found in: AVR
card_name('raging poltergeist', 'Raging Poltergeist').
card_type('raging poltergeist', 'Creature — Spirit').
card_types('raging poltergeist', ['Creature']).
card_subtypes('raging poltergeist', ['Spirit']).
card_colors('raging poltergeist', ['R']).
card_text('raging poltergeist', '').
card_mana_cost('raging poltergeist', ['4', 'R']).
card_cmc('raging poltergeist', 5).
card_layout('raging poltergeist', 'normal').
card_power('raging poltergeist', 6).
card_toughness('raging poltergeist', 1).

% Found in: WWK
card_name('raging ravine', 'Raging Ravine').
card_type('raging ravine', 'Land').
card_types('raging ravine', ['Land']).
card_subtypes('raging ravine', []).
card_colors('raging ravine', []).
card_text('raging ravine', 'Raging Ravine enters the battlefield tapped.\n{T}: Add {R} or {G} to your mana pool.\n{2}{R}{G}: Until end of turn, Raging Ravine becomes a 3/3 red and green Elemental creature with \"Whenever this creature attacks, put a +1/+1 counter on it.\" It\'s still a land.').
card_layout('raging ravine', 'normal').

% Found in: 2ED, CED, CEI, LEA, LEB
card_name('raging river', 'Raging River').
card_type('raging river', 'Enchantment').
card_types('raging river', ['Enchantment']).
card_subtypes('raging river', []).
card_colors('raging river', ['R']).
card_text('raging river', 'Whenever one or more creatures you control attack, each defending player divides all creatures without flying he or she controls into a \"left\" pile and a \"right\" pile. Then, for each attacking creature you control, choose \"left\" or \"right.\" That creature can\'t be blocked this combat except by creatures with flying and creatures in a pile with the chosen label.').
card_mana_cost('raging river', ['R', 'R']).
card_cmc('raging river', 2).
card_layout('raging river', 'normal').
card_reserved('raging river').

% Found in: MIR
card_name('raging spirit', 'Raging Spirit').
card_type('raging spirit', 'Creature — Spirit').
card_types('raging spirit', ['Creature']).
card_subtypes('raging spirit', ['Spirit']).
card_colors('raging spirit', ['R']).
card_text('raging spirit', '{2}: Raging Spirit becomes colorless until end of turn.').
card_mana_cost('raging spirit', ['3', 'R']).
card_cmc('raging spirit', 4).
card_layout('raging spirit', 'normal').
card_power('raging spirit', 3).
card_toughness('raging spirit', 3).

% Found in: LEG, ME3
card_name('ragnar', 'Ragnar').
card_type('ragnar', 'Legendary Creature — Human Cleric').
card_types('ragnar', ['Creature']).
card_subtypes('ragnar', ['Human', 'Cleric']).
card_supertypes('ragnar', ['Legendary']).
card_colors('ragnar', ['W', 'U', 'G']).
card_text('ragnar', '{G}{W}{U}, {T}: Regenerate target creature.').
card_mana_cost('ragnar', ['G', 'W', 'U']).
card_cmc('ragnar', 3).
card_layout('ragnar', 'normal').
card_power('ragnar', 2).
card_toughness('ragnar', 2).
card_reserved('ragnar').

% Found in: ROE
card_name('raid bombardment', 'Raid Bombardment').
card_type('raid bombardment', 'Enchantment').
card_types('raid bombardment', ['Enchantment']).
card_subtypes('raid bombardment', []).
card_colors('raid bombardment', ['R']).
card_text('raid bombardment', 'Whenever a creature you control with power 2 or less attacks, Raid Bombardment deals 1 damage to defending player.').
card_mana_cost('raid bombardment', ['2', 'R']).
card_cmc('raid bombardment', 3).
card_layout('raid bombardment', 'normal').

% Found in: KTK
card_name('raiders\' spoils', 'Raiders\' Spoils').
card_type('raiders\' spoils', 'Enchantment').
card_types('raiders\' spoils', ['Enchantment']).
card_subtypes('raiders\' spoils', []).
card_colors('raiders\' spoils', ['B']).
card_text('raiders\' spoils', 'Creatures you control get +1/+0.\nWhenever a Warrior you control deals combat damage to a player, you may pay 1 life. If you do, draw a card.').
card_mana_cost('raiders\' spoils', ['3', 'B']).
card_cmc('raiders\' spoils', 4).
card_layout('raiders\' spoils', 'normal').

% Found in: PO2
card_name('raiding nightstalker', 'Raiding Nightstalker').
card_type('raiding nightstalker', 'Creature — Nightstalker').
card_types('raiding nightstalker', ['Creature']).
card_subtypes('raiding nightstalker', ['Nightstalker']).
card_colors('raiding nightstalker', ['B']).
card_text('raiding nightstalker', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('raiding nightstalker', ['2', 'B']).
card_cmc('raiding nightstalker', 3).
card_layout('raiding nightstalker', 'normal').
card_power('raiding nightstalker', 2).
card_toughness('raiding nightstalker', 2).

% Found in: FEM
card_name('raiding party', 'Raiding Party').
card_type('raiding party', 'Enchantment').
card_types('raiding party', ['Enchantment']).
card_subtypes('raiding party', []).
card_colors('raiding party', ['R']).
card_text('raiding party', 'Raiding Party can\'t be the target of white spells or abilities from white sources.\nSacrifice an Orc: Each player may tap any number of untapped white creatures he or she controls. For each creature tapped this way, that player chooses up to two Plains. Then destroy all Plains that weren\'t chosen this way by any player.').
card_mana_cost('raiding party', ['2', 'R']).
card_cmc('raiding party', 3).
card_layout('raiding party', 'normal').

% Found in: 8ED, M13, SCG
card_name('rain of blades', 'Rain of Blades').
card_type('rain of blades', 'Instant').
card_types('rain of blades', ['Instant']).
card_subtypes('rain of blades', []).
card_colors('rain of blades', ['W']).
card_text('rain of blades', 'Rain of Blades deals 1 damage to each attacking creature.').
card_mana_cost('rain of blades', ['W']).
card_cmc('rain of blades', 1).
card_layout('rain of blades', 'normal').

% Found in: ME4, PO2
card_name('rain of daggers', 'Rain of Daggers').
card_type('rain of daggers', 'Sorcery').
card_types('rain of daggers', ['Sorcery']).
card_subtypes('rain of daggers', []).
card_colors('rain of daggers', ['B']).
card_text('rain of daggers', 'Destroy all creatures target opponent controls. You lose 2 life for each creature destroyed this way.').
card_mana_cost('rain of daggers', ['4', 'B', 'B']).
card_cmc('rain of daggers', 6).
card_layout('rain of daggers', 'normal').

% Found in: RAV
card_name('rain of embers', 'Rain of Embers').
card_type('rain of embers', 'Sorcery').
card_types('rain of embers', ['Sorcery']).
card_subtypes('rain of embers', []).
card_colors('rain of embers', ['R']).
card_text('rain of embers', 'Rain of Embers deals 1 damage to each creature and each player.').
card_mana_cost('rain of embers', ['1', 'R']).
card_cmc('rain of embers', 2).
card_layout('rain of embers', 'normal').

% Found in: USG
card_name('rain of filth', 'Rain of Filth').
card_type('rain of filth', 'Instant').
card_types('rain of filth', ['Instant']).
card_subtypes('rain of filth', []).
card_colors('rain of filth', ['B']).
card_text('rain of filth', 'Until end of turn, lands you control gain \"Sacrifice this land: Add {B} to your mana pool.\"').
card_mana_cost('rain of filth', ['B']).
card_cmc('rain of filth', 1).
card_layout('rain of filth', 'normal').

% Found in: DIS
card_name('rain of gore', 'Rain of Gore').
card_type('rain of gore', 'Enchantment').
card_types('rain of gore', ['Enchantment']).
card_subtypes('rain of gore', []).
card_colors('rain of gore', ['B', 'R']).
card_text('rain of gore', 'If a spell or ability would cause its controller to gain life, that player loses that much life instead.').
card_mana_cost('rain of gore', ['B', 'R']).
card_cmc('rain of gore', 2).
card_layout('rain of gore', 'normal').

% Found in: 5DN
card_name('rain of rust', 'Rain of Rust').
card_type('rain of rust', 'Instant').
card_types('rain of rust', ['Instant']).
card_subtypes('rain of rust', []).
card_colors('rain of rust', ['R']).
card_text('rain of rust', 'Choose one —\n• Destroy target artifact.\n• Destroy target land.\nEntwine {3}{R} (Choose both if you pay the entwine cost.)').
card_mana_cost('rain of rust', ['3', 'R', 'R']).
card_cmc('rain of rust', 5).
card_layout('rain of rust', 'normal').

% Found in: POR, USG
card_name('rain of salt', 'Rain of Salt').
card_type('rain of salt', 'Sorcery').
card_types('rain of salt', ['Sorcery']).
card_subtypes('rain of salt', []).
card_colors('rain of salt', ['R']).
card_text('rain of salt', 'Destroy two target lands.').
card_mana_cost('rain of salt', ['4', 'R', 'R']).
card_cmc('rain of salt', 6).
card_layout('rain of salt', 'normal').

% Found in: 10E, MMQ, POR, TMP
card_name('rain of tears', 'Rain of Tears').
card_type('rain of tears', 'Sorcery').
card_types('rain of tears', ['Sorcery']).
card_subtypes('rain of tears', []).
card_colors('rain of tears', ['B']).
card_text('rain of tears', 'Destroy target land.').
card_mana_cost('rain of tears', ['1', 'B', 'B']).
card_cmc('rain of tears', 3).
card_layout('rain of tears', 'normal').

% Found in: AVR, C13
card_name('rain of thorns', 'Rain of Thorns').
card_type('rain of thorns', 'Sorcery').
card_types('rain of thorns', ['Sorcery']).
card_subtypes('rain of thorns', []).
card_colors('rain of thorns', ['G']).
card_text('rain of thorns', 'Choose one or more —\n• Destroy target artifact.\n• Destroy target enchantment.\n• Destroy target land.').
card_mana_cost('rain of thorns', ['4', 'G', 'G']).
card_cmc('rain of thorns', 6).
card_layout('rain of thorns', 'normal').

% Found in: INV
card_name('rainbow crow', 'Rainbow Crow').
card_type('rainbow crow', 'Creature — Bird').
card_types('rainbow crow', ['Creature']).
card_subtypes('rainbow crow', ['Bird']).
card_colors('rainbow crow', ['U']).
card_text('rainbow crow', 'Flying\n{1}: Rainbow Crow becomes the color of your choice until end of turn.').
card_mana_cost('rainbow crow', ['3', 'U']).
card_cmc('rainbow crow', 4).
card_layout('rainbow crow', 'normal').
card_power('rainbow crow', 2).
card_toughness('rainbow crow', 2).

% Found in: VIS
card_name('rainbow efreet', 'Rainbow Efreet').
card_type('rainbow efreet', 'Creature — Efreet').
card_types('rainbow efreet', ['Creature']).
card_subtypes('rainbow efreet', ['Efreet']).
card_colors('rainbow efreet', ['U']).
card_text('rainbow efreet', 'Flying\n{U}{U}: Rainbow Efreet phases out. (While it\'s phased out, it\'s treated as though it doesn\'t exist. It phases in before you untap during your next untap step.)').
card_mana_cost('rainbow efreet', ['3', 'U']).
card_cmc('rainbow efreet', 4).
card_layout('rainbow efreet', 'normal').
card_power('rainbow efreet', 3).
card_toughness('rainbow efreet', 1).
card_reserved('rainbow efreet').

% Found in: FEM, MED
card_name('rainbow vale', 'Rainbow Vale').
card_type('rainbow vale', 'Land').
card_types('rainbow vale', ['Land']).
card_subtypes('rainbow vale', []).
card_colors('rainbow vale', []).
card_text('rainbow vale', '{T}: Add one mana of any color to your mana pool. An opponent gains control of Rainbow Vale at the beginning of the next end step.').
card_layout('rainbow vale', 'normal').
card_reserved('rainbow vale').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, CED, CEI, ITP, LEA, LEB, PO2, POR, RQS, S99
card_name('raise dead', 'Raise Dead').
card_type('raise dead', 'Sorcery').
card_types('raise dead', ['Sorcery']).
card_subtypes('raise dead', []).
card_colors('raise dead', ['B']).
card_text('raise dead', 'Return target creature card from your graveyard to your hand.').
card_mana_cost('raise dead', ['B']).
card_cmc('raise dead', 1).
card_layout('raise dead', 'normal').

% Found in: DDF, DDO, M15, MD1, MM2, MRD
card_name('raise the alarm', 'Raise the Alarm').
card_type('raise the alarm', 'Instant').
card_types('raise the alarm', ['Instant']).
card_subtypes('raise the alarm', []).
card_colors('raise the alarm', ['W']).
card_text('raise the alarm', 'Put two 1/1 white Soldier creature tokens onto the battlefield.').
card_mana_cost('raise the alarm', ['1', 'W']).
card_cmc('raise the alarm', 2).
card_layout('raise the alarm', 'normal').

% Found in: BNG
card_name('raised by wolves', 'Raised by Wolves').
card_type('raised by wolves', 'Enchantment — Aura').
card_types('raised by wolves', ['Enchantment']).
card_subtypes('raised by wolves', ['Aura']).
card_colors('raised by wolves', ['G']).
card_text('raised by wolves', 'Enchant creature\nWhen Raised by Wolves enters the battlefield, put two 2/2 green Wolf creature tokens onto the battlefield.\nEnchanted creature gets +1/+1 for each Wolf you control.').
card_mana_cost('raised by wolves', ['3', 'G', 'G']).
card_cmc('raised by wolves', 5).
card_layout('raised by wolves', 'normal').

% Found in: APC
card_name('raka disciple', 'Raka Disciple').
card_type('raka disciple', 'Creature — Minotaur Wizard').
card_types('raka disciple', ['Creature']).
card_subtypes('raka disciple', ['Minotaur', 'Wizard']).
card_colors('raka disciple', ['R']).
card_text('raka disciple', '{W}, {T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\n{U}, {T}: Target creature gains flying until end of turn.').
card_mana_cost('raka disciple', ['R']).
card_cmc('raka disciple', 1).
card_layout('raka disciple', 'normal').
card_power('raka disciple', 1).
card_toughness('raka disciple', 1).

% Found in: APC
card_name('raka sanctuary', 'Raka Sanctuary').
card_type('raka sanctuary', 'Enchantment').
card_types('raka sanctuary', ['Enchantment']).
card_subtypes('raka sanctuary', []).
card_colors('raka sanctuary', ['R']).
card_text('raka sanctuary', 'At the beginning of your upkeep, if you control a white or blue permanent, Raka Sanctuary deals 1 damage to target creature. If you control a white permanent and a blue permanent, Raka Sanctuary deals 3 damage to that creature instead.').
card_mana_cost('raka sanctuary', ['2', 'R']).
card_cmc('raka sanctuary', 3).
card_layout('raka sanctuary', 'normal').

% Found in: ATQ, CHR, ME4
card_name('rakalite', 'Rakalite').
card_type('rakalite', 'Artifact').
card_types('rakalite', ['Artifact']).
card_subtypes('rakalite', []).
card_colors('rakalite', []).
card_text('rakalite', '{2}: Prevent the next 1 damage that would be dealt to target creature or player this turn. Return Rakalite to its owner\'s hand at the beginning of the next end step.').
card_mana_cost('rakalite', ['6']).
card_cmc('rakalite', 6).
card_layout('rakalite', 'normal').

% Found in: APC
card_name('rakavolver', 'Rakavolver').
card_type('rakavolver', 'Creature — Volver').
card_types('rakavolver', ['Creature']).
card_subtypes('rakavolver', ['Volver']).
card_colors('rakavolver', ['R']).
card_text('rakavolver', 'Kicker {1}{W} and/or {U} (You may pay an additional {1}{W} and/or {U} as you cast this spell.)\nIf Rakavolver was kicked with its {1}{W} kicker, it enters the battlefield with two +1/+1 counters on it and with \"Whenever Rakavolver deals damage, you gain that much life.\"\nIf Rakavolver was kicked with its {U} kicker, it enters the battlefield with a +1/+1 counter on it and with flying.').
card_mana_cost('rakavolver', ['2', 'R']).
card_cmc('rakavolver', 3).
card_layout('rakavolver', 'normal').
card_power('rakavolver', 2).
card_toughness('rakavolver', 2).

% Found in: DIS
card_name('rakdos augermage', 'Rakdos Augermage').
card_type('rakdos augermage', 'Creature — Human Wizard').
card_types('rakdos augermage', ['Creature']).
card_subtypes('rakdos augermage', ['Human', 'Wizard']).
card_colors('rakdos augermage', ['B', 'R']).
card_text('rakdos augermage', 'First strike\n{T}: Reveal your hand and discard a card of target opponent\'s choice. Then that player reveals his or her hand and discards a card of your choice. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('rakdos augermage', ['B', 'B', 'R']).
card_cmc('rakdos augermage', 3).
card_layout('rakdos augermage', 'normal').
card_power('rakdos augermage', 3).
card_toughness('rakdos augermage', 2).

% Found in: RTR, pFNM
card_name('rakdos cackler', 'Rakdos Cackler').
card_type('rakdos cackler', 'Creature — Devil').
card_types('rakdos cackler', ['Creature']).
card_subtypes('rakdos cackler', ['Devil']).
card_colors('rakdos cackler', ['B', 'R']).
card_text('rakdos cackler', 'Unleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_mana_cost('rakdos cackler', ['B/R']).
card_cmc('rakdos cackler', 1).
card_layout('rakdos cackler', 'normal').
card_power('rakdos cackler', 1).
card_toughness('rakdos cackler', 1).

% Found in: ARC, C13, CMD, DDK, DIS, MM2
card_name('rakdos carnarium', 'Rakdos Carnarium').
card_type('rakdos carnarium', 'Land').
card_types('rakdos carnarium', ['Land']).
card_subtypes('rakdos carnarium', []).
card_colors('rakdos carnarium', []).
card_text('rakdos carnarium', 'Rakdos Carnarium enters the battlefield tapped.\nWhen Rakdos Carnarium enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {B}{R} to your mana pool.').
card_layout('rakdos carnarium', 'normal').

% Found in: RTR
card_name('rakdos charm', 'Rakdos Charm').
card_type('rakdos charm', 'Instant').
card_types('rakdos charm', ['Instant']).
card_subtypes('rakdos charm', []).
card_colors('rakdos charm', ['B', 'R']).
card_text('rakdos charm', 'Choose one —\n• Exile all cards from target player\'s graveyard.\n• Destroy target artifact.\n• Each creature deals 1 damage to its controller.').
card_mana_cost('rakdos charm', ['B', 'R']).
card_cmc('rakdos charm', 2).
card_layout('rakdos charm', 'normal').

% Found in: DGM
card_name('rakdos cluestone', 'Rakdos Cluestone').
card_type('rakdos cluestone', 'Artifact').
card_types('rakdos cluestone', ['Artifact']).
card_subtypes('rakdos cluestone', []).
card_colors('rakdos cluestone', []).
card_text('rakdos cluestone', '{T}: Add {B} or {R} to your mana pool.\n{B}{R}, {T}, Sacrifice Rakdos Cluestone: Draw a card.').
card_mana_cost('rakdos cluestone', ['3']).
card_cmc('rakdos cluestone', 3).
card_layout('rakdos cluestone', 'normal').

% Found in: DGM
card_name('rakdos drake', 'Rakdos Drake').
card_type('rakdos drake', 'Creature — Drake').
card_types('rakdos drake', ['Creature']).
card_subtypes('rakdos drake', ['Drake']).
card_colors('rakdos drake', ['B']).
card_text('rakdos drake', 'Flying\nUnleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_mana_cost('rakdos drake', ['2', 'B']).
card_cmc('rakdos drake', 3).
card_layout('rakdos drake', 'normal').
card_power('rakdos drake', 1).
card_toughness('rakdos drake', 2).

% Found in: C13, DGM, RTR
card_name('rakdos guildgate', 'Rakdos Guildgate').
card_type('rakdos guildgate', 'Land — Gate').
card_types('rakdos guildgate', ['Land']).
card_subtypes('rakdos guildgate', ['Gate']).
card_colors('rakdos guildgate', []).
card_text('rakdos guildgate', 'Rakdos Guildgate enters the battlefield tapped.\n{T}: Add {B} or {R} to your mana pool.').
card_layout('rakdos guildgate', 'normal').

% Found in: ARC, DIS, pCMP
card_name('rakdos guildmage', 'Rakdos Guildmage').
card_type('rakdos guildmage', 'Creature — Zombie Shaman').
card_types('rakdos guildmage', ['Creature']).
card_subtypes('rakdos guildmage', ['Zombie', 'Shaman']).
card_colors('rakdos guildmage', ['B', 'R']).
card_text('rakdos guildmage', '({B/R} can be paid with either {B} or {R}.)\n{3}{B}, Discard a card: Target creature gets -2/-2 until end of turn.\n{3}{R}: Put a 2/1 red Goblin creature token with haste onto the battlefield. Exile it at the beginning of the next end step.').
card_mana_cost('rakdos guildmage', ['B/R', 'B/R']).
card_cmc('rakdos guildmage', 2).
card_layout('rakdos guildmage', 'normal').
card_power('rakdos guildmage', 2).
card_toughness('rakdos guildmage', 2).

% Found in: DIS
card_name('rakdos ickspitter', 'Rakdos Ickspitter').
card_type('rakdos ickspitter', 'Creature — Thrull').
card_types('rakdos ickspitter', ['Creature']).
card_subtypes('rakdos ickspitter', ['Thrull']).
card_colors('rakdos ickspitter', ['B', 'R']).
card_text('rakdos ickspitter', '{T}: Rakdos Ickspitter deals 1 damage to target creature and that creature\'s controller loses 1 life.').
card_mana_cost('rakdos ickspitter', ['1', 'B', 'R']).
card_cmc('rakdos ickspitter', 3).
card_layout('rakdos ickspitter', 'normal').
card_power('rakdos ickspitter', 1).
card_toughness('rakdos ickspitter', 1).

% Found in: RTR
card_name('rakdos keyrune', 'Rakdos Keyrune').
card_type('rakdos keyrune', 'Artifact').
card_types('rakdos keyrune', ['Artifact']).
card_subtypes('rakdos keyrune', []).
card_colors('rakdos keyrune', []).
card_text('rakdos keyrune', '{T}: Add {B} or {R} to your mana pool.\n{B}{R}: Rakdos Keyrune becomes a 3/1 black and red Devil artifact creature with first strike until end of turn.').
card_mana_cost('rakdos keyrune', ['3']).
card_cmc('rakdos keyrune', 3).
card_layout('rakdos keyrune', 'normal').

% Found in: DD2, DD3_JVC, DIS
card_name('rakdos pit dragon', 'Rakdos Pit Dragon').
card_type('rakdos pit dragon', 'Creature — Dragon').
card_types('rakdos pit dragon', ['Creature']).
card_subtypes('rakdos pit dragon', ['Dragon']).
card_colors('rakdos pit dragon', ['R']).
card_text('rakdos pit dragon', '{R}{R}: Rakdos Pit Dragon gains flying until end of turn.\n{R}: Rakdos Pit Dragon gets +1/+0 until end of turn.\nHellbent — Rakdos Pit Dragon has double strike as long as you have no cards in hand.').
card_mana_cost('rakdos pit dragon', ['2', 'R', 'R']).
card_cmc('rakdos pit dragon', 4).
card_layout('rakdos pit dragon', 'normal').
card_power('rakdos pit dragon', 3).
card_toughness('rakdos pit dragon', 3).

% Found in: RTR
card_name('rakdos ragemutt', 'Rakdos Ragemutt').
card_type('rakdos ragemutt', 'Creature — Elemental Hound').
card_types('rakdos ragemutt', ['Creature']).
card_subtypes('rakdos ragemutt', ['Elemental', 'Hound']).
card_colors('rakdos ragemutt', ['B', 'R']).
card_text('rakdos ragemutt', 'Lifelink, haste').
card_mana_cost('rakdos ragemutt', ['3', 'B', 'R']).
card_cmc('rakdos ragemutt', 5).
card_layout('rakdos ragemutt', 'normal').
card_power('rakdos ragemutt', 3).
card_toughness('rakdos ragemutt', 3).

% Found in: RTR
card_name('rakdos ringleader', 'Rakdos Ringleader').
card_type('rakdos ringleader', 'Creature — Skeleton Warrior').
card_types('rakdos ringleader', ['Creature']).
card_subtypes('rakdos ringleader', ['Skeleton', 'Warrior']).
card_colors('rakdos ringleader', ['B', 'R']).
card_text('rakdos ringleader', 'First strike\nWhenever Rakdos Ringleader deals combat damage to a player, that player discards a card at random.\n{B}: Regenerate Rakdos Ringleader.').
card_mana_cost('rakdos ringleader', ['4', 'B', 'R']).
card_cmc('rakdos ringleader', 6).
card_layout('rakdos ringleader', 'normal').
card_power('rakdos ringleader', 3).
card_toughness('rakdos ringleader', 1).

% Found in: DIS
card_name('rakdos riteknife', 'Rakdos Riteknife').
card_type('rakdos riteknife', 'Artifact — Equipment').
card_types('rakdos riteknife', ['Artifact']).
card_subtypes('rakdos riteknife', ['Equipment']).
card_colors('rakdos riteknife', []).
card_text('rakdos riteknife', 'Equipped creature gets +1/+0 for each blood counter on Rakdos Riteknife and has \"{T}, Sacrifice a creature: Put a blood counter on Rakdos Riteknife.\"\n{B}{R}, Sacrifice Rakdos Riteknife: Target player sacrifices a permanent for each blood counter on Rakdos Riteknife.\nEquip {2}').
card_mana_cost('rakdos riteknife', ['2']).
card_cmc('rakdos riteknife', 2).
card_layout('rakdos riteknife', 'normal').

% Found in: RTR
card_name('rakdos shred-freak', 'Rakdos Shred-Freak').
card_type('rakdos shred-freak', 'Creature — Human Berserker').
card_types('rakdos shred-freak', ['Creature']).
card_subtypes('rakdos shred-freak', ['Human', 'Berserker']).
card_colors('rakdos shred-freak', ['B', 'R']).
card_text('rakdos shred-freak', 'Haste').
card_mana_cost('rakdos shred-freak', ['B/R', 'B/R']).
card_cmc('rakdos shred-freak', 2).
card_layout('rakdos shred-freak', 'normal').
card_power('rakdos shred-freak', 2).
card_toughness('rakdos shred-freak', 1).

% Found in: ARC, CMD, DIS
card_name('rakdos signet', 'Rakdos Signet').
card_type('rakdos signet', 'Artifact').
card_types('rakdos signet', ['Artifact']).
card_subtypes('rakdos signet', []).
card_colors('rakdos signet', []).
card_text('rakdos signet', '{1}, {T}: Add {B}{R} to your mana pool.').
card_mana_cost('rakdos signet', ['2']).
card_cmc('rakdos signet', 2).
card_layout('rakdos signet', 'normal').

% Found in: DIS
card_name('rakdos the defiler', 'Rakdos the Defiler').
card_type('rakdos the defiler', 'Legendary Creature — Demon').
card_types('rakdos the defiler', ['Creature']).
card_subtypes('rakdos the defiler', ['Demon']).
card_supertypes('rakdos the defiler', ['Legendary']).
card_colors('rakdos the defiler', ['B', 'R']).
card_text('rakdos the defiler', 'Flying, trample\nWhenever Rakdos the Defiler attacks, sacrifice half the non-Demon permanents you control, rounded up.\nWhenever Rakdos deals combat damage to a player, that player sacrifices half the non-Demon permanents he or she controls, rounded up.').
card_mana_cost('rakdos the defiler', ['2', 'B', 'B', 'R', 'R']).
card_cmc('rakdos the defiler', 6).
card_layout('rakdos the defiler', 'normal').
card_power('rakdos the defiler', 7).
card_toughness('rakdos the defiler', 6).

% Found in: RTR
card_name('rakdos\'s return', 'Rakdos\'s Return').
card_type('rakdos\'s return', 'Sorcery').
card_types('rakdos\'s return', ['Sorcery']).
card_subtypes('rakdos\'s return', []).
card_colors('rakdos\'s return', ['B', 'R']).
card_text('rakdos\'s return', 'Rakdos\'s Return deals X damage to target opponent. That player discards X cards.').
card_mana_cost('rakdos\'s return', ['X', 'B', 'R']).
card_cmc('rakdos\'s return', 2).
card_layout('rakdos\'s return', 'normal').

% Found in: RTR
card_name('rakdos, lord of riots', 'Rakdos, Lord of Riots').
card_type('rakdos, lord of riots', 'Legendary Creature — Demon').
card_types('rakdos, lord of riots', ['Creature']).
card_subtypes('rakdos, lord of riots', ['Demon']).
card_supertypes('rakdos, lord of riots', ['Legendary']).
card_colors('rakdos, lord of riots', ['B', 'R']).
card_text('rakdos, lord of riots', 'You can\'t cast Rakdos, Lord of Riots unless an opponent lost life this turn.\nFlying, trample\nCreature spells you cast cost {1} less to cast for each 1 life your opponents have lost this turn.').
card_mana_cost('rakdos, lord of riots', ['B', 'B', 'R', 'R']).
card_cmc('rakdos, lord of riots', 4).
card_layout('rakdos, lord of riots', 'normal').
card_power('rakdos, lord of riots', 6).
card_toughness('rakdos, lord of riots', 6).

% Found in: ALA, C13
card_name('rakeclaw gargantuan', 'Rakeclaw Gargantuan').
card_type('rakeclaw gargantuan', 'Creature — Beast').
card_types('rakeclaw gargantuan', ['Creature']).
card_subtypes('rakeclaw gargantuan', ['Beast']).
card_colors('rakeclaw gargantuan', ['W', 'R', 'G']).
card_text('rakeclaw gargantuan', '{1}: Target creature with power 5 or greater gains first strike until end of turn.').
card_mana_cost('rakeclaw gargantuan', ['2', 'R', 'G', 'W']).
card_cmc('rakeclaw gargantuan', 5).
card_layout('rakeclaw gargantuan', 'normal').
card_power('rakeclaw gargantuan', 5).
card_toughness('rakeclaw gargantuan', 3).

% Found in: SHM
card_name('raking canopy', 'Raking Canopy').
card_type('raking canopy', 'Enchantment').
card_types('raking canopy', ['Enchantment']).
card_subtypes('raking canopy', []).
card_colors('raking canopy', ['G']).
card_text('raking canopy', 'Whenever a creature with flying attacks you, Raking Canopy deals 4 damage to it.').
card_mana_cost('raking canopy', ['1', 'G', 'G']).
card_cmc('raking canopy', 3).
card_layout('raking canopy', 'normal').

% Found in: ISD
card_name('rakish heir', 'Rakish Heir').
card_type('rakish heir', 'Creature — Vampire').
card_types('rakish heir', ['Creature']).
card_subtypes('rakish heir', ['Vampire']).
card_colors('rakish heir', ['R']).
card_text('rakish heir', 'Whenever a Vampire you control deals combat damage to a player, put a +1/+1 counter on it.').
card_mana_cost('rakish heir', ['2', 'R']).
card_cmc('rakish heir', 3).
card_layout('rakish heir', 'normal').
card_power('rakish heir', 2).
card_toughness('rakish heir', 2).

% Found in: CON
card_name('rakka mar', 'Rakka Mar').
card_type('rakka mar', 'Legendary Creature — Human Shaman').
card_types('rakka mar', ['Creature']).
card_subtypes('rakka mar', ['Human', 'Shaman']).
card_supertypes('rakka mar', ['Legendary']).
card_colors('rakka mar', ['R']).
card_text('rakka mar', 'Haste\n{R}, {T}: Put a 3/1 red Elemental creature token with haste onto the battlefield.').
card_mana_cost('rakka mar', ['2', 'R', 'R']).
card_cmc('rakka mar', 4).
card_layout('rakka mar', 'normal').
card_power('rakka mar', 2).
card_toughness('rakka mar', 2).

% Found in: 5DN
card_name('raksha golden cub', 'Raksha Golden Cub').
card_type('raksha golden cub', 'Legendary Creature — Cat Soldier').
card_types('raksha golden cub', ['Creature']).
card_subtypes('raksha golden cub', ['Cat', 'Soldier']).
card_supertypes('raksha golden cub', ['Legendary']).
card_colors('raksha golden cub', ['W']).
card_text('raksha golden cub', 'Vigilance\nAs long as Raksha Golden Cub is equipped, Cat creatures you control get +2/+2 and have double strike.').
card_mana_cost('raksha golden cub', ['5', 'W', 'W']).
card_cmc('raksha golden cub', 7).
card_layout('raksha golden cub', 'normal').
card_power('raksha golden cub', 3).
card_toughness('raksha golden cub', 4).

% Found in: VAN
card_name('raksha golden cub avatar', 'Raksha Golden Cub Avatar').
card_type('raksha golden cub avatar', 'Vanguard').
card_types('raksha golden cub avatar', ['Vanguard']).
card_subtypes('raksha golden cub avatar', []).
card_colors('raksha golden cub avatar', []).
card_text('raksha golden cub avatar', 'Creatures you control get +0/+1.\nEquipped creatures you control get +1/+0 and have first strike.').
card_layout('raksha golden cub avatar', 'vanguard').

% Found in: KTK
card_name('rakshasa deathdealer', 'Rakshasa Deathdealer').
card_type('rakshasa deathdealer', 'Creature — Cat Demon').
card_types('rakshasa deathdealer', ['Creature']).
card_subtypes('rakshasa deathdealer', ['Cat', 'Demon']).
card_colors('rakshasa deathdealer', ['B', 'G']).
card_text('rakshasa deathdealer', '{B}{G}: Rakshasa Deathdealer gets +2/+2 until end of turn.\n{B}{G}: Regenerate Rakshasa Deathdealer.').
card_mana_cost('rakshasa deathdealer', ['B', 'G']).
card_cmc('rakshasa deathdealer', 2).
card_layout('rakshasa deathdealer', 'normal').
card_power('rakshasa deathdealer', 2).
card_toughness('rakshasa deathdealer', 2).

% Found in: DTK
card_name('rakshasa gravecaller', 'Rakshasa Gravecaller').
card_type('rakshasa gravecaller', 'Creature — Cat Demon').
card_types('rakshasa gravecaller', ['Creature']).
card_subtypes('rakshasa gravecaller', ['Cat', 'Demon']).
card_colors('rakshasa gravecaller', ['B']).
card_text('rakshasa gravecaller', 'Exploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Rakshasa Gravecaller exploits a creature, put two 2/2 black Zombie creature tokens onto the battlefield.').
card_mana_cost('rakshasa gravecaller', ['4', 'B']).
card_cmc('rakshasa gravecaller', 5).
card_layout('rakshasa gravecaller', 'normal').
card_power('rakshasa gravecaller', 3).
card_toughness('rakshasa gravecaller', 6).

% Found in: KTK, pMEI, pPRE
card_name('rakshasa vizier', 'Rakshasa Vizier').
card_type('rakshasa vizier', 'Creature — Cat Demon').
card_types('rakshasa vizier', ['Creature']).
card_subtypes('rakshasa vizier', ['Cat', 'Demon']).
card_colors('rakshasa vizier', ['U', 'B', 'G']).
card_text('rakshasa vizier', 'Whenever one or more cards are put into exile from your graveyard, put that many +1/+1 counters on Rakshasa Vizier.').
card_mana_cost('rakshasa vizier', ['2', 'B', 'G', 'U']).
card_cmc('rakshasa vizier', 5).
card_layout('rakshasa vizier', 'normal').
card_power('rakshasa vizier', 4).
card_toughness('rakshasa vizier', 4).

% Found in: FRF
card_name('rakshasa\'s disdain', 'Rakshasa\'s Disdain').
card_type('rakshasa\'s disdain', 'Instant').
card_types('rakshasa\'s disdain', ['Instant']).
card_subtypes('rakshasa\'s disdain', []).
card_colors('rakshasa\'s disdain', ['U']).
card_text('rakshasa\'s disdain', 'Counter target spell unless its controller pays {1} for each card in your graveyard.').
card_mana_cost('rakshasa\'s disdain', ['2', 'U']).
card_cmc('rakshasa\'s disdain', 3).
card_layout('rakshasa\'s disdain', 'normal').

% Found in: KTK
card_name('rakshasa\'s secret', 'Rakshasa\'s Secret').
card_type('rakshasa\'s secret', 'Sorcery').
card_types('rakshasa\'s secret', ['Sorcery']).
card_subtypes('rakshasa\'s secret', []).
card_colors('rakshasa\'s secret', ['B']).
card_text('rakshasa\'s secret', 'Target opponent discards two cards. Put the top two cards of your library into your graveyard.').
card_mana_cost('rakshasa\'s secret', ['2', 'B']).
card_cmc('rakshasa\'s secret', 3).
card_layout('rakshasa\'s secret', 'normal').

% Found in: DGM
card_name('ral zarek', 'Ral Zarek').
card_type('ral zarek', 'Planeswalker — Ral').
card_types('ral zarek', ['Planeswalker']).
card_subtypes('ral zarek', ['Ral']).
card_colors('ral zarek', ['U', 'R']).
card_text('ral zarek', '+1: Tap target permanent, then untap another target permanent.\n−2: Ral Zarek deals 3 damage to target creature or player.\n−7: Flip five coins. Take an extra turn after this one for each coin that comes up heads.').
card_mana_cost('ral zarek', ['2', 'U', 'R']).
card_cmc('ral zarek', 4).
card_layout('ral zarek', 'normal').
card_loyalty('ral zarek', 4).

% Found in: ICE
card_name('rally', 'Rally').
card_type('rally', 'Instant').
card_types('rally', ['Instant']).
card_subtypes('rally', []).
card_colors('rally', ['W']).
card_text('rally', 'Blocking creatures get +1/+1 until end of turn.').
card_mana_cost('rally', ['W', 'W']).
card_cmc('rally', 2).
card_layout('rally', 'normal').

% Found in: FRF
card_name('rally the ancestors', 'Rally the Ancestors').
card_type('rally the ancestors', 'Instant').
card_types('rally the ancestors', ['Instant']).
card_subtypes('rally the ancestors', []).
card_colors('rally the ancestors', ['W']).
card_text('rally the ancestors', 'Return each creature card with converted mana cost X or less from your graveyard to the battlefield. Exile those creatures at the beginning of your next upkeep. Exile Rally the Ancestors.').
card_mana_cost('rally the ancestors', ['X', 'W', 'W']).
card_cmc('rally the ancestors', 2).
card_layout('rally the ancestors', 'normal').

% Found in: MBS
card_name('rally the forces', 'Rally the Forces').
card_type('rally the forces', 'Instant').
card_types('rally the forces', ['Instant']).
card_subtypes('rally the forces', []).
card_colors('rally the forces', ['R']).
card_text('rally the forces', 'Attacking creatures get +1/+0 and gain first strike until end of turn.').
card_mana_cost('rally the forces', ['2', 'R']).
card_cmc('rally the forces', 3).
card_layout('rally the forces', 'normal').

% Found in: SOK
card_name('rally the horde', 'Rally the Horde').
card_type('rally the horde', 'Sorcery').
card_types('rally the horde', ['Sorcery']).
card_subtypes('rally the horde', []).
card_colors('rally the horde', ['R']).
card_text('rally the horde', 'Exile the top card of your library. Exile the top card of your library. Exile the top card of your library. If the last card exiled isn\'t a land, repeat this process. Put a 1/1 red Warrior creature token onto the battlefield for each nonland card exiled this way.').
card_mana_cost('rally the horde', ['5', 'R']).
card_cmc('rally the horde', 6).
card_layout('rally the horde', 'normal').

% Found in: ISD
card_name('rally the peasants', 'Rally the Peasants').
card_type('rally the peasants', 'Instant').
card_types('rally the peasants', ['Instant']).
card_subtypes('rally the peasants', []).
card_colors('rally the peasants', ['W']).
card_text('rally the peasants', 'Creatures you control get +2/+0 until end of turn.\nFlashback {2}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('rally the peasants', ['2', 'W']).
card_cmc('rally the peasants', 3).
card_layout('rally the peasants', 'normal').

% Found in: RAV
card_name('rally the righteous', 'Rally the Righteous').
card_type('rally the righteous', 'Instant').
card_types('rally the righteous', ['Instant']).
card_subtypes('rally the righteous', []).
card_colors('rally the righteous', ['W', 'R']).
card_text('rally the righteous', 'Radiance — Untap target creature and each other creature that shares a color with it. Those creatures get +2/+0 until end of turn.').
card_mana_cost('rally the righteous', ['1', 'R', 'W']).
card_cmc('rally the righteous', 3).
card_layout('rally the righteous', 'normal').

% Found in: PO2, PTK
card_name('rally the troops', 'Rally the Troops').
card_type('rally the troops', 'Instant').
card_types('rally the troops', ['Instant']).
card_subtypes('rally the troops', []).
card_colors('rally the troops', ['W']).
card_text('rally the troops', 'Cast Rally the Troops only during the declare attackers step and only if you\'ve been attacked this step.\nUntap all creatures you control.').
card_mana_cost('rally the troops', ['W']).
card_cmc('rally the troops', 1).
card_layout('rally the troops', 'normal').

% Found in: LEG, ME3
card_name('ramirez depietro', 'Ramirez DePietro').
card_type('ramirez depietro', 'Legendary Creature — Human Pirate').
card_types('ramirez depietro', ['Creature']).
card_subtypes('ramirez depietro', ['Human', 'Pirate']).
card_supertypes('ramirez depietro', ['Legendary']).
card_colors('ramirez depietro', ['U', 'B']).
card_text('ramirez depietro', 'First strike').
card_mana_cost('ramirez depietro', ['3', 'U', 'B', 'B']).
card_cmc('ramirez depietro', 6).
card_layout('ramirez depietro', 'normal').
card_power('ramirez depietro', 4).
card_toughness('ramirez depietro', 3).

% Found in: MMQ
card_name('ramosian captain', 'Ramosian Captain').
card_type('ramosian captain', 'Creature — Human Rebel').
card_types('ramosian captain', ['Creature']).
card_subtypes('ramosian captain', ['Human', 'Rebel']).
card_colors('ramosian captain', ['W']).
card_text('ramosian captain', 'First strike\n{5}, {T}: Search your library for a Rebel permanent card with converted mana cost 4 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('ramosian captain', ['1', 'W', 'W']).
card_cmc('ramosian captain', 3).
card_layout('ramosian captain', 'normal').
card_power('ramosian captain', 2).
card_toughness('ramosian captain', 2).

% Found in: MMQ
card_name('ramosian commander', 'Ramosian Commander').
card_type('ramosian commander', 'Creature — Human Rebel').
card_types('ramosian commander', ['Creature']).
card_subtypes('ramosian commander', ['Human', 'Rebel']).
card_colors('ramosian commander', ['W']).
card_text('ramosian commander', '{6}, {T}: Search your library for a Rebel permanent card with converted mana cost 5 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('ramosian commander', ['2', 'W', 'W']).
card_cmc('ramosian commander', 4).
card_layout('ramosian commander', 'normal').
card_power('ramosian commander', 2).
card_toughness('ramosian commander', 4).

% Found in: MMQ
card_name('ramosian lieutenant', 'Ramosian Lieutenant').
card_type('ramosian lieutenant', 'Creature — Human Rebel').
card_types('ramosian lieutenant', ['Creature']).
card_subtypes('ramosian lieutenant', ['Human', 'Rebel']).
card_colors('ramosian lieutenant', ['W']).
card_text('ramosian lieutenant', '{4}, {T}: Search your library for a Rebel permanent card with converted mana cost 3 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('ramosian lieutenant', ['1', 'W']).
card_cmc('ramosian lieutenant', 2).
card_layout('ramosian lieutenant', 'normal').
card_power('ramosian lieutenant', 1).
card_toughness('ramosian lieutenant', 2).

% Found in: MMQ
card_name('ramosian rally', 'Ramosian Rally').
card_type('ramosian rally', 'Instant').
card_types('ramosian rally', ['Instant']).
card_subtypes('ramosian rally', []).
card_colors('ramosian rally', ['W']).
card_text('ramosian rally', 'If you control a Plains, you may tap an untapped creature you control rather than pay Ramosian Rally\'s mana cost.\nCreatures you control get +1/+1 until end of turn.').
card_mana_cost('ramosian rally', ['3', 'W']).
card_cmc('ramosian rally', 4).
card_layout('ramosian rally', 'normal').

% Found in: FUT
card_name('ramosian revivalist', 'Ramosian Revivalist').
card_type('ramosian revivalist', 'Creature — Human Rebel Cleric').
card_types('ramosian revivalist', ['Creature']).
card_subtypes('ramosian revivalist', ['Human', 'Rebel', 'Cleric']).
card_colors('ramosian revivalist', ['W']).
card_text('ramosian revivalist', '{6}, {T}: Return target Rebel permanent card with converted mana cost 5 or less from your graveyard to the battlefield.').
card_mana_cost('ramosian revivalist', ['3', 'W']).
card_cmc('ramosian revivalist', 4).
card_layout('ramosian revivalist', 'normal').
card_power('ramosian revivalist', 2).
card_toughness('ramosian revivalist', 2).

% Found in: MMQ
card_name('ramosian sergeant', 'Ramosian Sergeant').
card_type('ramosian sergeant', 'Creature — Human Rebel').
card_types('ramosian sergeant', ['Creature']).
card_subtypes('ramosian sergeant', ['Human', 'Rebel']).
card_colors('ramosian sergeant', ['W']).
card_text('ramosian sergeant', '{3}, {T}: Search your library for a Rebel permanent card with converted mana cost 2 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('ramosian sergeant', ['W']).
card_cmc('ramosian sergeant', 1).
card_layout('ramosian sergeant', 'normal').
card_power('ramosian sergeant', 1).
card_toughness('ramosian sergeant', 1).

% Found in: MMQ
card_name('ramosian sky marshal', 'Ramosian Sky Marshal').
card_type('ramosian sky marshal', 'Creature — Human Rebel').
card_types('ramosian sky marshal', ['Creature']).
card_subtypes('ramosian sky marshal', ['Human', 'Rebel']).
card_colors('ramosian sky marshal', ['W']).
card_text('ramosian sky marshal', 'Flying\n{7}, {T}: Search your library for a Rebel permanent card with converted mana cost 6 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('ramosian sky marshal', ['3', 'W', 'W']).
card_cmc('ramosian sky marshal', 5).
card_layout('ramosian sky marshal', 'normal').
card_power('ramosian sky marshal', 3).
card_toughness('ramosian sky marshal', 3).

% Found in: C13, C14, ZEN, pPRE
card_name('rampaging baloths', 'Rampaging Baloths').
card_type('rampaging baloths', 'Creature — Beast').
card_types('rampaging baloths', ['Creature']).
card_subtypes('rampaging baloths', ['Beast']).
card_colors('rampaging baloths', ['G']).
card_text('rampaging baloths', 'Trample\nLandfall — Whenever a land enters the battlefield under your control, you may put a 4/4 green Beast creature token onto the battlefield.').
card_mana_cost('rampaging baloths', ['4', 'G', 'G']).
card_cmc('rampaging baloths', 6).
card_layout('rampaging baloths', 'normal').
card_power('rampaging baloths', 6).
card_toughness('rampaging baloths', 6).

% Found in: ISD
card_name('rampaging werewolf', 'Rampaging Werewolf').
card_type('rampaging werewolf', 'Creature — Werewolf').
card_types('rampaging werewolf', ['Creature']).
card_subtypes('rampaging werewolf', ['Werewolf']).
card_colors('rampaging werewolf', ['R']).
card_text('rampaging werewolf', 'At the beginning of each upkeep, if a player cast two or more spells last turn, transform Rampaging Werewolf.').
card_layout('rampaging werewolf', 'double-faced').
card_power('rampaging werewolf', 6).
card_toughness('rampaging werewolf', 4).

% Found in: INV
card_name('rampant elephant', 'Rampant Elephant').
card_type('rampant elephant', 'Creature — Elephant').
card_types('rampant elephant', ['Creature']).
card_subtypes('rampant elephant', ['Elephant']).
card_colors('rampant elephant', ['W']).
card_text('rampant elephant', '{G}: Target creature blocks Rampant Elephant this turn if able.').
card_mana_cost('rampant elephant', ['3', 'W']).
card_cmc('rampant elephant', 4).
card_layout('rampant elephant', 'normal').
card_power('rampant elephant', 2).
card_toughness('rampant elephant', 2).

% Found in: 10E, 6ED, 7ED, 8ED, 9ED, BTD, DPA, HOP, M10, M12, MIR, MM2, TMP, TPR, pMPR
card_name('rampant growth', 'Rampant Growth').
card_type('rampant growth', 'Sorcery').
card_types('rampant growth', ['Sorcery']).
card_subtypes('rampant growth', []).
card_colors('rampant growth', ['G']).
card_text('rampant growth', 'Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.').
card_mana_cost('rampant growth', ['1', 'G']).
card_cmc('rampant growth', 2).
card_layout('rampant growth', 'normal').

% Found in: MMQ
card_name('rampart crawler', 'Rampart Crawler').
card_type('rampart crawler', 'Creature — Lizard Mercenary').
card_types('rampart crawler', ['Creature']).
card_subtypes('rampart crawler', ['Lizard', 'Mercenary']).
card_colors('rampart crawler', ['B']).
card_text('rampart crawler', 'Rampart Crawler can\'t be blocked by Walls.').
card_mana_cost('rampart crawler', ['B']).
card_cmc('rampart crawler', 1).
card_layout('rampart crawler', 'normal').
card_power('rampart crawler', 1).
card_toughness('rampart crawler', 1).

% Found in: ORI
card_name('ramroller', 'Ramroller').
card_type('ramroller', 'Artifact Creature — Juggernaut').
card_types('ramroller', ['Artifact', 'Creature']).
card_subtypes('ramroller', ['Juggernaut']).
card_colors('ramroller', []).
card_text('ramroller', 'Ramroller attacks each turn if able.\nRamroller gets +2/+0 as long as you control another artifact.').
card_mana_cost('ramroller', ['3']).
card_cmc('ramroller', 3).
card_layout('ramroller', 'normal').
card_power('ramroller', 2).
card_toughness('ramroller', 3).

% Found in: LEG, ME3
card_name('ramses overdark', 'Ramses Overdark').
card_type('ramses overdark', 'Legendary Creature — Human Assassin').
card_types('ramses overdark', ['Creature']).
card_subtypes('ramses overdark', ['Human', 'Assassin']).
card_supertypes('ramses overdark', ['Legendary']).
card_colors('ramses overdark', ['U', 'B']).
card_text('ramses overdark', '{T}: Destroy target enchanted creature.').
card_mana_cost('ramses overdark', ['2', 'U', 'U', 'B', 'B']).
card_cmc('ramses overdark', 6).
card_layout('ramses overdark', 'normal').
card_power('ramses overdark', 4).
card_toughness('ramses overdark', 3).
card_reserved('ramses overdark').

% Found in: TOR
card_name('rancid earth', 'Rancid Earth').
card_type('rancid earth', 'Sorcery').
card_types('rancid earth', ['Sorcery']).
card_subtypes('rancid earth', []).
card_colors('rancid earth', ['B']).
card_text('rancid earth', 'Destroy target land.\nThreshold — If seven or more cards are in your graveyard, instead destroy that land and Rancid Earth deals 1 damage to each creature and each player.').
card_mana_cost('rancid earth', ['1', 'B', 'B']).
card_cmc('rancid earth', 3).
card_layout('rancid earth', 'normal').

% Found in: ARC, DD3_GVL, DDD, M13, PC2, ULG, pFNM
card_name('rancor', 'Rancor').
card_type('rancor', 'Enchantment — Aura').
card_types('rancor', ['Enchantment']).
card_subtypes('rancor', ['Aura']).
card_colors('rancor', ['G']).
card_text('rancor', 'Enchant creature\nEnchanted creature gets +2/+0 and has trample.\nWhen Rancor is put into a graveyard from the battlefield, return Rancor to its owner\'s hand.').
card_mana_cost('rancor', ['G']).
card_cmc('rancor', 1).
card_layout('rancor', 'normal').

% Found in: ATH, TMP
card_name('ranger en-vec', 'Ranger en-Vec').
card_type('ranger en-vec', 'Creature — Human Soldier Archer').
card_types('ranger en-vec', ['Creature']).
card_subtypes('ranger en-vec', ['Human', 'Soldier', 'Archer']).
card_colors('ranger en-vec', ['W', 'G']).
card_text('ranger en-vec', 'First strike\n{G}: Regenerate Ranger en-Vec.').
card_mana_cost('ranger en-vec', ['1', 'G', 'W']).
card_cmc('ranger en-vec', 3).
card_layout('ranger en-vec', 'normal').
card_power('ranger en-vec', 2).
card_toughness('ranger en-vec', 2).

% Found in: ALA
card_name('ranger of eos', 'Ranger of Eos').
card_type('ranger of eos', 'Creature — Human Soldier').
card_types('ranger of eos', ['Creature']).
card_subtypes('ranger of eos', ['Human', 'Soldier']).
card_colors('ranger of eos', ['W']).
card_text('ranger of eos', 'When Ranger of Eos enters the battlefield, you may search your library for up to two creature cards with converted mana cost 1 or less, reveal them, and put them into your hand. If you do, shuffle your library.').
card_mana_cost('ranger of eos', ['3', 'W']).
card_cmc('ranger of eos', 4).
card_layout('ranger of eos', 'normal').
card_power('ranger of eos', 3).
card_toughness('ranger of eos', 2).

% Found in: ISD, M14, M15
card_name('ranger\'s guile', 'Ranger\'s Guile').
card_type('ranger\'s guile', 'Instant').
card_types('ranger\'s guile', ['Instant']).
card_subtypes('ranger\'s guile', []).
card_colors('ranger\'s guile', ['G']).
card_text('ranger\'s guile', 'Target creature you control gets +1/+1 and gains hexproof until end of turn. (It can\'t be the target of spells or abilities your opponents control.)').
card_mana_cost('ranger\'s guile', ['G']).
card_cmc('ranger\'s guile', 1).
card_layout('ranger\'s guile', 'normal').

% Found in: M13
card_name('ranger\'s path', 'Ranger\'s Path').
card_type('ranger\'s path', 'Sorcery').
card_types('ranger\'s path', ['Sorcery']).
card_subtypes('ranger\'s path', []).
card_colors('ranger\'s path', ['G']).
card_text('ranger\'s path', 'Search your library for up to two Forest cards and put them onto the battlefield tapped. Then shuffle your library.').
card_mana_cost('ranger\'s path', ['3', 'G']).
card_cmc('ranger\'s path', 4).
card_layout('ranger\'s path', 'normal').

% Found in: ULG
card_name('rank and file', 'Rank and File').
card_type('rank and file', 'Creature — Zombie').
card_types('rank and file', ['Creature']).
card_subtypes('rank and file', ['Zombie']).
card_colors('rank and file', ['B']).
card_text('rank and file', 'When Rank and File enters the battlefield, green creatures get -1/-1 until end of turn.').
card_mana_cost('rank and file', ['2', 'B', 'B']).
card_cmc('rank and file', 4).
card_layout('rank and file', 'normal').
card_power('rank and file', 3).
card_toughness('rank and file', 3).

% Found in: S99, STH
card_name('ransack', 'Ransack').
card_type('ransack', 'Sorcery').
card_types('ransack', ['Sorcery']).
card_subtypes('ransack', []).
card_colors('ransack', ['U']).
card_text('ransack', 'Look at the top five cards of target player\'s library. Put any number of them on the bottom of that library in any order and the rest on top of the library in any order.').
card_mana_cost('ransack', ['3', 'U']).
card_cmc('ransack', 4).
card_layout('ransack', 'normal').

% Found in: CMD, ROE
card_name('rapacious one', 'Rapacious One').
card_type('rapacious one', 'Creature — Eldrazi Drone').
card_types('rapacious one', ['Creature']).
card_subtypes('rapacious one', ['Eldrazi', 'Drone']).
card_colors('rapacious one', ['R']).
card_text('rapacious one', 'Trample\nWhenever Rapacious One deals combat damage to a player, put that many 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('rapacious one', ['5', 'R']).
card_cmc('rapacious one', 6).
card_layout('rapacious one', 'normal').
card_power('rapacious one', 5).
card_toughness('rapacious one', 4).

% Found in: UDS
card_name('rapid decay', 'Rapid Decay').
card_type('rapid decay', 'Instant').
card_types('rapid decay', ['Instant']).
card_subtypes('rapid decay', []).
card_colors('rapid decay', ['B']).
card_text('rapid decay', 'Exile up to three target cards from a single graveyard.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('rapid decay', ['1', 'B']).
card_cmc('rapid decay', 2).
card_layout('rapid decay', 'normal').

% Found in: LEG
card_name('rapid fire', 'Rapid Fire').
card_type('rapid fire', 'Instant').
card_types('rapid fire', ['Instant']).
card_subtypes('rapid fire', []).
card_colors('rapid fire', ['W']).
card_text('rapid fire', 'Cast Rapid Fire only before blockers are declared.\nTarget creature gains first strike until end of turn. If it doesn\'t have rampage, that creature gains rampage 2 until end of turn. (Whenever this creature becomes blocked, it gets +2/+2 until end of turn for each creature blocking it beyond the first.)').
card_mana_cost('rapid fire', ['3', 'W']).
card_cmc('rapid fire', 4).
card_layout('rapid fire', 'normal').
card_reserved('rapid fire').

% Found in: GTC
card_name('rapid hybridization', 'Rapid Hybridization').
card_type('rapid hybridization', 'Instant').
card_types('rapid hybridization', ['Instant']).
card_subtypes('rapid hybridization', []).
card_colors('rapid hybridization', ['U']).
card_text('rapid hybridization', 'Destroy target creature. It can\'t be regenerated. That creature\'s controller puts a 3/3 green Frog Lizard creature token onto the battlefield.').
card_mana_cost('rapid hybridization', ['U']).
card_cmc('rapid hybridization', 1).
card_layout('rapid hybridization', 'normal').

% Found in: MMQ
card_name('rappelling scouts', 'Rappelling Scouts').
card_type('rappelling scouts', 'Creature — Human Rebel Scout').
card_types('rappelling scouts', ['Creature']).
card_subtypes('rappelling scouts', ['Human', 'Rebel', 'Scout']).
card_colors('rappelling scouts', ['W']).
card_text('rappelling scouts', 'Flying\n{2}{W}: Rappelling Scouts gains protection from the color of your choice until end of turn.').
card_mana_cost('rappelling scouts', ['2', 'W', 'W']).
card_cmc('rappelling scouts', 4).
card_layout('rappelling scouts', 'normal').
card_power('rappelling scouts', 1).
card_toughness('rappelling scouts', 4).

% Found in: UNH
card_name('rare-b-gone', 'Rare-B-Gone').
card_type('rare-b-gone', 'Sorcery').
card_types('rare-b-gone', ['Sorcery']).
card_subtypes('rare-b-gone', []).
card_colors('rare-b-gone', ['B', 'R']).
card_text('rare-b-gone', 'Each player sacrifices all rare permanents, then reveals his or her hand and discards all rare cards.').
card_mana_cost('rare-b-gone', ['2', 'B', 'R']).
card_cmc('rare-b-gone', 4).
card_layout('rare-b-gone', 'normal').

% Found in: MIR
card_name('rashida scalebane', 'Rashida Scalebane').
card_type('rashida scalebane', 'Legendary Creature — Human Soldier').
card_types('rashida scalebane', ['Creature']).
card_subtypes('rashida scalebane', ['Human', 'Soldier']).
card_supertypes('rashida scalebane', ['Legendary']).
card_colors('rashida scalebane', ['W']).
card_text('rashida scalebane', '{T}: Destroy target attacking or blocking Dragon. It can\'t be regenerated. You gain life equal to its power.').
card_mana_cost('rashida scalebane', ['3', 'W', 'W']).
card_cmc('rashida scalebane', 5).
card_layout('rashida scalebane', 'normal').
card_power('rashida scalebane', 3).
card_toughness('rashida scalebane', 4).
card_reserved('rashida scalebane').

% Found in: HML
card_name('rashka the slayer', 'Rashka the Slayer').
card_type('rashka the slayer', 'Legendary Creature — Human Archer').
card_types('rashka the slayer', ['Creature']).
card_subtypes('rashka the slayer', ['Human', 'Archer']).
card_supertypes('rashka the slayer', ['Legendary']).
card_colors('rashka the slayer', ['W']).
card_text('rashka the slayer', 'Reach (This creature can block creatures with flying.)\nWhenever Rashka the Slayer blocks one or more black creatures, Rashka gets +1/+2 until end of turn.').
card_mana_cost('rashka the slayer', ['3', 'W', 'W']).
card_cmc('rashka the slayer', 5).
card_layout('rashka the slayer', 'normal').
card_power('rashka the slayer', 3).
card_toughness('rashka the slayer', 3).

% Found in: LEG, ME3
card_name('rasputin dreamweaver', 'Rasputin Dreamweaver').
card_type('rasputin dreamweaver', 'Legendary Creature — Human Wizard').
card_types('rasputin dreamweaver', ['Creature']).
card_subtypes('rasputin dreamweaver', ['Human', 'Wizard']).
card_supertypes('rasputin dreamweaver', ['Legendary']).
card_colors('rasputin dreamweaver', ['W', 'U']).
card_text('rasputin dreamweaver', 'Rasputin Dreamweaver enters the battlefield with seven dream counters on it.\nRemove a dream counter from Rasputin: Add {1} to your mana pool.\nRemove a dream counter from Rasputin: Prevent the next 1 damage that would be dealt to Rasputin this turn.\nAt the beginning of your upkeep, if Rasputin started the turn untapped, put a dream counter on it.\nRasputin can\'t have more than seven dream counters on it.').
card_mana_cost('rasputin dreamweaver', ['4', 'W', 'U']).
card_cmc('rasputin dreamweaver', 6).
card_layout('rasputin dreamweaver', 'normal').
card_power('rasputin dreamweaver', 4).
card_toughness('rasputin dreamweaver', 1).
card_reserved('rasputin dreamweaver').

% Found in: DIS
card_name('ratcatcher', 'Ratcatcher').
card_type('ratcatcher', 'Creature — Ogre Rogue').
card_types('ratcatcher', ['Creature']).
card_subtypes('ratcatcher', ['Ogre', 'Rogue']).
card_colors('ratcatcher', ['B']).
card_text('ratcatcher', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nAt the beginning of your upkeep, you may search your library for a Rat card, reveal it, and put it into your hand. If you do, shuffle your library.').
card_mana_cost('ratcatcher', ['4', 'B', 'B']).
card_cmc('ratcatcher', 6).
card_layout('ratcatcher', 'normal').
card_power('ratcatcher', 4).
card_toughness('ratcatcher', 4).

% Found in: M14, SOM, pMEI
card_name('ratchet bomb', 'Ratchet Bomb').
card_type('ratchet bomb', 'Artifact').
card_types('ratchet bomb', ['Artifact']).
card_subtypes('ratchet bomb', []).
card_colors('ratchet bomb', []).
card_text('ratchet bomb', '{T}: Put a charge counter on Ratchet Bomb.\n{T}, Sacrifice Ratchet Bomb: Destroy each nonland permanent with converted mana cost equal to the number of charge counters on Ratchet Bomb.').
card_mana_cost('ratchet bomb', ['2']).
card_cmc('ratchet bomb', 2).
card_layout('ratchet bomb', 'normal').

% Found in: NMS
card_name('rath\'s edge', 'Rath\'s Edge').
card_type('rath\'s edge', 'Legendary Land').
card_types('rath\'s edge', ['Land']).
card_subtypes('rath\'s edge', []).
card_supertypes('rath\'s edge', ['Legendary']).
card_colors('rath\'s edge', []).
card_text('rath\'s edge', '{T}: Add {1} to your mana pool.\n{4}, {T}, Sacrifice a land: Rath\'s Edge deals 1 damage to target creature or player.').
card_layout('rath\'s edge', 'normal').

% Found in: NMS, pPRE
card_name('rathi assassin', 'Rathi Assassin').
card_type('rathi assassin', 'Creature — Zombie Mercenary Assassin').
card_types('rathi assassin', ['Creature']).
card_subtypes('rathi assassin', ['Zombie', 'Mercenary', 'Assassin']).
card_colors('rathi assassin', ['B']).
card_text('rathi assassin', '{1}{B}{B}, {T}: Destroy target tapped nonblack creature.\n{3}, {T}: Search your library for a Mercenary permanent card with converted mana cost 3 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('rathi assassin', ['2', 'B', 'B']).
card_cmc('rathi assassin', 4).
card_layout('rathi assassin', 'normal').
card_power('rathi assassin', 2).
card_toughness('rathi assassin', 2).

% Found in: 9ED, TMP, TPR
card_name('rathi dragon', 'Rathi Dragon').
card_type('rathi dragon', 'Creature — Dragon').
card_types('rathi dragon', ['Creature']).
card_subtypes('rathi dragon', ['Dragon']).
card_colors('rathi dragon', ['R']).
card_text('rathi dragon', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhen Rathi Dragon enters the battlefield, sacrifice it unless you sacrifice two Mountains.').
card_mana_cost('rathi dragon', ['2', 'R', 'R']).
card_cmc('rathi dragon', 4).
card_layout('rathi dragon', 'normal').
card_power('rathi dragon', 5).
card_toughness('rathi dragon', 5).

% Found in: NMS
card_name('rathi fiend', 'Rathi Fiend').
card_type('rathi fiend', 'Creature — Horror Mercenary').
card_types('rathi fiend', ['Creature']).
card_subtypes('rathi fiend', ['Horror', 'Mercenary']).
card_colors('rathi fiend', ['B']).
card_text('rathi fiend', 'When Rathi Fiend enters the battlefield, each player loses 3 life.\n{3}, {T}: Search your library for a Mercenary permanent card with converted mana cost 3 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('rathi fiend', ['3', 'B']).
card_cmc('rathi fiend', 4).
card_layout('rathi fiend', 'normal').
card_power('rathi fiend', 2).
card_toughness('rathi fiend', 2).

% Found in: NMS
card_name('rathi intimidator', 'Rathi Intimidator').
card_type('rathi intimidator', 'Creature — Horror Mercenary').
card_types('rathi intimidator', ['Creature']).
card_subtypes('rathi intimidator', ['Horror', 'Mercenary']).
card_colors('rathi intimidator', ['B']).
card_text('rathi intimidator', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\n{2}, {T}: Search your library for a Mercenary permanent card with converted mana cost 2 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('rathi intimidator', ['1', 'B', 'B']).
card_cmc('rathi intimidator', 3).
card_layout('rathi intimidator', 'normal').
card_power('rathi intimidator', 2).
card_toughness('rathi intimidator', 1).

% Found in: MMA, PLC
card_name('rathi trapper', 'Rathi Trapper').
card_type('rathi trapper', 'Creature — Human Rebel Rogue').
card_types('rathi trapper', ['Creature']).
card_subtypes('rathi trapper', ['Human', 'Rebel', 'Rogue']).
card_colors('rathi trapper', ['B']).
card_text('rathi trapper', '{B}, {T}: Tap target creature.').
card_mana_cost('rathi trapper', ['1', 'B']).
card_cmc('rathi trapper', 2).
card_layout('rathi trapper', 'normal').
card_power('rathi trapper', 1).
card_toughness('rathi trapper', 2).

% Found in: TMP, TPR
card_name('rats of rath', 'Rats of Rath').
card_type('rats of rath', 'Creature — Rat').
card_types('rats of rath', ['Creature']).
card_subtypes('rats of rath', ['Rat']).
card_colors('rats of rath', ['B']).
card_text('rats of rath', '{B}: Destroy target artifact, creature, or land you control.').
card_mana_cost('rats of rath', ['1', 'B']).
card_cmc('rats of rath', 2).
card_layout('rats of rath', 'normal').
card_power('rats of rath', 2).
card_toughness('rats of rath', 1).

% Found in: JUD
card_name('rats\' feast', 'Rats\' Feast').
card_type('rats\' feast', 'Sorcery').
card_types('rats\' feast', ['Sorcery']).
card_subtypes('rats\' feast', []).
card_colors('rats\' feast', ['B']).
card_text('rats\' feast', 'Exile X target cards from a single graveyard.').
card_mana_cost('rats\' feast', ['X', 'B']).
card_cmc('rats\' feast', 1).
card_layout('rats\' feast', 'normal').

% Found in: SHM
card_name('rattleblaze scarecrow', 'Rattleblaze Scarecrow').
card_type('rattleblaze scarecrow', 'Artifact Creature — Scarecrow').
card_types('rattleblaze scarecrow', ['Artifact', 'Creature']).
card_subtypes('rattleblaze scarecrow', ['Scarecrow']).
card_colors('rattleblaze scarecrow', []).
card_text('rattleblaze scarecrow', 'Rattleblaze Scarecrow has persist as long as you control a black creature. (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)\nRattleblaze Scarecrow has haste as long as you control a red creature.').
card_mana_cost('rattleblaze scarecrow', ['6']).
card_cmc('rattleblaze scarecrow', 6).
card_layout('rattleblaze scarecrow', 'normal').
card_power('rattleblaze scarecrow', 5).
card_toughness('rattleblaze scarecrow', 3).

% Found in: KTK, pMEI, pPRE
card_name('rattleclaw mystic', 'Rattleclaw Mystic').
card_type('rattleclaw mystic', 'Creature — Human Shaman').
card_types('rattleclaw mystic', ['Creature']).
card_subtypes('rattleclaw mystic', ['Human', 'Shaman']).
card_colors('rattleclaw mystic', ['G']).
card_text('rattleclaw mystic', '{T}: Add {G}, {U}, or {R} to your mana pool.\nMorph {2} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Rattleclaw Mystic is turned face up, add {G}{U}{R} to your mana pool.').
card_mana_cost('rattleclaw mystic', ['1', 'G']).
card_cmc('rattleclaw mystic', 2).
card_layout('rattleclaw mystic', 'normal').
card_power('rattleclaw mystic', 2).
card_toughness('rattleclaw mystic', 1).

% Found in: ODY
card_name('ravaged highlands', 'Ravaged Highlands').
card_type('ravaged highlands', 'Land').
card_types('ravaged highlands', ['Land']).
card_subtypes('ravaged highlands', []).
card_colors('ravaged highlands', []).
card_text('ravaged highlands', 'Ravaged Highlands enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\n{T}, Sacrifice Ravaged Highlands: Add one mana of any color to your mana pool.').
card_layout('ravaged highlands', 'normal').

% Found in: DKA
card_name('ravager of the fells', 'Ravager of the Fells').
card_type('ravager of the fells', 'Creature — Werewolf').
card_types('ravager of the fells', ['Creature']).
card_subtypes('ravager of the fells', ['Werewolf']).
card_colors('ravager of the fells', ['R', 'G']).
card_text('ravager of the fells', 'Trample\nWhenever this creature transforms into Ravager of the Fells, it deals 2 damage to target opponent and 2 damage to up to one target creature that player controls.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Ravager of the Fells.').
card_layout('ravager of the fells', 'double-faced').
card_power('ravager of the fells', 4).
card_toughness('ravager of the fells', 4).

% Found in: ME2, PTK
card_name('ravages of war', 'Ravages of War').
card_type('ravages of war', 'Sorcery').
card_types('ravages of war', ['Sorcery']).
card_subtypes('ravages of war', []).
card_colors('ravages of war', ['W']).
card_text('ravages of war', 'Destroy all lands.').
card_mana_cost('ravages of war', ['3', 'W']).
card_cmc('ravages of war', 4).
card_layout('ravages of war', 'normal').

% Found in: ORI
card_name('ravaging blaze', 'Ravaging Blaze').
card_type('ravaging blaze', 'Instant').
card_types('ravaging blaze', ['Instant']).
card_subtypes('ravaging blaze', []).
card_colors('ravaging blaze', ['R']).
card_text('ravaging blaze', 'Ravaging Blaze deals X damage to target creature. \nSpell mastery — If there are two or more instant and/or sorcery cards in your graveyard, Ravaging Blaze also deals X damage to that creature\'s controller.').
card_mana_cost('ravaging blaze', ['X', 'R', 'R']).
card_cmc('ravaging blaze', 2).
card_layout('ravaging blaze', 'normal').

% Found in: PTK
card_name('ravaging horde', 'Ravaging Horde').
card_type('ravaging horde', 'Creature — Human Soldier').
card_types('ravaging horde', ['Creature']).
card_subtypes('ravaging horde', ['Human', 'Soldier']).
card_colors('ravaging horde', ['R']).
card_text('ravaging horde', 'When Ravaging Horde enters the battlefield, destroy target land.').
card_mana_cost('ravaging horde', ['3', 'R', 'R']).
card_cmc('ravaging horde', 5).
card_layout('ravaging horde', 'normal').
card_power('ravaging horde', 3).
card_toughness('ravaging horde', 3).

% Found in: FUT
card_name('ravaging riftwurm', 'Ravaging Riftwurm').
card_type('ravaging riftwurm', 'Creature — Wurm').
card_types('ravaging riftwurm', ['Creature']).
card_subtypes('ravaging riftwurm', ['Wurm']).
card_colors('ravaging riftwurm', ['G']).
card_text('ravaging riftwurm', 'Kicker {4} (You may pay an additional {4} as you cast this spell.)\nVanishing 2 (This permanent enters the battlefield with two time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nIf Ravaging Riftwurm was kicked, it enters the battlefield with three additional time counters on it.').
card_mana_cost('ravaging riftwurm', ['1', 'G', 'G']).
card_cmc('ravaging riftwurm', 3).
card_layout('ravaging riftwurm', 'normal').
card_power('ravaging riftwurm', 6).
card_toughness('ravaging riftwurm', 6).

% Found in: C13, ULG
card_name('raven familiar', 'Raven Familiar').
card_type('raven familiar', 'Creature — Bird').
card_types('raven familiar', ['Creature']).
card_subtypes('raven familiar', ['Bird']).
card_colors('raven familiar', ['U']).
card_text('raven familiar', 'Flying\nEcho {2}{U} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Raven Familiar enters the battlefield, look at the top three cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('raven familiar', ['2', 'U']).
card_cmc('raven familiar', 3).
card_layout('raven familiar', 'normal').
card_power('raven familiar', 1).
card_toughness('raven familiar', 2).

% Found in: SCG
card_name('raven guild initiate', 'Raven Guild Initiate').
card_type('raven guild initiate', 'Creature — Human Wizard').
card_types('raven guild initiate', ['Creature']).
card_subtypes('raven guild initiate', ['Human', 'Wizard']).
card_colors('raven guild initiate', ['U']).
card_text('raven guild initiate', 'Morph—Return a Bird you control to its owner\'s hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('raven guild initiate', ['2', 'U']).
card_cmc('raven guild initiate', 3).
card_layout('raven guild initiate', 'normal').
card_power('raven guild initiate', 1).
card_toughness('raven guild initiate', 4).

% Found in: SCG
card_name('raven guild master', 'Raven Guild Master').
card_type('raven guild master', 'Creature — Human Wizard Mutant').
card_types('raven guild master', ['Creature']).
card_subtypes('raven guild master', ['Human', 'Wizard', 'Mutant']).
card_colors('raven guild master', ['U']).
card_text('raven guild master', 'Whenever Raven Guild Master deals combat damage to a player, that player exiles the top ten cards of his or her library.\nMorph {2}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('raven guild master', ['1', 'U', 'U']).
card_cmc('raven guild master', 3).
card_layout('raven guild master', 'normal').
card_power('raven guild master', 1).
card_toughness('raven guild master', 1).

% Found in: EVE, MMA
card_name('raven\'s crime', 'Raven\'s Crime').
card_type('raven\'s crime', 'Sorcery').
card_types('raven\'s crime', ['Sorcery']).
card_subtypes('raven\'s crime', []).
card_colors('raven\'s crime', ['B']).
card_text('raven\'s crime', 'Target player discards a card.\nRetrace (You may cast this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_mana_cost('raven\'s crime', ['B']).
card_cmc('raven\'s crime', 1).
card_layout('raven\'s crime', 'normal').

% Found in: HOP
card_name('raven\'s run', 'Raven\'s Run').
card_type('raven\'s run', 'Plane — Shadowmoor').
card_types('raven\'s run', ['Plane']).
card_subtypes('raven\'s run', ['Shadowmoor']).
card_colors('raven\'s run', []).
card_text('raven\'s run', 'All creatures have wither. (They deal damage to creatures in the form of -1/-1 counters.)\nWhenever you roll {C}, put a -1/-1 counter on target creature, two -1/-1 counters on another target creature, and three -1/-1 counters on a third target creature.').
card_layout('raven\'s run', 'plane').

% Found in: SHM
card_name('raven\'s run dragoon', 'Raven\'s Run Dragoon').
card_type('raven\'s run dragoon', 'Creature — Elf Knight').
card_types('raven\'s run dragoon', ['Creature']).
card_subtypes('raven\'s run dragoon', ['Elf', 'Knight']).
card_colors('raven\'s run dragoon', ['W', 'G']).
card_text('raven\'s run dragoon', 'Raven\'s Run Dragoon can\'t be blocked by black creatures.').
card_mana_cost('raven\'s run dragoon', ['2', 'G/W', 'G/W']).
card_cmc('raven\'s run dragoon', 4).
card_layout('raven\'s run dragoon', 'normal').
card_power('raven\'s run dragoon', 3).
card_toughness('raven\'s run dragoon', 3).

% Found in: EXO
card_name('ravenous baboons', 'Ravenous Baboons').
card_type('ravenous baboons', 'Creature — Ape').
card_types('ravenous baboons', ['Creature']).
card_subtypes('ravenous baboons', ['Ape']).
card_colors('ravenous baboons', ['R']).
card_text('ravenous baboons', 'When Ravenous Baboons enters the battlefield, destroy target nonbasic land.').
card_mana_cost('ravenous baboons', ['3', 'R']).
card_cmc('ravenous baboons', 4).
card_layout('ravenous baboons', 'normal').
card_power('ravenous baboons', 2).
card_toughness('ravenous baboons', 2).

% Found in: C13, DD3_GVL, DDD, ONS, pJGP
card_name('ravenous baloth', 'Ravenous Baloth').
card_type('ravenous baloth', 'Creature — Beast').
card_types('ravenous baloth', ['Creature']).
card_subtypes('ravenous baloth', ['Beast']).
card_colors('ravenous baloth', ['G']).
card_text('ravenous baloth', 'Sacrifice a Beast: You gain 4 life.').
card_mana_cost('ravenous baloth', ['2', 'G', 'G']).
card_cmc('ravenous baloth', 4).
card_layout('ravenous baloth', 'normal').
card_power('ravenous baloth', 4).
card_toughness('ravenous baloth', 4).

% Found in: DKA, pPRE
card_name('ravenous demon', 'Ravenous Demon').
card_type('ravenous demon', 'Creature — Demon').
card_types('ravenous demon', ['Creature']).
card_subtypes('ravenous demon', ['Demon']).
card_colors('ravenous demon', ['B']).
card_text('ravenous demon', 'Sacrifice a Human: Transform Ravenous Demon. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('ravenous demon', ['3', 'B', 'B']).
card_cmc('ravenous demon', 5).
card_layout('ravenous demon', 'double-faced').
card_power('ravenous demon', 4).
card_toughness('ravenous demon', 4).
card_sides('ravenous demon', 'archdemon of greed').

% Found in: JOU
card_name('ravenous leucrocota', 'Ravenous Leucrocota').
card_type('ravenous leucrocota', 'Creature — Beast').
card_types('ravenous leucrocota', ['Creature']).
card_subtypes('ravenous leucrocota', ['Beast']).
card_colors('ravenous leucrocota', ['G']).
card_text('ravenous leucrocota', 'Vigilance\n{6}{G}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)').
card_mana_cost('ravenous leucrocota', ['3', 'G']).
card_cmc('ravenous leucrocota', 4).
card_layout('ravenous leucrocota', 'normal').
card_power('ravenous leucrocota', 2).
card_toughness('ravenous leucrocota', 4).

% Found in: 10E, 8ED, 9ED, DD3_GVL, DDD, DDJ, DPA, INV, M13, PO2, S99, UDS
card_name('ravenous rats', 'Ravenous Rats').
card_type('ravenous rats', 'Creature — Rat').
card_types('ravenous rats', ['Creature']).
card_subtypes('ravenous rats', ['Rat']).
card_colors('ravenous rats', ['B']).
card_text('ravenous rats', 'When Ravenous Rats enters the battlefield, target opponent discards a card.').
card_mana_cost('ravenous rats', ['1', 'B']).
card_cmc('ravenous rats', 2).
card_layout('ravenous rats', 'normal').
card_power('ravenous rats', 1).
card_toughness('ravenous rats', 1).

% Found in: USG
card_name('ravenous skirge', 'Ravenous Skirge').
card_type('ravenous skirge', 'Creature — Imp').
card_types('ravenous skirge', ['Creature']).
card_subtypes('ravenous skirge', ['Imp']).
card_colors('ravenous skirge', ['B']).
card_text('ravenous skirge', 'Flying\nWhenever Ravenous Skirge attacks, it gets +2/+0 until end of turn.').
card_mana_cost('ravenous skirge', ['2', 'B']).
card_cmc('ravenous skirge', 3).
card_layout('ravenous skirge', 'normal').
card_power('ravenous skirge', 1).
card_toughness('ravenous skirge', 1).

% Found in: ZEN
card_name('ravenous trap', 'Ravenous Trap').
card_type('ravenous trap', 'Instant — Trap').
card_types('ravenous trap', ['Instant']).
card_subtypes('ravenous trap', ['Trap']).
card_colors('ravenous trap', ['B']).
card_text('ravenous trap', 'If an opponent had three or more cards put into his or her graveyard from anywhere this turn, you may pay {0} rather than pay Ravenous Trap\'s mana cost.\nExile all cards from target player\'s graveyard.').
card_mana_cost('ravenous trap', ['2', 'B', 'B']).
card_cmc('ravenous trap', 4).
card_layout('ravenous trap', 'normal').

% Found in: MIR
card_name('ravenous vampire', 'Ravenous Vampire').
card_type('ravenous vampire', 'Creature — Vampire').
card_types('ravenous vampire', ['Creature']).
card_subtypes('ravenous vampire', ['Vampire']).
card_colors('ravenous vampire', ['B']).
card_text('ravenous vampire', 'Flying\nAt the beginning of your upkeep, you may sacrifice a nonartifact creature. If you do, put a +1/+1 counter on Ravenous Vampire. If you don\'t, tap Ravenous Vampire.').
card_mana_cost('ravenous vampire', ['3', 'B', 'B']).
card_cmc('ravenous vampire', 5).
card_layout('ravenous vampire', 'normal').
card_power('ravenous vampire', 3).
card_toughness('ravenous vampire', 3).

% Found in: C14
card_name('raving dead', 'Raving Dead').
card_type('raving dead', 'Creature — Zombie').
card_types('raving dead', ['Creature']).
card_subtypes('raving dead', ['Zombie']).
card_colors('raving dead', ['B']).
card_text('raving dead', 'Deathtouch\nAt the beginning of combat on your turn, choose an opponent at random. Raving Dead attacks that player this combat if able.\nWhenever Raving Dead deals combat damage to a player, that player loses half his or her life, rounded down.').
card_mana_cost('raving dead', ['4', 'B']).
card_cmc('raving dead', 5).
card_layout('raving dead', 'normal').
card_power('raving dead', 2).
card_toughness('raving dead', 6).

% Found in: SOK
card_name('raving oni-slave', 'Raving Oni-Slave').
card_type('raving oni-slave', 'Creature — Ogre Warrior').
card_types('raving oni-slave', ['Creature']).
card_subtypes('raving oni-slave', ['Ogre', 'Warrior']).
card_colors('raving oni-slave', ['B']).
card_text('raving oni-slave', 'When Raving Oni-Slave enters the battlefield or leaves the battlefield, you lose 3 life if you don\'t control a Demon.').
card_mana_cost('raving oni-slave', ['1', 'B']).
card_cmc('raving oni-slave', 2).
card_layout('raving oni-slave', 'normal').
card_power('raving oni-slave', 3).
card_toughness('raving oni-slave', 3).

% Found in: 5ED, BRB, CMD, DDM, ICE, ME2, MIR
card_name('ray of command', 'Ray of Command').
card_type('ray of command', 'Instant').
card_types('ray of command', ['Instant']).
card_subtypes('ray of command', []).
card_colors('ray of command', ['U']).
card_text('ray of command', 'Untap target creature an opponent controls and gain control of it until end of turn. That creature gains haste until end of turn. When you lose control of the creature, tap it.').
card_mana_cost('ray of command', ['3', 'U']).
card_cmc('ray of command', 4).
card_layout('ray of command', 'normal').

% Found in: THS
card_name('ray of dissolution', 'Ray of Dissolution').
card_type('ray of dissolution', 'Instant').
card_types('ray of dissolution', ['Instant']).
card_subtypes('ray of dissolution', []).
card_colors('ray of dissolution', ['W']).
card_text('ray of dissolution', 'Destroy target enchantment. You gain 3 life.').
card_mana_cost('ray of dissolution', ['2', 'W']).
card_cmc('ray of dissolution', 3).
card_layout('ray of dissolution', 'normal').

% Found in: ODY
card_name('ray of distortion', 'Ray of Distortion').
card_type('ray of distortion', 'Instant').
card_types('ray of distortion', ['Instant']).
card_subtypes('ray of distortion', []).
card_colors('ray of distortion', ['W']).
card_text('ray of distortion', 'Destroy target artifact or enchantment.\nFlashback {4}{W}{W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('ray of distortion', ['3', 'W']).
card_cmc('ray of distortion', 4).
card_layout('ray of distortion', 'normal').

% Found in: ICE
card_name('ray of erasure', 'Ray of Erasure').
card_type('ray of erasure', 'Instant').
card_types('ray of erasure', ['Instant']).
card_subtypes('ray of erasure', []).
card_colors('ray of erasure', ['U']).
card_text('ray of erasure', 'Target player puts the top card of his or her library into his or her graveyard.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('ray of erasure', ['U']).
card_cmc('ray of erasure', 1).
card_layout('ray of erasure', 'normal').

% Found in: DKA, JUD
card_name('ray of revelation', 'Ray of Revelation').
card_type('ray of revelation', 'Instant').
card_types('ray of revelation', ['Instant']).
card_subtypes('ray of revelation', []).
card_colors('ray of revelation', ['W']).
card_text('ray of revelation', 'Destroy target enchantment.\nFlashback {G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('ray of revelation', ['1', 'W']).
card_cmc('ray of revelation', 2).
card_layout('ray of revelation', 'normal').

% Found in: UDS
card_name('rayne, academy chancellor', 'Rayne, Academy Chancellor').
card_type('rayne, academy chancellor', 'Legendary Creature — Human Wizard').
card_types('rayne, academy chancellor', ['Creature']).
card_subtypes('rayne, academy chancellor', ['Human', 'Wizard']).
card_supertypes('rayne, academy chancellor', ['Legendary']).
card_colors('rayne, academy chancellor', ['U']).
card_text('rayne, academy chancellor', 'Whenever you or a permanent you control becomes the target of a spell or ability an opponent controls, you may draw a card. You may draw an additional card if Rayne, Academy Chancellor is enchanted.').
card_mana_cost('rayne, academy chancellor', ['2', 'U']).
card_cmc('rayne, academy chancellor', 3).
card_layout('rayne, academy chancellor', 'normal').
card_power('rayne, academy chancellor', 1).
card_toughness('rayne, academy chancellor', 1).

% Found in: USG
card_name('raze', 'Raze').
card_type('raze', 'Sorcery').
card_types('raze', ['Sorcery']).
card_subtypes('raze', []).
card_colors('raze', ['R']).
card_text('raze', 'As an additional cost to cast Raze, sacrifice a land.\nDestroy target land.').
card_mana_cost('raze', ['R']).
card_cmc('raze', 1).
card_layout('raze', 'normal').

% Found in: RAV
card_name('razia\'s purification', 'Razia\'s Purification').
card_type('razia\'s purification', 'Sorcery').
card_types('razia\'s purification', ['Sorcery']).
card_subtypes('razia\'s purification', []).
card_colors('razia\'s purification', ['W', 'R']).
card_text('razia\'s purification', 'Each player chooses three permanents he or she controls, then sacrifices the rest.').
card_mana_cost('razia\'s purification', ['4', 'R', 'W']).
card_cmc('razia\'s purification', 6).
card_layout('razia\'s purification', 'normal').

% Found in: HOP, RAV
card_name('razia, boros archangel', 'Razia, Boros Archangel').
card_type('razia, boros archangel', 'Legendary Creature — Angel').
card_types('razia, boros archangel', ['Creature']).
card_subtypes('razia, boros archangel', ['Angel']).
card_supertypes('razia, boros archangel', ['Legendary']).
card_colors('razia, boros archangel', ['W', 'R']).
card_text('razia, boros archangel', 'Flying, vigilance, haste\n{T}: The next 3 damage that would be dealt to target creature you control this turn is dealt to another target creature instead.').
card_mana_cost('razia, boros archangel', ['4', 'R', 'R', 'W', 'W']).
card_cmc('razia, boros archangel', 8).
card_layout('razia, boros archangel', 'normal').
card_power('razia, boros archangel', 6).
card_toughness('razia, boros archangel', 3).

% Found in: PLS
card_name('razing snidd', 'Razing Snidd').
card_type('razing snidd', 'Creature — Beast').
card_types('razing snidd', ['Creature']).
card_subtypes('razing snidd', ['Beast']).
card_colors('razing snidd', ['B', 'R']).
card_text('razing snidd', 'When Razing Snidd enters the battlefield, return a black or red creature you control to its owner\'s hand.\nWhen Razing Snidd enters the battlefield, each player sacrifices a land.').
card_mana_cost('razing snidd', ['4', 'B', 'R']).
card_cmc('razing snidd', 6).
card_layout('razing snidd', 'normal').
card_power('razing snidd', 3).
card_toughness('razing snidd', 3).

% Found in: DDF, MRD
card_name('razor barrier', 'Razor Barrier').
card_type('razor barrier', 'Instant').
card_types('razor barrier', ['Instant']).
card_subtypes('razor barrier', []).
card_colors('razor barrier', ['W']).
card_text('razor barrier', 'Target permanent you control gains protection from artifacts or from the color of your choice until end of turn.').
card_mana_cost('razor barrier', ['1', 'W']).
card_cmc('razor barrier', 2).
card_layout('razor barrier', 'normal').

% Found in: WWK
card_name('razor boomerang', 'Razor Boomerang').
card_type('razor boomerang', 'Artifact — Equipment').
card_types('razor boomerang', ['Artifact']).
card_subtypes('razor boomerang', ['Equipment']).
card_colors('razor boomerang', []).
card_text('razor boomerang', 'Equipped creature has \"{T}, Unattach Razor Boomerang: Razor Boomerang deals 1 damage to target creature or player. Return Razor Boomerang to its owner\'s hand.\"\nEquip {2}').
card_mana_cost('razor boomerang', ['3']).
card_cmc('razor boomerang', 3).
card_layout('razor boomerang', 'normal').

% Found in: DST
card_name('razor golem', 'Razor Golem').
card_type('razor golem', 'Artifact Creature — Golem').
card_types('razor golem', ['Artifact', 'Creature']).
card_subtypes('razor golem', ['Golem']).
card_colors('razor golem', []).
card_text('razor golem', 'Affinity for Plains (This spell costs {1} less to cast for each Plains you control.)\nVigilance').
card_mana_cost('razor golem', ['6']).
card_cmc('razor golem', 6).
card_layout('razor golem', 'normal').
card_power('razor golem', 3).
card_toughness('razor golem', 4).

% Found in: C13, SOM
card_name('razor hippogriff', 'Razor Hippogriff').
card_type('razor hippogriff', 'Creature — Hippogriff').
card_types('razor hippogriff', ['Creature']).
card_subtypes('razor hippogriff', ['Hippogriff']).
card_colors('razor hippogriff', ['W']).
card_text('razor hippogriff', 'Flying\nWhen Razor Hippogriff enters the battlefield, return target artifact card from your graveyard to your hand. You gain life equal to that card\'s converted mana cost.').
card_mana_cost('razor hippogriff', ['3', 'W', 'W']).
card_cmc('razor hippogriff', 5).
card_layout('razor hippogriff', 'normal').
card_power('razor hippogriff', 3).
card_toughness('razor hippogriff', 3).

% Found in: MIR
card_name('razor pendulum', 'Razor Pendulum').
card_type('razor pendulum', 'Artifact').
card_types('razor pendulum', ['Artifact']).
card_subtypes('razor pendulum', []).
card_colors('razor pendulum', []).
card_text('razor pendulum', 'At the beginning of each player\'s end step, if that player has 5 or less life, Razor Pendulum deals 2 damage to him or her.').
card_mana_cost('razor pendulum', ['4']).
card_cmc('razor pendulum', 4).
card_layout('razor pendulum', 'normal').
card_reserved('razor pendulum').

% Found in: NPH
card_name('razor swine', 'Razor Swine').
card_type('razor swine', 'Creature — Boar').
card_types('razor swine', ['Creature']).
card_subtypes('razor swine', ['Boar']).
card_colors('razor swine', ['R']).
card_text('razor swine', 'First strike\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('razor swine', ['2', 'R']).
card_cmc('razor swine', 3).
card_layout('razor swine', 'normal').
card_power('razor swine', 2).
card_toughness('razor swine', 1).

% Found in: PO2
card_name('razorclaw bear', 'Razorclaw Bear').
card_type('razorclaw bear', 'Creature — Bear').
card_types('razorclaw bear', ['Creature']).
card_subtypes('razorclaw bear', ['Bear']).
card_colors('razorclaw bear', ['G']).
card_text('razorclaw bear', 'Whenever Razorclaw Bear becomes blocked, it gets +2/+2 until end of turn.').
card_mana_cost('razorclaw bear', ['2', 'G', 'G']).
card_cmc('razorclaw bear', 4).
card_layout('razorclaw bear', 'normal').
card_power('razorclaw bear', 3).
card_toughness('razorclaw bear', 3).

% Found in: MBS
card_name('razorfield rhino', 'Razorfield Rhino').
card_type('razorfield rhino', 'Artifact Creature — Rhino').
card_types('razorfield rhino', ['Artifact', 'Creature']).
card_subtypes('razorfield rhino', ['Rhino']).
card_colors('razorfield rhino', []).
card_text('razorfield rhino', 'Metalcraft — Razorfield Rhino gets +2/+2 as long as you control three or more artifacts.').
card_mana_cost('razorfield rhino', ['6']).
card_cmc('razorfield rhino', 6).
card_layout('razorfield rhino', 'normal').
card_power('razorfield rhino', 4).
card_toughness('razorfield rhino', 4).

% Found in: SOM
card_name('razorfield thresher', 'Razorfield Thresher').
card_type('razorfield thresher', 'Artifact Creature — Construct').
card_types('razorfield thresher', ['Artifact', 'Creature']).
card_subtypes('razorfield thresher', ['Construct']).
card_colors('razorfield thresher', []).
card_text('razorfield thresher', '').
card_mana_cost('razorfield thresher', ['7']).
card_cmc('razorfield thresher', 7).
card_layout('razorfield thresher', 'normal').
card_power('razorfield thresher', 6).
card_toughness('razorfield thresher', 4).

% Found in: EVE
card_name('razorfin abolisher', 'Razorfin Abolisher').
card_type('razorfin abolisher', 'Creature — Merfolk Wizard').
card_types('razorfin abolisher', ['Creature']).
card_subtypes('razorfin abolisher', ['Merfolk', 'Wizard']).
card_colors('razorfin abolisher', ['U']).
card_text('razorfin abolisher', '{1}{U}, {T}: Return target creature with a counter on it to its owner\'s hand.').
card_mana_cost('razorfin abolisher', ['2', 'U']).
card_cmc('razorfin abolisher', 3).
card_layout('razorfin abolisher', 'normal').
card_power('razorfin abolisher', 2).
card_toughness('razorfin abolisher', 2).

% Found in: APC
card_name('razorfin hunter', 'Razorfin Hunter').
card_type('razorfin hunter', 'Creature — Merfolk Goblin').
card_types('razorfin hunter', ['Creature']).
card_subtypes('razorfin hunter', ['Merfolk', 'Goblin']).
card_colors('razorfin hunter', ['U', 'R']).
card_text('razorfin hunter', '{T}: Razorfin Hunter deals 1 damage to target creature or player.').
card_mana_cost('razorfin hunter', ['U', 'R']).
card_cmc('razorfin hunter', 2).
card_layout('razorfin hunter', 'normal').
card_power('razorfin hunter', 1).
card_toughness('razorfin hunter', 1).

% Found in: 7ED, 8ED, INV, M10, M15
card_name('razorfoot griffin', 'Razorfoot Griffin').
card_type('razorfoot griffin', 'Creature — Griffin').
card_types('razorfoot griffin', ['Creature']).
card_subtypes('razorfoot griffin', ['Griffin']).
card_colors('razorfoot griffin', ['W']).
card_text('razorfoot griffin', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_mana_cost('razorfoot griffin', ['3', 'W']).
card_cmc('razorfoot griffin', 4).
card_layout('razorfoot griffin', 'normal').
card_power('razorfoot griffin', 2).
card_toughness('razorfoot griffin', 2).

% Found in: 5DN
card_name('razorgrass screen', 'Razorgrass Screen').
card_type('razorgrass screen', 'Artifact Creature — Wall').
card_types('razorgrass screen', ['Artifact', 'Creature']).
card_subtypes('razorgrass screen', ['Wall']).
card_colors('razorgrass screen', []).
card_text('razorgrass screen', 'Defender (This creature can\'t attack.)\nRazorgrass Screen blocks each turn if able.').
card_mana_cost('razorgrass screen', ['1']).
card_cmc('razorgrass screen', 1).
card_layout('razorgrass screen', 'normal').
card_power('razorgrass screen', 2).
card_toughness('razorgrass screen', 1).

% Found in: CMD, SOK
card_name('razorjaw oni', 'Razorjaw Oni').
card_type('razorjaw oni', 'Creature — Demon Spirit').
card_types('razorjaw oni', ['Creature']).
card_subtypes('razorjaw oni', ['Demon', 'Spirit']).
card_colors('razorjaw oni', ['B']).
card_text('razorjaw oni', 'Black creatures can\'t block.').
card_mana_cost('razorjaw oni', ['3', 'B']).
card_cmc('razorjaw oni', 4).
card_layout('razorjaw oni', 'normal').
card_power('razorjaw oni', 4).
card_toughness('razorjaw oni', 5).

% Found in: 10E, 5DN, DDF
card_name('razormane masticore', 'Razormane Masticore').
card_type('razormane masticore', 'Artifact Creature — Masticore').
card_types('razormane masticore', ['Artifact', 'Creature']).
card_subtypes('razormane masticore', ['Masticore']).
card_colors('razormane masticore', []).
card_text('razormane masticore', 'First strike (This creature deals combat damage before creatures without first strike.)\nAt the beginning of your upkeep, sacrifice Razormane Masticore unless you discard a card.\nAt the beginning of your draw step, you may have Razormane Masticore deal 3 damage to target creature.').
card_mana_cost('razormane masticore', ['5']).
card_cmc('razormane masticore', 5).
card_layout('razormane masticore', 'normal').
card_power('razormane masticore', 5).
card_toughness('razormane masticore', 5).

% Found in: GTC
card_name('razortip whip', 'Razortip Whip').
card_type('razortip whip', 'Artifact').
card_types('razortip whip', ['Artifact']).
card_subtypes('razortip whip', []).
card_colors('razortip whip', []).
card_text('razortip whip', '{1}, {T}: Razortip Whip deals 1 damage to target opponent.').
card_mana_cost('razortip whip', ['2']).
card_cmc('razortip whip', 2).
card_layout('razortip whip', 'normal').

% Found in: 6ED, 7ED, 9ED, WTH
card_name('razortooth rats', 'Razortooth Rats').
card_type('razortooth rats', 'Creature — Rat').
card_types('razortooth rats', ['Creature']).
card_subtypes('razortooth rats', ['Rat']).
card_colors('razortooth rats', ['B']).
card_text('razortooth rats', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('razortooth rats', ['2', 'B']).
card_cmc('razortooth rats', 3).
card_layout('razortooth rats', 'normal').
card_power('razortooth rats', 2).
card_toughness('razortooth rats', 1).

% Found in: SOM
card_name('razorverge thicket', 'Razorverge Thicket').
card_type('razorverge thicket', 'Land').
card_types('razorverge thicket', ['Land']).
card_subtypes('razorverge thicket', []).
card_colors('razorverge thicket', []).
card_text('razorverge thicket', 'Razorverge Thicket enters the battlefield tapped unless you control two or fewer other lands.\n{T}: Add {G} or {W} to your mana pool.').
card_layout('razorverge thicket', 'normal').

% Found in: MMA, MOR
card_name('reach of branches', 'Reach of Branches').
card_type('reach of branches', 'Tribal Instant — Treefolk').
card_types('reach of branches', ['Tribal', 'Instant']).
card_subtypes('reach of branches', ['Treefolk']).
card_colors('reach of branches', ['G']).
card_text('reach of branches', 'Put a 2/5 green Treefolk Shaman creature token onto the battlefield.\nWhenever a Forest enters the battlefield under your control, you may return Reach of Branches from your graveyard to your hand.').
card_mana_cost('reach of branches', ['4', 'G']).
card_cmc('reach of branches', 5).
card_layout('reach of branches', 'normal').

% Found in: FRF
card_name('reach of shadows', 'Reach of Shadows').
card_type('reach of shadows', 'Instant').
card_types('reach of shadows', ['Instant']).
card_subtypes('reach of shadows', []).
card_colors('reach of shadows', ['B']).
card_text('reach of shadows', 'Destroy target creature that\'s one or more colors.').
card_mana_cost('reach of shadows', ['4', 'B']).
card_cmc('reach of shadows', 5).
card_layout('reach of shadows', 'normal').

% Found in: CHK, MMA
card_name('reach through mists', 'Reach Through Mists').
card_type('reach through mists', 'Instant — Arcane').
card_types('reach through mists', ['Instant']).
card_subtypes('reach through mists', ['Arcane']).
card_colors('reach through mists', ['U']).
card_text('reach through mists', 'Draw a card.').
card_mana_cost('reach through mists', ['U']).
card_cmc('reach through mists', 1).
card_layout('reach through mists', 'normal').

% Found in: C14, DDP, ORI, THS
card_name('read the bones', 'Read the Bones').
card_type('read the bones', 'Sorcery').
card_types('read the bones', ['Sorcery']).
card_subtypes('read the bones', []).
card_colors('read the bones', ['B']).
card_text('read the bones', 'Scry 2, then draw two cards. You lose 2 life. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('read the bones', ['2', 'B']).
card_cmc('read the bones', 3).
card_layout('read the bones', 'normal').

% Found in: ONS
card_name('read the runes', 'Read the Runes').
card_type('read the runes', 'Instant').
card_types('read the runes', ['Instant']).
card_subtypes('read the runes', []).
card_colors('read the runes', ['U']).
card_text('read the runes', 'Draw X cards. For each card drawn this way, discard a card unless you sacrifice a permanent.').
card_mana_cost('read the runes', ['X', 'U']).
card_cmc('read the runes', 1).
card_layout('read the runes', 'normal').

% Found in: DGM
card_name('ready', 'Ready').
card_type('ready', 'Instant').
card_types('ready', ['Instant']).
card_subtypes('ready', []).
card_colors('ready', ['W', 'G']).
card_text('ready', 'Creatures you control gain indestructible until end of turn. Untap each creature you control.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('ready', ['1', 'G', 'W']).
card_cmc('ready', 3).
card_layout('ready', 'split').
card_sides('ready', 'willing').

% Found in: APC
card_name('reality', 'Reality').
card_type('reality', 'Instant').
card_types('reality', ['Instant']).
card_subtypes('reality', []).
card_colors('reality', ['G']).
card_text('reality', 'Destroy target artifact.').
card_mana_cost('reality', ['2', 'G']).
card_cmc('reality', 3).
card_layout('reality', 'split').

% Found in: PLC
card_name('reality acid', 'Reality Acid').
card_type('reality acid', 'Enchantment — Aura').
card_types('reality acid', ['Enchantment']).
card_subtypes('reality acid', ['Aura']).
card_colors('reality acid', ['U']).
card_text('reality acid', 'Enchant permanent\nVanishing 3 (This permanent enters the battlefield with three time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Reality Acid leaves the battlefield, enchanted permanent\'s controller sacrifices it.').
card_mana_cost('reality acid', ['2', 'U']).
card_cmc('reality acid', 3).
card_layout('reality acid', 'normal').

% Found in: TMP, TPR
card_name('reality anchor', 'Reality Anchor').
card_type('reality anchor', 'Instant').
card_types('reality anchor', ['Instant']).
card_subtypes('reality anchor', []).
card_colors('reality anchor', ['G']).
card_text('reality anchor', 'Target creature loses shadow until end of turn.\nDraw a card.').
card_mana_cost('reality anchor', ['1', 'G']).
card_cmc('reality anchor', 2).
card_layout('reality anchor', 'normal').

% Found in: MIR
card_name('reality ripple', 'Reality Ripple').
card_type('reality ripple', 'Instant').
card_types('reality ripple', ['Instant']).
card_subtypes('reality ripple', []).
card_colors('reality ripple', ['U']).
card_text('reality ripple', 'Target artifact, creature, or land phases out. (While it\'s phased out, it\'s treated as though it doesn\'t exist. It phases in before its controller untaps during his or her next untap step.)').
card_mana_cost('reality ripple', ['1', 'U']).
card_cmc('reality ripple', 2).
card_layout('reality ripple', 'normal').

% Found in: PC2
card_name('reality shaping', 'Reality Shaping').
card_type('reality shaping', 'Phenomenon').
card_types('reality shaping', ['Phenomenon']).
card_subtypes('reality shaping', []).
card_colors('reality shaping', []).
card_text('reality shaping', 'When you encounter Reality Shaping, starting with you, each player may put a permanent card from his or her hand onto the battlefield. (Then planeswalk away from this phenomenon.)').
card_layout('reality shaping', 'phenomenon').

% Found in: FRF, FRF_UGIN
card_name('reality shift', 'Reality Shift').
card_type('reality shift', 'Instant').
card_types('reality shift', ['Instant']).
card_subtypes('reality shift', []).
card_colors('reality shift', ['U']).
card_text('reality shift', 'Exile target creature. Its controller manifests the top card of his or her library. (That player puts the top card of his or her library onto the battlefield face down as a 2/2 creature. If it\'s a creature card, it can be turned face up any time for its mana cost.)').
card_mana_cost('reality shift', ['1', 'U']).
card_cmc('reality shift', 2).
card_layout('reality shift', 'normal').

% Found in: ROE
card_name('reality spasm', 'Reality Spasm').
card_type('reality spasm', 'Instant').
card_types('reality spasm', ['Instant']).
card_subtypes('reality spasm', []).
card_colors('reality spasm', ['U']).
card_text('reality spasm', 'Choose one —\n• Tap X target permanents.\n• Untap X target permanents.').
card_mana_cost('reality spasm', ['X', 'U', 'U']).
card_cmc('reality spasm', 2).
card_layout('reality spasm', 'normal').

% Found in: FUT
card_name('reality strobe', 'Reality Strobe').
card_type('reality strobe', 'Sorcery').
card_types('reality strobe', ['Sorcery']).
card_subtypes('reality strobe', []).
card_colors('reality strobe', ['U']).
card_text('reality strobe', 'Return target permanent to its owner\'s hand. Exile Reality Strobe with three time counters on it.\nSuspend 3—{2}{U} (Rather than cast this card from your hand, you may pay {2}{U} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_mana_cost('reality strobe', ['4', 'U', 'U']).
card_cmc('reality strobe', 6).
card_layout('reality strobe', 'normal').

% Found in: ICE
card_name('reality twist', 'Reality Twist').
card_type('reality twist', 'Enchantment').
card_types('reality twist', ['Enchantment']).
card_subtypes('reality twist', []).
card_colors('reality twist', ['U']).
card_text('reality twist', 'Cumulative upkeep {1}{U}{U} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nIf tapped for mana, Plains produce {R}, Swamps produce {G}, Mountains produce {W}, and Forests produce {B} instead of any other type.').
card_mana_cost('reality twist', ['U', 'U', 'U']).
card_cmc('reality twist', 3).
card_layout('reality twist', 'normal').
card_reserved('reality twist').

% Found in: ALA
card_name('realm razer', 'Realm Razer').
card_type('realm razer', 'Creature — Beast').
card_types('realm razer', ['Creature']).
card_subtypes('realm razer', ['Beast']).
card_colors('realm razer', ['W', 'R', 'G']).
card_text('realm razer', 'When Realm Razer enters the battlefield, exile all lands.\nWhen Realm Razer leaves the battlefield, return the exiled cards to the battlefield tapped under their owners\' control.').
card_mana_cost('realm razer', ['3', 'R', 'G', 'W']).
card_cmc('realm razer', 6).
card_layout('realm razer', 'normal').
card_power('realm razer', 4).
card_toughness('realm razer', 2).

% Found in: CNS, VMA
card_name('realm seekers', 'Realm Seekers').
card_type('realm seekers', 'Creature — Elf Scout').
card_types('realm seekers', ['Creature']).
card_subtypes('realm seekers', ['Elf', 'Scout']).
card_colors('realm seekers', ['G']).
card_text('realm seekers', 'Realm Seekers enters the battlefield with X +1/+1 counters on it, where X is the total number of cards in all players\' hands.\n{2}{G}, Remove a +1/+1 counter from Realm Seekers: Search your library for a land card, reveal it, put it into your hand, then shuffle your library.').
card_mana_cost('realm seekers', ['4', 'G', 'G']).
card_cmc('realm seekers', 6).
card_layout('realm seekers', 'normal').
card_power('realm seekers', 0).
card_toughness('realm seekers', 0).

% Found in: ARC
card_name('realms befitting my majesty', 'Realms Befitting My Majesty').
card_type('realms befitting my majesty', 'Scheme').
card_types('realms befitting my majesty', ['Scheme']).
card_subtypes('realms befitting my majesty', []).
card_colors('realms befitting my majesty', []).
card_text('realms befitting my majesty', 'When you set this scheme in motion, search your library for up to two basic land cards, put them onto the battlefield tapped, then shuffle your library.').
card_layout('realms befitting my majesty', 'scheme').

% Found in: ROE
card_name('realms uncharted', 'Realms Uncharted').
card_type('realms uncharted', 'Instant').
card_types('realms uncharted', ['Instant']).
card_subtypes('realms uncharted', []).
card_colors('realms uncharted', ['G']).
card_text('realms uncharted', 'Search your library for up to four land cards with different names and reveal them. An opponent chooses two of those cards. Put the chosen cards into your graveyard and the rest into your hand. Then shuffle your library.').
card_mana_cost('realms uncharted', ['2', 'G']).
card_cmc('realms uncharted', 3).
card_layout('realms uncharted', 'normal').

% Found in: GTC
card_name('realmwright', 'Realmwright').
card_type('realmwright', 'Creature — Vedalken Wizard').
card_types('realmwright', ['Creature']).
card_subtypes('realmwright', ['Vedalken', 'Wizard']).
card_colors('realmwright', ['U']).
card_text('realmwright', 'As Realmwright enters the battlefield, choose a basic land type.\nLands you control are the chosen type in addition to their other types.').
card_mana_cost('realmwright', ['U']).
card_cmc('realmwright', 1).
card_layout('realmwright', 'normal').
card_power('realmwright', 1).
card_toughness('realmwright', 1).

% Found in: ARC, BRB, PD3, TMP, TPR, VMA, pFNM
card_name('reanimate', 'Reanimate').
card_type('reanimate', 'Sorcery').
card_types('reanimate', ['Sorcery']).
card_subtypes('reanimate', []).
card_colors('reanimate', ['B']).
card_text('reanimate', 'Put target creature card from a graveyard onto the battlefield under your control. You lose life equal to its converted mana cost.').
card_mana_cost('reanimate', ['B']).
card_cmc('reanimate', 1).
card_layout('reanimate', 'normal').

% Found in: TMP
card_name('reap', 'Reap').
card_type('reap', 'Instant').
card_types('reap', ['Instant']).
card_subtypes('reap', []).
card_colors('reap', ['G']).
card_text('reap', 'Return up to X target cards from your graveyard to your hand, where X is the number of black permanents target opponent controls as you cast Reap.').
card_mana_cost('reap', ['1', 'G']).
card_cmc('reap', 2).
card_layout('reap', 'normal').

% Found in: DST
card_name('reap and sow', 'Reap and Sow').
card_type('reap and sow', 'Sorcery').
card_types('reap and sow', ['Sorcery']).
card_subtypes('reap and sow', []).
card_colors('reap and sow', ['G']).
card_text('reap and sow', 'Choose one —\n• Destroy target land.\n• Search your library for a land card, put that card onto the battlefield, then shuffle your library.\nEntwine {1}{G} (Choose both if you pay the entwine cost.)').
card_mana_cost('reap and sow', ['3', 'G']).
card_cmc('reap and sow', 4).
card_layout('reap and sow', 'normal').

% Found in: DGM
card_name('reap intellect', 'Reap Intellect').
card_type('reap intellect', 'Sorcery').
card_types('reap intellect', ['Sorcery']).
card_subtypes('reap intellect', []).
card_colors('reap intellect', ['U', 'B']).
card_text('reap intellect', 'Target opponent reveals his or her hand. You choose up to X nonland cards from it and exile them. For each card exiled this way, search that player\'s graveyard, hand, and library for any number of cards with the same name as that card and exile them. Then that player shuffles his or her library.').
card_mana_cost('reap intellect', ['X', '2', 'U', 'B']).
card_cmc('reap intellect', 4).
card_layout('reap intellect', 'normal').

% Found in: DKA
card_name('reap the seagraf', 'Reap the Seagraf').
card_type('reap the seagraf', 'Sorcery').
card_types('reap the seagraf', ['Sorcery']).
card_subtypes('reap the seagraf', []).
card_colors('reap the seagraf', ['B']).
card_text('reap the seagraf', 'Put a 2/2 black Zombie creature token onto the battlefield.\nFlashback {4}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('reap the seagraf', ['2', 'B']).
card_cmc('reap the seagraf', 3).
card_layout('reap the seagraf', 'normal').

% Found in: BNG
card_name('reap what is sown', 'Reap What Is Sown').
card_type('reap what is sown', 'Instant').
card_types('reap what is sown', ['Instant']).
card_subtypes('reap what is sown', []).
card_colors('reap what is sown', ['W', 'G']).
card_text('reap what is sown', 'Put a +1/+1 counter on each of up to three target creatures.').
card_mana_cost('reap what is sown', ['1', 'G', 'W']).
card_cmc('reap what is sown', 3).
card_layout('reap what is sown', 'normal').

% Found in: C14, ISD
card_name('reaper from the abyss', 'Reaper from the Abyss').
card_type('reaper from the abyss', 'Creature — Demon').
card_types('reaper from the abyss', ['Creature']).
card_subtypes('reaper from the abyss', ['Demon']).
card_colors('reaper from the abyss', ['B']).
card_text('reaper from the abyss', 'Flying\nMorbid — At the beginning of each end step, if a creature died this turn, destroy target non-Demon creature.').
card_mana_cost('reaper from the abyss', ['3', 'B', 'B', 'B']).
card_cmc('reaper from the abyss', 6).
card_layout('reaper from the abyss', 'normal').
card_power('reaper from the abyss', 6).
card_toughness('reaper from the abyss', 6).

% Found in: SHM
card_name('reaper king', 'Reaper King').
card_type('reaper king', 'Legendary Artifact Creature — Scarecrow').
card_types('reaper king', ['Artifact', 'Creature']).
card_subtypes('reaper king', ['Scarecrow']).
card_supertypes('reaper king', ['Legendary']).
card_colors('reaper king', ['W', 'U', 'B', 'R', 'G']).
card_text('reaper king', '({2/W} can be paid with any two mana or with {W}. This card\'s converted mana cost is 10.)\nOther Scarecrow creatures you control get +1/+1.\nWhenever another Scarecrow enters the battlefield under your control, destroy target permanent.').
card_mana_cost('reaper king', ['2/W', '2/U', '2/B', '2/R', '2/G']).
card_cmc('reaper king', 10).
card_layout('reaper king', 'normal').
card_power('reaper king', 6).
card_toughness('reaper king', 6).

% Found in: VAN
card_name('reaper king avatar', 'Reaper King Avatar').
card_type('reaper king avatar', 'Vanguard').
card_types('reaper king avatar', ['Vanguard']).
card_subtypes('reaper king avatar', []).
card_colors('reaper king avatar', []).
card_text('reaper king avatar', 'Each creature you control gets +1/+1 for each of its colors.').
card_layout('reaper king avatar', 'vanguard').

% Found in: NPH
card_name('reaper of sheoldred', 'Reaper of Sheoldred').
card_type('reaper of sheoldred', 'Creature — Horror').
card_types('reaper of sheoldred', ['Creature']).
card_subtypes('reaper of sheoldred', ['Horror']).
card_colors('reaper of sheoldred', ['B']).
card_text('reaper of sheoldred', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhenever a source deals damage to Reaper of Sheoldred, that source\'s controller gets a poison counter.').
card_mana_cost('reaper of sheoldred', ['4', 'B']).
card_cmc('reaper of sheoldred', 5).
card_layout('reaper of sheoldred', 'normal').
card_power('reaper of sheoldred', 2).
card_toughness('reaper of sheoldred', 5).

% Found in: CPK, DDM, THS
card_name('reaper of the wilds', 'Reaper of the Wilds').
card_type('reaper of the wilds', 'Creature — Gorgon').
card_types('reaper of the wilds', ['Creature']).
card_subtypes('reaper of the wilds', ['Gorgon']).
card_colors('reaper of the wilds', ['B', 'G']).
card_text('reaper of the wilds', 'Whenever another creature dies, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{B}: Reaper of the Wilds gains deathtouch until end of turn.\n{1}{G}: Reaper of the Wilds gains hexproof until end of turn.').
card_mana_cost('reaper of the wilds', ['2', 'B', 'G']).
card_cmc('reaper of the wilds', 4).
card_layout('reaper of the wilds', 'normal').
card_power('reaper of the wilds', 4).
card_toughness('reaper of the wilds', 5).

% Found in: SCG
card_name('reaping the graves', 'Reaping the Graves').
card_type('reaping the graves', 'Instant').
card_types('reaping the graves', ['Instant']).
card_subtypes('reaping the graves', []).
card_colors('reaping the graves', ['B']).
card_text('reaping the graves', 'Return target creature card from your graveyard to your hand.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_mana_cost('reaping the graves', ['2', 'B']).
card_cmc('reaping the graves', 3).
card_layout('reaping the graves', 'normal').

% Found in: EXO
card_name('reaping the rewards', 'Reaping the Rewards').
card_type('reaping the rewards', 'Instant').
card_types('reaping the rewards', ['Instant']).
card_subtypes('reaping the rewards', []).
card_colors('reaping the rewards', ['W']).
card_text('reaping the rewards', 'Buyback—Sacrifice a land. (You may sacrifice a land in addition to any other costs as you cast this spell. If you do, put this card into your hand as it resolves.)\nYou gain 2 life.').
card_mana_cost('reaping the rewards', ['W']).
card_cmc('reaping the rewards', 1).
card_layout('reaping the rewards', 'normal').

% Found in: ARC, DDJ, DDK, M11, M12, MM2
card_name('reassembling skeleton', 'Reassembling Skeleton').
card_type('reassembling skeleton', 'Creature — Skeleton Warrior').
card_types('reassembling skeleton', ['Creature']).
card_subtypes('reassembling skeleton', ['Skeleton', 'Warrior']).
card_colors('reassembling skeleton', ['B']).
card_text('reassembling skeleton', '{1}{B}: Return Reassembling Skeleton from your graveyard to the battlefield tapped.').
card_mana_cost('reassembling skeleton', ['1', 'B']).
card_cmc('reassembling skeleton', 2).
card_layout('reassembling skeleton', 'normal').
card_power('reassembling skeleton', 1).
card_toughness('reassembling skeleton', 1).

% Found in: ORI
card_name('reave soul', 'Reave Soul').
card_type('reave soul', 'Sorcery').
card_types('reave soul', ['Sorcery']).
card_subtypes('reave soul', []).
card_colors('reave soul', ['B']).
card_text('reave soul', 'Destroy target creature with power 3 or less.').
card_mana_cost('reave soul', ['1', 'B']).
card_cmc('reave soul', 2).
card_layout('reave soul', 'normal').

% Found in: PCY
card_name('rebel informer', 'Rebel Informer').
card_type('rebel informer', 'Creature — Human Mercenary Rebel').
card_types('rebel informer', ['Creature']).
card_subtypes('rebel informer', ['Human', 'Mercenary', 'Rebel']).
card_colors('rebel informer', ['B']).
card_text('rebel informer', 'Rebel Informer can\'t be the target of white spells or abilities from white sources.\n{3}: Put target nontoken Rebel on the bottom of its owner\'s library.').
card_mana_cost('rebel informer', ['2', 'B']).
card_cmc('rebel informer', 3).
card_layout('rebel informer', 'normal').
card_power('rebel informer', 1).
card_toughness('rebel informer', 2).

% Found in: LRW
card_name('rebellion of the flamekin', 'Rebellion of the Flamekin').
card_type('rebellion of the flamekin', 'Tribal Enchantment — Elemental').
card_types('rebellion of the flamekin', ['Tribal', 'Enchantment']).
card_subtypes('rebellion of the flamekin', ['Elemental']).
card_colors('rebellion of the flamekin', ['R']).
card_text('rebellion of the flamekin', 'Whenever you clash, you may pay {1}. If you do, put a 3/1 red Elemental Shaman creature token onto the battlefield. If you won, that token gains haste until end of turn. (This ability triggers after the clash ends.)').
card_mana_cost('rebellion of the flamekin', ['3', 'R']).
card_cmc('rebellion of the flamekin', 4).
card_layout('rebellion of the flamekin', 'normal').

% Found in: 4ED, LEG
card_name('rebirth', 'Rebirth').
card_type('rebirth', 'Sorcery').
card_types('rebirth', ['Sorcery']).
card_subtypes('rebirth', []).
card_colors('rebirth', ['G']).
card_text('rebirth', 'Remove Rebirth from your deck before playing if you\'re not playing for ante.\nEach player may ante the top card of his or her library. If a player does, his or her life total becomes 20.').
card_mana_cost('rebirth', ['3', 'G', 'G', 'G']).
card_cmc('rebirth', 6).
card_layout('rebirth', 'normal').

% Found in: TOR
card_name('reborn hero', 'Reborn Hero').
card_type('reborn hero', 'Creature — Human Soldier').
card_types('reborn hero', ['Creature']).
card_subtypes('reborn hero', ['Human', 'Soldier']).
card_colors('reborn hero', ['W']).
card_text('reborn hero', 'Vigilance\nThreshold — As long as seven or more cards are in your graveyard, Reborn Hero has \"When Reborn Hero dies, you may pay {W}{W}. If you do, return Reborn Hero to the battlefield under your control.\"').
card_mana_cost('reborn hero', ['2', 'W']).
card_cmc('reborn hero', 3).
card_layout('reborn hero', 'normal').
card_power('reborn hero', 2).
card_toughness('reborn hero', 2).

% Found in: ARB
card_name('reborn hope', 'Reborn Hope').
card_type('reborn hope', 'Sorcery').
card_types('reborn hope', ['Sorcery']).
card_subtypes('reborn hope', []).
card_colors('reborn hope', ['W', 'G']).
card_text('reborn hope', 'Return target multicolored card from your graveyard to your hand.').
card_mana_cost('reborn hope', ['G', 'W']).
card_cmc('reborn hope', 2).
card_layout('reborn hope', 'normal').

% Found in: STH
card_name('rebound', 'Rebound').
card_type('rebound', 'Instant').
card_types('rebound', ['Instant']).
card_subtypes('rebound', []).
card_colors('rebound', ['U']).
card_text('rebound', 'Change the target of target spell that targets only a player. The new target must be a player.').
card_mana_cost('rebound', ['1', 'U']).
card_cmc('rebound', 2).
card_layout('rebound', 'normal').

% Found in: PLC
card_name('rebuff the wicked', 'Rebuff the Wicked').
card_type('rebuff the wicked', 'Instant').
card_types('rebuff the wicked', ['Instant']).
card_subtypes('rebuff the wicked', []).
card_colors('rebuff the wicked', ['W']).
card_text('rebuff the wicked', 'Counter target spell that targets a permanent you control.').
card_mana_cost('rebuff the wicked', ['W']).
card_cmc('rebuff the wicked', 1).
card_layout('rebuff the wicked', 'normal').

% Found in: ULG
card_name('rebuild', 'Rebuild').
card_type('rebuild', 'Instant').
card_types('rebuild', ['Instant']).
card_subtypes('rebuild', []).
card_colors('rebuild', ['U']).
card_text('rebuild', 'Return all artifacts to their owners\' hands.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('rebuild', ['2', 'U']).
card_cmc('rebuild', 3).
card_layout('rebuild', 'normal').

% Found in: ISD
card_name('rebuke', 'Rebuke').
card_type('rebuke', 'Instant').
card_types('rebuke', ['Instant']).
card_subtypes('rebuke', []).
card_colors('rebuke', ['W']).
card_text('rebuke', 'Destroy target attacking creature.').
card_mana_cost('rebuke', ['2', 'W']).
card_cmc('rebuke', 3).
card_layout('rebuke', 'normal').

% Found in: DST
card_name('rebuking ceremony', 'Rebuking Ceremony').
card_type('rebuking ceremony', 'Sorcery').
card_types('rebuking ceremony', ['Sorcery']).
card_subtypes('rebuking ceremony', []).
card_colors('rebuking ceremony', ['G']).
card_text('rebuking ceremony', 'Put two target artifacts on top of their owners\' libraries.').
card_mana_cost('rebuking ceremony', ['3', 'G', 'G']).
card_cmc('rebuking ceremony', 5).
card_layout('rebuking ceremony', 'normal').

% Found in: 5ED, 6ED, CHR, LEG, ME3
card_name('recall', 'Recall').
card_type('recall', 'Sorcery').
card_types('recall', ['Sorcery']).
card_subtypes('recall', []).
card_colors('recall', ['U']).
card_text('recall', 'Discard X cards, then return a card from your graveyard to your hand for each card discarded this way. Exile Recall.').
card_mana_cost('recall', ['X', 'X', 'U']).
card_cmc('recall', 1).
card_layout('recall', 'normal').

% Found in: USG
card_name('recantation', 'Recantation').
card_type('recantation', 'Enchantment').
card_types('recantation', ['Enchantment']).
card_subtypes('recantation', []).
card_colors('recantation', ['U']).
card_text('recantation', 'At the beginning of your upkeep, you may put a verse counter on Recantation.\n{U}, Sacrifice Recantation: Return up to X target permanents to their owners\' hands, where X is the number of verse counters on Recantation.').
card_mana_cost('recantation', ['3', 'U', 'U']).
card_cmc('recantation', 5).
card_layout('recantation', 'normal').

% Found in: CHK, DDG, pMPR
card_name('reciprocate', 'Reciprocate').
card_type('reciprocate', 'Instant').
card_types('reciprocate', ['Instant']).
card_subtypes('reciprocate', []).
card_colors('reciprocate', ['W']).
card_text('reciprocate', 'Exile target creature that dealt damage to you this turn.').
card_mana_cost('reciprocate', ['W']).
card_cmc('reciprocate', 1).
card_layout('reciprocate', 'normal').

% Found in: DDN, UDS
card_name('reckless abandon', 'Reckless Abandon').
card_type('reckless abandon', 'Sorcery').
card_types('reckless abandon', ['Sorcery']).
card_subtypes('reckless abandon', []).
card_colors('reckless abandon', ['R']).
card_text('reckless abandon', 'As an additional cost to cast Reckless Abandon, sacrifice a creature.\nReckless Abandon deals 4 damage to target creature or player.').
card_mana_cost('reckless abandon', ['R']).
card_cmc('reckless abandon', 1).
card_layout('reckless abandon', 'normal').

% Found in: INV
card_name('reckless assault', 'Reckless Assault').
card_type('reckless assault', 'Enchantment').
card_types('reckless assault', ['Enchantment']).
card_subtypes('reckless assault', []).
card_colors('reckless assault', ['B', 'R']).
card_text('reckless assault', '{1}, Pay 2 life: Reckless Assault deals 1 damage to target creature or player.').
card_mana_cost('reckless assault', ['2', 'B', 'R']).
card_cmc('reckless assault', 4).
card_layout('reckless assault', 'normal').

% Found in: M13
card_name('reckless brute', 'Reckless Brute').
card_type('reckless brute', 'Creature — Ogre Warrior').
card_types('reckless brute', ['Creature']).
card_subtypes('reckless brute', ['Ogre', 'Warrior']).
card_colors('reckless brute', ['R']).
card_text('reckless brute', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\nReckless Brute attacks each turn if able.').
card_mana_cost('reckless brute', ['2', 'R']).
card_cmc('reckless brute', 3).
card_layout('reckless brute', 'normal').
card_power('reckless brute', 3).
card_toughness('reckless brute', 1).

% Found in: HOP, ODY, VMA
card_name('reckless charge', 'Reckless Charge').
card_type('reckless charge', 'Sorcery').
card_types('reckless charge', ['Sorcery']).
card_subtypes('reckless charge', []).
card_colors('reckless charge', ['R']).
card_text('reckless charge', 'Target creature gets +3/+0 and gains haste until end of turn.\nFlashback {2}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('reckless charge', ['R']).
card_cmc('reckless charge', 1).
card_layout('reckless charge', 'normal').

% Found in: BFZ
card_name('reckless cohort', 'Reckless Cohort').
card_type('reckless cohort', 'Creature — Human Warrior Ally').
card_types('reckless cohort', ['Creature']).
card_subtypes('reckless cohort', ['Human', 'Warrior', 'Ally']).
card_colors('reckless cohort', ['R']).
card_text('reckless cohort', 'Reckless Cohort attacks each combat if able unless you control another Ally.').
card_mana_cost('reckless cohort', ['1', 'R']).
card_cmc('reckless cohort', 2).
card_layout('reckless cohort', 'normal').
card_power('reckless cohort', 2).
card_toughness('reckless cohort', 2).

% Found in: 6ED, 7ED, MIR
card_name('reckless embermage', 'Reckless Embermage').
card_type('reckless embermage', 'Creature — Human Wizard').
card_types('reckless embermage', ['Creature']).
card_subtypes('reckless embermage', ['Human', 'Wizard']).
card_colors('reckless embermage', ['R']).
card_text('reckless embermage', '{1}{R}: Reckless Embermage deals 1 damage to target creature or player and 1 damage to itself.').
card_mana_cost('reckless embermage', ['3', 'R']).
card_cmc('reckless embermage', 4).
card_layout('reckless embermage', 'normal').
card_power('reckless embermage', 2).
card_toughness('reckless embermage', 2).

% Found in: DTK
card_name('reckless imp', 'Reckless Imp').
card_type('reckless imp', 'Creature — Imp').
card_types('reckless imp', ['Creature']).
card_subtypes('reckless imp', ['Imp']).
card_colors('reckless imp', ['B']).
card_text('reckless imp', 'Flying\nReckless Imp can\'t block.\nDash {1}{B} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('reckless imp', ['2', 'B']).
card_cmc('reckless imp', 3).
card_layout('reckless imp', 'normal').
card_power('reckless imp', 2).
card_toughness('reckless imp', 2).

% Found in: EXO
card_name('reckless ogre', 'Reckless Ogre').
card_type('reckless ogre', 'Creature — Ogre').
card_types('reckless ogre', ['Creature']).
card_subtypes('reckless ogre', ['Ogre']).
card_colors('reckless ogre', ['R']).
card_text('reckless ogre', 'Whenever Reckless Ogre attacks alone, it gets +3/+0 until end of turn.').
card_mana_cost('reckless ogre', ['3', 'R']).
card_cmc('reckless ogre', 4).
card_layout('reckless ogre', 'normal').
card_power('reckless ogre', 3).
card_toughness('reckless ogre', 2).

% Found in: DD3_EVG, EVG, ONS
card_name('reckless one', 'Reckless One').
card_type('reckless one', 'Creature — Goblin Avatar').
card_types('reckless one', ['Creature']).
card_subtypes('reckless one', ['Goblin', 'Avatar']).
card_colors('reckless one', ['R']).
card_text('reckless one', 'Haste\nReckless One\'s power and toughness are each equal to the number of Goblins on the battlefield.').
card_mana_cost('reckless one', ['3', 'R']).
card_cmc('reckless one', 4).
card_layout('reckless one', 'normal').
card_power('reckless one', '*').
card_toughness('reckless one', '*').

% Found in: BNG
card_name('reckless reveler', 'Reckless Reveler').
card_type('reckless reveler', 'Creature — Satyr').
card_types('reckless reveler', ['Creature']).
card_subtypes('reckless reveler', ['Satyr']).
card_colors('reckless reveler', ['R']).
card_text('reckless reveler', '{R}, Sacrifice Reckless Reveler: Destroy target artifact.').
card_mana_cost('reckless reveler', ['1', 'R']).
card_cmc('reckless reveler', 2).
card_layout('reckless reveler', 'normal').
card_power('reckless reveler', 2).
card_toughness('reckless reveler', 1).

% Found in: CNS, ZEN
card_name('reckless scholar', 'Reckless Scholar').
card_type('reckless scholar', 'Creature — Human Wizard').
card_types('reckless scholar', ['Creature']).
card_subtypes('reckless scholar', ['Human', 'Wizard']).
card_colors('reckless scholar', ['U']).
card_text('reckless scholar', '{T}: Target player draws a card, then discards a card.').
card_mana_cost('reckless scholar', ['2', 'U']).
card_cmc('reckless scholar', 3).
card_layout('reckless scholar', 'normal').
card_power('reckless scholar', 2).
card_toughness('reckless scholar', 1).

% Found in: C13, CNS, INV, TMP
card_name('reckless spite', 'Reckless Spite').
card_type('reckless spite', 'Instant').
card_types('reckless spite', ['Instant']).
card_subtypes('reckless spite', []).
card_colors('reckless spite', ['B']).
card_text('reckless spite', 'Destroy two target nonblack creatures. You lose 5 life.').
card_mana_cost('reckless spite', ['1', 'B', 'B']).
card_cmc('reckless spite', 3).
card_layout('reckless spite', 'normal').

% Found in: ISD
card_name('reckless waif', 'Reckless Waif').
card_type('reckless waif', 'Creature — Human Rogue Werewolf').
card_types('reckless waif', ['Creature']).
card_subtypes('reckless waif', ['Human', 'Rogue', 'Werewolf']).
card_colors('reckless waif', ['R']).
card_text('reckless waif', 'At the beginning of each upkeep, if no spells were cast last turn, transform Reckless Waif.').
card_mana_cost('reckless waif', ['R']).
card_cmc('reckless waif', 1).
card_layout('reckless waif', 'double-faced').
card_power('reckless waif', 1).
card_toughness('reckless waif', 1).
card_sides('reckless waif', 'merciless predator').

% Found in: PLC, pGTW
card_name('reckless wurm', 'Reckless Wurm').
card_type('reckless wurm', 'Creature — Wurm').
card_types('reckless wurm', ['Creature']).
card_subtypes('reckless wurm', ['Wurm']).
card_colors('reckless wurm', ['R']).
card_text('reckless wurm', 'Trample\nMadness {2}{R} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('reckless wurm', ['3', 'R', 'R']).
card_cmc('reckless wurm', 5).
card_layout('reckless wurm', 'normal').
card_power('reckless wurm', 4).
card_toughness('reckless wurm', 4).

% Found in: 7ED, 9ED, EXO, M12, ORI
card_name('reclaim', 'Reclaim').
card_type('reclaim', 'Instant').
card_types('reclaim', ['Instant']).
card_subtypes('reclaim', []).
card_colors('reclaim', ['G']).
card_text('reclaim', 'Put target card from your graveyard on top of your library.').
card_mana_cost('reclaim', ['G']).
card_cmc('reclaim', 1).
card_layout('reclaim', 'normal').

% Found in: BFZ
card_name('reclaiming vines', 'Reclaiming Vines').
card_type('reclaiming vines', 'Sorcery').
card_types('reclaiming vines', ['Sorcery']).
card_subtypes('reclaiming vines', []).
card_colors('reclaiming vines', ['G']).
card_text('reclaiming vines', 'Destroy target artifact, enchantment, or land.').
card_mana_cost('reclaiming vines', ['2', 'G', 'G']).
card_cmc('reclaiming vines', 4).
card_layout('reclaiming vines', 'normal').

% Found in: ICE
card_name('reclamation', 'Reclamation').
card_type('reclamation', 'Enchantment').
card_types('reclamation', ['Enchantment']).
card_subtypes('reclamation', []).
card_colors('reclamation', ['W', 'G']).
card_text('reclamation', 'Black creatures can\'t attack unless their controller sacrifices a land for each black creature he or she controls that\'s attacking. (This cost is paid as attackers are declared.)').
card_mana_cost('reclamation', ['2', 'G', 'W']).
card_cmc('reclamation', 4).
card_layout('reclamation', 'normal').

% Found in: C14, M15, pMGD
card_name('reclamation sage', 'Reclamation Sage').
card_type('reclamation sage', 'Creature — Elf Shaman').
card_types('reclamation sage', ['Creature']).
card_subtypes('reclamation sage', ['Elf', 'Shaman']).
card_colors('reclamation sage', ['G']).
card_text('reclamation sage', 'When Reclamation Sage enters the battlefield, you may destroy target artifact or enchantment.').
card_mana_cost('reclamation sage', ['2', 'G']).
card_cmc('reclamation sage', 3).
card_layout('reclamation sage', 'normal').
card_power('reclamation sage', 2).
card_toughness('reclamation sage', 1).

% Found in: ORI
card_name('reclusive artificer', 'Reclusive Artificer').
card_type('reclusive artificer', 'Creature — Human Artificer').
card_types('reclusive artificer', ['Creature']).
card_subtypes('reclusive artificer', ['Human', 'Artificer']).
card_colors('reclusive artificer', ['U', 'R']).
card_text('reclusive artificer', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\nWhen Reclusive Artificer enters the battlefield, you may have it deal damage to target creature equal to the number of artifacts you control.').
card_mana_cost('reclusive artificer', ['2', 'U', 'R']).
card_cmc('reclusive artificer', 4).
card_layout('reclusive artificer', 'normal').
card_power('reclusive artificer', 2).
card_toughness('reclusive artificer', 3).

% Found in: USG
card_name('reclusive wight', 'Reclusive Wight').
card_type('reclusive wight', 'Creature — Zombie Minion').
card_types('reclusive wight', ['Creature']).
card_subtypes('reclusive wight', ['Zombie', 'Minion']).
card_colors('reclusive wight', ['B']).
card_text('reclusive wight', 'At the beginning of your upkeep, if you control another nonland permanent, sacrifice Reclusive Wight.').
card_mana_cost('reclusive wight', ['3', 'B']).
card_cmc('reclusive wight', 4).
card_layout('reclusive wight', 'normal').
card_power('reclusive wight', 4).
card_toughness('reclusive wight', 4).

% Found in: DDH, INV
card_name('recoil', 'Recoil').
card_type('recoil', 'Instant').
card_types('recoil', ['Instant']).
card_subtypes('recoil', []).
card_colors('recoil', ['U', 'B']).
card_text('recoil', 'Return target permanent to its owner\'s hand. Then that player discards a card.').
card_mana_cost('recoil', ['1', 'U', 'B']).
card_cmc('recoil', 3).
card_layout('recoil', 'normal').

% Found in: 10E, RAV, pMPR
card_name('recollect', 'Recollect').
card_type('recollect', 'Sorcery').
card_types('recollect', ['Sorcery']).
card_subtypes('recollect', []).
card_colors('recollect', ['G']).
card_text('recollect', 'Return target card from your graveyard to your hand.').
card_mana_cost('recollect', ['2', 'G']).
card_cmc('recollect', 3).
card_layout('recollect', 'normal').

% Found in: EXO
card_name('reconnaissance', 'Reconnaissance').
card_type('reconnaissance', 'Enchantment').
card_types('reconnaissance', ['Enchantment']).
card_subtypes('reconnaissance', []).
card_colors('reconnaissance', ['W']).
card_text('reconnaissance', '{0}: Remove target attacking creature you control from combat and untap it.').
card_mana_cost('reconnaissance', ['W']).
card_cmc('reconnaissance', 1).
card_layout('reconnaissance', 'normal').

% Found in: 3ED, ATQ, ME4
card_name('reconstruction', 'Reconstruction').
card_type('reconstruction', 'Sorcery').
card_types('reconstruction', ['Sorcery']).
card_subtypes('reconstruction', []).
card_colors('reconstruction', ['U']).
card_text('reconstruction', 'Return target artifact card from your graveyard to your hand.').
card_mana_cost('reconstruction', ['U']).
card_cmc('reconstruction', 1).
card_layout('reconstruction', 'normal').

% Found in: DDK, ODY
card_name('recoup', 'Recoup').
card_type('recoup', 'Sorcery').
card_types('recoup', ['Sorcery']).
card_subtypes('recoup', []).
card_colors('recoup', ['R']).
card_text('recoup', 'Target sorcery card in your graveyard gains flashback until end of turn. The flashback cost is equal to its mana cost. (Mana cost includes color.)\nFlashback {3}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('recoup', ['1', 'R']).
card_cmc('recoup', 2).
card_layout('recoup', 'normal').

% Found in: 10E, INV
card_name('recover', 'Recover').
card_type('recover', 'Sorcery').
card_types('recover', ['Sorcery']).
card_subtypes('recover', []).
card_colors('recover', ['B']).
card_text('recover', 'Return target creature card from your graveyard to your hand.\nDraw a card.').
card_mana_cost('recover', ['2', 'B']).
card_cmc('recover', 3).
card_layout('recover', 'normal').

% Found in: MOR
card_name('recross the paths', 'Recross the Paths').
card_type('recross the paths', 'Sorcery').
card_types('recross the paths', ['Sorcery']).
card_subtypes('recross the paths', []).
card_colors('recross the paths', ['G']).
card_text('recross the paths', 'Reveal cards from the top of your library until you reveal a land card. Put that card onto the battlefield and the rest on the bottom of your library in any order. Clash with an opponent. If you win, return Recross the Paths to its owner\'s hand. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('recross the paths', ['2', 'G']).
card_cmc('recross the paths', 3).
card_layout('recross the paths', 'normal').

% Found in: DDH, EVE
card_name('recumbent bliss', 'Recumbent Bliss').
card_type('recumbent bliss', 'Enchantment — Aura').
card_types('recumbent bliss', ['Enchantment']).
card_subtypes('recumbent bliss', ['Aura']).
card_colors('recumbent bliss', ['W']).
card_text('recumbent bliss', 'Enchant creature\nEnchanted creature can\'t attack or block.\nAt the beginning of your upkeep, you may gain 1 life.').
card_mana_cost('recumbent bliss', ['2', 'W']).
card_cmc('recumbent bliss', 3).
card_layout('recumbent bliss', 'normal').

% Found in: SCG
card_name('recuperate', 'Recuperate').
card_type('recuperate', 'Instant').
card_types('recuperate', ['Instant']).
card_subtypes('recuperate', []).
card_colors('recuperate', ['W']).
card_text('recuperate', 'Choose one —\n• You gain 6 life.\n• Prevent the next 6 damage that would be dealt to target creature this turn.').
card_mana_cost('recuperate', ['3', 'W']).
card_cmc('recuperate', 4).
card_layout('recuperate', 'normal').

% Found in: ROE
card_name('recurring insight', 'Recurring Insight').
card_type('recurring insight', 'Sorcery').
card_types('recurring insight', ['Sorcery']).
card_subtypes('recurring insight', []).
card_colors('recurring insight', ['U']).
card_text('recurring insight', 'Draw cards equal to the number of cards in target opponent\'s hand.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('recurring insight', ['4', 'U', 'U']).
card_cmc('recurring insight', 6).
card_layout('recurring insight', 'normal').

% Found in: EXO, TPR, VMA
card_name('recurring nightmare', 'Recurring Nightmare').
card_type('recurring nightmare', 'Enchantment').
card_types('recurring nightmare', ['Enchantment']).
card_subtypes('recurring nightmare', []).
card_colors('recurring nightmare', ['B']).
card_text('recurring nightmare', 'Sacrifice a creature, Return Recurring Nightmare to its owner\'s hand: Return target creature card from your graveyard to the battlefield. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('recurring nightmare', ['2', 'B']).
card_cmc('recurring nightmare', 3).
card_layout('recurring nightmare', 'normal').
card_reserved('recurring nightmare').

% Found in: TMP, TPR
card_name('recycle', 'Recycle').
card_type('recycle', 'Enchantment').
card_types('recycle', ['Enchantment']).
card_subtypes('recycle', []).
card_colors('recycle', ['G']).
card_text('recycle', 'Skip your draw step.\nWhenever you play a card, draw a card.\nYour maximum hand size is two.').
card_mana_cost('recycle', ['4', 'G', 'G']).
card_cmc('recycle', 6).
card_layout('recycle', 'normal').
card_reserved('recycle').

% Found in: ME2, PTK
card_name('red cliffs armada', 'Red Cliffs Armada').
card_type('red cliffs armada', 'Creature — Human Soldier').
card_types('red cliffs armada', ['Creature']).
card_subtypes('red cliffs armada', ['Human', 'Soldier']).
card_colors('red cliffs armada', ['U']).
card_text('red cliffs armada', 'Red Cliffs Armada can\'t attack unless defending player controls an Island.').
card_mana_cost('red cliffs armada', ['4', 'U']).
card_cmc('red cliffs armada', 5).
card_layout('red cliffs armada', 'normal').
card_power('red cliffs armada', 5).
card_toughness('red cliffs armada', 4).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB, ME4
card_name('red elemental blast', 'Red Elemental Blast').
card_type('red elemental blast', 'Instant').
card_types('red elemental blast', ['Instant']).
card_subtypes('red elemental blast', []).
card_colors('red elemental blast', ['R']).
card_text('red elemental blast', 'Choose one —\n• Counter target blue spell.\n• Destroy target blue permanent.').
card_mana_cost('red elemental blast', ['R']).
card_cmc('red elemental blast', 1).
card_layout('red elemental blast', 'normal').

% Found in: 4ED, LEG
card_name('red mana battery', 'Red Mana Battery').
card_type('red mana battery', 'Artifact').
card_types('red mana battery', ['Artifact']).
card_subtypes('red mana battery', []).
card_colors('red mana battery', []).
card_text('red mana battery', '{2}, {T}: Put a charge counter on Red Mana Battery.\n{T}, Remove any number of charge counters from Red Mana Battery: Add {R} to your mana pool, then add an additional {R} to your mana pool for each charge counter removed this way.').
card_mana_cost('red mana battery', ['4']).
card_cmc('red mana battery', 4).
card_layout('red mana battery', 'normal').

% Found in: ICE
card_name('red scarab', 'Red Scarab').
card_type('red scarab', 'Enchantment — Aura').
card_types('red scarab', ['Enchantment']).
card_subtypes('red scarab', ['Aura']).
card_colors('red scarab', ['W']).
card_text('red scarab', 'Enchant creature\nEnchanted creature can\'t be blocked by red creatures.\nEnchanted creature gets +2/+2 as long as an opponent controls a red permanent.').
card_mana_cost('red scarab', ['W']).
card_cmc('red scarab', 1).
card_layout('red scarab', 'normal').

% Found in: MBS
card_name('red sun\'s zenith', 'Red Sun\'s Zenith').
card_type('red sun\'s zenith', 'Sorcery').
card_types('red sun\'s zenith', ['Sorcery']).
card_subtypes('red sun\'s zenith', []).
card_colors('red sun\'s zenith', ['R']).
card_text('red sun\'s zenith', 'Red Sun\'s Zenith deals X damage to target creature or player. If a creature dealt damage this way would die this turn, exile it instead. Shuffle Red Sun\'s Zenith into its owner\'s library.').
card_mana_cost('red sun\'s zenith', ['X', 'R']).
card_cmc('red sun\'s zenith', 1).
card_layout('red sun\'s zenith', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('red ward', 'Red Ward').
card_type('red ward', 'Enchantment — Aura').
card_types('red ward', ['Enchantment']).
card_subtypes('red ward', ['Aura']).
card_colors('red ward', ['W']).
card_text('red ward', 'Enchant creature\nEnchanted creature has protection from red. This effect doesn\'t remove Red Ward.').
card_mana_cost('red ward', ['W']).
card_cmc('red ward', 1).
card_layout('red ward', 'normal').

% Found in: UNH
card_name('red-hot hottie', 'Red-Hot Hottie').
card_type('red-hot hottie', 'Creature — Elemental').
card_types('red-hot hottie', ['Creature']).
card_subtypes('red-hot hottie', ['Elemental']).
card_colors('red-hot hottie', ['R']).
card_text('red-hot hottie', 'Whenever Red-Hot Hottie deals damage to a creature, put a third-degree-burn counter on that creature. It has \"At the end of each turn, sacrifice this creature unless you scream ‘Aaah\' at the top of your lungs.\"').
card_mana_cost('red-hot hottie', ['2', 'R', 'R']).
card_cmc('red-hot hottie', 4).
card_layout('red-hot hottie', 'normal').
card_power('red-hot hottie', 2).
card_toughness('red-hot hottie', 5).

% Found in: 8ED, USG
card_name('redeem', 'Redeem').
card_type('redeem', 'Instant').
card_types('redeem', ['Instant']).
card_subtypes('redeem', []).
card_colors('redeem', ['W']).
card_text('redeem', 'Prevent all damage that would be dealt this turn to up to two target creatures.').
card_mana_cost('redeem', ['1', 'W']).
card_cmc('redeem', 2).
card_layout('redeem', 'normal').

% Found in: MOR
card_name('redeem the lost', 'Redeem the Lost').
card_type('redeem the lost', 'Instant').
card_types('redeem the lost', ['Instant']).
card_subtypes('redeem the lost', []).
card_colors('redeem the lost', ['W']).
card_text('redeem the lost', 'Target creature you control gains protection from the color of your choice until end of turn. Clash with an opponent. If you win, return Redeem the Lost to its owner\'s hand. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('redeem the lost', ['1', 'W']).
card_cmc('redeem the lost', 2).
card_layout('redeem the lost', 'normal').

% Found in: M11, M12, M13
card_name('redirect', 'Redirect').
card_type('redirect', 'Instant').
card_types('redirect', ['Instant']).
card_subtypes('redirect', []).
card_colors('redirect', ['U']).
card_text('redirect', 'You may choose new targets for target spell.').
card_mana_cost('redirect', ['U', 'U']).
card_cmc('redirect', 2).
card_layout('redirect', 'normal').

% Found in: DTK
card_name('reduce in stature', 'Reduce in Stature').
card_type('reduce in stature', 'Enchantment — Aura').
card_types('reduce in stature', ['Enchantment']).
card_subtypes('reduce in stature', ['Aura']).
card_colors('reduce in stature', ['U']).
card_text('reduce in stature', 'Enchant creature\nEnchanted creature has base power and toughness 0/2.').
card_mana_cost('reduce in stature', ['2', 'U']).
card_cmc('reduce in stature', 3).
card_layout('reduce in stature', 'normal').

% Found in: BOK
card_name('reduce to dreams', 'Reduce to Dreams').
card_type('reduce to dreams', 'Sorcery').
card_types('reduce to dreams', ['Sorcery']).
card_subtypes('reduce to dreams', []).
card_colors('reduce to dreams', ['U']).
card_text('reduce to dreams', 'Return all artifacts and enchantments to their owners\' hands.').
card_mana_cost('reduce to dreams', ['3', 'U', 'U']).
card_cmc('reduce to dreams', 5).
card_layout('reduce to dreams', 'normal').

% Found in: 6ED, 7ED, POR, WTH
card_name('redwood treefolk', 'Redwood Treefolk').
card_type('redwood treefolk', 'Creature — Treefolk').
card_types('redwood treefolk', ['Creature']).
card_subtypes('redwood treefolk', ['Treefolk']).
card_colors('redwood treefolk', ['G']).
card_text('redwood treefolk', '').
card_mana_cost('redwood treefolk', ['4', 'G']).
card_cmc('redwood treefolk', 5).
card_layout('redwood treefolk', 'normal').
card_power('redwood treefolk', 3).
card_toughness('redwood treefolk', 6).

% Found in: 5ED, HML
card_name('reef pirates', 'Reef Pirates').
card_type('reef pirates', 'Creature — Zombie Pirate').
card_types('reef pirates', ['Creature']).
card_subtypes('reef pirates', ['Zombie', 'Pirate']).
card_colors('reef pirates', ['U']).
card_text('reef pirates', 'Whenever Reef Pirates deals damage to an opponent, that player puts the top card of his or her library into his or her graveyard.').
card_mana_cost('reef pirates', ['1', 'U', 'U']).
card_cmc('reef pirates', 3).
card_layout('reef pirates', 'normal').
card_power('reef pirates', 2).
card_toughness('reef pirates', 2).

% Found in: APC
card_name('reef shaman', 'Reef Shaman').
card_type('reef shaman', 'Creature — Merfolk Shaman').
card_types('reef shaman', ['Creature']).
card_subtypes('reef shaman', ['Merfolk', 'Shaman']).
card_colors('reef shaman', ['U']).
card_text('reef shaman', '{T}: Target land becomes the basic land type of your choice until end of turn.').
card_mana_cost('reef shaman', ['U']).
card_cmc('reef shaman', 1).
card_layout('reef shaman', 'normal').
card_power('reef shaman', 0).
card_toughness('reef shaman', 2).

% Found in: C14
card_name('reef worm', 'Reef Worm').
card_type('reef worm', 'Creature — Worm').
card_types('reef worm', ['Creature']).
card_subtypes('reef worm', ['Worm']).
card_colors('reef worm', ['U']).
card_text('reef worm', 'When Reef Worm dies, put a 3/3 blue Fish creature token onto the battlefield with \"When this creature dies, put a 6/6 blue Whale creature token onto the battlefield with ‘When this creature dies, put a 9/9 blue Kraken creature token onto the battlefield.\'\"').
card_mana_cost('reef worm', ['3', 'U']).
card_cmc('reef worm', 4).
card_layout('reef worm', 'normal').
card_power('reef worm', 0).
card_toughness('reef worm', 1).

% Found in: MIR
card_name('reflect damage', 'Reflect Damage').
card_type('reflect damage', 'Instant').
card_types('reflect damage', ['Instant']).
card_subtypes('reflect damage', []).
card_colors('reflect damage', ['W', 'R']).
card_text('reflect damage', 'The next time a source of your choice would deal damage this turn, that damage is dealt to that source\'s controller instead.').
card_mana_cost('reflect damage', ['3', 'R', 'W']).
card_cmc('reflect damage', 5).
card_layout('reflect damage', 'normal').
card_reserved('reflect damage').

% Found in: DRK
card_name('reflecting mirror', 'Reflecting Mirror').
card_type('reflecting mirror', 'Artifact').
card_types('reflecting mirror', ['Artifact']).
card_subtypes('reflecting mirror', []).
card_colors('reflecting mirror', []).
card_text('reflecting mirror', '{X}, {T}: Change the target of target spell with a single target if that target is you. The new target must be a player. X is twice the converted mana cost of that spell.').
card_mana_cost('reflecting mirror', ['4']).
card_cmc('reflecting mirror', 4).
card_layout('reflecting mirror', 'normal').

% Found in: CNS, SHM, TMP
card_name('reflecting pool', 'Reflecting Pool').
card_type('reflecting pool', 'Land').
card_types('reflecting pool', ['Land']).
card_subtypes('reflecting pool', []).
card_colors('reflecting pool', []).
card_text('reflecting pool', '{T}: Add to your mana pool one mana of any type that a land you control could produce.').
card_layout('reflecting pool', 'normal').

% Found in: PLC
card_name('reflex sliver', 'Reflex Sliver').
card_type('reflex sliver', 'Creature — Sliver').
card_types('reflex sliver', ['Creature']).
card_subtypes('reflex sliver', ['Sliver']).
card_colors('reflex sliver', ['G']).
card_text('reflex sliver', 'All Sliver creatures have haste.').
card_mana_cost('reflex sliver', ['3', 'G']).
card_cmc('reflex sliver', 4).
card_layout('reflex sliver', 'normal').
card_power('reflex sliver', 2).
card_toughness('reflex sliver', 2).

% Found in: 7ED, 8ED, 9ED, USG
card_name('reflexes', 'Reflexes').
card_type('reflexes', 'Enchantment — Aura').
card_types('reflexes', ['Enchantment']).
card_subtypes('reflexes', ['Aura']).
card_colors('reflexes', ['R']).
card_text('reflexes', 'Enchant creature (Target a creature as you cast this. This card enters the battlefield attached to that creature.)\nEnchanted creature has first strike. (It deals combat damage before creatures without first strike.)').
card_mana_cost('reflexes', ['R']).
card_cmc('reflexes', 1).
card_layout('reflexes', 'normal').

% Found in: FRF
card_name('refocus', 'Refocus').
card_type('refocus', 'Instant').
card_types('refocus', ['Instant']).
card_subtypes('refocus', []).
card_colors('refocus', ['U']).
card_text('refocus', 'Untap target creature.\nDraw a card.').
card_mana_cost('refocus', ['1', 'U']).
card_cmc('refocus', 2).
card_layout('refocus', 'normal').

% Found in: AVR
card_name('reforge the soul', 'Reforge the Soul').
card_type('reforge the soul', 'Sorcery').
card_types('reforge the soul', ['Sorcery']).
card_subtypes('reforge the soul', []).
card_colors('reforge the soul', ['R']).
card_text('reforge the soul', 'Each player discards his or her hand, then draws seven cards.\nMiracle {1}{R} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_mana_cost('reforge the soul', ['3', 'R', 'R']).
card_cmc('reforge the soul', 5).
card_layout('reforge the soul', 'normal').

% Found in: WWK
card_name('refraction trap', 'Refraction Trap').
card_type('refraction trap', 'Instant — Trap').
card_types('refraction trap', ['Instant']).
card_subtypes('refraction trap', ['Trap']).
card_colors('refraction trap', ['W']).
card_text('refraction trap', 'If an opponent cast a red instant or sorcery spell this turn, you may pay {W} rather than pay Refraction Trap\'s mana cost.\nPrevent the next 3 damage that a source of your choice would deal to you and/or permanents you control this turn. If damage is prevented this way, Refraction Trap deals that much damage to target creature or player.').
card_mana_cost('refraction trap', ['3', 'W']).
card_cmc('refraction trap', 4).
card_layout('refraction trap', 'normal').

% Found in: ODY
card_name('refresh', 'Refresh').
card_type('refresh', 'Instant').
card_types('refresh', ['Instant']).
card_subtypes('refresh', []).
card_colors('refresh', ['G']).
card_text('refresh', 'Regenerate target creature.\nDraw a card.').
card_mana_cost('refresh', ['2', 'G']).
card_cmc('refresh', 3).
card_layout('refresh', 'normal').

% Found in: NMS
card_name('refreshing rain', 'Refreshing Rain').
card_type('refreshing rain', 'Instant').
card_types('refreshing rain', ['Instant']).
card_subtypes('refreshing rain', []).
card_colors('refreshing rain', ['G']).
card_text('refreshing rain', 'If an opponent controls a Swamp and you control a Forest, you may cast Refreshing Rain without paying its mana cost.\nTarget player gains 6 life.').
card_mana_cost('refreshing rain', ['3', 'G']).
card_cmc('refreshing rain', 4).
card_layout('refreshing rain', 'normal').

% Found in: EVE
card_name('regal force', 'Regal Force').
card_type('regal force', 'Creature — Elemental').
card_types('regal force', ['Creature']).
card_subtypes('regal force', ['Elemental']).
card_colors('regal force', ['G']).
card_text('regal force', 'When Regal Force enters the battlefield, draw a card for each green creature you control.').
card_mana_cost('regal force', ['4', 'G', 'G', 'G']).
card_cmc('regal force', 7).
card_layout('regal force', 'normal').
card_power('regal force', 5).
card_toughness('regal force', 5).

% Found in: 6ED, POR
card_name('regal unicorn', 'Regal Unicorn').
card_type('regal unicorn', 'Creature — Unicorn').
card_types('regal unicorn', ['Creature']).
card_subtypes('regal unicorn', ['Unicorn']).
card_colors('regal unicorn', ['W']).
card_text('regal unicorn', '').
card_mana_cost('regal unicorn', ['2', 'W']).
card_cmc('regal unicorn', 3).
card_layout('regal unicorn', 'normal').
card_power('regal unicorn', 2).
card_toughness('regal unicorn', 3).

% Found in: M14
card_name('regathan firecat', 'Regathan Firecat').
card_type('regathan firecat', 'Creature — Elemental Cat').
card_types('regathan firecat', ['Creature']).
card_subtypes('regathan firecat', ['Elemental', 'Cat']).
card_colors('regathan firecat', ['R']).
card_text('regathan firecat', '').
card_mana_cost('regathan firecat', ['2', 'R']).
card_cmc('regathan firecat', 3).
card_layout('regathan firecat', 'normal').
card_power('regathan firecat', 4).
card_toughness('regathan firecat', 1).

% Found in: M10
card_name('regenerate', 'Regenerate').
card_type('regenerate', 'Instant').
card_types('regenerate', ['Instant']).
card_subtypes('regenerate', []).
card_colors('regenerate', ['G']).
card_text('regenerate', 'Regenerate target creature. (The next time that creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_mana_cost('regenerate', ['1', 'G']).
card_cmc('regenerate', 2).
card_layout('regenerate', 'normal').

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, CED, CEI, ICE, LEA, LEB, MIR
card_name('regeneration', 'Regeneration').
card_type('regeneration', 'Enchantment — Aura').
card_types('regeneration', ['Enchantment']).
card_subtypes('regeneration', ['Aura']).
card_colors('regeneration', ['G']).
card_text('regeneration', 'Enchant creature (Target a creature as you cast this. This card enters the battlefield attached to that creature.)\n{G}: Regenerate enchanted creature. (The next time that creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_mana_cost('regeneration', ['1', 'G']).
card_cmc('regeneration', 2).
card_layout('regeneration', 'normal').

% Found in: MRD, ROE
card_name('regress', 'Regress').
card_type('regress', 'Instant').
card_types('regress', ['Instant']).
card_subtypes('regress', []).
card_colors('regress', ['U']).
card_text('regress', 'Return target permanent to its owner\'s hand.').
card_mana_cost('regress', ['2', 'U']).
card_cmc('regress', 3).
card_layout('regress', 'normal').

% Found in: 2ED, 3ED, CED, CEI, DDL, LEA, LEB, ME4, VMA, pJGP
card_name('regrowth', 'Regrowth').
card_type('regrowth', 'Sorcery').
card_types('regrowth', ['Sorcery']).
card_subtypes('regrowth', []).
card_colors('regrowth', ['G']).
card_text('regrowth', 'Return target card from your graveyard to your hand.').
card_mana_cost('regrowth', ['1', 'G']).
card_cmc('regrowth', 2).
card_layout('regrowth', 'normal').

% Found in: MIR
card_name('reign of chaos', 'Reign of Chaos').
card_type('reign of chaos', 'Sorcery').
card_types('reign of chaos', ['Sorcery']).
card_subtypes('reign of chaos', []).
card_colors('reign of chaos', ['R']).
card_text('reign of chaos', 'Choose one —\n• Destroy target Plains and target white creature.\n• Destroy target Island and target blue creature.').
card_mana_cost('reign of chaos', ['2', 'R', 'R']).
card_cmc('reign of chaos', 4).
card_layout('reign of chaos', 'normal').

% Found in: MIR
card_name('reign of terror', 'Reign of Terror').
card_type('reign of terror', 'Sorcery').
card_types('reign of terror', ['Sorcery']).
card_subtypes('reign of terror', []).
card_colors('reign of terror', ['B']).
card_text('reign of terror', 'Destroy all green creatures or all white creatures. They can\'t be regenerated. You lose 2 life for each creature that died this way.').
card_mana_cost('reign of terror', ['3', 'B', 'B']).
card_cmc('reign of terror', 5).
card_layout('reign of terror', 'normal').

% Found in: CNS, VMA
card_name('reign of the pit', 'Reign of the Pit').
card_type('reign of the pit', 'Sorcery').
card_types('reign of the pit', ['Sorcery']).
card_subtypes('reign of the pit', []).
card_colors('reign of the pit', ['B']).
card_text('reign of the pit', 'Each player sacrifices a creature. Put an X/X black Demon creature token with flying onto the battlefield, where X is the total power of the creatures sacrificed this way.').
card_mana_cost('reign of the pit', ['4', 'B', 'B']).
card_cmc('reign of the pit', 6).
card_layout('reign of the pit', 'normal').

% Found in: C13, LEG, ME3
card_name('reincarnation', 'Reincarnation').
card_type('reincarnation', 'Instant').
card_types('reincarnation', ['Instant']).
card_subtypes('reincarnation', []).
card_colors('reincarnation', ['G']).
card_text('reincarnation', 'Choose target creature. When that creature dies this turn, return a creature card from its owner\'s graveyard to the battlefield under the control of that creature\'s owner.').
card_mana_cost('reincarnation', ['1', 'G', 'G']).
card_cmc('reincarnation', 3).
card_layout('reincarnation', 'normal').

% Found in: ROE
card_name('reinforced bulwark', 'Reinforced Bulwark').
card_type('reinforced bulwark', 'Artifact Creature — Wall').
card_types('reinforced bulwark', ['Artifact', 'Creature']).
card_subtypes('reinforced bulwark', ['Wall']).
card_colors('reinforced bulwark', []).
card_text('reinforced bulwark', 'Defender\n{T}: Prevent the next 1 damage that would be dealt to you this turn.').
card_mana_cost('reinforced bulwark', ['3']).
card_cmc('reinforced bulwark', 3).
card_layout('reinforced bulwark', 'normal').
card_power('reinforced bulwark', 0).
card_toughness('reinforced bulwark', 4).

% Found in: ALL, CST, ME2
card_name('reinforcements', 'Reinforcements').
card_type('reinforcements', 'Instant').
card_types('reinforcements', ['Instant']).
card_subtypes('reinforcements', []).
card_colors('reinforcements', ['W']).
card_text('reinforcements', 'Put up to three target creature cards from your graveyard on top of your library.').
card_mana_cost('reinforcements', ['W']).
card_cmc('reinforcements', 1).
card_layout('reinforcements', 'normal').

% Found in: CMD, STH
card_name('reins of power', 'Reins of Power').
card_type('reins of power', 'Instant').
card_types('reins of power', ['Instant']).
card_subtypes('reins of power', []).
card_colors('reins of power', ['U']).
card_text('reins of power', 'Untap all creatures you control and all creatures target opponent controls. You and that opponent each gain control of all creatures the other controls until end of turn. Those creatures gain haste until end of turn.').
card_mana_cost('reins of power', ['2', 'U', 'U']).
card_cmc('reins of power', 4).
card_layout('reins of power', 'normal').

% Found in: MOR
card_name('reins of the vinesteed', 'Reins of the Vinesteed').
card_type('reins of the vinesteed', 'Enchantment — Aura').
card_types('reins of the vinesteed', ['Enchantment']).
card_subtypes('reins of the vinesteed', ['Aura']).
card_colors('reins of the vinesteed', ['G']).
card_text('reins of the vinesteed', 'Enchant creature\nEnchanted creature gets +2/+2.\nWhen enchanted creature dies, you may return Reins of the Vinesteed from your graveyard to the battlefield attached to a creature that shares a creature type with that creature.').
card_mana_cost('reins of the vinesteed', ['3', 'G']).
card_cmc('reins of the vinesteed', 4).
card_layout('reins of the vinesteed', 'normal').

% Found in: TSP
card_name('reiterate', 'Reiterate').
card_type('reiterate', 'Instant').
card_types('reiterate', ['Instant']).
card_subtypes('reiterate', []).
card_colors('reiterate', ['R']).
card_text('reiterate', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nCopy target instant or sorcery spell. You may choose new targets for the copy.').
card_mana_cost('reiterate', ['1', 'R', 'R']).
card_cmc('reiterate', 3).
card_layout('reiterate', 'normal').

% Found in: CHK, CNS
card_name('reito lantern', 'Reito Lantern').
card_type('reito lantern', 'Artifact').
card_types('reito lantern', ['Artifact']).
card_subtypes('reito lantern', []).
card_colors('reito lantern', []).
card_text('reito lantern', '{3}: Put target card from a graveyard on the bottom of its owner\'s library.').
card_mana_cost('reito lantern', ['2']).
card_cmc('reito lantern', 2).
card_layout('reito lantern', 'normal').

% Found in: CMD, DD3_DVD, DDC, MRD
card_name('reiver demon', 'Reiver Demon').
card_type('reiver demon', 'Creature — Demon').
card_types('reiver demon', ['Creature']).
card_subtypes('reiver demon', ['Demon']).
card_colors('reiver demon', ['B']).
card_text('reiver demon', 'Flying\nWhen Reiver Demon enters the battlefield, if you cast it from your hand, destroy all nonartifact, nonblack creatures. They can\'t be regenerated.').
card_mana_cost('reiver demon', ['4', 'B', 'B', 'B', 'B']).
card_cmc('reiver demon', 8).
card_layout('reiver demon', 'normal').
card_power('reiver demon', 6).
card_toughness('reiver demon', 6).

% Found in: USG
card_name('rejuvenate', 'Rejuvenate').
card_type('rejuvenate', 'Sorcery').
card_types('rejuvenate', ['Sorcery']).
card_subtypes('rejuvenate', []).
card_colors('rejuvenate', ['G']).
card_text('rejuvenate', 'You gain 6 life.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('rejuvenate', ['3', 'G']).
card_cmc('rejuvenate', 4).
card_layout('rejuvenate', 'normal').

% Found in: NMS
card_name('rejuvenation chamber', 'Rejuvenation Chamber').
card_type('rejuvenation chamber', 'Artifact').
card_types('rejuvenation chamber', ['Artifact']).
card_subtypes('rejuvenation chamber', []).
card_colors('rejuvenation chamber', []).
card_text('rejuvenation chamber', 'Fading 2 (This artifact enters the battlefield with two fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\n{T}: You gain 2 life.').
card_mana_cost('rejuvenation chamber', ['3']).
card_cmc('rejuvenation chamber', 3).
card_layout('rejuvenation chamber', 'normal').

% Found in: SOK
card_name('reki, the history of kamigawa', 'Reki, the History of Kamigawa').
card_type('reki, the history of kamigawa', 'Legendary Creature — Human Shaman').
card_types('reki, the history of kamigawa', ['Creature']).
card_subtypes('reki, the history of kamigawa', ['Human', 'Shaman']).
card_supertypes('reki, the history of kamigawa', ['Legendary']).
card_colors('reki, the history of kamigawa', ['G']).
card_text('reki, the history of kamigawa', 'Whenever you cast a legendary spell, draw a card.').
card_mana_cost('reki, the history of kamigawa', ['2', 'G']).
card_cmc('reki, the history of kamigawa', 3).
card_layout('reki, the history of kamigawa', 'normal').
card_power('reki, the history of kamigawa', 1).
card_toughness('reki, the history of kamigawa', 2).

% Found in: EVE
card_name('rekindled flame', 'Rekindled Flame').
card_type('rekindled flame', 'Sorcery').
card_types('rekindled flame', ['Sorcery']).
card_subtypes('rekindled flame', []).
card_colors('rekindled flame', ['R']).
card_text('rekindled flame', 'Rekindled Flame deals 4 damage to target creature or player.\nAt the beginning of your upkeep, if an opponent has no cards in hand, you may return Rekindled Flame from your graveyard to your hand.').
card_mana_cost('rekindled flame', ['2', 'R', 'R']).
card_cmc('rekindled flame', 4).
card_layout('rekindled flame', 'normal').

% Found in: SHM
card_name('reknit', 'Reknit').
card_type('reknit', 'Instant').
card_types('reknit', ['Instant']).
card_subtypes('reknit', []).
card_colors('reknit', ['W', 'G']).
card_text('reknit', 'Regenerate target permanent.').
card_mana_cost('reknit', ['1', 'G/W']).
card_cmc('reknit', 2).
card_layout('reknit', 'normal').

% Found in: 6ED, S99, WTH
card_name('relearn', 'Relearn').
card_type('relearn', 'Sorcery').
card_types('relearn', ['Sorcery']).
card_subtypes('relearn', []).
card_colors('relearn', ['U']).
card_text('relearn', 'Return target instant or sorcery card from your graveyard to your hand.').
card_mana_cost('relearn', ['1', 'U', 'U']).
card_cmc('relearn', 3).
card_layout('relearn', 'normal').

% Found in: DGM
card_name('release', 'Release').
card_type('release', 'Sorcery').
card_types('release', ['Sorcery']).
card_subtypes('release', []).
card_colors('release', ['W', 'R']).
card_text('release', 'Each player sacrifices an artifact, a creature, an enchantment, a land, and a planeswalker.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('release', ['4', 'R', 'W']).
card_cmc('release', 6).
card_layout('release', 'split').

% Found in: MOR
card_name('release the ants', 'Release the Ants').
card_type('release the ants', 'Instant').
card_types('release the ants', ['Instant']).
card_subtypes('release the ants', []).
card_colors('release the ants', ['R']).
card_text('release the ants', 'Release the Ants deals 1 damage to target creature or player. Clash with an opponent. If you win, return Release the Ants to its owner\'s hand. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('release the ants', ['1', 'R']).
card_cmc('release the ants', 2).
card_layout('release the ants', 'normal').

% Found in: 10E, 6ED, 7ED, 8ED, 9ED, HOP, PO2, PTK, S99, VIS
card_name('relentless assault', 'Relentless Assault').
card_type('relentless assault', 'Sorcery').
card_types('relentless assault', ['Sorcery']).
card_subtypes('relentless assault', []).
card_colors('relentless assault', ['R']).
card_text('relentless assault', 'Untap all creatures that attacked this turn. After this main phase, there is an additional combat phase followed by an additional main phase.').
card_mana_cost('relentless assault', ['2', 'R', 'R']).
card_cmc('relentless assault', 4).
card_layout('relentless assault', 'normal').

% Found in: 10E, 5DN, M10, M11
card_name('relentless rats', 'Relentless Rats').
card_type('relentless rats', 'Creature — Rat').
card_types('relentless rats', ['Creature']).
card_subtypes('relentless rats', ['Rat']).
card_colors('relentless rats', ['B']).
card_text('relentless rats', 'Relentless Rats gets +1/+1 for each other creature on the battlefield named Relentless Rats.\nA deck can have any number of cards named Relentless Rats.').
card_mana_cost('relentless rats', ['1', 'B', 'B']).
card_cmc('relentless rats', 3).
card_layout('relentless rats', 'normal').
card_power('relentless rats', 2).
card_toughness('relentless rats', 2).

% Found in: DKA
card_name('relentless skaabs', 'Relentless Skaabs').
card_type('relentless skaabs', 'Creature — Zombie').
card_types('relentless skaabs', ['Creature']).
card_subtypes('relentless skaabs', ['Zombie']).
card_colors('relentless skaabs', ['U']).
card_text('relentless skaabs', 'As an additional cost to cast Relentless Skaabs, exile a creature card from your graveyard.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('relentless skaabs', ['3', 'U', 'U']).
card_cmc('relentless skaabs', 5).
card_layout('relentless skaabs', 'normal').
card_power('relentless skaabs', 4).
card_toughness('relentless skaabs', 4).

% Found in: MRD
card_name('relic bane', 'Relic Bane').
card_type('relic bane', 'Enchantment — Aura').
card_types('relic bane', ['Enchantment']).
card_subtypes('relic bane', ['Aura']).
card_colors('relic bane', ['B']).
card_text('relic bane', 'Enchant artifact\nEnchanted artifact has \"At the beginning of your upkeep, you lose 2 life.\"').
card_mana_cost('relic bane', ['1', 'B', 'B']).
card_cmc('relic bane', 3).
card_layout('relic bane', 'normal').

% Found in: 5DN, LEG
card_name('relic barrier', 'Relic Barrier').
card_type('relic barrier', 'Artifact').
card_types('relic barrier', ['Artifact']).
card_subtypes('relic barrier', []).
card_colors('relic barrier', []).
card_text('relic barrier', '{T}: Tap target artifact.').
card_mana_cost('relic barrier', ['2']).
card_cmc('relic barrier', 2).
card_layout('relic barrier', 'normal').

% Found in: 4ED, LEG
card_name('relic bind', 'Relic Bind').
card_type('relic bind', 'Enchantment — Aura').
card_types('relic bind', ['Enchantment']).
card_subtypes('relic bind', ['Aura']).
card_colors('relic bind', ['U']).
card_text('relic bind', 'Enchant artifact an opponent controls\nWhenever enchanted artifact becomes tapped, choose one —\n• Relic Bind deals 1 damage to target player.\n• Target player gains 1 life.').
card_mana_cost('relic bind', ['2', 'U']).
card_cmc('relic bind', 3).
card_layout('relic bind', 'normal').

% Found in: CMD, CNS, ZEN
card_name('relic crush', 'Relic Crush').
card_type('relic crush', 'Instant').
card_types('relic crush', ['Instant']).
card_subtypes('relic crush', []).
card_colors('relic crush', ['G']).
card_text('relic crush', 'Destroy target artifact or enchantment and up to one other target artifact or enchantment.').
card_mana_cost('relic crush', ['4', 'G']).
card_cmc('relic crush', 5).
card_layout('relic crush', 'normal').

% Found in: ALA, HOP, MD1, MMA
card_name('relic of progenitus', 'Relic of Progenitus').
card_type('relic of progenitus', 'Artifact').
card_types('relic of progenitus', ['Artifact']).
card_subtypes('relic of progenitus', []).
card_colors('relic of progenitus', []).
card_text('relic of progenitus', '{T}: Target player exiles a card from his or her graveyard.\n{1}, Exile Relic of Progenitus: Exile all cards from all graveyards. Draw a card.').
card_mana_cost('relic of progenitus', ['1']).
card_cmc('relic of progenitus', 1).
card_layout('relic of progenitus', 'normal').

% Found in: SOM
card_name('relic putrescence', 'Relic Putrescence').
card_type('relic putrescence', 'Enchantment — Aura').
card_types('relic putrescence', ['Enchantment']).
card_subtypes('relic putrescence', ['Aura']).
card_colors('relic putrescence', ['B']).
card_text('relic putrescence', 'Enchant artifact\nWhenever enchanted artifact becomes tapped, its controller gets a poison counter.').
card_mana_cost('relic putrescence', ['2', 'B']).
card_cmc('relic putrescence', 3).
card_layout('relic putrescence', 'normal').

% Found in: ORI
card_name('relic seeker', 'Relic Seeker').
card_type('relic seeker', 'Creature — Human Soldier').
card_types('relic seeker', ['Creature']).
card_subtypes('relic seeker', ['Human', 'Soldier']).
card_colors('relic seeker', ['W']).
card_text('relic seeker', 'Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)\nWhen Relic Seeker becomes renowned, you may search your library for an Equipment card, reveal it, put it into your hand, then shuffle your library.').
card_mana_cost('relic seeker', ['1', 'W']).
card_cmc('relic seeker', 2).
card_layout('relic seeker', 'normal').
card_power('relic seeker', 2).
card_toughness('relic seeker', 2).

% Found in: VIS
card_name('relic ward', 'Relic Ward').
card_type('relic ward', 'Enchantment — Aura').
card_types('relic ward', ['Enchantment']).
card_subtypes('relic ward', ['Aura']).
card_colors('relic ward', ['W']).
card_text('relic ward', 'You may cast Relic Ward as though it had flash. If you cast it any time a sorcery couldn\'t have been cast, the controller of the permanent it becomes sacrifices it at the beginning of the next cleanup step.\nEnchant artifact\nEnchanted artifact has shroud. (It can\'t be the target of spells or abilities.)').
card_mana_cost('relic ward', ['1', 'W']).
card_cmc('relic ward', 2).
card_layout('relic ward', 'normal').

% Found in: UDS
card_name('reliquary monk', 'Reliquary Monk').
card_type('reliquary monk', 'Creature — Human Monk Cleric').
card_types('reliquary monk', ['Creature']).
card_subtypes('reliquary monk', ['Human', 'Monk', 'Cleric']).
card_colors('reliquary monk', ['W']).
card_text('reliquary monk', 'When Reliquary Monk dies, destroy target artifact or enchantment.').
card_mana_cost('reliquary monk', ['2', 'W']).
card_cmc('reliquary monk', 3).
card_layout('reliquary monk', 'normal').
card_power('reliquary monk', 2).
card_toughness('reliquary monk', 2).

% Found in: C14, CON, M13, pFNM
card_name('reliquary tower', 'Reliquary Tower').
card_type('reliquary tower', 'Land').
card_types('reliquary tower', ['Land']).
card_subtypes('reliquary tower', []).
card_colors('reliquary tower', []).
card_text('reliquary tower', 'You have no maximum hand size.\n{T}: Add {1} to your mana pool.').
card_layout('reliquary tower', 'normal').

% Found in: DDM, MM2, RAV, pFNM
card_name('remand', 'Remand').
card_type('remand', 'Instant').
card_types('remand', ['Instant']).
card_subtypes('remand', []).
card_colors('remand', ['U']).
card_text('remand', 'Counter target spell. If that spell is countered this way, put it into its owner\'s hand instead of into that player\'s graveyard.\nDraw a card.').
card_mana_cost('remand', ['1', 'U']).
card_cmc('remand', 2).
card_layout('remand', 'normal').

% Found in: 6ED, VIS
card_name('remedy', 'Remedy').
card_type('remedy', 'Instant').
card_types('remedy', ['Instant']).
card_subtypes('remedy', []).
card_colors('remedy', ['W']).
card_text('remedy', 'Prevent the next 5 damage that would be dealt this turn to any number of target creatures and/or players, divided as you choose.').
card_mana_cost('remedy', ['1', 'W']).
card_cmc('remedy', 2).
card_layout('remedy', 'normal').

% Found in: NPH
card_name('remember the fallen', 'Remember the Fallen').
card_type('remember the fallen', 'Sorcery').
card_types('remember the fallen', ['Sorcery']).
card_subtypes('remember the fallen', []).
card_colors('remember the fallen', ['W']).
card_text('remember the fallen', 'Choose one or both —\n• Return target creature card from your graveyard to your hand.\n• Return target artifact card from your graveyard to your hand.').
card_mana_cost('remember the fallen', ['2', 'W']).
card_cmc('remember the fallen', 3).
card_layout('remember the fallen', 'normal').

% Found in: USG
card_name('remembrance', 'Remembrance').
card_type('remembrance', 'Enchantment').
card_types('remembrance', ['Enchantment']).
card_subtypes('remembrance', []).
card_colors('remembrance', ['W']).
card_text('remembrance', 'Whenever a nontoken creature you control dies, you may search your library for a card with the same name as that creature, reveal it, and put it into your hand. If you do, shuffle your library.').
card_mana_cost('remembrance', ['3', 'W']).
card_cmc('remembrance', 4).
card_layout('remembrance', 'normal').

% Found in: 10E, 9ED, DDJ, ONS
card_name('reminisce', 'Reminisce').
card_type('reminisce', 'Sorcery').
card_types('reminisce', ['Sorcery']).
card_subtypes('reminisce', []).
card_colors('reminisce', ['U']).
card_text('reminisce', 'Target player shuffles his or her graveyard into his or her library.').
card_mana_cost('reminisce', ['2', 'U']).
card_cmc('reminisce', 3).
card_layout('reminisce', 'normal').

% Found in: UNH
card_name('remodel', 'Remodel').
card_type('remodel', 'Instant').
card_types('remodel', ['Instant']).
card_subtypes('remodel', []).
card_colors('remodel', ['G']).
card_text('remodel', 'If you control two or more green permanents that share an artist, you may play Remodel without paying its mana cost.\nRemove target artifact from the game.').
card_mana_cost('remodel', ['2', 'G']).
card_cmc('remodel', 3).
card_layout('remodel', 'normal').

% Found in: MMQ
card_name('remote farm', 'Remote Farm').
card_type('remote farm', 'Land').
card_types('remote farm', ['Land']).
card_subtypes('remote farm', []).
card_colors('remote farm', []).
card_text('remote farm', 'Remote Farm enters the battlefield tapped with two depletion counters on it.\n{T}, Remove a depletion counter from Remote Farm: Add {W}{W} to your mana pool. If there are no depletion counters on Remote Farm, sacrifice it.').
card_layout('remote farm', 'normal').

% Found in: BRB, BTD, C14, USG
card_name('remote isle', 'Remote Isle').
card_type('remote isle', 'Land').
card_types('remote isle', ['Land']).
card_subtypes('remote isle', []).
card_colors('remote isle', []).
card_text('remote isle', 'Remote Isle enters the battlefield tapped.\n{T}: Add {U} to your mana pool.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_layout('remote isle', 'normal').

% Found in: PO2
card_name('remove', 'Remove').
card_type('remove', 'Instant').
card_types('remove', ['Instant']).
card_subtypes('remove', []).
card_colors('remove', ['U']).
card_text('remove', 'Cast Remove only during the declare attackers step and only if you\'ve been attacked this step.\nReturn target attacking creature to its owner\'s hand.').
card_mana_cost('remove', ['U']).
card_cmc('remove', 1).
card_layout('remove', 'normal').

% Found in: LEG
card_name('remove enchantments', 'Remove Enchantments').
card_type('remove enchantments', 'Instant').
card_types('remove enchantments', ['Instant']).
card_subtypes('remove enchantments', []).
card_colors('remove enchantments', ['W']).
card_text('remove enchantments', 'Return to your hand all enchantments you both own and control, all Auras you own attached to permanents you control, and all Auras you own attached to attacking creatures your opponents control. Then destroy all other enchantments you control, all other Auras attached to permanents you control, and all other Auras attached to attacking creatures your opponents control.').
card_mana_cost('remove enchantments', ['W']).
card_cmc('remove enchantments', 1).
card_layout('remove enchantments', 'normal').

% Found in: 10E, 5ED, 6ED, 7ED, 8ED, 9ED, CHR, LEG, ME3, S99, pMPR
card_name('remove soul', 'Remove Soul').
card_type('remove soul', 'Instant').
card_types('remove soul', ['Instant']).
card_subtypes('remove soul', []).
card_colors('remove soul', ['U']).
card_text('remove soul', 'Counter target creature spell.').
card_mana_cost('remove soul', ['1', 'U']).
card_cmc('remove soul', 2).
card_layout('remove soul', 'normal').

% Found in: CHK
card_name('rend flesh', 'Rend Flesh').
card_type('rend flesh', 'Instant — Arcane').
card_types('rend flesh', ['Instant']).
card_subtypes('rend flesh', ['Arcane']).
card_colors('rend flesh', ['B']).
card_text('rend flesh', 'Destroy target non-Spirit creature.').
card_mana_cost('rend flesh', ['2', 'B']).
card_cmc('rend flesh', 3).
card_layout('rend flesh', 'normal').

% Found in: CHK
card_name('rend spirit', 'Rend Spirit').
card_type('rend spirit', 'Instant').
card_types('rend spirit', ['Instant']).
card_subtypes('rend spirit', []).
card_colors('rend spirit', ['B']).
card_text('rend spirit', 'Destroy target Spirit.').
card_mana_cost('rend spirit', ['2', 'B']).
card_cmc('rend spirit', 3).
card_layout('rend spirit', 'normal').

% Found in: EVE
card_name('rendclaw trow', 'Rendclaw Trow').
card_type('rendclaw trow', 'Creature — Troll').
card_types('rendclaw trow', ['Creature']).
card_subtypes('rendclaw trow', ['Troll']).
card_colors('rendclaw trow', ['B', 'G']).
card_text('rendclaw trow', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('rendclaw trow', ['2', 'B/G']).
card_cmc('rendclaw trow', 3).
card_layout('rendclaw trow', 'normal').
card_power('rendclaw trow', 2).
card_toughness('rendclaw trow', 2).

% Found in: DGM, pMEI
card_name('render silent', 'Render Silent').
card_type('render silent', 'Instant').
card_types('render silent', ['Instant']).
card_subtypes('render silent', []).
card_colors('render silent', ['W', 'U']).
card_text('render silent', 'Counter target spell. Its controller can\'t cast spells this turn.').
card_mana_cost('render silent', ['W', 'U', 'U']).
card_cmc('render silent', 3).
card_layout('render silent', 'normal').

% Found in: SOK
card_name('rending vines', 'Rending Vines').
card_type('rending vines', 'Instant — Arcane').
card_types('rending vines', ['Instant']).
card_subtypes('rending vines', ['Arcane']).
card_colors('rending vines', ['G']).
card_text('rending vines', 'Destroy target artifact or enchantment if its converted mana cost is less than or equal to the number of cards in your hand.\nDraw a card.').
card_mana_cost('rending vines', ['1', 'G', 'G']).
card_cmc('rending vines', 3).
card_layout('rending vines', 'normal').

% Found in: DTK
card_name('rending volley', 'Rending Volley').
card_type('rending volley', 'Instant').
card_types('rending volley', ['Instant']).
card_subtypes('rending volley', []).
card_colors('rending volley', ['R']).
card_text('rending volley', 'Rending Volley can\'t be countered by spells or abilities.\nRending Volley deals 4 damage to target white or blue creature.').
card_mana_cost('rending volley', ['R']).
card_cmc('rending volley', 1).
card_layout('rending volley', 'normal').

% Found in: AVR
card_name('renegade demon', 'Renegade Demon').
card_type('renegade demon', 'Creature — Demon').
card_types('renegade demon', ['Creature']).
card_subtypes('renegade demon', ['Demon']).
card_colors('renegade demon', ['B']).
card_text('renegade demon', '').
card_mana_cost('renegade demon', ['3', 'B', 'B']).
card_cmc('renegade demon', 5).
card_layout('renegade demon', 'normal').
card_power('renegade demon', 5).
card_toughness('renegade demon', 3).

% Found in: ROE
card_name('renegade doppelganger', 'Renegade Doppelganger').
card_type('renegade doppelganger', 'Creature — Shapeshifter').
card_types('renegade doppelganger', ['Creature']).
card_subtypes('renegade doppelganger', ['Shapeshifter']).
card_colors('renegade doppelganger', ['U']).
card_text('renegade doppelganger', 'Whenever another creature enters the battlefield under your control, you may have Renegade Doppelganger become a copy of that creature until end of turn. (If it does, it loses this ability for the rest of the turn.)').
card_mana_cost('renegade doppelganger', ['1', 'U']).
card_cmc('renegade doppelganger', 2).
card_layout('renegade doppelganger', 'normal').
card_power('renegade doppelganger', 0).
card_toughness('renegade doppelganger', 1).

% Found in: DGM
card_name('renegade krasis', 'Renegade Krasis').
card_type('renegade krasis', 'Creature — Beast Mutant').
card_types('renegade krasis', ['Creature']).
card_subtypes('renegade krasis', ['Beast', 'Mutant']).
card_colors('renegade krasis', ['G']).
card_text('renegade krasis', 'Evolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)\nWhenever Renegade Krasis evolves, put a +1/+1 counter on each other creature you control with a +1/+1 counter on it.').
card_mana_cost('renegade krasis', ['1', 'G', 'G']).
card_cmc('renegade krasis', 3).
card_layout('renegade krasis', 'normal').
card_power('renegade krasis', 3).
card_toughness('renegade krasis', 2).

% Found in: PTK
card_name('renegade troops', 'Renegade Troops').
card_type('renegade troops', 'Creature — Human Soldier').
card_types('renegade troops', ['Creature']).
card_subtypes('renegade troops', ['Human', 'Soldier']).
card_colors('renegade troops', ['R']).
card_text('renegade troops', 'Haste').
card_mana_cost('renegade troops', ['4', 'R']).
card_cmc('renegade troops', 5).
card_layout('renegade troops', 'normal').
card_power('renegade troops', 4).
card_toughness('renegade troops', 2).

% Found in: TMP, TPR
card_name('renegade warlord', 'Renegade Warlord').
card_type('renegade warlord', 'Creature — Human Soldier').
card_types('renegade warlord', ['Creature']).
card_subtypes('renegade warlord', ['Human', 'Soldier']).
card_colors('renegade warlord', ['R']).
card_text('renegade warlord', 'First strike\nWhenever Renegade Warlord attacks, each other attacking creature gets +1/+0 until end of turn.').
card_mana_cost('renegade warlord', ['4', 'R']).
card_cmc('renegade warlord', 5).
card_layout('renegade warlord', 'normal').
card_power('renegade warlord', 3).
card_toughness('renegade warlord', 3).

% Found in: HML
card_name('renewal', 'Renewal').
card_type('renewal', 'Sorcery').
card_types('renewal', ['Sorcery']).
card_subtypes('renewal', []).
card_colors('renewal', ['G']).
card_text('renewal', 'As an additional cost to cast Renewal, sacrifice a land.\nSearch your library for a basic land card and put that card onto the battlefield. Then shuffle your library.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('renewal', ['2', 'G']).
card_cmc('renewal', 3).
card_layout('renewal', 'normal').

% Found in: ONS, VMA
card_name('renewed faith', 'Renewed Faith').
card_type('renewed faith', 'Instant').
card_types('renewed faith', ['Instant']).
card_subtypes('renewed faith', []).
card_colors('renewed faith', ['W']).
card_text('renewed faith', 'You gain 6 life.\nCycling {1}{W} ({1}{W}, Discard this card: Draw a card.)\nWhen you cycle Renewed Faith, you may gain 2 life.').
card_mana_cost('renewed faith', ['2', 'W']).
card_cmc('renewed faith', 3).
card_layout('renewed faith', 'normal').

% Found in: POR
card_name('renewing dawn', 'Renewing Dawn').
card_type('renewing dawn', 'Sorcery').
card_types('renewing dawn', ['Sorcery']).
card_subtypes('renewing dawn', []).
card_colors('renewing dawn', ['W']).
card_text('renewing dawn', 'You gain 2 life for each Mountain target opponent controls.').
card_mana_cost('renewing dawn', ['1', 'W']).
card_cmc('renewing dawn', 2).
card_layout('renewing dawn', 'normal').

% Found in: PO2, S99
card_name('renewing touch', 'Renewing Touch').
card_type('renewing touch', 'Sorcery').
card_types('renewing touch', ['Sorcery']).
card_subtypes('renewing touch', []).
card_colors('renewing touch', ['G']).
card_text('renewing touch', 'Shuffle any number of target creature cards from your graveyard into your library.').
card_mana_cost('renewing touch', ['G']).
card_cmc('renewing touch', 1).
card_layout('renewing touch', 'normal').

% Found in: MMQ
card_name('renounce', 'Renounce').
card_type('renounce', 'Instant').
card_types('renounce', ['Instant']).
card_subtypes('renounce', []).
card_colors('renounce', ['W']).
card_text('renounce', 'Sacrifice any number of permanents. You gain 2 life for each permanent sacrificed this way.').
card_mana_cost('renounce', ['1', 'W']).
card_cmc('renounce', 2).
card_layout('renounce', 'normal').

% Found in: DGM
card_name('renounce the guilds', 'Renounce the Guilds').
card_type('renounce the guilds', 'Instant').
card_types('renounce the guilds', ['Instant']).
card_subtypes('renounce the guilds', []).
card_colors('renounce the guilds', ['W']).
card_text('renounce the guilds', 'Each player sacrifices a multicolored permanent.').
card_mana_cost('renounce the guilds', ['1', 'W']).
card_cmc('renounce the guilds', 2).
card_layout('renounce the guilds', 'normal').

% Found in: FRF
card_name('renowned weaponsmith', 'Renowned Weaponsmith').
card_type('renowned weaponsmith', 'Creature — Human Artificer').
card_types('renowned weaponsmith', ['Creature']).
card_subtypes('renowned weaponsmith', ['Human', 'Artificer']).
card_colors('renowned weaponsmith', ['U']).
card_text('renowned weaponsmith', '{T}: Add {2} to your mana pool. Spend this mana only to cast artifact spells or activate abilities of artifacts.\n{U}, {T}: Search your library for a card named Heart-Piercer Bow or Vial of Dragonfire, reveal it, put it into your hand, then shuffle your library.').
card_mana_cost('renowned weaponsmith', ['1', 'U']).
card_cmc('renowned weaponsmith', 2).
card_layout('renowned weaponsmith', 'normal').
card_power('renowned weaponsmith', 1).
card_toughness('renowned weaponsmith', 3).

% Found in: JOU
card_name('renowned weaver', 'Renowned Weaver').
card_type('renowned weaver', 'Creature — Human Shaman').
card_types('renowned weaver', ['Creature']).
card_subtypes('renowned weaver', ['Human', 'Shaman']).
card_colors('renowned weaver', ['G']).
card_text('renowned weaver', '{1}{G}, Sacrifice Renowned Weaver: Put a 1/3 green Spider enchantment creature token with reach onto the battlefield. (It can block creatures with flying.)').
card_mana_cost('renowned weaver', ['G']).
card_cmc('renowned weaver', 1).
card_layout('renowned weaver', 'normal').
card_power('renowned weaver', 1).
card_toughness('renowned weaver', 1).

% Found in: MIR
card_name('reparations', 'Reparations').
card_type('reparations', 'Enchantment').
card_types('reparations', ['Enchantment']).
card_subtypes('reparations', []).
card_colors('reparations', ['W', 'U']).
card_text('reparations', 'Whenever an opponent casts a spell that targets you or a creature you control, you may draw a card.').
card_mana_cost('reparations', ['1', 'W', 'U']).
card_cmc('reparations', 3).
card_layout('reparations', 'normal').
card_reserved('reparations').

% Found in: ROE
card_name('repay in kind', 'Repay in Kind').
card_type('repay in kind', 'Sorcery').
card_types('repay in kind', ['Sorcery']).
card_subtypes('repay in kind', []).
card_colors('repay in kind', ['B']).
card_text('repay in kind', 'Each player\'s life total becomes the lowest life total among all players.').
card_mana_cost('repay in kind', ['5', 'B', 'B']).
card_cmc('repay in kind', 7).
card_layout('repay in kind', 'normal').

% Found in: DDN, GPT, MM2
card_name('repeal', 'Repeal').
card_type('repeal', 'Instant').
card_types('repeal', ['Instant']).
card_subtypes('repeal', []).
card_colors('repeal', ['U']).
card_text('repeal', 'Return target nonland permanent with converted mana cost X to its owner\'s hand.\nDraw a card.').
card_mana_cost('repeal', ['X', 'U']).
card_cmc('repeal', 1).
card_layout('repeal', 'normal').

% Found in: ODY, VMA
card_name('repel', 'Repel').
card_type('repel', 'Instant').
card_types('repel', ['Instant']).
card_subtypes('repel', []).
card_colors('repel', ['U']).
card_text('repel', 'Put target creature on top of its owner\'s library.').
card_mana_cost('repel', ['3', 'U']).
card_cmc('repel', 4).
card_layout('repel', 'normal').

% Found in: SHM
card_name('repel intruders', 'Repel Intruders').
card_type('repel intruders', 'Instant').
card_types('repel intruders', ['Instant']).
card_subtypes('repel intruders', []).
card_colors('repel intruders', ['W', 'U']).
card_text('repel intruders', 'Put two 1/1 white Kithkin Soldier creature tokens onto the battlefield if {W} was spent to cast Repel Intruders. Counter up to one target creature spell if {U} was spent to cast Repel Intruders. (Do both if {W}{U} was spent.)').
card_mana_cost('repel intruders', ['3', 'W/U']).
card_cmc('repel intruders', 4).
card_layout('repel intruders', 'normal').

% Found in: DDP, ROE
card_name('repel the darkness', 'Repel the Darkness').
card_type('repel the darkness', 'Instant').
card_types('repel the darkness', ['Instant']).
card_subtypes('repel the darkness', []).
card_colors('repel the darkness', ['W']).
card_text('repel the darkness', 'Tap up to two target creatures.\nDraw a card.').
card_mana_cost('repel the darkness', ['2', 'W']).
card_cmc('repel the darkness', 3).
card_layout('repel the darkness', 'normal').

% Found in: TMP, TPR
card_name('repentance', 'Repentance').
card_type('repentance', 'Sorcery').
card_types('repentance', ['Sorcery']).
card_subtypes('repentance', []).
card_colors('repentance', ['W']).
card_text('repentance', 'Target creature deals damage to itself equal to its power.').
card_mana_cost('repentance', ['2', 'W']).
card_cmc('repentance', 3).
card_layout('repentance', 'normal').

% Found in: 5ED, ARN, CHR
card_name('repentant blacksmith', 'Repentant Blacksmith').
card_type('repentant blacksmith', 'Creature — Human').
card_types('repentant blacksmith', ['Creature']).
card_subtypes('repentant blacksmith', ['Human']).
card_colors('repentant blacksmith', ['W']).
card_text('repentant blacksmith', 'Protection from red').
card_mana_cost('repentant blacksmith', ['1', 'W']).
card_cmc('repentant blacksmith', 2).
card_layout('repentant blacksmith', 'normal').
card_power('repentant blacksmith', 1).
card_toughness('repentant blacksmith', 2).

% Found in: ODY
card_name('repentant vampire', 'Repentant Vampire').
card_type('repentant vampire', 'Creature — Vampire').
card_types('repentant vampire', ['Creature']).
card_subtypes('repentant vampire', ['Vampire']).
card_colors('repentant vampire', ['B']).
card_text('repentant vampire', 'Flying\nWhenever a creature dealt damage by Repentant Vampire this turn dies, put a +1/+1 counter on Repentant Vampire.\nThreshold — As long as seven or more cards are in your graveyard, Repentant Vampire is white and has \"{T}: Destroy target black creature.\"').
card_mana_cost('repentant vampire', ['3', 'B', 'B']).
card_cmc('repentant vampire', 5).
card_layout('repentant vampire', 'normal').
card_power('repentant vampire', 3).
card_toughness('repentant vampire', 3).

% Found in: UDS
card_name('repercussion', 'Repercussion').
card_type('repercussion', 'Enchantment').
card_types('repercussion', ['Enchantment']).
card_subtypes('repercussion', []).
card_colors('repercussion', ['R']).
card_text('repercussion', 'Whenever a creature is dealt damage, Repercussion deals that much damage to that creature\'s controller.').
card_mana_cost('repercussion', ['1', 'R', 'R']).
card_cmc('repercussion', 3).
card_layout('repercussion', 'normal').

% Found in: UDS
card_name('replenish', 'Replenish').
card_type('replenish', 'Sorcery').
card_types('replenish', ['Sorcery']).
card_subtypes('replenish', []).
card_colors('replenish', ['W']).
card_text('replenish', 'Return all enchantment cards from your graveyard to the battlefield. (Auras with nothing to enchant remain in your graveyard.)').
card_mana_cost('replenish', ['3', 'W']).
card_cmc('replenish', 4).
card_layout('replenish', 'normal').
card_reserved('replenish').

% Found in: ULG
card_name('repopulate', 'Repopulate').
card_type('repopulate', 'Instant').
card_types('repopulate', ['Instant']).
card_subtypes('repopulate', []).
card_colors('repopulate', ['G']).
card_text('repopulate', 'Shuffle all creature cards from target player\'s graveyard into that player\'s library.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('repopulate', ['1', 'G']).
card_cmc('repopulate', 2).
card_layout('repopulate', 'normal').

% Found in: 6ED, 7ED, ALL, DDG, JOU, ME2
card_name('reprisal', 'Reprisal').
card_type('reprisal', 'Instant').
card_types('reprisal', ['Instant']).
card_subtypes('reprisal', []).
card_colors('reprisal', ['W']).
card_text('reprisal', 'Destroy target creature with power 4 or greater. It can\'t be regenerated.').
card_mana_cost('reprisal', ['1', 'W']).
card_cmc('reprisal', 2).
card_layout('reprisal', 'normal').

% Found in: 7ED, USG
card_name('reprocess', 'Reprocess').
card_type('reprocess', 'Sorcery').
card_types('reprocess', ['Sorcery']).
card_subtypes('reprocess', []).
card_colors('reprocess', ['B']).
card_text('reprocess', 'Sacrifice any number of artifacts, creatures, and/or lands. Draw a card for each permanent sacrificed this way.').
card_mana_cost('reprocess', ['2', 'B', 'B']).
card_cmc('reprocess', 4).
card_layout('reprocess', 'normal').

% Found in: CMD, DD2, DD3_JVC, INV
card_name('repulse', 'Repulse').
card_type('repulse', 'Instant').
card_types('repulse', ['Instant']).
card_subtypes('repulse', []).
card_colors('repulse', ['U']).
card_text('repulse', 'Return target creature to its owner\'s hand.\nDraw a card.').
card_mana_cost('repulse', ['2', 'U']).
card_cmc('repulse', 3).
card_layout('repulse', 'normal').

% Found in: C14, DKA
card_name('requiem angel', 'Requiem Angel').
card_type('requiem angel', 'Creature — Angel').
card_types('requiem angel', ['Creature']).
card_subtypes('requiem angel', ['Angel']).
card_colors('requiem angel', ['W']).
card_text('requiem angel', 'Flying\nWhenever another non-Spirit creature you control dies, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_mana_cost('requiem angel', ['5', 'W']).
card_cmc('requiem angel', 6).
card_layout('requiem angel', 'normal').
card_power('requiem angel', 5).
card_toughness('requiem angel', 5).

% Found in: RAV
card_name('reroute', 'Reroute').
card_type('reroute', 'Instant').
card_types('reroute', ['Instant']).
card_subtypes('reroute', []).
card_colors('reroute', ['R']).
card_text('reroute', 'Change the target of target activated ability with a single target. (Mana abilities can\'t be targeted.)\nDraw a card.').
card_mana_cost('reroute', ['1', 'R']).
card_cmc('reroute', 2).
card_layout('reroute', 'normal').

% Found in: USG, VMA
card_name('rescind', 'Rescind').
card_type('rescind', 'Instant').
card_types('rescind', ['Instant']).
card_subtypes('rescind', []).
card_colors('rescind', ['U']).
card_text('rescind', 'Return target permanent to its owner\'s hand.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('rescind', ['1', 'U', 'U']).
card_cmc('rescind', 3).
card_layout('rescind', 'normal').

% Found in: UDS
card_name('rescue', 'Rescue').
card_type('rescue', 'Instant').
card_types('rescue', ['Instant']).
card_subtypes('rescue', []).
card_colors('rescue', ['U']).
card_text('rescue', 'Return target permanent you control to its owner\'s hand.').
card_mana_cost('rescue', ['U']).
card_cmc('rescue', 1).
card_layout('rescue', 'normal').

% Found in: THS
card_name('rescue from the underworld', 'Rescue from the Underworld').
card_type('rescue from the underworld', 'Instant').
card_types('rescue from the underworld', ['Instant']).
card_subtypes('rescue from the underworld', []).
card_colors('rescue from the underworld', ['B']).
card_text('rescue from the underworld', 'As an additional cost to cast Rescue from the Underworld, sacrifice a creature.\nChoose target creature card in your graveyard. Return that card and the sacrificed card to the battlefield under your control at the beginning of your next upkeep. Exile Rescue from the Underworld.').
card_mana_cost('rescue from the underworld', ['4', 'B']).
card_cmc('rescue from the underworld', 5).
card_layout('rescue from the underworld', 'normal').

% Found in: DIS
card_name('research', 'Research').
card_type('research', 'Instant').
card_types('research', ['Instant']).
card_subtypes('research', []).
card_colors('research', ['U', 'G']).
card_text('research', 'Choose up to four cards you own from outside the game and shuffle them into your library.').
card_mana_cost('research', ['G', 'U']).
card_cmc('research', 2).
card_layout('research', 'split').
card_sides('research', 'development').

% Found in: M15
card_name('research assistant', 'Research Assistant').
card_type('research assistant', 'Creature — Human Wizard').
card_types('research assistant', ['Creature']).
card_subtypes('research assistant', ['Human', 'Wizard']).
card_colors('research assistant', ['U']).
card_text('research assistant', '{3}{U}, {T}: Draw a card, then discard a card.').
card_mana_cost('research assistant', ['1', 'U']).
card_cmc('research assistant', 2).
card_layout('research assistant', 'normal').
card_power('research assistant', 1).
card_toughness('research assistant', 3).

% Found in: MOR
card_name('research the deep', 'Research the Deep').
card_type('research the deep', 'Sorcery').
card_types('research the deep', ['Sorcery']).
card_subtypes('research the deep', []).
card_colors('research the deep', ['U']).
card_text('research the deep', 'Draw a card. Clash with an opponent. If you win, return Research the Deep to its owner\'s hand. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('research the deep', ['1', 'U']).
card_cmc('research the deep', 2).
card_layout('research the deep', 'normal').

% Found in: LEG, ME3
card_name('reset', 'Reset').
card_type('reset', 'Instant').
card_types('reset', ['Instant']).
card_subtypes('reset', []).
card_colors('reset', ['U']).
card_text('reset', 'Cast Reset only during an opponent\'s turn after his or her upkeep step.\nUntap all lands you control.').
card_mana_cost('reset', ['U', 'U']).
card_cmc('reset', 2).
card_layout('reset', 'normal').

% Found in: DST
card_name('reshape', 'Reshape').
card_type('reshape', 'Sorcery').
card_types('reshape', ['Sorcery']).
card_subtypes('reshape', []).
card_colors('reshape', ['U']).
card_text('reshape', 'As an additional cost to cast Reshape, sacrifice an artifact.\nSearch your library for an artifact card with converted mana cost X or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('reshape', ['X', 'U', 'U']).
card_cmc('reshape', 2).
card_layout('reshape', 'normal').

% Found in: ODY
card_name('resilient wanderer', 'Resilient Wanderer').
card_type('resilient wanderer', 'Creature — Human Nomad').
card_types('resilient wanderer', ['Creature']).
card_subtypes('resilient wanderer', ['Human', 'Nomad']).
card_colors('resilient wanderer', ['W']).
card_text('resilient wanderer', 'First strike\nDiscard a card: Resilient Wanderer gains protection from the color of your choice until end of turn.').
card_mana_cost('resilient wanderer', ['2', 'W', 'W']).
card_cmc('resilient wanderer', 4).
card_layout('resilient wanderer', 'normal').
card_power('resilient wanderer', 2).
card_toughness('resilient wanderer', 3).

% Found in: 6ED, VIS
card_name('resistance fighter', 'Resistance Fighter').
card_type('resistance fighter', 'Creature — Human Soldier').
card_types('resistance fighter', ['Creature']).
card_subtypes('resistance fighter', ['Human', 'Soldier']).
card_colors('resistance fighter', ['W']).
card_text('resistance fighter', 'Sacrifice Resistance Fighter: Prevent all combat damage target creature would deal this turn.').
card_mana_cost('resistance fighter', ['W']).
card_cmc('resistance fighter', 1).
card_layout('resistance fighter', 'normal').
card_power('resistance fighter', 1).
card_toughness('resistance fighter', 1).

% Found in: CSP
card_name('resize', 'Resize').
card_type('resize', 'Instant').
card_types('resize', ['Instant']).
card_subtypes('resize', []).
card_colors('resize', ['G']).
card_text('resize', 'Target creature gets +3/+3 until end of turn.\nRecover {1}{G} (When a creature is put into your graveyard from the battlefield, you may pay {1}{G}. If you do, return this card from your graveyard to your hand. Otherwise, exile this card.)').
card_mana_cost('resize', ['1', 'G']).
card_cmc('resize', 2).
card_layout('resize', 'normal').

% Found in: M15, pPRE
card_name('resolute archangel', 'Resolute Archangel').
card_type('resolute archangel', 'Creature — Angel').
card_types('resolute archangel', ['Creature']).
card_subtypes('resolute archangel', ['Angel']).
card_colors('resolute archangel', ['W']).
card_text('resolute archangel', 'Flying\nWhen Resolute Archangel enters the battlefield, if your life total is less than your starting life total, it becomes equal to your starting life total.').
card_mana_cost('resolute archangel', ['5', 'W', 'W']).
card_cmc('resolute archangel', 7).
card_layout('resolute archangel', 'normal').
card_power('resolute archangel', 4).
card_toughness('resolute archangel', 4).

% Found in: BFZ
card_name('resolute blademaster', 'Resolute Blademaster').
card_type('resolute blademaster', 'Creature — Human Soldier Ally').
card_types('resolute blademaster', ['Creature']).
card_subtypes('resolute blademaster', ['Human', 'Soldier', 'Ally']).
card_colors('resolute blademaster', ['W', 'R']).
card_text('resolute blademaster', 'Rally — Whenever Resolute Blademaster or another Ally enters the battlefield under your control, creatures you control gain double strike until end of turn.').
card_mana_cost('resolute blademaster', ['3', 'R', 'W']).
card_cmc('resolute blademaster', 5).
card_layout('resolute blademaster', 'normal').
card_power('resolute blademaster', 2).
card_toughness('resolute blademaster', 2).

% Found in: ALA
card_name('resounding roar', 'Resounding Roar').
card_type('resounding roar', 'Instant').
card_types('resounding roar', ['Instant']).
card_subtypes('resounding roar', []).
card_colors('resounding roar', ['G']).
card_text('resounding roar', 'Target creature gets +3/+3 until end of turn.\nCycling {5}{R}{G}{W} ({5}{R}{G}{W}, Discard this card: Draw a card.)\nWhen you cycle Resounding Roar, target creature gets +6/+6 until end of turn.').
card_mana_cost('resounding roar', ['1', 'G']).
card_cmc('resounding roar', 2).
card_layout('resounding roar', 'normal').

% Found in: ALA
card_name('resounding scream', 'Resounding Scream').
card_type('resounding scream', 'Sorcery').
card_types('resounding scream', ['Sorcery']).
card_subtypes('resounding scream', []).
card_colors('resounding scream', ['B']).
card_text('resounding scream', 'Target player discards a card at random.\nCycling {5}{U}{B}{R} ({5}{U}{B}{R}, Discard this card: Draw a card.)\nWhen you cycle Resounding Scream, target player discards two cards at random.').
card_mana_cost('resounding scream', ['2', 'B']).
card_cmc('resounding scream', 3).
card_layout('resounding scream', 'normal').

% Found in: ALA
card_name('resounding silence', 'Resounding Silence').
card_type('resounding silence', 'Instant').
card_types('resounding silence', ['Instant']).
card_subtypes('resounding silence', []).
card_colors('resounding silence', ['W']).
card_text('resounding silence', 'Exile target attacking creature.\nCycling {5}{G}{W}{U} ({5}{G}{W}{U}, Discard this card: Draw a card.)\nWhen you cycle Resounding Silence, exile up to two target attacking creatures.').
card_mana_cost('resounding silence', ['3', 'W']).
card_cmc('resounding silence', 4).
card_layout('resounding silence', 'normal').

% Found in: ALA
card_name('resounding thunder', 'Resounding Thunder').
card_type('resounding thunder', 'Instant').
card_types('resounding thunder', ['Instant']).
card_subtypes('resounding thunder', []).
card_colors('resounding thunder', ['R']).
card_text('resounding thunder', 'Resounding Thunder deals 3 damage to target creature or player.\nCycling {5}{B}{R}{G} ({5}{B}{R}{G}, Discard this card: Draw a card.)\nWhen you cycle Resounding Thunder, it deals 6 damage to target creature or player.').
card_mana_cost('resounding thunder', ['2', 'R']).
card_cmc('resounding thunder', 3).
card_layout('resounding thunder', 'normal').

% Found in: ALA
card_name('resounding wave', 'Resounding Wave').
card_type('resounding wave', 'Instant').
card_types('resounding wave', ['Instant']).
card_subtypes('resounding wave', []).
card_colors('resounding wave', ['U']).
card_text('resounding wave', 'Return target permanent to its owner\'s hand.\nCycling {5}{W}{U}{B} ({5}{W}{U}{B}, Discard this card: Draw a card.)\nWhen you cycle Resounding Wave, return two target permanents to their owners\' hands.').
card_mana_cost('resounding wave', ['2', 'U']).
card_cmc('resounding wave', 3).
card_layout('resounding wave', 'normal').

% Found in: CNS, TMP
card_name('respite', 'Respite').
card_type('respite', 'Instant').
card_types('respite', ['Instant']).
card_subtypes('respite', []).
card_colors('respite', ['G']).
card_text('respite', 'Prevent all combat damage that would be dealt this turn. You gain 1 life for each attacking creature.').
card_mana_cost('respite', ['1', 'G']).
card_cmc('respite', 2).
card_layout('respite', 'normal').

% Found in: SHM
card_name('resplendent mentor', 'Resplendent Mentor').
card_type('resplendent mentor', 'Creature — Kithkin Cleric').
card_types('resplendent mentor', ['Creature']).
card_subtypes('resplendent mentor', ['Kithkin', 'Cleric']).
card_colors('resplendent mentor', ['W']).
card_text('resplendent mentor', 'White creatures you control have \"{T}: You gain 1 life.\"').
card_mana_cost('resplendent mentor', ['4', 'W']).
card_cmc('resplendent mentor', 5).
card_layout('resplendent mentor', 'normal').
card_power('resplendent mentor', 2).
card_toughness('resplendent mentor', 2).

% Found in: WWK
card_name('rest for the weary', 'Rest for the Weary').
card_type('rest for the weary', 'Instant').
card_types('rest for the weary', ['Instant']).
card_subtypes('rest for the weary', []).
card_colors('rest for the weary', ['W']).
card_text('rest for the weary', 'Target player gains 4 life.\nLandfall — If you had a land enter the battlefield under your control this turn, that player gains 8 life instead.').
card_mana_cost('rest for the weary', ['1', 'W']).
card_cmc('rest for the weary', 2).
card_layout('rest for the weary', 'normal').

% Found in: RTR
card_name('rest in peace', 'Rest in Peace').
card_type('rest in peace', 'Enchantment').
card_types('rest in peace', ['Enchantment']).
card_subtypes('rest in peace', []).
card_colors('rest in peace', ['W']).
card_text('rest in peace', 'When Rest in Peace enters the battlefield, exile all cards from all graveyards.\nIf a card or token would be put into a graveyard from anywhere, exile it instead.').
card_mana_cost('rest in peace', ['1', 'W']).
card_cmc('rest in peace', 2).
card_layout('rest in peace', 'normal').

% Found in: EVE, MM2
card_name('restless apparition', 'Restless Apparition').
card_type('restless apparition', 'Creature — Spirit').
card_types('restless apparition', ['Creature']).
card_subtypes('restless apparition', ['Spirit']).
card_colors('restless apparition', ['W', 'B']).
card_text('restless apparition', '{W/B}{W/B}{W/B}: Restless Apparition gets +3/+3 until end of turn.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('restless apparition', ['W/B', 'W/B', 'W/B']).
card_cmc('restless apparition', 3).
card_layout('restless apparition', 'normal').
card_power('restless apparition', 2).
card_toughness('restless apparition', 2).

% Found in: GPT
card_name('restless bones', 'Restless Bones').
card_type('restless bones', 'Creature — Skeleton').
card_types('restless bones', ['Creature']).
card_subtypes('restless bones', ['Skeleton']).
card_colors('restless bones', ['B']).
card_text('restless bones', '{3}{B}, {T}: Target creature gains swampwalk until end of turn. (It can\'t be blocked as long as defending player controls a Swamp.)\n{1}{B}: Regenerate Restless Bones.').
card_mana_cost('restless bones', ['2', 'B']).
card_cmc('restless bones', 3).
card_layout('restless bones', 'normal').
card_power('restless bones', 1).
card_toughness('restless bones', 1).

% Found in: MIR
card_name('restless dead', 'Restless Dead').
card_type('restless dead', 'Creature — Skeleton').
card_types('restless dead', ['Creature']).
card_subtypes('restless dead', ['Skeleton']).
card_colors('restless dead', ['B']).
card_text('restless dead', '{B}: Regenerate Restless Dead.').
card_mana_cost('restless dead', ['1', 'B']).
card_cmc('restless dead', 2).
card_layout('restless dead', 'normal').
card_power('restless dead', 1).
card_toughness('restless dead', 1).

% Found in: TOR
card_name('restless dreams', 'Restless Dreams').
card_type('restless dreams', 'Sorcery').
card_types('restless dreams', ['Sorcery']).
card_subtypes('restless dreams', []).
card_colors('restless dreams', ['B']).
card_text('restless dreams', 'As an additional cost to cast Restless Dreams, discard X cards.\nReturn X target creature cards from your graveyard to your hand.').
card_mana_cost('restless dreams', ['B']).
card_cmc('restless dreams', 1).
card_layout('restless dreams', 'normal').

% Found in: INV, M15
card_name('restock', 'Restock').
card_type('restock', 'Sorcery').
card_types('restock', ['Sorcery']).
card_subtypes('restock', []).
card_colors('restock', ['G']).
card_text('restock', 'Return two target cards from your graveyard to your hand. Exile Restock.').
card_mana_cost('restock', ['3', 'G', 'G']).
card_cmc('restock', 5).
card_layout('restock', 'normal').

% Found in: AVR, pLPA
card_name('restoration angel', 'Restoration Angel').
card_type('restoration angel', 'Creature — Angel').
card_types('restoration angel', ['Creature']).
card_subtypes('restoration angel', ['Angel']).
card_colors('restoration angel', ['W']).
card_text('restoration angel', 'Flash\nFlying\nWhen Restoration Angel enters the battlefield, you may exile target non-Angel creature you control, then return that card to the battlefield under your control.').
card_mana_cost('restoration angel', ['3', 'W']).
card_cmc('restoration angel', 4).
card_layout('restoration angel', 'normal').
card_power('restoration angel', 3).
card_toughness('restoration angel', 4).

% Found in: C13
card_name('restore', 'Restore').
card_type('restore', 'Sorcery').
card_types('restore', ['Sorcery']).
card_subtypes('restore', []).
card_colors('restore', ['G']).
card_text('restore', 'Put target land card from a graveyard onto the battlefield under your control.').
card_mana_cost('restore', ['1', 'G']).
card_cmc('restore', 2).
card_layout('restore', 'normal').

% Found in: TSP
card_name('restore balance', 'Restore Balance').
card_type('restore balance', 'Sorcery').
card_types('restore balance', ['Sorcery']).
card_subtypes('restore balance', []).
card_colors('restore balance', ['W']).
card_text('restore balance', 'Suspend 6—{W} (Rather than cast this card from your hand, pay {W} and exile it with six time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)\nEach player chooses a number of lands he or she controls equal to the number of lands controlled by the player who controls the fewest, then sacrifices the rest. Players sacrifice creatures and discard cards the same way.').
card_layout('restore balance', 'normal').

% Found in: DGM
card_name('restore the peace', 'Restore the Peace').
card_type('restore the peace', 'Instant').
card_types('restore the peace', ['Instant']).
card_subtypes('restore the peace', []).
card_colors('restore the peace', ['W', 'U']).
card_text('restore the peace', 'Return each creature that dealt damage this turn to its owner\'s hand.').
card_mana_cost('restore the peace', ['1', 'W', 'U']).
card_cmc('restore the peace', 3).
card_layout('restore the peace', 'normal').

% Found in: INV
card_name('restrain', 'Restrain').
card_type('restrain', 'Instant').
card_types('restrain', ['Instant']).
card_subtypes('restrain', []).
card_colors('restrain', ['W']).
card_text('restrain', 'Prevent all combat damage that would be dealt by target attacking creature this turn.\nDraw a card.').
card_mana_cost('restrain', ['2', 'W']).
card_cmc('restrain', 3).
card_layout('restrain', 'normal').

% Found in: DTK
card_name('resupply', 'Resupply').
card_type('resupply', 'Instant').
card_types('resupply', ['Instant']).
card_subtypes('resupply', []).
card_colors('resupply', ['W']).
card_text('resupply', 'You gain 6 life.\nDraw a card.').
card_mana_cost('resupply', ['5', 'W']).
card_cmc('resupply', 6).
card_layout('resupply', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, TSB, pFNM
card_name('resurrection', 'Resurrection').
card_type('resurrection', 'Sorcery').
card_types('resurrection', ['Sorcery']).
card_subtypes('resurrection', []).
card_colors('resurrection', ['W']).
card_text('resurrection', 'Return target creature card from your graveyard to the battlefield.').
card_mana_cost('resurrection', ['2', 'W', 'W']).
card_cmc('resurrection', 4).
card_layout('resurrection', 'normal').

% Found in: EXO
card_name('resuscitate', 'Resuscitate').
card_type('resuscitate', 'Instant').
card_types('resuscitate', ['Instant']).
card_subtypes('resuscitate', []).
card_colors('resuscitate', ['G']).
card_text('resuscitate', 'Until end of turn, creatures you control gain \"{1}: Regenerate this creature.\"').
card_mana_cost('resuscitate', ['1', 'G']).
card_cmc('resuscitate', 2).
card_layout('resuscitate', 'normal').

% Found in: 5DN
card_name('retaliate', 'Retaliate').
card_type('retaliate', 'Instant').
card_types('retaliate', ['Instant']).
card_subtypes('retaliate', []).
card_colors('retaliate', ['W']).
card_text('retaliate', 'Destroy all creatures that dealt damage to you this turn.').
card_mana_cost('retaliate', ['2', 'W', 'W']).
card_cmc('retaliate', 4).
card_layout('retaliate', 'normal').

% Found in: USG
card_name('retaliation', 'Retaliation').
card_type('retaliation', 'Enchantment').
card_types('retaliation', ['Enchantment']).
card_subtypes('retaliation', []).
card_colors('retaliation', ['G']).
card_text('retaliation', 'Creatures you control have \"Whenever this creature becomes blocked by a creature, this creature gets +1/+1 until end of turn.\"').
card_mana_cost('retaliation', ['2', 'G']).
card_cmc('retaliation', 3).
card_layout('retaliation', 'normal').

% Found in: ARB, pMEI
card_name('retaliator griffin', 'Retaliator Griffin').
card_type('retaliator griffin', 'Creature — Griffin').
card_types('retaliator griffin', ['Creature']).
card_subtypes('retaliator griffin', ['Griffin']).
card_colors('retaliator griffin', ['W', 'R', 'G']).
card_text('retaliator griffin', 'Flying\nWhenever a source an opponent controls deals damage to you, you may put that many +1/+1 counters on Retaliator Griffin.').
card_mana_cost('retaliator griffin', ['1', 'R', 'G', 'W']).
card_cmc('retaliator griffin', 4).
card_layout('retaliator griffin', 'normal').
card_power('retaliator griffin', 2).
card_toughness('retaliator griffin', 2).

% Found in: PLC
card_name('retether', 'Retether').
card_type('retether', 'Sorcery').
card_types('retether', ['Sorcery']).
card_subtypes('retether', []).
card_colors('retether', ['W']).
card_text('retether', 'Return each Aura card from your graveyard to the battlefield. Only creatures can be enchanted this way. (Aura cards that can\'t enchant a creature on the battlefield remain in your graveyard.)').
card_mana_cost('retether', ['3', 'W']).
card_cmc('retether', 4).
card_layout('retether', 'normal').

% Found in: PCY
card_name('rethink', 'Rethink').
card_type('rethink', 'Instant').
card_types('rethink', ['Instant']).
card_subtypes('rethink', []).
card_colors('rethink', ['U']).
card_text('rethink', 'Counter target spell unless its controller pays {X}, where X is its converted mana cost.').
card_mana_cost('rethink', ['2', 'U']).
card_cmc('rethink', 3).
card_layout('rethink', 'normal').

% Found in: TOR
card_name('retraced image', 'Retraced Image').
card_type('retraced image', 'Sorcery').
card_types('retraced image', ['Sorcery']).
card_subtypes('retraced image', []).
card_colors('retraced image', ['U']).
card_text('retraced image', 'Reveal a card in your hand, then put that card onto the battlefield if it has the same name as a permanent.').
card_mana_cost('retraced image', ['U']).
card_cmc('retraced image', 1).
card_layout('retraced image', 'normal').

% Found in: DST
card_name('retract', 'Retract').
card_type('retract', 'Instant').
card_types('retract', ['Instant']).
card_subtypes('retract', []).
card_colors('retract', ['U']).
card_text('retract', 'Return all artifacts you control to their owner\'s hand.').
card_mana_cost('retract', ['U']).
card_cmc('retract', 1).
card_layout('retract', 'normal').

% Found in: BNG
card_name('retraction helix', 'Retraction Helix').
card_type('retraction helix', 'Instant').
card_types('retraction helix', ['Instant']).
card_subtypes('retraction helix', []).
card_colors('retraction helix', ['U']).
card_text('retraction helix', 'Until end of turn, target creature gains \"{T}: Return target nonland permanent to its owner\'s hand.\"').
card_mana_cost('retraction helix', ['U']).
card_cmc('retraction helix', 1).
card_layout('retraction helix', 'normal').

% Found in: BFZ
card_name('retreat to coralhelm', 'Retreat to Coralhelm').
card_type('retreat to coralhelm', 'Enchantment').
card_types('retreat to coralhelm', ['Enchantment']).
card_subtypes('retreat to coralhelm', []).
card_colors('retreat to coralhelm', ['U']).
card_text('retreat to coralhelm', 'Landfall — Whenever a land enters the battlefield under your control, choose one —\n• You may tap or untap target creature.\n• Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('retreat to coralhelm', ['2', 'U']).
card_cmc('retreat to coralhelm', 3).
card_layout('retreat to coralhelm', 'normal').

% Found in: BFZ
card_name('retreat to emeria', 'Retreat to Emeria').
card_type('retreat to emeria', 'Enchantment').
card_types('retreat to emeria', ['Enchantment']).
card_subtypes('retreat to emeria', []).
card_colors('retreat to emeria', ['W']).
card_text('retreat to emeria', 'Landfall — Whenever a land enters the battlefield under your control, choose one —\n• Put a 1/1 white Kor Ally creature token onto the battlefield.\n• Creatures you control get +1/+1 until end of turn.').
card_mana_cost('retreat to emeria', ['3', 'W']).
card_cmc('retreat to emeria', 4).
card_layout('retreat to emeria', 'normal').

% Found in: BFZ
card_name('retreat to hagra', 'Retreat to Hagra').
card_type('retreat to hagra', 'Enchantment').
card_types('retreat to hagra', ['Enchantment']).
card_subtypes('retreat to hagra', []).
card_colors('retreat to hagra', ['B']).
card_text('retreat to hagra', 'Landfall — Whenever a land enters the battlefield under your control, choose one —\n• Target creature gets +1/+0 and gains deathtouch until end of turn.\n• Each opponent loses 1 life and you gain 1 life.').
card_mana_cost('retreat to hagra', ['2', 'B']).
card_cmc('retreat to hagra', 3).
card_layout('retreat to hagra', 'normal').

% Found in: BFZ, DDP
card_name('retreat to kazandu', 'Retreat to Kazandu').
card_type('retreat to kazandu', 'Enchantment').
card_types('retreat to kazandu', ['Enchantment']).
card_subtypes('retreat to kazandu', []).
card_colors('retreat to kazandu', ['G']).
card_text('retreat to kazandu', 'Landfall — Whenever a land enters the battlefield under your control, choose one —\n• Put a +1/+1 counter on target creature.\n• You gain 2 life.').
card_mana_cost('retreat to kazandu', ['2', 'G']).
card_cmc('retreat to kazandu', 3).
card_layout('retreat to kazandu', 'normal').

% Found in: BFZ
card_name('retreat to valakut', 'Retreat to Valakut').
card_type('retreat to valakut', 'Enchantment').
card_types('retreat to valakut', ['Enchantment']).
card_subtypes('retreat to valakut', []).
card_colors('retreat to valakut', ['R']).
card_text('retreat to valakut', 'Landfall — Whenever a land enters the battlefield under your control, choose one —\n• Target creature gets +2/+0 until end of turn.\n• Target creature can\'t block this turn.').
card_mana_cost('retreat to valakut', ['2', 'R']).
card_cmc('retreat to valakut', 3).
card_layout('retreat to valakut', 'normal').

% Found in: HML, ME2
card_name('retribution', 'Retribution').
card_type('retribution', 'Sorcery').
card_types('retribution', ['Sorcery']).
card_subtypes('retribution', []).
card_colors('retribution', ['R']).
card_text('retribution', 'Choose two target creatures an opponent controls. That player chooses and sacrifices one of those creatures. Put a -1/-1 counter on the other.').
card_mana_cost('retribution', ['2', 'R', 'R']).
card_cmc('retribution', 4).
card_layout('retribution', 'normal').

% Found in: KTK
card_name('retribution of the ancients', 'Retribution of the Ancients').
card_type('retribution of the ancients', 'Enchantment').
card_types('retribution of the ancients', ['Enchantment']).
card_subtypes('retribution of the ancients', []).
card_colors('retribution of the ancients', ['B']).
card_text('retribution of the ancients', '{B}, Remove X +1/+1 counters from among creatures you control: Target creature gets -X/-X until end of turn.').
card_mana_cost('retribution of the ancients', ['B']).
card_cmc('retribution of the ancients', 1).
card_layout('retribution of the ancients', 'normal').

% Found in: VIS
card_name('retribution of the meek', 'Retribution of the Meek').
card_type('retribution of the meek', 'Sorcery').
card_types('retribution of the meek', ['Sorcery']).
card_subtypes('retribution of the meek', []).
card_colors('retribution of the meek', ['W']).
card_text('retribution of the meek', 'Destroy all creatures with power 4 or greater. They can\'t be regenerated.').
card_mana_cost('retribution of the meek', ['2', 'W']).
card_cmc('retribution of the meek', 3).
card_layout('retribution of the meek', 'normal').
card_reserved('retribution of the meek').

% Found in: USG
card_name('retromancer', 'Retromancer').
card_type('retromancer', 'Creature — Viashino Shaman').
card_types('retromancer', ['Creature']).
card_subtypes('retromancer', ['Viashino', 'Shaman']).
card_colors('retromancer', ['R']).
card_text('retromancer', 'Whenever Retromancer becomes the target of a spell or ability, Retromancer deals 3 damage to that spell or ability\'s controller.').
card_mana_cost('retromancer', ['2', 'R', 'R']).
card_cmc('retromancer', 4).
card_layout('retromancer', 'normal').
card_power('retromancer', 3).
card_toughness('retromancer', 3).

% Found in: PO2
card_name('return of the nightstalkers', 'Return of the Nightstalkers').
card_type('return of the nightstalkers', 'Sorcery').
card_types('return of the nightstalkers', ['Sorcery']).
card_subtypes('return of the nightstalkers', []).
card_colors('return of the nightstalkers', ['B']).
card_text('return of the nightstalkers', 'Return all Nightstalker permanent cards from your graveyard to the battlefield. Then destroy all Swamps you control.').
card_mana_cost('return of the nightstalkers', ['5', 'B', 'B']).
card_cmc('return of the nightstalkers', 7).
card_layout('return of the nightstalkers', 'normal').

% Found in: PTK
card_name('return to battle', 'Return to Battle').
card_type('return to battle', 'Sorcery').
card_types('return to battle', ['Sorcery']).
card_subtypes('return to battle', []).
card_colors('return to battle', ['B']).
card_text('return to battle', 'Return target creature card from your graveyard to your hand.').
card_mana_cost('return to battle', ['B']).
card_cmc('return to battle', 1).
card_layout('return to battle', 'normal').

% Found in: C14, CMD, TSP
card_name('return to dust', 'Return to Dust').
card_type('return to dust', 'Instant').
card_types('return to dust', ['Instant']).
card_subtypes('return to dust', []).
card_colors('return to dust', ['W']).
card_text('return to dust', 'Exile target artifact or enchantment. If you cast this spell during your main phase, you may exile up to one other target artifact or enchantment.').
card_mana_cost('return to dust', ['2', 'W', 'W']).
card_cmc('return to dust', 4).
card_layout('return to dust', 'normal').

% Found in: FRF
card_name('return to the earth', 'Return to the Earth').
card_type('return to the earth', 'Instant').
card_types('return to the earth', ['Instant']).
card_subtypes('return to the earth', []).
card_colors('return to the earth', ['G']).
card_text('return to the earth', 'Destroy target artifact, enchantment, or creature with flying.').
card_mana_cost('return to the earth', ['3', 'G']).
card_cmc('return to the earth', 4).
card_layout('return to the earth', 'normal').

% Found in: M15
card_name('return to the ranks', 'Return to the Ranks').
card_type('return to the ranks', 'Sorcery').
card_types('return to the ranks', ['Sorcery']).
card_subtypes('return to the ranks', []).
card_colors('return to the ranks', ['W']).
card_text('return to the ranks', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nReturn X target creature cards with converted mana cost 2 or less from your graveyard to the battlefield.').
card_mana_cost('return to the ranks', ['X', 'W', 'W']).
card_cmc('return to the ranks', 2).
card_layout('return to the ranks', 'normal').

% Found in: ORI, THS
card_name('returned centaur', 'Returned Centaur').
card_type('returned centaur', 'Creature — Zombie Centaur').
card_types('returned centaur', ['Creature']).
card_subtypes('returned centaur', ['Zombie', 'Centaur']).
card_colors('returned centaur', ['B']).
card_text('returned centaur', 'When Returned Centaur enters the battlefield, target player puts the top four cards of his or her library into his or her graveyard.').
card_mana_cost('returned centaur', ['3', 'B']).
card_cmc('returned centaur', 4).
card_layout('returned centaur', 'normal').
card_power('returned centaur', 2).
card_toughness('returned centaur', 4).

% Found in: THS
card_name('returned phalanx', 'Returned Phalanx').
card_type('returned phalanx', 'Creature — Zombie Soldier').
card_types('returned phalanx', ['Creature']).
card_subtypes('returned phalanx', ['Zombie', 'Soldier']).
card_colors('returned phalanx', ['B']).
card_text('returned phalanx', 'Defender\n{1}{U}: Returned Phalanx can attack this turn as though it didn\'t have defender.').
card_mana_cost('returned phalanx', ['1', 'B']).
card_cmc('returned phalanx', 2).
card_layout('returned phalanx', 'normal').
card_power('returned phalanx', 3).
card_toughness('returned phalanx', 3).

% Found in: JOU
card_name('returned reveler', 'Returned Reveler').
card_type('returned reveler', 'Creature — Zombie Satyr').
card_types('returned reveler', ['Creature']).
card_subtypes('returned reveler', ['Zombie', 'Satyr']).
card_colors('returned reveler', ['B']).
card_text('returned reveler', 'When Returned Reveler dies, each player puts the top three cards of his or her library into his or her graveyard.').
card_mana_cost('returned reveler', ['1', 'B']).
card_cmc('returned reveler', 2).
card_layout('returned reveler', 'normal').
card_power('returned reveler', 1).
card_toughness('returned reveler', 3).

% Found in: DTK
card_name('revealing wind', 'Revealing Wind').
card_type('revealing wind', 'Instant').
card_types('revealing wind', ['Instant']).
card_subtypes('revealing wind', []).
card_colors('revealing wind', ['G']).
card_text('revealing wind', 'Prevent all combat damage that would be dealt this turn. You may look at each face-down creature that\'s attacking or blocking.').
card_mana_cost('revealing wind', ['2', 'G']).
card_cmc('revealing wind', 3).
card_layout('revealing wind', 'normal').

% Found in: MMA, MOR
card_name('reveillark', 'Reveillark').
card_type('reveillark', 'Creature — Elemental').
card_types('reveillark', ['Creature']).
card_subtypes('reveillark', ['Elemental']).
card_colors('reveillark', ['W']).
card_text('reveillark', 'Flying\nWhen Reveillark leaves the battlefield, return up to two target creature cards with power 2 or less from your graveyard to the battlefield.\nEvoke {5}{W} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('reveillark', ['4', 'W']).
card_cmc('reveillark', 5).
card_layout('reveillark', 'normal').
card_power('reveillark', 4).
card_toughness('reveillark', 3).

% Found in: PCY
card_name('reveille squad', 'Reveille Squad').
card_type('reveille squad', 'Creature — Human Rebel').
card_types('reveille squad', ['Creature']).
card_subtypes('reveille squad', ['Human', 'Rebel']).
card_colors('reveille squad', ['W']).
card_text('reveille squad', 'Whenever one or more creatures attack you, if Reveille Squad is untapped, you may untap all creatures you control.').
card_mana_cost('reveille squad', ['2', 'W', 'W']).
card_cmc('reveille squad', 4).
card_layout('reveille squad', 'normal').
card_power('reveille squad', 3).
card_toughness('reveille squad', 3).

% Found in: HML, ME3
card_name('reveka, wizard savant', 'Reveka, Wizard Savant').
card_type('reveka, wizard savant', 'Legendary Creature — Dwarf Wizard').
card_types('reveka, wizard savant', ['Creature']).
card_subtypes('reveka, wizard savant', ['Dwarf', 'Wizard']).
card_supertypes('reveka, wizard savant', ['Legendary']).
card_colors('reveka, wizard savant', ['U']).
card_text('reveka, wizard savant', '{T}: Reveka, Wizard Savant deals 2 damage to target creature or player and doesn\'t untap during your next untap step.').
card_mana_cost('reveka, wizard savant', ['2', 'U', 'U']).
card_cmc('reveka, wizard savant', 4).
card_layout('reveka, wizard savant', 'normal').
card_power('reveka, wizard savant', 0).
card_toughness('reveka, wizard savant', 1).
card_reserved('reveka, wizard savant').

% Found in: JOU
card_name('revel of the fallen god', 'Revel of the Fallen God').
card_type('revel of the fallen god', 'Sorcery').
card_types('revel of the fallen god', ['Sorcery']).
card_subtypes('revel of the fallen god', []).
card_colors('revel of the fallen god', ['R', 'G']).
card_text('revel of the fallen god', 'Put four 2/2 red and green Satyr creature tokens with haste onto the battlefield.').
card_mana_cost('revel of the fallen god', ['3', 'R', 'R', 'G', 'G']).
card_cmc('revel of the fallen god', 7).
card_layout('revel of the fallen god', 'normal').

% Found in: CHR, LEG
card_name('revelation', 'Revelation').
card_type('revelation', 'World Enchantment').
card_types('revelation', ['Enchantment']).
card_subtypes('revelation', []).
card_supertypes('revelation', ['World']).
card_colors('revelation', ['G']).
card_text('revelation', 'Players play with their hands revealed.').
card_mana_cost('revelation', ['G']).
card_cmc('revelation', 1).
card_layout('revelation', 'normal').

% Found in: SHM
card_name('revelsong horn', 'Revelsong Horn').
card_type('revelsong horn', 'Artifact').
card_types('revelsong horn', ['Artifact']).
card_subtypes('revelsong horn', []).
card_colors('revelsong horn', []).
card_text('revelsong horn', '{1}, {T}, Tap an untapped creature you control: Target creature gets +1/+1 until end of turn.').
card_mana_cost('revelsong horn', ['2']).
card_cmc('revelsong horn', 2).
card_layout('revelsong horn', 'normal').

% Found in: 7ED, ORI, STH, TPR, pPRE
card_name('revenant', 'Revenant').
card_type('revenant', 'Creature — Spirit').
card_types('revenant', ['Creature']).
card_subtypes('revenant', ['Spirit']).
card_colors('revenant', ['B']).
card_text('revenant', 'Flying\nRevenant\'s power and toughness are each equal to the number of creature cards in your graveyard.').
card_mana_cost('revenant', ['4', 'B']).
card_cmc('revenant', 5).
card_layout('revenant', 'normal').
card_power('revenant', '*').
card_toughness('revenant', '*').

% Found in: DDK, GPT
card_name('revenant patriarch', 'Revenant Patriarch').
card_type('revenant patriarch', 'Creature — Spirit').
card_types('revenant patriarch', ['Creature']).
card_subtypes('revenant patriarch', ['Spirit']).
card_colors('revenant patriarch', ['B']).
card_text('revenant patriarch', 'When Revenant Patriarch enters the battlefield, if {W} was spent to cast it, target player skips his or her next combat phase.\nRevenant Patriarch can\'t block.').
card_mana_cost('revenant patriarch', ['4', 'B']).
card_cmc('revenant patriarch', 5).
card_layout('revenant patriarch', 'normal').
card_power('revenant patriarch', 4).
card_toughness('revenant patriarch', 3).

% Found in: AVR
card_name('revenge of the hunted', 'Revenge of the Hunted').
card_type('revenge of the hunted', 'Sorcery').
card_types('revenge of the hunted', ['Sorcery']).
card_subtypes('revenge of the hunted', []).
card_colors('revenge of the hunted', ['G']).
card_text('revenge of the hunted', 'Until end of turn, target creature gets +6/+6 and gains trample, and all creatures able to block it this turn do so.\nMiracle {G} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_mana_cost('revenge of the hunted', ['4', 'G', 'G']).
card_cmc('revenge of the hunted', 6).
card_layout('revenge of the hunted', 'normal').

% Found in: M11, M12, M13, PD2
card_name('reverberate', 'Reverberate').
card_type('reverberate', 'Instant').
card_types('reverberate', ['Instant']).
card_subtypes('reverberate', []).
card_colors('reverberate', ['R']).
card_text('reverberate', 'Copy target instant or sorcery spell. You may choose new targets for the copy.').
card_mana_cost('reverberate', ['R', 'R']).
card_cmc('reverberate', 2).
card_layout('reverberate', 'normal').

% Found in: LEG
card_name('reverberation', 'Reverberation').
card_type('reverberation', 'Instant').
card_types('reverberation', ['Instant']).
card_subtypes('reverberation', []).
card_colors('reverberation', ['U']).
card_text('reverberation', 'All damage that would be dealt this turn by target sorcery spell is dealt to that spell\'s controller instead.').
card_mana_cost('reverberation', ['2', 'U', 'U']).
card_cmc('reverberation', 4).
card_layout('reverberation', 'normal').
card_reserved('reverberation').

% Found in: PLC
card_name('revered dead', 'Revered Dead').
card_type('revered dead', 'Creature — Spirit Soldier').
card_types('revered dead', ['Creature']).
card_subtypes('revered dead', ['Spirit', 'Soldier']).
card_colors('revered dead', ['W']).
card_text('revered dead', '{W}: Regenerate Revered Dead.').
card_mana_cost('revered dead', ['1', 'W']).
card_cmc('revered dead', 2).
card_layout('revered dead', 'normal').
card_power('revered dead', 1).
card_toughness('revered dead', 1).

% Found in: MMQ
card_name('revered elder', 'Revered Elder').
card_type('revered elder', 'Creature — Human Cleric').
card_types('revered elder', ['Creature']).
card_subtypes('revered elder', ['Human', 'Cleric']).
card_colors('revered elder', ['W']).
card_text('revered elder', '{1}: Prevent the next 1 damage that would be dealt to Revered Elder this turn.').
card_mana_cost('revered elder', ['2', 'W']).
card_cmc('revered elder', 3).
card_layout('revered elder', 'normal').
card_power('revered elder', 1).
card_toughness('revered elder', 2).

% Found in: WTH
card_name('revered unicorn', 'Revered Unicorn').
card_type('revered unicorn', 'Creature — Unicorn').
card_types('revered unicorn', ['Creature']).
card_subtypes('revered unicorn', ['Unicorn']).
card_colors('revered unicorn', ['W']).
card_text('revered unicorn', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhen Revered Unicorn leaves the battlefield, you gain life equal to the number of age counters on it.').
card_mana_cost('revered unicorn', ['1', 'W']).
card_cmc('revered unicorn', 2).
card_layout('revered unicorn', 'normal').
card_power('revered unicorn', 2).
card_toughness('revered unicorn', 3).

% Found in: SOK
card_name('reverence', 'Reverence').
card_type('reverence', 'Enchantment').
card_types('reverence', ['Enchantment']).
card_subtypes('reverence', []).
card_colors('reverence', ['W']).
card_text('reverence', 'Creatures with power 2 or less can\'t attack you.').
card_mana_cost('reverence', ['2', 'W', 'W']).
card_cmc('reverence', 4).
card_layout('reverence', 'normal').

% Found in: THS
card_name('reverent hunter', 'Reverent Hunter').
card_type('reverent hunter', 'Creature — Human Archer').
card_types('reverent hunter', ['Creature']).
card_subtypes('reverent hunter', ['Human', 'Archer']).
card_colors('reverent hunter', ['G']).
card_text('reverent hunter', 'When Reverent Hunter enters the battlefield, put a number of +1/+1 counters on it equal to your devotion to green. (Each {G} in the mana costs of permanents you control counts toward your devotion to green.)').
card_mana_cost('reverent hunter', ['2', 'G']).
card_cmc('reverent hunter', 3).
card_layout('reverent hunter', 'normal').
card_power('reverent hunter', 1).
card_toughness('reverent hunter', 1).

% Found in: MMQ
card_name('reverent mantra', 'Reverent Mantra').
card_type('reverent mantra', 'Instant').
card_types('reverent mantra', ['Instant']).
card_subtypes('reverent mantra', []).
card_colors('reverent mantra', ['W']).
card_text('reverent mantra', 'You may exile a white card from your hand rather than pay Reverent Mantra\'s mana cost.\nChoose a color. All creatures gain protection from the chosen color until end of turn.').
card_mana_cost('reverent mantra', ['3', 'W']).
card_cmc('reverent mantra', 4).
card_layout('reverent mantra', 'normal').

% Found in: NMS
card_name('reverent silence', 'Reverent Silence').
card_type('reverent silence', 'Sorcery').
card_types('reverent silence', ['Sorcery']).
card_subtypes('reverent silence', []).
card_colors('reverent silence', ['G']).
card_text('reverent silence', 'If you control a Forest, rather than pay Reverent Silence\'s mana cost, you may have each other player gain 6 life.\nDestroy all enchantments.').
card_mana_cost('reverent silence', ['3', 'G']).
card_cmc('reverent silence', 4).
card_layout('reverent silence', 'normal').

% Found in: 5DN
card_name('reversal of fortune', 'Reversal of Fortune').
card_type('reversal of fortune', 'Sorcery').
card_types('reversal of fortune', ['Sorcery']).
card_subtypes('reversal of fortune', []).
card_colors('reversal of fortune', ['R']).
card_text('reversal of fortune', 'Target opponent reveals his or her hand. You may copy an instant or sorcery card in it. If you do, you may cast the copy without paying its mana cost.').
card_mana_cost('reversal of fortune', ['4', 'R', 'R']).
card_cmc('reversal of fortune', 6).
card_layout('reversal of fortune', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 9ED, CED, CEI, ITP, LEA, LEB
card_name('reverse damage', 'Reverse Damage').
card_type('reverse damage', 'Instant').
card_types('reverse damage', ['Instant']).
card_subtypes('reverse damage', []).
card_colors('reverse damage', ['W']).
card_text('reverse damage', 'The next time a source of your choice would deal damage to you this turn, prevent that damage. You gain life equal to the damage prevented this way.').
card_mana_cost('reverse damage', ['1', 'W', 'W']).
card_cmc('reverse damage', 3).
card_layout('reverse damage', 'normal').

% Found in: 3ED, ATQ
card_name('reverse polarity', 'Reverse Polarity').
card_type('reverse polarity', 'Instant').
card_types('reverse polarity', ['Instant']).
card_subtypes('reverse polarity', []).
card_colors('reverse polarity', ['W']).
card_text('reverse polarity', 'You gain X life, where X is twice the damage dealt to you so far this turn by artifacts.').
card_mana_cost('reverse polarity', ['W', 'W']).
card_cmc('reverse polarity', 2).
card_layout('reverse polarity', 'normal').

% Found in: CHK
card_name('reverse the sands', 'Reverse the Sands').
card_type('reverse the sands', 'Sorcery').
card_types('reverse the sands', ['Sorcery']).
card_subtypes('reverse the sands', []).
card_colors('reverse the sands', ['W']).
card_text('reverse the sands', 'Redistribute any number of players\' life totals. (Each of those players gets one life total back.)').
card_mana_cost('reverse the sands', ['6', 'W', 'W']).
card_cmc('reverse the sands', 8).
card_layout('reverse the sands', 'normal').

% Found in: 8ED, M13, MMQ
card_name('revive', 'Revive').
card_type('revive', 'Sorcery').
card_types('revive', ['Sorcery']).
card_subtypes('revive', []).
card_colors('revive', ['G']).
card_text('revive', 'Return target green card from your graveyard to your hand.').
card_mana_cost('revive', ['1', 'G']).
card_cmc('revive', 2).
card_layout('revive', 'normal').

% Found in: MOR
card_name('revive the fallen', 'Revive the Fallen').
card_type('revive the fallen', 'Sorcery').
card_types('revive the fallen', ['Sorcery']).
card_subtypes('revive the fallen', []).
card_colors('revive the fallen', ['B']).
card_text('revive the fallen', 'Return target creature card from a graveyard to its owner\'s hand. Clash with an opponent. If you win, return Revive the Fallen to its owner\'s hand. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('revive the fallen', ['1', 'B']).
card_cmc('revive the fallen', 2).
card_layout('revive the fallen', 'normal').

% Found in: 10E, INV
card_name('reviving dose', 'Reviving Dose').
card_type('reviving dose', 'Instant').
card_types('reviving dose', ['Instant']).
card_subtypes('reviving dose', []).
card_colors('reviving dose', ['W']).
card_text('reviving dose', 'You gain 3 life.\nDraw a card.').
card_mana_cost('reviving dose', ['2', 'W']).
card_cmc('reviving dose', 3).
card_layout('reviving dose', 'normal').

% Found in: JOU
card_name('reviving melody', 'Reviving Melody').
card_type('reviving melody', 'Sorcery').
card_types('reviving melody', ['Sorcery']).
card_subtypes('reviving melody', []).
card_colors('reviving melody', ['G']).
card_text('reviving melody', 'Choose one or both —\n• Return target creature card from your graveyard to your hand.\n• Return target enchantment card from your graveyard to your hand.').
card_mana_cost('reviving melody', ['2', 'G']).
card_cmc('reviving melody', 3).
card_layout('reviving melody', 'normal').

% Found in: INV, VMA
card_name('reviving vapors', 'Reviving Vapors').
card_type('reviving vapors', 'Instant').
card_types('reviving vapors', ['Instant']).
card_subtypes('reviving vapors', []).
card_colors('reviving vapors', ['W', 'U']).
card_text('reviving vapors', 'Reveal the top three cards of your library and put one of them into your hand. You gain life equal to that card\'s converted mana cost. Put all other cards revealed this way into your graveyard.').
card_mana_cost('reviving vapors', ['2', 'W', 'U']).
card_cmc('reviving vapors', 4).
card_layout('reviving vapors', 'normal').

% Found in: BNG, DDI, SOM
card_name('revoke existence', 'Revoke Existence').
card_type('revoke existence', 'Sorcery').
card_types('revoke existence', ['Sorcery']).
card_subtypes('revoke existence', []).
card_colors('revoke existence', ['W']).
card_text('revoke existence', 'Exile target artifact or enchantment.').
card_mana_cost('revoke existence', ['1', 'W']).
card_cmc('revoke existence', 2).
card_layout('revoke existence', 'normal').

% Found in: SCG
card_name('reward the faithful', 'Reward the Faithful').
card_type('reward the faithful', 'Instant').
card_types('reward the faithful', ['Instant']).
card_subtypes('reward the faithful', []).
card_colors('reward the faithful', ['W']).
card_text('reward the faithful', 'Any number of target players each gain life equal to the highest converted mana cost among permanents you control.').
card_mana_cost('reward the faithful', ['W']).
card_cmc('reward the faithful', 1).
card_layout('reward the faithful', 'normal').

% Found in: INV
card_name('rewards of diversity', 'Rewards of Diversity').
card_type('rewards of diversity', 'Enchantment').
card_types('rewards of diversity', ['Enchantment']).
card_subtypes('rewards of diversity', []).
card_colors('rewards of diversity', ['W']).
card_text('rewards of diversity', 'Whenever an opponent casts a multicolored spell, you gain 4 life.').
card_mana_cost('rewards of diversity', ['2', 'W']).
card_cmc('rewards of diversity', 3).
card_layout('rewards of diversity', 'normal').

% Found in: CHK
card_name('reweave', 'Reweave').
card_type('reweave', 'Instant — Arcane').
card_types('reweave', ['Instant']).
card_subtypes('reweave', ['Arcane']).
card_colors('reweave', ['U']).
card_text('reweave', 'Target permanent\'s controller sacrifices it. If he or she does, that player reveals cards from the top of his or her library until he or she reveals a permanent card that shares a card type with the sacrificed permanent, puts that card onto the battlefield, then shuffles his or her library.\nSplice onto Arcane {2}{U}{U} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('reweave', ['5', 'U']).
card_cmc('reweave', 6).
card_layout('reweave', 'normal').

% Found in: 8ED, 9ED, M13, USG, pARL
card_name('rewind', 'Rewind').
card_type('rewind', 'Instant').
card_types('rewind', ['Instant']).
card_subtypes('rewind', []).
card_colors('rewind', ['U']).
card_text('rewind', 'Counter target spell. Untap up to four lands.').
card_mana_cost('rewind', ['2', 'U', 'U']).
card_cmc('rewind', 4).
card_layout('rewind', 'normal').

% Found in: 10E, CNS, DD3_DVD, DDC, INV, pMGD
card_name('reya dawnbringer', 'Reya Dawnbringer').
card_type('reya dawnbringer', 'Legendary Creature — Angel').
card_types('reya dawnbringer', ['Creature']).
card_subtypes('reya dawnbringer', ['Angel']).
card_supertypes('reya dawnbringer', ['Legendary']).
card_colors('reya dawnbringer', ['W']).
card_text('reya dawnbringer', 'Flying\nAt the beginning of your upkeep, you may return target creature card from your graveyard to the battlefield.').
card_mana_cost('reya dawnbringer', ['6', 'W', 'W', 'W']).
card_cmc('reya dawnbringer', 9).
card_layout('reya dawnbringer', 'normal').
card_power('reya dawnbringer', 4).
card_toughness('reya dawnbringer', 6).

% Found in: 10E, 8ED, NMS, S00
card_name('rhox', 'Rhox').
card_type('rhox', 'Creature — Rhino Beast').
card_types('rhox', ['Creature']).
card_subtypes('rhox', ['Rhino', 'Beast']).
card_colors('rhox', ['G']).
card_text('rhox', 'You may have Rhox assign its combat damage as though it weren\'t blocked.\n{2}{G}: Regenerate Rhox. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_mana_cost('rhox', ['4', 'G', 'G']).
card_cmc('rhox', 6).
card_layout('rhox', 'normal').
card_power('rhox', 5).
card_toughness('rhox', 5).

% Found in: CON
card_name('rhox bodyguard', 'Rhox Bodyguard').
card_type('rhox bodyguard', 'Creature — Rhino Monk Soldier').
card_types('rhox bodyguard', ['Creature']).
card_subtypes('rhox bodyguard', ['Rhino', 'Monk', 'Soldier']).
card_colors('rhox bodyguard', ['W', 'G']).
card_text('rhox bodyguard', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhen Rhox Bodyguard enters the battlefield, you gain 3 life.').
card_mana_cost('rhox bodyguard', ['3', 'G', 'W']).
card_cmc('rhox bodyguard', 5).
card_layout('rhox bodyguard', 'normal').
card_power('rhox bodyguard', 2).
card_toughness('rhox bodyguard', 3).

% Found in: ARB
card_name('rhox brute', 'Rhox Brute').
card_type('rhox brute', 'Creature — Rhino Warrior').
card_types('rhox brute', ['Creature']).
card_subtypes('rhox brute', ['Rhino', 'Warrior']).
card_colors('rhox brute', ['R', 'G']).
card_text('rhox brute', '').
card_mana_cost('rhox brute', ['2', 'R', 'G']).
card_cmc('rhox brute', 4).
card_layout('rhox brute', 'normal').
card_power('rhox brute', 4).
card_toughness('rhox brute', 4).

% Found in: ALA
card_name('rhox charger', 'Rhox Charger').
card_type('rhox charger', 'Creature — Rhino Soldier').
card_types('rhox charger', ['Creature']).
card_subtypes('rhox charger', ['Rhino', 'Soldier']).
card_colors('rhox charger', ['G']).
card_text('rhox charger', 'Trample\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_mana_cost('rhox charger', ['3', 'G']).
card_cmc('rhox charger', 4).
card_layout('rhox charger', 'normal').
card_power('rhox charger', 3).
card_toughness('rhox charger', 3).

% Found in: M13
card_name('rhox faithmender', 'Rhox Faithmender').
card_type('rhox faithmender', 'Creature — Rhino Monk').
card_types('rhox faithmender', ['Creature']).
card_subtypes('rhox faithmender', ['Rhino', 'Monk']).
card_colors('rhox faithmender', ['W']).
card_text('rhox faithmender', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)\nIf you would gain life, you gain twice that much life instead.').
card_mana_cost('rhox faithmender', ['3', 'W']).
card_cmc('rhox faithmender', 4).
card_layout('rhox faithmender', 'normal').
card_power('rhox faithmender', 1).
card_toughness('rhox faithmender', 5).

% Found in: ORI
card_name('rhox maulers', 'Rhox Maulers').
card_type('rhox maulers', 'Creature — Rhino Soldier').
card_types('rhox maulers', ['Creature']).
card_subtypes('rhox maulers', ['Rhino', 'Soldier']).
card_colors('rhox maulers', ['G']).
card_text('rhox maulers', 'Trample (This creature can deal excess combat damage to defending player or planeswalker while attacking.)\nRenown 2 (When this creature deals combat damage to a player, if it isn\'t renowned, put two +1/+1 counters on it and it becomes renowned.)').
card_mana_cost('rhox maulers', ['4', 'G']).
card_cmc('rhox maulers', 5).
card_layout('rhox maulers', 'normal').
card_power('rhox maulers', 4).
card_toughness('rhox maulers', 4).

% Found in: CON
card_name('rhox meditant', 'Rhox Meditant').
card_type('rhox meditant', 'Creature — Rhino Monk').
card_types('rhox meditant', ['Creature']).
card_subtypes('rhox meditant', ['Rhino', 'Monk']).
card_colors('rhox meditant', ['W']).
card_text('rhox meditant', 'When Rhox Meditant enters the battlefield, if you control a green permanent, draw a card.').
card_mana_cost('rhox meditant', ['3', 'W']).
card_cmc('rhox meditant', 4).
card_layout('rhox meditant', 'normal').
card_power('rhox meditant', 2).
card_toughness('rhox meditant', 4).

% Found in: M10
card_name('rhox pikemaster', 'Rhox Pikemaster').
card_type('rhox pikemaster', 'Creature — Rhino Soldier').
card_types('rhox pikemaster', ['Creature']).
card_subtypes('rhox pikemaster', ['Rhino', 'Soldier']).
card_colors('rhox pikemaster', ['W']).
card_text('rhox pikemaster', 'First strike (This creature deals combat damage before creatures without first strike.)\nOther Soldier creatures you control have first strike.').
card_mana_cost('rhox pikemaster', ['2', 'W', 'W']).
card_cmc('rhox pikemaster', 4).
card_layout('rhox pikemaster', 'normal').
card_power('rhox pikemaster', 3).
card_toughness('rhox pikemaster', 3).

% Found in: ALA, pFNM
card_name('rhox war monk', 'Rhox War Monk').
card_type('rhox war monk', 'Creature — Rhino Monk').
card_types('rhox war monk', ['Creature']).
card_subtypes('rhox war monk', ['Rhino', 'Monk']).
card_colors('rhox war monk', ['W', 'U', 'G']).
card_text('rhox war monk', 'Lifelink').
card_mana_cost('rhox war monk', ['G', 'W', 'U']).
card_cmc('rhox war monk', 3).
card_layout('rhox war monk', 'normal').
card_power('rhox war monk', 3).
card_toughness('rhox war monk', 4).

% Found in: MOR
card_name('rhys the exiled', 'Rhys the Exiled').
card_type('rhys the exiled', 'Legendary Creature — Elf Warrior').
card_types('rhys the exiled', ['Creature']).
card_subtypes('rhys the exiled', ['Elf', 'Warrior']).
card_supertypes('rhys the exiled', ['Legendary']).
card_colors('rhys the exiled', ['G']).
card_text('rhys the exiled', 'Whenever Rhys the Exiled attacks, you gain 1 life for each Elf you control.\n{B}, Sacrifice an Elf: Regenerate Rhys the Exiled.').
card_mana_cost('rhys the exiled', ['2', 'G']).
card_cmc('rhys the exiled', 3).
card_layout('rhys the exiled', 'normal').
card_power('rhys the exiled', 3).
card_toughness('rhys the exiled', 2).

% Found in: SHM
card_name('rhys the redeemed', 'Rhys the Redeemed').
card_type('rhys the redeemed', 'Legendary Creature — Elf Warrior').
card_types('rhys the redeemed', ['Creature']).
card_subtypes('rhys the redeemed', ['Elf', 'Warrior']).
card_supertypes('rhys the redeemed', ['Legendary']).
card_colors('rhys the redeemed', ['W', 'G']).
card_text('rhys the redeemed', '{2}{G/W}, {T}: Put a 1/1 green and white Elf Warrior creature token onto the battlefield.\n{4}{G/W}{G/W}, {T}: For each creature token you control, put a token that\'s a copy of that creature onto the battlefield.').
card_mana_cost('rhys the redeemed', ['G/W']).
card_cmc('rhys the redeemed', 1).
card_layout('rhys the redeemed', 'normal').
card_power('rhys the redeemed', 1).
card_toughness('rhys the redeemed', 1).

% Found in: PCY
card_name('rhystic cave', 'Rhystic Cave').
card_type('rhystic cave', 'Land').
card_types('rhystic cave', ['Land']).
card_subtypes('rhystic cave', []).
card_colors('rhystic cave', []).
card_text('rhystic cave', '{T}: Choose a color. Add one mana of that color to your mana pool unless any player pays {1}. Activate this ability only any time you could cast an instant.').
card_layout('rhystic cave', 'normal').

% Found in: PCY
card_name('rhystic circle', 'Rhystic Circle').
card_type('rhystic circle', 'Enchantment').
card_types('rhystic circle', ['Enchantment']).
card_subtypes('rhystic circle', []).
card_colors('rhystic circle', ['W']).
card_text('rhystic circle', '{1}: Any player may pay {1}. If no one does, the next time a source of your choice would deal damage to you this turn, prevent that damage.').
card_mana_cost('rhystic circle', ['2', 'W', 'W']).
card_cmc('rhystic circle', 4).
card_layout('rhystic circle', 'normal').

% Found in: PCY
card_name('rhystic deluge', 'Rhystic Deluge').
card_type('rhystic deluge', 'Enchantment').
card_types('rhystic deluge', ['Enchantment']).
card_subtypes('rhystic deluge', []).
card_colors('rhystic deluge', ['U']).
card_text('rhystic deluge', '{U}: Tap target creature unless its controller pays {1}.').
card_mana_cost('rhystic deluge', ['2', 'U']).
card_cmc('rhystic deluge', 3).
card_layout('rhystic deluge', 'normal').

% Found in: PCY
card_name('rhystic lightning', 'Rhystic Lightning').
card_type('rhystic lightning', 'Instant').
card_types('rhystic lightning', ['Instant']).
card_subtypes('rhystic lightning', []).
card_colors('rhystic lightning', ['R']).
card_text('rhystic lightning', 'Rhystic Lightning deals 4 damage to target creature or player unless that creature\'s controller or that player pays {2}. If he or she does, Rhystic Lightning deals 2 damage to the creature or player.').
card_mana_cost('rhystic lightning', ['2', 'R']).
card_cmc('rhystic lightning', 3).
card_layout('rhystic lightning', 'normal').

% Found in: PCY
card_name('rhystic scrying', 'Rhystic Scrying').
card_type('rhystic scrying', 'Sorcery').
card_types('rhystic scrying', ['Sorcery']).
card_subtypes('rhystic scrying', []).
card_colors('rhystic scrying', ['U']).
card_text('rhystic scrying', 'Draw three cards. Then, if any player pays {2}, discard three cards.').
card_mana_cost('rhystic scrying', ['2', 'U', 'U']).
card_cmc('rhystic scrying', 4).
card_layout('rhystic scrying', 'normal').

% Found in: PCY
card_name('rhystic shield', 'Rhystic Shield').
card_type('rhystic shield', 'Instant').
card_types('rhystic shield', ['Instant']).
card_subtypes('rhystic shield', []).
card_colors('rhystic shield', ['W']).
card_text('rhystic shield', 'Creatures you control get +0/+1 until end of turn. They get an additional +0/+2 until end of turn unless any player pays {2}.').
card_mana_cost('rhystic shield', ['1', 'W']).
card_cmc('rhystic shield', 2).
card_layout('rhystic shield', 'normal').

% Found in: CM1, PCY
card_name('rhystic study', 'Rhystic Study').
card_type('rhystic study', 'Enchantment').
card_types('rhystic study', ['Enchantment']).
card_subtypes('rhystic study', []).
card_colors('rhystic study', ['U']).
card_text('rhystic study', 'Whenever an opponent casts a spell, you may draw a card unless that player pays {1}.').
card_mana_cost('rhystic study', ['2', 'U']).
card_cmc('rhystic study', 3).
card_layout('rhystic study', 'normal').

% Found in: PCY
card_name('rhystic syphon', 'Rhystic Syphon').
card_type('rhystic syphon', 'Sorcery').
card_types('rhystic syphon', ['Sorcery']).
card_subtypes('rhystic syphon', []).
card_colors('rhystic syphon', ['B']).
card_text('rhystic syphon', 'Unless target player pays {3}, he or she loses 5 life and you gain 5 life.').
card_mana_cost('rhystic syphon', ['3', 'B', 'B']).
card_cmc('rhystic syphon', 5).
card_layout('rhystic syphon', 'normal').

% Found in: PCY
card_name('rhystic tutor', 'Rhystic Tutor').
card_type('rhystic tutor', 'Sorcery').
card_types('rhystic tutor', ['Sorcery']).
card_subtypes('rhystic tutor', []).
card_colors('rhystic tutor', ['B']).
card_text('rhystic tutor', 'Unless any player pays {2}, search your library for a card, put that card into your hand, then shuffle your library.').
card_mana_cost('rhystic tutor', ['2', 'B']).
card_cmc('rhystic tutor', 3).
card_layout('rhystic tutor', 'normal').

% Found in: PCY
card_name('rib cage spider', 'Rib Cage Spider').
card_type('rib cage spider', 'Creature — Spider').
card_types('rib cage spider', ['Creature']).
card_subtypes('rib cage spider', ['Spider']).
card_colors('rib cage spider', ['G']).
card_text('rib cage spider', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('rib cage spider', ['2', 'G']).
card_cmc('rib cage spider', 3).
card_layout('rib cage spider', 'normal').
card_power('rib cage spider', 1).
card_toughness('rib cage spider', 4).

% Found in: PCY
card_name('ribbon snake', 'Ribbon Snake').
card_type('ribbon snake', 'Creature — Snake').
card_types('ribbon snake', ['Creature']).
card_subtypes('ribbon snake', ['Snake']).
card_colors('ribbon snake', ['U']).
card_text('ribbon snake', 'Flying\n{2}: Ribbon Snake loses flying until end of turn. Any player may activate this ability.').
card_mana_cost('ribbon snake', ['1', 'U', 'U']).
card_cmc('ribbon snake', 3).
card_layout('ribbon snake', 'normal').
card_power('ribbon snake', 2).
card_toughness('ribbon snake', 3).

% Found in: RAV
card_name('ribbons of night', 'Ribbons of Night').
card_type('ribbons of night', 'Sorcery').
card_types('ribbons of night', ['Sorcery']).
card_subtypes('ribbons of night', []).
card_colors('ribbons of night', ['B']).
card_text('ribbons of night', 'Ribbons of Night deals 4 damage to target creature and you gain 4 life. If {U} was spent to cast Ribbons of Night, draw a card.').
card_mana_cost('ribbons of night', ['4', 'B']).
card_cmc('ribbons of night', 5).
card_layout('ribbons of night', 'normal').

% Found in: BOK
card_name('ribbons of the reikai', 'Ribbons of the Reikai').
card_type('ribbons of the reikai', 'Sorcery — Arcane').
card_types('ribbons of the reikai', ['Sorcery']).
card_subtypes('ribbons of the reikai', ['Arcane']).
card_colors('ribbons of the reikai', ['U']).
card_text('ribbons of the reikai', 'Draw a card for each Spirit you control.').
card_mana_cost('ribbons of the reikai', ['4', 'U']).
card_cmc('ribbons of the reikai', 5).
card_layout('ribbons of the reikai', 'normal').

% Found in: UNH
card_name('richard garfield, ph.d.', 'Richard Garfield, Ph.D.').
card_type('richard garfield, ph.d.', 'Legendary Creature — Human Designer').
card_types('richard garfield, ph.d.', ['Creature']).
card_subtypes('richard garfield, ph.d.', ['Human', 'Designer']).
card_supertypes('richard garfield, ph.d.', ['Legendary']).
card_colors('richard garfield, ph.d.', ['U']).
card_text('richard garfield, ph.d.', 'You may play cards as though they were other Magic cards of your choice with the same mana cost. (Mana cost includes color.) You can\'t choose the same card twice.').
card_mana_cost('richard garfield, ph.d.', ['3', 'U', 'U']).
card_cmc('richard garfield, ph.d.', 5).
card_layout('richard garfield, ph.d.', 'normal').
card_power('richard garfield, ph.d.', 2).
card_toughness('richard garfield, ph.d.', 2).

% Found in: UGL
card_name('ricochet', 'Ricochet').
card_type('ricochet', 'Enchantment').
card_types('ricochet', ['Enchantment']).
card_subtypes('ricochet', []).
card_colors('ricochet', ['R']).
card_text('ricochet', 'Whenever any spell targets a single player, each player rolls a six-sided die. That spell is redirected to the player or players with the lowest die roll. If two or more players tie for the lowest die roll, they reroll until there is no tie.').
card_mana_cost('ricochet', ['R']).
card_cmc('ricochet', 1).
card_layout('ricochet', 'normal').

% Found in: WWK
card_name('ricochet trap', 'Ricochet Trap').
card_type('ricochet trap', 'Instant — Trap').
card_types('ricochet trap', ['Instant']).
card_subtypes('ricochet trap', ['Trap']).
card_colors('ricochet trap', ['R']).
card_text('ricochet trap', 'If an opponent cast a blue spell this turn, you may pay {R} rather than pay Ricochet Trap\'s mana cost.\nChange the target of target spell with a single target.').
card_mana_cost('ricochet trap', ['3', 'R']).
card_cmc('ricochet trap', 4).
card_layout('ricochet trap', 'normal').

% Found in: FUT, JOU
card_name('riddle of lightning', 'Riddle of Lightning').
card_type('riddle of lightning', 'Instant').
card_types('riddle of lightning', ['Instant']).
card_subtypes('riddle of lightning', []).
card_colors('riddle of lightning', ['R']).
card_text('riddle of lightning', 'Choose target creature or player. Scry 3, then reveal the top card of your library. Riddle of Lightning deals damage equal to that card\'s converted mana cost to that creature or player. (To scry 3, look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('riddle of lightning', ['3', 'R', 'R']).
card_cmc('riddle of lightning', 5).
card_layout('riddle of lightning', 'normal').

% Found in: CMD
card_name('riddlekeeper', 'Riddlekeeper').
card_type('riddlekeeper', 'Creature — Homunculus').
card_types('riddlekeeper', ['Creature']).
card_subtypes('riddlekeeper', ['Homunculus']).
card_colors('riddlekeeper', ['U']).
card_text('riddlekeeper', 'Whenever a creature attacks you or a planeswalker you control, that creature\'s controller puts the top two cards of his or her library into his or her graveyard.').
card_mana_cost('riddlekeeper', ['2', 'U']).
card_cmc('riddlekeeper', 3).
card_layout('riddlekeeper', 'normal').
card_power('riddlekeeper', 1).
card_toughness('riddlekeeper', 4).

% Found in: SOM
card_name('riddlesmith', 'Riddlesmith').
card_type('riddlesmith', 'Creature — Human Artificer').
card_types('riddlesmith', ['Creature']).
card_subtypes('riddlesmith', ['Human', 'Artificer']).
card_colors('riddlesmith', ['U']).
card_text('riddlesmith', 'Whenever you cast an artifact spell, you may draw a card. If you do, discard a card.').
card_mana_cost('riddlesmith', ['1', 'U']).
card_cmc('riddlesmith', 2).
card_layout('riddlesmith', 'normal').
card_power('riddlesmith', 2).
card_toughness('riddlesmith', 1).

% Found in: KTK
card_name('ride down', 'Ride Down').
card_type('ride down', 'Instant').
card_types('ride down', ['Instant']).
card_subtypes('ride down', []).
card_colors('ride down', ['W', 'R']).
card_text('ride down', 'Destroy target blocking creature. Creatures that were blocked by that creature this combat gain trample until end of turn.').
card_mana_cost('ride down', ['R', 'W']).
card_cmc('ride down', 2).
card_layout('ride down', 'normal').

% Found in: AVR
card_name('riders of gavony', 'Riders of Gavony').
card_type('riders of gavony', 'Creature — Human Knight').
card_types('riders of gavony', ['Creature']).
card_subtypes('riders of gavony', ['Human', 'Knight']).
card_colors('riders of gavony', ['W']).
card_text('riders of gavony', 'Vigilance\nAs Riders of Gavony enters the battlefield, choose a creature type.\nHuman creatures you control have protection from creatures of the chosen type.').
card_mana_cost('riders of gavony', ['2', 'W', 'W']).
card_cmc('riders of gavony', 4).
card_layout('riders of gavony', 'normal').
card_power('riders of gavony', 3).
card_toughness('riders of gavony', 3).

% Found in: ALA
card_name('ridge rannet', 'Ridge Rannet').
card_type('ridge rannet', 'Creature — Beast').
card_types('ridge rannet', ['Creature']).
card_subtypes('ridge rannet', ['Beast']).
card_colors('ridge rannet', ['R']).
card_text('ridge rannet', 'Cycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('ridge rannet', ['5', 'R', 'R']).
card_cmc('ridge rannet', 7).
card_layout('ridge rannet', 'normal').
card_power('ridge rannet', 6).
card_toughness('ridge rannet', 4).

% Found in: PLC
card_name('ridged kusite', 'Ridged Kusite').
card_type('ridged kusite', 'Creature — Horror Spellshaper').
card_types('ridged kusite', ['Creature']).
card_subtypes('ridged kusite', ['Horror', 'Spellshaper']).
card_colors('ridged kusite', ['B']).
card_text('ridged kusite', '{1}{B}, {T}, Discard a card: Target creature gets +1/+0 and gains first strike until end of turn.').
card_mana_cost('ridged kusite', ['B']).
card_cmc('ridged kusite', 1).
card_layout('ridged kusite', 'normal').
card_power('ridged kusite', 1).
card_toughness('ridged kusite', 1).

% Found in: 8ED, PCY
card_name('ridgeline rager', 'Ridgeline Rager').
card_type('ridgeline rager', 'Creature — Beast').
card_types('ridgeline rager', ['Creature']).
card_subtypes('ridgeline rager', ['Beast']).
card_colors('ridgeline rager', ['R']).
card_text('ridgeline rager', '{R}: Ridgeline Rager gets +1/+0 until end of turn.').
card_mana_cost('ridgeline rager', ['2', 'R']).
card_cmc('ridgeline rager', 3).
card_layout('ridgeline rager', 'normal').
card_power('ridgeline rager', 1).
card_toughness('ridgeline rager', 2).

% Found in: LGN
card_name('ridgetop raptor', 'Ridgetop Raptor').
card_type('ridgetop raptor', 'Creature — Lizard Beast').
card_types('ridgetop raptor', ['Creature']).
card_subtypes('ridgetop raptor', ['Lizard', 'Beast']).
card_colors('ridgetop raptor', ['R']).
card_text('ridgetop raptor', 'Double strike (This creature deals both first-strike and regular combat damage.)').
card_mana_cost('ridgetop raptor', ['3', 'R']).
card_cmc('ridgetop raptor', 4).
card_layout('ridgetop raptor', 'normal').
card_power('ridgetop raptor', 2).
card_toughness('ridgetop raptor', 1).

% Found in: PTK
card_name('riding red hare', 'Riding Red Hare').
card_type('riding red hare', 'Sorcery').
card_types('riding red hare', ['Sorcery']).
card_subtypes('riding red hare', []).
card_colors('riding red hare', ['W']).
card_text('riding red hare', 'Target creature gets +3/+3 and gains horsemanship until end of turn. (It can\'t be blocked except by creatures with horsemanship.)').
card_mana_cost('riding red hare', ['2', 'W']).
card_cmc('riding red hare', 3).
card_layout('riding red hare', 'normal').

% Found in: ME3, PTK
card_name('riding the dilu horse', 'Riding the Dilu Horse').
card_type('riding the dilu horse', 'Sorcery').
card_types('riding the dilu horse', ['Sorcery']).
card_subtypes('riding the dilu horse', []).
card_colors('riding the dilu horse', ['G']).
card_text('riding the dilu horse', 'Target creature gets +2/+2 and gains horsemanship. (It can\'t be blocked except by creatures with horsemanship. This effect lasts indefinitely.)').
card_mana_cost('riding the dilu horse', ['2', 'G']).
card_cmc('riding the dilu horse', 3).
card_layout('riding the dilu horse', 'normal').

% Found in: MMA, TSP, pFNM
card_name('rift bolt', 'Rift Bolt').
card_type('rift bolt', 'Sorcery').
card_types('rift bolt', ['Sorcery']).
card_subtypes('rift bolt', []).
card_colors('rift bolt', ['R']).
card_text('rift bolt', 'Rift Bolt deals 3 damage to target creature or player.\nSuspend 1—{R} (Rather than cast this card from your hand, you may pay {R} and exile it with a time counter on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_mana_cost('rift bolt', ['2', 'R']).
card_cmc('rift bolt', 3).
card_layout('rift bolt', 'normal').

% Found in: FUT, MMA
card_name('rift elemental', 'Rift Elemental').
card_type('rift elemental', 'Creature — Elemental').
card_types('rift elemental', ['Creature']).
card_subtypes('rift elemental', ['Elemental']).
card_colors('rift elemental', ['R']).
card_text('rift elemental', '{1}{R}, Remove a time counter from a permanent you control or suspended card you own: Rift Elemental gets +2/+0 until end of turn.').
card_mana_cost('rift elemental', ['R']).
card_cmc('rift elemental', 1).
card_layout('rift elemental', 'normal').
card_power('rift elemental', 1).
card_toughness('rift elemental', 1).

% Found in: PLC
card_name('riftmarked knight', 'Riftmarked Knight').
card_type('riftmarked knight', 'Creature — Human Rebel Knight').
card_types('riftmarked knight', ['Creature']).
card_subtypes('riftmarked knight', ['Human', 'Rebel', 'Knight']).
card_colors('riftmarked knight', ['W']).
card_text('riftmarked knight', 'Protection from black; flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nSuspend 3—{1}{W}{W} (Rather than cast this card from your hand, you may pay {1}{W}{W} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)\nWhen the last time counter is removed from Riftmarked Knight while it\'s exiled, put a 2/2 black Knight creature token with flanking, protection from white, and haste onto the battlefield.').
card_mana_cost('riftmarked knight', ['1', 'W', 'W']).
card_cmc('riftmarked knight', 3).
card_layout('riftmarked knight', 'normal').
card_power('riftmarked knight', 2).
card_toughness('riftmarked knight', 2).

% Found in: JUD
card_name('riftstone portal', 'Riftstone Portal').
card_type('riftstone portal', 'Land').
card_types('riftstone portal', ['Land']).
card_subtypes('riftstone portal', []).
card_colors('riftstone portal', []).
card_text('riftstone portal', '{T}: Add {1} to your mana pool.\nAs long as Riftstone Portal is in your graveyard, lands you control have \"{T}: Add {G} or {W} to your mana pool.\"').
card_layout('riftstone portal', 'normal').

% Found in: FUT, MMA
card_name('riftsweeper', 'Riftsweeper').
card_type('riftsweeper', 'Creature — Elf Shaman').
card_types('riftsweeper', ['Creature']).
card_subtypes('riftsweeper', ['Elf', 'Shaman']).
card_colors('riftsweeper', ['G']).
card_text('riftsweeper', 'When Riftsweeper enters the battlefield, choose target face-up exiled card. Its owner shuffles it into his or her library.').
card_mana_cost('riftsweeper', ['1', 'G']).
card_cmc('riftsweeper', 2).
card_layout('riftsweeper', 'normal').
card_power('riftsweeper', 2).
card_toughness('riftsweeper', 2).

% Found in: DD2, DD3_JVC, DDM, MMA, TSP
card_name('riftwing cloudskate', 'Riftwing Cloudskate').
card_type('riftwing cloudskate', 'Creature — Illusion').
card_types('riftwing cloudskate', ['Creature']).
card_subtypes('riftwing cloudskate', ['Illusion']).
card_colors('riftwing cloudskate', ['U']).
card_text('riftwing cloudskate', 'Flying\nWhen Riftwing Cloudskate enters the battlefield, return target permanent to its owner\'s hand.\nSuspend 3—{1}{U} (Rather than cast this card from your hand, you may pay {1}{U} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_mana_cost('riftwing cloudskate', ['3', 'U', 'U']).
card_cmc('riftwing cloudskate', 5).
card_layout('riftwing cloudskate', 'normal').
card_power('riftwing cloudskate', 2).
card_toughness('riftwing cloudskate', 2).

% Found in: MMQ, VIS
card_name('righteous aura', 'Righteous Aura').
card_type('righteous aura', 'Enchantment').
card_types('righteous aura', ['Enchantment']).
card_subtypes('righteous aura', []).
card_colors('righteous aura', ['W']).
card_text('righteous aura', '{W}, Pay 2 life: The next time a source of your choice would deal damage to you this turn, prevent that damage.').
card_mana_cost('righteous aura', ['1', 'W']).
card_cmc('righteous aura', 2).
card_layout('righteous aura', 'normal').

% Found in: RTR
card_name('righteous authority', 'Righteous Authority').
card_type('righteous authority', 'Enchantment — Aura').
card_types('righteous authority', ['Enchantment']).
card_subtypes('righteous authority', ['Aura']).
card_colors('righteous authority', ['W', 'U']).
card_text('righteous authority', 'Enchant creature\nEnchanted creature gets +1/+1 for each card in its controller\'s hand.\nAt the beginning of the draw step of enchanted creature\'s controller, that player draws an additional card.').
card_mana_cost('righteous authority', ['3', 'W', 'U']).
card_cmc('righteous authority', 5).
card_layout('righteous authority', 'normal').

% Found in: LEG, MED
card_name('righteous avengers', 'Righteous Avengers').
card_type('righteous avengers', 'Creature — Human Soldier').
card_types('righteous avengers', ['Creature']).
card_subtypes('righteous avengers', ['Human', 'Soldier']).
card_colors('righteous avengers', ['W']).
card_text('righteous avengers', 'Plainswalk (This creature can\'t be blocked as long as defending player controls a Plains.)').
card_mana_cost('righteous avengers', ['4', 'W']).
card_cmc('righteous avengers', 5).
card_layout('righteous avengers', 'normal').
card_power('righteous avengers', 3).
card_toughness('righteous avengers', 1).

% Found in: AVR
card_name('righteous blow', 'Righteous Blow').
card_type('righteous blow', 'Instant').
card_types('righteous blow', ['Instant']).
card_subtypes('righteous blow', []).
card_colors('righteous blow', ['W']).
card_text('righteous blow', 'Righteous Blow deals 2 damage to target attacking or blocking creature.').
card_mana_cost('righteous blow', ['W']).
card_cmc('righteous blow', 1).
card_layout('righteous blow', 'normal').

% Found in: CMD, DD3_DVD, DDC, ONS
card_name('righteous cause', 'Righteous Cause').
card_type('righteous cause', 'Enchantment').
card_types('righteous cause', ['Enchantment']).
card_subtypes('righteous cause', []).
card_colors('righteous cause', ['W']).
card_text('righteous cause', 'Whenever a creature attacks, you gain 1 life.').
card_mana_cost('righteous cause', ['3', 'W', 'W']).
card_cmc('righteous cause', 5).
card_layout('righteous cause', 'normal').

% Found in: GTC, ME4, PO2, S99
card_name('righteous charge', 'Righteous Charge').
card_type('righteous charge', 'Sorcery').
card_types('righteous charge', ['Sorcery']).
card_subtypes('righteous charge', []).
card_colors('righteous charge', ['W']).
card_text('righteous charge', 'Creatures you control get +2/+2 until end of turn.').
card_mana_cost('righteous charge', ['1', 'W', 'W']).
card_cmc('righteous charge', 3).
card_layout('righteous charge', 'normal').

% Found in: ME2, PO2, S99
card_name('righteous fury', 'Righteous Fury').
card_type('righteous fury', 'Sorcery').
card_types('righteous fury', ['Sorcery']).
card_subtypes('righteous fury', []).
card_colors('righteous fury', ['W']).
card_text('righteous fury', 'Destroy all tapped creatures. You gain 2 life for each creature destroyed this way.').
card_mana_cost('righteous fury', ['4', 'W', 'W']).
card_cmc('righteous fury', 6).
card_layout('righteous fury', 'normal').

% Found in: MMQ
card_name('righteous indignation', 'Righteous Indignation').
card_type('righteous indignation', 'Enchantment').
card_types('righteous indignation', ['Enchantment']).
card_subtypes('righteous indignation', []).
card_colors('righteous indignation', ['W']).
card_text('righteous indignation', 'Whenever a creature blocks a black or red creature, the blocking creature gets +1/+1 until end of turn.').
card_mana_cost('righteous indignation', ['2', 'W']).
card_cmc('righteous indignation', 3).
card_layout('righteous indignation', 'normal').

% Found in: VIS
card_name('righteous war', 'Righteous War').
card_type('righteous war', 'Enchantment').
card_types('righteous war', ['Enchantment']).
card_subtypes('righteous war', []).
card_colors('righteous war', ['W', 'B']).
card_text('righteous war', 'White creatures you control have protection from black.\nBlack creatures you control have protection from white.').
card_mana_cost('righteous war', ['1', 'W', 'B']).
card_cmc('righteous war', 3).
card_layout('righteous war', 'normal').
card_reserved('righteous war').

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 9ED, CED, CEI, DDL, LEA, LEB, M10
card_name('righteousness', 'Righteousness').
card_type('righteousness', 'Instant').
card_types('righteousness', ['Instant']).
card_subtypes('righteousness', []).
card_colors('righteousness', ['W']).
card_text('righteousness', 'Target blocking creature gets +7/+7 until end of turn.').
card_mana_cost('righteousness', ['W']).
card_cmc('righteousness', 1).
card_layout('righteousness', 'normal').

% Found in: CMD, pJGP
card_name('riku of two reflections', 'Riku of Two Reflections').
card_type('riku of two reflections', 'Legendary Creature — Human Wizard').
card_types('riku of two reflections', ['Creature']).
card_subtypes('riku of two reflections', ['Human', 'Wizard']).
card_supertypes('riku of two reflections', ['Legendary']).
card_colors('riku of two reflections', ['U', 'R', 'G']).
card_text('riku of two reflections', 'Whenever you cast an instant or sorcery spell, you may pay {U}{R}. If you do, copy that spell. You may choose new targets for the copy.\nWhenever another nontoken creature enters the battlefield under your control, you may pay {G}{U}. If you do, put a token that\'s a copy of that creature onto the battlefield.').
card_mana_cost('riku of two reflections', ['2', 'U', 'R', 'G']).
card_cmc('riku of two reflections', 5).
card_layout('riku of two reflections', 'normal').
card_power('riku of two reflections', 2).
card_toughness('riku of two reflections', 2).

% Found in: ICE
card_name('rime dryad', 'Rime Dryad').
card_type('rime dryad', 'Creature — Dryad').
card_types('rime dryad', ['Creature']).
card_subtypes('rime dryad', ['Dryad']).
card_colors('rime dryad', ['G']).
card_text('rime dryad', 'Snow forestwalk (This creature can\'t be blocked as long as defending player controls a snow Forest.)').
card_mana_cost('rime dryad', ['G']).
card_cmc('rime dryad', 1).
card_layout('rime dryad', 'normal').
card_power('rime dryad', 1).
card_toughness('rime dryad', 2).

% Found in: CSP
card_name('rime transfusion', 'Rime Transfusion').
card_type('rime transfusion', 'Snow Enchantment — Aura').
card_types('rime transfusion', ['Enchantment']).
card_subtypes('rime transfusion', ['Aura']).
card_supertypes('rime transfusion', ['Snow']).
card_colors('rime transfusion', ['B']).
card_text('rime transfusion', 'Enchant creature\nEnchanted creature gets +2/+1 and has \"{S}: This creature can\'t be blocked this turn except by snow creatures.\" ({S} can be paid with one mana from a snow permanent.)').
card_mana_cost('rime transfusion', ['1', 'B']).
card_cmc('rime transfusion', 2).
card_layout('rime transfusion', 'normal').

% Found in: CSP
card_name('rimebound dead', 'Rimebound Dead').
card_type('rimebound dead', 'Snow Creature — Skeleton').
card_types('rimebound dead', ['Creature']).
card_subtypes('rimebound dead', ['Skeleton']).
card_supertypes('rimebound dead', ['Snow']).
card_colors('rimebound dead', ['B']).
card_text('rimebound dead', '{S}: Regenerate Rimebound Dead. ({S} can be paid with one mana from a snow permanent.)').
card_mana_cost('rimebound dead', ['B']).
card_cmc('rimebound dead', 1).
card_layout('rimebound dead', 'normal').
card_power('rimebound dead', 1).
card_toughness('rimebound dead', 1).

% Found in: CSP
card_name('rimefeather owl', 'Rimefeather Owl').
card_type('rimefeather owl', 'Snow Creature — Bird').
card_types('rimefeather owl', ['Creature']).
card_subtypes('rimefeather owl', ['Bird']).
card_supertypes('rimefeather owl', ['Snow']).
card_colors('rimefeather owl', ['U']).
card_text('rimefeather owl', 'Flying\nRimefeather Owl\'s power and toughness are each equal to the number of snow permanents on the battlefield.\n{1}{S}: Put an ice counter on target permanent.\nPermanents with ice counters on them are snow.').
card_mana_cost('rimefeather owl', ['5', 'U', 'U']).
card_cmc('rimefeather owl', 7).
card_layout('rimefeather owl', 'normal').
card_power('rimefeather owl', '*').
card_toughness('rimefeather owl', '*').

% Found in: CSP
card_name('rimehorn aurochs', 'Rimehorn Aurochs').
card_type('rimehorn aurochs', 'Snow Creature — Aurochs').
card_types('rimehorn aurochs', ['Creature']).
card_subtypes('rimehorn aurochs', ['Aurochs']).
card_supertypes('rimehorn aurochs', ['Snow']).
card_colors('rimehorn aurochs', ['G']).
card_text('rimehorn aurochs', 'Trample\nWhenever Rimehorn Aurochs attacks, it gets +1/+0 until end of turn for each other attacking Aurochs.\n{2}{S}: Target creature blocks target creature this turn if able. ({S} can be paid with one mana from a snow permanent.)').
card_mana_cost('rimehorn aurochs', ['4', 'G']).
card_cmc('rimehorn aurochs', 5).
card_layout('rimehorn aurochs', 'normal').
card_power('rimehorn aurochs', 3).
card_toughness('rimehorn aurochs', 3).

% Found in: CSP
card_name('rimescale dragon', 'Rimescale Dragon').
card_type('rimescale dragon', 'Snow Creature — Dragon').
card_types('rimescale dragon', ['Creature']).
card_subtypes('rimescale dragon', ['Dragon']).
card_supertypes('rimescale dragon', ['Snow']).
card_colors('rimescale dragon', ['R']).
card_text('rimescale dragon', 'Flying\n{2}{S}: Tap target creature and put an ice counter on it. ({S} can be paid with one mana from a snow permanent.)\nCreatures with ice counters on them don\'t untap during their controllers\' untap steps.').
card_mana_cost('rimescale dragon', ['5', 'R', 'R']).
card_cmc('rimescale dragon', 7).
card_layout('rimescale dragon', 'normal').
card_power('rimescale dragon', 5).
card_toughness('rimescale dragon', 5).

% Found in: CSP
card_name('rimewind cryomancer', 'Rimewind Cryomancer').
card_type('rimewind cryomancer', 'Creature — Human Wizard').
card_types('rimewind cryomancer', ['Creature']).
card_subtypes('rimewind cryomancer', ['Human', 'Wizard']).
card_colors('rimewind cryomancer', ['U']).
card_text('rimewind cryomancer', '{1}, {T}: Counter target activated ability. Activate this ability only if you control four or more snow permanents. (Mana abilities can\'t be targeted.)').
card_mana_cost('rimewind cryomancer', ['3', 'U']).
card_cmc('rimewind cryomancer', 4).
card_layout('rimewind cryomancer', 'normal').
card_power('rimewind cryomancer', 2).
card_toughness('rimewind cryomancer', 3).

% Found in: CSP
card_name('rimewind taskmage', 'Rimewind Taskmage').
card_type('rimewind taskmage', 'Creature — Human Wizard').
card_types('rimewind taskmage', ['Creature']).
card_subtypes('rimewind taskmage', ['Human', 'Wizard']).
card_colors('rimewind taskmage', ['U']).
card_text('rimewind taskmage', '{1}, {T}: You may tap or untap target permanent. Activate this ability only if you control four or more snow permanents.').
card_mana_cost('rimewind taskmage', ['1', 'U']).
card_cmc('rimewind taskmage', 2).
card_layout('rimewind taskmage', 'normal').
card_power('rimewind taskmage', 1).
card_toughness('rimewind taskmage', 2).

% Found in: M13
card_name('ring of evos isle', 'Ring of Evos Isle').
card_type('ring of evos isle', 'Artifact — Equipment').
card_types('ring of evos isle', ['Artifact']).
card_subtypes('ring of evos isle', ['Equipment']).
card_colors('ring of evos isle', []).
card_text('ring of evos isle', '{2}: Equipped creature gains hexproof until end of turn. (It can\'t be the target of spells or abilities your opponents control.)\nAt the beginning of your upkeep, put a +1/+1 counter on equipped creature if it\'s blue.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('ring of evos isle', ['2']).
card_cmc('ring of evos isle', 2).
card_layout('ring of evos isle', 'normal').

% Found in: ULG, VMA
card_name('ring of gix', 'Ring of Gix').
card_type('ring of gix', 'Artifact').
card_types('ring of gix', ['Artifact']).
card_subtypes('ring of gix', []).
card_colors('ring of gix', []).
card_text('ring of gix', 'Echo {3} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\n{1}, {T}: Tap target artifact, creature, or land.').
card_mana_cost('ring of gix', ['3']).
card_cmc('ring of gix', 3).
card_layout('ring of gix', 'normal').
card_reserved('ring of gix').

% Found in: LEG
card_name('ring of immortals', 'Ring of Immortals').
card_type('ring of immortals', 'Artifact').
card_types('ring of immortals', ['Artifact']).
card_subtypes('ring of immortals', []).
card_colors('ring of immortals', []).
card_text('ring of immortals', '{3}, {T}: Counter target instant or Aura spell that targets a permanent you control.').
card_mana_cost('ring of immortals', ['5']).
card_cmc('ring of immortals', 5).
card_layout('ring of immortals', 'normal').
card_reserved('ring of immortals').

% Found in: M13
card_name('ring of kalonia', 'Ring of Kalonia').
card_type('ring of kalonia', 'Artifact — Equipment').
card_types('ring of kalonia', ['Artifact']).
card_subtypes('ring of kalonia', ['Equipment']).
card_colors('ring of kalonia', []).
card_text('ring of kalonia', 'Equipped creature has trample. (If it would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)\nAt the beginning of your upkeep, put a +1/+1 counter on equipped creature if it\'s green.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('ring of kalonia', ['2']).
card_cmc('ring of kalonia', 2).
card_layout('ring of kalonia', 'normal').

% Found in: ARN, MED
card_name('ring of ma\'rûf', 'Ring of Ma\'rûf').
card_type('ring of ma\'rûf', 'Artifact').
card_types('ring of ma\'rûf', ['Artifact']).
card_subtypes('ring of ma\'rûf', []).
card_colors('ring of ma\'rûf', []).
card_text('ring of ma\'rûf', '{5}, {T}, Exile Ring of Ma\'rûf: The next time you would draw a card this turn, instead choose a card you own from outside the game and put it into your hand.').
card_mana_cost('ring of ma\'rûf', ['5']).
card_cmc('ring of ma\'rûf', 5).
card_layout('ring of ma\'rûf', 'normal').
card_reserved('ring of ma\'rûf').

% Found in: FEM, ME4
card_name('ring of renewal', 'Ring of Renewal').
card_type('ring of renewal', 'Artifact').
card_types('ring of renewal', ['Artifact']).
card_subtypes('ring of renewal', []).
card_colors('ring of renewal', []).
card_text('ring of renewal', '{5}, {T}: Discard a card at random, then draw two cards.').
card_mana_cost('ring of renewal', ['5']).
card_cmc('ring of renewal', 5).
card_layout('ring of renewal', 'normal').
card_reserved('ring of renewal').

% Found in: M14
card_name('ring of three wishes', 'Ring of Three Wishes').
card_type('ring of three wishes', 'Artifact').
card_types('ring of three wishes', ['Artifact']).
card_subtypes('ring of three wishes', []).
card_colors('ring of three wishes', []).
card_text('ring of three wishes', 'Ring of Three Wishes enters the battlefield with three wish counters on it.\n{5}, {T}, Remove a wish counter from Ring of Three Wishes: Search your library for a card and put that card into your hand. Then shuffle your library.').
card_mana_cost('ring of three wishes', ['5']).
card_cmc('ring of three wishes', 5).
card_layout('ring of three wishes', 'normal').

% Found in: M13
card_name('ring of thune', 'Ring of Thune').
card_type('ring of thune', 'Artifact — Equipment').
card_types('ring of thune', ['Artifact']).
card_subtypes('ring of thune', ['Equipment']).
card_colors('ring of thune', []).
card_text('ring of thune', 'Equipped creature has vigilance. (Attacking doesn\'t cause it to tap.)\nAt the beginning of your upkeep, put a +1/+1 counter on equipped creature if it\'s white.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('ring of thune', ['2']).
card_cmc('ring of thune', 2).
card_layout('ring of thune', 'normal').

% Found in: M13
card_name('ring of valkas', 'Ring of Valkas').
card_type('ring of valkas', 'Artifact — Equipment').
card_types('ring of valkas', ['Artifact']).
card_subtypes('ring of valkas', ['Equipment']).
card_colors('ring of valkas', []).
card_text('ring of valkas', 'Equipped creature has haste. (It can attack and {T} no matter when it came under your control.)\nAt the beginning of your upkeep, put a +1/+1 counter on equipped creature if it\'s red.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('ring of valkas', ['2']).
card_cmc('ring of valkas', 2).
card_layout('ring of valkas', 'normal').

% Found in: M13
card_name('ring of xathrid', 'Ring of Xathrid').
card_type('ring of xathrid', 'Artifact — Equipment').
card_types('ring of xathrid', ['Artifact']).
card_subtypes('ring of xathrid', ['Equipment']).
card_colors('ring of xathrid', []).
card_text('ring of xathrid', '{2}: Regenerate equipped creature. (The next time that creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)\nAt the beginning of your upkeep, put a +1/+1 counter on equipped creature if it\'s black.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('ring of xathrid', ['2']).
card_cmc('ring of xathrid', 2).
card_layout('ring of xathrid', 'normal').

% Found in: LRW
card_name('rings of brighthearth', 'Rings of Brighthearth').
card_type('rings of brighthearth', 'Artifact').
card_types('rings of brighthearth', ['Artifact']).
card_subtypes('rings of brighthearth', []).
card_colors('rings of brighthearth', []).
card_text('rings of brighthearth', 'Whenever you activate an ability, if it isn\'t a mana ability, you may pay {2}. If you do, copy that ability. You may choose new targets for the copy.').
card_mana_cost('rings of brighthearth', ['3']).
card_cmc('rings of brighthearth', 3).
card_layout('rings of brighthearth', 'normal').

% Found in: LRW
card_name('ringskipper', 'Ringskipper').
card_type('ringskipper', 'Creature — Faerie Wizard').
card_types('ringskipper', ['Creature']).
card_subtypes('ringskipper', ['Faerie', 'Wizard']).
card_colors('ringskipper', ['U']).
card_text('ringskipper', 'Flying\nWhen Ringskipper dies, clash with an opponent. If you win, return Ringskipper to its owner\'s hand. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('ringskipper', ['1', 'U']).
card_cmc('ringskipper', 2).
card_layout('ringskipper', 'normal').
card_power('ringskipper', 1).
card_toughness('ringskipper', 1).

% Found in: ORI
card_name('ringwarden owl', 'Ringwarden Owl').
card_type('ringwarden owl', 'Creature — Bird').
card_types('ringwarden owl', ['Creature']).
card_subtypes('ringwarden owl', ['Bird']).
card_colors('ringwarden owl', ['U']).
card_text('ringwarden owl', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_mana_cost('ringwarden owl', ['3', 'U', 'U']).
card_cmc('ringwarden owl', 5).
card_layout('ringwarden owl', 'normal').
card_power('ringwarden owl', 3).
card_toughness('ringwarden owl', 3).

% Found in: DGM
card_name('riot control', 'Riot Control').
card_type('riot control', 'Instant').
card_types('riot control', ['Instant']).
card_subtypes('riot control', []).
card_colors('riot control', ['W']).
card_text('riot control', 'You gain 1 life for each creature your opponents control. Prevent all damage that would be dealt to you this turn.').
card_mana_cost('riot control', ['2', 'W']).
card_cmc('riot control', 3).
card_layout('riot control', 'normal').

% Found in: ISD
card_name('riot devils', 'Riot Devils').
card_type('riot devils', 'Creature — Devil').
card_types('riot devils', ['Creature']).
card_subtypes('riot devils', ['Devil']).
card_colors('riot devils', ['R']).
card_text('riot devils', '').
card_mana_cost('riot devils', ['2', 'R']).
card_cmc('riot devils', 3).
card_layout('riot devils', 'normal').
card_power('riot devils', 2).
card_toughness('riot devils', 3).

% Found in: GTC
card_name('riot gear', 'Riot Gear').
card_type('riot gear', 'Artifact — Equipment').
card_types('riot gear', ['Artifact']).
card_subtypes('riot gear', ['Equipment']).
card_colors('riot gear', []).
card_text('riot gear', 'Equipped creature gets +1/+2.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('riot gear', ['2']).
card_cmc('riot gear', 2).
card_layout('riot gear', 'normal').

% Found in: DGM
card_name('riot piker', 'Riot Piker').
card_type('riot piker', 'Creature — Goblin Berserker').
card_types('riot piker', ['Creature']).
card_subtypes('riot piker', ['Goblin', 'Berserker']).
card_colors('riot piker', ['R']).
card_text('riot piker', 'First strike\nRiot Piker attacks each turn if able.').
card_mana_cost('riot piker', ['1', 'R']).
card_cmc('riot piker', 2).
card_layout('riot piker', 'normal').
card_power('riot piker', 2).
card_toughness('riot piker', 1).

% Found in: AVR
card_name('riot ringleader', 'Riot Ringleader').
card_type('riot ringleader', 'Creature — Human Warrior').
card_types('riot ringleader', ['Creature']).
card_subtypes('riot ringleader', ['Human', 'Warrior']).
card_colors('riot ringleader', ['R']).
card_text('riot ringleader', 'Whenever Riot Ringleader attacks, Human creatures you control get +1/+0 until end of turn.').
card_mana_cost('riot ringleader', ['2', 'R']).
card_cmc('riot ringleader', 3).
card_layout('riot ringleader', 'normal').
card_power('riot ringleader', 2).
card_toughness('riot ringleader', 2).

% Found in: DIS
card_name('riot spikes', 'Riot Spikes').
card_type('riot spikes', 'Enchantment — Aura').
card_types('riot spikes', ['Enchantment']).
card_subtypes('riot spikes', ['Aura']).
card_colors('riot spikes', ['B', 'R']).
card_text('riot spikes', '({B/R} can be paid with either {B} or {R}.)\nEnchant creature\nEnchanted creature gets +2/-1.').
card_mana_cost('riot spikes', ['B/R']).
card_cmc('riot spikes', 1).
card_layout('riot spikes', 'normal').

% Found in: ALA
card_name('rip-clan crasher', 'Rip-Clan Crasher').
card_type('rip-clan crasher', 'Creature — Human Warrior').
card_types('rip-clan crasher', ['Creature']).
card_subtypes('rip-clan crasher', ['Human', 'Warrior']).
card_colors('rip-clan crasher', ['R', 'G']).
card_text('rip-clan crasher', 'Haste').
card_mana_cost('rip-clan crasher', ['R', 'G']).
card_cmc('rip-clan crasher', 2).
card_layout('rip-clan crasher', 'normal').
card_power('rip-clan crasher', 2).
card_toughness('rip-clan crasher', 2).

% Found in: GTC
card_name('ripscale predator', 'Ripscale Predator').
card_type('ripscale predator', 'Creature — Lizard').
card_types('ripscale predator', ['Creature']).
card_subtypes('ripscale predator', ['Lizard']).
card_colors('ripscale predator', ['R']).
card_text('ripscale predator', 'Menace (This creature can\'t be blocked except by two or more creatures.)').
card_mana_cost('ripscale predator', ['4', 'R', 'R']).
card_cmc('ripscale predator', 6).
card_layout('ripscale predator', 'normal').
card_power('ripscale predator', 6).
card_toughness('ripscale predator', 5).

% Found in: DRK
card_name('riptide', 'Riptide').
card_type('riptide', 'Instant').
card_types('riptide', ['Instant']).
card_subtypes('riptide', []).
card_colors('riptide', ['U']).
card_text('riptide', 'Tap all blue creatures.').
card_mana_cost('riptide', ['U']).
card_cmc('riptide', 1).
card_layout('riptide', 'normal').

% Found in: ONS
card_name('riptide biologist', 'Riptide Biologist').
card_type('riptide biologist', 'Creature — Human Wizard').
card_types('riptide biologist', ['Creature']).
card_subtypes('riptide biologist', ['Human', 'Wizard']).
card_colors('riptide biologist', ['U']).
card_text('riptide biologist', 'Protection from Beasts\nMorph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('riptide biologist', ['1', 'U']).
card_cmc('riptide biologist', 2).
card_layout('riptide biologist', 'normal').
card_power('riptide biologist', 1).
card_toughness('riptide biologist', 2).

% Found in: JOU
card_name('riptide chimera', 'Riptide Chimera').
card_type('riptide chimera', 'Enchantment Creature — Chimera').
card_types('riptide chimera', ['Enchantment', 'Creature']).
card_subtypes('riptide chimera', ['Chimera']).
card_colors('riptide chimera', ['U']).
card_text('riptide chimera', 'Flying\nAt the beginning of your upkeep, return an enchantment you control to its owner\'s hand.').
card_mana_cost('riptide chimera', ['2', 'U']).
card_cmc('riptide chimera', 3).
card_layout('riptide chimera', 'normal').
card_power('riptide chimera', 3).
card_toughness('riptide chimera', 4).

% Found in: ONS
card_name('riptide chronologist', 'Riptide Chronologist').
card_type('riptide chronologist', 'Creature — Human Wizard').
card_types('riptide chronologist', ['Creature']).
card_subtypes('riptide chronologist', ['Human', 'Wizard']).
card_colors('riptide chronologist', ['U']).
card_text('riptide chronologist', '{U}, Sacrifice Riptide Chronologist: Untap all creatures of the creature type of your choice.').
card_mana_cost('riptide chronologist', ['3', 'U', 'U']).
card_cmc('riptide chronologist', 5).
card_layout('riptide chronologist', 'normal').
card_power('riptide chronologist', 1).
card_toughness('riptide chronologist', 3).

% Found in: INV
card_name('riptide crab', 'Riptide Crab').
card_type('riptide crab', 'Creature — Crab').
card_types('riptide crab', ['Creature']).
card_subtypes('riptide crab', ['Crab']).
card_colors('riptide crab', ['W', 'U']).
card_text('riptide crab', 'Vigilance\nWhen Riptide Crab dies, draw a card.').
card_mana_cost('riptide crab', ['1', 'W', 'U']).
card_cmc('riptide crab', 3).
card_layout('riptide crab', 'normal').
card_power('riptide crab', 1).
card_toughness('riptide crab', 3).

% Found in: LGN
card_name('riptide director', 'Riptide Director').
card_type('riptide director', 'Creature — Human Wizard').
card_types('riptide director', ['Creature']).
card_subtypes('riptide director', ['Human', 'Wizard']).
card_colors('riptide director', ['U']).
card_text('riptide director', '{2}{U}{U}, {T}: Draw a card for each Wizard you control.').
card_mana_cost('riptide director', ['2', 'U', 'U']).
card_cmc('riptide director', 4).
card_layout('riptide director', 'normal').
card_power('riptide director', 2).
card_toughness('riptide director', 3).

% Found in: ONS
card_name('riptide entrancer', 'Riptide Entrancer').
card_type('riptide entrancer', 'Creature — Human Wizard').
card_types('riptide entrancer', ['Creature']).
card_subtypes('riptide entrancer', ['Human', 'Wizard']).
card_colors('riptide entrancer', ['U']).
card_text('riptide entrancer', 'Whenever Riptide Entrancer deals combat damage to a player, you may sacrifice it. If you do, gain control of target creature that player controls. (This effect lasts indefinitely.)\nMorph {U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('riptide entrancer', ['1', 'U', 'U']).
card_cmc('riptide entrancer', 3).
card_layout('riptide entrancer', 'normal').
card_power('riptide entrancer', 1).
card_toughness('riptide entrancer', 1).

% Found in: ONS
card_name('riptide laboratory', 'Riptide Laboratory').
card_type('riptide laboratory', 'Land').
card_types('riptide laboratory', ['Land']).
card_subtypes('riptide laboratory', []).
card_colors('riptide laboratory', []).
card_text('riptide laboratory', '{T}: Add {1} to your mana pool.\n{1}{U}, {T}: Return target Wizard you control to its owner\'s hand.').
card_layout('riptide laboratory', 'normal').

% Found in: LGN
card_name('riptide mangler', 'Riptide Mangler').
card_type('riptide mangler', 'Creature — Beast').
card_types('riptide mangler', ['Creature']).
card_subtypes('riptide mangler', ['Beast']).
card_colors('riptide mangler', ['U']).
card_text('riptide mangler', '{1}{U}: Change Riptide Mangler\'s base power to target creature\'s power. (This effect lasts indefinitely.)').
card_mana_cost('riptide mangler', ['1', 'U']).
card_cmc('riptide mangler', 2).
card_layout('riptide mangler', 'normal').
card_power('riptide mangler', 0).
card_toughness('riptide mangler', 3).

% Found in: PLC
card_name('riptide pilferer', 'Riptide Pilferer').
card_type('riptide pilferer', 'Creature — Merfolk Rogue').
card_types('riptide pilferer', ['Creature']).
card_subtypes('riptide pilferer', ['Merfolk', 'Rogue']).
card_colors('riptide pilferer', ['U']).
card_text('riptide pilferer', 'Whenever Riptide Pilferer deals combat damage to a player, that player discards a card.\nMorph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('riptide pilferer', ['1', 'U']).
card_cmc('riptide pilferer', 2).
card_layout('riptide pilferer', 'normal').
card_power('riptide pilferer', 1).
card_toughness('riptide pilferer', 1).

% Found in: ONS
card_name('riptide replicator', 'Riptide Replicator').
card_type('riptide replicator', 'Artifact').
card_types('riptide replicator', ['Artifact']).
card_subtypes('riptide replicator', []).
card_colors('riptide replicator', []).
card_text('riptide replicator', 'As Riptide Replicator enters the battlefield, choose a color and a creature type.\nRiptide Replicator enters the battlefield with X charge counters on it.\n{4}, {T}: Put an X/X creature token of the chosen color and type onto the battlefield, where X is the number of charge counters on Riptide Replicator.').
card_mana_cost('riptide replicator', ['X', '4']).
card_cmc('riptide replicator', 4).
card_layout('riptide replicator', 'normal').

% Found in: ONS
card_name('riptide shapeshifter', 'Riptide Shapeshifter').
card_type('riptide shapeshifter', 'Creature — Shapeshifter').
card_types('riptide shapeshifter', ['Creature']).
card_subtypes('riptide shapeshifter', ['Shapeshifter']).
card_colors('riptide shapeshifter', ['U']).
card_text('riptide shapeshifter', '{2}{U}{U}, Sacrifice Riptide Shapeshifter: Choose a creature type. Reveal cards from the top of your library until you reveal a creature card of that type. Put that card onto the battlefield and shuffle the rest into your library.').
card_mana_cost('riptide shapeshifter', ['3', 'U', 'U']).
card_cmc('riptide shapeshifter', 5).
card_layout('riptide shapeshifter', 'normal').
card_power('riptide shapeshifter', 3).
card_toughness('riptide shapeshifter', 3).

% Found in: C14, SCG
card_name('riptide survivor', 'Riptide Survivor').
card_type('riptide survivor', 'Creature — Human Wizard').
card_types('riptide survivor', ['Creature']).
card_subtypes('riptide survivor', ['Human', 'Wizard']).
card_colors('riptide survivor', ['U']).
card_text('riptide survivor', 'Morph {1}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Riptide Survivor is turned face up, discard two cards, then draw three cards.').
card_mana_cost('riptide survivor', ['2', 'U']).
card_cmc('riptide survivor', 3).
card_layout('riptide survivor', 'normal').
card_power('riptide survivor', 2).
card_toughness('riptide survivor', 1).

% Found in: DDH, DIS
card_name('rise', 'Rise').
card_type('rise', 'Sorcery').
card_types('rise', ['Sorcery']).
card_subtypes('rise', []).
card_colors('rise', ['U', 'B']).
card_text('rise', 'Return target creature card from a graveyard and target creature on the battlefield to their owners\' hands.').
card_mana_cost('rise', ['U', 'B']).
card_cmc('rise', 2).
card_layout('rise', 'split').
card_sides('rise', 'fall').

% Found in: CMD, DD3_GVL, DDD, M10, M11, M13, pWPN
card_name('rise from the grave', 'Rise from the Grave').
card_type('rise from the grave', 'Sorcery').
card_types('rise from the grave', ['Sorcery']).
card_subtypes('rise from the grave', []).
card_colors('rise from the grave', ['B']).
card_text('rise from the grave', 'Put target creature card from a graveyard onto the battlefield under your control. That creature is a black Zombie in addition to its other colors and types.').
card_mana_cost('rise from the grave', ['4', 'B']).
card_cmc('rise from the grave', 5).
card_layout('rise from the grave', 'normal').

% Found in: JOU
card_name('rise of eagles', 'Rise of Eagles').
card_type('rise of eagles', 'Sorcery').
card_types('rise of eagles', ['Sorcery']).
card_subtypes('rise of eagles', []).
card_colors('rise of eagles', ['U']).
card_text('rise of eagles', 'Put two 2/2 blue Bird enchantment creature tokens with flying onto the battlefield. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('rise of eagles', ['4', 'U', 'U']).
card_cmc('rise of eagles', 6).
card_layout('rise of eagles', 'normal').

% Found in: M14
card_name('rise of the dark realms', 'Rise of the Dark Realms').
card_type('rise of the dark realms', 'Sorcery').
card_types('rise of the dark realms', ['Sorcery']).
card_subtypes('rise of the dark realms', []).
card_colors('rise of the dark realms', ['B']).
card_text('rise of the dark realms', 'Put all creature cards from all graveyards onto the battlefield under your control.').
card_mana_cost('rise of the dark realms', ['7', 'B', 'B']).
card_cmc('rise of the dark realms', 9).
card_layout('rise of the dark realms', 'normal').

% Found in: EVE
card_name('rise of the hobgoblins', 'Rise of the Hobgoblins').
card_type('rise of the hobgoblins', 'Enchantment').
card_types('rise of the hobgoblins', ['Enchantment']).
card_subtypes('rise of the hobgoblins', []).
card_colors('rise of the hobgoblins', ['W', 'R']).
card_text('rise of the hobgoblins', 'When Rise of the Hobgoblins enters the battlefield, you may pay {X}. If you do, put X 1/1 red and white Goblin Soldier creature tokens onto the battlefield.\n{R/W}: Red creatures and white creatures you control gain first strike until end of turn.').
card_mana_cost('rise of the hobgoblins', ['R/W', 'R/W']).
card_cmc('rise of the hobgoblins', 2).
card_layout('rise of the hobgoblins', 'normal').

% Found in: BNG
card_name('rise to the challenge', 'Rise to the Challenge').
card_type('rise to the challenge', 'Instant').
card_types('rise to the challenge', ['Instant']).
card_subtypes('rise to the challenge', []).
card_colors('rise to the challenge', ['R']).
card_text('rise to the challenge', 'Target creature gets +2/+0 and gains first strike until end of turn.').
card_mana_cost('rise to the challenge', ['1', 'R']).
card_cmc('rise to the challenge', 2).
card_layout('rise to the challenge', 'normal').

% Found in: DTK
card_name('risen executioner', 'Risen Executioner').
card_type('risen executioner', 'Creature — Zombie Warrior').
card_types('risen executioner', ['Creature']).
card_subtypes('risen executioner', ['Zombie', 'Warrior']).
card_colors('risen executioner', ['B']).
card_text('risen executioner', 'Risen Executioner can\'t block.\nOther Zombie creatures you control get +1/+1.\nYou may cast Risen Executioner from your graveyard if you pay {1} more to cast it for each other creature card in your graveyard.').
card_mana_cost('risen executioner', ['2', 'B', 'B']).
card_cmc('risen executioner', 4).
card_layout('risen executioner', 'normal').
card_power('risen executioner', 4).
card_toughness('risen executioner', 3).

% Found in: RTR
card_name('risen sanctuary', 'Risen Sanctuary').
card_type('risen sanctuary', 'Creature — Elemental').
card_types('risen sanctuary', ['Creature']).
card_subtypes('risen sanctuary', ['Elemental']).
card_colors('risen sanctuary', ['W', 'G']).
card_text('risen sanctuary', 'Vigilance').
card_mana_cost('risen sanctuary', ['5', 'G', 'W']).
card_cmc('risen sanctuary', 7).
card_layout('risen sanctuary', 'normal').
card_power('risen sanctuary', 8).
card_toughness('risen sanctuary', 8).

% Found in: MMQ
card_name('rishadan airship', 'Rishadan Airship').
card_type('rishadan airship', 'Creature — Human Pirate').
card_types('rishadan airship', ['Creature']).
card_subtypes('rishadan airship', ['Human', 'Pirate']).
card_colors('rishadan airship', ['U']).
card_text('rishadan airship', 'Flying\nRishadan Airship can block only creatures with flying.').
card_mana_cost('rishadan airship', ['2', 'U']).
card_cmc('rishadan airship', 3).
card_layout('rishadan airship', 'normal').
card_power('rishadan airship', 3).
card_toughness('rishadan airship', 1).

% Found in: MMQ
card_name('rishadan brigand', 'Rishadan Brigand').
card_type('rishadan brigand', 'Creature — Human Pirate').
card_types('rishadan brigand', ['Creature']).
card_subtypes('rishadan brigand', ['Human', 'Pirate']).
card_colors('rishadan brigand', ['U']).
card_text('rishadan brigand', 'Flying\nWhen Rishadan Brigand enters the battlefield, each opponent sacrifices a permanent unless he or she pays {3}.\nRishadan Brigand can block only creatures with flying.').
card_mana_cost('rishadan brigand', ['4', 'U']).
card_cmc('rishadan brigand', 5).
card_layout('rishadan brigand', 'normal').
card_power('rishadan brigand', 3).
card_toughness('rishadan brigand', 2).

% Found in: MMQ
card_name('rishadan cutpurse', 'Rishadan Cutpurse').
card_type('rishadan cutpurse', 'Creature — Human Pirate').
card_types('rishadan cutpurse', ['Creature']).
card_subtypes('rishadan cutpurse', ['Human', 'Pirate']).
card_colors('rishadan cutpurse', ['U']).
card_text('rishadan cutpurse', 'When Rishadan Cutpurse enters the battlefield, each opponent sacrifices a permanent unless he or she pays {1}.').
card_mana_cost('rishadan cutpurse', ['2', 'U']).
card_cmc('rishadan cutpurse', 3).
card_layout('rishadan cutpurse', 'normal').
card_power('rishadan cutpurse', 1).
card_toughness('rishadan cutpurse', 1).

% Found in: MMQ
card_name('rishadan footpad', 'Rishadan Footpad').
card_type('rishadan footpad', 'Creature — Human Pirate').
card_types('rishadan footpad', ['Creature']).
card_subtypes('rishadan footpad', ['Human', 'Pirate']).
card_colors('rishadan footpad', ['U']).
card_text('rishadan footpad', 'When Rishadan Footpad enters the battlefield, each opponent sacrifices a permanent unless he or she pays {2}.').
card_mana_cost('rishadan footpad', ['3', 'U']).
card_cmc('rishadan footpad', 4).
card_layout('rishadan footpad', 'normal').
card_power('rishadan footpad', 2).
card_toughness('rishadan footpad', 2).

% Found in: MMQ
card_name('rishadan pawnshop', 'Rishadan Pawnshop').
card_type('rishadan pawnshop', 'Artifact').
card_types('rishadan pawnshop', ['Artifact']).
card_subtypes('rishadan pawnshop', []).
card_colors('rishadan pawnshop', []).
card_text('rishadan pawnshop', '{2}, {T}: Shuffle target nontoken permanent you control into its owner\'s library.').
card_mana_cost('rishadan pawnshop', ['2']).
card_cmc('rishadan pawnshop', 2).
card_layout('rishadan pawnshop', 'normal').

% Found in: MMQ
card_name('rishadan port', 'Rishadan Port').
card_type('rishadan port', 'Land').
card_types('rishadan port', ['Land']).
card_subtypes('rishadan port', []).
card_colors('rishadan port', []).
card_text('rishadan port', '{T}: Add {1} to your mana pool.\n{1}, {T}: Tap target land.').
card_layout('rishadan port', 'normal').

% Found in: BFZ
card_name('rising miasma', 'Rising Miasma').
card_type('rising miasma', 'Sorcery').
card_types('rising miasma', ['Sorcery']).
card_subtypes('rising miasma', []).
card_colors('rising miasma', ['B']).
card_text('rising miasma', 'All creatures get -2/-2 until end of turn.\nAwaken 3—{5}{B}{B} (If you cast this spell for {5}{B}{B}, also put three +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_mana_cost('rising miasma', ['3', 'B']).
card_cmc('rising miasma', 4).
card_layout('rising miasma', 'normal').

% Found in: NMS
card_name('rising waters', 'Rising Waters').
card_type('rising waters', 'Enchantment').
card_types('rising waters', ['Enchantment']).
card_subtypes('rising waters', []).
card_colors('rising waters', ['U']).
card_text('rising waters', 'Lands don\'t untap during their controllers\' untap steps.\nAt the beginning of each player\'s upkeep, that player untaps a land he or she controls.').
card_mana_cost('rising waters', ['3', 'U']).
card_cmc('rising waters', 4).
card_layout('rising waters', 'normal').

% Found in: ONS
card_name('risky move', 'Risky Move').
card_type('risky move', 'Enchantment').
card_types('risky move', ['Enchantment']).
card_subtypes('risky move', []).
card_colors('risky move', ['R']).
card_text('risky move', 'At the beginning of each player\'s upkeep, that player gains control of Risky Move.\nWhen you gain control of Risky Move from another player, choose a creature you control and an opponent. Flip a coin. If you lose the flip, that opponent gains control of that creature.').
card_mana_cost('risky move', ['3', 'R', 'R', 'R']).
card_cmc('risky move', 6).
card_layout('risky move', 'normal').

% Found in: SHM
card_name('rite of consumption', 'Rite of Consumption').
card_type('rite of consumption', 'Sorcery').
card_types('rite of consumption', ['Sorcery']).
card_subtypes('rite of consumption', []).
card_colors('rite of consumption', ['B']).
card_text('rite of consumption', 'As an additional cost to cast Rite of Consumption, sacrifice a creature.\nRite of Consumption deals damage equal to the sacrificed creature\'s power to target player. You gain life equal to the damage dealt this way.').
card_mana_cost('rite of consumption', ['1', 'B']).
card_cmc('rite of consumption', 2).
card_layout('rite of consumption', 'normal').

% Found in: CSP
card_name('rite of flame', 'Rite of Flame').
card_type('rite of flame', 'Sorcery').
card_types('rite of flame', ['Sorcery']).
card_subtypes('rite of flame', []).
card_colors('rite of flame', ['R']).
card_text('rite of flame', 'Add {R}{R} to your mana pool, then add {R} to your mana pool for each card named Rite of Flame in each graveyard.').
card_mana_cost('rite of flame', ['R']).
card_cmc('rite of flame', 1).
card_layout('rite of flame', 'normal').

% Found in: 5DN
card_name('rite of passage', 'Rite of Passage').
card_type('rite of passage', 'Enchantment').
card_types('rite of passage', ['Enchantment']).
card_subtypes('rite of passage', []).
card_colors('rite of passage', ['G']).
card_text('rite of passage', 'Whenever a creature you control is dealt damage, put a +1/+1 counter on it. (The damage is dealt before the counter is put on.)').
card_mana_cost('rite of passage', ['2', 'G']).
card_cmc('rite of passage', 3).
card_layout('rite of passage', 'normal').

% Found in: C14, ZEN
card_name('rite of replication', 'Rite of Replication').
card_type('rite of replication', 'Sorcery').
card_types('rite of replication', ['Sorcery']).
card_subtypes('rite of replication', []).
card_colors('rite of replication', ['U']).
card_text('rite of replication', 'Kicker {5} (You may pay an additional {5} as you cast this spell.)\nPut a token onto the battlefield that\'s a copy of target creature. If Rite of Replication was kicked, put five of those tokens onto the battlefield instead.').
card_mana_cost('rite of replication', ['2', 'U', 'U']).
card_cmc('rite of replication', 4).
card_layout('rite of replication', 'normal').

% Found in: AVR
card_name('rite of ruin', 'Rite of Ruin').
card_type('rite of ruin', 'Sorcery').
card_types('rite of ruin', ['Sorcery']).
card_subtypes('rite of ruin', []).
card_colors('rite of ruin', ['R']).
card_text('rite of ruin', 'Choose an order for artifacts, creatures, and lands. Each player sacrifices one permanent of the first type, sacrifices two of the second type, then sacrifices three of the third type.').
card_mana_cost('rite of ruin', ['5', 'R', 'R']).
card_cmc('rite of ruin', 7).
card_layout('rite of ruin', 'normal').

% Found in: KTK
card_name('rite of the serpent', 'Rite of the Serpent').
card_type('rite of the serpent', 'Sorcery').
card_types('rite of the serpent', ['Sorcery']).
card_subtypes('rite of the serpent', []).
card_colors('rite of the serpent', ['B']).
card_text('rite of the serpent', 'Destroy target creature. If that creature had a +1/+1 counter on it, put a 1/1 green Snake creature token onto the battlefield.').
card_mana_cost('rite of the serpent', ['4', 'B', 'B']).
card_cmc('rite of the serpent', 6).
card_layout('rite of the serpent', 'normal').

% Found in: FRF
card_name('rite of undoing', 'Rite of Undoing').
card_type('rite of undoing', 'Instant').
card_types('rite of undoing', ['Instant']).
card_subtypes('rite of undoing', []).
card_colors('rite of undoing', ['U']).
card_text('rite of undoing', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nReturn target nonland permanent you control and target nonland permanent you don\'t control to their owners\' hands.').
card_mana_cost('rite of undoing', ['4', 'U']).
card_cmc('rite of undoing', 5).
card_layout('rite of undoing', 'normal').

% Found in: FUT, M12
card_name('rites of flourishing', 'Rites of Flourishing').
card_type('rites of flourishing', 'Enchantment').
card_types('rites of flourishing', ['Enchantment']).
card_subtypes('rites of flourishing', []).
card_colors('rites of flourishing', ['G']).
card_text('rites of flourishing', 'At the beginning of each player\'s draw step, that player draws an additional card.\nEach player may play an additional land on each of his or her turns.').
card_mana_cost('rites of flourishing', ['2', 'G']).
card_cmc('rites of flourishing', 3).
card_layout('rites of flourishing', 'normal').

% Found in: ODY, VMA
card_name('rites of initiation', 'Rites of Initiation').
card_type('rites of initiation', 'Instant').
card_types('rites of initiation', ['Instant']).
card_subtypes('rites of initiation', []).
card_colors('rites of initiation', ['R']).
card_text('rites of initiation', 'Discard any number of cards at random. Creatures you control get +1/+0 until end of turn for each card discarded this way.').
card_mana_cost('rites of initiation', ['R']).
card_cmc('rites of initiation', 1).
card_layout('rites of initiation', 'normal').

% Found in: RTR
card_name('rites of reaping', 'Rites of Reaping').
card_type('rites of reaping', 'Sorcery').
card_types('rites of reaping', ['Sorcery']).
card_subtypes('rites of reaping', []).
card_colors('rites of reaping', ['B', 'G']).
card_text('rites of reaping', 'Target creature gets +3/+3 until end of turn. Another target creature gets -3/-3 until end of turn.').
card_mana_cost('rites of reaping', ['4', 'B', 'G']).
card_cmc('rites of reaping', 6).
card_layout('rites of reaping', 'normal').

% Found in: ODY
card_name('rites of refusal', 'Rites of Refusal').
card_type('rites of refusal', 'Instant').
card_types('rites of refusal', ['Instant']).
card_subtypes('rites of refusal', []).
card_colors('rites of refusal', ['U']).
card_text('rites of refusal', 'Discard any number of cards. Counter target spell unless its controller pays {3} for each card discarded this way.').
card_mana_cost('rites of refusal', ['1', 'U']).
card_cmc('rites of refusal', 2).
card_layout('rites of refusal', 'normal').

% Found in: ODY
card_name('rites of spring', 'Rites of Spring').
card_type('rites of spring', 'Sorcery').
card_types('rites of spring', ['Sorcery']).
card_subtypes('rites of spring', []).
card_colors('rites of spring', ['G']).
card_text('rites of spring', 'Discard any number of cards. Search your library for up to that many basic land cards, reveal those cards, and put them into your hand. Then shuffle your library.').
card_mana_cost('rites of spring', ['1', 'G']).
card_cmc('rites of spring', 2).
card_layout('rites of spring', 'normal').

% Found in: INV
card_name('rith\'s attendant', 'Rith\'s Attendant').
card_type('rith\'s attendant', 'Artifact Creature — Golem').
card_types('rith\'s attendant', ['Artifact', 'Creature']).
card_subtypes('rith\'s attendant', ['Golem']).
card_colors('rith\'s attendant', []).
card_text('rith\'s attendant', '{1}, Sacrifice Rith\'s Attendant: Add {R}{G}{W} to your mana pool.').
card_mana_cost('rith\'s attendant', ['5']).
card_cmc('rith\'s attendant', 5).
card_layout('rith\'s attendant', 'normal').
card_power('rith\'s attendant', 3).
card_toughness('rith\'s attendant', 3).

% Found in: DDE, PLS
card_name('rith\'s charm', 'Rith\'s Charm').
card_type('rith\'s charm', 'Instant').
card_types('rith\'s charm', ['Instant']).
card_subtypes('rith\'s charm', []).
card_colors('rith\'s charm', ['W', 'R', 'G']).
card_text('rith\'s charm', 'Choose one —\n• Destroy target nonbasic land.\n• Put three 1/1 green Saproling creature tokens onto the battlefield.\n• Prevent all damage a source of your choice would deal this turn.').
card_mana_cost('rith\'s charm', ['R', 'G', 'W']).
card_cmc('rith\'s charm', 3).
card_layout('rith\'s charm', 'normal').

% Found in: PLS
card_name('rith\'s grove', 'Rith\'s Grove').
card_type('rith\'s grove', 'Land — Lair').
card_types('rith\'s grove', ['Land']).
card_subtypes('rith\'s grove', ['Lair']).
card_colors('rith\'s grove', []).
card_text('rith\'s grove', 'When Rith\'s Grove enters the battlefield, sacrifice it unless you return a non-Lair land you control to its owner\'s hand.\n{T}: Add {R}, {G}, or {W} to your mana pool.').
card_layout('rith\'s grove', 'normal').

% Found in: DDE, DRB, INV
card_name('rith, the awakener', 'Rith, the Awakener').
card_type('rith, the awakener', 'Legendary Creature — Dragon').
card_types('rith, the awakener', ['Creature']).
card_subtypes('rith, the awakener', ['Dragon']).
card_supertypes('rith, the awakener', ['Legendary']).
card_colors('rith, the awakener', ['W', 'R', 'G']).
card_text('rith, the awakener', 'Flying\nWhenever Rith, the Awakener deals combat damage to a player, you may pay {2}{G}. If you do, choose a color, then put a 1/1 green Saproling creature token onto the battlefield for each permanent of that color.').
card_mana_cost('rith, the awakener', ['3', 'R', 'G', 'W']).
card_cmc('rith, the awakener', 6).
card_layout('rith, the awakener', 'normal').
card_power('rith, the awakener', 6).
card_toughness('rith, the awakener', 6).

% Found in: VAN
card_name('rith, the awakener avatar', 'Rith, the Awakener Avatar').
card_type('rith, the awakener avatar', 'Vanguard').
card_types('rith, the awakener avatar', ['Vanguard']).
card_subtypes('rith, the awakener avatar', []).
card_colors('rith, the awakener avatar', []).
card_text('rith, the awakener avatar', 'Whenever a creature you control deals combat damage to a player, you may pay {5}. If you do, put a 5/5 red Dragon creature token with flying onto the battlefield.').
card_layout('rith, the awakener avatar', 'vanguard').

% Found in: DST
card_name('ritual of restoration', 'Ritual of Restoration').
card_type('ritual of restoration', 'Sorcery').
card_types('ritual of restoration', ['Sorcery']).
card_subtypes('ritual of restoration', []).
card_colors('ritual of restoration', ['W']).
card_text('ritual of restoration', 'Return target artifact card from your graveyard to your hand.').
card_mana_cost('ritual of restoration', ['W']).
card_cmc('ritual of restoration', 1).
card_layout('ritual of restoration', 'normal').

% Found in: MIR
card_name('ritual of steel', 'Ritual of Steel').
card_type('ritual of steel', 'Enchantment — Aura').
card_types('ritual of steel', ['Enchantment']).
card_subtypes('ritual of steel', ['Aura']).
card_colors('ritual of steel', ['W']).
card_text('ritual of steel', 'Enchant creature\nWhen Ritual of Steel enters the battlefield, draw a card at the beginning of the next turn\'s upkeep.\nEnchanted creature gets +0/+2.').
card_mana_cost('ritual of steel', ['2', 'W']).
card_cmc('ritual of steel', 3).
card_layout('ritual of steel', 'normal').

% Found in: ICE, ME2
card_name('ritual of subdual', 'Ritual of Subdual').
card_type('ritual of subdual', 'Enchantment').
card_types('ritual of subdual', ['Enchantment']).
card_subtypes('ritual of subdual', []).
card_colors('ritual of subdual', ['G']).
card_text('ritual of subdual', 'Cumulative upkeep {2} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nIf a land is tapped for mana, it produces colorless mana instead of any other type.').
card_mana_cost('ritual of subdual', ['4', 'G', 'G']).
card_cmc('ritual of subdual', 6).
card_layout('ritual of subdual', 'normal').
card_reserved('ritual of subdual').

% Found in: ALL, ME2
card_name('ritual of the machine', 'Ritual of the Machine').
card_type('ritual of the machine', 'Sorcery').
card_types('ritual of the machine', ['Sorcery']).
card_subtypes('ritual of the machine', []).
card_colors('ritual of the machine', ['B']).
card_text('ritual of the machine', 'As an additional cost to cast Ritual of the Machine, sacrifice a creature.\nGain control of target nonartifact, nonblack creature.').
card_mana_cost('ritual of the machine', ['2', 'B', 'B']).
card_cmc('ritual of the machine', 4).
card_layout('ritual of the machine', 'normal').
card_reserved('ritual of the machine').

% Found in: JOU
card_name('ritual of the returned', 'Ritual of the Returned').
card_type('ritual of the returned', 'Instant').
card_types('ritual of the returned', ['Instant']).
card_subtypes('ritual of the returned', []).
card_colors('ritual of the returned', ['B']).
card_text('ritual of the returned', 'Exile target creature card from your graveyard. Put a black Zombie creature token onto the battlefield. Its power is equal to that card\'s power and its toughness is equal to that card\'s toughness.').
card_mana_cost('ritual of the returned', ['3', 'B']).
card_cmc('ritual of the returned', 4).
card_layout('ritual of the returned', 'normal').

% Found in: ULG
card_name('rivalry', 'Rivalry').
card_type('rivalry', 'Enchantment').
card_types('rivalry', ['Enchantment']).
card_subtypes('rivalry', []).
card_colors('rivalry', ['R']).
card_text('rivalry', 'At the beginning of each player\'s upkeep, if that player controls more lands than each other player, Rivalry deals 2 damage to him or her.').
card_mana_cost('rivalry', ['2', 'R']).
card_cmc('rivalry', 3).
card_layout('rivalry', 'normal').

% Found in: MOR, PC2
card_name('rivals\' duel', 'Rivals\' Duel').
card_type('rivals\' duel', 'Sorcery').
card_types('rivals\' duel', ['Sorcery']).
card_subtypes('rivals\' duel', []).
card_colors('rivals\' duel', ['R']).
card_text('rivals\' duel', 'Choose two target creatures that share no creature types. Those creatures fight each other. (Each deals damage equal to its power to the other.)').
card_mana_cost('rivals\' duel', ['3', 'R']).
card_cmc('rivals\' duel', 4).
card_layout('rivals\' duel', 'normal').

% Found in: LEG, ME3
card_name('riven turnbull', 'Riven Turnbull').
card_type('riven turnbull', 'Legendary Creature — Human Advisor').
card_types('riven turnbull', ['Creature']).
card_subtypes('riven turnbull', ['Human', 'Advisor']).
card_supertypes('riven turnbull', ['Legendary']).
card_colors('riven turnbull', ['U', 'B']).
card_text('riven turnbull', '{T}: Add {B} to your mana pool.').
card_mana_cost('riven turnbull', ['5', 'U', 'B']).
card_cmc('riven turnbull', 7).
card_layout('riven turnbull', 'normal').
card_power('riven turnbull', 5).
card_toughness('riven turnbull', 7).

% Found in: 9ED, PO2
card_name('river bear', 'River Bear').
card_type('river bear', 'Creature — Bear').
card_types('river bear', ['Creature']).
card_subtypes('river bear', ['Bear']).
card_colors('river bear', ['G']).
card_text('river bear', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('river bear', ['3', 'G']).
card_cmc('river bear', 4).
card_layout('river bear', 'normal').
card_power('river bear', 3).
card_toughness('river bear', 3).

% Found in: 6ED, BRB, DDM, DPA, VIS, ZEN, pFNM
card_name('river boa', 'River Boa').
card_type('river boa', 'Creature — Snake').
card_types('river boa', ['Creature']).
card_subtypes('river boa', ['Snake']).
card_colors('river boa', ['G']).
card_text('river boa', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\n{G}: Regenerate River Boa.').
card_mana_cost('river boa', ['1', 'G']).
card_cmc('river boa', 2).
card_layout('river boa', 'normal').
card_power('river boa', 2).
card_toughness('river boa', 1).

% Found in: ICE
card_name('river delta', 'River Delta').
card_type('river delta', 'Land').
card_types('river delta', ['Land']).
card_subtypes('river delta', []).
card_colors('river delta', []).
card_text('river delta', 'River Delta doesn\'t untap during your untap step if it has a depletion counter on it.\nAt the beginning of your upkeep, remove a depletion counter from River Delta.\n{T}: Add {U} or {B} to your mana pool. Put a depletion counter on River Delta.').
card_layout('river delta', 'normal').
card_reserved('river delta').

% Found in: CHK
card_name('river kaijin', 'River Kaijin').
card_type('river kaijin', 'Creature — Spirit').
card_types('river kaijin', ['Creature']).
card_subtypes('river kaijin', ['Spirit']).
card_colors('river kaijin', ['U']).
card_text('river kaijin', '').
card_mana_cost('river kaijin', ['2', 'U']).
card_cmc('river kaijin', 3).
card_layout('river kaijin', 'normal').
card_power('river kaijin', 1).
card_toughness('river kaijin', 4).

% Found in: SHM
card_name('river kelpie', 'River Kelpie').
card_type('river kelpie', 'Creature — Beast').
card_types('river kelpie', ['Creature']).
card_subtypes('river kelpie', ['Beast']).
card_colors('river kelpie', ['U']).
card_text('river kelpie', 'Whenever River Kelpie or another permanent enters the battlefield from a graveyard, draw a card.\nWhenever a player casts a spell from a graveyard, draw a card.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('river kelpie', ['3', 'U', 'U']).
card_cmc('river kelpie', 5).
card_layout('river kelpie', 'normal').
card_power('river kelpie', 3).
card_toughness('river kelpie', 3).

% Found in: FEM, MED
card_name('river merfolk', 'River Merfolk').
card_type('river merfolk', 'Creature — Merfolk').
card_types('river merfolk', ['Creature']).
card_subtypes('river merfolk', ['Merfolk']).
card_colors('river merfolk', ['U']).
card_text('river merfolk', '{U}: River Merfolk gains mountainwalk until end of turn. (It can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('river merfolk', ['U', 'U']).
card_cmc('river merfolk', 2).
card_layout('river merfolk', 'normal').
card_power('river merfolk', 2).
card_toughness('river merfolk', 1).
card_reserved('river merfolk').

% Found in: FUT
card_name('river of tears', 'River of Tears').
card_type('river of tears', 'Land').
card_types('river of tears', ['Land']).
card_subtypes('river of tears', []).
card_colors('river of tears', []).
card_text('river of tears', '{T}: Add {U} to your mana pool. If you played a land this turn, add {B} to your mana pool instead.').
card_layout('river of tears', 'normal').

% Found in: SHM
card_name('river\'s grasp', 'River\'s Grasp').
card_type('river\'s grasp', 'Sorcery').
card_types('river\'s grasp', ['Sorcery']).
card_subtypes('river\'s grasp', []).
card_colors('river\'s grasp', ['U', 'B']).
card_text('river\'s grasp', 'If {U} was spent to cast River\'s Grasp, return up to one target creature to its owner\'s hand. If {B} was spent to cast River\'s Grasp, target player reveals his or her hand, you choose a nonland card from it, then that player discards that card. (Do both if {U}{B} was spent.)').
card_mana_cost('river\'s grasp', ['3', 'U/B']).
card_cmc('river\'s grasp', 4).
card_layout('river\'s grasp', 'normal').

% Found in: EVE
card_name('riverfall mimic', 'Riverfall Mimic').
card_type('riverfall mimic', 'Creature — Shapeshifter').
card_types('riverfall mimic', ['Creature']).
card_subtypes('riverfall mimic', ['Shapeshifter']).
card_colors('riverfall mimic', ['U', 'R']).
card_text('riverfall mimic', 'Whenever you cast a spell that\'s both blue and red, Riverfall Mimic has base power and toughness 3/3 until end of turn and can\'t be blocked this turn.').
card_mana_cost('riverfall mimic', ['1', 'U/R']).
card_cmc('riverfall mimic', 2).
card_layout('riverfall mimic', 'normal').
card_power('riverfall mimic', 2).
card_toughness('riverfall mimic', 1).

% Found in: KTK
card_name('riverwheel aerialists', 'Riverwheel Aerialists').
card_type('riverwheel aerialists', 'Creature — Djinn Monk').
card_types('riverwheel aerialists', ['Creature']).
card_subtypes('riverwheel aerialists', ['Djinn', 'Monk']).
card_colors('riverwheel aerialists', ['U']).
card_text('riverwheel aerialists', 'Flying\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_mana_cost('riverwheel aerialists', ['5', 'U']).
card_cmc('riverwheel aerialists', 6).
card_layout('riverwheel aerialists', 'normal').
card_power('riverwheel aerialists', 4).
card_toughness('riverwheel aerialists', 5).

% Found in: RTR
card_name('rix maadi guildmage', 'Rix Maadi Guildmage').
card_type('rix maadi guildmage', 'Creature — Human Shaman').
card_types('rix maadi guildmage', ['Creature']).
card_subtypes('rix maadi guildmage', ['Human', 'Shaman']).
card_colors('rix maadi guildmage', ['B', 'R']).
card_text('rix maadi guildmage', '{B}{R}: Target blocking creature gets -1/-1 until end of turn.\n{B}{R}: Target player who lost life this turn loses 1 life.').
card_mana_cost('rix maadi guildmage', ['B', 'R']).
card_cmc('rix maadi guildmage', 2).
card_layout('rix maadi guildmage', 'normal').
card_power('rix maadi guildmage', 2).
card_toughness('rix maadi guildmage', 2).

% Found in: DIS
card_name('rix maadi, dungeon palace', 'Rix Maadi, Dungeon Palace').
card_type('rix maadi, dungeon palace', 'Land').
card_types('rix maadi, dungeon palace', ['Land']).
card_subtypes('rix maadi, dungeon palace', []).
card_colors('rix maadi, dungeon palace', []).
card_text('rix maadi, dungeon palace', '{T}: Add {1} to your mana pool.\n{1}{B}{R}, {T}: Each player discards a card. Activate this ability only any time you could cast a sorcery.').
card_layout('rix maadi, dungeon palace', 'normal').

% Found in: KTK
card_name('roar of challenge', 'Roar of Challenge').
card_type('roar of challenge', 'Sorcery').
card_types('roar of challenge', ['Sorcery']).
card_subtypes('roar of challenge', []).
card_colors('roar of challenge', ['G']).
card_text('roar of challenge', 'All creatures able to block target creature this turn do so.\nFerocious — That creature gains indestructible until end of turn if you control a creature with power 4 or greater.').
card_mana_cost('roar of challenge', ['2', 'G']).
card_cmc('roar of challenge', 3).
card_layout('roar of challenge', 'normal').

% Found in: BOK
card_name('roar of jukai', 'Roar of Jukai').
card_type('roar of jukai', 'Instant — Arcane').
card_types('roar of jukai', ['Instant']).
card_subtypes('roar of jukai', ['Arcane']).
card_colors('roar of jukai', ['G']).
card_text('roar of jukai', 'If you control a Forest, each blocked creature gets +2/+2 until end of turn.\nSplice onto Arcane—An opponent gains 5 life. (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('roar of jukai', ['2', 'G']).
card_cmc('roar of jukai', 3).
card_layout('roar of jukai', 'normal').

% Found in: 5DN
card_name('roar of reclamation', 'Roar of Reclamation').
card_type('roar of reclamation', 'Sorcery').
card_types('roar of reclamation', ['Sorcery']).
card_subtypes('roar of reclamation', []).
card_colors('roar of reclamation', ['W']).
card_text('roar of reclamation', 'Each player returns all artifact cards from his or her graveyard to the battlefield.').
card_mana_cost('roar of reclamation', ['5', 'W', 'W']).
card_cmc('roar of reclamation', 7).
card_layout('roar of reclamation', 'normal').

% Found in: MOR
card_name('roar of the crowd', 'Roar of the Crowd').
card_type('roar of the crowd', 'Sorcery').
card_types('roar of the crowd', ['Sorcery']).
card_subtypes('roar of the crowd', []).
card_colors('roar of the crowd', ['R']).
card_text('roar of the crowd', 'Choose a creature type. Roar of the Crowd deals damage to target creature or player equal to the number of permanents you control of the chosen type.').
card_mana_cost('roar of the crowd', ['3', 'R']).
card_cmc('roar of the crowd', 4).
card_layout('roar of the crowd', 'normal').

% Found in: MRD
card_name('roar of the kha', 'Roar of the Kha').
card_type('roar of the kha', 'Instant').
card_types('roar of the kha', ['Instant']).
card_subtypes('roar of the kha', []).
card_colors('roar of the kha', ['W']).
card_text('roar of the kha', 'Choose one —\n• Creatures you control get +1/+1 until end of turn.\n• Untap all creatures you control.\nEntwine {1}{W} (Choose both if you pay the entwine cost.)').
card_mana_cost('roar of the kha', ['1', 'W']).
card_cmc('roar of the kha', 2).
card_layout('roar of the kha', 'normal').

% Found in: ODY, VMA, pFNM
card_name('roar of the wurm', 'Roar of the Wurm').
card_type('roar of the wurm', 'Sorcery').
card_types('roar of the wurm', ['Sorcery']).
card_subtypes('roar of the wurm', []).
card_colors('roar of the wurm', ['G']).
card_text('roar of the wurm', 'Put a 6/6 green Wurm creature token onto the battlefield.\nFlashback {3}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('roar of the wurm', ['6', 'G']).
card_cmc('roar of the wurm', 7).
card_layout('roar of the wurm', 'normal').

% Found in: M13, M15
card_name('roaring primadox', 'Roaring Primadox').
card_type('roaring primadox', 'Creature — Beast').
card_types('roaring primadox', ['Creature']).
card_subtypes('roaring primadox', ['Beast']).
card_colors('roaring primadox', ['G']).
card_text('roaring primadox', 'At the beginning of your upkeep, return a creature you control to its owner\'s hand.').
card_mana_cost('roaring primadox', ['3', 'G']).
card_cmc('roaring primadox', 4).
card_layout('roaring primadox', 'normal').
card_power('roaring primadox', 4).
card_toughness('roaring primadox', 4).

% Found in: DST
card_name('roaring slagwurm', 'Roaring Slagwurm').
card_type('roaring slagwurm', 'Creature — Wurm').
card_types('roaring slagwurm', ['Creature']).
card_subtypes('roaring slagwurm', ['Wurm']).
card_colors('roaring slagwurm', ['G']).
card_text('roaring slagwurm', 'Whenever Roaring Slagwurm attacks, tap all artifacts.').
card_mana_cost('roaring slagwurm', ['5', 'G', 'G']).
card_cmc('roaring slagwurm', 7).
card_layout('roaring slagwurm', 'normal').
card_power('roaring slagwurm', 6).
card_toughness('roaring slagwurm', 4).

% Found in: DTK
card_name('roast', 'Roast').
card_type('roast', 'Sorcery').
card_types('roast', ['Sorcery']).
card_subtypes('roast', []).
card_colors('roast', ['R']).
card_text('roast', 'Roast deals 5 damage to target creature without flying.').
card_mana_cost('roast', ['1', 'R']).
card_cmc('roast', 2).
card_layout('roast', 'normal').

% Found in: MMQ
card_name('robber fly', 'Robber Fly').
card_type('robber fly', 'Creature — Insect').
card_types('robber fly', ['Creature']).
card_subtypes('robber fly', ['Insect']).
card_colors('robber fly', ['R']).
card_text('robber fly', 'Flying\nWhenever Robber Fly becomes blocked, defending player discards all the cards in his or her hand, then draws that many cards.').
card_mana_cost('robber fly', ['2', 'R']).
card_cmc('robber fly', 3).
card_layout('robber fly', 'normal').
card_power('robber fly', 1).
card_toughness('robber fly', 1).

% Found in: 10E, EXO
card_name('robe of mirrors', 'Robe of Mirrors').
card_type('robe of mirrors', 'Enchantment — Aura').
card_types('robe of mirrors', ['Enchantment']).
card_subtypes('robe of mirrors', ['Aura']).
card_colors('robe of mirrors', ['U']).
card_text('robe of mirrors', 'Enchant creature (Target a creature as you cast this. This card enters the battlefield attached to that creature.)\nEnchanted creature has shroud. (It can\'t be the target of spells or abilities.)').
card_mana_cost('robe of mirrors', ['U']).
card_cmc('robe of mirrors', 1).
card_layout('robe of mirrors', 'normal').

% Found in: pCEL
card_name('robot chicken', 'Robot Chicken').
card_type('robot chicken', 'Artifact Creature — Chicken Construct').
card_types('robot chicken', ['Artifact', 'Creature']).
card_subtypes('robot chicken', ['Chicken', 'Construct']).
card_colors('robot chicken', []).
card_text('robot chicken', 'Whenever you cast a spell, put a 0/1 colorless Egg artifact creature token onto the battlefield.\nWhenever an Egg you control is put into a graveyard from the battlefield, destroy target artifact or creature.').
card_mana_cost('robot chicken', ['4']).
card_cmc('robot chicken', 4).
card_layout('robot chicken', 'normal').
card_power('robot chicken', 2).
card_toughness('robot chicken', 2).

% Found in: M11, M12
card_name('roc egg', 'Roc Egg').
card_type('roc egg', 'Creature — Bird').
card_types('roc egg', ['Creature']).
card_subtypes('roc egg', ['Bird']).
card_colors('roc egg', ['W']).
card_text('roc egg', 'Defender (This creature can\'t attack.)\nWhen Roc Egg dies, put a 3/3 white Bird creature token with flying onto the battlefield.').
card_mana_cost('roc egg', ['2', 'W']).
card_cmc('roc egg', 3).
card_layout('roc egg', 'normal').
card_power('roc egg', 0).
card_toughness('roc egg', 3).

% Found in: WTH
card_name('roc hatchling', 'Roc Hatchling').
card_type('roc hatchling', 'Creature — Bird').
card_types('roc hatchling', ['Creature']).
card_subtypes('roc hatchling', ['Bird']).
card_colors('roc hatchling', ['R']).
card_text('roc hatchling', 'Roc Hatchling enters the battlefield with four shell counters on it.\nAt the beginning of your upkeep, remove a shell counter from Roc Hatchling.\nAs long as Roc Hatchling has no shell counters on it, it gets +3/+2 and has flying.').
card_mana_cost('roc hatchling', ['R']).
card_cmc('roc hatchling', 1).
card_layout('roc hatchling', 'normal').
card_power('roc hatchling', 0).
card_toughness('roc hatchling', 1).

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME4
card_name('roc of kher ridges', 'Roc of Kher Ridges').
card_type('roc of kher ridges', 'Creature — Bird').
card_types('roc of kher ridges', ['Creature']).
card_subtypes('roc of kher ridges', ['Bird']).
card_colors('roc of kher ridges', ['R']).
card_text('roc of kher ridges', 'Flying').
card_mana_cost('roc of kher ridges', ['3', 'R']).
card_cmc('roc of kher ridges', 4).
card_layout('roc of kher ridges', 'normal').
card_power('roc of kher ridges', 3).
card_toughness('roc of kher ridges', 3).
card_reserved('roc of kher ridges').

% Found in: 10E, MMQ
card_name('rock badger', 'Rock Badger').
card_type('rock badger', 'Creature — Badger Beast').
card_types('rock badger', ['Creature']).
card_subtypes('rock badger', ['Badger', 'Beast']).
card_colors('rock badger', ['R']).
card_text('rock badger', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('rock badger', ['4', 'R']).
card_cmc('rock badger', 5).
card_layout('rock badger', 'normal').
card_power('rock badger', 3).
card_toughness('rock badger', 3).

% Found in: MIR
card_name('rock basilisk', 'Rock Basilisk').
card_type('rock basilisk', 'Creature — Basilisk').
card_types('rock basilisk', ['Creature']).
card_subtypes('rock basilisk', ['Basilisk']).
card_colors('rock basilisk', ['R', 'G']).
card_text('rock basilisk', 'Whenever Rock Basilisk blocks or becomes blocked by a non-Wall creature, destroy that creature at end of combat.').
card_mana_cost('rock basilisk', ['4', 'R', 'G']).
card_cmc('rock basilisk', 6).
card_layout('rock basilisk', 'normal').
card_power('rock basilisk', 4).
card_toughness('rock basilisk', 5).
card_reserved('rock basilisk').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME4
card_name('rock hydra', 'Rock Hydra').
card_type('rock hydra', 'Creature — Hydra').
card_types('rock hydra', ['Creature']).
card_subtypes('rock hydra', ['Hydra']).
card_colors('rock hydra', ['R']).
card_text('rock hydra', 'Rock Hydra enters the battlefield with X +1/+1 counters on it.\nFor each 1 damage that would be dealt to Rock Hydra, if it has a +1/+1 counter on it, remove a +1/+1 counter from it and prevent that 1 damage.\n{R}: Prevent the next 1 damage that would be dealt to Rock Hydra this turn.\n{R}{R}{R}: Put a +1/+1 counter on Rock Hydra. Activate this ability only during your upkeep.').
card_mana_cost('rock hydra', ['X', 'R', 'R']).
card_cmc('rock hydra', 2).
card_layout('rock hydra', 'normal').
card_power('rock hydra', 0).
card_toughness('rock hydra', 0).
card_reserved('rock hydra').

% Found in: SCG
card_name('rock jockey', 'Rock Jockey').
card_type('rock jockey', 'Creature — Goblin').
card_types('rock jockey', ['Creature']).
card_subtypes('rock jockey', ['Goblin']).
card_colors('rock jockey', ['R']).
card_text('rock jockey', 'You can\'t cast Rock Jockey if you\'ve played a land this turn.\nYou can\'t play lands if you\'ve cast Rock Jockey this turn.').
card_mana_cost('rock jockey', ['2', 'R']).
card_cmc('rock jockey', 3).
card_layout('rock jockey', 'normal').
card_power('rock jockey', 3).
card_toughness('rock jockey', 3).

% Found in: UGL
card_name('rock lobster', 'Rock Lobster').
card_type('rock lobster', 'Artifact Creature').
card_types('rock lobster', ['Artifact', 'Creature']).
card_subtypes('rock lobster', []).
card_colors('rock lobster', []).
card_text('rock lobster', 'Scissors Lizards cannot attack or block.').
card_mana_cost('rock lobster', ['4']).
card_cmc('rock lobster', 4).
card_layout('rock lobster', 'normal').
card_power('rock lobster', 4).
card_toughness('rock lobster', 3).

% Found in: VIS
card_name('rock slide', 'Rock Slide').
card_type('rock slide', 'Instant').
card_types('rock slide', ['Instant']).
card_subtypes('rock slide', []).
card_colors('rock slide', ['R']).
card_text('rock slide', 'Rock Slide deals X damage divided as you choose among any number of target attacking or blocking creatures without flying.').
card_mana_cost('rock slide', ['X', 'R']).
card_cmc('rock slide', 1).
card_layout('rock slide', 'normal').

% Found in: ALA
card_name('rockcaster platoon', 'Rockcaster Platoon').
card_type('rockcaster platoon', 'Creature — Rhino Soldier').
card_types('rockcaster platoon', ['Creature']).
card_subtypes('rockcaster platoon', ['Rhino', 'Soldier']).
card_colors('rockcaster platoon', ['W']).
card_text('rockcaster platoon', '{4}{G}: Rockcaster Platoon deals 2 damage to each creature with flying and each player.').
card_mana_cost('rockcaster platoon', ['5', 'W', 'W']).
card_cmc('rockcaster platoon', 7).
card_layout('rockcaster platoon', 'normal').
card_power('rockcaster platoon', 5).
card_toughness('rockcaster platoon', 7).

% Found in: 3ED, ATQ
card_name('rocket launcher', 'Rocket Launcher').
card_type('rocket launcher', 'Artifact').
card_types('rocket launcher', ['Artifact']).
card_subtypes('rocket launcher', []).
card_colors('rocket launcher', []).
card_text('rocket launcher', '{2}: Rocket Launcher deals 1 damage to target creature or player. Destroy Rocket Launcher at the beginning of the next end step. Activate this ability only if you\'ve controlled Rocket Launcher continuously since the beginning of your most recent turn.').
card_mana_cost('rocket launcher', ['4']).
card_cmc('rocket launcher', 4).
card_layout('rocket launcher', 'normal').

% Found in: UNH
card_name('rocket-powered turbo slug', 'Rocket-Powered Turbo Slug').
card_type('rocket-powered turbo slug', 'Creature — Slug').
card_types('rocket-powered turbo slug', ['Creature']).
card_subtypes('rocket-powered turbo slug', ['Slug']).
card_colors('rocket-powered turbo slug', ['R']).
card_text('rocket-powered turbo slug', 'Super haste (This may attack the turn before you play it. (You may put this card into play from your hand, tapped and attacking, during your declare attackers step. If you do, you lose the game at the end of your next turn unless you pay this card\'s mana cost during that turn.))').
card_mana_cost('rocket-powered turbo slug', ['3', 'R']).
card_cmc('rocket-powered turbo slug', 4).
card_layout('rocket-powered turbo slug', 'normal').
card_power('rocket-powered turbo slug', 3).
card_toughness('rocket-powered turbo slug', 1).

% Found in: LGN
card_name('rockshard elemental', 'Rockshard Elemental').
card_type('rockshard elemental', 'Creature — Elemental').
card_types('rockshard elemental', ['Creature']).
card_subtypes('rockshard elemental', ['Elemental']).
card_colors('rockshard elemental', ['R']).
card_text('rockshard elemental', 'Double strike (This creature deals both first-strike and regular combat damage.)\nMorph {4}{R}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('rockshard elemental', ['5', 'R', 'R']).
card_cmc('rockshard elemental', 7).
card_layout('rockshard elemental', 'normal').
card_power('rockshard elemental', 4).
card_toughness('rockshard elemental', 3).

% Found in: ME4, PTK
card_name('rockslide ambush', 'Rockslide Ambush').
card_type('rockslide ambush', 'Sorcery').
card_types('rockslide ambush', ['Sorcery']).
card_subtypes('rockslide ambush', []).
card_colors('rockslide ambush', ['R']).
card_text('rockslide ambush', 'Rockslide Ambush deals damage to target creature equal to the number of Mountains you control.').
card_mana_cost('rockslide ambush', ['1', 'R']).
card_cmc('rockslide ambush', 2).
card_layout('rockslide ambush', 'normal').

% Found in: ALA, HOP
card_name('rockslide elemental', 'Rockslide Elemental').
card_type('rockslide elemental', 'Creature — Elemental').
card_types('rockslide elemental', ['Creature']).
card_subtypes('rockslide elemental', ['Elemental']).
card_colors('rockslide elemental', ['R']).
card_text('rockslide elemental', 'First strike\nWhenever another creature dies, you may put a +1/+1 counter on Rockslide Elemental.').
card_mana_cost('rockslide elemental', ['2', 'R']).
card_cmc('rockslide elemental', 3).
card_layout('rockslide elemental', 'normal').
card_power('rockslide elemental', 1).
card_toughness('rockslide elemental', 1).

% Found in: DDP, MIR, VMA
card_name('rocky tar pit', 'Rocky Tar Pit').
card_type('rocky tar pit', 'Land').
card_types('rocky tar pit', ['Land']).
card_subtypes('rocky tar pit', []).
card_colors('rocky tar pit', []).
card_text('rocky tar pit', 'Rocky Tar Pit enters the battlefield tapped.\n{T}, Sacrifice Rocky Tar Pit: Search your library for a Swamp or Mountain card and put it onto the battlefield. Then shuffle your library.').
card_layout('rocky tar pit', 'normal').

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, CED, CEI, ITP, LEA, LEB, M10, M14, RQS, S00
card_name('rod of ruin', 'Rod of Ruin').
card_type('rod of ruin', 'Artifact').
card_types('rod of ruin', ['Artifact']).
card_subtypes('rod of ruin', []).
card_colors('rod of ruin', []).
card_text('rod of ruin', '{3}, {T}: Rod of Ruin deals 1 damage to target creature or player.').
card_mana_cost('rod of ruin', ['4']).
card_cmc('rod of ruin', 4).
card_layout('rod of ruin', 'normal').

% Found in: UNH
card_name('rod of spanking', 'Rod of Spanking').
card_type('rod of spanking', 'Artifact').
card_types('rod of spanking', ['Artifact']).
card_subtypes('rod of spanking', []).
card_colors('rod of spanking', []).
card_text('rod of spanking', '{2}, {T}: Rod of Spanking deals 1 damage to target player. Then untap Rod of Spanking unless that player says \"Thank you, sir. May I have another?\"').
card_mana_cost('rod of spanking', ['1']).
card_cmc('rod of spanking', 1).
card_layout('rod of spanking', 'normal').

% Found in: VAN
card_name('rofellos', 'Rofellos').
card_type('rofellos', 'Vanguard').
card_types('rofellos', ['Vanguard']).
card_subtypes('rofellos', []).
card_colors('rofellos', []).
card_text('rofellos', 'Whenever a creature you control dies, draw a card.').
card_layout('rofellos', 'vanguard').

% Found in: UDS
card_name('rofellos\'s gift', 'Rofellos\'s Gift').
card_type('rofellos\'s gift', 'Sorcery').
card_types('rofellos\'s gift', ['Sorcery']).
card_subtypes('rofellos\'s gift', []).
card_colors('rofellos\'s gift', ['G']).
card_text('rofellos\'s gift', 'Reveal any number of green cards in your hand. Return an enchantment card from your graveyard to your hand for each card revealed this way.').
card_mana_cost('rofellos\'s gift', ['G']).
card_cmc('rofellos\'s gift', 1).
card_layout('rofellos\'s gift', 'normal').

% Found in: UDS, VMA
card_name('rofellos, llanowar emissary', 'Rofellos, Llanowar Emissary').
card_type('rofellos, llanowar emissary', 'Legendary Creature — Elf Druid').
card_types('rofellos, llanowar emissary', ['Creature']).
card_subtypes('rofellos, llanowar emissary', ['Elf', 'Druid']).
card_supertypes('rofellos, llanowar emissary', ['Legendary']).
card_colors('rofellos, llanowar emissary', ['G']).
card_text('rofellos, llanowar emissary', '{T}: Add {G} to your mana pool for each Forest you control.').
card_mana_cost('rofellos, llanowar emissary', ['G', 'G']).
card_cmc('rofellos, llanowar emissary', 2).
card_layout('rofellos, llanowar emissary', 'normal').
card_power('rofellos, llanowar emissary', 2).
card_toughness('rofellos, llanowar emissary', 1).
card_reserved('rofellos, llanowar emissary').

% Found in: WTH
card_name('rogue elephant', 'Rogue Elephant').
card_type('rogue elephant', 'Creature — Elephant').
card_types('rogue elephant', ['Creature']).
card_subtypes('rogue elephant', ['Elephant']).
card_colors('rogue elephant', ['G']).
card_text('rogue elephant', 'When Rogue Elephant enters the battlefield, sacrifice it unless you sacrifice a Forest.').
card_mana_cost('rogue elephant', ['G']).
card_cmc('rogue elephant', 1).
card_layout('rogue elephant', 'normal').
card_power('rogue elephant', 3).
card_toughness('rogue elephant', 3).

% Found in: 9ED, INV
card_name('rogue kavu', 'Rogue Kavu').
card_type('rogue kavu', 'Creature — Kavu').
card_types('rogue kavu', ['Creature']).
card_subtypes('rogue kavu', ['Kavu']).
card_colors('rogue kavu', ['R']).
card_text('rogue kavu', 'Whenever Rogue Kavu attacks alone, it gets +2/+0 until end of turn.').
card_mana_cost('rogue kavu', ['1', 'R']).
card_cmc('rogue kavu', 2).
card_layout('rogue kavu', 'normal').
card_power('rogue kavu', 1).
card_toughness('rogue kavu', 1).

% Found in: ALL, ME2
card_name('rogue skycaptain', 'Rogue Skycaptain').
card_type('rogue skycaptain', 'Creature — Human Rogue Mercenary').
card_types('rogue skycaptain', ['Creature']).
card_subtypes('rogue skycaptain', ['Human', 'Rogue', 'Mercenary']).
card_colors('rogue skycaptain', ['R']).
card_text('rogue skycaptain', 'Flying\nAt the beginning of your upkeep, put a wage counter on Rogue Skycaptain. You may pay {2} for each wage counter on it. If you don\'t, remove all wage counters from Rogue Skycaptain and an opponent gains control of it.').
card_mana_cost('rogue skycaptain', ['2', 'R']).
card_cmc('rogue skycaptain', 3).
card_layout('rogue skycaptain', 'normal').
card_power('rogue skycaptain', 3).
card_toughness('rogue skycaptain', 4).
card_reserved('rogue skycaptain').

% Found in: M15
card_name('rogue\'s gloves', 'Rogue\'s Gloves').
card_type('rogue\'s gloves', 'Artifact — Equipment').
card_types('rogue\'s gloves', ['Artifact']).
card_subtypes('rogue\'s gloves', ['Equipment']).
card_colors('rogue\'s gloves', []).
card_text('rogue\'s gloves', 'Whenever equipped creature deals combat damage to a player, you may draw a card.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('rogue\'s gloves', ['2']).
card_cmc('rogue\'s gloves', 2).
card_layout('rogue\'s gloves', 'normal').

% Found in: DDM, ORI, RTR
card_name('rogue\'s passage', 'Rogue\'s Passage').
card_type('rogue\'s passage', 'Land').
card_types('rogue\'s passage', ['Land']).
card_subtypes('rogue\'s passage', []).
card_colors('rogue\'s passage', []).
card_text('rogue\'s passage', '{T}: Add {1} to your mana pool.\n{4}, {T}: Target creature can\'t be blocked this turn.').
card_layout('rogue\'s passage', 'normal').

% Found in: LEG, ME3
card_name('rohgahh of kher keep', 'Rohgahh of Kher Keep').
card_type('rohgahh of kher keep', 'Legendary Creature — Kobold').
card_types('rohgahh of kher keep', ['Creature']).
card_subtypes('rohgahh of kher keep', ['Kobold']).
card_supertypes('rohgahh of kher keep', ['Legendary']).
card_colors('rohgahh of kher keep', ['B', 'R']).
card_text('rohgahh of kher keep', 'At the beginning of your upkeep, you may pay {R}{R}{R}. If you don\'t, tap Rohgahh of Kher Keep and all creatures named Kobolds of Kher Keep, then an opponent gains control of them.\nCreatures you control named Kobolds of Kher Keep get +2/+2.').
card_mana_cost('rohgahh of kher keep', ['2', 'B', 'B', 'R', 'R']).
card_cmc('rohgahh of kher keep', 6).
card_layout('rohgahh of kher keep', 'normal').
card_power('rohgahh of kher keep', 5).
card_toughness('rohgahh of kher keep', 5).
card_reserved('rohgahh of kher keep').

% Found in: ZEN
card_name('roil elemental', 'Roil Elemental').
card_type('roil elemental', 'Creature — Elemental').
card_types('roil elemental', ['Creature']).
card_subtypes('roil elemental', ['Elemental']).
card_colors('roil elemental', ['U']).
card_text('roil elemental', 'Flying\nLandfall — Whenever a land enters the battlefield under your control, you may gain control of target creature for as long as you control Roil Elemental.').
card_mana_cost('roil elemental', ['3', 'U', 'U', 'U']).
card_cmc('roil elemental', 6).
card_layout('roil elemental', 'normal').
card_power('roil elemental', 3).
card_toughness('roil elemental', 2).

% Found in: BFZ
card_name('roil spout', 'Roil Spout').
card_type('roil spout', 'Sorcery').
card_types('roil spout', ['Sorcery']).
card_subtypes('roil spout', []).
card_colors('roil spout', ['W', 'U']).
card_text('roil spout', 'Put target creature on top of its owner\'s library.\nAwaken 4—{4}{W}{U} (If you cast this spell for {4}{W}{U}, also put four +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_mana_cost('roil spout', ['1', 'W', 'U']).
card_cmc('roil spout', 3).
card_layout('roil spout', 'normal').

% Found in: BFZ
card_name('roil\'s retribution', 'Roil\'s Retribution').
card_type('roil\'s retribution', 'Instant').
card_types('roil\'s retribution', ['Instant']).
card_subtypes('roil\'s retribution', []).
card_colors('roil\'s retribution', ['W']).
card_text('roil\'s retribution', 'Roil\'s Retribution deals 5 damage divided as you choose among any number of target attacking or blocking creatures.').
card_mana_cost('roil\'s retribution', ['3', 'W', 'W']).
card_cmc('roil\'s retribution', 5).
card_layout('roil\'s retribution', 'normal').

% Found in: PLC
card_name('roiling horror', 'Roiling Horror').
card_type('roiling horror', 'Creature — Horror').
card_types('roiling horror', ['Creature']).
card_subtypes('roiling horror', ['Horror']).
card_colors('roiling horror', ['B']).
card_text('roiling horror', 'Roiling Horror\'s power and toughness are each equal to your life total minus the life total of an opponent with the most life.\nSuspend X—{X}{B}{B}{B}. X can\'t be 0. (Rather than cast this card from your hand, you may pay {X}{B}{B}{B} and exile it with X time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)\nWhenever a time counter is removed from Roiling Horror while it\'s exiled, target player loses 1 life and you gain 1 life.').
card_mana_cost('roiling horror', ['3', 'B', 'B']).
card_cmc('roiling horror', 5).
card_layout('roiling horror', 'normal').
card_power('roiling horror', '*').
card_toughness('roiling horror', '*').

% Found in: WWK
card_name('roiling terrain', 'Roiling Terrain').
card_type('roiling terrain', 'Sorcery').
card_types('roiling terrain', ['Sorcery']).
card_subtypes('roiling terrain', []).
card_colors('roiling terrain', ['R']).
card_text('roiling terrain', 'Destroy target land, then Roiling Terrain deals damage to that land\'s controller equal to the number of land cards in that player\'s graveyard.').
card_mana_cost('roiling terrain', ['2', 'R', 'R']).
card_cmc('roiling terrain', 4).
card_layout('roiling terrain', 'normal').

% Found in: BFZ
card_name('roilmage\'s trick', 'Roilmage\'s Trick').
card_type('roilmage\'s trick', 'Instant').
card_types('roilmage\'s trick', ['Instant']).
card_subtypes('roilmage\'s trick', []).
card_colors('roilmage\'s trick', ['U']).
card_text('roilmage\'s trick', 'Converge — Creatures your opponents control get -X/-0 until end of turn, where X is the number of colors of mana spent to cast Roilmage\'s Trick.\nDraw a card.').
card_mana_cost('roilmage\'s trick', ['3', 'U']).
card_cmc('roilmage\'s trick', 4).
card_layout('roilmage\'s trick', 'normal').

% Found in: JOU
card_name('rollick of abandon', 'Rollick of Abandon').
card_type('rollick of abandon', 'Sorcery').
card_types('rollick of abandon', ['Sorcery']).
card_subtypes('rollick of abandon', []).
card_colors('rollick of abandon', ['R']).
card_text('rollick of abandon', 'All creatures get +2/-2 until end of turn.').
card_mana_cost('rollick of abandon', ['3', 'R', 'R']).
card_cmc('rollick of abandon', 5).
card_layout('rollick of abandon', 'normal').

% Found in: ME3, PTK, V14
card_name('rolling earthquake', 'Rolling Earthquake').
card_type('rolling earthquake', 'Sorcery').
card_types('rolling earthquake', ['Sorcery']).
card_subtypes('rolling earthquake', []).
card_colors('rolling earthquake', ['R']).
card_text('rolling earthquake', 'Rolling Earthquake deals X damage to each creature without horsemanship and each player.').
card_mana_cost('rolling earthquake', ['X', 'R']).
card_cmc('rolling earthquake', 1).
card_layout('rolling earthquake', 'normal').

% Found in: RAV
card_name('rolling spoil', 'Rolling Spoil').
card_type('rolling spoil', 'Sorcery').
card_types('rolling spoil', ['Sorcery']).
card_subtypes('rolling spoil', []).
card_colors('rolling spoil', ['G']).
card_text('rolling spoil', 'Destroy target land. If {B} was spent to cast Rolling Spoil, all creatures get -1/-1 until end of turn.').
card_mana_cost('rolling spoil', ['2', 'G', 'G']).
card_cmc('rolling spoil', 4).
card_layout('rolling spoil', 'normal').

% Found in: 7ED, 8ED, STH
card_name('rolling stones', 'Rolling Stones').
card_type('rolling stones', 'Enchantment').
card_types('rolling stones', ['Enchantment']).
card_subtypes('rolling stones', []).
card_colors('rolling stones', ['W']).
card_text('rolling stones', 'Wall creatures can attack as though they didn\'t have defender.').
card_mana_cost('rolling stones', ['1', 'W']).
card_cmc('rolling stones', 2).
card_layout('rolling stones', 'normal').

% Found in: ISD
card_name('rolling temblor', 'Rolling Temblor').
card_type('rolling temblor', 'Sorcery').
card_types('rolling temblor', ['Sorcery']).
card_subtypes('rolling temblor', []).
card_colors('rolling temblor', ['R']).
card_text('rolling temblor', 'Rolling Temblor deals 2 damage to each creature without flying.\nFlashback {4}{R}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('rolling temblor', ['2', 'R']).
card_cmc('rolling temblor', 3).
card_layout('rolling temblor', 'normal').

% Found in: BFZ, BRB, HOP, TMP, TPR
card_name('rolling thunder', 'Rolling Thunder').
card_type('rolling thunder', 'Sorcery').
card_types('rolling thunder', ['Sorcery']).
card_subtypes('rolling thunder', []).
card_colors('rolling thunder', ['R']).
card_text('rolling thunder', 'Rolling Thunder deals X damage divided as you choose among any number of target creatures and/or players.').
card_mana_cost('rolling thunder', ['X', 'R', 'R']).
card_cmc('rolling thunder', 2).
card_layout('rolling thunder', 'normal').

% Found in: SOK
card_name('ronin cavekeeper', 'Ronin Cavekeeper').
card_type('ronin cavekeeper', 'Creature — Human Samurai').
card_types('ronin cavekeeper', ['Creature']).
card_subtypes('ronin cavekeeper', ['Human', 'Samurai']).
card_colors('ronin cavekeeper', ['R']).
card_text('ronin cavekeeper', 'Bushido 2 (Whenever this creature blocks or becomes blocked, it gets +2/+2 until end of turn.)').
card_mana_cost('ronin cavekeeper', ['5', 'R']).
card_cmc('ronin cavekeeper', 6).
card_layout('ronin cavekeeper', 'normal').
card_power('ronin cavekeeper', 4).
card_toughness('ronin cavekeeper', 3).

% Found in: BOK
card_name('ronin cliffrider', 'Ronin Cliffrider').
card_type('ronin cliffrider', 'Creature — Human Samurai').
card_types('ronin cliffrider', ['Creature']).
card_subtypes('ronin cliffrider', ['Human', 'Samurai']).
card_colors('ronin cliffrider', ['R']).
card_text('ronin cliffrider', 'Bushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)\nWhenever Ronin Cliffrider attacks, you may have it deal 1 damage to each creature defending player controls.').
card_mana_cost('ronin cliffrider', ['3', 'R', 'R']).
card_cmc('ronin cliffrider', 5).
card_layout('ronin cliffrider', 'normal').
card_power('ronin cliffrider', 2).
card_toughness('ronin cliffrider', 2).

% Found in: CHK
card_name('ronin houndmaster', 'Ronin Houndmaster').
card_type('ronin houndmaster', 'Creature — Human Samurai').
card_types('ronin houndmaster', ['Creature']).
card_subtypes('ronin houndmaster', ['Human', 'Samurai']).
card_colors('ronin houndmaster', ['R']).
card_text('ronin houndmaster', 'Haste\nBushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_mana_cost('ronin houndmaster', ['2', 'R']).
card_cmc('ronin houndmaster', 3).
card_layout('ronin houndmaster', 'normal').
card_power('ronin houndmaster', 2).
card_toughness('ronin houndmaster', 2).

% Found in: BOK
card_name('ronin warclub', 'Ronin Warclub').
card_type('ronin warclub', 'Artifact — Equipment').
card_types('ronin warclub', ['Artifact']).
card_subtypes('ronin warclub', ['Equipment']).
card_colors('ronin warclub', []).
card_text('ronin warclub', 'Equipped creature gets +2/+1.\nWhenever a creature enters the battlefield under your control, attach Ronin Warclub to that creature.\nEquip {5} ({5}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('ronin warclub', ['3']).
card_cmc('ronin warclub', 3).
card_layout('ronin warclub', 'normal').

% Found in: CSP
card_name('ronom hulk', 'Ronom Hulk').
card_type('ronom hulk', 'Creature — Beast').
card_types('ronom hulk', ['Creature']).
card_subtypes('ronom hulk', ['Beast']).
card_colors('ronom hulk', ['G']).
card_text('ronom hulk', 'Protection from snow\nCumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('ronom hulk', ['4', 'G']).
card_cmc('ronom hulk', 5).
card_layout('ronom hulk', 'normal').
card_power('ronom hulk', 5).
card_toughness('ronom hulk', 6).

% Found in: CSP
card_name('ronom serpent', 'Ronom Serpent').
card_type('ronom serpent', 'Snow Creature — Serpent').
card_types('ronom serpent', ['Creature']).
card_subtypes('ronom serpent', ['Serpent']).
card_supertypes('ronom serpent', ['Snow']).
card_colors('ronom serpent', ['U']).
card_text('ronom serpent', 'Ronom Serpent can\'t attack unless defending player controls a snow land.\nWhen you control no snow lands, sacrifice Ronom Serpent.').
card_mana_cost('ronom serpent', ['5', 'U']).
card_cmc('ronom serpent', 6).
card_layout('ronom serpent', 'normal').
card_power('ronom serpent', 5).
card_toughness('ronom serpent', 6).

% Found in: CSP
card_name('ronom unicorn', 'Ronom Unicorn').
card_type('ronom unicorn', 'Creature — Unicorn').
card_types('ronom unicorn', ['Creature']).
card_subtypes('ronom unicorn', ['Unicorn']).
card_colors('ronom unicorn', ['W']).
card_text('ronom unicorn', 'Sacrifice Ronom Unicorn: Destroy target enchantment.').
card_mana_cost('ronom unicorn', ['1', 'W']).
card_cmc('ronom unicorn', 2).
card_layout('ronom unicorn', 'normal').
card_power('ronom unicorn', 2).
card_toughness('ronom unicorn', 2).

% Found in: RAV
card_name('roofstalker wight', 'Roofstalker Wight').
card_type('roofstalker wight', 'Creature — Zombie').
card_types('roofstalker wight', ['Creature']).
card_subtypes('roofstalker wight', ['Zombie']).
card_colors('roofstalker wight', ['B']).
card_text('roofstalker wight', '{1}{U}: Roofstalker Wight gains flying until end of turn.').
card_mana_cost('roofstalker wight', ['1', 'B']).
card_cmc('roofstalker wight', 2).
card_layout('roofstalker wight', 'normal').
card_power('roofstalker wight', 2).
card_toughness('roofstalker wight', 1).

% Found in: ISD
card_name('rooftop storm', 'Rooftop Storm').
card_type('rooftop storm', 'Enchantment').
card_types('rooftop storm', ['Enchantment']).
card_subtypes('rooftop storm', []).
card_colors('rooftop storm', ['U']).
card_text('rooftop storm', 'You may pay {0} rather than pay the mana cost for Zombie creature spells you cast.').
card_mana_cost('rooftop storm', ['5', 'U']).
card_cmc('rooftop storm', 6).
card_layout('rooftop storm', 'normal').

% Found in: C13
card_name('roon of the hidden realm', 'Roon of the Hidden Realm').
card_type('roon of the hidden realm', 'Legendary Creature — Rhino Soldier').
card_types('roon of the hidden realm', ['Creature']).
card_subtypes('roon of the hidden realm', ['Rhino', 'Soldier']).
card_supertypes('roon of the hidden realm', ['Legendary']).
card_colors('roon of the hidden realm', ['W', 'U', 'G']).
card_text('roon of the hidden realm', 'Vigilance, trample\n{2}, {T}: Exile another target creature. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_mana_cost('roon of the hidden realm', ['2', 'G', 'W', 'U']).
card_cmc('roon of the hidden realm', 5).
card_layout('roon of the hidden realm', 'normal').
card_power('roon of the hidden realm', 4).
card_toughness('roon of the hidden realm', 4).

% Found in: PCY
card_name('root cage', 'Root Cage').
card_type('root cage', 'Enchantment').
card_types('root cage', ['Enchantment']).
card_subtypes('root cage', []).
card_colors('root cage', ['G']).
card_text('root cage', 'Mercenaries don\'t untap during their controllers\' untap steps.').
card_mana_cost('root cage', ['1', 'G']).
card_cmc('root cage', 2).
card_layout('root cage', 'normal').

% Found in: SCG
card_name('root elemental', 'Root Elemental').
card_type('root elemental', 'Creature — Elemental').
card_types('root elemental', ['Creature']).
card_subtypes('root elemental', ['Elemental']).
card_colors('root elemental', ['G']).
card_text('root elemental', 'Morph {5}{G}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Root Elemental is turned face up, you may put a creature card from your hand onto the battlefield.').
card_mana_cost('root elemental', ['4', 'G', 'G']).
card_cmc('root elemental', 6).
card_layout('root elemental', 'normal').
card_power('root elemental', 6).
card_toughness('root elemental', 5).

% Found in: PLS
card_name('root greevil', 'Root Greevil').
card_type('root greevil', 'Creature — Beast').
card_types('root greevil', ['Creature']).
card_subtypes('root greevil', ['Beast']).
card_colors('root greevil', ['G']).
card_text('root greevil', '{2}{G}, {T}, Sacrifice Root Greevil: Destroy all enchantments of the color of your choice.').
card_mana_cost('root greevil', ['3', 'G']).
card_cmc('root greevil', 4).
card_layout('root greevil', 'normal').
card_power('root greevil', 2).
card_toughness('root greevil', 3).

% Found in: 10E, TMP
card_name('root maze', 'Root Maze').
card_type('root maze', 'Enchantment').
card_types('root maze', ['Enchantment']).
card_subtypes('root maze', []).
card_colors('root maze', ['G']).
card_text('root maze', 'Artifacts and lands enter the battlefield tapped.').
card_mana_cost('root maze', ['G']).
card_cmc('root maze', 1).
card_layout('root maze', 'normal').

% Found in: LGN
card_name('root sliver', 'Root Sliver').
card_type('root sliver', 'Creature — Sliver').
card_types('root sliver', ['Creature']).
card_subtypes('root sliver', ['Sliver']).
card_colors('root sliver', ['G']).
card_text('root sliver', 'Root Sliver can\'t be countered.\nSliver spells can\'t be countered by spells or abilities.').
card_mana_cost('root sliver', ['3', 'G']).
card_cmc('root sliver', 4).
card_layout('root sliver', 'normal').
card_power('root sliver', 2).
card_toughness('root sliver', 2).

% Found in: HML
card_name('root spider', 'Root Spider').
card_type('root spider', 'Creature — Spider').
card_types('root spider', ['Creature']).
card_subtypes('root spider', ['Spider']).
card_colors('root spider', ['G']).
card_text('root spider', 'Whenever Root Spider blocks, it gets +1/+0 and gains first strike until end of turn.').
card_mana_cost('root spider', ['3', 'G']).
card_cmc('root spider', 4).
card_layout('root spider', 'normal').
card_power('root spider', 2).
card_toughness('root spider', 2).

% Found in: MM2, RAV
card_name('root-kin ally', 'Root-Kin Ally').
card_type('root-kin ally', 'Creature — Elemental Warrior').
card_types('root-kin ally', ['Creature']).
card_subtypes('root-kin ally', ['Elemental', 'Warrior']).
card_colors('root-kin ally', ['G']).
card_text('root-kin ally', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nTap two untapped creatures you control: Root-Kin Ally gets +2/+2 until end of turn.').
card_mana_cost('root-kin ally', ['4', 'G', 'G']).
card_cmc('root-kin ally', 6).
card_layout('root-kin ally', 'normal').
card_power('root-kin ally', 3).
card_toughness('root-kin ally', 3).

% Found in: RTR
card_name('rootborn defenses', 'Rootborn Defenses').
card_type('rootborn defenses', 'Instant').
card_types('rootborn defenses', ['Instant']).
card_subtypes('rootborn defenses', []).
card_colors('rootborn defenses', ['W']).
card_text('rootborn defenses', 'Populate. Creatures you control gain indestructible until end of turn. (To populate, put a token onto the battlefield that\'s a copy of a creature token you control. Damage and effects that say \"destroy\" don\'t destroy creatures with indestructible.)').
card_mana_cost('rootborn defenses', ['2', 'W']).
card_cmc('rootborn defenses', 3).
card_layout('rootborn defenses', 'normal').

% Found in: H09, M10, M11, M12, M13
card_name('rootbound crag', 'Rootbound Crag').
card_type('rootbound crag', 'Land').
card_types('rootbound crag', ['Land']).
card_subtypes('rootbound crag', []).
card_colors('rootbound crag', []).
card_text('rootbound crag', 'Rootbound Crag enters the battlefield tapped unless you control a Mountain or a Forest.\n{T}: Add {R} or {G} to your mana pool.').
card_layout('rootbound crag', 'normal').

% Found in: 9ED, TMP, TPR
card_name('rootbreaker wurm', 'Rootbreaker Wurm').
card_type('rootbreaker wurm', 'Creature — Wurm').
card_types('rootbreaker wurm', ['Creature']).
card_subtypes('rootbreaker wurm', ['Wurm']).
card_colors('rootbreaker wurm', ['G']).
card_text('rootbreaker wurm', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('rootbreaker wurm', ['5', 'G', 'G']).
card_cmc('rootbreaker wurm', 7).
card_layout('rootbreaker wurm', 'normal').
card_power('rootbreaker wurm', 6).
card_toughness('rootbreaker wurm', 6).

% Found in: LRW
card_name('rootgrapple', 'Rootgrapple').
card_type('rootgrapple', 'Tribal Instant — Treefolk').
card_types('rootgrapple', ['Tribal', 'Instant']).
card_subtypes('rootgrapple', ['Treefolk']).
card_colors('rootgrapple', ['G']).
card_text('rootgrapple', 'Destroy target noncreature permanent. If you control a Treefolk, draw a card.').
card_mana_cost('rootgrapple', ['4', 'G']).
card_cmc('rootgrapple', 5).
card_layout('rootgrapple', 'normal').

% Found in: INV
card_name('rooting kavu', 'Rooting Kavu').
card_type('rooting kavu', 'Creature — Kavu').
card_types('rooting kavu', ['Creature']).
card_subtypes('rooting kavu', ['Kavu']).
card_colors('rooting kavu', ['G']).
card_text('rooting kavu', 'When Rooting Kavu dies, you may exile it. If you do, shuffle all creature cards from your graveyard into your library.').
card_mana_cost('rooting kavu', ['2', 'G', 'G']).
card_cmc('rooting kavu', 4).
card_layout('rooting kavu', 'normal').
card_power('rooting kavu', 4).
card_toughness('rooting kavu', 3).

% Found in: CHK
card_name('rootrunner', 'Rootrunner').
card_type('rootrunner', 'Creature — Spirit').
card_types('rootrunner', ['Creature']).
card_subtypes('rootrunner', ['Spirit']).
card_colors('rootrunner', ['G']).
card_text('rootrunner', '{G}{G}, Sacrifice Rootrunner: Put target land on top of its owner\'s library.\nSoulshift 3 (When this creature dies, you may return target Spirit card with converted mana cost 3 or less from your graveyard to your hand.)').
card_mana_cost('rootrunner', ['2', 'G', 'G']).
card_cmc('rootrunner', 4).
card_layout('rootrunner', 'normal').
card_power('rootrunner', 3).
card_toughness('rootrunner', 3).

% Found in: HML, MED
card_name('roots', 'Roots').
card_type('roots', 'Enchantment — Aura').
card_types('roots', ['Enchantment']).
card_subtypes('roots', ['Aura']).
card_colors('roots', ['G']).
card_text('roots', 'Enchant creature without flying\nWhen Roots enters the battlefield, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_mana_cost('roots', ['3', 'G']).
card_cmc('roots', 4).
card_layout('roots', 'normal').

% Found in: ARC
card_name('roots of all evil', 'Roots of All Evil').
card_type('roots of all evil', 'Scheme').
card_types('roots of all evil', ['Scheme']).
card_subtypes('roots of all evil', []).
card_colors('roots of all evil', []).
card_text('roots of all evil', 'When you set this scheme in motion, put five 1/1 green Saproling creature tokens onto the battlefield.').
card_layout('roots of all evil', 'scheme').

% Found in: MIR
card_name('roots of life', 'Roots of Life').
card_type('roots of life', 'Enchantment').
card_types('roots of life', ['Enchantment']).
card_subtypes('roots of life', []).
card_colors('roots of life', ['G']).
card_text('roots of life', 'As Roots of Life enters the battlefield, choose Island or Swamp.\nWhenever a land of the chosen type an opponent controls becomes tapped, you gain 1 life.').
card_mana_cost('roots of life', ['1', 'G', 'G']).
card_cmc('roots of life', 3).
card_layout('roots of life', 'normal').

% Found in: 10E, 9ED, M14, TMP, TPR
card_name('rootwalla', 'Rootwalla').
card_type('rootwalla', 'Creature — Lizard').
card_types('rootwalla', ['Creature']).
card_subtypes('rootwalla', ['Lizard']).
card_colors('rootwalla', ['G']).
card_text('rootwalla', '{1}{G}: Rootwalla gets +2/+2 until end of turn. Activate this ability only once each turn.').
card_mana_cost('rootwalla', ['2', 'G']).
card_cmc('rootwalla', 3).
card_layout('rootwalla', 'normal').
card_power('rootwalla', 2).
card_toughness('rootwalla', 2).

% Found in: EXO
card_name('rootwater alligator', 'Rootwater Alligator').
card_type('rootwater alligator', 'Creature — Crocodile').
card_types('rootwater alligator', ['Creature']).
card_subtypes('rootwater alligator', ['Crocodile']).
card_colors('rootwater alligator', ['G']).
card_text('rootwater alligator', 'Sacrifice a Forest: Regenerate Rootwater Alligator.').
card_mana_cost('rootwater alligator', ['3', 'G']).
card_cmc('rootwater alligator', 4).
card_layout('rootwater alligator', 'normal').
card_power('rootwater alligator', 3).
card_toughness('rootwater alligator', 2).

% Found in: 10E, NMS
card_name('rootwater commando', 'Rootwater Commando').
card_type('rootwater commando', 'Creature — Merfolk').
card_types('rootwater commando', ['Creature']).
card_subtypes('rootwater commando', ['Merfolk']).
card_colors('rootwater commando', ['U']).
card_text('rootwater commando', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('rootwater commando', ['2', 'U']).
card_cmc('rootwater commando', 3).
card_layout('rootwater commando', 'normal').
card_power('rootwater commando', 2).
card_toughness('rootwater commando', 2).

% Found in: TMP, TPR
card_name('rootwater depths', 'Rootwater Depths').
card_type('rootwater depths', 'Land').
card_types('rootwater depths', ['Land']).
card_subtypes('rootwater depths', []).
card_colors('rootwater depths', []).
card_text('rootwater depths', '{T}: Add {1} to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Rootwater Depths doesn\'t untap during your next untap step.').
card_layout('rootwater depths', 'normal').

% Found in: TMP
card_name('rootwater diver', 'Rootwater Diver').
card_type('rootwater diver', 'Creature — Merfolk').
card_types('rootwater diver', ['Creature']).
card_subtypes('rootwater diver', ['Merfolk']).
card_colors('rootwater diver', ['U']).
card_text('rootwater diver', '{T}, Sacrifice Rootwater Diver: Return target artifact card from your graveyard to your hand.').
card_mana_cost('rootwater diver', ['U']).
card_cmc('rootwater diver', 1).
card_layout('rootwater diver', 'normal').
card_power('rootwater diver', 1).
card_toughness('rootwater diver', 1).

% Found in: TMP, TPR
card_name('rootwater hunter', 'Rootwater Hunter').
card_type('rootwater hunter', 'Creature — Merfolk').
card_types('rootwater hunter', ['Creature']).
card_subtypes('rootwater hunter', ['Merfolk']).
card_colors('rootwater hunter', ['U']).
card_text('rootwater hunter', '{T}: Rootwater Hunter deals 1 damage to target creature or player.').
card_mana_cost('rootwater hunter', ['2', 'U']).
card_cmc('rootwater hunter', 3).
card_layout('rootwater hunter', 'normal').
card_power('rootwater hunter', 1).
card_toughness('rootwater hunter', 1).

% Found in: 10E, TMP
card_name('rootwater matriarch', 'Rootwater Matriarch').
card_type('rootwater matriarch', 'Creature — Merfolk').
card_types('rootwater matriarch', ['Creature']).
card_subtypes('rootwater matriarch', ['Merfolk']).
card_colors('rootwater matriarch', ['U']).
card_text('rootwater matriarch', '{T}: Gain control of target creature for as long as that creature is enchanted.').
card_mana_cost('rootwater matriarch', ['2', 'U', 'U']).
card_cmc('rootwater matriarch', 4).
card_layout('rootwater matriarch', 'normal').
card_power('rootwater matriarch', 2).
card_toughness('rootwater matriarch', 3).

% Found in: EXO
card_name('rootwater mystic', 'Rootwater Mystic').
card_type('rootwater mystic', 'Creature — Merfolk Wizard').
card_types('rootwater mystic', ['Creature']).
card_subtypes('rootwater mystic', ['Merfolk', 'Wizard']).
card_colors('rootwater mystic', ['U']).
card_text('rootwater mystic', '{1}{U}: Look at the top card of target player\'s library.').
card_mana_cost('rootwater mystic', ['U']).
card_cmc('rootwater mystic', 1).
card_layout('rootwater mystic', 'normal').
card_power('rootwater mystic', 1).
card_toughness('rootwater mystic', 1).

% Found in: TMP
card_name('rootwater shaman', 'Rootwater Shaman').
card_type('rootwater shaman', 'Creature — Merfolk Shaman').
card_types('rootwater shaman', ['Creature']).
card_subtypes('rootwater shaman', ['Merfolk', 'Shaman']).
card_colors('rootwater shaman', ['U']).
card_text('rootwater shaman', 'You may cast Aura cards with enchant creature as though they had flash.').
card_mana_cost('rootwater shaman', ['2', 'U']).
card_cmc('rootwater shaman', 3).
card_layout('rootwater shaman', 'normal').
card_power('rootwater shaman', 2).
card_toughness('rootwater shaman', 2).

% Found in: NMS
card_name('rootwater thief', 'Rootwater Thief').
card_type('rootwater thief', 'Creature — Merfolk Rogue').
card_types('rootwater thief', ['Creature']).
card_subtypes('rootwater thief', ['Merfolk', 'Rogue']).
card_colors('rootwater thief', ['U']).
card_text('rootwater thief', '{U}: Rootwater Thief gains flying until end of turn.\nWhenever Rootwater Thief deals combat damage to a player, you may pay {2}. If you do, search that player\'s library for a card and exile it, then the player shuffles his or her library.').
card_mana_cost('rootwater thief', ['1', 'U']).
card_cmc('rootwater thief', 2).
card_layout('rootwater thief', 'normal').
card_power('rootwater thief', 1).
card_toughness('rootwater thief', 2).

% Found in: HOP, ONS, VMA
card_name('rorix bladewing', 'Rorix Bladewing').
card_type('rorix bladewing', 'Legendary Creature — Dragon').
card_types('rorix bladewing', ['Creature']).
card_subtypes('rorix bladewing', ['Dragon']).
card_supertypes('rorix bladewing', ['Legendary']).
card_colors('rorix bladewing', ['R']).
card_text('rorix bladewing', 'Flying, haste').
card_mana_cost('rorix bladewing', ['3', 'R', 'R', 'R']).
card_cmc('rorix bladewing', 6).
card_layout('rorix bladewing', 'normal').
card_power('rorix bladewing', 6).
card_toughness('rorix bladewing', 5).

% Found in: SHM
card_name('rosheen meanderer', 'Rosheen Meanderer').
card_type('rosheen meanderer', 'Legendary Creature — Giant Shaman').
card_types('rosheen meanderer', ['Creature']).
card_subtypes('rosheen meanderer', ['Giant', 'Shaman']).
card_supertypes('rosheen meanderer', ['Legendary']).
card_colors('rosheen meanderer', ['R', 'G']).
card_text('rosheen meanderer', '{T}: Add {4} to your mana pool. Spend this mana only on costs that contain {X}.').
card_mana_cost('rosheen meanderer', ['3', 'R/G']).
card_cmc('rosheen meanderer', 4).
card_layout('rosheen meanderer', 'normal').
card_power('rosheen meanderer', 4).
card_toughness('rosheen meanderer', 4).

% Found in: DGM
card_name('rot farm skeleton', 'Rot Farm Skeleton').
card_type('rot farm skeleton', 'Creature — Plant Skeleton').
card_types('rot farm skeleton', ['Creature']).
card_subtypes('rot farm skeleton', ['Plant', 'Skeleton']).
card_colors('rot farm skeleton', ['B', 'G']).
card_text('rot farm skeleton', 'Rot Farm Skeleton can\'t block.\n{2}{B}{G}, Put the top four cards of your library into your graveyard: Return Rot Farm Skeleton from your graveyard to the battlefield. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('rot farm skeleton', ['2', 'B', 'G']).
card_cmc('rot farm skeleton', 4).
card_layout('rot farm skeleton', 'normal').
card_power('rot farm skeleton', 4).
card_toughness('rot farm skeleton', 1).

% Found in: BFZ
card_name('rot shambler', 'Rot Shambler').
card_type('rot shambler', 'Creature — Fungus').
card_types('rot shambler', ['Creature']).
card_subtypes('rot shambler', ['Fungus']).
card_colors('rot shambler', ['G']).
card_text('rot shambler', 'Whenever another creature you control dies, put a +1/+1 counter on Rot Shambler.').
card_mana_cost('rot shambler', ['1', 'G']).
card_cmc('rot shambler', 2).
card_layout('rot shambler', 'normal').
card_power('rot shambler', 1).
card_toughness('rot shambler', 1).

% Found in: MBS
card_name('rot wolf', 'Rot Wolf').
card_type('rot wolf', 'Creature — Wolf').
card_types('rot wolf', ['Creature']).
card_subtypes('rot wolf', ['Wolf']).
card_colors('rot wolf', ['G']).
card_text('rot wolf', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhenever a creature dealt damage by Rot Wolf this turn dies, you may draw a card.').
card_mana_cost('rot wolf', ['2', 'G']).
card_cmc('rot wolf', 3).
card_layout('rot wolf', 'normal').
card_power('rot wolf', 2).
card_toughness('rot wolf', 2).

% Found in: AVR
card_name('rotcrown ghoul', 'Rotcrown Ghoul').
card_type('rotcrown ghoul', 'Creature — Zombie').
card_types('rotcrown ghoul', ['Creature']).
card_subtypes('rotcrown ghoul', ['Zombie']).
card_colors('rotcrown ghoul', ['U']).
card_text('rotcrown ghoul', 'When Rotcrown Ghoul dies, target player puts the top five cards of his or her library into his or her graveyard.').
card_mana_cost('rotcrown ghoul', ['4', 'U']).
card_cmc('rotcrown ghoul', 5).
card_layout('rotcrown ghoul', 'normal').
card_power('rotcrown ghoul', 3).
card_toughness('rotcrown ghoul', 3).

% Found in: HML, ME2
card_name('roterothopter', 'Roterothopter').
card_type('roterothopter', 'Artifact Creature — Thopter').
card_types('roterothopter', ['Artifact', 'Creature']).
card_subtypes('roterothopter', ['Thopter']).
card_colors('roterothopter', []).
card_text('roterothopter', 'Flying\n{2}: Roterothopter gets +1/+0 until end of turn. Activate this ability no more than twice each turn.').
card_mana_cost('roterothopter', ['1']).
card_cmc('roterothopter', 1).
card_layout('roterothopter', 'normal').
card_power('roterothopter', 0).
card_toughness('roterothopter', 2).

% Found in: M15
card_name('rotfeaster maggot', 'Rotfeaster Maggot').
card_type('rotfeaster maggot', 'Creature — Insect').
card_types('rotfeaster maggot', ['Creature']).
card_subtypes('rotfeaster maggot', ['Insect']).
card_colors('rotfeaster maggot', ['B']).
card_text('rotfeaster maggot', 'When Rotfeaster Maggot enters the battlefield, exile target creature card from a graveyard. You gain life equal to that card\'s toughness.').
card_mana_cost('rotfeaster maggot', ['4', 'B']).
card_cmc('rotfeaster maggot', 5).
card_layout('rotfeaster maggot', 'normal').
card_power('rotfeaster maggot', 3).
card_toughness('rotfeaster maggot', 5).

% Found in: ONS
card_name('rotlung reanimator', 'Rotlung Reanimator').
card_type('rotlung reanimator', 'Creature — Zombie Cleric').
card_types('rotlung reanimator', ['Creature']).
card_subtypes('rotlung reanimator', ['Zombie', 'Cleric']).
card_colors('rotlung reanimator', ['B']).
card_text('rotlung reanimator', 'Whenever Rotlung Reanimator or another Cleric dies, put a 2/2 black Zombie creature token onto the battlefield.').
card_mana_cost('rotlung reanimator', ['2', 'B']).
card_cmc('rotlung reanimator', 3).
card_layout('rotlung reanimator', 'normal').
card_power('rotlung reanimator', 2).
card_toughness('rotlung reanimator', 2).

% Found in: JOU
card_name('rotted hulk', 'Rotted Hulk').
card_type('rotted hulk', 'Creature — Elemental').
card_types('rotted hulk', ['Creature']).
card_subtypes('rotted hulk', ['Elemental']).
card_colors('rotted hulk', ['B']).
card_text('rotted hulk', '').
card_mana_cost('rotted hulk', ['3', 'B']).
card_cmc('rotted hulk', 4).
card_layout('rotted hulk', 'normal').
card_power('rotted hulk', 2).
card_toughness('rotted hulk', 5).

% Found in: NPH
card_name('rotted hystrix', 'Rotted Hystrix').
card_type('rotted hystrix', 'Creature — Beast').
card_types('rotted hystrix', ['Creature']).
card_subtypes('rotted hystrix', ['Beast']).
card_colors('rotted hystrix', ['G']).
card_text('rotted hystrix', '').
card_mana_cost('rotted hystrix', ['4', 'G']).
card_cmc('rotted hystrix', 5).
card_layout('rotted hystrix', 'normal').
card_power('rotted hystrix', 3).
card_toughness('rotted hystrix', 6).

% Found in: ARC
card_name('rotted ones, lay siege', 'Rotted Ones, Lay Siege').
card_type('rotted ones, lay siege', 'Scheme').
card_types('rotted ones, lay siege', ['Scheme']).
card_subtypes('rotted ones, lay siege', []).
card_colors('rotted ones, lay siege', []).
card_text('rotted ones, lay siege', 'When you set this scheme in motion, for each opponent, put a 2/2 black Zombie creature token onto the battlefield that attacks that player each combat if able.').
card_layout('rotted ones, lay siege', 'scheme').

% Found in: ISD
card_name('rotting fensnake', 'Rotting Fensnake').
card_type('rotting fensnake', 'Creature — Zombie Snake').
card_types('rotting fensnake', ['Creature']).
card_subtypes('rotting fensnake', ['Zombie', 'Snake']).
card_colors('rotting fensnake', ['B']).
card_text('rotting fensnake', '').
card_mana_cost('rotting fensnake', ['3', 'B']).
card_cmc('rotting fensnake', 4).
card_layout('rotting fensnake', 'normal').
card_power('rotting fensnake', 5).
card_toughness('rotting fensnake', 1).

% Found in: ODY
card_name('rotting giant', 'Rotting Giant').
card_type('rotting giant', 'Creature — Zombie Giant').
card_types('rotting giant', ['Creature']).
card_subtypes('rotting giant', ['Zombie', 'Giant']).
card_colors('rotting giant', ['B']).
card_text('rotting giant', 'Whenever Rotting Giant attacks or blocks, sacrifice it unless you exile a card from your graveyard.').
card_mana_cost('rotting giant', ['1', 'B']).
card_cmc('rotting giant', 2).
card_layout('rotting giant', 'normal').
card_power('rotting giant', 3).
card_toughness('rotting giant', 3).

% Found in: M11
card_name('rotting legion', 'Rotting Legion').
card_type('rotting legion', 'Creature — Zombie').
card_types('rotting legion', ['Creature']).
card_subtypes('rotting legion', ['Zombie']).
card_colors('rotting legion', ['B']).
card_text('rotting legion', 'Rotting Legion enters the battlefield tapped.').
card_mana_cost('rotting legion', ['4', 'B']).
card_cmc('rotting legion', 5).
card_layout('rotting legion', 'normal').
card_power('rotting legion', 4).
card_toughness('rotting legion', 5).

% Found in: KTK
card_name('rotting mastodon', 'Rotting Mastodon').
card_type('rotting mastodon', 'Creature — Zombie Elephant').
card_types('rotting mastodon', ['Creature']).
card_subtypes('rotting mastodon', ['Zombie', 'Elephant']).
card_colors('rotting mastodon', ['B']).
card_text('rotting mastodon', '').
card_mana_cost('rotting mastodon', ['4', 'B']).
card_cmc('rotting mastodon', 5).
card_layout('rotting mastodon', 'normal').
card_power('rotting mastodon', 2).
card_toughness('rotting mastodon', 8).

% Found in: CON, HOP
card_name('rotting rats', 'Rotting Rats').
card_type('rotting rats', 'Creature — Zombie Rat').
card_types('rotting rats', ['Creature']).
card_subtypes('rotting rats', ['Zombie', 'Rat']).
card_colors('rotting rats', ['B']).
card_text('rotting rats', 'When Rotting Rats enters the battlefield, each player discards a card.\nUnearth {1}{B} ({1}{B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('rotting rats', ['1', 'B']).
card_cmc('rotting rats', 2).
card_layout('rotting rats', 'normal').
card_power('rotting rats', 1).
card_toughness('rotting rats', 1).

% Found in: C13, PLC
card_name('rough', 'Rough').
card_type('rough', 'Sorcery').
card_types('rough', ['Sorcery']).
card_subtypes('rough', []).
card_colors('rough', ['R']).
card_text('rough', 'Rough deals 2 damage to each creature without flying.').
card_mana_cost('rough', ['1', 'R']).
card_cmc('rough', 2).
card_layout('rough', 'split').
card_sides('rough', 'tumble').

% Found in: DPA, SHM
card_name('roughshod mentor', 'Roughshod Mentor').
card_type('roughshod mentor', 'Creature — Giant Warrior').
card_types('roughshod mentor', ['Creature']).
card_subtypes('roughshod mentor', ['Giant', 'Warrior']).
card_colors('roughshod mentor', ['G']).
card_text('roughshod mentor', 'Green creatures you control have trample.').
card_mana_cost('roughshod mentor', ['5', 'G']).
card_cmc('roughshod mentor', 6).
card_layout('roughshod mentor', 'normal').
card_power('roughshod mentor', 5).
card_toughness('roughshod mentor', 4).

% Found in: MMQ
card_name('rouse', 'Rouse').
card_type('rouse', 'Instant').
card_types('rouse', ['Instant']).
card_subtypes('rouse', []).
card_colors('rouse', ['B']).
card_text('rouse', 'If you control a Swamp, you may pay 2 life rather than pay Rouse\'s mana cost.\nTarget creature gets +2/+0 until end of turn.').
card_mana_cost('rouse', ['1', 'B']).
card_cmc('rouse', 2).
card_layout('rouse', 'normal').

% Found in: JOU
card_name('rouse the mob', 'Rouse the Mob').
card_type('rouse the mob', 'Instant').
card_types('rouse the mob', ['Instant']).
card_subtypes('rouse the mob', []).
card_colors('rouse the mob', ['R']).
card_text('rouse the mob', 'Strive — Rouse the Mob costs {2}{R} more to cast for each target beyond the first.\nAny number of target creatures each get +2/+0 and gain trample until end of turn.').
card_mana_cost('rouse the mob', ['R']).
card_cmc('rouse the mob', 1).
card_layout('rouse the mob', 'normal').

% Found in: CNS
card_name('rousing of souls', 'Rousing of Souls').
card_type('rousing of souls', 'Sorcery').
card_types('rousing of souls', ['Sorcery']).
card_subtypes('rousing of souls', []).
card_colors('rousing of souls', ['W']).
card_text('rousing of souls', 'Parley — Each player reveals the top card of his or her library. For each nonland card revealed this way, you put a 1/1 white Spirit creature token with flying onto the battlefield. Then each player draws a card.').
card_mana_cost('rousing of souls', ['2', 'W']).
card_cmc('rousing of souls', 3).
card_layout('rousing of souls', 'normal').

% Found in: CNS, INV
card_name('rout', 'Rout').
card_type('rout', 'Sorcery').
card_types('rout', ['Sorcery']).
card_subtypes('rout', []).
card_colors('rout', ['W']).
card_text('rout', 'You may cast Rout as though it had flash if you pay {2} more to cast it. (You may cast it any time you could cast an instant.)\nDestroy all creatures. They can\'t be regenerated.').
card_mana_cost('rout', ['3', 'W', 'W']).
card_cmc('rout', 5).
card_layout('rout', 'normal').

% Found in: POR
card_name('rowan treefolk', 'Rowan Treefolk').
card_type('rowan treefolk', 'Creature — Treefolk').
card_types('rowan treefolk', ['Creature']).
card_subtypes('rowan treefolk', ['Treefolk']).
card_colors('rowan treefolk', ['G']).
card_text('rowan treefolk', '').
card_mana_cost('rowan treefolk', ['3', 'G']).
card_cmc('rowan treefolk', 4).
card_layout('rowan treefolk', 'normal').
card_power('rowan treefolk', 3).
card_toughness('rowan treefolk', 4).

% Found in: 6ED, 7ED, VIS
card_name('rowen', 'Rowen').
card_type('rowen', 'Enchantment').
card_types('rowen', ['Enchantment']).
card_subtypes('rowen', []).
card_colors('rowen', ['G']).
card_text('rowen', 'Reveal the first card you draw each turn. Whenever you reveal a basic land card this way, draw a card.').
card_mana_cost('rowen', ['2', 'G', 'G']).
card_cmc('rowen', 4).
card_layout('rowen', 'normal').

% Found in: 10E, 2ED, 3ED, 4ED, 8ED, 9ED, CED, CEI, LEA, LEB, M10, M11, M12, pSUS
card_name('royal assassin', 'Royal Assassin').
card_type('royal assassin', 'Creature — Human Assassin').
card_types('royal assassin', ['Creature']).
card_subtypes('royal assassin', ['Human', 'Assassin']).
card_colors('royal assassin', ['B']).
card_text('royal assassin', '{T}: Destroy target tapped creature.').
card_mana_cost('royal assassin', ['1', 'B', 'B']).
card_cmc('royal assassin', 3).
card_layout('royal assassin', 'normal').
card_power('royal assassin', 1).
card_toughness('royal assassin', 1).

% Found in: VAN
card_name('royal assassin avatar', 'Royal Assassin Avatar').
card_type('royal assassin avatar', 'Vanguard').
card_types('royal assassin avatar', ['Vanguard']).
card_subtypes('royal assassin avatar', []).
card_colors('royal assassin avatar', []).
card_text('royal assassin avatar', 'At the beginning of your upkeep, you draw a card and you lose 1 life.').
card_layout('royal assassin avatar', 'vanguard').

% Found in: ALL, ME2
card_name('royal decree', 'Royal Decree').
card_type('royal decree', 'Enchantment').
card_types('royal decree', ['Enchantment']).
card_subtypes('royal decree', []).
card_colors('royal decree', ['W']).
card_text('royal decree', 'Cumulative upkeep {W} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhenever a Swamp, Mountain, black permanent, or red permanent becomes tapped, Royal Decree deals 1 damage to that permanent\'s controller.').
card_mana_cost('royal decree', ['2', 'W', 'W']).
card_cmc('royal decree', 4).
card_layout('royal decree', 'normal').
card_reserved('royal decree').

% Found in: S00, S99
card_name('royal falcon', 'Royal Falcon').
card_type('royal falcon', 'Creature — Bird').
card_types('royal falcon', ['Creature']).
card_subtypes('royal falcon', ['Bird']).
card_colors('royal falcon', ['W']).
card_text('royal falcon', 'Flying').
card_mana_cost('royal falcon', ['1', 'W']).
card_cmc('royal falcon', 2).
card_layout('royal falcon', 'normal').
card_power('royal falcon', 1).
card_toughness('royal falcon', 1).

% Found in: ALL
card_name('royal herbalist', 'Royal Herbalist').
card_type('royal herbalist', 'Creature — Human Cleric').
card_types('royal herbalist', ['Creature']).
card_subtypes('royal herbalist', ['Human', 'Cleric']).
card_colors('royal herbalist', ['W']).
card_text('royal herbalist', '{2}, Exile the top card of your library: You gain 1 life.').
card_mana_cost('royal herbalist', ['W']).
card_cmc('royal herbalist', 1).
card_layout('royal herbalist', 'normal').
card_power('royal herbalist', 1).
card_toughness('royal herbalist', 1).

% Found in: ME2, S99
card_name('royal trooper', 'Royal Trooper').
card_type('royal trooper', 'Creature — Human Soldier').
card_types('royal trooper', ['Creature']).
card_subtypes('royal trooper', ['Human', 'Soldier']).
card_colors('royal trooper', ['W']).
card_text('royal trooper', 'Whenever Royal Trooper blocks, it gets +2/+2 until end of turn.').
card_mana_cost('royal trooper', ['2', 'W']).
card_cmc('royal trooper', 3).
card_layout('royal trooper', 'normal').
card_power('royal trooper', 2).
card_toughness('royal trooper', 2).

% Found in: RTR
card_name('rubbleback rhino', 'Rubbleback Rhino').
card_type('rubbleback rhino', 'Creature — Rhino').
card_types('rubbleback rhino', ['Creature']).
card_subtypes('rubbleback rhino', ['Rhino']).
card_colors('rubbleback rhino', ['G']).
card_text('rubbleback rhino', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_mana_cost('rubbleback rhino', ['4', 'G']).
card_cmc('rubbleback rhino', 5).
card_layout('rubbleback rhino', 'normal').
card_power('rubbleback rhino', 3).
card_toughness('rubbleback rhino', 4).

% Found in: DGM
card_name('rubblebelt maaka', 'Rubblebelt Maaka').
card_type('rubblebelt maaka', 'Creature — Cat').
card_types('rubblebelt maaka', ['Creature']).
card_subtypes('rubblebelt maaka', ['Cat']).
card_colors('rubblebelt maaka', ['R']).
card_text('rubblebelt maaka', 'Bloodrush — {R}, Discard Rubblebelt Maaka: Target attacking creature gets +3/+3 until end of turn.').
card_mana_cost('rubblebelt maaka', ['3', 'R']).
card_cmc('rubblebelt maaka', 4).
card_layout('rubblebelt maaka', 'normal').
card_power('rubblebelt maaka', 3).
card_toughness('rubblebelt maaka', 3).

% Found in: GTC
card_name('rubblebelt raiders', 'Rubblebelt Raiders').
card_type('rubblebelt raiders', 'Creature — Human Warrior').
card_types('rubblebelt raiders', ['Creature']).
card_subtypes('rubblebelt raiders', ['Human', 'Warrior']).
card_colors('rubblebelt raiders', ['R', 'G']).
card_text('rubblebelt raiders', 'Whenever Rubblebelt Raiders attacks, put a +1/+1 counter on it for each attacking creature you control.').
card_mana_cost('rubblebelt raiders', ['1', 'R/G', 'R/G', 'R/G']).
card_cmc('rubblebelt raiders', 4).
card_layout('rubblebelt raiders', 'normal').
card_power('rubblebelt raiders', 3).
card_toughness('rubblebelt raiders', 3).

% Found in: GTC, pPRE
card_name('rubblehulk', 'Rubblehulk').
card_type('rubblehulk', 'Creature — Elemental').
card_types('rubblehulk', ['Creature']).
card_subtypes('rubblehulk', ['Elemental']).
card_colors('rubblehulk', ['R', 'G']).
card_text('rubblehulk', 'Rubblehulk\'s power and toughness are each equal to the number of lands you control.\nBloodrush — {1}{R}{G}, Discard Rubblehulk: Target attacking creature gets +X/+X until end of turn, where X is the number of lands you control.').
card_mana_cost('rubblehulk', ['4', 'R', 'G']).
card_cmc('rubblehulk', 6).
card_layout('rubblehulk', 'normal').
card_power('rubblehulk', '*').
card_toughness('rubblehulk', '*').

% Found in: C13, CHR, LEG, ME3
card_name('rubinia soulsinger', 'Rubinia Soulsinger').
card_type('rubinia soulsinger', 'Legendary Creature — Faerie').
card_types('rubinia soulsinger', ['Creature']).
card_subtypes('rubinia soulsinger', ['Faerie']).
card_supertypes('rubinia soulsinger', ['Legendary']).
card_colors('rubinia soulsinger', ['W', 'U', 'G']).
card_text('rubinia soulsinger', 'You may choose not to untap Rubinia Soulsinger during your untap step.\n{T}: Gain control of target creature for as long as you control Rubinia and Rubinia remains tapped.').
card_mana_cost('rubinia soulsinger', ['2', 'G', 'W', 'U']).
card_cmc('rubinia soulsinger', 5).
card_layout('rubinia soulsinger', 'normal').
card_power('rubinia soulsinger', 2).
card_toughness('rubinia soulsinger', 3).

% Found in: INV
card_name('ruby leech', 'Ruby Leech').
card_type('ruby leech', 'Creature — Leech').
card_types('ruby leech', ['Creature']).
card_subtypes('ruby leech', ['Leech']).
card_colors('ruby leech', ['R']).
card_text('ruby leech', 'First strike\nRed spells you cast cost {R} more to cast.').
card_mana_cost('ruby leech', ['1', 'R']).
card_cmc('ruby leech', 2).
card_layout('ruby leech', 'normal').
card_power('ruby leech', 2).
card_toughness('ruby leech', 2).

% Found in: C14, TMP
card_name('ruby medallion', 'Ruby Medallion').
card_type('ruby medallion', 'Artifact').
card_types('ruby medallion', ['Artifact']).
card_subtypes('ruby medallion', []).
card_colors('ruby medallion', []).
card_text('ruby medallion', 'Red spells you cast cost {1} less to cast.').
card_mana_cost('ruby medallion', ['2']).
card_cmc('ruby medallion', 2).
card_layout('ruby medallion', 'normal').

% Found in: 5DN, DD3_GVL, DDD, MMA
card_name('rude awakening', 'Rude Awakening').
card_type('rude awakening', 'Sorcery').
card_types('rude awakening', ['Sorcery']).
card_subtypes('rude awakening', []).
card_colors('rude awakening', ['G']).
card_text('rude awakening', 'Choose one —\n• Untap all lands you control.\n• Until end of turn, lands you control become 2/2 creatures that are still lands.\nEntwine {2}{G} (Choose both if you pay the entwine cost.)').
card_mana_cost('rude awakening', ['4', 'G']).
card_cmc('rude awakening', 5).
card_layout('rude awakening', 'normal').

% Found in: FRF, KTK
card_name('rugged highlands', 'Rugged Highlands').
card_type('rugged highlands', 'Land').
card_types('rugged highlands', ['Land']).
card_subtypes('rugged highlands', []).
card_colors('rugged highlands', []).
card_text('rugged highlands', 'Rugged Highlands enters the battlefield tapped.\nWhen Rugged Highlands enters the battlefield, you gain 1 life.\n{T}: Add {R} or {G} to your mana pool.').
card_layout('rugged highlands', 'normal').

% Found in: EVE
card_name('rugged prairie', 'Rugged Prairie').
card_type('rugged prairie', 'Land').
card_types('rugged prairie', ['Land']).
card_subtypes('rugged prairie', []).
card_colors('rugged prairie', []).
card_text('rugged prairie', '{T}: Add {1} to your mana pool.\n{R/W}, {T}: Add {R}{R}, {R}{W}, or {W}{W} to your mana pool.').
card_layout('rugged prairie', 'normal').

% Found in: INV
card_name('ruham djinn', 'Ruham Djinn').
card_type('ruham djinn', 'Creature — Djinn').
card_types('ruham djinn', ['Creature']).
card_subtypes('ruham djinn', ['Djinn']).
card_colors('ruham djinn', ['W']).
card_text('ruham djinn', 'First strike\nRuham Djinn gets -2/-2 as long as white is the most common color among all permanents or is tied for most common.').
card_mana_cost('ruham djinn', ['5', 'W']).
card_cmc('ruham djinn', 6).
card_layout('ruham djinn', 'normal').
card_power('ruham djinn', 5).
card_toughness('ruham djinn', 5).

% Found in: CMD
card_name('ruhan of the fomori', 'Ruhan of the Fomori').
card_type('ruhan of the fomori', 'Legendary Creature — Giant Warrior').
card_types('ruhan of the fomori', ['Creature']).
card_subtypes('ruhan of the fomori', ['Giant', 'Warrior']).
card_supertypes('ruhan of the fomori', ['Legendary']).
card_colors('ruhan of the fomori', ['W', 'U', 'R']).
card_text('ruhan of the fomori', 'At the beginning of combat on your turn, choose an opponent at random. Ruhan of the Fomori attacks that player this combat if able.').
card_mana_cost('ruhan of the fomori', ['1', 'R', 'W', 'U']).
card_cmc('ruhan of the fomori', 4).
card_layout('ruhan of the fomori', 'normal').
card_power('ruhan of the fomori', 7).
card_toughness('ruhan of the fomori', 7).

% Found in: WWK
card_name('ruin ghost', 'Ruin Ghost').
card_type('ruin ghost', 'Creature — Spirit').
card_types('ruin ghost', ['Creature']).
card_subtypes('ruin ghost', ['Spirit']).
card_colors('ruin ghost', ['W']).
card_text('ruin ghost', '{W}, {T}: Exile target land you control, then return it to the battlefield under your control.').
card_mana_cost('ruin ghost', ['1', 'W']).
card_cmc('ruin ghost', 2).
card_layout('ruin ghost', 'normal').
card_power('ruin ghost', 1).
card_toughness('ruin ghost', 1).

% Found in: BFZ
card_name('ruin processor', 'Ruin Processor').
card_type('ruin processor', 'Creature — Eldrazi Processor').
card_types('ruin processor', ['Creature']).
card_subtypes('ruin processor', ['Eldrazi', 'Processor']).
card_colors('ruin processor', []).
card_text('ruin processor', 'When you cast Ruin Processor, you may put a card an opponent owns from exile into that player\'s graveyard. If you do, you gain 5 life.').
card_mana_cost('ruin processor', ['7']).
card_cmc('ruin processor', 7).
card_layout('ruin processor', 'normal').
card_power('ruin processor', 7).
card_toughness('ruin processor', 8).

% Found in: CMD, STH
card_name('ruination', 'Ruination').
card_type('ruination', 'Sorcery').
card_types('ruination', ['Sorcery']).
card_subtypes('ruination', []).
card_colors('ruination', ['R']).
card_text('ruination', 'Destroy all nonbasic lands.').
card_mana_cost('ruination', ['3', 'R']).
card_cmc('ruination', 4).
card_layout('ruination', 'normal').

% Found in: BFZ
card_name('ruination guide', 'Ruination Guide').
card_type('ruination guide', 'Creature — Eldrazi Drone').
card_types('ruination guide', ['Creature']).
card_subtypes('ruination guide', ['Eldrazi', 'Drone']).
card_colors('ruination guide', []).
card_text('ruination guide', 'Devoid (This card has no color.)\nIngest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.)\nOther colorless creatures you control get +1/+0.').
card_mana_cost('ruination guide', ['2', 'U']).
card_cmc('ruination guide', 3).
card_layout('ruination guide', 'normal').
card_power('ruination guide', 3).
card_toughness('ruination guide', 2).

% Found in: GTC
card_name('ruination wurm', 'Ruination Wurm').
card_type('ruination wurm', 'Creature — Wurm').
card_types('ruination wurm', ['Creature']).
card_subtypes('ruination wurm', ['Wurm']).
card_colors('ruination wurm', ['R', 'G']).
card_text('ruination wurm', '').
card_mana_cost('ruination wurm', ['4', 'R', 'G']).
card_cmc('ruination wurm', 6).
card_layout('ruination wurm', 'normal').
card_power('ruination wurm', 7).
card_toughness('ruination wurm', 6).

% Found in: ZEN
card_name('ruinous minotaur', 'Ruinous Minotaur').
card_type('ruinous minotaur', 'Creature — Minotaur Warrior').
card_types('ruinous minotaur', ['Creature']).
card_subtypes('ruinous minotaur', ['Minotaur', 'Warrior']).
card_colors('ruinous minotaur', ['R']).
card_text('ruinous minotaur', 'Whenever Ruinous Minotaur deals damage to an opponent, sacrifice a land.').
card_mana_cost('ruinous minotaur', ['1', 'R', 'R']).
card_cmc('ruinous minotaur', 3).
card_layout('ruinous minotaur', 'normal').
card_power('ruinous minotaur', 5).
card_toughness('ruinous minotaur', 2).

% Found in: BFZ
card_name('ruinous path', 'Ruinous Path').
card_type('ruinous path', 'Sorcery').
card_types('ruinous path', ['Sorcery']).
card_subtypes('ruinous path', []).
card_colors('ruinous path', ['B']).
card_text('ruinous path', 'Destroy target creature or planeswalker.\nAwaken 4—{5}{B}{B} (If you cast this spell for {5}{B}{B}, also put four +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_mana_cost('ruinous path', ['1', 'B', 'B']).
card_cmc('ruinous path', 3).
card_layout('ruinous path', 'normal').

% Found in: 5ED, 6ED, FEM, ME2
card_name('ruins of trokair', 'Ruins of Trokair').
card_type('ruins of trokair', 'Land').
card_types('ruins of trokair', ['Land']).
card_subtypes('ruins of trokair', []).
card_colors('ruins of trokair', []).
card_text('ruins of trokair', 'Ruins of Trokair enters the battlefield tapped.\n{T}: Add {W} to your mana pool.\n{T}, Sacrifice Ruins of Trokair: Add {W}{W} to your mana pool.').
card_layout('ruins of trokair', 'normal').

% Found in: 8ED, 9ED, ARN, pREL
card_name('rukh egg', 'Rukh Egg').
card_type('rukh egg', 'Creature — Bird').
card_types('rukh egg', ['Creature']).
card_subtypes('rukh egg', ['Bird']).
card_colors('rukh egg', ['R']).
card_text('rukh egg', 'When Rukh Egg dies, put a 4/4 red Bird creature token with flying onto the battlefield at the beginning of the next end step.').
card_mana_cost('rukh egg', ['3', 'R']).
card_cmc('rukh egg', 4).
card_layout('rukh egg', 'normal').
card_power('rukh egg', 0).
card_toughness('rukh egg', 3).

% Found in: 10E, MRD
card_name('rule of law', 'Rule of Law').
card_type('rule of law', 'Enchantment').
card_types('rule of law', ['Enchantment']).
card_subtypes('rule of law', []).
card_colors('rule of law', ['W']).
card_text('rule of law', 'Each player can\'t cast more than one spell each turn.').
card_mana_cost('rule of law', ['2', 'W']).
card_cmc('rule of law', 3).
card_layout('rule of law', 'normal').

% Found in: WWK
card_name('rumbling aftershocks', 'Rumbling Aftershocks').
card_type('rumbling aftershocks', 'Enchantment').
card_types('rumbling aftershocks', ['Enchantment']).
card_subtypes('rumbling aftershocks', []).
card_colors('rumbling aftershocks', ['R']).
card_text('rumbling aftershocks', 'Whenever you cast a kicked spell, you may have Rumbling Aftershocks deal damage to target creature or player equal to the number of times that spell was kicked.').
card_mana_cost('rumbling aftershocks', ['4', 'R']).
card_cmc('rumbling aftershocks', 5).
card_layout('rumbling aftershocks', 'normal').

% Found in: M14
card_name('rumbling baloth', 'Rumbling Baloth').
card_type('rumbling baloth', 'Creature — Beast').
card_types('rumbling baloth', ['Creature']).
card_subtypes('rumbling baloth', ['Beast']).
card_colors('rumbling baloth', ['G']).
card_text('rumbling baloth', '').
card_mana_cost('rumbling baloth', ['2', 'G', 'G']).
card_cmc('rumbling baloth', 4).
card_layout('rumbling baloth', 'normal').
card_power('rumbling baloth', 4).
card_toughness('rumbling baloth', 4).

% Found in: USG
card_name('rumbling crescendo', 'Rumbling Crescendo').
card_type('rumbling crescendo', 'Enchantment').
card_types('rumbling crescendo', ['Enchantment']).
card_subtypes('rumbling crescendo', []).
card_colors('rumbling crescendo', ['R']).
card_text('rumbling crescendo', 'At the beginning of your upkeep, you may put a verse counter on Rumbling Crescendo.\n{R}, Sacrifice Rumbling Crescendo: Destroy up to X target lands, where X is the number of verse counters on Rumbling Crescendo.').
card_mana_cost('rumbling crescendo', ['3', 'R', 'R']).
card_cmc('rumbling crescendo', 5).
card_layout('rumbling crescendo', 'normal').

% Found in: GPT, HOP
card_name('rumbling slum', 'Rumbling Slum').
card_type('rumbling slum', 'Creature — Elemental').
card_types('rumbling slum', ['Creature']).
card_subtypes('rumbling slum', ['Elemental']).
card_colors('rumbling slum', ['R', 'G']).
card_text('rumbling slum', 'At the beginning of your upkeep, Rumbling Slum deals 1 damage to each player.').
card_mana_cost('rumbling slum', ['1', 'R', 'G', 'G']).
card_cmc('rumbling slum', 4).
card_layout('rumbling slum', 'normal').
card_power('rumbling slum', 5).
card_toughness('rumbling slum', 5).

% Found in: VAN
card_name('rumbling slum avatar', 'Rumbling Slum Avatar').
card_type('rumbling slum avatar', 'Vanguard').
card_types('rumbling slum avatar', ['Vanguard']).
card_subtypes('rumbling slum avatar', []).
card_colors('rumbling slum avatar', []).
card_text('rumbling slum avatar', 'At the beginning of your upkeep, Rumbling Slum Avatar deals 1 damage to each opponent.').
card_layout('rumbling slum avatar', 'vanguard').

% Found in: M13, M15
card_name('rummaging goblin', 'Rummaging Goblin').
card_type('rummaging goblin', 'Creature — Goblin Rogue').
card_types('rummaging goblin', ['Creature']).
card_subtypes('rummaging goblin', ['Goblin', 'Rogue']).
card_colors('rummaging goblin', ['R']).
card_text('rummaging goblin', '{T}, Discard a card: Draw a card.').
card_mana_cost('rummaging goblin', ['2', 'R']).
card_cmc('rummaging goblin', 3).
card_layout('rummaging goblin', 'normal').
card_power('rummaging goblin', 1).
card_toughness('rummaging goblin', 1).

% Found in: ONS
card_name('rummaging wizard', 'Rummaging Wizard').
card_type('rummaging wizard', 'Creature — Human Wizard').
card_types('rummaging wizard', ['Creature']).
card_subtypes('rummaging wizard', ['Human', 'Wizard']).
card_colors('rummaging wizard', ['U']).
card_text('rummaging wizard', '{2}{U}: Look at the top card of your library. You may put that card into your graveyard.').
card_mana_cost('rummaging wizard', ['3', 'U']).
card_cmc('rummaging wizard', 4).
card_layout('rummaging wizard', 'normal').
card_power('rummaging wizard', 2).
card_toughness('rummaging wizard', 2).

% Found in: DIS
card_name('run', 'Run').
card_type('run', 'Instant').
card_types('run', ['Instant']).
card_subtypes('run', []).
card_colors('run', ['R', 'G']).
card_text('run', 'Attacking creatures you control get +1/+0 until end of turn for each other attacking creature.').
card_mana_cost('run', ['3', 'R', 'G']).
card_cmc('run', 5).
card_layout('run', 'split').

% Found in: ONS
card_name('run wild', 'Run Wild').
card_type('run wild', 'Instant').
card_types('run wild', ['Instant']).
card_subtypes('run wild', []).
card_colors('run wild', ['G']).
card_text('run wild', 'Until end of turn, target creature gains trample and \"{G}: Regenerate this creature.\"').
card_mana_cost('run wild', ['G']).
card_cmc('run wild', 1).
card_layout('run wild', 'normal').

% Found in: USG
card_name('rune of protection: artifacts', 'Rune of Protection: Artifacts').
card_type('rune of protection: artifacts', 'Enchantment').
card_types('rune of protection: artifacts', ['Enchantment']).
card_subtypes('rune of protection: artifacts', []).
card_colors('rune of protection: artifacts', ['W']).
card_text('rune of protection: artifacts', '{W}: The next time an artifact source of your choice would deal damage to you this turn, prevent that damage.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('rune of protection: artifacts', ['1', 'W']).
card_cmc('rune of protection: artifacts', 2).
card_layout('rune of protection: artifacts', 'normal').

% Found in: USG
card_name('rune of protection: black', 'Rune of Protection: Black').
card_type('rune of protection: black', 'Enchantment').
card_types('rune of protection: black', ['Enchantment']).
card_subtypes('rune of protection: black', []).
card_colors('rune of protection: black', ['W']).
card_text('rune of protection: black', '{W}: The next time a black source of your choice would deal damage to you this turn, prevent that damage.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('rune of protection: black', ['1', 'W']).
card_cmc('rune of protection: black', 2).
card_layout('rune of protection: black', 'normal').

% Found in: USG
card_name('rune of protection: blue', 'Rune of Protection: Blue').
card_type('rune of protection: blue', 'Enchantment').
card_types('rune of protection: blue', ['Enchantment']).
card_subtypes('rune of protection: blue', []).
card_colors('rune of protection: blue', ['W']).
card_text('rune of protection: blue', '{W}: The next time a blue source of your choice would deal damage to you this turn, prevent that damage.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('rune of protection: blue', ['1', 'W']).
card_cmc('rune of protection: blue', 2).
card_layout('rune of protection: blue', 'normal').

% Found in: USG
card_name('rune of protection: green', 'Rune of Protection: Green').
card_type('rune of protection: green', 'Enchantment').
card_types('rune of protection: green', ['Enchantment']).
card_subtypes('rune of protection: green', []).
card_colors('rune of protection: green', ['W']).
card_text('rune of protection: green', '{W}: The next time a green source of your choice would deal damage to you this turn, prevent that damage.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('rune of protection: green', ['1', 'W']).
card_cmc('rune of protection: green', 2).
card_layout('rune of protection: green', 'normal').

% Found in: USG
card_name('rune of protection: lands', 'Rune of Protection: Lands').
card_type('rune of protection: lands', 'Enchantment').
card_types('rune of protection: lands', ['Enchantment']).
card_subtypes('rune of protection: lands', []).
card_colors('rune of protection: lands', ['W']).
card_text('rune of protection: lands', '{W}: The next time a land source of your choice would deal damage to you this turn, prevent that damage.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('rune of protection: lands', ['1', 'W']).
card_cmc('rune of protection: lands', 2).
card_layout('rune of protection: lands', 'normal').

% Found in: USG
card_name('rune of protection: red', 'Rune of Protection: Red').
card_type('rune of protection: red', 'Enchantment').
card_types('rune of protection: red', ['Enchantment']).
card_subtypes('rune of protection: red', []).
card_colors('rune of protection: red', ['W']).
card_text('rune of protection: red', '{W}: The next time a red source of your choice would deal damage to you this turn, prevent that damage.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('rune of protection: red', ['1', 'W']).
card_cmc('rune of protection: red', 2).
card_layout('rune of protection: red', 'normal').

% Found in: USG
card_name('rune of protection: white', 'Rune of Protection: White').
card_type('rune of protection: white', 'Enchantment').
card_types('rune of protection: white', ['Enchantment']).
card_subtypes('rune of protection: white', []).
card_colors('rune of protection: white', ['W']).
card_text('rune of protection: white', '{W}: The next time a white source of your choice would deal damage to you this turn, prevent that damage.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('rune of protection: white', ['1', 'W']).
card_cmc('rune of protection: white', 2).
card_layout('rune of protection: white', 'normal').

% Found in: CSP
card_name('rune snag', 'Rune Snag').
card_type('rune snag', 'Instant').
card_types('rune snag', ['Instant']).
card_subtypes('rune snag', []).
card_colors('rune snag', ['U']).
card_text('rune snag', 'Counter target spell unless its controller pays {2} plus an additional {2} for each card named Rune Snag in each graveyard.').
card_mana_cost('rune snag', ['1', 'U']).
card_cmc('rune snag', 2).
card_layout('rune snag', 'normal').

% Found in: SHM
card_name('rune-cervin rider', 'Rune-Cervin Rider').
card_type('rune-cervin rider', 'Creature — Elf Knight').
card_types('rune-cervin rider', ['Creature']).
card_subtypes('rune-cervin rider', ['Elf', 'Knight']).
card_colors('rune-cervin rider', ['W']).
card_text('rune-cervin rider', 'Flying\n{G/W}{G/W}: Rune-Cervin Rider gets +1/+1 until end of turn.').
card_mana_cost('rune-cervin rider', ['3', 'W']).
card_cmc('rune-cervin rider', 4).
card_layout('rune-cervin rider', 'normal').
card_power('rune-cervin rider', 2).
card_toughness('rune-cervin rider', 2).

% Found in: M12
card_name('rune-scarred demon', 'Rune-Scarred Demon').
card_type('rune-scarred demon', 'Creature — Demon').
card_types('rune-scarred demon', ['Creature']).
card_subtypes('rune-scarred demon', ['Demon']).
card_colors('rune-scarred demon', ['B']).
card_text('rune-scarred demon', 'Flying\nWhen Rune-Scarred Demon enters the battlefield, search your library for a card, put it into your hand, then shuffle your library.').
card_mana_cost('rune-scarred demon', ['5', 'B', 'B']).
card_cmc('rune-scarred demon', 7).
card_layout('rune-scarred demon', 'normal').
card_power('rune-scarred demon', 6).
card_toughness('rune-scarred demon', 6).

% Found in: SOK
card_name('rune-tail\'s essence', 'Rune-Tail\'s Essence').
card_type('rune-tail\'s essence', 'Legendary Enchantment').
card_types('rune-tail\'s essence', ['Enchantment']).
card_subtypes('rune-tail\'s essence', []).
card_supertypes('rune-tail\'s essence', ['Legendary']).
card_colors('rune-tail\'s essence', ['W']).
card_text('rune-tail\'s essence', 'Prevent all damage that would be dealt to creatures you control.').
card_mana_cost('rune-tail\'s essence', ['2', 'W']).
card_cmc('rune-tail\'s essence', 3).
card_layout('rune-tail\'s essence', 'flip').

% Found in: SOK
card_name('rune-tail, kitsune ascendant', 'Rune-Tail, Kitsune Ascendant').
card_type('rune-tail, kitsune ascendant', 'Legendary Creature — Fox Monk').
card_types('rune-tail, kitsune ascendant', ['Creature']).
card_subtypes('rune-tail, kitsune ascendant', ['Fox', 'Monk']).
card_supertypes('rune-tail, kitsune ascendant', ['Legendary']).
card_colors('rune-tail, kitsune ascendant', ['W']).
card_text('rune-tail, kitsune ascendant', 'When you have 30 or more life, flip Rune-Tail, Kitsune Ascendant.').
card_mana_cost('rune-tail, kitsune ascendant', ['2', 'W']).
card_cmc('rune-tail, kitsune ascendant', 3).
card_layout('rune-tail, kitsune ascendant', 'flip').
card_power('rune-tail, kitsune ascendant', 2).
card_toughness('rune-tail, kitsune ascendant', 2).
card_sides('rune-tail, kitsune ascendant', 'rune-tail\'s essence').

% Found in: GPT
card_name('runeboggle', 'Runeboggle').
card_type('runeboggle', 'Instant').
card_types('runeboggle', ['Instant']).
card_subtypes('runeboggle', []).
card_colors('runeboggle', ['U']).
card_text('runeboggle', 'Counter target spell unless its controller pays {1}.\nDraw a card.').
card_mana_cost('runeboggle', ['2', 'U']).
card_cmc('runeboggle', 3).
card_layout('runeboggle', 'normal').

% Found in: ISD
card_name('runechanter\'s pike', 'Runechanter\'s Pike').
card_type('runechanter\'s pike', 'Artifact — Equipment').
card_types('runechanter\'s pike', ['Artifact']).
card_subtypes('runechanter\'s pike', ['Equipment']).
card_colors('runechanter\'s pike', []).
card_text('runechanter\'s pike', 'Equipped creature has first strike and gets +X/+0, where X is the number of instant and sorcery cards in your graveyard.\nEquip {2}').
card_mana_cost('runechanter\'s pike', ['2']).
card_cmc('runechanter\'s pike', 2).
card_layout('runechanter\'s pike', 'normal').

% Found in: DPA, M10, M11, M12, M15
card_name('runeclaw bear', 'Runeclaw Bear').
card_type('runeclaw bear', 'Creature — Bear').
card_types('runeclaw bear', ['Creature']).
card_subtypes('runeclaw bear', ['Bear']).
card_colors('runeclaw bear', ['G']).
card_text('runeclaw bear', '').
card_mana_cost('runeclaw bear', ['1', 'G']).
card_cmc('runeclaw bear', 2).
card_layout('runeclaw bear', 'normal').
card_power('runeclaw bear', 2).
card_toughness('runeclaw bear', 2).

% Found in: ICE
card_name('runed arch', 'Runed Arch').
card_type('runed arch', 'Artifact').
card_types('runed arch', ['Artifact']).
card_subtypes('runed arch', []).
card_colors('runed arch', []).
card_text('runed arch', 'Runed Arch enters the battlefield tapped.\n{X}, {T}, Sacrifice Runed Arch: X target creatures with power 2 or less can\'t be blocked this turn.').
card_mana_cost('runed arch', ['3']).
card_cmc('runed arch', 3).
card_layout('runed arch', 'normal').

% Found in: SHM
card_name('runed halo', 'Runed Halo').
card_type('runed halo', 'Enchantment').
card_types('runed halo', ['Enchantment']).
card_subtypes('runed halo', []).
card_colors('runed halo', ['W']).
card_text('runed halo', 'As Runed Halo enters the battlefield, name a card.\nYou have protection from the chosen name. (You can\'t be targeted, dealt damage, or enchanted by anything with that name.)').
card_mana_cost('runed halo', ['W', 'W']).
card_cmc('runed halo', 2).
card_layout('runed halo', 'normal').

% Found in: CNS, DDF, DDP, MM2, ORI, ROE
card_name('runed servitor', 'Runed Servitor').
card_type('runed servitor', 'Artifact Creature — Construct').
card_types('runed servitor', ['Artifact', 'Creature']).
card_subtypes('runed servitor', ['Construct']).
card_colors('runed servitor', []).
card_text('runed servitor', 'When Runed Servitor dies, each player draws a card.').
card_mana_cost('runed servitor', ['2']).
card_cmc('runed servitor', 2).
card_layout('runed servitor', 'normal').
card_power('runed servitor', 2).
card_toughness('runed servitor', 2).

% Found in: LRW, MMA
card_name('runed stalactite', 'Runed Stalactite').
card_type('runed stalactite', 'Artifact — Equipment').
card_types('runed stalactite', ['Artifact']).
card_subtypes('runed stalactite', ['Equipment']).
card_colors('runed stalactite', []).
card_text('runed stalactite', 'Equipped creature gets +1/+1 and is every creature type.\nEquip {2}').
card_mana_cost('runed stalactite', ['1']).
card_cmc('runed stalactite', 1).
card_layout('runed stalactite', 'normal').

% Found in: ZEN
card_name('runeflare trap', 'Runeflare Trap').
card_type('runeflare trap', 'Instant — Trap').
card_types('runeflare trap', ['Instant']).
card_subtypes('runeflare trap', ['Trap']).
card_colors('runeflare trap', ['R']).
card_text('runeflare trap', 'If an opponent drew three or more cards this turn, you may pay {R} rather than pay Runeflare Trap\'s mana cost.\nRuneflare Trap deals damage to target player equal to the number of cards in that player\'s hand.').
card_mana_cost('runeflare trap', ['4', 'R', 'R']).
card_cmc('runeflare trap', 6).
card_layout('runeflare trap', 'normal').

% Found in: SHM
card_name('runes of the deus', 'Runes of the Deus').
card_type('runes of the deus', 'Enchantment — Aura').
card_types('runes of the deus', ['Enchantment']).
card_subtypes('runes of the deus', ['Aura']).
card_colors('runes of the deus', ['R', 'G']).
card_text('runes of the deus', 'Enchant creature\nAs long as enchanted creature is red, it gets +1/+1 and has double strike. (It deals both first-strike and regular combat damage.)\nAs long as enchanted creature is green, it gets +1/+1 and has trample.').
card_mana_cost('runes of the deus', ['4', 'R/G']).
card_cmc('runes of the deus', 5).
card_layout('runes of the deus', 'normal').

% Found in: CHR, DRK
card_name('runesword', 'Runesword').
card_type('runesword', 'Artifact').
card_types('runesword', ['Artifact']).
card_subtypes('runesword', []).
card_colors('runesword', []).
card_text('runesword', '{3}, {T}: Target attacking creature gets +2/+0 until end of turn. When that creature leaves the battlefield this turn, sacrifice Runesword. If the creature deals damage to a creature this turn, the creature dealt damage can\'t be regenerated this turn. If a creature dealt damage by the targeted creature would die this turn, exile that creature instead.').
card_mana_cost('runesword', ['6']).
card_cmc('runesword', 6).
card_layout('runesword', 'normal').

% Found in: RTR
card_name('runewing', 'Runewing').
card_type('runewing', 'Creature — Bird').
card_types('runewing', ['Creature']).
card_subtypes('runewing', ['Bird']).
card_colors('runewing', ['U']).
card_text('runewing', 'Flying\nWhen Runewing dies, draw a card.').
card_mana_cost('runewing', ['3', 'U']).
card_cmc('runewing', 4).
card_layout('runewing', 'normal').
card_power('runewing', 2).
card_toughness('runewing', 2).

% Found in: ISD
card_name('runic repetition', 'Runic Repetition').
card_type('runic repetition', 'Sorcery').
card_types('runic repetition', ['Sorcery']).
card_subtypes('runic repetition', []).
card_colors('runic repetition', ['U']).
card_text('runic repetition', 'Return target exiled card with flashback you own to your hand.').
card_mana_cost('runic repetition', ['2', 'U']).
card_cmc('runic repetition', 3).
card_layout('runic repetition', 'normal').

% Found in: DGM
card_name('runner\'s bane', 'Runner\'s Bane').
card_type('runner\'s bane', 'Enchantment — Aura').
card_types('runner\'s bane', ['Enchantment']).
card_subtypes('runner\'s bane', ['Aura']).
card_colors('runner\'s bane', ['U']).
card_text('runner\'s bane', 'Enchant creature with power 3 or less\nWhen Runner\'s Bane enters the battlefield, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_mana_cost('runner\'s bane', ['1', 'U']).
card_cmc('runner\'s bane', 2).
card_layout('runner\'s bane', 'normal').

% Found in: NMS
card_name('rupture', 'Rupture').
card_type('rupture', 'Sorcery').
card_types('rupture', ['Sorcery']).
card_subtypes('rupture', []).
card_colors('rupture', ['R']).
card_text('rupture', 'Sacrifice a creature. Rupture deals damage equal to that creature\'s power to each creature without flying and each player.').
card_mana_cost('rupture', ['2', 'R']).
card_cmc('rupture', 3).
card_layout('rupture', 'normal').

% Found in: C13, CMD, CON, DDH, H09, PC2
card_name('rupture spire', 'Rupture Spire').
card_type('rupture spire', 'Land').
card_types('rupture spire', ['Land']).
card_subtypes('rupture spire', []).
card_colors('rupture spire', []).
card_text('rupture spire', 'Rupture Spire enters the battlefield tapped.\nWhen Rupture Spire enters the battlefield, sacrifice it unless you pay {1}.\n{T}: Add one mana of any color to your mana pool.').
card_layout('rupture spire', 'normal').

% Found in: DGM
card_name('ruric thar, the unbowed', 'Ruric Thar, the Unbowed').
card_type('ruric thar, the unbowed', 'Legendary Creature — Ogre Warrior').
card_types('ruric thar, the unbowed', ['Creature']).
card_subtypes('ruric thar, the unbowed', ['Ogre', 'Warrior']).
card_supertypes('ruric thar, the unbowed', ['Legendary']).
card_colors('ruric thar, the unbowed', ['R', 'G']).
card_text('ruric thar, the unbowed', 'Vigilance, reach\nRuric Thar, the Unbowed attacks each turn if able.\nWhenever a player casts a noncreature spell, Ruric Thar deals 6 damage to that player.').
card_mana_cost('ruric thar, the unbowed', ['4', 'R', 'G']).
card_cmc('ruric thar, the unbowed', 6).
card_layout('ruric thar, the unbowed', 'normal').
card_power('ruric thar, the unbowed', 6).
card_toughness('ruric thar, the unbowed', 6).

% Found in: KTK
card_name('rush of battle', 'Rush of Battle').
card_type('rush of battle', 'Sorcery').
card_types('rush of battle', ['Sorcery']).
card_subtypes('rush of battle', []).
card_colors('rush of battle', ['W']).
card_text('rush of battle', 'Creatures you control get +2/+1 until end of turn. Warrior creatures you control gain lifelink until end of turn. (Damage dealt by those Warriors also causes their controller to gain that much life.)').
card_mana_cost('rush of battle', ['3', 'W']).
card_cmc('rush of battle', 4).
card_layout('rush of battle', 'normal').

% Found in: AVR
card_name('rush of blood', 'Rush of Blood').
card_type('rush of blood', 'Instant').
card_types('rush of blood', ['Instant']).
card_subtypes('rush of blood', []).
card_colors('rush of blood', ['R']).
card_text('rush of blood', 'Target creature gets +X/+0 until end of turn, where X is its power.').
card_mana_cost('rush of blood', ['2', 'R']).
card_cmc('rush of blood', 3).
card_layout('rush of blood', 'normal').

% Found in: BFZ
card_name('rush of ice', 'Rush of Ice').
card_type('rush of ice', 'Sorcery').
card_types('rush of ice', ['Sorcery']).
card_subtypes('rush of ice', []).
card_colors('rush of ice', ['U']).
card_text('rush of ice', 'Tap target creature. It doesn\'t untap during its controller\'s next untap step.\nAwaken 3—{4}{U} (If you cast this spell for {4}{U}, also put three +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_mana_cost('rush of ice', ['U']).
card_cmc('rush of ice', 1).
card_layout('rush of ice', 'normal').

% Found in: C14, SCG
card_name('rush of knowledge', 'Rush of Knowledge').
card_type('rush of knowledge', 'Sorcery').
card_types('rush of knowledge', ['Sorcery']).
card_subtypes('rush of knowledge', []).
card_colors('rush of knowledge', ['U']).
card_text('rush of knowledge', 'Draw cards equal to the highest converted mana cost among permanents you control.').
card_mana_cost('rush of knowledge', ['4', 'U']).
card_cmc('rush of knowledge', 5).
card_layout('rush of knowledge', 'normal').

% Found in: PLS
card_name('rushing river', 'Rushing River').
card_type('rushing river', 'Instant').
card_types('rushing river', ['Instant']).
card_subtypes('rushing river', []).
card_colors('rushing river', ['U']).
card_text('rushing river', 'Kicker—Sacrifice a land. (You may sacrifice a land in addition to any other costs as you cast this spell.)\nReturn target nonland permanent to its owner\'s hand. If Rushing River was kicked, return another target nonland permanent to its owner\'s hand.').
card_mana_cost('rushing river', ['2', 'U']).
card_cmc('rushing river', 3).
card_layout('rushing river', 'normal').

% Found in: SOK
card_name('rushing-tide zubera', 'Rushing-Tide Zubera').
card_type('rushing-tide zubera', 'Creature — Zubera Spirit').
card_types('rushing-tide zubera', ['Creature']).
card_subtypes('rushing-tide zubera', ['Zubera', 'Spirit']).
card_colors('rushing-tide zubera', ['U']).
card_text('rushing-tide zubera', 'When Rushing-Tide Zubera dies, if 4 or more damage was dealt to it this turn, draw three cards.').
card_mana_cost('rushing-tide zubera', ['2', 'U', 'U']).
card_cmc('rushing-tide zubera', 4).
card_layout('rushing-tide zubera', 'normal').
card_power('rushing-tide zubera', 3).
card_toughness('rushing-tide zubera', 3).

% Found in: 10E, 8ED, MMQ
card_name('rushwood dryad', 'Rushwood Dryad').
card_type('rushwood dryad', 'Creature — Dryad').
card_types('rushwood dryad', ['Creature']).
card_subtypes('rushwood dryad', ['Dryad']).
card_colors('rushwood dryad', ['G']).
card_text('rushwood dryad', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('rushwood dryad', ['1', 'G']).
card_cmc('rushwood dryad', 2).
card_layout('rushwood dryad', 'normal').
card_power('rushwood dryad', 2).
card_toughness('rushwood dryad', 1).

% Found in: MMQ
card_name('rushwood elemental', 'Rushwood Elemental').
card_type('rushwood elemental', 'Creature — Elemental').
card_types('rushwood elemental', ['Creature']).
card_subtypes('rushwood elemental', ['Elemental']).
card_colors('rushwood elemental', ['G']).
card_text('rushwood elemental', 'Trample\nAt the beginning of your upkeep, you may put a +1/+1 counter on Rushwood Elemental.').
card_mana_cost('rushwood elemental', ['G', 'G', 'G', 'G', 'G']).
card_cmc('rushwood elemental', 5).
card_layout('rushwood elemental', 'normal').
card_power('rushwood elemental', 4).
card_toughness('rushwood elemental', 4).

% Found in: MMQ
card_name('rushwood grove', 'Rushwood Grove').
card_type('rushwood grove', 'Land').
card_types('rushwood grove', ['Land']).
card_subtypes('rushwood grove', []).
card_colors('rushwood grove', []).
card_text('rushwood grove', 'Rushwood Grove enters the battlefield tapped.\n{T}: Put a storage counter on Rushwood Grove.\n{T}, Remove any number of storage counters from Rushwood Grove: Add {G} to your mana pool for each storage counter removed this way.').
card_layout('rushwood grove', 'normal').

% Found in: MMQ
card_name('rushwood herbalist', 'Rushwood Herbalist').
card_type('rushwood herbalist', 'Creature — Human Spellshaper').
card_types('rushwood herbalist', ['Creature']).
card_subtypes('rushwood herbalist', ['Human', 'Spellshaper']).
card_colors('rushwood herbalist', ['G']).
card_text('rushwood herbalist', '{G}, {T}, Discard a card: Regenerate target creature.').
card_mana_cost('rushwood herbalist', ['2', 'G']).
card_cmc('rushwood herbalist', 3).
card_layout('rushwood herbalist', 'normal').
card_power('rushwood herbalist', 2).
card_toughness('rushwood herbalist', 2).

% Found in: MMQ
card_name('rushwood legate', 'Rushwood Legate').
card_type('rushwood legate', 'Creature — Dryad').
card_types('rushwood legate', ['Creature']).
card_subtypes('rushwood legate', ['Dryad']).
card_colors('rushwood legate', ['G']).
card_text('rushwood legate', 'If an opponent controls an Island and you control a Forest, you may cast Rushwood Legate without paying its mana cost.').
card_mana_cost('rushwood legate', ['2', 'G']).
card_cmc('rushwood legate', 3).
card_layout('rushwood legate', 'normal').
card_power('rushwood legate', 2).
card_toughness('rushwood legate', 1).

% Found in: DKA
card_name('russet wolves', 'Russet Wolves').
card_type('russet wolves', 'Creature — Wolf').
card_types('russet wolves', ['Creature']).
card_subtypes('russet wolves', ['Wolf']).
card_colors('russet wolves', ['R']).
card_text('russet wolves', '').
card_mana_cost('russet wolves', ['3', 'R']).
card_cmc('russet wolves', 4).
card_layout('russet wolves', 'normal').
card_power('russet wolves', 3).
card_toughness('russet wolves', 3).

% Found in: LEG
card_name('rust', 'Rust').
card_type('rust', 'Instant').
card_types('rust', ['Instant']).
card_subtypes('rust', []).
card_colors('rust', ['G']).
card_text('rust', 'Counter target activated ability from an artifact source. (Mana abilities can\'t be targeted.)').
card_mana_cost('rust', ['G']).
card_cmc('rust', 1).
card_layout('rust', 'normal').

% Found in: MRD
card_name('rust elemental', 'Rust Elemental').
card_type('rust elemental', 'Artifact Creature — Elemental').
card_types('rust elemental', ['Artifact', 'Creature']).
card_subtypes('rust elemental', ['Elemental']).
card_colors('rust elemental', []).
card_text('rust elemental', 'Flying\nAt the beginning of your upkeep, sacrifice an artifact other than Rust Elemental. If you can\'t, tap Rust Elemental and you lose 4 life.').
card_mana_cost('rust elemental', ['4']).
card_cmc('rust elemental', 4).
card_layout('rust elemental', 'normal').
card_power('rust elemental', 4).
card_toughness('rust elemental', 4).

% Found in: GTC
card_name('rust scarab', 'Rust Scarab').
card_type('rust scarab', 'Creature — Insect').
card_types('rust scarab', ['Creature']).
card_subtypes('rust scarab', ['Insect']).
card_colors('rust scarab', ['G']).
card_text('rust scarab', 'Whenever Rust Scarab becomes blocked, you may destroy target artifact or enchantment defending player controls.').
card_mana_cost('rust scarab', ['4', 'G']).
card_cmc('rust scarab', 5).
card_layout('rust scarab', 'normal').
card_power('rust scarab', 4).
card_toughness('rust scarab', 5).

% Found in: SOM
card_name('rust tick', 'Rust Tick').
card_type('rust tick', 'Artifact Creature — Insect').
card_types('rust tick', ['Artifact', 'Creature']).
card_subtypes('rust tick', ['Insect']).
card_colors('rust tick', []).
card_text('rust tick', 'You may choose not to untap Rust Tick during your untap step.\n{1}, {T}: Tap target artifact. It doesn\'t untap during its controller\'s untap step for as long as Rust Tick remains tapped.').
card_mana_cost('rust tick', ['3']).
card_cmc('rust tick', 3).
card_layout('rust tick', 'normal').
card_power('rust tick', 1).
card_toughness('rust tick', 3).

% Found in: MM2, SOM
card_name('rusted relic', 'Rusted Relic').
card_type('rusted relic', 'Artifact').
card_types('rusted relic', ['Artifact']).
card_subtypes('rusted relic', []).
card_colors('rusted relic', []).
card_text('rusted relic', 'Metalcraft — Rusted Relic is a 5/5 Golem artifact creature as long as you control three or more artifacts.').
card_mana_cost('rusted relic', ['4']).
card_cmc('rusted relic', 4).
card_layout('rusted relic', 'normal').

% Found in: M12
card_name('rusted sentinel', 'Rusted Sentinel').
card_type('rusted sentinel', 'Artifact Creature — Golem').
card_types('rusted sentinel', ['Artifact', 'Creature']).
card_subtypes('rusted sentinel', ['Golem']).
card_colors('rusted sentinel', []).
card_text('rusted sentinel', 'Rusted Sentinel enters the battlefield tapped.').
card_mana_cost('rusted sentinel', ['4']).
card_cmc('rusted sentinel', 4).
card_layout('rusted sentinel', 'normal').
card_power('rusted sentinel', 3).
card_toughness('rusted sentinel', 4).

% Found in: MBS
card_name('rusted slasher', 'Rusted Slasher').
card_type('rusted slasher', 'Artifact Creature — Horror').
card_types('rusted slasher', ['Artifact', 'Creature']).
card_subtypes('rusted slasher', ['Horror']).
card_colors('rusted slasher', []).
card_text('rusted slasher', 'Sacrifice an artifact: Regenerate Rusted Slasher.').
card_mana_cost('rusted slasher', ['4']).
card_cmc('rusted slasher', 4).
card_layout('rusted slasher', 'normal').
card_power('rusted slasher', 4).
card_toughness('rusted slasher', 1).

% Found in: DDF, MOR
card_name('rustic clachan', 'Rustic Clachan').
card_type('rustic clachan', 'Land').
card_types('rustic clachan', ['Land']).
card_subtypes('rustic clachan', []).
card_colors('rustic clachan', []).
card_text('rustic clachan', 'As Rustic Clachan enters the battlefield, you may reveal a Kithkin card from your hand. If you don\'t, Rustic Clachan enters the battlefield tapped.\n{T}: Add {W} to your mana pool.\nReinforce 1—{1}{W} ({1}{W}, Discard this card: Put a +1/+1 counter on target creature.)').
card_layout('rustic clachan', 'normal').

% Found in: NMS
card_name('rusting golem', 'Rusting Golem').
card_type('rusting golem', 'Artifact Creature — Golem').
card_types('rusting golem', ['Artifact', 'Creature']).
card_subtypes('rusting golem', ['Golem']).
card_colors('rusting golem', []).
card_text('rusting golem', 'Fading 5 (This creature enters the battlefield with five fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nRusting Golem\'s power and toughness are each equal to the number of fade counters on it.').
card_mana_cost('rusting golem', ['4']).
card_cmc('rusting golem', 4).
card_layout('rusting golem', 'normal').
card_power('rusting golem', '*').
card_toughness('rusting golem', '*').

% Found in: MRD
card_name('rustmouth ogre', 'Rustmouth Ogre').
card_type('rustmouth ogre', 'Creature — Ogre').
card_types('rustmouth ogre', ['Creature']).
card_subtypes('rustmouth ogre', ['Ogre']).
card_colors('rustmouth ogre', ['R']).
card_text('rustmouth ogre', 'Whenever Rustmouth Ogre deals combat damage to a player, you may destroy target artifact that player controls.').
card_mana_cost('rustmouth ogre', ['4', 'R', 'R']).
card_cmc('rustmouth ogre', 6).
card_layout('rustmouth ogre', 'normal').
card_power('rustmouth ogre', 5).
card_toughness('rustmouth ogre', 4).

% Found in: SHM
card_name('rustrazor butcher', 'Rustrazor Butcher').
card_type('rustrazor butcher', 'Creature — Goblin Warrior').
card_types('rustrazor butcher', ['Creature']).
card_subtypes('rustrazor butcher', ['Goblin', 'Warrior']).
card_colors('rustrazor butcher', ['R']).
card_text('rustrazor butcher', 'First strike\nWither (This deals damage to creatures in the form of -1/-1 counters.)').
card_mana_cost('rustrazor butcher', ['1', 'R']).
card_cmc('rustrazor butcher', 2).
card_layout('rustrazor butcher', 'normal').
card_power('rustrazor butcher', 1).
card_toughness('rustrazor butcher', 2).

% Found in: MRD
card_name('rustspore ram', 'Rustspore Ram').
card_type('rustspore ram', 'Artifact Creature — Sheep').
card_types('rustspore ram', ['Artifact', 'Creature']).
card_subtypes('rustspore ram', ['Sheep']).
card_colors('rustspore ram', []).
card_text('rustspore ram', 'When Rustspore Ram enters the battlefield, destroy target Equipment.').
card_mana_cost('rustspore ram', ['4']).
card_cmc('rustspore ram', 4).
card_layout('rustspore ram', 'normal').
card_power('rustspore ram', 1).
card_toughness('rustspore ram', 3).

% Found in: WWK
card_name('ruthless cullblade', 'Ruthless Cullblade').
card_type('ruthless cullblade', 'Creature — Vampire Warrior').
card_types('ruthless cullblade', ['Creature']).
card_subtypes('ruthless cullblade', ['Vampire', 'Warrior']).
card_colors('ruthless cullblade', ['B']).
card_text('ruthless cullblade', 'Ruthless Cullblade gets +2/+1 as long as an opponent has 10 or less life.').
card_mana_cost('ruthless cullblade', ['1', 'B']).
card_cmc('ruthless cullblade', 2).
card_layout('ruthless cullblade', 'normal').
card_power('ruthless cullblade', 2).
card_toughness('ruthless cullblade', 1).

% Found in: DTK
card_name('ruthless deathfang', 'Ruthless Deathfang').
card_type('ruthless deathfang', 'Creature — Dragon').
card_types('ruthless deathfang', ['Creature']).
card_subtypes('ruthless deathfang', ['Dragon']).
card_colors('ruthless deathfang', ['U', 'B']).
card_text('ruthless deathfang', 'Flying\nWhenever you sacrifice a creature, target opponent sacrifices a creature.').
card_mana_cost('ruthless deathfang', ['4', 'U', 'B']).
card_cmc('ruthless deathfang', 6).
card_layout('ruthless deathfang', 'normal').
card_power('ruthless deathfang', 4).
card_toughness('ruthless deathfang', 4).

% Found in: FRF
card_name('ruthless instincts', 'Ruthless Instincts').
card_type('ruthless instincts', 'Instant').
card_types('ruthless instincts', ['Instant']).
card_subtypes('ruthless instincts', []).
card_colors('ruthless instincts', ['G']).
card_text('ruthless instincts', 'Choose one —\n• Target nonattacking creature gains reach and deathtouch until end of turn. Untap it.\n• Target attacking creature gets +2/+2 and gains trample until end of turn.').
card_mana_cost('ruthless instincts', ['2', 'G']).
card_cmc('ruthless instincts', 3).
card_layout('ruthless instincts', 'normal').

% Found in: NPH
card_name('ruthless invasion', 'Ruthless Invasion').
card_type('ruthless invasion', 'Sorcery').
card_types('ruthless invasion', ['Sorcery']).
card_subtypes('ruthless invasion', []).
card_colors('ruthless invasion', ['R']).
card_text('ruthless invasion', '({R/P} can be paid with either {R} or 2 life.)\nNonartifact creatures can\'t block this turn.').
card_mana_cost('ruthless invasion', ['3', 'R/P']).
card_cmc('ruthless invasion', 4).
card_layout('ruthless invasion', 'normal').

% Found in: FRF_UGIN, KTK
card_name('ruthless ripper', 'Ruthless Ripper').
card_type('ruthless ripper', 'Creature — Human Assassin').
card_types('ruthless ripper', ['Creature']).
card_subtypes('ruthless ripper', ['Human', 'Assassin']).
card_colors('ruthless ripper', ['B']).
card_text('ruthless ripper', 'Deathtouch\nMorph—Reveal a black card in your hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Ruthless Ripper is turned face up, target player loses 2 life.').
card_mana_cost('ruthless ripper', ['B']).
card_cmc('ruthless ripper', 1).
card_layout('ruthless ripper', 'normal').
card_power('ruthless ripper', 1).
card_toughness('ruthless ripper', 1).

% Found in: HML
card_name('rysorian badger', 'Rysorian Badger').
card_type('rysorian badger', 'Creature — Badger').
card_types('rysorian badger', ['Creature']).
card_subtypes('rysorian badger', ['Badger']).
card_colors('rysorian badger', ['G']).
card_text('rysorian badger', 'Whenever Rysorian Badger attacks and isn\'t blocked, you may exile up to two target creature cards from defending player\'s graveyard. If you do, you gain 1 life for each card exiled this way and Rysorian Badger assigns no combat damage this turn.').
card_mana_cost('rysorian badger', ['2', 'G']).
card_cmc('rysorian badger', 3).
card_layout('rysorian badger', 'normal').
card_power('rysorian badger', 2).
card_toughness('rysorian badger', 2).
card_reserved('rysorian badger').

% Found in: ARC, CHK, MMA, pPRE
card_name('ryusei, the falling star', 'Ryusei, the Falling Star').
card_type('ryusei, the falling star', 'Legendary Creature — Dragon Spirit').
card_types('ryusei, the falling star', ['Creature']).
card_subtypes('ryusei, the falling star', ['Dragon', 'Spirit']).
card_supertypes('ryusei, the falling star', ['Legendary']).
card_colors('ryusei, the falling star', ['R']).
card_text('ryusei, the falling star', 'Flying\nWhen Ryusei, the Falling Star dies, it deals 5 damage to each creature without flying.').
card_mana_cost('ryusei, the falling star', ['5', 'R']).
card_cmc('ryusei, the falling star', 6).
card_layout('ryusei, the falling star', 'normal').
card_power('ryusei, the falling star', 5).
card_toughness('ryusei, the falling star', 5).

