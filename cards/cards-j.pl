% Card-specific data

% Found in: WTH
card_name('jabari\'s banner', 'Jabari\'s Banner').
card_type('jabari\'s banner', 'Artifact').
card_types('jabari\'s banner', ['Artifact']).
card_subtypes('jabari\'s banner', []).
card_colors('jabari\'s banner', []).
card_text('jabari\'s banner', '{1}, {T}: Target creature gains flanking until end of turn. (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)').
card_mana_cost('jabari\'s banner', ['2']).
card_cmc('jabari\'s banner', 2).
card_layout('jabari\'s banner', 'normal').

% Found in: MIR
card_name('jabari\'s influence', 'Jabari\'s Influence').
card_type('jabari\'s influence', 'Instant').
card_types('jabari\'s influence', ['Instant']).
card_subtypes('jabari\'s influence', []).
card_colors('jabari\'s influence', ['W']).
card_text('jabari\'s influence', 'Cast Jabari\'s Influence only after combat.\nGain control of target nonartifact, nonblack creature that attacked you this turn and put a -1/-0 counter on it.').
card_mana_cost('jabari\'s influence', ['3', 'W', 'W']).
card_cmc('jabari\'s influence', 5).
card_layout('jabari\'s influence', 'normal').
card_reserved('jabari\'s influence').

% Found in: DD2, DD3_JVC, LRW, M10, M11, pMEI
card_name('jace beleren', 'Jace Beleren').
card_type('jace beleren', 'Planeswalker — Jace').
card_types('jace beleren', ['Planeswalker']).
card_subtypes('jace beleren', ['Jace']).
card_colors('jace beleren', ['U']).
card_text('jace beleren', '+2: Each player draws a card.\n−1: Target player draws a card.\n−10: Target player puts the top twenty cards of his or her library into his or her graveyard.').
card_mana_cost('jace beleren', ['1', 'U', 'U']).
card_cmc('jace beleren', 3).
card_layout('jace beleren', 'normal').
card_loyalty('jace beleren', 3).

% Found in: C13, M12
card_name('jace\'s archivist', 'Jace\'s Archivist').
card_type('jace\'s archivist', 'Creature — Vedalken Wizard').
card_types('jace\'s archivist', ['Creature']).
card_subtypes('jace\'s archivist', ['Vedalken', 'Wizard']).
card_colors('jace\'s archivist', ['U']).
card_text('jace\'s archivist', '{U}, {T}: Each player discards his or her hand, then draws cards equal to the greatest number of cards a player discarded this way.').
card_mana_cost('jace\'s archivist', ['1', 'U', 'U']).
card_cmc('jace\'s archivist', 3).
card_layout('jace\'s archivist', 'normal').
card_power('jace\'s archivist', 2).
card_toughness('jace\'s archivist', 2).

% Found in: M11, M12
card_name('jace\'s erasure', 'Jace\'s Erasure').
card_type('jace\'s erasure', 'Enchantment').
card_types('jace\'s erasure', ['Enchantment']).
card_subtypes('jace\'s erasure', []).
card_colors('jace\'s erasure', ['U']).
card_text('jace\'s erasure', 'Whenever you draw a card, you may have target player put the top card of his or her library into his or her graveyard.').
card_mana_cost('jace\'s erasure', ['1', 'U']).
card_cmc('jace\'s erasure', 2).
card_layout('jace\'s erasure', 'normal').

% Found in: DDM, M11, M15, pFNM
card_name('jace\'s ingenuity', 'Jace\'s Ingenuity').
card_type('jace\'s ingenuity', 'Instant').
card_types('jace\'s ingenuity', ['Instant']).
card_subtypes('jace\'s ingenuity', []).
card_colors('jace\'s ingenuity', ['U']).
card_text('jace\'s ingenuity', 'Draw three cards.').
card_mana_cost('jace\'s ingenuity', ['3', 'U', 'U']).
card_cmc('jace\'s ingenuity', 5).
card_layout('jace\'s ingenuity', 'normal').

% Found in: DDM, M14
card_name('jace\'s mindseeker', 'Jace\'s Mindseeker').
card_type('jace\'s mindseeker', 'Creature — Fish Illusion').
card_types('jace\'s mindseeker', ['Creature']).
card_subtypes('jace\'s mindseeker', ['Fish', 'Illusion']).
card_colors('jace\'s mindseeker', ['U']).
card_text('jace\'s mindseeker', 'Flying\nWhen Jace\'s Mindseeker enters the battlefield, target opponent puts the top five cards of his or her library into his or her graveyard. You may cast an instant or sorcery card from among them without paying its mana cost.').
card_mana_cost('jace\'s mindseeker', ['4', 'U', 'U']).
card_cmc('jace\'s mindseeker', 6).
card_layout('jace\'s mindseeker', 'normal').
card_power('jace\'s mindseeker', 4).
card_toughness('jace\'s mindseeker', 4).

% Found in: DDM, M13
card_name('jace\'s phantasm', 'Jace\'s Phantasm').
card_type('jace\'s phantasm', 'Creature — Illusion').
card_types('jace\'s phantasm', ['Creature']).
card_subtypes('jace\'s phantasm', ['Illusion']).
card_colors('jace\'s phantasm', ['U']).
card_text('jace\'s phantasm', 'Flying\nJace\'s Phantasm gets +4/+4 as long as an opponent has ten or more cards in his or her graveyard.').
card_mana_cost('jace\'s phantasm', ['U']).
card_cmc('jace\'s phantasm', 1).
card_layout('jace\'s phantasm', 'normal').
card_power('jace\'s phantasm', 1).
card_toughness('jace\'s phantasm', 1).

% Found in: ORI
card_name('jace\'s sanctum', 'Jace\'s Sanctum').
card_type('jace\'s sanctum', 'Enchantment').
card_types('jace\'s sanctum', ['Enchantment']).
card_subtypes('jace\'s sanctum', []).
card_colors('jace\'s sanctum', ['U']).
card_text('jace\'s sanctum', 'Instant and sorcery spells you cast cost {1} less to cast.\nWhenever you cast an instant or sorcery spell, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('jace\'s sanctum', ['3', 'U']).
card_cmc('jace\'s sanctum', 4).
card_layout('jace\'s sanctum', 'normal').

% Found in: DDM, RTR
card_name('jace, architect of thought', 'Jace, Architect of Thought').
card_type('jace, architect of thought', 'Planeswalker — Jace').
card_types('jace, architect of thought', ['Planeswalker']).
card_subtypes('jace, architect of thought', ['Jace']).
card_colors('jace, architect of thought', ['U']).
card_text('jace, architect of thought', '+1: Until your next turn, whenever a creature an opponent controls attacks, it gets -1/-0 until end of turn.\n−2: Reveal the top three cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other on the bottom of your library in any order.\n−8: For each player, search that player\'s library for a nonland card and exile it, then that player shuffles his or her library. You may cast those cards without paying their mana costs.').
card_mana_cost('jace, architect of thought', ['2', 'U', 'U']).
card_cmc('jace, architect of thought', 4).
card_layout('jace, architect of thought', 'normal').
card_loyalty('jace, architect of thought', 4).

% Found in: M12, M13, M14, pMEI
card_name('jace, memory adept', 'Jace, Memory Adept').
card_type('jace, memory adept', 'Planeswalker — Jace').
card_types('jace, memory adept', ['Planeswalker']).
card_subtypes('jace, memory adept', ['Jace']).
card_colors('jace, memory adept', ['U']).
card_text('jace, memory adept', '+1: Draw a card. Target player puts the top card of his or her library into his or her graveyard.\n0: Target player puts the top ten cards of his or her library into his or her graveyard.\n−7: Any number of target players each draw twenty cards.').
card_mana_cost('jace, memory adept', ['3', 'U', 'U']).
card_cmc('jace, memory adept', 5).
card_layout('jace, memory adept', 'normal').
card_loyalty('jace, memory adept', 4).

% Found in: ORI
card_name('jace, telepath unbound', 'Jace, Telepath Unbound').
card_type('jace, telepath unbound', 'Planeswalker — Jace').
card_types('jace, telepath unbound', ['Planeswalker']).
card_subtypes('jace, telepath unbound', ['Jace']).
card_colors('jace, telepath unbound', ['U']).
card_text('jace, telepath unbound', '+1: Up to one target creature gets -2/-0 until your next turn.\n−3: You may cast target instant or sorcery card from your graveyard this turn. If that card would be put into your graveyard this turn, exile it instead.\n−9: You get an emblem with \"Whenever you cast a spell, target opponent puts the top five cards of his or her library into his or her graveyard.\"').
card_layout('jace, telepath unbound', 'double-faced').
card_loyalty('jace, telepath unbound', 5).

% Found in: M15, pMEI
card_name('jace, the living guildpact', 'Jace, the Living Guildpact').
card_type('jace, the living guildpact', 'Planeswalker — Jace').
card_types('jace, the living guildpact', ['Planeswalker']).
card_subtypes('jace, the living guildpact', ['Jace']).
card_colors('jace, the living guildpact', ['U']).
card_text('jace, the living guildpact', '+1: Look at the top two cards of your library. Put one of them into your graveyard.\n−3: Return another target nonland permanent to its owner\'s hand.\n−8: Each player shuffles his or her hand and graveyard into his or her library. You draw seven cards.').
card_mana_cost('jace, the living guildpact', ['2', 'U', 'U']).
card_cmc('jace, the living guildpact', 4).
card_layout('jace, the living guildpact', 'normal').
card_loyalty('jace, the living guildpact', 5).

% Found in: V13, VMA, WWK
card_name('jace, the mind sculptor', 'Jace, the Mind Sculptor').
card_type('jace, the mind sculptor', 'Planeswalker — Jace').
card_types('jace, the mind sculptor', ['Planeswalker']).
card_subtypes('jace, the mind sculptor', ['Jace']).
card_colors('jace, the mind sculptor', ['U']).
card_text('jace, the mind sculptor', '+2: Look at the top card of target player\'s library. You may put that card on the bottom of that player\'s library.\n0: Draw three cards, then put two cards from your hand on top of your library in any order.\n−1: Return target creature to its owner\'s hand.\n−12: Exile all cards from target player\'s library, then that player shuffles his or her hand into his or her library.').
card_mana_cost('jace, the mind sculptor', ['2', 'U', 'U']).
card_cmc('jace, the mind sculptor', 4).
card_layout('jace, the mind sculptor', 'normal').
card_loyalty('jace, the mind sculptor', 3).

% Found in: ORI
card_name('jace, vryn\'s prodigy', 'Jace, Vryn\'s Prodigy').
card_type('jace, vryn\'s prodigy', 'Legendary Creature — Human Wizard').
card_types('jace, vryn\'s prodigy', ['Creature']).
card_subtypes('jace, vryn\'s prodigy', ['Human', 'Wizard']).
card_supertypes('jace, vryn\'s prodigy', ['Legendary']).
card_colors('jace, vryn\'s prodigy', ['U']).
card_text('jace, vryn\'s prodigy', '{T}: Draw a card, then discard a card. If there are five or more cards in your graveyard, exile Jace, Vryn\'s Prodigy, then return him to the battlefield transformed under his owner\'s control.').
card_mana_cost('jace, vryn\'s prodigy', ['1', 'U']).
card_cmc('jace, vryn\'s prodigy', 2).
card_layout('jace, vryn\'s prodigy', 'double-faced').
card_power('jace, vryn\'s prodigy', 0).
card_toughness('jace, vryn\'s prodigy', 2).
card_sides('jace, vryn\'s prodigy', 'jace, telepath unbound').

% Found in: UGL
card_name('jack-in-the-mox', 'Jack-in-the-Mox').
card_type('jack-in-the-mox', 'Artifact').
card_types('jack-in-the-mox', ['Artifact']).
card_subtypes('jack-in-the-mox', []).
card_colors('jack-in-the-mox', []).
card_text('jack-in-the-mox', '{T}: Roll a six-sided die for Jack-in-the-Mox. On a 1, sacrifice Jack-in-the-Mox and lose 5 life. Otherwise, Jack-in-the-Mox has one of the following effects. Treat this ability as a mana source.\n2 Add {W} to your mana pool.\n3 Add {U} to your mana pool.\n4 Add {B} to your mana pool.\n5 Add {R} to your mana pool.\n6 Add {G} to your mana pool.').
card_mana_cost('jack-in-the-mox', ['0']).
card_cmc('jack-in-the-mox', 0).
card_layout('jack-in-the-mox', 'normal').

% Found in: M10
card_name('jackal familiar', 'Jackal Familiar').
card_type('jackal familiar', 'Creature — Hound').
card_types('jackal familiar', ['Creature']).
card_subtypes('jackal familiar', ['Hound']).
card_colors('jackal familiar', ['R']).
card_text('jackal familiar', 'Jackal Familiar can\'t attack or block alone.').
card_mana_cost('jackal familiar', ['R']).
card_cmc('jackal familiar', 1).
card_layout('jackal familiar', 'normal').
card_power('jackal familiar', 2).
card_toughness('jackal familiar', 2).

% Found in: PD2, TMP, pFNM
card_name('jackal pup', 'Jackal Pup').
card_type('jackal pup', 'Creature — Hound').
card_types('jackal pup', ['Creature']).
card_subtypes('jackal pup', ['Hound']).
card_colors('jackal pup', ['R']).
card_text('jackal pup', 'Whenever Jackal Pup is dealt damage, it deals that much damage to you.').
card_mana_cost('jackal pup', ['R']).
card_cmc('jackal pup', 1).
card_layout('jackal pup', 'normal').
card_power('jackal pup', 2).
card_toughness('jackal pup', 1).

% Found in: EXO
card_name('jackalope herd', 'Jackalope Herd').
card_type('jackalope herd', 'Creature — Rabbit Beast').
card_types('jackalope herd', ['Creature']).
card_subtypes('jackalope herd', ['Rabbit', 'Beast']).
card_colors('jackalope herd', ['G']).
card_text('jackalope herd', 'When you cast a spell, return Jackalope Herd to its owner\'s hand.').
card_mana_cost('jackalope herd', ['3', 'G']).
card_cmc('jackalope herd', 4).
card_layout('jackalope herd', 'normal').
card_power('jackalope herd', 4).
card_toughness('jackalope herd', 5).

% Found in: LEG, MED
card_name('jacques le vert', 'Jacques le Vert').
card_type('jacques le vert', 'Legendary Creature — Human Warrior').
card_types('jacques le vert', ['Creature']).
card_subtypes('jacques le vert', ['Human', 'Warrior']).
card_supertypes('jacques le vert', ['Legendary']).
card_colors('jacques le vert', ['W', 'R', 'G']).
card_text('jacques le vert', 'Green creatures you control get +0/+2.').
card_mana_cost('jacques le vert', ['1', 'R', 'G', 'W']).
card_cmc('jacques le vert', 4).
card_layout('jacques le vert', 'normal').
card_power('jacques le vert', 3).
card_toughness('jacques le vert', 2).
card_reserved('jacques le vert').

% Found in: ROE
card_name('jaddi lifestrider', 'Jaddi Lifestrider').
card_type('jaddi lifestrider', 'Creature — Elemental').
card_types('jaddi lifestrider', ['Creature']).
card_subtypes('jaddi lifestrider', ['Elemental']).
card_colors('jaddi lifestrider', ['G']).
card_text('jaddi lifestrider', 'When Jaddi Lifestrider enters the battlefield, you may tap any number of untapped creatures you control. You gain 2 life for each creature tapped this way.').
card_mana_cost('jaddi lifestrider', ['4', 'G']).
card_cmc('jaddi lifestrider', 5).
card_layout('jaddi lifestrider', 'normal').
card_power('jaddi lifestrider', 2).
card_toughness('jaddi lifestrider', 8).

% Found in: BFZ
card_name('jaddi offshoot', 'Jaddi Offshoot').
card_type('jaddi offshoot', 'Creature — Plant').
card_types('jaddi offshoot', ['Creature']).
card_subtypes('jaddi offshoot', ['Plant']).
card_colors('jaddi offshoot', ['G']).
card_text('jaddi offshoot', 'Defender\nLandfall — Whenever a land enters the battlefield under your control, you gain 1 life.').
card_mana_cost('jaddi offshoot', ['G']).
card_cmc('jaddi offshoot', 1).
card_layout('jaddi offshoot', 'normal').
card_power('jaddi offshoot', 0).
card_toughness('jaddi offshoot', 3).

% Found in: CHK
card_name('jade idol', 'Jade Idol').
card_type('jade idol', 'Artifact').
card_types('jade idol', ['Artifact']).
card_subtypes('jade idol', []).
card_colors('jade idol', []).
card_text('jade idol', 'Whenever you cast a Spirit or Arcane spell, Jade Idol becomes a 4/4 Spirit artifact creature until end of turn.').
card_mana_cost('jade idol', ['4']).
card_cmc('jade idol', 4).
card_layout('jade idol', 'normal').

% Found in: INV
card_name('jade leech', 'Jade Leech').
card_type('jade leech', 'Creature — Leech').
card_types('jade leech', ['Creature']).
card_subtypes('jade leech', ['Leech']).
card_colors('jade leech', ['G']).
card_text('jade leech', 'Green spells you cast cost {G} more to cast.').
card_mana_cost('jade leech', ['2', 'G', 'G']).
card_cmc('jade leech', 4).
card_layout('jade leech', 'normal').
card_power('jade leech', 5).
card_toughness('jade leech', 5).

% Found in: C13, DDH, M12
card_name('jade mage', 'Jade Mage').
card_type('jade mage', 'Creature — Human Shaman').
card_types('jade mage', ['Creature']).
card_subtypes('jade mage', ['Human', 'Shaman']).
card_colors('jade mage', ['G']).
card_text('jade mage', '{2}{G}: Put a 1/1 green Saproling creature token onto the battlefield.').
card_mana_cost('jade mage', ['1', 'G']).
card_cmc('jade mage', 2).
card_layout('jade mage', 'normal').
card_power('jade mage', 2).
card_toughness('jade mage', 1).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, CED, CEI, LEA, LEB, ME4
card_name('jade monolith', 'Jade Monolith').
card_type('jade monolith', 'Artifact').
card_types('jade monolith', ['Artifact']).
card_subtypes('jade monolith', []).
card_colors('jade monolith', []).
card_text('jade monolith', '{1}: The next time a source of your choice would deal damage to target creature this turn, that source deals that damage to you instead.').
card_mana_cost('jade monolith', ['4']).
card_cmc('jade monolith', 4).
card_layout('jade monolith', 'normal').

% Found in: 2ED, 9ED, CED, CEI, LEA, LEB
card_name('jade statue', 'Jade Statue').
card_type('jade statue', 'Artifact').
card_types('jade statue', ['Artifact']).
card_subtypes('jade statue', []).
card_colors('jade statue', []).
card_text('jade statue', '{2}: Jade Statue becomes a 3/6 Golem artifact creature until end of combat. Activate this ability only during combat.').
card_mana_cost('jade statue', ['4']).
card_cmc('jade statue', 4).
card_layout('jade statue', 'normal').

% Found in: APC
card_name('jaded response', 'Jaded Response').
card_type('jaded response', 'Instant').
card_types('jaded response', ['Instant']).
card_subtypes('jaded response', []).
card_colors('jaded response', ['U']).
card_text('jaded response', 'Counter target spell if it shares a color with a creature you control.').
card_mana_cost('jaded response', ['1', 'U']).
card_cmc('jaded response', 2).
card_layout('jaded response', 'normal').

% Found in: PO2, S99, USG
card_name('jagged lightning', 'Jagged Lightning').
card_type('jagged lightning', 'Sorcery').
card_types('jagged lightning', ['Sorcery']).
card_subtypes('jagged lightning', []).
card_colors('jagged lightning', ['R']).
card_text('jagged lightning', 'Jagged Lightning deals 3 damage to each of two target creatures.').
card_mana_cost('jagged lightning', ['3', 'R', 'R']).
card_cmc('jagged lightning', 5).
card_layout('jagged lightning', 'normal').

% Found in: DIS
card_name('jagged poppet', 'Jagged Poppet').
card_type('jagged poppet', 'Creature — Ogre Warrior').
card_types('jagged poppet', ['Creature']).
card_subtypes('jagged poppet', ['Ogre', 'Warrior']).
card_colors('jagged poppet', ['B', 'R']).
card_text('jagged poppet', 'Whenever Jagged Poppet is dealt damage, discard that many cards.\nHellbent — Whenever Jagged Poppet deals combat damage to a player, if you have no cards in hand, that player discards cards equal to the damage.').
card_mana_cost('jagged poppet', ['1', 'B', 'R']).
card_cmc('jagged poppet', 3).
card_layout('jagged poppet', 'normal').
card_power('jagged poppet', 3).
card_toughness('jagged poppet', 4).

% Found in: DPA, LRW
card_name('jagged-scar archers', 'Jagged-Scar Archers').
card_type('jagged-scar archers', 'Creature — Elf Archer').
card_types('jagged-scar archers', ['Creature']).
card_subtypes('jagged-scar archers', ['Elf', 'Archer']).
card_colors('jagged-scar archers', ['G']).
card_text('jagged-scar archers', 'Jagged-Scar Archers\'s power and toughness are each equal to the number of Elves you control.\n{T}: Jagged-Scar Archers deals damage equal to its power to target creature with flying.').
card_mana_cost('jagged-scar archers', ['1', 'G', 'G']).
card_cmc('jagged-scar archers', 3).
card_layout('jagged-scar archers', 'normal').
card_power('jagged-scar archers', '*').
card_toughness('jagged-scar archers', '*').

% Found in: WWK
card_name('jagwasp swarm', 'Jagwasp Swarm').
card_type('jagwasp swarm', 'Creature — Insect').
card_types('jagwasp swarm', ['Creature']).
card_subtypes('jagwasp swarm', ['Insect']).
card_colors('jagwasp swarm', ['B']).
card_text('jagwasp swarm', 'Flying').
card_mana_cost('jagwasp swarm', ['3', 'B']).
card_cmc('jagwasp swarm', 4).
card_layout('jagwasp swarm', 'normal').
card_power('jagwasp swarm', 3).
card_toughness('jagwasp swarm', 2).

% Found in: M15
card_name('jalira, master polymorphist', 'Jalira, Master Polymorphist').
card_type('jalira, master polymorphist', 'Legendary Creature — Human Wizard').
card_types('jalira, master polymorphist', ['Creature']).
card_subtypes('jalira, master polymorphist', ['Human', 'Wizard']).
card_supertypes('jalira, master polymorphist', ['Legendary']).
card_colors('jalira, master polymorphist', ['U']).
card_text('jalira, master polymorphist', '{2}{U}, {T}, Sacrifice another creature: Reveal cards from the top of your library until you reveal a nonlegendary creature card. Put that card onto the battlefield and the rest on the bottom of your library in a random order.').
card_mana_cost('jalira, master polymorphist', ['3', 'U']).
card_cmc('jalira, master polymorphist', 4).
card_layout('jalira, master polymorphist', 'normal').
card_power('jalira, master polymorphist', 2).
card_toughness('jalira, master polymorphist', 2).

% Found in: UGL
card_name('jalum grifter', 'Jalum Grifter').
card_type('jalum grifter', 'Creature — Legend').
card_types('jalum grifter', ['Creature']).
card_subtypes('jalum grifter', ['Legend']).
card_colors('jalum grifter', ['R']).
card_text('jalum grifter', '{1}{R}, {T}: Put Jalum Grifter and two lands you control face down in front of target opponent after revealing each card to him or her. Then, rearrange the order of the three cards as often as you wish, keeping them on the table at all times. That opponent then chooses one of those cards. If a land is chosen, destroy target card in play. Otherwise, sacrifice Jalum Grifter.').
card_mana_cost('jalum grifter', ['3', 'R', 'R']).
card_cmc('jalum grifter', 5).
card_layout('jalum grifter', 'normal').
card_power('jalum grifter', 3).
card_toughness('jalum grifter', 5).

% Found in: 5ED, 6ED, 7ED, ATH, ATQ, C14, CHR
card_name('jalum tome', 'Jalum Tome').
card_type('jalum tome', 'Artifact').
card_types('jalum tome', ['Artifact']).
card_subtypes('jalum tome', []).
card_colors('jalum tome', []).
card_text('jalum tome', '{2}, {T}: Draw a card, then discard a card.').
card_mana_cost('jalum tome', ['3']).
card_cmc('jalum tome', 3).
card_layout('jalum tome', 'normal').

% Found in: VIS
card_name('jamuraan lion', 'Jamuraan Lion').
card_type('jamuraan lion', 'Creature — Cat').
card_types('jamuraan lion', ['Creature']).
card_subtypes('jamuraan lion', ['Cat']).
card_colors('jamuraan lion', ['W']).
card_text('jamuraan lion', '{W}, {T}: Target creature can\'t block this turn.').
card_mana_cost('jamuraan lion', ['2', 'W']).
card_cmc('jamuraan lion', 3).
card_layout('jamuraan lion', 'normal').
card_power('jamuraan lion', 3).
card_toughness('jamuraan lion', 1).

% Found in: 3ED, ARN
card_name('jandor\'s ring', 'Jandor\'s Ring').
card_type('jandor\'s ring', 'Artifact').
card_types('jandor\'s ring', ['Artifact']).
card_subtypes('jandor\'s ring', []).
card_colors('jandor\'s ring', []).
card_text('jandor\'s ring', '{2}, {T}, Discard the last card you drew this turn: Draw a card.').
card_mana_cost('jandor\'s ring', ['6']).
card_cmc('jandor\'s ring', 6).
card_layout('jandor\'s ring', 'normal').

% Found in: 3ED, 4ED, 5ED, 7ED, ARN
card_name('jandor\'s saddlebags', 'Jandor\'s Saddlebags').
card_type('jandor\'s saddlebags', 'Artifact').
card_types('jandor\'s saddlebags', ['Artifact']).
card_subtypes('jandor\'s saddlebags', []).
card_colors('jandor\'s saddlebags', []).
card_text('jandor\'s saddlebags', '{3}, {T}: Untap target creature.').
card_mana_cost('jandor\'s saddlebags', ['2']).
card_cmc('jandor\'s saddlebags', 2).
card_layout('jandor\'s saddlebags', 'normal').

% Found in: WTH
card_name('jangling automaton', 'Jangling Automaton').
card_type('jangling automaton', 'Artifact Creature — Construct').
card_types('jangling automaton', ['Artifact', 'Creature']).
card_subtypes('jangling automaton', ['Construct']).
card_colors('jangling automaton', []).
card_text('jangling automaton', 'Whenever Jangling Automaton attacks, untap all creatures defending player controls.').
card_mana_cost('jangling automaton', ['3']).
card_cmc('jangling automaton', 3).
card_layout('jangling automaton', 'normal').
card_power('jangling automaton', 3).
card_toughness('jangling automaton', 2).

% Found in: C13, DKA
card_name('jar of eyeballs', 'Jar of Eyeballs').
card_type('jar of eyeballs', 'Artifact').
card_types('jar of eyeballs', ['Artifact']).
card_subtypes('jar of eyeballs', []).
card_colors('jar of eyeballs', []).
card_text('jar of eyeballs', 'Whenever a creature you control dies, put two eyeball counters on Jar of Eyeballs.\n{3}, {T}, Remove all eyeball counters from Jar of Eyeballs: Look at the top X cards of your library, where X is the number of eyeball counters removed this way. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('jar of eyeballs', ['3']).
card_cmc('jar of eyeballs', 3).
card_layout('jar of eyeballs', 'normal').

% Found in: RTR
card_name('jarad\'s orders', 'Jarad\'s Orders').
card_type('jarad\'s orders', 'Sorcery').
card_types('jarad\'s orders', ['Sorcery']).
card_subtypes('jarad\'s orders', []).
card_colors('jarad\'s orders', ['B', 'G']).
card_text('jarad\'s orders', 'Search your library for up to two creature cards and reveal them. Put one into your hand and the other into your graveyard. Then shuffle your library.').
card_mana_cost('jarad\'s orders', ['2', 'B', 'G']).
card_cmc('jarad\'s orders', 4).
card_layout('jarad\'s orders', 'normal').

% Found in: DDJ, RTR
card_name('jarad, golgari lich lord', 'Jarad, Golgari Lich Lord').
card_type('jarad, golgari lich lord', 'Legendary Creature — Zombie Elf').
card_types('jarad, golgari lich lord', ['Creature']).
card_subtypes('jarad, golgari lich lord', ['Zombie', 'Elf']).
card_supertypes('jarad, golgari lich lord', ['Legendary']).
card_colors('jarad, golgari lich lord', ['B', 'G']).
card_text('jarad, golgari lich lord', 'Jarad, Golgari Lich Lord gets +1/+1 for each creature card in your graveyard.\n{1}{B}{G}, Sacrifice another creature: Each opponent loses life equal to the sacrificed creature\'s power.\nSacrifice a Swamp and a Forest: Return Jarad from your graveyard to your hand.').
card_mana_cost('jarad, golgari lich lord', ['B', 'B', 'G', 'G']).
card_cmc('jarad, golgari lich lord', 4).
card_layout('jarad, golgari lich lord', 'normal').
card_power('jarad, golgari lich lord', 2).
card_toughness('jarad, golgari lich lord', 2).

% Found in: BOK
card_name('jaraku the interloper', 'Jaraku the Interloper').
card_type('jaraku the interloper', 'Legendary Creature — Spirit').
card_types('jaraku the interloper', ['Creature']).
card_subtypes('jaraku the interloper', ['Spirit']).
card_supertypes('jaraku the interloper', ['Legendary']).
card_colors('jaraku the interloper', ['U']).
card_text('jaraku the interloper', 'Remove a ki counter from Jaraku the Interloper: Counter target spell unless its controller pays {2}.').
card_mana_cost('jaraku the interloper', ['1', 'U', 'U']).
card_cmc('jaraku the interloper', 3).
card_layout('jaraku the interloper', 'flip').
card_power('jaraku the interloper', 3).
card_toughness('jaraku the interloper', 4).

% Found in: ONS, VMA
card_name('jareth, leonine titan', 'Jareth, Leonine Titan').
card_type('jareth, leonine titan', 'Legendary Creature — Cat Giant').
card_types('jareth, leonine titan', ['Creature']).
card_subtypes('jareth, leonine titan', ['Cat', 'Giant']).
card_supertypes('jareth, leonine titan', ['Legendary']).
card_colors('jareth, leonine titan', ['W']).
card_text('jareth, leonine titan', 'Whenever Jareth, Leonine Titan blocks, it gets +7/+7 until end of turn.\n{W}: Jareth gains protection from the color of your choice until end of turn.').
card_mana_cost('jareth, leonine titan', ['3', 'W', 'W', 'W']).
card_cmc('jareth, leonine titan', 6).
card_layout('jareth, leonine titan', 'normal').
card_power('jareth, leonine titan', 4).
card_toughness('jareth, leonine titan', 7).

% Found in: LEG, TSB
card_name('jasmine boreal', 'Jasmine Boreal').
card_type('jasmine boreal', 'Legendary Creature — Human').
card_types('jasmine boreal', ['Creature']).
card_subtypes('jasmine boreal', ['Human']).
card_supertypes('jasmine boreal', ['Legendary']).
card_colors('jasmine boreal', ['W', 'G']).
card_text('jasmine boreal', '').
card_mana_cost('jasmine boreal', ['3', 'G', 'W']).
card_cmc('jasmine boreal', 5).
card_layout('jasmine boreal', 'normal').
card_power('jasmine boreal', 4).
card_toughness('jasmine boreal', 5).

% Found in: UDS
card_name('jasmine seer', 'Jasmine Seer').
card_type('jasmine seer', 'Creature — Human Wizard').
card_types('jasmine seer', ['Creature']).
card_subtypes('jasmine seer', ['Human', 'Wizard']).
card_colors('jasmine seer', ['W']).
card_text('jasmine seer', '{2}{W}, {T}: Reveal any number of white cards in your hand. You gain 2 life for each card revealed this way.').
card_mana_cost('jasmine seer', ['3', 'W']).
card_cmc('jasmine seer', 4).
card_layout('jasmine seer', 'normal').
card_power('jasmine seer', 1).
card_toughness('jasmine seer', 1).

% Found in: EVE
card_name('jawbone skulkin', 'Jawbone Skulkin').
card_type('jawbone skulkin', 'Artifact Creature — Scarecrow').
card_types('jawbone skulkin', ['Artifact', 'Creature']).
card_subtypes('jawbone skulkin', ['Scarecrow']).
card_colors('jawbone skulkin', []).
card_text('jawbone skulkin', '{2}: Target red creature gains haste until end of turn.').
card_mana_cost('jawbone skulkin', ['1']).
card_cmc('jawbone skulkin', 1).
card_layout('jawbone skulkin', 'normal').
card_power('jawbone skulkin', 1).
card_toughness('jawbone skulkin', 1).

% Found in: DDG, DDI, SHM
card_name('jaws of stone', 'Jaws of Stone').
card_type('jaws of stone', 'Sorcery').
card_types('jaws of stone', ['Sorcery']).
card_subtypes('jaws of stone', []).
card_colors('jaws of stone', ['R']).
card_text('jaws of stone', 'Jaws of Stone deals X damage divided as you choose among any number of target creatures and/or players, where X is the number of Mountains you control as you cast Jaws of Stone.').
card_mana_cost('jaws of stone', ['5', 'R']).
card_cmc('jaws of stone', 6).
card_layout('jaws of stone', 'normal').

% Found in: VAN
card_name('jaya ballard avatar', 'Jaya Ballard Avatar').
card_type('jaya ballard avatar', 'Vanguard').
card_types('jaya ballard avatar', ['Vanguard']).
card_subtypes('jaya ballard avatar', []).
card_colors('jaya ballard avatar', []).
card_text('jaya ballard avatar', '{X}: Jaya Ballard Avatar deals an amount of damage chosen at random from 0 to X to target creature or player. Activate this ability only once each turn.').
card_layout('jaya ballard avatar', 'vanguard').

% Found in: PD2, TSP, pMEI
card_name('jaya ballard, task mage', 'Jaya Ballard, Task Mage').
card_type('jaya ballard, task mage', 'Legendary Creature — Human Spellshaper').
card_types('jaya ballard, task mage', ['Creature']).
card_subtypes('jaya ballard, task mage', ['Human', 'Spellshaper']).
card_supertypes('jaya ballard, task mage', ['Legendary']).
card_colors('jaya ballard, task mage', ['R']).
card_text('jaya ballard, task mage', '{R}, {T}, Discard a card: Destroy target blue permanent.\n{1}{R}, {T}, Discard a card: Jaya Ballard, Task Mage deals 3 damage to target creature or player. A creature dealt damage this way can\'t be regenerated this turn.\n{5}{R}{R}, {T}, Discard a card: Jaya Ballard deals 6 damage to each creature and each player.').
card_mana_cost('jaya ballard, task mage', ['1', 'R', 'R']).
card_cmc('jaya ballard, task mage', 3).
card_layout('jaya ballard, task mage', 'normal').
card_power('jaya ballard, task mage', 2).
card_toughness('jaya ballard, task mage', 2).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, CED, CEI, ITP, LEA, LEB, M13, ORI, RQS
card_name('jayemdae tome', 'Jayemdae Tome').
card_type('jayemdae tome', 'Artifact').
card_types('jayemdae tome', ['Artifact']).
card_subtypes('jayemdae tome', []).
card_colors('jayemdae tome', []).
card_text('jayemdae tome', '{4}, {T}: Draw a card.').
card_mana_cost('jayemdae tome', ['4']).
card_cmc('jayemdae tome', 4).
card_layout('jayemdae tome', 'normal').

% Found in: C14
card_name('jazal goldmane', 'Jazal Goldmane').
card_type('jazal goldmane', 'Legendary Creature — Cat Warrior').
card_types('jazal goldmane', ['Creature']).
card_subtypes('jazal goldmane', ['Cat', 'Warrior']).
card_supertypes('jazal goldmane', ['Legendary']).
card_colors('jazal goldmane', ['W']).
card_text('jazal goldmane', 'First strike\n{3}{W}{W}: Attacking creatures you control get +X/+X until end of turn, where X is the number of attacking creatures.').
card_mana_cost('jazal goldmane', ['2', 'W', 'W']).
card_cmc('jazal goldmane', 4).
card_layout('jazal goldmane', 'normal').
card_power('jazal goldmane', 4).
card_toughness('jazal goldmane', 4).

% Found in: LEG, ME3
card_name('jedit ojanen', 'Jedit Ojanen').
card_type('jedit ojanen', 'Legendary Creature — Cat Warrior').
card_types('jedit ojanen', ['Creature']).
card_subtypes('jedit ojanen', ['Cat', 'Warrior']).
card_supertypes('jedit ojanen', ['Legendary']).
card_colors('jedit ojanen', ['W', 'U']).
card_text('jedit ojanen', '').
card_mana_cost('jedit ojanen', ['4', 'W', 'W', 'U']).
card_cmc('jedit ojanen', 7).
card_layout('jedit ojanen', 'normal').
card_power('jedit ojanen', 5).
card_toughness('jedit ojanen', 5).

% Found in: PLC
card_name('jedit ojanen of efrava', 'Jedit Ojanen of Efrava').
card_type('jedit ojanen of efrava', 'Legendary Creature — Cat Warrior').
card_types('jedit ojanen of efrava', ['Creature']).
card_subtypes('jedit ojanen of efrava', ['Cat', 'Warrior']).
card_supertypes('jedit ojanen of efrava', ['Legendary']).
card_colors('jedit ojanen of efrava', ['G']).
card_text('jedit ojanen of efrava', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)\nWhenever Jedit Ojanen of Efrava attacks or blocks, put a 2/2 green Cat Warrior creature token with forestwalk onto the battlefield.').
card_mana_cost('jedit ojanen of efrava', ['3', 'G', 'G', 'G']).
card_cmc('jedit ojanen of efrava', 6).
card_layout('jedit ojanen of efrava', 'normal').
card_power('jedit ojanen of efrava', 5).
card_toughness('jedit ojanen of efrava', 5).

% Found in: DDI, TSP
card_name('jedit\'s dragoons', 'Jedit\'s Dragoons').
card_type('jedit\'s dragoons', 'Creature — Cat Soldier').
card_types('jedit\'s dragoons', ['Creature']).
card_subtypes('jedit\'s dragoons', ['Cat', 'Soldier']).
card_colors('jedit\'s dragoons', ['W']).
card_text('jedit\'s dragoons', 'Vigilance\nWhen Jedit\'s Dragoons enters the battlefield, you gain 4 life.').
card_mana_cost('jedit\'s dragoons', ['5', 'W']).
card_cmc('jedit\'s dragoons', 6).
card_layout('jedit\'s dragoons', 'normal').
card_power('jedit\'s dragoons', 2).
card_toughness('jedit\'s dragoons', 5).

% Found in: FRF_UGIN, KTK, pPRE
card_name('jeering instigator', 'Jeering Instigator').
card_type('jeering instigator', 'Creature — Goblin Rogue').
card_types('jeering instigator', ['Creature']).
card_subtypes('jeering instigator', ['Goblin', 'Rogue']).
card_colors('jeering instigator', ['R']).
card_text('jeering instigator', 'Morph {2}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Jeering Instigator is turned face up, if it\'s your turn, gain control of another target creature until end of turn. Untap that creature. It gains haste until end of turn.').
card_mana_cost('jeering instigator', ['1', 'R']).
card_cmc('jeering instigator', 2).
card_layout('jeering instigator', 'normal').
card_power('jeering instigator', 2).
card_toughness('jeering instigator', 1).

% Found in: DGM
card_name('jelenn sphinx', 'Jelenn Sphinx').
card_type('jelenn sphinx', 'Creature — Sphinx').
card_types('jelenn sphinx', ['Creature']).
card_subtypes('jelenn sphinx', ['Sphinx']).
card_colors('jelenn sphinx', ['W', 'U']).
card_text('jelenn sphinx', 'Flying, vigilance\nWhenever Jelenn Sphinx attacks, other attacking creatures get +1/+1 until end of turn.').
card_mana_cost('jelenn sphinx', ['3', 'W', 'U']).
card_cmc('jelenn sphinx', 5).
card_layout('jelenn sphinx', 'normal').
card_power('jelenn sphinx', 1).
card_toughness('jelenn sphinx', 5).

% Found in: C13
card_name('jeleva, nephalia\'s scourge', 'Jeleva, Nephalia\'s Scourge').
card_type('jeleva, nephalia\'s scourge', 'Legendary Creature — Vampire Wizard').
card_types('jeleva, nephalia\'s scourge', ['Creature']).
card_subtypes('jeleva, nephalia\'s scourge', ['Vampire', 'Wizard']).
card_supertypes('jeleva, nephalia\'s scourge', ['Legendary']).
card_colors('jeleva, nephalia\'s scourge', ['U', 'B', 'R']).
card_text('jeleva, nephalia\'s scourge', 'Flying\nWhen Jeleva, Nephalia\'s Scourge enters the battlefield, each player exiles the top X cards of his or her library, where X is the amount of mana spent to cast Jeleva.\nWhenever Jeleva attacks, you may cast an instant or sorcery card exiled with it without paying its mana cost.').
card_mana_cost('jeleva, nephalia\'s scourge', ['1', 'U', 'B', 'R']).
card_cmc('jeleva, nephalia\'s scourge', 4).
card_layout('jeleva, nephalia\'s scourge', 'normal').
card_power('jeleva, nephalia\'s scourge', 1).
card_toughness('jeleva, nephalia\'s scourge', 3).

% Found in: ARB, V15
card_name('jenara, asura of war', 'Jenara, Asura of War').
card_type('jenara, asura of war', 'Legendary Creature — Angel').
card_types('jenara, asura of war', ['Creature']).
card_subtypes('jenara, asura of war', ['Angel']).
card_supertypes('jenara, asura of war', ['Legendary']).
card_colors('jenara, asura of war', ['W', 'U', 'G']).
card_text('jenara, asura of war', 'Flying\n{1}{W}: Put a +1/+1 counter on Jenara, Asura of War.').
card_mana_cost('jenara, asura of war', ['G', 'W', 'U']).
card_cmc('jenara, asura of war', 3).
card_layout('jenara, asura of war', 'normal').
card_power('jenara, asura of war', 3).
card_toughness('jenara, asura of war', 3).

% Found in: LEG, ME3
card_name('jerrard of the closed fist', 'Jerrard of the Closed Fist').
card_type('jerrard of the closed fist', 'Legendary Creature — Human Knight').
card_types('jerrard of the closed fist', ['Creature']).
card_subtypes('jerrard of the closed fist', ['Human', 'Knight']).
card_supertypes('jerrard of the closed fist', ['Legendary']).
card_colors('jerrard of the closed fist', ['R', 'G']).
card_text('jerrard of the closed fist', '').
card_mana_cost('jerrard of the closed fist', ['3', 'R', 'G', 'G']).
card_cmc('jerrard of the closed fist', 6).
card_layout('jerrard of the closed fist', 'normal').
card_power('jerrard of the closed fist', 6).
card_toughness('jerrard of the closed fist', 5).

% Found in: JUD
card_name('jeska, warrior adept', 'Jeska, Warrior Adept').
card_type('jeska, warrior adept', 'Legendary Creature — Human Barbarian Warrior').
card_types('jeska, warrior adept', ['Creature']).
card_subtypes('jeska, warrior adept', ['Human', 'Barbarian', 'Warrior']).
card_supertypes('jeska, warrior adept', ['Legendary']).
card_colors('jeska, warrior adept', ['R']).
card_text('jeska, warrior adept', 'First strike, haste\n{T}: Jeska, Warrior Adept deals 1 damage to target creature or player.').
card_mana_cost('jeska, warrior adept', ['2', 'R', 'R']).
card_cmc('jeska, warrior adept', 4).
card_layout('jeska, warrior adept', 'normal').
card_power('jeska, warrior adept', 3).
card_toughness('jeska, warrior adept', 1).

% Found in: KTK, pPRE
card_name('jeskai ascendancy', 'Jeskai Ascendancy').
card_type('jeskai ascendancy', 'Enchantment').
card_types('jeskai ascendancy', ['Enchantment']).
card_subtypes('jeskai ascendancy', []).
card_colors('jeskai ascendancy', ['W', 'U', 'R']).
card_text('jeskai ascendancy', 'Whenever you cast a noncreature spell, creatures you control get +1/+1 until end of turn. Untap those creatures.\nWhenever you cast a noncreature spell, you may draw a card. If you do, discard a card.').
card_mana_cost('jeskai ascendancy', ['U', 'R', 'W']).
card_cmc('jeskai ascendancy', 3).
card_layout('jeskai ascendancy', 'normal').

% Found in: KTK
card_name('jeskai banner', 'Jeskai Banner').
card_type('jeskai banner', 'Artifact').
card_types('jeskai banner', ['Artifact']).
card_subtypes('jeskai banner', []).
card_colors('jeskai banner', []).
card_text('jeskai banner', '{T}: Add {U}, {R}, or {W} to your mana pool.\n{U}{R}{W}, {T}, Sacrifice Jeskai Banner: Draw a card.').
card_mana_cost('jeskai banner', ['3']).
card_cmc('jeskai banner', 3).
card_layout('jeskai banner', 'normal').

% Found in: FRF
card_name('jeskai barricade', 'Jeskai Barricade').
card_type('jeskai barricade', 'Creature — Wall').
card_types('jeskai barricade', ['Creature']).
card_subtypes('jeskai barricade', ['Wall']).
card_colors('jeskai barricade', ['W']).
card_text('jeskai barricade', 'Flash (You may cast this spell any time you could cast an instant.)\nDefender\nWhen Jeskai Barricade enters the battlefield, you may return another target creature you control to its owner\'s hand.').
card_mana_cost('jeskai barricade', ['1', 'W']).
card_cmc('jeskai barricade', 2).
card_layout('jeskai barricade', 'normal').
card_power('jeskai barricade', 0).
card_toughness('jeskai barricade', 4).

% Found in: KTK
card_name('jeskai charm', 'Jeskai Charm').
card_type('jeskai charm', 'Instant').
card_types('jeskai charm', ['Instant']).
card_subtypes('jeskai charm', []).
card_colors('jeskai charm', ['W', 'U', 'R']).
card_text('jeskai charm', 'Choose one —\n• Put target creature on top of its owner\'s library.\n• Jeskai Charm deals 4 damage to target opponent.\n• Creatures you control get +1/+1 and gain lifelink until end of turn.').
card_mana_cost('jeskai charm', ['U', 'R', 'W']).
card_cmc('jeskai charm', 3).
card_layout('jeskai charm', 'normal').

% Found in: DDN, KTK
card_name('jeskai elder', 'Jeskai Elder').
card_type('jeskai elder', 'Creature — Human Monk').
card_types('jeskai elder', ['Creature']).
card_subtypes('jeskai elder', ['Human', 'Monk']).
card_colors('jeskai elder', ['U']).
card_text('jeskai elder', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhenever Jeskai Elder deals combat damage to a player, you may draw a card. If you do, discard a card.').
card_mana_cost('jeskai elder', ['1', 'U']).
card_cmc('jeskai elder', 2).
card_layout('jeskai elder', 'normal').
card_power('jeskai elder', 1).
card_toughness('jeskai elder', 2).

% Found in: FRF, FRF_UGIN
card_name('jeskai infiltrator', 'Jeskai Infiltrator').
card_type('jeskai infiltrator', 'Creature — Human Monk').
card_types('jeskai infiltrator', ['Creature']).
card_subtypes('jeskai infiltrator', ['Human', 'Monk']).
card_colors('jeskai infiltrator', ['U']).
card_text('jeskai infiltrator', 'Jeskai Infiltrator can\'t be blocked as long as you control no other creatures.\nWhen Jeskai Infiltrator deals combat damage to a player, exile it and the top card of your library in a face-down pile, shuffle that pile, then manifest those cards. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_mana_cost('jeskai infiltrator', ['2', 'U']).
card_cmc('jeskai infiltrator', 3).
card_layout('jeskai infiltrator', 'normal').
card_power('jeskai infiltrator', 2).
card_toughness('jeskai infiltrator', 3).

% Found in: FRF
card_name('jeskai runemark', 'Jeskai Runemark').
card_type('jeskai runemark', 'Enchantment — Aura').
card_types('jeskai runemark', ['Enchantment']).
card_subtypes('jeskai runemark', ['Aura']).
card_colors('jeskai runemark', ['U']).
card_text('jeskai runemark', 'Enchant creature\nEnchanted creature gets +2/+2.\nEnchanted creature has flying as long as you control a red or white permanent.').
card_mana_cost('jeskai runemark', ['2', 'U']).
card_cmc('jeskai runemark', 3).
card_layout('jeskai runemark', 'normal').

% Found in: FRF
card_name('jeskai sage', 'Jeskai Sage').
card_type('jeskai sage', 'Creature — Human Monk').
card_types('jeskai sage', ['Creature']).
card_subtypes('jeskai sage', ['Human', 'Monk']).
card_colors('jeskai sage', ['U']).
card_text('jeskai sage', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhen Jeskai Sage dies, draw a card.').
card_mana_cost('jeskai sage', ['1', 'U']).
card_cmc('jeskai sage', 2).
card_layout('jeskai sage', 'normal').
card_power('jeskai sage', 1).
card_toughness('jeskai sage', 1).

% Found in: KTK
card_name('jeskai student', 'Jeskai Student').
card_type('jeskai student', 'Creature — Human Monk').
card_types('jeskai student', ['Creature']).
card_subtypes('jeskai student', ['Human', 'Monk']).
card_colors('jeskai student', ['W']).
card_text('jeskai student', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_mana_cost('jeskai student', ['1', 'W']).
card_cmc('jeskai student', 2).
card_layout('jeskai student', 'normal').
card_power('jeskai student', 1).
card_toughness('jeskai student', 3).

% Found in: KTK
card_name('jeskai windscout', 'Jeskai Windscout').
card_type('jeskai windscout', 'Creature — Bird Scout').
card_types('jeskai windscout', ['Creature']).
card_subtypes('jeskai windscout', ['Bird', 'Scout']).
card_colors('jeskai windscout', ['U']).
card_text('jeskai windscout', 'Flying\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_mana_cost('jeskai windscout', ['2', 'U']).
card_cmc('jeskai windscout', 3).
card_layout('jeskai windscout', 'normal').
card_power('jeskai windscout', 2).
card_toughness('jeskai windscout', 1).

% Found in: 5ED, 9ED, ICE, V10
card_name('jester\'s cap', 'Jester\'s Cap').
card_type('jester\'s cap', 'Artifact').
card_types('jester\'s cap', ['Artifact']).
card_subtypes('jester\'s cap', []).
card_colors('jester\'s cap', []).
card_text('jester\'s cap', '{2}, {T}, Sacrifice Jester\'s Cap: Search target player\'s library for three cards and exile them. Then that player shuffles his or her library.').
card_mana_cost('jester\'s cap', ['4']).
card_cmc('jester\'s cap', 4).
card_layout('jester\'s cap', 'normal').

% Found in: ICE, ME2
card_name('jester\'s mask', 'Jester\'s Mask').
card_type('jester\'s mask', 'Artifact').
card_types('jester\'s mask', ['Artifact']).
card_subtypes('jester\'s mask', []).
card_colors('jester\'s mask', []).
card_text('jester\'s mask', 'Jester\'s Mask enters the battlefield tapped.\n{1}, {T}, Sacrifice Jester\'s Mask: Target opponent puts the cards from his or her hand on top of his or her library. Search that player\'s library for that many cards. That player puts those cards into his or her hand, then shuffles his or her library.').
card_mana_cost('jester\'s mask', ['5']).
card_cmc('jester\'s mask', 5).
card_layout('jester\'s mask', 'normal').
card_reserved('jester\'s mask').

% Found in: CSP
card_name('jester\'s scepter', 'Jester\'s Scepter').
card_type('jester\'s scepter', 'Artifact').
card_types('jester\'s scepter', ['Artifact']).
card_subtypes('jester\'s scepter', []).
card_colors('jester\'s scepter', []).
card_text('jester\'s scepter', 'When Jester\'s Scepter enters the battlefield, exile the top five cards of target player\'s library face down. You may look at those cards for as long as they remain exiled.\n{2}, {T}, Put a card exiled with Jester\'s Scepter into its owner\'s graveyard: Counter target spell if it has the same name as that card.').
card_mana_cost('jester\'s scepter', ['3']).
card_cmc('jester\'s scepter', 3).
card_layout('jester\'s scepter', 'normal').

% Found in: UGL
card_name('jester\'s sombrero', 'Jester\'s Sombrero').
card_type('jester\'s sombrero', 'Artifact').
card_types('jester\'s sombrero', ['Artifact']).
card_subtypes('jester\'s sombrero', []).
card_colors('jester\'s sombrero', []).
card_text('jester\'s sombrero', '{2}, {T}, Sacrifice Jester\'s Sombrero: Look through target player\'s sideboard and remove any three of those cards from it for the remainder of the match.').
card_mana_cost('jester\'s sombrero', ['2']).
card_cmc('jester\'s sombrero', 2).
card_layout('jester\'s sombrero', 'normal').

% Found in: C14, TMP
card_name('jet medallion', 'Jet Medallion').
card_type('jet medallion', 'Artifact').
card_types('jet medallion', ['Artifact']).
card_subtypes('jet medallion', []).
card_colors('jet medallion', []).
card_text('jet medallion', 'Black spells you cast cost {1} less to cast.').
card_mana_cost('jet medallion', ['2']).
card_cmc('jet medallion', 2).
card_layout('jet medallion', 'normal').

% Found in: BOK, CNS
card_name('jetting glasskite', 'Jetting Glasskite').
card_type('jetting glasskite', 'Creature — Spirit').
card_types('jetting glasskite', ['Creature']).
card_subtypes('jetting glasskite', ['Spirit']).
card_colors('jetting glasskite', ['U']).
card_text('jetting glasskite', 'Flying\nWhenever Jetting Glasskite becomes the target of a spell or ability for the first time in a turn, counter that spell or ability.').
card_mana_cost('jetting glasskite', ['4', 'U', 'U']).
card_cmc('jetting glasskite', 6).
card_layout('jetting glasskite', 'normal').
card_power('jetting glasskite', 4).
card_toughness('jetting glasskite', 4).

% Found in: ICE, ME2
card_name('jeweled amulet', 'Jeweled Amulet').
card_type('jeweled amulet', 'Artifact').
card_types('jeweled amulet', ['Artifact']).
card_subtypes('jeweled amulet', []).
card_colors('jeweled amulet', []).
card_text('jeweled amulet', '{1}, {T}: Put a charge counter on Jeweled Amulet. Note the type of mana spent to pay this activation cost. Activate this ability only if there are no charge counters on Jeweled Amulet.\n{T}, Remove a charge counter from Jeweled Amulet: Add one mana of Jeweled Amulet\'s last noted type to your mana pool.').
card_mana_cost('jeweled amulet', ['0']).
card_cmc('jeweled amulet', 0).
card_layout('jeweled amulet', 'normal').

% Found in: ARN, CHR
card_name('jeweled bird', 'Jeweled Bird').
card_type('jeweled bird', 'Artifact').
card_types('jeweled bird', ['Artifact']).
card_subtypes('jeweled bird', []).
card_colors('jeweled bird', []).
card_text('jeweled bird', 'Remove Jeweled Bird from your deck before playing if you\'re not playing for ante.\n{T}: Ante Jeweled Bird. If you do, put all other cards you own from the ante into your graveyard, then draw a card.').
card_mana_cost('jeweled bird', ['1']).
card_cmc('jeweled bird', 1).
card_layout('jeweled bird', 'normal').

% Found in: PCY
card_name('jeweled spirit', 'Jeweled Spirit').
card_type('jeweled spirit', 'Creature — Spirit').
card_types('jeweled spirit', ['Creature']).
card_subtypes('jeweled spirit', ['Spirit']).
card_colors('jeweled spirit', ['W']).
card_text('jeweled spirit', 'Flying\nSacrifice two lands: Jeweled Spirit gains protection from artifacts or from the color of your choice until end of turn.').
card_mana_cost('jeweled spirit', ['3', 'W', 'W']).
card_cmc('jeweled spirit', 5).
card_layout('jeweled spirit', 'normal').
card_power('jeweled spirit', 3).
card_toughness('jeweled spirit', 3).

% Found in: MMQ
card_name('jeweled torque', 'Jeweled Torque').
card_type('jeweled torque', 'Artifact').
card_types('jeweled torque', ['Artifact']).
card_subtypes('jeweled torque', []).
card_colors('jeweled torque', []).
card_text('jeweled torque', 'As Jeweled Torque enters the battlefield, choose a color.\nWhenever a player casts a spell of the chosen color, you may pay {2}. If you do, you gain 2 life.').
card_mana_cost('jeweled torque', ['2']).
card_cmc('jeweled torque', 2).
card_layout('jeweled torque', 'normal').

% Found in: CON
card_name('jhessian balmgiver', 'Jhessian Balmgiver').
card_type('jhessian balmgiver', 'Creature — Human Cleric').
card_types('jhessian balmgiver', ['Creature']).
card_subtypes('jhessian balmgiver', ['Human', 'Cleric']).
card_colors('jhessian balmgiver', ['W', 'U']).
card_text('jhessian balmgiver', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\n{T}: Target creature can\'t be blocked this turn.').
card_mana_cost('jhessian balmgiver', ['1', 'W', 'U']).
card_cmc('jhessian balmgiver', 3).
card_layout('jhessian balmgiver', 'normal').
card_power('jhessian balmgiver', 1).
card_toughness('jhessian balmgiver', 1).

% Found in: ALA
card_name('jhessian infiltrator', 'Jhessian Infiltrator').
card_type('jhessian infiltrator', 'Creature — Human Rogue').
card_types('jhessian infiltrator', ['Creature']).
card_subtypes('jhessian infiltrator', ['Human', 'Rogue']).
card_colors('jhessian infiltrator', ['U', 'G']).
card_text('jhessian infiltrator', 'Jhessian Infiltrator can\'t be blocked.').
card_mana_cost('jhessian infiltrator', ['G', 'U']).
card_cmc('jhessian infiltrator', 2).
card_layout('jhessian infiltrator', 'normal').
card_power('jhessian infiltrator', 2).
card_toughness('jhessian infiltrator', 2).

% Found in: ALA
card_name('jhessian lookout', 'Jhessian Lookout').
card_type('jhessian lookout', 'Creature — Human Scout').
card_types('jhessian lookout', ['Creature']).
card_subtypes('jhessian lookout', ['Human', 'Scout']).
card_colors('jhessian lookout', ['U']).
card_text('jhessian lookout', '').
card_mana_cost('jhessian lookout', ['1', 'U']).
card_cmc('jhessian lookout', 2).
card_layout('jhessian lookout', 'normal').
card_power('jhessian lookout', 2).
card_toughness('jhessian lookout', 1).

% Found in: ORI
card_name('jhessian thief', 'Jhessian Thief').
card_type('jhessian thief', 'Creature — Human Rogue').
card_types('jhessian thief', ['Creature']).
card_subtypes('jhessian thief', ['Human', 'Rogue']).
card_colors('jhessian thief', ['U']).
card_text('jhessian thief', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhenever Jhessian Thief deals combat damage to a player, draw a card.').
card_mana_cost('jhessian thief', ['2', 'U']).
card_cmc('jhessian thief', 3).
card_layout('jhessian thief', 'normal').
card_power('jhessian thief', 1).
card_toughness('jhessian thief', 3).

% Found in: ARB, DDH
card_name('jhessian zombies', 'Jhessian Zombies').
card_type('jhessian zombies', 'Creature — Zombie').
card_types('jhessian zombies', ['Creature']).
card_subtypes('jhessian zombies', ['Zombie']).
card_colors('jhessian zombies', ['U', 'B']).
card_text('jhessian zombies', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nIslandcycling {2}, swampcycling {2} ({2}, Discard this card: Search your library for an Island or Swamp card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('jhessian zombies', ['4', 'U', 'B']).
card_cmc('jhessian zombies', 6).
card_layout('jhessian zombies', 'normal').
card_power('jhessian zombies', 2).
card_toughness('jhessian zombies', 4).

% Found in: FUT, MMA
card_name('jhoira of the ghitu', 'Jhoira of the Ghitu').
card_type('jhoira of the ghitu', 'Legendary Creature — Human Wizard').
card_types('jhoira of the ghitu', ['Creature']).
card_subtypes('jhoira of the ghitu', ['Human', 'Wizard']).
card_supertypes('jhoira of the ghitu', ['Legendary']).
card_colors('jhoira of the ghitu', ['U', 'R']).
card_text('jhoira of the ghitu', '{2}, Exile a nonland card from your hand: Put four time counters on the exiled card. If it doesn\'t have suspend, it gains suspend. (At the beginning of your upkeep, remove a time counter from that card. When the last is removed, cast it without paying its mana cost. If it\'s a creature, it has haste.)').
card_mana_cost('jhoira of the ghitu', ['1', 'U', 'R']).
card_cmc('jhoira of the ghitu', 3).
card_layout('jhoira of the ghitu', 'normal').
card_power('jhoira of the ghitu', 2).
card_toughness('jhoira of the ghitu', 2).

% Found in: VAN
card_name('jhoira of the ghitu avatar', 'Jhoira of the Ghitu Avatar').
card_type('jhoira of the ghitu avatar', 'Vanguard').
card_types('jhoira of the ghitu avatar', ['Vanguard']).
card_subtypes('jhoira of the ghitu avatar', []).
card_colors('jhoira of the ghitu avatar', []).
card_text('jhoira of the ghitu avatar', '{3}, Discard a card: Copy three instant cards chosen at random. You may cast one of the copies without paying its mana cost.\n{3}, Discard a card: Copy three sorcery cards chosen at random. You may cast one of the copies without paying its mana cost. Activate this ability only any time you could cast a sorcery.').
card_layout('jhoira of the ghitu avatar', 'vanguard').

% Found in: TSP
card_name('jhoira\'s timebug', 'Jhoira\'s Timebug').
card_type('jhoira\'s timebug', 'Artifact Creature — Insect').
card_types('jhoira\'s timebug', ['Artifact', 'Creature']).
card_subtypes('jhoira\'s timebug', ['Insect']).
card_colors('jhoira\'s timebug', []).
card_text('jhoira\'s timebug', '{T}: Choose target permanent you control or suspended card you own. If that permanent or card has a time counter on it, you may remove a time counter from it or put another time counter on it.').
card_mana_cost('jhoira\'s timebug', ['2']).
card_cmc('jhoira\'s timebug', 2).
card_layout('jhoira\'s timebug', 'normal').
card_power('jhoira\'s timebug', 1).
card_toughness('jhoira\'s timebug', 2).

% Found in: ULG
card_name('jhoira\'s toolbox', 'Jhoira\'s Toolbox').
card_type('jhoira\'s toolbox', 'Artifact Creature — Insect').
card_types('jhoira\'s toolbox', ['Artifact', 'Creature']).
card_subtypes('jhoira\'s toolbox', ['Insect']).
card_colors('jhoira\'s toolbox', []).
card_text('jhoira\'s toolbox', '{2}: Regenerate target artifact creature.').
card_mana_cost('jhoira\'s toolbox', ['2']).
card_cmc('jhoira\'s toolbox', 2).
card_layout('jhoira\'s toolbox', 'normal').
card_power('jhoira\'s toolbox', 1).
card_toughness('jhoira\'s toolbox', 1).

% Found in: MMQ
card_name('jhovall queen', 'Jhovall Queen').
card_type('jhovall queen', 'Creature — Cat Rebel').
card_types('jhovall queen', ['Creature']).
card_subtypes('jhovall queen', ['Cat', 'Rebel']).
card_colors('jhovall queen', ['W']).
card_text('jhovall queen', 'Vigilance').
card_mana_cost('jhovall queen', ['4', 'W', 'W']).
card_cmc('jhovall queen', 6).
card_layout('jhovall queen', 'normal').
card_power('jhovall queen', 4).
card_toughness('jhovall queen', 7).

% Found in: MMQ
card_name('jhovall rider', 'Jhovall Rider').
card_type('jhovall rider', 'Creature — Human Rebel').
card_types('jhovall rider', ['Creature']).
card_subtypes('jhovall rider', ['Human', 'Rebel']).
card_colors('jhovall rider', ['W']).
card_text('jhovall rider', 'Trample').
card_mana_cost('jhovall rider', ['4', 'W']).
card_cmc('jhovall rider', 5).
card_layout('jhovall rider', 'normal').
card_power('jhovall rider', 3).
card_toughness('jhovall rider', 3).

% Found in: ARN
card_name('jihad', 'Jihad').
card_type('jihad', 'Enchantment').
card_types('jihad', ['Enchantment']).
card_subtypes('jihad', []).
card_colors('jihad', ['W']).
card_text('jihad', 'As Jihad enters the battlefield, choose a color and an opponent.\nWhite creatures get +2/+1 as long as the chosen player controls a nontoken permanent of the chosen color.\nWhen the chosen player controls no nontoken permanents of the chosen color, sacrifice Jihad.').
card_mana_cost('jihad', ['W', 'W', 'W']).
card_cmc('jihad', 3).
card_layout('jihad', 'normal').
card_reserved('jihad').

% Found in: APC
card_name('jilt', 'Jilt').
card_type('jilt', 'Instant').
card_types('jilt', ['Instant']).
card_subtypes('jilt', []).
card_colors('jilt', ['U']).
card_text('jilt', 'Kicker {1}{R} (You may pay an additional {1}{R} as you cast this spell.)\nReturn target creature to its owner\'s hand. If Jilt was kicked, it deals 2 damage to another target creature.').
card_mana_cost('jilt', ['1', 'U']).
card_cmc('jilt', 2).
card_layout('jilt', 'normal').

% Found in: NPH
card_name('jin-gitaxias, core augur', 'Jin-Gitaxias, Core Augur').
card_type('jin-gitaxias, core augur', 'Legendary Creature — Praetor').
card_types('jin-gitaxias, core augur', ['Creature']).
card_subtypes('jin-gitaxias, core augur', ['Praetor']).
card_supertypes('jin-gitaxias, core augur', ['Legendary']).
card_colors('jin-gitaxias, core augur', ['U']).
card_text('jin-gitaxias, core augur', 'Flash\nAt the beginning of your end step, draw seven cards.\nEach opponent\'s maximum hand size is reduced by seven.').
card_mana_cost('jin-gitaxias, core augur', ['8', 'U', 'U']).
card_cmc('jin-gitaxias, core augur', 10).
card_layout('jin-gitaxias, core augur', 'normal').
card_power('jin-gitaxias, core augur', 5).
card_toughness('jin-gitaxias, core augur', 4).

% Found in: HML
card_name('jinx', 'Jinx').
card_type('jinx', 'Instant').
card_types('jinx', ['Instant']).
card_subtypes('jinx', []).
card_colors('jinx', ['U']).
card_text('jinx', 'Target land becomes the basic land type of your choice until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('jinx', ['1', 'U']).
card_cmc('jinx', 2).
card_layout('jinx', 'normal').

% Found in: MRD
card_name('jinxed choker', 'Jinxed Choker').
card_type('jinxed choker', 'Artifact').
card_types('jinxed choker', ['Artifact']).
card_subtypes('jinxed choker', []).
card_colors('jinxed choker', []).
card_text('jinxed choker', 'At the beginning of your end step, target opponent gains control of Jinxed Choker and puts a charge counter on it.\nAt the beginning of your upkeep, Jinxed Choker deals damage to you equal to the number of charge counters on it.\n{3}: Put a charge counter on Jinxed Choker or remove one from it.').
card_mana_cost('jinxed choker', ['3']).
card_cmc('jinxed choker', 3).
card_layout('jinxed choker', 'normal').

% Found in: M11, TMP, TPR
card_name('jinxed idol', 'Jinxed Idol').
card_type('jinxed idol', 'Artifact').
card_types('jinxed idol', ['Artifact']).
card_subtypes('jinxed idol', []).
card_colors('jinxed idol', []).
card_text('jinxed idol', 'At the beginning of your upkeep, Jinxed Idol deals 2 damage to you.\nSacrifice a creature: Target opponent gains control of Jinxed Idol.').
card_mana_cost('jinxed idol', ['2']).
card_cmc('jinxed idol', 2).
card_layout('jinxed idol', 'normal').

% Found in: STH
card_name('jinxed ring', 'Jinxed Ring').
card_type('jinxed ring', 'Artifact').
card_types('jinxed ring', ['Artifact']).
card_subtypes('jinxed ring', []).
card_colors('jinxed ring', []).
card_text('jinxed ring', 'Whenever a nontoken permanent is put into your graveyard from the battlefield, Jinxed Ring deals 1 damage to you.\nSacrifice a creature: Target opponent gains control of Jinxed Ring. (This effect lasts indefinitely.)').
card_mana_cost('jinxed ring', ['2']).
card_cmc('jinxed ring', 2).
card_layout('jinxed ring', 'normal').

% Found in: SOK
card_name('jiwari, the earth aflame', 'Jiwari, the Earth Aflame').
card_type('jiwari, the earth aflame', 'Legendary Creature — Spirit').
card_types('jiwari, the earth aflame', ['Creature']).
card_subtypes('jiwari, the earth aflame', ['Spirit']).
card_supertypes('jiwari, the earth aflame', ['Legendary']).
card_colors('jiwari, the earth aflame', ['R']).
card_text('jiwari, the earth aflame', '{X}{R}, {T}: Jiwari, the Earth Aflame deals X damage to target creature without flying.\nChannel — {X}{R}{R}{R}, Discard Jiwari: Jiwari deals X damage to each creature without flying.').
card_mana_cost('jiwari, the earth aflame', ['3', 'R', 'R']).
card_cmc('jiwari, the earth aflame', 5).
card_layout('jiwari, the earth aflame', 'normal').
card_power('jiwari, the earth aflame', 3).
card_toughness('jiwari, the earth aflame', 3).

% Found in: PLC
card_name('jodah\'s avenger', 'Jodah\'s Avenger').
card_type('jodah\'s avenger', 'Creature — Shapeshifter').
card_types('jodah\'s avenger', ['Creature']).
card_subtypes('jodah\'s avenger', ['Shapeshifter']).
card_colors('jodah\'s avenger', ['U']).
card_text('jodah\'s avenger', '{0}: Until end of turn, Jodah\'s Avenger gets -1/-1 and gains your choice of double strike, protection from red, vigilance, or shadow. (A creature with shadow can block or be blocked by only creatures with shadow.)').
card_mana_cost('jodah\'s avenger', ['5', 'U']).
card_cmc('jodah\'s avenger', 6).
card_layout('jodah\'s avenger', 'normal').
card_power('jodah\'s avenger', 4).
card_toughness('jodah\'s avenger', 4).

% Found in: CHR, LEG
card_name('johan', 'Johan').
card_type('johan', 'Legendary Creature — Human Wizard').
card_types('johan', ['Creature']).
card_subtypes('johan', ['Human', 'Wizard']).
card_supertypes('johan', ['Legendary']).
card_colors('johan', ['W', 'R', 'G']).
card_text('johan', 'At the beginning of combat on your turn, you may have Johan gain \"Johan can\'t attack\" until end of combat. If you do, attacking doesn\'t cause creatures you control to tap this combat if Johan is untapped.').
card_mana_cost('johan', ['3', 'R', 'G', 'W']).
card_cmc('johan', 6).
card_layout('johan', 'normal').
card_power('johan', 5).
card_toughness('johan', 4).

% Found in: UNH
card_name('johnny, combo player', 'Johnny, Combo Player').
card_type('johnny, combo player', 'Legendary Creature — Human Gamer').
card_types('johnny, combo player', ['Creature']).
card_subtypes('johnny, combo player', ['Human', 'Gamer']).
card_supertypes('johnny, combo player', ['Legendary']).
card_colors('johnny, combo player', ['U']).
card_text('johnny, combo player', '{4}: Search your library for a card and put that card into your hand. Then shuffle your library.').
card_mana_cost('johnny, combo player', ['2', 'U', 'U']).
card_cmc('johnny, combo player', 4).
card_layout('johnny, combo player', 'normal').
card_power('johnny, combo player', 1).
card_toughness('johnny, combo player', 1).

% Found in: 5ED, ICE, ME2
card_name('johtull wurm', 'Johtull Wurm').
card_type('johtull wurm', 'Creature — Wurm').
card_types('johtull wurm', ['Creature']).
card_subtypes('johtull wurm', ['Wurm']).
card_colors('johtull wurm', ['G']).
card_text('johtull wurm', 'Whenever Johtull Wurm becomes blocked, it gets -2/-1 until end of turn for each creature blocking it beyond the first.').
card_mana_cost('johtull wurm', ['5', 'G']).
card_cmc('johtull wurm', 6).
card_layout('johtull wurm', 'normal').
card_power('johtull wurm', 6).
card_toughness('johtull wurm', 6).

% Found in: WWK
card_name('join the ranks', 'Join the Ranks').
card_type('join the ranks', 'Instant').
card_types('join the ranks', ['Instant']).
card_subtypes('join the ranks', []).
card_colors('join the ranks', ['W']).
card_text('join the ranks', 'Put two 1/1 white Soldier Ally creature tokens onto the battlefield.').
card_mana_cost('join the ranks', ['3', 'W']).
card_cmc('join the ranks', 4).
card_layout('join the ranks', 'normal').

% Found in: 10E, 5DN
card_name('joiner adept', 'Joiner Adept').
card_type('joiner adept', 'Creature — Elf Druid').
card_types('joiner adept', ['Creature']).
card_subtypes('joiner adept', ['Elf', 'Druid']).
card_colors('joiner adept', ['G']).
card_text('joiner adept', 'Lands you control have \"{T}: Add one mana of any color to your mana pool.\"').
card_mana_cost('joiner adept', ['1', 'G']).
card_cmc('joiner adept', 2).
card_layout('joiner adept', 'normal').
card_power('joiner adept', 2).
card_toughness('joiner adept', 1).

% Found in: AVR
card_name('joint assault', 'Joint Assault').
card_type('joint assault', 'Instant').
card_types('joint assault', ['Instant']).
card_subtypes('joint assault', []).
card_colors('joint assault', ['G']).
card_text('joint assault', 'Target creature gets +2/+2 until end of turn. If it\'s paired with a creature, that creature also gets +2/+2 until end of turn.').
card_mana_cost('joint assault', ['G']).
card_cmc('joint assault', 1).
card_layout('joint assault', 'normal').

% Found in: 5ED, 6ED, DKM, ICE, MED
card_name('jokulhaups', 'Jokulhaups').
card_type('jokulhaups', 'Sorcery').
card_types('jokulhaups', ['Sorcery']).
card_subtypes('jokulhaups', []).
card_colors('jokulhaups', ['R']).
card_text('jokulhaups', 'Destroy all artifacts, creatures, and lands. They can\'t be regenerated.').
card_mana_cost('jokulhaups', ['4', 'R', 'R']).
card_cmc('jokulhaups', 6).
card_layout('jokulhaups', 'normal').

% Found in: CSP
card_name('jokulmorder', 'Jokulmorder').
card_type('jokulmorder', 'Creature — Leviathan').
card_types('jokulmorder', ['Creature']).
card_subtypes('jokulmorder', ['Leviathan']).
card_colors('jokulmorder', ['U']).
card_text('jokulmorder', 'Trample\nJokulmorder enters the battlefield tapped.\nWhen Jokulmorder enters the battlefield, sacrifice it unless you sacrifice five lands.\nJokulmorder doesn\'t untap during your untap step.\nWhenever you play an Island, you may untap Jokulmorder.').
card_mana_cost('jokulmorder', ['4', 'U', 'U', 'U']).
card_cmc('jokulmorder', 7).
card_layout('jokulmorder', 'normal').
card_power('jokulmorder', 12).
card_toughness('jokulmorder', 12).

% Found in: MIR
card_name('jolrael\'s centaur', 'Jolrael\'s Centaur').
card_type('jolrael\'s centaur', 'Creature — Centaur Archer').
card_types('jolrael\'s centaur', ['Creature']).
card_subtypes('jolrael\'s centaur', ['Centaur', 'Archer']).
card_colors('jolrael\'s centaur', ['G']).
card_text('jolrael\'s centaur', 'Shroud (This creature can\'t be the target of spells or abilities.)\nFlanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)').
card_mana_cost('jolrael\'s centaur', ['1', 'G', 'G']).
card_cmc('jolrael\'s centaur', 3).
card_layout('jolrael\'s centaur', 'normal').
card_power('jolrael\'s centaur', 2).
card_toughness('jolrael\'s centaur', 2).

% Found in: PCY
card_name('jolrael\'s favor', 'Jolrael\'s Favor').
card_type('jolrael\'s favor', 'Enchantment — Aura').
card_types('jolrael\'s favor', ['Enchantment']).
card_subtypes('jolrael\'s favor', ['Aura']).
card_colors('jolrael\'s favor', ['G']).
card_text('jolrael\'s favor', 'Flash\nEnchant creature\n{1}{G}: Regenerate enchanted creature.').
card_mana_cost('jolrael\'s favor', ['1', 'G']).
card_cmc('jolrael\'s favor', 2).
card_layout('jolrael\'s favor', 'normal').

% Found in: PCY, TSB
card_name('jolrael, empress of beasts', 'Jolrael, Empress of Beasts').
card_type('jolrael, empress of beasts', 'Legendary Creature — Human Spellshaper').
card_types('jolrael, empress of beasts', ['Creature']).
card_subtypes('jolrael, empress of beasts', ['Human', 'Spellshaper']).
card_supertypes('jolrael, empress of beasts', ['Legendary']).
card_colors('jolrael, empress of beasts', ['G']).
card_text('jolrael, empress of beasts', '{2}{G}, {T}, Discard two cards: All lands target player controls become 3/3 creatures until end of turn. They\'re still lands.').
card_mana_cost('jolrael, empress of beasts', ['3', 'G', 'G']).
card_cmc('jolrael, empress of beasts', 5).
card_layout('jolrael, empress of beasts', 'normal').
card_power('jolrael, empress of beasts', 3).
card_toughness('jolrael, empress of beasts', 3).

% Found in: MIR
card_name('jolt', 'Jolt').
card_type('jolt', 'Instant').
card_types('jolt', ['Instant']).
card_subtypes('jolt', []).
card_colors('jolt', ['U']).
card_text('jolt', 'You may tap or untap target artifact, creature, or land.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('jolt', ['2', 'U']).
card_cmc('jolt', 3).
card_layout('jolt', 'normal').

% Found in: NMS
card_name('jolting merfolk', 'Jolting Merfolk').
card_type('jolting merfolk', 'Creature — Merfolk').
card_types('jolting merfolk', ['Creature']).
card_subtypes('jolting merfolk', ['Merfolk']).
card_colors('jolting merfolk', ['U']).
card_text('jolting merfolk', 'Fading 4 (This creature enters the battlefield with four fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nRemove a fade counter from Jolting Merfolk: Tap target creature.').
card_mana_cost('jolting merfolk', ['2', 'U', 'U']).
card_cmc('jolting merfolk', 4).
card_layout('jolting merfolk', 'normal').
card_power('jolting merfolk', 2).
card_toughness('jolting merfolk', 2).

% Found in: NPH
card_name('jor kadeen, the prevailer', 'Jor Kadeen, the Prevailer').
card_type('jor kadeen, the prevailer', 'Legendary Creature — Human Warrior').
card_types('jor kadeen, the prevailer', ['Creature']).
card_subtypes('jor kadeen, the prevailer', ['Human', 'Warrior']).
card_supertypes('jor kadeen, the prevailer', ['Legendary']).
card_colors('jor kadeen, the prevailer', ['W', 'R']).
card_text('jor kadeen, the prevailer', 'First strike\nMetalcraft — Creatures you control get +3/+0 as long as you control three or more artifacts.').
card_mana_cost('jor kadeen, the prevailer', ['3', 'R', 'W']).
card_cmc('jor kadeen, the prevailer', 5).
card_layout('jor kadeen, the prevailer', 'normal').
card_power('jor kadeen, the prevailer', 5).
card_toughness('jor kadeen, the prevailer', 4).

% Found in: DDP, ZEN
card_name('joraga bard', 'Joraga Bard').
card_type('joraga bard', 'Creature — Elf Rogue Ally').
card_types('joraga bard', ['Creature']).
card_subtypes('joraga bard', ['Elf', 'Rogue', 'Ally']).
card_colors('joraga bard', ['G']).
card_text('joraga bard', 'Whenever Joraga Bard or another Ally enters the battlefield under your control, you may have Ally creatures you control gain vigilance until end of turn.').
card_mana_cost('joraga bard', ['3', 'G']).
card_cmc('joraga bard', 4).
card_layout('joraga bard', 'normal').
card_power('joraga bard', 1).
card_toughness('joraga bard', 4).

% Found in: ORI
card_name('joraga invocation', 'Joraga Invocation').
card_type('joraga invocation', 'Sorcery').
card_types('joraga invocation', ['Sorcery']).
card_subtypes('joraga invocation', []).
card_colors('joraga invocation', ['G']).
card_text('joraga invocation', 'Each creature you control gets +3/+3 until end of turn and must be blocked this turn if able.').
card_mana_cost('joraga invocation', ['4', 'G', 'G']).
card_cmc('joraga invocation', 6).
card_layout('joraga invocation', 'normal').

% Found in: ROE
card_name('joraga treespeaker', 'Joraga Treespeaker').
card_type('joraga treespeaker', 'Creature — Elf Druid').
card_types('joraga treespeaker', ['Creature']).
card_subtypes('joraga treespeaker', ['Elf', 'Druid']).
card_colors('joraga treespeaker', ['G']).
card_text('joraga treespeaker', 'Level up {1}{G} ({1}{G}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-4\n1/2\n{T}: Add {G}{G} to your mana pool.\nLEVEL 5+\n1/4\nElves you control have \"{T}: Add {G}{G} to your mana pool.\"').
card_mana_cost('joraga treespeaker', ['G']).
card_cmc('joraga treespeaker', 1).
card_layout('joraga treespeaker', 'leveler').
card_power('joraga treespeaker', 1).
card_toughness('joraga treespeaker', 1).

% Found in: C14, WWK, pLPA
card_name('joraga warcaller', 'Joraga Warcaller').
card_type('joraga warcaller', 'Creature — Elf Warrior').
card_types('joraga warcaller', ['Creature']).
card_subtypes('joraga warcaller', ['Elf', 'Warrior']).
card_colors('joraga warcaller', ['G']).
card_text('joraga warcaller', 'Multikicker {1}{G} (You may pay an additional {1}{G} any number of times as you cast this spell.)\nJoraga Warcaller enters the battlefield with a +1/+1 counter on it for each time it was kicked.\nOther Elf creatures you control get +1/+1 for each +1/+1 counter on Joraga Warcaller.').
card_mana_cost('joraga warcaller', ['G']).
card_cmc('joraga warcaller', 1).
card_layout('joraga warcaller', 'normal').
card_power('joraga warcaller', 1).
card_toughness('joraga warcaller', 1).

% Found in: M15
card_name('jorubai murk lurker', 'Jorubai Murk Lurker').
card_type('jorubai murk lurker', 'Creature — Leech').
card_types('jorubai murk lurker', ['Creature']).
card_subtypes('jorubai murk lurker', ['Leech']).
card_colors('jorubai murk lurker', ['U']).
card_text('jorubai murk lurker', 'Jorubai Murk Lurker gets +1/+1 as long as you control a Swamp.\n{1}{B}: Target creature gains lifelink until end of turn. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_mana_cost('jorubai murk lurker', ['2', 'U']).
card_cmc('jorubai murk lurker', 3).
card_layout('jorubai murk lurker', 'normal').
card_power('jorubai murk lurker', 1).
card_toughness('jorubai murk lurker', 3).

% Found in: MRD
card_name('journey of discovery', 'Journey of Discovery').
card_type('journey of discovery', 'Sorcery').
card_types('journey of discovery', ['Sorcery']).
card_subtypes('journey of discovery', []).
card_colors('journey of discovery', ['G']).
card_text('journey of discovery', 'Choose one —\n• Search your library for up to two basic land cards, reveal them, put them into your hand, then shuffle your library.\n• You may play up to two additional lands this turn.\nEntwine {2}{G} (Choose both if you pay the entwine cost.)').
card_mana_cost('journey of discovery', ['2', 'G']).
card_cmc('journey of discovery', 3).
card_layout('journey of discovery', 'normal').

% Found in: CMD, DDF, ZEN
card_name('journey to nowhere', 'Journey to Nowhere').
card_type('journey to nowhere', 'Enchantment').
card_types('journey to nowhere', ['Enchantment']).
card_subtypes('journey to nowhere', []).
card_colors('journey to nowhere', ['W']).
card_text('journey to nowhere', 'When Journey to Nowhere enters the battlefield, exile target creature.\nWhen Journey to Nowhere leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_mana_cost('journey to nowhere', ['1', 'W']).
card_cmc('journey to nowhere', 2).
card_layout('journey to nowhere', 'normal').

% Found in: CHK, DDI
card_name('journeyer\'s kite', 'Journeyer\'s Kite').
card_type('journeyer\'s kite', 'Artifact').
card_types('journeyer\'s kite', ['Artifact']).
card_subtypes('journeyer\'s kite', []).
card_colors('journeyer\'s kite', []).
card_text('journeyer\'s kite', '{3}, {T}: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('journeyer\'s kite', ['2']).
card_cmc('journeyer\'s kite', 2).
card_layout('journeyer\'s kite', 'normal').

% Found in: HML
card_name('joven', 'Joven').
card_type('joven', 'Legendary Creature — Human Rogue').
card_types('joven', ['Creature']).
card_subtypes('joven', ['Human', 'Rogue']).
card_supertypes('joven', ['Legendary']).
card_colors('joven', ['R']).
card_text('joven', '{R}{R}{R}, {T}: Destroy target noncreature artifact.').
card_mana_cost('joven', ['3', 'R', 'R']).
card_cmc('joven', 5).
card_layout('joven', 'normal').
card_power('joven', 3).
card_toughness('joven', 3).

% Found in: HML, ME2
card_name('joven\'s ferrets', 'Joven\'s Ferrets').
card_type('joven\'s ferrets', 'Creature — Ferret').
card_types('joven\'s ferrets', ['Creature']).
card_subtypes('joven\'s ferrets', ['Ferret']).
card_colors('joven\'s ferrets', ['G']).
card_text('joven\'s ferrets', 'Whenever Joven\'s Ferrets attacks, it gets +0/+2 until end of turn.\nAt end of combat, tap all creatures that blocked Joven\'s Ferrets this turn. They don\'t untap during their controller\'s next untap step.').
card_mana_cost('joven\'s ferrets', ['G']).
card_cmc('joven\'s ferrets', 1).
card_layout('joven\'s ferrets', 'normal').
card_power('joven\'s ferrets', 1).
card_toughness('joven\'s ferrets', 1).

% Found in: 5ED, HML
card_name('joven\'s tools', 'Joven\'s Tools').
card_type('joven\'s tools', 'Artifact').
card_types('joven\'s tools', ['Artifact']).
card_subtypes('joven\'s tools', []).
card_colors('joven\'s tools', []).
card_text('joven\'s tools', '{4}, {T}: Target creature can\'t be blocked this turn except by Walls.').
card_mana_cost('joven\'s tools', ['6']).
card_cmc('joven\'s tools', 6).
card_layout('joven\'s tools', 'normal').

% Found in: LEG
card_name('jovial evil', 'Jovial Evil').
card_type('jovial evil', 'Sorcery').
card_types('jovial evil', ['Sorcery']).
card_subtypes('jovial evil', []).
card_colors('jovial evil', ['B']).
card_text('jovial evil', 'Jovial Evil deals X damage to target opponent, where X is twice the number of white creatures that player controls.').
card_mana_cost('jovial evil', ['2', 'B']).
card_cmc('jovial evil', 3).
card_layout('jovial evil', 'normal').
card_reserved('jovial evil').

% Found in: CHK
card_name('joyous respite', 'Joyous Respite').
card_type('joyous respite', 'Sorcery — Arcane').
card_types('joyous respite', ['Sorcery']).
card_subtypes('joyous respite', ['Arcane']).
card_colors('joyous respite', ['G']).
card_text('joyous respite', 'You gain 1 life for each land you control.').
card_mana_cost('joyous respite', ['3', 'G']).
card_cmc('joyous respite', 4).
card_layout('joyous respite', 'normal').

% Found in: LRW
card_name('judge of currents', 'Judge of Currents').
card_type('judge of currents', 'Creature — Merfolk Wizard').
card_types('judge of currents', ['Creature']).
card_subtypes('judge of currents', ['Merfolk', 'Wizard']).
card_colors('judge of currents', ['W']).
card_text('judge of currents', 'Whenever a Merfolk you control becomes tapped, you may gain 1 life.').
card_mana_cost('judge of currents', ['1', 'W']).
card_cmc('judge of currents', 2).
card_layout('judge of currents', 'normal').
card_power('judge of currents', 1).
card_toughness('judge of currents', 1).

% Found in: FUT
card_name('judge unworthy', 'Judge Unworthy').
card_type('judge unworthy', 'Instant').
card_types('judge unworthy', ['Instant']).
card_subtypes('judge unworthy', []).
card_colors('judge unworthy', ['W']).
card_text('judge unworthy', 'Choose target attacking or blocking creature. Scry 3, then reveal the top card of your library. Judge Unworthy deals damage equal to that card\'s converted mana cost to that creature. (To scry 3, look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('judge unworthy', ['1', 'W']).
card_cmc('judge unworthy', 2).
card_layout('judge unworthy', 'normal').

% Found in: RTR, pFNM
card_name('judge\'s familiar', 'Judge\'s Familiar').
card_type('judge\'s familiar', 'Creature — Bird').
card_types('judge\'s familiar', ['Creature']).
card_subtypes('judge\'s familiar', ['Bird']).
card_colors('judge\'s familiar', ['W', 'U']).
card_text('judge\'s familiar', 'Flying\nSacrifice Judge\'s Familiar: Counter target instant or sorcery spell unless its controller pays {1}.').
card_mana_cost('judge\'s familiar', ['W/U']).
card_cmc('judge\'s familiar', 1).
card_layout('judge\'s familiar', 'normal').
card_power('judge\'s familiar', 1).
card_toughness('judge\'s familiar', 1).

% Found in: CHK, MMA
card_name('jugan, the rising star', 'Jugan, the Rising Star').
card_type('jugan, the rising star', 'Legendary Creature — Dragon Spirit').
card_types('jugan, the rising star', ['Creature']).
card_subtypes('jugan, the rising star', ['Dragon', 'Spirit']).
card_supertypes('jugan, the rising star', ['Legendary']).
card_colors('jugan, the rising star', ['G']).
card_text('jugan, the rising star', 'Flying\nWhen Jugan, the Rising Star dies, you may distribute five +1/+1 counters among any number of target creatures.').
card_mana_cost('jugan, the rising star', ['3', 'G', 'G', 'G']).
card_cmc('jugan, the rising star', 6).
card_layout('jugan, the rising star', 'normal').
card_power('jugan, the rising star', 5).
card_toughness('jugan, the rising star', 5).

% Found in: 10E, 2ED, 3ED, ARC, CED, CEI, DDF, DST, LEA, LEB, M11, M15, ME4, pFNM
card_name('juggernaut', 'Juggernaut').
card_type('juggernaut', 'Artifact Creature — Juggernaut').
card_types('juggernaut', ['Artifact', 'Creature']).
card_subtypes('juggernaut', ['Juggernaut']).
card_colors('juggernaut', []).
card_text('juggernaut', 'Juggernaut attacks each turn if able.\nJuggernaut can\'t be blocked by Walls.').
card_mana_cost('juggernaut', ['4']).
card_cmc('juggernaut', 4).
card_layout('juggernaut', 'normal').
card_power('juggernaut', 5).
card_toughness('juggernaut', 3).

% Found in: VIS
card_name('juju bubble', 'Juju Bubble').
card_type('juju bubble', 'Artifact').
card_types('juju bubble', ['Artifact']).
card_subtypes('juju bubble', []).
card_colors('juju bubble', []).
card_text('juju bubble', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhen you play a card, sacrifice Juju Bubble.\n{2}: You gain 1 life.').
card_mana_cost('juju bubble', ['1']).
card_cmc('juju bubble', 1).
card_layout('juju bubble', 'normal').

% Found in: CHK
card_name('jukai messenger', 'Jukai Messenger').
card_type('jukai messenger', 'Creature — Human Monk').
card_types('jukai messenger', ['Creature']).
card_subtypes('jukai messenger', ['Human', 'Monk']).
card_colors('jukai messenger', ['G']).
card_text('jukai messenger', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('jukai messenger', ['G']).
card_cmc('jukai messenger', 1).
card_layout('jukai messenger', 'normal').
card_power('jukai messenger', 1).
card_toughness('jukai messenger', 1).

% Found in: UGL
card_name('jumbo imp', 'Jumbo Imp').
card_type('jumbo imp', 'Creature — Imp').
card_types('jumbo imp', ['Creature']).
card_subtypes('jumbo imp', ['Imp']).
card_colors('jumbo imp', ['B']).
card_text('jumbo imp', 'Flying\nWhen you play Jumbo Imp, roll a six-sided die. Jumbo Imp comes into play with a number of +1/+1 counters on it equal to the die roll.\nDuring your upkeep, roll a six-sided die and put on Jumbo Imp a number of +1/+1 counters equal to the die roll.\nAt the end of your turn, roll a six-sided die and remove from Jumbo Imp a number of +1/+1 counters equal to the die roll.').
card_mana_cost('jumbo imp', ['2', 'B']).
card_cmc('jumbo imp', 3).
card_layout('jumbo imp', 'normal').
card_power('jumbo imp', 0).
card_toughness('jumbo imp', 0).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB, M10
card_name('jump', 'Jump').
card_type('jump', 'Instant').
card_types('jump', ['Instant']).
card_subtypes('jump', []).
card_colors('jump', ['U']).
card_text('jump', 'Target creature gains flying until end of turn.').
card_mana_cost('jump', ['U']).
card_cmc('jump', 1).
card_layout('jump', 'normal').

% Found in: PC2
card_name('jund', 'Jund').
card_type('jund', 'Plane — Alara').
card_types('jund', ['Plane']).
card_subtypes('jund', ['Alara']).
card_colors('jund', []).
card_text('jund', 'Whenever a player casts a black, red, or green creature spell, it gains devour 5. (As the creature enters the battlefield, its controller may sacrifice any number of creatures. The creature enters the battlefield with five times that many +1/+1 counters on it.)\nWhenever you roll {C}, put two 1/1 red Goblin creature tokens onto the battlefield.').
card_layout('jund', 'plane').

% Found in: ALA
card_name('jund battlemage', 'Jund Battlemage').
card_type('jund battlemage', 'Creature — Human Shaman').
card_types('jund battlemage', ['Creature']).
card_subtypes('jund battlemage', ['Human', 'Shaman']).
card_colors('jund battlemage', ['R']).
card_text('jund battlemage', '{B}, {T}: Target player loses 1 life.\n{G}, {T}: Put a 1/1 green Saproling creature token onto the battlefield.').
card_mana_cost('jund battlemage', ['2', 'R']).
card_cmc('jund battlemage', 3).
card_layout('jund battlemage', 'normal').
card_power('jund battlemage', 2).
card_toughness('jund battlemage', 2).

% Found in: ALA, C13
card_name('jund charm', 'Jund Charm').
card_type('jund charm', 'Instant').
card_types('jund charm', ['Instant']).
card_subtypes('jund charm', []).
card_colors('jund charm', ['B', 'R', 'G']).
card_text('jund charm', 'Choose one —\n• Exile all cards from target player\'s graveyard.\n• Jund Charm deals 2 damage to each creature.\n• Put two +1/+1 counters on target creature.').
card_mana_cost('jund charm', ['B', 'R', 'G']).
card_cmc('jund charm', 3).
card_layout('jund charm', 'normal').

% Found in: ARB
card_name('jund hackblade', 'Jund Hackblade').
card_type('jund hackblade', 'Creature — Goblin Berserker').
card_types('jund hackblade', ['Creature']).
card_subtypes('jund hackblade', ['Goblin', 'Berserker']).
card_colors('jund hackblade', ['B', 'R', 'G']).
card_text('jund hackblade', 'As long as you control another multicolored permanent, Jund Hackblade gets +1/+1 and has haste.').
card_mana_cost('jund hackblade', ['B/G', 'R']).
card_cmc('jund hackblade', 2).
card_layout('jund hackblade', 'normal').
card_power('jund hackblade', 2).
card_toughness('jund hackblade', 1).

% Found in: ALA, C13
card_name('jund panorama', 'Jund Panorama').
card_type('jund panorama', 'Land').
card_types('jund panorama', ['Land']).
card_subtypes('jund panorama', []).
card_colors('jund panorama', []).
card_text('jund panorama', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Jund Panorama: Search your library for a basic Swamp, Mountain, or Forest card and put it onto the battlefield tapped. Then shuffle your library.').
card_layout('jund panorama', 'normal').

% Found in: ARB
card_name('jund sojourners', 'Jund Sojourners').
card_type('jund sojourners', 'Creature — Viashino Shaman').
card_types('jund sojourners', ['Creature']).
card_subtypes('jund sojourners', ['Viashino', 'Shaman']).
card_colors('jund sojourners', ['B', 'R', 'G']).
card_text('jund sojourners', 'When you cycle Jund Sojourners or it dies, you may have it deal 1 damage to target creature or player.\nCycling {2}{R} ({2}{R}, Discard this card: Draw a card.)').
card_mana_cost('jund sojourners', ['B', 'R', 'G']).
card_cmc('jund sojourners', 3).
card_layout('jund sojourners', 'normal').
card_power('jund sojourners', 3).
card_toughness('jund sojourners', 2).

% Found in: APC
card_name('jungle barrier', 'Jungle Barrier').
card_type('jungle barrier', 'Creature — Plant Wall').
card_types('jungle barrier', ['Creature']).
card_subtypes('jungle barrier', ['Plant', 'Wall']).
card_colors('jungle barrier', ['U', 'G']).
card_text('jungle barrier', 'Defender (This creature can\'t attack.)\nWhen Jungle Barrier enters the battlefield, draw a card.').
card_mana_cost('jungle barrier', ['2', 'G', 'U']).
card_cmc('jungle barrier', 4).
card_layout('jungle barrier', 'normal').
card_power('jungle barrier', 2).
card_toughness('jungle barrier', 6).

% Found in: C14, VIS
card_name('jungle basin', 'Jungle Basin').
card_type('jungle basin', 'Land').
card_types('jungle basin', ['Land']).
card_subtypes('jungle basin', []).
card_colors('jungle basin', []).
card_text('jungle basin', 'Jungle Basin enters the battlefield tapped.\nWhen Jungle Basin enters the battlefield, sacrifice it unless you return an untapped Forest you control to its owner\'s hand.\n{T}: Add {1}{G} to your mana pool.').
card_layout('jungle basin', 'normal').

% Found in: FRF, KTK
card_name('jungle hollow', 'Jungle Hollow').
card_type('jungle hollow', 'Land').
card_types('jungle hollow', ['Land']).
card_subtypes('jungle hollow', []).
card_colors('jungle hollow', []).
card_text('jungle hollow', 'Jungle Hollow enters the battlefield tapped.\nWhen Jungle Hollow enters the battlefield, you gain 1 life.\n{T}: Add {B} or {G} to your mana pool.').
card_layout('jungle hollow', 'normal').

% Found in: ME3, POR
card_name('jungle lion', 'Jungle Lion').
card_type('jungle lion', 'Creature — Cat').
card_types('jungle lion', ['Creature']).
card_subtypes('jungle lion', ['Cat']).
card_colors('jungle lion', ['G']).
card_text('jungle lion', 'Jungle Lion can\'t block.').
card_mana_cost('jungle lion', ['G']).
card_cmc('jungle lion', 1).
card_layout('jungle lion', 'normal').
card_power('jungle lion', 2).
card_toughness('jungle lion', 1).

% Found in: MIR
card_name('jungle patrol', 'Jungle Patrol').
card_type('jungle patrol', 'Creature — Human Soldier').
card_types('jungle patrol', ['Creature']).
card_subtypes('jungle patrol', ['Human', 'Soldier']).
card_colors('jungle patrol', ['G']).
card_text('jungle patrol', '{1}{G}, {T}:  Put a 0/1 green Wall creature token with defender named Wood onto the battlefield.\nSacrifice a token named Wood: Add {R} to your mana pool.').
card_mana_cost('jungle patrol', ['3', 'G']).
card_cmc('jungle patrol', 4).
card_layout('jungle patrol', 'normal').
card_power('jungle patrol', 3).
card_toughness('jungle patrol', 2).
card_reserved('jungle patrol').

% Found in: ALA, C13, DDH
card_name('jungle shrine', 'Jungle Shrine').
card_type('jungle shrine', 'Land').
card_types('jungle shrine', ['Land']).
card_subtypes('jungle shrine', []).
card_colors('jungle shrine', []).
card_text('jungle shrine', 'Jungle Shrine enters the battlefield tapped.\n{T}: Add {R}, {G}, or {W} to your mana pool.').
card_layout('jungle shrine', 'normal').

% Found in: MIR
card_name('jungle troll', 'Jungle Troll').
card_type('jungle troll', 'Creature — Troll').
card_types('jungle troll', ['Creature']).
card_subtypes('jungle troll', ['Troll']).
card_colors('jungle troll', ['R', 'G']).
card_text('jungle troll', '{R}: Regenerate Jungle Troll.\n{G}: Regenerate Jungle Troll.').
card_mana_cost('jungle troll', ['1', 'R', 'G']).
card_cmc('jungle troll', 3).
card_layout('jungle troll', 'normal').
card_power('jungle troll', 2).
card_toughness('jungle troll', 1).

% Found in: ALA
card_name('jungle weaver', 'Jungle Weaver').
card_type('jungle weaver', 'Creature — Spider').
card_types('jungle weaver', ['Creature']).
card_subtypes('jungle weaver', ['Spider']).
card_colors('jungle weaver', ['G']).
card_text('jungle weaver', 'Reach (This creature can block creatures with flying.)\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('jungle weaver', ['5', 'G', 'G']).
card_cmc('jungle weaver', 7).
card_layout('jungle weaver', 'normal').
card_power('jungle weaver', 5).
card_toughness('jungle weaver', 6).

% Found in: MIR, VMA
card_name('jungle wurm', 'Jungle Wurm').
card_type('jungle wurm', 'Creature — Wurm').
card_types('jungle wurm', ['Creature']).
card_subtypes('jungle wurm', ['Wurm']).
card_colors('jungle wurm', ['G']).
card_text('jungle wurm', 'Whenever Jungle Wurm becomes blocked, it gets -1/-1 until end of turn for each creature blocking it beyond the first.').
card_mana_cost('jungle wurm', ['3', 'G', 'G']).
card_cmc('jungle wurm', 5).
card_layout('jungle wurm', 'normal').
card_power('jungle wurm', 5).
card_toughness('jungle wurm', 5).

% Found in: ALL, ME2
card_name('juniper order advocate', 'Juniper Order Advocate').
card_type('juniper order advocate', 'Creature — Human Knight').
card_types('juniper order advocate', ['Creature']).
card_subtypes('juniper order advocate', ['Human', 'Knight']).
card_colors('juniper order advocate', ['W']).
card_text('juniper order advocate', 'As long as Juniper Order Advocate is untapped, green creatures you control get +1/+1.').
card_mana_cost('juniper order advocate', ['2', 'W']).
card_cmc('juniper order advocate', 3).
card_layout('juniper order advocate', 'normal').
card_power('juniper order advocate', 1).
card_toughness('juniper order advocate', 2).

% Found in: ICE
card_name('juniper order druid', 'Juniper Order Druid').
card_type('juniper order druid', 'Creature — Human Cleric Druid').
card_types('juniper order druid', ['Creature']).
card_subtypes('juniper order druid', ['Human', 'Cleric', 'Druid']).
card_colors('juniper order druid', ['G']).
card_text('juniper order druid', '{T}: Untap target land.').
card_mana_cost('juniper order druid', ['2', 'G']).
card_cmc('juniper order druid', 3).
card_layout('juniper order druid', 'normal').
card_power('juniper order druid', 1).
card_toughness('juniper order druid', 1).

% Found in: CSP, DDG
card_name('juniper order ranger', 'Juniper Order Ranger').
card_type('juniper order ranger', 'Creature — Human Knight').
card_types('juniper order ranger', ['Creature']).
card_subtypes('juniper order ranger', ['Human', 'Knight']).
card_colors('juniper order ranger', ['W', 'G']).
card_text('juniper order ranger', 'Whenever another creature enters the battlefield under your control, put a +1/+1 counter on that creature and a +1/+1 counter on Juniper Order Ranger.').
card_mana_cost('juniper order ranger', ['3', 'G', 'W']).
card_cmc('juniper order ranger', 5).
card_layout('juniper order ranger', 'normal').
card_power('juniper order ranger', 2).
card_toughness('juniper order ranger', 4).

% Found in: C14, UDS
card_name('junk diver', 'Junk Diver').
card_type('junk diver', 'Artifact Creature — Bird').
card_types('junk diver', ['Artifact', 'Creature']).
card_subtypes('junk diver', ['Bird']).
card_colors('junk diver', []).
card_text('junk diver', 'Flying\nWhen Junk Diver dies, return another target artifact card from your graveyard to your hand.').
card_mana_cost('junk diver', ['3']).
card_cmc('junk diver', 3).
card_layout('junk diver', 'normal').
card_power('junk diver', 1).
card_toughness('junk diver', 1).

% Found in: ODY
card_name('junk golem', 'Junk Golem').
card_type('junk golem', 'Artifact Creature — Golem').
card_types('junk golem', ['Artifact', 'Creature']).
card_subtypes('junk golem', ['Golem']).
card_colors('junk golem', []).
card_text('junk golem', 'Junk Golem enters the battlefield with three +1/+1 counters on it.\nAt the beginning of your upkeep, sacrifice Junk Golem unless you remove a +1/+1 counter from it.\n{1}, Discard a card: Put a +1/+1 counter on Junk Golem.').
card_mana_cost('junk golem', ['4']).
card_cmc('junk golem', 4).
card_layout('junk golem', 'normal').
card_power('junk golem', 0).
card_toughness('junk golem', 0).

% Found in: RAV
card_name('junktroller', 'Junktroller').
card_type('junktroller', 'Artifact Creature — Golem').
card_types('junktroller', ['Artifact', 'Creature']).
card_subtypes('junktroller', ['Golem']).
card_colors('junktroller', []).
card_text('junktroller', 'Defender (This creature can\'t attack.)\n{T}: Put target card from a graveyard on the bottom of its owner\'s library.').
card_mana_cost('junktroller', ['4']).
card_cmc('junktroller', 4).
card_layout('junktroller', 'normal').
card_power('junktroller', 0).
card_toughness('junktroller', 6).

% Found in: CHK
card_name('junkyo bell', 'Junkyo Bell').
card_type('junkyo bell', 'Artifact').
card_types('junkyo bell', ['Artifact']).
card_subtypes('junkyo bell', []).
card_colors('junkyo bell', []).
card_text('junkyo bell', 'At the beginning of your upkeep, you may have target creature you control get +X/+X until end of turn, where X is the number of creatures you control. If you do, sacrifice that creature at the beginning of the next end step.').
card_mana_cost('junkyo bell', ['4']).
card_cmc('junkyo bell', 4).
card_layout('junkyo bell', 'normal').

% Found in: INV
card_name('juntu stakes', 'Juntu Stakes').
card_type('juntu stakes', 'Artifact').
card_types('juntu stakes', ['Artifact']).
card_subtypes('juntu stakes', []).
card_colors('juntu stakes', []).
card_text('juntu stakes', 'Creatures with power 1 or less don\'t untap during their controllers\' untap steps.').
card_mana_cost('juntu stakes', ['2']).
card_cmc('juntu stakes', 2).
card_layout('juntu stakes', 'normal').

% Found in: 4ED, ARN, ME4
card_name('junún efreet', 'Junún Efreet').
card_type('junún efreet', 'Creature — Efreet').
card_types('junún efreet', ['Creature']).
card_subtypes('junún efreet', ['Efreet']).
card_colors('junún efreet', ['B']).
card_text('junún efreet', 'Flying\nAt the beginning of your upkeep, sacrifice Junún Efreet unless you pay {B}{B}.').
card_mana_cost('junún efreet', ['1', 'B', 'B']).
card_cmc('junún efreet', 3).
card_layout('junún efreet', 'normal').
card_power('junún efreet', 3).
card_toughness('junún efreet', 3).

% Found in: CHK
card_name('jushi apprentice', 'Jushi Apprentice').
card_type('jushi apprentice', 'Creature — Human Wizard').
card_types('jushi apprentice', ['Creature']).
card_subtypes('jushi apprentice', ['Human', 'Wizard']).
card_colors('jushi apprentice', ['U']).
card_text('jushi apprentice', '{2}{U}, {T}: Draw a card. If you have nine or more cards in hand, flip Jushi Apprentice.').
card_mana_cost('jushi apprentice', ['1', 'U']).
card_cmc('jushi apprentice', 2).
card_layout('jushi apprentice', 'flip').
card_power('jushi apprentice', 1).
card_toughness('jushi apprentice', 2).
card_sides('jushi apprentice', 'tomoya the revealer').

% Found in: ME4, PO2
card_name('just fate', 'Just Fate').
card_type('just fate', 'Instant').
card_types('just fate', ['Instant']).
card_subtypes('just fate', []).
card_colors('just fate', ['W']).
card_text('just fate', 'Cast Just Fate only during the declare attackers step and only if you\'ve been attacked this step.\nDestroy target attacking creature.').
card_mana_cost('just fate', ['2', 'W']).
card_cmc('just fate', 3).
card_layout('just fate', 'normal').

% Found in: 5ED, ICE
card_name('justice', 'Justice').
card_type('justice', 'Enchantment').
card_types('justice', ['Enchantment']).
card_subtypes('justice', []).
card_colors('justice', ['W']).
card_text('justice', 'At the beginning of your upkeep, sacrifice Justice unless you pay {W}{W}.\nWhenever a red creature or spell deals damage, Justice deals that much damage to that creature\'s or spell\'s controller.').
card_mana_cost('justice', ['2', 'W', 'W']).
card_cmc('justice', 4).
card_layout('justice', 'normal').

% Found in: SHM
card_name('juvenile gloomwidow', 'Juvenile Gloomwidow').
card_type('juvenile gloomwidow', 'Creature — Spider').
card_types('juvenile gloomwidow', ['Creature']).
card_subtypes('juvenile gloomwidow', ['Spider']).
card_colors('juvenile gloomwidow', ['G']).
card_text('juvenile gloomwidow', 'Reach (This creature can block creatures with flying.)\nWither (This deals damage to creatures in the form of -1/-1 counters.)').
card_mana_cost('juvenile gloomwidow', ['G', 'G']).
card_cmc('juvenile gloomwidow', 2).
card_layout('juvenile gloomwidow', 'normal').
card_power('juvenile gloomwidow', 1).
card_toughness('juvenile gloomwidow', 3).

% Found in: 5ED, 6ED, CHR, LEG, MED
card_name('juxtapose', 'Juxtapose').
card_type('juxtapose', 'Sorcery').
card_types('juxtapose', ['Sorcery']).
card_subtypes('juxtapose', []).
card_colors('juxtapose', ['U']).
card_text('juxtapose', 'You and target player exchange control of the creature you each control with the highest converted mana cost. Then exchange control of artifacts the same way. If two or more permanents a player controls are tied for highest cost, their controller chooses one of them.').
card_mana_cost('juxtapose', ['3', 'U']).
card_cmc('juxtapose', 4).
card_layout('juxtapose', 'normal').

% Found in: ARN, MED
card_name('juzám djinn', 'Juzám Djinn').
card_type('juzám djinn', 'Creature — Djinn').
card_types('juzám djinn', ['Creature']).
card_subtypes('juzám djinn', ['Djinn']).
card_colors('juzám djinn', ['B']).
card_text('juzám djinn', 'At the beginning of your upkeep, Juzám Djinn deals 1 damage to you.').
card_mana_cost('juzám djinn', ['2', 'B', 'B']).
card_cmc('juzám djinn', 4).
card_layout('juzám djinn', 'normal').
card_power('juzám djinn', 5).
card_toughness('juzám djinn', 5).
card_reserved('juzám djinn').

% Found in: C13, CMD, PC2, ZEN
card_name('jwar isle refuge', 'Jwar Isle Refuge').
card_type('jwar isle refuge', 'Land').
card_types('jwar isle refuge', ['Land']).
card_subtypes('jwar isle refuge', []).
card_colors('jwar isle refuge', []).
card_text('jwar isle refuge', 'Jwar Isle Refuge enters the battlefield tapped.\nWhen Jwar Isle Refuge enters the battlefield, you gain 1 life.\n{T}: Add {U} or {B} to your mana pool.').
card_layout('jwar isle refuge', 'normal').

% Found in: ROE
card_name('jwari scuttler', 'Jwari Scuttler').
card_type('jwari scuttler', 'Creature — Crab').
card_types('jwari scuttler', ['Creature']).
card_subtypes('jwari scuttler', ['Crab']).
card_colors('jwari scuttler', ['U']).
card_text('jwari scuttler', '').
card_mana_cost('jwari scuttler', ['2', 'U']).
card_cmc('jwari scuttler', 3).
card_layout('jwari scuttler', 'normal').
card_power('jwari scuttler', 2).
card_toughness('jwari scuttler', 3).

% Found in: WWK
card_name('jwari shapeshifter', 'Jwari Shapeshifter').
card_type('jwari shapeshifter', 'Creature — Shapeshifter Ally').
card_types('jwari shapeshifter', ['Creature']).
card_subtypes('jwari shapeshifter', ['Shapeshifter', 'Ally']).
card_colors('jwari shapeshifter', ['U']).
card_text('jwari shapeshifter', 'You may have Jwari Shapeshifter enter the battlefield as a copy of any Ally creature on the battlefield.').
card_mana_cost('jwari shapeshifter', ['1', 'U']).
card_cmc('jwari shapeshifter', 2).
card_layout('jwari shapeshifter', 'normal').
card_power('jwari shapeshifter', 0).
card_toughness('jwari shapeshifter', 0).

% Found in: CMD, CSP
card_name('jötun grunt', 'Jötun Grunt').
card_type('jötun grunt', 'Creature — Giant Soldier').
card_types('jötun grunt', ['Creature']).
card_subtypes('jötun grunt', ['Giant', 'Soldier']).
card_colors('jötun grunt', ['W']).
card_text('jötun grunt', 'Cumulative upkeep—Put two cards from a single graveyard on the bottom of their owner\'s library. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('jötun grunt', ['1', 'W']).
card_cmc('jötun grunt', 2).
card_layout('jötun grunt', 'normal').
card_power('jötun grunt', 4).
card_toughness('jötun grunt', 4).

% Found in: CSP
card_name('jötun owl keeper', 'Jötun Owl Keeper').
card_type('jötun owl keeper', 'Creature — Giant').
card_types('jötun owl keeper', ['Creature']).
card_subtypes('jötun owl keeper', ['Giant']).
card_colors('jötun owl keeper', ['W']).
card_text('jötun owl keeper', 'Cumulative upkeep {W} or {U} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhen Jötun Owl Keeper dies, put a 1/1 white Bird creature token with flying onto the battlefield for each age counter on it.').
card_mana_cost('jötun owl keeper', ['2', 'W']).
card_cmc('jötun owl keeper', 3).
card_layout('jötun owl keeper', 'normal').
card_power('jötun owl keeper', 3).
card_toughness('jötun owl keeper', 3).

