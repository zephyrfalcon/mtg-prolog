% Card-specific data

% Found in: DDJ, GPT
card_name('vacuumelt', 'Vacuumelt').
card_type('vacuumelt', 'Sorcery').
card_types('vacuumelt', ['Sorcery']).
card_subtypes('vacuumelt', []).
card_colors('vacuumelt', ['U']).
card_text('vacuumelt', 'Replicate {2}{U} (When you cast this spell, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)\nReturn target creature to its owner\'s hand.').
card_mana_cost('vacuumelt', ['2', 'U']).
card_cmc('vacuumelt', 3).
card_layout('vacuumelt', 'normal').

% Found in: CHR, LEG, ME3
card_name('vaevictis asmadi', 'Vaevictis Asmadi').
card_type('vaevictis asmadi', 'Legendary Creature — Elder Dragon').
card_types('vaevictis asmadi', ['Creature']).
card_subtypes('vaevictis asmadi', ['Elder', 'Dragon']).
card_supertypes('vaevictis asmadi', ['Legendary']).
card_colors('vaevictis asmadi', ['B', 'R', 'G']).
card_text('vaevictis asmadi', 'Flying\nAt the beginning of your upkeep, sacrifice Vaevictis Asmadi unless you pay {B}{R}{G}.\n{B}: Vaevictis Asmadi gets +1/+0 until end of turn.\n{R}: Vaevictis Asmadi gets +1/+0 until end of turn.\n{G}: Vaevictis Asmadi gets +1/+0 until end of turn.').
card_mana_cost('vaevictis asmadi', ['2', 'B', 'B', 'R', 'R', 'G', 'G']).
card_cmc('vaevictis asmadi', 8).
card_layout('vaevictis asmadi', 'normal').
card_power('vaevictis asmadi', 7).
card_toughness('vaevictis asmadi', 7).

% Found in: CON
card_name('vagrant plowbeasts', 'Vagrant Plowbeasts').
card_type('vagrant plowbeasts', 'Creature — Beast').
card_types('vagrant plowbeasts', ['Creature']).
card_subtypes('vagrant plowbeasts', ['Beast']).
card_colors('vagrant plowbeasts', ['W', 'G']).
card_text('vagrant plowbeasts', '{1}: Regenerate target creature with power 5 or greater.').
card_mana_cost('vagrant plowbeasts', ['5', 'G', 'W']).
card_cmc('vagrant plowbeasts', 7).
card_layout('vagrant plowbeasts', 'normal').
card_power('vagrant plowbeasts', 6).
card_toughness('vagrant plowbeasts', 6).

% Found in: ROE
card_name('valakut fireboar', 'Valakut Fireboar').
card_type('valakut fireboar', 'Creature — Elemental Boar').
card_types('valakut fireboar', ['Creature']).
card_subtypes('valakut fireboar', ['Elemental', 'Boar']).
card_colors('valakut fireboar', ['R']).
card_text('valakut fireboar', 'Whenever Valakut Fireboar attacks, switch its power and toughness until end of turn.').
card_mana_cost('valakut fireboar', ['4', 'R']).
card_cmc('valakut fireboar', 5).
card_layout('valakut fireboar', 'normal').
card_power('valakut fireboar', 1).
card_toughness('valakut fireboar', 7).

% Found in: BFZ
card_name('valakut invoker', 'Valakut Invoker').
card_type('valakut invoker', 'Creature — Human Shaman').
card_types('valakut invoker', ['Creature']).
card_subtypes('valakut invoker', ['Human', 'Shaman']).
card_colors('valakut invoker', ['R']).
card_text('valakut invoker', '{8}: Valakut Invoker deals 3 damage to target creature or player.').
card_mana_cost('valakut invoker', ['2', 'R']).
card_cmc('valakut invoker', 3).
card_layout('valakut invoker', 'normal').
card_power('valakut invoker', 2).
card_toughness('valakut invoker', 3).

% Found in: BFZ
card_name('valakut predator', 'Valakut Predator').
card_type('valakut predator', 'Creature — Elemental').
card_types('valakut predator', ['Creature']).
card_subtypes('valakut predator', ['Elemental']).
card_colors('valakut predator', ['R']).
card_text('valakut predator', 'Landfall — Whenever a land enters the battlefield under your control, Valakut Predator gets +2/+2 until end of turn.').
card_mana_cost('valakut predator', ['2', 'R']).
card_cmc('valakut predator', 3).
card_layout('valakut predator', 'normal').
card_power('valakut predator', 2).
card_toughness('valakut predator', 2).

% Found in: ZEN, pLPA
card_name('valakut, the molten pinnacle', 'Valakut, the Molten Pinnacle').
card_type('valakut, the molten pinnacle', 'Land').
card_types('valakut, the molten pinnacle', ['Land']).
card_subtypes('valakut, the molten pinnacle', []).
card_colors('valakut, the molten pinnacle', []).
card_text('valakut, the molten pinnacle', 'Valakut, the Molten Pinnacle enters the battlefield tapped.\nWhenever a Mountain enters the battlefield under your control, if you control at least five other Mountains, you may have Valakut, the Molten Pinnacle deal 3 damage to target creature or player.\n{T}: Add {R} to your mana pool.').
card_layout('valakut, the molten pinnacle', 'normal').

% Found in: CON
card_name('valeron outlander', 'Valeron Outlander').
card_type('valeron outlander', 'Creature — Human Scout').
card_types('valeron outlander', ['Creature']).
card_subtypes('valeron outlander', ['Human', 'Scout']).
card_colors('valeron outlander', ['W', 'G']).
card_text('valeron outlander', 'Protection from black').
card_mana_cost('valeron outlander', ['G', 'W']).
card_cmc('valeron outlander', 2).
card_layout('valeron outlander', 'normal').
card_power('valeron outlander', 2).
card_toughness('valeron outlander', 2).

% Found in: ORI
card_name('valeron wardens', 'Valeron Wardens').
card_type('valeron wardens', 'Creature — Human Monk').
card_types('valeron wardens', ['Creature']).
card_subtypes('valeron wardens', ['Human', 'Monk']).
card_colors('valeron wardens', ['G']).
card_text('valeron wardens', 'Renown 2 (When this creature deals combat damage to a player, if it isn\'t renowned, put two +1/+1 counters on it and it becomes renowned.)\nWhenever a creature you control becomes renowned, draw a card.').
card_mana_cost('valeron wardens', ['2', 'G']).
card_cmc('valeron wardens', 3).
card_layout('valeron wardens', 'normal').
card_power('valeron wardens', 1).
card_toughness('valeron wardens', 3).

% Found in: CON
card_name('valiant guard', 'Valiant Guard').
card_type('valiant guard', 'Creature — Human Soldier').
card_types('valiant guard', ['Creature']).
card_subtypes('valiant guard', ['Human', 'Soldier']).
card_colors('valiant guard', ['W']).
card_text('valiant guard', '').
card_mana_cost('valiant guard', ['W']).
card_cmc('valiant guard', 1).
card_layout('valiant guard', 'normal').
card_power('valiant guard', 0).
card_toughness('valiant guard', 3).

% Found in: KTK
card_name('valley dasher', 'Valley Dasher').
card_type('valley dasher', 'Creature — Human Berserker').
card_types('valley dasher', ['Creature']).
card_subtypes('valley dasher', ['Human', 'Berserker']).
card_colors('valley dasher', ['R']).
card_text('valley dasher', 'Haste\nValley Dasher attacks each turn if able.').
card_mana_cost('valley dasher', ['1', 'R']).
card_cmc('valley dasher', 2).
card_layout('valley dasher', 'normal').
card_power('valley dasher', 2).
card_toughness('valley dasher', 2).

% Found in: ARB, C13, CMD, DDL
card_name('valley rannet', 'Valley Rannet').
card_type('valley rannet', 'Creature — Beast').
card_types('valley rannet', ['Creature']).
card_subtypes('valley rannet', ['Beast']).
card_colors('valley rannet', ['R', 'G']).
card_text('valley rannet', 'Mountaincycling {2}, forestcycling {2} ({2}, Discard this card: Search your library for a Mountain or Forest card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('valley rannet', ['4', 'R', 'G']).
card_cmc('valley rannet', 6).
card_layout('valley rannet', 'normal').
card_power('valley rannet', 6).
card_toughness('valley rannet', 3).

% Found in: SHM
card_name('valleymaker', 'Valleymaker').
card_type('valleymaker', 'Creature — Giant Shaman').
card_types('valleymaker', ['Creature']).
card_subtypes('valleymaker', ['Giant', 'Shaman']).
card_colors('valleymaker', ['R', 'G']).
card_text('valleymaker', '{T}, Sacrifice a Mountain: Valleymaker deals 3 damage to target creature.\n{T}, Sacrifice a Forest: Choose a player. That player adds {G}{G}{G} to his or her mana pool.').
card_mana_cost('valleymaker', ['5', 'R/G']).
card_cmc('valleymaker', 6).
card_layout('valleymaker', 'normal').
card_power('valleymaker', 5).
card_toughness('valleymaker', 5).

% Found in: JUD, TSB
card_name('valor', 'Valor').
card_type('valor', 'Creature — Incarnation').
card_types('valor', ['Creature']).
card_subtypes('valor', ['Incarnation']).
card_colors('valor', ['W']).
card_text('valor', 'First strike\nAs long as Valor is in your graveyard and you control a Plains, creatures you control have first strike.').
card_mana_cost('valor', ['3', 'W']).
card_cmc('valor', 4).
card_layout('valor', 'normal').
card_power('valor', 2).
card_toughness('valor', 2).

% Found in: ORI
card_name('valor in akros', 'Valor in Akros').
card_type('valor in akros', 'Enchantment').
card_types('valor in akros', ['Enchantment']).
card_subtypes('valor in akros', []).
card_colors('valor in akros', ['W']).
card_text('valor in akros', 'Whenever a creature enters the battlefield under your control, creatures you control get +1/+1 until end of turn.').
card_mana_cost('valor in akros', ['3', 'W']).
card_cmc('valor in akros', 4).
card_layout('valor in akros', 'normal').

% Found in: CNS, DIS
card_name('valor made real', 'Valor Made Real').
card_type('valor made real', 'Instant').
card_types('valor made real', ['Instant']).
card_subtypes('valor made real', []).
card_colors('valor made real', ['W']).
card_text('valor made real', 'Target creature can block any number of creatures this turn.').
card_mana_cost('valor made real', ['W']).
card_cmc('valor made real', 1).
card_layout('valor made real', 'normal').

% Found in: POR
card_name('valorous charge', 'Valorous Charge').
card_type('valorous charge', 'Sorcery').
card_types('valorous charge', ['Sorcery']).
card_subtypes('valorous charge', []).
card_colors('valorous charge', ['W']).
card_text('valorous charge', 'White creatures get +2/+0 until end of turn.').
card_mana_cost('valorous charge', ['1', 'W', 'W']).
card_cmc('valorous charge', 3).
card_layout('valorous charge', 'normal').

% Found in: FRF
card_name('valorous stance', 'Valorous Stance').
card_type('valorous stance', 'Instant').
card_types('valorous stance', ['Instant']).
card_subtypes('valorous stance', []).
card_colors('valorous stance', ['W']).
card_text('valorous stance', 'Choose one —\n• Target creature gains indestructible until end of turn.\n• Destroy target creature with toughness 4 or greater.').
card_mana_cost('valorous stance', ['1', 'W']).
card_cmc('valorous stance', 2).
card_layout('valorous stance', 'normal').

% Found in: M10
card_name('vampire aristocrat', 'Vampire Aristocrat').
card_type('vampire aristocrat', 'Creature — Vampire Rogue').
card_types('vampire aristocrat', ['Creature']).
card_subtypes('vampire aristocrat', ['Vampire', 'Rogue']).
card_colors('vampire aristocrat', ['B']).
card_text('vampire aristocrat', 'Sacrifice a creature: Vampire Aristocrat gets +2/+2 until end of turn.').
card_mana_cost('vampire aristocrat', ['2', 'B']).
card_cmc('vampire aristocrat', 3).
card_layout('vampire aristocrat', 'normal').
card_power('vampire aristocrat', 2).
card_toughness('vampire aristocrat', 2).

% Found in: 10E, 4ED, 5ED, DD3_GVL, DDD, ITP, LEG, RQS
card_name('vampire bats', 'Vampire Bats').
card_type('vampire bats', 'Creature — Bat').
card_types('vampire bats', ['Creature']).
card_subtypes('vampire bats', ['Bat']).
card_colors('vampire bats', ['B']).
card_text('vampire bats', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{B}: Vampire Bats gets +1/+0 until end of turn. Activate this ability no more than twice each turn.').
card_mana_cost('vampire bats', ['B']).
card_cmc('vampire bats', 1).
card_layout('vampire bats', 'normal').
card_power('vampire bats', 0).
card_toughness('vampire bats', 1).

% Found in: C14, CNS, ZEN
card_name('vampire hexmage', 'Vampire Hexmage').
card_type('vampire hexmage', 'Creature — Vampire Shaman').
card_types('vampire hexmage', ['Creature']).
card_subtypes('vampire hexmage', ['Vampire', 'Shaman']).
card_colors('vampire hexmage', ['B']).
card_text('vampire hexmage', 'First strike\nSacrifice Vampire Hexmage: Remove all counters from target permanent.').
card_mana_cost('vampire hexmage', ['B', 'B']).
card_cmc('vampire hexmage', 2).
card_layout('vampire hexmage', 'normal').
card_power('vampire hexmage', 2).
card_toughness('vampire hexmage', 1).

% Found in: EXO, TPR
card_name('vampire hounds', 'Vampire Hounds').
card_type('vampire hounds', 'Creature — Vampire Hound').
card_types('vampire hounds', ['Creature']).
card_subtypes('vampire hounds', ['Vampire', 'Hound']).
card_colors('vampire hounds', ['B']).
card_text('vampire hounds', 'Discard a creature card: Vampire Hounds gets +2/+2 until end of turn.').
card_mana_cost('vampire hounds', ['2', 'B']).
card_cmc('vampire hounds', 3).
card_layout('vampire hounds', 'normal').
card_power('vampire hounds', 2).
card_toughness('vampire hounds', 2).

% Found in: ISD
card_name('vampire interloper', 'Vampire Interloper').
card_type('vampire interloper', 'Creature — Vampire Scout').
card_types('vampire interloper', ['Creature']).
card_subtypes('vampire interloper', ['Vampire', 'Scout']).
card_colors('vampire interloper', ['B']).
card_text('vampire interloper', 'Flying\nVampire Interloper can\'t block.').
card_mana_cost('vampire interloper', ['1', 'B']).
card_cmc('vampire interloper', 2).
card_layout('vampire interloper', 'normal').
card_power('vampire interloper', 2).
card_toughness('vampire interloper', 1).

% Found in: DDK, MM2, ZEN
card_name('vampire lacerator', 'Vampire Lacerator').
card_type('vampire lacerator', 'Creature — Vampire Warrior').
card_types('vampire lacerator', ['Creature']).
card_subtypes('vampire lacerator', ['Vampire', 'Warrior']).
card_colors('vampire lacerator', ['B']).
card_text('vampire lacerator', 'At the beginning of your upkeep, you lose 1 life unless an opponent has 10 or less life.').
card_mana_cost('vampire lacerator', ['B']).
card_cmc('vampire lacerator', 1).
card_layout('vampire lacerator', 'normal').
card_power('vampire lacerator', 2).
card_toughness('vampire lacerator', 2).

% Found in: C13, CMD, DDK, DDP, M13, ZEN, pWPN
card_name('vampire nighthawk', 'Vampire Nighthawk').
card_type('vampire nighthawk', 'Creature — Vampire Shaman').
card_types('vampire nighthawk', ['Creature']).
card_subtypes('vampire nighthawk', ['Vampire', 'Shaman']).
card_colors('vampire nighthawk', ['B']).
card_text('vampire nighthawk', 'Flying\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('vampire nighthawk', ['1', 'B', 'B']).
card_cmc('vampire nighthawk', 3).
card_layout('vampire nighthawk', 'normal').
card_power('vampire nighthawk', 2).
card_toughness('vampire nighthawk', 3).

% Found in: M10, M13, pMEI, pPRE
card_name('vampire nocturnus', 'Vampire Nocturnus').
card_type('vampire nocturnus', 'Creature — Vampire').
card_types('vampire nocturnus', ['Creature']).
card_subtypes('vampire nocturnus', ['Vampire']).
card_colors('vampire nocturnus', ['B']).
card_text('vampire nocturnus', 'Play with the top card of your library revealed.\nAs long as the top card of your library is black, Vampire Nocturnus and other Vampire creatures you control get +2/+1 and have flying.').
card_mana_cost('vampire nocturnus', ['1', 'B', 'B', 'B']).
card_cmc('vampire nocturnus', 4).
card_layout('vampire nocturnus', 'normal').
card_power('vampire nocturnus', 3).
card_toughness('vampire nocturnus', 3).

% Found in: VAN
card_name('vampire nocturnus avatar', 'Vampire Nocturnus Avatar').
card_type('vampire nocturnus avatar', 'Vanguard').
card_types('vampire nocturnus avatar', ['Vanguard']).
card_subtypes('vampire nocturnus avatar', []).
card_colors('vampire nocturnus avatar', []).
card_text('vampire nocturnus avatar', 'Play with the top card of your library revealed.\nAs long as the top card of your library is black, black creatures you control get +2/+1.').
card_layout('vampire nocturnus avatar', 'vanguard').

% Found in: DDK, M12, MM2
card_name('vampire outcasts', 'Vampire Outcasts').
card_type('vampire outcasts', 'Creature — Vampire').
card_types('vampire outcasts', ['Creature']).
card_subtypes('vampire outcasts', ['Vampire']).
card_colors('vampire outcasts', ['B']).
card_text('vampire outcasts', 'Bloodthirst 2 (If an opponent was dealt damage this turn, this creature enters the battlefield with two +1/+1 counters on it.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('vampire outcasts', ['2', 'B', 'B']).
card_cmc('vampire outcasts', 4).
card_layout('vampire outcasts', 'normal').
card_power('vampire outcasts', 2).
card_toughness('vampire outcasts', 2).

% Found in: M14
card_name('vampire warlord', 'Vampire Warlord').
card_type('vampire warlord', 'Creature — Vampire Warrior').
card_types('vampire warlord', ['Creature']).
card_subtypes('vampire warlord', ['Vampire', 'Warrior']).
card_colors('vampire warlord', ['B']).
card_text('vampire warlord', 'Sacrifice another creature: Regenerate Vampire Warlord. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_mana_cost('vampire warlord', ['4', 'B']).
card_cmc('vampire warlord', 5).
card_layout('vampire warlord', 'normal').
card_power('vampire warlord', 4).
card_toughness('vampire warlord', 2).

% Found in: DDK, ZEN
card_name('vampire\'s bite', 'Vampire\'s Bite').
card_type('vampire\'s bite', 'Instant').
card_types('vampire\'s bite', ['Instant']).
card_subtypes('vampire\'s bite', []).
card_colors('vampire\'s bite', ['B']).
card_text('vampire\'s bite', 'Kicker {2}{B} (You may pay an additional {2}{B} as you cast this spell.)\nTarget creature gets +3/+0 until end of turn. If Vampire\'s Bite was kicked, that creature gains lifelink until end of turn. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_mana_cost('vampire\'s bite', ['B']).
card_cmc('vampire\'s bite', 1).
card_layout('vampire\'s bite', 'normal').

% Found in: ARC, ODY
card_name('vampiric dragon', 'Vampiric Dragon').
card_type('vampiric dragon', 'Creature — Vampire Dragon').
card_types('vampiric dragon', ['Creature']).
card_subtypes('vampiric dragon', ['Vampire', 'Dragon']).
card_colors('vampiric dragon', ['B', 'R']).
card_text('vampiric dragon', 'Flying\nWhenever a creature dealt damage by Vampiric Dragon this turn dies, put a +1/+1 counter on Vampiric Dragon.\n{1}{R}: Vampiric Dragon deals 1 damage to target creature.').
card_mana_cost('vampiric dragon', ['6', 'B', 'R']).
card_cmc('vampiric dragon', 8).
card_layout('vampiric dragon', 'normal').
card_power('vampiric dragon', 5).
card_toughness('vampiric dragon', 5).

% Found in: USG
card_name('vampiric embrace', 'Vampiric Embrace').
card_type('vampiric embrace', 'Enchantment — Aura').
card_types('vampiric embrace', ['Enchantment']).
card_subtypes('vampiric embrace', ['Aura']).
card_colors('vampiric embrace', ['B']).
card_text('vampiric embrace', 'Enchant creature\nEnchanted creature gets +2/+2 and has flying.\nWhenever a creature dealt damage by enchanted creature this turn dies, put a +1/+1 counter on that creature.').
card_mana_cost('vampiric embrace', ['2', 'B', 'B']).
card_cmc('vampiric embrace', 4).
card_layout('vampiric embrace', 'normal').

% Found in: POR
card_name('vampiric feast', 'Vampiric Feast').
card_type('vampiric feast', 'Sorcery').
card_types('vampiric feast', ['Sorcery']).
card_subtypes('vampiric feast', []).
card_colors('vampiric feast', ['B']).
card_text('vampiric feast', 'Vampiric Feast deals 4 damage to target creature or player and you gain 4 life.').
card_mana_cost('vampiric feast', ['5', 'B', 'B']).
card_cmc('vampiric feast', 7).
card_layout('vampiric feast', 'normal').

% Found in: ISD
card_name('vampiric fury', 'Vampiric Fury').
card_type('vampiric fury', 'Instant').
card_types('vampiric fury', ['Instant']).
card_subtypes('vampiric fury', []).
card_colors('vampiric fury', ['R']).
card_text('vampiric fury', 'Vampire creatures you control get +2/+0 and gain first strike until end of turn.').
card_mana_cost('vampiric fury', ['1', 'R']).
card_cmc('vampiric fury', 2).
card_layout('vampiric fury', 'normal').

% Found in: PLC
card_name('vampiric link', 'Vampiric Link').
card_type('vampiric link', 'Enchantment — Aura').
card_types('vampiric link', ['Enchantment']).
card_subtypes('vampiric link', ['Aura']).
card_colors('vampiric link', ['B']).
card_text('vampiric link', 'Enchant creature\nWhenever enchanted creature deals damage, you gain that much life.').
card_mana_cost('vampiric link', ['B']).
card_cmc('vampiric link', 1).
card_layout('vampiric link', 'normal').

% Found in: BFZ
card_name('vampiric rites', 'Vampiric Rites').
card_type('vampiric rites', 'Enchantment').
card_types('vampiric rites', ['Enchantment']).
card_subtypes('vampiric rites', []).
card_colors('vampiric rites', ['B']).
card_text('vampiric rites', '{1}{B}, Sacrifice a creature: You gain 1 life and draw a card.').
card_mana_cost('vampiric rites', ['B']).
card_cmc('vampiric rites', 1).
card_layout('vampiric rites', 'normal').

% Found in: TSP
card_name('vampiric sliver', 'Vampiric Sliver').
card_type('vampiric sliver', 'Creature — Sliver').
card_types('vampiric sliver', ['Creature']).
card_subtypes('vampiric sliver', ['Sliver']).
card_colors('vampiric sliver', ['B']).
card_text('vampiric sliver', 'All Sliver creatures have \"Whenever a creature dealt damage by this creature this turn dies, put a +1/+1 counter on this creature.\"').
card_mana_cost('vampiric sliver', ['3', 'B']).
card_cmc('vampiric sliver', 4).
card_layout('vampiric sliver', 'normal').
card_power('vampiric sliver', 3).
card_toughness('vampiric sliver', 3).

% Found in: 8ED, PO2
card_name('vampiric spirit', 'Vampiric Spirit').
card_type('vampiric spirit', 'Creature — Spirit').
card_types('vampiric spirit', ['Creature']).
card_subtypes('vampiric spirit', ['Spirit']).
card_colors('vampiric spirit', ['B']).
card_text('vampiric spirit', 'Flying\nWhen Vampiric Spirit enters the battlefield, you lose 4 life.').
card_mana_cost('vampiric spirit', ['2', 'B', 'B']).
card_cmc('vampiric spirit', 4).
card_layout('vampiric spirit', 'normal').
card_power('vampiric spirit', 4).
card_toughness('vampiric spirit', 3).

% Found in: POR
card_name('vampiric touch', 'Vampiric Touch').
card_type('vampiric touch', 'Sorcery').
card_types('vampiric touch', ['Sorcery']).
card_subtypes('vampiric touch', []).
card_colors('vampiric touch', ['B']).
card_text('vampiric touch', 'Vampiric Touch deals 2 damage to target opponent and you gain 2 life.').
card_mana_cost('vampiric touch', ['2', 'B']).
card_cmc('vampiric touch', 3).
card_layout('vampiric touch', 'normal').

% Found in: 6ED, VIS, VMA, pJGP
card_name('vampiric tutor', 'Vampiric Tutor').
card_type('vampiric tutor', 'Instant').
card_types('vampiric tutor', ['Instant']).
card_subtypes('vampiric tutor', []).
card_colors('vampiric tutor', ['B']).
card_text('vampiric tutor', 'Search your library for a card, then shuffle your library and put that card on top of it. You lose 2 life.').
card_mana_cost('vampiric tutor', ['B']).
card_cmc('vampiric tutor', 1).
card_layout('vampiric tutor', 'normal').

% Found in: MGB, VIS
card_name('vampirism', 'Vampirism').
card_type('vampirism', 'Enchantment — Aura').
card_types('vampirism', ['Enchantment']).
card_subtypes('vampirism', ['Aura']).
card_colors('vampirism', ['B']).
card_text('vampirism', 'Enchant creature\nWhen Vampirism enters the battlefield, draw a card at the beginning of the next turn\'s upkeep.\nEnchanted creature gets +1/+1 for each other creature you control.\nOther creatures you control get -1/-1.').
card_mana_cost('vampirism', ['1', 'B']).
card_cmc('vampirism', 2).
card_layout('vampirism', 'normal').

% Found in: RTR
card_name('vandalblast', 'Vandalblast').
card_type('vandalblast', 'Sorcery').
card_types('vandalblast', ['Sorcery']).
card_subtypes('vandalblast', []).
card_colors('vandalblast', ['R']).
card_text('vandalblast', 'Destroy target artifact you don\'t control.\nOverload {4}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_mana_cost('vandalblast', ['R']).
card_cmc('vandalblast', 1).
card_layout('vandalblast', 'normal').

% Found in: DTK
card_name('vandalize', 'Vandalize').
card_type('vandalize', 'Sorcery').
card_types('vandalize', ['Sorcery']).
card_subtypes('vandalize', []).
card_colors('vandalize', ['R']).
card_text('vandalize', 'Choose one or both —\n• Destroy target artifact.\n• Destroy target land.').
card_mana_cost('vandalize', ['4', 'R']).
card_cmc('vandalize', 5).
card_layout('vandalize', 'normal').

% Found in: BNG
card_name('vanguard of brimaz', 'Vanguard of Brimaz').
card_type('vanguard of brimaz', 'Creature — Cat Soldier').
card_types('vanguard of brimaz', ['Creature']).
card_subtypes('vanguard of brimaz', ['Cat', 'Soldier']).
card_colors('vanguard of brimaz', ['W']).
card_text('vanguard of brimaz', 'Vigilance\nHeroic — Whenever you cast a spell that targets Vanguard of Brimaz, put a 1/1 white Cat Soldier creature token with vigilance onto the battlefield.').
card_mana_cost('vanguard of brimaz', ['W', 'W']).
card_cmc('vanguard of brimaz', 2).
card_layout('vanguard of brimaz', 'normal').
card_power('vanguard of brimaz', 2).
card_toughness('vanguard of brimaz', 2).

% Found in: AVR
card_name('vanguard\'s shield', 'Vanguard\'s Shield').
card_type('vanguard\'s shield', 'Artifact — Equipment').
card_types('vanguard\'s shield', ['Artifact']).
card_subtypes('vanguard\'s shield', ['Equipment']).
card_colors('vanguard\'s shield', []).
card_text('vanguard\'s shield', 'Equipped creature gets +0/+3 and can block an additional creature.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('vanguard\'s shield', ['2']).
card_cmc('vanguard\'s shield', 2).
card_layout('vanguard\'s shield', 'normal').

% Found in: CSP, DDI
card_name('vanish into memory', 'Vanish into Memory').
card_type('vanish into memory', 'Instant').
card_types('vanish into memory', ['Instant']).
card_subtypes('vanish into memory', []).
card_colors('vanish into memory', ['W', 'U']).
card_text('vanish into memory', 'Exile target creature. You draw cards equal to that creature\'s power. At the beginning of your next upkeep, return that card to the battlefield under its owner\'s control. If you do, discard cards equal to that creature\'s toughness.').
card_mana_cost('vanish into memory', ['2', 'W', 'U']).
card_cmc('vanish into memory', 4).
card_layout('vanish into memory', 'normal').

% Found in: VIS
card_name('vanishing', 'Vanishing').
card_type('vanishing', 'Enchantment — Aura').
card_types('vanishing', ['Enchantment']).
card_subtypes('vanishing', ['Aura']).
card_colors('vanishing', ['U']).
card_text('vanishing', 'Enchant creature\n{U}{U}: Enchanted creature phases out. (While it\'s phased out, it\'s treated as though it doesn\'t exist. It phases in before its controller untaps during his or her next untap step.)').
card_mana_cost('vanishing', ['U']).
card_cmc('vanishing', 1).
card_layout('vanishing', 'normal').

% Found in: AVR
card_name('vanishment', 'Vanishment').
card_type('vanishment', 'Instant').
card_types('vanishment', ['Instant']).
card_subtypes('vanishment', []).
card_colors('vanishment', ['U']).
card_text('vanishment', 'Put target nonland permanent on top of its owner\'s library.\nMiracle {U} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_mana_cost('vanishment', ['4', 'U']).
card_cmc('vanishment', 5).
card_layout('vanishment', 'normal').

% Found in: 5DN
card_name('vanquish', 'Vanquish').
card_type('vanquish', 'Instant').
card_types('vanquish', ['Instant']).
card_subtypes('vanquish', []).
card_colors('vanquish', ['W']).
card_text('vanquish', 'Destroy target blocking creature.').
card_mana_cost('vanquish', ['2', 'W']).
card_cmc('vanquish', 3).
card_layout('vanquish', 'normal').

% Found in: THS
card_name('vanquish the foul', 'Vanquish the Foul').
card_type('vanquish the foul', 'Sorcery').
card_types('vanquish the foul', ['Sorcery']).
card_subtypes('vanquish the foul', []).
card_colors('vanquish the foul', ['W']).
card_text('vanquish the foul', 'Destroy target creature with power 4 or greater. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('vanquish the foul', ['5', 'W']).
card_cmc('vanquish the foul', 6).
card_layout('vanquish the foul', 'normal').

% Found in: DDH, MM2, NPH
card_name('vapor snag', 'Vapor Snag').
card_type('vapor snag', 'Instant').
card_types('vapor snag', ['Instant']).
card_subtypes('vapor snag', []).
card_colors('vapor snag', ['U']).
card_text('vapor snag', 'Return target creature to its owner\'s hand. Its controller loses 1 life.').
card_mana_cost('vapor snag', ['U']).
card_cmc('vapor snag', 1).
card_layout('vapor snag', 'normal').

% Found in: WWK
card_name('vapor snare', 'Vapor Snare').
card_type('vapor snare', 'Enchantment — Aura').
card_types('vapor snare', ['Enchantment']).
card_subtypes('vapor snare', ['Aura']).
card_colors('vapor snare', ['U']).
card_text('vapor snare', 'Enchant creature\nYou control enchanted creature.\nAt the beginning of your upkeep, sacrifice Vapor Snare unless you return a land you control to its owner\'s hand.').
card_mana_cost('vapor snare', ['4', 'U']).
card_cmc('vapor snare', 5).
card_layout('vapor snare', 'normal').

% Found in: THS
card_name('vaporkin', 'Vaporkin').
card_type('vaporkin', 'Creature — Elemental').
card_types('vaporkin', ['Creature']).
card_subtypes('vaporkin', ['Elemental']).
card_colors('vaporkin', ['U']).
card_text('vaporkin', 'Flying\nVaporkin can block only creatures with flying.').
card_mana_cost('vaporkin', ['1', 'U']).
card_cmc('vaporkin', 2).
card_layout('vaporkin', 'normal').
card_power('vaporkin', 2).
card_toughness('vaporkin', 1).

% Found in: MIR
card_name('vaporous djinn', 'Vaporous Djinn').
card_type('vaporous djinn', 'Creature — Djinn').
card_types('vaporous djinn', ['Creature']).
card_subtypes('vaporous djinn', ['Djinn']).
card_colors('vaporous djinn', ['U']).
card_text('vaporous djinn', 'Flying\nAt the beginning of your upkeep, Vaporous Djinn phases out unless you pay {U}{U}. (While it\'s phased out, it\'s treated as though it doesn\'t exist. It phases in before you untap during your next untap step.)').
card_mana_cost('vaporous djinn', ['2', 'U', 'U']).
card_cmc('vaporous djinn', 4).
card_layout('vaporous djinn', 'normal').
card_power('vaporous djinn', 3).
card_toughness('vaporous djinn', 4).

% Found in: ALL, ME2
card_name('varchild\'s crusader', 'Varchild\'s Crusader').
card_type('varchild\'s crusader', 'Creature — Human Knight').
card_types('varchild\'s crusader', ['Creature']).
card_subtypes('varchild\'s crusader', ['Human', 'Knight']).
card_colors('varchild\'s crusader', ['R']).
card_text('varchild\'s crusader', '{0}: Varchild\'s Crusader can\'t be blocked this turn except by Walls. Sacrifice Varchild\'s Crusader at the beginning of the next end step.').
card_mana_cost('varchild\'s crusader', ['3', 'R']).
card_cmc('varchild\'s crusader', 4).
card_layout('varchild\'s crusader', 'normal').
card_power('varchild\'s crusader', 3).
card_toughness('varchild\'s crusader', 2).

% Found in: ALL, MED
card_name('varchild\'s war-riders', 'Varchild\'s War-Riders').
card_type('varchild\'s war-riders', 'Creature — Human Warrior').
card_types('varchild\'s war-riders', ['Creature']).
card_subtypes('varchild\'s war-riders', ['Human', 'Warrior']).
card_colors('varchild\'s war-riders', ['R']).
card_text('varchild\'s war-riders', 'Cumulative upkeep—Have an opponent put a 1/1 red Survivor creature token onto the battlefield. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nTrample; rampage 1 (Whenever this creature becomes blocked, it gets +1/+1 until end of turn for each creature blocking it beyond the first.)').
card_mana_cost('varchild\'s war-riders', ['1', 'R']).
card_cmc('varchild\'s war-riders', 2).
card_layout('varchild\'s war-riders', 'normal').
card_power('varchild\'s war-riders', 3).
card_toughness('varchild\'s war-riders', 4).
card_reserved('varchild\'s war-riders').

% Found in: DGM
card_name('varolz, the scar-striped', 'Varolz, the Scar-Striped').
card_type('varolz, the scar-striped', 'Legendary Creature — Troll Warrior').
card_types('varolz, the scar-striped', ['Creature']).
card_subtypes('varolz, the scar-striped', ['Troll', 'Warrior']).
card_supertypes('varolz, the scar-striped', ['Legendary']).
card_colors('varolz, the scar-striped', ['B', 'G']).
card_text('varolz, the scar-striped', 'Each creature card in your graveyard has scavenge. The scavenge cost is equal to its mana cost. (Exile a creature card from your graveyard and pay its mana cost: Put a number of +1/+1 counters equal to that card\'s power on target creature. Scavenge only as a sorcery.)\nSacrifice another creature: Regenerate Varolz, the Scar-Striped.').
card_mana_cost('varolz, the scar-striped', ['1', 'B', 'G']).
card_cmc('varolz, the scar-striped', 3).
card_layout('varolz, the scar-striped', 'normal').
card_power('varolz, the scar-striped', 2).
card_toughness('varolz, the scar-striped', 2).

% Found in: RTR
card_name('vassal soul', 'Vassal Soul').
card_type('vassal soul', 'Creature — Spirit').
card_types('vassal soul', ['Creature']).
card_subtypes('vassal soul', ['Spirit']).
card_colors('vassal soul', ['W', 'U']).
card_text('vassal soul', 'Flying').
card_mana_cost('vassal soul', ['1', 'W/U', 'W/U']).
card_cmc('vassal soul', 3).
card_layout('vassal soul', 'normal').
card_power('vassal soul', 2).
card_toughness('vassal soul', 2).

% Found in: CHK
card_name('vassal\'s duty', 'Vassal\'s Duty').
card_type('vassal\'s duty', 'Enchantment').
card_types('vassal\'s duty', ['Enchantment']).
card_subtypes('vassal\'s duty', []).
card_colors('vassal\'s duty', ['W']).
card_text('vassal\'s duty', '{1}: The next 1 damage that would be dealt to target legendary creature you control this turn is dealt to you instead.').
card_mana_cost('vassal\'s duty', ['3', 'W']).
card_cmc('vassal\'s duty', 4).
card_layout('vassal\'s duty', 'normal').

% Found in: WWK
card_name('vastwood animist', 'Vastwood Animist').
card_type('vastwood animist', 'Creature — Elf Shaman Ally').
card_types('vastwood animist', ['Creature']).
card_subtypes('vastwood animist', ['Elf', 'Shaman', 'Ally']).
card_colors('vastwood animist', ['G']).
card_text('vastwood animist', '{T}: Target land you control becomes an X/X Elemental creature until end of turn, where X is the number of Allies you control. It\'s still a land.').
card_mana_cost('vastwood animist', ['2', 'G']).
card_cmc('vastwood animist', 3).
card_layout('vastwood animist', 'normal').
card_power('vastwood animist', 1).
card_toughness('vastwood animist', 1).

% Found in: M12, M13, ORI, ZEN
card_name('vastwood gorger', 'Vastwood Gorger').
card_type('vastwood gorger', 'Creature — Wurm').
card_types('vastwood gorger', ['Creature']).
card_subtypes('vastwood gorger', ['Wurm']).
card_colors('vastwood gorger', ['G']).
card_text('vastwood gorger', '').
card_mana_cost('vastwood gorger', ['5', 'G']).
card_cmc('vastwood gorger', 6).
card_layout('vastwood gorger', 'normal').
card_power('vastwood gorger', 5).
card_toughness('vastwood gorger', 6).

% Found in: M14
card_name('vastwood hydra', 'Vastwood Hydra').
card_type('vastwood hydra', 'Creature — Hydra').
card_types('vastwood hydra', ['Creature']).
card_subtypes('vastwood hydra', ['Hydra']).
card_colors('vastwood hydra', ['G']).
card_text('vastwood hydra', 'Vastwood Hydra enters the battlefield with X +1/+1 counters on it.\nWhen Vastwood Hydra dies, you may distribute a number of +1/+1 counters equal to the number of +1/+1 counters on Vastwood Hydra among any number of creatures you control.').
card_mana_cost('vastwood hydra', ['X', 'G', 'G']).
card_cmc('vastwood hydra', 2).
card_layout('vastwood hydra', 'normal').
card_power('vastwood hydra', 0).
card_toughness('vastwood hydra', 0).

% Found in: WWK
card_name('vastwood zendikon', 'Vastwood Zendikon').
card_type('vastwood zendikon', 'Enchantment — Aura').
card_types('vastwood zendikon', ['Enchantment']).
card_subtypes('vastwood zendikon', ['Aura']).
card_colors('vastwood zendikon', ['G']).
card_text('vastwood zendikon', 'Enchant land\nEnchanted land is a 6/4 green Elemental creature. It\'s still a land.\nWhen enchanted land dies, return that card to its owner\'s hand.').
card_mana_cost('vastwood zendikon', ['4', 'G']).
card_cmc('vastwood zendikon', 5).
card_layout('vastwood zendikon', 'normal').

% Found in: DKA, MD1
card_name('vault of the archangel', 'Vault of the Archangel').
card_type('vault of the archangel', 'Land').
card_types('vault of the archangel', ['Land']).
card_subtypes('vault of the archangel', []).
card_colors('vault of the archangel', []).
card_text('vault of the archangel', '{T}: Add {1} to your mana pool.\n{2}{W}{B}, {T}: Creatures you control gain deathtouch and lifelink until end of turn.').
card_layout('vault of the archangel', 'normal').

% Found in: HOP, MRD
card_name('vault of whispers', 'Vault of Whispers').
card_type('vault of whispers', 'Artifact Land').
card_types('vault of whispers', ['Artifact', 'Land']).
card_subtypes('vault of whispers', []).
card_colors('vault of whispers', []).
card_text('vault of whispers', '(Vault of Whispers isn\'t a spell.)\n{T}: Add {B} to your mana pool.').
card_layout('vault of whispers', 'normal').

% Found in: NPH, pWPN
card_name('vault skirge', 'Vault Skirge').
card_type('vault skirge', 'Artifact Creature — Imp').
card_types('vault skirge', ['Artifact', 'Creature']).
card_subtypes('vault skirge', ['Imp']).
card_colors('vault skirge', ['B']).
card_text('vault skirge', '({B/P} can be paid with either {B} or 2 life.)\nFlying\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('vault skirge', ['1', 'B/P']).
card_cmc('vault skirge', 2).
card_layout('vault skirge', 'normal').
card_power('vault skirge', 1).
card_toughness('vault skirge', 1).

% Found in: SOM
card_name('vault skyward', 'Vault Skyward').
card_type('vault skyward', 'Instant').
card_types('vault skyward', ['Instant']).
card_subtypes('vault skyward', []).
card_colors('vault skyward', ['U']).
card_text('vault skyward', 'Target creature gains flying until end of turn. Untap it.').
card_mana_cost('vault skyward', ['U']).
card_cmc('vault skyward', 1).
card_layout('vault skyward', 'normal').

% Found in: FRF
card_name('vaultbreaker', 'Vaultbreaker').
card_type('vaultbreaker', 'Creature — Orc Rogue').
card_types('vaultbreaker', ['Creature']).
card_subtypes('vaultbreaker', ['Orc', 'Rogue']).
card_colors('vaultbreaker', ['R']).
card_text('vaultbreaker', 'Whenever Vaultbreaker attacks, you may discard a card. If you do, draw a card.\nDash {2}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('vaultbreaker', ['3', 'R']).
card_cmc('vaultbreaker', 4).
card_layout('vaultbreaker', 'normal').
card_power('vaultbreaker', 4).
card_toughness('vaultbreaker', 2).

% Found in: USG
card_name('vebulid', 'Vebulid').
card_type('vebulid', 'Creature — Horror').
card_types('vebulid', ['Creature']).
card_subtypes('vebulid', ['Horror']).
card_colors('vebulid', ['B']).
card_text('vebulid', 'Vebulid enters the battlefield with a +1/+1 counter on it.\nAt the beginning of your upkeep, you may put a +1/+1 counter on Vebulid.\nWhen Vebulid attacks or blocks, destroy it at end of combat.').
card_mana_cost('vebulid', ['B']).
card_cmc('vebulid', 1).
card_layout('vebulid', 'normal').
card_power('vebulid', 0).
card_toughness('vebulid', 0).

% Found in: BRB, TMP, TPR
card_name('vec townships', 'Vec Townships').
card_type('vec townships', 'Land').
card_types('vec townships', ['Land']).
card_subtypes('vec townships', []).
card_colors('vec townships', []).
card_text('vec townships', '{T}: Add {1} to your mana pool.\n{T}: Add {G} or {W} to your mana pool. Vec Townships doesn\'t untap during your next untap step.').
card_layout('vec townships', 'normal').

% Found in: CON
card_name('vectis agents', 'Vectis Agents').
card_type('vectis agents', 'Artifact Creature — Human Rogue').
card_types('vectis agents', ['Artifact', 'Creature']).
card_subtypes('vectis agents', ['Human', 'Rogue']).
card_colors('vectis agents', ['U', 'B']).
card_text('vectis agents', '{U}{B}: Vectis Agents gets -2/-0 until end of turn and can\'t be blocked this turn.').
card_mana_cost('vectis agents', ['3', 'U', 'B']).
card_cmc('vectis agents', 5).
card_layout('vectis agents', 'normal').
card_power('vectis agents', 4).
card_toughness('vectis agents', 3).

% Found in: ARB
card_name('vectis dominator', 'Vectis Dominator').
card_type('vectis dominator', 'Artifact Creature — Human Wizard').
card_types('vectis dominator', ['Artifact', 'Creature']).
card_subtypes('vectis dominator', ['Human', 'Wizard']).
card_colors('vectis dominator', ['W', 'B']).
card_text('vectis dominator', '{T}: Tap target creature unless its controller pays 2 life.').
card_mana_cost('vectis dominator', ['1', 'W', 'B']).
card_cmc('vectis dominator', 3).
card_layout('vectis dominator', 'normal').
card_power('vectis dominator', 0).
card_toughness('vectis dominator', 2).

% Found in: ALA
card_name('vectis silencers', 'Vectis Silencers').
card_type('vectis silencers', 'Artifact Creature — Human Rogue').
card_types('vectis silencers', ['Artifact', 'Creature']).
card_subtypes('vectis silencers', ['Human', 'Rogue']).
card_colors('vectis silencers', ['U']).
card_text('vectis silencers', '{2}{B}: Vectis Silencers gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy that creature.)').
card_mana_cost('vectis silencers', ['2', 'U']).
card_cmc('vectis silencers', 3).
card_layout('vectis silencers', 'normal').
card_power('vectis silencers', 1).
card_toughness('vectis silencers', 2).

% Found in: SOM
card_name('vector asp', 'Vector Asp').
card_type('vector asp', 'Artifact Creature — Snake').
card_types('vector asp', ['Artifact', 'Creature']).
card_subtypes('vector asp', ['Snake']).
card_colors('vector asp', []).
card_text('vector asp', '{B}: Vector Asp gains infect until end of turn. (It deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('vector asp', ['1']).
card_cmc('vector asp', 1).
card_layout('vector asp', 'normal').
card_power('vector asp', 1).
card_toughness('vector asp', 1).

% Found in: MBS
card_name('vedalken anatomist', 'Vedalken Anatomist').
card_type('vedalken anatomist', 'Creature — Vedalken Wizard').
card_types('vedalken anatomist', ['Creature']).
card_subtypes('vedalken anatomist', ['Vedalken', 'Wizard']).
card_colors('vedalken anatomist', ['U']).
card_text('vedalken anatomist', '{2}{U}, {T}: Put a -1/-1 counter on target creature. You may tap or untap that creature.').
card_mana_cost('vedalken anatomist', ['2', 'U']).
card_cmc('vedalken anatomist', 3).
card_layout('vedalken anatomist', 'normal').
card_power('vedalken anatomist', 1).
card_toughness('vedalken anatomist', 2).

% Found in: MRD
card_name('vedalken archmage', 'Vedalken Archmage').
card_type('vedalken archmage', 'Creature — Vedalken Wizard').
card_types('vedalken archmage', ['Creature']).
card_subtypes('vedalken archmage', ['Vedalken', 'Wizard']).
card_colors('vedalken archmage', ['U']).
card_text('vedalken archmage', 'Whenever you cast an artifact spell, draw a card.').
card_mana_cost('vedalken archmage', ['2', 'U', 'U']).
card_cmc('vedalken archmage', 4).
card_layout('vedalken archmage', 'normal').
card_power('vedalken archmage', 0).
card_toughness('vedalken archmage', 2).

% Found in: SOM
card_name('vedalken certarch', 'Vedalken Certarch').
card_type('vedalken certarch', 'Creature — Vedalken Wizard').
card_types('vedalken certarch', ['Creature']).
card_subtypes('vedalken certarch', ['Vedalken', 'Wizard']).
card_colors('vedalken certarch', ['U']).
card_text('vedalken certarch', 'Metalcraft — {T}: Tap target artifact, creature, or land. Activate this ability only if you control three or more artifacts.').
card_mana_cost('vedalken certarch', ['U']).
card_cmc('vedalken certarch', 1).
card_layout('vedalken certarch', 'normal').
card_power('vedalken certarch', 1).
card_toughness('vedalken certarch', 1).

% Found in: MMA, RAV
card_name('vedalken dismisser', 'Vedalken Dismisser').
card_type('vedalken dismisser', 'Creature — Vedalken Wizard').
card_types('vedalken dismisser', ['Creature']).
card_subtypes('vedalken dismisser', ['Vedalken', 'Wizard']).
card_colors('vedalken dismisser', ['U']).
card_text('vedalken dismisser', 'When Vedalken Dismisser enters the battlefield, put target creature on top of its owner\'s library.').
card_mana_cost('vedalken dismisser', ['5', 'U']).
card_cmc('vedalken dismisser', 6).
card_layout('vedalken dismisser', 'normal').
card_power('vedalken dismisser', 2).
card_toughness('vedalken dismisser', 2).

% Found in: DST, HOP
card_name('vedalken engineer', 'Vedalken Engineer').
card_type('vedalken engineer', 'Creature — Vedalken Artificer').
card_types('vedalken engineer', ['Creature']).
card_subtypes('vedalken engineer', ['Vedalken', 'Artificer']).
card_colors('vedalken engineer', ['U']).
card_text('vedalken engineer', '{T}: Add two mana of any one color to your mana pool. Spend this mana only to cast artifact spells or activate abilities of artifacts.').
card_mana_cost('vedalken engineer', ['1', 'U']).
card_cmc('vedalken engineer', 2).
card_layout('vedalken engineer', 'normal').
card_power('vedalken engineer', 1).
card_toughness('vedalken engineer', 1).

% Found in: M13, RAV
card_name('vedalken entrancer', 'Vedalken Entrancer').
card_type('vedalken entrancer', 'Creature — Vedalken Wizard').
card_types('vedalken entrancer', ['Creature']).
card_subtypes('vedalken entrancer', ['Vedalken', 'Wizard']).
card_colors('vedalken entrancer', ['U']).
card_text('vedalken entrancer', '{U}, {T}: Target player puts the top two cards of his or her library into his or her graveyard.').
card_mana_cost('vedalken entrancer', ['3', 'U']).
card_cmc('vedalken entrancer', 4).
card_layout('vedalken entrancer', 'normal').
card_power('vedalken entrancer', 1).
card_toughness('vedalken entrancer', 4).

% Found in: ARB
card_name('vedalken ghoul', 'Vedalken Ghoul').
card_type('vedalken ghoul', 'Creature — Vedalken Zombie').
card_types('vedalken ghoul', ['Creature']).
card_subtypes('vedalken ghoul', ['Vedalken', 'Zombie']).
card_colors('vedalken ghoul', ['U', 'B']).
card_text('vedalken ghoul', 'Whenever Vedalken Ghoul becomes blocked, defending player loses 4 life.').
card_mana_cost('vedalken ghoul', ['U', 'B']).
card_cmc('vedalken ghoul', 2).
card_layout('vedalken ghoul', 'normal').
card_power('vedalken ghoul', 1).
card_toughness('vedalken ghoul', 1).

% Found in: ARB
card_name('vedalken heretic', 'Vedalken Heretic').
card_type('vedalken heretic', 'Creature — Vedalken Rogue').
card_types('vedalken heretic', ['Creature']).
card_subtypes('vedalken heretic', ['Vedalken', 'Rogue']).
card_colors('vedalken heretic', ['U', 'G']).
card_text('vedalken heretic', 'Whenever Vedalken Heretic deals damage to an opponent, you may draw a card.').
card_mana_cost('vedalken heretic', ['G', 'U']).
card_cmc('vedalken heretic', 2).
card_layout('vedalken heretic', 'normal').
card_power('vedalken heretic', 1).
card_toughness('vedalken heretic', 1).

% Found in: MBS
card_name('vedalken infuser', 'Vedalken Infuser').
card_type('vedalken infuser', 'Creature — Vedalken Wizard').
card_types('vedalken infuser', ['Creature']).
card_subtypes('vedalken infuser', ['Vedalken', 'Wizard']).
card_colors('vedalken infuser', ['U']).
card_text('vedalken infuser', 'At the beginning of your upkeep, you may put a charge counter on target artifact.').
card_mana_cost('vedalken infuser', ['3', 'U']).
card_cmc('vedalken infuser', 4).
card_layout('vedalken infuser', 'normal').
card_power('vedalken infuser', 1).
card_toughness('vedalken infuser', 4).

% Found in: 10E, 5DN
card_name('vedalken mastermind', 'Vedalken Mastermind').
card_type('vedalken mastermind', 'Creature — Vedalken Wizard').
card_types('vedalken mastermind', ['Creature']).
card_subtypes('vedalken mastermind', ['Vedalken', 'Wizard']).
card_colors('vedalken mastermind', ['U']).
card_text('vedalken mastermind', '{U}, {T}: Return target permanent you control to its owner\'s hand.').
card_mana_cost('vedalken mastermind', ['U', 'U']).
card_cmc('vedalken mastermind', 2).
card_layout('vedalken mastermind', 'normal').
card_power('vedalken mastermind', 1).
card_toughness('vedalken mastermind', 2).

% Found in: 5DN, CNS
card_name('vedalken orrery', 'Vedalken Orrery').
card_type('vedalken orrery', 'Artifact').
card_types('vedalken orrery', ['Artifact']).
card_subtypes('vedalken orrery', []).
card_colors('vedalken orrery', []).
card_text('vedalken orrery', 'You may cast nonland cards as though they had flash.').
card_mana_cost('vedalken orrery', ['4']).
card_cmc('vedalken orrery', 4).
card_layout('vedalken orrery', 'normal').

% Found in: CON
card_name('vedalken outlander', 'Vedalken Outlander').
card_type('vedalken outlander', 'Artifact Creature — Vedalken Scout').
card_types('vedalken outlander', ['Artifact', 'Creature']).
card_subtypes('vedalken outlander', ['Vedalken', 'Scout']).
card_colors('vedalken outlander', ['W', 'U']).
card_text('vedalken outlander', 'Protection from red').
card_mana_cost('vedalken outlander', ['W', 'U']).
card_cmc('vedalken outlander', 2).
card_layout('vedalken outlander', 'normal').
card_power('vedalken outlander', 2).
card_toughness('vedalken outlander', 2).

% Found in: CMD, GPT
card_name('vedalken plotter', 'Vedalken Plotter').
card_type('vedalken plotter', 'Creature — Vedalken Wizard').
card_types('vedalken plotter', ['Creature']).
card_subtypes('vedalken plotter', ['Vedalken', 'Wizard']).
card_colors('vedalken plotter', ['U']).
card_text('vedalken plotter', 'When Vedalken Plotter enters the battlefield, exchange control of target land you control and target land an opponent controls.').
card_mana_cost('vedalken plotter', ['2', 'U']).
card_cmc('vedalken plotter', 3).
card_layout('vedalken plotter', 'normal').
card_power('vedalken plotter', 1).
card_toughness('vedalken plotter', 1).

% Found in: 5DN, MMA
card_name('vedalken shackles', 'Vedalken Shackles').
card_type('vedalken shackles', 'Artifact').
card_types('vedalken shackles', ['Artifact']).
card_subtypes('vedalken shackles', []).
card_colors('vedalken shackles', []).
card_text('vedalken shackles', 'You may choose not to untap Vedalken Shackles during your untap step.\n{2}, {T}: Gain control of target creature with power less than or equal to the number of Islands you control for as long as Vedalken Shackles remains tapped.').
card_mana_cost('vedalken shackles', ['3']).
card_cmc('vedalken shackles', 3).
card_layout('vedalken shackles', 'normal').

% Found in: FUT
card_name('vedalken æthermage', 'Vedalken Æthermage').
card_type('vedalken æthermage', 'Creature — Vedalken Wizard').
card_types('vedalken æthermage', ['Creature']).
card_subtypes('vedalken æthermage', ['Vedalken', 'Wizard']).
card_colors('vedalken æthermage', ['U']).
card_text('vedalken æthermage', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Vedalken Æthermage enters the battlefield, return target Sliver to its owner\'s hand.\nWizardcycling {3} ({3}, Discard this card: Search your library for a Wizard card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('vedalken æthermage', ['1', 'U']).
card_cmc('vedalken æthermage', 2).
card_layout('vedalken æthermage', 'normal').
card_power('vedalken æthermage', 1).
card_toughness('vedalken æthermage', 2).

% Found in: USG
card_name('veil of birds', 'Veil of Birds').
card_type('veil of birds', 'Enchantment').
card_types('veil of birds', ['Enchantment']).
card_subtypes('veil of birds', []).
card_colors('veil of birds', ['U']).
card_text('veil of birds', 'When an opponent casts a spell, if Veil of Birds is an enchantment, Veil of Birds becomes a 1/1 Bird creature with flying.').
card_mana_cost('veil of birds', ['U']).
card_cmc('veil of birds', 1).
card_layout('veil of birds', 'normal').

% Found in: BOK
card_name('veil of secrecy', 'Veil of Secrecy').
card_type('veil of secrecy', 'Instant — Arcane').
card_types('veil of secrecy', ['Instant']).
card_subtypes('veil of secrecy', ['Arcane']).
card_colors('veil of secrecy', ['U']).
card_text('veil of secrecy', 'Target creature gains shroud until end of turn and can\'t be blocked this turn. (A creature with shroud can\'t be the target of spells or abilities.)\nSplice onto Arcane—Return a blue creature you control to its owner\'s hand. (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('veil of secrecy', ['1', 'U']).
card_cmc('veil of secrecy', 2).
card_layout('veil of secrecy', 'normal').

% Found in: M13
card_name('veilborn ghoul', 'Veilborn Ghoul').
card_type('veilborn ghoul', 'Creature — Zombie').
card_types('veilborn ghoul', ['Creature']).
card_subtypes('veilborn ghoul', ['Zombie']).
card_colors('veilborn ghoul', ['B']).
card_text('veilborn ghoul', 'Veilborn Ghoul can\'t block.\nWhenever a Swamp enters the battlefield under your control, you may return Veilborn Ghoul from your graveyard to your hand.').
card_mana_cost('veilborn ghoul', ['4', 'B']).
card_cmc('veilborn ghoul', 5).
card_layout('veilborn ghoul', 'normal').
card_power('veilborn ghoul', 4).
card_toughness('veilborn ghoul', 1).

% Found in: USG
card_name('veiled apparition', 'Veiled Apparition').
card_type('veiled apparition', 'Enchantment').
card_types('veiled apparition', ['Enchantment']).
card_subtypes('veiled apparition', []).
card_colors('veiled apparition', ['U']).
card_text('veiled apparition', 'When an opponent casts a spell, if Veiled Apparition is an enchantment, Veiled Apparition becomes a 3/3 Illusion creature with flying and \"At the beginning of your upkeep, sacrifice Veiled Apparition unless you pay {1}{U}.\"').
card_mana_cost('veiled apparition', ['1', 'U']).
card_cmc('veiled apparition', 2).
card_layout('veiled apparition', 'normal').

% Found in: USG
card_name('veiled crocodile', 'Veiled Crocodile').
card_type('veiled crocodile', 'Enchantment').
card_types('veiled crocodile', ['Enchantment']).
card_subtypes('veiled crocodile', []).
card_colors('veiled crocodile', ['U']).
card_text('veiled crocodile', 'When a player has no cards in hand, if Veiled Crocodile is an enchantment, Veiled Crocodile becomes a 4/4 Crocodile creature.').
card_mana_cost('veiled crocodile', ['2', 'U']).
card_cmc('veiled crocodile', 3).
card_layout('veiled crocodile', 'normal').

% Found in: USG
card_name('veiled sentry', 'Veiled Sentry').
card_type('veiled sentry', 'Enchantment').
card_types('veiled sentry', ['Enchantment']).
card_subtypes('veiled sentry', []).
card_colors('veiled sentry', ['U']).
card_text('veiled sentry', 'When an opponent casts a spell, if Veiled Sentry is an enchantment, Veiled Sentry becomes an Illusion creature with power and toughness each equal to that spell\'s converted mana cost.').
card_mana_cost('veiled sentry', ['U']).
card_cmc('veiled sentry', 1).
card_layout('veiled sentry', 'normal').

% Found in: USG
card_name('veiled serpent', 'Veiled Serpent').
card_type('veiled serpent', 'Enchantment').
card_types('veiled serpent', ['Enchantment']).
card_subtypes('veiled serpent', []).
card_colors('veiled serpent', ['U']).
card_text('veiled serpent', 'When an opponent casts a spell, if Veiled Serpent is an enchantment, Veiled Serpent becomes a 4/4 Serpent creature with \"This creature can\'t attack unless defending player controls an Island.\"\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('veiled serpent', ['2', 'U']).
card_cmc('veiled serpent', 3).
card_layout('veiled serpent', 'normal').

% Found in: PLC
card_name('veiling oddity', 'Veiling Oddity').
card_type('veiling oddity', 'Creature — Illusion').
card_types('veiling oddity', ['Creature']).
card_subtypes('veiling oddity', ['Illusion']).
card_colors('veiling oddity', ['U']).
card_text('veiling oddity', 'Suspend 4—{1}{U} (Rather than cast this card from your hand, you may pay {1}{U} and exile it with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)\nWhen the last time counter is removed from Veiling Oddity while it\'s exiled, creatures can\'t be blocked this turn.').
card_mana_cost('veiling oddity', ['3', 'U']).
card_cmc('veiling oddity', 4).
card_layout('veiling oddity', 'normal').
card_power('veiling oddity', 2).
card_toughness('veiling oddity', 3).

% Found in: FUT
card_name('veilstone amulet', 'Veilstone Amulet').
card_type('veilstone amulet', 'Artifact').
card_types('veilstone amulet', ['Artifact']).
card_subtypes('veilstone amulet', []).
card_colors('veilstone amulet', []).
card_text('veilstone amulet', 'Whenever you cast a spell, creatures you control can\'t be the targets of spells or abilities your opponents control this turn.').
card_mana_cost('veilstone amulet', ['3']).
card_cmc('veilstone amulet', 3).
card_layout('veilstone amulet', 'normal').

% Found in: ALA
card_name('vein drinker', 'Vein Drinker').
card_type('vein drinker', 'Creature — Vampire').
card_types('vein drinker', ['Creature']).
card_subtypes('vein drinker', ['Vampire']).
card_colors('vein drinker', ['B']).
card_text('vein drinker', 'Flying\n{R}, {T}: Vein Drinker deals damage equal to its power to target creature. That creature deals damage equal to its power to Vein Drinker.\nWhenever a creature dealt damage by Vein Drinker this turn dies, put a +1/+1 counter on Vein Drinker.').
card_mana_cost('vein drinker', ['4', 'B', 'B']).
card_cmc('vein drinker', 6).
card_layout('vein drinker', 'normal').
card_power('vein drinker', 4).
card_toughness('vein drinker', 4).

% Found in: ARB
card_name('veinfire borderpost', 'Veinfire Borderpost').
card_type('veinfire borderpost', 'Artifact').
card_types('veinfire borderpost', ['Artifact']).
card_subtypes('veinfire borderpost', []).
card_colors('veinfire borderpost', ['B', 'R']).
card_text('veinfire borderpost', 'You may pay {1} and return a basic land you control to its owner\'s hand rather than pay Veinfire Borderpost\'s mana cost.\nVeinfire Borderpost enters the battlefield tapped.\n{T}: Add {B} or {R} to your mana pool.').
card_mana_cost('veinfire borderpost', ['1', 'B', 'R']).
card_cmc('veinfire borderpost', 3).
card_layout('veinfire borderpost', 'normal').

% Found in: CM1, PC2
card_name('vela the night-clad', 'Vela the Night-Clad').
card_type('vela the night-clad', 'Legendary Creature — Human Wizard').
card_types('vela the night-clad', ['Creature']).
card_subtypes('vela the night-clad', ['Human', 'Wizard']).
card_supertypes('vela the night-clad', ['Legendary']).
card_colors('vela the night-clad', ['U', 'B']).
card_text('vela the night-clad', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nOther creatures you control have intimidate.\nWhenever Vela the Night-Clad or another creature you control leaves the battlefield, each opponent loses 1 life.').
card_mana_cost('vela the night-clad', ['4', 'U', 'B']).
card_cmc('vela the night-clad', 6).
card_layout('vela the night-clad', 'normal').
card_power('vela the night-clad', 4).
card_toughness('vela the night-clad', 4).

% Found in: HML
card_name('veldrane of sengir', 'Veldrane of Sengir').
card_type('veldrane of sengir', 'Legendary Creature — Human Rogue').
card_types('veldrane of sengir', ['Creature']).
card_subtypes('veldrane of sengir', ['Human', 'Rogue']).
card_supertypes('veldrane of sengir', ['Legendary']).
card_colors('veldrane of sengir', ['B']).
card_text('veldrane of sengir', '{1}{B}{B}: Veldrane of Sengir gets -3/-0 and gains forestwalk until end of turn. (It can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('veldrane of sengir', ['5', 'B', 'B']).
card_cmc('veldrane of sengir', 7).
card_layout('veldrane of sengir', 'normal').
card_power('veldrane of sengir', 5).
card_toughness('veldrane of sengir', 5).
card_reserved('veldrane of sengir').

% Found in: ICE
card_name('veldt', 'Veldt').
card_type('veldt', 'Land').
card_types('veldt', ['Land']).
card_subtypes('veldt', []).
card_colors('veldt', []).
card_text('veldt', 'Veldt doesn\'t untap during your untap step if it has a depletion counter on it.\nAt the beginning of your upkeep, remove a depletion counter from Veldt.\n{T}: Add {G} or {W} to your mana pool. Put a depletion counter on Veldt.').
card_layout('veldt', 'normal').
card_reserved('veldt').

% Found in: HOP
card_name('velis vel', 'Velis Vel').
card_type('velis vel', 'Plane — Lorwyn').
card_types('velis vel', ['Plane']).
card_subtypes('velis vel', ['Lorwyn']).
card_colors('velis vel', []).
card_text('velis vel', 'Each creature gets +1/+1 for each other creature on the battlefield that shares at least one creature type with it. (For example, if two Elemental Shamans and an Elemental Spirit are on the battlefield, each gets +2/+2.)\nWhenever you roll {C}, target creature gains all creature types until end of turn.').
card_layout('velis vel', 'plane').

% Found in: PLC
card_name('venarian glimmer', 'Venarian Glimmer').
card_type('venarian glimmer', 'Instant').
card_types('venarian glimmer', ['Instant']).
card_subtypes('venarian glimmer', []).
card_colors('venarian glimmer', ['U']).
card_text('venarian glimmer', 'Target player reveals his or her hand. You choose a nonland card with converted mana cost X or less from it. That player discards that card.').
card_mana_cost('venarian glimmer', ['X', 'U']).
card_cmc('venarian glimmer', 1).
card_layout('venarian glimmer', 'normal').

% Found in: LEG
card_name('venarian gold', 'Venarian Gold').
card_type('venarian gold', 'Enchantment — Aura').
card_types('venarian gold', ['Enchantment']).
card_subtypes('venarian gold', ['Aura']).
card_colors('venarian gold', ['U']).
card_text('venarian gold', 'Enchant creature\nWhen Venarian Gold enters the battlefield, tap enchanted creature and put a number of sleep counters on it equal to the value of X as you cast Venarian Gold.\nEnchanted creature doesn\'t untap during its controller\'s untap step if it has a sleep counter on it.\nAt the beginning of the upkeep of enchanted creature\'s controller, remove a sleep counter from that creature.').
card_mana_cost('venarian gold', ['X', 'U', 'U']).
card_cmc('venarian gold', 2).
card_layout('venarian gold', 'normal').

% Found in: MMQ, ROE
card_name('vendetta', 'Vendetta').
card_type('vendetta', 'Instant').
card_types('vendetta', ['Instant']).
card_subtypes('vendetta', []).
card_colors('vendetta', ['B']).
card_text('vendetta', 'Destroy target nonblack creature. It can\'t be regenerated. You lose life equal to that creature\'s toughness.').
card_mana_cost('vendetta', ['B']).
card_cmc('vendetta', 1).
card_layout('vendetta', 'normal').

% Found in: MM2, MMA, MOR, pJGP
card_name('vendilion clique', 'Vendilion Clique').
card_type('vendilion clique', 'Legendary Creature — Faerie Wizard').
card_types('vendilion clique', ['Creature']).
card_subtypes('vendilion clique', ['Faerie', 'Wizard']).
card_supertypes('vendilion clique', ['Legendary']).
card_colors('vendilion clique', ['U']).
card_text('vendilion clique', 'Flash\nFlying\nWhen Vendilion Clique enters the battlefield, look at target player\'s hand. You may choose a nonland card from it. If you do, that player reveals the chosen card, puts it on the bottom of his or her library, then draws a card.').
card_mana_cost('vendilion clique', ['1', 'U', 'U']).
card_cmc('vendilion clique', 3).
card_layout('vendilion clique', 'normal').
card_power('vendilion clique', 3).
card_toughness('vendilion clique', 1).

% Found in: CHK
card_name('venerable kumo', 'Venerable Kumo').
card_type('venerable kumo', 'Creature — Spirit').
card_types('venerable kumo', ['Creature']).
card_subtypes('venerable kumo', ['Spirit']).
card_colors('venerable kumo', ['G']).
card_text('venerable kumo', 'Reach (This creature can block creatures with flying.)\nSoulshift 4 (When this creature dies, you may return target Spirit card with converted mana cost 4 or less from your graveyard to your hand.)').
card_mana_cost('venerable kumo', ['4', 'G']).
card_cmc('venerable kumo', 5).
card_layout('venerable kumo', 'normal').
card_power('venerable kumo', 2).
card_toughness('venerable kumo', 3).

% Found in: KTK
card_name('venerable lammasu', 'Venerable Lammasu').
card_type('venerable lammasu', 'Creature — Lammasu').
card_types('venerable lammasu', ['Creature']).
card_subtypes('venerable lammasu', ['Lammasu']).
card_colors('venerable lammasu', ['W']).
card_text('venerable lammasu', 'Flying').
card_mana_cost('venerable lammasu', ['6', 'W']).
card_cmc('venerable lammasu', 7).
card_layout('venerable lammasu', 'normal').
card_power('venerable lammasu', 5).
card_toughness('venerable lammasu', 4).

% Found in: 10E, 6ED, 7ED, 8ED, 9ED, DD3_DVD, DDC, POR, S00, S99, STH
card_name('venerable monk', 'Venerable Monk').
card_type('venerable monk', 'Creature — Human Monk Cleric').
card_types('venerable monk', ['Creature']).
card_subtypes('venerable monk', ['Human', 'Monk', 'Cleric']).
card_colors('venerable monk', ['W']).
card_text('venerable monk', 'When Venerable Monk enters the battlefield, you gain 2 life.').
card_mana_cost('venerable monk', ['2', 'W']).
card_cmc('venerable monk', 3).
card_layout('venerable monk', 'normal').
card_power('venerable monk', 2).
card_toughness('venerable monk', 2).

% Found in: ROE
card_name('venerated teacher', 'Venerated Teacher').
card_type('venerated teacher', 'Creature — Human Wizard').
card_types('venerated teacher', ['Creature']).
card_subtypes('venerated teacher', ['Human', 'Wizard']).
card_colors('venerated teacher', ['U']).
card_text('venerated teacher', 'When Venerated Teacher enters the battlefield, put two level counters on each creature you control with level up.').
card_mana_cost('venerated teacher', ['2', 'U']).
card_cmc('venerated teacher', 3).
card_layout('venerated teacher', 'normal').
card_power('venerated teacher', 2).
card_toughness('venerated teacher', 2).

% Found in: 7ED, 8ED, 9ED, PO2, POR, PTK, S99
card_name('vengeance', 'Vengeance').
card_type('vengeance', 'Sorcery').
card_types('vengeance', ['Sorcery']).
card_subtypes('vengeance', []).
card_colors('vengeance', ['W']).
card_text('vengeance', 'Destroy target tapped creature.').
card_mana_cost('vengeance', ['3', 'W']).
card_cmc('vengeance', 4).
card_layout('vengeance', 'normal').

% Found in: M11
card_name('vengeful archon', 'Vengeful Archon').
card_type('vengeful archon', 'Creature — Archon').
card_types('vengeful archon', ['Creature']).
card_subtypes('vengeful archon', ['Archon']).
card_colors('vengeful archon', ['W']).
card_text('vengeful archon', 'Flying\n{X}: Prevent the next X damage that would be dealt to you this turn. If damage is prevented this way, Vengeful Archon deals that much damage to target player.').
card_mana_cost('vengeful archon', ['4', 'W', 'W', 'W']).
card_cmc('vengeful archon', 7).
card_layout('vengeful archon', 'normal').
card_power('vengeful archon', 7).
card_toughness('vengeful archon', 7).

% Found in: SCG
card_name('vengeful dead', 'Vengeful Dead').
card_type('vengeful dead', 'Creature — Zombie').
card_types('vengeful dead', ['Creature']).
card_subtypes('vengeful dead', ['Zombie']).
card_colors('vengeful dead', ['B']).
card_text('vengeful dead', 'Whenever Vengeful Dead or another Zombie dies, each opponent loses 1 life.').
card_mana_cost('vengeful dead', ['3', 'B']).
card_cmc('vengeful dead', 4).
card_layout('vengeful dead', 'normal').
card_power('vengeful dead', 3).
card_toughness('vengeful dead', 2).

% Found in: TOR
card_name('vengeful dreams', 'Vengeful Dreams').
card_type('vengeful dreams', 'Instant').
card_types('vengeful dreams', ['Instant']).
card_subtypes('vengeful dreams', []).
card_colors('vengeful dreams', ['W']).
card_text('vengeful dreams', 'As an additional cost to cast Vengeful Dreams, discard X cards.\nExile X target attacking creatures.').
card_mana_cost('vengeful dreams', ['W', 'W']).
card_cmc('vengeful dreams', 2).
card_layout('vengeful dreams', 'normal').

% Found in: MOR
card_name('vengeful firebrand', 'Vengeful Firebrand').
card_type('vengeful firebrand', 'Creature — Elemental Warrior').
card_types('vengeful firebrand', ['Creature']).
card_subtypes('vengeful firebrand', ['Elemental', 'Warrior']).
card_colors('vengeful firebrand', ['R']).
card_text('vengeful firebrand', 'Vengeful Firebrand has haste as long as a Warrior card is in your graveyard.\n{R}: Vengeful Firebrand gets +1/+0 until end of turn.').
card_mana_cost('vengeful firebrand', ['3', 'R']).
card_cmc('vengeful firebrand', 4).
card_layout('vengeful firebrand', 'normal').
card_power('vengeful firebrand', 5).
card_toughness('vengeful firebrand', 2).

% Found in: M12
card_name('vengeful pharaoh', 'Vengeful Pharaoh').
card_type('vengeful pharaoh', 'Creature — Zombie').
card_types('vengeful pharaoh', ['Creature']).
card_subtypes('vengeful pharaoh', ['Zombie']).
card_colors('vengeful pharaoh', ['B']).
card_text('vengeful pharaoh', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nWhenever combat damage is dealt to you or a planeswalker you control, if Vengeful Pharaoh is in your graveyard, destroy target attacking creature, then put Vengeful Pharaoh on top of your library.').
card_mana_cost('vengeful pharaoh', ['2', 'B', 'B', 'B']).
card_cmc('vengeful pharaoh', 5).
card_layout('vengeful pharaoh', 'normal').
card_power('vengeful pharaoh', 5).
card_toughness('vengeful pharaoh', 4).

% Found in: ARB, CMD, MM2
card_name('vengeful rebirth', 'Vengeful Rebirth').
card_type('vengeful rebirth', 'Sorcery').
card_types('vengeful rebirth', ['Sorcery']).
card_subtypes('vengeful rebirth', []).
card_colors('vengeful rebirth', ['R', 'G']).
card_text('vengeful rebirth', 'Return target card from your graveyard to your hand. If you return a nonland card to your hand this way, Vengeful Rebirth deals damage equal to that card\'s converted mana cost to target creature or player.\nExile Vengeful Rebirth.').
card_mana_cost('vengeful rebirth', ['4', 'R', 'G']).
card_cmc('vengeful rebirth', 6).
card_layout('vengeful rebirth', 'normal').

% Found in: DKA
card_name('vengeful vampire', 'Vengeful Vampire').
card_type('vengeful vampire', 'Creature — Vampire').
card_types('vengeful vampire', ['Creature']).
card_subtypes('vengeful vampire', ['Vampire']).
card_colors('vengeful vampire', ['B']).
card_text('vengeful vampire', 'Flying\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('vengeful vampire', ['4', 'B', 'B']).
card_cmc('vengeful vampire', 6).
card_layout('vengeful vampire', 'normal').
card_power('vengeful vampire', 3).
card_toughness('vengeful vampire', 2).

% Found in: ROE, pWCQ
card_name('vengevine', 'Vengevine').
card_type('vengevine', 'Creature — Elemental').
card_types('vengevine', ['Creature']).
card_subtypes('vengevine', ['Elemental']).
card_colors('vengevine', ['G']).
card_text('vengevine', 'Haste\nWhenever you cast a spell, if it\'s the second creature spell you cast this turn, you may return Vengevine from your graveyard to the battlefield.').
card_mana_cost('vengevine', ['2', 'G', 'G']).
card_cmc('vengevine', 4).
card_layout('vengevine', 'normal').
card_power('vengevine', 4).
card_toughness('vengevine', 3).

% Found in: 4ED, 5ED, DRK
card_name('venom', 'Venom').
card_type('venom', 'Enchantment — Aura').
card_types('venom', ['Enchantment']).
card_subtypes('venom', ['Aura']).
card_colors('venom', ['G']).
card_text('venom', 'Enchant creature\nWhenever enchanted creature blocks or becomes blocked by a non-Wall creature, destroy the other creature at end of combat.').
card_mana_cost('venom', ['1', 'G', 'G']).
card_cmc('venom', 3).
card_layout('venom', 'normal').

% Found in: M15
card_name('venom sliver', 'Venom Sliver').
card_type('venom sliver', 'Creature — Sliver').
card_types('venom sliver', ['Creature']).
card_subtypes('venom sliver', ['Sliver']).
card_colors('venom sliver', ['G']).
card_text('venom sliver', 'Sliver creatures you control have deathtouch. (Any amount of damage a creature with deathtouch deals to a creature is enough to destroy it.)').
card_mana_cost('venom sliver', ['1', 'G']).
card_cmc('venom sliver', 2).
card_layout('venom sliver', 'normal').
card_power('venom sliver', 1).
card_toughness('venom sliver', 1).

% Found in: ICE, MMQ
card_name('venomous breath', 'Venomous Breath').
card_type('venomous breath', 'Instant').
card_types('venomous breath', ['Instant']).
card_subtypes('venomous breath', []).
card_colors('venomous breath', ['G']).
card_text('venomous breath', 'Choose target creature. At this turn\'s next end of combat, destroy all creatures that blocked or were blocked by it this turn.').
card_mana_cost('venomous breath', ['3', 'G']).
card_cmc('venomous breath', 4).
card_layout('venomous breath', 'normal').

% Found in: MMQ
card_name('venomous dragonfly', 'Venomous Dragonfly').
card_type('venomous dragonfly', 'Creature — Insect').
card_types('venomous dragonfly', ['Creature']).
card_subtypes('venomous dragonfly', ['Insect']).
card_colors('venomous dragonfly', ['G']).
card_text('venomous dragonfly', 'Flying\nWhenever Venomous Dragonfly blocks or becomes blocked by a creature, destroy that creature at end of combat.').
card_mana_cost('venomous dragonfly', ['3', 'G']).
card_cmc('venomous dragonfly', 4).
card_layout('venomous dragonfly', 'normal').
card_power('venomous dragonfly', 1).
card_toughness('venomous dragonfly', 1).

% Found in: USG
card_name('venomous fangs', 'Venomous Fangs').
card_type('venomous fangs', 'Enchantment — Aura').
card_types('venomous fangs', ['Enchantment']).
card_subtypes('venomous fangs', ['Aura']).
card_colors('venomous fangs', ['G']).
card_text('venomous fangs', 'Enchant creature\nWhenever enchanted creature deals damage to a creature, destroy the other creature.').
card_mana_cost('venomous fangs', ['2', 'G']).
card_cmc('venomous fangs', 3).
card_layout('venomous fangs', 'normal').

% Found in: JUD
card_name('venomous vines', 'Venomous Vines').
card_type('venomous vines', 'Sorcery').
card_types('venomous vines', ['Sorcery']).
card_subtypes('venomous vines', []).
card_colors('venomous vines', ['G']).
card_text('venomous vines', 'Destroy target enchanted permanent.').
card_mana_cost('venomous vines', ['2', 'G', 'G']).
card_cmc('venomous vines', 4).
card_layout('venomous vines', 'normal').

% Found in: ONS
card_name('venomspout brackus', 'Venomspout Brackus').
card_type('venomspout brackus', 'Creature — Beast').
card_types('venomspout brackus', ['Creature']).
card_subtypes('venomspout brackus', ['Beast']).
card_colors('venomspout brackus', ['G']).
card_text('venomspout brackus', '{1}{G}, {T}: Venomspout Brackus deals 5 damage to target attacking or blocking creature with flying.\nMorph {3}{G}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('venomspout brackus', ['6', 'G']).
card_cmc('venomspout brackus', 7).
card_layout('venomspout brackus', 'normal').
card_power('venomspout brackus', 5).
card_toughness('venomspout brackus', 5).

% Found in: FUT
card_name('venser\'s diffusion', 'Venser\'s Diffusion').
card_type('venser\'s diffusion', 'Instant').
card_types('venser\'s diffusion', ['Instant']).
card_subtypes('venser\'s diffusion', []).
card_colors('venser\'s diffusion', ['U']).
card_text('venser\'s diffusion', 'Return target nonland permanent or suspended card to its owner\'s hand.').
card_mana_cost('venser\'s diffusion', ['2', 'U']).
card_cmc('venser\'s diffusion', 3).
card_layout('venser\'s diffusion', 'normal').

% Found in: SOM
card_name('venser\'s journal', 'Venser\'s Journal').
card_type('venser\'s journal', 'Artifact').
card_types('venser\'s journal', ['Artifact']).
card_subtypes('venser\'s journal', []).
card_colors('venser\'s journal', []).
card_text('venser\'s journal', 'You have no maximum hand size.\nAt the beginning of your upkeep, you gain 1 life for each card in your hand.').
card_mana_cost('venser\'s journal', ['5']).
card_cmc('venser\'s journal', 5).
card_layout('venser\'s journal', 'normal').

% Found in: TSP
card_name('venser\'s sliver', 'Venser\'s Sliver').
card_type('venser\'s sliver', 'Artifact Creature — Sliver').
card_types('venser\'s sliver', ['Artifact', 'Creature']).
card_subtypes('venser\'s sliver', ['Sliver']).
card_colors('venser\'s sliver', []).
card_text('venser\'s sliver', '').
card_mana_cost('venser\'s sliver', ['5']).
card_cmc('venser\'s sliver', 5).
card_layout('venser\'s sliver', 'normal').
card_power('venser\'s sliver', 3).
card_toughness('venser\'s sliver', 3).

% Found in: FUT, V13
card_name('venser, shaper savant', 'Venser, Shaper Savant').
card_type('venser, shaper savant', 'Legendary Creature — Human Wizard').
card_types('venser, shaper savant', ['Creature']).
card_subtypes('venser, shaper savant', ['Human', 'Wizard']).
card_supertypes('venser, shaper savant', ['Legendary']).
card_colors('venser, shaper savant', ['U']).
card_text('venser, shaper savant', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Venser, Shaper Savant enters the battlefield, return target spell or permanent to its owner\'s hand.').
card_mana_cost('venser, shaper savant', ['2', 'U', 'U']).
card_cmc('venser, shaper savant', 4).
card_layout('venser, shaper savant', 'normal').
card_power('venser, shaper savant', 2).
card_toughness('venser, shaper savant', 2).

% Found in: DDI, SOM
card_name('venser, the sojourner', 'Venser, the Sojourner').
card_type('venser, the sojourner', 'Planeswalker — Venser').
card_types('venser, the sojourner', ['Planeswalker']).
card_subtypes('venser, the sojourner', ['Venser']).
card_colors('venser, the sojourner', ['W', 'U']).
card_text('venser, the sojourner', '+2: Exile target permanent you own. Return it to the battlefield under your control at the beginning of the next end step.\n−1: Creatures can\'t be blocked this turn.\n−8: You get an emblem with \"Whenever you cast a spell, exile target permanent.\"').
card_mana_cost('venser, the sojourner', ['3', 'W', 'U']).
card_cmc('venser, the sojourner', 5).
card_layout('venser, the sojourner', 'normal').
card_loyalty('venser, the sojourner', 3).

% Found in: CNS, ROE
card_name('vent sentinel', 'Vent Sentinel').
card_type('vent sentinel', 'Creature — Elemental').
card_types('vent sentinel', ['Creature']).
card_subtypes('vent sentinel', ['Elemental']).
card_colors('vent sentinel', ['R']).
card_text('vent sentinel', 'Defender\n{1}{R}, {T}: Vent Sentinel deals damage to target player equal to the number of creatures with defender you control.').
card_mana_cost('vent sentinel', ['3', 'R']).
card_cmc('vent sentinel', 4).
card_layout('vent sentinel', 'normal').
card_power('vent sentinel', 2).
card_toughness('vent sentinel', 4).

% Found in: MIR
card_name('ventifact bottle', 'Ventifact Bottle').
card_type('ventifact bottle', 'Artifact').
card_types('ventifact bottle', ['Artifact']).
card_subtypes('ventifact bottle', []).
card_colors('ventifact bottle', []).
card_text('ventifact bottle', '{X}{1}, {T}: Put X charge counters on Ventifact Bottle. Activate this ability only any time you could cast a sorcery.\nAt the beginning of your precombat main phase, if Ventifact Bottle has a charge counter on it, tap it and remove all charge counters from it. Add {1} to your mana pool for each charge counter removed this way.').
card_mana_cost('ventifact bottle', ['3']).
card_cmc('ventifact bottle', 3).
card_layout('ventifact bottle', 'normal').
card_reserved('ventifact bottle').

% Found in: EXP, ZEN
card_name('verdant catacombs', 'Verdant Catacombs').
card_type('verdant catacombs', 'Land').
card_types('verdant catacombs', ['Land']).
card_subtypes('verdant catacombs', []).
card_colors('verdant catacombs', []).
card_text('verdant catacombs', '{T}, Pay 1 life, Sacrifice Verdant Catacombs: Search your library for a Swamp or Forest card and put it onto the battlefield. Then shuffle your library.').
card_layout('verdant catacombs', 'normal').

% Found in: DIS
card_name('verdant eidolon', 'Verdant Eidolon').
card_type('verdant eidolon', 'Creature — Spirit').
card_types('verdant eidolon', ['Creature']).
card_subtypes('verdant eidolon', ['Spirit']).
card_colors('verdant eidolon', ['G']).
card_text('verdant eidolon', '{G}, Sacrifice Verdant Eidolon: Add three mana of any one color to your mana pool.\nWhenever you cast a multicolored spell, you may return Verdant Eidolon from your graveyard to your hand.').
card_mana_cost('verdant eidolon', ['3', 'G']).
card_cmc('verdant eidolon', 4).
card_layout('verdant eidolon', 'normal').
card_power('verdant eidolon', 2).
card_toughness('verdant eidolon', 2).

% Found in: TSP
card_name('verdant embrace', 'Verdant Embrace').
card_type('verdant embrace', 'Enchantment — Aura').
card_types('verdant embrace', ['Enchantment']).
card_subtypes('verdant embrace', ['Aura']).
card_colors('verdant embrace', ['G']).
card_text('verdant embrace', 'Enchant creature\nEnchanted creature gets +3/+3 and has \"At the beginning of each upkeep, put a 1/1 green Saproling creature token onto the battlefield.\"').
card_mana_cost('verdant embrace', ['3', 'G', 'G']).
card_cmc('verdant embrace', 5).
card_layout('verdant embrace', 'normal').

% Found in: PCY
card_name('verdant field', 'Verdant Field').
card_type('verdant field', 'Enchantment — Aura').
card_types('verdant field', ['Enchantment']).
card_subtypes('verdant field', ['Aura']).
card_colors('verdant field', ['G']).
card_text('verdant field', 'Enchant land\nEnchanted land has \"{T}: Target creature gets +1/+1 until end of turn.\"').
card_mana_cost('verdant field', ['2', 'G']).
card_cmc('verdant field', 3).
card_layout('verdant field', 'normal').

% Found in: 10E, 9ED, DPA, HOP, PD3, TMP, TPR
card_name('verdant force', 'Verdant Force').
card_type('verdant force', 'Creature — Elemental').
card_types('verdant force', ['Creature']).
card_subtypes('verdant force', ['Elemental']).
card_colors('verdant force', ['G']).
card_text('verdant force', 'At the beginning of each upkeep, put a 1/1 green Saproling creature token onto the battlefield.').
card_mana_cost('verdant force', ['5', 'G', 'G', 'G']).
card_cmc('verdant force', 8).
card_layout('verdant force', 'normal').
card_power('verdant force', 7).
card_toughness('verdant force', 7).

% Found in: GTC, M14, M15
card_name('verdant haven', 'Verdant Haven').
card_type('verdant haven', 'Enchantment — Aura').
card_types('verdant haven', ['Enchantment']).
card_subtypes('verdant haven', ['Aura']).
card_colors('verdant haven', ['G']).
card_text('verdant haven', 'Enchant land\nWhen Verdant Haven enters the battlefield, you gain 2 life.\nWhenever enchanted land is tapped for mana, its controller adds one mana of any color to his or her mana pool (in addition to the mana the land produces).').
card_mana_cost('verdant haven', ['2', 'G']).
card_cmc('verdant haven', 3).
card_layout('verdant haven', 'normal').

% Found in: ODY
card_name('verdant succession', 'Verdant Succession').
card_type('verdant succession', 'Enchantment').
card_types('verdant succession', ['Enchantment']).
card_subtypes('verdant succession', []).
card_colors('verdant succession', ['G']).
card_text('verdant succession', 'Whenever a green nontoken creature dies, that creature\'s controller may search his or her library for a card with the same name as that creature and put it onto the battlefield. If that player does, he or she shuffles his or her library.').
card_mana_cost('verdant succession', ['4', 'G']).
card_cmc('verdant succession', 5).
card_layout('verdant succession', 'normal').

% Found in: STH, TPR
card_name('verdant touch', 'Verdant Touch').
card_type('verdant touch', 'Sorcery').
card_types('verdant touch', ['Sorcery']).
card_subtypes('verdant touch', []).
card_colors('verdant touch', ['G']).
card_text('verdant touch', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nTarget land becomes a 2/2 creature that\'s still a land. (This effect lasts indefinitely.)').
card_mana_cost('verdant touch', ['1', 'G']).
card_cmc('verdant touch', 2).
card_layout('verdant touch', 'normal').

% Found in: ARC, INV, MMA, TSB
card_name('verdeloth the ancient', 'Verdeloth the Ancient').
card_type('verdeloth the ancient', 'Legendary Creature — Treefolk').
card_types('verdeloth the ancient', ['Creature']).
card_subtypes('verdeloth the ancient', ['Treefolk']).
card_supertypes('verdeloth the ancient', ['Legendary']).
card_colors('verdeloth the ancient', ['G']).
card_text('verdeloth the ancient', 'Kicker {X} (You may pay an additional {X} as you cast this spell.)\nSaproling creatures and other Treefolk creatures get +1/+1.\nWhen Verdeloth the Ancient enters the battlefield, if it was kicked, put X 1/1 green Saproling creature tokens onto the battlefield.').
card_mana_cost('verdeloth the ancient', ['4', 'G', 'G']).
card_cmc('verdeloth the ancient', 6).
card_layout('verdeloth the ancient', 'normal').
card_power('verdeloth the ancient', 4).
card_toughness('verdeloth the ancient', 7).

% Found in: TMP, TPR
card_name('verdigris', 'Verdigris').
card_type('verdigris', 'Instant').
card_types('verdigris', ['Instant']).
card_subtypes('verdigris', []).
card_colors('verdigris', ['G']).
card_text('verdigris', 'Destroy target artifact.').
card_mana_cost('verdigris', ['2', 'G']).
card_cmc('verdigris', 3).
card_layout('verdigris', 'normal').

% Found in: DDE, INV
card_name('verduran emissary', 'Verduran Emissary').
card_type('verduran emissary', 'Creature — Human Wizard').
card_types('verduran emissary', ['Creature']).
card_subtypes('verduran emissary', ['Human', 'Wizard']).
card_colors('verduran emissary', ['G']).
card_text('verduran emissary', 'Kicker {1}{R} (You may pay an additional {1}{R} as you cast this spell.)\nWhen Verduran Emissary enters the battlefield, if it was kicked, destroy target artifact. It can\'t be regenerated.').
card_mana_cost('verduran emissary', ['2', 'G']).
card_cmc('verduran emissary', 3).
card_layout('verduran emissary', 'normal').
card_power('verduran emissary', 2).
card_toughness('verduran emissary', 3).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, CED, CEI, LEA, LEB
card_name('verduran enchantress', 'Verduran Enchantress').
card_type('verduran enchantress', 'Creature — Human Druid').
card_types('verduran enchantress', ['Creature']).
card_subtypes('verduran enchantress', ['Human', 'Druid']).
card_colors('verduran enchantress', ['G']).
card_text('verduran enchantress', 'Whenever you cast an enchantment spell, you may draw a card.').
card_mana_cost('verduran enchantress', ['1', 'G', 'G']).
card_cmc('verduran enchantress', 3).
card_layout('verduran enchantress', 'normal').
card_power('verduran enchantress', 0).
card_toughness('verduran enchantress', 2).

% Found in: MRD
card_name('vermiculos', 'Vermiculos').
card_type('vermiculos', 'Creature — Horror').
card_types('vermiculos', ['Creature']).
card_subtypes('vermiculos', ['Horror']).
card_colors('vermiculos', ['B']).
card_text('vermiculos', 'Whenever an artifact enters the battlefield, Vermiculos gets +4/+4 until end of turn.').
card_mana_cost('vermiculos', ['4', 'B']).
card_cmc('vermiculos', 5).
card_layout('vermiculos', 'normal').
card_power('vermiculos', 1).
card_toughness('vermiculos', 1).

% Found in: 7ED, 8ED, USG
card_name('vernal bloom', 'Vernal Bloom').
card_type('vernal bloom', 'Enchantment').
card_types('vernal bloom', ['Enchantment']).
card_subtypes('vernal bloom', []).
card_colors('vernal bloom', ['G']).
card_text('vernal bloom', 'Whenever a Forest is tapped for mana, its controller adds {G} to his or her mana pool (in addition to the mana the land produces).').
card_mana_cost('vernal bloom', ['3', 'G']).
card_cmc('vernal bloom', 4).
card_layout('vernal bloom', 'normal').

% Found in: MMQ
card_name('vernal equinox', 'Vernal Equinox').
card_type('vernal equinox', 'Enchantment').
card_types('vernal equinox', ['Enchantment']).
card_subtypes('vernal equinox', []).
card_colors('vernal equinox', ['G']).
card_text('vernal equinox', 'Any player may cast creature and enchantment cards as though they had flash.').
card_mana_cost('vernal equinox', ['3', 'G']).
card_cmc('vernal equinox', 4).
card_layout('vernal equinox', 'normal').

% Found in: 6ED, ICE
card_name('vertigo', 'Vertigo').
card_type('vertigo', 'Instant').
card_types('vertigo', ['Instant']).
card_subtypes('vertigo', []).
card_colors('vertigo', ['R']).
card_text('vertigo', 'Vertigo deals 2 damage to target creature with flying. That creature loses flying until end of turn.').
card_mana_cost('vertigo', ['R']).
card_cmc('vertigo', 1).
card_layout('vertigo', 'normal').

% Found in: GPT
card_name('vertigo spawn', 'Vertigo Spawn').
card_type('vertigo spawn', 'Creature — Illusion').
card_types('vertigo spawn', ['Creature']).
card_subtypes('vertigo spawn', ['Illusion']).
card_colors('vertigo spawn', ['U']).
card_text('vertigo spawn', 'Defender (This creature can\'t attack.)\nWhenever Vertigo Spawn blocks a creature, tap that creature. That creature doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('vertigo spawn', ['1', 'U']).
card_cmc('vertigo spawn', 2).
card_layout('vertigo spawn', 'normal').
card_power('vertigo spawn', 0).
card_toughness('vertigo spawn', 3).

% Found in: DIS
card_name('vesper ghoul', 'Vesper Ghoul').
card_type('vesper ghoul', 'Creature — Zombie Druid').
card_types('vesper ghoul', ['Creature']).
card_subtypes('vesper ghoul', ['Zombie', 'Druid']).
card_colors('vesper ghoul', ['B']).
card_text('vesper ghoul', '{T}, Pay 1 life: Add one mana of any color to your mana pool.').
card_mana_cost('vesper ghoul', ['2', 'B']).
card_cmc('vesper ghoul', 3).
card_layout('vesper ghoul', 'normal').
card_power('vesper ghoul', 1).
card_toughness('vesper ghoul', 1).

% Found in: AVR
card_name('vessel of endless rest', 'Vessel of Endless Rest').
card_type('vessel of endless rest', 'Artifact').
card_types('vessel of endless rest', ['Artifact']).
card_subtypes('vessel of endless rest', []).
card_colors('vessel of endless rest', []).
card_text('vessel of endless rest', 'When Vessel of Endless Rest enters the battlefield, put target card from a graveyard on the bottom of its owner\'s library.\n{T}: Add one mana of any color to your mana pool.').
card_mana_cost('vessel of endless rest', ['3']).
card_cmc('vessel of endless rest', 3).
card_layout('vessel of endless rest', 'normal').

% Found in: BFZ
card_name('vestige of emrakul', 'Vestige of Emrakul').
card_type('vestige of emrakul', 'Creature — Eldrazi Drone').
card_types('vestige of emrakul', ['Creature']).
card_subtypes('vestige of emrakul', ['Eldrazi', 'Drone']).
card_colors('vestige of emrakul', []).
card_text('vestige of emrakul', 'Devoid (This card has no color.)\nTrample').
card_mana_cost('vestige of emrakul', ['3', 'R']).
card_cmc('vestige of emrakul', 4).
card_layout('vestige of emrakul', 'normal').
card_power('vestige of emrakul', 3).
card_toughness('vestige of emrakul', 4).

% Found in: TSP, V12
card_name('vesuva', 'Vesuva').
card_type('vesuva', 'Land').
card_types('vesuva', ['Land']).
card_subtypes('vesuva', []).
card_colors('vesuva', []).
card_text('vesuva', 'You may have Vesuva enter the battlefield tapped as a copy of any land on the battlefield.').
card_layout('vesuva', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, MED
card_name('vesuvan doppelganger', 'Vesuvan Doppelganger').
card_type('vesuvan doppelganger', 'Creature — Shapeshifter').
card_types('vesuvan doppelganger', ['Creature']).
card_subtypes('vesuvan doppelganger', ['Shapeshifter']).
card_colors('vesuvan doppelganger', ['U']).
card_text('vesuvan doppelganger', 'You may have Vesuvan Doppelganger enter the battlefield as a copy of any creature on the battlefield except it doesn\'t copy that creature\'s color and it gains \"At the beginning of your upkeep, you may have this creature become a copy of target creature except it doesn\'t copy that creature\'s color. If you do, this creature gains this ability.\"').
card_mana_cost('vesuvan doppelganger', ['3', 'U', 'U']).
card_cmc('vesuvan doppelganger', 5).
card_layout('vesuvan doppelganger', 'normal').
card_power('vesuvan doppelganger', 0).
card_toughness('vesuvan doppelganger', 0).
card_reserved('vesuvan doppelganger').

% Found in: TSP
card_name('vesuvan shapeshifter', 'Vesuvan Shapeshifter').
card_type('vesuvan shapeshifter', 'Creature — Shapeshifter').
card_types('vesuvan shapeshifter', ['Creature']).
card_subtypes('vesuvan shapeshifter', ['Shapeshifter']).
card_colors('vesuvan shapeshifter', ['U']).
card_text('vesuvan shapeshifter', 'As Vesuvan Shapeshifter enters the battlefield or is turned face up, you may choose another creature on the battlefield. If you do, until Vesuvan Shapeshifter is turned face down, it becomes a copy of that creature and gains \"At the beginning of your upkeep, you may turn this creature face down.\"\nMorph {1}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('vesuvan shapeshifter', ['3', 'U', 'U']).
card_cmc('vesuvan shapeshifter', 5).
card_layout('vesuvan shapeshifter', 'normal').
card_power('vesuvan shapeshifter', 0).
card_toughness('vesuvan shapeshifter', 0).

% Found in: MMA, RAV
card_name('veteran armorer', 'Veteran Armorer').
card_type('veteran armorer', 'Creature — Human Soldier').
card_types('veteran armorer', ['Creature']).
card_subtypes('veteran armorer', ['Human', 'Soldier']).
card_colors('veteran armorer', ['W']).
card_text('veteran armorer', 'Other creatures you control get +0/+1.').
card_mana_cost('veteran armorer', ['1', 'W']).
card_cmc('veteran armorer', 2).
card_layout('veteran armorer', 'normal').
card_power('veteran armorer', 2).
card_toughness('veteran armorer', 2).

% Found in: DDO, M10
card_name('veteran armorsmith', 'Veteran Armorsmith').
card_type('veteran armorsmith', 'Creature — Human Soldier').
card_types('veteran armorsmith', ['Creature']).
card_subtypes('veteran armorsmith', ['Human', 'Soldier']).
card_colors('veteran armorsmith', ['W']).
card_text('veteran armorsmith', 'Other Soldier creatures you control get +0/+1.').
card_mana_cost('veteran armorsmith', ['W', 'W']).
card_cmc('veteran armorsmith', 2).
card_layout('veteran armorsmith', 'normal').
card_power('veteran armorsmith', 2).
card_toughness('veteran armorsmith', 3).

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME4
card_name('veteran bodyguard', 'Veteran Bodyguard').
card_type('veteran bodyguard', 'Creature — Human').
card_types('veteran bodyguard', ['Creature']).
card_subtypes('veteran bodyguard', ['Human']).
card_colors('veteran bodyguard', ['W']).
card_text('veteran bodyguard', 'As long as Veteran Bodyguard is untapped, all damage that would be dealt to you by unblocked creatures is dealt to Veteran Bodyguard instead.').
card_mana_cost('veteran bodyguard', ['3', 'W', 'W']).
card_cmc('veteran bodyguard', 5).
card_layout('veteran bodyguard', 'normal').
card_power('veteran bodyguard', 2).
card_toughness('veteran bodyguard', 5).
card_reserved('veteran bodyguard').

% Found in: PCY
card_name('veteran brawlers', 'Veteran Brawlers').
card_type('veteran brawlers', 'Creature — Human Soldier').
card_types('veteran brawlers', ['Creature']).
card_subtypes('veteran brawlers', ['Human', 'Soldier']).
card_colors('veteran brawlers', ['R']).
card_text('veteran brawlers', 'Veteran Brawlers can\'t attack if defending player controls an untapped land.\nVeteran Brawlers can\'t block if you control an untapped land.').
card_mana_cost('veteran brawlers', ['1', 'R']).
card_cmc('veteran brawlers', 2).
card_layout('veteran brawlers', 'normal').
card_power('veteran brawlers', 4).
card_toughness('veteran brawlers', 4).

% Found in: 9ED, S99
card_name('veteran cavalier', 'Veteran Cavalier').
card_type('veteran cavalier', 'Creature — Human Knight').
card_types('veteran cavalier', ['Creature']).
card_subtypes('veteran cavalier', ['Human', 'Knight']).
card_colors('veteran cavalier', ['W']).
card_text('veteran cavalier', 'Vigilance (Attacking doesn\'t cause this creature to tap.)').
card_mana_cost('veteran cavalier', ['W', 'W']).
card_cmc('veteran cavalier', 2).
card_layout('veteran cavalier', 'normal').
card_power('veteran cavalier', 2).
card_toughness('veteran cavalier', 2).

% Found in: CMD, WTH
card_name('veteran explorer', 'Veteran Explorer').
card_type('veteran explorer', 'Creature — Human Soldier Scout').
card_types('veteran explorer', ['Creature']).
card_subtypes('veteran explorer', ['Human', 'Soldier', 'Scout']).
card_colors('veteran explorer', ['G']).
card_text('veteran explorer', 'When Veteran Explorer dies, each player may search his or her library for up to two basic land cards and put them onto the battlefield. Then each player who searched his or her library this way shuffles it.').
card_mana_cost('veteran explorer', ['G']).
card_cmc('veteran explorer', 1).
card_layout('veteran explorer', 'normal').
card_power('veteran explorer', 1).
card_toughness('veteran explorer', 1).

% Found in: LRW
card_name('veteran of the depths', 'Veteran of the Depths').
card_type('veteran of the depths', 'Creature — Merfolk Soldier').
card_types('veteran of the depths', ['Creature']).
card_subtypes('veteran of the depths', ['Merfolk', 'Soldier']).
card_colors('veteran of the depths', ['W']).
card_text('veteran of the depths', 'Whenever Veteran of the Depths becomes tapped, you may put a +1/+1 counter on it.').
card_mana_cost('veteran of the depths', ['3', 'W']).
card_cmc('veteran of the depths', 4).
card_layout('veteran of the depths', 'normal').
card_power('veteran of the depths', 2).
card_toughness('veteran of the depths', 2).

% Found in: DDO, M10
card_name('veteran swordsmith', 'Veteran Swordsmith').
card_type('veteran swordsmith', 'Creature — Human Soldier').
card_types('veteran swordsmith', ['Creature']).
card_subtypes('veteran swordsmith', ['Human', 'Soldier']).
card_colors('veteran swordsmith', ['W']).
card_text('veteran swordsmith', 'Other Soldier creatures you control get +1/+0.').
card_mana_cost('veteran swordsmith', ['2', 'W']).
card_cmc('veteran swordsmith', 3).
card_layout('veteran swordsmith', 'normal').
card_power('veteran swordsmith', 3).
card_toughness('veteran swordsmith', 2).

% Found in: BFZ, DDP
card_name('veteran warleader', 'Veteran Warleader').
card_type('veteran warleader', 'Creature — Human Soldier Ally').
card_types('veteran warleader', ['Creature']).
card_subtypes('veteran warleader', ['Human', 'Soldier', 'Ally']).
card_colors('veteran warleader', ['W', 'G']).
card_text('veteran warleader', 'Veteran Warleader\'s power and toughness are each equal to the number of creatures you control.\nTap another untapped Ally you control: Veteran Warleader gains your choice of first strike, vigilance, or trample until end of turn.').
card_mana_cost('veteran warleader', ['1', 'G', 'W']).
card_cmc('veteran warleader', 3).
card_layout('veteran warleader', 'normal').
card_power('veteran warleader', '*').
card_toughness('veteran warleader', '*').

% Found in: MOR
card_name('veteran\'s armaments', 'Veteran\'s Armaments').
card_type('veteran\'s armaments', 'Tribal Artifact — Soldier Equipment').
card_types('veteran\'s armaments', ['Tribal', 'Artifact']).
card_subtypes('veteran\'s armaments', ['Soldier', 'Equipment']).
card_colors('veteran\'s armaments', []).
card_text('veteran\'s armaments', 'Equipped creature has \"Whenever this creature attacks or blocks, it gets +1/+1 until end of turn for each attacking creature.\"\nWhenever a Soldier creature enters the battlefield, you may attach Veteran\'s Armaments to it.\nEquip {2}').
card_mana_cost('veteran\'s armaments', ['2']).
card_cmc('veteran\'s armaments', 2).
card_layout('veteran\'s armaments', 'normal').

% Found in: WWK
card_name('veteran\'s reflexes', 'Veteran\'s Reflexes').
card_type('veteran\'s reflexes', 'Instant').
card_types('veteran\'s reflexes', ['Instant']).
card_subtypes('veteran\'s reflexes', []).
card_colors('veteran\'s reflexes', ['W']).
card_text('veteran\'s reflexes', 'Target creature gets +1/+1 until end of turn. Untap that creature.').
card_mana_cost('veteran\'s reflexes', ['W']).
card_cmc('veteran\'s reflexes', 1).
card_layout('veteran\'s reflexes', 'normal').

% Found in: ORI
card_name('veteran\'s sidearm', 'Veteran\'s Sidearm').
card_type('veteran\'s sidearm', 'Artifact — Equipment').
card_types('veteran\'s sidearm', ['Artifact']).
card_subtypes('veteran\'s sidearm', ['Equipment']).
card_colors('veteran\'s sidearm', []).
card_text('veteran\'s sidearm', 'Equipped creature gets +1/+1.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('veteran\'s sidearm', ['2']).
card_cmc('veteran\'s sidearm', 2).
card_layout('veteran\'s sidearm', 'normal').

% Found in: ALL
card_name('veteran\'s voice', 'Veteran\'s Voice').
card_type('veteran\'s voice', 'Enchantment — Aura').
card_types('veteran\'s voice', ['Enchantment']).
card_subtypes('veteran\'s voice', ['Aura']).
card_colors('veteran\'s voice', ['R']).
card_text('veteran\'s voice', 'Enchant creature you control\nTap enchanted creature: Target creature other than the creature tapped this way gets +2/+1 until end of turn. Activate this ability only if enchanted creature is untapped.').
card_mana_cost('veteran\'s voice', ['R']).
card_cmc('veteran\'s voice', 1).
card_layout('veteran\'s voice', 'normal').

% Found in: DST
card_name('vex', 'Vex').
card_type('vex', 'Instant').
card_types('vex', ['Instant']).
card_subtypes('vex', []).
card_colors('vex', ['U']).
card_text('vex', 'Counter target spell. That spell\'s controller may draw a card.').
card_mana_cost('vex', ['2', 'U']).
card_cmc('vex', 3).
card_layout('vex', 'normal').

% Found in: 8ED, ICE
card_name('vexing arcanix', 'Vexing Arcanix').
card_type('vexing arcanix', 'Artifact').
card_types('vexing arcanix', ['Artifact']).
card_subtypes('vexing arcanix', []).
card_colors('vexing arcanix', []).
card_text('vexing arcanix', '{3}, {T}: Target player names a card, then reveals the top card of his or her library. If it\'s the named card, the player puts it into his or her hand. Otherwise, the player puts it into his or her graveyard and Vexing Arcanix deals 2 damage to him or her.').
card_mana_cost('vexing arcanix', ['4']).
card_cmc('vexing arcanix', 4).
card_layout('vexing arcanix', 'normal').

% Found in: LGN
card_name('vexing beetle', 'Vexing Beetle').
card_type('vexing beetle', 'Creature — Insect').
card_types('vexing beetle', ['Creature']).
card_subtypes('vexing beetle', ['Insect']).
card_colors('vexing beetle', ['G']).
card_text('vexing beetle', 'Vexing Beetle can\'t be countered.\nVexing Beetle gets +3/+3 as long as no opponent controls a creature.').
card_mana_cost('vexing beetle', ['4', 'G']).
card_cmc('vexing beetle', 5).
card_layout('vexing beetle', 'normal').
card_power('vexing beetle', 3).
card_toughness('vexing beetle', 3).

% Found in: AVR
card_name('vexing devil', 'Vexing Devil').
card_type('vexing devil', 'Creature — Devil').
card_types('vexing devil', ['Creature']).
card_subtypes('vexing devil', ['Devil']).
card_colors('vexing devil', ['R']).
card_text('vexing devil', 'When Vexing Devil enters the battlefield, any opponent may have it deal 4 damage to him or her. If a player does, sacrifice Vexing Devil.').
card_mana_cost('vexing devil', ['R']).
card_cmc('vexing devil', 1).
card_layout('vexing devil', 'normal').
card_power('vexing devil', 4).
card_toughness('vexing devil', 3).

% Found in: SHM, pLPA
card_name('vexing shusher', 'Vexing Shusher').
card_type('vexing shusher', 'Creature — Goblin Shaman').
card_types('vexing shusher', ['Creature']).
card_subtypes('vexing shusher', ['Goblin', 'Shaman']).
card_colors('vexing shusher', ['R', 'G']).
card_text('vexing shusher', 'Vexing Shusher can\'t be countered.\n{R/G}: Target spell can\'t be countered by spells or abilities.').
card_mana_cost('vexing shusher', ['R/G', 'R/G']).
card_cmc('vexing shusher', 2).
card_layout('vexing shusher', 'normal').
card_power('vexing shusher', 2).
card_toughness('vexing shusher', 2).

% Found in: CSP
card_name('vexing sphinx', 'Vexing Sphinx').
card_type('vexing sphinx', 'Creature — Sphinx').
card_types('vexing sphinx', ['Creature']).
card_subtypes('vexing sphinx', ['Sphinx']).
card_colors('vexing sphinx', ['U']).
card_text('vexing sphinx', 'Flying\nCumulative upkeep—Discard a card. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhen Vexing Sphinx dies, draw a card for each age counter on it.').
card_mana_cost('vexing sphinx', ['1', 'U', 'U']).
card_cmc('vexing sphinx', 3).
card_layout('vexing sphinx', 'normal').
card_power('vexing sphinx', 4).
card_toughness('vexing sphinx', 4).

% Found in: TMP, TPR, TSB
card_name('vhati il-dal', 'Vhati il-Dal').
card_type('vhati il-dal', 'Legendary Creature — Human Warrior').
card_types('vhati il-dal', ['Creature']).
card_subtypes('vhati il-dal', ['Human', 'Warrior']).
card_supertypes('vhati il-dal', ['Legendary']).
card_colors('vhati il-dal', ['B', 'G']).
card_text('vhati il-dal', '{T}: Until end of turn, target creature has base power 1 or base toughness 1.').
card_mana_cost('vhati il-dal', ['2', 'B', 'G']).
card_cmc('vhati il-dal', 4).
card_layout('vhati il-dal', 'normal').
card_power('vhati il-dal', 3).
card_toughness('vhati il-dal', 3).

% Found in: DTK
card_name('vial of dragonfire', 'Vial of Dragonfire').
card_type('vial of dragonfire', 'Artifact').
card_types('vial of dragonfire', ['Artifact']).
card_subtypes('vial of dragonfire', []).
card_colors('vial of dragonfire', []).
card_text('vial of dragonfire', '{2}, {T}, Sacrifice Vial of Dragonfire: Vial of Dragonfire deals 2 damage to target creature.').
card_mana_cost('vial of dragonfire', ['2']).
card_cmc('vial of dragonfire', 2).
card_layout('vial of dragonfire', 'normal').

% Found in: M14
card_name('vial of poison', 'Vial of Poison').
card_type('vial of poison', 'Artifact').
card_types('vial of poison', ['Artifact']).
card_subtypes('vial of poison', []).
card_colors('vial of poison', []).
card_text('vial of poison', '{1}, Sacrifice Vial of Poison: Target creature gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_mana_cost('vial of poison', ['1']).
card_cmc('vial of poison', 1).
card_layout('vial of poison', 'normal').

% Found in: ULG
card_name('viashino bey', 'Viashino Bey').
card_type('viashino bey', 'Creature — Viashino').
card_types('viashino bey', ['Creature']).
card_subtypes('viashino bey', ['Viashino']).
card_colors('viashino bey', ['R']).
card_text('viashino bey', 'If Viashino Bey attacks, all creatures you control attack if able.').
card_mana_cost('viashino bey', ['2', 'R', 'R']).
card_cmc('viashino bey', 4).
card_layout('viashino bey', 'normal').
card_power('viashino bey', 4).
card_toughness('viashino bey', 3).

% Found in: TSP
card_name('viashino bladescout', 'Viashino Bladescout').
card_type('viashino bladescout', 'Creature — Viashino Scout').
card_types('viashino bladescout', ['Creature']).
card_subtypes('viashino bladescout', ['Viashino', 'Scout']).
card_colors('viashino bladescout', ['R']).
card_text('viashino bladescout', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Viashino Bladescout enters the battlefield, target creature gains first strike until end of turn.').
card_mana_cost('viashino bladescout', ['1', 'R', 'R']).
card_cmc('viashino bladescout', 3).
card_layout('viashino bladescout', 'normal').
card_power('viashino bladescout', 2).
card_toughness('viashino bladescout', 1).

% Found in: ULG
card_name('viashino cutthroat', 'Viashino Cutthroat').
card_type('viashino cutthroat', 'Creature — Viashino').
card_types('viashino cutthroat', ['Creature']).
card_subtypes('viashino cutthroat', ['Viashino']).
card_colors('viashino cutthroat', ['R']).
card_text('viashino cutthroat', 'Haste\nAt the beginning of the end step, return Viashino Cutthroat to its owner\'s hand.').
card_mana_cost('viashino cutthroat', ['2', 'R', 'R']).
card_cmc('viashino cutthroat', 4).
card_layout('viashino cutthroat', 'normal').
card_power('viashino cutthroat', 5).
card_toughness('viashino cutthroat', 3).

% Found in: RAV
card_name('viashino fangtail', 'Viashino Fangtail').
card_type('viashino fangtail', 'Creature — Viashino Warrior').
card_types('viashino fangtail', ['Creature']).
card_subtypes('viashino fangtail', ['Viashino', 'Warrior']).
card_colors('viashino fangtail', ['R']).
card_text('viashino fangtail', '{T}: Viashino Fangtail deals 1 damage to target creature or player.').
card_mana_cost('viashino fangtail', ['2', 'R', 'R']).
card_cmc('viashino fangtail', 4).
card_layout('viashino fangtail', 'normal').
card_power('viashino fangtail', 3).
card_toughness('viashino fangtail', 3).

% Found in: DGM
card_name('viashino firstblade', 'Viashino Firstblade').
card_type('viashino firstblade', 'Creature — Viashino Soldier').
card_types('viashino firstblade', ['Creature']).
card_subtypes('viashino firstblade', ['Viashino', 'Soldier']).
card_colors('viashino firstblade', ['W', 'R']).
card_text('viashino firstblade', 'Haste\nWhen Viashino Firstblade enters the battlefield, it gets +2/+2 until end of turn.').
card_mana_cost('viashino firstblade', ['1', 'R', 'W']).
card_cmc('viashino firstblade', 3).
card_layout('viashino firstblade', 'normal').
card_power('viashino firstblade', 2).
card_toughness('viashino firstblade', 2).

% Found in: INV
card_name('viashino grappler', 'Viashino Grappler').
card_type('viashino grappler', 'Creature — Viashino').
card_types('viashino grappler', ['Creature']).
card_subtypes('viashino grappler', ['Viashino']).
card_colors('viashino grappler', ['R']).
card_text('viashino grappler', '{G}: Viashino Grappler gains trample until end of turn.').
card_mana_cost('viashino grappler', ['2', 'R']).
card_cmc('viashino grappler', 3).
card_layout('viashino grappler', 'normal').
card_power('viashino grappler', 3).
card_toughness('viashino grappler', 1).

% Found in: ULG
card_name('viashino heretic', 'Viashino Heretic').
card_type('viashino heretic', 'Creature — Viashino').
card_types('viashino heretic', ['Creature']).
card_subtypes('viashino heretic', ['Viashino']).
card_colors('viashino heretic', ['R']).
card_text('viashino heretic', '{1}{R}, {T}: Destroy target artifact. Viashino Heretic deals damage to that artifact\'s controller equal to the artifact\'s converted mana cost.').
card_mana_cost('viashino heretic', ['2', 'R']).
card_cmc('viashino heretic', 3).
card_layout('viashino heretic', 'normal').
card_power('viashino heretic', 1).
card_toughness('viashino heretic', 3).

% Found in: USG
card_name('viashino outrider', 'Viashino Outrider').
card_type('viashino outrider', 'Creature — Viashino').
card_types('viashino outrider', ['Creature']).
card_subtypes('viashino outrider', ['Viashino']).
card_colors('viashino outrider', ['R']).
card_text('viashino outrider', 'Echo {2}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_mana_cost('viashino outrider', ['2', 'R']).
card_cmc('viashino outrider', 3).
card_layout('viashino outrider', 'normal').
card_power('viashino outrider', 4).
card_toughness('viashino outrider', 3).

% Found in: RTR
card_name('viashino racketeer', 'Viashino Racketeer').
card_type('viashino racketeer', 'Creature — Viashino Rogue').
card_types('viashino racketeer', ['Creature']).
card_subtypes('viashino racketeer', ['Viashino', 'Rogue']).
card_colors('viashino racketeer', ['R']).
card_text('viashino racketeer', 'When Viashino Racketeer enters the battlefield, you may discard a card. If you do, draw a card.').
card_mana_cost('viashino racketeer', ['2', 'R']).
card_cmc('viashino racketeer', 3).
card_layout('viashino racketeer', 'normal').
card_power('viashino racketeer', 2).
card_toughness('viashino racketeer', 1).

% Found in: 10E, USG
card_name('viashino runner', 'Viashino Runner').
card_type('viashino runner', 'Creature — Viashino').
card_types('viashino runner', ['Creature']).
card_subtypes('viashino runner', ['Viashino']).
card_colors('viashino runner', ['R']).
card_text('viashino runner', 'Menace (This creature can\'t be blocked except by two or more creatures.)').
card_mana_cost('viashino runner', ['3', 'R']).
card_cmc('viashino runner', 4).
card_layout('viashino runner', 'normal').
card_power('viashino runner', 3).
card_toughness('viashino runner', 2).

% Found in: 10E, ULG
card_name('viashino sandscout', 'Viashino Sandscout').
card_type('viashino sandscout', 'Creature — Viashino Scout').
card_types('viashino sandscout', ['Creature']).
card_subtypes('viashino sandscout', ['Viashino', 'Scout']).
card_colors('viashino sandscout', ['R']).
card_text('viashino sandscout', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\nAt the beginning of the end step, return Viashino Sandscout to its owner\'s hand. (Return it only if it\'s on the battlefield.)').
card_mana_cost('viashino sandscout', ['1', 'R']).
card_cmc('viashino sandscout', 2).
card_layout('viashino sandscout', 'normal').
card_power('viashino sandscout', 2).
card_toughness('viashino sandscout', 1).

% Found in: 8ED, 9ED, MGB, VIS
card_name('viashino sandstalker', 'Viashino Sandstalker').
card_type('viashino sandstalker', 'Creature — Viashino Warrior').
card_types('viashino sandstalker', ['Creature']).
card_subtypes('viashino sandstalker', ['Viashino', 'Warrior']).
card_colors('viashino sandstalker', ['R']).
card_text('viashino sandstalker', 'Haste (This creature can attack the turn it comes under your control.)\nAt the beginning of the end step, return Viashino Sandstalker to its owner\'s hand. (Return it only if it\'s on the battlefield.)').
card_mana_cost('viashino sandstalker', ['1', 'R', 'R']).
card_cmc('viashino sandstalker', 3).
card_layout('viashino sandstalker', 'normal').
card_power('viashino sandstalker', 4).
card_toughness('viashino sandstalker', 2).

% Found in: USG
card_name('viashino sandswimmer', 'Viashino Sandswimmer').
card_type('viashino sandswimmer', 'Creature — Viashino').
card_types('viashino sandswimmer', ['Creature']).
card_subtypes('viashino sandswimmer', ['Viashino']).
card_colors('viashino sandswimmer', ['R']).
card_text('viashino sandswimmer', '{R}: Flip a coin. If you win the flip, return Viashino Sandswimmer to its owner\'s hand. If you lose the flip, sacrifice Viashino Sandswimmer.').
card_mana_cost('viashino sandswimmer', ['2', 'R', 'R']).
card_cmc('viashino sandswimmer', 4).
card_layout('viashino sandswimmer', 'normal').
card_power('viashino sandswimmer', 3).
card_toughness('viashino sandswimmer', 2).

% Found in: GTC
card_name('viashino shanktail', 'Viashino Shanktail').
card_type('viashino shanktail', 'Creature — Viashino Warrior').
card_types('viashino shanktail', ['Creature']).
card_subtypes('viashino shanktail', ['Viashino', 'Warrior']).
card_colors('viashino shanktail', ['R']).
card_text('viashino shanktail', 'First strike\nBloodrush — {2}{R}, Discard Viashino Shanktail: Target attacking creature gets +3/+1 and gains first strike until end of turn.').
card_mana_cost('viashino shanktail', ['3', 'R']).
card_cmc('viashino shanktail', 4).
card_layout('viashino shanktail', 'normal').
card_power('viashino shanktail', 3).
card_toughness('viashino shanktail', 1).

% Found in: ALA
card_name('viashino skeleton', 'Viashino Skeleton').
card_type('viashino skeleton', 'Creature — Viashino Skeleton').
card_types('viashino skeleton', ['Creature']).
card_subtypes('viashino skeleton', ['Viashino', 'Skeleton']).
card_colors('viashino skeleton', ['R']).
card_text('viashino skeleton', '{1}{B}, Discard a card: Regenerate Viashino Skeleton.').
card_mana_cost('viashino skeleton', ['3', 'R']).
card_cmc('viashino skeleton', 4).
card_layout('viashino skeleton', 'normal').
card_power('viashino skeleton', 2).
card_toughness('viashino skeleton', 1).

% Found in: RAV
card_name('viashino slasher', 'Viashino Slasher').
card_type('viashino slasher', 'Creature — Viashino Warrior').
card_types('viashino slasher', ['Creature']).
card_subtypes('viashino slasher', ['Viashino', 'Warrior']).
card_colors('viashino slasher', ['R']).
card_text('viashino slasher', '{R}: Viashino Slasher gets +1/-1 until end of turn.').
card_mana_cost('viashino slasher', ['1', 'R']).
card_cmc('viashino slasher', 2).
card_layout('viashino slasher', 'normal').
card_power('viashino slasher', 1).
card_toughness('viashino slasher', 2).

% Found in: CON, MM2
card_name('viashino slaughtermaster', 'Viashino Slaughtermaster').
card_type('viashino slaughtermaster', 'Creature — Viashino Warrior').
card_types('viashino slaughtermaster', ['Creature']).
card_subtypes('viashino slaughtermaster', ['Viashino', 'Warrior']).
card_colors('viashino slaughtermaster', ['R']).
card_text('viashino slaughtermaster', 'Double strike\n{B}{G}: Viashino Slaughtermaster gets +1/+1 until end of turn. Activate this ability only once each turn.').
card_mana_cost('viashino slaughtermaster', ['1', 'R']).
card_cmc('viashino slaughtermaster', 2).
card_layout('viashino slaughtermaster', 'normal').
card_power('viashino slaughtermaster', 1).
card_toughness('viashino slaughtermaster', 1).

% Found in: M10
card_name('viashino spearhunter', 'Viashino Spearhunter').
card_type('viashino spearhunter', 'Creature — Viashino Warrior').
card_types('viashino spearhunter', ['Creature']).
card_subtypes('viashino spearhunter', ['Viashino', 'Warrior']).
card_colors('viashino spearhunter', ['R']).
card_text('viashino spearhunter', 'First strike (This creature deals combat damage before creatures without first strike.)').
card_mana_cost('viashino spearhunter', ['2', 'R']).
card_cmc('viashino spearhunter', 3).
card_layout('viashino spearhunter', 'normal').
card_power('viashino spearhunter', 2).
card_toughness('viashino spearhunter', 1).

% Found in: 6ED, BTD, MIR
card_name('viashino warrior', 'Viashino Warrior').
card_type('viashino warrior', 'Creature — Viashino Warrior').
card_types('viashino warrior', ['Creature']).
card_subtypes('viashino warrior', ['Viashino', 'Warrior']).
card_colors('viashino warrior', ['R']).
card_text('viashino warrior', '').
card_mana_cost('viashino warrior', ['3', 'R']).
card_cmc('viashino warrior', 4).
card_layout('viashino warrior', 'normal').
card_power('viashino warrior', 4).
card_toughness('viashino warrior', 2).

% Found in: USG
card_name('viashino weaponsmith', 'Viashino Weaponsmith').
card_type('viashino weaponsmith', 'Creature — Viashino').
card_types('viashino weaponsmith', ['Creature']).
card_subtypes('viashino weaponsmith', ['Viashino']).
card_colors('viashino weaponsmith', ['R']).
card_text('viashino weaponsmith', 'Whenever Viashino Weaponsmith becomes blocked by a creature, Viashino Weaponsmith gets +2/+2 until end of turn.').
card_mana_cost('viashino weaponsmith', ['3', 'R']).
card_cmc('viashino weaponsmith', 4).
card_layout('viashino weaponsmith', 'normal').
card_power('viashino weaponsmith', 2).
card_toughness('viashino weaponsmith', 2).

% Found in: VIS
card_name('viashivan dragon', 'Viashivan Dragon').
card_type('viashivan dragon', 'Creature — Dragon').
card_types('viashivan dragon', ['Creature']).
card_subtypes('viashivan dragon', ['Dragon']).
card_colors('viashivan dragon', ['R', 'G']).
card_text('viashivan dragon', 'Flying\n{R}: Viashivan Dragon gets +1/+0 until end of turn.\n{G}: Viashivan Dragon gets +0/+1 until end of turn.').
card_mana_cost('viashivan dragon', ['2', 'R', 'R', 'G', 'G']).
card_cmc('viashivan dragon', 6).
card_layout('viashivan dragon', 'normal').
card_power('viashivan dragon', 4).
card_toughness('viashivan dragon', 4).
card_reserved('viashivan dragon').

% Found in: ICE, ME4
card_name('vibrating sphere', 'Vibrating Sphere').
card_type('vibrating sphere', 'Artifact').
card_types('vibrating sphere', ['Artifact']).
card_subtypes('vibrating sphere', []).
card_colors('vibrating sphere', []).
card_text('vibrating sphere', 'As long as it\'s your turn, creatures you control get +2/+0.\nAs long as it\'s not your turn, creatures you control get -0/-2.').
card_mana_cost('vibrating sphere', ['4']).
card_cmc('vibrating sphere', 4).
card_layout('vibrating sphere', 'normal').

% Found in: 5DN
card_name('vicious betrayal', 'Vicious Betrayal').
card_type('vicious betrayal', 'Sorcery').
card_types('vicious betrayal', ['Sorcery']).
card_subtypes('vicious betrayal', []).
card_colors('vicious betrayal', ['B']).
card_text('vicious betrayal', 'As an additional cost to cast Vicious Betrayal, sacrifice any number of creatures.\nTarget creature gets +2/+2 until end of turn for each creature sacrificed this way.').
card_mana_cost('vicious betrayal', ['3', 'B', 'B']).
card_cmc('vicious betrayal', 5).
card_layout('vicious betrayal', 'normal').

% Found in: 8ED, DD3_GVL, DDD, NMS
card_name('vicious hunger', 'Vicious Hunger').
card_type('vicious hunger', 'Sorcery').
card_types('vicious hunger', ['Sorcery']).
card_subtypes('vicious hunger', []).
card_colors('vicious hunger', ['B']).
card_text('vicious hunger', 'Vicious Hunger deals 2 damage to target creature and you gain 2 life.').
card_mana_cost('vicious hunger', ['B', 'B']).
card_cmc('vicious hunger', 2).
card_layout('vicious hunger', 'normal').

% Found in: INV
card_name('vicious kavu', 'Vicious Kavu').
card_type('vicious kavu', 'Creature — Kavu').
card_types('vicious kavu', ['Creature']).
card_subtypes('vicious kavu', ['Kavu']).
card_colors('vicious kavu', ['B', 'R']).
card_text('vicious kavu', 'Whenever Vicious Kavu attacks, it gets +2/+0 until end of turn.').
card_mana_cost('vicious kavu', ['1', 'B', 'R']).
card_cmc('vicious kavu', 3).
card_layout('vicious kavu', 'normal').
card_power('vicious kavu', 2).
card_toughness('vicious kavu', 2).

% Found in: ALA
card_name('vicious shadows', 'Vicious Shadows').
card_type('vicious shadows', 'Enchantment').
card_types('vicious shadows', ['Enchantment']).
card_subtypes('vicious shadows', []).
card_colors('vicious shadows', ['R']).
card_text('vicious shadows', 'Whenever a creature dies, you may have Vicious Shadows deal damage to target player equal to the number of cards in that player\'s hand.').
card_mana_cost('vicious shadows', ['6', 'R']).
card_cmc('vicious shadows', 7).
card_layout('vicious shadows', 'normal').

% Found in: ISD
card_name('victim of night', 'Victim of Night').
card_type('victim of night', 'Instant').
card_types('victim of night', ['Instant']).
card_subtypes('victim of night', []).
card_colors('victim of night', ['B']).
card_text('victim of night', 'Destroy target non-Vampire, non-Werewolf, non-Zombie creature.').
card_mana_cost('victim of night', ['B', 'B']).
card_cmc('victim of night', 2).
card_layout('victim of night', 'normal').

% Found in: C14, CNS, USG
card_name('victimize', 'Victimize').
card_type('victimize', 'Sorcery').
card_types('victimize', ['Sorcery']).
card_subtypes('victimize', []).
card_colors('victimize', ['B']).
card_text('victimize', 'Choose two target creature cards in your graveyard. Sacrifice a creature. If you do, return the chosen cards to the battlefield tapped.').
card_mana_cost('victimize', ['2', 'B']).
card_cmc('victimize', 3).
card_layout('victimize', 'normal').

% Found in: NPH
card_name('victorious destruction', 'Victorious Destruction').
card_type('victorious destruction', 'Sorcery').
card_types('victorious destruction', ['Sorcery']).
card_subtypes('victorious destruction', []).
card_colors('victorious destruction', ['R']).
card_text('victorious destruction', 'Destroy target artifact or land. Its controller loses 1 life.').
card_mana_cost('victorious destruction', ['4', 'R']).
card_cmc('victorious destruction', 5).
card_layout('victorious destruction', 'normal').

% Found in: MBS
card_name('victory\'s herald', 'Victory\'s Herald').
card_type('victory\'s herald', 'Creature — Angel').
card_types('victory\'s herald', ['Creature']).
card_subtypes('victory\'s herald', ['Angel']).
card_colors('victory\'s herald', ['W']).
card_text('victory\'s herald', 'Flying\nWhenever Victory\'s Herald attacks, attacking creatures gain flying and lifelink until end of turn.').
card_mana_cost('victory\'s herald', ['3', 'W', 'W', 'W']).
card_cmc('victory\'s herald', 6).
card_layout('victory\'s herald', 'normal').
card_power('victory\'s herald', 4).
card_toughness('victory\'s herald', 4).

% Found in: H09, STH, TPR
card_name('victual sliver', 'Victual Sliver').
card_type('victual sliver', 'Creature — Sliver').
card_types('victual sliver', ['Creature']).
card_subtypes('victual sliver', ['Sliver']).
card_colors('victual sliver', ['W', 'G']).
card_text('victual sliver', 'All Slivers have \"{2}, Sacrifice this permanent: You gain 4 life.\"').
card_mana_cost('victual sliver', ['G', 'W']).
card_cmc('victual sliver', 2).
card_layout('victual sliver', 'normal').
card_power('victual sliver', 2).
card_toughness('victual sliver', 2).

% Found in: CON
card_name('view from above', 'View from Above').
card_type('view from above', 'Instant').
card_types('view from above', ['Instant']).
card_subtypes('view from above', []).
card_colors('view from above', ['U']).
card_text('view from above', 'Target creature gains flying until end of turn. If you control a white permanent, return View from Above to its owner\'s hand.').
card_mana_cost('view from above', ['1', 'U']).
card_cmc('view from above', 2).
card_layout('view from above', 'normal').

% Found in: DIS, MM2
card_name('vigean graftmage', 'Vigean Graftmage').
card_type('vigean graftmage', 'Creature — Vedalken Wizard Mutant').
card_types('vigean graftmage', ['Creature']).
card_subtypes('vigean graftmage', ['Vedalken', 'Wizard', 'Mutant']).
card_colors('vigean graftmage', ['U']).
card_text('vigean graftmage', 'Graft 2 (This creature enters the battlefield with two +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\n{1}{U}: Untap target creature with a +1/+1 counter on it.').
card_mana_cost('vigean graftmage', ['2', 'U']).
card_cmc('vigean graftmage', 3).
card_layout('vigean graftmage', 'normal').
card_power('vigean graftmage', 0).
card_toughness('vigean graftmage', 0).

% Found in: DIS
card_name('vigean hydropon', 'Vigean Hydropon').
card_type('vigean hydropon', 'Creature — Plant Mutant').
card_types('vigean hydropon', ['Creature']).
card_subtypes('vigean hydropon', ['Plant', 'Mutant']).
card_colors('vigean hydropon', ['U', 'G']).
card_text('vigean hydropon', 'Graft 5 (This creature enters the battlefield with five +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\nVigean Hydropon can\'t attack or block.').
card_mana_cost('vigean hydropon', ['1', 'G', 'U']).
card_cmc('vigean hydropon', 3).
card_layout('vigean hydropon', 'normal').
card_power('vigean hydropon', 0).
card_toughness('vigean hydropon', 0).

% Found in: DIS
card_name('vigean intuition', 'Vigean Intuition').
card_type('vigean intuition', 'Instant').
card_types('vigean intuition', ['Instant']).
card_subtypes('vigean intuition', []).
card_colors('vigean intuition', ['U', 'G']).
card_text('vigean intuition', 'Choose a card type, then reveal the top four cards of your library. Put all cards of the chosen type revealed this way into your hand and the rest into your graveyard. (Artifact, creature, enchantment, instant, land, planeswalker, sorcery, and tribal are card types.)').
card_mana_cost('vigean intuition', ['3', 'G', 'U']).
card_cmc('vigean intuition', 5).
card_layout('vigean intuition', 'normal').

% Found in: SOM
card_name('vigil for the lost', 'Vigil for the Lost').
card_type('vigil for the lost', 'Enchantment').
card_types('vigil for the lost', ['Enchantment']).
card_subtypes('vigil for the lost', []).
card_colors('vigil for the lost', ['W']).
card_text('vigil for the lost', 'Whenever a creature you control dies, you may pay {X}. If you do, you gain X life.').
card_mana_cost('vigil for the lost', ['3', 'W']).
card_cmc('vigil for the lost', 4).
card_layout('vigil for the lost', 'normal').

% Found in: CHK
card_name('vigilance', 'Vigilance').
card_type('vigilance', 'Enchantment — Aura').
card_types('vigilance', ['Enchantment']).
card_subtypes('vigilance', ['Aura']).
card_colors('vigilance', ['W']).
card_text('vigilance', 'Enchant creature\nEnchanted creature has vigilance. (Attacking doesn\'t cause it to tap.)').
card_mana_cost('vigilance', ['W']).
card_cmc('vigilance', 1).
card_layout('vigilance', 'normal').

% Found in: 7ED, BTD, ULG
card_name('vigilant drake', 'Vigilant Drake').
card_type('vigilant drake', 'Creature — Drake').
card_types('vigilant drake', ['Creature']).
card_subtypes('vigilant drake', ['Drake']).
card_colors('vigilant drake', ['U']).
card_text('vigilant drake', 'Flying\n{2}{U}: Untap Vigilant Drake.').
card_mana_cost('vigilant drake', ['4', 'U']).
card_cmc('vigilant drake', 5).
card_layout('vigilant drake', 'normal').
card_power('vigilant drake', 3).
card_toughness('vigilant drake', 3).

% Found in: MIR
card_name('vigilant martyr', 'Vigilant Martyr').
card_type('vigilant martyr', 'Creature — Human Cleric').
card_types('vigilant martyr', ['Creature']).
card_subtypes('vigilant martyr', ['Human', 'Cleric']).
card_colors('vigilant martyr', ['W']).
card_text('vigilant martyr', 'Sacrifice Vigilant Martyr: Regenerate target creature.\n{W}{W}, {T}, Sacrifice Vigilant Martyr: Counter target spell that targets an enchantment.').
card_mana_cost('vigilant martyr', ['W']).
card_cmc('vigilant martyr', 1).
card_layout('vigilant martyr', 'normal').
card_power('vigilant martyr', 1).
card_toughness('vigilant martyr', 1).

% Found in: JUD
card_name('vigilant sentry', 'Vigilant Sentry').
card_type('vigilant sentry', 'Creature — Human Nomad').
card_types('vigilant sentry', ['Creature']).
card_subtypes('vigilant sentry', ['Human', 'Nomad']).
card_colors('vigilant sentry', ['W']).
card_text('vigilant sentry', 'Threshold — As long as seven or more cards are in your graveyard, Vigilant Sentry gets +1/+1 and has \"{T}: Target attacking or blocking creature gets +3/+3 until end of turn.\"').
card_mana_cost('vigilant sentry', ['1', 'W', 'W']).
card_cmc('vigilant sentry', 3).
card_layout('vigilant sentry', 'normal').
card_power('vigilant sentry', 2).
card_toughness('vigilant sentry', 2).

% Found in: AVR
card_name('vigilante justice', 'Vigilante Justice').
card_type('vigilante justice', 'Enchantment').
card_types('vigilante justice', ['Enchantment']).
card_subtypes('vigilante justice', []).
card_colors('vigilante justice', ['R']).
card_text('vigilante justice', 'Whenever a Human enters the battlefield under your control, Vigilante Justice deals 1 damage to target creature or player.').
card_mana_cost('vigilante justice', ['3', 'R']).
card_cmc('vigilante justice', 4).
card_layout('vigilante justice', 'normal').

% Found in: DPA, LRW
card_name('vigor', 'Vigor').
card_type('vigor', 'Creature — Elemental Incarnation').
card_types('vigor', ['Creature']).
card_subtypes('vigor', ['Elemental', 'Incarnation']).
card_colors('vigor', ['G']).
card_text('vigor', 'Trample\nIf damage would be dealt to a creature you control other than Vigor, prevent that damage. Put a +1/+1 counter on that creature for each 1 damage prevented this way.\nWhen Vigor is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_mana_cost('vigor', ['3', 'G', 'G', 'G']).
card_cmc('vigor', 6).
card_layout('vigor', 'normal').
card_power('vigor', 6).
card_toughness('vigor', 6).

% Found in: DDJ, RAV
card_name('vigor mortis', 'Vigor Mortis').
card_type('vigor mortis', 'Sorcery').
card_types('vigor mortis', ['Sorcery']).
card_subtypes('vigor mortis', []).
card_colors('vigor mortis', ['B']).
card_text('vigor mortis', 'Return target creature card from your graveyard to the battlefield. If {G} was spent to cast Vigor Mortis, that creature enters the battlefield with an additional +1/+1 counter on it.').
card_mana_cost('vigor mortis', ['2', 'B', 'B']).
card_cmc('vigor mortis', 4).
card_layout('vigor mortis', 'normal').

% Found in: INV
card_name('vigorous charge', 'Vigorous Charge').
card_type('vigorous charge', 'Instant').
card_types('vigorous charge', ['Instant']).
card_subtypes('vigorous charge', []).
card_colors('vigorous charge', ['G']).
card_text('vigorous charge', 'Kicker {W} (You may pay an additional {W} as you cast this spell.)\nTarget creature gains trample until end of turn. Whenever that creature deals combat damage this turn, if Vigorous Charge was kicked, you gain life equal to that damage.').
card_mana_cost('vigorous charge', ['G']).
card_cmc('vigorous charge', 1).
card_layout('vigorous charge', 'normal').

% Found in: BFZ
card_name('vile aggregate', 'Vile Aggregate').
card_type('vile aggregate', 'Creature — Eldrazi Drone').
card_types('vile aggregate', ['Creature']).
card_subtypes('vile aggregate', ['Eldrazi', 'Drone']).
card_colors('vile aggregate', []).
card_text('vile aggregate', 'Devoid (This card has no color.)\nVile Aggregate\'s power is equal to the number of colorless creatures you control.\nTrample\nIngest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.)').
card_mana_cost('vile aggregate', ['2', 'R']).
card_cmc('vile aggregate', 3).
card_layout('vile aggregate', 'normal').
card_power('vile aggregate', '*').
card_toughness('vile aggregate', 5).

% Found in: UNH
card_name('vile bile', 'Vile Bile').
card_type('vile bile', 'Creature — Ooze').
card_types('vile bile', ['Creature']).
card_subtypes('vile bile', ['Ooze']).
card_colors('vile bile', ['B']).
card_text('vile bile', 'Whenever a player\'s skin or fingernail touches Vile Bile, that player loses 2 life.').
card_mana_cost('vile bile', ['1', 'B']).
card_cmc('vile bile', 2).
card_layout('vile bile', 'normal').
card_power('vile bile', 2.5).
card_toughness('vile bile', 2.5).

% Found in: INV
card_name('vile consumption', 'Vile Consumption').
card_type('vile consumption', 'Enchantment').
card_types('vile consumption', ['Enchantment']).
card_subtypes('vile consumption', []).
card_colors('vile consumption', ['U', 'B']).
card_text('vile consumption', 'All creatures have \"At the beginning of your upkeep, sacrifice this creature unless you pay 1 life.\"').
card_mana_cost('vile consumption', ['1', 'U', 'B']).
card_cmc('vile consumption', 3).
card_layout('vile consumption', 'normal').

% Found in: LGN
card_name('vile deacon', 'Vile Deacon').
card_type('vile deacon', 'Creature — Human Cleric').
card_types('vile deacon', ['Creature']).
card_subtypes('vile deacon', ['Human', 'Cleric']).
card_colors('vile deacon', ['B']).
card_text('vile deacon', 'Whenever Vile Deacon attacks, it gets +X/+X until end of turn, where X is the number of Clerics on the battlefield.').
card_mana_cost('vile deacon', ['2', 'B', 'B']).
card_cmc('vile deacon', 4).
card_layout('vile deacon', 'normal').
card_power('vile deacon', 2).
card_toughness('vile deacon', 2).

% Found in: M13, M14
card_name('vile rebirth', 'Vile Rebirth').
card_type('vile rebirth', 'Instant').
card_types('vile rebirth', ['Instant']).
card_subtypes('vile rebirth', []).
card_colors('vile rebirth', ['B']).
card_text('vile rebirth', 'Exile target creature card from a graveyard. Put a 2/2 black Zombie creature token onto the battlefield.').
card_mana_cost('vile rebirth', ['B']).
card_cmc('vile rebirth', 1).
card_layout('vile rebirth', 'normal').

% Found in: C13, USG
card_name('vile requiem', 'Vile Requiem').
card_type('vile requiem', 'Enchantment').
card_types('vile requiem', ['Enchantment']).
card_subtypes('vile requiem', []).
card_colors('vile requiem', ['B']).
card_text('vile requiem', 'At the beginning of your upkeep, you may put a verse counter on Vile Requiem.\n{1}{B}, Sacrifice Vile Requiem: Destroy up to X target nonblack creatures, where X is the number of verse counters on Vile Requiem. They can\'t be regenerated.').
card_mana_cost('vile requiem', ['2', 'B', 'B']).
card_cmc('vile requiem', 4).
card_layout('vile requiem', 'normal').

% Found in: ISD
card_name('village bell-ringer', 'Village Bell-Ringer').
card_type('village bell-ringer', 'Creature — Human Scout').
card_types('village bell-ringer', ['Creature']).
card_subtypes('village bell-ringer', ['Human', 'Scout']).
card_colors('village bell-ringer', ['W']).
card_text('village bell-ringer', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Village Bell-Ringer enters the battlefield, untap all creatures you control.').
card_mana_cost('village bell-ringer', ['2', 'W']).
card_cmc('village bell-ringer', 3).
card_layout('village bell-ringer', 'normal').
card_power('village bell-ringer', 1).
card_toughness('village bell-ringer', 4).

% Found in: ISD
card_name('village cannibals', 'Village Cannibals').
card_type('village cannibals', 'Creature — Human').
card_types('village cannibals', ['Creature']).
card_subtypes('village cannibals', ['Human']).
card_colors('village cannibals', ['B']).
card_text('village cannibals', 'Whenever another Human creature dies, put a +1/+1 counter on Village Cannibals.').
card_mana_cost('village cannibals', ['2', 'B']).
card_cmc('village cannibals', 3).
card_layout('village cannibals', 'normal').
card_power('village cannibals', 2).
card_toughness('village cannibals', 2).

% Found in: BRB, MIR
card_name('village elder', 'Village Elder').
card_type('village elder', 'Creature — Human Druid').
card_types('village elder', ['Creature']).
card_subtypes('village elder', ['Human', 'Druid']).
card_colors('village elder', ['G']).
card_text('village elder', '{G}, {T}, Sacrifice a Forest: Regenerate target creature.').
card_mana_cost('village elder', ['G']).
card_cmc('village elder', 1).
card_layout('village elder', 'normal').
card_power('village elder', 1).
card_toughness('village elder', 1).

% Found in: ISD
card_name('village ironsmith', 'Village Ironsmith').
card_type('village ironsmith', 'Creature — Human Werewolf').
card_types('village ironsmith', ['Creature']).
card_subtypes('village ironsmith', ['Human', 'Werewolf']).
card_colors('village ironsmith', ['R']).
card_text('village ironsmith', 'First strike\nAt the beginning of each upkeep, if no spells were cast last turn, transform Village Ironsmith.').
card_mana_cost('village ironsmith', ['1', 'R']).
card_cmc('village ironsmith', 2).
card_layout('village ironsmith', 'double-faced').
card_power('village ironsmith', 1).
card_toughness('village ironsmith', 1).
card_sides('village ironsmith', 'ironfang').

% Found in: DKA
card_name('village survivors', 'Village Survivors').
card_type('village survivors', 'Creature — Human').
card_types('village survivors', ['Creature']).
card_subtypes('village survivors', ['Human']).
card_colors('village survivors', ['G']).
card_text('village survivors', 'Vigilance\nFateful hour — As long as you have 5 or less life, other creatures you control have vigilance.').
card_mana_cost('village survivors', ['4', 'G']).
card_cmc('village survivors', 5).
card_layout('village survivors', 'normal').
card_power('village survivors', 4).
card_toughness('village survivors', 5).

% Found in: ISD
card_name('villagers of estwald', 'Villagers of Estwald').
card_type('villagers of estwald', 'Creature — Human Werewolf').
card_types('villagers of estwald', ['Creature']).
card_subtypes('villagers of estwald', ['Human', 'Werewolf']).
card_colors('villagers of estwald', ['G']).
card_text('villagers of estwald', 'At the beginning of each upkeep, if no spells were cast last turn, transform Villagers of Estwald.').
card_mana_cost('villagers of estwald', ['2', 'G']).
card_cmc('villagers of estwald', 3).
card_layout('villagers of estwald', 'double-faced').
card_power('villagers of estwald', 2).
card_toughness('villagers of estwald', 3).
card_sides('villagers of estwald', 'howlpack of estwald').

% Found in: CHK
card_name('villainous ogre', 'Villainous Ogre').
card_type('villainous ogre', 'Creature — Ogre Warrior').
card_types('villainous ogre', ['Creature']).
card_subtypes('villainous ogre', ['Ogre', 'Warrior']).
card_colors('villainous ogre', ['B']).
card_text('villainous ogre', 'Villainous Ogre can\'t block.\nAs long as you control a Demon, Villainous Ogre has \"{B}: Regenerate Villainous Ogre.\"').
card_mana_cost('villainous ogre', ['2', 'B']).
card_cmc('villainous ogre', 3).
card_layout('villainous ogre', 'normal').
card_power('villainous ogre', 3).
card_toughness('villainous ogre', 2).

% Found in: KTK, pPRE
card_name('villainous wealth', 'Villainous Wealth').
card_type('villainous wealth', 'Sorcery').
card_types('villainous wealth', ['Sorcery']).
card_subtypes('villainous wealth', []).
card_colors('villainous wealth', ['U', 'B', 'G']).
card_text('villainous wealth', 'Target opponent exiles the top X cards of his or her library. You may cast any number of nonland cards with converted mana cost X or less from among them without paying their mana costs.').
card_mana_cost('villainous wealth', ['X', 'B', 'G', 'U']).
card_cmc('villainous wealth', 3).
card_layout('villainous wealth', 'normal').

% Found in: APC, pJGP
card_name('vindicate', 'Vindicate').
card_type('vindicate', 'Sorcery').
card_types('vindicate', ['Sorcery']).
card_subtypes('vindicate', []).
card_colors('vindicate', ['W', 'B']).
card_text('vindicate', 'Destroy target permanent.').
card_mana_cost('vindicate', ['1', 'W', 'B']).
card_cmc('vindicate', 3).
card_layout('vindicate', 'normal').

% Found in: RAV
card_name('vindictive mob', 'Vindictive Mob').
card_type('vindictive mob', 'Creature — Human Berserker').
card_types('vindictive mob', ['Creature']).
card_subtypes('vindictive mob', ['Human', 'Berserker']).
card_colors('vindictive mob', ['B']).
card_text('vindictive mob', 'When Vindictive Mob enters the battlefield, sacrifice a creature.\nVindictive Mob can\'t be blocked by Saprolings.').
card_mana_cost('vindictive mob', ['4', 'B', 'B']).
card_cmc('vindictive mob', 6).
card_layout('vindictive mob', 'normal').
card_power('vindictive mob', 5).
card_toughness('vindictive mob', 5).

% Found in: MMQ
card_name('vine dryad', 'Vine Dryad').
card_type('vine dryad', 'Creature — Dryad').
card_types('vine dryad', ['Creature']).
card_subtypes('vine dryad', ['Dryad']).
card_colors('vine dryad', ['G']).
card_text('vine dryad', 'Flash\nForestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)\nYou may exile a green card from your hand rather than pay Vine Dryad\'s mana cost.').
card_mana_cost('vine dryad', ['3', 'G']).
card_cmc('vine dryad', 4).
card_layout('vine dryad', 'normal').
card_power('vine dryad', 1).
card_toughness('vine dryad', 3).

% Found in: CHK
card_name('vine kami', 'Vine Kami').
card_type('vine kami', 'Creature — Spirit').
card_types('vine kami', ['Creature']).
card_subtypes('vine kami', ['Spirit']).
card_colors('vine kami', ['G']).
card_text('vine kami', 'Menace (This creature can\'t be blocked except by two or more creatures.)\nSoulshift 6 (When this creature dies, you may return target Spirit card with converted mana cost 6 or less from your graveyard to your hand.)').
card_mana_cost('vine kami', ['6', 'G']).
card_cmc('vine kami', 7).
card_layout('vine kami', 'normal').
card_power('vine kami', 4).
card_toughness('vine kami', 4).

% Found in: ORI
card_name('vine snare', 'Vine Snare').
card_type('vine snare', 'Instant').
card_types('vine snare', ['Instant']).
card_subtypes('vine snare', []).
card_colors('vine snare', ['G']).
card_text('vine snare', 'Prevent all combat damage that would be dealt this turn by creatures with power 4 or less.').
card_mana_cost('vine snare', ['2', 'G']).
card_cmc('vine snare', 3).
card_layout('vine snare', 'normal').

% Found in: 8ED, DD3_GVL, DDD, MMQ
card_name('vine trellis', 'Vine Trellis').
card_type('vine trellis', 'Creature — Plant Wall').
card_types('vine trellis', ['Creature']).
card_subtypes('vine trellis', ['Plant', 'Wall']).
card_colors('vine trellis', ['G']).
card_text('vine trellis', 'Defender (This creature can\'t attack.)\n{T}: Add {G} to your mana pool.').
card_mana_cost('vine trellis', ['1', 'G']).
card_cmc('vine trellis', 2).
card_layout('vine trellis', 'normal').
card_power('vine trellis', 0).
card_toughness('vine trellis', 4).

% Found in: DDM, RAV
card_name('vinelasher kudzu', 'Vinelasher Kudzu').
card_type('vinelasher kudzu', 'Creature — Plant').
card_types('vinelasher kudzu', ['Creature']).
card_subtypes('vinelasher kudzu', ['Plant']).
card_colors('vinelasher kudzu', ['G']).
card_text('vinelasher kudzu', 'Whenever a land enters the battlefield under your control, put a +1/+1 counter on Vinelasher Kudzu.').
card_mana_cost('vinelasher kudzu', ['1', 'G']).
card_cmc('vinelasher kudzu', 2).
card_layout('vinelasher kudzu', 'normal').
card_power('vinelasher kudzu', 1).
card_toughness('vinelasher kudzu', 1).

% Found in: MM2, ZEN
card_name('vines of vastwood', 'Vines of Vastwood').
card_type('vines of vastwood', 'Instant').
card_types('vines of vastwood', ['Instant']).
card_subtypes('vines of vastwood', []).
card_colors('vines of vastwood', ['G']).
card_text('vines of vastwood', 'Kicker {G} (You may pay an additional {G} as you cast this spell.)\nTarget creature can\'t be the target of spells or abilities your opponents control this turn. If Vines of Vastwood was kicked, that creature gets +4/+4 until end of turn.').
card_mana_cost('vines of vastwood', ['G']).
card_cmc('vines of vastwood', 1).
card_layout('vines of vastwood', 'normal').

% Found in: M15
card_name('vineweft', 'Vineweft').
card_type('vineweft', 'Enchantment — Aura').
card_types('vineweft', ['Enchantment']).
card_subtypes('vineweft', ['Aura']).
card_colors('vineweft', ['G']).
card_text('vineweft', 'Enchant creature\nEnchanted creature gets +1/+1.\n{4}{G}: Return Vineweft from your graveyard to your hand.').
card_mana_cost('vineweft', ['G']).
card_cmc('vineweft', 1).
card_layout('vineweft', 'normal').

% Found in: PCY
card_name('vintara elephant', 'Vintara Elephant').
card_type('vintara elephant', 'Creature — Elephant').
card_types('vintara elephant', ['Creature']).
card_subtypes('vintara elephant', ['Elephant']).
card_colors('vintara elephant', ['G']).
card_text('vintara elephant', 'Trample\n{3}: Vintara Elephant loses trample until end of turn. Any player may activate this ability.').
card_mana_cost('vintara elephant', ['4', 'G']).
card_cmc('vintara elephant', 5).
card_layout('vintara elephant', 'normal').
card_power('vintara elephant', 4).
card_toughness('vintara elephant', 3).

% Found in: PCY
card_name('vintara snapper', 'Vintara Snapper').
card_type('vintara snapper', 'Creature — Turtle').
card_types('vintara snapper', ['Creature']).
card_subtypes('vintara snapper', ['Turtle']).
card_colors('vintara snapper', ['G']).
card_text('vintara snapper', 'Vintara Snapper has shroud as long as you control no untapped lands. (It can\'t be the target of spells or abilities.)').
card_mana_cost('vintara snapper', ['G', 'G']).
card_cmc('vintara snapper', 2).
card_layout('vintara snapper', 'normal').
card_power('vintara snapper', 2).
card_toughness('vintara snapper', 2).

% Found in: TOR
card_name('violent eruption', 'Violent Eruption').
card_type('violent eruption', 'Instant').
card_types('violent eruption', ['Instant']).
card_subtypes('violent eruption', []).
card_colors('violent eruption', ['R']).
card_text('violent eruption', 'Violent Eruption deals 4 damage divided as you choose among any number of target creatures and/or players.\nMadness {1}{R}{R} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('violent eruption', ['1', 'R', 'R', 'R']).
card_cmc('violent eruption', 4).
card_layout('violent eruption', 'normal').

% Found in: ARB
card_name('violent outburst', 'Violent Outburst').
card_type('violent outburst', 'Instant').
card_types('violent outburst', ['Instant']).
card_subtypes('violent outburst', []).
card_colors('violent outburst', ['R', 'G']).
card_text('violent outburst', 'Cascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)\nCreatures you control get +1/+0 until end of turn.').
card_mana_cost('violent outburst', ['1', 'R', 'G']).
card_cmc('violent outburst', 3).
card_layout('violent outburst', 'normal').

% Found in: ALA
card_name('violent ultimatum', 'Violent Ultimatum').
card_type('violent ultimatum', 'Sorcery').
card_types('violent ultimatum', ['Sorcery']).
card_subtypes('violent ultimatum', []).
card_colors('violent ultimatum', ['B', 'R', 'G']).
card_text('violent ultimatum', 'Destroy three target permanents.').
card_mana_cost('violent ultimatum', ['B', 'B', 'R', 'R', 'R', 'G', 'G']).
card_cmc('violent ultimatum', 7).
card_layout('violent ultimatum', 'normal').

% Found in: MOR
card_name('violet pall', 'Violet Pall').
card_type('violet pall', 'Tribal Instant — Faerie').
card_types('violet pall', ['Tribal', 'Instant']).
card_subtypes('violet pall', ['Faerie']).
card_colors('violet pall', ['B']).
card_text('violet pall', 'Destroy target nonblack creature. Put a 1/1 black Faerie Rogue creature token with flying onto the battlefield.').
card_mana_cost('violet pall', ['4', 'B']).
card_cmc('violet pall', 5).
card_layout('violet pall', 'normal').

% Found in: THS
card_name('viper\'s kiss', 'Viper\'s Kiss').
card_type('viper\'s kiss', 'Enchantment — Aura').
card_types('viper\'s kiss', ['Enchantment']).
card_subtypes('viper\'s kiss', ['Aura']).
card_colors('viper\'s kiss', ['B']).
card_text('viper\'s kiss', 'Enchant creature\nEnchanted creature gets -1/-1, and its activated abilities can\'t be activated.').
card_mana_cost('viper\'s kiss', ['B']).
card_cmc('viper\'s kiss', 1).
card_layout('viper\'s kiss', 'normal').

% Found in: NPH
card_name('viral drake', 'Viral Drake').
card_type('viral drake', 'Creature — Drake').
card_types('viral drake', ['Creature']).
card_subtypes('viral drake', ['Drake']).
card_colors('viral drake', ['U']).
card_text('viral drake', 'Flying\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\n{3}{U}: Proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_mana_cost('viral drake', ['3', 'U']).
card_cmc('viral drake', 4).
card_layout('viral drake', 'normal').
card_power('viral drake', 1).
card_toughness('viral drake', 4).

% Found in: SHM
card_name('viridescent wisps', 'Viridescent Wisps').
card_type('viridescent wisps', 'Instant').
card_types('viridescent wisps', ['Instant']).
card_subtypes('viridescent wisps', []).
card_colors('viridescent wisps', ['G']).
card_text('viridescent wisps', 'Target creature becomes green and gets +1/+0 until end of turn.\nDraw a card.').
card_mana_cost('viridescent wisps', ['G']).
card_cmc('viridescent wisps', 1).
card_layout('viridescent wisps', 'normal').

% Found in: DST
card_name('viridian acolyte', 'Viridian Acolyte').
card_type('viridian acolyte', 'Creature — Elf Shaman').
card_types('viridian acolyte', ['Creature']).
card_subtypes('viridian acolyte', ['Elf', 'Shaman']).
card_colors('viridian acolyte', ['G']).
card_text('viridian acolyte', '{1}, {T}: Add one mana of any color to your mana pool.').
card_mana_cost('viridian acolyte', ['G']).
card_cmc('viridian acolyte', 1).
card_layout('viridian acolyte', 'normal').
card_power('viridian acolyte', 1).
card_toughness('viridian acolyte', 1).

% Found in: NPH
card_name('viridian betrayers', 'Viridian Betrayers').
card_type('viridian betrayers', 'Creature — Elf Warrior').
card_types('viridian betrayers', ['Creature']).
card_subtypes('viridian betrayers', ['Elf', 'Warrior']).
card_colors('viridian betrayers', ['G']).
card_text('viridian betrayers', 'Viridian Betrayers has infect as long as an opponent is poisoned. (It deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('viridian betrayers', ['1', 'G', 'G']).
card_cmc('viridian betrayers', 3).
card_layout('viridian betrayers', 'normal').
card_power('viridian betrayers', 3).
card_toughness('viridian betrayers', 1).

% Found in: MBS
card_name('viridian claw', 'Viridian Claw').
card_type('viridian claw', 'Artifact — Equipment').
card_types('viridian claw', ['Artifact']).
card_subtypes('viridian claw', ['Equipment']).
card_colors('viridian claw', []).
card_text('viridian claw', 'Equipped creature gets +1/+0 and has first strike.\nEquip {1}').
card_mana_cost('viridian claw', ['2']).
card_cmc('viridian claw', 2).
card_layout('viridian claw', 'normal').

% Found in: MBS
card_name('viridian corrupter', 'Viridian Corrupter').
card_type('viridian corrupter', 'Creature — Elf Shaman').
card_types('viridian corrupter', ['Creature']).
card_subtypes('viridian corrupter', ['Elf', 'Shaman']).
card_colors('viridian corrupter', ['G']).
card_text('viridian corrupter', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhen Viridian Corrupter enters the battlefield, destroy target artifact.').
card_mana_cost('viridian corrupter', ['1', 'G', 'G']).
card_cmc('viridian corrupter', 3).
card_layout('viridian corrupter', 'normal').
card_power('viridian corrupter', 2).
card_toughness('viridian corrupter', 2).

% Found in: MBS, PC2
card_name('viridian emissary', 'Viridian Emissary').
card_type('viridian emissary', 'Creature — Elf Scout').
card_types('viridian emissary', ['Creature']).
card_subtypes('viridian emissary', ['Elf', 'Scout']).
card_colors('viridian emissary', ['G']).
card_text('viridian emissary', 'When Viridian Emissary dies, you may search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.').
card_mana_cost('viridian emissary', ['1', 'G']).
card_cmc('viridian emissary', 2).
card_layout('viridian emissary', 'normal').
card_power('viridian emissary', 2).
card_toughness('viridian emissary', 1).

% Found in: NPH
card_name('viridian harvest', 'Viridian Harvest').
card_type('viridian harvest', 'Enchantment — Aura').
card_types('viridian harvest', ['Enchantment']).
card_subtypes('viridian harvest', ['Aura']).
card_colors('viridian harvest', ['G']).
card_text('viridian harvest', 'Enchant artifact\nWhen enchanted artifact is put into a graveyard, you gain 6 life.').
card_mana_cost('viridian harvest', ['G']).
card_cmc('viridian harvest', 1).
card_layout('viridian harvest', 'normal').

% Found in: MRD
card_name('viridian joiner', 'Viridian Joiner').
card_type('viridian joiner', 'Creature — Elf Druid').
card_types('viridian joiner', ['Creature']).
card_subtypes('viridian joiner', ['Elf', 'Druid']).
card_colors('viridian joiner', ['G']).
card_text('viridian joiner', '{T}: Add to your mana pool an amount of {G} equal to Viridian Joiner\'s power.').
card_mana_cost('viridian joiner', ['2', 'G']).
card_cmc('viridian joiner', 3).
card_layout('viridian joiner', 'normal').
card_power('viridian joiner', 1).
card_toughness('viridian joiner', 2).

% Found in: MRD
card_name('viridian longbow', 'Viridian Longbow').
card_type('viridian longbow', 'Artifact — Equipment').
card_types('viridian longbow', ['Artifact']).
card_subtypes('viridian longbow', ['Equipment']).
card_colors('viridian longbow', []).
card_text('viridian longbow', 'Equipped creature has \"{T}: This creature deals 1 damage to target creature or player.\"\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery. This card enters the battlefield unattached and stays on the battlefield if the creature leaves.)').
card_mana_cost('viridian longbow', ['1']).
card_cmc('viridian longbow', 1).
card_layout('viridian longbow', 'normal').

% Found in: 5DN
card_name('viridian lorebearers', 'Viridian Lorebearers').
card_type('viridian lorebearers', 'Creature — Elf Shaman').
card_types('viridian lorebearers', ['Creature']).
card_subtypes('viridian lorebearers', ['Elf', 'Shaman']).
card_colors('viridian lorebearers', ['G']).
card_text('viridian lorebearers', '{3}{G}, {T}: Target creature gets +X/+X until end of turn, where X is the number of artifacts your opponents control.').
card_mana_cost('viridian lorebearers', ['3', 'G']).
card_cmc('viridian lorebearers', 4).
card_layout('viridian lorebearers', 'normal').
card_power('viridian lorebearers', 3).
card_toughness('viridian lorebearers', 3).

% Found in: SOM
card_name('viridian revel', 'Viridian Revel').
card_type('viridian revel', 'Enchantment').
card_types('viridian revel', ['Enchantment']).
card_subtypes('viridian revel', []).
card_colors('viridian revel', ['G']).
card_text('viridian revel', 'Whenever an artifact is put into an opponent\'s graveyard from the battlefield, you may draw a card.').
card_mana_cost('viridian revel', ['1', 'G', 'G']).
card_cmc('viridian revel', 3).
card_layout('viridian revel', 'normal').

% Found in: 5DN
card_name('viridian scout', 'Viridian Scout').
card_type('viridian scout', 'Creature — Elf Warrior Scout').
card_types('viridian scout', ['Creature']).
card_subtypes('viridian scout', ['Elf', 'Warrior', 'Scout']).
card_colors('viridian scout', ['G']).
card_text('viridian scout', '{2}{G}, Sacrifice Viridian Scout: Viridian Scout deals 2 damage to target creature with flying.').
card_mana_cost('viridian scout', ['3', 'G']).
card_cmc('viridian scout', 4).
card_layout('viridian scout', 'normal').
card_power('viridian scout', 1).
card_toughness('viridian scout', 2).

% Found in: 10E, 9ED, MRD
card_name('viridian shaman', 'Viridian Shaman').
card_type('viridian shaman', 'Creature — Elf Shaman').
card_types('viridian shaman', ['Creature']).
card_subtypes('viridian shaman', ['Elf', 'Shaman']).
card_colors('viridian shaman', ['G']).
card_text('viridian shaman', 'When Viridian Shaman enters the battlefield, destroy target artifact.').
card_mana_cost('viridian shaman', ['2', 'G']).
card_cmc('viridian shaman', 3).
card_layout('viridian shaman', 'normal').
card_power('viridian shaman', 2).
card_toughness('viridian shaman', 2).

% Found in: DST
card_name('viridian zealot', 'Viridian Zealot').
card_type('viridian zealot', 'Creature — Elf Warrior').
card_types('viridian zealot', ['Creature']).
card_subtypes('viridian zealot', ['Elf', 'Warrior']).
card_colors('viridian zealot', ['G']).
card_text('viridian zealot', '{1}{G}, Sacrifice Viridian Zealot: Destroy target artifact or enchantment.').
card_mana_cost('viridian zealot', ['G', 'G']).
card_cmc('viridian zealot', 2).
card_layout('viridian zealot', 'normal').
card_power('viridian zealot', 2).
card_toughness('viridian zealot', 1).

% Found in: VAN
card_name('viridian zealot avatar', 'Viridian Zealot Avatar').
card_type('viridian zealot avatar', 'Vanguard').
card_types('viridian zealot avatar', ['Vanguard']).
card_subtypes('viridian zealot avatar', []).
card_colors('viridian zealot avatar', []).
card_text('viridian zealot avatar', '{2}, Sacrifice a creature: Destroy target artifact or enchantment. Search your library for a card with the same name as the sacrificed creature, reveal that card, and put it into your hand. Then shuffle your library.').
card_layout('viridian zealot avatar', 'vanguard').

% Found in: POR, V14
card_name('virtue\'s ruin', 'Virtue\'s Ruin').
card_type('virtue\'s ruin', 'Sorcery').
card_types('virtue\'s ruin', ['Sorcery']).
card_subtypes('virtue\'s ruin', []).
card_colors('virtue\'s ruin', ['B']).
card_text('virtue\'s ruin', 'Destroy all white creatures.').
card_mana_cost('virtue\'s ruin', ['2', 'B']).
card_cmc('virtue\'s ruin', 3).
card_layout('virtue\'s ruin', 'normal').

% Found in: PTK
card_name('virtuous charge', 'Virtuous Charge').
card_type('virtuous charge', 'Sorcery').
card_types('virtuous charge', ['Sorcery']).
card_subtypes('virtuous charge', []).
card_colors('virtuous charge', ['W']).
card_text('virtuous charge', 'Creatures you control get +1/+1 until end of turn.').
card_mana_cost('virtuous charge', ['2', 'W']).
card_cmc('virtuous charge', 3).
card_layout('virtuous charge', 'normal').

% Found in: DTK
card_name('virulent plague', 'Virulent Plague').
card_type('virulent plague', 'Enchantment').
card_types('virulent plague', ['Enchantment']).
card_subtypes('virulent plague', []).
card_colors('virulent plague', ['B']).
card_text('virulent plague', 'Creature tokens get -2/-2.').
card_mana_cost('virulent plague', ['2', 'B']).
card_cmc('virulent plague', 3).
card_layout('virulent plague', 'normal').

% Found in: FUT, H09
card_name('virulent sliver', 'Virulent Sliver').
card_type('virulent sliver', 'Creature — Sliver').
card_types('virulent sliver', ['Creature']).
card_subtypes('virulent sliver', ['Sliver']).
card_colors('virulent sliver', ['G']).
card_text('virulent sliver', 'All Sliver creatures have poisonous 1. (Whenever a Sliver deals combat damage to a player, that player gets a poison counter. A player with ten or more poison counters loses the game.)').
card_mana_cost('virulent sliver', ['G']).
card_cmc('virulent sliver', 1).
card_layout('virulent sliver', 'normal').
card_power('virulent sliver', 1).
card_toughness('virulent sliver', 1).

% Found in: ROE
card_name('virulent swipe', 'Virulent Swipe').
card_type('virulent swipe', 'Instant').
card_types('virulent swipe', ['Instant']).
card_subtypes('virulent swipe', []).
card_colors('virulent swipe', ['B']).
card_text('virulent swipe', 'Target creature gets +2/+0 and gains deathtouch until end of turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('virulent swipe', ['B']).
card_cmc('virulent swipe', 1).
card_layout('virulent swipe', 'normal').

% Found in: MBS
card_name('virulent wound', 'Virulent Wound').
card_type('virulent wound', 'Instant').
card_types('virulent wound', ['Instant']).
card_subtypes('virulent wound', []).
card_colors('virulent wound', ['B']).
card_text('virulent wound', 'Put a -1/-1 counter on target creature. When that creature dies this turn, its controller gets a poison counter.').
card_mana_cost('virulent wound', ['B']).
card_cmc('virulent wound', 1).
card_layout('virulent wound', 'normal').

% Found in: ONS, V11, VMA
card_name('visara the dreadful', 'Visara the Dreadful').
card_type('visara the dreadful', 'Legendary Creature — Gorgon').
card_types('visara the dreadful', ['Creature']).
card_subtypes('visara the dreadful', ['Gorgon']).
card_supertypes('visara the dreadful', ['Legendary']).
card_colors('visara the dreadful', ['B']).
card_text('visara the dreadful', 'Flying\n{T}: Destroy target creature. It can\'t be regenerated.').
card_mana_cost('visara the dreadful', ['3', 'B', 'B', 'B']).
card_cmc('visara the dreadful', 6).
card_layout('visara the dreadful', 'normal').
card_power('visara the dreadful', 5).
card_toughness('visara the dreadful', 5).

% Found in: ALA
card_name('viscera dragger', 'Viscera Dragger').
card_type('viscera dragger', 'Creature — Zombie Ogre Warrior').
card_types('viscera dragger', ['Creature']).
card_subtypes('viscera dragger', ['Zombie', 'Ogre', 'Warrior']).
card_colors('viscera dragger', ['B']).
card_text('viscera dragger', 'Cycling {2} ({2}, Discard this card: Draw a card.)\nUnearth {1}{B} ({1}{B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('viscera dragger', ['3', 'B']).
card_cmc('viscera dragger', 4).
card_layout('viscera dragger', 'normal').
card_power('viscera dragger', 3).
card_toughness('viscera dragger', 3).

% Found in: C13, M11
card_name('viscera seer', 'Viscera Seer').
card_type('viscera seer', 'Creature — Vampire Wizard').
card_types('viscera seer', ['Creature']).
card_subtypes('viscera seer', ['Vampire', 'Wizard']).
card_colors('viscera seer', ['B']).
card_text('viscera seer', 'Sacrifice a creature: Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('viscera seer', ['B']).
card_cmc('viscera seer', 1).
card_layout('viscera seer', 'normal').
card_power('viscera seer', 1).
card_toughness('viscera seer', 1).

% Found in: ALL, ME2
card_name('viscerid armor', 'Viscerid Armor').
card_type('viscerid armor', 'Enchantment — Aura').
card_types('viscerid armor', ['Enchantment']).
card_subtypes('viscerid armor', ['Aura']).
card_colors('viscerid armor', ['U']).
card_text('viscerid armor', 'Enchant creature\nEnchanted creature gets +1/+1.\n{1}{U}: Return Viscerid Armor to its owner\'s hand.').
card_mana_cost('viscerid armor', ['1', 'U']).
card_cmc('viscerid armor', 2).
card_layout('viscerid armor', 'normal').

% Found in: TSP
card_name('viscerid deepwalker', 'Viscerid Deepwalker').
card_type('viscerid deepwalker', 'Creature — Homarid Warrior').
card_types('viscerid deepwalker', ['Creature']).
card_subtypes('viscerid deepwalker', ['Homarid', 'Warrior']).
card_colors('viscerid deepwalker', ['U']).
card_text('viscerid deepwalker', '{U}: Viscerid Deepwalker gets +1/+0 until end of turn.\nSuspend 4—{U} (Rather than cast this card from your hand, you may pay {U} and exile it with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_mana_cost('viscerid deepwalker', ['4', 'U']).
card_cmc('viscerid deepwalker', 5).
card_layout('viscerid deepwalker', 'normal').
card_power('viscerid deepwalker', 2).
card_toughness('viscerid deepwalker', 3).

% Found in: ALL, CST, ME2
card_name('viscerid drone', 'Viscerid Drone').
card_type('viscerid drone', 'Creature — Homarid Drone').
card_types('viscerid drone', ['Creature']).
card_subtypes('viscerid drone', ['Homarid', 'Drone']).
card_colors('viscerid drone', ['U']).
card_text('viscerid drone', '{T}, Sacrifice a creature and a Swamp: Destroy target nonartifact creature. It can\'t be regenerated.\n{T}, Sacrifice a creature and a snow Swamp: Destroy target creature. It can\'t be regenerated.').
card_mana_cost('viscerid drone', ['1', 'U']).
card_cmc('viscerid drone', 2).
card_layout('viscerid drone', 'normal').
card_power('viscerid drone', 1).
card_toughness('viscerid drone', 2).

% Found in: TSP
card_name('viscid lemures', 'Viscid Lemures').
card_type('viscid lemures', 'Creature — Spirit').
card_types('viscid lemures', ['Creature']).
card_subtypes('viscid lemures', ['Spirit']).
card_colors('viscid lemures', ['B']).
card_text('viscid lemures', '{0}: Viscid Lemures gets -1/-0 and gains swampwalk until end of turn. (It can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('viscid lemures', ['4', 'B']).
card_cmc('viscid lemures', 5).
card_layout('viscid lemures', 'normal').
card_power('viscid lemures', 4).
card_toughness('viscid lemures', 3).

% Found in: C13, NMS
card_name('viseling', 'Viseling').
card_type('viseling', 'Artifact Creature — Construct').
card_types('viseling', ['Artifact', 'Creature']).
card_subtypes('viseling', ['Construct']).
card_colors('viseling', []).
card_text('viseling', 'At the beginning of each opponent\'s upkeep, Viseling deals X damage to that player, where X is the number of cards in his or her hand minus 4.').
card_mana_cost('viseling', ['4']).
card_cmc('viseling', 4).
card_layout('viseling', 'normal').
card_power('viseling', 2).
card_toughness('viseling', 2).

% Found in: CMD
card_name('vish kal, blood arbiter', 'Vish Kal, Blood Arbiter').
card_type('vish kal, blood arbiter', 'Legendary Creature — Vampire').
card_types('vish kal, blood arbiter', ['Creature']).
card_subtypes('vish kal, blood arbiter', ['Vampire']).
card_supertypes('vish kal, blood arbiter', ['Legendary']).
card_colors('vish kal, blood arbiter', ['W', 'B']).
card_text('vish kal, blood arbiter', 'Flying, lifelink\nSacrifice a creature: Put X +1/+1 counters on Vish Kal, Blood Arbiter, where X is the sacrificed creature\'s power.\nRemove all +1/+1 counters from Vish Kal: Target creature gets -1/-1 until end of turn for each +1/+1 counter removed this way.').
card_mana_cost('vish kal, blood arbiter', ['4', 'W', 'B', 'B']).
card_cmc('vish kal, blood arbiter', 7).
card_layout('vish kal, blood arbiter', 'normal').
card_power('vish kal, blood arbiter', 5).
card_toughness('vish kal, blood arbiter', 5).

% Found in: VIS
card_name('vision charm', 'Vision Charm').
card_type('vision charm', 'Instant').
card_types('vision charm', ['Instant']).
card_subtypes('vision charm', []).
card_colors('vision charm', ['U']).
card_text('vision charm', 'Choose one —\n• Target player puts the top four cards of his or her library into his or her graveyard.\n• Choose a land type and a basic land type. Each land of the first chosen type becomes the second chosen type until end of turn.\n• Target artifact phases out. (While it\'s phased out, it\'s treated as though it doesn\'t exist. It phases in before its controller untaps during his or her next untap step.)').
card_mana_cost('vision charm', ['U']).
card_cmc('vision charm', 1).
card_layout('vision charm', 'normal').

% Found in: C13, CMD, DIS
card_name('vision skeins', 'Vision Skeins').
card_type('vision skeins', 'Instant').
card_types('vision skeins', ['Instant']).
card_subtypes('vision skeins', []).
card_colors('vision skeins', ['U']).
card_text('vision skeins', 'Each player draws two cards.').
card_mana_cost('vision skeins', ['1', 'U']).
card_cmc('vision skeins', 2).
card_layout('vision skeins', 'normal').

% Found in: 4ED, LEG
card_name('visions', 'Visions').
card_type('visions', 'Sorcery').
card_types('visions', ['Sorcery']).
card_subtypes('visions', []).
card_colors('visions', ['W']).
card_text('visions', 'Look at the top five cards of target player\'s library. You may then have that player shuffle that library.').
card_mana_cost('visions', ['W']).
card_cmc('visions', 1).
card_layout('visions', 'normal').

% Found in: M12
card_name('visions of beyond', 'Visions of Beyond').
card_type('visions of beyond', 'Instant').
card_types('visions of beyond', ['Instant']).
card_subtypes('visions of beyond', []).
card_colors('visions of beyond', ['U']).
card_text('visions of beyond', 'Draw a card. If a graveyard has twenty or more cards in it, draw three cards instead.').
card_mana_cost('visions of beyond', ['U']).
card_cmc('visions of beyond', 1).
card_layout('visions of beyond', 'normal').

% Found in: NPH
card_name('vital splicer', 'Vital Splicer').
card_type('vital splicer', 'Creature — Human Artificer').
card_types('vital splicer', ['Creature']).
card_subtypes('vital splicer', ['Human', 'Artificer']).
card_colors('vital splicer', ['G']).
card_text('vital splicer', 'When Vital Splicer enters the battlefield, put a 3/3 colorless Golem artifact creature token onto the battlefield.\n{1}: Regenerate target Golem you control.').
card_mana_cost('vital splicer', ['3', 'G']).
card_cmc('vital splicer', 4).
card_layout('vital splicer', 'normal').
card_power('vital splicer', 1).
card_toughness('vital splicer', 1).

% Found in: BOK
card_name('vital surge', 'Vital Surge').
card_type('vital surge', 'Instant — Arcane').
card_types('vital surge', ['Instant']).
card_subtypes('vital surge', ['Arcane']).
card_colors('vital surge', ['G']).
card_text('vital surge', 'You gain 3 life.\nSplice onto Arcane {1}{G} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('vital surge', ['1', 'G']).
card_cmc('vital surge', 2).
card_layout('vital surge', 'normal').

% Found in: ONS
card_name('vitality charm', 'Vitality Charm').
card_type('vitality charm', 'Instant').
card_types('vitality charm', ['Instant']).
card_subtypes('vitality charm', []).
card_colors('vitality charm', ['G']).
card_text('vitality charm', 'Choose one —\n• Put a 1/1 green Insect creature token onto the battlefield.\n• Target creature gets +1/+1 and gains trample until end of turn.\n• Regenerate target Beast.').
card_mana_cost('vitality charm', ['G']).
card_cmc('vitality charm', 1).
card_layout('vitality charm', 'normal').

% Found in: 6ED, WTH
card_name('vitalize', 'Vitalize').
card_type('vitalize', 'Instant').
card_types('vitalize', ['Instant']).
card_subtypes('vitalize', []).
card_colors('vitalize', ['G']).
card_text('vitalize', 'Untap all creatures you control.').
card_mana_cost('vitalize', ['G']).
card_cmc('vitalize', 1).
card_layout('vitalize', 'normal').

% Found in: MIR
card_name('vitalizing cascade', 'Vitalizing Cascade').
card_type('vitalizing cascade', 'Instant').
card_types('vitalizing cascade', ['Instant']).
card_subtypes('vitalizing cascade', []).
card_colors('vitalizing cascade', ['W', 'G']).
card_text('vitalizing cascade', 'You gain X plus 3 life.').
card_mana_cost('vitalizing cascade', ['X', 'G', 'W']).
card_cmc('vitalizing cascade', 2).
card_layout('vitalizing cascade', 'normal').

% Found in: PCY
card_name('vitalizing wind', 'Vitalizing Wind').
card_type('vitalizing wind', 'Instant').
card_types('vitalizing wind', ['Instant']).
card_subtypes('vitalizing wind', []).
card_colors('vitalizing wind', ['G']).
card_text('vitalizing wind', 'Creatures you control get +7/+7 until end of turn.').
card_mana_cost('vitalizing wind', ['8', 'G']).
card_cmc('vitalizing wind', 9).
card_layout('vitalizing wind', 'normal').

% Found in: PLC
card_name('vitaspore thallid', 'Vitaspore Thallid').
card_type('vitaspore thallid', 'Creature — Fungus').
card_types('vitaspore thallid', ['Creature']).
card_subtypes('vitaspore thallid', ['Fungus']).
card_colors('vitaspore thallid', ['G']).
card_text('vitaspore thallid', 'At the beginning of your upkeep, put a spore counter on Vitaspore Thallid.\nRemove three spore counters from Vitaspore Thallid: Put a 1/1 green Saproling creature token onto the battlefield.\nSacrifice a Saproling: Target creature gains haste until end of turn.').
card_mana_cost('vitaspore thallid', ['1', 'G']).
card_cmc('vitaspore thallid', 2).
card_layout('vitaspore thallid', 'normal').
card_power('vitaspore thallid', 1).
card_toughness('vitaspore thallid', 1).

% Found in: ARB
card_name('vithian renegades', 'Vithian Renegades').
card_type('vithian renegades', 'Creature — Human Shaman').
card_types('vithian renegades', ['Creature']).
card_subtypes('vithian renegades', ['Human', 'Shaman']).
card_colors('vithian renegades', ['R', 'G']).
card_text('vithian renegades', 'When Vithian Renegades enters the battlefield, destroy target artifact.').
card_mana_cost('vithian renegades', ['1', 'R', 'G']).
card_cmc('vithian renegades', 3).
card_layout('vithian renegades', 'normal').
card_power('vithian renegades', 3).
card_toughness('vithian renegades', 2).

% Found in: ALA, DDK
card_name('vithian stinger', 'Vithian Stinger').
card_type('vithian stinger', 'Creature — Human Shaman').
card_types('vithian stinger', ['Creature']).
card_subtypes('vithian stinger', ['Human', 'Shaman']).
card_colors('vithian stinger', ['R']).
card_text('vithian stinger', '{T}: Vithian Stinger deals 1 damage to target creature or player.\nUnearth {1}{R} ({1}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('vithian stinger', ['2', 'R']).
card_cmc('vithian stinger', 3).
card_layout('vithian stinger', 'normal').
card_power('vithian stinger', 0).
card_toughness('vithian stinger', 1).

% Found in: RTR
card_name('vitu-ghazi guildmage', 'Vitu-Ghazi Guildmage').
card_type('vitu-ghazi guildmage', 'Creature — Dryad Shaman').
card_types('vitu-ghazi guildmage', ['Creature']).
card_subtypes('vitu-ghazi guildmage', ['Dryad', 'Shaman']).
card_colors('vitu-ghazi guildmage', ['W', 'G']).
card_text('vitu-ghazi guildmage', '{4}{G}{W}: Put a 3/3 green Centaur creature token onto the battlefield.\n{2}{G}{W}: Populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_mana_cost('vitu-ghazi guildmage', ['G', 'W']).
card_cmc('vitu-ghazi guildmage', 2).
card_layout('vitu-ghazi guildmage', 'normal').
card_power('vitu-ghazi guildmage', 2).
card_toughness('vitu-ghazi guildmage', 2).

% Found in: ARC, C13, DDH, PC2, RAV
card_name('vitu-ghazi, the city-tree', 'Vitu-Ghazi, the City-Tree').
card_type('vitu-ghazi, the city-tree', 'Land').
card_types('vitu-ghazi, the city-tree', ['Land']).
card_subtypes('vitu-ghazi, the city-tree', []).
card_colors('vitu-ghazi, the city-tree', []).
card_text('vitu-ghazi, the city-tree', '{T}: Add {1} to your mana pool.\n{2}{G}{W}, {T}: Put a 1/1 green Saproling creature token onto the battlefield.').
card_layout('vitu-ghazi, the city-tree', 'normal').

% Found in: C13, CMD, LRW, MMA
card_name('vivid crag', 'Vivid Crag').
card_type('vivid crag', 'Land').
card_types('vivid crag', ['Land']).
card_subtypes('vivid crag', []).
card_colors('vivid crag', []).
card_text('vivid crag', 'Vivid Crag enters the battlefield tapped with two charge counters on it.\n{T}: Add {R} to your mana pool.\n{T}, Remove a charge counter from Vivid Crag: Add one mana of any color to your mana pool.').
card_layout('vivid crag', 'normal').

% Found in: C13, CMD, H09, LRW, MMA, PC2
card_name('vivid creek', 'Vivid Creek').
card_type('vivid creek', 'Land').
card_types('vivid creek', ['Land']).
card_subtypes('vivid creek', []).
card_colors('vivid creek', []).
card_text('vivid creek', 'Vivid Creek enters the battlefield tapped with two charge counters on it.\n{T}: Add {U} to your mana pool.\n{T}, Remove a charge counter from Vivid Creek: Add one mana of any color to your mana pool.').
card_layout('vivid creek', 'normal').

% Found in: C13, CMD, H09, LRW, MMA
card_name('vivid grove', 'Vivid Grove').
card_type('vivid grove', 'Land').
card_types('vivid grove', ['Land']).
card_subtypes('vivid grove', []).
card_colors('vivid grove', []).
card_text('vivid grove', 'Vivid Grove enters the battlefield tapped with two charge counters on it.\n{T}: Add {G} to your mana pool.\n{T}, Remove a charge counter from Vivid Grove: Add one mana of any color to your mana pool.').
card_layout('vivid grove', 'normal').

% Found in: C13, CMD, LRW, MMA
card_name('vivid marsh', 'Vivid Marsh').
card_type('vivid marsh', 'Land').
card_types('vivid marsh', ['Land']).
card_subtypes('vivid marsh', []).
card_colors('vivid marsh', []).
card_text('vivid marsh', 'Vivid Marsh enters the battlefield tapped with two charge counters on it.\n{T}: Add {B} to your mana pool.\n{T}, Remove a charge counter from Vivid Marsh: Add one mana of any color to your mana pool.').
card_layout('vivid marsh', 'normal').

% Found in: CMD, LRW, MMA
card_name('vivid meadow', 'Vivid Meadow').
card_type('vivid meadow', 'Land').
card_types('vivid meadow', ['Land']).
card_subtypes('vivid meadow', []).
card_colors('vivid meadow', []).
card_text('vivid meadow', 'Vivid Meadow enters the battlefield tapped with two charge counters on it.\n{T}: Add {W} to your mana pool.\n{T}, Remove a charge counter from Vivid Meadow: Add one mana of any color to your mana pool.').
card_layout('vivid meadow', 'normal').

% Found in: ODY
card_name('vivify', 'Vivify').
card_type('vivify', 'Instant').
card_types('vivify', ['Instant']).
card_subtypes('vivify', []).
card_colors('vivify', ['G']).
card_text('vivify', 'Target land becomes a 3/3 creature until end of turn. It\'s still a land.\nDraw a card.').
card_mana_cost('vivify', ['2', 'G']).
card_cmc('vivify', 3).
card_layout('vivify', 'normal').

% Found in: MBS
card_name('vivisection', 'Vivisection').
card_type('vivisection', 'Sorcery').
card_types('vivisection', ['Sorcery']).
card_subtypes('vivisection', []).
card_colors('vivisection', ['U']).
card_text('vivisection', 'As an additional cost to cast Vivisection, sacrifice a creature.\nDraw three cards.').
card_mana_cost('vivisection', ['3', 'U']).
card_cmc('vivisection', 4).
card_layout('vivisection', 'normal').

% Found in: GTC
card_name('vizkopa confessor', 'Vizkopa Confessor').
card_type('vizkopa confessor', 'Creature — Human Cleric').
card_types('vizkopa confessor', ['Creature']).
card_subtypes('vizkopa confessor', ['Human', 'Cleric']).
card_colors('vizkopa confessor', ['W', 'B']).
card_text('vizkopa confessor', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)\nWhen Vizkopa Confessor enters the battlefield, pay any amount of life. Target opponent reveals that many cards from his or her hand. You choose one of them and exile it.').
card_mana_cost('vizkopa confessor', ['3', 'W', 'B']).
card_cmc('vizkopa confessor', 5).
card_layout('vizkopa confessor', 'normal').
card_power('vizkopa confessor', 1).
card_toughness('vizkopa confessor', 3).

% Found in: C13, GTC
card_name('vizkopa guildmage', 'Vizkopa Guildmage').
card_type('vizkopa guildmage', 'Creature — Human Wizard').
card_types('vizkopa guildmage', ['Creature']).
card_subtypes('vizkopa guildmage', ['Human', 'Wizard']).
card_colors('vizkopa guildmage', ['W', 'B']).
card_text('vizkopa guildmage', '{1}{W}{B}: Target creature gains lifelink until end of turn.\n{1}{W}{B}: Whenever you gain life this turn, each opponent loses that much life.').
card_mana_cost('vizkopa guildmage', ['W', 'B']).
card_cmc('vizkopa guildmage', 2).
card_layout('vizkopa guildmage', 'normal').
card_power('vizkopa guildmage', 2).
card_toughness('vizkopa guildmage', 2).

% Found in: 7ED, 8ED, 9ED, S00, S99
card_name('vizzerdrix', 'Vizzerdrix').
card_type('vizzerdrix', 'Creature — Rabbit Beast').
card_types('vizzerdrix', ['Creature']).
card_subtypes('vizzerdrix', ['Rabbit', 'Beast']).
card_colors('vizzerdrix', ['U']).
card_text('vizzerdrix', '').
card_mana_cost('vizzerdrix', ['6', 'U']).
card_cmc('vizzerdrix', 7).
card_layout('vizzerdrix', 'normal').
card_power('vizzerdrix', 6).
card_toughness('vizzerdrix', 6).

% Found in: INV
card_name('vodalian hypnotist', 'Vodalian Hypnotist').
card_type('vodalian hypnotist', 'Creature — Merfolk Wizard').
card_types('vodalian hypnotist', ['Creature']).
card_subtypes('vodalian hypnotist', ['Merfolk', 'Wizard']).
card_colors('vodalian hypnotist', ['U']).
card_text('vodalian hypnotist', '{2}{B}, {T}: Target player discards a card. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('vodalian hypnotist', ['1', 'U']).
card_cmc('vodalian hypnotist', 2).
card_layout('vodalian hypnotist', 'normal').
card_power('vodalian hypnotist', 1).
card_toughness('vodalian hypnotist', 1).

% Found in: WTH
card_name('vodalian illusionist', 'Vodalian Illusionist').
card_type('vodalian illusionist', 'Creature — Merfolk Wizard').
card_types('vodalian illusionist', ['Creature']).
card_subtypes('vodalian illusionist', ['Merfolk', 'Wizard']).
card_colors('vodalian illusionist', ['U']).
card_text('vodalian illusionist', '{U}{U}, {T}: Target creature phases out. (While it\'s phased out, it\'s treated as though it doesn\'t exist. It phases in before its controller untaps during his or her next untap step.)').
card_mana_cost('vodalian illusionist', ['2', 'U']).
card_cmc('vodalian illusionist', 3).
card_layout('vodalian illusionist', 'normal').
card_power('vodalian illusionist', 2).
card_toughness('vodalian illusionist', 2).

% Found in: FEM, MED
card_name('vodalian knights', 'Vodalian Knights').
card_type('vodalian knights', 'Creature — Merfolk Knight').
card_types('vodalian knights', ['Creature']).
card_subtypes('vodalian knights', ['Merfolk', 'Knight']).
card_colors('vodalian knights', ['U']).
card_text('vodalian knights', 'First strike\nVodalian Knights can\'t attack unless defending player controls an Island.\nWhen you control no Islands, sacrifice Vodalian Knights.\n{U}: Vodalian Knights gains flying until end of turn.').
card_mana_cost('vodalian knights', ['1', 'U', 'U']).
card_cmc('vodalian knights', 3).
card_layout('vodalian knights', 'normal').
card_power('vodalian knights', 2).
card_toughness('vodalian knights', 2).
card_reserved('vodalian knights').

% Found in: FEM
card_name('vodalian mage', 'Vodalian Mage').
card_type('vodalian mage', 'Creature — Merfolk Wizard').
card_types('vodalian mage', ['Creature']).
card_subtypes('vodalian mage', ['Merfolk', 'Wizard']).
card_colors('vodalian mage', ['U']).
card_text('vodalian mage', '{U}, {T}: Counter target spell unless its controller pays {1}.').
card_mana_cost('vodalian mage', ['2', 'U']).
card_cmc('vodalian mage', 3).
card_layout('vodalian mage', 'normal').
card_power('vodalian mage', 1).
card_toughness('vodalian mage', 1).

% Found in: INV
card_name('vodalian merchant', 'Vodalian Merchant').
card_type('vodalian merchant', 'Creature — Merfolk').
card_types('vodalian merchant', ['Creature']).
card_subtypes('vodalian merchant', ['Merfolk']).
card_colors('vodalian merchant', ['U']).
card_text('vodalian merchant', 'When Vodalian Merchant enters the battlefield, draw a card, then discard a card.').
card_mana_cost('vodalian merchant', ['1', 'U']).
card_cmc('vodalian merchant', 2).
card_layout('vodalian merchant', 'normal').
card_power('vodalian merchant', 1).
card_toughness('vodalian merchant', 2).

% Found in: APC
card_name('vodalian mystic', 'Vodalian Mystic').
card_type('vodalian mystic', 'Creature — Merfolk Wizard').
card_types('vodalian mystic', ['Creature']).
card_subtypes('vodalian mystic', ['Merfolk', 'Wizard']).
card_colors('vodalian mystic', ['U']).
card_text('vodalian mystic', '{T}: Target instant or sorcery spell becomes the color of your choice.').
card_mana_cost('vodalian mystic', ['1', 'U']).
card_cmc('vodalian mystic', 2).
card_layout('vodalian mystic', 'normal').
card_power('vodalian mystic', 1).
card_toughness('vodalian mystic', 1).

% Found in: INV
card_name('vodalian serpent', 'Vodalian Serpent').
card_type('vodalian serpent', 'Creature — Serpent').
card_types('vodalian serpent', ['Creature']).
card_subtypes('vodalian serpent', ['Serpent']).
card_colors('vodalian serpent', ['U']).
card_text('vodalian serpent', 'Kicker {2} (You may pay an additional {2} as you cast this spell.)\nVodalian Serpent can\'t attack unless defending player controls an Island.\nIf Vodalian Serpent was kicked, it enters the battlefield with four +1/+1 counters on it.').
card_mana_cost('vodalian serpent', ['3', 'U']).
card_cmc('vodalian serpent', 4).
card_layout('vodalian serpent', 'normal').
card_power('vodalian serpent', 2).
card_toughness('vodalian serpent', 2).

% Found in: 5ED, 6ED, FEM
card_name('vodalian soldiers', 'Vodalian Soldiers').
card_type('vodalian soldiers', 'Creature — Merfolk Soldier').
card_types('vodalian soldiers', ['Creature']).
card_subtypes('vodalian soldiers', ['Merfolk', 'Soldier']).
card_colors('vodalian soldiers', ['U']).
card_text('vodalian soldiers', '').
card_mana_cost('vodalian soldiers', ['1', 'U']).
card_cmc('vodalian soldiers', 2).
card_layout('vodalian soldiers', 'normal').
card_power('vodalian soldiers', 1).
card_toughness('vodalian soldiers', 2).

% Found in: FEM
card_name('vodalian war machine', 'Vodalian War Machine').
card_type('vodalian war machine', 'Creature — Wall').
card_types('vodalian war machine', ['Creature']).
card_subtypes('vodalian war machine', ['Wall']).
card_colors('vodalian war machine', ['U']).
card_text('vodalian war machine', 'Defender (This creature can\'t attack.)\nTap an untapped Merfolk you control: Vodalian War Machine can attack this turn as though it didn\'t have defender.\nTap an untapped Merfolk you control: Vodalian War Machine gets +2/+1 until end of turn.\nWhen Vodalian War Machine dies, destroy all Merfolk tapped this turn to pay for its abilities.').
card_mana_cost('vodalian war machine', ['1', 'U', 'U']).
card_cmc('vodalian war machine', 3).
card_layout('vodalian war machine', 'normal').
card_power('vodalian war machine', 0).
card_toughness('vodalian war machine', 4).
card_reserved('vodalian war machine').

% Found in: INV
card_name('vodalian zombie', 'Vodalian Zombie').
card_type('vodalian zombie', 'Creature — Merfolk Zombie').
card_types('vodalian zombie', ['Creature']).
card_subtypes('vodalian zombie', ['Merfolk', 'Zombie']).
card_colors('vodalian zombie', ['U', 'B']).
card_text('vodalian zombie', 'Protection from green').
card_mana_cost('vodalian zombie', ['U', 'B']).
card_cmc('vodalian zombie', 2).
card_layout('vodalian zombie', 'normal').
card_power('vodalian zombie', 2).
card_toughness('vodalian zombie', 2).

% Found in: 10E, CMD, PLS
card_name('voice of all', 'Voice of All').
card_type('voice of all', 'Creature — Angel').
card_types('voice of all', ['Creature']).
card_subtypes('voice of all', ['Angel']).
card_colors('voice of all', ['W']).
card_text('voice of all', 'Flying\nAs Voice of All enters the battlefield, choose a color.\nVoice of All has protection from the chosen color.').
card_mana_cost('voice of all', ['2', 'W', 'W']).
card_cmc('voice of all', 4).
card_layout('voice of all', 'normal').
card_power('voice of all', 2).
card_toughness('voice of all', 2).

% Found in: UDS
card_name('voice of duty', 'Voice of Duty').
card_type('voice of duty', 'Creature — Angel').
card_types('voice of duty', ['Creature']).
card_subtypes('voice of duty', ['Angel']).
card_colors('voice of duty', ['W']).
card_text('voice of duty', 'Flying, protection from green').
card_mana_cost('voice of duty', ['3', 'W']).
card_cmc('voice of duty', 4).
card_layout('voice of duty', 'normal').
card_power('voice of duty', 2).
card_toughness('voice of duty', 2).

% Found in: USG
card_name('voice of grace', 'Voice of Grace').
card_type('voice of grace', 'Creature — Angel').
card_types('voice of grace', ['Creature']).
card_subtypes('voice of grace', ['Angel']).
card_colors('voice of grace', ['W']).
card_text('voice of grace', 'Flying, protection from black').
card_mana_cost('voice of grace', ['3', 'W']).
card_cmc('voice of grace', 4).
card_layout('voice of grace', 'normal').
card_power('voice of grace', 2).
card_toughness('voice of grace', 2).

% Found in: USG
card_name('voice of law', 'Voice of Law').
card_type('voice of law', 'Creature — Angel').
card_types('voice of law', ['Creature']).
card_subtypes('voice of law', ['Angel']).
card_colors('voice of law', ['W']).
card_text('voice of law', 'Flying, protection from red').
card_mana_cost('voice of law', ['3', 'W']).
card_cmc('voice of law', 4).
card_layout('voice of law', 'normal').
card_power('voice of law', 2).
card_toughness('voice of law', 2).

% Found in: UDS
card_name('voice of reason', 'Voice of Reason').
card_type('voice of reason', 'Creature — Angel').
card_types('voice of reason', ['Creature']).
card_subtypes('voice of reason', ['Angel']).
card_colors('voice of reason', ['W']).
card_text('voice of reason', 'Flying, protection from blue').
card_mana_cost('voice of reason', ['3', 'W']).
card_cmc('voice of reason', 4).
card_layout('voice of reason', 'normal').
card_power('voice of reason', 2).
card_toughness('voice of reason', 2).

% Found in: DGM
card_name('voice of resurgence', 'Voice of Resurgence').
card_type('voice of resurgence', 'Creature — Elemental').
card_types('voice of resurgence', ['Creature']).
card_subtypes('voice of resurgence', ['Elemental']).
card_colors('voice of resurgence', ['W', 'G']).
card_text('voice of resurgence', 'Whenever an opponent casts a spell during your turn or when Voice of Resurgence dies, put a green and white Elemental creature token onto the battlefield with \"This creature\'s power and toughness are each equal to the number of creatures you control.\"').
card_mana_cost('voice of resurgence', ['G', 'W']).
card_cmc('voice of resurgence', 2).
card_layout('voice of resurgence', 'normal').
card_power('voice of resurgence', 2).
card_toughness('voice of resurgence', 2).

% Found in: AVR
card_name('voice of the provinces', 'Voice of the Provinces').
card_type('voice of the provinces', 'Creature — Angel').
card_types('voice of the provinces', ['Creature']).
card_subtypes('voice of the provinces', ['Angel']).
card_colors('voice of the provinces', ['W']).
card_text('voice of the provinces', 'Flying\nWhen Voice of the Provinces enters the battlefield, put a 1/1 white Human creature token onto the battlefield.').
card_mana_cost('voice of the provinces', ['4', 'W', 'W']).
card_cmc('voice of the provinces', 6).
card_layout('voice of the provinces', 'normal').
card_power('voice of the provinces', 3).
card_toughness('voice of the provinces', 3).

% Found in: DD3_EVG, EVG, ONS
card_name('voice of the woods', 'Voice of the Woods').
card_type('voice of the woods', 'Creature — Elf').
card_types('voice of the woods', ['Creature']).
card_subtypes('voice of the woods', ['Elf']).
card_colors('voice of the woods', ['G']).
card_text('voice of the woods', 'Tap five untapped Elves you control: Put a 7/7 green Elemental creature token with trample onto the battlefield.').
card_mana_cost('voice of the woods', ['3', 'G', 'G']).
card_cmc('voice of the woods', 5).
card_layout('voice of the woods', 'normal').
card_power('voice of the woods', 2).
card_toughness('voice of the woods', 2).

% Found in: NMS
card_name('voice of truth', 'Voice of Truth').
card_type('voice of truth', 'Creature — Angel').
card_types('voice of truth', ['Creature']).
card_subtypes('voice of truth', ['Angel']).
card_colors('voice of truth', ['W']).
card_text('voice of truth', 'Flying, protection from white').
card_mana_cost('voice of truth', ['3', 'W']).
card_cmc('voice of truth', 4).
card_layout('voice of truth', 'normal').
card_power('voice of truth', 2).
card_toughness('voice of truth', 2).

% Found in: ISD
card_name('voiceless spirit', 'Voiceless Spirit').
card_type('voiceless spirit', 'Creature — Spirit').
card_types('voiceless spirit', ['Creature']).
card_subtypes('voiceless spirit', ['Spirit']).
card_colors('voiceless spirit', ['W']).
card_text('voiceless spirit', 'Flying, first strike').
card_mana_cost('voiceless spirit', ['2', 'W']).
card_cmc('voiceless spirit', 3).
card_layout('voiceless spirit', 'normal').
card_power('voiceless spirit', 2).
card_toughness('voiceless spirit', 1).

% Found in: CON
card_name('voices from the void', 'Voices from the Void').
card_type('voices from the void', 'Sorcery').
card_types('voices from the void', ['Sorcery']).
card_subtypes('voices from the void', []).
card_colors('voices from the void', ['B']).
card_text('voices from the void', 'Domain — Target player discards a card for each basic land type among lands you control.').
card_mana_cost('voices from the void', ['4', 'B']).
card_cmc('voices from the void', 5).
card_layout('voices from the void', 'normal').

% Found in: INV, TSB
card_name('void', 'Void').
card_type('void', 'Sorcery').
card_types('void', ['Sorcery']).
card_subtypes('void', []).
card_colors('void', ['B', 'R']).
card_text('void', 'Choose a number. Destroy all artifacts and creatures with converted mana cost equal to that number. Then target player reveals his or her hand and discards all nonland cards with converted mana cost equal to the number.').
card_mana_cost('void', ['3', 'B', 'R']).
card_cmc('void', 5).
card_layout('void', 'normal').

% Found in: BFZ
card_name('void attendant', 'Void Attendant').
card_type('void attendant', 'Creature — Eldrazi Processor').
card_types('void attendant', ['Creature']).
card_subtypes('void attendant', ['Eldrazi', 'Processor']).
card_colors('void attendant', []).
card_text('void attendant', 'Devoid (This card has no color.)\n{1}{G}, Put a card an opponent owns from exile into that player\'s graveyard: Put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('void attendant', ['2', 'G']).
card_cmc('void attendant', 3).
card_layout('void attendant', 'normal').
card_power('void attendant', 2).
card_toughness('void attendant', 3).

% Found in: CSP
card_name('void maw', 'Void Maw').
card_type('void maw', 'Creature — Horror').
card_types('void maw', ['Creature']).
card_subtypes('void maw', ['Horror']).
card_colors('void maw', ['B']).
card_text('void maw', 'Trample\nIf another creature would die, exile it instead.\nPut a card exiled with Void Maw into its owner\'s graveyard: Void Maw gets +2/+2 until end of turn.').
card_mana_cost('void maw', ['4', 'B', 'B']).
card_cmc('void maw', 6).
card_layout('void maw', 'normal').
card_power('void maw', 4).
card_toughness('void maw', 5).

% Found in: M15
card_name('void snare', 'Void Snare').
card_type('void snare', 'Sorcery').
card_types('void snare', ['Sorcery']).
card_subtypes('void snare', []).
card_colors('void snare', ['U']).
card_text('void snare', 'Return target nonland permanent to its owner\'s hand.').
card_mana_cost('void snare', ['U']).
card_cmc('void snare', 1).
card_layout('void snare', 'normal').

% Found in: DTK
card_name('void squall', 'Void Squall').
card_type('void squall', 'Sorcery').
card_types('void squall', ['Sorcery']).
card_subtypes('void squall', []).
card_colors('void squall', ['U']).
card_text('void squall', 'Return target nonland permanent to its owner\'s hand.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('void squall', ['4', 'U']).
card_cmc('void squall', 5).
card_layout('void squall', 'normal').

% Found in: M13
card_name('void stalker', 'Void Stalker').
card_type('void stalker', 'Creature — Elemental').
card_types('void stalker', ['Creature']).
card_subtypes('void stalker', ['Elemental']).
card_colors('void stalker', ['U']).
card_text('void stalker', '{2}{U}, {T}: Put Void Stalker and target creature on top of their owners\' libraries, then those players shuffle their libraries.').
card_mana_cost('void stalker', ['1', 'U']).
card_cmc('void stalker', 2).
card_layout('void stalker', 'normal').
card_power('void stalker', 2).
card_toughness('void stalker', 1).

% Found in: BFZ
card_name('void winnower', 'Void Winnower').
card_type('void winnower', 'Creature — Eldrazi').
card_types('void winnower', ['Creature']).
card_subtypes('void winnower', ['Eldrazi']).
card_colors('void winnower', []).
card_text('void winnower', 'Your opponents can\'t cast spells with even converted mana costs. (Zero is even.)\nYour opponents can\'t block with creatures with even converted mana costs.').
card_mana_cost('void winnower', ['9']).
card_cmc('void winnower', 9).
card_layout('void winnower', 'normal').
card_power('void winnower', 11).
card_toughness('void winnower', 9).

% Found in: DD2, DD3_JVC, LGN
card_name('voidmage apprentice', 'Voidmage Apprentice').
card_type('voidmage apprentice', 'Creature — Human Wizard').
card_types('voidmage apprentice', ['Creature']).
card_subtypes('voidmage apprentice', ['Human', 'Wizard']).
card_colors('voidmage apprentice', ['U']).
card_text('voidmage apprentice', 'Morph {2}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Voidmage Apprentice is turned face up, counter target spell.').
card_mana_cost('voidmage apprentice', ['1', 'U']).
card_cmc('voidmage apprentice', 2).
card_layout('voidmage apprentice', 'normal').
card_power('voidmage apprentice', 1).
card_toughness('voidmage apprentice', 1).

% Found in: TSP, pMEI
card_name('voidmage husher', 'Voidmage Husher').
card_type('voidmage husher', 'Creature — Human Wizard').
card_types('voidmage husher', ['Creature']).
card_subtypes('voidmage husher', ['Human', 'Wizard']).
card_colors('voidmage husher', ['U']).
card_text('voidmage husher', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Voidmage Husher enters the battlefield, counter target activated ability. (Mana abilities can\'t be targeted.)\nWhenever you cast a spell, you may return Voidmage Husher to its owner\'s hand.').
card_mana_cost('voidmage husher', ['3', 'U']).
card_cmc('voidmage husher', 4).
card_layout('voidmage husher', 'normal').
card_power('voidmage husher', 2).
card_toughness('voidmage husher', 2).

% Found in: ONS, TSB, pMPR
card_name('voidmage prodigy', 'Voidmage Prodigy').
card_type('voidmage prodigy', 'Creature — Human Wizard').
card_types('voidmage prodigy', ['Creature']).
card_subtypes('voidmage prodigy', ['Human', 'Wizard']).
card_colors('voidmage prodigy', ['U']).
card_text('voidmage prodigy', '{U}{U}, Sacrifice a Wizard: Counter target spell.\nMorph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('voidmage prodigy', ['U', 'U']).
card_cmc('voidmage prodigy', 2).
card_layout('voidmage prodigy', 'normal').
card_power('voidmage prodigy', 2).
card_toughness('voidmage prodigy', 1).

% Found in: DIS, pCMP
card_name('voidslime', 'Voidslime').
card_type('voidslime', 'Instant').
card_types('voidslime', ['Instant']).
card_subtypes('voidslime', []).
card_colors('voidslime', ['U', 'G']).
card_text('voidslime', 'Counter target spell, activated ability, or triggered ability. (Mana abilities can\'t be targeted.)').
card_mana_cost('voidslime', ['G', 'U', 'U']).
card_cmc('voidslime', 3).
card_layout('voidslime', 'normal').

% Found in: PLC
card_name('voidstone gargoyle', 'Voidstone Gargoyle').
card_type('voidstone gargoyle', 'Creature — Gargoyle').
card_types('voidstone gargoyle', ['Creature']).
card_subtypes('voidstone gargoyle', ['Gargoyle']).
card_colors('voidstone gargoyle', ['W']).
card_text('voidstone gargoyle', 'Flying\nAs Voidstone Gargoyle enters the battlefield, name a nonland card.\nThe named card can\'t be cast.\nActivated abilities of sources with the chosen name can\'t be activated.').
card_mana_cost('voidstone gargoyle', ['3', 'W', 'W']).
card_cmc('voidstone gargoyle', 5).
card_layout('voidstone gargoyle', 'normal').
card_power('voidstone gargoyle', 3).
card_toughness('voidstone gargoyle', 3).

% Found in: GTC
card_name('voidwalk', 'Voidwalk').
card_type('voidwalk', 'Sorcery').
card_types('voidwalk', ['Sorcery']).
card_subtypes('voidwalk', []).
card_colors('voidwalk', ['U']).
card_text('voidwalk', 'Exile target creature. Return it to the battlefield under its owner\'s control at the beginning of the next end step.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_mana_cost('voidwalk', ['3', 'U']).
card_cmc('voidwalk', 4).
card_layout('voidwalk', 'normal').

% Found in: RTR
card_name('voidwielder', 'Voidwielder').
card_type('voidwielder', 'Creature — Human Wizard').
card_types('voidwielder', ['Creature']).
card_subtypes('voidwielder', ['Human', 'Wizard']).
card_colors('voidwielder', ['U']).
card_text('voidwielder', 'When Voidwielder enters the battlefield, you may return target creature to its owner\'s hand.').
card_mana_cost('voidwielder', ['4', 'U']).
card_cmc('voidwielder', 5).
card_layout('voidwielder', 'normal').
card_power('voidwielder', 1).
card_toughness('voidwielder', 4).

% Found in: RTR
card_name('volatile rig', 'Volatile Rig').
card_type('volatile rig', 'Artifact Creature — Construct').
card_types('volatile rig', ['Artifact', 'Creature']).
card_subtypes('volatile rig', ['Construct']).
card_colors('volatile rig', []).
card_text('volatile rig', 'Trample\nVolatile Rig attacks each turn if able.\nWhenever Volatile Rig is dealt damage, flip a coin. If you lose the flip, sacrifice Volatile Rig.\nWhen Volatile Rig dies, flip a coin. If you lose the flip, it deals 4 damage to each creature and each player.').
card_mana_cost('volatile rig', ['4']).
card_cmc('volatile rig', 4).
card_layout('volatile rig', 'normal').
card_power('volatile rig', 4).
card_toughness('volatile rig', 4).

% Found in: TSP
card_name('volcanic awakening', 'Volcanic Awakening').
card_type('volcanic awakening', 'Sorcery').
card_types('volcanic awakening', ['Sorcery']).
card_subtypes('volcanic awakening', []).
card_colors('volcanic awakening', ['R']).
card_text('volcanic awakening', 'Destroy target land.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_mana_cost('volcanic awakening', ['4', 'R', 'R']).
card_cmc('volcanic awakening', 6).
card_layout('volcanic awakening', 'normal').

% Found in: 6ED, ATH, M12, MIR, POR, S99
card_name('volcanic dragon', 'Volcanic Dragon').
card_type('volcanic dragon', 'Creature — Dragon').
card_types('volcanic dragon', ['Creature']).
card_subtypes('volcanic dragon', ['Dragon']).
card_colors('volcanic dragon', ['R']).
card_text('volcanic dragon', 'Flying\nHaste (This creature can attack and {T} as soon as it comes under your control.)').
card_mana_cost('volcanic dragon', ['4', 'R', 'R']).
card_cmc('volcanic dragon', 6).
card_layout('volcanic dragon', 'normal').
card_power('volcanic dragon', 4).
card_toughness('volcanic dragon', 4).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('volcanic eruption', 'Volcanic Eruption').
card_type('volcanic eruption', 'Sorcery').
card_types('volcanic eruption', ['Sorcery']).
card_subtypes('volcanic eruption', []).
card_colors('volcanic eruption', ['U']).
card_text('volcanic eruption', 'Destroy X target Mountains. Volcanic Eruption deals damage to each creature and each player equal to the number of Mountains put into a graveyard this way.').
card_mana_cost('volcanic eruption', ['X', 'U', 'U', 'U']).
card_cmc('volcanic eruption', 3).
card_layout('volcanic eruption', 'normal').

% Found in: ARC, CNS, CON, pMPR
card_name('volcanic fallout', 'Volcanic Fallout').
card_type('volcanic fallout', 'Instant').
card_types('volcanic fallout', ['Instant']).
card_subtypes('volcanic fallout', []).
card_colors('volcanic fallout', ['R']).
card_text('volcanic fallout', 'Volcanic Fallout can\'t be countered.\nVolcanic Fallout deals 2 damage to each creature and each player.').
card_mana_cost('volcanic fallout', ['1', 'R', 'R']).
card_cmc('volcanic fallout', 3).
card_layout('volcanic fallout', 'normal').

% Found in: 6ED, M13, M14, MIR, pFNM
card_name('volcanic geyser', 'Volcanic Geyser').
card_type('volcanic geyser', 'Instant').
card_types('volcanic geyser', ['Instant']).
card_subtypes('volcanic geyser', []).
card_colors('volcanic geyser', ['R']).
card_text('volcanic geyser', 'Volcanic Geyser deals X damage to target creature or player.').
card_mana_cost('volcanic geyser', ['X', 'R', 'R']).
card_cmc('volcanic geyser', 2).
card_layout('volcanic geyser', 'normal').

% Found in: 7ED, 8ED, 9ED, PO2, POR, S99, pSUS
card_name('volcanic hammer', 'Volcanic Hammer').
card_type('volcanic hammer', 'Sorcery').
card_types('volcanic hammer', ['Sorcery']).
card_subtypes('volcanic hammer', []).
card_colors('volcanic hammer', ['R']).
card_text('volcanic hammer', 'Volcanic Hammer deals 3 damage to target creature or player.').
card_mana_cost('volcanic hammer', ['1', 'R']).
card_cmc('volcanic hammer', 2).
card_layout('volcanic hammer', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEB, ME3, ME4, VMA
card_name('volcanic island', 'Volcanic Island').
card_type('volcanic island', 'Land — Island Mountain').
card_types('volcanic island', ['Land']).
card_subtypes('volcanic island', ['Island', 'Mountain']).
card_colors('volcanic island', []).
card_text('volcanic island', '({T}: Add {U} or {R} to your mana pool.)').
card_layout('volcanic island', 'normal').
card_reserved('volcanic island').

% Found in: C14
card_name('volcanic offering', 'Volcanic Offering').
card_type('volcanic offering', 'Instant').
card_types('volcanic offering', ['Instant']).
card_subtypes('volcanic offering', []).
card_colors('volcanic offering', ['R']).
card_text('volcanic offering', 'Destroy target nonbasic land you don\'t control and target nonbasic land of an opponent\'s choice you don\'t control.\nVolcanic Offering deals 7 damage to target creature you don\'t control and 7 damage to target creature of an opponent\'s choice you don\'t control.').
card_mana_cost('volcanic offering', ['4', 'R']).
card_cmc('volcanic offering', 5).
card_layout('volcanic offering', 'normal').

% Found in: ORI
card_name('volcanic rambler', 'Volcanic Rambler').
card_type('volcanic rambler', 'Creature — Elemental').
card_types('volcanic rambler', ['Creature']).
card_subtypes('volcanic rambler', ['Elemental']).
card_colors('volcanic rambler', ['R']).
card_text('volcanic rambler', '{2}{R}: Volcanic Rambler deals 1 damage to target player.').
card_mana_cost('volcanic rambler', ['5', 'R']).
card_cmc('volcanic rambler', 6).
card_layout('volcanic rambler', 'normal').
card_power('volcanic rambler', 6).
card_toughness('volcanic rambler', 4).

% Found in: DTK
card_name('volcanic rush', 'Volcanic Rush').
card_type('volcanic rush', 'Instant').
card_types('volcanic rush', ['Instant']).
card_subtypes('volcanic rush', []).
card_colors('volcanic rush', ['R']).
card_text('volcanic rush', 'Attacking creatures get +2/+0 and gain trample until end of turn.').
card_mana_cost('volcanic rush', ['4', 'R']).
card_cmc('volcanic rush', 5).
card_layout('volcanic rush', 'normal').

% Found in: ODY
card_name('volcanic spray', 'Volcanic Spray').
card_type('volcanic spray', 'Sorcery').
card_types('volcanic spray', ['Sorcery']).
card_subtypes('volcanic spray', []).
card_colors('volcanic spray', ['R']).
card_text('volcanic spray', 'Volcanic Spray deals 1 damage to each creature without flying and each player.\nFlashback {1}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('volcanic spray', ['1', 'R']).
card_cmc('volcanic spray', 2).
card_layout('volcanic spray', 'normal').

% Found in: M11, M13
card_name('volcanic strength', 'Volcanic Strength').
card_type('volcanic strength', 'Enchantment — Aura').
card_types('volcanic strength', ['Enchantment']).
card_subtypes('volcanic strength', ['Aura']).
card_colors('volcanic strength', ['R']).
card_text('volcanic strength', 'Enchant creature\nEnchanted creature gets +2/+2 and has mountainwalk. (It can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('volcanic strength', ['1', 'R']).
card_cmc('volcanic strength', 2).
card_layout('volcanic strength', 'normal').

% Found in: ALA
card_name('volcanic submersion', 'Volcanic Submersion').
card_type('volcanic submersion', 'Sorcery').
card_types('volcanic submersion', ['Sorcery']).
card_subtypes('volcanic submersion', []).
card_colors('volcanic submersion', ['R']).
card_text('volcanic submersion', 'Destroy target artifact or land.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('volcanic submersion', ['4', 'R']).
card_cmc('volcanic submersion', 5).
card_layout('volcanic submersion', 'normal').

% Found in: BFZ
card_name('volcanic upheaval', 'Volcanic Upheaval').
card_type('volcanic upheaval', 'Instant').
card_types('volcanic upheaval', ['Instant']).
card_subtypes('volcanic upheaval', []).
card_colors('volcanic upheaval', ['R']).
card_text('volcanic upheaval', 'Destroy target land.').
card_mana_cost('volcanic upheaval', ['3', 'R']).
card_cmc('volcanic upheaval', 4).
card_layout('volcanic upheaval', 'normal').

% Found in: DTK
card_name('volcanic vision', 'Volcanic Vision').
card_type('volcanic vision', 'Sorcery').
card_types('volcanic vision', ['Sorcery']).
card_subtypes('volcanic vision', []).
card_colors('volcanic vision', ['R']).
card_text('volcanic vision', 'Return target instant or sorcery card from your graveyard to your hand. Volcanic Vision deals damage equal to that card\'s converted mana cost to each creature your opponents control. Exile Volcanic Vision.').
card_mana_cost('volcanic vision', ['5', 'R', 'R']).
card_cmc('volcanic vision', 7).
card_layout('volcanic vision', 'normal').

% Found in: MMQ
card_name('volcanic wind', 'Volcanic Wind').
card_type('volcanic wind', 'Sorcery').
card_types('volcanic wind', ['Sorcery']).
card_subtypes('volcanic wind', []).
card_colors('volcanic wind', ['R']).
card_text('volcanic wind', 'Volcanic Wind deals X damage divided as you choose among any number of target creatures, where X is the number of creatures on the battlefield as you cast Volcanic Wind.').
card_mana_cost('volcanic wind', ['4', 'R', 'R']).
card_cmc('volcanic wind', 6).
card_layout('volcanic wind', 'normal').

% Found in: PLC
card_name('volcano hellion', 'Volcano Hellion').
card_type('volcano hellion', 'Creature — Hellion').
card_types('volcano hellion', ['Creature']).
card_subtypes('volcano hellion', ['Hellion']).
card_colors('volcano hellion', ['R']).
card_text('volcano hellion', 'Volcano Hellion has echo {X}, where X is your life total. (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Volcano Hellion enters the battlefield, it deals an amount of damage of your choice to you and target creature. The damage can\'t be prevented.').
card_mana_cost('volcano hellion', ['2', 'R', 'R']).
card_cmc('volcano hellion', 4).
card_layout('volcano hellion', 'normal').
card_power('volcano hellion', 6).
card_toughness('volcano hellion', 5).

% Found in: PLS
card_name('volcano imp', 'Volcano Imp').
card_type('volcano imp', 'Creature — Imp').
card_types('volcano imp', ['Creature']).
card_subtypes('volcano imp', ['Imp']).
card_colors('volcano imp', ['B']).
card_text('volcano imp', 'Flying\n{1}{R}: Volcano Imp gains first strike until end of turn.').
card_mana_cost('volcano imp', ['3', 'B']).
card_cmc('volcano imp', 4).
card_layout('volcano imp', 'normal').
card_power('volcano imp', 2).
card_toughness('volcano imp', 2).

% Found in: SOM
card_name('volition reins', 'Volition Reins').
card_type('volition reins', 'Enchantment — Aura').
card_types('volition reins', ['Enchantment']).
card_subtypes('volition reins', ['Aura']).
card_colors('volition reins', ['U']).
card_text('volition reins', 'Enchant permanent\nWhen Volition Reins enters the battlefield, if enchanted permanent is tapped, untap it.\nYou control enchanted permanent.').
card_mana_cost('volition reins', ['3', 'U', 'U', 'U']).
card_cmc('volition reins', 6).
card_layout('volition reins', 'normal').

% Found in: DDI, ODY
card_name('volley of boulders', 'Volley of Boulders').
card_type('volley of boulders', 'Sorcery').
card_types('volley of boulders', ['Sorcery']).
card_subtypes('volley of boulders', []).
card_colors('volley of boulders', ['R']).
card_text('volley of boulders', 'Volley of Boulders deals 6 damage divided as you choose among any number of target creatures and/or players.\nFlashback {R}{R}{R}{R}{R}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('volley of boulders', ['8', 'R']).
card_cmc('volley of boulders', 9).
card_layout('volley of boulders', 'normal').

% Found in: VAN
card_name('volrath', 'Volrath').
card_type('volrath', 'Vanguard').
card_types('volrath', ['Vanguard']).
card_subtypes('volrath', []).
card_colors('volrath', []).
card_text('volrath', 'Whenever a creature you control is put into your graveyard from the battlefield, you may put it on top of your library.').
card_layout('volrath', 'vanguard').

% Found in: NMS
card_name('volrath the fallen', 'Volrath the Fallen').
card_type('volrath the fallen', 'Legendary Creature — Shapeshifter').
card_types('volrath the fallen', ['Creature']).
card_subtypes('volrath the fallen', ['Shapeshifter']).
card_supertypes('volrath the fallen', ['Legendary']).
card_colors('volrath the fallen', ['B']).
card_text('volrath the fallen', '{1}{B}, Discard a creature card: Volrath the Fallen gets +X/+X until end of turn, where X is the discarded card\'s converted mana cost.').
card_mana_cost('volrath the fallen', ['3', 'B', 'B', 'B']).
card_cmc('volrath the fallen', 6).
card_layout('volrath the fallen', 'normal').
card_power('volrath the fallen', 6).
card_toughness('volrath the fallen', 4).

% Found in: TMP, TPR
card_name('volrath\'s curse', 'Volrath\'s Curse').
card_type('volrath\'s curse', 'Enchantment — Aura').
card_types('volrath\'s curse', ['Enchantment']).
card_subtypes('volrath\'s curse', ['Aura']).
card_colors('volrath\'s curse', ['U']).
card_text('volrath\'s curse', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated. That creature\'s controller may sacrifice a permanent for that player to ignore this effect until end of turn.\n{1}{U}: Return Volrath\'s Curse to its owner\'s hand.').
card_mana_cost('volrath\'s curse', ['1', 'U']).
card_cmc('volrath\'s curse', 2).
card_layout('volrath\'s curse', 'normal').

% Found in: EXO
card_name('volrath\'s dungeon', 'Volrath\'s Dungeon').
card_type('volrath\'s dungeon', 'Enchantment').
card_types('volrath\'s dungeon', ['Enchantment']).
card_subtypes('volrath\'s dungeon', []).
card_colors('volrath\'s dungeon', ['B']).
card_text('volrath\'s dungeon', 'Pay 5 life: Destroy Volrath\'s Dungeon. Any player may activate this ability but only during his or her turn.\nDiscard a card: Target player puts a card from his or her hand on top of his or her library. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('volrath\'s dungeon', ['2', 'B', 'B']).
card_cmc('volrath\'s dungeon', 4).
card_layout('volrath\'s dungeon', 'normal').

% Found in: STH
card_name('volrath\'s gardens', 'Volrath\'s Gardens').
card_type('volrath\'s gardens', 'Enchantment').
card_types('volrath\'s gardens', ['Enchantment']).
card_subtypes('volrath\'s gardens', []).
card_colors('volrath\'s gardens', ['G']).
card_text('volrath\'s gardens', '{2}, Tap an untapped creature you control: You gain 2 life. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('volrath\'s gardens', ['1', 'G']).
card_cmc('volrath\'s gardens', 2).
card_layout('volrath\'s gardens', 'normal').

% Found in: STH, TPR
card_name('volrath\'s laboratory', 'Volrath\'s Laboratory').
card_type('volrath\'s laboratory', 'Artifact').
card_types('volrath\'s laboratory', ['Artifact']).
card_subtypes('volrath\'s laboratory', []).
card_colors('volrath\'s laboratory', []).
card_text('volrath\'s laboratory', 'As Volrath\'s Laboratory enters the battlefield, choose a color and a creature type.\n{5}, {T}: Put a 2/2 creature token of the chosen color and type onto the battlefield.').
card_mana_cost('volrath\'s laboratory', ['5']).
card_cmc('volrath\'s laboratory', 5).
card_layout('volrath\'s laboratory', 'normal').

% Found in: UGL
card_name('volrath\'s motion sensor', 'Volrath\'s Motion Sensor').
card_type('volrath\'s motion sensor', 'Enchant Player').
card_types('volrath\'s motion sensor', ['Enchant', 'Player']).
card_subtypes('volrath\'s motion sensor', []).
card_colors('volrath\'s motion sensor', ['B']).
card_text('volrath\'s motion sensor', 'When Volrath\'s Motion Sensor comes into play, choose target hand controlled by an opponent. Enchanted player balances Volrath\'s Motion Sensor on the back of that hand.\nIf Volrath\'s Motion Sensor falls off the hand, sacrifice Volrath\'s Motion Sensor and that player loses 3 life.').
card_mana_cost('volrath\'s motion sensor', ['B']).
card_cmc('volrath\'s motion sensor', 1).
card_layout('volrath\'s motion sensor', 'normal').

% Found in: STH, VMA
card_name('volrath\'s shapeshifter', 'Volrath\'s Shapeshifter').
card_type('volrath\'s shapeshifter', 'Creature — Shapeshifter').
card_types('volrath\'s shapeshifter', ['Creature']).
card_subtypes('volrath\'s shapeshifter', ['Shapeshifter']).
card_colors('volrath\'s shapeshifter', ['U']).
card_text('volrath\'s shapeshifter', 'As long as the top card of your graveyard is a creature card, Volrath\'s Shapeshifter has the full text of that card and has the text \"{2}: Discard a card.\" (Volrath\'s Shapeshifter has that card\'s name, mana cost, color, types, abilities, power, and toughness.)\n{2}: Discard a card.').
card_mana_cost('volrath\'s shapeshifter', ['1', 'U', 'U']).
card_cmc('volrath\'s shapeshifter', 3).
card_layout('volrath\'s shapeshifter', 'normal').
card_power('volrath\'s shapeshifter', 0).
card_toughness('volrath\'s shapeshifter', 1).
card_reserved('volrath\'s shapeshifter').

% Found in: STH, TPR
card_name('volrath\'s stronghold', 'Volrath\'s Stronghold').
card_type('volrath\'s stronghold', 'Legendary Land').
card_types('volrath\'s stronghold', ['Land']).
card_subtypes('volrath\'s stronghold', []).
card_supertypes('volrath\'s stronghold', ['Legendary']).
card_colors('volrath\'s stronghold', []).
card_text('volrath\'s stronghold', '{T}: Add {1} to your mana pool.\n{1}{B}, {T}: Put target creature card from your graveyard on top of your library.').
card_layout('volrath\'s stronghold', 'normal').
card_reserved('volrath\'s stronghold').

% Found in: DDL, NPH
card_name('volt charge', 'Volt Charge').
card_type('volt charge', 'Instant').
card_types('volt charge', ['Instant']).
card_subtypes('volt charge', []).
card_colors('volt charge', ['R']).
card_text('volt charge', 'Volt Charge deals 3 damage to target creature or player. Proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_mana_cost('volt charge', ['2', 'R']).
card_cmc('volt charge', 3).
card_layout('volt charge', 'normal').

% Found in: DST
card_name('voltaic construct', 'Voltaic Construct').
card_type('voltaic construct', 'Artifact Creature — Golem Construct').
card_types('voltaic construct', ['Artifact', 'Creature']).
card_subtypes('voltaic construct', ['Golem', 'Construct']).
card_colors('voltaic construct', []).
card_text('voltaic construct', '{2}: Untap target artifact creature.').
card_mana_cost('voltaic construct', ['4']).
card_cmc('voltaic construct', 4).
card_layout('voltaic construct', 'normal').
card_power('voltaic construct', 2).
card_toughness('voltaic construct', 2).

% Found in: DDE, M11, USG
card_name('voltaic key', 'Voltaic Key').
card_type('voltaic key', 'Artifact').
card_types('voltaic key', ['Artifact']).
card_subtypes('voltaic key', []).
card_colors('voltaic key', []).
card_text('voltaic key', '{1}, {T}: Untap target artifact.').
card_mana_cost('voltaic key', ['1']).
card_cmc('voltaic key', 1).
card_layout('voltaic key', 'normal').

% Found in: PO2, PTK
card_name('volunteer militia', 'Volunteer Militia').
card_type('volunteer militia', 'Creature — Human Soldier').
card_types('volunteer militia', ['Creature']).
card_subtypes('volunteer militia', ['Human', 'Soldier']).
card_colors('volunteer militia', ['W']).
card_text('volunteer militia', '').
card_mana_cost('volunteer militia', ['W']).
card_cmc('volunteer militia', 1).
card_layout('volunteer militia', 'normal').
card_power('volunteer militia', 1).
card_toughness('volunteer militia', 2).

% Found in: WTH
card_name('volunteer reserves', 'Volunteer Reserves').
card_type('volunteer reserves', 'Creature — Human Soldier').
card_types('volunteer reserves', ['Creature']).
card_subtypes('volunteer reserves', ['Human', 'Soldier']).
card_colors('volunteer reserves', ['W']).
card_text('volunteer reserves', 'Banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)\nCumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('volunteer reserves', ['1', 'W']).
card_cmc('volunteer reserves', 2).
card_layout('volunteer reserves', 'normal').
card_power('volunteer reserves', 2).
card_toughness('volunteer reserves', 4).

% Found in: CHR, LEG, ME3
card_name('voodoo doll', 'Voodoo Doll').
card_type('voodoo doll', 'Artifact').
card_types('voodoo doll', ['Artifact']).
card_subtypes('voodoo doll', []).
card_colors('voodoo doll', []).
card_text('voodoo doll', 'At the beginning of your upkeep, put a pin counter on Voodoo Doll.\nAt the beginning of your end step, if Voodoo Doll is untapped, destroy Voodoo Doll and it deals damage to you equal to the number of pin counters on it.\n{X}{X}, {T}: Voodoo Doll deals damage equal to the number of pin counters on it to target creature or player. X is the number of pin counters on Voodoo Doll.').
card_mana_cost('voodoo doll', ['6']).
card_cmc('voodoo doll', 6).
card_layout('voodoo doll', 'normal').

% Found in: INV
card_name('voracious cobra', 'Voracious Cobra').
card_type('voracious cobra', 'Creature — Snake').
card_types('voracious cobra', ['Creature']).
card_subtypes('voracious cobra', ['Snake']).
card_colors('voracious cobra', ['R', 'G']).
card_text('voracious cobra', 'First strike\nWhenever Voracious Cobra deals combat damage to a creature, destroy that creature.').
card_mana_cost('voracious cobra', ['2', 'R', 'G']).
card_cmc('voracious cobra', 4).
card_layout('voracious cobra', 'normal').
card_power('voracious cobra', 2).
card_toughness('voracious cobra', 2).

% Found in: CON, DDG
card_name('voracious dragon', 'Voracious Dragon').
card_type('voracious dragon', 'Creature — Dragon').
card_types('voracious dragon', ['Creature']).
card_subtypes('voracious dragon', ['Dragon']).
card_colors('voracious dragon', ['R']).
card_text('voracious dragon', 'Flying\nDevour 1 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with that many +1/+1 counters on it.)\nWhen Voracious Dragon enters the battlefield, it deals damage to target creature or player equal to twice the number of Goblins it devoured.').
card_mana_cost('voracious dragon', ['3', 'R', 'R']).
card_cmc('voracious dragon', 5).
card_layout('voracious dragon', 'normal').
card_power('voracious dragon', 4).
card_toughness('voracious dragon', 4).

% Found in: EVE
card_name('voracious hatchling', 'Voracious Hatchling').
card_type('voracious hatchling', 'Creature — Elemental').
card_types('voracious hatchling', ['Creature']).
card_subtypes('voracious hatchling', ['Elemental']).
card_colors('voracious hatchling', ['W', 'B']).
card_text('voracious hatchling', 'Lifelink\nVoracious Hatchling enters the battlefield with four -1/-1 counters on it.\nWhenever you cast a white spell, remove a -1/-1 counter from Voracious Hatchling.\nWhenever you cast a black spell, remove a -1/-1 counter from Voracious Hatchling.').
card_mana_cost('voracious hatchling', ['3', 'W/B']).
card_cmc('voracious hatchling', 4).
card_layout('voracious hatchling', 'normal').
card_power('voracious hatchling', 6).
card_toughness('voracious hatchling', 6).

% Found in: BFZ
card_name('voracious null', 'Voracious Null').
card_type('voracious null', 'Creature — Zombie').
card_types('voracious null', ['Creature']).
card_subtypes('voracious null', ['Zombie']).
card_colors('voracious null', ['B']).
card_text('voracious null', '{1}{B}, Sacrifice another creature: Put two +1/+1 counters on Voracious Null. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('voracious null', ['2', 'B']).
card_cmc('voracious null', 3).
card_layout('voracious null', 'normal').
card_power('voracious null', 2).
card_toughness('voracious null', 2).

% Found in: M14
card_name('voracious wurm', 'Voracious Wurm').
card_type('voracious wurm', 'Creature — Wurm').
card_types('voracious wurm', ['Creature']).
card_subtypes('voracious wurm', ['Wurm']).
card_colors('voracious wurm', ['G']).
card_text('voracious wurm', 'Voracious Wurm enters the battlefield with X +1/+1 counters on it, where X is the amount of life you\'ve gained this turn.').
card_mana_cost('voracious wurm', ['1', 'G']).
card_cmc('voracious wurm', 2).
card_layout('voracious wurm', 'normal').
card_power('voracious wurm', 2).
card_toughness('voracious wurm', 2).

% Found in: DKA
card_name('vorapede', 'Vorapede').
card_type('vorapede', 'Creature — Insect').
card_types('vorapede', ['Creature']).
card_subtypes('vorapede', ['Insect']).
card_colors('vorapede', ['G']).
card_text('vorapede', 'Vigilance, trample\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('vorapede', ['2', 'G', 'G', 'G']).
card_cmc('vorapede', 5).
card_layout('vorapede', 'normal').
card_power('vorapede', 5).
card_toughness('vorapede', 4).

% Found in: DGM
card_name('vorel of the hull clade', 'Vorel of the Hull Clade').
card_type('vorel of the hull clade', 'Legendary Creature — Human Merfolk').
card_types('vorel of the hull clade', ['Creature']).
card_subtypes('vorel of the hull clade', ['Human', 'Merfolk']).
card_supertypes('vorel of the hull clade', ['Legendary']).
card_colors('vorel of the hull clade', ['U', 'G']).
card_text('vorel of the hull clade', '{G}{U}, {T}: For each counter on target artifact, creature, or land, put another of those counters on that permanent.').
card_mana_cost('vorel of the hull clade', ['1', 'G', 'U']).
card_cmc('vorel of the hull clade', 3).
card_layout('vorel of the hull clade', 'normal').
card_power('vorel of the hull clade', 1).
card_toughness('vorel of the hull clade', 4).

% Found in: NPH
card_name('vorinclex, voice of hunger', 'Vorinclex, Voice of Hunger').
card_type('vorinclex, voice of hunger', 'Legendary Creature — Praetor').
card_types('vorinclex, voice of hunger', ['Creature']).
card_subtypes('vorinclex, voice of hunger', ['Praetor']).
card_supertypes('vorinclex, voice of hunger', ['Legendary']).
card_colors('vorinclex, voice of hunger', ['G']).
card_text('vorinclex, voice of hunger', 'Trample\nWhenever you tap a land for mana, add one mana to your mana pool of any type that land produced.\nWhenever an opponent taps a land for mana, that land doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('vorinclex, voice of hunger', ['6', 'G', 'G']).
card_cmc('vorinclex, voice of hunger', 8).
card_layout('vorinclex, voice of hunger', 'normal').
card_power('vorinclex, voice of hunger', 7).
card_toughness('vorinclex, voice of hunger', 6).

% Found in: CMD, PLC
card_name('vorosh, the hunter', 'Vorosh, the Hunter').
card_type('vorosh, the hunter', 'Legendary Creature — Dragon').
card_types('vorosh, the hunter', ['Creature']).
card_subtypes('vorosh, the hunter', ['Dragon']).
card_supertypes('vorosh, the hunter', ['Legendary']).
card_colors('vorosh, the hunter', ['U', 'B', 'G']).
card_text('vorosh, the hunter', 'Flying\nWhenever Vorosh, the Hunter deals combat damage to a player, you may pay {2}{G}. If you do, put six +1/+1 counters on Vorosh.').
card_mana_cost('vorosh, the hunter', ['3', 'G', 'U', 'B']).
card_cmc('vorosh, the hunter', 6).
card_layout('vorosh, the hunter', 'normal').
card_power('vorosh, the hunter', 6).
card_toughness('vorosh, the hunter', 6).

% Found in: MRD
card_name('vorrac battlehorns', 'Vorrac Battlehorns').
card_type('vorrac battlehorns', 'Artifact — Equipment').
card_types('vorrac battlehorns', ['Artifact']).
card_subtypes('vorrac battlehorns', ['Equipment']).
card_colors('vorrac battlehorns', []).
card_text('vorrac battlehorns', 'Equipped creature has trample and can\'t be blocked by more than one creature.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery. This card enters the battlefield unattached and stays on the battlefield if the creature leaves.)').
card_mana_cost('vorrac battlehorns', ['2']).
card_cmc('vorrac battlehorns', 2).
card_layout('vorrac battlehorns', 'normal').

% Found in: AVR
card_name('vorstclaw', 'Vorstclaw').
card_type('vorstclaw', 'Creature — Elemental Horror').
card_types('vorstclaw', ['Creature']).
card_subtypes('vorstclaw', ['Elemental', 'Horror']).
card_colors('vorstclaw', ['G']).
card_text('vorstclaw', '').
card_mana_cost('vorstclaw', ['4', 'G', 'G']).
card_cmc('vorstclaw', 6).
card_layout('vorstclaw', 'normal').
card_power('vorstclaw', 7).
card_toughness('vorstclaw', 7).

% Found in: BNG
card_name('vortex elemental', 'Vortex Elemental').
card_type('vortex elemental', 'Creature — Elemental').
card_types('vortex elemental', ['Creature']).
card_subtypes('vortex elemental', ['Elemental']).
card_colors('vortex elemental', ['U']).
card_text('vortex elemental', '{U}: Put Vortex Elemental and each creature blocking or blocked by it on top of their owners\' libraries, then those players shuffle their libraries.\n{3}{U}{U}: Target creature blocks Vortex Elemental this turn if able.').
card_mana_cost('vortex elemental', ['U']).
card_cmc('vortex elemental', 1).
card_layout('vortex elemental', 'normal').
card_power('vortex elemental', 0).
card_toughness('vortex elemental', 1).

% Found in: RAV
card_name('votary of the conclave', 'Votary of the Conclave').
card_type('votary of the conclave', 'Creature — Human Soldier').
card_types('votary of the conclave', ['Creature']).
card_subtypes('votary of the conclave', ['Human', 'Soldier']).
card_colors('votary of the conclave', ['W']).
card_text('votary of the conclave', '{2}{G}: Regenerate Votary of the Conclave.').
card_mana_cost('votary of the conclave', ['W']).
card_cmc('votary of the conclave', 1).
card_layout('votary of the conclave', 'normal').
card_power('votary of the conclave', 1).
card_toughness('votary of the conclave', 1).

% Found in: CMD, CNS
card_name('vow of duty', 'Vow of Duty').
card_type('vow of duty', 'Enchantment — Aura').
card_types('vow of duty', ['Enchantment']).
card_subtypes('vow of duty', ['Aura']).
card_colors('vow of duty', ['W']).
card_text('vow of duty', 'Enchant creature\nEnchanted creature gets +2/+2, has vigilance, and can\'t attack you or a planeswalker you control.').
card_mana_cost('vow of duty', ['2', 'W']).
card_cmc('vow of duty', 3).
card_layout('vow of duty', 'normal').

% Found in: CMD
card_name('vow of flight', 'Vow of Flight').
card_type('vow of flight', 'Enchantment — Aura').
card_types('vow of flight', ['Enchantment']).
card_subtypes('vow of flight', ['Aura']).
card_colors('vow of flight', ['U']).
card_text('vow of flight', 'Enchant creature\nEnchanted creature gets +2/+2, has flying, and can\'t attack you or a planeswalker you control.').
card_mana_cost('vow of flight', ['2', 'U']).
card_cmc('vow of flight', 3).
card_layout('vow of flight', 'normal').

% Found in: CMD
card_name('vow of lightning', 'Vow of Lightning').
card_type('vow of lightning', 'Enchantment — Aura').
card_types('vow of lightning', ['Enchantment']).
card_subtypes('vow of lightning', ['Aura']).
card_colors('vow of lightning', ['R']).
card_text('vow of lightning', 'Enchant creature\nEnchanted creature gets +2/+2, has first strike, and can\'t attack you or a planeswalker you control.').
card_mana_cost('vow of lightning', ['2', 'R']).
card_cmc('vow of lightning', 3).
card_layout('vow of lightning', 'normal').

% Found in: CMD
card_name('vow of malice', 'Vow of Malice').
card_type('vow of malice', 'Enchantment — Aura').
card_types('vow of malice', ['Enchantment']).
card_subtypes('vow of malice', ['Aura']).
card_colors('vow of malice', ['B']).
card_text('vow of malice', 'Enchant creature\nEnchanted creature gets +2/+2, has intimidate, and can\'t attack you or a planeswalker you control. (A creature with intimidate can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_mana_cost('vow of malice', ['2', 'B']).
card_cmc('vow of malice', 3).
card_layout('vow of malice', 'normal').

% Found in: CMD
card_name('vow of wildness', 'Vow of Wildness').
card_type('vow of wildness', 'Enchantment — Aura').
card_types('vow of wildness', ['Enchantment']).
card_subtypes('vow of wildness', ['Aura']).
card_colors('vow of wildness', ['G']).
card_text('vow of wildness', 'Enchant creature\nEnchanted creature gets +3/+3, has trample, and can\'t attack you or a planeswalker you control.').
card_mana_cost('vow of wildness', ['2', 'G']).
card_cmc('vow of wildness', 3).
card_layout('vow of wildness', 'normal').

% Found in: THS
card_name('voyage\'s end', 'Voyage\'s End').
card_type('voyage\'s end', 'Instant').
card_types('voyage\'s end', ['Instant']).
card_subtypes('voyage\'s end', []).
card_colors('voyage\'s end', ['U']).
card_text('voyage\'s end', 'Return target creature to its owner\'s hand. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('voyage\'s end', ['1', 'U']).
card_cmc('voyage\'s end', 2).
card_layout('voyage\'s end', 'normal').

% Found in: WWK
card_name('voyager drake', 'Voyager Drake').
card_type('voyager drake', 'Creature — Drake').
card_types('voyager drake', ['Creature']).
card_subtypes('voyager drake', ['Drake']).
card_colors('voyager drake', ['U']).
card_text('voyager drake', 'Multikicker {U} (You may pay an additional {U} any number of times as you cast this spell.)\nFlying\nWhen Voyager Drake enters the battlefield, up to X target creatures gain flying until end of turn, where X is the number of times Voyager Drake was kicked.').
card_mana_cost('voyager drake', ['3', 'U']).
card_cmc('voyager drake', 4).
card_layout('voyager drake', 'normal').
card_power('voyager drake', 3).
card_toughness('voyager drake', 3).

% Found in: RAV
card_name('voyager staff', 'Voyager Staff').
card_type('voyager staff', 'Artifact').
card_types('voyager staff', ['Artifact']).
card_subtypes('voyager staff', []).
card_colors('voyager staff', []).
card_text('voyager staff', '{2}, Sacrifice Voyager Staff: Exile target creature. Return the exiled card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_mana_cost('voyager staff', ['1']).
card_cmc('voyager staff', 1).
card_layout('voyager staff', 'normal').

% Found in: THS
card_name('voyaging satyr', 'Voyaging Satyr').
card_type('voyaging satyr', 'Creature — Satyr Druid').
card_types('voyaging satyr', ['Creature']).
card_subtypes('voyaging satyr', ['Satyr', 'Druid']).
card_colors('voyaging satyr', ['G']).
card_text('voyaging satyr', '{T}: Untap target land.').
card_mana_cost('voyaging satyr', ['1', 'G']).
card_cmc('voyaging satyr', 2).
card_layout('voyaging satyr', 'normal').
card_power('voyaging satyr', 1).
card_toughness('voyaging satyr', 2).

% Found in: DDM, RTR
card_name('vraska the unseen', 'Vraska the Unseen').
card_type('vraska the unseen', 'Planeswalker — Vraska').
card_types('vraska the unseen', ['Planeswalker']).
card_subtypes('vraska the unseen', ['Vraska']).
card_colors('vraska the unseen', ['B', 'G']).
card_text('vraska the unseen', '+1: Until your next turn, whenever a creature deals combat damage to Vraska the Unseen, destroy that creature.\n−3: Destroy target nonland permanent.\n−7: Put three 1/1 black Assassin creature tokens onto the battlefield with \"Whenever this creature deals combat damage to a player, that player loses the game.\"').
card_mana_cost('vraska the unseen', ['3', 'B', 'G']).
card_cmc('vraska the unseen', 5).
card_layout('vraska the unseen', 'normal').
card_loyalty('vraska the unseen', 5).

% Found in: ORI
card_name('vryn wingmare', 'Vryn Wingmare').
card_type('vryn wingmare', 'Creature — Pegasus').
card_types('vryn wingmare', ['Creature']).
card_subtypes('vryn wingmare', ['Pegasus']).
card_colors('vryn wingmare', ['W']).
card_text('vryn wingmare', 'Flying\nNoncreature spells cost {1} more to cast.').
card_mana_cost('vryn wingmare', ['2', 'W']).
card_cmc('vryn wingmare', 3).
card_layout('vryn wingmare', 'normal').
card_power('vryn wingmare', 2).
card_toughness('vryn wingmare', 1).

% Found in: USG
card_name('vug lizard', 'Vug Lizard').
card_type('vug lizard', 'Creature — Lizard').
card_types('vug lizard', ['Creature']).
card_subtypes('vug lizard', ['Lizard']).
card_colors('vug lizard', ['R']).
card_text('vug lizard', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)\nEcho {1}{R}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_mana_cost('vug lizard', ['1', 'R', 'R']).
card_cmc('vug lizard', 3).
card_layout('vug lizard', 'normal').
card_power('vug lizard', 3).
card_toughness('vug lizard', 4).

% Found in: THS
card_name('vulpine goliath', 'Vulpine Goliath').
card_type('vulpine goliath', 'Creature — Fox').
card_types('vulpine goliath', ['Creature']).
card_subtypes('vulpine goliath', ['Fox']).
card_colors('vulpine goliath', ['G']).
card_text('vulpine goliath', 'Trample').
card_mana_cost('vulpine goliath', ['4', 'G', 'G']).
card_cmc('vulpine goliath', 6).
card_layout('vulpine goliath', 'normal').
card_power('vulpine goliath', 6).
card_toughness('vulpine goliath', 5).

% Found in: DDI, MRD
card_name('vulshok battlegear', 'Vulshok Battlegear').
card_type('vulshok battlegear', 'Artifact — Equipment').
card_types('vulshok battlegear', ['Artifact']).
card_subtypes('vulshok battlegear', ['Equipment']).
card_colors('vulshok battlegear', []).
card_text('vulshok battlegear', 'Equipped creature gets +3/+3.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery. This card enters the battlefield unattached and stays on the battlefield if the creature leaves.)').
card_mana_cost('vulshok battlegear', ['3']).
card_cmc('vulshok battlegear', 3).
card_layout('vulshok battlegear', 'normal').

% Found in: MRD
card_name('vulshok battlemaster', 'Vulshok Battlemaster').
card_type('vulshok battlemaster', 'Creature — Human Warrior').
card_types('vulshok battlemaster', ['Creature']).
card_subtypes('vulshok battlemaster', ['Human', 'Warrior']).
card_colors('vulshok battlemaster', ['R']).
card_text('vulshok battlemaster', 'Haste\nWhen Vulshok Battlemaster enters the battlefield, attach all Equipment on the battlefield to it. (Control of the Equipment doesn\'t change.)').
card_mana_cost('vulshok battlemaster', ['4', 'R']).
card_cmc('vulshok battlemaster', 5).
card_layout('vulshok battlemaster', 'normal').
card_power('vulshok battlemaster', 2).
card_toughness('vulshok battlemaster', 2).

% Found in: DDI, M11, MRD
card_name('vulshok berserker', 'Vulshok Berserker').
card_type('vulshok berserker', 'Creature — Human Berserker').
card_types('vulshok berserker', ['Creature']).
card_subtypes('vulshok berserker', ['Human', 'Berserker']).
card_colors('vulshok berserker', ['R']).
card_text('vulshok berserker', 'Haste (This creature can attack and {T} as soon as it comes under your control.)').
card_mana_cost('vulshok berserker', ['3', 'R']).
card_cmc('vulshok berserker', 4).
card_layout('vulshok berserker', 'normal').
card_power('vulshok berserker', 3).
card_toughness('vulshok berserker', 2).

% Found in: MRD
card_name('vulshok gauntlets', 'Vulshok Gauntlets').
card_type('vulshok gauntlets', 'Artifact — Equipment').
card_types('vulshok gauntlets', ['Artifact']).
card_subtypes('vulshok gauntlets', ['Equipment']).
card_colors('vulshok gauntlets', []).
card_text('vulshok gauntlets', 'Equipped creature gets +4/+2 and doesn\'t untap during its controller\'s untap step.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery. This card enters the battlefield unattached and stays on the battlefield if the creature leaves.)').
card_mana_cost('vulshok gauntlets', ['2']).
card_cmc('vulshok gauntlets', 2).
card_layout('vulshok gauntlets', 'normal').

% Found in: SOM
card_name('vulshok heartstoker', 'Vulshok Heartstoker').
card_type('vulshok heartstoker', 'Creature — Human Shaman').
card_types('vulshok heartstoker', ['Creature']).
card_subtypes('vulshok heartstoker', ['Human', 'Shaman']).
card_colors('vulshok heartstoker', ['R']).
card_text('vulshok heartstoker', 'When Vulshok Heartstoker enters the battlefield, target creature gets +2/+0 until end of turn.').
card_mana_cost('vulshok heartstoker', ['2', 'R']).
card_cmc('vulshok heartstoker', 3).
card_layout('vulshok heartstoker', 'normal').
card_power('vulshok heartstoker', 2).
card_toughness('vulshok heartstoker', 2).

% Found in: 9ED, DDI, DST
card_name('vulshok morningstar', 'Vulshok Morningstar').
card_type('vulshok morningstar', 'Artifact — Equipment').
card_types('vulshok morningstar', ['Artifact']).
card_subtypes('vulshok morningstar', ['Equipment']).
card_colors('vulshok morningstar', []).
card_text('vulshok morningstar', 'Equipped creature gets +2/+2.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('vulshok morningstar', ['2']).
card_cmc('vulshok morningstar', 2).
card_layout('vulshok morningstar', 'normal').

% Found in: NPH
card_name('vulshok refugee', 'Vulshok Refugee').
card_type('vulshok refugee', 'Creature — Human Warrior').
card_types('vulshok refugee', ['Creature']).
card_subtypes('vulshok refugee', ['Human', 'Warrior']).
card_colors('vulshok refugee', ['R']).
card_text('vulshok refugee', 'Protection from red').
card_mana_cost('vulshok refugee', ['1', 'R', 'R']).
card_cmc('vulshok refugee', 3).
card_layout('vulshok refugee', 'normal').
card_power('vulshok refugee', 3).
card_toughness('vulshok refugee', 2).

% Found in: SOM
card_name('vulshok replica', 'Vulshok Replica').
card_type('vulshok replica', 'Artifact Creature — Berserker').
card_types('vulshok replica', ['Artifact', 'Creature']).
card_subtypes('vulshok replica', ['Berserker']).
card_colors('vulshok replica', []).
card_text('vulshok replica', '{1}{R}, Sacrifice Vulshok Replica: Vulshok Replica deals 3 damage to target player.').
card_mana_cost('vulshok replica', ['3']).
card_cmc('vulshok replica', 3).
card_layout('vulshok replica', 'normal').
card_power('vulshok replica', 3).
card_toughness('vulshok replica', 1).

% Found in: 5DN, DDI, PD2
card_name('vulshok sorcerer', 'Vulshok Sorcerer').
card_type('vulshok sorcerer', 'Creature — Human Shaman').
card_types('vulshok sorcerer', ['Creature']).
card_subtypes('vulshok sorcerer', ['Human', 'Shaman']).
card_colors('vulshok sorcerer', ['R']).
card_text('vulshok sorcerer', 'Haste\n{T}: Vulshok Sorcerer deals 1 damage to target creature or player.').
card_mana_cost('vulshok sorcerer', ['1', 'R', 'R']).
card_cmc('vulshok sorcerer', 3).
card_layout('vulshok sorcerer', 'normal').
card_power('vulshok sorcerer', 1).
card_toughness('vulshok sorcerer', 1).

% Found in: DST
card_name('vulshok war boar', 'Vulshok War Boar').
card_type('vulshok war boar', 'Creature — Boar Beast').
card_types('vulshok war boar', ['Creature']).
card_subtypes('vulshok war boar', ['Boar', 'Beast']).
card_colors('vulshok war boar', ['R']).
card_text('vulshok war boar', 'When Vulshok War Boar enters the battlefield, sacrifice it unless you sacrifice an artifact.').
card_mana_cost('vulshok war boar', ['2', 'R', 'R']).
card_cmc('vulshok war boar', 4).
card_layout('vulshok war boar', 'normal').
card_power('vulshok war boar', 5).
card_toughness('vulshok war boar', 5).

% Found in: DTK
card_name('vulturous aven', 'Vulturous Aven').
card_type('vulturous aven', 'Creature — Bird Shaman').
card_types('vulturous aven', ['Creature']).
card_subtypes('vulturous aven', ['Bird', 'Shaman']).
card_colors('vulturous aven', ['B']).
card_text('vulturous aven', 'Flying\nExploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Vulturous Aven exploits a creature, you draw two cards and you lose 2 life.').
card_mana_cost('vulturous aven', ['3', 'B']).
card_cmc('vulturous aven', 4).
card_layout('vulturous aven', 'normal').
card_power('vulturous aven', 2).
card_toughness('vulturous aven', 3).

% Found in: CMD, RAV
card_name('vulturous zombie', 'Vulturous Zombie').
card_type('vulturous zombie', 'Creature — Plant Zombie').
card_types('vulturous zombie', ['Creature']).
card_subtypes('vulturous zombie', ['Plant', 'Zombie']).
card_colors('vulturous zombie', ['B', 'G']).
card_text('vulturous zombie', 'Flying\nWhenever a card is put into an opponent\'s graveyard from anywhere, put a +1/+1 counter on Vulturous Zombie.').
card_mana_cost('vulturous zombie', ['3', 'B', 'G']).
card_cmc('vulturous zombie', 5).
card_layout('vulturous zombie', 'normal').
card_power('vulturous zombie', 3).
card_toughness('vulturous zombie', 3).

