% Card-specific data

% Found in: 5ED, 6ED, CHR, LEG, ME3
card_name('d\'avenant archer', 'D\'Avenant Archer').
card_type('d\'avenant archer', 'Creature — Human Soldier Archer').
card_types('d\'avenant archer', ['Creature']).
card_subtypes('d\'avenant archer', ['Human', 'Soldier', 'Archer']).
card_colors('d\'avenant archer', ['W']).
card_text('d\'avenant archer', '{T}: D\'Avenant Archer deals 1 damage to target attacking or blocking creature.').
card_mana_cost('d\'avenant archer', ['2', 'W']).
card_cmc('d\'avenant archer', 3).
card_layout('d\'avenant archer', 'normal').
card_power('d\'avenant archer', 1).
card_toughness('d\'avenant archer', 2).

% Found in: TSP
card_name('d\'avenant healer', 'D\'Avenant Healer').
card_type('d\'avenant healer', 'Creature — Human Cleric Archer').
card_types('d\'avenant healer', ['Creature']).
card_subtypes('d\'avenant healer', ['Human', 'Cleric', 'Archer']).
card_colors('d\'avenant healer', ['W']).
card_text('d\'avenant healer', '{T}: D\'Avenant Healer deals 1 damage to target attacking or blocking creature.\n{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_mana_cost('d\'avenant healer', ['1', 'W', 'W']).
card_cmc('d\'avenant healer', 3).
card_layout('d\'avenant healer', 'normal').
card_power('d\'avenant healer', 1).
card_toughness('d\'avenant healer', 2).

% Found in: CNS, VMA
card_name('dack fayden', 'Dack Fayden').
card_type('dack fayden', 'Planeswalker — Dack').
card_types('dack fayden', ['Planeswalker']).
card_subtypes('dack fayden', ['Dack']).
card_colors('dack fayden', ['U', 'R']).
card_text('dack fayden', '+1: Target player draws two cards, then discards two cards.\n−2: Gain control of target artifact.\n−6: You get an emblem with \"Whenever you cast a spell that targets one or more permanents, gain control of those permanents.\"').
card_mana_cost('dack fayden', ['1', 'U', 'R']).
card_cmc('dack fayden', 3).
card_layout('dack fayden', 'normal').
card_loyalty('dack fayden', 3).

% Found in: CNS, VMA
card_name('dack\'s duplicate', 'Dack\'s Duplicate').
card_type('dack\'s duplicate', 'Creature — Shapeshifter').
card_types('dack\'s duplicate', ['Creature']).
card_subtypes('dack\'s duplicate', ['Shapeshifter']).
card_colors('dack\'s duplicate', ['U', 'R']).
card_text('dack\'s duplicate', 'You may have Dack\'s Duplicate enter the battlefield as a copy of any creature on the battlefield except it gains haste and dethrone. (Whenever it attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)').
card_mana_cost('dack\'s duplicate', ['2', 'U', 'R']).
card_cmc('dack\'s duplicate', 4).
card_layout('dack\'s duplicate', 'normal').
card_power('dack\'s duplicate', 0).
card_toughness('dack\'s duplicate', 0).

% Found in: DDP, ROE
card_name('daggerback basilisk', 'Daggerback Basilisk').
card_type('daggerback basilisk', 'Creature — Basilisk').
card_types('daggerback basilisk', ['Creature']).
card_subtypes('daggerback basilisk', ['Basilisk']).
card_colors('daggerback basilisk', ['G']).
card_text('daggerback basilisk', 'Deathtouch').
card_mana_cost('daggerback basilisk', ['2', 'G']).
card_cmc('daggerback basilisk', 3).
card_layout('daggerback basilisk', 'normal').
card_power('daggerback basilisk', 2).
card_toughness('daggerback basilisk', 2).

% Found in: DD3_DVD, DDC, GPT, MM2
card_name('daggerclaw imp', 'Daggerclaw Imp').
card_type('daggerclaw imp', 'Creature — Imp').
card_types('daggerclaw imp', ['Creature']).
card_subtypes('daggerclaw imp', ['Imp']).
card_colors('daggerclaw imp', ['B']).
card_text('daggerclaw imp', 'Flying\nDaggerclaw Imp can\'t block.').
card_mana_cost('daggerclaw imp', ['2', 'B']).
card_cmc('daggerclaw imp', 3).
card_layout('daggerclaw imp', 'normal').
card_power('daggerclaw imp', 3).
card_toughness('daggerclaw imp', 1).

% Found in: RTR
card_name('daggerdrome imp', 'Daggerdrome Imp').
card_type('daggerdrome imp', 'Creature — Imp').
card_types('daggerdrome imp', ['Creature']).
card_subtypes('daggerdrome imp', ['Imp']).
card_colors('daggerdrome imp', ['B']).
card_text('daggerdrome imp', 'Flying\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('daggerdrome imp', ['1', 'B']).
card_cmc('daggerdrome imp', 2).
card_layout('daggerdrome imp', 'normal').
card_power('daggerdrome imp', 1).
card_toughness('daggerdrome imp', 1).

% Found in: FRF
card_name('daghatar the adamant', 'Daghatar the Adamant').
card_type('daghatar the adamant', 'Legendary Creature — Human Warrior').
card_types('daghatar the adamant', ['Creature']).
card_subtypes('daghatar the adamant', ['Human', 'Warrior']).
card_supertypes('daghatar the adamant', ['Legendary']).
card_colors('daghatar the adamant', ['W']).
card_text('daghatar the adamant', 'Vigilance\nDaghatar the Adamant enters the battlefield with four +1/+1 counters on it.\n{1}{B/G}{B/G}: Move a +1/+1 counter from target creature onto a second target creature.').
card_mana_cost('daghatar the adamant', ['3', 'W']).
card_cmc('daghatar the adamant', 4).
card_layout('daghatar the adamant', 'normal').
card_power('daghatar the adamant', 0).
card_toughness('daghatar the adamant', 0).

% Found in: DDL, MOR
card_name('daily regimen', 'Daily Regimen').
card_type('daily regimen', 'Enchantment — Aura').
card_types('daily regimen', ['Enchantment']).
card_subtypes('daily regimen', ['Aura']).
card_colors('daily regimen', ['W']).
card_text('daily regimen', 'Enchant creature\n{1}{W}: Put a +1/+1 counter on enchanted creature.').
card_mana_cost('daily regimen', ['W']).
card_cmc('daily regimen', 1).
card_layout('daily regimen', 'normal').

% Found in: CHR, LEG, MED
card_name('dakkon blackblade', 'Dakkon Blackblade').
card_type('dakkon blackblade', 'Legendary Creature — Human Warrior').
card_types('dakkon blackblade', ['Creature']).
card_subtypes('dakkon blackblade', ['Human', 'Warrior']).
card_supertypes('dakkon blackblade', ['Legendary']).
card_colors('dakkon blackblade', ['W', 'U', 'B']).
card_text('dakkon blackblade', 'Dakkon Blackblade\'s power and toughness are each equal to the number of lands you control.').
card_mana_cost('dakkon blackblade', ['2', 'W', 'U', 'U', 'B']).
card_cmc('dakkon blackblade', 6).
card_layout('dakkon blackblade', 'normal').
card_power('dakkon blackblade', '*').
card_toughness('dakkon blackblade', '*').

% Found in: VAN
card_name('dakkon blackblade avatar', 'Dakkon Blackblade Avatar').
card_type('dakkon blackblade avatar', 'Vanguard').
card_types('dakkon blackblade avatar', ['Vanguard']).
card_subtypes('dakkon blackblade avatar', []).
card_colors('dakkon blackblade avatar', []).
card_text('dakkon blackblade avatar', 'You may play any colored card from your hand as a copy of a basic land card chosen at random that can produce mana of one of the card\'s colors.').
card_layout('dakkon blackblade avatar', 'vanguard').

% Found in: PO2
card_name('dakmor bat', 'Dakmor Bat').
card_type('dakmor bat', 'Creature — Bat').
card_types('dakmor bat', ['Creature']).
card_subtypes('dakmor bat', ['Bat']).
card_colors('dakmor bat', ['B']).
card_text('dakmor bat', 'Flying').
card_mana_cost('dakmor bat', ['1', 'B']).
card_cmc('dakmor bat', 2).
card_layout('dakmor bat', 'normal').
card_power('dakmor bat', 1).
card_toughness('dakmor bat', 1).

% Found in: S99
card_name('dakmor ghoul', 'Dakmor Ghoul').
card_type('dakmor ghoul', 'Creature — Zombie').
card_types('dakmor ghoul', ['Creature']).
card_subtypes('dakmor ghoul', ['Zombie']).
card_colors('dakmor ghoul', ['B']).
card_text('dakmor ghoul', 'When Dakmor Ghoul enters the battlefield, target opponent loses 2 life and you gain 2 life.').
card_mana_cost('dakmor ghoul', ['2', 'B', 'B']).
card_cmc('dakmor ghoul', 4).
card_layout('dakmor ghoul', 'normal').
card_power('dakmor ghoul', 2).
card_toughness('dakmor ghoul', 2).

% Found in: 7ED, S99
card_name('dakmor lancer', 'Dakmor Lancer').
card_type('dakmor lancer', 'Creature — Human Knight').
card_types('dakmor lancer', ['Creature']).
card_subtypes('dakmor lancer', ['Human', 'Knight']).
card_colors('dakmor lancer', ['B']).
card_text('dakmor lancer', 'When Dakmor Lancer enters the battlefield, destroy target nonblack creature.').
card_mana_cost('dakmor lancer', ['4', 'B', 'B']).
card_cmc('dakmor lancer', 6).
card_layout('dakmor lancer', 'normal').
card_power('dakmor lancer', 3).
card_toughness('dakmor lancer', 3).

% Found in: ME4, PO2, S99
card_name('dakmor plague', 'Dakmor Plague').
card_type('dakmor plague', 'Sorcery').
card_types('dakmor plague', ['Sorcery']).
card_subtypes('dakmor plague', []).
card_colors('dakmor plague', ['B']).
card_text('dakmor plague', 'Dakmor Plague deals 3 damage to each creature and each player.').
card_mana_cost('dakmor plague', ['3', 'B', 'B']).
card_cmc('dakmor plague', 5).
card_layout('dakmor plague', 'normal').

% Found in: DDJ, FUT, MMA
card_name('dakmor salvage', 'Dakmor Salvage').
card_type('dakmor salvage', 'Land').
card_types('dakmor salvage', ['Land']).
card_subtypes('dakmor salvage', []).
card_colors('dakmor salvage', []).
card_text('dakmor salvage', 'Dakmor Salvage enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\nDredge 2 (If you would draw a card, instead you may put exactly two cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_layout('dakmor salvage', 'normal').

% Found in: PO2, S99
card_name('dakmor scorpion', 'Dakmor Scorpion').
card_type('dakmor scorpion', 'Creature — Scorpion').
card_types('dakmor scorpion', ['Creature']).
card_subtypes('dakmor scorpion', ['Scorpion']).
card_colors('dakmor scorpion', ['B']).
card_text('dakmor scorpion', '').
card_mana_cost('dakmor scorpion', ['1', 'B']).
card_cmc('dakmor scorpion', 2).
card_layout('dakmor scorpion', 'normal').
card_power('dakmor scorpion', 2).
card_toughness('dakmor scorpion', 1).

% Found in: PO2, S99
card_name('dakmor sorceress', 'Dakmor Sorceress').
card_type('dakmor sorceress', 'Creature — Human Wizard').
card_types('dakmor sorceress', ['Creature']).
card_subtypes('dakmor sorceress', ['Human', 'Wizard']).
card_colors('dakmor sorceress', ['B']).
card_text('dakmor sorceress', 'Dakmor Sorceress\'s power is equal to the number of Swamps you control.').
card_mana_cost('dakmor sorceress', ['5', 'B']).
card_cmc('dakmor sorceress', 6).
card_layout('dakmor sorceress', 'normal').
card_power('dakmor sorceress', '*').
card_toughness('dakmor sorceress', 4).

% Found in: JOU
card_name('dakra mystic', 'Dakra Mystic').
card_type('dakra mystic', 'Creature — Merfolk Wizard').
card_types('dakra mystic', ['Creature']).
card_subtypes('dakra mystic', ['Merfolk', 'Wizard']).
card_colors('dakra mystic', ['U']).
card_text('dakra mystic', '{U}, {T}: Each player reveals the top card of his or her library. You may put the revealed cards into their owners\' graveyards. If you don\'t, each player draws a card.').
card_mana_cost('dakra mystic', ['U']).
card_cmc('dakra mystic', 1).
card_layout('dakra mystic', 'normal').
card_power('dakra mystic', 1).
card_toughness('dakra mystic', 1).

% Found in: CMD
card_name('damia, sage of stone', 'Damia, Sage of Stone').
card_type('damia, sage of stone', 'Legendary Creature — Gorgon Wizard').
card_types('damia, sage of stone', ['Creature']).
card_subtypes('damia, sage of stone', ['Gorgon', 'Wizard']).
card_supertypes('damia, sage of stone', ['Legendary']).
card_colors('damia, sage of stone', ['U', 'B', 'G']).
card_text('damia, sage of stone', 'Deathtouch\nSkip your draw step.\nAt the beginning of your upkeep, if you have fewer than seven cards in hand, draw cards equal to the difference.').
card_mana_cost('damia, sage of stone', ['4', 'G', 'U', 'B']).
card_cmc('damia, sage of stone', 7).
card_layout('damia, sage of stone', 'normal').
card_power('damia, sage of stone', 4).
card_toughness('damia, sage of stone', 4).

% Found in: DTK
card_name('damnable pact', 'Damnable Pact').
card_type('damnable pact', 'Sorcery').
card_types('damnable pact', ['Sorcery']).
card_subtypes('damnable pact', []).
card_colors('damnable pact', ['B']).
card_text('damnable pact', 'Target player draws X cards and loses X life.').
card_mana_cost('damnable pact', ['X', 'B', 'B']).
card_cmc('damnable pact', 2).
card_layout('damnable pact', 'normal').

% Found in: PLC, pMPR
card_name('damnation', 'Damnation').
card_type('damnation', 'Sorcery').
card_types('damnation', ['Sorcery']).
card_subtypes('damnation', []).
card_colors('damnation', ['B']).
card_text('damnation', 'Destroy all creatures. They can\'t be regenerated.').
card_mana_cost('damnation', ['2', 'B', 'B']).
card_cmc('damnation', 4).
card_layout('damnation', 'normal').

% Found in: CHK, MMA
card_name('dampen thought', 'Dampen Thought').
card_type('dampen thought', 'Instant — Arcane').
card_types('dampen thought', ['Instant']).
card_subtypes('dampen thought', ['Arcane']).
card_colors('dampen thought', ['U']).
card_text('dampen thought', 'Target player puts the top four cards of his or her library into his or her graveyard.\nSplice onto Arcane {1}{U} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('dampen thought', ['1', 'U']).
card_cmc('dampen thought', 2).
card_layout('dampen thought', 'normal').

% Found in: BFZ
card_name('dampening pulse', 'Dampening Pulse').
card_type('dampening pulse', 'Enchantment').
card_types('dampening pulse', ['Enchantment']).
card_subtypes('dampening pulse', []).
card_colors('dampening pulse', ['U']).
card_text('dampening pulse', 'Creatures your opponents control get -1/-0.').
card_mana_cost('dampening pulse', ['3', 'U']).
card_cmc('dampening pulse', 4).
card_layout('dampening pulse', 'normal').

% Found in: ULG
card_name('damping engine', 'Damping Engine').
card_type('damping engine', 'Artifact').
card_types('damping engine', ['Artifact']).
card_subtypes('damping engine', []).
card_colors('damping engine', []).
card_text('damping engine', 'A player who controls more permanents than each other player can\'t play lands or cast artifact, creature, or enchantment spells. That player may sacrifice a permanent for that player to ignore this effect until end of turn.').
card_mana_cost('damping engine', ['4']).
card_cmc('damping engine', 4).
card_layout('damping engine', 'normal').

% Found in: ATQ
card_name('damping field', 'Damping Field').
card_type('damping field', 'Enchantment').
card_types('damping field', ['Enchantment']).
card_subtypes('damping field', []).
card_colors('damping field', ['W']).
card_text('damping field', 'Players can\'t untap more than one artifact during their untap steps.').
card_mana_cost('damping field', ['2', 'W']).
card_cmc('damping field', 3).
card_layout('damping field', 'normal').
card_reserved('damping field').

% Found in: MRD
card_name('damping matrix', 'Damping Matrix').
card_type('damping matrix', 'Artifact').
card_types('damping matrix', ['Artifact']).
card_subtypes('damping matrix', []).
card_colors('damping matrix', []).
card_text('damping matrix', 'Activated abilities of artifacts and creatures can\'t be activated unless they\'re mana abilities.').
card_mana_cost('damping matrix', ['3']).
card_cmc('damping matrix', 3).
card_layout('damping matrix', 'normal').

% Found in: 5ED, CHR, DRK, ME3
card_name('dance of many', 'Dance of Many').
card_type('dance of many', 'Enchantment').
card_types('dance of many', ['Enchantment']).
card_subtypes('dance of many', []).
card_colors('dance of many', ['U']).
card_text('dance of many', 'When Dance of Many enters the battlefield, put a token that\'s a copy of target nontoken creature onto the battlefield.\nWhen Dance of Many leaves the battlefield, exile the token.\nWhen the token leaves the battlefield, sacrifice Dance of Many.\nAt the beginning of your upkeep, sacrifice Dance of Many unless you pay {U}{U}.').
card_mana_cost('dance of many', ['U', 'U']).
card_cmc('dance of many', 2).
card_layout('dance of many', 'normal').

% Found in: CHK
card_name('dance of shadows', 'Dance of Shadows').
card_type('dance of shadows', 'Sorcery — Arcane').
card_types('dance of shadows', ['Sorcery']).
card_subtypes('dance of shadows', ['Arcane']).
card_colors('dance of shadows', ['B']).
card_text('dance of shadows', 'Creatures you control get +1/+0 and gain fear until end of turn. (They can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('dance of shadows', ['3', 'B', 'B']).
card_cmc('dance of shadows', 5).
card_layout('dance of shadows', 'normal').

% Found in: ICE, ME2
card_name('dance of the dead', 'Dance of the Dead').
card_type('dance of the dead', 'Enchantment — Aura').
card_types('dance of the dead', ['Enchantment']).
card_subtypes('dance of the dead', ['Aura']).
card_colors('dance of the dead', ['B']).
card_text('dance of the dead', 'Enchant creature card in a graveyard\nWhen Dance of the Dead enters the battlefield, if it\'s on the battlefield, it loses \"enchant creature card in a graveyard\" and gains \"enchant creature put onto the battlefield with Dance of the Dead.\" Put enchanted creature card onto the battlefield tapped under your control and attach Dance of the Dead to it. When Dance of the Dead leaves the battlefield, that creature\'s controller sacrifices it.\nEnchanted creature gets +1/+1 and doesn\'t untap during its controller\'s untap step.\nAt the beginning of the upkeep of enchanted creature\'s controller, that player may pay {1}{B}. If he or she does, untap that creature.').
card_mana_cost('dance of the dead', ['1', 'B']).
card_cmc('dance of the dead', 2).
card_layout('dance of the dead', 'normal').

% Found in: DTK
card_name('dance of the skywise', 'Dance of the Skywise').
card_type('dance of the skywise', 'Instant').
card_types('dance of the skywise', ['Instant']).
card_subtypes('dance of the skywise', []).
card_colors('dance of the skywise', ['U']).
card_text('dance of the skywise', 'Until end of turn, target creature you control becomes a blue Dragon Illusion with base power and toughness 4/4, loses all abilities, and gains flying.').
card_mana_cost('dance of the skywise', ['1', 'U']).
card_cmc('dance of the skywise', 2).
card_layout('dance of the skywise', 'normal').

% Found in: ARC
card_name('dance, pathetic marionette', 'Dance, Pathetic Marionette').
card_type('dance, pathetic marionette', 'Scheme').
card_types('dance, pathetic marionette', ['Scheme']).
card_subtypes('dance, pathetic marionette', []).
card_colors('dance, pathetic marionette', []).
card_text('dance, pathetic marionette', 'When you set this scheme in motion, each opponent reveals cards from the top of his or her library until he or she reveals a creature card. Choose one of the revealed creature cards and put it onto the battlefield under your control. Put all other cards revealed this way into their owners\' graveyards.').
card_layout('dance, pathetic marionette', 'scheme').

% Found in: 3ED, 4ED, 5ED, 6ED, 9ED, ARN
card_name('dancing scimitar', 'Dancing Scimitar').
card_type('dancing scimitar', 'Artifact Creature — Spirit').
card_types('dancing scimitar', ['Artifact', 'Creature']).
card_subtypes('dancing scimitar', ['Spirit']).
card_colors('dancing scimitar', []).
card_text('dancing scimitar', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_mana_cost('dancing scimitar', ['4']).
card_cmc('dancing scimitar', 4).
card_layout('dancing scimitar', 'normal').
card_power('dancing scimitar', 1).
card_toughness('dancing scimitar', 5).

% Found in: 5ED, ARN, CHR, TSB
card_name('dandân', 'Dandân').
card_type('dandân', 'Creature — Fish').
card_types('dandân', ['Creature']).
card_subtypes('dandân', ['Fish']).
card_colors('dandân', ['U']).
card_text('dandân', 'Dandân can\'t attack unless defending player controls an Island.\nWhen you control no Islands, sacrifice Dandân.').
card_mana_cost('dandân', ['U', 'U']).
card_cmc('dandân', 2).
card_layout('dandân', 'normal').
card_power('dandân', 4).
card_toughness('dandân', 1).

% Found in: DGM
card_name('dangerous', 'Dangerous').
card_type('dangerous', 'Sorcery').
card_types('dangerous', ['Sorcery']).
card_subtypes('dangerous', []).
card_colors('dangerous', ['G']).
card_text('dangerous', 'All creatures able to block target creature this turn do so.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('dangerous', ['3', 'G']).
card_cmc('dangerous', 4).
card_layout('dangerous', 'split').

% Found in: AVR
card_name('dangerous wager', 'Dangerous Wager').
card_type('dangerous wager', 'Instant').
card_types('dangerous wager', ['Instant']).
card_subtypes('dangerous wager', []).
card_colors('dangerous wager', ['R']).
card_text('dangerous wager', 'Discard your hand, then draw two cards.').
card_mana_cost('dangerous wager', ['1', 'R']).
card_cmc('dangerous wager', 2).
card_layout('dangerous wager', 'normal').

% Found in: 6ED, VIS
card_name('daraja griffin', 'Daraja Griffin').
card_type('daraja griffin', 'Creature — Griffin').
card_types('daraja griffin', ['Creature']).
card_subtypes('daraja griffin', ['Griffin']).
card_colors('daraja griffin', ['W']).
card_text('daraja griffin', 'Flying\nSacrifice Daraja Griffin: Destroy target black creature.').
card_mana_cost('daraja griffin', ['3', 'W']).
card_cmc('daraja griffin', 4).
card_layout('daraja griffin', 'normal').
card_power('daraja griffin', 2).
card_toughness('daraja griffin', 2).

% Found in: PCY
card_name('darba', 'Darba').
card_type('darba', 'Creature — Bird Beast').
card_types('darba', ['Creature']).
card_subtypes('darba', ['Bird', 'Beast']).
card_colors('darba', ['G']).
card_text('darba', 'At the beginning of your upkeep, sacrifice Darba unless you pay {G}{G}.').
card_mana_cost('darba', ['3', 'G']).
card_cmc('darba', 4).
card_layout('darba', 'normal').
card_power('darba', 5).
card_toughness('darba', 4).

% Found in: C14
card_name('daretti, scrap savant', 'Daretti, Scrap Savant').
card_type('daretti, scrap savant', 'Planeswalker — Daretti').
card_types('daretti, scrap savant', ['Planeswalker']).
card_subtypes('daretti, scrap savant', ['Daretti']).
card_colors('daretti, scrap savant', ['R']).
card_text('daretti, scrap savant', '+2: Discard up to two cards, then draw that many cards.\n−2: Sacrifice an artifact. If you do, return target artifact card from your graveyard to the battlefield.\n−10: You get an emblem with \"Whenever an artifact is put into your graveyard from the battlefield, return that card to the battlefield at the beginning of the next end step.\"\nDaretti, Scrap Savant can be your commander.').
card_mana_cost('daretti, scrap savant', ['3', 'R']).
card_cmc('daretti, scrap savant', 4).
card_layout('daretti, scrap savant', 'normal').
card_loyalty('daretti, scrap savant', 3).

% Found in: CSP
card_name('darien, king of kjeldor', 'Darien, King of Kjeldor').
card_type('darien, king of kjeldor', 'Legendary Creature — Human Soldier').
card_types('darien, king of kjeldor', ['Creature']).
card_subtypes('darien, king of kjeldor', ['Human', 'Soldier']).
card_supertypes('darien, king of kjeldor', ['Legendary']).
card_colors('darien, king of kjeldor', ['W']).
card_text('darien, king of kjeldor', 'Whenever you\'re dealt damage, you may put that many 1/1 white Soldier creature tokens onto the battlefield.').
card_mana_cost('darien, king of kjeldor', ['4', 'W', 'W']).
card_cmc('darien, king of kjeldor', 6).
card_layout('darien, king of kjeldor', 'normal').
card_power('darien, king of kjeldor', 3).
card_toughness('darien, king of kjeldor', 3).

% Found in: INV
card_name('darigaaz\'s attendant', 'Darigaaz\'s Attendant').
card_type('darigaaz\'s attendant', 'Artifact Creature — Golem').
card_types('darigaaz\'s attendant', ['Artifact', 'Creature']).
card_subtypes('darigaaz\'s attendant', ['Golem']).
card_colors('darigaaz\'s attendant', []).
card_text('darigaaz\'s attendant', '{1}, Sacrifice Darigaaz\'s Attendant: Add {B}{R}{G} to your mana pool.').
card_mana_cost('darigaaz\'s attendant', ['5']).
card_cmc('darigaaz\'s attendant', 5).
card_layout('darigaaz\'s attendant', 'normal').
card_power('darigaaz\'s attendant', 3).
card_toughness('darigaaz\'s attendant', 3).

% Found in: PLS
card_name('darigaaz\'s caldera', 'Darigaaz\'s Caldera').
card_type('darigaaz\'s caldera', 'Land — Lair').
card_types('darigaaz\'s caldera', ['Land']).
card_subtypes('darigaaz\'s caldera', ['Lair']).
card_colors('darigaaz\'s caldera', []).
card_text('darigaaz\'s caldera', 'When Darigaaz\'s Caldera enters the battlefield, sacrifice it unless you return a non-Lair land you control to its owner\'s hand.\n{T}: Add {B}, {R}, or {G} to your mana pool.').
card_layout('darigaaz\'s caldera', 'normal').

% Found in: DDE, PLS
card_name('darigaaz\'s charm', 'Darigaaz\'s Charm').
card_type('darigaaz\'s charm', 'Instant').
card_types('darigaaz\'s charm', ['Instant']).
card_subtypes('darigaaz\'s charm', []).
card_colors('darigaaz\'s charm', ['B', 'R', 'G']).
card_text('darigaaz\'s charm', 'Choose one —\n• Return target creature card from your graveyard to your hand.\n• Darigaaz\'s Charm deals 3 damage to target creature or player.\n• Target creature gets +3/+3 until end of turn.').
card_mana_cost('darigaaz\'s charm', ['B', 'R', 'G']).
card_cmc('darigaaz\'s charm', 3).
card_layout('darigaaz\'s charm', 'normal').

% Found in: DDE, INV
card_name('darigaaz, the igniter', 'Darigaaz, the Igniter').
card_type('darigaaz, the igniter', 'Legendary Creature — Dragon').
card_types('darigaaz, the igniter', ['Creature']).
card_subtypes('darigaaz, the igniter', ['Dragon']).
card_supertypes('darigaaz, the igniter', ['Legendary']).
card_colors('darigaaz, the igniter', ['B', 'R', 'G']).
card_text('darigaaz, the igniter', 'Flying\nWhenever Darigaaz, the Igniter deals combat damage to a player, you may pay {2}{R}. If you do, choose a color, then that player reveals his or her hand and Darigaaz deals damage to the player equal to the number of cards of that color revealed this way.').
card_mana_cost('darigaaz, the igniter', ['3', 'B', 'R', 'G']).
card_cmc('darigaaz, the igniter', 6).
card_layout('darigaaz, the igniter', 'normal').
card_power('darigaaz, the igniter', 6).
card_toughness('darigaaz, the igniter', 6).

% Found in: 6ED, 7ED, 8ED, 9ED, MIR
card_name('daring apprentice', 'Daring Apprentice').
card_type('daring apprentice', 'Creature — Human Wizard').
card_types('daring apprentice', ['Creature']).
card_subtypes('daring apprentice', ['Human', 'Wizard']).
card_colors('daring apprentice', ['U']).
card_text('daring apprentice', '{T}, Sacrifice Daring Apprentice: Counter target spell.').
card_mana_cost('daring apprentice', ['1', 'U', 'U']).
card_cmc('daring apprentice', 3).
card_layout('daring apprentice', 'normal').
card_power('daring apprentice', 1).
card_toughness('daring apprentice', 1).

% Found in: PLS
card_name('daring leap', 'Daring Leap').
card_type('daring leap', 'Instant').
card_types('daring leap', ['Instant']).
card_subtypes('daring leap', []).
card_colors('daring leap', ['W', 'U']).
card_text('daring leap', 'Target creature gets +1/+1 and gains flying and first strike until end of turn.').
card_mana_cost('daring leap', ['1', 'W', 'U']).
card_cmc('daring leap', 3).
card_layout('daring leap', 'normal').

% Found in: GTC
card_name('daring skyjek', 'Daring Skyjek').
card_type('daring skyjek', 'Creature — Human Knight').
card_types('daring skyjek', ['Creature']).
card_subtypes('daring skyjek', ['Human', 'Knight']).
card_colors('daring skyjek', ['W']).
card_text('daring skyjek', 'Battalion — Whenever Daring Skyjek and at least two other creatures attack, Daring Skyjek gains flying until end of turn.').
card_mana_cost('daring skyjek', ['1', 'W']).
card_cmc('daring skyjek', 2).
card_layout('daring skyjek', 'normal').
card_power('daring skyjek', 3).
card_toughness('daring skyjek', 1).

% Found in: JOU
card_name('daring thief', 'Daring Thief').
card_type('daring thief', 'Creature — Human Rogue').
card_types('daring thief', ['Creature']).
card_subtypes('daring thief', ['Human', 'Rogue']).
card_colors('daring thief', ['U']).
card_text('daring thief', 'Inspired — Whenever Daring Thief becomes untapped, you may exchange control of target nonland permanent you control and target permanent an opponent controls that shares a card type with it.').
card_mana_cost('daring thief', ['2', 'U']).
card_cmc('daring thief', 3).
card_layout('daring thief', 'normal').
card_power('daring thief', 2).
card_toughness('daring thief', 3).

% Found in: 7ED, 8ED, 9ED, CST, DD3_DVD, DDC, DKM, ICE, ME2, MIR, TMP, TPR
card_name('dark banishing', 'Dark Banishing').
card_type('dark banishing', 'Instant').
card_types('dark banishing', ['Instant']).
card_subtypes('dark banishing', []).
card_colors('dark banishing', ['B']).
card_text('dark banishing', 'Destroy target nonblack creature. It can\'t be regenerated.').
card_mana_cost('dark banishing', ['2', 'B']).
card_cmc('dark banishing', 3).
card_layout('dark banishing', 'normal').

% Found in: THS
card_name('dark betrayal', 'Dark Betrayal').
card_type('dark betrayal', 'Instant').
card_types('dark betrayal', ['Instant']).
card_subtypes('dark betrayal', []).
card_colors('dark betrayal', ['B']).
card_text('dark betrayal', 'Destroy target black creature.').
card_mana_cost('dark betrayal', ['B']).
card_cmc('dark betrayal', 1).
card_layout('dark betrayal', 'normal').

% Found in: MM2, MMA, RAV, pJGP
card_name('dark confidant', 'Dark Confidant').
card_type('dark confidant', 'Creature — Human Wizard').
card_types('dark confidant', ['Creature']).
card_subtypes('dark confidant', ['Human', 'Wizard']).
card_colors('dark confidant', ['B']).
card_text('dark confidant', 'At the beginning of your upkeep, reveal the top card of your library and put that card into your hand. You lose life equal to its converted mana cost.').
card_mana_cost('dark confidant', ['1', 'B']).
card_cmc('dark confidant', 2).
card_layout('dark confidant', 'normal').
card_power('dark confidant', 2).
card_toughness('dark confidant', 1).

% Found in: ORI
card_name('dark dabbling', 'Dark Dabbling').
card_type('dark dabbling', 'Instant').
card_types('dark dabbling', ['Instant']).
card_subtypes('dark dabbling', []).
card_colors('dark dabbling', ['B']).
card_text('dark dabbling', 'Regenerate target creature. Draw a card. (The next time the creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)\nSpell mastery — If there are two or more instant and/or sorcery cards in your graveyard, also regenerate each other creature you control.').
card_mana_cost('dark dabbling', ['2', 'B']).
card_cmc('dark dabbling', 3).
card_layout('dark dabbling', 'normal').

% Found in: FRF
card_name('dark deal', 'Dark Deal').
card_type('dark deal', 'Sorcery').
card_types('dark deal', ['Sorcery']).
card_subtypes('dark deal', []).
card_colors('dark deal', ['B']).
card_text('dark deal', 'Each player discards all the cards in his or her hand, then draws that many cards minus one.').
card_mana_cost('dark deal', ['2', 'B']).
card_cmc('dark deal', 3).
card_layout('dark deal', 'normal').

% Found in: CSP
card_name('dark depths', 'Dark Depths').
card_type('dark depths', 'Legendary Snow Land').
card_types('dark depths', ['Land']).
card_subtypes('dark depths', []).
card_supertypes('dark depths', ['Legendary', 'Snow']).
card_colors('dark depths', []).
card_text('dark depths', 'Dark Depths enters the battlefield with ten ice counters on it.\n{3}: Remove an ice counter from Dark Depths.\nWhen Dark Depths has no ice counters on it, sacrifice it. If you do, put a legendary 20/20 black Avatar creature token with flying and indestructible named Marit Lage onto the battlefield.').
card_layout('dark depths', 'normal').

% Found in: M12, M13, M14
card_name('dark favor', 'Dark Favor').
card_type('dark favor', 'Enchantment — Aura').
card_types('dark favor', ['Enchantment']).
card_subtypes('dark favor', ['Aura']).
card_colors('dark favor', ['B']).
card_text('dark favor', 'Enchant creature\nWhen Dark Favor enters the battlefield, you lose 1 life.\nEnchanted creature gets +3/+1.').
card_mana_cost('dark favor', ['1', 'B']).
card_cmc('dark favor', 2).
card_layout('dark favor', 'normal').

% Found in: CMD, PC2, USG, VMA
card_name('dark hatchling', 'Dark Hatchling').
card_type('dark hatchling', 'Creature — Horror').
card_types('dark hatchling', ['Creature']).
card_subtypes('dark hatchling', ['Horror']).
card_colors('dark hatchling', ['B']).
card_text('dark hatchling', 'Flying\nWhen Dark Hatchling enters the battlefield, destroy target nonblack creature. It can\'t be regenerated.').
card_mana_cost('dark hatchling', ['4', 'B', 'B']).
card_cmc('dark hatchling', 6).
card_layout('dark hatchling', 'normal').
card_power('dark hatchling', 3).
card_toughness('dark hatchling', 3).

% Found in: DRK, RAV
card_name('dark heart of the wood', 'Dark Heart of the Wood').
card_type('dark heart of the wood', 'Enchantment').
card_types('dark heart of the wood', ['Enchantment']).
card_subtypes('dark heart of the wood', []).
card_colors('dark heart of the wood', ['B', 'G']).
card_text('dark heart of the wood', 'Sacrifice a Forest: You gain 3 life.').
card_mana_cost('dark heart of the wood', ['B', 'G']).
card_cmc('dark heart of the wood', 2).
card_layout('dark heart of the wood', 'normal').

% Found in: AVR
card_name('dark impostor', 'Dark Impostor').
card_type('dark impostor', 'Creature — Vampire Assassin').
card_types('dark impostor', ['Creature']).
card_subtypes('dark impostor', ['Vampire', 'Assassin']).
card_colors('dark impostor', ['B']).
card_text('dark impostor', '{4}{B}{B}: Exile target creature and put a +1/+1 counter on Dark Impostor.\nDark Impostor has all activated abilities of all creature cards exiled with it.').
card_mana_cost('dark impostor', ['2', 'B']).
card_cmc('dark impostor', 3).
card_layout('dark impostor', 'normal').
card_power('dark impostor', 2).
card_toughness('dark impostor', 2).

% Found in: 5ED, HML
card_name('dark maze', 'Dark Maze').
card_type('dark maze', 'Creature — Wall').
card_types('dark maze', ['Creature']).
card_subtypes('dark maze', ['Wall']).
card_colors('dark maze', ['U']).
card_text('dark maze', 'Defender (This creature can\'t attack.)\n{0}: Dark Maze can attack this turn as though it didn\'t have defender.  Exile it at the beginning of the next end step.').
card_mana_cost('dark maze', ['4', 'U']).
card_cmc('dark maze', 5).
card_layout('dark maze', 'normal').
card_power('dark maze', 4).
card_toughness('dark maze', 5).

% Found in: PO2, S99
card_name('dark offering', 'Dark Offering').
card_type('dark offering', 'Sorcery').
card_types('dark offering', ['Sorcery']).
card_subtypes('dark offering', []).
card_colors('dark offering', ['B']).
card_text('dark offering', 'Destroy target nonblack creature. You gain 3 life.').
card_mana_cost('dark offering', ['4', 'B', 'B']).
card_cmc('dark offering', 6).
card_layout('dark offering', 'normal').

% Found in: ORI
card_name('dark petition', 'Dark Petition').
card_type('dark petition', 'Sorcery').
card_types('dark petition', ['Sorcery']).
card_subtypes('dark petition', []).
card_colors('dark petition', ['B']).
card_text('dark petition', 'Search your library for a card and put that card into your hand. Then shuffle your library.\nSpell mastery — If there are two or more instant and/or sorcery cards in your graveyard, add {B}{B}{B} to your mana pool.').
card_mana_cost('dark petition', ['3', 'B', 'B']).
card_cmc('dark petition', 5).
card_layout('dark petition', 'normal').

% Found in: MGB, VIS
card_name('dark privilege', 'Dark Privilege').
card_type('dark privilege', 'Enchantment — Aura').
card_types('dark privilege', ['Enchantment']).
card_subtypes('dark privilege', ['Aura']).
card_colors('dark privilege', ['B']).
card_text('dark privilege', 'Enchant creature\nEnchanted creature gets +1/+1.\nSacrifice a creature: Regenerate enchanted creature.').
card_mana_cost('dark privilege', ['1', 'B']).
card_cmc('dark privilege', 2).
card_layout('dark privilege', 'normal').

% Found in: M14
card_name('dark prophecy', 'Dark Prophecy').
card_type('dark prophecy', 'Enchantment').
card_types('dark prophecy', ['Enchantment']).
card_subtypes('dark prophecy', []).
card_colors('dark prophecy', ['B']).
card_text('dark prophecy', 'Whenever a creature you control dies, you draw a card and you lose 1 life.').
card_mana_cost('dark prophecy', ['B', 'B', 'B']).
card_cmc('dark prophecy', 3).
card_layout('dark prophecy', 'normal').

% Found in: RTR
card_name('dark revenant', 'Dark Revenant').
card_type('dark revenant', 'Creature — Spirit').
card_types('dark revenant', ['Creature']).
card_subtypes('dark revenant', ['Spirit']).
card_colors('dark revenant', ['B']).
card_text('dark revenant', 'Flying\nWhen Dark Revenant dies, put it on top of its owner\'s library.').
card_mana_cost('dark revenant', ['3', 'B']).
card_cmc('dark revenant', 4).
card_layout('dark revenant', 'normal').
card_power('dark revenant', 2).
card_toughness('dark revenant', 2).

% Found in: 2ED, 3ED, 4ED, 5ED, BRB, BTD, CED, CEI, CST, DD3_DVD, DDC, DDE, DKM, HOP, ICE, ITP, LEA, LEB, ME4, MIR, MMQ, RQS, TMP, TPR, USG, V13, VMA, pJGP
card_name('dark ritual', 'Dark Ritual').
card_type('dark ritual', 'Instant').
card_types('dark ritual', ['Instant']).
card_subtypes('dark ritual', []).
card_colors('dark ritual', ['B']).
card_text('dark ritual', 'Add {B}{B}{B} to your mana pool.').
card_mana_cost('dark ritual', ['B']).
card_cmc('dark ritual', 1).
card_layout('dark ritual', 'normal').

% Found in: DRK
card_name('dark sphere', 'Dark Sphere').
card_type('dark sphere', 'Artifact').
card_types('dark sphere', ['Artifact']).
card_subtypes('dark sphere', []).
card_colors('dark sphere', []).
card_text('dark sphere', '{T}, Sacrifice Dark Sphere: The next time a source of your choice would deal damage to you this turn, prevent half that damage, rounded down.').
card_mana_cost('dark sphere', ['0']).
card_cmc('dark sphere', 0).
card_layout('dark sphere', 'normal').

% Found in: LGN
card_name('dark supplicant', 'Dark Supplicant').
card_type('dark supplicant', 'Creature — Human Cleric').
card_types('dark supplicant', ['Creature']).
card_subtypes('dark supplicant', ['Human', 'Cleric']).
card_colors('dark supplicant', ['B']).
card_text('dark supplicant', '{T}, Sacrifice three Clerics: Search your graveyard, hand, and/or library for a card named Scion of Darkness and put it onto the battlefield. If you search your library this way, shuffle it.').
card_mana_cost('dark supplicant', ['B']).
card_cmc('dark supplicant', 1).
card_layout('dark supplicant', 'normal').
card_power('dark supplicant', 1).
card_toughness('dark supplicant', 1).

% Found in: PLS
card_name('dark suspicions', 'Dark Suspicions').
card_type('dark suspicions', 'Enchantment').
card_types('dark suspicions', ['Enchantment']).
card_subtypes('dark suspicions', []).
card_colors('dark suspicions', ['B']).
card_text('dark suspicions', 'At the beginning of each opponent\'s upkeep, that player loses X life, where X is the number of cards in that player\'s hand minus the number of cards in your hand.').
card_mana_cost('dark suspicions', ['2', 'B', 'B']).
card_cmc('dark suspicions', 4).
card_layout('dark suspicions', 'normal').

% Found in: CON
card_name('dark temper', 'Dark Temper').
card_type('dark temper', 'Instant').
card_types('dark temper', ['Instant']).
card_subtypes('dark temper', []).
card_colors('dark temper', ['R']).
card_text('dark temper', 'Dark Temper deals 2 damage to target creature. If you control a black permanent, destroy the creature instead.').
card_mana_cost('dark temper', ['2', 'R']).
card_cmc('dark temper', 3).
card_layout('dark temper', 'normal').

% Found in: NMS
card_name('dark triumph', 'Dark Triumph').
card_type('dark triumph', 'Instant').
card_types('dark triumph', ['Instant']).
card_subtypes('dark triumph', []).
card_colors('dark triumph', ['B']).
card_text('dark triumph', 'If you control a Swamp, you may sacrifice a creature rather than pay Dark Triumph\'s mana cost.\nCreatures you control get +2/+0 until end of turn.').
card_mana_cost('dark triumph', ['4', 'B']).
card_cmc('dark triumph', 5).
card_layout('dark triumph', 'normal').

% Found in: M11
card_name('dark tutelage', 'Dark Tutelage').
card_type('dark tutelage', 'Enchantment').
card_types('dark tutelage', ['Enchantment']).
card_subtypes('dark tutelage', []).
card_colors('dark tutelage', ['B']).
card_text('dark tutelage', 'At the beginning of your upkeep, reveal the top card of your library and put that card into your hand. You lose life equal to its converted mana cost.').
card_mana_cost('dark tutelage', ['2', 'B']).
card_cmc('dark tutelage', 3).
card_layout('dark tutelage', 'normal').

% Found in: TSP
card_name('dark withering', 'Dark Withering').
card_type('dark withering', 'Instant').
card_types('dark withering', ['Instant']).
card_subtypes('dark withering', []).
card_colors('dark withering', ['B']).
card_text('dark withering', 'Destroy target nonblack creature.\nMadness {B} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('dark withering', ['4', 'B', 'B']).
card_cmc('dark withering', 6).
card_layout('dark withering', 'normal').

% Found in: RAV
card_name('darkblast', 'Darkblast').
card_type('darkblast', 'Instant').
card_types('darkblast', ['Instant']).
card_subtypes('darkblast', []).
card_colors('darkblast', ['B']).
card_text('darkblast', 'Target creature gets -1/-1 until end of turn.\nDredge 3 (If you would draw a card, instead you may put exactly three cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_mana_cost('darkblast', ['B']).
card_cmc('darkblast', 1).
card_layout('darkblast', 'normal').

% Found in: 7ED, USG
card_name('darkest hour', 'Darkest Hour').
card_type('darkest hour', 'Enchantment').
card_types('darkest hour', ['Enchantment']).
card_subtypes('darkest hour', []).
card_colors('darkest hour', ['B']).
card_text('darkest hour', 'All creatures are black.').
card_mana_cost('darkest hour', ['B']).
card_cmc('darkest hour', 1).
card_layout('darkest hour', 'normal').

% Found in: PLC
card_name('darkheart sliver', 'Darkheart Sliver').
card_type('darkheart sliver', 'Creature — Sliver').
card_types('darkheart sliver', ['Creature']).
card_subtypes('darkheart sliver', ['Sliver']).
card_colors('darkheart sliver', ['B', 'G']).
card_text('darkheart sliver', 'All Slivers have \"Sacrifice this permanent: You gain 3 life.\"').
card_mana_cost('darkheart sliver', ['B', 'G']).
card_cmc('darkheart sliver', 2).
card_layout('darkheart sliver', 'normal').
card_power('darkheart sliver', 2).
card_toughness('darkheart sliver', 2).

% Found in: TMP
card_name('darkling stalker', 'Darkling Stalker').
card_type('darkling stalker', 'Creature — Shade Spirit').
card_types('darkling stalker', ['Creature']).
card_subtypes('darkling stalker', ['Shade', 'Spirit']).
card_colors('darkling stalker', ['B']).
card_text('darkling stalker', '{B}: Regenerate Darkling Stalker.\n{B}: Darkling Stalker gets +1/+1 until end of turn.').
card_mana_cost('darkling stalker', ['3', 'B']).
card_cmc('darkling stalker', 4).
card_layout('darkling stalker', 'normal').
card_power('darkling stalker', 1).
card_toughness('darkling stalker', 1).

% Found in: CON
card_name('darklit gargoyle', 'Darklit Gargoyle').
card_type('darklit gargoyle', 'Artifact Creature — Gargoyle').
card_types('darklit gargoyle', ['Artifact', 'Creature']).
card_subtypes('darklit gargoyle', ['Gargoyle']).
card_colors('darklit gargoyle', ['W']).
card_text('darklit gargoyle', 'Flying\n{B}: Darklit Gargoyle gets +2/-1 until end of turn.').
card_mana_cost('darklit gargoyle', ['1', 'W']).
card_cmc('darklit gargoyle', 2).
card_layout('darklit gargoyle', 'normal').
card_power('darklit gargoyle', 1).
card_toughness('darklit gargoyle', 2).

% Found in: LEG, TSB
card_name('darkness', 'Darkness').
card_type('darkness', 'Instant').
card_types('darkness', ['Instant']).
card_subtypes('darkness', []).
card_colors('darkness', ['B']).
card_text('darkness', 'Prevent all combat damage that would be dealt this turn.').
card_mana_cost('darkness', ['B']).
card_cmc('darkness', 1).
card_layout('darkness', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB
card_name('darkpact', 'Darkpact').
card_type('darkpact', 'Sorcery').
card_types('darkpact', ['Sorcery']).
card_subtypes('darkpact', []).
card_colors('darkpact', ['B']).
card_text('darkpact', 'Remove Darkpact from your deck before playing if you\'re not playing for ante.\nYou own target card in the ante. Exchange that card with the top card of your library.').
card_mana_cost('darkpact', ['B', 'B', 'B']).
card_cmc('darkpact', 3).
card_layout('darkpact', 'normal').
card_reserved('darkpact').

% Found in: SOM
card_name('darkslick drake', 'Darkslick Drake').
card_type('darkslick drake', 'Creature — Drake').
card_types('darkslick drake', ['Creature']).
card_subtypes('darkslick drake', ['Drake']).
card_colors('darkslick drake', ['U']).
card_text('darkslick drake', 'Flying\nWhen Darkslick Drake dies, draw a card.').
card_mana_cost('darkslick drake', ['2', 'U', 'U']).
card_cmc('darkslick drake', 4).
card_layout('darkslick drake', 'normal').
card_power('darkslick drake', 2).
card_toughness('darkslick drake', 4).

% Found in: SOM
card_name('darkslick shores', 'Darkslick Shores').
card_type('darkslick shores', 'Land').
card_types('darkslick shores', ['Land']).
card_subtypes('darkslick shores', []).
card_colors('darkslick shores', []).
card_text('darkslick shores', 'Darkslick Shores enters the battlefield tapped unless you control two or fewer other lands.\n{T}: Add {U} or {B} to your mana pool.').
card_layout('darkslick shores', 'normal').

% Found in: MM2, SOM
card_name('darksteel axe', 'Darksteel Axe').
card_type('darksteel axe', 'Artifact — Equipment').
card_types('darksteel axe', ['Artifact']).
card_subtypes('darksteel axe', ['Equipment']).
card_colors('darksteel axe', []).
card_text('darksteel axe', 'Indestructible (Effects that say \"destroy\" don\'t destroy this artifact.)\nEquipped creature gets +2/+0.\nEquip {2}').
card_mana_cost('darksteel axe', ['1']).
card_cmc('darksteel axe', 1).
card_layout('darksteel axe', 'normal').

% Found in: DST
card_name('darksteel brute', 'Darksteel Brute').
card_type('darksteel brute', 'Artifact').
card_types('darksteel brute', ['Artifact']).
card_subtypes('darksteel brute', []).
card_colors('darksteel brute', []).
card_text('darksteel brute', 'Indestructible (Damage and effects that say \"destroy\" don\'t destroy this artifact.)\n{3}: Darksteel Brute becomes a 2/2 Beast artifact creature until end of turn.').
card_mana_cost('darksteel brute', ['2']).
card_cmc('darksteel brute', 2).
card_layout('darksteel brute', 'normal').

% Found in: C14, DDF, DST, M15, MM2
card_name('darksteel citadel', 'Darksteel Citadel').
card_type('darksteel citadel', 'Artifact Land').
card_types('darksteel citadel', ['Artifact', 'Land']).
card_subtypes('darksteel citadel', []).
card_colors('darksteel citadel', []).
card_text('darksteel citadel', 'Indestructible (Effects that say \"destroy\" don\'t destroy this land.)\n{T}: Add {1} to your mana pool.').
card_layout('darksteel citadel', 'normal').

% Found in: DST, M10
card_name('darksteel colossus', 'Darksteel Colossus').
card_type('darksteel colossus', 'Artifact Creature — Golem').
card_types('darksteel colossus', ['Artifact', 'Creature']).
card_subtypes('darksteel colossus', ['Golem']).
card_colors('darksteel colossus', []).
card_text('darksteel colossus', 'Trample, indestructible\nIf Darksteel Colossus would be put into a graveyard from anywhere, reveal Darksteel Colossus and shuffle it into its owner\'s library instead.').
card_mana_cost('darksteel colossus', ['11']).
card_cmc('darksteel colossus', 11).
card_layout('darksteel colossus', 'normal').
card_power('darksteel colossus', 11).
card_toughness('darksteel colossus', 11).

% Found in: DST, HOP, M14
card_name('darksteel forge', 'Darksteel Forge').
card_type('darksteel forge', 'Artifact').
card_types('darksteel forge', ['Artifact']).
card_subtypes('darksteel forge', []).
card_colors('darksteel forge', []).
card_text('darksteel forge', 'Artifacts you control have indestructible. (Effects that say \"destroy\" don\'t destroy them. Artifact creatures with indestructible can\'t be destroyed by damage.)').
card_mana_cost('darksteel forge', ['9']).
card_cmc('darksteel forge', 9).
card_layout('darksteel forge', 'normal').

% Found in: DST
card_name('darksteel gargoyle', 'Darksteel Gargoyle').
card_type('darksteel gargoyle', 'Artifact Creature — Gargoyle').
card_types('darksteel gargoyle', ['Artifact', 'Creature']).
card_subtypes('darksteel gargoyle', ['Gargoyle']).
card_colors('darksteel gargoyle', []).
card_text('darksteel gargoyle', 'Flying\nIndestructible (Damage and effects that say \"destroy\" don\'t destroy this creature.)').
card_mana_cost('darksteel gargoyle', ['7']).
card_cmc('darksteel gargoyle', 7).
card_layout('darksteel gargoyle', 'normal').
card_power('darksteel gargoyle', 3).
card_toughness('darksteel gargoyle', 3).

% Found in: FUT
card_name('darksteel garrison', 'Darksteel Garrison').
card_type('darksteel garrison', 'Artifact — Fortification').
card_types('darksteel garrison', ['Artifact']).
card_subtypes('darksteel garrison', ['Fortification']).
card_colors('darksteel garrison', []).
card_text('darksteel garrison', 'Fortified land has indestructible.\nWhenever fortified land becomes tapped, target creature gets +1/+1 until end of turn.\nFortify {3} ({3}: Attach to target land you control. Fortify only as a sorcery. This card enters the battlefield unattached and stays on the battlefield if the land leaves.)').
card_mana_cost('darksteel garrison', ['2']).
card_cmc('darksteel garrison', 2).
card_layout('darksteel garrison', 'normal').

% Found in: C13, CMD, DST, M14, pARL
card_name('darksteel ingot', 'Darksteel Ingot').
card_type('darksteel ingot', 'Artifact').
card_types('darksteel ingot', ['Artifact']).
card_subtypes('darksteel ingot', []).
card_colors('darksteel ingot', []).
card_text('darksteel ingot', 'Indestructible\n{T}: Add one mana of any color to your mana pool.').
card_mana_cost('darksteel ingot', ['3']).
card_cmc('darksteel ingot', 3).
card_layout('darksteel ingot', 'normal').

% Found in: SOM
card_name('darksteel juggernaut', 'Darksteel Juggernaut').
card_type('darksteel juggernaut', 'Artifact Creature — Juggernaut').
card_types('darksteel juggernaut', ['Artifact', 'Creature']).
card_subtypes('darksteel juggernaut', ['Juggernaut']).
card_colors('darksteel juggernaut', []).
card_text('darksteel juggernaut', 'Indestructible\nDarksteel Juggernaut\'s power and toughness are each equal to the number of artifacts you control.\nDarksteel Juggernaut attacks each turn if able.').
card_mana_cost('darksteel juggernaut', ['5']).
card_cmc('darksteel juggernaut', 5).
card_layout('darksteel juggernaut', 'normal').
card_power('darksteel juggernaut', '*').
card_toughness('darksteel juggernaut', '*').

% Found in: C13
card_name('darksteel mutation', 'Darksteel Mutation').
card_type('darksteel mutation', 'Enchantment — Aura').
card_types('darksteel mutation', ['Enchantment']).
card_subtypes('darksteel mutation', ['Aura']).
card_colors('darksteel mutation', ['W']).
card_text('darksteel mutation', 'Enchant creature\nEnchanted creature is an Insect artifact creature with base power and toughness 0/1 and has indestructible, and it loses all other abilities, card types, and creature types.').
card_mana_cost('darksteel mutation', ['1', 'W']).
card_cmc('darksteel mutation', 2).
card_layout('darksteel mutation', 'normal').

% Found in: SOM
card_name('darksteel myr', 'Darksteel Myr').
card_type('darksteel myr', 'Artifact Creature — Myr').
card_types('darksteel myr', ['Artifact', 'Creature']).
card_subtypes('darksteel myr', ['Myr']).
card_colors('darksteel myr', []).
card_text('darksteel myr', 'Indestructible (Damage and effects that say \"destroy\" don\'t destroy this creature. If its toughness is 0 or less, it\'s still put into its owner\'s graveyard.)').
card_mana_cost('darksteel myr', ['3']).
card_cmc('darksteel myr', 3).
card_layout('darksteel myr', 'normal').
card_power('darksteel myr', 0).
card_toughness('darksteel myr', 1).

% Found in: DST
card_name('darksteel pendant', 'Darksteel Pendant').
card_type('darksteel pendant', 'Artifact').
card_types('darksteel pendant', ['Artifact']).
card_subtypes('darksteel pendant', []).
card_colors('darksteel pendant', []).
card_text('darksteel pendant', 'Indestructible (Effects that say \"destroy\" don\'t destroy this artifact.)\n{1}, {T}: Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('darksteel pendant', ['2']).
card_cmc('darksteel pendant', 2).
card_layout('darksteel pendant', 'normal').

% Found in: MBS
card_name('darksteel plate', 'Darksteel Plate').
card_type('darksteel plate', 'Artifact — Equipment').
card_types('darksteel plate', ['Artifact']).
card_subtypes('darksteel plate', ['Equipment']).
card_colors('darksteel plate', []).
card_text('darksteel plate', 'Indestructible\nEquipped creature has indestructible.\nEquip {2}').
card_mana_cost('darksteel plate', ['3']).
card_cmc('darksteel plate', 3).
card_layout('darksteel plate', 'normal').

% Found in: DST
card_name('darksteel reactor', 'Darksteel Reactor').
card_type('darksteel reactor', 'Artifact').
card_types('darksteel reactor', ['Artifact']).
card_subtypes('darksteel reactor', []).
card_colors('darksteel reactor', []).
card_text('darksteel reactor', 'Indestructible (Effects that say \"destroy\" don\'t destroy this artifact.)\nAt the beginning of your upkeep, you may put a charge counter on Darksteel Reactor.\nWhen Darksteel Reactor has twenty or more charge counters on it, you win the game.').
card_mana_cost('darksteel reactor', ['4']).
card_cmc('darksteel reactor', 4).
card_layout('darksteel reactor', 'normal').

% Found in: NPH
card_name('darksteel relic', 'Darksteel Relic').
card_type('darksteel relic', 'Artifact').
card_types('darksteel relic', ['Artifact']).
card_subtypes('darksteel relic', []).
card_colors('darksteel relic', []).
card_text('darksteel relic', 'Indestructible (Effects that say \"destroy\" don\'t destroy this artifact.)').
card_mana_cost('darksteel relic', ['0']).
card_cmc('darksteel relic', 0).
card_layout('darksteel relic', 'normal').

% Found in: SOM
card_name('darksteel sentinel', 'Darksteel Sentinel').
card_type('darksteel sentinel', 'Artifact Creature — Golem').
card_types('darksteel sentinel', ['Artifact', 'Creature']).
card_subtypes('darksteel sentinel', ['Golem']).
card_colors('darksteel sentinel', []).
card_text('darksteel sentinel', 'Flash (You may cast this spell any time you could cast an instant.)\nVigilance\nIndestructible (Damage and effects that say \"destroy\" don\'t destroy this creature. If its toughness is 0 or less, it\'s still put into its owner\'s graveyard.)').
card_mana_cost('darksteel sentinel', ['6']).
card_cmc('darksteel sentinel', 6).
card_layout('darksteel sentinel', 'normal').
card_power('darksteel sentinel', 3).
card_toughness('darksteel sentinel', 3).

% Found in: ISD
card_name('darkthicket wolf', 'Darkthicket Wolf').
card_type('darkthicket wolf', 'Creature — Wolf').
card_types('darkthicket wolf', ['Creature']).
card_subtypes('darkthicket wolf', ['Wolf']).
card_colors('darkthicket wolf', ['G']).
card_text('darkthicket wolf', '{2}{G}: Darkthicket Wolf gets +2/+2 until end of turn. Activate this ability only once each turn.').
card_mana_cost('darkthicket wolf', ['1', 'G']).
card_cmc('darkthicket wolf', 2).
card_layout('darkthicket wolf', 'normal').
card_power('darkthicket wolf', 2).
card_toughness('darkthicket wolf', 2).

% Found in: ULG
card_name('darkwatch elves', 'Darkwatch Elves').
card_type('darkwatch elves', 'Creature — Elf').
card_types('darkwatch elves', ['Creature']).
card_subtypes('darkwatch elves', ['Elf']).
card_colors('darkwatch elves', ['G']).
card_text('darkwatch elves', 'Protection from black\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('darkwatch elves', ['2', 'G']).
card_cmc('darkwatch elves', 3).
card_layout('darkwatch elves', 'normal').
card_power('darkwatch elves', 2).
card_toughness('darkwatch elves', 2).

% Found in: ODY
card_name('darkwater catacombs', 'Darkwater Catacombs').
card_type('darkwater catacombs', 'Land').
card_types('darkwater catacombs', ['Land']).
card_subtypes('darkwater catacombs', []).
card_colors('darkwater catacombs', []).
card_text('darkwater catacombs', '{1}, {T}: Add {U}{B} to your mana pool.').
card_layout('darkwater catacombs', 'normal').

% Found in: ODY
card_name('darkwater egg', 'Darkwater Egg').
card_type('darkwater egg', 'Artifact').
card_types('darkwater egg', ['Artifact']).
card_subtypes('darkwater egg', []).
card_colors('darkwater egg', []).
card_text('darkwater egg', '{2}, {T}, Sacrifice Darkwater Egg: Add {U}{B} to your mana pool. Draw a card.').
card_mana_cost('darkwater egg', ['1']).
card_cmc('darkwater egg', 1).
card_layout('darkwater egg', 'normal').

% Found in: MMQ
card_name('darting merfolk', 'Darting Merfolk').
card_type('darting merfolk', 'Creature — Merfolk').
card_types('darting merfolk', ['Creature']).
card_subtypes('darting merfolk', ['Merfolk']).
card_colors('darting merfolk', ['U']).
card_text('darting merfolk', '{U}: Return Darting Merfolk to its owner\'s hand.').
card_mana_cost('darting merfolk', ['1', 'U']).
card_cmc('darting merfolk', 2).
card_layout('darting merfolk', 'normal').
card_power('darting merfolk', 1).
card_toughness('darting merfolk', 1).

% Found in: ONS
card_name('daru cavalier', 'Daru Cavalier').
card_type('daru cavalier', 'Creature — Human Soldier').
card_types('daru cavalier', ['Creature']).
card_subtypes('daru cavalier', ['Human', 'Soldier']).
card_colors('daru cavalier', ['W']).
card_text('daru cavalier', 'First strike\nWhen Daru Cavalier enters the battlefield, you may search your library for a card named Daru Cavalier, reveal it, and put it into your hand. If you do, shuffle your library.').
card_mana_cost('daru cavalier', ['3', 'W']).
card_cmc('daru cavalier', 4).
card_layout('daru cavalier', 'normal').
card_power('daru cavalier', 2).
card_toughness('daru cavalier', 2).

% Found in: DDF, ONS
card_name('daru encampment', 'Daru Encampment').
card_type('daru encampment', 'Land').
card_types('daru encampment', ['Land']).
card_subtypes('daru encampment', []).
card_colors('daru encampment', []).
card_text('daru encampment', '{T}: Add {1} to your mana pool.\n{W}, {T}: Target Soldier creature gets +1/+1 until end of turn.').
card_layout('daru encampment', 'normal').

% Found in: ONS
card_name('daru healer', 'Daru Healer').
card_type('daru healer', 'Creature — Human Cleric').
card_types('daru healer', ['Creature']).
card_subtypes('daru healer', ['Human', 'Cleric']).
card_colors('daru healer', ['W']).
card_text('daru healer', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\nMorph {W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('daru healer', ['2', 'W']).
card_cmc('daru healer', 3).
card_layout('daru healer', 'normal').
card_power('daru healer', 1).
card_toughness('daru healer', 2).

% Found in: ONS
card_name('daru lancer', 'Daru Lancer').
card_type('daru lancer', 'Creature — Human Soldier').
card_types('daru lancer', ['Creature']).
card_subtypes('daru lancer', ['Human', 'Soldier']).
card_colors('daru lancer', ['W']).
card_text('daru lancer', 'First strike\nMorph {2}{W}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('daru lancer', ['4', 'W', 'W']).
card_cmc('daru lancer', 6).
card_layout('daru lancer', 'normal').
card_power('daru lancer', 3).
card_toughness('daru lancer', 4).

% Found in: LGN
card_name('daru mender', 'Daru Mender').
card_type('daru mender', 'Creature — Human Cleric').
card_types('daru mender', ['Creature']).
card_subtypes('daru mender', ['Human', 'Cleric']).
card_colors('daru mender', ['W']).
card_text('daru mender', 'Morph {W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Daru Mender is turned face up, regenerate target creature.').
card_mana_cost('daru mender', ['W']).
card_cmc('daru mender', 1).
card_layout('daru mender', 'normal').
card_power('daru mender', 1).
card_toughness('daru mender', 1).

% Found in: LGN
card_name('daru sanctifier', 'Daru Sanctifier').
card_type('daru sanctifier', 'Creature — Human Cleric').
card_types('daru sanctifier', ['Creature']).
card_subtypes('daru sanctifier', ['Human', 'Cleric']).
card_colors('daru sanctifier', ['W']).
card_text('daru sanctifier', 'Morph {1}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Daru Sanctifier is turned face up, destroy target enchantment.').
card_mana_cost('daru sanctifier', ['3', 'W']).
card_cmc('daru sanctifier', 4).
card_layout('daru sanctifier', 'normal').
card_power('daru sanctifier', 1).
card_toughness('daru sanctifier', 4).

% Found in: SCG
card_name('daru spiritualist', 'Daru Spiritualist').
card_type('daru spiritualist', 'Creature — Human Cleric').
card_types('daru spiritualist', ['Creature']).
card_subtypes('daru spiritualist', ['Human', 'Cleric']).
card_colors('daru spiritualist', ['W']).
card_text('daru spiritualist', 'Whenever a Cleric creature you control becomes the target of a spell or ability, it gets +0/+2 until end of turn.').
card_mana_cost('daru spiritualist', ['1', 'W']).
card_cmc('daru spiritualist', 2).
card_layout('daru spiritualist', 'normal').
card_power('daru spiritualist', 1).
card_toughness('daru spiritualist', 1).

% Found in: LGN
card_name('daru stinger', 'Daru Stinger').
card_type('daru stinger', 'Creature — Soldier').
card_types('daru stinger', ['Creature']).
card_subtypes('daru stinger', ['Soldier']).
card_colors('daru stinger', ['W']).
card_text('daru stinger', 'Amplify 1 (As this creature enters the battlefield, put a +1/+1 counter on it for each  Soldier card you reveal in your hand.)\n{T}: Daru Stinger deals damage equal to the number of +1/+1 counters on it to target attacking or blocking creature.').
card_mana_cost('daru stinger', ['3', 'W']).
card_cmc('daru stinger', 4).
card_layout('daru stinger', 'normal').
card_power('daru stinger', 1).
card_toughness('daru stinger', 1).

% Found in: SCG
card_name('daru warchief', 'Daru Warchief').
card_type('daru warchief', 'Creature — Human Soldier').
card_types('daru warchief', ['Creature']).
card_subtypes('daru warchief', ['Human', 'Soldier']).
card_colors('daru warchief', ['W']).
card_text('daru warchief', 'Soldier spells you cast cost {1} less to cast.\nSoldier creatures you control get +1/+2.').
card_mana_cost('daru warchief', ['2', 'W', 'W']).
card_cmc('daru warchief', 4).
card_layout('daru warchief', 'normal').
card_power('daru warchief', 1).
card_toughness('daru warchief', 1).

% Found in: PLC
card_name('dash hopes', 'Dash Hopes').
card_type('dash hopes', 'Instant').
card_types('dash hopes', ['Instant']).
card_subtypes('dash hopes', []).
card_colors('dash hopes', ['B']).
card_text('dash hopes', 'When you cast Dash Hopes, any player may pay 5 life. If a player does, counter Dash Hopes.\nCounter target spell.').
card_mana_cost('dash hopes', ['B', 'B']).
card_cmc('dash hopes', 2).
card_layout('dash hopes', 'normal').

% Found in: HML
card_name('daughter of autumn', 'Daughter of Autumn').
card_type('daughter of autumn', 'Legendary Creature — Avatar').
card_types('daughter of autumn', ['Creature']).
card_subtypes('daughter of autumn', ['Avatar']).
card_supertypes('daughter of autumn', ['Legendary']).
card_colors('daughter of autumn', ['G']).
card_text('daughter of autumn', '{W}: The next 1 damage that would be dealt to target white creature this turn is dealt to Daughter of Autumn instead.').
card_mana_cost('daughter of autumn', ['2', 'G', 'G']).
card_cmc('daughter of autumn', 4).
card_layout('daughter of autumn', 'normal').
card_power('daughter of autumn', 2).
card_toughness('daughter of autumn', 4).
card_reserved('daughter of autumn').

% Found in: ONS
card_name('daunting defender', 'Daunting Defender').
card_type('daunting defender', 'Creature — Human Cleric').
card_types('daunting defender', ['Creature']).
card_subtypes('daunting defender', ['Human', 'Cleric']).
card_colors('daunting defender', ['W']).
card_text('daunting defender', 'If a source would deal damage to a Cleric creature you control, prevent 1 of that damage.').
card_mana_cost('daunting defender', ['4', 'W']).
card_cmc('daunting defender', 5).
card_layout('daunting defender', 'normal').
card_power('daunting defender', 3).
card_toughness('daunting defender', 3).

% Found in: LRW, pGTW
card_name('dauntless dourbark', 'Dauntless Dourbark').
card_type('dauntless dourbark', 'Creature — Treefolk Warrior').
card_types('dauntless dourbark', ['Creature']).
card_subtypes('dauntless dourbark', ['Treefolk', 'Warrior']).
card_colors('dauntless dourbark', ['G']).
card_text('dauntless dourbark', 'Dauntless Dourbark\'s power and toughness are each equal to the number of Forests you control plus the number of Treefolk you control.\nDauntless Dourbark has trample as long as you control another Treefolk.').
card_mana_cost('dauntless dourbark', ['3', 'G']).
card_cmc('dauntless dourbark', 4).
card_layout('dauntless dourbark', 'normal').
card_power('dauntless dourbark', '*').
card_toughness('dauntless dourbark', '*').

% Found in: ARB
card_name('dauntless escort', 'Dauntless Escort').
card_type('dauntless escort', 'Creature — Rhino Soldier').
card_types('dauntless escort', ['Creature']).
card_subtypes('dauntless escort', ['Rhino', 'Soldier']).
card_colors('dauntless escort', ['W', 'G']).
card_text('dauntless escort', 'Sacrifice Dauntless Escort: Creatures you control gain indestructible until end of turn.').
card_mana_cost('dauntless escort', ['1', 'G', 'W']).
card_cmc('dauntless escort', 3).
card_layout('dauntless escort', 'normal').
card_power('dauntless escort', 3).
card_toughness('dauntless escort', 3).

% Found in: VAN
card_name('dauntless escort avatar', 'Dauntless Escort Avatar').
card_type('dauntless escort avatar', 'Vanguard').
card_types('dauntless escort avatar', ['Vanguard']).
card_subtypes('dauntless escort avatar', []).
card_colors('dauntless escort avatar', []).
card_text('dauntless escort avatar', 'Creatures you control have exalted. (Whenever a creature you control attacks alone, it gets +1/+1 until end of turn for each instance of exalted among permanents you control.)').
card_layout('dauntless escort avatar', 'vanguard').

% Found in: DDN, DDO, THS
card_name('dauntless onslaught', 'Dauntless Onslaught').
card_type('dauntless onslaught', 'Instant').
card_types('dauntless onslaught', ['Instant']).
card_subtypes('dauntless onslaught', []).
card_colors('dauntless onslaught', ['W']).
card_text('dauntless onslaught', 'Up to two target creatures each get +2/+2 until end of turn.').
card_mana_cost('dauntless onslaught', ['2', 'W']).
card_cmc('dauntless onslaught', 3).
card_layout('dauntless onslaught', 'normal').

% Found in: M15
card_name('dauntless river marshal', 'Dauntless River Marshal').
card_type('dauntless river marshal', 'Creature — Human Soldier').
card_types('dauntless river marshal', ['Creature']).
card_subtypes('dauntless river marshal', ['Human', 'Soldier']).
card_colors('dauntless river marshal', ['W']).
card_text('dauntless river marshal', 'Dauntless River Marshal gets +1/+1 as long as you control an Island.\n{3}{U}: Tap target creature.').
card_mana_cost('dauntless river marshal', ['1', 'W']).
card_cmc('dauntless river marshal', 2).
card_layout('dauntless river marshal', 'normal').
card_power('dauntless river marshal', 2).
card_toughness('dauntless river marshal', 1).

% Found in: EXO
card_name('dauthi cutthroat', 'Dauthi Cutthroat').
card_type('dauthi cutthroat', 'Creature — Dauthi Minion').
card_types('dauthi cutthroat', ['Creature']).
card_subtypes('dauthi cutthroat', ['Dauthi', 'Minion']).
card_colors('dauthi cutthroat', ['B']).
card_text('dauthi cutthroat', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\n{1}{B}, {T}: Destroy target creature with shadow.').
card_mana_cost('dauthi cutthroat', ['1', 'B']).
card_cmc('dauthi cutthroat', 2).
card_layout('dauthi cutthroat', 'normal').
card_power('dauthi cutthroat', 1).
card_toughness('dauthi cutthroat', 1).

% Found in: TMP
card_name('dauthi embrace', 'Dauthi Embrace').
card_type('dauthi embrace', 'Enchantment').
card_types('dauthi embrace', ['Enchantment']).
card_subtypes('dauthi embrace', []).
card_colors('dauthi embrace', ['B']).
card_text('dauthi embrace', '{B}{B}: Target creature gains shadow until end of turn. (It can block or be blocked by only creatures with shadow.)').
card_mana_cost('dauthi embrace', ['2', 'B']).
card_cmc('dauthi embrace', 3).
card_layout('dauthi embrace', 'normal').

% Found in: TMP
card_name('dauthi ghoul', 'Dauthi Ghoul').
card_type('dauthi ghoul', 'Creature — Dauthi Zombie').
card_types('dauthi ghoul', ['Creature']).
card_subtypes('dauthi ghoul', ['Dauthi', 'Zombie']).
card_colors('dauthi ghoul', ['B']).
card_text('dauthi ghoul', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhenever a creature with shadow dies, put a +1/+1 counter on Dauthi Ghoul.').
card_mana_cost('dauthi ghoul', ['1', 'B']).
card_cmc('dauthi ghoul', 2).
card_layout('dauthi ghoul', 'normal').
card_power('dauthi ghoul', 1).
card_toughness('dauthi ghoul', 1).

% Found in: TMP, TPR
card_name('dauthi horror', 'Dauthi Horror').
card_type('dauthi horror', 'Creature — Dauthi Horror').
card_types('dauthi horror', ['Creature']).
card_subtypes('dauthi horror', ['Dauthi', 'Horror']).
card_colors('dauthi horror', ['B']).
card_text('dauthi horror', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nDauthi Horror can\'t be blocked by white creatures.').
card_mana_cost('dauthi horror', ['1', 'B']).
card_cmc('dauthi horror', 2).
card_layout('dauthi horror', 'normal').
card_power('dauthi horror', 2).
card_toughness('dauthi horror', 1).

% Found in: EXO, TPR
card_name('dauthi jackal', 'Dauthi Jackal').
card_type('dauthi jackal', 'Creature — Dauthi Hound').
card_types('dauthi jackal', ['Creature']).
card_subtypes('dauthi jackal', ['Dauthi', 'Hound']).
card_colors('dauthi jackal', ['B']).
card_text('dauthi jackal', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\n{B}{B}, Sacrifice Dauthi Jackal: Destroy target blocking creature.').
card_mana_cost('dauthi jackal', ['2', 'B']).
card_cmc('dauthi jackal', 3).
card_layout('dauthi jackal', 'normal').
card_power('dauthi jackal', 2).
card_toughness('dauthi jackal', 1).

% Found in: TMP, TPR
card_name('dauthi marauder', 'Dauthi Marauder').
card_type('dauthi marauder', 'Creature — Dauthi Minion').
card_types('dauthi marauder', ['Creature']).
card_subtypes('dauthi marauder', ['Dauthi', 'Minion']).
card_colors('dauthi marauder', ['B']).
card_text('dauthi marauder', 'Shadow (This creature can block or be blocked by only creatures with shadow.)').
card_mana_cost('dauthi marauder', ['2', 'B']).
card_cmc('dauthi marauder', 3).
card_layout('dauthi marauder', 'normal').
card_power('dauthi marauder', 3).
card_toughness('dauthi marauder', 1).

% Found in: TMP, VMA
card_name('dauthi mercenary', 'Dauthi Mercenary').
card_type('dauthi mercenary', 'Creature — Dauthi Knight Mercenary').
card_types('dauthi mercenary', ['Creature']).
card_subtypes('dauthi mercenary', ['Dauthi', 'Knight', 'Mercenary']).
card_colors('dauthi mercenary', ['B']).
card_text('dauthi mercenary', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\n{1}{B}: Dauthi Mercenary gets +1/+0 until end of turn.').
card_mana_cost('dauthi mercenary', ['2', 'B']).
card_cmc('dauthi mercenary', 3).
card_layout('dauthi mercenary', 'normal').
card_power('dauthi mercenary', 2).
card_toughness('dauthi mercenary', 1).

% Found in: TMP
card_name('dauthi mindripper', 'Dauthi Mindripper').
card_type('dauthi mindripper', 'Creature — Dauthi Minion').
card_types('dauthi mindripper', ['Creature']).
card_subtypes('dauthi mindripper', ['Dauthi', 'Minion']).
card_colors('dauthi mindripper', ['B']).
card_text('dauthi mindripper', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhenever Dauthi Mindripper attacks and isn\'t blocked, you may sacrifice it. If you do, defending player discards three cards.').
card_mana_cost('dauthi mindripper', ['3', 'B']).
card_cmc('dauthi mindripper', 4).
card_layout('dauthi mindripper', 'normal').
card_power('dauthi mindripper', 2).
card_toughness('dauthi mindripper', 1).

% Found in: TMP, TPR, TSB, pARL
card_name('dauthi slayer', 'Dauthi Slayer').
card_type('dauthi slayer', 'Creature — Dauthi Soldier').
card_types('dauthi slayer', ['Creature']).
card_subtypes('dauthi slayer', ['Dauthi', 'Soldier']).
card_colors('dauthi slayer', ['B']).
card_text('dauthi slayer', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nDauthi Slayer attacks each turn if able.').
card_mana_cost('dauthi slayer', ['B', 'B']).
card_cmc('dauthi slayer', 2).
card_layout('dauthi slayer', 'normal').
card_power('dauthi slayer', 2).
card_toughness('dauthi slayer', 2).

% Found in: STH
card_name('dauthi trapper', 'Dauthi Trapper').
card_type('dauthi trapper', 'Creature — Dauthi Minion').
card_types('dauthi trapper', ['Creature']).
card_subtypes('dauthi trapper', ['Dauthi', 'Minion']).
card_colors('dauthi trapper', ['B']).
card_text('dauthi trapper', '{T}: Target creature gains shadow until end of turn. (It can block or be blocked by only creatures with shadow.)').
card_mana_cost('dauthi trapper', ['2', 'B']).
card_cmc('dauthi trapper', 3).
card_layout('dauthi trapper', 'normal').
card_power('dauthi trapper', 1).
card_toughness('dauthi trapper', 1).

% Found in: EXO, TPR
card_name('dauthi warlord', 'Dauthi Warlord').
card_type('dauthi warlord', 'Creature — Dauthi Soldier').
card_types('dauthi warlord', ['Creature']).
card_subtypes('dauthi warlord', ['Dauthi', 'Soldier']).
card_colors('dauthi warlord', ['B']).
card_text('dauthi warlord', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nDauthi Warlord\'s power is equal to the number of creatures with shadow on the battlefield.').
card_mana_cost('dauthi warlord', ['1', 'B']).
card_cmc('dauthi warlord', 2).
card_layout('dauthi warlord', 'normal').
card_power('dauthi warlord', '*').
card_toughness('dauthi warlord', 1).

% Found in: PLC
card_name('dawn charm', 'Dawn Charm').
card_type('dawn charm', 'Instant').
card_types('dawn charm', ['Instant']).
card_subtypes('dawn charm', []).
card_colors('dawn charm', ['W']).
card_text('dawn charm', 'Choose one —\n• Prevent all combat damage that would be dealt this turn.\n• Regenerate target creature.\n• Counter target spell that targets you.').
card_mana_cost('dawn charm', ['1', 'W']).
card_cmc('dawn charm', 2).
card_layout('dawn charm', 'normal').

% Found in: SCG
card_name('dawn elemental', 'Dawn Elemental').
card_type('dawn elemental', 'Creature — Elemental').
card_types('dawn elemental', ['Creature']).
card_subtypes('dawn elemental', ['Elemental']).
card_colors('dawn elemental', ['W']).
card_text('dawn elemental', 'Flying\nPrevent all damage that would be dealt to Dawn Elemental.').
card_mana_cost('dawn elemental', ['W', 'W', 'W', 'W']).
card_cmc('dawn elemental', 4).
card_layout('dawn elemental', 'normal').
card_power('dawn elemental', 3).
card_toughness('dawn elemental', 3).

% Found in: TOR
card_name('dawn of the dead', 'Dawn of the Dead').
card_type('dawn of the dead', 'Enchantment').
card_types('dawn of the dead', ['Enchantment']).
card_subtypes('dawn of the dead', []).
card_colors('dawn of the dead', ['B']).
card_text('dawn of the dead', 'At the beginning of your upkeep, you lose 1 life.\nAt the beginning of your upkeep, you may return target creature card from your graveyard to the battlefield. That creature gains haste until end of turn. Exile it at the beginning of the next end step.').
card_mana_cost('dawn of the dead', ['2', 'B', 'B', 'B']).
card_cmc('dawn of the dead', 5).
card_layout('dawn of the dead', 'normal').

% Found in: BNG
card_name('dawn to dusk', 'Dawn to Dusk').
card_type('dawn to dusk', 'Sorcery').
card_types('dawn to dusk', ['Sorcery']).
card_subtypes('dawn to dusk', []).
card_colors('dawn to dusk', ['W']).
card_text('dawn to dusk', 'Choose one or both —\n• Return target enchantment card from your graveyard to your hand.\n• Destroy target enchantment.').
card_mana_cost('dawn to dusk', ['2', 'W', 'W']).
card_cmc('dawn to dusk', 4).
card_layout('dawn to dusk', 'normal').

% Found in: 5DN
card_name('dawn\'s reflection', 'Dawn\'s Reflection').
card_type('dawn\'s reflection', 'Enchantment — Aura').
card_types('dawn\'s reflection', ['Enchantment']).
card_subtypes('dawn\'s reflection', ['Aura']).
card_colors('dawn\'s reflection', ['G']).
card_text('dawn\'s reflection', 'Enchant land\nWhenever enchanted land is tapped for mana, its controller adds two mana in any combination of colors to his or her mana pool (in addition to the mana the land produces).').
card_mana_cost('dawn\'s reflection', ['3', 'G']).
card_cmc('dawn\'s reflection', 4).
card_layout('dawn\'s reflection', 'normal').

% Found in: JOU, pPRE
card_name('dawnbringer charioteers', 'Dawnbringer Charioteers').
card_type('dawnbringer charioteers', 'Creature — Human Soldier').
card_types('dawnbringer charioteers', ['Creature']).
card_subtypes('dawnbringer charioteers', ['Human', 'Soldier']).
card_colors('dawnbringer charioteers', ['W']).
card_text('dawnbringer charioteers', 'Flying, lifelink\nHeroic — Whenever you cast a spell that targets Dawnbringer Charioteers, put a +1/+1 counter on Dawnbringer Charioteers.').
card_mana_cost('dawnbringer charioteers', ['2', 'W', 'W']).
card_cmc('dawnbringer charioteers', 4).
card_layout('dawnbringer charioteers', 'normal').
card_power('dawnbringer charioteers', 2).
card_toughness('dawnbringer charioteers', 4).

% Found in: LRW
card_name('dawnfluke', 'Dawnfluke').
card_type('dawnfluke', 'Creature — Elemental').
card_types('dawnfluke', ['Creature']).
card_subtypes('dawnfluke', ['Elemental']).
card_colors('dawnfluke', ['W']).
card_text('dawnfluke', 'Flash\nWhen Dawnfluke enters the battlefield, prevent the next 3 damage that would be dealt to target creature or player this turn.\nEvoke {W} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('dawnfluke', ['3', 'W']).
card_cmc('dawnfluke', 4).
card_layout('dawnfluke', 'normal').
card_power('dawnfluke', 0).
card_toughness('dawnfluke', 3).

% Found in: ROE
card_name('dawnglare invoker', 'Dawnglare Invoker').
card_type('dawnglare invoker', 'Creature — Kor Wizard').
card_types('dawnglare invoker', ['Creature']).
card_subtypes('dawnglare invoker', ['Kor', 'Wizard']).
card_colors('dawnglare invoker', ['W']).
card_text('dawnglare invoker', 'Flying\n{8}: Tap all creatures target player controls.').
card_mana_cost('dawnglare invoker', ['2', 'W']).
card_cmc('dawnglare invoker', 3).
card_layout('dawnglare invoker', 'normal').
card_power('dawnglare invoker', 2).
card_toughness('dawnglare invoker', 1).

% Found in: SHM
card_name('dawnglow infusion', 'Dawnglow Infusion').
card_type('dawnglow infusion', 'Sorcery').
card_types('dawnglow infusion', ['Sorcery']).
card_subtypes('dawnglow infusion', []).
card_colors('dawnglow infusion', ['W', 'G']).
card_text('dawnglow infusion', 'You gain X life if {G} was spent to cast Dawnglow Infusion and X life if {W} was spent to cast it. (Do both if {G}{W} was spent.)').
card_mana_cost('dawnglow infusion', ['X', 'G/W']).
card_cmc('dawnglow infusion', 1).
card_layout('dawnglow infusion', 'normal').

% Found in: ONS
card_name('dawning purist', 'Dawning Purist').
card_type('dawning purist', 'Creature — Human Cleric').
card_types('dawning purist', ['Creature']).
card_subtypes('dawning purist', ['Human', 'Cleric']).
card_colors('dawning purist', ['W']).
card_text('dawning purist', 'Whenever Dawning Purist deals combat damage to a player, you may destroy target enchantment that player controls.\nMorph {1}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('dawning purist', ['2', 'W']).
card_cmc('dawning purist', 3).
card_layout('dawning purist', 'normal').
card_power('dawning purist', 2).
card_toughness('dawning purist', 2).

% Found in: ALA
card_name('dawnray archer', 'Dawnray Archer').
card_type('dawnray archer', 'Creature — Human Archer').
card_types('dawnray archer', ['Creature']).
card_subtypes('dawnray archer', ['Human', 'Archer']).
card_colors('dawnray archer', ['U']).
card_text('dawnray archer', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{W}, {T}: Dawnray Archer deals 1 damage to target attacking or blocking creature.').
card_mana_cost('dawnray archer', ['2', 'U']).
card_cmc('dawnray archer', 3).
card_layout('dawnray archer', 'normal').
card_power('dawnray archer', 1).
card_toughness('dawnray archer', 1).

% Found in: MMQ
card_name('dawnstrider', 'Dawnstrider').
card_type('dawnstrider', 'Creature — Dryad Spellshaper').
card_types('dawnstrider', ['Creature']).
card_subtypes('dawnstrider', ['Dryad', 'Spellshaper']).
card_colors('dawnstrider', ['G']).
card_text('dawnstrider', '{G}, {T}, Discard a card: Prevent all combat damage that would be dealt this turn.').
card_mana_cost('dawnstrider', ['1', 'G']).
card_cmc('dawnstrider', 2).
card_layout('dawnstrider', 'normal').
card_power('dawnstrider', 1).
card_toughness('dawnstrider', 1).

% Found in: DDL, M14
card_name('dawnstrike paladin', 'Dawnstrike Paladin').
card_type('dawnstrike paladin', 'Creature — Human Knight').
card_types('dawnstrike paladin', ['Creature']).
card_subtypes('dawnstrike paladin', ['Human', 'Knight']).
card_colors('dawnstrike paladin', ['W']).
card_text('dawnstrike paladin', 'Vigilance (Attacking doesn\'t cause this creature to tap.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('dawnstrike paladin', ['3', 'W', 'W']).
card_cmc('dawnstrike paladin', 5).
card_layout('dawnstrike paladin', 'normal').
card_power('dawnstrike paladin', 2).
card_toughness('dawnstrike paladin', 4).

% Found in: DKA
card_name('dawntreader elk', 'Dawntreader Elk').
card_type('dawntreader elk', 'Creature — Elk').
card_types('dawntreader elk', ['Creature']).
card_subtypes('dawntreader elk', ['Elk']).
card_colors('dawntreader elk', ['G']).
card_text('dawntreader elk', '{G}, Sacrifice Dawntreader Elk: Search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.').
card_mana_cost('dawntreader elk', ['1', 'G']).
card_cmc('dawntreader elk', 2).
card_layout('dawntreader elk', 'normal').
card_power('dawntreader elk', 2).
card_toughness('dawntreader elk', 2).

% Found in: THS
card_name('daxos of meletis', 'Daxos of Meletis').
card_type('daxos of meletis', 'Legendary Creature — Human Soldier').
card_types('daxos of meletis', ['Creature']).
card_subtypes('daxos of meletis', ['Human', 'Soldier']).
card_supertypes('daxos of meletis', ['Legendary']).
card_colors('daxos of meletis', ['W', 'U']).
card_text('daxos of meletis', 'Daxos of Meletis can\'t be blocked by creatures with power 3 or greater.\nWhenever Daxos of Meletis deals combat damage to a player, exile the top card of that player\'s library. You gain life equal to that card\'s converted mana cost. Until end of turn, you may cast that card and you may spend mana as though it were mana of any color to cast it.').
card_mana_cost('daxos of meletis', ['1', 'W', 'U']).
card_cmc('daxos of meletis', 3).
card_layout('daxos of meletis', 'normal').
card_power('daxos of meletis', 2).
card_toughness('daxos of meletis', 2).

% Found in: APC
card_name('day', 'Day').
card_type('day', 'Instant').
card_types('day', ['Instant']).
card_subtypes('day', []).
card_colors('day', ['W']).
card_text('day', 'Creatures target player controls get +1/+1 until end of turn.').
card_mana_cost('day', ['2', 'W']).
card_cmc('day', 3).
card_layout('day', 'split').

% Found in: BOK
card_name('day of destiny', 'Day of Destiny').
card_type('day of destiny', 'Legendary Enchantment').
card_types('day of destiny', ['Enchantment']).
card_subtypes('day of destiny', []).
card_supertypes('day of destiny', ['Legendary']).
card_colors('day of destiny', ['W']).
card_text('day of destiny', 'Legendary creatures you control get +2/+2.').
card_mana_cost('day of destiny', ['3', 'W']).
card_cmc('day of destiny', 4).
card_layout('day of destiny', 'normal').

% Found in: M11, M12, ZEN, pMEI, pMPR
card_name('day of judgment', 'Day of Judgment').
card_type('day of judgment', 'Sorcery').
card_types('day of judgment', ['Sorcery']).
card_subtypes('day of judgment', []).
card_colors('day of judgment', ['W']).
card_text('day of judgment', 'Destroy all creatures.').
card_mana_cost('day of judgment', ['2', 'W', 'W']).
card_cmc('day of judgment', 4).
card_layout('day of judgment', 'normal').

% Found in: SCG
card_name('day of the dragons', 'Day of the Dragons').
card_type('day of the dragons', 'Enchantment').
card_types('day of the dragons', ['Enchantment']).
card_subtypes('day of the dragons', []).
card_colors('day of the dragons', ['U']).
card_text('day of the dragons', 'When Day of the Dragons enters the battlefield, exile all creatures you control. Then put that many 5/5 red Dragon creature tokens with flying onto the battlefield.\nWhen Day of the Dragons leaves the battlefield, sacrifice all Dragons you control. Then return the exiled cards to the battlefield under your control.').
card_mana_cost('day of the dragons', ['4', 'U', 'U', 'U']).
card_cmc('day of the dragons', 7).
card_layout('day of the dragons', 'normal').

% Found in: ORI
card_name('day\'s undoing', 'Day\'s Undoing').
card_type('day\'s undoing', 'Sorcery').
card_types('day\'s undoing', ['Sorcery']).
card_subtypes('day\'s undoing', []).
card_colors('day\'s undoing', ['U']).
card_text('day\'s undoing', 'Each player shuffles his or her hand and graveyard into his or her library, then draws seven cards. If it\'s your turn, end the turn. (Exile all spells and abilities on the stack, including this card. Discard down to your maximum hand size. Damage wears off, and \"this turn\" and \"until end of turn\" effects end.)').
card_mana_cost('day\'s undoing', ['2', 'U']).
card_cmc('day\'s undoing', 3).
card_layout('day\'s undoing', 'normal').

% Found in: FUT, MM2
card_name('daybreak coronet', 'Daybreak Coronet').
card_type('daybreak coronet', 'Enchantment — Aura').
card_types('daybreak coronet', ['Enchantment']).
card_subtypes('daybreak coronet', ['Aura']).
card_colors('daybreak coronet', ['W']).
card_text('daybreak coronet', 'Enchant creature with another Aura attached to it\nEnchanted creature gets +3/+3 and has first strike, vigilance, and lifelink. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_mana_cost('daybreak coronet', ['W', 'W']).
card_cmc('daybreak coronet', 2).
card_layout('daybreak coronet', 'normal').

% Found in: ISD
card_name('daybreak ranger', 'Daybreak Ranger').
card_type('daybreak ranger', 'Creature — Human Archer Werewolf').
card_types('daybreak ranger', ['Creature']).
card_subtypes('daybreak ranger', ['Human', 'Archer', 'Werewolf']).
card_colors('daybreak ranger', ['G']).
card_text('daybreak ranger', '{T}: Daybreak Ranger deals 2 damage to target creature with flying.\nAt the beginning of each upkeep, if no spells were cast last turn, transform Daybreak Ranger.').
card_mana_cost('daybreak ranger', ['2', 'G']).
card_cmc('daybreak ranger', 3).
card_layout('daybreak ranger', 'double-faced').
card_power('daybreak ranger', 2).
card_toughness('daybreak ranger', 2).
card_sides('daybreak ranger', 'nightfall predator').

% Found in: DD2, DD3_JVC, NMS
card_name('daze', 'Daze').
card_type('daze', 'Instant').
card_types('daze', ['Instant']).
card_subtypes('daze', []).
card_colors('daze', ['U']).
card_text('daze', 'You may return an Island you control to its owner\'s hand rather than pay Daze\'s mana cost.\nCounter target spell unless its controller pays {1}.').
card_mana_cost('daze', ['1', 'U']).
card_cmc('daze', 2).
card_layout('daze', 'normal').

% Found in: MIR
card_name('dazzling beauty', 'Dazzling Beauty').
card_type('dazzling beauty', 'Instant').
card_types('dazzling beauty', ['Instant']).
card_subtypes('dazzling beauty', []).
card_colors('dazzling beauty', ['W']).
card_text('dazzling beauty', 'Cast Dazzling Beauty only during the declare blockers step.\nTarget unblocked attacking creature becomes blocked. (This spell works on creatures that can\'t be blocked.)\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('dazzling beauty', ['2', 'W']).
card_cmc('dazzling beauty', 3).
card_layout('dazzling beauty', 'normal').

% Found in: KTK
card_name('dazzling ramparts', 'Dazzling Ramparts').
card_type('dazzling ramparts', 'Creature — Wall').
card_types('dazzling ramparts', ['Creature']).
card_subtypes('dazzling ramparts', ['Wall']).
card_colors('dazzling ramparts', ['W']).
card_text('dazzling ramparts', 'Defender\n{1}{W}, {T}: Tap target creature.').
card_mana_cost('dazzling ramparts', ['4', 'W']).
card_cmc('dazzling ramparts', 5).
card_layout('dazzling ramparts', 'normal').
card_power('dazzling ramparts', 0).
card_toughness('dazzling ramparts', 7).

% Found in: PLC
card_name('dead', 'Dead').
card_type('dead', 'Instant').
card_types('dead', ['Instant']).
card_subtypes('dead', []).
card_colors('dead', ['R']).
card_text('dead', 'Dead deals 2 damage to target creature.').
card_mana_cost('dead', ['R']).
card_cmc('dead', 1).
card_layout('dead', 'split').
card_sides('dead', 'gone').

% Found in: KTK
card_name('dead drop', 'Dead Drop').
card_type('dead drop', 'Sorcery').
card_types('dead drop', ['Sorcery']).
card_subtypes('dead drop', []).
card_colors('dead drop', ['B']).
card_text('dead drop', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nTarget player sacrifices two creatures.').
card_mana_cost('dead drop', ['9', 'B']).
card_cmc('dead drop', 10).
card_layout('dead drop', 'normal').

% Found in: WWK
card_name('dead reckoning', 'Dead Reckoning').
card_type('dead reckoning', 'Sorcery').
card_types('dead reckoning', ['Sorcery']).
card_subtypes('dead reckoning', []).
card_colors('dead reckoning', ['B']).
card_text('dead reckoning', 'You may put target creature card from your graveyard on top of your library. If you do, Dead Reckoning deals damage equal to that card\'s power to target creature.').
card_mana_cost('dead reckoning', ['1', 'B', 'B']).
card_cmc('dead reckoning', 3).
card_layout('dead reckoning', 'normal').

% Found in: RTR
card_name('dead reveler', 'Dead Reveler').
card_type('dead reveler', 'Creature — Zombie').
card_types('dead reveler', ['Creature']).
card_subtypes('dead reveler', ['Zombie']).
card_colors('dead reveler', ['B']).
card_text('dead reveler', 'Unleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_mana_cost('dead reveler', ['2', 'B']).
card_cmc('dead reveler', 3).
card_layout('dead reveler', 'normal').
card_power('dead reveler', 2).
card_toughness('dead reveler', 3).

% Found in: APC
card_name('dead ringers', 'Dead Ringers').
card_type('dead ringers', 'Sorcery').
card_types('dead ringers', ['Sorcery']).
card_subtypes('dead ringers', []).
card_colors('dead ringers', ['B']).
card_text('dead ringers', 'Destroy two target nonblack creatures unless either one is a color the other isn\'t. They can\'t be regenerated.').
card_mana_cost('dead ringers', ['4', 'B']).
card_cmc('dead ringers', 5).
card_layout('dead ringers', 'normal').

% Found in: ISD
card_name('dead weight', 'Dead Weight').
card_type('dead weight', 'Enchantment — Aura').
card_types('dead weight', ['Enchantment']).
card_subtypes('dead weight', ['Aura']).
card_colors('dead weight', ['B']).
card_text('dead weight', 'Enchant creature\nEnchanted creature gets -2/-2.').
card_mana_cost('dead weight', ['B']).
card_cmc('dead weight', 1).
card_layout('dead weight', 'normal').

% Found in: MRD
card_name('dead-iron sledge', 'Dead-Iron Sledge').
card_type('dead-iron sledge', 'Artifact — Equipment').
card_types('dead-iron sledge', ['Artifact']).
card_subtypes('dead-iron sledge', ['Equipment']).
card_colors('dead-iron sledge', []).
card_text('dead-iron sledge', 'Whenever equipped creature blocks or becomes blocked by a creature, destroy both creatures.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery. This card enters the battlefield unattached and stays on the battlefield if the creature leaves.)').
card_mana_cost('dead-iron sledge', ['1']).
card_cmc('dead-iron sledge', 1).
card_layout('dead-iron sledge', 'normal').

% Found in: PLS
card_name('deadapult', 'Deadapult').
card_type('deadapult', 'Enchantment').
card_types('deadapult', ['Enchantment']).
card_subtypes('deadapult', []).
card_colors('deadapult', ['R']).
card_text('deadapult', '{R}, Sacrifice a Zombie: Deadapult deals 2 damage to target creature or player.').
card_mana_cost('deadapult', ['2', 'R']).
card_cmc('deadapult', 3).
card_layout('deadapult', 'normal').

% Found in: DGM
card_name('deadbridge chant', 'Deadbridge Chant').
card_type('deadbridge chant', 'Enchantment').
card_types('deadbridge chant', ['Enchantment']).
card_subtypes('deadbridge chant', []).
card_colors('deadbridge chant', ['B', 'G']).
card_text('deadbridge chant', 'When Deadbridge Chant enters the battlefield, put the top ten cards of your library into your graveyard.\nAt the beginning of your upkeep, choose a card at random in your graveyard. If it\'s a creature card, put it onto the battlefield. Otherwise, put it into your hand.').
card_mana_cost('deadbridge chant', ['4', 'B', 'G']).
card_cmc('deadbridge chant', 6).
card_layout('deadbridge chant', 'normal').

% Found in: RTR, pLPA
card_name('deadbridge goliath', 'Deadbridge Goliath').
card_type('deadbridge goliath', 'Creature — Insect').
card_types('deadbridge goliath', ['Creature']).
card_subtypes('deadbridge goliath', ['Insect']).
card_colors('deadbridge goliath', ['G']).
card_text('deadbridge goliath', 'Scavenge {4}{G}{G} ({4}{G}{G}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_mana_cost('deadbridge goliath', ['2', 'G', 'G']).
card_cmc('deadbridge goliath', 4).
card_layout('deadbridge goliath', 'normal').
card_power('deadbridge goliath', 5).
card_toughness('deadbridge goliath', 5).

% Found in: ORI
card_name('deadbridge shaman', 'Deadbridge Shaman').
card_type('deadbridge shaman', 'Creature — Elf Shaman').
card_types('deadbridge shaman', ['Creature']).
card_subtypes('deadbridge shaman', ['Elf', 'Shaman']).
card_colors('deadbridge shaman', ['B']).
card_text('deadbridge shaman', 'When Deadbridge Shaman dies, target opponent discards a card.').
card_mana_cost('deadbridge shaman', ['2', 'B']).
card_cmc('deadbridge shaman', 3).
card_layout('deadbridge shaman', 'normal').
card_power('deadbridge shaman', 3).
card_toughness('deadbridge shaman', 1).

% Found in: AVR
card_name('deadeye navigator', 'Deadeye Navigator').
card_type('deadeye navigator', 'Creature — Spirit').
card_types('deadeye navigator', ['Creature']).
card_subtypes('deadeye navigator', ['Spirit']).
card_colors('deadeye navigator', ['U']).
card_text('deadeye navigator', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Deadeye Navigator is paired with another creature, each of those creatures has \"{1}{U}: Exile this creature, then return it to the battlefield under your control.\"').
card_mana_cost('deadeye navigator', ['4', 'U', 'U']).
card_cmc('deadeye navigator', 6).
card_layout('deadeye navigator', 'normal').
card_power('deadeye navigator', 5).
card_toughness('deadeye navigator', 5).

% Found in: LEG
card_name('deadfall', 'Deadfall').
card_type('deadfall', 'Enchantment').
card_types('deadfall', ['Enchantment']).
card_subtypes('deadfall', []).
card_colors('deadfall', ['G']).
card_text('deadfall', 'Creatures with forestwalk can be blocked as though they didn\'t have forestwalk.').
card_mana_cost('deadfall', ['2', 'G']).
card_cmc('deadfall', 3).
card_layout('deadfall', 'normal').

% Found in: UGL
card_name('deadhead', 'Deadhead').
card_type('deadhead', 'Creature — Zombie').
card_types('deadhead', ['Creature']).
card_subtypes('deadhead', ['Zombie']).
card_colors('deadhead', ['B']).
card_text('deadhead', 'Put Deadhead into play. Use this ability only if any opponent loses contact with his or her hand of cards and only if Deadhead is in your graveyard.').
card_mana_cost('deadhead', ['3', 'B']).
card_cmc('deadhead', 4).
card_layout('deadhead', 'normal').
card_power('deadhead', 3).
card_toughness('deadhead', 3).

% Found in: DKA
card_name('deadly allure', 'Deadly Allure').
card_type('deadly allure', 'Sorcery').
card_types('deadly allure', ['Sorcery']).
card_subtypes('deadly allure', []).
card_colors('deadly allure', ['B']).
card_text('deadly allure', 'Target creature gains deathtouch until end of turn and must be blocked this turn if able.\nFlashback {G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('deadly allure', ['B']).
card_cmc('deadly allure', 1).
card_layout('deadly allure', 'normal').

% Found in: PLC
card_name('deadly grub', 'Deadly Grub').
card_type('deadly grub', 'Creature — Insect').
card_types('deadly grub', ['Creature']).
card_subtypes('deadly grub', ['Insect']).
card_colors('deadly grub', ['B']).
card_text('deadly grub', 'Vanishing 3 (This permanent enters the battlefield with three time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Deadly Grub dies, if it had no time counters on it, put a 6/1 green Insect creature token with shroud onto the battlefield. (It can\'t be the target of spells or abilities.)').
card_mana_cost('deadly grub', ['2', 'B']).
card_cmc('deadly grub', 3).
card_layout('deadly grub', 'normal').
card_power('deadly grub', 3).
card_toughness('deadly grub', 1).

% Found in: ALL, BTD, CST, MMQ
card_name('deadly insect', 'Deadly Insect').
card_type('deadly insect', 'Creature — Insect').
card_types('deadly insect', ['Creature']).
card_subtypes('deadly insect', ['Insect']).
card_colors('deadly insect', ['G']).
card_text('deadly insect', 'Shroud (This creature can\'t be the target of spells or abilities.)').
card_mana_cost('deadly insect', ['4', 'G']).
card_cmc('deadly insect', 5).
card_layout('deadly insect', 'normal').
card_power('deadly insect', 6).
card_toughness('deadly insect', 1).

% Found in: CMD, DDL, M10, M13, M14
card_name('deadly recluse', 'Deadly Recluse').
card_type('deadly recluse', 'Creature — Spider').
card_types('deadly recluse', ['Creature']).
card_subtypes('deadly recluse', ['Spider']).
card_colors('deadly recluse', ['G']).
card_text('deadly recluse', 'Reach (This creature can block creatures with flying.)\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_mana_cost('deadly recluse', ['1', 'G']).
card_cmc('deadly recluse', 2).
card_layout('deadly recluse', 'normal').
card_power('deadly recluse', 1).
card_toughness('deadly recluse', 2).

% Found in: DTK
card_name('deadly wanderings', 'Deadly Wanderings').
card_type('deadly wanderings', 'Enchantment').
card_types('deadly wanderings', ['Enchantment']).
card_subtypes('deadly wanderings', []).
card_colors('deadly wanderings', ['B']).
card_text('deadly wanderings', 'As long as you control exactly one creature, that creature gets +2/+0 and has deathtouch and lifelink.').
card_mana_cost('deadly wanderings', ['3', 'B', 'B']).
card_cmc('deadly wanderings', 5).
card_layout('deadly wanderings', 'normal').

% Found in: TMP, TPR
card_name('deadshot', 'Deadshot').
card_type('deadshot', 'Sorcery').
card_types('deadshot', ['Sorcery']).
card_subtypes('deadshot', []).
card_colors('deadshot', ['R']).
card_text('deadshot', 'Tap target creature. It deals damage equal to its power to another target creature.').
card_mana_cost('deadshot', ['3', 'R']).
card_cmc('deadshot', 4).
card_layout('deadshot', 'normal').

% Found in: ARB
card_name('deadshot minotaur', 'Deadshot Minotaur').
card_type('deadshot minotaur', 'Creature — Minotaur').
card_types('deadshot minotaur', ['Creature']).
card_subtypes('deadshot minotaur', ['Minotaur']).
card_colors('deadshot minotaur', ['R', 'G']).
card_text('deadshot minotaur', 'When Deadshot Minotaur enters the battlefield, it deals 3 damage to target creature with flying.\nCycling {R/G} ({R/G}, Discard this card: Draw a card.)').
card_mana_cost('deadshot minotaur', ['3', 'R', 'G']).
card_cmc('deadshot minotaur', 5).
card_layout('deadshot minotaur', 'normal').
card_power('deadshot minotaur', 3).
card_toughness('deadshot minotaur', 4).

% Found in: C13, CMD, PLC
card_name('deadwood treefolk', 'Deadwood Treefolk').
card_type('deadwood treefolk', 'Creature — Treefolk').
card_types('deadwood treefolk', ['Creature']).
card_subtypes('deadwood treefolk', ['Treefolk']).
card_colors('deadwood treefolk', ['G']).
card_text('deadwood treefolk', 'Vanishing 3 (This permanent enters the battlefield with three time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Deadwood Treefolk enters the battlefield or leaves the battlefield, return another target creature card from your graveyard to your hand.').
card_mana_cost('deadwood treefolk', ['5', 'G']).
card_cmc('deadwood treefolk', 6).
card_layout('deadwood treefolk', 'normal').
card_power('deadwood treefolk', 3).
card_toughness('deadwood treefolk', 6).

% Found in: CNS
card_name('deal broker', 'Deal Broker').
card_type('deal broker', 'Artifact Creature — Construct').
card_types('deal broker', ['Artifact', 'Creature']).
card_subtypes('deal broker', ['Construct']).
card_colors('deal broker', []).
card_text('deal broker', 'Draft Deal Broker face up.\nImmediately after the draft, you may reveal a card in your card pool. Each other player may offer you one card in his or her card pool in exchange. You may accept any one offer.\n{T}: Draw a card, then discard a card.').
card_mana_cost('deal broker', ['3']).
card_cmc('deal broker', 3).
card_layout('deal broker', 'normal').
card_power('deal broker', 2).
card_toughness('deal broker', 3).

% Found in: UNH
card_name('deal damage', 'Deal Damage').
card_type('deal damage', 'Instant').
card_types('deal damage', ['Instant']).
card_subtypes('deal damage', []).
card_colors('deal damage', ['R']).
card_text('deal damage', 'Deal Damage deals 4 damage to target creature or player.\nGotcha Whenever an opponent says \"Deal\" or \"Damage,\" you may say \"Gotcha\" If you do, return Deal Damage from your graveyard to your hand.').
card_mana_cost('deal damage', ['2', 'R', 'R']).
card_cmc('deal damage', 4).
card_layout('deal damage', 'normal').

% Found in: ISD
card_name('dearly departed', 'Dearly Departed').
card_type('dearly departed', 'Creature — Spirit').
card_types('dearly departed', ['Creature']).
card_subtypes('dearly departed', ['Spirit']).
card_colors('dearly departed', ['W']).
card_text('dearly departed', 'Flying\nAs long as Dearly Departed is in your graveyard, each Human creature you control enters the battlefield with an additional +1/+1 counter on it.').
card_mana_cost('dearly departed', ['4', 'W', 'W']).
card_cmc('dearly departed', 6).
card_layout('dearly departed', 'normal').
card_power('dearly departed', 5).
card_toughness('dearly departed', 5).

% Found in: APC, DDJ, pFNM
card_name('death', 'Death').
card_type('death', 'Sorcery').
card_types('death', ['Sorcery']).
card_subtypes('death', []).
card_colors('death', ['B']).
card_text('death', 'Return target creature card from your graveyard to the battlefield. You lose life equal to its converted mana cost.').
card_mana_cost('death', ['1', 'B']).
card_cmc('death', 2).
card_layout('death', 'split').

% Found in: ALA, HOP
card_name('death baron', 'Death Baron').
card_type('death baron', 'Creature — Zombie Wizard').
card_types('death baron', ['Creature']).
card_subtypes('death baron', ['Zombie', 'Wizard']).
card_colors('death baron', ['B']).
card_text('death baron', 'Skeleton creatures you control and other Zombie creatures you control get +1/+1 and have deathtouch.').
card_mana_cost('death baron', ['1', 'B', 'B']).
card_cmc('death baron', 3).
card_layout('death baron', 'normal').
card_power('death baron', 2).
card_toughness('death baron', 2).

% Found in: PLS
card_name('death bomb', 'Death Bomb').
card_type('death bomb', 'Instant').
card_types('death bomb', ['Instant']).
card_subtypes('death bomb', []).
card_colors('death bomb', ['B']).
card_text('death bomb', 'As an additional cost to cast Death Bomb, sacrifice a creature.\nDestroy target nonblack creature. It can\'t be regenerated. Its controller loses 2 life.').
card_mana_cost('death bomb', ['3', 'B']).
card_cmc('death bomb', 4).
card_layout('death bomb', 'normal').

% Found in: CMD
card_name('death by dragons', 'Death by Dragons').
card_type('death by dragons', 'Sorcery').
card_types('death by dragons', ['Sorcery']).
card_subtypes('death by dragons', []).
card_colors('death by dragons', ['R']).
card_text('death by dragons', 'Each player other than target player puts a 5/5 red Dragon creature token with flying onto the battlefield.').
card_mana_cost('death by dragons', ['4', 'R', 'R']).
card_cmc('death by dragons', 6).
card_layout('death by dragons', 'normal').

% Found in: PCY
card_name('death charmer', 'Death Charmer').
card_type('death charmer', 'Creature — Worm Mercenary').
card_types('death charmer', ['Creature']).
card_subtypes('death charmer', ['Worm', 'Mercenary']).
card_colors('death charmer', ['B']).
card_text('death charmer', 'Whenever Death Charmer deals combat damage to a creature, that creature\'s controller loses 2 life unless he or she pays {2}.').
card_mana_cost('death charmer', ['2', 'B']).
card_cmc('death charmer', 3).
card_layout('death charmer', 'normal').
card_power('death charmer', 2).
card_toughness('death charmer', 2).

% Found in: DST, MMA
card_name('death cloud', 'Death Cloud').
card_type('death cloud', 'Sorcery').
card_types('death cloud', ['Sorcery']).
card_subtypes('death cloud', []).
card_colors('death cloud', ['B']).
card_text('death cloud', 'Each player loses X life, discards X cards, sacrifices X creatures, then sacrifices X lands.').
card_mana_cost('death cloud', ['X', 'B', 'B', 'B']).
card_cmc('death cloud', 3).
card_layout('death cloud', 'normal').

% Found in: ROE
card_name('death cultist', 'Death Cultist').
card_type('death cultist', 'Creature — Human Wizard').
card_types('death cultist', ['Creature']).
card_subtypes('death cultist', ['Human', 'Wizard']).
card_colors('death cultist', ['B']).
card_text('death cultist', 'Sacrifice Death Cultist: Target player loses 1 life and you gain 1 life.').
card_mana_cost('death cultist', ['B']).
card_cmc('death cultist', 1).
card_layout('death cultist', 'normal').
card_power('death cultist', 1).
card_toughness('death cultist', 1).

% Found in: MM2, MMA, SOK
card_name('death denied', 'Death Denied').
card_type('death denied', 'Instant — Arcane').
card_types('death denied', ['Instant']).
card_subtypes('death denied', ['Arcane']).
card_colors('death denied', ['B']).
card_text('death denied', 'Return X target creature cards from your graveyard to your hand.').
card_mana_cost('death denied', ['X', 'B', 'B']).
card_cmc('death denied', 2).
card_layout('death denied', 'normal').

% Found in: KTK
card_name('death frenzy', 'Death Frenzy').
card_type('death frenzy', 'Sorcery').
card_types('death frenzy', ['Sorcery']).
card_subtypes('death frenzy', []).
card_colors('death frenzy', ['B', 'G']).
card_text('death frenzy', 'All creatures get -2/-2 until end of turn. Whenever a creature dies this turn, you gain 1 life.').
card_mana_cost('death frenzy', ['3', 'B', 'G']).
card_cmc('death frenzy', 5).
card_layout('death frenzy', 'normal').

% Found in: APC, C13, DDK, VMA
card_name('death grasp', 'Death Grasp').
card_type('death grasp', 'Sorcery').
card_types('death grasp', ['Sorcery']).
card_subtypes('death grasp', []).
card_colors('death grasp', ['W', 'B']).
card_text('death grasp', 'Death Grasp deals X damage to target creature or player. You gain X life.').
card_mana_cost('death grasp', ['X', 'W', 'B']).
card_cmc('death grasp', 2).
card_layout('death grasp', 'normal').

% Found in: ONS
card_name('death match', 'Death Match').
card_type('death match', 'Enchantment').
card_types('death match', ['Enchantment']).
card_subtypes('death match', []).
card_colors('death match', ['B']).
card_text('death match', 'Whenever a creature enters the battlefield, that creature\'s controller may have target creature of his or her choice get -3/-3 until end of turn.').
card_mana_cost('death match', ['3', 'B']).
card_cmc('death match', 4).
card_layout('death match', 'normal').

% Found in: APC, CMD
card_name('death mutation', 'Death Mutation').
card_type('death mutation', 'Sorcery').
card_types('death mutation', ['Sorcery']).
card_subtypes('death mutation', []).
card_colors('death mutation', ['B', 'G']).
card_text('death mutation', 'Destroy target nonblack creature. It can\'t be regenerated. Put X 1/1 green Saproling creature tokens onto the battlefield, where X is that creature\'s converted mana cost.').
card_mana_cost('death mutation', ['6', 'B', 'G']).
card_cmc('death mutation', 8).
card_layout('death mutation', 'normal').

% Found in: SOK
card_name('death of a thousand stings', 'Death of a Thousand Stings').
card_type('death of a thousand stings', 'Instant — Arcane').
card_types('death of a thousand stings', ['Instant']).
card_subtypes('death of a thousand stings', ['Arcane']).
card_colors('death of a thousand stings', ['B']).
card_text('death of a thousand stings', 'Target player loses 1 life and you gain 1 life.\nAt the beginning of your upkeep, if you have more cards in hand than each opponent, you may return Death of a Thousand Stings from your graveyard to your hand.').
card_mana_cost('death of a thousand stings', ['4', 'B']).
card_cmc('death of a thousand stings', 5).
card_layout('death of a thousand stings', 'normal').

% Found in: INV
card_name('death or glory', 'Death or Glory').
card_type('death or glory', 'Sorcery').
card_types('death or glory', ['Sorcery']).
card_subtypes('death or glory', []).
card_colors('death or glory', ['W']).
card_text('death or glory', 'Separate all creature cards in your graveyard into two piles. Exile the pile of an opponent\'s choice and return the other to the battlefield.').
card_mana_cost('death or glory', ['4', 'W']).
card_cmc('death or glory', 5).
card_layout('death or glory', 'normal').

% Found in: 8ED, NMS
card_name('death pit offering', 'Death Pit Offering').
card_type('death pit offering', 'Enchantment').
card_types('death pit offering', ['Enchantment']).
card_subtypes('death pit offering', []).
card_colors('death pit offering', ['B']).
card_text('death pit offering', 'When Death Pit Offering enters the battlefield, sacrifice all creatures you control.\nCreatures you control get +2/+2.').
card_mana_cost('death pit offering', ['2', 'B', 'B']).
card_cmc('death pit offering', 4).
card_layout('death pit offering', 'normal').

% Found in: 8ED, 9ED, TMP, TPR
card_name('death pits of rath', 'Death Pits of Rath').
card_type('death pits of rath', 'Enchantment').
card_types('death pits of rath', ['Enchantment']).
card_subtypes('death pits of rath', []).
card_colors('death pits of rath', ['B']).
card_text('death pits of rath', 'Whenever a creature is dealt damage, destroy it. It can\'t be regenerated.').
card_mana_cost('death pits of rath', ['3', 'B', 'B']).
card_cmc('death pits of rath', 5).
card_layout('death pits of rath', 'normal').

% Found in: ONS
card_name('death pulse', 'Death Pulse').
card_type('death pulse', 'Instant').
card_types('death pulse', ['Instant']).
card_subtypes('death pulse', []).
card_colors('death pulse', ['B']).
card_text('death pulse', 'Target creature gets -4/-4 until end of turn.\nCycling {1}{B}{B} ({1}{B}{B}, Discard this card: Draw a card.)\nWhen you cycle Death Pulse, you may have target creature get -1/-1 until end of turn.').
card_mana_cost('death pulse', ['2', 'B', 'B']).
card_cmc('death pulse', 4).
card_layout('death pulse', 'normal').

% Found in: FUT, MMA
card_name('death rattle', 'Death Rattle').
card_type('death rattle', 'Instant').
card_types('death rattle', ['Instant']).
card_subtypes('death rattle', []).
card_colors('death rattle', ['B']).
card_text('death rattle', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nDestroy target nongreen creature. It can\'t be regenerated.').
card_mana_cost('death rattle', ['5', 'B']).
card_cmc('death rattle', 6).
card_layout('death rattle', 'normal').

% Found in: ALL, CST, DKM, ME2
card_name('death spark', 'Death Spark').
card_type('death spark', 'Instant').
card_types('death spark', ['Instant']).
card_subtypes('death spark', []).
card_colors('death spark', ['R']).
card_text('death spark', 'Death Spark deals 1 damage to target creature or player.\nAt the beginning of your upkeep, if Death Spark is in your graveyard with a creature card directly above it, you may pay {1}. If you do, return Death Spark to your hand.').
card_mana_cost('death spark', ['R']).
card_cmc('death spark', 1).
card_layout('death spark', 'normal').

% Found in: 5ED, HML, MED
card_name('death speakers', 'Death Speakers').
card_type('death speakers', 'Creature — Human Cleric').
card_types('death speakers', ['Creature']).
card_subtypes('death speakers', ['Human', 'Cleric']).
card_colors('death speakers', ['W']).
card_text('death speakers', 'Protection from black').
card_mana_cost('death speakers', ['W']).
card_cmc('death speakers', 1).
card_layout('death speakers', 'normal').
card_power('death speakers', 1).
card_toughness('death speakers', 1).

% Found in: BTD, STH, TPR
card_name('death stroke', 'Death Stroke').
card_type('death stroke', 'Sorcery').
card_types('death stroke', ['Sorcery']).
card_subtypes('death stroke', []).
card_colors('death stroke', ['B']).
card_text('death stroke', 'Destroy target tapped creature.').
card_mana_cost('death stroke', ['B', 'B']).
card_cmc('death stroke', 2).
card_layout('death stroke', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, ICE, LEA, LEB, MED
card_name('death ward', 'Death Ward').
card_type('death ward', 'Instant').
card_types('death ward', ['Instant']).
card_subtypes('death ward', []).
card_colors('death ward', ['W']).
card_text('death ward', 'Regenerate target creature.').
card_mana_cost('death ward', ['W']).
card_cmc('death ward', 1).
card_layout('death ward', 'normal').

% Found in: VIS
card_name('death watch', 'Death Watch').
card_type('death watch', 'Enchantment — Aura').
card_types('death watch', ['Enchantment']).
card_subtypes('death watch', ['Aura']).
card_colors('death watch', ['B']).
card_text('death watch', 'Enchant creature\nWhen enchanted creature dies, its controller loses life equal to its power and you gain life equal to its toughness.').
card_mana_cost('death watch', ['B']).
card_cmc('death watch', 1).
card_layout('death watch', 'normal').

% Found in: AVR, DTK
card_name('death wind', 'Death Wind').
card_type('death wind', 'Instant').
card_types('death wind', ['Instant']).
card_subtypes('death wind', []).
card_colors('death wind', ['B']).
card_text('death wind', 'Target creature gets -X/-X until end of turn.').
card_mana_cost('death wind', ['X', 'B']).
card_cmc('death wind', 1).
card_layout('death wind', 'normal').

% Found in: JUD
card_name('death wish', 'Death Wish').
card_type('death wish', 'Sorcery').
card_types('death wish', ['Sorcery']).
card_subtypes('death wish', []).
card_colors('death wish', ['B']).
card_text('death wish', 'You may choose a card you own from outside the game and put it into your hand. You lose half your life, rounded up. Exile Death Wish.').
card_mana_cost('death wish', ['1', 'B', 'B']).
card_cmc('death wish', 3).
card_layout('death wish', 'normal').

% Found in: GTC
card_name('death\'s approach', 'Death\'s Approach').
card_type('death\'s approach', 'Enchantment — Aura').
card_types('death\'s approach', ['Enchantment']).
card_subtypes('death\'s approach', ['Aura']).
card_colors('death\'s approach', ['B']).
card_text('death\'s approach', 'Enchant creature\nEnchanted creature gets -X/-X, where X is the number of creature cards in its controller\'s graveyard.').
card_mana_cost('death\'s approach', ['B']).
card_cmc('death\'s approach', 1).
card_layout('death\'s approach', 'normal').

% Found in: DKA
card_name('death\'s caress', 'Death\'s Caress').
card_type('death\'s caress', 'Sorcery').
card_types('death\'s caress', ['Sorcery']).
card_subtypes('death\'s caress', []).
card_colors('death\'s caress', ['B']).
card_text('death\'s caress', 'Destroy target creature. If that creature was a Human, you gain life equal to its toughness.').
card_mana_cost('death\'s caress', ['3', 'B', 'B']).
card_cmc('death\'s caress', 5).
card_layout('death\'s caress', 'normal').

% Found in: EXO, TPR
card_name('death\'s duet', 'Death\'s Duet').
card_type('death\'s duet', 'Sorcery').
card_types('death\'s duet', ['Sorcery']).
card_subtypes('death\'s duet', []).
card_colors('death\'s duet', ['B']).
card_text('death\'s duet', 'Return two target creature cards from your graveyard to your hand.').
card_mana_cost('death\'s duet', ['2', 'B']).
card_cmc('death\'s duet', 3).
card_layout('death\'s duet', 'normal').

% Found in: RTR
card_name('death\'s presence', 'Death\'s Presence').
card_type('death\'s presence', 'Enchantment').
card_types('death\'s presence', ['Enchantment']).
card_subtypes('death\'s presence', []).
card_colors('death\'s presence', ['G']).
card_text('death\'s presence', 'Whenever a creature you control dies, put X +1/+1 counters on target creature you control, where X is the power of the creature that died.').
card_mana_cost('death\'s presence', ['5', 'G']).
card_cmc('death\'s presence', 6).
card_layout('death\'s presence', 'normal').

% Found in: WWK
card_name('death\'s shadow', 'Death\'s Shadow').
card_type('death\'s shadow', 'Creature — Avatar').
card_types('death\'s shadow', ['Creature']).
card_subtypes('death\'s shadow', ['Avatar']).
card_colors('death\'s shadow', ['B']).
card_text('death\'s shadow', 'Death\'s Shadow gets -X/-X, where X is your life total.').
card_mana_cost('death\'s shadow', ['B']).
card_cmc('death\'s shadow', 1).
card_layout('death\'s shadow', 'normal').
card_power('death\'s shadow', 13).
card_toughness('death\'s shadow', 13).

% Found in: SCG, VMA
card_name('death\'s-head buzzard', 'Death\'s-Head Buzzard').
card_type('death\'s-head buzzard', 'Creature — Bird').
card_types('death\'s-head buzzard', ['Creature']).
card_subtypes('death\'s-head buzzard', ['Bird']).
card_colors('death\'s-head buzzard', ['B']).
card_text('death\'s-head buzzard', 'Flying\nWhen Death\'s-Head Buzzard dies, all creatures get -1/-1 until end of turn.').
card_mana_cost('death\'s-head buzzard', ['1', 'B', 'B']).
card_cmc('death\'s-head buzzard', 3).
card_layout('death\'s-head buzzard', 'normal').
card_power('death\'s-head buzzard', 2).
card_toughness('death\'s-head buzzard', 1).

% Found in: DDM, NPH
card_name('death-hood cobra', 'Death-Hood Cobra').
card_type('death-hood cobra', 'Creature — Snake').
card_types('death-hood cobra', ['Creature']).
card_subtypes('death-hood cobra', ['Snake']).
card_colors('death-hood cobra', ['G']).
card_text('death-hood cobra', '{1}{G}: Death-Hood Cobra gains reach until end of turn. (It can block creatures with flying.)\n{1}{G}: Death-Hood Cobra gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_mana_cost('death-hood cobra', ['1', 'G']).
card_cmc('death-hood cobra', 2).
card_layout('death-hood cobra', 'normal').
card_power('death-hood cobra', 2).
card_toughness('death-hood cobra', 2).

% Found in: DST
card_name('death-mask duplicant', 'Death-Mask Duplicant').
card_type('death-mask duplicant', 'Artifact Creature — Shapeshifter').
card_types('death-mask duplicant', ['Artifact', 'Creature']).
card_subtypes('death-mask duplicant', ['Shapeshifter']).
card_colors('death-mask duplicant', []).
card_text('death-mask duplicant', 'Imprint — {1}: Exile target creature card from your graveyard.\nAs long as a card exiled with Death-Mask Duplicant has flying, Death-Mask Duplicant has flying. The same is true for fear, first strike, double strike, haste, landwalk, protection, and trample.').
card_mana_cost('death-mask duplicant', ['7']).
card_cmc('death-mask duplicant', 7).
card_layout('death-mask duplicant', 'normal').
card_power('death-mask duplicant', 5).
card_toughness('death-mask duplicant', 5).

% Found in: THS
card_name('deathbellow raider', 'Deathbellow Raider').
card_type('deathbellow raider', 'Creature — Minotaur Berserker').
card_types('deathbellow raider', ['Creature']).
card_subtypes('deathbellow raider', ['Minotaur', 'Berserker']).
card_colors('deathbellow raider', ['R']).
card_text('deathbellow raider', 'Deathbellow Raider attacks each turn if able.\n{2}{B}: Regenerate Deathbellow Raider.').
card_mana_cost('deathbellow raider', ['1', 'R']).
card_cmc('deathbellow raider', 2).
card_layout('deathbellow raider', 'normal').
card_power('deathbellow raider', 2).
card_toughness('deathbellow raider', 3).

% Found in: EVE
card_name('deathbringer liege', 'Deathbringer Liege').
card_type('deathbringer liege', 'Creature — Horror').
card_types('deathbringer liege', ['Creature']).
card_subtypes('deathbringer liege', ['Horror']).
card_colors('deathbringer liege', ['W', 'B']).
card_text('deathbringer liege', 'Other white creatures you control get +1/+1.\nOther black creatures you control get +1/+1.\nWhenever you cast a white spell, you may tap target creature.\nWhenever you cast a black spell, you may destroy target creature if it\'s tapped.').
card_mana_cost('deathbringer liege', ['2', 'W/B', 'W/B', 'W/B']).
card_cmc('deathbringer liege', 5).
card_layout('deathbringer liege', 'normal').
card_power('deathbringer liege', 3).
card_toughness('deathbringer liege', 4).

% Found in: DTK, pLPA
card_name('deathbringer regent', 'Deathbringer Regent').
card_type('deathbringer regent', 'Creature — Dragon').
card_types('deathbringer regent', ['Creature']).
card_subtypes('deathbringer regent', ['Dragon']).
card_colors('deathbringer regent', ['B']).
card_text('deathbringer regent', 'Flying\nWhen Deathbringer Regent enters the battlefield, if you cast it from your hand and there are five or more other creatures on the battlefield, destroy all other creatures.').
card_mana_cost('deathbringer regent', ['5', 'B', 'B']).
card_cmc('deathbringer regent', 7).
card_layout('deathbringer regent', 'normal').
card_power('deathbringer regent', 5).
card_toughness('deathbringer regent', 6).

% Found in: ARB, C13
card_name('deathbringer thoctar', 'Deathbringer Thoctar').
card_type('deathbringer thoctar', 'Creature — Zombie Beast').
card_types('deathbringer thoctar', ['Creature']).
card_subtypes('deathbringer thoctar', ['Zombie', 'Beast']).
card_colors('deathbringer thoctar', ['B', 'R']).
card_text('deathbringer thoctar', 'Whenever another creature dies, you may put a +1/+1 counter on Deathbringer Thoctar.\nRemove a +1/+1 counter from Deathbringer Thoctar: Deathbringer Thoctar deals 1 damage to target creature or player.').
card_mana_cost('deathbringer thoctar', ['4', 'B', 'R']).
card_cmc('deathbringer thoctar', 6).
card_layout('deathbringer thoctar', 'normal').
card_power('deathbringer thoctar', 3).
card_toughness('deathbringer thoctar', 3).

% Found in: ME4, PO2
card_name('deathcoil wurm', 'Deathcoil Wurm').
card_type('deathcoil wurm', 'Creature — Wurm').
card_types('deathcoil wurm', ['Creature']).
card_subtypes('deathcoil wurm', ['Wurm']).
card_colors('deathcoil wurm', ['G']).
card_text('deathcoil wurm', 'You may have Deathcoil Wurm assign its combat damage as though it weren\'t blocked.').
card_mana_cost('deathcoil wurm', ['6', 'G', 'G']).
card_cmc('deathcoil wurm', 8).
card_layout('deathcoil wurm', 'normal').
card_power('deathcoil wurm', 7).
card_toughness('deathcoil wurm', 6).

% Found in: GTC
card_name('deathcult rogue', 'Deathcult Rogue').
card_type('deathcult rogue', 'Creature — Human Rogue').
card_types('deathcult rogue', ['Creature']).
card_subtypes('deathcult rogue', ['Human', 'Rogue']).
card_colors('deathcult rogue', ['U', 'B']).
card_text('deathcult rogue', 'Deathcult Rogue can\'t be blocked except by Rogues.').
card_mana_cost('deathcult rogue', ['1', 'U/B', 'U/B']).
card_cmc('deathcult rogue', 3).
card_layout('deathcult rogue', 'normal').
card_power('deathcult rogue', 2).
card_toughness('deathcult rogue', 2).

% Found in: CHK
card_name('deathcurse ogre', 'Deathcurse Ogre').
card_type('deathcurse ogre', 'Creature — Ogre Warrior').
card_types('deathcurse ogre', ['Creature']).
card_subtypes('deathcurse ogre', ['Ogre', 'Warrior']).
card_colors('deathcurse ogre', ['B']).
card_text('deathcurse ogre', 'When Deathcurse Ogre dies, each player loses 3 life.').
card_mana_cost('deathcurse ogre', ['5', 'B']).
card_cmc('deathcurse ogre', 6).
card_layout('deathcurse ogre', 'normal').
card_power('deathcurse ogre', 3).
card_toughness('deathcurse ogre', 3).

% Found in: CNS, WWK
card_name('deathforge shaman', 'Deathforge Shaman').
card_type('deathforge shaman', 'Creature — Ogre Shaman').
card_types('deathforge shaman', ['Creature']).
card_subtypes('deathforge shaman', ['Ogre', 'Shaman']).
card_colors('deathforge shaman', ['R']).
card_text('deathforge shaman', 'Multikicker {R} (You may pay an additional {R} any number of times as you cast this spell.)\nWhen Deathforge Shaman enters the battlefield, it deals damage to target player equal to twice the number of times it was kicked.').
card_mana_cost('deathforge shaman', ['4', 'R']).
card_cmc('deathforge shaman', 5).
card_layout('deathforge shaman', 'normal').
card_power('deathforge shaman', 4).
card_toughness('deathforge shaman', 3).

% Found in: M14
card_name('deathgaze cockatrice', 'Deathgaze Cockatrice').
card_type('deathgaze cockatrice', 'Creature — Cockatrice').
card_types('deathgaze cockatrice', ['Creature']).
card_subtypes('deathgaze cockatrice', ['Cockatrice']).
card_colors('deathgaze cockatrice', ['B']).
card_text('deathgaze cockatrice', 'Flying\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_mana_cost('deathgaze cockatrice', ['2', 'B', 'B']).
card_cmc('deathgaze cockatrice', 4).
card_layout('deathgaze cockatrice', 'normal').
card_power('deathgaze cockatrice', 2).
card_toughness('deathgaze cockatrice', 2).

% Found in: 8ED, 9ED, MMQ
card_name('deathgazer', 'Deathgazer').
card_type('deathgazer', 'Creature — Lizard').
card_types('deathgazer', ['Creature']).
card_subtypes('deathgazer', ['Lizard']).
card_colors('deathgazer', ['B']).
card_text('deathgazer', 'Whenever Deathgazer blocks or becomes blocked by a nonblack creature, destroy that creature at end of combat.').
card_mana_cost('deathgazer', ['3', 'B']).
card_cmc('deathgazer', 4).
card_layout('deathgazer', 'normal').
card_power('deathgazer', 2).
card_toughness('deathgazer', 2).

% Found in: ALA, DD3_GVL, DDD
card_name('deathgreeter', 'Deathgreeter').
card_type('deathgreeter', 'Creature — Human Shaman').
card_types('deathgreeter', ['Creature']).
card_subtypes('deathgreeter', ['Human', 'Shaman']).
card_colors('deathgreeter', ['B']).
card_text('deathgreeter', 'Whenever another creature dies, you may gain 1 life.').
card_mana_cost('deathgreeter', ['B']).
card_cmc('deathgreeter', 1).
card_layout('deathgreeter', 'normal').
card_power('deathgreeter', 1).
card_toughness('deathgreeter', 1).

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, ME4
card_name('deathgrip', 'Deathgrip').
card_type('deathgrip', 'Enchantment').
card_types('deathgrip', ['Enchantment']).
card_subtypes('deathgrip', []).
card_colors('deathgrip', ['B']).
card_text('deathgrip', '{B}{B}: Counter target green spell.').
card_mana_cost('deathgrip', ['B', 'B']).
card_cmc('deathgrip', 2).
card_layout('deathgrip', 'normal').

% Found in: SOK
card_name('deathknell kami', 'Deathknell Kami').
card_type('deathknell kami', 'Creature — Spirit').
card_types('deathknell kami', ['Creature']).
card_subtypes('deathknell kami', ['Spirit']).
card_colors('deathknell kami', ['B']).
card_text('deathknell kami', 'Flying\n{2}: Deathknell Kami gets +1/+1 until end of turn. Sacrifice it at the beginning of the next end step.\nSoulshift 1 (When this creature dies, you may return target Spirit card with converted mana cost 1 or less from your graveyard to your hand.)').
card_mana_cost('deathknell kami', ['1', 'B']).
card_cmc('deathknell kami', 2).
card_layout('deathknell kami', 'normal').
card_power('deathknell kami', 0).
card_toughness('deathknell kami', 1).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('deathlace', 'Deathlace').
card_type('deathlace', 'Instant').
card_types('deathlace', ['Instant']).
card_subtypes('deathlace', []).
card_colors('deathlace', ['B']).
card_text('deathlace', 'Target spell or permanent becomes black. (Mana symbols on that permanent remain unchanged.)').
card_mana_cost('deathlace', ['B']).
card_cmc('deathlace', 1).
card_layout('deathlace', 'normal').

% Found in: ROE, pWPN
card_name('deathless angel', 'Deathless Angel').
card_type('deathless angel', 'Creature — Angel').
card_types('deathless angel', ['Creature']).
card_subtypes('deathless angel', ['Angel']).
card_colors('deathless angel', ['W']).
card_text('deathless angel', 'Flying\n{W}{W}: Target creature gains indestructible until end of turn.').
card_mana_cost('deathless angel', ['4', 'W', 'W']).
card_cmc('deathless angel', 6).
card_layout('deathless angel', 'normal').
card_power('deathless angel', 5).
card_toughness('deathless angel', 7).

% Found in: BFZ
card_name('deathless behemoth', 'Deathless Behemoth').
card_type('deathless behemoth', 'Creature — Eldrazi').
card_types('deathless behemoth', ['Creature']).
card_subtypes('deathless behemoth', ['Eldrazi']).
card_colors('deathless behemoth', []).
card_text('deathless behemoth', 'Vigilance\nSacrifice two Eldrazi Scions: Return Deathless Behemoth from your graveyard to your hand. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('deathless behemoth', ['6']).
card_cmc('deathless behemoth', 6).
card_layout('deathless behemoth', 'normal').
card_power('deathless behemoth', 6).
card_toughness('deathless behemoth', 6).

% Found in: 10E, CSP, M10, M11, M12, MM2
card_name('deathmark', 'Deathmark').
card_type('deathmark', 'Sorcery').
card_types('deathmark', ['Sorcery']).
card_subtypes('deathmark', []).
card_colors('deathmark', ['B']).
card_text('deathmark', 'Destroy target green or white creature.').
card_mana_cost('deathmark', ['B']).
card_cmc('deathmark', 1).
card_layout('deathmark', 'normal').

% Found in: LGN
card_name('deathmark prelate', 'Deathmark Prelate').
card_type('deathmark prelate', 'Creature — Human Cleric').
card_types('deathmark prelate', ['Creature']).
card_subtypes('deathmark prelate', ['Human', 'Cleric']).
card_colors('deathmark prelate', ['B']).
card_text('deathmark prelate', '{2}{B}, {T}, Sacrifice a Zombie: Destroy target non-Zombie creature. It can\'t be regenerated. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('deathmark prelate', ['3', 'B']).
card_cmc('deathmark prelate', 4).
card_layout('deathmark prelate', 'normal').
card_power('deathmark prelate', 2).
card_toughness('deathmark prelate', 3).

% Found in: SOK
card_name('deathmask nezumi', 'Deathmask Nezumi').
card_type('deathmask nezumi', 'Creature — Rat Shaman').
card_types('deathmask nezumi', ['Creature']).
card_subtypes('deathmask nezumi', ['Rat', 'Shaman']).
card_colors('deathmask nezumi', ['B']).
card_text('deathmask nezumi', 'As long as you have seven or more cards in hand, Deathmask Nezumi gets +2/+1 and has fear. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('deathmask nezumi', ['2', 'B']).
card_cmc('deathmask nezumi', 3).
card_layout('deathmask nezumi', 'normal').
card_power('deathmask nezumi', 2).
card_toughness('deathmask nezumi', 2).

% Found in: DTK
card_name('deathmist raptor', 'Deathmist Raptor').
card_type('deathmist raptor', 'Creature — Lizard Beast').
card_types('deathmist raptor', ['Creature']).
card_subtypes('deathmist raptor', ['Lizard', 'Beast']).
card_colors('deathmist raptor', ['G']).
card_text('deathmist raptor', 'Deathtouch\nWhenever a permanent you control is turned face up, you may return Deathmist Raptor from your graveyard to the battlefield face up or face down.\nMegamorph {4}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_mana_cost('deathmist raptor', ['1', 'G', 'G']).
card_cmc('deathmist raptor', 3).
card_layout('deathmist raptor', 'normal').
card_power('deathmist raptor', 3).
card_toughness('deathmist raptor', 3).

% Found in: GTC
card_name('deathpact angel', 'Deathpact Angel').
card_type('deathpact angel', 'Creature — Angel').
card_types('deathpact angel', ['Creature']).
card_subtypes('deathpact angel', ['Angel']).
card_colors('deathpact angel', ['W', 'B']).
card_text('deathpact angel', 'Flying\nWhen Deathpact Angel dies, put a 1/1 white and black Cleric creature token onto the battlefield. It has \"{3}{W}{B}{B}, {T}, Sacrifice this creature: Return a card named Deathpact Angel from your graveyard to the battlefield.\"').
card_mana_cost('deathpact angel', ['3', 'W', 'B', 'B']).
card_cmc('deathpact angel', 6).
card_layout('deathpact angel', 'normal').
card_power('deathpact angel', 5).
card_toughness('deathpact angel', 5).

% Found in: CNS, VMA
card_name('deathreap ritual', 'Deathreap Ritual').
card_type('deathreap ritual', 'Enchantment').
card_types('deathreap ritual', ['Enchantment']).
card_subtypes('deathreap ritual', []).
card_colors('deathreap ritual', ['B', 'G']).
card_text('deathreap ritual', 'Morbid — At the beginning of each end step, if a creature died this turn, you may draw a card.').
card_mana_cost('deathreap ritual', ['2', 'B', 'G']).
card_cmc('deathreap ritual', 4).
card_layout('deathreap ritual', 'normal').

% Found in: CNS, LRW
card_name('deathrender', 'Deathrender').
card_type('deathrender', 'Artifact — Equipment').
card_types('deathrender', ['Artifact']).
card_subtypes('deathrender', ['Equipment']).
card_colors('deathrender', []).
card_text('deathrender', 'Equipped creature gets +2/+2.\nWhenever equipped creature dies, you may put a creature card from your hand onto the battlefield and attach Deathrender to it.\nEquip {2}').
card_mana_cost('deathrender', ['4']).
card_cmc('deathrender', 4).
card_layout('deathrender', 'normal').

% Found in: RTR
card_name('deathrite shaman', 'Deathrite Shaman').
card_type('deathrite shaman', 'Creature — Elf Shaman').
card_types('deathrite shaman', ['Creature']).
card_subtypes('deathrite shaman', ['Elf', 'Shaman']).
card_colors('deathrite shaman', ['B', 'G']).
card_text('deathrite shaman', '{T}: Exile target land card from a graveyard. Add one mana of any color to your mana pool.\n{B}, {T}: Exile target instant or sorcery card from a graveyard. Each opponent loses 2 life.\n{G}, {T}: Exile target creature card from a graveyard. You gain 2 life.').
card_mana_cost('deathrite shaman', ['B/G']).
card_cmc('deathrite shaman', 1).
card_layout('deathrite shaman', 'normal').
card_power('deathrite shaman', 1).
card_toughness('deathrite shaman', 2).

% Found in: TSP
card_name('deathspore thallid', 'Deathspore Thallid').
card_type('deathspore thallid', 'Creature — Zombie Fungus').
card_types('deathspore thallid', ['Creature']).
card_subtypes('deathspore thallid', ['Zombie', 'Fungus']).
card_colors('deathspore thallid', ['B']).
card_text('deathspore thallid', 'At the beginning of your upkeep, put a spore counter on Deathspore Thallid.\nRemove three spore counters from Deathspore Thallid: Put a 1/1 green Saproling creature token onto the battlefield.\nSacrifice a Saproling: Target creature gets -1/-1 until end of turn.').
card_mana_cost('deathspore thallid', ['1', 'B']).
card_cmc('deathspore thallid', 2).
card_layout('deathspore thallid', 'normal').
card_power('deathspore thallid', 1).
card_toughness('deathspore thallid', 1).

% Found in: FRF_UGIN, KTK
card_name('debilitating injury', 'Debilitating Injury').
card_type('debilitating injury', 'Enchantment — Aura').
card_types('debilitating injury', ['Enchantment']).
card_subtypes('debilitating injury', ['Aura']).
card_colors('debilitating injury', ['B']).
card_text('debilitating injury', 'Enchant creature\nEnchanted creature gets -2/-2.').
card_mana_cost('debilitating injury', ['1', 'B']).
card_cmc('debilitating injury', 2).
card_layout('debilitating injury', 'normal').

% Found in: WTH
card_name('debt of loyalty', 'Debt of Loyalty').
card_type('debt of loyalty', 'Instant').
card_types('debt of loyalty', ['Instant']).
card_subtypes('debt of loyalty', []).
card_colors('debt of loyalty', ['W']).
card_text('debt of loyalty', 'Regenerate target creature. You gain control of that creature if it regenerates this way.').
card_mana_cost('debt of loyalty', ['1', 'W', 'W']).
card_cmc('debt of loyalty', 3).
card_layout('debt of loyalty', 'normal').
card_reserved('debt of loyalty').

% Found in: DGM
card_name('debt to the deathless', 'Debt to the Deathless').
card_type('debt to the deathless', 'Sorcery').
card_types('debt to the deathless', ['Sorcery']).
card_subtypes('debt to the deathless', []).
card_colors('debt to the deathless', ['W', 'B']).
card_text('debt to the deathless', 'Each opponent loses two times X life. You gain life equal to the life lost this way.').
card_mana_cost('debt to the deathless', ['X', 'W', 'W', 'B', 'B']).
card_cmc('debt to the deathless', 4).
card_layout('debt to the deathless', 'normal').

% Found in: GTC
card_name('debtor\'s pulpit', 'Debtor\'s Pulpit').
card_type('debtor\'s pulpit', 'Enchantment — Aura').
card_types('debtor\'s pulpit', ['Enchantment']).
card_subtypes('debtor\'s pulpit', ['Aura']).
card_colors('debtor\'s pulpit', ['W']).
card_text('debtor\'s pulpit', 'Enchant land\nEnchanted land has \"{T}: Tap target creature.\"').
card_mana_cost('debtor\'s pulpit', ['4', 'W']).
card_cmc('debtor\'s pulpit', 5).
card_layout('debtor\'s pulpit', 'normal').

% Found in: GPT
card_name('debtors\' knell', 'Debtors\' Knell').
card_type('debtors\' knell', 'Enchantment').
card_types('debtors\' knell', ['Enchantment']).
card_subtypes('debtors\' knell', []).
card_colors('debtors\' knell', ['W', 'B']).
card_text('debtors\' knell', '({W/B} can be paid with either {W} or {B}.)\nAt the beginning of your upkeep, put target creature card from a graveyard onto the battlefield under your control.').
card_mana_cost('debtors\' knell', ['4', 'W/B', 'W/B', 'W/B']).
card_cmc('debtors\' knell', 7).
card_layout('debtors\' knell', 'normal').

% Found in: ODY
card_name('decaying soil', 'Decaying Soil').
card_type('decaying soil', 'Enchantment').
card_types('decaying soil', ['Enchantment']).
card_subtypes('decaying soil', []).
card_colors('decaying soil', ['B']).
card_text('decaying soil', 'At the beginning of your upkeep, exile a card from your graveyard.\nThreshold — As long as seven or more cards are in your graveyard, Decaying Soil has \"Whenever a nontoken creature is put into your graveyard from the battlefield, you may pay {1}. If you do, return that card to your hand.\"').
card_mana_cost('decaying soil', ['1', 'B', 'B']).
card_cmc('decaying soil', 3).
card_layout('decaying soil', 'normal').

% Found in: C13, NPH
card_name('deceiver exarch', 'Deceiver Exarch').
card_type('deceiver exarch', 'Creature — Cleric').
card_types('deceiver exarch', ['Creature']).
card_subtypes('deceiver exarch', ['Cleric']).
card_colors('deceiver exarch', ['U']).
card_text('deceiver exarch', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Deceiver Exarch enters the battlefield, choose one —\n• Untap target permanent you control.\n• Tap target permanent an opponent controls.').
card_mana_cost('deceiver exarch', ['2', 'U']).
card_cmc('deceiver exarch', 3).
card_layout('deceiver exarch', 'normal').
card_power('deceiver exarch', 1).
card_toughness('deceiver exarch', 4).

% Found in: PTK
card_name('deception', 'Deception').
card_type('deception', 'Sorcery').
card_types('deception', ['Sorcery']).
card_subtypes('deception', []).
card_colors('deception', ['B']).
card_text('deception', 'Target opponent discards two cards.').
card_mana_cost('deception', ['2', 'B']).
card_cmc('deception', 3).
card_layout('deception', 'normal').

% Found in: CNS, ODY
card_name('decimate', 'Decimate').
card_type('decimate', 'Sorcery').
card_types('decimate', ['Sorcery']).
card_subtypes('decimate', []).
card_colors('decimate', ['R', 'G']).
card_text('decimate', 'Destroy target artifact, target creature, target enchantment, and target land.').
card_mana_cost('decimate', ['2', 'R', 'G']).
card_cmc('decimate', 4).
card_layout('decimate', 'normal').

% Found in: MBS
card_name('decimator web', 'Decimator Web').
card_type('decimator web', 'Artifact').
card_types('decimator web', ['Artifact']).
card_subtypes('decimator web', []).
card_colors('decimator web', []).
card_text('decimator web', '{4}, {T}: Target opponent loses 2 life, gets a poison counter, then puts the top six cards of his or her library into his or her graveyard.').
card_mana_cost('decimator web', ['4']).
card_cmc('decimator web', 4).
card_layout('decimator web', 'normal').

% Found in: MOR
card_name('declaration of naught', 'Declaration of Naught').
card_type('declaration of naught', 'Enchantment').
card_types('declaration of naught', ['Enchantment']).
card_subtypes('declaration of naught', []).
card_colors('declaration of naught', ['U']).
card_text('declaration of naught', 'As Declaration of Naught enters the battlefield, name a card.\n{U}: Counter target spell with the chosen name.').
card_mana_cost('declaration of naught', ['U', 'U']).
card_cmc('declaration of naught', 2).
card_layout('declaration of naught', 'normal').

% Found in: DDK, ODY
card_name('decompose', 'Decompose').
card_type('decompose', 'Sorcery').
card_types('decompose', ['Sorcery']).
card_subtypes('decompose', []).
card_colors('decompose', ['B']).
card_text('decompose', 'Exile up to three target cards from a single graveyard.').
card_mana_cost('decompose', ['1', 'B']).
card_cmc('decompose', 2).
card_layout('decompose', 'normal').

% Found in: MIR
card_name('decomposition', 'Decomposition').
card_type('decomposition', 'Enchantment — Aura').
card_types('decomposition', ['Enchantment']).
card_subtypes('decomposition', ['Aura']).
card_colors('decomposition', ['G']).
card_text('decomposition', 'Enchant black creature\nEnchanted creature has \"Cumulative upkeep—Pay 1 life.\" (At the beginning of its controller\'s upkeep, that player puts an age counter on it, then sacrifices it unless he or she pays its upkeep cost for each age counter on it.)\nWhen enchanted creature dies, its controller loses 2 life.').
card_mana_cost('decomposition', ['1', 'G']).
card_cmc('decomposition', 2).
card_layout('decomposition', 'normal').

% Found in: MRD
card_name('deconstruct', 'Deconstruct').
card_type('deconstruct', 'Sorcery').
card_types('deconstruct', ['Sorcery']).
card_subtypes('deconstruct', []).
card_colors('deconstruct', ['G']).
card_text('deconstruct', 'Destroy target artifact. Add {G}{G}{G} to your mana pool.').
card_mana_cost('deconstruct', ['2', 'G']).
card_cmc('deconstruct', 3).
card_layout('deconstruct', 'normal').

% Found in: THS
card_name('decorated griffin', 'Decorated Griffin').
card_type('decorated griffin', 'Creature — Griffin').
card_types('decorated griffin', ['Creature']).
card_subtypes('decorated griffin', ['Griffin']).
card_colors('decorated griffin', ['W']).
card_text('decorated griffin', 'Flying\n{1}{W}: Prevent the next 1 combat damage that would be dealt to you this turn.').
card_mana_cost('decorated griffin', ['4', 'W']).
card_cmc('decorated griffin', 5).
card_layout('decorated griffin', 'normal').
card_power('decorated griffin', 2).
card_toughness('decorated griffin', 3).

% Found in: SCG, V14
card_name('decree of annihilation', 'Decree of Annihilation').
card_type('decree of annihilation', 'Sorcery').
card_types('decree of annihilation', ['Sorcery']).
card_subtypes('decree of annihilation', []).
card_colors('decree of annihilation', ['R']).
card_text('decree of annihilation', 'Exile all artifacts, creatures, and lands from the battlefield, all cards from all graveyards, and all cards from all hands.\nCycling {5}{R}{R} ({5}{R}{R}, Discard this card: Draw a card.)\nWhen you cycle Decree of Annihilation, destroy all lands.').
card_mana_cost('decree of annihilation', ['8', 'R', 'R']).
card_cmc('decree of annihilation', 10).
card_layout('decree of annihilation', 'normal').

% Found in: C14, DDO, SCG, VMA, pJGP
card_name('decree of justice', 'Decree of Justice').
card_type('decree of justice', 'Sorcery').
card_types('decree of justice', ['Sorcery']).
card_subtypes('decree of justice', []).
card_colors('decree of justice', ['W']).
card_text('decree of justice', 'Put X 4/4 white Angel creature tokens with flying onto the battlefield.\nCycling {2}{W} ({2}{W}, Discard this card: Draw a card.)\nWhen you cycle Decree of Justice, you may pay {X}. If you do, put X 1/1 white Soldier creature tokens onto the battlefield.').
card_mana_cost('decree of justice', ['X', 'X', '2', 'W', 'W']).
card_cmc('decree of justice', 4).
card_layout('decree of justice', 'normal').

% Found in: C13, CM1, SCG
card_name('decree of pain', 'Decree of Pain').
card_type('decree of pain', 'Sorcery').
card_types('decree of pain', ['Sorcery']).
card_subtypes('decree of pain', []).
card_colors('decree of pain', ['B']).
card_text('decree of pain', 'Destroy all creatures. They can\'t be regenerated. Draw a card for each creature destroyed this way.\nCycling {3}{B}{B} ({3}{B}{B}, Discard this card: Draw a card.)\nWhen you cycle Decree of Pain, all creatures get -2/-2 until end of turn.').
card_mana_cost('decree of pain', ['6', 'B', 'B']).
card_cmc('decree of pain', 8).
card_layout('decree of pain', 'normal').

% Found in: SCG
card_name('decree of savagery', 'Decree of Savagery').
card_type('decree of savagery', 'Instant').
card_types('decree of savagery', ['Instant']).
card_subtypes('decree of savagery', []).
card_colors('decree of savagery', ['G']).
card_text('decree of savagery', 'Put four +1/+1 counters on each creature you control.\nCycling {4}{G}{G} ({4}{G}{G}, Discard this card: Draw a card.)\nWhen you cycle Decree of Savagery, you may put four +1/+1 counters on target creature.').
card_mana_cost('decree of savagery', ['7', 'G', 'G']).
card_cmc('decree of savagery', 9).
card_layout('decree of savagery', 'normal').

% Found in: SCG
card_name('decree of silence', 'Decree of Silence').
card_type('decree of silence', 'Enchantment').
card_types('decree of silence', ['Enchantment']).
card_subtypes('decree of silence', []).
card_colors('decree of silence', ['U']).
card_text('decree of silence', 'Whenever an opponent casts a spell, counter that spell and put a depletion counter on Decree of Silence. If there are three or more depletion counters on Decree of Silence, sacrifice it.\nCycling {4}{U}{U} ({4}{U}{U}, Discard this card: Draw a card.)\nWhen you cycle Decree of Silence, you may counter target spell.').
card_mana_cost('decree of silence', ['6', 'U', 'U']).
card_cmc('decree of silence', 8).
card_layout('decree of silence', 'normal').

% Found in: ODY
card_name('dedicated martyr', 'Dedicated Martyr').
card_type('dedicated martyr', 'Creature — Human Cleric').
card_types('dedicated martyr', ['Creature']).
card_subtypes('dedicated martyr', ['Human', 'Cleric']).
card_colors('dedicated martyr', ['W']).
card_text('dedicated martyr', '{W}, Sacrifice Dedicated Martyr: You gain 3 life.').
card_mana_cost('dedicated martyr', ['W']).
card_cmc('dedicated martyr', 1).
card_layout('dedicated martyr', 'normal').
card_power('dedicated martyr', 1).
card_toughness('dedicated martyr', 1).

% Found in: C13, DDH, TOR, VMA, pFNM
card_name('deep analysis', 'Deep Analysis').
card_type('deep analysis', 'Sorcery').
card_types('deep analysis', ['Sorcery']).
card_subtypes('deep analysis', []).
card_colors('deep analysis', ['U']).
card_text('deep analysis', 'Target player draws two cards.\nFlashback—{1}{U}, Pay 3 life. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('deep analysis', ['3', 'U']).
card_cmc('deep analysis', 4).
card_layout('deep analysis', 'normal').

% Found in: ODY
card_name('deep reconnaissance', 'Deep Reconnaissance').
card_type('deep reconnaissance', 'Sorcery').
card_types('deep reconnaissance', ['Sorcery']).
card_subtypes('deep reconnaissance', []).
card_colors('deep reconnaissance', ['G']).
card_text('deep reconnaissance', 'Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.\nFlashback {4}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('deep reconnaissance', ['2', 'G']).
card_cmc('deep reconnaissance', 3).
card_layout('deep reconnaissance', 'normal').

% Found in: FEM, ME2
card_name('deep spawn', 'Deep Spawn').
card_type('deep spawn', 'Creature — Homarid').
card_types('deep spawn', ['Creature']).
card_subtypes('deep spawn', ['Homarid']).
card_colors('deep spawn', ['U']).
card_text('deep spawn', 'Trample\nAt the beginning of your upkeep, sacrifice Deep Spawn unless you put the top two cards of your library into your graveyard.\n{U}: Deep Spawn gains shroud until end of turn and doesn\'t untap during your next untap step. Tap Deep Spawn. (A creature with shroud can\'t be the target of spells or abilities.)').
card_mana_cost('deep spawn', ['5', 'U', 'U', 'U']).
card_cmc('deep spawn', 8).
card_layout('deep spawn', 'normal').
card_power('deep spawn', 6).
card_toughness('deep spawn', 6).

% Found in: DRK
card_name('deep water', 'Deep Water').
card_type('deep water', 'Enchantment').
card_types('deep water', ['Enchantment']).
card_subtypes('deep water', []).
card_colors('deep water', ['U']).
card_text('deep water', '{U}: Until end of turn, if you tap a land you control for mana, it produces {U} instead of any other type.').
card_mana_cost('deep water', ['U', 'U']).
card_cmc('deep water', 2).
card_layout('deep water', 'normal').

% Found in: PO2, POR
card_name('deep wood', 'Deep Wood').
card_type('deep wood', 'Instant').
card_types('deep wood', ['Instant']).
card_subtypes('deep wood', []).
card_colors('deep wood', ['G']).
card_text('deep wood', 'Cast Deep Wood only during the declare attackers step and only if you\'ve been attacked this step.\nPrevent all damage that would be dealt to you this turn by attacking creatures.').
card_mana_cost('deep wood', ['1', 'G']).
card_cmc('deep wood', 2).
card_layout('deep wood', 'normal').

% Found in: C14, TSP
card_name('deep-sea kraken', 'Deep-Sea Kraken').
card_type('deep-sea kraken', 'Creature — Kraken').
card_types('deep-sea kraken', ['Creature']).
card_subtypes('deep-sea kraken', ['Kraken']).
card_colors('deep-sea kraken', ['U']).
card_text('deep-sea kraken', 'Deep-Sea Kraken can\'t be blocked.\nSuspend 9—{2}{U} (Rather than cast this card from your hand, you may pay {2}{U} and exile it with nine time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)\nWhenever an opponent casts a spell, if Deep-Sea Kraken is suspended, remove a time counter from it.').
card_mana_cost('deep-sea kraken', ['7', 'U', 'U', 'U']).
card_cmc('deep-sea kraken', 10).
card_layout('deep-sea kraken', 'normal').
card_power('deep-sea kraken', 6).
card_toughness('deep-sea kraken', 6).

% Found in: POR
card_name('deep-sea serpent', 'Deep-Sea Serpent').
card_type('deep-sea serpent', 'Creature — Serpent').
card_types('deep-sea serpent', ['Creature']).
card_subtypes('deep-sea serpent', ['Serpent']).
card_colors('deep-sea serpent', ['U']).
card_text('deep-sea serpent', 'Deep-Sea Serpent can\'t attack unless defending player controls an Island.').
card_mana_cost('deep-sea serpent', ['4', 'U', 'U']).
card_cmc('deep-sea serpent', 6).
card_layout('deep-sea serpent', 'normal').
card_power('deep-sea serpent', 5).
card_toughness('deep-sea serpent', 5).

% Found in: ORI
card_name('deep-sea terror', 'Deep-Sea Terror').
card_type('deep-sea terror', 'Creature — Serpent').
card_types('deep-sea terror', ['Creature']).
card_subtypes('deep-sea terror', ['Serpent']).
card_colors('deep-sea terror', ['U']).
card_text('deep-sea terror', 'Deep-Sea Terror can\'t attack unless there are seven or more cards in your graveyard.').
card_mana_cost('deep-sea terror', ['4', 'U', 'U']).
card_cmc('deep-sea terror', 6).
card_layout('deep-sea terror', 'normal').
card_power('deep-sea terror', 6).
card_toughness('deep-sea terror', 6).

% Found in: SHM
card_name('deep-slumber titan', 'Deep-Slumber Titan').
card_type('deep-slumber titan', 'Creature — Giant Warrior').
card_types('deep-slumber titan', ['Creature']).
card_subtypes('deep-slumber titan', ['Giant', 'Warrior']).
card_colors('deep-slumber titan', ['R']).
card_text('deep-slumber titan', 'Deep-Slumber Titan enters the battlefield tapped.\nDeep-Slumber Titan doesn\'t untap during your untap step.\nWhenever Deep-Slumber Titan is dealt damage, untap it.').
card_mana_cost('deep-slumber titan', ['2', 'R', 'R']).
card_cmc('deep-slumber titan', 4).
card_layout('deep-slumber titan', 'normal').
card_power('deep-slumber titan', 7).
card_toughness('deep-slumber titan', 7).

% Found in: FUT, MMA
card_name('deepcavern imp', 'Deepcavern Imp').
card_type('deepcavern imp', 'Creature — Imp Rebel').
card_types('deepcavern imp', ['Creature']).
card_subtypes('deepcavern imp', ['Imp', 'Rebel']).
card_colors('deepcavern imp', ['B']).
card_text('deepcavern imp', 'Flying, haste\nEcho—Discard a card. (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_mana_cost('deepcavern imp', ['2', 'B']).
card_cmc('deepcavern imp', 3).
card_layout('deepcavern imp', 'normal').
card_power('deepcavern imp', 2).
card_toughness('deepcavern imp', 2).

% Found in: SHM
card_name('deepchannel mentor', 'Deepchannel Mentor').
card_type('deepchannel mentor', 'Creature — Merfolk Rogue').
card_types('deepchannel mentor', ['Creature']).
card_subtypes('deepchannel mentor', ['Merfolk', 'Rogue']).
card_colors('deepchannel mentor', ['U']).
card_text('deepchannel mentor', 'Blue creatures you control can\'t be blocked.').
card_mana_cost('deepchannel mentor', ['5', 'U']).
card_cmc('deepchannel mentor', 6).
card_layout('deepchannel mentor', 'normal').
card_power('deepchannel mentor', 2).
card_toughness('deepchannel mentor', 2).

% Found in: C13, CSP
card_name('deepfire elemental', 'Deepfire Elemental').
card_type('deepfire elemental', 'Creature — Elemental').
card_types('deepfire elemental', ['Creature']).
card_subtypes('deepfire elemental', ['Elemental']).
card_colors('deepfire elemental', ['B', 'R']).
card_text('deepfire elemental', '{X}{X}{1}: Destroy target artifact or creature with converted mana cost X.').
card_mana_cost('deepfire elemental', ['4', 'B', 'R']).
card_cmc('deepfire elemental', 6).
card_layout('deepfire elemental', 'normal').
card_power('deepfire elemental', 4).
card_toughness('deepfire elemental', 4).

% Found in: LRW
card_name('deeptread merrow', 'Deeptread Merrow').
card_type('deeptread merrow', 'Creature — Merfolk Rogue').
card_types('deeptread merrow', ['Creature']).
card_subtypes('deeptread merrow', ['Merfolk', 'Rogue']).
card_colors('deeptread merrow', ['U']).
card_text('deeptread merrow', '{U}: Deeptread Merrow gains islandwalk until end of turn. (It can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('deeptread merrow', ['1', 'U']).
card_cmc('deeptread merrow', 2).
card_layout('deeptread merrow', 'normal').
card_power('deeptread merrow', 2).
card_toughness('deeptread merrow', 1).

% Found in: BNG
card_name('deepwater hypnotist', 'Deepwater Hypnotist').
card_type('deepwater hypnotist', 'Creature — Merfolk Wizard').
card_types('deepwater hypnotist', ['Creature']).
card_subtypes('deepwater hypnotist', ['Merfolk', 'Wizard']).
card_colors('deepwater hypnotist', ['U']).
card_text('deepwater hypnotist', 'Inspired — Whenever Deepwater Hypnotist becomes untapped, target creature an opponent controls gets -3/-0 until end of turn.').
card_mana_cost('deepwater hypnotist', ['1', 'U']).
card_cmc('deepwater hypnotist', 2).
card_layout('deepwater hypnotist', 'normal').
card_power('deepwater hypnotist', 2).
card_toughness('deepwater hypnotist', 1).

% Found in: MMQ
card_name('deepwood drummer', 'Deepwood Drummer').
card_type('deepwood drummer', 'Creature — Human Spellshaper').
card_types('deepwood drummer', ['Creature']).
card_subtypes('deepwood drummer', ['Human', 'Spellshaper']).
card_colors('deepwood drummer', ['G']).
card_text('deepwood drummer', '{G}, {T}, Discard a card: Target creature gets +2/+2 until end of turn.').
card_mana_cost('deepwood drummer', ['1', 'G']).
card_cmc('deepwood drummer', 2).
card_layout('deepwood drummer', 'normal').
card_power('deepwood drummer', 1).
card_toughness('deepwood drummer', 1).

% Found in: MMQ
card_name('deepwood elder', 'Deepwood Elder').
card_type('deepwood elder', 'Creature — Dryad Spellshaper').
card_types('deepwood elder', ['Creature']).
card_subtypes('deepwood elder', ['Dryad', 'Spellshaper']).
card_colors('deepwood elder', ['G']).
card_text('deepwood elder', '{X}{G}{G}, {T}, Discard a card: X target lands become Forests until end of turn.').
card_mana_cost('deepwood elder', ['G', 'G']).
card_cmc('deepwood elder', 2).
card_layout('deepwood elder', 'normal').
card_power('deepwood elder', 2).
card_toughness('deepwood elder', 2).

% Found in: 8ED, MMQ
card_name('deepwood ghoul', 'Deepwood Ghoul').
card_type('deepwood ghoul', 'Creature — Zombie').
card_types('deepwood ghoul', ['Creature']).
card_subtypes('deepwood ghoul', ['Zombie']).
card_colors('deepwood ghoul', ['B']).
card_text('deepwood ghoul', 'Pay 2 life: Regenerate Deepwood Ghoul.').
card_mana_cost('deepwood ghoul', ['2', 'B']).
card_cmc('deepwood ghoul', 3).
card_layout('deepwood ghoul', 'normal').
card_power('deepwood ghoul', 2).
card_toughness('deepwood ghoul', 1).

% Found in: MMQ
card_name('deepwood legate', 'Deepwood Legate').
card_type('deepwood legate', 'Creature — Shade').
card_types('deepwood legate', ['Creature']).
card_subtypes('deepwood legate', ['Shade']).
card_colors('deepwood legate', ['B']).
card_text('deepwood legate', 'If an opponent controls a Forest and you control a Swamp, you may cast Deepwood Legate without paying its mana cost.\n{B}: Deepwood Legate gets +1/+1 until end of turn.').
card_mana_cost('deepwood legate', ['3', 'B']).
card_cmc('deepwood legate', 4).
card_layout('deepwood legate', 'normal').
card_power('deepwood legate', 1).
card_toughness('deepwood legate', 1).

% Found in: MMQ
card_name('deepwood tantiv', 'Deepwood Tantiv').
card_type('deepwood tantiv', 'Creature — Beast').
card_types('deepwood tantiv', ['Creature']).
card_subtypes('deepwood tantiv', ['Beast']).
card_colors('deepwood tantiv', ['G']).
card_text('deepwood tantiv', 'Whenever Deepwood Tantiv becomes blocked, you gain 2 life.').
card_mana_cost('deepwood tantiv', ['4', 'G']).
card_cmc('deepwood tantiv', 5).
card_layout('deepwood tantiv', 'normal').
card_power('deepwood tantiv', 2).
card_toughness('deepwood tantiv', 4).

% Found in: MMQ
card_name('deepwood wolverine', 'Deepwood Wolverine').
card_type('deepwood wolverine', 'Creature — Wolverine').
card_types('deepwood wolverine', ['Creature']).
card_subtypes('deepwood wolverine', ['Wolverine']).
card_colors('deepwood wolverine', ['G']).
card_text('deepwood wolverine', 'Whenever Deepwood Wolverine becomes blocked, it gets +2/+0 until end of turn.').
card_mana_cost('deepwood wolverine', ['G']).
card_cmc('deepwood wolverine', 1).
card_layout('deepwood wolverine', 'normal').
card_power('deepwood wolverine', 1).
card_toughness('deepwood wolverine', 1).

% Found in: AVR
card_name('defang', 'Defang').
card_type('defang', 'Enchantment — Aura').
card_types('defang', ['Enchantment']).
card_subtypes('defang', ['Aura']).
card_colors('defang', ['W']).
card_text('defang', 'Enchant creature\nPrevent all damage that would be dealt by enchanted creature.').
card_mana_cost('defang', ['1', 'W']).
card_cmc('defang', 2).
card_layout('defang', 'normal').

% Found in: DTK
card_name('defeat', 'Defeat').
card_type('defeat', 'Sorcery').
card_types('defeat', ['Sorcery']).
card_subtypes('defeat', []).
card_colors('defeat', ['B']).
card_text('defeat', 'Destroy target creature with power 2 or less.').
card_mana_cost('defeat', ['1', 'B']).
card_cmc('defeat', 2).
card_layout('defeat', 'normal').

% Found in: THS
card_name('defend the hearth', 'Defend the Hearth').
card_type('defend the hearth', 'Instant').
card_types('defend the hearth', ['Instant']).
card_subtypes('defend the hearth', []).
card_colors('defend the hearth', ['G']).
card_text('defend the hearth', 'Prevent all combat damage that would be dealt to players this turn.').
card_mana_cost('defend the hearth', ['1', 'G']).
card_cmc('defend the hearth', 2).
card_layout('defend the hearth', 'normal').

% Found in: NMS
card_name('defender en-vec', 'Defender en-Vec').
card_type('defender en-vec', 'Creature — Human Cleric').
card_types('defender en-vec', ['Creature']).
card_subtypes('defender en-vec', ['Human', 'Cleric']).
card_colors('defender en-vec', ['W']).
card_text('defender en-vec', 'Fading 4 (This creature enters the battlefield with four fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nRemove a fade counter from Defender en-Vec: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_mana_cost('defender en-vec', ['3', 'W']).
card_cmc('defender en-vec', 4).
card_layout('defender en-vec', 'normal').
card_power('defender en-vec', 2).
card_toughness('defender en-vec', 4).

% Found in: ULG
card_name('defender of chaos', 'Defender of Chaos').
card_type('defender of chaos', 'Creature — Human Knight').
card_types('defender of chaos', ['Creature']).
card_subtypes('defender of chaos', ['Human', 'Knight']).
card_colors('defender of chaos', ['R']).
card_text('defender of chaos', 'Flash\nProtection from white').
card_mana_cost('defender of chaos', ['2', 'R']).
card_cmc('defender of chaos', 3).
card_layout('defender of chaos', 'normal').
card_power('defender of chaos', 2).
card_toughness('defender of chaos', 1).

% Found in: ULG
card_name('defender of law', 'Defender of Law').
card_type('defender of law', 'Creature — Human Knight').
card_types('defender of law', ['Creature']).
card_subtypes('defender of law', ['Human', 'Knight']).
card_colors('defender of law', ['W']).
card_text('defender of law', 'Flash\nProtection from red').
card_mana_cost('defender of law', ['2', 'W']).
card_cmc('defender of law', 3).
card_layout('defender of law', 'normal').
card_power('defender of law', 2).
card_toughness('defender of law', 1).

% Found in: LGN
card_name('defender of the order', 'Defender of the Order').
card_type('defender of the order', 'Creature — Human Cleric').
card_types('defender of the order', ['Creature']).
card_subtypes('defender of the order', ['Human', 'Cleric']).
card_colors('defender of the order', ['W']).
card_text('defender of the order', 'Morph {W}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Defender of the Order is turned face up, creatures you control get +0/+2 until end of turn.').
card_mana_cost('defender of the order', ['3', 'W']).
card_cmc('defender of the order', 4).
card_layout('defender of the order', 'normal').
card_power('defender of the order', 2).
card_toughness('defender of the order', 4).

% Found in: 8ED, 9ED, ULG
card_name('defense grid', 'Defense Grid').
card_type('defense grid', 'Artifact').
card_types('defense grid', ['Artifact']).
card_subtypes('defense grid', []).
card_colors('defense grid', []).
card_text('defense grid', 'Each spell costs {3} more to cast except during its controller\'s turn.').
card_mana_cost('defense grid', ['2']).
card_cmc('defense grid', 2).
card_layout('defense grid', 'normal').

% Found in: ULG
card_name('defense of the heart', 'Defense of the Heart').
card_type('defense of the heart', 'Enchantment').
card_types('defense of the heart', ['Enchantment']).
card_subtypes('defense of the heart', []).
card_colors('defense of the heart', ['G']).
card_text('defense of the heart', 'At the beginning of your upkeep, if an opponent controls three or more creatures, sacrifice Defense of the Heart, search your library for up to two creature cards, and put those cards onto the battlefield. Then shuffle your library.').
card_mana_cost('defense of the heart', ['3', 'G']).
card_cmc('defense of the heart', 4).
card_layout('defense of the heart', 'normal').

% Found in: USG
card_name('defensive formation', 'Defensive Formation').
card_type('defensive formation', 'Enchantment').
card_types('defensive formation', ['Enchantment']).
card_subtypes('defensive formation', []).
card_colors('defensive formation', ['W']).
card_text('defensive formation', 'Rather than the attacking player, you assign the combat damage of each creature attacking you. You can divide that creature\'s combat damage as you choose among any of the creatures blocking it.').
card_mana_cost('defensive formation', ['W']).
card_cmc('defensive formation', 1).
card_layout('defensive formation', 'normal').

% Found in: ONS
card_name('defensive maneuvers', 'Defensive Maneuvers').
card_type('defensive maneuvers', 'Instant').
card_types('defensive maneuvers', ['Instant']).
card_subtypes('defensive maneuvers', []).
card_colors('defensive maneuvers', ['W']).
card_text('defensive maneuvers', 'Creatures of the creature type of your choice get +0/+4 until end of turn.').
card_mana_cost('defensive maneuvers', ['3', 'W']).
card_cmc('defensive maneuvers', 4).
card_layout('defensive maneuvers', 'normal').

% Found in: NPH
card_name('defensive stance', 'Defensive Stance').
card_type('defensive stance', 'Enchantment — Aura').
card_types('defensive stance', ['Enchantment']).
card_subtypes('defensive stance', ['Aura']).
card_colors('defensive stance', ['U']).
card_text('defensive stance', 'Enchant creature\nEnchanted creature gets -1/+1.').
card_mana_cost('defensive stance', ['U']).
card_cmc('defensive stance', 1).
card_layout('defensive stance', 'normal').

% Found in: BFZ
card_name('defiant bloodlord', 'Defiant Bloodlord').
card_type('defiant bloodlord', 'Creature — Vampire').
card_types('defiant bloodlord', ['Creature']).
card_subtypes('defiant bloodlord', ['Vampire']).
card_colors('defiant bloodlord', ['B']).
card_text('defiant bloodlord', 'Flying\nWhenever you gain life, target opponent loses that much life.').
card_mana_cost('defiant bloodlord', ['5', 'B', 'B']).
card_cmc('defiant bloodlord', 7).
card_layout('defiant bloodlord', 'normal').
card_power('defiant bloodlord', 4).
card_toughness('defiant bloodlord', 5).

% Found in: LGN
card_name('defiant elf', 'Defiant Elf').
card_type('defiant elf', 'Creature — Elf').
card_types('defiant elf', ['Creature']).
card_subtypes('defiant elf', ['Elf']).
card_colors('defiant elf', ['G']).
card_text('defiant elf', 'Trample').
card_mana_cost('defiant elf', ['G']).
card_cmc('defiant elf', 1).
card_layout('defiant elf', 'normal').
card_power('defiant elf', 1).
card_toughness('defiant elf', 1).

% Found in: NMS
card_name('defiant falcon', 'Defiant Falcon').
card_type('defiant falcon', 'Creature — Rebel Bird').
card_types('defiant falcon', ['Creature']).
card_subtypes('defiant falcon', ['Rebel', 'Bird']).
card_colors('defiant falcon', ['W']).
card_text('defiant falcon', 'Flying\n{4}, {T}: Search your library for a Rebel permanent card with converted mana cost 3 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('defiant falcon', ['1', 'W']).
card_cmc('defiant falcon', 2).
card_layout('defiant falcon', 'normal').
card_power('defiant falcon', 1).
card_toughness('defiant falcon', 1).

% Found in: FRF
card_name('defiant ogre', 'Defiant Ogre').
card_type('defiant ogre', 'Creature — Ogre Warrior').
card_types('defiant ogre', ['Creature']).
card_subtypes('defiant ogre', ['Ogre', 'Warrior']).
card_colors('defiant ogre', ['R']).
card_text('defiant ogre', 'When Defiant Ogre enters the battlefield, choose one —\n• Put a +1/+1 counter on Defiant Ogre.\n• Destroy target artifact.').
card_mana_cost('defiant ogre', ['5', 'R']).
card_cmc('defiant ogre', 6).
card_layout('defiant ogre', 'normal').
card_power('defiant ogre', 3).
card_toughness('defiant ogre', 5).

% Found in: POR
card_name('defiant stand', 'Defiant Stand').
card_type('defiant stand', 'Instant').
card_types('defiant stand', ['Instant']).
card_subtypes('defiant stand', []).
card_colors('defiant stand', ['W']).
card_text('defiant stand', 'Cast Defiant Stand only during the declare attackers step and only if you\'ve been attacked this step.\nTarget creature gets +1/+3 until end of turn. Untap that creature.').
card_mana_cost('defiant stand', ['1', 'W']).
card_cmc('defiant stand', 2).
card_layout('defiant stand', 'normal').

% Found in: KTK
card_name('defiant strike', 'Defiant Strike').
card_type('defiant strike', 'Instant').
card_types('defiant strike', ['Instant']).
card_subtypes('defiant strike', []).
card_colors('defiant strike', ['W']).
card_text('defiant strike', 'Target creature gets +1/+0 until end of turn.\nDraw a card.').
card_mana_cost('defiant strike', ['W']).
card_cmc('defiant strike', 1).
card_layout('defiant strike', 'normal').

% Found in: NMS, TSB
card_name('defiant vanguard', 'Defiant Vanguard').
card_type('defiant vanguard', 'Creature — Human Rebel').
card_types('defiant vanguard', ['Creature']).
card_subtypes('defiant vanguard', ['Human', 'Rebel']).
card_colors('defiant vanguard', ['W']).
card_text('defiant vanguard', 'When Defiant Vanguard blocks, at end of combat, destroy it and all creatures it blocked this turn.\n{5}, {T}: Search your library for a Rebel permanent card with converted mana cost 4 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('defiant vanguard', ['2', 'W']).
card_cmc('defiant vanguard', 3).
card_layout('defiant vanguard', 'normal').
card_power('defiant vanguard', 2).
card_toughness('defiant vanguard', 2).

% Found in: ARB
card_name('defiler of souls', 'Defiler of Souls').
card_type('defiler of souls', 'Creature — Demon').
card_types('defiler of souls', ['Creature']).
card_subtypes('defiler of souls', ['Demon']).
card_colors('defiler of souls', ['B', 'R']).
card_text('defiler of souls', 'Flying\nAt the beginning of each player\'s upkeep, that player sacrifices a monocolored creature.').
card_mana_cost('defiler of souls', ['3', 'B', 'B', 'R']).
card_cmc('defiler of souls', 6).
card_layout('defiler of souls', 'normal').
card_power('defiler of souls', 5).
card_toughness('defiler of souls', 5).

% Found in: INV
card_name('defiling tears', 'Defiling Tears').
card_type('defiling tears', 'Instant').
card_types('defiling tears', ['Instant']).
card_subtypes('defiling tears', []).
card_colors('defiling tears', ['B']).
card_text('defiling tears', 'Until end of turn, target creature becomes black, gets +1/-1, and gains \"{B}: Regenerate this creature.\"').
card_mana_cost('defiling tears', ['2', 'B']).
card_cmc('defiling tears', 3).
card_layout('defiling tears', 'normal').

% Found in: KTK, pPRE
card_name('deflecting palm', 'Deflecting Palm').
card_type('deflecting palm', 'Instant').
card_types('deflecting palm', ['Instant']).
card_subtypes('deflecting palm', []).
card_colors('deflecting palm', ['W', 'R']).
card_text('deflecting palm', 'The next time a source of your choice would deal damage to you this turn, prevent that damage. If damage is prevented this way, Deflecting Palm deals that much damage to that source\'s controller.').
card_mana_cost('deflecting palm', ['R', 'W']).
card_cmc('deflecting palm', 2).
card_layout('deflecting palm', 'normal').

% Found in: 5ED, 6ED, 7ED, 8ED, ICE
card_name('deflection', 'Deflection').
card_type('deflection', 'Instant').
card_types('deflection', ['Instant']).
card_subtypes('deflection', []).
card_colors('deflection', ['U']).
card_text('deflection', 'Change the target of target spell with a single target.').
card_mana_cost('deflection', ['3', 'U']).
card_cmc('deflection', 4).
card_layout('deflection', 'normal').

% Found in: ALA
card_name('deft duelist', 'Deft Duelist').
card_type('deft duelist', 'Creature — Human Rogue').
card_types('deft duelist', ['Creature']).
card_subtypes('deft duelist', ['Human', 'Rogue']).
card_colors('deft duelist', ['W', 'U']).
card_text('deft duelist', 'First strike\nShroud (This creature can\'t be the target of spells or abilities.)').
card_mana_cost('deft duelist', ['W', 'U']).
card_cmc('deft duelist', 2).
card_layout('deft duelist', 'normal').
card_power('deft duelist', 2).
card_toughness('deft duelist', 1).

% Found in: LGN, VMA
card_name('deftblade elite', 'Deftblade Elite').
card_type('deftblade elite', 'Creature — Human Soldier').
card_types('deftblade elite', ['Creature']).
card_subtypes('deftblade elite', ['Human', 'Soldier']).
card_colors('deftblade elite', ['W']).
card_text('deftblade elite', 'Provoke (Whenever this creature attacks, you may have target creature defending player controls untap and block it if able.)\n{1}{W}: Prevent all combat damage that would be dealt to and dealt by Deftblade Elite this turn.').
card_mana_cost('deftblade elite', ['W']).
card_cmc('deftblade elite', 1).
card_layout('deftblade elite', 'normal').
card_power('deftblade elite', 1).
card_toughness('deftblade elite', 1).

% Found in: AVR
card_name('defy death', 'Defy Death').
card_type('defy death', 'Sorcery').
card_types('defy death', ['Sorcery']).
card_subtypes('defy death', []).
card_colors('defy death', ['W']).
card_text('defy death', 'Return target creature card from your graveyard to the battlefield. If it\'s an Angel, put two +1/+1 counters on it.').
card_mana_cost('defy death', ['3', 'W', 'W']).
card_cmc('defy death', 5).
card_layout('defy death', 'normal').

% Found in: JUD
card_name('defy gravity', 'Defy Gravity').
card_type('defy gravity', 'Instant').
card_types('defy gravity', ['Instant']).
card_subtypes('defy gravity', []).
card_colors('defy gravity', ['U']).
card_text('defy gravity', 'Target creature gains flying until end of turn.\nFlashback {U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('defy gravity', ['U']).
card_cmc('defy gravity', 1).
card_layout('defy gravity', 'normal').

% Found in: APC
card_name('dega disciple', 'Dega Disciple').
card_type('dega disciple', 'Creature — Human Wizard').
card_types('dega disciple', ['Creature']).
card_subtypes('dega disciple', ['Human', 'Wizard']).
card_colors('dega disciple', ['W']).
card_text('dega disciple', '{B}, {T}: Target creature gets -2/-0 until end of turn.\n{R}, {T}: Target creature gets +2/+0 until end of turn.').
card_mana_cost('dega disciple', ['W']).
card_cmc('dega disciple', 1).
card_layout('dega disciple', 'normal').
card_power('dega disciple', 1).
card_toughness('dega disciple', 1).

% Found in: APC
card_name('dega sanctuary', 'Dega Sanctuary').
card_type('dega sanctuary', 'Enchantment').
card_types('dega sanctuary', ['Enchantment']).
card_subtypes('dega sanctuary', []).
card_colors('dega sanctuary', ['W']).
card_text('dega sanctuary', 'At the beginning of your upkeep, if you control a black or red permanent, you gain 2 life. If you control a black permanent and a red permanent, you gain 4 life instead.').
card_mana_cost('dega sanctuary', ['2', 'W']).
card_cmc('dega sanctuary', 3).
card_layout('dega sanctuary', 'normal').

% Found in: APC
card_name('degavolver', 'Degavolver').
card_type('degavolver', 'Creature — Volver').
card_types('degavolver', ['Creature']).
card_subtypes('degavolver', ['Volver']).
card_colors('degavolver', ['W']).
card_text('degavolver', 'Kicker {1}{B} and/or {R} (You may pay an additional {1}{B} and/or {R} as you cast this spell.)\nIf Degavolver was kicked with its {1}{B} kicker, it enters the battlefield with two +1/+1 counters on it and with \"Pay 3 life: Regenerate Degavolver.\"\nIf Degavolver was kicked with its {R} kicker, it enters the battlefield with a +1/+1 counter on it and with first strike.').
card_mana_cost('degavolver', ['1', 'W']).
card_cmc('degavolver', 2).
card_layout('degavolver', 'normal').
card_power('degavolver', 1).
card_toughness('degavolver', 1).

% Found in: MOR
card_name('deglamer', 'Deglamer').
card_type('deglamer', 'Instant').
card_types('deglamer', ['Instant']).
card_subtypes('deglamer', []).
card_colors('deglamer', ['G']).
card_text('deglamer', 'Choose target artifact or enchantment. Its owner shuffles it into his or her library.').
card_mana_cost('deglamer', ['1', 'G']).
card_cmc('deglamer', 2).
card_layout('deglamer', 'normal').

% Found in: 10E, 8ED, 9ED, MMQ
card_name('dehydration', 'Dehydration').
card_type('dehydration', 'Enchantment — Aura').
card_types('dehydration', ['Enchantment']).
card_subtypes('dehydration', ['Aura']).
card_colors('dehydration', ['U']).
card_text('dehydration', 'Enchant creature (Target a creature as you cast this. This card enters the battlefield attached to that creature.)\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_mana_cost('dehydration', ['3', 'U']).
card_cmc('dehydration', 4).
card_layout('dehydration', 'normal').

% Found in: JOU
card_name('deicide', 'Deicide').
card_type('deicide', 'Instant').
card_types('deicide', ['Instant']).
card_subtypes('deicide', []).
card_colors('deicide', ['W']).
card_text('deicide', 'Exile target enchantment. If the exiled card is a God card, search its controller\'s graveyard, hand, and library for any number of cards with the same name as that card and exile them, then that player shuffles his or her library.').
card_mana_cost('deicide', ['1', 'W']).
card_cmc('deicide', 2).
card_layout('deicide', 'normal').

% Found in: EVE
card_name('deity of scars', 'Deity of Scars').
card_type('deity of scars', 'Creature — Spirit Avatar').
card_types('deity of scars', ['Creature']).
card_subtypes('deity of scars', ['Spirit', 'Avatar']).
card_colors('deity of scars', ['B', 'G']).
card_text('deity of scars', 'Trample\nDeity of Scars enters the battlefield with two -1/-1 counters on it.\n{B/G}, Remove a -1/-1 counter from Deity of Scars: Regenerate Deity of Scars.').
card_mana_cost('deity of scars', ['B/G', 'B/G', 'B/G', 'B/G', 'B/G']).
card_cmc('deity of scars', 5).
card_layout('deity of scars', 'normal').
card_power('deity of scars', 7).
card_toughness('deity of scars', 7).

% Found in: FUT
card_name('delay', 'Delay').
card_type('delay', 'Instant').
card_types('delay', ['Instant']).
card_subtypes('delay', []).
card_colors('delay', ['U']).
card_text('delay', 'Counter target spell. If the spell is countered this way, exile it with three time counters on it instead of putting it into its owner\'s graveyard. If it doesn\'t have suspend, it gains suspend. (At the beginning of its owner\'s upkeep, remove a time counter from that card. When the last is removed, the player plays it without paying its mana cost. If it\'s a creature, it has haste.)').
card_mana_cost('delay', ['1', 'U']).
card_cmc('delay', 2).
card_layout('delay', 'normal').

% Found in: ODY
card_name('delaying shield', 'Delaying Shield').
card_type('delaying shield', 'Enchantment').
card_types('delaying shield', ['Enchantment']).
card_subtypes('delaying shield', []).
card_colors('delaying shield', ['W']).
card_text('delaying shield', 'If damage would be dealt to you, put that many delay counters on Delaying Shield instead.\nAt the beginning of your upkeep, remove all delay counters from Delaying Shield. For each delay counter removed this way, you lose 1 life unless you pay {1}{W}.').
card_mana_cost('delaying shield', ['3', 'W']).
card_cmc('delaying shield', 4).
card_layout('delaying shield', 'normal').

% Found in: FEM
card_name('delif\'s cone', 'Delif\'s Cone').
card_type('delif\'s cone', 'Artifact').
card_types('delif\'s cone', ['Artifact']).
card_subtypes('delif\'s cone', []).
card_colors('delif\'s cone', []).
card_text('delif\'s cone', '{T}, Sacrifice Delif\'s Cone: This turn, when target creature you control attacks and isn\'t blocked, you may gain life equal to its power. If you do, it assigns no combat damage this turn.').
card_mana_cost('delif\'s cone', ['0']).
card_cmc('delif\'s cone', 0).
card_layout('delif\'s cone', 'normal').

% Found in: FEM
card_name('delif\'s cube', 'Delif\'s Cube').
card_type('delif\'s cube', 'Artifact').
card_types('delif\'s cube', ['Artifact']).
card_subtypes('delif\'s cube', []).
card_colors('delif\'s cube', []).
card_text('delif\'s cube', '{2}, {T}: This turn, when target creature you control attacks and isn\'t blocked, it assigns no combat damage this turn and you put a cube counter on Delif\'s Cube.\n{2}, Remove a cube counter from Delif\'s Cube: Regenerate target creature.').
card_mana_cost('delif\'s cube', ['1']).
card_cmc('delif\'s cube', 1).
card_layout('delif\'s cube', 'normal').
card_reserved('delif\'s cube').

% Found in: MIR
card_name('delirium', 'Delirium').
card_type('delirium', 'Instant').
card_types('delirium', ['Instant']).
card_subtypes('delirium', []).
card_colors('delirium', ['B', 'R']).
card_text('delirium', 'Cast Delirium only during an opponent\'s turn.\nTap target creature that player controls. That creature deals damage equal to its power to the player. Prevent all combat damage that would be dealt to and dealt by the creature this turn.').
card_mana_cost('delirium', ['1', 'B', 'R']).
card_cmc('delirium', 3).
card_layout('delirium', 'normal').

% Found in: DIS
card_name('delirium skeins', 'Delirium Skeins').
card_type('delirium skeins', 'Sorcery').
card_types('delirium skeins', ['Sorcery']).
card_subtypes('delirium skeins', []).
card_colors('delirium skeins', ['B']).
card_text('delirium skeins', 'Each player discards three cards.').
card_mana_cost('delirium skeins', ['2', 'B']).
card_cmc('delirium skeins', 3).
card_layout('delirium skeins', 'normal').

% Found in: INV
card_name('deliver', 'Deliver').
card_type('deliver', 'Instant').
card_types('deliver', ['Instant']).
card_subtypes('deliver', []).
card_colors('deliver', ['U']).
card_text('deliver', 'Return target permanent to its owner\'s hand.').
card_mana_cost('deliver', ['2', 'U']).
card_cmc('deliver', 3).
card_layout('deliver', 'split').

% Found in: MMQ
card_name('delraich', 'Delraich').
card_type('delraich', 'Creature — Horror').
card_types('delraich', ['Creature']).
card_subtypes('delraich', ['Horror']).
card_colors('delraich', ['B']).
card_text('delraich', 'Trample\nYou may sacrifice three black creatures rather than pay Delraich\'s mana cost.').
card_mana_cost('delraich', ['6', 'B']).
card_cmc('delraich', 7).
card_layout('delraich', 'normal').
card_power('delraich', 6).
card_toughness('delraich', 6).

% Found in: 10E, DPA, ODY
card_name('deluge', 'Deluge').
card_type('deluge', 'Instant').
card_types('deluge', ['Instant']).
card_subtypes('deluge', []).
card_colors('deluge', ['U']).
card_text('deluge', 'Tap all creatures without flying.').
card_mana_cost('deluge', ['2', 'U']).
card_cmc('deluge', 3).
card_layout('deluge', 'normal').

% Found in: 7ED, ULG
card_name('delusions of mediocrity', 'Delusions of Mediocrity').
card_type('delusions of mediocrity', 'Enchantment').
card_types('delusions of mediocrity', ['Enchantment']).
card_subtypes('delusions of mediocrity', []).
card_colors('delusions of mediocrity', ['U']).
card_text('delusions of mediocrity', 'When Delusions of Mediocrity enters the battlefield, you gain 10 life.\nWhen Delusions of Mediocrity leaves the battlefield, you lose 10 life.').
card_mana_cost('delusions of mediocrity', ['3', 'U']).
card_cmc('delusions of mediocrity', 4).
card_layout('delusions of mediocrity', 'normal').

% Found in: ISD
card_name('delver of secrets', 'Delver of Secrets').
card_type('delver of secrets', 'Creature — Human Wizard').
card_types('delver of secrets', ['Creature']).
card_subtypes('delver of secrets', ['Human', 'Wizard']).
card_colors('delver of secrets', ['U']).
card_text('delver of secrets', 'At the beginning of your upkeep, look at the top card of your library. You may reveal that card. If an instant or sorcery card is revealed this way, transform Delver of Secrets.').
card_mana_cost('delver of secrets', ['U']).
card_cmc('delver of secrets', 1).
card_layout('delver of secrets', 'double-faced').
card_power('delver of secrets', 1).
card_toughness('delver of secrets', 1).
card_sides('delver of secrets', 'insectile aberration').

% Found in: DIS
card_name('demand', 'Demand').
card_type('demand', 'Sorcery').
card_types('demand', ['Sorcery']).
card_subtypes('demand', []).
card_colors('demand', ['W', 'U']).
card_text('demand', 'Search your library for a multicolored card, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('demand', ['1', 'W', 'U']).
card_cmc('demand', 3).
card_layout('demand', 'split').

% Found in: ODY
card_name('dematerialize', 'Dematerialize').
card_type('dematerialize', 'Sorcery').
card_types('dematerialize', ['Sorcery']).
card_subtypes('dematerialize', []).
card_colors('dematerialize', ['U']).
card_text('dematerialize', 'Return target permanent to its owner\'s hand.\nFlashback {5}{U}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('dematerialize', ['3', 'U']).
card_cmc('dematerialize', 4).
card_layout('dematerialize', 'normal').

% Found in: NPH
card_name('dementia bat', 'Dementia Bat').
card_type('dementia bat', 'Creature — Bat').
card_types('dementia bat', ['Creature']).
card_subtypes('dementia bat', ['Bat']).
card_colors('dementia bat', ['B']).
card_text('dementia bat', 'Flying\n{4}{B}, Sacrifice Dementia Bat: Target player discards two cards.').
card_mana_cost('dementia bat', ['4', 'B']).
card_cmc('dementia bat', 5).
card_layout('dementia bat', 'normal').
card_power('dementia bat', 2).
card_toughness('dementia bat', 2).

% Found in: TSP
card_name('dementia sliver', 'Dementia Sliver').
card_type('dementia sliver', 'Creature — Sliver').
card_types('dementia sliver', ['Creature']).
card_subtypes('dementia sliver', ['Sliver']).
card_colors('dementia sliver', ['U', 'B']).
card_text('dementia sliver', 'All Slivers have \"{T}: Name a card. Target opponent reveals a card at random from his or her hand. If it\'s the named card, that player discards it. Activate this ability only during your turn.\"').
card_mana_cost('dementia sliver', ['3', 'U', 'B']).
card_cmc('dementia sliver', 5).
card_layout('dementia sliver', 'normal').
card_power('dementia sliver', 3).
card_toughness('dementia sliver', 3).

% Found in: MMA, SHM, pPRE
card_name('demigod of revenge', 'Demigod of Revenge').
card_type('demigod of revenge', 'Creature — Spirit Avatar').
card_types('demigod of revenge', ['Creature']).
card_subtypes('demigod of revenge', ['Spirit', 'Avatar']).
card_colors('demigod of revenge', ['B', 'R']).
card_text('demigod of revenge', 'Flying, haste\nWhen you cast Demigod of Revenge, return all cards named Demigod of Revenge from your graveyard to the battlefield.').
card_mana_cost('demigod of revenge', ['B/R', 'B/R', 'B/R', 'B/R', 'B/R']).
card_cmc('demigod of revenge', 5).
card_layout('demigod of revenge', 'normal').
card_power('demigod of revenge', 5).
card_toughness('demigod of revenge', 4).

% Found in: 10E, 8ED, 9ED, AVR, M11, M14, ODY, ORI, THS, ZEN
card_name('demolish', 'Demolish').
card_type('demolish', 'Sorcery').
card_types('demolish', ['Sorcery']).
card_subtypes('demolish', []).
card_colors('demolish', ['R']).
card_text('demolish', 'Destroy target artifact or land.').
card_mana_cost('demolish', ['3', 'R']).
card_cmc('demolish', 4).
card_layout('demolish', 'normal').

% Found in: DDC
card_name('demon', 'Demon').
card_type('demon', 'Creature — Demon').
card_types('demon', ['Creature']).
card_subtypes('demon', ['Demon']).
card_colors('demon', []).
card_text('demon', 'Flying').
card_layout('demon', 'token').
card_power('demon', '*').
card_toughness('demon', '*').

% Found in: M11
card_name('demon of death\'s gate', 'Demon of Death\'s Gate').
card_type('demon of death\'s gate', 'Creature — Demon').
card_types('demon of death\'s gate', ['Creature']).
card_subtypes('demon of death\'s gate', ['Demon']).
card_colors('demon of death\'s gate', ['B']).
card_text('demon of death\'s gate', 'You may pay 6 life and sacrifice three black creatures rather than pay Demon of Death\'s Gate\'s mana cost.\nFlying, trample').
card_mana_cost('demon of death\'s gate', ['6', 'B', 'B', 'B']).
card_cmc('demon of death\'s gate', 9).
card_layout('demon of death\'s gate', 'normal').
card_power('demon of death\'s gate', 9).
card_toughness('demon of death\'s gate', 9).

% Found in: C14
card_name('demon of wailing agonies', 'Demon of Wailing Agonies').
card_type('demon of wailing agonies', 'Creature — Demon').
card_types('demon of wailing agonies', ['Creature']).
card_subtypes('demon of wailing agonies', ['Demon']).
card_colors('demon of wailing agonies', ['B']).
card_text('demon of wailing agonies', 'Flying\nLieutenant — As long as you control your commander, Demon of Wailing Agonies gets +2/+2 and has \"Whenever Demon of Wailing Agonies deals combat damage to a player, that player sacrifices a creature.\"').
card_mana_cost('demon of wailing agonies', ['3', 'B', 'B']).
card_cmc('demon of wailing agonies', 5).
card_layout('demon of wailing agonies', 'normal').
card_power('demon of wailing agonies', 4).
card_toughness('demon of wailing agonies', 4).

% Found in: BFZ
card_name('demon\'s grasp', 'Demon\'s Grasp').
card_type('demon\'s grasp', 'Sorcery').
card_types('demon\'s grasp', ['Sorcery']).
card_subtypes('demon\'s grasp', []).
card_colors('demon\'s grasp', ['B']).
card_text('demon\'s grasp', 'Target creature gets -5/-5 until end of turn.').
card_mana_cost('demon\'s grasp', ['4', 'B']).
card_cmc('demon\'s grasp', 5).
card_layout('demon\'s grasp', 'normal').

% Found in: ALA
card_name('demon\'s herald', 'Demon\'s Herald').
card_type('demon\'s herald', 'Creature — Human Wizard').
card_types('demon\'s herald', ['Creature']).
card_subtypes('demon\'s herald', ['Human', 'Wizard']).
card_colors('demon\'s herald', ['B']).
card_text('demon\'s herald', '{2}{B}, {T}, Sacrifice a blue creature, a black creature, and a red creature: Search your library for a card named Prince of Thralls and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('demon\'s herald', ['B']).
card_cmc('demon\'s herald', 1).
card_layout('demon\'s herald', 'normal').
card_power('demon\'s herald', 1).
card_toughness('demon\'s herald', 1).

% Found in: 10E, 9ED, DD3_DVD, DDC, DPA, DST, M10, M11, M12
card_name('demon\'s horn', 'Demon\'s Horn').
card_type('demon\'s horn', 'Artifact').
card_types('demon\'s horn', ['Artifact']).
card_subtypes('demon\'s horn', []).
card_colors('demon\'s horn', []).
card_text('demon\'s horn', 'Whenever a player casts a black spell, you may gain 1 life.').
card_mana_cost('demon\'s horn', ['2']).
card_cmc('demon\'s horn', 2).
card_layout('demon\'s horn', 'normal').

% Found in: DD3_DVD, DDC, DIS
card_name('demon\'s jester', 'Demon\'s Jester').
card_type('demon\'s jester', 'Creature — Imp').
card_types('demon\'s jester', ['Creature']).
card_subtypes('demon\'s jester', ['Imp']).
card_colors('demon\'s jester', ['B']).
card_text('demon\'s jester', 'Flying\nHellbent — Demon\'s Jester gets +2/+1 as long as you have no cards in hand.').
card_mana_cost('demon\'s jester', ['3', 'B']).
card_cmc('demon\'s jester', 4).
card_layout('demon\'s jester', 'normal').
card_power('demon\'s jester', 2).
card_toughness('demon\'s jester', 2).

% Found in: DD2, DD3_JVC, DIS
card_name('demonfire', 'Demonfire').
card_type('demonfire', 'Sorcery').
card_types('demonfire', ['Sorcery']).
card_subtypes('demonfire', []).
card_colors('demonfire', ['R']).
card_text('demonfire', 'Demonfire deals X damage to target creature or player. If a creature dealt damage this way would die this turn, exile it instead.\nHellbent — If you have no cards in hand, Demonfire can\'t be countered by spells or abilities and the damage can\'t be prevented.').
card_mana_cost('demonfire', ['X', 'R']).
card_cmc('demonfire', 1).
card_layout('demonfire', 'normal').

% Found in: ROE
card_name('demonic appetite', 'Demonic Appetite').
card_type('demonic appetite', 'Enchantment — Aura').
card_types('demonic appetite', ['Enchantment']).
card_subtypes('demonic appetite', ['Aura']).
card_colors('demonic appetite', ['B']).
card_text('demonic appetite', 'Enchant creature you control\nEnchanted creature gets +3/+3.\nAt the beginning of your upkeep, sacrifice a creature.').
card_mana_cost('demonic appetite', ['B']).
card_cmc('demonic appetite', 1).
card_layout('demonic appetite', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB
card_name('demonic attorney', 'Demonic Attorney').
card_type('demonic attorney', 'Sorcery').
card_types('demonic attorney', ['Sorcery']).
card_subtypes('demonic attorney', []).
card_colors('demonic attorney', ['B']).
card_text('demonic attorney', 'Remove Demonic Attorney from your deck before playing if you\'re not playing for ante.\nEach player antes the top card of his or her library.').
card_mana_cost('demonic attorney', ['1', 'B', 'B']).
card_cmc('demonic attorney', 3).
card_layout('demonic attorney', 'normal').
card_reserved('demonic attorney').

% Found in: TSP
card_name('demonic collusion', 'Demonic Collusion').
card_type('demonic collusion', 'Sorcery').
card_types('demonic collusion', ['Sorcery']).
card_subtypes('demonic collusion', []).
card_colors('demonic collusion', ['B']).
card_text('demonic collusion', 'Buyback—Discard two cards. (You may discard two cards in addition to any other costs as you cast this spell. If you do, put this card into your hand as it resolves.)\nSearch your library for a card and put that card into your hand. Then shuffle your library.').
card_mana_cost('demonic collusion', ['3', 'B', 'B']).
card_cmc('demonic collusion', 5).
card_layout('demonic collusion', 'normal').

% Found in: ICE, ME2
card_name('demonic consultation', 'Demonic Consultation').
card_type('demonic consultation', 'Instant').
card_types('demonic consultation', ['Instant']).
card_subtypes('demonic consultation', []).
card_colors('demonic consultation', ['B']).
card_text('demonic consultation', 'Name a card. Exile the top six cards of your library, then reveal cards from the top of your library until you reveal the named card. Put that card into your hand and exile all other cards revealed this way.').
card_mana_cost('demonic consultation', ['B']).
card_cmc('demonic consultation', 1).
card_layout('demonic consultation', 'normal').

% Found in: ARB
card_name('demonic dread', 'Demonic Dread').
card_type('demonic dread', 'Sorcery').
card_types('demonic dread', ['Sorcery']).
card_subtypes('demonic dread', []).
card_colors('demonic dread', ['B', 'R']).
card_text('demonic dread', 'Cascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)\nTarget creature can\'t block this turn.').
card_mana_cost('demonic dread', ['1', 'B', 'R']).
card_cmc('demonic dread', 3).
card_layout('demonic dread', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME4
card_name('demonic hordes', 'Demonic Hordes').
card_type('demonic hordes', 'Creature — Demon').
card_types('demonic hordes', ['Creature']).
card_subtypes('demonic hordes', ['Demon']).
card_colors('demonic hordes', ['B']).
card_text('demonic hordes', '{T}: Destroy target land.\nAt the beginning of your upkeep, unless you pay {B}{B}{B}, tap Demonic Hordes and sacrifice a land of an opponent\'s choice.').
card_mana_cost('demonic hordes', ['3', 'B', 'B', 'B']).
card_cmc('demonic hordes', 6).
card_layout('demonic hordes', 'normal').
card_power('demonic hordes', 5).
card_toughness('demonic hordes', 5).
card_reserved('demonic hordes').

% Found in: ORI
card_name('demonic pact', 'Demonic Pact').
card_type('demonic pact', 'Enchantment').
card_types('demonic pact', ['Enchantment']).
card_subtypes('demonic pact', []).
card_colors('demonic pact', ['B']).
card_text('demonic pact', 'At the beginning of your upkeep, choose one that hasn\'t been chosen —\n• Demonic Pact deals 4 damage to target creature or player and you gain 4 life.\n• Target opponent discards two cards.\n• Draw two cards.\n• You lose the game.').
card_mana_cost('demonic pact', ['2', 'B', 'B']).
card_cmc('demonic pact', 4).
card_layout('demonic pact', 'normal').

% Found in: AVR
card_name('demonic rising', 'Demonic Rising').
card_type('demonic rising', 'Enchantment').
card_types('demonic rising', ['Enchantment']).
card_subtypes('demonic rising', []).
card_colors('demonic rising', ['B']).
card_text('demonic rising', 'At the beginning of your end step, if you control exactly one creature, put a 5/5 black Demon creature token with flying onto the battlefield.').
card_mana_cost('demonic rising', ['3', 'B', 'B']).
card_cmc('demonic rising', 5).
card_layout('demonic rising', 'normal').

% Found in: AVR
card_name('demonic taskmaster', 'Demonic Taskmaster').
card_type('demonic taskmaster', 'Creature — Demon').
card_types('demonic taskmaster', ['Creature']).
card_subtypes('demonic taskmaster', ['Demon']).
card_colors('demonic taskmaster', ['B']).
card_text('demonic taskmaster', 'Flying\nAt the beginning of your upkeep, sacrifice a creature other than Demonic Taskmaster.').
card_mana_cost('demonic taskmaster', ['2', 'B']).
card_cmc('demonic taskmaster', 3).
card_layout('demonic taskmaster', 'normal').
card_power('demonic taskmaster', 4).
card_toughness('demonic taskmaster', 3).

% Found in: LEG, ME3
card_name('demonic torment', 'Demonic Torment').
card_type('demonic torment', 'Enchantment — Aura').
card_types('demonic torment', ['Enchantment']).
card_subtypes('demonic torment', ['Aura']).
card_colors('demonic torment', ['B']).
card_text('demonic torment', 'Enchant creature\nEnchanted creature can\'t attack.\nPrevent all combat damage that would be dealt by enchanted creature.').
card_mana_cost('demonic torment', ['2', 'B']).
card_cmc('demonic torment', 3).
card_layout('demonic torment', 'normal').

% Found in: 2ED, 3ED, CED, CEI, DD3_DVD, DDC, LEA, LEB, ME4, VMA, pJGP
card_name('demonic tutor', 'Demonic Tutor').
card_type('demonic tutor', 'Sorcery').
card_types('demonic tutor', ['Sorcery']).
card_subtypes('demonic tutor', []).
card_colors('demonic tutor', ['B']).
card_text('demonic tutor', 'Search your library for a card and put that card into your hand. Then shuffle your library.').
card_mana_cost('demonic tutor', ['1', 'B']).
card_cmc('demonic tutor', 2).
card_layout('demonic tutor', 'normal').

% Found in: AVR
card_name('demonlord of ashmouth', 'Demonlord of Ashmouth').
card_type('demonlord of ashmouth', 'Creature — Demon').
card_types('demonlord of ashmouth', ['Creature']).
card_subtypes('demonlord of ashmouth', ['Demon']).
card_colors('demonlord of ashmouth', ['B']).
card_text('demonlord of ashmouth', 'Flying\nWhen Demonlord of Ashmouth enters the battlefield, exile it unless you sacrifice another creature.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('demonlord of ashmouth', ['2', 'B', 'B']).
card_cmc('demonlord of ashmouth', 4).
card_layout('demonlord of ashmouth', 'normal').
card_power('demonlord of ashmouth', 5).
card_toughness('demonlord of ashmouth', 4).

% Found in: ISD
card_name('demonmail hauberk', 'Demonmail Hauberk').
card_type('demonmail hauberk', 'Artifact — Equipment').
card_types('demonmail hauberk', ['Artifact']).
card_subtypes('demonmail hauberk', ['Equipment']).
card_colors('demonmail hauberk', []).
card_text('demonmail hauberk', 'Equipped creature gets +4/+2.\nEquip—Sacrifice a creature.').
card_mana_cost('demonmail hauberk', ['4']).
card_cmc('demonmail hauberk', 4).
card_layout('demonmail hauberk', 'normal').

% Found in: ARB
card_name('demonspine whip', 'Demonspine Whip').
card_type('demonspine whip', 'Artifact — Equipment').
card_types('demonspine whip', ['Artifact']).
card_subtypes('demonspine whip', ['Equipment']).
card_colors('demonspine whip', ['B', 'R']).
card_text('demonspine whip', '{X}: Equipped creature gets +X/+0 until end of turn.\nEquip {1}').
card_mana_cost('demonspine whip', ['B', 'R']).
card_cmc('demonspine whip', 2).
card_layout('demonspine whip', 'normal').

% Found in: ODY
card_name('demoralize', 'Demoralize').
card_type('demoralize', 'Instant').
card_types('demoralize', ['Instant']).
card_subtypes('demoralize', []).
card_colors('demoralize', ['R']).
card_text('demoralize', 'All creatures gain menace until end of turn. (They can\'t be blocked except by two or more creatures.)\nThreshold — If seven or more cards are in your graveyard, creatures can\'t block this turn.').
card_mana_cost('demoralize', ['2', 'R']).
card_cmc('demoralize', 3).
card_layout('demoralize', 'normal').

% Found in: 10E, 8ED, 9ED, M12, ONS, ROE
card_name('demystify', 'Demystify').
card_type('demystify', 'Instant').
card_types('demystify', ['Instant']).
card_subtypes('demystify', []).
card_colors('demystify', ['W']).
card_text('demystify', 'Destroy target enchantment.').
card_mana_cost('demystify', ['W']).
card_cmc('demystify', 1).
card_layout('demystify', 'normal').

% Found in: DTK
card_name('den protector', 'Den Protector').
card_type('den protector', 'Creature — Human Warrior').
card_types('den protector', ['Creature']).
card_subtypes('den protector', ['Human', 'Warrior']).
card_colors('den protector', ['G']).
card_text('den protector', 'Creatures with power less than Den Protector\'s power can\'t block it.\nMegamorph {1}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Den Protector is turned face up, return target card from your graveyard to your hand.').
card_mana_cost('den protector', ['1', 'G']).
card_cmc('den protector', 2).
card_layout('den protector', 'normal').
card_power('den protector', 2).
card_toughness('den protector', 1).

% Found in: UGL
card_name('denied!', 'Denied!').
card_type('denied!', 'Instant').
card_types('denied!', ['Instant']).
card_subtypes('denied!', []).
card_colors('denied!', ['U']).
card_text('denied!', 'Play Denied only as any opponent casts target spell. Name a card, then look at all cards in that player\'s hand. If the named card is in the player\'s hand, counter target spell.').
card_mana_cost('denied!', ['U']).
card_cmc('denied!', 1).
card_layout('denied!', 'normal').

% Found in: 10E, DPA, PO2, S99
card_name('denizen of the deep', 'Denizen of the Deep').
card_type('denizen of the deep', 'Creature — Serpent').
card_types('denizen of the deep', ['Creature']).
card_subtypes('denizen of the deep', ['Serpent']).
card_colors('denizen of the deep', ['U']).
card_text('denizen of the deep', 'When Denizen of the Deep enters the battlefield, return each other creature you control to its owner\'s hand.').
card_mana_cost('denizen of the deep', ['6', 'U', 'U']).
card_cmc('denizen of the deep', 8).
card_layout('denizen of the deep', 'normal').
card_power('denizen of the deep', 11).
card_toughness('denizen of the deep', 11).

% Found in: SOK
card_name('dense canopy', 'Dense Canopy').
card_type('dense canopy', 'Enchantment').
card_types('dense canopy', ['Enchantment']).
card_subtypes('dense canopy', []).
card_colors('dense canopy', ['G']).
card_text('dense canopy', 'Creatures with flying can block only creatures with flying.').
card_mana_cost('dense canopy', ['1', 'G']).
card_cmc('dense canopy', 2).
card_layout('dense canopy', 'normal').

% Found in: 6ED, WTH
card_name('dense foliage', 'Dense Foliage').
card_type('dense foliage', 'Enchantment').
card_types('dense foliage', ['Enchantment']).
card_subtypes('dense foliage', []).
card_colors('dense foliage', ['G']).
card_text('dense foliage', 'Creatures can\'t be the targets of spells.').
card_mana_cost('dense foliage', ['2', 'G']).
card_cmc('dense foliage', 3).
card_layout('dense foliage', 'normal').

% Found in: ARB, PC2
card_name('deny reality', 'Deny Reality').
card_type('deny reality', 'Sorcery').
card_types('deny reality', ['Sorcery']).
card_subtypes('deny reality', []).
card_colors('deny reality', ['U', 'B']).
card_text('deny reality', 'Cascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)\nReturn target permanent to its owner\'s hand.').
card_mana_cost('deny reality', ['3', 'U', 'B']).
card_cmc('deny reality', 5).
card_layout('deny reality', 'normal').

% Found in: PCY
card_name('denying wind', 'Denying Wind').
card_type('denying wind', 'Sorcery').
card_types('denying wind', ['Sorcery']).
card_subtypes('denying wind', []).
card_colors('denying wind', ['U']).
card_text('denying wind', 'Search target player\'s library for up to seven cards and exile them. Then that player shuffles his or her library.').
card_mana_cost('denying wind', ['7', 'U', 'U']).
card_cmc('denying wind', 9).
card_layout('denying wind', 'normal').

% Found in: C14
card_name('deploy to the front', 'Deploy to the Front').
card_type('deploy to the front', 'Sorcery').
card_types('deploy to the front', ['Sorcery']).
card_subtypes('deploy to the front', []).
card_colors('deploy to the front', ['W']).
card_text('deploy to the front', 'Put X 1/1 white Soldier creature tokens onto the battlefield, where X is the number of creatures on the battlefield.').
card_mana_cost('deploy to the front', ['5', 'W', 'W']).
card_cmc('deploy to the front', 7).
card_layout('deploy to the front', 'normal').

% Found in: ROE
card_name('deprive', 'Deprive').
card_type('deprive', 'Instant').
card_types('deprive', ['Instant']).
card_subtypes('deprive', []).
card_colors('deprive', ['U']).
card_text('deprive', 'As an additional cost to cast Deprive, return a land you control to its owner\'s hand.\nCounter target spell.').
card_mana_cost('deprive', ['U', 'U']).
card_cmc('deprive', 2).
card_layout('deprive', 'normal').

% Found in: DGM
card_name('deputy of acquittals', 'Deputy of Acquittals').
card_type('deputy of acquittals', 'Creature — Human Wizard').
card_types('deputy of acquittals', ['Creature']).
card_subtypes('deputy of acquittals', ['Human', 'Wizard']).
card_colors('deputy of acquittals', ['W', 'U']).
card_text('deputy of acquittals', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Deputy of Acquittals enters the battlefield, you may return another target creature you control to its owner\'s hand.').
card_mana_cost('deputy of acquittals', ['W', 'U']).
card_cmc('deputy of acquittals', 2).
card_layout('deputy of acquittals', 'normal').
card_power('deputy of acquittals', 2).
card_toughness('deputy of acquittals', 2).

% Found in: ISD
card_name('deranged assistant', 'Deranged Assistant').
card_type('deranged assistant', 'Creature — Human Wizard').
card_types('deranged assistant', ['Creature']).
card_subtypes('deranged assistant', ['Human', 'Wizard']).
card_colors('deranged assistant', ['U']).
card_text('deranged assistant', '{T}, Put the top card of your library into your graveyard: Add {1} to your mana pool.').
card_mana_cost('deranged assistant', ['1', 'U']).
card_cmc('deranged assistant', 2).
card_layout('deranged assistant', 'normal').
card_power('deranged assistant', 1).
card_toughness('deranged assistant', 1).

% Found in: ULG, VMA, pJGP
card_name('deranged hermit', 'Deranged Hermit').
card_type('deranged hermit', 'Creature — Elf').
card_types('deranged hermit', ['Creature']).
card_subtypes('deranged hermit', ['Elf']).
card_colors('deranged hermit', ['G']).
card_text('deranged hermit', 'Echo {3}{G}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Deranged Hermit enters the battlefield, put four 1/1 green Squirrel creature tokens onto the battlefield.\nSquirrel creatures get +1/+1.').
card_mana_cost('deranged hermit', ['3', 'G', 'G']).
card_cmc('deranged hermit', 5).
card_layout('deranged hermit', 'normal').
card_power('deranged hermit', 1).
card_toughness('deranged hermit', 1).
card_reserved('deranged hermit').

% Found in: DKA
card_name('deranged outcast', 'Deranged Outcast').
card_type('deranged outcast', 'Creature — Human Rogue').
card_types('deranged outcast', ['Creature']).
card_subtypes('deranged outcast', ['Human', 'Rogue']).
card_colors('deranged outcast', ['G']).
card_text('deranged outcast', '{1}{G}, Sacrifice a Human: Put two +1/+1 counters on target creature.').
card_mana_cost('deranged outcast', ['1', 'G']).
card_cmc('deranged outcast', 2).
card_layout('deranged outcast', 'normal').
card_power('deranged outcast', 2).
card_toughness('deranged outcast', 1).

% Found in: 5ED, 6ED, FEM, MED
card_name('derelor', 'Derelor').
card_type('derelor', 'Creature — Thrull').
card_types('derelor', ['Creature']).
card_subtypes('derelor', ['Thrull']).
card_colors('derelor', ['B']).
card_text('derelor', 'Black spells you cast cost {B} more to cast.').
card_mana_cost('derelor', ['3', 'B']).
card_cmc('derelor', 4).
card_layout('derelor', 'normal').
card_power('derelor', 4).
card_toughness('derelor', 4).

% Found in: C13
card_name('derevi, empyrial tactician', 'Derevi, Empyrial Tactician').
card_type('derevi, empyrial tactician', 'Legendary Creature — Bird Wizard').
card_types('derevi, empyrial tactician', ['Creature']).
card_subtypes('derevi, empyrial tactician', ['Bird', 'Wizard']).
card_supertypes('derevi, empyrial tactician', ['Legendary']).
card_colors('derevi, empyrial tactician', ['W', 'U', 'G']).
card_text('derevi, empyrial tactician', 'Flying\nWhenever Derevi, Empyrial Tactician enters the battlefield or a creature you control deals combat damage to a player, you may tap or untap target permanent.\n{1}{G}{W}{U}: Put Derevi onto the battlefield from the command zone.').
card_mana_cost('derevi, empyrial tactician', ['G', 'W', 'U']).
card_cmc('derevi, empyrial tactician', 3).
card_layout('derevi, empyrial tactician', 'normal').
card_power('derevi, empyrial tactician', 2).
card_toughness('derevi, empyrial tactician', 3).

% Found in: LGN
card_name('dermoplasm', 'Dermoplasm').
card_type('dermoplasm', 'Creature — Shapeshifter').
card_types('dermoplasm', ['Creature']).
card_subtypes('dermoplasm', ['Shapeshifter']).
card_colors('dermoplasm', ['U']).
card_text('dermoplasm', 'Flying\nMorph {2}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Dermoplasm is turned face up, you may put a creature card with a morph ability from your hand onto the battlefield face up. If you do, return Dermoplasm to its owner\'s hand.').
card_mana_cost('dermoplasm', ['2', 'U']).
card_cmc('dermoplasm', 3).
card_layout('dermoplasm', 'normal').
card_power('dermoplasm', 1).
card_toughness('dermoplasm', 1).

% Found in: SOK
card_name('descendant of kiyomaro', 'Descendant of Kiyomaro').
card_type('descendant of kiyomaro', 'Creature — Human Soldier').
card_types('descendant of kiyomaro', ['Creature']).
card_subtypes('descendant of kiyomaro', ['Human', 'Soldier']).
card_colors('descendant of kiyomaro', ['W']).
card_text('descendant of kiyomaro', 'As long as you have more cards in hand than each opponent, Descendant of Kiyomaro gets +1/+2 and has \"Whenever this creature deals combat damage, you gain 3 life.\"').
card_mana_cost('descendant of kiyomaro', ['1', 'W', 'W']).
card_cmc('descendant of kiyomaro', 3).
card_layout('descendant of kiyomaro', 'normal').
card_power('descendant of kiyomaro', 2).
card_toughness('descendant of kiyomaro', 3).

% Found in: SOK
card_name('descendant of masumaro', 'Descendant of Masumaro').
card_type('descendant of masumaro', 'Creature — Human Monk').
card_types('descendant of masumaro', ['Creature']).
card_subtypes('descendant of masumaro', ['Human', 'Monk']).
card_colors('descendant of masumaro', ['G']).
card_text('descendant of masumaro', 'At the beginning of your upkeep, put a +1/+1 counter on Descendant of Masumaro for each card in your hand, then remove a +1/+1 counter from Descendant of Masumaro for each card in target opponent\'s hand.').
card_mana_cost('descendant of masumaro', ['2', 'G']).
card_cmc('descendant of masumaro', 3).
card_layout('descendant of masumaro', 'normal').
card_power('descendant of masumaro', 1).
card_toughness('descendant of masumaro', 1).

% Found in: SOK
card_name('descendant of soramaro', 'Descendant of Soramaro').
card_type('descendant of soramaro', 'Creature — Human Wizard').
card_types('descendant of soramaro', ['Creature']).
card_subtypes('descendant of soramaro', ['Human', 'Wizard']).
card_colors('descendant of soramaro', ['U']).
card_text('descendant of soramaro', '{1}{U}: Look at the top X cards of your library, where X is the number of cards in your hand, then put them back in any order.').
card_mana_cost('descendant of soramaro', ['3', 'U']).
card_cmc('descendant of soramaro', 4).
card_layout('descendant of soramaro', 'normal').
card_power('descendant of soramaro', 2).
card_toughness('descendant of soramaro', 3).

% Found in: AVR
card_name('descendants\' path', 'Descendants\' Path').
card_type('descendants\' path', 'Enchantment').
card_types('descendants\' path', ['Enchantment']).
card_subtypes('descendants\' path', []).
card_colors('descendants\' path', ['G']).
card_text('descendants\' path', 'At the beginning of your upkeep, reveal the top card of your library. If it\'s a creature card that shares a creature type with a creature you control, you may cast that card without paying its mana cost. Otherwise, put that card on the bottom of your library.').
card_mana_cost('descendants\' path', ['2', 'G']).
card_cmc('descendants\' path', 3).
card_layout('descendants\' path', 'normal').

% Found in: AVR
card_name('descent into madness', 'Descent into Madness').
card_type('descent into madness', 'Enchantment').
card_types('descent into madness', ['Enchantment']).
card_subtypes('descent into madness', []).
card_colors('descent into madness', ['B']).
card_text('descent into madness', 'At the beginning of your upkeep, put a despair counter on Descent into Madness, then each player exiles X permanents he or she controls and/or cards from his or her hand, where X is the number of despair counters on Descent into Madness.').
card_mana_cost('descent into madness', ['3', 'B', 'B']).
card_cmc('descent into madness', 5).
card_layout('descent into madness', 'normal').

% Found in: DTK
card_name('descent of the dragons', 'Descent of the Dragons').
card_type('descent of the dragons', 'Sorcery').
card_types('descent of the dragons', ['Sorcery']).
card_subtypes('descent of the dragons', []).
card_colors('descent of the dragons', ['R']).
card_text('descent of the dragons', 'Destroy any number of target creatures. For each creature destroyed this way, its controller puts a 4/4 red Dragon creature token with flying onto the battlefield.').
card_mana_cost('descent of the dragons', ['4', 'R', 'R']).
card_cmc('descent of the dragons', 6).
card_layout('descent of the dragons', 'normal').

% Found in: ZEN
card_name('desecrated earth', 'Desecrated Earth').
card_type('desecrated earth', 'Sorcery').
card_types('desecrated earth', ['Sorcery']).
card_subtypes('desecrated earth', []).
card_colors('desecrated earth', ['B']).
card_text('desecrated earth', 'Destroy target land. Its controller discards a card.').
card_mana_cost('desecrated earth', ['4', 'B']).
card_cmc('desecrated earth', 5).
card_layout('desecrated earth', 'normal').

% Found in: RTR
card_name('desecration demon', 'Desecration Demon').
card_type('desecration demon', 'Creature — Demon').
card_types('desecration demon', ['Creature']).
card_subtypes('desecration demon', ['Demon']).
card_colors('desecration demon', ['B']).
card_text('desecration demon', 'Flying\nAt the beginning of each combat, any opponent may sacrifice a creature. If a player does, tap Desecration Demon and put a +1/+1 counter on it.').
card_mana_cost('desecration demon', ['2', 'B', 'B']).
card_cmc('desecration demon', 4).
card_layout('desecration demon', 'normal').
card_power('desecration demon', 6).
card_toughness('desecration demon', 6).

% Found in: 5DN
card_name('desecration elemental', 'Desecration Elemental').
card_type('desecration elemental', 'Creature — Elemental').
card_types('desecration elemental', ['Creature']).
card_subtypes('desecration elemental', ['Elemental']).
card_colors('desecration elemental', ['B']).
card_text('desecration elemental', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nWhenever a player casts a spell, sacrifice a creature.').
card_mana_cost('desecration elemental', ['3', 'B']).
card_cmc('desecration elemental', 4).
card_layout('desecration elemental', 'normal').
card_power('desecration elemental', 8).
card_toughness('desecration elemental', 8).

% Found in: JOU
card_name('desecration plague', 'Desecration Plague').
card_type('desecration plague', 'Sorcery').
card_types('desecration plague', ['Sorcery']).
card_subtypes('desecration plague', []).
card_colors('desecration plague', ['G']).
card_text('desecration plague', 'Destroy target enchantment or land.').
card_mana_cost('desecration plague', ['3', 'G']).
card_cmc('desecration plague', 4).
card_layout('desecration plague', 'normal').

% Found in: CMD, EVE
card_name('desecrator hag', 'Desecrator Hag').
card_type('desecrator hag', 'Creature — Hag').
card_types('desecrator hag', ['Creature']).
card_subtypes('desecrator hag', ['Hag']).
card_colors('desecrator hag', ['B', 'G']).
card_text('desecrator hag', 'When Desecrator Hag enters the battlefield, return to your hand the creature card in your graveyard with the greatest power. If two or more cards are tied for greatest power, you choose one of them.').
card_mana_cost('desecrator hag', ['2', 'B/G', 'B/G']).
card_cmc('desecrator hag', 4).
card_layout('desecrator hag', 'normal').
card_power('desecrator hag', 2).
card_toughness('desecrator hag', 2).

% Found in: ARN, TSB, V12, pFNM
card_name('desert', 'Desert').
card_type('desert', 'Land — Desert').
card_types('desert', ['Land']).
card_subtypes('desert', ['Desert']).
card_colors('desert', []).
card_text('desert', '{T}: Add {1} to your mana pool.\n{T}: Desert deals 1 damage to target attacking creature. Activate this ability only during the end of combat step.').
card_layout('desert', 'normal').

% Found in: POR
card_name('desert drake', 'Desert Drake').
card_type('desert drake', 'Creature — Drake').
card_types('desert drake', ['Creature']).
card_subtypes('desert drake', ['Drake']).
card_colors('desert drake', ['R']).
card_text('desert drake', 'Flying').
card_mana_cost('desert drake', ['3', 'R']).
card_cmc('desert drake', 4).
card_layout('desert drake', 'normal').
card_power('desert drake', 2).
card_toughness('desert drake', 2).

% Found in: ARN
card_name('desert nomads', 'Desert Nomads').
card_type('desert nomads', 'Creature — Human Nomad').
card_types('desert nomads', ['Creature']).
card_subtypes('desert nomads', ['Human', 'Nomad']).
card_colors('desert nomads', ['R']).
card_text('desert nomads', 'Desertwalk\nPrevent all damage that would be dealt to Desert Nomads by Deserts.').
card_mana_cost('desert nomads', ['2', 'R']).
card_cmc('desert nomads', 3).
card_layout('desert nomads', 'normal').
card_power('desert nomads', 2).
card_toughness('desert nomads', 2).

% Found in: PTK
card_name('desert sandstorm', 'Desert Sandstorm').
card_type('desert sandstorm', 'Sorcery').
card_types('desert sandstorm', ['Sorcery']).
card_subtypes('desert sandstorm', []).
card_colors('desert sandstorm', ['R']).
card_text('desert sandstorm', 'Desert Sandstorm deals 1 damage to each creature.').
card_mana_cost('desert sandstorm', ['2', 'R']).
card_cmc('desert sandstorm', 3).
card_layout('desert sandstorm', 'normal').

% Found in: 3ED, 4ED, 5ED, ARN, C14, ME3, MMQ, VMA
card_name('desert twister', 'Desert Twister').
card_type('desert twister', 'Sorcery').
card_types('desert twister', ['Sorcery']).
card_subtypes('desert twister', []).
card_colors('desert twister', ['G']).
card_text('desert twister', 'Destroy target permanent.').
card_mana_cost('desert twister', ['4', 'G', 'G']).
card_cmc('desert twister', 6).
card_layout('desert twister', 'normal').

% Found in: ODY
card_name('deserted temple', 'Deserted Temple').
card_type('deserted temple', 'Land').
card_types('deserted temple', ['Land']).
card_subtypes('deserted temple', []).
card_colors('deserted temple', []).
card_text('deserted temple', '{T}: Add {1} to your mana pool.\n{1}, {T}: Untap target land.').
card_layout('deserted temple', 'normal').

% Found in: JOU
card_name('deserter\'s quarters', 'Deserter\'s Quarters').
card_type('deserter\'s quarters', 'Artifact').
card_types('deserter\'s quarters', ['Artifact']).
card_subtypes('deserter\'s quarters', []).
card_colors('deserter\'s quarters', []).
card_text('deserter\'s quarters', 'You may choose not to untap Deserter\'s Quarters during your untap step.\n{6}, {T}: Tap target creature. It doesn\'t untap during its controller\'s untap step for as long as Deserter\'s Quarters remains tapped.').
card_mana_cost('deserter\'s quarters', ['2']).
card_cmc('deserter\'s quarters', 2).
card_layout('deserter\'s quarters', 'normal').

% Found in: 6ED, CM1, VIS
card_name('desertion', 'Desertion').
card_type('desertion', 'Instant').
card_types('desertion', ['Instant']).
card_subtypes('desertion', []).
card_colors('desertion', ['U']).
card_text('desertion', 'Counter target spell. If an artifact or creature spell is countered this way, put that card onto the battlefield under your control instead of into its owner\'s graveyard.').
card_mana_cost('desertion', ['3', 'U', 'U']).
card_cmc('desertion', 5).
card_layout('desertion', 'normal').

% Found in: AVR
card_name('desolate lighthouse', 'Desolate Lighthouse').
card_type('desolate lighthouse', 'Land').
card_types('desolate lighthouse', ['Land']).
card_subtypes('desolate lighthouse', []).
card_colors('desolate lighthouse', []).
card_text('desolate lighthouse', '{T}: Add {1} to your mana pool.\n{1}{U}{R}, {T}: Draw a card, then discard a card.').
card_layout('desolate lighthouse', 'normal').

% Found in: VIS
card_name('desolation', 'Desolation').
card_type('desolation', 'Enchantment').
card_types('desolation', ['Enchantment']).
card_subtypes('desolation', []).
card_colors('desolation', ['B']).
card_text('desolation', 'At the beginning of each end step, each player who tapped a land for mana this turn sacrifices a land. Desolation deals 2 damage to each player who sacrificed a Plains this way.').
card_mana_cost('desolation', ['1', 'B', 'B']).
card_cmc('desolation', 3).
card_layout('desolation', 'normal').

% Found in: APC
card_name('desolation angel', 'Desolation Angel').
card_type('desolation angel', 'Creature — Angel').
card_types('desolation angel', ['Creature']).
card_subtypes('desolation angel', ['Angel']).
card_colors('desolation angel', ['B']).
card_text('desolation angel', 'Kicker {W}{W} (You may pay an additional {W}{W} as you cast this spell.)\nFlying\nWhen Desolation Angel enters the battlefield, destroy all lands you control. If it was kicked, destroy all lands instead.').
card_mana_cost('desolation angel', ['3', 'B', 'B']).
card_cmc('desolation angel', 5).
card_layout('desolation angel', 'normal').
card_power('desolation angel', 5).
card_toughness('desolation angel', 4).

% Found in: APC, TSB
card_name('desolation giant', 'Desolation Giant').
card_type('desolation giant', 'Creature — Giant').
card_types('desolation giant', ['Creature']).
card_subtypes('desolation giant', ['Giant']).
card_colors('desolation giant', ['R']).
card_text('desolation giant', 'Kicker {W}{W} (You may pay an additional {W}{W} as you cast this spell.)\nWhen Desolation Giant enters the battlefield, destroy all other creatures you control. If it was kicked, destroy all other creatures instead.').
card_mana_cost('desolation giant', ['2', 'R', 'R']).
card_cmc('desolation giant', 4).
card_layout('desolation giant', 'normal').
card_power('desolation giant', 3).
card_toughness('desolation giant', 3).

% Found in: BFZ
card_name('desolation twin', 'Desolation Twin').
card_type('desolation twin', 'Creature — Eldrazi').
card_types('desolation twin', ['Creature']).
card_subtypes('desolation twin', ['Eldrazi']).
card_colors('desolation twin', []).
card_text('desolation twin', 'When you cast Desolation Twin, put a 10/10 colorless Eldrazi creature token onto the battlefield.').
card_mana_cost('desolation twin', ['10']).
card_cmc('desolation twin', 10).
card_layout('desolation twin', 'normal').
card_power('desolation twin', 10).
card_toughness('desolation twin', 10).

% Found in: ME3, PTK
card_name('desperate charge', 'Desperate Charge').
card_type('desperate charge', 'Sorcery').
card_types('desperate charge', ['Sorcery']).
card_subtypes('desperate charge', []).
card_colors('desperate charge', ['B']).
card_text('desperate charge', 'Creatures you control get +2/+0 until end of turn.').
card_mana_cost('desperate charge', ['2', 'B']).
card_cmc('desperate charge', 3).
card_layout('desperate charge', 'normal').

% Found in: WTH
card_name('desperate gambit', 'Desperate Gambit').
card_type('desperate gambit', 'Instant').
card_types('desperate gambit', ['Instant']).
card_subtypes('desperate gambit', []).
card_colors('desperate gambit', ['R']).
card_text('desperate gambit', 'Choose a source you control and flip a coin. If you win the flip, the next time that source would deal damage this turn, it deals double that damage instead. If you lose the flip, the next time it would deal damage this turn, prevent that damage.').
card_mana_cost('desperate gambit', ['R']).
card_cmc('desperate gambit', 1).
card_layout('desperate gambit', 'normal').

% Found in: ISD
card_name('desperate ravings', 'Desperate Ravings').
card_type('desperate ravings', 'Instant').
card_types('desperate ravings', ['Instant']).
card_subtypes('desperate ravings', []).
card_colors('desperate ravings', ['R']).
card_text('desperate ravings', 'Draw two cards, then discard a card at random.\nFlashback {2}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('desperate ravings', ['1', 'R']).
card_cmc('desperate ravings', 2).
card_layout('desperate ravings', 'normal').

% Found in: INV
card_name('desperate research', 'Desperate Research').
card_type('desperate research', 'Sorcery').
card_types('desperate research', ['Sorcery']).
card_subtypes('desperate research', []).
card_colors('desperate research', ['B']).
card_text('desperate research', 'Name a card other than a basic land card. Then reveal the top seven cards of your library and put all of them with that name into your hand. Exile the rest.').
card_mana_cost('desperate research', ['1', 'B']).
card_cmc('desperate research', 2).
card_layout('desperate research', 'normal').

% Found in: CHK, MMA
card_name('desperate ritual', 'Desperate Ritual').
card_type('desperate ritual', 'Instant — Arcane').
card_types('desperate ritual', ['Instant']).
card_subtypes('desperate ritual', ['Arcane']).
card_colors('desperate ritual', ['R']).
card_text('desperate ritual', 'Add {R}{R}{R} to your mana pool.\nSplice onto Arcane {1}{R} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('desperate ritual', ['1', 'R']).
card_cmc('desperate ritual', 2).
card_layout('desperate ritual', 'normal').

% Found in: JOU
card_name('desperate stand', 'Desperate Stand').
card_type('desperate stand', 'Sorcery').
card_types('desperate stand', ['Sorcery']).
card_subtypes('desperate stand', []).
card_colors('desperate stand', ['W', 'R']).
card_text('desperate stand', 'Strive — Desperate Stand costs {R}{W} more to cast for each target beyond the first.\nAny number of target creatures each get +2/+0 and gain first strike and vigilance until end of turn.').
card_mana_cost('desperate stand', ['R', 'W']).
card_cmc('desperate stand', 2).
card_layout('desperate stand', 'normal').

% Found in: KTK, NPH, pFNM
card_name('despise', 'Despise').
card_type('despise', 'Sorcery').
card_types('despise', ['Sorcery']).
card_subtypes('despise', []).
card_colors('despise', ['B']).
card_text('despise', 'Target opponent reveals his or her hand. You choose a creature or planeswalker card from it. That player discards that card.').
card_mana_cost('despise', ['B']).
card_cmc('despise', 1).
card_layout('despise', 'normal').

% Found in: PCY
card_name('despoil', 'Despoil').
card_type('despoil', 'Sorcery').
card_types('despoil', ['Sorcery']).
card_subtypes('despoil', []).
card_colors('despoil', ['B']).
card_text('despoil', 'Destroy target land. Its controller loses 2 life.').
card_mana_cost('despoil', ['3', 'B']).
card_cmc('despoil', 4).
card_layout('despoil', 'normal').

% Found in: ORI
card_name('despoiler of souls', 'Despoiler of Souls').
card_type('despoiler of souls', 'Creature — Horror').
card_types('despoiler of souls', ['Creature']).
card_subtypes('despoiler of souls', ['Horror']).
card_colors('despoiler of souls', ['B']).
card_text('despoiler of souls', 'Despoiler of Souls can\'t block.\n{B}{B}, Exile two other creature cards from your graveyard: Return Despoiler of Souls from your graveyard to the battlefield.').
card_mana_cost('despoiler of souls', ['B', 'B']).
card_cmc('despoiler of souls', 2).
card_layout('despoiler of souls', 'normal').
card_power('despoiler of souls', 3).
card_toughness('despoiler of souls', 1).

% Found in: USG
card_name('despondency', 'Despondency').
card_type('despondency', 'Enchantment — Aura').
card_types('despondency', ['Enchantment']).
card_subtypes('despondency', ['Aura']).
card_colors('despondency', ['B']).
card_text('despondency', 'Enchant creature\nEnchanted creature gets -2/-0.\nWhen Despondency is put into a graveyard from the battlefield, return Despondency to its owner\'s hand.').
card_mana_cost('despondency', ['1', 'B']).
card_cmc('despondency', 2).
card_layout('despondency', 'normal').

% Found in: ICE, ME2
card_name('despotic scepter', 'Despotic Scepter').
card_type('despotic scepter', 'Artifact').
card_types('despotic scepter', ['Artifact']).
card_subtypes('despotic scepter', []).
card_colors('despotic scepter', []).
card_text('despotic scepter', '{T}: Destroy target permanent you own. It can\'t be regenerated.').
card_mana_cost('despotic scepter', ['1']).
card_cmc('despotic scepter', 1).
card_layout('despotic scepter', 'normal').

% Found in: RTR
card_name('destroy the evidence', 'Destroy the Evidence').
card_type('destroy the evidence', 'Sorcery').
card_types('destroy the evidence', ['Sorcery']).
card_subtypes('destroy the evidence', []).
card_colors('destroy the evidence', ['B']).
card_text('destroy the evidence', 'Destroy target land. Its controller reveals cards from the top of his or her library until he or she reveals a land card, then puts those cards into his or her graveyard.').
card_mana_cost('destroy the evidence', ['4', 'B']).
card_cmc('destroy the evidence', 5).
card_layout('destroy the evidence', 'normal').

% Found in: PLS
card_name('destructive flow', 'Destructive Flow').
card_type('destructive flow', 'Enchantment').
card_types('destructive flow', ['Enchantment']).
card_subtypes('destructive flow', []).
card_colors('destructive flow', ['B', 'R', 'G']).
card_text('destructive flow', 'At the beginning of each player\'s upkeep, that player sacrifices a nonbasic land.').
card_mana_cost('destructive flow', ['B', 'R', 'G']).
card_cmc('destructive flow', 3).
card_layout('destructive flow', 'normal').

% Found in: M11
card_name('destructive force', 'Destructive Force').
card_type('destructive force', 'Sorcery').
card_types('destructive force', ['Sorcery']).
card_subtypes('destructive force', []).
card_colors('destructive force', ['R']).
card_text('destructive force', 'Each player sacrifices five lands. Destructive Force deals 5 damage to each creature.').
card_mana_cost('destructive force', ['5', 'R', 'R']).
card_cmc('destructive force', 7).
card_layout('destructive force', 'normal').

% Found in: DDL, THS
card_name('destructive revelry', 'Destructive Revelry').
card_type('destructive revelry', 'Instant').
card_types('destructive revelry', ['Instant']).
card_subtypes('destructive revelry', []).
card_colors('destructive revelry', ['R', 'G']).
card_text('destructive revelry', 'Destroy target artifact or enchantment. Destructive Revelry deals 2 damage to that permanent\'s controller.').
card_mana_cost('destructive revelry', ['R', 'G']).
card_cmc('destructive revelry', 2).
card_layout('destructive revelry', 'normal').

% Found in: USG
card_name('destructive urge', 'Destructive Urge').
card_type('destructive urge', 'Enchantment — Aura').
card_types('destructive urge', ['Enchantment']).
card_subtypes('destructive urge', ['Aura']).
card_colors('destructive urge', ['R']).
card_text('destructive urge', 'Enchant creature\nWhenever enchanted creature deals combat damage to a player, that player sacrifices a land.').
card_mana_cost('destructive urge', ['1', 'R', 'R']).
card_cmc('destructive urge', 3).
card_layout('destructive urge', 'normal').

% Found in: FRF
card_name('destructor dragon', 'Destructor Dragon').
card_type('destructor dragon', 'Creature — Dragon').
card_types('destructor dragon', ['Creature']).
card_subtypes('destructor dragon', ['Dragon']).
card_colors('destructor dragon', ['G']).
card_text('destructor dragon', 'Flying\nWhen Destructor Dragon dies, destroy target noncreature permanent.').
card_mana_cost('destructor dragon', ['4', 'G', 'G']).
card_cmc('destructor dragon', 6).
card_layout('destructor dragon', 'normal').
card_power('destructor dragon', 4).
card_toughness('destructor dragon', 4).

% Found in: TSP
card_name('detainment spell', 'Detainment Spell').
card_type('detainment spell', 'Enchantment — Aura').
card_types('detainment spell', ['Enchantment']).
card_subtypes('detainment spell', ['Aura']).
card_colors('detainment spell', ['W']).
card_text('detainment spell', 'Enchant creature\nEnchanted creature\'s activated abilities can\'t be activated.\n{1}{W}: Attach Detainment Spell to target creature.').
card_mana_cost('detainment spell', ['W']).
card_cmc('detainment spell', 1).
card_layout('detainment spell', 'normal').

% Found in: RTR
card_name('detention sphere', 'Detention Sphere').
card_type('detention sphere', 'Enchantment').
card_types('detention sphere', ['Enchantment']).
card_subtypes('detention sphere', []).
card_colors('detention sphere', ['W', 'U']).
card_text('detention sphere', 'When Detention Sphere enters the battlefield, you may exile target nonland permanent not named Detention Sphere and all other permanents with the same name as that permanent.\nWhen Detention Sphere leaves the battlefield, return the exiled cards to the battlefield under their owner\'s control.').
card_mana_cost('detention sphere', ['1', 'W', 'U']).
card_cmc('detention sphere', 3).
card_layout('detention sphere', 'normal').

% Found in: DIS
card_name('determined', 'Determined').
card_type('determined', 'Instant').
card_types('determined', ['Instant']).
card_subtypes('determined', []).
card_colors('determined', ['U', 'G']).
card_text('determined', 'Other spells you control can\'t be countered by spells or abilities this turn.\nDraw a card.').
card_mana_cost('determined', ['G', 'U']).
card_cmc('determined', 2).
card_layout('determined', 'split').

% Found in: 4ED, 5ED, ATQ, ITP, ME4, MRD, RQS
card_name('detonate', 'Detonate').
card_type('detonate', 'Sorcery').
card_types('detonate', ['Sorcery']).
card_subtypes('detonate', []).
card_colors('detonate', ['R']).
card_text('detonate', 'Destroy target artifact with converted mana cost X. It can\'t be regenerated. Detonate deals X damage to that artifact\'s controller.').
card_mana_cost('detonate', ['X', 'R']).
card_cmc('detonate', 1).
card_layout('detonate', 'normal').

% Found in: PLC
card_name('detritivore', 'Detritivore').
card_type('detritivore', 'Creature — Lhurgoyf').
card_types('detritivore', ['Creature']).
card_subtypes('detritivore', ['Lhurgoyf']).
card_colors('detritivore', ['R']).
card_text('detritivore', 'Detritivore\'s power and toughness are each equal to the number of nonbasic land cards in your opponents\' graveyards.\nSuspend X—{X}{3}{R}. X can\'t be 0. (Rather than cast this card from your hand, you may pay {X}{3}{R} and exile it with X time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)\nWhenever a time counter is removed from Detritivore while it\'s exiled, destroy target nonbasic land.').
card_mana_cost('detritivore', ['2', 'R', 'R']).
card_cmc('detritivore', 4).
card_layout('detritivore', 'normal').
card_power('detritivore', '*').
card_toughness('detritivore', '*').

% Found in: DDL, SHM
card_name('deus of calamity', 'Deus of Calamity').
card_type('deus of calamity', 'Creature — Spirit Avatar').
card_types('deus of calamity', ['Creature']).
card_subtypes('deus of calamity', ['Spirit', 'Avatar']).
card_colors('deus of calamity', ['R', 'G']).
card_text('deus of calamity', 'Trample\nWhenever Deus of Calamity deals 6 or more damage to an opponent, destroy target land that player controls.').
card_mana_cost('deus of calamity', ['R/G', 'R/G', 'R/G', 'R/G', 'R/G']).
card_cmc('deus of calamity', 5).
card_layout('deus of calamity', 'normal').
card_power('deus of calamity', 6).
card_toughness('deus of calamity', 6).

% Found in: PCY
card_name('devastate', 'Devastate').
card_type('devastate', 'Sorcery').
card_types('devastate', ['Sorcery']).
card_subtypes('devastate', []).
card_colors('devastate', ['R']).
card_text('devastate', 'Destroy target land. Devastate deals 1 damage to each creature and each player.').
card_mana_cost('devastate', ['3', 'R', 'R']).
card_cmc('devastate', 5).
card_layout('devastate', 'normal').

% Found in: TOR
card_name('devastating dreams', 'Devastating Dreams').
card_type('devastating dreams', 'Sorcery').
card_types('devastating dreams', ['Sorcery']).
card_subtypes('devastating dreams', []).
card_colors('devastating dreams', ['R']).
card_text('devastating dreams', 'As an additional cost to cast Devastating Dreams, discard X cards at random.\nEach player sacrifices X lands. Devastating Dreams deals X damage to each creature.').
card_mana_cost('devastating dreams', ['R', 'R']).
card_cmc('devastating dreams', 2).
card_layout('devastating dreams', 'normal').

% Found in: ROE
card_name('devastating summons', 'Devastating Summons').
card_type('devastating summons', 'Sorcery').
card_types('devastating summons', ['Sorcery']).
card_subtypes('devastating summons', []).
card_colors('devastating summons', ['R']).
card_text('devastating summons', 'As an additional cost to cast Devastating Summons, sacrifice X lands.\nPut two X/X red Elemental creature tokens onto the battlefield.').
card_mana_cost('devastating summons', ['R']).
card_cmc('devastating summons', 1).
card_layout('devastating summons', 'normal').

% Found in: ME4, POR, S99
card_name('devastation', 'Devastation').
card_type('devastation', 'Sorcery').
card_types('devastation', ['Sorcery']).
card_subtypes('devastation', []).
card_colors('devastation', ['R']).
card_text('devastation', 'Destroy all creatures and lands.').
card_mana_cost('devastation', ['5', 'R', 'R']).
card_cmc('devastation', 7).
card_layout('devastation', 'normal').

% Found in: AVR
card_name('devastation tide', 'Devastation Tide').
card_type('devastation tide', 'Sorcery').
card_types('devastation tide', ['Sorcery']).
card_subtypes('devastation tide', []).
card_colors('devastation tide', ['U']).
card_text('devastation tide', 'Return all nonland permanents to their owners\' hands.\nMiracle {1}{U} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_mana_cost('devastation tide', ['3', 'U', 'U']).
card_cmc('devastation tide', 5).
card_layout('devastation tide', 'normal').

% Found in: DIS
card_name('development', 'Development').
card_type('development', 'Instant').
card_types('development', ['Instant']).
card_subtypes('development', []).
card_colors('development', ['U', 'R']).
card_text('development', 'Put a 3/1 red Elemental creature token onto the battlefield unless any opponent has you draw a card. Repeat this process two more times.').
card_mana_cost('development', ['3', 'U', 'R']).
card_cmc('development', 5).
card_layout('development', 'split').

% Found in: RTR
card_name('deviant glee', 'Deviant Glee').
card_type('deviant glee', 'Enchantment — Aura').
card_types('deviant glee', ['Enchantment']).
card_subtypes('deviant glee', ['Aura']).
card_colors('deviant glee', ['B']).
card_text('deviant glee', 'Enchant creature\nEnchanted creature gets +2/+1 and has \"{R}: This creature gains trample until end of turn.\"').
card_mana_cost('deviant glee', ['B']).
card_cmc('deviant glee', 1).
card_layout('deviant glee', 'normal').

% Found in: DDK, ISD, pMEI
card_name('devil\'s play', 'Devil\'s Play').
card_type('devil\'s play', 'Sorcery').
card_types('devil\'s play', ['Sorcery']).
card_subtypes('devil\'s play', []).
card_colors('devil\'s play', ['R']).
card_text('devil\'s play', 'Devil\'s Play deals X damage to target creature or player.\nFlashback {X}{R}{R}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('devil\'s play', ['X', 'R']).
card_cmc('devil\'s play', 1).
card_layout('devil\'s play', 'normal').

% Found in: ODY
card_name('devoted caretaker', 'Devoted Caretaker').
card_type('devoted caretaker', 'Creature — Human Cleric').
card_types('devoted caretaker', ['Creature']).
card_subtypes('devoted caretaker', ['Human', 'Cleric']).
card_colors('devoted caretaker', ['W']).
card_text('devoted caretaker', '{W}, {T}: Target permanent you control gains protection from instant spells and from sorcery spells until end of turn.').
card_mana_cost('devoted caretaker', ['W']).
card_cmc('devoted caretaker', 1).
card_layout('devoted caretaker', 'normal').
card_power('devoted caretaker', 1).
card_toughness('devoted caretaker', 2).

% Found in: SHM
card_name('devoted druid', 'Devoted Druid').
card_type('devoted druid', 'Creature — Elf Druid').
card_types('devoted druid', ['Creature']).
card_subtypes('devoted druid', ['Elf', 'Druid']).
card_colors('devoted druid', ['G']).
card_text('devoted druid', '{T}: Add {G} to your mana pool.\nPut a -1/-1 counter on Devoted Druid: Untap Devoted Druid.').
card_mana_cost('devoted druid', ['1', 'G']).
card_cmc('devoted druid', 2).
card_layout('devoted druid', 'normal').
card_power('devoted druid', 0).
card_toughness('devoted druid', 2).

% Found in: POR, S99
card_name('devoted hero', 'Devoted Hero').
card_type('devoted hero', 'Creature — Elf Soldier').
card_types('devoted hero', ['Creature']).
card_subtypes('devoted hero', ['Elf', 'Soldier']).
card_colors('devoted hero', ['W']).
card_text('devoted hero', '').
card_mana_cost('devoted hero', ['W']).
card_cmc('devoted hero', 1).
card_layout('devoted hero', 'normal').
card_power('devoted hero', 1).
card_toughness('devoted hero', 2).

% Found in: CHK
card_name('devoted retainer', 'Devoted Retainer').
card_type('devoted retainer', 'Creature — Human Samurai').
card_types('devoted retainer', ['Creature']).
card_subtypes('devoted retainer', ['Human', 'Samurai']).
card_colors('devoted retainer', ['W']).
card_text('devoted retainer', 'Bushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_mana_cost('devoted retainer', ['W']).
card_cmc('devoted retainer', 1).
card_layout('devoted retainer', 'normal').
card_power('devoted retainer', 1).
card_toughness('devoted retainer', 1).

% Found in: GTC
card_name('devour flesh', 'Devour Flesh').
card_type('devour flesh', 'Instant').
card_types('devour flesh', ['Instant']).
card_subtypes('devour flesh', []).
card_colors('devour flesh', ['B']).
card_text('devour flesh', 'Target player sacrifices a creature, then gains life equal to that creature\'s toughness.').
card_mana_cost('devour flesh', ['1', 'B']).
card_cmc('devour flesh', 2).
card_layout('devour flesh', 'normal').

% Found in: 5DN
card_name('devour in shadow', 'Devour in Shadow').
card_type('devour in shadow', 'Instant').
card_types('devour in shadow', ['Instant']).
card_subtypes('devour in shadow', []).
card_colors('devour in shadow', ['B']).
card_text('devour in shadow', 'Destroy target creature. It can\'t be regenerated. You lose life equal to that creature\'s toughness.').
card_mana_cost('devour in shadow', ['B', 'B']).
card_cmc('devour in shadow', 2).
card_layout('devour in shadow', 'normal').

% Found in: LEG
card_name('devouring deep', 'Devouring Deep').
card_type('devouring deep', 'Creature — Fish').
card_types('devouring deep', ['Creature']).
card_subtypes('devouring deep', ['Fish']).
card_colors('devouring deep', ['U']).
card_text('devouring deep', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('devouring deep', ['2', 'U']).
card_cmc('devouring deep', 3).
card_layout('devouring deep', 'normal').
card_power('devouring deep', 1).
card_toughness('devouring deep', 2).

% Found in: CHK, MM2
card_name('devouring greed', 'Devouring Greed').
card_type('devouring greed', 'Sorcery — Arcane').
card_types('devouring greed', ['Sorcery']).
card_subtypes('devouring greed', ['Arcane']).
card_colors('devouring greed', ['B']).
card_text('devouring greed', 'As an additional cost to cast Devouring Greed, you may sacrifice any number of Spirits.\nTarget player loses 2 life plus 2 life for each Spirit sacrificed this way. You gain that much life.').
card_mana_cost('devouring greed', ['2', 'B', 'B']).
card_cmc('devouring greed', 4).
card_layout('devouring greed', 'normal').

% Found in: M15, RAV
card_name('devouring light', 'Devouring Light').
card_type('devouring light', 'Instant').
card_types('devouring light', ['Instant']).
card_subtypes('devouring light', []).
card_colors('devouring light', ['W']).
card_text('devouring light', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nExile target attacking or blocking creature.').
card_mana_cost('devouring light', ['1', 'W', 'W']).
card_cmc('devouring light', 3).
card_layout('devouring light', 'normal').

% Found in: CHK
card_name('devouring rage', 'Devouring Rage').
card_type('devouring rage', 'Instant — Arcane').
card_types('devouring rage', ['Instant']).
card_subtypes('devouring rage', ['Arcane']).
card_colors('devouring rage', ['R']).
card_text('devouring rage', 'As an additional cost to cast Devouring Rage, you may sacrifice any number of Spirits.\nTarget creature gets +3/+0 until end of turn. For each Spirit sacrificed this way, that creature gets an additional +3/+0 until end of turn.').
card_mana_cost('devouring rage', ['4', 'R']).
card_cmc('devouring rage', 5).
card_layout('devouring rage', 'normal').

% Found in: INV
card_name('devouring strossus', 'Devouring Strossus').
card_type('devouring strossus', 'Creature — Horror').
card_types('devouring strossus', ['Creature']).
card_subtypes('devouring strossus', ['Horror']).
card_colors('devouring strossus', ['B']).
card_text('devouring strossus', 'Flying, trample\nAt the beginning of your upkeep, sacrifice a creature.\nSacrifice a creature: Regenerate Devouring Strossus.').
card_mana_cost('devouring strossus', ['5', 'B', 'B', 'B']).
card_cmc('devouring strossus', 8).
card_layout('devouring strossus', 'normal').
card_power('devouring strossus', 9).
card_toughness('devouring strossus', 9).

% Found in: M12
card_name('devouring swarm', 'Devouring Swarm').
card_type('devouring swarm', 'Creature — Insect').
card_types('devouring swarm', ['Creature']).
card_subtypes('devouring swarm', ['Insect']).
card_colors('devouring swarm', ['B']).
card_text('devouring swarm', 'Flying\nSacrifice a creature: Devouring Swarm gets +1/+1 until end of turn.').
card_mana_cost('devouring swarm', ['1', 'B', 'B']).
card_cmc('devouring swarm', 3).
card_layout('devouring swarm', 'normal').
card_power('devouring swarm', 2).
card_toughness('devouring swarm', 1).

% Found in: AVR
card_name('devout chaplain', 'Devout Chaplain').
card_type('devout chaplain', 'Creature — Human Cleric').
card_types('devout chaplain', ['Creature']).
card_subtypes('devout chaplain', ['Human', 'Cleric']).
card_colors('devout chaplain', ['W']).
card_text('devout chaplain', '{T}, Tap two untapped Humans you control: Exile target artifact or enchantment.').
card_mana_cost('devout chaplain', ['2', 'W']).
card_cmc('devout chaplain', 3).
card_layout('devout chaplain', 'normal').
card_power('devout chaplain', 2).
card_toughness('devout chaplain', 2).

% Found in: ULG
card_name('devout harpist', 'Devout Harpist').
card_type('devout harpist', 'Creature — Human').
card_types('devout harpist', ['Creature']).
card_subtypes('devout harpist', ['Human']).
card_colors('devout harpist', ['W']).
card_text('devout harpist', '{T}: Destroy target Aura attached to a creature.').
card_mana_cost('devout harpist', ['W']).
card_cmc('devout harpist', 1).
card_layout('devout harpist', 'normal').
card_power('devout harpist', 1).
card_toughness('devout harpist', 1).

% Found in: M14
card_name('devout invocation', 'Devout Invocation').
card_type('devout invocation', 'Sorcery').
card_types('devout invocation', ['Sorcery']).
card_subtypes('devout invocation', []).
card_colors('devout invocation', ['W']).
card_text('devout invocation', 'Tap any number of untapped creatures you control. Put a 4/4 white Angel creature token with flying onto the battlefield for each creature tapped this way.').
card_mana_cost('devout invocation', ['6', 'W']).
card_cmc('devout invocation', 7).
card_layout('devout invocation', 'normal').

% Found in: ZEN
card_name('devout lightcaster', 'Devout Lightcaster').
card_type('devout lightcaster', 'Creature — Kor Cleric').
card_types('devout lightcaster', ['Creature']).
card_subtypes('devout lightcaster', ['Kor', 'Cleric']).
card_colors('devout lightcaster', ['W']).
card_text('devout lightcaster', 'Protection from black\nWhen Devout Lightcaster enters the battlefield, exile target black permanent.').
card_mana_cost('devout lightcaster', ['W', 'W', 'W']).
card_cmc('devout lightcaster', 3).
card_layout('devout lightcaster', 'normal').
card_power('devout lightcaster', 2).
card_toughness('devout lightcaster', 2).

% Found in: S99
card_name('devout monk', 'Devout Monk').
card_type('devout monk', 'Creature — Human Monk Cleric').
card_types('devout monk', ['Creature']).
card_subtypes('devout monk', ['Human', 'Monk', 'Cleric']).
card_colors('devout monk', ['W']).
card_text('devout monk', 'When Devout Monk enters the battlefield, you gain 1 life.').
card_mana_cost('devout monk', ['W']).
card_cmc('devout monk', 1).
card_layout('devout monk', 'normal').
card_power('devout monk', 1).
card_toughness('devout monk', 1).

% Found in: MMQ, VMA
card_name('devout witness', 'Devout Witness').
card_type('devout witness', 'Creature — Human Spellshaper').
card_types('devout witness', ['Creature']).
card_subtypes('devout witness', ['Human', 'Spellshaper']).
card_colors('devout witness', ['W']).
card_text('devout witness', '{1}{W}, {T}, Discard a card: Destroy target artifact or enchantment.').
card_mana_cost('devout witness', ['2', 'W']).
card_cmc('devout witness', 3).
card_layout('devout witness', 'normal').
card_power('devout witness', 2).
card_toughness('devout witness', 2).

% Found in: MOR
card_name('dewdrop spy', 'Dewdrop Spy').
card_type('dewdrop spy', 'Creature — Faerie Rogue').
card_types('dewdrop spy', ['Creature']).
card_subtypes('dewdrop spy', ['Faerie', 'Rogue']).
card_colors('dewdrop spy', ['U']).
card_text('dewdrop spy', 'Flash\nFlying\nWhen Dewdrop Spy enters the battlefield, look at the top card of target player\'s library.').
card_mana_cost('dewdrop spy', ['1', 'U', 'U']).
card_cmc('dewdrop spy', 3).
card_layout('dewdrop spy', 'normal').
card_power('dewdrop spy', 2).
card_toughness('dewdrop spy', 2).

% Found in: BTD, TMP, TPR, pARL
card_name('diabolic edict', 'Diabolic Edict').
card_type('diabolic edict', 'Instant').
card_types('diabolic edict', ['Instant']).
card_subtypes('diabolic edict', []).
card_colors('diabolic edict', ['B']).
card_text('diabolic edict', 'Target player sacrifices a creature.').
card_mana_cost('diabolic edict', ['1', 'B']).
card_cmc('diabolic edict', 2).
card_layout('diabolic edict', 'normal').

% Found in: PLS
card_name('diabolic intent', 'Diabolic Intent').
card_type('diabolic intent', 'Sorcery').
card_types('diabolic intent', ['Sorcery']).
card_subtypes('diabolic intent', []).
card_colors('diabolic intent', ['B']).
card_text('diabolic intent', 'As an additional cost to cast Diabolic Intent, sacrifice a creature.\nSearch your library for a card and put that card into your hand. Then shuffle your library.').
card_mana_cost('diabolic intent', ['1', 'B']).
card_cmc('diabolic intent', 2).
card_layout('diabolic intent', 'normal').

% Found in: 4ED, 5ED, DRK, ME4
card_name('diabolic machine', 'Diabolic Machine').
card_type('diabolic machine', 'Artifact Creature — Construct').
card_types('diabolic machine', ['Artifact', 'Creature']).
card_subtypes('diabolic machine', ['Construct']).
card_colors('diabolic machine', []).
card_text('diabolic machine', '{3}: Regenerate Diabolic Machine.').
card_mana_cost('diabolic machine', ['7']).
card_cmc('diabolic machine', 7).
card_layout('diabolic machine', 'normal').
card_power('diabolic machine', 4).
card_toughness('diabolic machine', 4).

% Found in: M13
card_name('diabolic revelation', 'Diabolic Revelation').
card_type('diabolic revelation', 'Sorcery').
card_types('diabolic revelation', ['Sorcery']).
card_subtypes('diabolic revelation', []).
card_colors('diabolic revelation', ['B']).
card_text('diabolic revelation', 'Search your library for up to X cards and put those cards into your hand. Then shuffle your library.').
card_mana_cost('diabolic revelation', ['X', '3', 'B', 'B']).
card_cmc('diabolic revelation', 5).
card_layout('diabolic revelation', 'normal').

% Found in: PD3, USG
card_name('diabolic servitude', 'Diabolic Servitude').
card_type('diabolic servitude', 'Enchantment').
card_types('diabolic servitude', ['Enchantment']).
card_subtypes('diabolic servitude', []).
card_colors('diabolic servitude', ['B']).
card_text('diabolic servitude', 'When Diabolic Servitude enters the battlefield, return target creature card from your graveyard to the battlefield.\nWhen the creature put onto the battlefield with Diabolic Servitude dies, exile it and return Diabolic Servitude to its owner\'s hand.\nWhen Diabolic Servitude leaves the battlefield, exile the creature put onto the battlefield with Diabolic Servitude.').
card_mana_cost('diabolic servitude', ['3', 'B']).
card_cmc('diabolic servitude', 4).
card_layout('diabolic servitude', 'normal').

% Found in: 10E, 8ED, 9ED, CMD, M10, M11, M12, M14, ODY
card_name('diabolic tutor', 'Diabolic Tutor').
card_type('diabolic tutor', 'Sorcery').
card_types('diabolic tutor', ['Sorcery']).
card_subtypes('diabolic tutor', []).
card_colors('diabolic tutor', ['B']).
card_text('diabolic tutor', 'Search your library for a card and put that card into your hand. Then shuffle your library.').
card_mana_cost('diabolic tutor', ['2', 'B', 'B']).
card_cmc('diabolic tutor', 4).
card_layout('diabolic tutor', 'normal').

% Found in: BTD, ICE, ME2
card_name('diabolic vision', 'Diabolic Vision').
card_type('diabolic vision', 'Sorcery').
card_types('diabolic vision', ['Sorcery']).
card_subtypes('diabolic vision', []).
card_colors('diabolic vision', ['U', 'B']).
card_text('diabolic vision', 'Look at the top five cards of your library. Put one of them into your hand and the rest on top of your library in any order.').
card_mana_cost('diabolic vision', ['U', 'B']).
card_cmc('diabolic vision', 2).
card_layout('diabolic vision', 'normal').

% Found in: CSP
card_name('diamond faerie', 'Diamond Faerie').
card_type('diamond faerie', 'Snow Creature — Faerie').
card_types('diamond faerie', ['Creature']).
card_subtypes('diamond faerie', ['Faerie']).
card_supertypes('diamond faerie', ['Snow']).
card_colors('diamond faerie', ['W', 'U', 'G']).
card_text('diamond faerie', 'Flying\n{1}{S}: Snow creatures you control get +1/+1 until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_mana_cost('diamond faerie', ['2', 'G', 'W', 'U']).
card_cmc('diamond faerie', 5).
card_layout('diamond faerie', 'normal').
card_power('diamond faerie', 3).
card_toughness('diamond faerie', 3).

% Found in: VAN
card_name('diamond faerie avatar', 'Diamond Faerie Avatar').
card_type('diamond faerie avatar', 'Vanguard').
card_types('diamond faerie avatar', ['Vanguard']).
card_subtypes('diamond faerie avatar', []).
card_colors('diamond faerie avatar', []).
card_text('diamond faerie avatar', '{S}: Target creature you control gets +1/+1 until end of turn.').
card_layout('diamond faerie avatar', 'vanguard').

% Found in: VIS
card_name('diamond kaleidoscope', 'Diamond Kaleidoscope').
card_type('diamond kaleidoscope', 'Artifact').
card_types('diamond kaleidoscope', ['Artifact']).
card_subtypes('diamond kaleidoscope', []).
card_colors('diamond kaleidoscope', []).
card_text('diamond kaleidoscope', '{3}, {T}: Put a 0/1 colorless Prism artifact creature token onto the battlefield.\nSacrifice a Prism token: Add one mana of any color to your mana pool.').
card_mana_cost('diamond kaleidoscope', ['4']).
card_cmc('diamond kaleidoscope', 4).
card_layout('diamond kaleidoscope', 'normal').
card_reserved('diamond kaleidoscope').

% Found in: ARN, MED
card_name('diamond valley', 'Diamond Valley').
card_type('diamond valley', 'Land').
card_types('diamond valley', ['Land']).
card_subtypes('diamond valley', []).
card_colors('diamond valley', []).
card_text('diamond valley', '{T}, Sacrifice a creature: You gain life equal to the sacrificed creature\'s toughness.').
card_layout('diamond valley', 'normal').
card_reserved('diamond valley').

% Found in: CM1, PTK
card_name('diaochan, artful beauty', 'Diaochan, Artful Beauty').
card_type('diaochan, artful beauty', 'Legendary Creature — Human Advisor').
card_types('diaochan, artful beauty', ['Creature']).
card_subtypes('diaochan, artful beauty', ['Human', 'Advisor']).
card_supertypes('diaochan, artful beauty', ['Legendary']).
card_colors('diaochan, artful beauty', ['R']).
card_text('diaochan, artful beauty', '{T}: Destroy target creature of your choice, then destroy target creature of an opponent\'s choice. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('diaochan, artful beauty', ['3', 'R']).
card_cmc('diaochan, artful beauty', 4).
card_layout('diaochan, artful beauty', 'normal').
card_power('diaochan, artful beauty', 1).
card_toughness('diaochan, artful beauty', 1).

% Found in: PLC
card_name('dichotomancy', 'Dichotomancy').
card_type('dichotomancy', 'Sorcery').
card_types('dichotomancy', ['Sorcery']).
card_subtypes('dichotomancy', []).
card_colors('dichotomancy', ['U']).
card_text('dichotomancy', 'For each tapped nonland permanent target opponent controls, search that player\'s library for a card with the same name as that permanent and put it onto the battlefield under your control. Then that player shuffles his or her library.\nSuspend 3—{1}{U}{U} (Rather than cast this card from your hand, you may pay {1}{U}{U} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_mana_cost('dichotomancy', ['7', 'U', 'U']).
card_cmc('dichotomancy', 9).
card_layout('dichotomancy', 'normal').

% Found in: JOU
card_name('dictate of erebos', 'Dictate of Erebos').
card_type('dictate of erebos', 'Enchantment').
card_types('dictate of erebos', ['Enchantment']).
card_subtypes('dictate of erebos', []).
card_colors('dictate of erebos', ['B']).
card_text('dictate of erebos', 'Flash\nWhenever a creature you control dies, each opponent sacrifices a creature.').
card_mana_cost('dictate of erebos', ['3', 'B', 'B']).
card_cmc('dictate of erebos', 5).
card_layout('dictate of erebos', 'normal').

% Found in: DDO, JOU
card_name('dictate of heliod', 'Dictate of Heliod').
card_type('dictate of heliod', 'Enchantment').
card_types('dictate of heliod', ['Enchantment']).
card_subtypes('dictate of heliod', []).
card_colors('dictate of heliod', ['W']).
card_text('dictate of heliod', 'Flash\nCreatures you control get +2/+2.').
card_mana_cost('dictate of heliod', ['3', 'W', 'W']).
card_cmc('dictate of heliod', 5).
card_layout('dictate of heliod', 'normal').

% Found in: JOU
card_name('dictate of karametra', 'Dictate of Karametra').
card_type('dictate of karametra', 'Enchantment').
card_types('dictate of karametra', ['Enchantment']).
card_subtypes('dictate of karametra', []).
card_colors('dictate of karametra', ['G']).
card_text('dictate of karametra', 'Flash\nWhenever a player taps a land for mana, that player adds one mana to his or her mana pool of any type that land produced.').
card_mana_cost('dictate of karametra', ['3', 'G', 'G']).
card_cmc('dictate of karametra', 5).
card_layout('dictate of karametra', 'normal').

% Found in: JOU, pMGD
card_name('dictate of kruphix', 'Dictate of Kruphix').
card_type('dictate of kruphix', 'Enchantment').
card_types('dictate of kruphix', ['Enchantment']).
card_subtypes('dictate of kruphix', []).
card_colors('dictate of kruphix', ['U']).
card_text('dictate of kruphix', 'Flash\nAt the beginning of each player\'s draw step, that player draws an additional card.').
card_mana_cost('dictate of kruphix', ['1', 'U', 'U']).
card_cmc('dictate of kruphix', 3).
card_layout('dictate of kruphix', 'normal').

% Found in: JOU, pLPA
card_name('dictate of the twin gods', 'Dictate of the Twin Gods').
card_type('dictate of the twin gods', 'Enchantment').
card_types('dictate of the twin gods', ['Enchantment']).
card_subtypes('dictate of the twin gods', []).
card_colors('dictate of the twin gods', ['R']).
card_text('dictate of the twin gods', 'Flash\nIf a source would deal damage to a permanent or player, it deals double that damage to that permanent or player instead.').
card_mana_cost('dictate of the twin gods', ['3', 'R', 'R']).
card_cmc('dictate of the twin gods', 5).
card_layout('dictate of the twin gods', 'normal').

% Found in: HML, ME3
card_name('didgeridoo', 'Didgeridoo').
card_type('didgeridoo', 'Artifact').
card_types('didgeridoo', ['Artifact']).
card_subtypes('didgeridoo', []).
card_colors('didgeridoo', []).
card_text('didgeridoo', '{3}: You may put a Minotaur permanent card from your hand onto the battlefield.').
card_mana_cost('didgeridoo', ['1']).
card_cmc('didgeridoo', 1).
card_layout('didgeridoo', 'normal').
card_reserved('didgeridoo').

% Found in: M15
card_name('diffusion sliver', 'Diffusion Sliver').
card_type('diffusion sliver', 'Creature — Sliver').
card_types('diffusion sliver', ['Creature']).
card_subtypes('diffusion sliver', ['Sliver']).
card_colors('diffusion sliver', ['U']).
card_text('diffusion sliver', 'Whenever a Sliver creature you control becomes the target of a spell or ability an opponent controls, counter that spell or ability unless its controller pays {2}.').
card_mana_cost('diffusion sliver', ['1', 'U']).
card_cmc('diffusion sliver', 2).
card_layout('diffusion sliver', 'normal').
card_power('diffusion sliver', 1).
card_toughness('diffusion sliver', 1).

% Found in: KTK, pPRE
card_name('dig through time', 'Dig Through Time').
card_type('dig through time', 'Instant').
card_types('dig through time', ['Instant']).
card_subtypes('dig through time', []).
card_colors('dig through time', ['U']).
card_text('dig through time', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nLook at the top seven cards of your library. Put two of them into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('dig through time', ['6', 'U', 'U']).
card_cmc('dig through time', 8).
card_layout('dig through time', 'normal').

% Found in: ODY
card_name('diligent farmhand', 'Diligent Farmhand').
card_type('diligent farmhand', 'Creature — Human Druid').
card_types('diligent farmhand', ['Creature']).
card_subtypes('diligent farmhand', ['Human', 'Druid']).
card_colors('diligent farmhand', ['G']).
card_text('diligent farmhand', '{1}{G}, Sacrifice Diligent Farmhand: Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.\nIf Diligent Farmhand is in a graveyard, effects from spells named Muscle Burst count it as a card named Muscle Burst.').
card_mana_cost('diligent farmhand', ['G']).
card_cmc('diligent farmhand', 1).
card_layout('diligent farmhand', 'normal').
card_power('diligent farmhand', 1).
card_toughness('diligent farmhand', 1).

% Found in: GTC
card_name('diluvian primordial', 'Diluvian Primordial').
card_type('diluvian primordial', 'Creature — Avatar').
card_types('diluvian primordial', ['Creature']).
card_subtypes('diluvian primordial', ['Avatar']).
card_colors('diluvian primordial', ['U']).
card_text('diluvian primordial', 'Flying\nWhen Diluvian Primordial enters the battlefield, for each opponent, you may cast up to one target instant or sorcery card from that player\'s graveyard without paying its mana cost. If a card cast this way would be put into a graveyard this turn, exile it instead.').
card_mana_cost('diluvian primordial', ['5', 'U', 'U']).
card_cmc('diluvian primordial', 7).
card_layout('diluvian primordial', 'normal').
card_power('diluvian primordial', 5).
card_toughness('diluvian primordial', 5).

% Found in: SCG
card_name('dimensional breach', 'Dimensional Breach').
card_type('dimensional breach', 'Sorcery').
card_types('dimensional breach', ['Sorcery']).
card_subtypes('dimensional breach', []).
card_colors('dimensional breach', ['W']).
card_text('dimensional breach', 'Exile all permanents. For as long as any of those cards remain exiled, at the beginning of each player\'s upkeep, that player returns one of the exiled cards he or she owns to the battlefield.').
card_mana_cost('dimensional breach', ['5', 'W', 'W']).
card_cmc('dimensional breach', 7).
card_layout('dimensional breach', 'normal').

% Found in: M11
card_name('diminish', 'Diminish').
card_type('diminish', 'Instant').
card_types('diminish', ['Instant']).
card_subtypes('diminish', []).
card_colors('diminish', ['U']).
card_text('diminish', 'Target creature has base power and toughness 1/1 until end of turn.').
card_mana_cost('diminish', ['U']).
card_cmc('diminish', 1).
card_layout('diminish', 'normal').

% Found in: 6ED, ALL, MED
card_name('diminishing returns', 'Diminishing Returns').
card_type('diminishing returns', 'Sorcery').
card_types('diminishing returns', ['Sorcery']).
card_subtypes('diminishing returns', []).
card_colors('diminishing returns', ['U']).
card_text('diminishing returns', 'Each player shuffles his or her hand and graveyard into his or her library. You exile the top ten cards of your library. Then each player draws up to seven cards.').
card_mana_cost('diminishing returns', ['2', 'U', 'U']).
card_cmc('diminishing returns', 4).
card_layout('diminishing returns', 'normal').

% Found in: CMD, MM2, PC2, RAV
card_name('dimir aqueduct', 'Dimir Aqueduct').
card_type('dimir aqueduct', 'Land').
card_types('dimir aqueduct', ['Land']).
card_subtypes('dimir aqueduct', []).
card_colors('dimir aqueduct', []).
card_text('dimir aqueduct', 'Dimir Aqueduct enters the battlefield tapped.\nWhen Dimir Aqueduct enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {U}{B} to your mana pool.').
card_layout('dimir aqueduct', 'normal').

% Found in: GTC, pFNM
card_name('dimir charm', 'Dimir Charm').
card_type('dimir charm', 'Instant').
card_types('dimir charm', ['Instant']).
card_subtypes('dimir charm', []).
card_colors('dimir charm', ['U', 'B']).
card_text('dimir charm', 'Choose one —\n• Counter target sorcery spell.\n• Destroy target creature with power 2 or less.\n• Look at the top three cards of target player\'s library, then put one back and the rest into that player\'s graveyard.').
card_mana_cost('dimir charm', ['U', 'B']).
card_cmc('dimir charm', 2).
card_layout('dimir charm', 'normal').

% Found in: DGM
card_name('dimir cluestone', 'Dimir Cluestone').
card_type('dimir cluestone', 'Artifact').
card_types('dimir cluestone', ['Artifact']).
card_subtypes('dimir cluestone', []).
card_colors('dimir cluestone', []).
card_text('dimir cluestone', '{T}: Add {U} or {B} to your mana pool.\n{U}{B}, {T}, Sacrifice Dimir Cluestone: Draw a card.').
card_mana_cost('dimir cluestone', ['3']).
card_cmc('dimir cluestone', 3).
card_layout('dimir cluestone', 'normal').

% Found in: DDH, RAV
card_name('dimir cutpurse', 'Dimir Cutpurse').
card_type('dimir cutpurse', 'Creature — Spirit').
card_types('dimir cutpurse', ['Creature']).
card_subtypes('dimir cutpurse', ['Spirit']).
card_colors('dimir cutpurse', ['U', 'B']).
card_text('dimir cutpurse', 'Whenever Dimir Cutpurse deals combat damage to a player, that player discards a card and you draw a card.').
card_mana_cost('dimir cutpurse', ['1', 'U', 'B']).
card_cmc('dimir cutpurse', 3).
card_layout('dimir cutpurse', 'normal').
card_power('dimir cutpurse', 2).
card_toughness('dimir cutpurse', 2).

% Found in: CNS, RAV
card_name('dimir doppelganger', 'Dimir Doppelganger').
card_type('dimir doppelganger', 'Creature — Shapeshifter').
card_types('dimir doppelganger', ['Creature']).
card_subtypes('dimir doppelganger', ['Shapeshifter']).
card_colors('dimir doppelganger', ['U', 'B']).
card_text('dimir doppelganger', '{1}{U}{B}: Exile target creature card from a graveyard. Dimir Doppelganger becomes a copy of that card and gains this ability.').
card_mana_cost('dimir doppelganger', ['1', 'U', 'B']).
card_cmc('dimir doppelganger', 3).
card_layout('dimir doppelganger', 'normal').
card_power('dimir doppelganger', 0).
card_toughness('dimir doppelganger', 2).

% Found in: C13, DGM, GTC
card_name('dimir guildgate', 'Dimir Guildgate').
card_type('dimir guildgate', 'Land — Gate').
card_types('dimir guildgate', ['Land']).
card_subtypes('dimir guildgate', ['Gate']).
card_colors('dimir guildgate', []).
card_text('dimir guildgate', 'Dimir Guildgate enters the battlefield tapped.\n{T}: Add {U} or {B} to your mana pool.').
card_layout('dimir guildgate', 'normal').

% Found in: MM2, RAV, pREL
card_name('dimir guildmage', 'Dimir Guildmage').
card_type('dimir guildmage', 'Creature — Human Wizard').
card_types('dimir guildmage', ['Creature']).
card_subtypes('dimir guildmage', ['Human', 'Wizard']).
card_colors('dimir guildmage', ['U', 'B']).
card_text('dimir guildmage', '({U/B} can be paid with either {U} or {B}.)\n{3}{U}: Target player draws a card. Activate this ability only any time you could cast a sorcery.\n{3}{B}: Target player discards a card. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('dimir guildmage', ['U/B', 'U/B']).
card_cmc('dimir guildmage', 2).
card_layout('dimir guildmage', 'normal').
card_power('dimir guildmage', 2).
card_toughness('dimir guildmage', 2).

% Found in: RAV
card_name('dimir house guard', 'Dimir House Guard').
card_type('dimir house guard', 'Creature — Skeleton').
card_types('dimir house guard', ['Creature']).
card_subtypes('dimir house guard', ['Skeleton']).
card_colors('dimir house guard', ['B']).
card_text('dimir house guard', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nSacrifice a creature: Regenerate Dimir House Guard.\nTransmute {1}{B}{B} ({1}{B}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Transmute only as a sorcery.)').
card_mana_cost('dimir house guard', ['3', 'B']).
card_cmc('dimir house guard', 4).
card_layout('dimir house guard', 'normal').
card_power('dimir house guard', 2).
card_toughness('dimir house guard', 3).

% Found in: PC2, RAV
card_name('dimir infiltrator', 'Dimir Infiltrator').
card_type('dimir infiltrator', 'Creature — Spirit').
card_types('dimir infiltrator', ['Creature']).
card_subtypes('dimir infiltrator', ['Spirit']).
card_colors('dimir infiltrator', ['U', 'B']).
card_text('dimir infiltrator', 'Dimir Infiltrator can\'t be blocked.\nTransmute {1}{U}{B} ({1}{U}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Transmute only as a sorcery.)').
card_mana_cost('dimir infiltrator', ['U', 'B']).
card_cmc('dimir infiltrator', 2).
card_layout('dimir infiltrator', 'normal').
card_power('dimir infiltrator', 1).
card_toughness('dimir infiltrator', 3).

% Found in: GTC
card_name('dimir keyrune', 'Dimir Keyrune').
card_type('dimir keyrune', 'Artifact').
card_types('dimir keyrune', ['Artifact']).
card_subtypes('dimir keyrune', []).
card_colors('dimir keyrune', []).
card_text('dimir keyrune', '{T}: Add {U} or {B} to your mana pool.\n{U}{B}: Dimir Keyrune becomes a 2/2 blue and black Horror artifact creature until end of turn and can\'t be blocked this turn.').
card_mana_cost('dimir keyrune', ['3']).
card_cmc('dimir keyrune', 3).
card_layout('dimir keyrune', 'normal').

% Found in: RAV
card_name('dimir machinations', 'Dimir Machinations').
card_type('dimir machinations', 'Sorcery').
card_types('dimir machinations', ['Sorcery']).
card_subtypes('dimir machinations', []).
card_colors('dimir machinations', ['B']).
card_text('dimir machinations', 'Look at the top three cards of target player\'s library. Exile any number of those cards, then put the rest back in any order.\nTransmute {1}{B}{B} ({1}{B}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Transmute only as a sorcery.)').
card_mana_cost('dimir machinations', ['2', 'B']).
card_cmc('dimir machinations', 3).
card_layout('dimir machinations', 'normal').

% Found in: ARC, CMD, RAV
card_name('dimir signet', 'Dimir Signet').
card_type('dimir signet', 'Artifact').
card_types('dimir signet', ['Artifact']).
card_subtypes('dimir signet', []).
card_colors('dimir signet', []).
card_text('dimir signet', '{1}, {T}: Add {U}{B} to your mana pool.').
card_mana_cost('dimir signet', ['2']).
card_cmc('dimir signet', 2).
card_layout('dimir signet', 'normal').

% Found in: SHM
card_name('din of the fireherd', 'Din of the Fireherd').
card_type('din of the fireherd', 'Sorcery').
card_types('din of the fireherd', ['Sorcery']).
card_subtypes('din of the fireherd', []).
card_colors('din of the fireherd', ['B', 'R']).
card_text('din of the fireherd', 'Put a 5/5 black and red Elemental creature token onto the battlefield. Target opponent sacrifices a creature for each black creature you control, then sacrifices a land for each red creature you control.').
card_mana_cost('din of the fireherd', ['5', 'B/R', 'B/R', 'B/R']).
card_cmc('din of the fireherd', 8).
card_layout('din of the fireherd', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, CED, CEI, LEA, LEB
card_name('dingus egg', 'Dingus Egg').
card_type('dingus egg', 'Artifact').
card_types('dingus egg', ['Artifact']).
card_subtypes('dingus egg', []).
card_colors('dingus egg', []).
card_text('dingus egg', 'Whenever a land is put into a graveyard from the battlefield, Dingus Egg deals 2 damage to that land\'s controller.').
card_mana_cost('dingus egg', ['4']).
card_cmc('dingus egg', 4).
card_layout('dingus egg', 'normal').

% Found in: WTH
card_name('dingus staff', 'Dingus Staff').
card_type('dingus staff', 'Artifact').
card_types('dingus staff', ['Artifact']).
card_subtypes('dingus staff', []).
card_colors('dingus staff', []).
card_text('dingus staff', 'Whenever a creature dies, Dingus Staff deals 2 damage to that creature\'s controller.').
card_mana_cost('dingus staff', ['4']).
card_cmc('dingus staff', 4).
card_layout('dingus staff', 'normal').

% Found in: GTC
card_name('dinrova horror', 'Dinrova Horror').
card_type('dinrova horror', 'Creature — Horror').
card_types('dinrova horror', ['Creature']).
card_subtypes('dinrova horror', ['Horror']).
card_colors('dinrova horror', ['U', 'B']).
card_text('dinrova horror', 'When Dinrova Horror enters the battlefield, return target permanent to its owner\'s hand, then that player discards a card.').
card_mana_cost('dinrova horror', ['4', 'U', 'B']).
card_cmc('dinrova horror', 6).
card_layout('dinrova horror', 'normal').
card_power('dinrova horror', 4).
card_toughness('dinrova horror', 4).

% Found in: FRF
card_name('diplomacy of the wastes', 'Diplomacy of the Wastes').
card_type('diplomacy of the wastes', 'Sorcery').
card_types('diplomacy of the wastes', ['Sorcery']).
card_subtypes('diplomacy of the wastes', []).
card_colors('diplomacy of the wastes', ['B']).
card_text('diplomacy of the wastes', 'Target opponent reveals his or her hand. You choose a nonland card from it. That player discards that card. If you control a Warrior, that player loses 2 life.').
card_mana_cost('diplomacy of the wastes', ['2', 'B']).
card_cmc('diplomacy of the wastes', 3).
card_layout('diplomacy of the wastes', 'normal').

% Found in: MMQ
card_name('diplomatic escort', 'Diplomatic Escort').
card_type('diplomatic escort', 'Creature — Human Spellshaper').
card_types('diplomatic escort', ['Creature']).
card_subtypes('diplomatic escort', ['Human', 'Spellshaper']).
card_colors('diplomatic escort', ['U']).
card_text('diplomatic escort', '{U}, {T}, Discard a card: Counter target spell or ability that targets a creature.').
card_mana_cost('diplomatic escort', ['1', 'U']).
card_cmc('diplomatic escort', 2).
card_layout('diplomatic escort', 'normal').
card_power('diplomatic escort', 1).
card_toughness('diplomatic escort', 1).

% Found in: MMQ
card_name('diplomatic immunity', 'Diplomatic Immunity').
card_type('diplomatic immunity', 'Enchantment — Aura').
card_types('diplomatic immunity', ['Enchantment']).
card_subtypes('diplomatic immunity', ['Aura']).
card_colors('diplomatic immunity', ['U']).
card_text('diplomatic immunity', 'Enchant creature\nShroud (A permanent with shroud can\'t be the target of spells or abilities.)\nEnchanted creature has shroud.').
card_mana_cost('diplomatic immunity', ['1', 'U']).
card_cmc('diplomatic immunity', 2).
card_layout('diplomatic immunity', 'normal').

% Found in: SHM
card_name('dire undercurrents', 'Dire Undercurrents').
card_type('dire undercurrents', 'Enchantment').
card_types('dire undercurrents', ['Enchantment']).
card_subtypes('dire undercurrents', []).
card_colors('dire undercurrents', ['U', 'B']).
card_text('dire undercurrents', 'Whenever a blue creature enters the battlefield under your control, you may have target player draw a card.\nWhenever a black creature enters the battlefield under your control, you may have target player discard a card.').
card_mana_cost('dire undercurrents', ['3', 'U/B', 'U/B']).
card_cmc('dire undercurrents', 5).
card_layout('dire undercurrents', 'normal').

% Found in: ICE
card_name('dire wolves', 'Dire Wolves').
card_type('dire wolves', 'Creature — Wolf').
card_types('dire wolves', ['Creature']).
card_subtypes('dire wolves', ['Wolf']).
card_colors('dire wolves', ['G']).
card_text('dire wolves', 'Dire Wolves has banding as long as you control a Plains. (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('dire wolves', ['2', 'G']).
card_cmc('dire wolves', 3).
card_layout('dire wolves', 'normal').
card_power('dire wolves', 2).
card_toughness('dire wolves', 2).

% Found in: DKA
card_name('diregraf captain', 'Diregraf Captain').
card_type('diregraf captain', 'Creature — Zombie Soldier').
card_types('diregraf captain', ['Creature']).
card_subtypes('diregraf captain', ['Zombie', 'Soldier']).
card_colors('diregraf captain', ['U', 'B']).
card_text('diregraf captain', 'Deathtouch\nOther Zombie creatures you control get +1/+1.\nWhenever another Zombie you control dies, target opponent loses 1 life.').
card_mana_cost('diregraf captain', ['1', 'U', 'B']).
card_cmc('diregraf captain', 3).
card_layout('diregraf captain', 'normal').
card_power('diregraf captain', 2).
card_toughness('diregraf captain', 2).

% Found in: AVR
card_name('diregraf escort', 'Diregraf Escort').
card_type('diregraf escort', 'Creature — Human Cleric').
card_types('diregraf escort', ['Creature']).
card_subtypes('diregraf escort', ['Human', 'Cleric']).
card_colors('diregraf escort', ['G']).
card_text('diregraf escort', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Diregraf Escort is paired with another creature, both creatures have protection from Zombies.').
card_mana_cost('diregraf escort', ['G']).
card_cmc('diregraf escort', 1).
card_layout('diregraf escort', 'normal').
card_power('diregraf escort', 1).
card_toughness('diregraf escort', 1).

% Found in: ISD, pMGD
card_name('diregraf ghoul', 'Diregraf Ghoul').
card_type('diregraf ghoul', 'Creature — Zombie').
card_types('diregraf ghoul', ['Creature']).
card_subtypes('diregraf ghoul', ['Zombie']).
card_colors('diregraf ghoul', ['B']).
card_text('diregraf ghoul', 'Diregraf Ghoul enters the battlefield tapped.').
card_mana_cost('diregraf ghoul', ['B']).
card_cmc('diregraf ghoul', 1).
card_layout('diregraf ghoul', 'normal').
card_power('diregraf ghoul', 2).
card_toughness('diregraf ghoul', 2).

% Found in: C13, ONS
card_name('dirge of dread', 'Dirge of Dread').
card_type('dirge of dread', 'Sorcery').
card_types('dirge of dread', ['Sorcery']).
card_subtypes('dirge of dread', []).
card_colors('dirge of dread', ['B']).
card_text('dirge of dread', 'All creatures gain fear until end of turn. (They can\'t be blocked except by artifact creatures and/or black creatures.)\nCycling {1}{B} ({1}{B}, Discard this card: Draw a card.)\nWhen you cycle Dirge of Dread, you may have target creature gain fear until end of turn.').
card_mana_cost('dirge of dread', ['2', 'B']).
card_cmc('dirge of dread', 3).
card_layout('dirge of dread', 'normal').

% Found in: DTK
card_name('dirgur nemesis', 'Dirgur Nemesis').
card_type('dirgur nemesis', 'Creature — Serpent').
card_types('dirgur nemesis', ['Creature']).
card_subtypes('dirgur nemesis', ['Serpent']).
card_colors('dirgur nemesis', ['U']).
card_text('dirgur nemesis', 'Defender\nMegamorph {6}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_mana_cost('dirgur nemesis', ['5', 'U']).
card_cmc('dirgur nemesis', 6).
card_layout('dirgur nemesis', 'normal').
card_power('dirgur nemesis', 6).
card_toughness('dirgur nemesis', 5).

% Found in: BRB, TMP, pPRE
card_name('dirtcowl wurm', 'Dirtcowl Wurm').
card_type('dirtcowl wurm', 'Creature — Wurm').
card_types('dirtcowl wurm', ['Creature']).
card_subtypes('dirtcowl wurm', ['Wurm']).
card_colors('dirtcowl wurm', ['G']).
card_text('dirtcowl wurm', 'Whenever an opponent plays a land, put a +1/+1 counter on Dirtcowl Wurm.').
card_mana_cost('dirtcowl wurm', ['4', 'G']).
card_cmc('dirtcowl wurm', 5).
card_layout('dirtcowl wurm', 'normal').
card_power('dirtcowl wurm', 3).
card_toughness('dirtcowl wurm', 4).

% Found in: MIR
card_name('dirtwater wraith', 'Dirtwater Wraith').
card_type('dirtwater wraith', 'Creature — Wraith').
card_types('dirtwater wraith', ['Creature']).
card_subtypes('dirtwater wraith', ['Wraith']).
card_colors('dirtwater wraith', ['B']).
card_text('dirtwater wraith', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)\n{B}: Dirtwater Wraith gets +1/+0 until end of turn.').
card_mana_cost('dirtwater wraith', ['3', 'B']).
card_cmc('dirtwater wraith', 4).
card_layout('dirtwater wraith', 'normal').
card_power('dirtwater wraith', 1).
card_toughness('dirtwater wraith', 3).

% Found in: DGM
card_name('dirty', 'Dirty').
card_type('dirty', 'Sorcery').
card_types('dirty', ['Sorcery']).
card_subtypes('dirty', []).
card_colors('dirty', ['G']).
card_text('dirty', 'Return target card from your graveyard to your hand.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('dirty', ['2', 'G']).
card_cmc('dirty', 3).
card_layout('dirty', 'split').

% Found in: ODY
card_name('dirty wererat', 'Dirty Wererat').
card_type('dirty wererat', 'Creature — Human Rat Minion').
card_types('dirty wererat', ['Creature']).
card_subtypes('dirty wererat', ['Human', 'Rat', 'Minion']).
card_colors('dirty wererat', ['B']).
card_text('dirty wererat', '{B}, Discard a card: Regenerate Dirty Wererat.\nThreshold — As long as seven or more cards are in your graveyard, Dirty Wererat gets +2/+2 and can\'t block.').
card_mana_cost('dirty wererat', ['3', 'B']).
card_cmc('dirty wererat', 4).
card_layout('dirty wererat', 'normal').
card_power('dirty wererat', 2).
card_toughness('dirty wererat', 3).

% Found in: UDS
card_name('disappear', 'Disappear').
card_type('disappear', 'Enchantment — Aura').
card_types('disappear', ['Enchantment']).
card_subtypes('disappear', ['Aura']).
card_colors('disappear', ['U']).
card_text('disappear', 'Enchant creature\n{U}: Return enchanted creature and Disappear to their owners\' hands.').
card_mana_cost('disappear', ['2', 'U', 'U']).
card_cmc('disappear', 4).
card_layout('disappear', 'normal').

% Found in: MRD
card_name('disarm', 'Disarm').
card_type('disarm', 'Instant').
card_types('disarm', ['Instant']).
card_subtypes('disarm', []).
card_colors('disarm', ['U']).
card_text('disarm', 'Unattach all Equipment from target creature.').
card_mana_cost('disarm', ['U']).
card_cmc('disarm', 1).
card_layout('disarm', 'normal').

% Found in: CMD, ROE
card_name('disaster radius', 'Disaster Radius').
card_type('disaster radius', 'Sorcery').
card_types('disaster radius', ['Sorcery']).
card_subtypes('disaster radius', []).
card_colors('disaster radius', ['R']).
card_text('disaster radius', 'As an additional cost to cast Disaster Radius, reveal a creature card from your hand.\nDisaster Radius deals X damage to each creature your opponents control, where X is the revealed card\'s converted mana cost.').
card_mana_cost('disaster radius', ['5', 'R', 'R']).
card_cmc('disaster radius', 7).
card_layout('disaster radius', 'normal').

% Found in: C14, M13
card_name('disciple of bolas', 'Disciple of Bolas').
card_type('disciple of bolas', 'Creature — Human Wizard').
card_types('disciple of bolas', ['Creature']).
card_subtypes('disciple of bolas', ['Human', 'Wizard']).
card_colors('disciple of bolas', ['B']).
card_text('disciple of bolas', 'When Disciple of Bolas enters the battlefield, sacrifice another creature. You gain X life and draw X cards, where X is that creature\'s power.').
card_mana_cost('disciple of bolas', ['3', 'B']).
card_cmc('disciple of bolas', 4).
card_layout('disciple of bolas', 'normal').
card_power('disciple of bolas', 2).
card_toughness('disciple of bolas', 1).

% Found in: JOU
card_name('disciple of deceit', 'Disciple of Deceit').
card_type('disciple of deceit', 'Creature — Human Rogue').
card_types('disciple of deceit', ['Creature']).
card_subtypes('disciple of deceit', ['Human', 'Rogue']).
card_colors('disciple of deceit', ['U', 'B']).
card_text('disciple of deceit', 'Inspired — Whenever Disciple of Deceit becomes untapped, you may discard a nonland card. If you do, search your library for a card with the same converted mana cost as that card, reveal it, put it into your hand, then shuffle your library.').
card_mana_cost('disciple of deceit', ['U', 'B']).
card_cmc('disciple of deceit', 2).
card_layout('disciple of deceit', 'normal').
card_power('disciple of deceit', 1).
card_toughness('disciple of deceit', 3).

% Found in: ONS, USG
card_name('disciple of grace', 'Disciple of Grace').
card_type('disciple of grace', 'Creature — Human Cleric').
card_types('disciple of grace', ['Creature']).
card_subtypes('disciple of grace', ['Human', 'Cleric']).
card_colors('disciple of grace', ['W']).
card_text('disciple of grace', 'Protection from black\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('disciple of grace', ['1', 'W']).
card_cmc('disciple of grace', 2).
card_layout('disciple of grace', 'normal').
card_power('disciple of grace', 1).
card_toughness('disciple of grace', 2).

% Found in: C13, ISD
card_name('disciple of griselbrand', 'Disciple of Griselbrand').
card_type('disciple of griselbrand', 'Creature — Human Cleric').
card_types('disciple of griselbrand', ['Creature']).
card_subtypes('disciple of griselbrand', ['Human', 'Cleric']).
card_colors('disciple of griselbrand', ['B']).
card_text('disciple of griselbrand', '{1}, Sacrifice a creature: You gain life equal to the sacrificed creature\'s toughness.').
card_mana_cost('disciple of griselbrand', ['1', 'B']).
card_cmc('disciple of griselbrand', 2).
card_layout('disciple of griselbrand', 'normal').
card_power('disciple of griselbrand', 1).
card_toughness('disciple of griselbrand', 1).

% Found in: PLS
card_name('disciple of kangee', 'Disciple of Kangee').
card_type('disciple of kangee', 'Creature — Human Wizard').
card_types('disciple of kangee', ['Creature']).
card_subtypes('disciple of kangee', ['Human', 'Wizard']).
card_colors('disciple of kangee', ['W']).
card_text('disciple of kangee', '{U}, {T}: Target creature gains flying and becomes blue until end of turn.').
card_mana_cost('disciple of kangee', ['2', 'W']).
card_cmc('disciple of kangee', 3).
card_layout('disciple of kangee', 'normal').
card_power('disciple of kangee', 2).
card_toughness('disciple of kangee', 2).

% Found in: USG
card_name('disciple of law', 'Disciple of Law').
card_type('disciple of law', 'Creature — Human Cleric').
card_types('disciple of law', ['Creature']).
card_subtypes('disciple of law', ['Human', 'Cleric']).
card_colors('disciple of law', ['W']).
card_text('disciple of law', 'Protection from red\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('disciple of law', ['1', 'W']).
card_cmc('disciple of law', 2).
card_layout('disciple of law', 'normal').
card_power('disciple of law', 1).
card_toughness('disciple of law', 2).

% Found in: ONS
card_name('disciple of malice', 'Disciple of Malice').
card_type('disciple of malice', 'Creature — Human Cleric').
card_types('disciple of malice', ['Creature']).
card_subtypes('disciple of malice', ['Human', 'Cleric']).
card_colors('disciple of malice', ['B']).
card_text('disciple of malice', 'Protection from white\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('disciple of malice', ['1', 'B']).
card_cmc('disciple of malice', 2).
card_layout('disciple of malice', 'normal').
card_power('disciple of malice', 1).
card_toughness('disciple of malice', 2).

% Found in: THS
card_name('disciple of phenax', 'Disciple of Phenax').
card_type('disciple of phenax', 'Creature — Human Cleric').
card_types('disciple of phenax', ['Creature']).
card_subtypes('disciple of phenax', ['Human', 'Cleric']).
card_colors('disciple of phenax', ['B']).
card_text('disciple of phenax', 'When Disciple of Phenax enters the battlefield, target player reveals a number of cards from his or her hand equal to your devotion to black. You choose one of them. That player discards that card. (Each {B} in the mana costs of permanents you control counts toward your devotion to black.)').
card_mana_cost('disciple of phenax', ['2', 'B', 'B']).
card_cmc('disciple of phenax', 4).
card_layout('disciple of phenax', 'normal').
card_power('disciple of phenax', 1).
card_toughness('disciple of phenax', 3).

% Found in: CSP
card_name('disciple of tevesh szat', 'Disciple of Tevesh Szat').
card_type('disciple of tevesh szat', 'Creature — Human Cleric').
card_types('disciple of tevesh szat', ['Creature']).
card_subtypes('disciple of tevesh szat', ['Human', 'Cleric']).
card_colors('disciple of tevesh szat', ['B']).
card_text('disciple of tevesh szat', '{T}: Target creature gets -1/-1 until end of turn.\n{4}{B}{B}, {T}, Sacrifice Disciple of Tevesh Szat: Target creature gets -6/-6 until end of turn.').
card_mana_cost('disciple of tevesh szat', ['2', 'B', 'B']).
card_cmc('disciple of tevesh szat', 4).
card_layout('disciple of tevesh szat', 'normal').
card_power('disciple of tevesh szat', 3).
card_toughness('disciple of tevesh szat', 1).

% Found in: GTC
card_name('disciple of the old ways', 'Disciple of the Old Ways').
card_type('disciple of the old ways', 'Creature — Human Warrior').
card_types('disciple of the old ways', ['Creature']).
card_subtypes('disciple of the old ways', ['Human', 'Warrior']).
card_colors('disciple of the old ways', ['G']).
card_text('disciple of the old ways', '{R}: Disciple of the Old Ways gains first strike until end of turn.').
card_mana_cost('disciple of the old ways', ['1', 'G']).
card_cmc('disciple of the old ways', 2).
card_layout('disciple of the old ways', 'normal').
card_power('disciple of the old ways', 2).
card_toughness('disciple of the old ways', 2).

% Found in: ORI
card_name('disciple of the ring', 'Disciple of the Ring').
card_type('disciple of the ring', 'Creature — Human Wizard').
card_types('disciple of the ring', ['Creature']).
card_subtypes('disciple of the ring', ['Human', 'Wizard']).
card_colors('disciple of the ring', ['U']).
card_text('disciple of the ring', '{1}, Exile an instant or sorcery card from your graveyard: Choose one —\n• Counter target noncreature spell unless its controller pays {2}.\n• Disciple of the Ring gets +1/+1 until end of turn.\n• Tap target creature.\n• Untap target creature.').
card_mana_cost('disciple of the ring', ['3', 'U', 'U']).
card_cmc('disciple of the ring', 5).
card_layout('disciple of the ring', 'normal').
card_power('disciple of the ring', 3).
card_toughness('disciple of the ring', 4).

% Found in: MRD
card_name('disciple of the vault', 'Disciple of the Vault').
card_type('disciple of the vault', 'Creature — Human Cleric').
card_types('disciple of the vault', ['Creature']).
card_subtypes('disciple of the vault', ['Human', 'Cleric']).
card_colors('disciple of the vault', ['B']).
card_text('disciple of the vault', 'Whenever an artifact is put into a graveyard from the battlefield, you may have target opponent lose 1 life.').
card_mana_cost('disciple of the vault', ['B']).
card_cmc('disciple of the vault', 1).
card_layout('disciple of the vault', 'normal').
card_power('disciple of the vault', 1).
card_toughness('disciple of the vault', 1).

% Found in: 10E, ONS
card_name('discombobulate', 'Discombobulate').
card_type('discombobulate', 'Instant').
card_types('discombobulate', ['Instant']).
card_subtypes('discombobulate', []).
card_colors('discombobulate', ['U']).
card_text('discombobulate', 'Counter target spell. Look at the top four cards of your library, then put them back in any order.').
card_mana_cost('discombobulate', ['2', 'U', 'U']).
card_cmc('discombobulate', 4).
card_layout('discombobulate', 'normal').

% Found in: USG
card_name('discordant dirge', 'Discordant Dirge').
card_type('discordant dirge', 'Enchantment').
card_types('discordant dirge', ['Enchantment']).
card_subtypes('discordant dirge', []).
card_colors('discordant dirge', ['B']).
card_text('discordant dirge', 'At the beginning of your upkeep, you may put a verse counter on Discordant Dirge.\n{B}, Sacrifice Discordant Dirge: Look at target opponent\'s hand and choose up to X cards from it, where X is the number of verse counters on Discordant Dirge. That player discards those cards.').
card_mana_cost('discordant dirge', ['3', 'B', 'B']).
card_cmc('discordant dirge', 5).
card_layout('discordant dirge', 'normal').

% Found in: MIR
card_name('discordant spirit', 'Discordant Spirit').
card_type('discordant spirit', 'Creature — Spirit').
card_types('discordant spirit', ['Creature']).
card_subtypes('discordant spirit', ['Spirit']).
card_colors('discordant spirit', ['B', 'R']).
card_text('discordant spirit', 'At the beginning of each end step, if it\'s an opponent\'s turn, put a +1/+1 counter on Discordant Spirit for each 1 damage dealt to you this turn.\nAt the beginning of your end step, remove all +1/+1 counters from Discordant Spirit.').
card_mana_cost('discordant spirit', ['2', 'B', 'R']).
card_cmc('discordant spirit', 4).
card_layout('discordant spirit', 'normal').
card_power('discordant spirit', 2).
card_toughness('discordant spirit', 2).
card_reserved('discordant spirit').

% Found in: KTK, pFNM
card_name('disdainful stroke', 'Disdainful Stroke').
card_type('disdainful stroke', 'Instant').
card_types('disdainful stroke', ['Instant']).
card_subtypes('disdainful stroke', []).
card_colors('disdainful stroke', ['U']).
card_text('disdainful stroke', 'Counter target spell with converted mana cost 4 or greater.').
card_mana_cost('disdainful stroke', ['1', 'U']).
card_cmc('disdainful stroke', 2).
card_layout('disdainful stroke', 'normal').

% Found in: UDS
card_name('disease carriers', 'Disease Carriers').
card_type('disease carriers', 'Creature — Rat').
card_types('disease carriers', ['Creature']).
card_subtypes('disease carriers', ['Rat']).
card_colors('disease carriers', ['B']).
card_text('disease carriers', 'When Disease Carriers dies, target creature gets -2/-2 until end of turn.').
card_mana_cost('disease carriers', ['2', 'B', 'B']).
card_cmc('disease carriers', 4).
card_layout('disease carriers', 'normal').
card_power('disease carriers', 2).
card_toughness('disease carriers', 2).

% Found in: ALL
card_name('diseased vermin', 'Diseased Vermin').
card_type('diseased vermin', 'Creature — Rat').
card_types('diseased vermin', ['Creature']).
card_subtypes('diseased vermin', ['Rat']).
card_colors('diseased vermin', ['B']).
card_text('diseased vermin', 'Whenever Diseased Vermin deals combat damage to a player, put an infection counter on it.\nAt the beginning of your upkeep, Diseased Vermin deals X damage to target opponent previously dealt damage by it, where X is the number of infection counters on it.').
card_mana_cost('diseased vermin', ['2', 'B']).
card_cmc('diseased vermin', 3).
card_layout('diseased vermin', 'normal').
card_power('diseased vermin', 1).
card_toughness('diseased vermin', 1).

% Found in: RAV
card_name('disembowel', 'Disembowel').
card_type('disembowel', 'Instant').
card_types('disembowel', ['Instant']).
card_subtypes('disembowel', []).
card_colors('disembowel', ['B']).
card_text('disembowel', 'Destroy target creature with converted mana cost X.').
card_mana_cost('disembowel', ['X', 'B']).
card_cmc('disembowel', 1).
card_layout('disembowel', 'normal').

% Found in: MIR
card_name('disempower', 'Disempower').
card_type('disempower', 'Instant').
card_types('disempower', ['Instant']).
card_subtypes('disempower', []).
card_colors('disempower', ['W']).
card_text('disempower', 'Put target artifact or enchantment on top of its owner\'s library.').
card_mana_cost('disempower', ['1', 'W']).
card_cmc('disempower', 2).
card_layout('disempower', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, ATH, BRB, CED, CEI, CST, ICE, LEA, LEB, ME2, ME3, MIR, MMQ, S00, TMP, TPR, TSB, USG, pARL, pFNM, pMPR
card_name('disenchant', 'Disenchant').
card_type('disenchant', 'Instant').
card_types('disenchant', ['Instant']).
card_subtypes('disenchant', []).
card_colors('disenchant', ['W']).
card_text('disenchant', 'Destroy target artifact or enchantment.').
card_mana_cost('disenchant', ['1', 'W']).
card_cmc('disenchant', 2).
card_layout('disenchant', 'normal').

% Found in: M10, M11, M12, M13
card_name('disentomb', 'Disentomb').
card_type('disentomb', 'Sorcery').
card_types('disentomb', ['Sorcery']).
card_subtypes('disentomb', []).
card_colors('disentomb', ['B']).
card_text('disentomb', 'Return target creature card from your graveyard to your hand.').
card_mana_cost('disentomb', ['B']).
card_cmc('disentomb', 1).
card_layout('disentomb', 'normal').

% Found in: ZEN
card_name('disfigure', 'Disfigure').
card_type('disfigure', 'Instant').
card_types('disfigure', ['Instant']).
card_subtypes('disfigure', []).
card_colors('disfigure', ['B']).
card_text('disfigure', 'Target creature gets -2/-2 until end of turn.').
card_mana_cost('disfigure', ['B']).
card_cmc('disfigure', 1).
card_layout('disfigure', 'normal').

% Found in: LEG, ME3
card_name('disharmony', 'Disharmony').
card_type('disharmony', 'Instant').
card_types('disharmony', ['Instant']).
card_subtypes('disharmony', []).
card_colors('disharmony', ['R']).
card_text('disharmony', 'Cast Disharmony only during combat before blockers are declared.\nUntap target attacking creature and remove it from combat. Gain control of that creature until end of turn.').
card_mana_cost('disharmony', ['2', 'R']).
card_cmc('disharmony', 3).
card_layout('disharmony', 'normal').
card_reserved('disharmony').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, ITP, LEA, LEB, RQS, TSB
card_name('disintegrate', 'Disintegrate').
card_type('disintegrate', 'Sorcery').
card_types('disintegrate', ['Sorcery']).
card_subtypes('disintegrate', []).
card_colors('disintegrate', ['R']).
card_text('disintegrate', 'Disintegrate deals X damage to target creature or player. That creature can\'t be regenerated this turn. If the creature would die this turn, exile it instead.').
card_mana_cost('disintegrate', ['X', 'R']).
card_cmc('disintegrate', 1).
card_layout('disintegrate', 'normal').

% Found in: FRF, KTK
card_name('dismal backwater', 'Dismal Backwater').
card_type('dismal backwater', 'Land').
card_types('dismal backwater', ['Land']).
card_subtypes('dismal backwater', []).
card_colors('dismal backwater', []).
card_text('dismal backwater', 'Dismal Backwater enters the battlefield tapped.\nWhen Dismal Backwater enters the battlefield, you gain 1 life.\n{T}: Add {U} or {B} to your mana pool.').
card_layout('dismal backwater', 'normal').

% Found in: PLC
card_name('dismal failure', 'Dismal Failure').
card_type('dismal failure', 'Instant').
card_types('dismal failure', ['Instant']).
card_subtypes('dismal failure', []).
card_colors('dismal failure', ['U']).
card_text('dismal failure', 'Counter target spell. Its controller discards a card.').
card_mana_cost('dismal failure', ['2', 'U', 'U']).
card_cmc('dismal failure', 4).
card_layout('dismal failure', 'normal').

% Found in: DST
card_name('dismantle', 'Dismantle').
card_type('dismantle', 'Sorcery').
card_types('dismantle', ['Sorcery']).
card_subtypes('dismantle', []).
card_colors('dismantle', ['R']).
card_text('dismantle', 'Destroy target artifact. If that artifact had counters on it, put that many +1/+1 counters or charge counters on an artifact you control.').
card_mana_cost('dismantle', ['2', 'R']).
card_cmc('dismantle', 3).
card_layout('dismantle', 'normal').

% Found in: INV
card_name('dismantling blow', 'Dismantling Blow').
card_type('dismantling blow', 'Instant').
card_types('dismantling blow', ['Instant']).
card_subtypes('dismantling blow', []).
card_colors('dismantling blow', ['W']).
card_text('dismantling blow', 'Kicker {2}{U} (You may pay an additional {2}{U} as you cast this spell.)\nDestroy target artifact or enchantment.\nIf Dismantling Blow was kicked, draw two cards.').
card_mana_cost('dismantling blow', ['2', 'W']).
card_cmc('dismantling blow', 3).
card_layout('dismantling blow', 'normal').

% Found in: MD1, MM2, NPH, pFNM
card_name('dismember', 'Dismember').
card_type('dismember', 'Instant').
card_types('dismember', ['Instant']).
card_subtypes('dismember', []).
card_colors('dismember', ['B']).
card_text('dismember', '({B/P} can be paid with either {B} or 2 life.)\nTarget creature gets -5/-5 until end of turn.').
card_mana_cost('dismember', ['1', 'B/P', 'B/P']).
card_cmc('dismember', 3).
card_layout('dismember', 'normal').

% Found in: C13, C14, TMP, TPR, pARL
card_name('dismiss', 'Dismiss').
card_type('dismiss', 'Instant').
card_types('dismiss', ['Instant']).
card_subtypes('dismiss', []).
card_colors('dismiss', ['U']).
card_text('dismiss', 'Counter target spell.\nDraw a card.').
card_mana_cost('dismiss', ['2', 'U', 'U']).
card_cmc('dismiss', 4).
card_layout('dismiss', 'normal').

% Found in: M14
card_name('dismiss into dream', 'Dismiss into Dream').
card_type('dismiss into dream', 'Enchantment').
card_types('dismiss into dream', ['Enchantment']).
card_subtypes('dismiss into dream', []).
card_colors('dismiss into dream', ['U']).
card_text('dismiss into dream', 'Each creature your opponents control is an Illusion in addition to its other types and has \"When this creature becomes the target of a spell or ability, sacrifice it.\"').
card_mana_cost('dismiss into dream', ['6', 'U']).
card_cmc('dismiss into dream', 7).
card_layout('dismiss into dream', 'normal').

% Found in: 7ED, USG
card_name('disorder', 'Disorder').
card_type('disorder', 'Sorcery').
card_types('disorder', ['Sorcery']).
card_subtypes('disorder', []).
card_colors('disorder', ['R']).
card_text('disorder', 'Disorder deals 2 damage to each white creature and each player who controls a white creature.').
card_mana_cost('disorder', ['1', 'R']).
card_cmc('disorder', 2).
card_layout('disorder', 'normal').

% Found in: M10
card_name('disorient', 'Disorient').
card_type('disorient', 'Instant').
card_types('disorient', ['Instant']).
card_subtypes('disorient', []).
card_colors('disorient', ['U']).
card_text('disorient', 'Target creature gets -7/-0 until end of turn.').
card_mana_cost('disorient', ['3', 'U']).
card_cmc('disorient', 4).
card_layout('disorient', 'normal').

% Found in: KTK
card_name('disowned ancestor', 'Disowned Ancestor').
card_type('disowned ancestor', 'Creature — Spirit Warrior').
card_types('disowned ancestor', ['Creature']).
card_subtypes('disowned ancestor', ['Spirit', 'Warrior']).
card_colors('disowned ancestor', ['B']).
card_text('disowned ancestor', 'Outlast {1}{B} ({1}{B}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)').
card_mana_cost('disowned ancestor', ['B']).
card_cmc('disowned ancestor', 1).
card_layout('disowned ancestor', 'normal').
card_power('disowned ancestor', 0).
card_toughness('disowned ancestor', 4).

% Found in: MM2, NPH
card_name('dispatch', 'Dispatch').
card_type('dispatch', 'Instant').
card_types('dispatch', ['Instant']).
card_subtypes('dispatch', []).
card_colors('dispatch', ['W']).
card_text('dispatch', 'Tap target creature.\nMetalcraft — If you control three or more artifacts, exile that creature.').
card_mana_cost('dispatch', ['W']).
card_cmc('dispatch', 1).
card_layout('dispatch', 'normal').

% Found in: BFZ, RTR, WWK
card_name('dispel', 'Dispel').
card_type('dispel', 'Instant').
card_types('dispel', ['Instant']).
card_subtypes('dispel', []).
card_colors('dispel', ['U']).
card_text('dispel', 'Counter target instant spell.').
card_mana_cost('dispel', ['U']).
card_cmc('dispel', 1).
card_layout('dispel', 'normal').

% Found in: ALA, MMA
card_name('dispeller\'s capsule', 'Dispeller\'s Capsule').
card_type('dispeller\'s capsule', 'Artifact').
card_types('dispeller\'s capsule', ['Artifact']).
card_subtypes('dispeller\'s capsule', []).
card_colors('dispeller\'s capsule', ['W']).
card_text('dispeller\'s capsule', '{2}{W}, {T}, Sacrifice Dispeller\'s Capsule: Destroy target artifact or enchantment.').
card_mana_cost('dispeller\'s capsule', ['W']).
card_cmc('dispeller\'s capsule', 1).
card_layout('dispeller\'s capsule', 'normal').

% Found in: SOM
card_name('dispense justice', 'Dispense Justice').
card_type('dispense justice', 'Instant').
card_types('dispense justice', ['Instant']).
card_subtypes('dispense justice', []).
card_colors('dispense justice', ['W']).
card_text('dispense justice', 'Target player sacrifices an attacking creature.\nMetalcraft — That player sacrifices two attacking creatures instead if you control three or more artifacts.').
card_mana_cost('dispense justice', ['2', 'W']).
card_cmc('dispense justice', 3).
card_layout('dispense justice', 'normal').

% Found in: SCG
card_name('dispersal shield', 'Dispersal Shield').
card_type('dispersal shield', 'Instant').
card_types('dispersal shield', ['Instant']).
card_subtypes('dispersal shield', []).
card_colors('dispersal shield', ['U']).
card_text('dispersal shield', 'Counter target spell if its converted mana cost is less than or equal to the highest converted mana cost among permanents you control.').
card_mana_cost('dispersal shield', ['1', 'U']).
card_cmc('dispersal shield', 2).
card_layout('dispersal shield', 'normal').

% Found in: M14, MOR, ORI, SOM
card_name('disperse', 'Disperse').
card_type('disperse', 'Instant').
card_types('disperse', ['Instant']).
card_subtypes('disperse', []).
card_colors('disperse', ['U']).
card_text('disperse', 'Return target nonland permanent to its owner\'s hand.').
card_mana_cost('disperse', ['1', 'U']).
card_cmc('disperse', 2).
card_layout('disperse', 'normal').

% Found in: ONS
card_name('dispersing orb', 'Dispersing Orb').
card_type('dispersing orb', 'Enchantment').
card_types('dispersing orb', ['Enchantment']).
card_subtypes('dispersing orb', []).
card_colors('dispersing orb', ['U']).
card_text('dispersing orb', '{3}{U}, Sacrifice a permanent: Return target permanent to its owner\'s hand.').
card_mana_cost('dispersing orb', ['3', 'U', 'U']).
card_cmc('dispersing orb', 5).
card_layout('dispersing orb', 'normal').

% Found in: ORI
card_name('displacement wave', 'Displacement Wave').
card_type('displacement wave', 'Sorcery').
card_types('displacement wave', ['Sorcery']).
card_subtypes('displacement wave', []).
card_colors('displacement wave', ['U']).
card_text('displacement wave', 'Return all nonland permanents with converted mana cost X or less to their owners\' hands.').
card_mana_cost('displacement wave', ['X', 'U', 'U']).
card_cmc('displacement wave', 2).
card_layout('displacement wave', 'normal').

% Found in: DTK
card_name('display of dominance', 'Display of Dominance').
card_type('display of dominance', 'Instant').
card_types('display of dominance', ['Instant']).
card_subtypes('display of dominance', []).
card_colors('display of dominance', ['G']).
card_text('display of dominance', 'Choose one —\n• Destroy target blue or black noncreature permanent.\n• Permanents you control can\'t be the targets of blue or black spells your opponents control this turn.').
card_mana_cost('display of dominance', ['1', 'G']).
card_cmc('display of dominance', 2).
card_layout('display of dominance', 'normal').

% Found in: INV, WTH
card_name('disrupt', 'Disrupt').
card_type('disrupt', 'Instant').
card_types('disrupt', ['Instant']).
card_subtypes('disrupt', []).
card_colors('disrupt', ['U']).
card_text('disrupt', 'Counter target instant or sorcery spell unless its controller pays {1}.\nDraw a card.').
card_mana_cost('disrupt', ['U']).
card_cmc('disrupt', 1).
card_layout('disrupt', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, CED, CEI, LEA, LEB
card_name('disrupting scepter', 'Disrupting Scepter').
card_type('disrupting scepter', 'Artifact').
card_types('disrupting scepter', ['Artifact']).
card_subtypes('disrupting scepter', []).
card_colors('disrupting scepter', []).
card_text('disrupting scepter', '{3}, {T}: Target player discards a card. Activate this ability only during your turn.').
card_mana_cost('disrupting scepter', ['3']).
card_cmc('disrupting scepter', 3).
card_layout('disrupting scepter', 'normal').

% Found in: BOK
card_name('disrupting shoal', 'Disrupting Shoal').
card_type('disrupting shoal', 'Instant — Arcane').
card_types('disrupting shoal', ['Instant']).
card_subtypes('disrupting shoal', ['Arcane']).
card_colors('disrupting shoal', ['U']).
card_text('disrupting shoal', 'You may exile a blue card with converted mana cost X from your hand rather than pay Disrupting Shoal\'s mana cost.\nCounter target spell if its converted mana cost is X.').
card_mana_cost('disrupting shoal', ['X', 'U', 'U']).
card_cmc('disrupting shoal', 2).
card_layout('disrupting shoal', 'normal').

% Found in: 5DN
card_name('disruption aura', 'Disruption Aura').
card_type('disruption aura', 'Enchantment — Aura').
card_types('disruption aura', ['Enchantment']).
card_subtypes('disruption aura', ['Aura']).
card_colors('disruption aura', ['U']).
card_text('disruption aura', 'Enchant artifact\nEnchanted artifact has \"At the beginning of your upkeep, sacrifice this artifact unless you pay its mana cost.\"').
card_mana_cost('disruption aura', ['2', 'U']).
card_cmc('disruption aura', 3).
card_layout('disruption aura', 'normal').

% Found in: ONS
card_name('disruptive pitmage', 'Disruptive Pitmage').
card_type('disruptive pitmage', 'Creature — Human Wizard').
card_types('disruptive pitmage', ['Creature']).
card_subtypes('disruptive pitmage', ['Human', 'Wizard']).
card_colors('disruptive pitmage', ['U']).
card_text('disruptive pitmage', '{T}: Counter target spell unless its controller pays {1}.\nMorph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('disruptive pitmage', ['2', 'U']).
card_cmc('disruptive pitmage', 3).
card_layout('disruptive pitmage', 'normal').
card_power('disruptive pitmage', 1).
card_toughness('disruptive pitmage', 1).

% Found in: BRB, USG
card_name('disruptive student', 'Disruptive Student').
card_type('disruptive student', 'Creature — Human Wizard').
card_types('disruptive student', ['Creature']).
card_subtypes('disruptive student', ['Human', 'Wizard']).
card_colors('disruptive student', ['U']).
card_text('disruptive student', '{T}: Counter target spell unless its controller pays {1}.').
card_mana_cost('disruptive student', ['2', 'U']).
card_cmc('disruptive student', 3).
card_layout('disruptive student', 'normal').
card_power('disruptive student', 1).
card_toughness('disruptive student', 1).

% Found in: DDJ, ISD, M15, MIR, pFNM
card_name('dissipate', 'Dissipate').
card_type('dissipate', 'Instant').
card_types('dissipate', ['Instant']).
card_subtypes('dissipate', []).
card_colors('dissipate', ['U']).
card_text('dissipate', 'Counter target spell. If that spell is countered this way, exile it instead of putting it into its owner\'s graveyard.').
card_mana_cost('dissipate', ['1', 'U', 'U']).
card_cmc('dissipate', 3).
card_layout('dissipate', 'normal').

% Found in: SOM
card_name('dissipation field', 'Dissipation Field').
card_type('dissipation field', 'Enchantment').
card_types('dissipation field', ['Enchantment']).
card_subtypes('dissipation field', []).
card_colors('dissipation field', ['U']).
card_text('dissipation field', 'Whenever a permanent deals damage to you, return it to its owner\'s hand.').
card_mana_cost('dissipation field', ['2', 'U', 'U']).
card_cmc('dissipation field', 4).
card_layout('dissipation field', 'normal').

% Found in: THS, pFNM
card_name('dissolve', 'Dissolve').
card_type('dissolve', 'Instant').
card_types('dissolve', ['Instant']).
card_subtypes('dissolve', []).
card_colors('dissolve', ['U']).
card_text('dissolve', 'Counter target spell. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('dissolve', ['1', 'U', 'U']).
card_cmc('dissolve', 3).
card_layout('dissolve', 'normal').

% Found in: H09, MOR
card_name('distant melody', 'Distant Melody').
card_type('distant melody', 'Sorcery').
card_types('distant melody', ['Sorcery']).
card_subtypes('distant melody', []).
card_colors('distant melody', ['U']).
card_text('distant melody', 'Choose a creature type. Draw a card for each permanent you control of that type.').
card_mana_cost('distant melody', ['3', 'U']).
card_cmc('distant melody', 4).
card_layout('distant melody', 'normal').

% Found in: MBS
card_name('distant memories', 'Distant Memories').
card_type('distant memories', 'Sorcery').
card_types('distant memories', ['Sorcery']).
card_subtypes('distant memories', []).
card_colors('distant memories', ['U']).
card_text('distant memories', 'Search your library for a card, exile it, then shuffle your library. Any opponent may have you put that card into your hand. If no player does, you draw three cards.').
card_mana_cost('distant memories', ['2', 'U', 'U']).
card_cmc('distant memories', 4).
card_layout('distant memories', 'normal').

% Found in: 8ED, MMQ
card_name('distorting lens', 'Distorting Lens').
card_type('distorting lens', 'Artifact').
card_types('distorting lens', ['Artifact']).
card_subtypes('distorting lens', []).
card_colors('distorting lens', []).
card_text('distorting lens', '{T}: Target permanent becomes the color of your choice until end of turn.').
card_mana_cost('distorting lens', ['2']).
card_cmc('distorting lens', 2).
card_layout('distorting lens', 'normal').

% Found in: C14, INV
card_name('distorting wake', 'Distorting Wake').
card_type('distorting wake', 'Sorcery').
card_types('distorting wake', ['Sorcery']).
card_subtypes('distorting wake', []).
card_colors('distorting wake', ['U']).
card_text('distorting wake', 'Return X target nonland permanents to their owners\' hands.').
card_mana_cost('distorting wake', ['X', 'U', 'U', 'U']).
card_cmc('distorting wake', 3).
card_layout('distorting wake', 'normal').

% Found in: ROE
card_name('distortion strike', 'Distortion Strike').
card_type('distortion strike', 'Sorcery').
card_types('distortion strike', ['Sorcery']).
card_subtypes('distortion strike', []).
card_colors('distortion strike', ['U']).
card_text('distortion strike', 'Target creature gets +1/+0 until end of turn and can\'t be blocked this turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('distortion strike', ['U']).
card_cmc('distortion strike', 1).
card_layout('distortion strike', 'normal').

% Found in: 10E, CHK, M12
card_name('distress', 'Distress').
card_type('distress', 'Sorcery').
card_types('distress', ['Sorcery']).
card_subtypes('distress', []).
card_colors('distress', ['B']).
card_text('distress', 'Target player reveals his or her hand. You choose a nonland card from it. That player discards that card.').
card_mana_cost('distress', ['B', 'B']).
card_cmc('distress', 2).
card_layout('distress', 'normal').

% Found in: TMP
card_name('disturbed burial', 'Disturbed Burial').
card_type('disturbed burial', 'Sorcery').
card_types('disturbed burial', ['Sorcery']).
card_subtypes('disturbed burial', []).
card_colors('disturbed burial', ['B']).
card_text('disturbed burial', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nReturn target creature card from your graveyard to your hand.').
card_mana_cost('disturbed burial', ['1', 'B']).
card_cmc('disturbed burial', 2).
card_layout('disturbed burial', 'normal').

% Found in: SHM
card_name('disturbing plot', 'Disturbing Plot').
card_type('disturbing plot', 'Sorcery').
card_types('disturbing plot', ['Sorcery']).
card_subtypes('disturbing plot', []).
card_colors('disturbing plot', ['B']).
card_text('disturbing plot', 'Return target creature card from a graveyard to its owner\'s hand.\nConspire (As you cast this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_mana_cost('disturbing plot', ['1', 'B']).
card_cmc('disturbing plot', 2).
card_layout('disturbing plot', 'normal').

% Found in: ONS
card_name('dive bomber', 'Dive Bomber').
card_type('dive bomber', 'Creature — Bird Soldier').
card_types('dive bomber', ['Creature']).
card_subtypes('dive bomber', ['Bird', 'Soldier']).
card_colors('dive bomber', ['W']).
card_text('dive bomber', 'Flying\n{T}, Sacrifice Dive Bomber: Dive Bomber deals 2 damage to target attacking or blocking creature.').
card_mana_cost('dive bomber', ['3', 'W']).
card_cmc('dive bomber', 4).
card_layout('dive bomber', 'normal').
card_power('dive bomber', 2).
card_toughness('dive bomber', 2).

% Found in: RAV
card_name('divebomber griffin', 'Divebomber Griffin').
card_type('divebomber griffin', 'Creature — Griffin').
card_types('divebomber griffin', ['Creature']).
card_subtypes('divebomber griffin', ['Griffin']).
card_colors('divebomber griffin', ['W']).
card_text('divebomber griffin', 'Flying\n{T}, Sacrifice Divebomber Griffin: Divebomber Griffin deals 3 damage to target attacking or blocking creature.').
card_mana_cost('divebomber griffin', ['3', 'W', 'W']).
card_cmc('divebomber griffin', 5).
card_layout('divebomber griffin', 'normal').
card_power('divebomber griffin', 3).
card_toughness('divebomber griffin', 2).

% Found in: SCG
card_name('divergent growth', 'Divergent Growth').
card_type('divergent growth', 'Instant').
card_types('divergent growth', ['Instant']).
card_subtypes('divergent growth', []).
card_colors('divergent growth', ['G']).
card_text('divergent growth', 'Until end of turn, lands you control gain \"{T}: Add one mana of any color to your mana pool.\"').
card_mana_cost('divergent growth', ['G']).
card_cmc('divergent growth', 1).
card_layout('divergent growth', 'normal').

% Found in: APC
card_name('diversionary tactics', 'Diversionary Tactics').
card_type('diversionary tactics', 'Enchantment').
card_types('diversionary tactics', ['Enchantment']).
card_subtypes('diversionary tactics', []).
card_colors('diversionary tactics', ['W']).
card_text('diversionary tactics', 'Tap two untapped creatures you control: Tap target creature.').
card_mana_cost('diversionary tactics', ['3', 'W']).
card_cmc('diversionary tactics', 4).
card_layout('diversionary tactics', 'normal').

% Found in: ODY
card_name('divert', 'Divert').
card_type('divert', 'Instant').
card_types('divert', ['Instant']).
card_subtypes('divert', []).
card_colors('divert', ['U']).
card_text('divert', 'Change the target of target spell with a single target unless that spell\'s controller pays {2}.').
card_mana_cost('divert', ['U']).
card_cmc('divert', 1).
card_layout('divert', 'normal').

% Found in: BNG, DKA, M10, M12, M13, M14, M15
card_name('divination', 'Divination').
card_type('divination', 'Sorcery').
card_types('divination', ['Sorcery']).
card_subtypes('divination', []).
card_colors('divination', ['U']).
card_text('divination', 'Draw two cards.').
card_mana_cost('divination', ['2', 'U']).
card_cmc('divination', 3).
card_layout('divination', 'normal').

% Found in: TSP
card_name('divine congregation', 'Divine Congregation').
card_type('divine congregation', 'Sorcery').
card_types('divine congregation', ['Sorcery']).
card_subtypes('divine congregation', []).
card_colors('divine congregation', ['W']).
card_text('divine congregation', 'You gain 2 life for each creature target player controls.\nSuspend 5—{1}{W} (Rather than cast this card from your hand, you may pay {1}{W} and exile it with five time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_mana_cost('divine congregation', ['3', 'W']).
card_cmc('divine congregation', 4).
card_layout('divine congregation', 'normal').

% Found in: AVR
card_name('divine deflection', 'Divine Deflection').
card_type('divine deflection', 'Instant').
card_types('divine deflection', ['Instant']).
card_subtypes('divine deflection', []).
card_colors('divine deflection', ['W']).
card_text('divine deflection', 'Prevent the next X damage that would be dealt to you and/or permanents you control this turn. If damage is prevented this way, Divine Deflection deals that much damage to target creature or player.').
card_mana_cost('divine deflection', ['X', 'W']).
card_cmc('divine deflection', 1).
card_layout('divine deflection', 'normal').

% Found in: M12, M13, M14, M15
card_name('divine favor', 'Divine Favor').
card_type('divine favor', 'Enchantment — Aura').
card_types('divine favor', ['Enchantment']).
card_subtypes('divine favor', ['Aura']).
card_colors('divine favor', ['W']).
card_text('divine favor', 'Enchant creature\nWhen Divine Favor enters the battlefield, you gain 3 life.\nEnchanted creature gets +1/+3.').
card_mana_cost('divine favor', ['1', 'W']).
card_cmc('divine favor', 2).
card_layout('divine favor', 'normal').

% Found in: LEG, ME3
card_name('divine intervention', 'Divine Intervention').
card_type('divine intervention', 'Enchantment').
card_types('divine intervention', ['Enchantment']).
card_subtypes('divine intervention', []).
card_colors('divine intervention', ['W']).
card_text('divine intervention', 'Divine Intervention enters the battlefield with two intervention counters on it.\nAt the beginning of your upkeep, remove an intervention counter from Divine Intervention.\nWhen you remove the last intervention counter from Divine Intervention, the game is a draw.').
card_mana_cost('divine intervention', ['6', 'W', 'W']).
card_cmc('divine intervention', 8).
card_layout('divine intervention', 'normal').
card_reserved('divine intervention').

% Found in: APC
card_name('divine light', 'Divine Light').
card_type('divine light', 'Sorcery').
card_types('divine light', ['Sorcery']).
card_subtypes('divine light', []).
card_colors('divine light', ['W']).
card_text('divine light', 'Prevent all damage that would be dealt this turn to creatures you control.').
card_mana_cost('divine light', ['W']).
card_cmc('divine light', 1).
card_layout('divine light', 'normal').

% Found in: 5ED, CHR, LEG, MBS, ME4, MIR
card_name('divine offering', 'Divine Offering').
card_type('divine offering', 'Instant').
card_types('divine offering', ['Instant']).
card_subtypes('divine offering', []).
card_colors('divine offering', ['W']).
card_text('divine offering', 'Destroy target artifact. You gain life equal to its converted mana cost.').
card_mana_cost('divine offering', ['1', 'W']).
card_cmc('divine offering', 2).
card_layout('divine offering', 'normal').

% Found in: INV
card_name('divine presence', 'Divine Presence').
card_type('divine presence', 'Enchantment').
card_types('divine presence', ['Enchantment']).
card_subtypes('divine presence', []).
card_colors('divine presence', ['W']).
card_text('divine presence', 'If a source would deal 4 or more damage to a creature or player, that source deals 3 damage to that creature or player instead.').
card_mana_cost('divine presence', ['2', 'W']).
card_cmc('divine presence', 3).
card_layout('divine presence', 'normal').

% Found in: ISD
card_name('divine reckoning', 'Divine Reckoning').
card_type('divine reckoning', 'Sorcery').
card_types('divine reckoning', ['Sorcery']).
card_subtypes('divine reckoning', []).
card_colors('divine reckoning', ['W']).
card_text('divine reckoning', 'Each player chooses a creature he or she controls. Destroy the rest.\nFlashback {5}{W}{W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('divine reckoning', ['2', 'W', 'W']).
card_cmc('divine reckoning', 4).
card_layout('divine reckoning', 'normal').

% Found in: MIR
card_name('divine retribution', 'Divine Retribution').
card_type('divine retribution', 'Instant').
card_types('divine retribution', ['Instant']).
card_subtypes('divine retribution', []).
card_colors('divine retribution', ['W']).
card_text('divine retribution', 'Divine Retribution deals damage to target attacking creature equal to the number of attacking creatures.').
card_mana_cost('divine retribution', ['1', 'W']).
card_cmc('divine retribution', 2).
card_layout('divine retribution', 'normal').
card_reserved('divine retribution').

% Found in: ODY
card_name('divine sacrament', 'Divine Sacrament').
card_type('divine sacrament', 'Enchantment').
card_types('divine sacrament', ['Enchantment']).
card_subtypes('divine sacrament', []).
card_colors('divine sacrament', ['W']).
card_text('divine sacrament', 'White creatures get +1/+1.\nThreshold — White creatures get an additional +1/+1 as long as seven or more cards are in your graveyard.').
card_mana_cost('divine sacrament', ['1', 'W', 'W']).
card_cmc('divine sacrament', 3).
card_layout('divine sacrament', 'normal').

% Found in: 4ED, 5ED, 6ED, LEG, MED
card_name('divine transformation', 'Divine Transformation').
card_type('divine transformation', 'Enchantment — Aura').
card_types('divine transformation', ['Enchantment']).
card_subtypes('divine transformation', ['Aura']).
card_colors('divine transformation', ['W']).
card_text('divine transformation', 'Enchant creature\nEnchanted creature gets +3/+3.').
card_mana_cost('divine transformation', ['2', 'W', 'W']).
card_cmc('divine transformation', 4).
card_layout('divine transformation', 'normal').

% Found in: M10, M13, M15, ORI, THS
card_name('divine verdict', 'Divine Verdict').
card_type('divine verdict', 'Instant').
card_types('divine verdict', ['Instant']).
card_subtypes('divine verdict', []).
card_colors('divine verdict', ['W']).
card_text('divine verdict', 'Destroy target attacking or blocking creature.').
card_mana_cost('divine verdict', ['3', 'W']).
card_cmc('divine verdict', 4).
card_layout('divine verdict', 'normal').

% Found in: C13
card_name('diviner spirit', 'Diviner Spirit').
card_type('diviner spirit', 'Creature — Spirit').
card_types('diviner spirit', ['Creature']).
card_subtypes('diviner spirit', ['Spirit']).
card_colors('diviner spirit', ['U']).
card_text('diviner spirit', 'Whenever Diviner Spirit deals combat damage to a player, you and that player each draw that many cards.').
card_mana_cost('diviner spirit', ['4', 'U']).
card_cmc('diviner spirit', 5).
card_layout('diviner spirit', 'normal').
card_power('diviner spirit', 2).
card_toughness('diviner spirit', 4).

% Found in: MOR
card_name('diviner\'s wand', 'Diviner\'s Wand').
card_type('diviner\'s wand', 'Tribal Artifact — Wizard Equipment').
card_types('diviner\'s wand', ['Tribal', 'Artifact']).
card_subtypes('diviner\'s wand', ['Wizard', 'Equipment']).
card_colors('diviner\'s wand', []).
card_text('diviner\'s wand', 'Equipped creature has \"Whenever you draw a card, this creature gets +1/+1 and gains flying until end of turn\" and \"{4}: Draw a card.\"\nWhenever a Wizard creature enters the battlefield, you may attach Diviner\'s Wand to it.\nEquip {3}').
card_mana_cost('diviner\'s wand', ['3']).
card_cmc('diviner\'s wand', 3).
card_layout('diviner\'s wand', 'normal').

% Found in: 8ED, PCY
card_name('diving griffin', 'Diving Griffin').
card_type('diving griffin', 'Creature — Griffin').
card_types('diving griffin', ['Creature']).
card_subtypes('diving griffin', ['Griffin']).
card_colors('diving griffin', ['W']).
card_text('diving griffin', 'Flying, vigilance').
card_mana_cost('diving griffin', ['1', 'W', 'W']).
card_cmc('diving griffin', 3).
card_layout('diving griffin', 'normal').
card_power('diving griffin', 2).
card_toughness('diving griffin', 2).

% Found in: NMS
card_name('divining witch', 'Divining Witch').
card_type('divining witch', 'Creature — Human Spellshaper').
card_types('divining witch', ['Creature']).
card_subtypes('divining witch', ['Human', 'Spellshaper']).
card_colors('divining witch', ['B']).
card_text('divining witch', '{1}{B}, {T}, Discard a card: Name a card. Exile the top six cards of your library. Reveal cards from the top of your library until you reveal the named card, then put that card into your hand. Exile all other cards revealed this way.').
card_mana_cost('divining witch', ['1', 'B']).
card_cmc('divining witch', 2).
card_layout('divining witch', 'normal').
card_power('divining witch', 1).
card_toughness('divining witch', 1).

% Found in: C13, EVE, MMA
card_name('divinity of pride', 'Divinity of Pride').
card_type('divinity of pride', 'Creature — Spirit Avatar').
card_types('divinity of pride', ['Creature']).
card_subtypes('divinity of pride', ['Spirit', 'Avatar']).
card_colors('divinity of pride', ['W', 'B']).
card_text('divinity of pride', 'Flying, lifelink\nDivinity of Pride gets +4/+4 as long as you have 25 or more life.').
card_mana_cost('divinity of pride', ['W/B', 'W/B', 'W/B', 'W/B', 'W/B']).
card_cmc('divinity of pride', 5).
card_layout('divinity of pride', 'normal').
card_power('divinity of pride', 4).
card_toughness('divinity of pride', 4).

% Found in: RAV
card_name('dizzy spell', 'Dizzy Spell').
card_type('dizzy spell', 'Instant').
card_types('dizzy spell', ['Instant']).
card_subtypes('dizzy spell', []).
card_colors('dizzy spell', ['U']).
card_text('dizzy spell', 'Target creature gets -3/-0 until end of turn.\nTransmute {1}{U}{U} ({1}{U}{U}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Transmute only as a sorcery.)').
card_mana_cost('dizzy spell', ['U']).
card_cmc('dizzy spell', 1).
card_layout('dizzy spell', 'normal').

% Found in: EXO
card_name('dizzying gaze', 'Dizzying Gaze').
card_type('dizzying gaze', 'Enchantment — Aura').
card_types('dizzying gaze', ['Enchantment']).
card_subtypes('dizzying gaze', ['Aura']).
card_colors('dizzying gaze', ['R']).
card_text('dizzying gaze', 'Enchant creature you control\n{R}: Enchanted creature deals 1 damage to target creature with flying.').
card_mana_cost('dizzying gaze', ['R']).
card_cmc('dizzying gaze', 1).
card_layout('dizzying gaze', 'normal').

% Found in: DDJ, GPT, pPRE
card_name('djinn illuminatus', 'Djinn Illuminatus').
card_type('djinn illuminatus', 'Creature — Djinn').
card_types('djinn illuminatus', ['Creature']).
card_subtypes('djinn illuminatus', ['Djinn']).
card_colors('djinn illuminatus', ['U', 'R']).
card_text('djinn illuminatus', '({U/R} can be paid with either {U} or {R}.)\nFlying\nEach instant and sorcery spell you cast has replicate. The replicate cost is equal to its mana cost. (When you cast it, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)').
card_mana_cost('djinn illuminatus', ['5', 'U/R', 'U/R']).
card_cmc('djinn illuminatus', 7).
card_layout('djinn illuminatus', 'normal').
card_power('djinn illuminatus', 3).
card_toughness('djinn illuminatus', 5).

% Found in: C13
card_name('djinn of infinite deceits', 'Djinn of Infinite Deceits').
card_type('djinn of infinite deceits', 'Creature — Djinn').
card_types('djinn of infinite deceits', ['Creature']).
card_subtypes('djinn of infinite deceits', ['Djinn']).
card_colors('djinn of infinite deceits', ['U']).
card_text('djinn of infinite deceits', 'Flying\n{T}: Exchange control of two target nonlegendary creatures. You can\'t activate this ability during combat.').
card_mana_cost('djinn of infinite deceits', ['4', 'U', 'U']).
card_cmc('djinn of infinite deceits', 6).
card_layout('djinn of infinite deceits', 'normal').
card_power('djinn of infinite deceits', 2).
card_toughness('djinn of infinite deceits', 7).

% Found in: POR
card_name('djinn of the lamp', 'Djinn of the Lamp').
card_type('djinn of the lamp', 'Creature — Djinn').
card_types('djinn of the lamp', ['Creature']).
card_subtypes('djinn of the lamp', ['Djinn']).
card_colors('djinn of the lamp', ['U']).
card_text('djinn of the lamp', 'Flying').
card_mana_cost('djinn of the lamp', ['5', 'U', 'U']).
card_cmc('djinn of the lamp', 7).
card_layout('djinn of the lamp', 'normal').
card_power('djinn of the lamp', 5).
card_toughness('djinn of the lamp', 6).

% Found in: M10, M12
card_name('djinn of wishes', 'Djinn of Wishes').
card_type('djinn of wishes', 'Creature — Djinn').
card_types('djinn of wishes', ['Creature']).
card_subtypes('djinn of wishes', ['Djinn']).
card_colors('djinn of wishes', ['U']).
card_text('djinn of wishes', 'Flying\nDjinn of Wishes enters the battlefield with three wish counters on it.\n{2}{U}{U}, Remove a wish counter from Djinn of Wishes: Reveal the top card of your library. You may play that card without paying its mana cost. If you don\'t, exile it.').
card_mana_cost('djinn of wishes', ['3', 'U', 'U']).
card_cmc('djinn of wishes', 5).
card_layout('djinn of wishes', 'normal').
card_power('djinn of wishes', 4).
card_toughness('djinn of wishes', 4).

% Found in: INV
card_name('do or die', 'Do or Die').
card_type('do or die', 'Sorcery').
card_types('do or die', ['Sorcery']).
card_subtypes('do or die', []).
card_colors('do or die', ['B']).
card_text('do or die', 'Separate all creatures target player controls into two piles. Destroy all creatures in the pile of that player\'s choice. They can\'t be regenerated.').
card_mana_cost('do or die', ['1', 'B']).
card_cmc('do or die', 2).
card_layout('do or die', 'normal').

% Found in: APC, TSB
card_name('dodecapod', 'Dodecapod').
card_type('dodecapod', 'Artifact Creature — Golem').
card_types('dodecapod', ['Artifact', 'Creature']).
card_subtypes('dodecapod', ['Golem']).
card_colors('dodecapod', []).
card_text('dodecapod', 'If a spell or ability an opponent controls causes you to discard Dodecapod, put it onto the battlefield with two +1/+1 counters on it instead of putting it into your graveyard.').
card_mana_cost('dodecapod', ['4']).
card_cmc('dodecapod', 4).
card_layout('dodecapod', 'normal').
card_power('dodecapod', 3).
card_toughness('dodecapod', 3).

% Found in: ODY
card_name('dogged hunter', 'Dogged Hunter').
card_type('dogged hunter', 'Creature — Human Nomad').
card_types('dogged hunter', ['Creature']).
card_subtypes('dogged hunter', ['Human', 'Nomad']).
card_colors('dogged hunter', ['W']).
card_text('dogged hunter', '{T}: Destroy target creature token.').
card_mana_cost('dogged hunter', ['2', 'W']).
card_cmc('dogged hunter', 3).
card_layout('dogged hunter', 'normal').
card_power('dogged hunter', 1).
card_toughness('dogged hunter', 1).

% Found in: RAV
card_name('dogpile', 'Dogpile').
card_type('dogpile', 'Instant').
card_types('dogpile', ['Instant']).
card_subtypes('dogpile', []).
card_colors('dogpile', ['R']).
card_text('dogpile', 'Dogpile deals damage to target creature or player equal to the number of attacking creatures you control.').
card_mana_cost('dogpile', ['3', 'R']).
card_cmc('dogpile', 4).
card_layout('dogpile', 'normal').

% Found in: CHK
card_name('dokai, weaver of life', 'Dokai, Weaver of Life').
card_type('dokai, weaver of life', 'Legendary Creature — Human Monk').
card_types('dokai, weaver of life', ['Creature']).
card_subtypes('dokai, weaver of life', ['Human', 'Monk']).
card_supertypes('dokai, weaver of life', ['Legendary']).
card_colors('dokai, weaver of life', ['G']).
card_text('dokai, weaver of life', '{4}{G}{G}, {T}: Put an X/X green Elemental creature token onto the battlefield, where X is the number of lands you control.').
card_mana_cost('dokai, weaver of life', ['1', 'G']).
card_cmc('dokai, weaver of life', 2).
card_layout('dokai, weaver of life', 'flip').
card_power('dokai, weaver of life', 3).
card_toughness('dokai, weaver of life', 3).

% Found in: LRW
card_name('dolmen gate', 'Dolmen Gate').
card_type('dolmen gate', 'Artifact').
card_types('dolmen gate', ['Artifact']).
card_subtypes('dolmen gate', []).
card_colors('dolmen gate', []).
card_text('dolmen gate', 'Prevent all combat damage that would be dealt to attacking creatures you control.').
card_mana_cost('dolmen gate', ['2']).
card_cmc('dolmen gate', 2).
card_layout('dolmen gate', 'normal').

% Found in: M14, ROE
card_name('domestication', 'Domestication').
card_type('domestication', 'Enchantment — Aura').
card_types('domestication', ['Enchantment']).
card_subtypes('domestication', ['Aura']).
card_colors('domestication', ['U']).
card_text('domestication', 'Enchant creature\nYou control enchanted creature.\nAt the beginning of your end step, if enchanted creature\'s power is 4 or greater, sacrifice Domestication.').
card_mana_cost('domestication', ['2', 'U', 'U']).
card_cmc('domestication', 4).
card_layout('domestication', 'normal').

% Found in: PLS
card_name('dominaria\'s judgment', 'Dominaria\'s Judgment').
card_type('dominaria\'s judgment', 'Instant').
card_types('dominaria\'s judgment', ['Instant']).
card_subtypes('dominaria\'s judgment', []).
card_colors('dominaria\'s judgment', ['W']).
card_text('dominaria\'s judgment', 'Until end of turn, creatures you control gain protection from white if you control a Plains, from blue if you control an Island, from black if you control a Swamp, from red if you control a Mountain, and from green if you control a Forest.').
card_mana_cost('dominaria\'s judgment', ['2', 'W']).
card_cmc('dominaria\'s judgment', 3).
card_layout('dominaria\'s judgment', 'normal').

% Found in: NMS
card_name('dominate', 'Dominate').
card_type('dominate', 'Instant').
card_types('dominate', ['Instant']).
card_subtypes('dominate', []).
card_colors('dominate', ['U']).
card_text('dominate', 'Gain control of target creature with converted mana cost X or less. (This effect lasts indefinitely.)').
card_mana_cost('dominate', ['X', '1', 'U', 'U']).
card_cmc('dominate', 3).
card_layout('dominate', 'normal').

% Found in: EXO
card_name('dominating licid', 'Dominating Licid').
card_type('dominating licid', 'Creature — Licid').
card_types('dominating licid', ['Creature']).
card_subtypes('dominating licid', ['Licid']).
card_colors('dominating licid', ['U']).
card_text('dominating licid', '{1}{U}{U}, {T}: Dominating Licid loses this ability and becomes an Aura enchantment with enchant creature. Attach it to target creature. You may pay {U} to end this effect.\nYou control enchanted creature.').
card_mana_cost('dominating licid', ['1', 'U', 'U']).
card_cmc('dominating licid', 3).
card_layout('dominating licid', 'normal').
card_power('dominating licid', 1).
card_toughness('dominating licid', 1).
card_reserved('dominating licid').

% Found in: BFZ, DDP
card_name('dominator drone', 'Dominator Drone').
card_type('dominator drone', 'Creature — Eldrazi Drone').
card_types('dominator drone', ['Creature']).
card_subtypes('dominator drone', ['Eldrazi', 'Drone']).
card_colors('dominator drone', []).
card_text('dominator drone', 'Devoid (This card has no color.)\nIngest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.)\nWhen Dominator Drone enters the battlefield, if you control another colorless creature, each opponent loses 2 life.').
card_mana_cost('dominator drone', ['2', 'B']).
card_cmc('dominator drone', 3).
card_layout('dominator drone', 'normal').
card_power('dominator drone', 3).
card_toughness('dominator drone', 2).

% Found in: MRD
card_name('domineer', 'Domineer').
card_type('domineer', 'Enchantment — Aura').
card_types('domineer', ['Enchantment']).
card_subtypes('domineer', ['Aura']).
card_colors('domineer', ['U']).
card_text('domineer', 'Enchant artifact creature\nYou control enchanted artifact creature.').
card_mana_cost('domineer', ['1', 'U', 'U']).
card_cmc('domineer', 3).
card_layout('domineer', 'normal').

% Found in: C14
card_name('domineering will', 'Domineering Will').
card_type('domineering will', 'Instant').
card_types('domineering will', ['Instant']).
card_subtypes('domineering will', []).
card_colors('domineering will', ['U']).
card_text('domineering will', 'Target player gains control of up to three target nonattacking creatures until end of turn. Untap those creatures. They block this turn if able.').
card_mana_cost('domineering will', ['3', 'U']).
card_cmc('domineering will', 4).
card_layout('domineering will', 'normal').

% Found in: CMD, EVE
card_name('dominus of fealty', 'Dominus of Fealty').
card_type('dominus of fealty', 'Creature — Spirit Avatar').
card_types('dominus of fealty', ['Creature']).
card_subtypes('dominus of fealty', ['Spirit', 'Avatar']).
card_colors('dominus of fealty', ['U', 'R']).
card_text('dominus of fealty', 'Flying\nAt the beginning of your upkeep, you may gain control of target permanent until end of turn. If you do, untap it and it gains haste until end of turn.').
card_mana_cost('dominus of fealty', ['U/R', 'U/R', 'U/R', 'U/R', 'U/R']).
card_cmc('dominus of fealty', 5).
card_layout('dominus of fealty', 'normal').
card_power('dominus of fealty', 4).
card_toughness('dominus of fealty', 4).

% Found in: GTC
card_name('domri rade', 'Domri Rade').
card_type('domri rade', 'Planeswalker — Domri').
card_types('domri rade', ['Planeswalker']).
card_subtypes('domri rade', ['Domri']).
card_colors('domri rade', ['R', 'G']).
card_text('domri rade', '+1: Look at the top card of your library. If it\'s a creature card, you may reveal it and put it into your hand.\n−2: Target creature you control fights another target creature.\n−7: You get an emblem with \"Creatures you control have double strike, trample, hexproof, and haste.\"').
card_mana_cost('domri rade', ['1', 'R', 'G']).
card_cmc('domri rade', 3).
card_layout('domri rade', 'normal').
card_loyalty('domri rade', 3).

% Found in: UDS
card_name('donate', 'Donate').
card_type('donate', 'Sorcery').
card_types('donate', ['Sorcery']).
card_subtypes('donate', []).
card_colors('donate', ['U']).
card_text('donate', 'Target player gains control of target permanent you control.').
card_mana_cost('donate', ['2', 'U']).
card_cmc('donate', 3).
card_layout('donate', 'normal').
card_reserved('donate').

% Found in: ME3, PTK
card_name('dong zhou, the tyrant', 'Dong Zhou, the Tyrant').
card_type('dong zhou, the tyrant', 'Legendary Creature — Human Soldier').
card_types('dong zhou, the tyrant', ['Creature']).
card_subtypes('dong zhou, the tyrant', ['Human', 'Soldier']).
card_supertypes('dong zhou, the tyrant', ['Legendary']).
card_colors('dong zhou, the tyrant', ['R']).
card_text('dong zhou, the tyrant', 'When Dong Zhou, the Tyrant enters the battlefield, target creature an opponent controls deals damage equal to its power to that player.').
card_mana_cost('dong zhou, the tyrant', ['4', 'R']).
card_cmc('dong zhou, the tyrant', 5).
card_layout('dong zhou, the tyrant', 'normal').
card_power('dong zhou, the tyrant', 3).
card_toughness('dong zhou, the tyrant', 3).

% Found in: CMD, M10, M11, M12, M14, pMPR
card_name('doom blade', 'Doom Blade').
card_type('doom blade', 'Instant').
card_types('doom blade', ['Instant']).
card_subtypes('doom blade', []).
card_colors('doom blade', ['B']).
card_text('doom blade', 'Destroy target nonblack creature.').
card_mana_cost('doom blade', ['1', 'B']).
card_cmc('doom blade', 2).
card_layout('doom blade', 'normal').

% Found in: ONS
card_name('doom cannon', 'Doom Cannon').
card_type('doom cannon', 'Artifact').
card_types('doom cannon', ['Artifact']).
card_subtypes('doom cannon', []).
card_colors('doom cannon', []).
card_text('doom cannon', 'As Doom Cannon enters the battlefield, choose a creature type.\n{3}, {T}, Sacrifice a creature of the chosen type: Doom Cannon deals 3 damage to target creature or player.').
card_mana_cost('doom cannon', ['6']).
card_cmc('doom cannon', 6).
card_layout('doom cannon', 'normal').

% Found in: 10E, ONS
card_name('doomed necromancer', 'Doomed Necromancer').
card_type('doomed necromancer', 'Creature — Human Cleric Mercenary').
card_types('doomed necromancer', ['Creature']).
card_subtypes('doomed necromancer', ['Human', 'Cleric', 'Mercenary']).
card_colors('doomed necromancer', ['B']).
card_text('doomed necromancer', '{B}, {T}, Sacrifice Doomed Necromancer: Return target creature card from your graveyard to the battlefield.').
card_mana_cost('doomed necromancer', ['2', 'B']).
card_cmc('doomed necromancer', 3).
card_layout('doomed necromancer', 'normal').
card_power('doomed necromancer', 2).
card_toughness('doomed necromancer', 2).

% Found in: CNS, DDK, ISD
card_name('doomed traveler', 'Doomed Traveler').
card_type('doomed traveler', 'Creature — Human Soldier').
card_types('doomed traveler', ['Creature']).
card_subtypes('doomed traveler', ['Human', 'Soldier']).
card_colors('doomed traveler', ['W']).
card_text('doomed traveler', 'When Doomed Traveler dies, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_mana_cost('doomed traveler', ['W']).
card_cmc('doomed traveler', 1).
card_layout('doomed traveler', 'normal').
card_power('doomed traveler', 1).
card_toughness('doomed traveler', 1).

% Found in: DDJ, EVE
card_name('doomgape', 'Doomgape').
card_type('doomgape', 'Creature — Elemental').
card_types('doomgape', ['Creature']).
card_subtypes('doomgape', ['Elemental']).
card_colors('doomgape', ['B', 'G']).
card_text('doomgape', 'Trample\nAt the beginning of your upkeep, sacrifice a creature. You gain life equal to that creature\'s toughness.').
card_mana_cost('doomgape', ['4', 'B/G', 'B/G', 'B/G']).
card_cmc('doomgape', 7).
card_layout('doomgape', 'normal').
card_power('doomgape', 10).
card_toughness('doomgape', 10).

% Found in: 6ED, WTH
card_name('doomsday', 'Doomsday').
card_type('doomsday', 'Sorcery').
card_types('doomsday', ['Sorcery']).
card_subtypes('doomsday', []).
card_colors('doomsday', ['B']).
card_text('doomsday', 'Search your library and graveyard for five cards and exile the rest. Put the chosen cards on top of your library in any order. You lose half your life, rounded up.').
card_mana_cost('doomsday', ['B', 'B', 'B']).
card_cmc('doomsday', 3).
card_layout('doomsday', 'normal').

% Found in: PLS
card_name('doomsday specter', 'Doomsday Specter').
card_type('doomsday specter', 'Creature — Specter').
card_types('doomsday specter', ['Creature']).
card_subtypes('doomsday specter', ['Specter']).
card_colors('doomsday specter', ['U', 'B']).
card_text('doomsday specter', 'Flying\nWhen Doomsday Specter enters the battlefield, return a blue or black creature you control to its owner\'s hand.\nWhenever Doomsday Specter deals combat damage to a player, look at that player\'s hand and choose a card from it. The player discards that card.').
card_mana_cost('doomsday specter', ['2', 'U', 'B']).
card_cmc('doomsday specter', 4).
card_layout('doomsday specter', 'normal').
card_power('doomsday specter', 2).
card_toughness('doomsday specter', 3).

% Found in: JOU, pPRE
card_name('doomwake giant', 'Doomwake Giant').
card_type('doomwake giant', 'Enchantment Creature — Giant').
card_types('doomwake giant', ['Enchantment', 'Creature']).
card_subtypes('doomwake giant', ['Giant']).
card_colors('doomwake giant', ['B']).
card_text('doomwake giant', 'Constellation — Whenever Doomwake Giant or another enchantment enters the battlefield under your control, creatures your opponents control get -1/-1 until end of turn.').
card_mana_cost('doomwake giant', ['4', 'B']).
card_cmc('doomwake giant', 5).
card_layout('doomwake giant', 'normal').
card_power('doomwake giant', 4).
card_toughness('doomwake giant', 6).

% Found in: M14, MOR, pPRE
card_name('door of destinies', 'Door of Destinies').
card_type('door of destinies', 'Artifact').
card_types('door of destinies', ['Artifact']).
card_subtypes('door of destinies', []).
card_colors('door of destinies', []).
card_text('door of destinies', 'As Door of Destinies enters the battlefield, choose a creature type.\nWhenever you cast a spell of the chosen type, put a charge counter on Door of Destinies.\nCreatures you control of the chosen type get +1/+1 for each charge counter on Door of Destinies.').
card_mana_cost('door of destinies', ['4']).
card_cmc('door of destinies', 4).
card_layout('door of destinies', 'normal').

% Found in: 5DN, HOP, M13
card_name('door to nothingness', 'Door to Nothingness').
card_type('door to nothingness', 'Artifact').
card_types('door to nothingness', ['Artifact']).
card_subtypes('door to nothingness', []).
card_colors('door to nothingness', []).
card_text('door to nothingness', 'Door to Nothingness enters the battlefield tapped.\n{W}{W}{U}{U}{B}{B}{R}{R}{G}{G}, {T}, Sacrifice Door to Nothingness: Target player loses the game.').
card_mana_cost('door to nothingness', ['5']).
card_cmc('door to nothingness', 5).
card_layout('door to nothingness', 'normal').

% Found in: RTR
card_name('doorkeeper', 'Doorkeeper').
card_type('doorkeeper', 'Creature — Homunculus').
card_types('doorkeeper', ['Creature']).
card_subtypes('doorkeeper', ['Homunculus']).
card_colors('doorkeeper', ['U']).
card_text('doorkeeper', 'Defender\n{2}{U}, {T}: Target player puts the top X cards of his or her library into his or her graveyard, where X is the number of creatures with defender you control.').
card_mana_cost('doorkeeper', ['1', 'U']).
card_cmc('doorkeeper', 2).
card_layout('doorkeeper', 'normal').
card_power('doorkeeper', 0).
card_toughness('doorkeeper', 4).

% Found in: LRW, V11, pCMP
card_name('doran, the siege tower', 'Doran, the Siege Tower').
card_type('doran, the siege tower', 'Legendary Creature — Treefolk Shaman').
card_types('doran, the siege tower', ['Creature']).
card_subtypes('doran, the siege tower', ['Treefolk', 'Shaman']).
card_supertypes('doran, the siege tower', ['Legendary']).
card_colors('doran, the siege tower', ['W', 'B', 'G']).
card_text('doran, the siege tower', 'Each creature assigns combat damage equal to its toughness rather than its power.').
card_mana_cost('doran, the siege tower', ['B', 'G', 'W']).
card_cmc('doran, the siege tower', 3).
card_layout('doran, the siege tower', 'normal').
card_power('doran, the siege tower', 0).
card_toughness('doran, the siege tower', 5).

% Found in: ROE
card_name('dormant gomazoa', 'Dormant Gomazoa').
card_type('dormant gomazoa', 'Creature — Jellyfish').
card_types('dormant gomazoa', ['Creature']).
card_subtypes('dormant gomazoa', ['Jellyfish']).
card_colors('dormant gomazoa', ['U']).
card_text('dormant gomazoa', 'Flying\nDormant Gomazoa enters the battlefield tapped.\nDormant Gomazoa doesn\'t untap during your untap step.\nWhenever you become the target of a spell, you may untap Dormant Gomazoa.').
card_mana_cost('dormant gomazoa', ['1', 'U', 'U']).
card_cmc('dormant gomazoa', 3).
card_layout('dormant gomazoa', 'normal').
card_power('dormant gomazoa', 5).
card_toughness('dormant gomazoa', 5).

% Found in: PLC
card_name('dormant sliver', 'Dormant Sliver').
card_type('dormant sliver', 'Creature — Sliver').
card_types('dormant sliver', ['Creature']).
card_subtypes('dormant sliver', ['Sliver']).
card_colors('dormant sliver', ['U', 'G']).
card_text('dormant sliver', 'All Sliver creatures have defender.\nAll Slivers have \"When this permanent enters the battlefield, draw a card.\"').
card_mana_cost('dormant sliver', ['2', 'G', 'U']).
card_cmc('dormant sliver', 4).
card_layout('dormant sliver', 'normal').
card_power('dormant sliver', 2).
card_toughness('dormant sliver', 2).

% Found in: C14, VIS
card_name('dormant volcano', 'Dormant Volcano').
card_type('dormant volcano', 'Land').
card_types('dormant volcano', ['Land']).
card_subtypes('dormant volcano', []).
card_colors('dormant volcano', []).
card_text('dormant volcano', 'Dormant Volcano enters the battlefield tapped.\nWhen Dormant Volcano enters the battlefield, sacrifice it unless you return an untapped Mountain you control to its owner\'s hand.\n{T}: Add {1}{R} to your mana pool.').
card_layout('dormant volcano', 'normal').

% Found in: CHK
card_name('dosan the falling leaf', 'Dosan the Falling Leaf').
card_type('dosan the falling leaf', 'Legendary Creature — Human Monk').
card_types('dosan the falling leaf', ['Creature']).
card_subtypes('dosan the falling leaf', ['Human', 'Monk']).
card_supertypes('dosan the falling leaf', ['Legendary']).
card_colors('dosan the falling leaf', ['G']).
card_text('dosan the falling leaf', 'Players can cast spells only during their own turns.').
card_mana_cost('dosan the falling leaf', ['1', 'G', 'G']).
card_cmc('dosan the falling leaf', 3).
card_layout('dosan the falling leaf', 'normal').
card_power('dosan the falling leaf', 2).
card_toughness('dosan the falling leaf', 2).

% Found in: SOK
card_name('dosan\'s oldest chant', 'Dosan\'s Oldest Chant').
card_type('dosan\'s oldest chant', 'Sorcery').
card_types('dosan\'s oldest chant', ['Sorcery']).
card_subtypes('dosan\'s oldest chant', []).
card_colors('dosan\'s oldest chant', ['G']).
card_text('dosan\'s oldest chant', 'You gain 6 life.\nDraw a card.').
card_mana_cost('dosan\'s oldest chant', ['4', 'G']).
card_cmc('dosan\'s oldest chant', 5).
card_layout('dosan\'s oldest chant', 'normal').

% Found in: EVE, HOP
card_name('double cleave', 'Double Cleave').
card_type('double cleave', 'Instant').
card_types('double cleave', ['Instant']).
card_subtypes('double cleave', []).
card_colors('double cleave', ['W', 'R']).
card_text('double cleave', 'Target creature gains double strike until end of turn. (It deals both first-strike and regular combat damage.)').
card_mana_cost('double cleave', ['1', 'R/W']).
card_cmc('double cleave', 2).
card_layout('double cleave', 'normal').

% Found in: UGL
card_name('double cross', 'Double Cross').
card_type('double cross', 'Sorcery').
card_types('double cross', ['Sorcery']).
card_subtypes('double cross', []).
card_colors('double cross', ['B']).
card_text('double cross', 'Choose another player. Look at that player\'s hand and choose one of those cards other than a basic land. He or she discards that card. At the beginning of the next game with the player, look at the player\'s hand and choose one of those cards other than a basic land. He or she discards that card.').
card_mana_cost('double cross', ['3', 'B', 'B']).
card_cmc('double cross', 5).
card_layout('double cross', 'normal').

% Found in: UGL
card_name('double deal', 'Double Deal').
card_type('double deal', 'Sorcery').
card_types('double deal', ['Sorcery']).
card_subtypes('double deal', []).
card_colors('double deal', ['R']).
card_text('double deal', 'Choose another player. Double Deal deals 3 damage to that player now and deals an additional 3 damage to the player at the beginning of the next game with the player.').
card_mana_cost('double deal', ['4', 'R']).
card_cmc('double deal', 5).
card_layout('double deal', 'normal').

% Found in: UGL
card_name('double dip', 'Double Dip').
card_type('double dip', 'Instant').
card_types('double dip', ['Instant']).
card_subtypes('double dip', []).
card_colors('double dip', ['W']).
card_text('double dip', 'Choose another player. Gain 5 life now and an additional 5 life at the beginning of the next game with that player.').
card_mana_cost('double dip', ['4', 'W']).
card_cmc('double dip', 5).
card_layout('double dip', 'normal').

% Found in: UNH
card_name('double header', 'Double Header').
card_type('double header', 'Creature — Drake').
card_types('double header', ['Creature']).
card_subtypes('double header', ['Drake']).
card_colors('double header', ['U']).
card_text('double header', 'Flying\nWhen Double Header comes into play, you may return target permanent with a two-word name to its owner\'s hand.').
card_mana_cost('double header', ['3', 'U', 'U']).
card_cmc('double header', 5).
card_layout('double header', 'normal').
card_power('double header', 2).
card_toughness('double header', 3).

% Found in: ARB
card_name('double negative', 'Double Negative').
card_type('double negative', 'Instant').
card_types('double negative', ['Instant']).
card_subtypes('double negative', []).
card_colors('double negative', ['U', 'R']).
card_text('double negative', 'Counter up to two target spells.').
card_mana_cost('double negative', ['U', 'U', 'R']).
card_cmc('double negative', 3).
card_layout('double negative', 'normal').

% Found in: UGL
card_name('double play', 'Double Play').
card_type('double play', 'Sorcery').
card_types('double play', ['Sorcery']).
card_subtypes('double play', []).
card_colors('double play', ['G']).
card_text('double play', 'Choose another player. Search your library for a basic land and put that land into play. At the beginning of the next game with that player, search your library for an additional basic land and put that land into play. In both cases, shuffle your library afterwards.').
card_mana_cost('double play', ['3', 'G', 'G']).
card_cmc('double play', 5).
card_layout('double play', 'normal').

% Found in: CNS
card_name('double stroke', 'Double Stroke').
card_type('double stroke', 'Conspiracy').
card_types('double stroke', ['Conspiracy']).
card_subtypes('double stroke', []).
card_colors('double stroke', []).
card_text('double stroke', 'Hidden agenda (Start the game with this conspiracy face down in the command zone and secretly name a card. You may turn this conspiracy face up any time and reveal the chosen name.)\nWhenever you cast an instant or sorcery spell with the chosen name, you may copy it. You may choose new targets for the copy.').
card_layout('double stroke', 'normal').

% Found in: UGL
card_name('double take', 'Double Take').
card_type('double take', 'Instant').
card_types('double take', ['Instant']).
card_subtypes('double take', []).
card_colors('double take', ['U']).
card_text('double take', 'Choose another player. Draw two cards now and draw an additional two cards at the beginning of the next game with that player.').
card_mana_cost('double take', ['3', 'U', 'U']).
card_cmc('double take', 5).
card_layout('double take', 'normal').

% Found in: M12
card_name('doubling chant', 'Doubling Chant').
card_type('doubling chant', 'Sorcery').
card_types('doubling chant', ['Sorcery']).
card_subtypes('doubling chant', []).
card_colors('doubling chant', ['G']).
card_text('doubling chant', 'For each creature you control, you may search your library for a creature card with the same name as that creature. Put those cards onto the battlefield, then shuffle your library.').
card_mana_cost('doubling chant', ['5', 'G']).
card_cmc('doubling chant', 6).
card_layout('doubling chant', 'normal').

% Found in: 10E, 5DN
card_name('doubling cube', 'Doubling Cube').
card_type('doubling cube', 'Artifact').
card_types('doubling cube', ['Artifact']).
card_subtypes('doubling cube', []).
card_colors('doubling cube', []).
card_text('doubling cube', '{3}, {T}: Double the amount of each type of mana in your mana pool.').
card_mana_cost('doubling cube', ['2']).
card_cmc('doubling cube', 2).
card_layout('doubling cube', 'normal').

% Found in: MMA, RAV, pJGP
card_name('doubling season', 'Doubling Season').
card_type('doubling season', 'Enchantment').
card_types('doubling season', ['Enchantment']).
card_subtypes('doubling season', []).
card_colors('doubling season', ['G']).
card_text('doubling season', 'If an effect would put one or more tokens onto the battlefield under your control, it puts twice that many of those tokens onto the battlefield instead.\nIf an effect would place one or more counters on a permanent you control, it places twice that many of those counters on that permanent instead.').
card_mana_cost('doubling season', ['4', 'G']).
card_cmc('doubling season', 5).
card_layout('doubling season', 'normal').

% Found in: ONS
card_name('doubtless one', 'Doubtless One').
card_type('doubtless one', 'Creature — Cleric Avatar').
card_types('doubtless one', ['Creature']).
card_subtypes('doubtless one', ['Cleric', 'Avatar']).
card_colors('doubtless one', ['W']).
card_text('doubtless one', 'Doubtless One\'s power and toughness are each equal to the number of Clerics on the battlefield.\nWhenever Doubtless One deals damage, you gain that much life.').
card_mana_cost('doubtless one', ['3', 'W']).
card_cmc('doubtless one', 4).
card_layout('doubtless one', 'normal').
card_power('doubtless one', '*').
card_toughness('doubtless one', '*').

% Found in: USG
card_name('douse', 'Douse').
card_type('douse', 'Enchantment').
card_types('douse', ['Enchantment']).
card_subtypes('douse', []).
card_colors('douse', ['U']).
card_text('douse', '{1}{U}: Counter target red spell.').
card_mana_cost('douse', ['2', 'U']).
card_cmc('douse', 3).
card_layout('douse', 'normal').

% Found in: FRF, GPT
card_name('douse in gloom', 'Douse in Gloom').
card_type('douse in gloom', 'Instant').
card_types('douse in gloom', ['Instant']).
card_subtypes('douse in gloom', []).
card_colors('douse in gloom', ['B']).
card_text('douse in gloom', 'Douse in Gloom deals 2 damage to target creature and you gain 2 life.').
card_mana_cost('douse in gloom', ['2', 'B']).
card_cmc('douse in gloom', 3).
card_layout('douse in gloom', 'normal').

% Found in: DIS
card_name('dovescape', 'Dovescape').
card_type('dovescape', 'Enchantment').
card_types('dovescape', ['Enchantment']).
card_subtypes('dovescape', []).
card_colors('dovescape', ['W', 'U']).
card_text('dovescape', '({W/U} can be paid with either {W} or {U}.)\nWhenever a player casts a noncreature spell, counter that spell. That player puts X 1/1 white and blue Bird creature tokens with flying onto the battlefield, where X is the spell\'s converted mana cost.').
card_mana_cost('dovescape', ['3', 'W/U', 'W/U', 'W/U']).
card_cmc('dovescape', 6).
card_layout('dovescape', 'normal').

% Found in: DGM
card_name('down', 'Down').
card_type('down', 'Sorcery').
card_types('down', ['Sorcery']).
card_subtypes('down', []).
card_colors('down', ['B']).
card_text('down', 'Target player discards two cards.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('down', ['3', 'B']).
card_cmc('down', 4).
card_layout('down', 'split').
card_sides('down', 'dirty').

% Found in: WTH
card_name('downdraft', 'Downdraft').
card_type('downdraft', 'Enchantment').
card_types('downdraft', ['Enchantment']).
card_subtypes('downdraft', []).
card_colors('downdraft', ['G']).
card_text('downdraft', '{G}: Target creature loses flying until end of turn.\nSacrifice Downdraft: Downdraft deals 2 damage to each creature with flying.').
card_mana_cost('downdraft', ['2', 'G']).
card_cmc('downdraft', 3).
card_layout('downdraft', 'normal').

% Found in: DDI, NMS
card_name('downhill charge', 'Downhill Charge').
card_type('downhill charge', 'Instant').
card_types('downhill charge', ['Instant']).
card_subtypes('downhill charge', []).
card_colors('downhill charge', ['R']).
card_text('downhill charge', 'You may sacrifice a Mountain rather than pay Downhill Charge\'s mana cost.\nTarget creature gets +X/+0 until end of turn, where X is the number of Mountains you control.').
card_mana_cost('downhill charge', ['2', 'R']).
card_cmc('downhill charge', 3).
card_layout('downhill charge', 'normal').

% Found in: M13
card_name('downpour', 'Downpour').
card_type('downpour', 'Instant').
card_types('downpour', ['Instant']).
card_subtypes('downpour', []).
card_colors('downpour', ['U']).
card_text('downpour', 'Tap up to three target creatures.').
card_mana_cost('downpour', ['1', 'U']).
card_cmc('downpour', 2).
card_layout('downpour', 'normal').

% Found in: RTR
card_name('downsize', 'Downsize').
card_type('downsize', 'Instant').
card_types('downsize', ['Instant']).
card_subtypes('downsize', []).
card_colors('downsize', ['U']).
card_text('downsize', 'Target creature you don\'t control gets -4/-0 until end of turn.\nOverload {2}{U} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_mana_cost('downsize', ['U']).
card_cmc('downsize', 1).
card_layout('downsize', 'normal').

% Found in: PC2, RAV
card_name('dowsing shaman', 'Dowsing Shaman').
card_type('dowsing shaman', 'Creature — Centaur Shaman').
card_types('dowsing shaman', ['Creature']).
card_subtypes('dowsing shaman', ['Centaur', 'Shaman']).
card_colors('dowsing shaman', ['G']).
card_text('dowsing shaman', '{2}{G}, {T}: Return target enchantment card from your graveyard to your hand.').
card_mana_cost('dowsing shaman', ['4', 'G']).
card_cmc('dowsing shaman', 5).
card_layout('dowsing shaman', 'normal').
card_power('dowsing shaman', 3).
card_toughness('dowsing shaman', 4).

% Found in: DRB, PLS
card_name('draco', 'Draco').
card_type('draco', 'Artifact Creature — Dragon').
card_types('draco', ['Artifact', 'Creature']).
card_subtypes('draco', ['Dragon']).
card_colors('draco', []).
card_text('draco', 'Domain — Draco costs {2} less to cast for each basic land type among lands you control.\nFlying\nDomain — At the beginning of your upkeep, sacrifice Draco unless you pay {10}. This cost is reduced by {2} for each basic land type among lands you control.').
card_mana_cost('draco', ['16']).
card_cmc('draco', 16).
card_layout('draco', 'normal').
card_power('draco', 9).
card_toughness('draco', 9).

% Found in: FEM
card_name('draconian cylix', 'Draconian Cylix').
card_type('draconian cylix', 'Artifact').
card_types('draconian cylix', ['Artifact']).
card_subtypes('draconian cylix', []).
card_colors('draconian cylix', []).
card_text('draconian cylix', '{2}, {T}, Discard a card at random: Regenerate target creature.').
card_mana_cost('draconian cylix', ['3']).
card_cmc('draconian cylix', 3).
card_layout('draconian cylix', 'normal').
card_reserved('draconian cylix').

% Found in: DTK
card_name('draconic roar', 'Draconic Roar').
card_type('draconic roar', 'Instant').
card_types('draconic roar', ['Instant']).
card_subtypes('draconic roar', []).
card_colors('draconic roar', ['R']).
card_text('draconic roar', 'As an additional cost to cast Draconic Roar, you may reveal a Dragon card from your hand.\nDraconic Roar deals 3 damage to target creature. If you revealed a Dragon card or controlled a Dragon as you cast Draconic Roar, Draconic Roar deals 3 damage to that creature\'s controller.').
card_mana_cost('draconic roar', ['1', 'R']).
card_cmc('draconic roar', 2).
card_layout('draconic roar', 'normal').

% Found in: TMP, TPR
card_name('dracoplasm', 'Dracoplasm').
card_type('dracoplasm', 'Creature — Shapeshifter').
card_types('dracoplasm', ['Creature']).
card_subtypes('dracoplasm', ['Shapeshifter']).
card_colors('dracoplasm', ['U', 'R']).
card_text('dracoplasm', 'Flying\nAs Dracoplasm enters the battlefield, sacrifice any number of creatures. Dracoplasm\'s power becomes the total power of those creatures and its toughness becomes their total toughness.\n{R}: Dracoplasm gets +1/+0 until end of turn.').
card_mana_cost('dracoplasm', ['U', 'R']).
card_cmc('dracoplasm', 2).
card_layout('dracoplasm', 'normal').
card_power('dracoplasm', 0).
card_toughness('dracoplasm', 0).

% Found in: ATQ
card_name('drafna\'s restoration', 'Drafna\'s Restoration').
card_type('drafna\'s restoration', 'Sorcery').
card_types('drafna\'s restoration', ['Sorcery']).
card_subtypes('drafna\'s restoration', []).
card_colors('drafna\'s restoration', ['U']).
card_text('drafna\'s restoration', 'Return any number of target artifact cards from target player\'s graveyard to the top of his or her library in any order.').
card_mana_cost('drafna\'s restoration', ['U']).
card_cmc('drafna\'s restoration', 1).
card_layout('drafna\'s restoration', 'normal').

% Found in: CON, MMA
card_name('drag down', 'Drag Down').
card_type('drag down', 'Instant').
card_types('drag down', ['Instant']).
card_subtypes('drag down', []).
card_colors('drag down', ['B']).
card_text('drag down', 'Domain — Target creature gets -1/-1 until end of turn for each basic land type among lands you control.').
card_mana_cost('drag down', ['2', 'B']).
card_cmc('drag down', 3).
card_layout('drag down', 'normal').

% Found in: ARB
card_name('dragon appeasement', 'Dragon Appeasement').
card_type('dragon appeasement', 'Enchantment').
card_types('dragon appeasement', ['Enchantment']).
card_subtypes('dragon appeasement', []).
card_colors('dragon appeasement', ['B', 'R', 'G']).
card_text('dragon appeasement', 'Skip your draw step.\nWhenever you sacrifice a creature, you may draw a card.').
card_mana_cost('dragon appeasement', ['3', 'B', 'R', 'G']).
card_cmc('dragon appeasement', 6).
card_layout('dragon appeasement', 'normal').

% Found in: APC
card_name('dragon arch', 'Dragon Arch').
card_type('dragon arch', 'Artifact').
card_types('dragon arch', ['Artifact']).
card_subtypes('dragon arch', []).
card_colors('dragon arch', []).
card_text('dragon arch', '{2}, {T}: You may put a multicolored creature card from your hand onto the battlefield.').
card_mana_cost('dragon arch', ['5']).
card_cmc('dragon arch', 5).
card_layout('dragon arch', 'normal').

% Found in: FRF
card_name('dragon bell monk', 'Dragon Bell Monk').
card_type('dragon bell monk', 'Creature — Human Monk').
card_types('dragon bell monk', ['Creature']).
card_subtypes('dragon bell monk', ['Human', 'Monk']).
card_colors('dragon bell monk', ['W']).
card_text('dragon bell monk', 'Vigilance\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_mana_cost('dragon bell monk', ['2', 'W']).
card_cmc('dragon bell monk', 3).
card_layout('dragon bell monk', 'normal').
card_power('dragon bell monk', 2).
card_toughness('dragon bell monk', 2).

% Found in: DDL, MRD, USG
card_name('dragon blood', 'Dragon Blood').
card_type('dragon blood', 'Artifact').
card_types('dragon blood', ['Artifact']).
card_subtypes('dragon blood', []).
card_colors('dragon blood', []).
card_text('dragon blood', '{3}, {T}: Put a +1/+1 counter on target creature.').
card_mana_cost('dragon blood', ['3']).
card_cmc('dragon blood', 3).
card_layout('dragon blood', 'normal').

% Found in: ARC, SCG
card_name('dragon breath', 'Dragon Breath').
card_type('dragon breath', 'Enchantment — Aura').
card_types('dragon breath', ['Enchantment']).
card_subtypes('dragon breath', ['Aura']).
card_colors('dragon breath', ['R']).
card_text('dragon breath', 'Enchant creature\nEnchanted creature has haste.\n{R}: Enchanted creature gets +1/+0 until end of turn.\nWhen a creature with converted mana cost 6 or greater enters the battlefield, you may return Dragon Breath from your graveyard to the battlefield attached to that creature.').
card_mana_cost('dragon breath', ['1', 'R']).
card_cmc('dragon breath', 2).
card_layout('dragon breath', 'normal').

% Found in: ARB, pPRE
card_name('dragon broodmother', 'Dragon Broodmother').
card_type('dragon broodmother', 'Creature — Dragon').
card_types('dragon broodmother', ['Creature']).
card_subtypes('dragon broodmother', ['Dragon']).
card_colors('dragon broodmother', ['R', 'G']).
card_text('dragon broodmother', 'Flying\nAt the beginning of each upkeep, put a 1/1 red and green Dragon creature token with flying and devour 2 onto the battlefield. (As the token enters the battlefield, you may sacrifice any number of creatures. It enters the battlefield with twice that many +1/+1 counters on it.)').
card_mana_cost('dragon broodmother', ['2', 'R', 'R', 'R', 'G']).
card_cmc('dragon broodmother', 6).
card_layout('dragon broodmother', 'normal').
card_power('dragon broodmother', 4).
card_toughness('dragon broodmother', 4).

% Found in: M14
card_name('dragon egg', 'Dragon Egg').
card_type('dragon egg', 'Creature — Dragon').
card_types('dragon egg', ['Creature']).
card_subtypes('dragon egg', ['Dragon']).
card_colors('dragon egg', ['R']).
card_text('dragon egg', 'Defender (This creature can\'t attack.)\nWhen Dragon Egg dies, put a 2/2 red Dragon creature token with flying onto the battlefield. It has \"{R}: This creature gets +1/+0 until end of turn.\"').
card_mana_cost('dragon egg', ['2', 'R']).
card_cmc('dragon egg', 3).
card_layout('dragon egg', 'normal').
card_power('dragon egg', 0).
card_toughness('dragon egg', 2).

% Found in: 3ED, 4ED, 5ED, 6ED, ATQ, ME4, MED
card_name('dragon engine', 'Dragon Engine').
card_type('dragon engine', 'Artifact Creature — Construct').
card_types('dragon engine', ['Artifact', 'Creature']).
card_subtypes('dragon engine', ['Construct']).
card_colors('dragon engine', []).
card_text('dragon engine', '{2}: Dragon Engine gets +1/+0 until end of turn.').
card_mana_cost('dragon engine', ['3']).
card_cmc('dragon engine', 3).
card_layout('dragon engine', 'normal').
card_power('dragon engine', 1).
card_toughness('dragon engine', 3).

% Found in: SCG
card_name('dragon fangs', 'Dragon Fangs').
card_type('dragon fangs', 'Enchantment — Aura').
card_types('dragon fangs', ['Enchantment']).
card_subtypes('dragon fangs', ['Aura']).
card_colors('dragon fangs', ['G']).
card_text('dragon fangs', 'Enchant creature\nEnchanted creature gets +1/+1 and has trample.\nWhen a creature with converted mana cost 6 or greater enters the battlefield, you may return Dragon Fangs from your graveyard to the battlefield attached to that creature.').
card_mana_cost('dragon fangs', ['1', 'G']).
card_cmc('dragon fangs', 2).
card_layout('dragon fangs', 'normal').

% Found in: ALA, ARC, DDG, DTK, ORI, pMEI
card_name('dragon fodder', 'Dragon Fodder').
card_type('dragon fodder', 'Sorcery').
card_types('dragon fodder', ['Sorcery']).
card_subtypes('dragon fodder', []).
card_colors('dragon fodder', ['R']).
card_text('dragon fodder', 'Put two 1/1 red Goblin creature tokens onto the battlefield.').
card_mana_cost('dragon fodder', ['1', 'R']).
card_cmc('dragon fodder', 2).
card_layout('dragon fodder', 'normal').

% Found in: KTK
card_name('dragon grip', 'Dragon Grip').
card_type('dragon grip', 'Enchantment — Aura').
card_types('dragon grip', ['Enchantment']).
card_subtypes('dragon grip', ['Aura']).
card_colors('dragon grip', ['R']).
card_text('dragon grip', 'Ferocious — If you control a creature with power 4 or greater, you may cast Dragon Grip as though it had flash. (You may cast it any time you could cast an instant.)\nEnchant creature\nEnchanted creature gets +2/+0 and has first strike.').
card_mana_cost('dragon grip', ['2', 'R']).
card_cmc('dragon grip', 3).
card_layout('dragon grip', 'normal').

% Found in: M13, M14
card_name('dragon hatchling', 'Dragon Hatchling').
card_type('dragon hatchling', 'Creature — Dragon').
card_types('dragon hatchling', ['Creature']).
card_subtypes('dragon hatchling', ['Dragon']).
card_colors('dragon hatchling', ['R']).
card_text('dragon hatchling', 'Flying\n{R}: Dragon Hatchling gets +1/+0 until end of turn.').
card_mana_cost('dragon hatchling', ['1', 'R']).
card_cmc('dragon hatchling', 2).
card_layout('dragon hatchling', 'normal').
card_power('dragon hatchling', 0).
card_toughness('dragon hatchling', 1).

% Found in: DTK
card_name('dragon hunter', 'Dragon Hunter').
card_type('dragon hunter', 'Creature — Human Warrior').
card_types('dragon hunter', ['Creature']).
card_subtypes('dragon hunter', ['Human', 'Warrior']).
card_colors('dragon hunter', ['W']).
card_text('dragon hunter', 'Protection from Dragons\nDragon Hunter can block Dragons as though it had reach.').
card_mana_cost('dragon hunter', ['W']).
card_cmc('dragon hunter', 1).
card_layout('dragon hunter', 'normal').
card_power('dragon hunter', 2).
card_toughness('dragon hunter', 1).

% Found in: SCG
card_name('dragon mage', 'Dragon Mage').
card_type('dragon mage', 'Creature — Dragon Wizard').
card_types('dragon mage', ['Creature']).
card_subtypes('dragon mage', ['Dragon', 'Wizard']).
card_colors('dragon mage', ['R']).
card_text('dragon mage', 'Flying\nWhenever Dragon Mage deals combat damage to a player, each player discards his or her hand, then draws seven cards.').
card_mana_cost('dragon mage', ['5', 'R', 'R']).
card_cmc('dragon mage', 7).
card_layout('dragon mage', 'normal').
card_power('dragon mage', 5).
card_toughness('dragon mage', 5).

% Found in: THS
card_name('dragon mantle', 'Dragon Mantle').
card_type('dragon mantle', 'Enchantment — Aura').
card_types('dragon mantle', ['Enchantment']).
card_subtypes('dragon mantle', ['Aura']).
card_colors('dragon mantle', ['R']).
card_text('dragon mantle', 'Enchant creature\nWhen Dragon Mantle enters the battlefield, draw a card.\nEnchanted creature has \"{R}: This creature gets +1/+0 until end of turn.\"').
card_mana_cost('dragon mantle', ['R']).
card_cmc('dragon mantle', 1).
card_layout('dragon mantle', 'normal').

% Found in: 6ED, VIS
card_name('dragon mask', 'Dragon Mask').
card_type('dragon mask', 'Artifact').
card_types('dragon mask', ['Artifact']).
card_subtypes('dragon mask', []).
card_colors('dragon mask', []).
card_text('dragon mask', '{3}, {T}: Target creature you control gets +2/+2 until end of turn. Return it to its owner\'s hand at the beginning of the next end step. (Return it only if it\'s on the battlefield.)').
card_mana_cost('dragon mask', ['3']).
card_cmc('dragon mask', 3).
card_layout('dragon mask', 'normal').

% Found in: 10E, ONS
card_name('dragon roost', 'Dragon Roost').
card_type('dragon roost', 'Enchantment').
card_types('dragon roost', ['Enchantment']).
card_subtypes('dragon roost', []).
card_colors('dragon roost', ['R']).
card_text('dragon roost', '{5}{R}{R}: Put a 5/5 red Dragon creature token with flying onto the battlefield. (It can\'t be blocked except by creatures with flying or reach.)').
card_mana_cost('dragon roost', ['4', 'R', 'R']).
card_cmc('dragon roost', 6).
card_layout('dragon roost', 'normal').

% Found in: SCG
card_name('dragon scales', 'Dragon Scales').
card_type('dragon scales', 'Enchantment — Aura').
card_types('dragon scales', ['Enchantment']).
card_subtypes('dragon scales', ['Aura']).
card_colors('dragon scales', ['W']).
card_text('dragon scales', 'Enchant creature\nEnchanted creature gets +1/+2 and has vigilance.\nWhen a creature with converted mana cost 6 or greater enters the battlefield, you may return Dragon Scales from your graveyard to the battlefield attached to that creature.').
card_mana_cost('dragon scales', ['1', 'W']).
card_cmc('dragon scales', 2).
card_layout('dragon scales', 'normal').

% Found in: SCG
card_name('dragon shadow', 'Dragon Shadow').
card_type('dragon shadow', 'Enchantment — Aura').
card_types('dragon shadow', ['Enchantment']).
card_subtypes('dragon shadow', ['Aura']).
card_colors('dragon shadow', ['B']).
card_text('dragon shadow', 'Enchant creature\nEnchanted creature gets +1/+0 and has fear. (It can\'t be blocked except by artifact creatures and/or black creatures.)\nWhen a creature with converted mana cost 6 or greater enters the battlefield, you may return Dragon Shadow from your graveyard to the battlefield attached to that creature.').
card_mana_cost('dragon shadow', ['1', 'B']).
card_cmc('dragon shadow', 2).
card_layout('dragon shadow', 'normal').

% Found in: DTK
card_name('dragon tempest', 'Dragon Tempest').
card_type('dragon tempest', 'Enchantment').
card_types('dragon tempest', ['Enchantment']).
card_subtypes('dragon tempest', []).
card_colors('dragon tempest', ['R']).
card_text('dragon tempest', 'Whenever a creature with flying enters the battlefield under your control, it gains haste until end of turn.\nWhenever a Dragon enters the battlefield under your control, it deals X damage to target creature or player, where X is the number of Dragons you control.').
card_mana_cost('dragon tempest', ['1', 'R']).
card_cmc('dragon tempest', 2).
card_layout('dragon tempest', 'normal').

% Found in: KTK, pLPA
card_name('dragon throne of tarkir', 'Dragon Throne of Tarkir').
card_type('dragon throne of tarkir', 'Legendary Artifact — Equipment').
card_types('dragon throne of tarkir', ['Artifact']).
card_subtypes('dragon throne of tarkir', ['Equipment']).
card_supertypes('dragon throne of tarkir', ['Legendary']).
card_colors('dragon throne of tarkir', []).
card_text('dragon throne of tarkir', 'Equipped creature has defender and \"{2}, {T}: Other creatures you control gain trample and get +X/+X until end of turn, where X is this creature\'s power.\"\nEquip {3}').
card_mana_cost('dragon throne of tarkir', ['4']).
card_cmc('dragon throne of tarkir', 4).
card_layout('dragon throne of tarkir', 'normal').

% Found in: SCG
card_name('dragon tyrant', 'Dragon Tyrant').
card_type('dragon tyrant', 'Creature — Dragon').
card_types('dragon tyrant', ['Creature']).
card_subtypes('dragon tyrant', ['Dragon']).
card_colors('dragon tyrant', ['R']).
card_text('dragon tyrant', 'Flying, trample\nDouble strike (This creature deals both first-strike and regular combat damage.)\nAt the beginning of your upkeep, sacrifice Dragon Tyrant unless you pay {R}{R}{R}{R}.\n{R}: Dragon Tyrant gets +1/+0 until end of turn.').
card_mana_cost('dragon tyrant', ['8', 'R', 'R']).
card_cmc('dragon tyrant', 10).
card_layout('dragon tyrant', 'normal').
card_power('dragon tyrant', 6).
card_toughness('dragon tyrant', 6).

% Found in: 2ED, 3ED, 4ED, ARC, CED, CEI, CMD, DDG, DRB, LEA, LEB, M10, TSB
card_name('dragon whelp', 'Dragon Whelp').
card_type('dragon whelp', 'Creature — Dragon').
card_types('dragon whelp', ['Creature']).
card_subtypes('dragon whelp', ['Dragon']).
card_colors('dragon whelp', ['R']).
card_text('dragon whelp', 'Flying\n{R}: Dragon Whelp gets +1/+0 until end of turn. If this ability has been activated four or more times this turn, sacrifice Dragon Whelp at the beginning of the next end step.').
card_mana_cost('dragon whelp', ['2', 'R', 'R']).
card_cmc('dragon whelp', 4).
card_layout('dragon whelp', 'normal').
card_power('dragon whelp', 2).
card_toughness('dragon whelp', 3).

% Found in: DTK
card_name('dragon whisperer', 'Dragon Whisperer').
card_type('dragon whisperer', 'Creature — Human Shaman').
card_types('dragon whisperer', ['Creature']).
card_subtypes('dragon whisperer', ['Human', 'Shaman']).
card_colors('dragon whisperer', ['R']).
card_text('dragon whisperer', '{R}: Dragon Whisperer gains flying until end of turn.\n{1}{R}: Dragon Whisperer gets +1/+0 until end of turn.\nFormidable — {4}{R}{R}: Put a 4/4 red Dragon creature token with flying onto the battlefield. Activate this ability only if creatures you control have total power 8 or greater.').
card_mana_cost('dragon whisperer', ['R', 'R']).
card_cmc('dragon whisperer', 2).
card_layout('dragon whisperer', 'normal').
card_power('dragon whisperer', 2).
card_toughness('dragon whisperer', 2).

% Found in: SCG
card_name('dragon wings', 'Dragon Wings').
card_type('dragon wings', 'Enchantment — Aura').
card_types('dragon wings', ['Enchantment']).
card_subtypes('dragon wings', ['Aura']).
card_colors('dragon wings', ['U']).
card_text('dragon wings', 'Enchant creature\nEnchanted creature has flying.\nCycling {1}{U} ({1}{U}, Discard this card: Draw a card.)\nWhen a creature with converted mana cost 6 or greater enters the battlefield, you may return Dragon Wings from your graveyard to the battlefield attached to that creature.').
card_mana_cost('dragon wings', ['1', 'U']).
card_cmc('dragon wings', 2).
card_layout('dragon wings', 'normal').

% Found in: 10E, 9ED, DDG, DPA, DST, M10, M11, M12
card_name('dragon\'s claw', 'Dragon\'s Claw').
card_type('dragon\'s claw', 'Artifact').
card_types('dragon\'s claw', ['Artifact']).
card_subtypes('dragon\'s claw', []).
card_colors('dragon\'s claw', []).
card_text('dragon\'s claw', 'Whenever a player casts a red spell, you may gain 1 life.').
card_mana_cost('dragon\'s claw', ['2']).
card_cmc('dragon\'s claw', 2).
card_layout('dragon\'s claw', 'normal').

% Found in: KTK
card_name('dragon\'s eye savants', 'Dragon\'s Eye Savants').
card_type('dragon\'s eye savants', 'Creature — Human Wizard').
card_types('dragon\'s eye savants', ['Creature']).
card_subtypes('dragon\'s eye savants', ['Human', 'Wizard']).
card_colors('dragon\'s eye savants', ['U']).
card_text('dragon\'s eye savants', 'Morph—Reveal a blue card in your hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Dragon\'s Eye Savants is turned face up, look at target opponent\'s hand.').
card_mana_cost('dragon\'s eye savants', ['1', 'U']).
card_cmc('dragon\'s eye savants', 2).
card_layout('dragon\'s eye savants', 'normal').
card_power('dragon\'s eye savants', 0).
card_toughness('dragon\'s eye savants', 6).

% Found in: DTK
card_name('dragon\'s eye sentry', 'Dragon\'s Eye Sentry').
card_type('dragon\'s eye sentry', 'Creature — Human Monk').
card_types('dragon\'s eye sentry', ['Creature']).
card_subtypes('dragon\'s eye sentry', ['Human', 'Monk']).
card_colors('dragon\'s eye sentry', ['W']).
card_text('dragon\'s eye sentry', 'Defender, first strike').
card_mana_cost('dragon\'s eye sentry', ['W']).
card_cmc('dragon\'s eye sentry', 1).
card_layout('dragon\'s eye sentry', 'normal').
card_power('dragon\'s eye sentry', 1).
card_toughness('dragon\'s eye sentry', 3).

% Found in: ALA
card_name('dragon\'s herald', 'Dragon\'s Herald').
card_type('dragon\'s herald', 'Creature — Goblin Shaman').
card_types('dragon\'s herald', ['Creature']).
card_subtypes('dragon\'s herald', ['Goblin', 'Shaman']).
card_colors('dragon\'s herald', ['R']).
card_text('dragon\'s herald', '{2}{R}, {T}, Sacrifice a black creature, a red creature, and a green creature: Search your library for a card named Hellkite Overlord and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('dragon\'s herald', ['R']).
card_cmc('dragon\'s herald', 1).
card_layout('dragon\'s herald', 'normal').
card_power('dragon\'s herald', 1).
card_toughness('dragon\'s herald', 1).

% Found in: DTK
card_name('dragon-scarred bear', 'Dragon-Scarred Bear').
card_type('dragon-scarred bear', 'Creature — Bear').
card_types('dragon-scarred bear', ['Creature']).
card_subtypes('dragon-scarred bear', ['Bear']).
card_colors('dragon-scarred bear', ['G']).
card_text('dragon-scarred bear', 'Formidable — {1}{G}: Regenerate Dragon-Scarred Bear. Activate this ability only if creatures you control have total power 8 or greater.').
card_mana_cost('dragon-scarred bear', ['2', 'G']).
card_cmc('dragon-scarred bear', 3).
card_layout('dragon-scarred bear', 'normal').
card_power('dragon-scarred bear', 3).
card_toughness('dragon-scarred bear', 2).

% Found in: KTK, pPRE
card_name('dragon-style twins', 'Dragon-Style Twins').
card_type('dragon-style twins', 'Creature — Human Monk').
card_types('dragon-style twins', ['Creature']).
card_subtypes('dragon-style twins', ['Human', 'Monk']).
card_colors('dragon-style twins', ['R']).
card_text('dragon-style twins', 'Double strike\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_mana_cost('dragon-style twins', ['3', 'R', 'R']).
card_cmc('dragon-style twins', 5).
card_layout('dragon-style twins', 'normal').
card_power('dragon-style twins', 3).
card_toughness('dragon-style twins', 3).

% Found in: CM1, PC2
card_name('dragonlair spider', 'Dragonlair Spider').
card_type('dragonlair spider', 'Creature — Spider').
card_types('dragonlair spider', ['Creature']).
card_subtypes('dragonlair spider', ['Spider']).
card_colors('dragonlair spider', ['R', 'G']).
card_text('dragonlair spider', 'Reach\nWhenever an opponent casts a spell, put a 1/1 green Insect creature token onto the battlefield.').
card_mana_cost('dragonlair spider', ['2', 'R', 'R', 'G', 'G']).
card_cmc('dragonlair spider', 6).
card_layout('dragonlair spider', 'normal').
card_power('dragonlair spider', 5).
card_toughness('dragonlair spider', 6).

% Found in: DTK
card_name('dragonloft idol', 'Dragonloft Idol').
card_type('dragonloft idol', 'Artifact Creature — Gargoyle').
card_types('dragonloft idol', ['Artifact', 'Creature']).
card_subtypes('dragonloft idol', ['Gargoyle']).
card_colors('dragonloft idol', []).
card_text('dragonloft idol', 'As long as you control a Dragon, Dragonloft Idol gets +1/+1 and has flying and trample.').
card_mana_cost('dragonloft idol', ['4']).
card_cmc('dragonloft idol', 4).
card_layout('dragonloft idol', 'normal').
card_power('dragonloft idol', 3).
card_toughness('dragonloft idol', 3).

% Found in: DTK
card_name('dragonlord atarka', 'Dragonlord Atarka').
card_type('dragonlord atarka', 'Legendary Creature — Elder Dragon').
card_types('dragonlord atarka', ['Creature']).
card_subtypes('dragonlord atarka', ['Elder', 'Dragon']).
card_supertypes('dragonlord atarka', ['Legendary']).
card_colors('dragonlord atarka', ['R', 'G']).
card_text('dragonlord atarka', 'Flying, trample\nWhen Dragonlord Atarka enters the battlefield, it deals 5 damage divided as you choose among any number of target creatures and/or planeswalkers your opponents control.').
card_mana_cost('dragonlord atarka', ['5', 'R', 'G']).
card_cmc('dragonlord atarka', 7).
card_layout('dragonlord atarka', 'normal').
card_power('dragonlord atarka', 8).
card_toughness('dragonlord atarka', 8).

% Found in: DTK
card_name('dragonlord dromoka', 'Dragonlord Dromoka').
card_type('dragonlord dromoka', 'Legendary Creature — Elder Dragon').
card_types('dragonlord dromoka', ['Creature']).
card_subtypes('dragonlord dromoka', ['Elder', 'Dragon']).
card_supertypes('dragonlord dromoka', ['Legendary']).
card_colors('dragonlord dromoka', ['W', 'G']).
card_text('dragonlord dromoka', 'Dragonlord Dromoka can\'t be countered.\nFlying, lifelink\nYour opponents can\'t cast spells during your turn.').
card_mana_cost('dragonlord dromoka', ['4', 'G', 'W']).
card_cmc('dragonlord dromoka', 6).
card_layout('dragonlord dromoka', 'normal').
card_power('dragonlord dromoka', 5).
card_toughness('dragonlord dromoka', 7).

% Found in: DTK
card_name('dragonlord kolaghan', 'Dragonlord Kolaghan').
card_type('dragonlord kolaghan', 'Legendary Creature — Elder Dragon').
card_types('dragonlord kolaghan', ['Creature']).
card_subtypes('dragonlord kolaghan', ['Elder', 'Dragon']).
card_supertypes('dragonlord kolaghan', ['Legendary']).
card_colors('dragonlord kolaghan', ['B', 'R']).
card_text('dragonlord kolaghan', 'Flying, haste\nOther creatures you control have haste.\nWhenever an opponent casts a creature or planeswalker spell with the same name as a card in his or her graveyard, that player loses 10 life.').
card_mana_cost('dragonlord kolaghan', ['4', 'B', 'R']).
card_cmc('dragonlord kolaghan', 6).
card_layout('dragonlord kolaghan', 'normal').
card_power('dragonlord kolaghan', 6).
card_toughness('dragonlord kolaghan', 5).

% Found in: DTK
card_name('dragonlord ojutai', 'Dragonlord Ojutai').
card_type('dragonlord ojutai', 'Legendary Creature — Elder Dragon').
card_types('dragonlord ojutai', ['Creature']).
card_subtypes('dragonlord ojutai', ['Elder', 'Dragon']).
card_supertypes('dragonlord ojutai', ['Legendary']).
card_colors('dragonlord ojutai', ['W', 'U']).
card_text('dragonlord ojutai', 'Flying\nDragonlord Ojutai has hexproof as long as it\'s untapped.\nWhenever Dragonlord Ojutai deals combat damage to a player, look at the top three cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('dragonlord ojutai', ['3', 'W', 'U']).
card_cmc('dragonlord ojutai', 5).
card_layout('dragonlord ojutai', 'normal').
card_power('dragonlord ojutai', 5).
card_toughness('dragonlord ojutai', 4).

% Found in: DTK
card_name('dragonlord silumgar', 'Dragonlord Silumgar').
card_type('dragonlord silumgar', 'Legendary Creature — Elder Dragon').
card_types('dragonlord silumgar', ['Creature']).
card_subtypes('dragonlord silumgar', ['Elder', 'Dragon']).
card_supertypes('dragonlord silumgar', ['Legendary']).
card_colors('dragonlord silumgar', ['U', 'B']).
card_text('dragonlord silumgar', 'Flying, deathtouch\nWhen Dragonlord Silumgar enters the battlefield, gain control of target creature or planeswalker for as long as you control Dragonlord Silumgar.').
card_mana_cost('dragonlord silumgar', ['4', 'U', 'B']).
card_cmc('dragonlord silumgar', 6).
card_layout('dragonlord silumgar', 'normal').
card_power('dragonlord silumgar', 3).
card_toughness('dragonlord silumgar', 5).

% Found in: DTK
card_name('dragonlord\'s prerogative', 'Dragonlord\'s Prerogative').
card_type('dragonlord\'s prerogative', 'Instant').
card_types('dragonlord\'s prerogative', ['Instant']).
card_subtypes('dragonlord\'s prerogative', []).
card_colors('dragonlord\'s prerogative', ['U']).
card_text('dragonlord\'s prerogative', 'As an additional cost to cast Dragonlord\'s Prerogative, you may reveal a Dragon card from your hand.\nIf you revealed a Dragon card or controlled a Dragon as you cast Dragonlord\'s Prerogative, Dragonlord\'s Prerogative can\'t be countered.\nDraw four cards.').
card_mana_cost('dragonlord\'s prerogative', ['4', 'U', 'U']).
card_cmc('dragonlord\'s prerogative', 6).
card_layout('dragonlord\'s prerogative', 'normal').

% Found in: DTK, pMEI
card_name('dragonlord\'s servant', 'Dragonlord\'s Servant').
card_type('dragonlord\'s servant', 'Creature — Goblin Shaman').
card_types('dragonlord\'s servant', ['Creature']).
card_subtypes('dragonlord\'s servant', ['Goblin', 'Shaman']).
card_colors('dragonlord\'s servant', ['R']).
card_text('dragonlord\'s servant', 'Dragon spells you cast cost {1} less to cast.').
card_mana_cost('dragonlord\'s servant', ['1', 'R']).
card_cmc('dragonlord\'s servant', 2).
card_layout('dragonlord\'s servant', 'normal').
card_power('dragonlord\'s servant', 1).
card_toughness('dragonlord\'s servant', 3).

% Found in: BFZ, WWK
card_name('dragonmaster outcast', 'Dragonmaster Outcast').
card_type('dragonmaster outcast', 'Creature — Human Shaman').
card_types('dragonmaster outcast', ['Creature']).
card_subtypes('dragonmaster outcast', ['Human', 'Shaman']).
card_colors('dragonmaster outcast', ['R']).
card_text('dragonmaster outcast', 'At the beginning of your upkeep, if you control six or more lands, put a 5/5 red Dragon creature token with flying onto the battlefield.').
card_mana_cost('dragonmaster outcast', ['R']).
card_cmc('dragonmaster outcast', 1).
card_layout('dragonmaster outcast', 'normal').
card_power('dragonmaster outcast', 1).
card_toughness('dragonmaster outcast', 1).

% Found in: FRF
card_name('dragonrage', 'Dragonrage').
card_type('dragonrage', 'Instant').
card_types('dragonrage', ['Instant']).
card_subtypes('dragonrage', []).
card_colors('dragonrage', ['R']).
card_text('dragonrage', 'Add {R} to your mana pool for each attacking creature you control. Until end of turn, attacking creatures you control gain \"{R}: This creature gets +1/+0 until end of turn.\"').
card_mana_cost('dragonrage', ['2', 'R']).
card_cmc('dragonrage', 3).
card_layout('dragonrage', 'normal').

% Found in: FRF_UGIN, KTK
card_name('dragonscale boon', 'Dragonscale Boon').
card_type('dragonscale boon', 'Instant').
card_types('dragonscale boon', ['Instant']).
card_subtypes('dragonscale boon', []).
card_colors('dragonscale boon', ['G']).
card_text('dragonscale boon', 'Put two +1/+1 counters on target creature and untap it.').
card_mana_cost('dragonscale boon', ['3', 'G']).
card_cmc('dragonscale boon', 4).
card_layout('dragonscale boon', 'normal').

% Found in: FRF, pMEI
card_name('dragonscale general', 'Dragonscale General').
card_type('dragonscale general', 'Creature — Human Warrior').
card_types('dragonscale general', ['Creature']).
card_subtypes('dragonscale general', ['Human', 'Warrior']).
card_colors('dragonscale general', ['W']).
card_text('dragonscale general', 'At the beginning of your end step, bolster X, where X is the number of tapped creatures you control. (Choose a creature with the least toughness among creatures you control and put X +1/+1 counters on it.)').
card_mana_cost('dragonscale general', ['3', 'W']).
card_cmc('dragonscale general', 4).
card_layout('dragonscale general', 'normal').
card_power('dragonscale general', 2).
card_toughness('dragonscale general', 3).

% Found in: DGM
card_name('dragonshift', 'Dragonshift').
card_type('dragonshift', 'Instant').
card_types('dragonshift', ['Instant']).
card_subtypes('dragonshift', []).
card_colors('dragonshift', ['U', 'R']).
card_text('dragonshift', 'Until end of turn, target creature you control becomes a blue and red Dragon with base power and toughness 4/4, loses all abilities, and gains flying.\nOverload {3}{U}{U}{R}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_mana_cost('dragonshift', ['1', 'U', 'R']).
card_cmc('dragonshift', 3).
card_layout('dragonshift', 'normal').

% Found in: M10, M11, M12, M13
card_name('dragonskull summit', 'Dragonskull Summit').
card_type('dragonskull summit', 'Land').
card_types('dragonskull summit', ['Land']).
card_subtypes('dragonskull summit', []).
card_colors('dragonskull summit', []).
card_text('dragonskull summit', 'Dragonskull Summit enters the battlefield tapped unless you control a Swamp or a Mountain.\n{T}: Add {B} or {R} to your mana pool.').
card_layout('dragonskull summit', 'normal').

% Found in: CON, MM2
card_name('dragonsoul knight', 'Dragonsoul Knight').
card_type('dragonsoul knight', 'Creature — Human Knight').
card_types('dragonsoul knight', ['Creature']).
card_subtypes('dragonsoul knight', ['Human', 'Knight']).
card_colors('dragonsoul knight', ['R']).
card_text('dragonsoul knight', 'First strike\n{W}{U}{B}{R}{G}: Until end of turn, Dragonsoul Knight becomes a Dragon, gets +5/+3, and gains flying and trample.').
card_mana_cost('dragonsoul knight', ['2', 'R']).
card_cmc('dragonsoul knight', 3).
card_layout('dragonsoul knight', 'normal').
card_power('dragonsoul knight', 2).
card_toughness('dragonsoul knight', 2).

% Found in: ARC, DDG, SCG
card_name('dragonspeaker shaman', 'Dragonspeaker Shaman').
card_type('dragonspeaker shaman', 'Creature — Human Barbarian Shaman').
card_types('dragonspeaker shaman', ['Creature']).
card_subtypes('dragonspeaker shaman', ['Human', 'Barbarian', 'Shaman']).
card_colors('dragonspeaker shaman', ['R']).
card_text('dragonspeaker shaman', 'Dragon spells you cast cost {2} less to cast.').
card_mana_cost('dragonspeaker shaman', ['1', 'R', 'R']).
card_cmc('dragonspeaker shaman', 3).
card_layout('dragonspeaker shaman', 'normal').
card_power('dragonspeaker shaman', 2).
card_toughness('dragonspeaker shaman', 2).

% Found in: SCG
card_name('dragonstalker', 'Dragonstalker').
card_type('dragonstalker', 'Creature — Bird Soldier').
card_types('dragonstalker', ['Creature']).
card_subtypes('dragonstalker', ['Bird', 'Soldier']).
card_colors('dragonstalker', ['W']).
card_text('dragonstalker', 'Flying, protection from Dragons').
card_mana_cost('dragonstalker', ['4', 'W']).
card_cmc('dragonstalker', 5).
card_layout('dragonstalker', 'normal').
card_power('dragonstalker', 3).
card_toughness('dragonstalker', 3).

% Found in: DRB, MMA, SCG, TSB
card_name('dragonstorm', 'Dragonstorm').
card_type('dragonstorm', 'Sorcery').
card_types('dragonstorm', ['Sorcery']).
card_subtypes('dragonstorm', []).
card_colors('dragonstorm', ['R']).
card_text('dragonstorm', 'Search your library for a Dragon permanent card and put it onto the battlefield. Then shuffle your library.\nStorm (When you cast this spell, copy it for each spell cast before it this turn.)').
card_mana_cost('dragonstorm', ['8', 'R']).
card_cmc('dragonstorm', 9).
card_layout('dragonstorm', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, BTD, CED, CEI, LEA, LEB, MIR, pFNM
card_name('drain life', 'Drain Life').
card_type('drain life', 'Sorcery').
card_types('drain life', ['Sorcery']).
card_subtypes('drain life', []).
card_colors('drain life', ['B']).
card_text('drain life', 'Spend only black mana on X.\nDrain Life deals X damage to target creature or player. You gain life equal to the damage dealt, but not more life than the player\'s life total before Drain Life dealt damage or the creature\'s toughness.').
card_mana_cost('drain life', ['X', '1', 'B']).
card_cmc('drain life', 2).
card_layout('drain life', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, ME4
card_name('drain power', 'Drain Power').
card_type('drain power', 'Sorcery').
card_types('drain power', ['Sorcery']).
card_subtypes('drain power', []).
card_colors('drain power', ['U']).
card_text('drain power', 'Target player activates a mana ability of each land he or she controls. Then put all mana from that player\'s mana pool into yours.').
card_mana_cost('drain power', ['U', 'U']).
card_cmc('drain power', 2).
card_layout('drain power', 'normal').

% Found in: EVE
card_name('drain the well', 'Drain the Well').
card_type('drain the well', 'Sorcery').
card_types('drain the well', ['Sorcery']).
card_subtypes('drain the well', []).
card_colors('drain the well', ['B', 'G']).
card_text('drain the well', 'Destroy target land. You gain 2 life.').
card_mana_cost('drain the well', ['2', 'B/G', 'B/G']).
card_cmc('drain the well', 4).
card_layout('drain the well', 'normal').

% Found in: TSP
card_name('draining whelk', 'Draining Whelk').
card_type('draining whelk', 'Creature — Illusion').
card_types('draining whelk', ['Creature']).
card_subtypes('draining whelk', ['Illusion']).
card_colors('draining whelk', ['U']).
card_text('draining whelk', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nWhen Draining Whelk enters the battlefield, counter target spell. Put X +1/+1 counters on Draining Whelk, where X is that spell\'s converted mana cost.').
card_mana_cost('draining whelk', ['4', 'U', 'U']).
card_cmc('draining whelk', 6).
card_layout('draining whelk', 'normal').
card_power('draining whelk', 1).
card_toughness('draining whelk', 1).

% Found in: RTR
card_name('drainpipe vermin', 'Drainpipe Vermin').
card_type('drainpipe vermin', 'Creature — Rat').
card_types('drainpipe vermin', ['Creature']).
card_subtypes('drainpipe vermin', ['Rat']).
card_colors('drainpipe vermin', ['B']).
card_text('drainpipe vermin', 'When Drainpipe Vermin dies, you may pay {B}. If you do, target player discards a card.').
card_mana_cost('drainpipe vermin', ['B']).
card_cmc('drainpipe vermin', 1).
card_layout('drainpipe vermin', 'normal').
card_power('drainpipe vermin', 1).
card_toughness('drainpipe vermin', 1).

% Found in: RAV
card_name('drake familiar', 'Drake Familiar').
card_type('drake familiar', 'Creature — Drake').
card_types('drake familiar', ['Creature']).
card_subtypes('drake familiar', ['Drake']).
card_colors('drake familiar', ['U']).
card_text('drake familiar', 'Flying\nWhen Drake Familiar enters the battlefield, sacrifice it unless you return an enchantment to its owner\'s hand.').
card_mana_cost('drake familiar', ['1', 'U']).
card_cmc('drake familiar', 2).
card_layout('drake familiar', 'normal').
card_power('drake familiar', 2).
card_toughness('drake familiar', 1).

% Found in: MMQ
card_name('drake hatchling', 'Drake Hatchling').
card_type('drake hatchling', 'Creature — Drake').
card_types('drake hatchling', ['Creature']).
card_subtypes('drake hatchling', ['Drake']).
card_colors('drake hatchling', ['U']).
card_text('drake hatchling', 'Flying\n{U}: Drake Hatchling gets +1/+0 until end of turn. Activate this ability only once each turn.').
card_mana_cost('drake hatchling', ['2', 'U']).
card_cmc('drake hatchling', 3).
card_layout('drake hatchling', 'normal').
card_power('drake hatchling', 1).
card_toughness('drake hatchling', 3).

% Found in: ROE
card_name('drake umbra', 'Drake Umbra').
card_type('drake umbra', 'Enchantment — Aura').
card_types('drake umbra', ['Enchantment']).
card_subtypes('drake umbra', ['Aura']).
card_colors('drake umbra', ['U']).
card_text('drake umbra', 'Enchant creature\nEnchanted creature gets +3/+3 and has flying.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_mana_cost('drake umbra', ['4', 'U']).
card_cmc('drake umbra', 5).
card_layout('drake umbra', 'normal').

% Found in: INV
card_name('drake-skull cameo', 'Drake-Skull Cameo').
card_type('drake-skull cameo', 'Artifact').
card_types('drake-skull cameo', ['Artifact']).
card_subtypes('drake-skull cameo', []).
card_colors('drake-skull cameo', []).
card_text('drake-skull cameo', '{T}: Add {U} or {B} to your mana pool.').
card_mana_cost('drake-skull cameo', ['3']).
card_cmc('drake-skull cameo', 3).
card_layout('drake-skull cameo', 'normal').

% Found in: CNS, VMA
card_name('drakestown forgotten', 'Drakestown Forgotten').
card_type('drakestown forgotten', 'Creature — Zombie').
card_types('drakestown forgotten', ['Creature']).
card_subtypes('drakestown forgotten', ['Zombie']).
card_colors('drakestown forgotten', ['B']).
card_text('drakestown forgotten', 'Drakestown Forgotten enters the battlefield with X +1/+1 counters on it, where X is the number of creature cards in all graveyards.\n{2}{B}, Remove a +1/+1 counter from Drakestown Forgotten: Target creature gets -1/-1 until end of turn.').
card_mana_cost('drakestown forgotten', ['4', 'B']).
card_cmc('drakestown forgotten', 5).
card_layout('drakestown forgotten', 'normal').
card_power('drakestown forgotten', 0).
card_toughness('drakestown forgotten', 0).

% Found in: GTC
card_name('drakewing krasis', 'Drakewing Krasis').
card_type('drakewing krasis', 'Creature — Lizard Drake').
card_types('drakewing krasis', ['Creature']).
card_subtypes('drakewing krasis', ['Lizard', 'Drake']).
card_colors('drakewing krasis', ['U', 'G']).
card_text('drakewing krasis', 'Flying, trample').
card_mana_cost('drakewing krasis', ['1', 'G', 'U']).
card_cmc('drakewing krasis', 3).
card_layout('drakewing krasis', 'normal').
card_power('drakewing krasis', 3).
card_toughness('drakewing krasis', 1).

% Found in: PLS
card_name('dralnu\'s crusade', 'Dralnu\'s Crusade').
card_type('dralnu\'s crusade', 'Enchantment').
card_types('dralnu\'s crusade', ['Enchantment']).
card_subtypes('dralnu\'s crusade', []).
card_colors('dralnu\'s crusade', ['B', 'R']).
card_text('dralnu\'s crusade', 'Goblin creatures get +1/+1.\nAll Goblins are black and are Zombies in addition to their other creature types.').
card_mana_cost('dralnu\'s crusade', ['1', 'B', 'R']).
card_cmc('dralnu\'s crusade', 3).
card_layout('dralnu\'s crusade', 'normal').

% Found in: PLS
card_name('dralnu\'s pet', 'Dralnu\'s Pet').
card_type('dralnu\'s pet', 'Creature — Shapeshifter').
card_types('dralnu\'s pet', ['Creature']).
card_subtypes('dralnu\'s pet', ['Shapeshifter']).
card_colors('dralnu\'s pet', ['U']).
card_text('dralnu\'s pet', 'Kicker—{2}{B}, Discard a creature card. (You may pay {2}{B} and discard a creature card in addition to any other costs as you cast this spell.)\nIf Dralnu\'s Pet was kicked, it enters the battlefield with flying and with X +1/+1 counters on it, where X is the discarded card\'s converted mana cost.').
card_mana_cost('dralnu\'s pet', ['1', 'U', 'U']).
card_cmc('dralnu\'s pet', 3).
card_layout('dralnu\'s pet', 'normal').
card_power('dralnu\'s pet', 2).
card_toughness('dralnu\'s pet', 2).

% Found in: TSP
card_name('dralnu, lich lord', 'Dralnu, Lich Lord').
card_type('dralnu, lich lord', 'Legendary Creature — Zombie Wizard').
card_types('dralnu, lich lord', ['Creature']).
card_subtypes('dralnu, lich lord', ['Zombie', 'Wizard']).
card_supertypes('dralnu, lich lord', ['Legendary']).
card_colors('dralnu, lich lord', ['U', 'B']).
card_text('dralnu, lich lord', 'If damage would be dealt to Dralnu, Lich Lord, sacrifice that many permanents instead.\n{T}: Target instant or sorcery card in your graveyard gains flashback until end of turn. The flashback cost is equal to its mana cost. (You may cast that card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('dralnu, lich lord', ['3', 'U', 'B']).
card_cmc('dralnu, lich lord', 5).
card_layout('dralnu, lich lord', 'normal').
card_power('dralnu, lich lord', 3).
card_toughness('dralnu, lich lord', 3).

% Found in: SHM
card_name('dramatic entrance', 'Dramatic Entrance').
card_type('dramatic entrance', 'Instant').
card_types('dramatic entrance', ['Instant']).
card_subtypes('dramatic entrance', []).
card_colors('dramatic entrance', ['G']).
card_text('dramatic entrance', 'You may put a green creature card from your hand onto the battlefield.').
card_mana_cost('dramatic entrance', ['3', 'G', 'G']).
card_cmc('dramatic entrance', 5).
card_layout('dramatic entrance', 'normal').

% Found in: RTR
card_name('dramatic rescue', 'Dramatic Rescue').
card_type('dramatic rescue', 'Instant').
card_types('dramatic rescue', ['Instant']).
card_subtypes('dramatic rescue', []).
card_colors('dramatic rescue', ['W', 'U']).
card_text('dramatic rescue', 'Return target creature to its owner\'s hand. You gain 2 life.').
card_mana_cost('dramatic rescue', ['W', 'U']).
card_cmc('dramatic rescue', 2).
card_layout('dramatic rescue', 'normal').

% Found in: BFZ
card_name('drana\'s emissary', 'Drana\'s Emissary').
card_type('drana\'s emissary', 'Creature — Vampire Cleric Ally').
card_types('drana\'s emissary', ['Creature']).
card_subtypes('drana\'s emissary', ['Vampire', 'Cleric', 'Ally']).
card_colors('drana\'s emissary', ['W', 'B']).
card_text('drana\'s emissary', 'Flying\nAt the beginning of your upkeep, each opponent loses 1 life and you gain 1 life.').
card_mana_cost('drana\'s emissary', ['1', 'W', 'B']).
card_cmc('drana\'s emissary', 3).
card_layout('drana\'s emissary', 'normal').
card_power('drana\'s emissary', 2).
card_toughness('drana\'s emissary', 2).

% Found in: C14, ROE
card_name('drana, kalastria bloodchief', 'Drana, Kalastria Bloodchief').
card_type('drana, kalastria bloodchief', 'Legendary Creature — Vampire Shaman').
card_types('drana, kalastria bloodchief', ['Creature']).
card_subtypes('drana, kalastria bloodchief', ['Vampire', 'Shaman']).
card_supertypes('drana, kalastria bloodchief', ['Legendary']).
card_colors('drana, kalastria bloodchief', ['B']).
card_text('drana, kalastria bloodchief', 'Flying\n{X}{B}{B}: Target creature gets -0/-X until end of turn and Drana, Kalastria Bloodchief gets +X/+0 until end of turn.').
card_mana_cost('drana, kalastria bloodchief', ['3', 'B', 'B']).
card_cmc('drana, kalastria bloodchief', 5).
card_layout('drana, kalastria bloodchief', 'normal').
card_power('drana, kalastria bloodchief', 4).
card_toughness('drana, kalastria bloodchief', 4).

% Found in: BFZ
card_name('drana, liberator of malakir', 'Drana, Liberator of Malakir').
card_type('drana, liberator of malakir', 'Legendary Creature — Vampire Ally').
card_types('drana, liberator of malakir', ['Creature']).
card_subtypes('drana, liberator of malakir', ['Vampire', 'Ally']).
card_supertypes('drana, liberator of malakir', ['Legendary']).
card_colors('drana, liberator of malakir', ['B']).
card_text('drana, liberator of malakir', 'Flying, first strike\nWhenever Drana, Liberator of Malakir deals combat damage to a player, put a +1/+1 counter on each attacking creature you control.').
card_mana_cost('drana, liberator of malakir', ['1', 'B', 'B']).
card_cmc('drana, liberator of malakir', 3).
card_layout('drana, liberator of malakir', 'normal').
card_power('drana, liberator of malakir', 2).
card_toughness('drana, liberator of malakir', 3).

% Found in: ARB
card_name('drastic revelation', 'Drastic Revelation').
card_type('drastic revelation', 'Sorcery').
card_types('drastic revelation', ['Sorcery']).
card_subtypes('drastic revelation', []).
card_colors('drastic revelation', ['U', 'B', 'R']).
card_text('drastic revelation', 'Discard your hand. Draw seven cards, then discard three cards at random.').
card_mana_cost('drastic revelation', ['2', 'U', 'B', 'R']).
card_cmc('drastic revelation', 5).
card_layout('drastic revelation', 'normal').

% Found in: UNH
card_name('drawn together', 'Drawn Together').
card_type('drawn together', 'Enchantment').
card_types('drawn together', ['Enchantment']).
card_subtypes('drawn together', []).
card_colors('drawn together', ['W']).
card_text('drawn together', 'As Drawn Together comes into play, choose an artist.\nCreatures by the chosen artist get +2/+2.').
card_mana_cost('drawn together', ['2', 'W', 'W']).
card_cmc('drawn together', 4).
card_layout('drawn together', 'normal').

% Found in: LRW
card_name('dread', 'Dread').
card_type('dread', 'Creature — Elemental Incarnation').
card_types('dread', ['Creature']).
card_subtypes('dread', ['Elemental', 'Incarnation']).
card_colors('dread', ['B']).
card_text('dread', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nWhenever a creature deals damage to you, destroy it.\nWhen Dread is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_mana_cost('dread', ['3', 'B', 'B', 'B']).
card_cmc('dread', 6).
card_layout('dread', 'normal').
card_power('dread', 6).
card_toughness('dread', 6).

% Found in: CMD
card_name('dread cacodemon', 'Dread Cacodemon').
card_type('dread cacodemon', 'Creature — Demon').
card_types('dread cacodemon', ['Creature']).
card_subtypes('dread cacodemon', ['Demon']).
card_colors('dread cacodemon', ['B']).
card_text('dread cacodemon', 'When Dread Cacodemon enters the battlefield, if you cast it from your hand, destroy all creatures your opponents control, then tap all other creatures you control.').
card_mana_cost('dread cacodemon', ['7', 'B', 'B', 'B']).
card_cmc('dread cacodemon', 10).
card_layout('dread cacodemon', 'normal').
card_power('dread cacodemon', 8).
card_toughness('dread cacodemon', 8).

% Found in: POR
card_name('dread charge', 'Dread Charge').
card_type('dread charge', 'Sorcery').
card_types('dread charge', ['Sorcery']).
card_subtypes('dread charge', []).
card_colors('dread charge', ['B']).
card_text('dread charge', 'Black creatures you control can\'t be blocked this turn except by black creatures.').
card_mana_cost('dread charge', ['3', 'B']).
card_cmc('dread charge', 4).
card_layout('dread charge', 'normal').

% Found in: MM2, ROE
card_name('dread drone', 'Dread Drone').
card_type('dread drone', 'Creature — Eldrazi Drone').
card_types('dread drone', ['Creature']).
card_subtypes('dread drone', ['Eldrazi', 'Drone']).
card_colors('dread drone', ['B']).
card_text('dread drone', 'When Dread Drone enters the battlefield, put two 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('dread drone', ['4', 'B']).
card_cmc('dread drone', 5).
card_layout('dread drone', 'normal').
card_power('dread drone', 4).
card_toughness('dread drone', 1).

% Found in: 6ED, TMP
card_name('dread of night', 'Dread of Night').
card_type('dread of night', 'Enchantment').
card_types('dread of night', ['Enchantment']).
card_subtypes('dread of night', []).
card_colors('dread of night', ['B']).
card_text('dread of night', 'White creatures get -1/-1.').
card_mana_cost('dread of night', ['B']).
card_cmc('dread of night', 1).
card_layout('dread of night', 'normal').

% Found in: ME4, POR, S99
card_name('dread reaper', 'Dread Reaper').
card_type('dread reaper', 'Creature — Horror').
card_types('dread reaper', ['Creature']).
card_subtypes('dread reaper', ['Horror']).
card_colors('dread reaper', ['B']).
card_text('dread reaper', 'Flying\nWhen Dread Reaper enters the battlefield, you lose 5 life.').
card_mana_cost('dread reaper', ['3', 'B', 'B', 'B']).
card_cmc('dread reaper', 6).
card_layout('dread reaper', 'normal').
card_power('dread reaper', 6).
card_toughness('dread reaper', 5).

% Found in: C14, PD3, TSP
card_name('dread return', 'Dread Return').
card_type('dread return', 'Sorcery').
card_types('dread return', ['Sorcery']).
card_subtypes('dread return', []).
card_colors('dread return', ['B']).
card_text('dread return', 'Return target creature card from your graveyard to the battlefield.\nFlashback—Sacrifice three creatures. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('dread return', ['2', 'B', 'B']).
card_cmc('dread return', 4).
card_layout('dread return', 'normal').

% Found in: DIS
card_name('dread slag', 'Dread Slag').
card_type('dread slag', 'Creature — Horror').
card_types('dread slag', ['Creature']).
card_subtypes('dread slag', ['Horror']).
card_colors('dread slag', ['B', 'R']).
card_text('dread slag', 'Trample\nDread Slag gets -4/-4 for each card in your hand.').
card_mana_cost('dread slag', ['3', 'B', 'R']).
card_cmc('dread slag', 5).
card_layout('dread slag', 'normal').
card_power('dread slag', 9).
card_toughness('dread slag', 9).

% Found in: AVR
card_name('dread slaver', 'Dread Slaver').
card_type('dread slaver', 'Creature — Zombie Horror').
card_types('dread slaver', ['Creature']).
card_subtypes('dread slaver', ['Zombie', 'Horror']).
card_colors('dread slaver', ['B']).
card_text('dread slaver', 'Whenever a creature dealt damage by Dread Slaver this turn dies, return it to the battlefield under your control. That creature is a black Zombie in addition to its other colors and types.').
card_mana_cost('dread slaver', ['3', 'B', 'B']).
card_cmc('dread slaver', 5).
card_layout('dread slaver', 'normal').
card_power('dread slaver', 3).
card_toughness('dread slaver', 5).

% Found in: MIR
card_name('dread specter', 'Dread Specter').
card_type('dread specter', 'Creature — Specter').
card_types('dread specter', ['Creature']).
card_subtypes('dread specter', ['Specter']).
card_colors('dread specter', ['B']).
card_text('dread specter', 'Whenever Dread Specter blocks or becomes blocked by a nonblack creature, destroy that creature at end of combat.').
card_mana_cost('dread specter', ['3', 'B']).
card_cmc('dread specter', 4).
card_layout('dread specter', 'normal').
card_power('dread specter', 2).
card_toughness('dread specter', 2).

% Found in: DDM, WWK
card_name('dread statuary', 'Dread Statuary').
card_type('dread statuary', 'Land').
card_types('dread statuary', ['Land']).
card_subtypes('dread statuary', []).
card_colors('dread statuary', []).
card_text('dread statuary', '{T}: Add {1} to your mana pool.\n{4}: Dread Statuary becomes a 4/2 Golem artifact creature until end of turn. It\'s still a land.').
card_layout('dread statuary', 'normal').

% Found in: M10
card_name('dread warlock', 'Dread Warlock').
card_type('dread warlock', 'Creature — Human Wizard').
card_types('dread warlock', ['Creature']).
card_subtypes('dread warlock', ['Human', 'Wizard']).
card_colors('dread warlock', ['B']).
card_text('dread warlock', 'Dread Warlock can\'t be blocked except by black creatures.').
card_mana_cost('dread warlock', ['1', 'B', 'B']).
card_cmc('dread warlock', 3).
card_layout('dread warlock', 'normal').
card_power('dread warlock', 2).
card_toughness('dread warlock', 2).

% Found in: ICE, ME4
card_name('dread wight', 'Dread Wight').
card_type('dread wight', 'Creature — Zombie').
card_types('dread wight', ['Creature']).
card_subtypes('dread wight', ['Zombie']).
card_colors('dread wight', ['B']).
card_text('dread wight', 'At end of combat, put a paralyzation counter on each creature blocking or blocked by Dread Wight and tap those creatures. Each of those creatures doesn\'t untap during its controller\'s untap step for as long as it has a paralyzation counter on it. Each of those creatures gains \"{4}: Remove a paralyzation counter from this creature.\"').
card_mana_cost('dread wight', ['3', 'B', 'B']).
card_cmc('dread wight', 5).
card_layout('dread wight', 'normal').
card_power('dread wight', 3).
card_toughness('dread wight', 4).

% Found in: RTR
card_name('dreadbore', 'Dreadbore').
card_type('dreadbore', 'Sorcery').
card_types('dreadbore', ['Sorcery']).
card_subtypes('dreadbore', []).
card_colors('dreadbore', ['B', 'R']).
card_text('dreadbore', 'Destroy target creature or planeswalker.').
card_mana_cost('dreadbore', ['B', 'R']).
card_cmc('dreadbore', 2).
card_layout('dreadbore', 'normal').

% Found in: JOU
card_name('dreadbringer lampads', 'Dreadbringer Lampads').
card_type('dreadbringer lampads', 'Enchantment Creature — Nymph').
card_types('dreadbringer lampads', ['Enchantment', 'Creature']).
card_subtypes('dreadbringer lampads', ['Nymph']).
card_colors('dreadbringer lampads', ['B']).
card_text('dreadbringer lampads', 'Constellation — Whenever Dreadbringer Lampads or another enchantment enters the battlefield under your control, target creature gains intimidate until end of turn. (It can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_mana_cost('dreadbringer lampads', ['4', 'B']).
card_cmc('dreadbringer lampads', 5).
card_layout('dreadbringer lampads', 'normal').
card_power('dreadbringer lampads', 4).
card_toughness('dreadbringer lampads', 2).

% Found in: CMD, TSP
card_name('dreadship reef', 'Dreadship Reef').
card_type('dreadship reef', 'Land').
card_types('dreadship reef', ['Land']).
card_subtypes('dreadship reef', []).
card_colors('dreadship reef', []).
card_text('dreadship reef', '{T}: Add {1} to your mana pool.\n{1}, {T}: Put a storage counter on Dreadship Reef.\n{1}, Remove X storage counters from Dreadship Reef: Add X mana in any combination of {U} and/or {B} to your mana pool.').
card_layout('dreadship reef', 'normal').

% Found in: AVR, ORI
card_name('dreadwaters', 'Dreadwaters').
card_type('dreadwaters', 'Sorcery').
card_types('dreadwaters', ['Sorcery']).
card_subtypes('dreadwaters', []).
card_colors('dreadwaters', ['U']).
card_text('dreadwaters', 'Target player puts the top X cards of his or her library into his or her graveyard, where X is the number of lands you control.').
card_mana_cost('dreadwaters', ['3', 'U']).
card_cmc('dreadwaters', 4).
card_layout('dreadwaters', 'normal').

% Found in: CON
card_name('dreadwing', 'Dreadwing').
card_type('dreadwing', 'Creature — Zombie').
card_types('dreadwing', ['Creature']).
card_subtypes('dreadwing', ['Zombie']).
card_colors('dreadwing', ['B']).
card_text('dreadwing', '{1}{U}{R}: Dreadwing gets +3/+0 and gains flying until end of turn.').
card_mana_cost('dreadwing', ['B']).
card_cmc('dreadwing', 1).
card_layout('dreadwing', 'normal').
card_power('dreadwing', 1).
card_toughness('dreadwing', 1).

% Found in: 6ED, MIR, TMP
card_name('dream cache', 'Dream Cache').
card_type('dream cache', 'Sorcery').
card_types('dream cache', ['Sorcery']).
card_subtypes('dream cache', []).
card_colors('dream cache', ['U']).
card_text('dream cache', 'Draw three cards, then put two cards from your hand both on top of your library or both on the bottom of your library.').
card_mana_cost('dream cache', ['2', 'U']).
card_cmc('dream cache', 3).
card_layout('dream cache', 'normal').

% Found in: ONS
card_name('dream chisel', 'Dream Chisel').
card_type('dream chisel', 'Artifact').
card_types('dream chisel', ['Artifact']).
card_subtypes('dream chisel', []).
card_colors('dream chisel', []).
card_text('dream chisel', 'Face-down creature spells you cast cost {1} less to cast.').
card_mana_cost('dream chisel', ['2']).
card_cmc('dream chisel', 2).
card_layout('dream chisel', 'normal').

% Found in: LEG
card_name('dream coat', 'Dream Coat').
card_type('dream coat', 'Enchantment — Aura').
card_types('dream coat', ['Enchantment']).
card_subtypes('dream coat', ['Aura']).
card_colors('dream coat', ['U']).
card_text('dream coat', 'Enchant creature\n{0}: Enchanted creature becomes the color or colors of your choice. Activate this ability only once each turn.').
card_mana_cost('dream coat', ['U']).
card_cmc('dream coat', 1).
card_layout('dream coat', 'normal').

% Found in: MIR
card_name('dream fighter', 'Dream Fighter').
card_type('dream fighter', 'Creature — Human Soldier').
card_types('dream fighter', ['Creature']).
card_subtypes('dream fighter', ['Human', 'Soldier']).
card_colors('dream fighter', ['U']).
card_text('dream fighter', 'Whenever Dream Fighter blocks or becomes blocked by a creature, Dream Fighter and that creature phase out. (While they\'re phased out, they\'re treated as though they don\'t exist. Each one phases in before its controller untaps during his or her next untap step.)').
card_mana_cost('dream fighter', ['2', 'U']).
card_cmc('dream fighter', 3).
card_layout('dream fighter', 'normal').
card_power('dream fighter', 1).
card_toughness('dream fighter', 1).

% Found in: CNS, EVE
card_name('dream fracture', 'Dream Fracture').
card_type('dream fracture', 'Instant').
card_types('dream fracture', ['Instant']).
card_subtypes('dream fracture', []).
card_colors('dream fracture', ['U']).
card_text('dream fracture', 'Counter target spell. Its controller draws a card.\nDraw a card.').
card_mana_cost('dream fracture', ['1', 'U', 'U']).
card_cmc('dream fracture', 3).
card_layout('dream fracture', 'normal').

% Found in: STH, TPR
card_name('dream halls', 'Dream Halls').
card_type('dream halls', 'Enchantment').
card_types('dream halls', ['Enchantment']).
card_subtypes('dream halls', []).
card_colors('dream halls', ['U']).
card_text('dream halls', 'Rather than pay the mana cost for a spell, its controller may discard a card that shares a color with that spell.').
card_mana_cost('dream halls', ['3', 'U', 'U']).
card_cmc('dream halls', 5).
card_layout('dream halls', 'normal').
card_reserved('dream halls').

% Found in: RAV
card_name('dream leash', 'Dream Leash').
card_type('dream leash', 'Enchantment — Aura').
card_types('dream leash', ['Enchantment']).
card_subtypes('dream leash', ['Aura']).
card_colors('dream leash', ['U']).
card_text('dream leash', 'Enchant permanent\nYou can\'t choose an untapped permanent as Dream Leash\'s target as you cast Dream Leash.\nYou control enchanted permanent.').
card_mana_cost('dream leash', ['3', 'U', 'U']).
card_cmc('dream leash', 5).
card_layout('dream leash', 'normal').

% Found in: 9ED, STH, TPR
card_name('dream prowler', 'Dream Prowler').
card_type('dream prowler', 'Creature — Illusion').
card_types('dream prowler', ['Creature']).
card_subtypes('dream prowler', ['Illusion']).
card_colors('dream prowler', ['U']).
card_text('dream prowler', 'Dream Prowler can\'t be blocked as long as it\'s attacking alone.').
card_mana_cost('dream prowler', ['2', 'U', 'U']).
card_cmc('dream prowler', 4).
card_layout('dream prowler', 'normal').
card_power('dream prowler', 1).
card_toughness('dream prowler', 5).

% Found in: SHM
card_name('dream salvage', 'Dream Salvage').
card_type('dream salvage', 'Instant').
card_types('dream salvage', ['Instant']).
card_subtypes('dream salvage', []).
card_colors('dream salvage', ['U', 'B']).
card_text('dream salvage', 'Draw cards equal to the number of cards target opponent discarded this turn.').
card_mana_cost('dream salvage', ['U/B']).
card_cmc('dream salvage', 1).
card_layout('dream salvage', 'normal').

% Found in: DDM, TSP
card_name('dream stalker', 'Dream Stalker').
card_type('dream stalker', 'Creature — Illusion').
card_types('dream stalker', ['Creature']).
card_subtypes('dream stalker', ['Illusion']).
card_colors('dream stalker', ['U']).
card_text('dream stalker', 'When Dream Stalker enters the battlefield, return a permanent you control to its owner\'s hand.').
card_mana_cost('dream stalker', ['1', 'U']).
card_cmc('dream stalker', 2).
card_layout('dream stalker', 'normal').
card_power('dream stalker', 1).
card_toughness('dream stalker', 5).

% Found in: EVE
card_name('dream thief', 'Dream Thief').
card_type('dream thief', 'Creature — Faerie Rogue').
card_types('dream thief', ['Creature']).
card_subtypes('dream thief', ['Faerie', 'Rogue']).
card_colors('dream thief', ['U']).
card_text('dream thief', 'Flying\nWhen Dream Thief enters the battlefield, draw a card if you\'ve cast another blue spell this turn.').
card_mana_cost('dream thief', ['2', 'U']).
card_cmc('dream thief', 3).
card_layout('dream thief', 'normal').
card_power('dream thief', 2).
card_toughness('dream thief', 1).

% Found in: INV
card_name('dream thrush', 'Dream Thrush').
card_type('dream thrush', 'Creature — Bird').
card_types('dream thrush', ['Creature']).
card_subtypes('dream thrush', ['Bird']).
card_colors('dream thrush', ['U']).
card_text('dream thrush', 'Flying\n{T}: Target land becomes the basic land type of your choice until end of turn.').
card_mana_cost('dream thrush', ['1', 'U']).
card_cmc('dream thrush', 2).
card_layout('dream thrush', 'normal').
card_power('dream thrush', 1).
card_toughness('dream thrush', 1).

% Found in: VIS
card_name('dream tides', 'Dream Tides').
card_type('dream tides', 'Enchantment').
card_types('dream tides', ['Enchantment']).
card_subtypes('dream tides', []).
card_colors('dream tides', ['U']).
card_text('dream tides', 'Creatures don\'t untap during their controllers\' untap steps.\nAt the beginning of each player\'s upkeep, that player may choose any number of tapped nongreen creatures he or she controls and pay {2} for each creature chosen this way. If the player does, untap those creatures.').
card_mana_cost('dream tides', ['2', 'U', 'U']).
card_cmc('dream tides', 4).
card_layout('dream tides', 'normal').

% Found in: ISD
card_name('dream twist', 'Dream Twist').
card_type('dream twist', 'Instant').
card_types('dream twist', ['Instant']).
card_subtypes('dream twist', []).
card_colors('dream twist', ['U']).
card_text('dream twist', 'Target player puts the top three cards of his or her library into his or her graveyard.\nFlashback {1}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('dream twist', ['U']).
card_cmc('dream twist', 1).
card_layout('dream twist', 'normal').

% Found in: MRD
card_name('dream\'s grip', 'Dream\'s Grip').
card_type('dream\'s grip', 'Instant').
card_types('dream\'s grip', ['Instant']).
card_subtypes('dream\'s grip', []).
card_colors('dream\'s grip', ['U']).
card_text('dream\'s grip', 'Choose one —\n• Tap target permanent.\n• Untap target permanent.\nEntwine {1} (Choose both if you pay the entwine cost.)').
card_mana_cost('dream\'s grip', ['U']).
card_cmc('dream\'s grip', 1).
card_layout('dream\'s grip', 'normal').

% Found in: 10E, CMD, LGN
card_name('dreamborn muse', 'Dreamborn Muse').
card_type('dreamborn muse', 'Creature — Spirit').
card_types('dreamborn muse', ['Creature']).
card_subtypes('dreamborn muse', ['Spirit']).
card_colors('dreamborn muse', ['U']).
card_text('dreamborn muse', 'At the beginning of each player\'s upkeep, that player puts the top X cards of his or her library into his or her graveyard, where X is the number of cards in his or her hand.').
card_mana_cost('dreamborn muse', ['2', 'U', 'U']).
card_cmc('dreamborn muse', 4).
card_layout('dreamborn muse', 'normal').
card_power('dreamborn muse', 2).
card_toughness('dreamborn muse', 2).

% Found in: SOK
card_name('dreamcatcher', 'Dreamcatcher').
card_type('dreamcatcher', 'Creature — Spirit').
card_types('dreamcatcher', ['Creature']).
card_subtypes('dreamcatcher', ['Spirit']).
card_colors('dreamcatcher', ['U']).
card_text('dreamcatcher', 'Whenever you cast a Spirit or Arcane spell, you may sacrifice Dreamcatcher. If you do, draw a card.').
card_mana_cost('dreamcatcher', ['U']).
card_cmc('dreamcatcher', 1).
card_layout('dreamcatcher', 'normal').
card_power('dreamcatcher', 1).
card_toughness('dreamcatcher', 1).

% Found in: PC2, VMA
card_name('dreampod druid', 'Dreampod Druid').
card_type('dreampod druid', 'Creature — Human Druid').
card_types('dreampod druid', ['Creature']).
card_subtypes('dreampod druid', ['Human', 'Druid']).
card_colors('dreampod druid', ['G']).
card_text('dreampod druid', 'At the beginning of each upkeep, if Dreampod Druid is enchanted, put a 1/1 green Saproling creature token onto the battlefield.').
card_mana_cost('dreampod druid', ['1', 'G']).
card_cmc('dreampod druid', 2).
card_layout('dreampod druid', 'normal').
card_power('dreampod druid', 2).
card_toughness('dreampod druid', 2).

% Found in: ICE, ME2
card_name('dreams of the dead', 'Dreams of the Dead').
card_type('dreams of the dead', 'Enchantment').
card_types('dreams of the dead', ['Enchantment']).
card_subtypes('dreams of the dead', []).
card_colors('dreams of the dead', ['U']).
card_text('dreams of the dead', '{1}{U}: Return target white or black creature card from your graveyard to the battlefield. That creature gains \"Cumulative upkeep {2}.\" If the creature would leave the battlefield, exile it instead of putting it anywhere else. (At the beginning of its controller\'s upkeep, that player puts an age counter on it, then sacrifices it unless he or she pays its upkeep cost for each age counter on it.)').
card_mana_cost('dreams of the dead', ['3', 'U']).
card_cmc('dreams of the dead', 4).
card_layout('dreams of the dead', 'normal').

% Found in: PLC
card_name('dreamscape artist', 'Dreamscape Artist').
card_type('dreamscape artist', 'Creature — Human Spellshaper').
card_types('dreamscape artist', ['Creature']).
card_subtypes('dreamscape artist', ['Human', 'Spellshaper']).
card_colors('dreamscape artist', ['U']).
card_text('dreamscape artist', '{2}{U}, {T}, Discard a card, Sacrifice a land: Search your library for up to two basic land cards and put them onto the battlefield. Then shuffle your library.').
card_mana_cost('dreamscape artist', ['1', 'U']).
card_cmc('dreamscape artist', 2).
card_layout('dreamscape artist', 'normal').
card_power('dreamscape artist', 1).
card_toughness('dreamscape artist', 1).

% Found in: LRW, MMA
card_name('dreamspoiler witches', 'Dreamspoiler Witches').
card_type('dreamspoiler witches', 'Creature — Faerie Wizard').
card_types('dreamspoiler witches', ['Creature']).
card_subtypes('dreamspoiler witches', ['Faerie', 'Wizard']).
card_colors('dreamspoiler witches', ['B']).
card_text('dreamspoiler witches', 'Flying\nWhenever you cast a spell during an opponent\'s turn, you may have target creature get -1/-1 until end of turn.').
card_mana_cost('dreamspoiler witches', ['3', 'B']).
card_cmc('dreamspoiler witches', 4).
card_layout('dreamspoiler witches', 'normal').
card_power('dreamspoiler witches', 2).
card_toughness('dreamspoiler witches', 2).

% Found in: ARC, C14, CMD, ROE
card_name('dreamstone hedron', 'Dreamstone Hedron').
card_type('dreamstone hedron', 'Artifact').
card_types('dreamstone hedron', ['Artifact']).
card_subtypes('dreamstone hedron', []).
card_colors('dreamstone hedron', []).
card_text('dreamstone hedron', '{T}: Add {3} to your mana pool.\n{3}, {T}, Sacrifice Dreamstone Hedron: Draw three cards.').
card_mana_cost('dreamstone hedron', ['6']).
card_cmc('dreamstone hedron', 6).
card_layout('dreamstone hedron', 'normal').

% Found in: ODY
card_name('dreamwinder', 'Dreamwinder').
card_type('dreamwinder', 'Creature — Serpent').
card_types('dreamwinder', ['Creature']).
card_subtypes('dreamwinder', ['Serpent']).
card_colors('dreamwinder', ['U']).
card_text('dreamwinder', 'Dreamwinder can\'t attack unless defending player controls an Island.\n{U}, Sacrifice an Island: Target land becomes an Island until end of turn.').
card_mana_cost('dreamwinder', ['3', 'U']).
card_cmc('dreamwinder', 4).
card_layout('dreamwinder', 'normal').
card_power('dreamwinder', 4).
card_toughness('dreamwinder', 3).

% Found in: INV
card_name('dredge', 'Dredge').
card_type('dredge', 'Instant').
card_types('dredge', ['Instant']).
card_subtypes('dredge', []).
card_colors('dredge', ['B']).
card_text('dredge', 'Sacrifice a creature or land.\nDraw a card.').
card_mana_cost('dredge', ['B']).
card_cmc('dredge', 1).
card_layout('dredge', 'normal').

% Found in: DDJ, RTR, pMEI
card_name('dreg mangler', 'Dreg Mangler').
card_type('dreg mangler', 'Creature — Plant Zombie').
card_types('dreg mangler', ['Creature']).
card_subtypes('dreg mangler', ['Plant', 'Zombie']).
card_colors('dreg mangler', ['B', 'G']).
card_text('dreg mangler', 'Haste\nScavenge {3}{B}{G} ({3}{B}{G}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_mana_cost('dreg mangler', ['1', 'B', 'G']).
card_cmc('dreg mangler', 3).
card_layout('dreg mangler', 'normal').
card_power('dreg mangler', 3).
card_toughness('dreg mangler', 3).

% Found in: ALA
card_name('dreg reaver', 'Dreg Reaver').
card_type('dreg reaver', 'Creature — Zombie Beast').
card_types('dreg reaver', ['Creature']).
card_subtypes('dreg reaver', ['Zombie', 'Beast']).
card_colors('dreg reaver', ['B']).
card_text('dreg reaver', '').
card_mana_cost('dreg reaver', ['4', 'B']).
card_cmc('dreg reaver', 5).
card_layout('dreg reaver', 'normal').
card_power('dreg reaver', 4).
card_toughness('dreg reaver', 3).

% Found in: 7ED, C14, TMP
card_name('dregs of sorrow', 'Dregs of Sorrow').
card_type('dregs of sorrow', 'Sorcery').
card_types('dregs of sorrow', ['Sorcery']).
card_subtypes('dregs of sorrow', []).
card_colors('dregs of sorrow', ['B']).
card_text('dregs of sorrow', 'Destroy X target nonblack creatures. Draw X cards.').
card_mana_cost('dregs of sorrow', ['X', '4', 'B']).
card_cmc('dregs of sorrow', 5).
card_layout('dregs of sorrow', 'normal').

% Found in: ALA, ARC, DDN, HOP
card_name('dregscape zombie', 'Dregscape Zombie').
card_type('dregscape zombie', 'Creature — Zombie').
card_types('dregscape zombie', ['Creature']).
card_subtypes('dregscape zombie', ['Zombie']).
card_colors('dregscape zombie', ['B']).
card_text('dregscape zombie', 'Unearth {B} ({B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('dregscape zombie', ['1', 'B']).
card_cmc('dregscape zombie', 2).
card_layout('dregscape zombie', 'normal').
card_power('dregscape zombie', 2).
card_toughness('dregscape zombie', 1).

% Found in: DIS
card_name('drekavac', 'Drekavac').
card_type('drekavac', 'Creature — Beast').
card_types('drekavac', ['Creature']).
card_subtypes('drekavac', ['Beast']).
card_colors('drekavac', ['B']).
card_text('drekavac', 'When Drekavac enters the battlefield, sacrifice it unless you discard a noncreature card.').
card_mana_cost('drekavac', ['1', 'B']).
card_cmc('drekavac', 2).
card_layout('drekavac', 'normal').
card_power('drekavac', 3).
card_toughness('drekavac', 3).

% Found in: CSP
card_name('drelnoch', 'Drelnoch').
card_type('drelnoch', 'Creature — Yeti Mutant').
card_types('drelnoch', ['Creature']).
card_subtypes('drelnoch', ['Yeti', 'Mutant']).
card_colors('drelnoch', ['U']).
card_text('drelnoch', 'Whenever Drelnoch becomes blocked, you may draw two cards.').
card_mana_cost('drelnoch', ['4', 'U']).
card_cmc('drelnoch', 5).
card_layout('drelnoch', 'normal').
card_power('drelnoch', 3).
card_toughness('drelnoch', 3).

% Found in: RAV
card_name('drift of phantasms', 'Drift of Phantasms').
card_type('drift of phantasms', 'Creature — Spirit').
card_types('drift of phantasms', ['Creature']).
card_subtypes('drift of phantasms', ['Spirit']).
card_colors('drift of phantasms', ['U']).
card_text('drift of phantasms', 'Defender (This creature can\'t attack.)\nFlying\nTransmute {1}{U}{U} ({1}{U}{U}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Transmute only as a sorcery.)').
card_mana_cost('drift of phantasms', ['2', 'U']).
card_cmc('drift of phantasms', 3).
card_layout('drift of phantasms', 'normal').
card_power('drift of phantasms', 0).
card_toughness('drift of phantasms', 5).

% Found in: CST, ICE, ME2
card_name('drift of the dead', 'Drift of the Dead').
card_type('drift of the dead', 'Creature — Wall').
card_types('drift of the dead', ['Creature']).
card_subtypes('drift of the dead', ['Wall']).
card_colors('drift of the dead', ['B']).
card_text('drift of the dead', 'Defender (This creature can\'t attack.)\nDrift of the Dead\'s power and toughness are each equal to the number of snow lands you control.').
card_mana_cost('drift of the dead', ['3', 'B']).
card_cmc('drift of the dead', 4).
card_layout('drift of the dead', 'normal').
card_power('drift of the dead', '*').
card_toughness('drift of the dead', '*').

% Found in: TSP
card_name('drifter il-dal', 'Drifter il-Dal').
card_type('drifter il-dal', 'Creature — Human Wizard').
card_types('drifter il-dal', ['Creature']).
card_subtypes('drifter il-dal', ['Human', 'Wizard']).
card_colors('drifter il-dal', ['U']).
card_text('drifter il-dal', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nAt the beginning of your upkeep, sacrifice Drifter il-Dal unless you pay {U}.').
card_mana_cost('drifter il-dal', ['U']).
card_cmc('drifter il-dal', 1).
card_layout('drifter il-dal', 'normal').
card_power('drifter il-dal', 2).
card_toughness('drifter il-dal', 1).

% Found in: USG
card_name('drifting djinn', 'Drifting Djinn').
card_type('drifting djinn', 'Creature — Djinn').
card_types('drifting djinn', ['Creature']).
card_subtypes('drifting djinn', ['Djinn']).
card_colors('drifting djinn', ['U']).
card_text('drifting djinn', 'Flying\nAt the beginning of your upkeep, sacrifice Drifting Djinn unless you pay {1}{U}.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('drifting djinn', ['4', 'U', 'U']).
card_cmc('drifting djinn', 6).
card_layout('drifting djinn', 'normal').
card_power('drifting djinn', 5).
card_toughness('drifting djinn', 5).

% Found in: ATH, BRB, C13, C14, USG
card_name('drifting meadow', 'Drifting Meadow').
card_type('drifting meadow', 'Land').
card_types('drifting meadow', ['Land']).
card_subtypes('drifting meadow', []).
card_colors('drifting meadow', []).
card_text('drifting meadow', 'Drifting Meadow enters the battlefield tapped.\n{T}: Add {W} to your mana pool.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_layout('drifting meadow', 'normal').

% Found in: M12
card_name('drifting shade', 'Drifting Shade').
card_type('drifting shade', 'Creature — Shade').
card_types('drifting shade', ['Creature']).
card_subtypes('drifting shade', ['Shade']).
card_colors('drifting shade', ['B']).
card_text('drifting shade', 'Flying\n{B}: Drifting Shade gets +1/+1 until end of turn.').
card_mana_cost('drifting shade', ['3', 'B']).
card_cmc('drifting shade', 4).
card_layout('drifting shade', 'normal').
card_power('drifting shade', 1).
card_toughness('drifting shade', 1).

% Found in: DST
card_name('drill-skimmer', 'Drill-Skimmer').
card_type('drill-skimmer', 'Artifact Creature — Thopter').
card_types('drill-skimmer', ['Artifact', 'Creature']).
card_subtypes('drill-skimmer', ['Thopter']).
card_colors('drill-skimmer', []).
card_text('drill-skimmer', 'Flying\nDrill-Skimmer has shroud as long as you control another artifact creature. (It can\'t be the target of spells or abilities.)').
card_mana_cost('drill-skimmer', ['4']).
card_cmc('drill-skimmer', 4).
card_layout('drill-skimmer', 'normal').
card_power('drill-skimmer', 2).
card_toughness('drill-skimmer', 1).

% Found in: LGN
card_name('drinker of sorrow', 'Drinker of Sorrow').
card_type('drinker of sorrow', 'Creature — Horror').
card_types('drinker of sorrow', ['Creature']).
card_subtypes('drinker of sorrow', ['Horror']).
card_colors('drinker of sorrow', ['B']).
card_text('drinker of sorrow', 'Drinker of Sorrow can\'t block.\nWhenever Drinker of Sorrow deals combat damage, sacrifice a permanent.').
card_mana_cost('drinker of sorrow', ['2', 'B']).
card_cmc('drinker of sorrow', 3).
card_layout('drinker of sorrow', 'normal').
card_power('drinker of sorrow', 5).
card_toughness('drinker of sorrow', 3).

% Found in: LGN
card_name('dripping dead', 'Dripping Dead').
card_type('dripping dead', 'Creature — Zombie').
card_types('dripping dead', ['Creature']).
card_subtypes('dripping dead', ['Zombie']).
card_colors('dripping dead', ['B']).
card_text('dripping dead', 'Dripping Dead can\'t block.\nWhenever Dripping Dead deals combat damage to a creature, destroy that creature. It can\'t be regenerated.').
card_mana_cost('dripping dead', ['4', 'B', 'B']).
card_cmc('dripping dead', 6).
card_layout('dripping dead', 'normal').
card_power('dripping dead', 4).
card_toughness('dripping dead', 1).

% Found in: CHK
card_name('dripping-tongue zubera', 'Dripping-Tongue Zubera').
card_type('dripping-tongue zubera', 'Creature — Zubera Spirit').
card_types('dripping-tongue zubera', ['Creature']).
card_subtypes('dripping-tongue zubera', ['Zubera', 'Spirit']).
card_colors('dripping-tongue zubera', ['G']).
card_text('dripping-tongue zubera', 'When Dripping-Tongue Zubera dies, put a 1/1 colorless Spirit creature token onto the battlefield for each Zubera that died this turn.').
card_mana_cost('dripping-tongue zubera', ['1', 'G']).
card_cmc('dripping-tongue zubera', 2).
card_layout('dripping-tongue zubera', 'normal').
card_power('dripping-tongue zubera', 1).
card_toughness('dripping-tongue zubera', 2).

% Found in: AVR
card_name('driver of the dead', 'Driver of the Dead').
card_type('driver of the dead', 'Creature — Vampire').
card_types('driver of the dead', ['Creature']).
card_subtypes('driver of the dead', ['Vampire']).
card_colors('driver of the dead', ['B']).
card_text('driver of the dead', 'When Driver of the Dead dies, return target creature card with converted mana cost 2 or less from your graveyard to the battlefield.').
card_mana_cost('driver of the dead', ['3', 'B']).
card_cmc('driver of the dead', 4).
card_layout('driver of the dead', 'normal').
card_power('driver of the dead', 3).
card_toughness('driver of the dead', 2).

% Found in: DKA
card_name('drogskol captain', 'Drogskol Captain').
card_type('drogskol captain', 'Creature — Spirit Soldier').
card_types('drogskol captain', ['Creature']).
card_subtypes('drogskol captain', ['Spirit', 'Soldier']).
card_colors('drogskol captain', ['W', 'U']).
card_text('drogskol captain', 'Flying\nOther Spirit creatures you control get +1/+1 and have hexproof. (They can\'t be the targets of spells or abilities your opponents control.)').
card_mana_cost('drogskol captain', ['1', 'W', 'U']).
card_cmc('drogskol captain', 3).
card_layout('drogskol captain', 'normal').
card_power('drogskol captain', 2).
card_toughness('drogskol captain', 2).

% Found in: DKA
card_name('drogskol reaver', 'Drogskol Reaver').
card_type('drogskol reaver', 'Creature — Spirit').
card_types('drogskol reaver', ['Creature']).
card_subtypes('drogskol reaver', ['Spirit']).
card_colors('drogskol reaver', ['W', 'U']).
card_text('drogskol reaver', 'Flying, double strike, lifelink\nWhenever you gain life, draw a card.').
card_mana_cost('drogskol reaver', ['5', 'W', 'U']).
card_cmc('drogskol reaver', 7).
card_layout('drogskol reaver', 'normal').
card_power('drogskol reaver', 3).
card_toughness('drogskol reaver', 5).

% Found in: RAV
card_name('dromad purebred', 'Dromad Purebred').
card_type('dromad purebred', 'Creature — Camel Beast').
card_types('dromad purebred', ['Creature']).
card_subtypes('dromad purebred', ['Camel', 'Beast']).
card_colors('dromad purebred', ['W']).
card_text('dromad purebred', 'Whenever Dromad Purebred is dealt damage, you gain 1 life.').
card_mana_cost('dromad purebred', ['4', 'W']).
card_cmc('dromad purebred', 5).
card_layout('dromad purebred', 'normal').
card_power('dromad purebred', 1).
card_toughness('dromad purebred', 5).

% Found in: INV
card_name('dromar\'s attendant', 'Dromar\'s Attendant').
card_type('dromar\'s attendant', 'Artifact Creature — Golem').
card_types('dromar\'s attendant', ['Artifact', 'Creature']).
card_subtypes('dromar\'s attendant', ['Golem']).
card_colors('dromar\'s attendant', []).
card_text('dromar\'s attendant', '{1}, Sacrifice Dromar\'s Attendant: Add {W}{U}{B} to your mana pool.').
card_mana_cost('dromar\'s attendant', ['5']).
card_cmc('dromar\'s attendant', 5).
card_layout('dromar\'s attendant', 'normal').
card_power('dromar\'s attendant', 3).
card_toughness('dromar\'s attendant', 3).

% Found in: PLS
card_name('dromar\'s cavern', 'Dromar\'s Cavern').
card_type('dromar\'s cavern', 'Land — Lair').
card_types('dromar\'s cavern', ['Land']).
card_subtypes('dromar\'s cavern', ['Lair']).
card_colors('dromar\'s cavern', []).
card_text('dromar\'s cavern', 'When Dromar\'s Cavern enters the battlefield, sacrifice it unless you return a non-Lair land you control to its owner\'s hand.\n{T}: Add {W}, {U}, or {B} to your mana pool.').
card_layout('dromar\'s cavern', 'normal').

% Found in: C13, PLS
card_name('dromar\'s charm', 'Dromar\'s Charm').
card_type('dromar\'s charm', 'Instant').
card_types('dromar\'s charm', ['Instant']).
card_subtypes('dromar\'s charm', []).
card_colors('dromar\'s charm', ['W', 'U', 'B']).
card_text('dromar\'s charm', 'Choose one —\n• You gain 5 life.\n• Counter target spell.\n• Target creature gets -2/-2 until end of turn.').
card_mana_cost('dromar\'s charm', ['W', 'U', 'B']).
card_cmc('dromar\'s charm', 3).
card_layout('dromar\'s charm', 'normal').

% Found in: INV
card_name('dromar, the banisher', 'Dromar, the Banisher').
card_type('dromar, the banisher', 'Legendary Creature — Dragon').
card_types('dromar, the banisher', ['Creature']).
card_subtypes('dromar, the banisher', ['Dragon']).
card_supertypes('dromar, the banisher', ['Legendary']).
card_colors('dromar, the banisher', ['W', 'U', 'B']).
card_text('dromar, the banisher', 'Flying\nWhenever Dromar, the Banisher deals combat damage to a player, you may pay {2}{U}. If you do, choose a color, then return all creatures of that color to their owners\' hands.').
card_mana_cost('dromar, the banisher', ['3', 'W', 'U', 'B']).
card_cmc('dromar, the banisher', 6).
card_layout('dromar, the banisher', 'normal').
card_power('dromar, the banisher', 6).
card_toughness('dromar, the banisher', 6).

% Found in: DTK
card_name('dromoka captain', 'Dromoka Captain').
card_type('dromoka captain', 'Creature — Human Soldier').
card_types('dromoka captain', ['Creature']).
card_subtypes('dromoka captain', ['Human', 'Soldier']).
card_colors('dromoka captain', ['W']).
card_text('dromoka captain', 'First strike\nWhenever Dromoka Captain attacks, bolster 1. (Choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_mana_cost('dromoka captain', ['2', 'W']).
card_cmc('dromoka captain', 3).
card_layout('dromoka captain', 'normal').
card_power('dromoka captain', 1).
card_toughness('dromoka captain', 1).

% Found in: DTK
card_name('dromoka dunecaster', 'Dromoka Dunecaster').
card_type('dromoka dunecaster', 'Creature — Human Wizard').
card_types('dromoka dunecaster', ['Creature']).
card_subtypes('dromoka dunecaster', ['Human', 'Wizard']).
card_colors('dromoka dunecaster', ['W']).
card_text('dromoka dunecaster', '{1}{W}, {T}: Tap target creature without flying.').
card_mana_cost('dromoka dunecaster', ['W']).
card_cmc('dromoka dunecaster', 1).
card_layout('dromoka dunecaster', 'normal').
card_power('dromoka dunecaster', 0).
card_toughness('dromoka dunecaster', 2).

% Found in: DTK
card_name('dromoka monument', 'Dromoka Monument').
card_type('dromoka monument', 'Artifact').
card_types('dromoka monument', ['Artifact']).
card_subtypes('dromoka monument', []).
card_colors('dromoka monument', []).
card_text('dromoka monument', '{T}: Add {G} or {W} to your mana pool.\n{4}{G}{W}: Dromoka Monument becomes a 4/4 green and white Dragon artifact creature with flying until end of turn.').
card_mana_cost('dromoka monument', ['3']).
card_cmc('dromoka monument', 3).
card_layout('dromoka monument', 'normal').

% Found in: DTK
card_name('dromoka warrior', 'Dromoka Warrior').
card_type('dromoka warrior', 'Creature — Human Warrior').
card_types('dromoka warrior', ['Creature']).
card_subtypes('dromoka warrior', ['Human', 'Warrior']).
card_colors('dromoka warrior', ['W']).
card_text('dromoka warrior', '').
card_mana_cost('dromoka warrior', ['1', 'W']).
card_cmc('dromoka warrior', 2).
card_layout('dromoka warrior', 'normal').
card_power('dromoka warrior', 3).
card_toughness('dromoka warrior', 1).

% Found in: DTK
card_name('dromoka\'s command', 'Dromoka\'s Command').
card_type('dromoka\'s command', 'Instant').
card_types('dromoka\'s command', ['Instant']).
card_subtypes('dromoka\'s command', []).
card_colors('dromoka\'s command', ['W', 'G']).
card_text('dromoka\'s command', 'Choose two —\n• Prevent all damage target instant or sorcery spell would deal this turn.\n• Target player sacrifices an enchantment.\n• Put a +1/+1 counter on target creature.\n• Target creature you control fights target creature you don\'t control.').
card_mana_cost('dromoka\'s command', ['G', 'W']).
card_cmc('dromoka\'s command', 2).
card_layout('dromoka\'s command', 'normal').

% Found in: DTK
card_name('dromoka\'s gift', 'Dromoka\'s Gift').
card_type('dromoka\'s gift', 'Instant').
card_types('dromoka\'s gift', ['Instant']).
card_subtypes('dromoka\'s gift', []).
card_colors('dromoka\'s gift', ['G']).
card_text('dromoka\'s gift', 'Bolster 4. (Choose a creature with the least toughness among creatures you control and put four +1/+1 counters on it.)').
card_mana_cost('dromoka\'s gift', ['4', 'G']).
card_cmc('dromoka\'s gift', 5).
card_layout('dromoka\'s gift', 'normal').

% Found in: FRF
card_name('dromoka, the eternal', 'Dromoka, the Eternal').
card_type('dromoka, the eternal', 'Legendary Creature — Dragon').
card_types('dromoka, the eternal', ['Creature']).
card_subtypes('dromoka, the eternal', ['Dragon']).
card_supertypes('dromoka, the eternal', ['Legendary']).
card_colors('dromoka, the eternal', ['W', 'G']).
card_text('dromoka, the eternal', 'Flying\nWhenever a Dragon you control attacks, bolster 2. (Choose a creature with the least toughness among creatures you control and put two +1/+1 counters on it.)').
card_mana_cost('dromoka, the eternal', ['3', 'G', 'W']).
card_cmc('dromoka, the eternal', 5).
card_layout('dromoka, the eternal', 'normal').
card_power('dromoka, the eternal', 5).
card_toughness('dromoka, the eternal', 5).

% Found in: USG
card_name('dromosaur', 'Dromosaur').
card_type('dromosaur', 'Creature — Lizard').
card_types('dromosaur', ['Creature']).
card_subtypes('dromosaur', ['Lizard']).
card_colors('dromosaur', ['R']).
card_text('dromosaur', 'Whenever Dromosaur blocks or becomes blocked, it gets +2/-2 until end of turn.').
card_mana_cost('dromosaur', ['2', 'R']).
card_cmc('dromosaur', 3).
card_layout('dromosaur', 'normal').
card_power('dromosaur', 2).
card_toughness('dromosaur', 3).

% Found in: GPT
card_name('droning bureaucrats', 'Droning Bureaucrats').
card_type('droning bureaucrats', 'Creature — Human Advisor').
card_types('droning bureaucrats', ['Creature']).
card_subtypes('droning bureaucrats', ['Human', 'Advisor']).
card_colors('droning bureaucrats', ['W']).
card_text('droning bureaucrats', '{X}, {T}: Each creature with converted mana cost X can\'t attack or block this turn.').
card_mana_cost('droning bureaucrats', ['3', 'W']).
card_cmc('droning bureaucrats', 4).
card_layout('droning bureaucrats', 'normal').
card_power('droning bureaucrats', 1).
card_toughness('droning bureaucrats', 4).

% Found in: DDM, MM2, RAV
card_name('drooling groodion', 'Drooling Groodion').
card_type('drooling groodion', 'Creature — Beast').
card_types('drooling groodion', ['Creature']).
card_subtypes('drooling groodion', ['Beast']).
card_colors('drooling groodion', ['B', 'G']).
card_text('drooling groodion', '{2}{B}{G}, Sacrifice a creature: Target creature gets +2/+2 until end of turn. Another target creature gets -2/-2 until end of turn.').
card_mana_cost('drooling groodion', ['3', 'B', 'B', 'G']).
card_cmc('drooling groodion', 6).
card_layout('drooling groodion', 'normal').
card_power('drooling groodion', 4).
card_toughness('drooling groodion', 3).

% Found in: DST
card_name('drooling ogre', 'Drooling Ogre').
card_type('drooling ogre', 'Creature — Ogre').
card_types('drooling ogre', ['Creature']).
card_subtypes('drooling ogre', ['Ogre']).
card_colors('drooling ogre', ['R']).
card_text('drooling ogre', 'Whenever a player casts an artifact spell, that player gains control of Drooling Ogre. (This effect lasts indefinitely.)').
card_mana_cost('drooling ogre', ['1', 'R']).
card_cmc('drooling ogre', 2).
card_layout('drooling ogre', 'normal').
card_power('drooling ogre', 3).
card_toughness('drooling ogre', 3).

% Found in: ARN, ME4
card_name('drop of honey', 'Drop of Honey').
card_type('drop of honey', 'Enchantment').
card_types('drop of honey', ['Enchantment']).
card_subtypes('drop of honey', []).
card_colors('drop of honey', ['G']).
card_text('drop of honey', 'At the beginning of your upkeep, destroy the creature with the least power. It can\'t be regenerated. If two or more creatures are tied for least power, you choose one of them.\nWhen there are no creatures on the battlefield, sacrifice Drop of Honey.').
card_mana_cost('drop of honey', ['G']).
card_cmc('drop of honey', 1).
card_layout('drop of honey', 'normal').
card_reserved('drop of honey').

% Found in: 10E, 5DN
card_name('dross crocodile', 'Dross Crocodile').
card_type('dross crocodile', 'Creature — Zombie Crocodile').
card_types('dross crocodile', ['Creature']).
card_subtypes('dross crocodile', ['Zombie', 'Crocodile']).
card_colors('dross crocodile', ['B']).
card_text('dross crocodile', '').
card_mana_cost('dross crocodile', ['3', 'B']).
card_cmc('dross crocodile', 4).
card_layout('dross crocodile', 'normal').
card_power('dross crocodile', 5).
card_toughness('dross crocodile', 1).

% Found in: DST
card_name('dross golem', 'Dross Golem').
card_type('dross golem', 'Artifact Creature — Golem').
card_types('dross golem', ['Artifact', 'Creature']).
card_subtypes('dross golem', ['Golem']).
card_colors('dross golem', []).
card_text('dross golem', 'Affinity for Swamps (This spell costs {1} less to cast for each Swamp you control.)\nFear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('dross golem', ['5']).
card_cmc('dross golem', 5).
card_layout('dross golem', 'normal').
card_power('dross golem', 3).
card_toughness('dross golem', 2).

% Found in: MRD
card_name('dross harvester', 'Dross Harvester').
card_type('dross harvester', 'Creature — Horror').
card_types('dross harvester', ['Creature']).
card_subtypes('dross harvester', ['Horror']).
card_colors('dross harvester', ['B']).
card_text('dross harvester', 'Protection from white\nAt the beginning of your end step, you lose 4 life.\nWhenever a creature dies, you gain 2 life.').
card_mana_cost('dross harvester', ['1', 'B', 'B']).
card_cmc('dross harvester', 3).
card_layout('dross harvester', 'normal').
card_power('dross harvester', 4).
card_toughness('dross harvester', 4).

% Found in: SOM
card_name('dross hopper', 'Dross Hopper').
card_type('dross hopper', 'Creature — Insect Horror').
card_types('dross hopper', ['Creature']).
card_subtypes('dross hopper', ['Insect', 'Horror']).
card_colors('dross hopper', ['B']).
card_text('dross hopper', 'Sacrifice a creature: Dross Hopper gains flying until end of turn.').
card_mana_cost('dross hopper', ['1', 'B']).
card_cmc('dross hopper', 2).
card_layout('dross hopper', 'normal').
card_power('dross hopper', 2).
card_toughness('dross hopper', 1).

% Found in: MRD
card_name('dross prowler', 'Dross Prowler').
card_type('dross prowler', 'Creature — Zombie').
card_types('dross prowler', ['Creature']).
card_subtypes('dross prowler', ['Zombie']).
card_colors('dross prowler', ['B']).
card_text('dross prowler', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('dross prowler', ['2', 'B']).
card_cmc('dross prowler', 3).
card_layout('dross prowler', 'normal').
card_power('dross prowler', 2).
card_toughness('dross prowler', 1).

% Found in: MBS
card_name('dross ripper', 'Dross Ripper').
card_type('dross ripper', 'Artifact Creature — Hound').
card_types('dross ripper', ['Artifact', 'Creature']).
card_subtypes('dross ripper', ['Hound']).
card_colors('dross ripper', []).
card_text('dross ripper', '{2}{B}: Dross Ripper gets +1/+1 until end of turn.').
card_mana_cost('dross ripper', ['4']).
card_cmc('dross ripper', 4).
card_layout('dross ripper', 'normal').
card_power('dross ripper', 3).
card_toughness('dross ripper', 3).

% Found in: MRD
card_name('dross scorpion', 'Dross Scorpion').
card_type('dross scorpion', 'Artifact Creature — Scorpion').
card_types('dross scorpion', ['Artifact', 'Creature']).
card_subtypes('dross scorpion', ['Scorpion']).
card_colors('dross scorpion', []).
card_text('dross scorpion', 'Whenever Dross Scorpion or another artifact creature dies, you may untap target artifact.').
card_mana_cost('dross scorpion', ['4']).
card_cmc('dross scorpion', 4).
card_layout('dross scorpion', 'normal').
card_power('dross scorpion', 3).
card_toughness('dross scorpion', 1).

% Found in: ICE
card_name('drought', 'Drought').
card_type('drought', 'Enchantment').
card_types('drought', ['Enchantment']).
card_subtypes('drought', []).
card_colors('drought', ['W']).
card_text('drought', 'At the beginning of your upkeep, sacrifice Drought unless you pay {W}{W}.\nSpells cost an additional \"Sacrifice a Swamp\" to cast for each black mana symbol in their mana costs.\nActivated abilities cost an additional \"Sacrifice a Swamp\" to activate for each black mana symbol in their activation costs.').
card_mana_cost('drought', ['2', 'W', 'W']).
card_cmc('drought', 4).
card_layout('drought', 'normal').

% Found in: C14, DPA, SHM
card_name('drove of elves', 'Drove of Elves').
card_type('drove of elves', 'Creature — Elf').
card_types('drove of elves', ['Creature']).
card_subtypes('drove of elves', ['Elf']).
card_colors('drove of elves', ['G']).
card_text('drove of elves', 'Hexproof\nDrove of Elves\'s power and toughness are each equal to the number of green permanents you control.').
card_mana_cost('drove of elves', ['3', 'G']).
card_cmc('drove of elves', 4).
card_layout('drove of elves', 'normal').
card_power('drove of elves', '*').
card_toughness('drove of elves', '*').

% Found in: DGM
card_name('drown in filth', 'Drown in Filth').
card_type('drown in filth', 'Sorcery').
card_types('drown in filth', ['Sorcery']).
card_subtypes('drown in filth', []).
card_colors('drown in filth', ['B', 'G']).
card_text('drown in filth', 'Choose target creature. Put the top four cards of your library into your graveyard, then that creature gets -1/-1 until end of turn for each land card in your graveyard.').
card_mana_cost('drown in filth', ['B', 'G']).
card_cmc('drown in filth', 2).
card_layout('drown in filth', 'normal').

% Found in: BNG
card_name('drown in sorrow', 'Drown in Sorrow').
card_type('drown in sorrow', 'Sorcery').
card_types('drown in sorrow', ['Sorcery']).
card_subtypes('drown in sorrow', []).
card_colors('drown in sorrow', ['B']).
card_text('drown in sorrow', 'All creatures get -2/-2 until end of turn. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('drown in sorrow', ['1', 'B', 'B']).
card_cmc('drown in sorrow', 3).
card_layout('drown in sorrow', 'normal').

% Found in: DRK, ME4
card_name('drowned', 'Drowned').
card_type('drowned', 'Creature — Zombie').
card_types('drowned', ['Creature']).
card_subtypes('drowned', ['Zombie']).
card_colors('drowned', ['U']).
card_text('drowned', '{B}: Regenerate Drowned.').
card_mana_cost('drowned', ['1', 'U']).
card_cmc('drowned', 2).
card_layout('drowned', 'normal').
card_power('drowned', 1).
card_toughness('drowned', 1).

% Found in: M10, M11, M12, M13
card_name('drowned catacomb', 'Drowned Catacomb').
card_type('drowned catacomb', 'Land').
card_types('drowned catacomb', ['Land']).
card_subtypes('drowned catacomb', []).
card_colors('drowned catacomb', []).
card_text('drowned catacomb', 'Drowned Catacomb enters the battlefield tapped unless you control an Island or a Swamp.\n{T}: Add {U} or {B} to your mana pool.').
card_layout('drowned catacomb', 'normal').

% Found in: GPT
card_name('drowned rusalka', 'Drowned Rusalka').
card_type('drowned rusalka', 'Creature — Spirit').
card_types('drowned rusalka', ['Creature']).
card_subtypes('drowned rusalka', ['Spirit']).
card_colors('drowned rusalka', ['U']).
card_text('drowned rusalka', '{U}, Sacrifice a creature: Discard a card, then draw a card.').
card_mana_cost('drowned rusalka', ['U']).
card_cmc('drowned rusalka', 1).
card_layout('drowned rusalka', 'normal').
card_power('drowned rusalka', 1).
card_toughness('drowned rusalka', 1).

% Found in: SHM
card_name('drowner initiate', 'Drowner Initiate').
card_type('drowner initiate', 'Creature — Merfolk Wizard').
card_types('drowner initiate', ['Creature']).
card_subtypes('drowner initiate', ['Merfolk', 'Wizard']).
card_colors('drowner initiate', ['U']).
card_text('drowner initiate', 'Whenever a player casts a blue spell, you may pay {1}. If you do, target player puts the top two cards of his or her library into his or her graveyard.').
card_mana_cost('drowner initiate', ['U']).
card_cmc('drowner initiate', 1).
card_layout('drowner initiate', 'normal').
card_power('drowner initiate', 1).
card_toughness('drowner initiate', 1).

% Found in: BFZ
card_name('drowner of hope', 'Drowner of Hope').
card_type('drowner of hope', 'Creature — Eldrazi').
card_types('drowner of hope', ['Creature']).
card_subtypes('drowner of hope', ['Eldrazi']).
card_colors('drowner of hope', []).
card_text('drowner of hope', 'Devoid (This card has no color.)\nWhen Drowner of Hope enters the battlefield, put two 1/1 colorless Eldrazi Scion creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"\nSacrifice an Eldrazi Scion: Tap target creature.').
card_mana_cost('drowner of hope', ['5', 'U']).
card_cmc('drowner of hope', 6).
card_layout('drowner of hope', 'normal').
card_power('drowner of hope', 5).
card_toughness('drowner of hope', 5).

% Found in: LRW
card_name('drowner of secrets', 'Drowner of Secrets').
card_type('drowner of secrets', 'Creature — Merfolk Wizard').
card_types('drowner of secrets', ['Creature']).
card_subtypes('drowner of secrets', ['Merfolk', 'Wizard']).
card_colors('drowner of secrets', ['U']).
card_text('drowner of secrets', 'Tap an untapped Merfolk you control: Target player puts the top card of his or her library into his or her graveyard.').
card_mana_cost('drowner of secrets', ['2', 'U']).
card_cmc('drowner of secrets', 3).
card_layout('drowner of secrets', 'normal').
card_power('drowner of secrets', 1).
card_toughness('drowner of secrets', 3).

% Found in: RTR
card_name('drudge beetle', 'Drudge Beetle').
card_type('drudge beetle', 'Creature — Insect').
card_types('drudge beetle', ['Creature']).
card_subtypes('drudge beetle', ['Insect']).
card_colors('drudge beetle', ['G']).
card_text('drudge beetle', 'Scavenge {5}{G} ({5}{G}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_mana_cost('drudge beetle', ['1', 'G']).
card_cmc('drudge beetle', 2).
card_layout('drudge beetle', 'normal').
card_power('drudge beetle', 2).
card_toughness('drudge beetle', 2).

% Found in: TSP
card_name('drudge reavers', 'Drudge Reavers').
card_type('drudge reavers', 'Creature — Skeleton').
card_types('drudge reavers', ['Creature']).
card_subtypes('drudge reavers', ['Skeleton']).
card_colors('drudge reavers', ['B']).
card_text('drudge reavers', 'Flash (You may cast this spell any time you could cast an instant.)\n{B}: Regenerate Drudge Reavers.').
card_mana_cost('drudge reavers', ['3', 'B']).
card_cmc('drudge reavers', 4).
card_layout('drudge reavers', 'normal').
card_power('drudge reavers', 2).
card_toughness('drudge reavers', 1).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, CED, CEI, DD3_GVL, DDD, DPA, LEA, LEB, M10, S00
card_name('drudge skeletons', 'Drudge Skeletons').
card_type('drudge skeletons', 'Creature — Skeleton').
card_types('drudge skeletons', ['Creature']).
card_subtypes('drudge skeletons', ['Skeleton']).
card_colors('drudge skeletons', ['B']).
card_text('drudge skeletons', '{B}: Regenerate Drudge Skeletons. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_mana_cost('drudge skeletons', ['1', 'B']).
card_cmc('drudge skeletons', 2).
card_layout('drudge skeletons', 'normal').
card_power('drudge skeletons', 1).
card_toughness('drudge skeletons', 1).

% Found in: HML
card_name('drudge spell', 'Drudge Spell').
card_type('drudge spell', 'Enchantment').
card_types('drudge spell', ['Enchantment']).
card_subtypes('drudge spell', []).
card_colors('drudge spell', ['B']).
card_text('drudge spell', '{B}, Exile two creature cards from your graveyard: Put a 1/1 black Skeleton creature token onto the battlefield. It has \"{B}: Regenerate this creature.\"\nWhen Drudge Spell leaves the battlefield, destroy all Skeleton tokens. They can\'t be regenerated.').
card_mana_cost('drudge spell', ['B', 'B']).
card_cmc('drudge spell', 2).
card_layout('drudge spell', 'normal').

% Found in: ODY
card_name('druid lyrist', 'Druid Lyrist').
card_type('druid lyrist', 'Creature — Human Druid').
card_types('druid lyrist', ['Creature']).
card_subtypes('druid lyrist', ['Human', 'Druid']).
card_colors('druid lyrist', ['G']).
card_text('druid lyrist', '{G}, {T}, Sacrifice Druid Lyrist: Destroy target enchantment.').
card_mana_cost('druid lyrist', ['G']).
card_cmc('druid lyrist', 1).
card_layout('druid lyrist', 'normal').
card_power('druid lyrist', 1).
card_toughness('druid lyrist', 1).

% Found in: ALA
card_name('druid of the anima', 'Druid of the Anima').
card_type('druid of the anima', 'Creature — Elf Druid').
card_types('druid of the anima', ['Creature']).
card_subtypes('druid of the anima', ['Elf', 'Druid']).
card_colors('druid of the anima', ['G']).
card_text('druid of the anima', '{T}: Add {R}, {G}, or {W} to your mana pool.').
card_mana_cost('druid of the anima', ['1', 'G']).
card_cmc('druid of the anima', 2).
card_layout('druid of the anima', 'normal').
card_power('druid of the anima', 1).
card_toughness('druid of the anima', 1).

% Found in: ODY
card_name('druid\'s call', 'Druid\'s Call').
card_type('druid\'s call', 'Enchantment — Aura').
card_types('druid\'s call', ['Enchantment']).
card_subtypes('druid\'s call', ['Aura']).
card_colors('druid\'s call', ['G']).
card_text('druid\'s call', 'Enchant creature\nWhenever enchanted creature is dealt damage, its controller puts that many 1/1 green Squirrel creature tokens onto the battlefield.').
card_mana_cost('druid\'s call', ['1', 'G']).
card_cmc('druid\'s call', 2).
card_layout('druid\'s call', 'normal').

% Found in: RTR
card_name('druid\'s deliverance', 'Druid\'s Deliverance').
card_type('druid\'s deliverance', 'Instant').
card_types('druid\'s deliverance', ['Instant']).
card_subtypes('druid\'s deliverance', []).
card_colors('druid\'s deliverance', ['G']).
card_text('druid\'s deliverance', 'Prevent all combat damage that would be dealt to you this turn. Populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_mana_cost('druid\'s deliverance', ['1', 'G']).
card_cmc('druid\'s deliverance', 2).
card_layout('druid\'s deliverance', 'normal').

% Found in: AVR
card_name('druid\'s familiar', 'Druid\'s Familiar').
card_type('druid\'s familiar', 'Creature — Bear').
card_types('druid\'s familiar', ['Creature']).
card_subtypes('druid\'s familiar', ['Bear']).
card_colors('druid\'s familiar', ['G']).
card_text('druid\'s familiar', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Druid\'s Familiar is paired with another creature, each of those creatures gets +2/+2.').
card_mana_cost('druid\'s familiar', ['3', 'G']).
card_cmc('druid\'s familiar', 4).
card_layout('druid\'s familiar', 'normal').
card_power('druid\'s familiar', 2).
card_toughness('druid\'s familiar', 2).

% Found in: C13, M12
card_name('druidic satchel', 'Druidic Satchel').
card_type('druidic satchel', 'Artifact').
card_types('druidic satchel', ['Artifact']).
card_subtypes('druidic satchel', []).
card_colors('druidic satchel', []).
card_text('druidic satchel', '{2}, {T}: Reveal the top card of your library. If it\'s a creature card, put a 1/1 green Saproling creature token onto the battlefield. If it\'s a land card, put that card onto the battlefield under your control. If it\'s a noncreature, nonland card, you gain 2 life.').
card_mana_cost('druidic satchel', ['3']).
card_cmc('druidic satchel', 3).
card_layout('druidic satchel', 'normal').

% Found in: AVR
card_name('druids\' repository', 'Druids\' Repository').
card_type('druids\' repository', 'Enchantment').
card_types('druids\' repository', ['Enchantment']).
card_subtypes('druids\' repository', []).
card_colors('druids\' repository', ['G']).
card_text('druids\' repository', 'Whenever a creature you control attacks, put a charge counter on Druids\' Repository.\nRemove a charge counter from Druids\' Repository: Add one mana of any color to your mana pool.').
card_mana_cost('druids\' repository', ['1', 'G', 'G']).
card_cmc('druids\' repository', 3).
card_layout('druids\' repository', 'normal').

% Found in: ALA, C13
card_name('drumhunter', 'Drumhunter').
card_type('drumhunter', 'Creature — Human Druid Warrior').
card_types('drumhunter', ['Creature']).
card_subtypes('drumhunter', ['Human', 'Druid', 'Warrior']).
card_colors('drumhunter', ['G']).
card_text('drumhunter', 'At the beginning of your end step, if you control a creature with power 5 or greater, you may draw a card.\n{T}: Add {1} to your mana pool.').
card_mana_cost('drumhunter', ['3', 'G']).
card_cmc('drumhunter', 4).
card_layout('drumhunter', 'normal').
card_power('drumhunter', 2).
card_toughness('drumhunter', 2).

% Found in: 6ED, HML, ME2, POR
card_name('dry spell', 'Dry Spell').
card_type('dry spell', 'Sorcery').
card_types('dry spell', ['Sorcery']).
card_subtypes('dry spell', []).
card_colors('dry spell', ['B']).
card_text('dry spell', 'Dry Spell deals 1 damage to each creature and each player.').
card_mana_cost('dry spell', ['1', 'B']).
card_cmc('dry spell', 2).
card_layout('dry spell', 'normal').

% Found in: FUT, V12
card_name('dryad arbor', 'Dryad Arbor').
card_type('dryad arbor', 'Land Creature — Forest Dryad').
card_types('dryad arbor', ['Land', 'Creature']).
card_subtypes('dryad arbor', ['Forest', 'Dryad']).
card_colors('dryad arbor', ['G']).
card_text('dryad arbor', '(Dryad Arbor isn\'t a spell, it\'s affected by summoning sickness, and it has \"{T}: Add {G} to your mana pool.\")').
card_layout('dryad arbor', 'normal').
card_power('dryad arbor', 1).
card_toughness('dryad arbor', 1).

% Found in: RTR, pMGD
card_name('dryad militant', 'Dryad Militant').
card_type('dryad militant', 'Creature — Dryad Soldier').
card_types('dryad militant', ['Creature']).
card_subtypes('dryad militant', ['Dryad', 'Soldier']).
card_colors('dryad militant', ['W', 'G']).
card_text('dryad militant', 'If an instant or sorcery card would be put into a graveyard from anywhere, exile it instead.').
card_mana_cost('dryad militant', ['G/W']).
card_cmc('dryad militant', 1).
card_layout('dryad militant', 'normal').
card_power('dryad militant', 2).
card_toughness('dryad militant', 1).

% Found in: GPT
card_name('dryad sophisticate', 'Dryad Sophisticate').
card_type('dryad sophisticate', 'Creature — Dryad').
card_types('dryad sophisticate', ['Creature']).
card_subtypes('dryad sophisticate', ['Dryad']).
card_colors('dryad sophisticate', ['G']).
card_text('dryad sophisticate', 'Nonbasic landwalk (This creature can\'t be blocked as long as defending player controls a nonbasic land.)').
card_mana_cost('dryad sophisticate', ['1', 'G']).
card_cmc('dryad sophisticate', 2).
card_layout('dryad sophisticate', 'normal').
card_power('dryad sophisticate', 2).
card_toughness('dryad sophisticate', 1).

% Found in: RAV
card_name('dryad\'s caress', 'Dryad\'s Caress').
card_type('dryad\'s caress', 'Instant').
card_types('dryad\'s caress', ['Instant']).
card_subtypes('dryad\'s caress', []).
card_colors('dryad\'s caress', ['G']).
card_text('dryad\'s caress', 'You gain 1 life for each creature on the battlefield. If {W} was spent to cast Dryad\'s Caress, untap all creatures you control.').
card_mana_cost('dryad\'s caress', ['4', 'G', 'G']).
card_cmc('dryad\'s caress', 6).
card_layout('dryad\'s caress', 'normal').

% Found in: M11
card_name('dryad\'s favor', 'Dryad\'s Favor').
card_type('dryad\'s favor', 'Enchantment — Aura').
card_types('dryad\'s favor', ['Enchantment']).
card_subtypes('dryad\'s favor', ['Aura']).
card_colors('dryad\'s favor', ['G']).
card_text('dryad\'s favor', 'Enchant creature\nEnchanted creature has forestwalk. (It can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('dryad\'s favor', ['G']).
card_cmc('dryad\'s favor', 1).
card_layout('dryad\'s favor', 'normal').

% Found in: AVR
card_name('dual casting', 'Dual Casting').
card_type('dual casting', 'Enchantment — Aura').
card_types('dual casting', ['Enchantment']).
card_subtypes('dual casting', ['Aura']).
card_colors('dual casting', ['R']).
card_text('dual casting', 'Enchant creature\nEnchanted creature has \"{R}, {T}: Copy target instant or sorcery spell you control. You may choose new targets for the copy.\"').
card_mana_cost('dual casting', ['1', 'R']).
card_cmc('dual casting', 2).
card_layout('dual casting', 'normal').

% Found in: PCY
card_name('dual nature', 'Dual Nature').
card_type('dual nature', 'Enchantment').
card_types('dual nature', ['Enchantment']).
card_subtypes('dual nature', []).
card_colors('dual nature', ['G']).
card_text('dual nature', 'Whenever a nontoken creature enters the battlefield, its controller puts a token that\'s a copy of that creature onto the battlefield.\nWhenever a nontoken creature leaves the battlefield, exile all tokens with the same name as that creature.\nWhen Dual Nature leaves the battlefield, exile all tokens put onto the battlefield with Dual Nature.').
card_mana_cost('dual nature', ['4', 'G', 'G']).
card_cmc('dual nature', 6).
card_layout('dual nature', 'normal').

% Found in: C14
card_name('dualcaster mage', 'Dualcaster Mage').
card_type('dualcaster mage', 'Creature — Human Wizard').
card_types('dualcaster mage', ['Creature']).
card_subtypes('dualcaster mage', ['Human', 'Wizard']).
card_colors('dualcaster mage', ['R']).
card_text('dualcaster mage', 'Flash\nWhen Dualcaster Mage enters the battlefield, copy target instant or sorcery spell. You may choose new targets for the copy.').
card_mana_cost('dualcaster mage', ['1', 'R', 'R']).
card_cmc('dualcaster mage', 3).
card_layout('dualcaster mage', 'normal').
card_power('dualcaster mage', 2).
card_toughness('dualcaster mage', 2).

% Found in: 10E, STH
card_name('duct crawler', 'Duct Crawler').
card_type('duct crawler', 'Creature — Insect').
card_types('duct crawler', ['Creature']).
card_subtypes('duct crawler', ['Insect']).
card_colors('duct crawler', ['R']).
card_text('duct crawler', '{1}{R}: Target creature can\'t block Duct Crawler this turn.').
card_mana_cost('duct crawler', ['R']).
card_cmc('duct crawler', 1).
card_layout('duct crawler', 'normal').
card_power('duct crawler', 1).
card_toughness('duct crawler', 1).

% Found in: NPH
card_name('due respect', 'Due Respect').
card_type('due respect', 'Instant').
card_types('due respect', ['Instant']).
card_subtypes('due respect', []).
card_colors('due respect', ['W']).
card_text('due respect', 'Permanents enter the battlefield tapped this turn.\nDraw a card.').
card_mana_cost('due respect', ['1', 'W']).
card_cmc('due respect', 2).
card_layout('due respect', 'normal').

% Found in: INV
card_name('dueling grounds', 'Dueling Grounds').
card_type('dueling grounds', 'Enchantment').
card_types('dueling grounds', ['Enchantment']).
card_subtypes('dueling grounds', []).
card_colors('dueling grounds', ['W', 'G']).
card_text('dueling grounds', 'No more than one creature can attack each turn.\nNo more than one creature can block each turn.').
card_mana_cost('dueling grounds', ['1', 'G', 'W']).
card_cmc('dueling grounds', 3).
card_layout('dueling grounds', 'normal').

% Found in: EVE
card_name('duergar assailant', 'Duergar Assailant').
card_type('duergar assailant', 'Creature — Dwarf Soldier').
card_types('duergar assailant', ['Creature']).
card_subtypes('duergar assailant', ['Dwarf', 'Soldier']).
card_colors('duergar assailant', ['W', 'R']).
card_text('duergar assailant', 'Sacrifice Duergar Assailant: Duergar Assailant deals 1 damage to target attacking or blocking creature.').
card_mana_cost('duergar assailant', ['R/W']).
card_cmc('duergar assailant', 1).
card_layout('duergar assailant', 'normal').
card_power('duergar assailant', 1).
card_toughness('duergar assailant', 1).

% Found in: EVE
card_name('duergar cave-guard', 'Duergar Cave-Guard').
card_type('duergar cave-guard', 'Creature — Dwarf Warrior').
card_types('duergar cave-guard', ['Creature']).
card_subtypes('duergar cave-guard', ['Dwarf', 'Warrior']).
card_colors('duergar cave-guard', ['R']).
card_text('duergar cave-guard', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\n{R/W}: Duergar Cave-Guard gets +1/+0 until end of turn.').
card_mana_cost('duergar cave-guard', ['3', 'R']).
card_cmc('duergar cave-guard', 4).
card_layout('duergar cave-guard', 'normal').
card_power('duergar cave-guard', 1).
card_toughness('duergar cave-guard', 3).

% Found in: CMD, EVE, HOP, pGTW
card_name('duergar hedge-mage', 'Duergar Hedge-Mage').
card_type('duergar hedge-mage', 'Creature — Dwarf Shaman').
card_types('duergar hedge-mage', ['Creature']).
card_subtypes('duergar hedge-mage', ['Dwarf', 'Shaman']).
card_colors('duergar hedge-mage', ['W', 'R']).
card_text('duergar hedge-mage', 'When Duergar Hedge-Mage enters the battlefield, if you control two or more Mountains, you may destroy target artifact.\nWhen Duergar Hedge-Mage enters the battlefield, if you control two or more Plains, you may destroy target enchantment.').
card_mana_cost('duergar hedge-mage', ['2', 'R/W']).
card_cmc('duergar hedge-mage', 3).
card_layout('duergar hedge-mage', 'normal').
card_power('duergar hedge-mage', 2).
card_toughness('duergar hedge-mage', 2).

% Found in: EVE
card_name('duergar mine-captain', 'Duergar Mine-Captain').
card_type('duergar mine-captain', 'Creature — Dwarf Soldier').
card_types('duergar mine-captain', ['Creature']).
card_subtypes('duergar mine-captain', ['Dwarf', 'Soldier']).
card_colors('duergar mine-captain', ['W', 'R']).
card_text('duergar mine-captain', '{1}{R/W}, {Q}: Attacking creatures get +1/+0 until end of turn. ({Q} is the untap symbol.)').
card_mana_cost('duergar mine-captain', ['2', 'R/W']).
card_cmc('duergar mine-captain', 3).
card_layout('duergar mine-captain', 'normal').
card_power('duergar mine-captain', 2).
card_toughness('duergar mine-captain', 1).

% Found in: UNH
card_name('duh', 'Duh').
card_type('duh', 'Instant').
card_types('duh', ['Instant']).
card_subtypes('duh', []).
card_colors('duh', ['B']).
card_text('duh', 'Destroy target creature with reminder text. (Reminder text is any italicized text in parentheses that explains rules you already know.)').
card_mana_cost('duh', ['B']).
card_cmc('duh', 1).
card_layout('duh', 'normal').

% Found in: C14
card_name('dulcet sirens', 'Dulcet Sirens').
card_type('dulcet sirens', 'Creature — Siren').
card_types('dulcet sirens', ['Creature']).
card_subtypes('dulcet sirens', ['Siren']).
card_colors('dulcet sirens', ['U']).
card_text('dulcet sirens', '{U}, {T}: Target creature attacks target opponent this turn if able.\nMorph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('dulcet sirens', ['2', 'U']).
card_cmc('dulcet sirens', 3).
card_layout('dulcet sirens', 'normal').
card_power('dulcet sirens', 1).
card_toughness('dulcet sirens', 3).

% Found in: UNH
card_name('dumb ass', 'Dumb Ass').
card_type('dumb ass', 'Creature — Donkey Barbarian').
card_types('dumb ass', ['Creature']).
card_subtypes('dumb ass', ['Donkey', 'Barbarian']).
card_colors('dumb ass', ['R']).
card_text('dumb ass', 'At the beginning of your upkeep, flip a coin. If you lose the flip, target opponent chooses whether Dumb Ass attacks this turn.').
card_mana_cost('dumb ass', ['2', 'R']).
card_cmc('dumb ass', 3).
card_layout('dumb ass', 'normal').
card_power('dumb ass', 3.5).
card_toughness('dumb ass', 2).

% Found in: GPT
card_name('dune-brood nephilim', 'Dune-Brood Nephilim').
card_type('dune-brood nephilim', 'Creature — Nephilim').
card_types('dune-brood nephilim', ['Creature']).
card_subtypes('dune-brood nephilim', ['Nephilim']).
card_colors('dune-brood nephilim', ['W', 'B', 'R', 'G']).
card_text('dune-brood nephilim', 'Whenever Dune-Brood Nephilim deals combat damage to a player, put a 1/1 colorless Sand creature token onto the battlefield for each land you control.').
card_mana_cost('dune-brood nephilim', ['B', 'R', 'G', 'W']).
card_cmc('dune-brood nephilim', 4).
card_layout('dune-brood nephilim', 'normal').
card_power('dune-brood nephilim', 3).
card_toughness('dune-brood nephilim', 3).

% Found in: KTK, pPRE
card_name('duneblast', 'Duneblast').
card_type('duneblast', 'Sorcery').
card_types('duneblast', ['Sorcery']).
card_subtypes('duneblast', []).
card_colors('duneblast', ['W', 'B', 'G']).
card_text('duneblast', 'Choose up to one creature. Destroy the rest.').
card_mana_cost('duneblast', ['4', 'W', 'B', 'G']).
card_cmc('duneblast', 7).
card_layout('duneblast', 'normal').

% Found in: PLC
card_name('dunerider outlaw', 'Dunerider Outlaw').
card_type('dunerider outlaw', 'Creature — Human Rebel Rogue').
card_types('dunerider outlaw', ['Creature']).
card_subtypes('dunerider outlaw', ['Human', 'Rebel', 'Rogue']).
card_colors('dunerider outlaw', ['B']).
card_text('dunerider outlaw', 'Protection from green\nAt the beginning of each end step, if Dunerider Outlaw dealt damage to an opponent this turn, put a +1/+1 counter on it.').
card_mana_cost('dunerider outlaw', ['B', 'B']).
card_cmc('dunerider outlaw', 2).
card_layout('dunerider outlaw', 'normal').
card_power('dunerider outlaw', 1).
card_toughness('dunerider outlaw', 1).

% Found in: C13, DKA
card_name('dungeon geists', 'Dungeon Geists').
card_type('dungeon geists', 'Creature — Spirit').
card_types('dungeon geists', ['Creature']).
card_subtypes('dungeon geists', ['Spirit']).
card_colors('dungeon geists', ['U']).
card_text('dungeon geists', 'Flying\nWhen Dungeon Geists enters the battlefield, tap target creature an opponent controls. That creature doesn\'t untap during its controller\'s untap step for as long as you control Dungeon Geists.').
card_mana_cost('dungeon geists', ['2', 'U', 'U']).
card_cmc('dungeon geists', 4).
card_layout('dungeon geists', 'normal').
card_power('dungeon geists', 3).
card_toughness('dungeon geists', 3).

% Found in: STH, TPR
card_name('dungeon shade', 'Dungeon Shade').
card_type('dungeon shade', 'Creature — Shade Spirit').
card_types('dungeon shade', ['Creature']).
card_subtypes('dungeon shade', ['Shade', 'Spirit']).
card_colors('dungeon shade', ['B']).
card_text('dungeon shade', 'Flying\n{B}: Dungeon Shade gets +1/+1 until end of turn.').
card_mana_cost('dungeon shade', ['3', 'B']).
card_cmc('dungeon shade', 4).
card_layout('dungeon shade', 'normal').
card_power('dungeon shade', 1).
card_toughness('dungeon shade', 1).

% Found in: M12, pMGD
card_name('dungrove elder', 'Dungrove Elder').
card_type('dungrove elder', 'Creature — Treefolk').
card_types('dungrove elder', ['Creature']).
card_subtypes('dungrove elder', ['Treefolk']).
card_colors('dungrove elder', ['G']).
card_text('dungrove elder', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nDungrove Elder\'s power and toughness are each equal to the number of Forests you control.').
card_mana_cost('dungrove elder', ['2', 'G']).
card_cmc('dungrove elder', 3).
card_layout('dungrove elder', 'normal').
card_power('dungrove elder', '*').
card_toughness('dungrove elder', '*').

% Found in: ARC, CM1, MRD
card_name('duplicant', 'Duplicant').
card_type('duplicant', 'Artifact Creature — Shapeshifter').
card_types('duplicant', ['Artifact', 'Creature']).
card_subtypes('duplicant', ['Shapeshifter']).
card_colors('duplicant', []).
card_text('duplicant', 'Imprint — When Duplicant enters the battlefield, you may exile target nontoken creature.\nAs long as a card exiled with Duplicant is a creature card, Duplicant has the power, toughness, and creature types of the last creature card exiled with Duplicant. It\'s still a Shapeshifter.').
card_mana_cost('duplicant', ['6']).
card_cmc('duplicant', 6).
card_layout('duplicant', 'normal').
card_power('duplicant', 2).
card_toughness('duplicant', 4).

% Found in: TMP
card_name('duplicity', 'Duplicity').
card_type('duplicity', 'Enchantment').
card_types('duplicity', ['Enchantment']).
card_subtypes('duplicity', []).
card_colors('duplicity', ['U']).
card_text('duplicity', 'When Duplicity enters the battlefield, exile the top five cards of your library face down.\nAt the beginning of your upkeep, you may exile all cards from your hand face down. If you do, put all other cards you own exiled with Duplicity into your hand.\nAt the beginning of your end step, discard a card.\nWhen you lose control of Duplicity, put all cards exiled with Duplicity into their owner\'s graveyard.').
card_mana_cost('duplicity', ['3', 'U', 'U']).
card_cmc('duplicity', 5).
card_layout('duplicity', 'normal').

% Found in: 7ED, DD3_DVD, DDC, DTK, M10, M11, M13, M14, MD1, PD3, USG, pARL, pFNM, pMEI
card_name('duress', 'Duress').
card_type('duress', 'Sorcery').
card_types('duress', ['Sorcery']).
card_subtypes('duress', []).
card_colors('duress', ['B']).
card_text('duress', 'Target opponent reveals his or her hand. You choose a noncreature, nonland card from it. That player discards that card.').
card_mana_cost('duress', ['B']).
card_cmc('duress', 1).
card_layout('duress', 'normal').

% Found in: MMA, TSP
card_name('durkwood baloth', 'Durkwood Baloth').
card_type('durkwood baloth', 'Creature — Beast').
card_types('durkwood baloth', ['Creature']).
card_subtypes('durkwood baloth', ['Beast']).
card_colors('durkwood baloth', ['G']).
card_text('durkwood baloth', 'Suspend 5—{G} (Rather than cast this card from your hand, you may pay {G} and exile it with five time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_mana_cost('durkwood baloth', ['4', 'G', 'G']).
card_cmc('durkwood baloth', 6).
card_layout('durkwood baloth', 'normal').
card_power('durkwood baloth', 5).
card_toughness('durkwood baloth', 5).

% Found in: 4ED, 5ED, ITP, LEG, RQS, S00, S99
card_name('durkwood boars', 'Durkwood Boars').
card_type('durkwood boars', 'Creature — Boar').
card_types('durkwood boars', ['Creature']).
card_subtypes('durkwood boars', ['Boar']).
card_colors('durkwood boars', ['G']).
card_text('durkwood boars', '').
card_mana_cost('durkwood boars', ['4', 'G']).
card_cmc('durkwood boars', 5).
card_layout('durkwood boars', 'normal').
card_power('durkwood boars', 4).
card_toughness('durkwood boars', 4).

% Found in: TSP
card_name('durkwood tracker', 'Durkwood Tracker').
card_type('durkwood tracker', 'Creature — Giant').
card_types('durkwood tracker', ['Creature']).
card_subtypes('durkwood tracker', ['Giant']).
card_colors('durkwood tracker', ['G']).
card_text('durkwood tracker', '{1}{G}, {T}: If Durkwood Tracker is on the battlefield, it deals damage equal to its power to target attacking creature. That creature deals damage equal to its power to Durkwood Tracker.').
card_mana_cost('durkwood tracker', ['4', 'G']).
card_cmc('durkwood tracker', 5).
card_layout('durkwood tracker', 'normal').
card_power('durkwood tracker', 4).
card_toughness('durkwood tracker', 3).

% Found in: 10E, 8ED, DD3_DVD, DDC, DPA, ODY
card_name('dusk imp', 'Dusk Imp').
card_type('dusk imp', 'Creature — Imp').
card_types('dusk imp', ['Creature']).
card_subtypes('dusk imp', ['Imp']).
card_colors('dusk imp', ['B']).
card_text('dusk imp', 'Flying').
card_mana_cost('dusk imp', ['2', 'B']).
card_cmc('dusk imp', 3).
card_layout('dusk imp', 'normal').
card_power('dusk imp', 2).
card_toughness('dusk imp', 1).

% Found in: SHM
card_name('dusk urchins', 'Dusk Urchins').
card_type('dusk urchins', 'Creature — Ouphe').
card_types('dusk urchins', ['Creature']).
card_subtypes('dusk urchins', ['Ouphe']).
card_colors('dusk urchins', ['B']).
card_text('dusk urchins', 'Whenever Dusk Urchins attacks or blocks, put a -1/-1 counter on it.\nWhen Dusk Urchins dies, draw a card for each -1/-1 counter on it.').
card_mana_cost('dusk urchins', ['2', 'B']).
card_cmc('dusk urchins', 3).
card_layout('dusk urchins', 'normal').
card_power('dusk urchins', 4).
card_toughness('dusk urchins', 3).

% Found in: DPA, EVE, M11, M13
card_name('duskdale wurm', 'Duskdale Wurm').
card_type('duskdale wurm', 'Creature — Wurm').
card_types('duskdale wurm', ['Creature']).
card_subtypes('duskdale wurm', ['Wurm']).
card_colors('duskdale wurm', ['G']).
card_text('duskdale wurm', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('duskdale wurm', ['5', 'G', 'G']).
card_cmc('duskdale wurm', 7).
card_layout('duskdale wurm', 'normal').
card_power('duskdale wurm', 7).
card_toughness('duskdale wurm', 7).

% Found in: DDK, M12, MM2
card_name('duskhunter bat', 'Duskhunter Bat').
card_type('duskhunter bat', 'Creature — Bat').
card_types('duskhunter bat', ['Creature']).
card_subtypes('duskhunter bat', ['Bat']).
card_colors('duskhunter bat', ['B']).
card_text('duskhunter bat', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)\nFlying').
card_mana_cost('duskhunter bat', ['1', 'B']).
card_cmc('duskhunter bat', 2).
card_layout('duskhunter bat', 'normal').
card_power('duskhunter bat', 1).
card_toughness('duskhunter bat', 1).

% Found in: GTC
card_name('duskmantle guildmage', 'Duskmantle Guildmage').
card_type('duskmantle guildmage', 'Creature — Human Wizard').
card_types('duskmantle guildmage', ['Creature']).
card_subtypes('duskmantle guildmage', ['Human', 'Wizard']).
card_colors('duskmantle guildmage', ['U', 'B']).
card_text('duskmantle guildmage', '{1}{U}{B}: Whenever a card is put into an opponent\'s graveyard from anywhere this turn, that player loses 1 life.\n{2}{U}{B}: Target player puts the top two cards of his or her library into his or her graveyard.').
card_mana_cost('duskmantle guildmage', ['U', 'B']).
card_cmc('duskmantle guildmage', 2).
card_layout('duskmantle guildmage', 'normal').
card_power('duskmantle guildmage', 2).
card_toughness('duskmantle guildmage', 2).

% Found in: M13
card_name('duskmantle prowler', 'Duskmantle Prowler').
card_type('duskmantle prowler', 'Creature — Vampire Rogue').
card_types('duskmantle prowler', ['Creature']).
card_subtypes('duskmantle prowler', ['Vampire', 'Rogue']).
card_colors('duskmantle prowler', ['B']).
card_text('duskmantle prowler', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_mana_cost('duskmantle prowler', ['3', 'B']).
card_cmc('duskmantle prowler', 4).
card_layout('duskmantle prowler', 'normal').
card_power('duskmantle prowler', 2).
card_toughness('duskmantle prowler', 2).

% Found in: GTC
card_name('duskmantle seer', 'Duskmantle Seer').
card_type('duskmantle seer', 'Creature — Vampire Wizard').
card_types('duskmantle seer', ['Creature']).
card_subtypes('duskmantle seer', ['Vampire', 'Wizard']).
card_colors('duskmantle seer', ['U', 'B']).
card_text('duskmantle seer', 'Flying\nAt the beginning of your upkeep, each player reveals the top card of his or her library, loses life equal to that card\'s converted mana cost, then puts it into his or her hand.').
card_mana_cost('duskmantle seer', ['2', 'U', 'B']).
card_cmc('duskmantle seer', 4).
card_layout('duskmantle seer', 'normal').
card_power('duskmantle seer', 4).
card_toughness('duskmantle seer', 4).

% Found in: RAV
card_name('duskmantle, house of shadow', 'Duskmantle, House of Shadow').
card_type('duskmantle, house of shadow', 'Land').
card_types('duskmantle, house of shadow', ['Land']).
card_subtypes('duskmantle, house of shadow', []).
card_colors('duskmantle, house of shadow', []).
card_text('duskmantle, house of shadow', '{T}: Add {1} to your mana pool.\n{U}{B}, {T}: Target player puts the top card of his or her library into his or her graveyard.').
card_layout('duskmantle, house of shadow', 'normal').

% Found in: WTH
card_name('duskrider falcon', 'Duskrider Falcon').
card_type('duskrider falcon', 'Creature — Bird').
card_types('duskrider falcon', ['Creature']).
card_subtypes('duskrider falcon', ['Bird']).
card_colors('duskrider falcon', ['W']).
card_text('duskrider falcon', 'Flying, protection from black').
card_mana_cost('duskrider falcon', ['1', 'W']).
card_cmc('duskrider falcon', 2).
card_layout('duskrider falcon', 'normal').
card_power('duskrider falcon', 1).
card_toughness('duskrider falcon', 1).

% Found in: TSP
card_name('duskrider peregrine', 'Duskrider Peregrine').
card_type('duskrider peregrine', 'Creature — Bird').
card_types('duskrider peregrine', ['Creature']).
card_subtypes('duskrider peregrine', ['Bird']).
card_colors('duskrider peregrine', ['W']).
card_text('duskrider peregrine', 'Flying, protection from black\nSuspend 3—{1}{W} (Rather than cast this card from your hand, you may pay {1}{W} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_mana_cost('duskrider peregrine', ['5', 'W']).
card_cmc('duskrider peregrine', 6).
card_layout('duskrider peregrine', 'normal').
card_power('duskrider peregrine', 3).
card_toughness('duskrider peregrine', 3).

% Found in: INV
card_name('duskwalker', 'Duskwalker').
card_type('duskwalker', 'Creature — Human Minion').
card_types('duskwalker', ['Creature']).
card_subtypes('duskwalker', ['Human', 'Minion']).
card_colors('duskwalker', ['B']).
card_text('duskwalker', 'Kicker {3}{B} (You may pay an additional {3}{B} as you cast this spell.)\nIf Duskwalker was kicked, it enters the battlefield with two +1/+1 counters on it and with fear. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('duskwalker', ['B']).
card_cmc('duskwalker', 1).
card_layout('duskwalker', 'normal').
card_power('duskwalker', 1).
card_toughness('duskwalker', 1).

% Found in: MRD
card_name('duskworker', 'Duskworker').
card_type('duskworker', 'Artifact Creature — Construct').
card_types('duskworker', ['Artifact', 'Creature']).
card_subtypes('duskworker', ['Construct']).
card_colors('duskworker', []).
card_text('duskworker', 'Whenever Duskworker becomes blocked, regenerate it.\n{3}: Duskworker gets +1/+0 until end of turn.').
card_mana_cost('duskworker', ['4']).
card_cmc('duskworker', 4).
card_layout('duskworker', 'normal').
card_power('duskworker', 2).
card_toughness('duskworker', 2).

% Found in: MMQ
card_name('dust bowl', 'Dust Bowl').
card_type('dust bowl', 'Land').
card_types('dust bowl', ['Land']).
card_subtypes('dust bowl', []).
card_colors('dust bowl', []).
card_text('dust bowl', '{T}: Add {1} to your mana pool.\n{3}, {T}, Sacrifice a land: Destroy target nonbasic land.').
card_layout('dust bowl', 'normal').

% Found in: PLC
card_name('dust corona', 'Dust Corona').
card_type('dust corona', 'Enchantment — Aura').
card_types('dust corona', ['Enchantment']).
card_subtypes('dust corona', ['Aura']).
card_colors('dust corona', ['R']).
card_text('dust corona', 'Enchant creature\nEnchanted creature gets +2/+0 and can\'t be blocked by creatures with flying.').
card_mana_cost('dust corona', ['R']).
card_cmc('dust corona', 1).
card_layout('dust corona', 'normal').

% Found in: PLC
card_name('dust elemental', 'Dust Elemental').
card_type('dust elemental', 'Creature — Elemental').
card_types('dust elemental', ['Creature']).
card_subtypes('dust elemental', ['Elemental']).
card_colors('dust elemental', ['W']).
card_text('dust elemental', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying; fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nWhen Dust Elemental enters the battlefield, return three creatures you control to their owner\'s hand.').
card_mana_cost('dust elemental', ['2', 'W', 'W']).
card_cmc('dust elemental', 4).
card_layout('dust elemental', 'normal').
card_power('dust elemental', 6).
card_toughness('dust elemental', 6).

% Found in: FUT
card_name('dust of moments', 'Dust of Moments').
card_type('dust of moments', 'Instant').
card_types('dust of moments', ['Instant']).
card_subtypes('dust of moments', []).
card_colors('dust of moments', ['W']).
card_text('dust of moments', 'Choose one —\n• Remove two time counters from each permanent and each suspended card.\n• Put two time counters on each permanent with a time counter on it and each suspended card.').
card_mana_cost('dust of moments', ['2', 'W']).
card_cmc('dust of moments', 3).
card_layout('dust of moments', 'normal').

% Found in: BFZ
card_name('dust stalker', 'Dust Stalker').
card_type('dust stalker', 'Creature — Eldrazi').
card_types('dust stalker', ['Creature']).
card_subtypes('dust stalker', ['Eldrazi']).
card_colors('dust stalker', []).
card_text('dust stalker', 'Devoid (This card has no color.)\nHaste\nAt the beginning of each end step, if you control no other colorless creatures, return Dust Stalker to its owner\'s hand.').
card_mana_cost('dust stalker', ['2', 'B', 'R']).
card_cmc('dust stalker', 4).
card_layout('dust stalker', 'normal').
card_power('dust stalker', 5).
card_toughness('dust stalker', 3).

% Found in: 5ED, DRK, ME4, MED
card_name('dust to dust', 'Dust to Dust').
card_type('dust to dust', 'Sorcery').
card_types('dust to dust', ['Sorcery']).
card_subtypes('dust to dust', []).
card_colors('dust to dust', ['W']).
card_text('dust to dust', 'Exile two target artifacts.').
card_mana_cost('dust to dust', ['1', 'W', 'W']).
card_cmc('dust to dust', 3).
card_layout('dust to dust', 'normal').

% Found in: DTK
card_name('dutiful attendant', 'Dutiful Attendant').
card_type('dutiful attendant', 'Creature — Human Warrior').
card_types('dutiful attendant', ['Creature']).
card_subtypes('dutiful attendant', ['Human', 'Warrior']).
card_colors('dutiful attendant', ['B']).
card_text('dutiful attendant', 'When Dutiful Attendant dies, return another target creature card from your graveyard to your hand.').
card_mana_cost('dutiful attendant', ['2', 'B']).
card_cmc('dutiful attendant', 3).
card_layout('dutiful attendant', 'normal').
card_power('dutiful attendant', 1).
card_toughness('dutiful attendant', 2).

% Found in: BFZ, KTK
card_name('dutiful return', 'Dutiful Return').
card_type('dutiful return', 'Sorcery').
card_types('dutiful return', ['Sorcery']).
card_subtypes('dutiful return', []).
card_colors('dutiful return', ['B']).
card_text('dutiful return', 'Return up to two target creature cards from your graveyard to your hand.').
card_mana_cost('dutiful return', ['3', 'B']).
card_cmc('dutiful return', 4).
card_layout('dutiful return', 'normal').

% Found in: GTC
card_name('dutiful thrull', 'Dutiful Thrull').
card_type('dutiful thrull', 'Creature — Thrull').
card_types('dutiful thrull', ['Creature']).
card_subtypes('dutiful thrull', ['Thrull']).
card_colors('dutiful thrull', ['W']).
card_text('dutiful thrull', '{B}: Regenerate Dutiful Thrull.').
card_mana_cost('dutiful thrull', ['W']).
card_cmc('dutiful thrull', 1).
card_layout('dutiful thrull', 'normal').
card_power('dutiful thrull', 1).
card_toughness('dutiful thrull', 1).

% Found in: M13
card_name('duty-bound dead', 'Duty-Bound Dead').
card_type('duty-bound dead', 'Creature — Skeleton').
card_types('duty-bound dead', ['Creature']).
card_subtypes('duty-bound dead', ['Skeleton']).
card_colors('duty-bound dead', ['B']).
card_text('duty-bound dead', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{3}{B}: Regenerate Duty-Bound Dead. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_mana_cost('duty-bound dead', ['B']).
card_cmc('duty-bound dead', 1).
card_layout('duty-bound dead', 'normal').
card_power('duty-bound dead', 0).
card_toughness('duty-bound dead', 2).

% Found in: FEM
card_name('dwarven armorer', 'Dwarven Armorer').
card_type('dwarven armorer', 'Creature — Dwarf').
card_types('dwarven armorer', ['Creature']).
card_subtypes('dwarven armorer', ['Dwarf']).
card_colors('dwarven armorer', ['R']).
card_text('dwarven armorer', '{R}, {T}, Discard a card: Put a +0/+1 counter or a +1/+0 counter on target creature.').
card_mana_cost('dwarven armorer', ['R']).
card_cmc('dwarven armorer', 1).
card_layout('dwarven armorer', 'normal').
card_power('dwarven armorer', 0).
card_toughness('dwarven armorer', 2).
card_reserved('dwarven armorer').

% Found in: ICE
card_name('dwarven armory', 'Dwarven Armory').
card_type('dwarven armory', 'Enchantment').
card_types('dwarven armory', ['Enchantment']).
card_subtypes('dwarven armory', []).
card_colors('dwarven armory', ['R']).
card_text('dwarven armory', '{2}, Sacrifice a land: Put a +2/+2 counter on target creature. Activate this ability only during any upkeep step.').
card_mana_cost('dwarven armory', ['2', 'R', 'R']).
card_cmc('dwarven armory', 4).
card_layout('dwarven armory', 'normal').

% Found in: WTH
card_name('dwarven berserker', 'Dwarven Berserker').
card_type('dwarven berserker', 'Creature — Dwarf Berserker').
card_types('dwarven berserker', ['Creature']).
card_subtypes('dwarven berserker', ['Dwarf', 'Berserker']).
card_colors('dwarven berserker', ['R']).
card_text('dwarven berserker', 'Whenever Dwarven Berserker becomes blocked, it gets +3/+0 and gains trample until end of turn.').
card_mana_cost('dwarven berserker', ['1', 'R']).
card_cmc('dwarven berserker', 2).
card_layout('dwarven berserker', 'normal').
card_power('dwarven berserker', 1).
card_toughness('dwarven berserker', 1).

% Found in: ONS
card_name('dwarven blastminer', 'Dwarven Blastminer').
card_type('dwarven blastminer', 'Creature — Dwarf').
card_types('dwarven blastminer', ['Creature']).
card_subtypes('dwarven blastminer', ['Dwarf']).
card_colors('dwarven blastminer', ['R']).
card_text('dwarven blastminer', '{2}{R}, {T}: Destroy target nonbasic land.\nMorph {R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('dwarven blastminer', ['1', 'R']).
card_cmc('dwarven blastminer', 2).
card_layout('dwarven blastminer', 'normal').
card_power('dwarven blastminer', 1).
card_toughness('dwarven blastminer', 1).

% Found in: JUD
card_name('dwarven bloodboiler', 'Dwarven Bloodboiler').
card_type('dwarven bloodboiler', 'Creature — Dwarf').
card_types('dwarven bloodboiler', ['Creature']).
card_subtypes('dwarven bloodboiler', ['Dwarf']).
card_colors('dwarven bloodboiler', ['R']).
card_text('dwarven bloodboiler', 'Tap an untapped Dwarf you control: Target creature gets +2/+0 until end of turn.').
card_mana_cost('dwarven bloodboiler', ['R', 'R', 'R']).
card_cmc('dwarven bloodboiler', 3).
card_layout('dwarven bloodboiler', 'normal').
card_power('dwarven bloodboiler', 2).
card_toughness('dwarven bloodboiler', 2).

% Found in: 5ED, FEM, MED
card_name('dwarven catapult', 'Dwarven Catapult').
card_type('dwarven catapult', 'Instant').
card_types('dwarven catapult', ['Instant']).
card_subtypes('dwarven catapult', []).
card_colors('dwarven catapult', ['R']).
card_text('dwarven catapult', 'Dwarven Catapult deals X damage divided evenly, rounded down, among all creatures target opponent controls.').
card_mana_cost('dwarven catapult', ['X', 'R']).
card_cmc('dwarven catapult', 1).
card_layout('dwarven catapult', 'normal').

% Found in: 2ED, 8ED, CED, CEI, LEA, LEB
card_name('dwarven demolition team', 'Dwarven Demolition Team').
card_type('dwarven demolition team', 'Creature — Dwarf').
card_types('dwarven demolition team', ['Creature']).
card_subtypes('dwarven demolition team', ['Dwarf']).
card_colors('dwarven demolition team', ['R']).
card_text('dwarven demolition team', '{T}: Destroy target Wall.').
card_mana_cost('dwarven demolition team', ['2', 'R']).
card_cmc('dwarven demolition team', 3).
card_layout('dwarven demolition team', 'normal').
card_power('dwarven demolition team', 1).
card_toughness('dwarven demolition team', 1).

% Found in: JUD
card_name('dwarven driller', 'Dwarven Driller').
card_type('dwarven driller', 'Creature — Dwarf').
card_types('dwarven driller', ['Creature']).
card_subtypes('dwarven driller', ['Dwarf']).
card_colors('dwarven driller', ['R']).
card_text('dwarven driller', '{T}: Destroy target land unless its controller has Dwarven Driller deal 2 damage to him or her.').
card_mana_cost('dwarven driller', ['3', 'R']).
card_cmc('dwarven driller', 4).
card_layout('dwarven driller', 'normal').
card_power('dwarven driller', 2).
card_toughness('dwarven driller', 2).

% Found in: ODY
card_name('dwarven grunt', 'Dwarven Grunt').
card_type('dwarven grunt', 'Creature — Dwarf').
card_types('dwarven grunt', ['Creature']).
card_subtypes('dwarven grunt', ['Dwarf']).
card_colors('dwarven grunt', ['R']).
card_text('dwarven grunt', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('dwarven grunt', ['R']).
card_cmc('dwarven grunt', 1).
card_layout('dwarven grunt', 'normal').
card_power('dwarven grunt', 1).
card_toughness('dwarven grunt', 1).

% Found in: 5ED, FEM
card_name('dwarven hold', 'Dwarven Hold').
card_type('dwarven hold', 'Land').
card_types('dwarven hold', ['Land']).
card_subtypes('dwarven hold', []).
card_colors('dwarven hold', []).
card_text('dwarven hold', 'Dwarven Hold enters the battlefield tapped.\nYou may choose not to untap Dwarven Hold during your untap step.\nAt the beginning of your upkeep, if Dwarven Hold is tapped, put a storage counter on it.\n{T}, Remove any number of storage counters from Dwarven Hold: Add {R} to your mana pool for each storage counter removed this way.').
card_layout('dwarven hold', 'normal').

% Found in: APC
card_name('dwarven landslide', 'Dwarven Landslide').
card_type('dwarven landslide', 'Sorcery').
card_types('dwarven landslide', ['Sorcery']).
card_subtypes('dwarven landslide', []).
card_colors('dwarven landslide', ['R']).
card_text('dwarven landslide', 'Kicker—{2}{R}, Sacrifice a land. (You may pay {2}{R} and sacrifice a land in addition to any other costs as you cast this spell.)\nDestroy target land. If Dwarven Landslide was kicked, destroy another target land.').
card_mana_cost('dwarven landslide', ['3', 'R']).
card_cmc('dwarven landslide', 4).
card_layout('dwarven landslide', 'normal').

% Found in: FEM
card_name('dwarven lieutenant', 'Dwarven Lieutenant').
card_type('dwarven lieutenant', 'Creature — Dwarf Soldier').
card_types('dwarven lieutenant', ['Creature']).
card_subtypes('dwarven lieutenant', ['Dwarf', 'Soldier']).
card_colors('dwarven lieutenant', ['R']).
card_text('dwarven lieutenant', '{1}{R}: Target Dwarf creature gets +1/+0 until end of turn.').
card_mana_cost('dwarven lieutenant', ['R', 'R']).
card_cmc('dwarven lieutenant', 2).
card_layout('dwarven lieutenant', 'normal').
card_power('dwarven lieutenant', 1).
card_toughness('dwarven lieutenant', 2).

% Found in: MIR
card_name('dwarven miner', 'Dwarven Miner').
card_type('dwarven miner', 'Creature — Dwarf').
card_types('dwarven miner', ['Creature']).
card_subtypes('dwarven miner', ['Dwarf']).
card_colors('dwarven miner', ['R']).
card_text('dwarven miner', '{2}{R}, {T}: Destroy target nonbasic land.').
card_mana_cost('dwarven miner', ['1', 'R']).
card_cmc('dwarven miner', 2).
card_layout('dwarven miner', 'normal').
card_power('dwarven miner', 1).
card_toughness('dwarven miner', 2).

% Found in: MIR
card_name('dwarven nomad', 'Dwarven Nomad').
card_type('dwarven nomad', 'Creature — Dwarf Nomad').
card_types('dwarven nomad', ['Creature']).
card_subtypes('dwarven nomad', ['Dwarf', 'Nomad']).
card_colors('dwarven nomad', ['R']).
card_text('dwarven nomad', '{T}: Target creature with power 2 or less can\'t be blocked this turn.').
card_mana_cost('dwarven nomad', ['2', 'R']).
card_cmc('dwarven nomad', 3).
card_layout('dwarven nomad', 'normal').
card_power('dwarven nomad', 1).
card_toughness('dwarven nomad', 1).

% Found in: APC
card_name('dwarven patrol', 'Dwarven Patrol').
card_type('dwarven patrol', 'Creature — Dwarf').
card_types('dwarven patrol', ['Creature']).
card_subtypes('dwarven patrol', ['Dwarf']).
card_colors('dwarven patrol', ['R']).
card_text('dwarven patrol', 'Dwarven Patrol doesn\'t untap during your untap step.\nWhenever you cast a nonred spell, untap Dwarven Patrol.').
card_mana_cost('dwarven patrol', ['2', 'R']).
card_cmc('dwarven patrol', 3).
card_layout('dwarven patrol', 'normal').
card_power('dwarven patrol', 4).
card_toughness('dwarven patrol', 2).

% Found in: HML
card_name('dwarven pony', 'Dwarven Pony').
card_type('dwarven pony', 'Creature — Horse').
card_types('dwarven pony', ['Creature']).
card_subtypes('dwarven pony', ['Horse']).
card_colors('dwarven pony', ['R']).
card_text('dwarven pony', '{1}{R}, {T}: Target Dwarf creature gains mountainwalk until end of turn. (It can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('dwarven pony', ['R']).
card_cmc('dwarven pony', 1).
card_layout('dwarven pony', 'normal').
card_power('dwarven pony', 1).
card_toughness('dwarven pony', 1).
card_reserved('dwarven pony').

% Found in: ODY
card_name('dwarven recruiter', 'Dwarven Recruiter').
card_type('dwarven recruiter', 'Creature — Dwarf').
card_types('dwarven recruiter', ['Creature']).
card_subtypes('dwarven recruiter', ['Dwarf']).
card_colors('dwarven recruiter', ['R']).
card_text('dwarven recruiter', 'When Dwarven Recruiter enters the battlefield, search your library for any number of Dwarf cards and reveal those cards. Shuffle your library, then put them on top of it in any order.').
card_mana_cost('dwarven recruiter', ['2', 'R']).
card_cmc('dwarven recruiter', 3).
card_layout('dwarven recruiter', 'normal').
card_power('dwarven recruiter', 2).
card_toughness('dwarven recruiter', 2).

% Found in: 5ED, 6ED, BTD, FEM, ME2
card_name('dwarven ruins', 'Dwarven Ruins').
card_type('dwarven ruins', 'Land').
card_types('dwarven ruins', ['Land']).
card_subtypes('dwarven ruins', []).
card_colors('dwarven ruins', []).
card_text('dwarven ruins', 'Dwarven Ruins enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\n{T}, Sacrifice Dwarven Ruins: Add {R}{R} to your mana pool.').
card_layout('dwarven ruins', 'normal').

% Found in: JUD
card_name('dwarven scorcher', 'Dwarven Scorcher').
card_type('dwarven scorcher', 'Creature — Dwarf').
card_types('dwarven scorcher', ['Creature']).
card_subtypes('dwarven scorcher', ['Dwarf']).
card_colors('dwarven scorcher', ['R']).
card_text('dwarven scorcher', 'Sacrifice Dwarven Scorcher: Dwarven Scorcher deals 1 damage to target creature unless that creature\'s controller has Dwarven Scorcher deal 2 damage to him or her.').
card_mana_cost('dwarven scorcher', ['R']).
card_cmc('dwarven scorcher', 1).
card_layout('dwarven scorcher', 'normal').
card_power('dwarven scorcher', 1).
card_toughness('dwarven scorcher', 1).

% Found in: HML
card_name('dwarven sea clan', 'Dwarven Sea Clan').
card_type('dwarven sea clan', 'Creature — Dwarf').
card_types('dwarven sea clan', ['Creature']).
card_subtypes('dwarven sea clan', ['Dwarf']).
card_colors('dwarven sea clan', ['R']).
card_text('dwarven sea clan', '{T}: Choose target attacking or blocking creature whose controller controls an Island. Dwarven Sea Clan deals 2 damage to that creature at end of combat. Activate this ability only before the end of combat step.').
card_mana_cost('dwarven sea clan', ['2', 'R']).
card_cmc('dwarven sea clan', 3).
card_layout('dwarven sea clan', 'normal').
card_power('dwarven sea clan', 1).
card_toughness('dwarven sea clan', 1).
card_reserved('dwarven sea clan').

% Found in: ODY
card_name('dwarven shrine', 'Dwarven Shrine').
card_type('dwarven shrine', 'Enchantment').
card_types('dwarven shrine', ['Enchantment']).
card_subtypes('dwarven shrine', []).
card_colors('dwarven shrine', ['R']).
card_text('dwarven shrine', 'Whenever a player casts a spell, Dwarven Shrine deals X damage to that player, where X is twice the number of cards in all graveyards with the same name as that spell.').
card_mana_cost('dwarven shrine', ['1', 'R', 'R']).
card_cmc('dwarven shrine', 3).
card_layout('dwarven shrine', 'normal').

% Found in: 5ED, FEM, MED
card_name('dwarven soldier', 'Dwarven Soldier').
card_type('dwarven soldier', 'Creature — Dwarf Soldier').
card_types('dwarven soldier', ['Creature']).
card_subtypes('dwarven soldier', ['Dwarf', 'Soldier']).
card_colors('dwarven soldier', ['R']).
card_text('dwarven soldier', 'Whenever Dwarven Soldier blocks or becomes blocked by one or more Orcs, Dwarven Soldier gets +0/+2 until end of turn.').
card_mana_cost('dwarven soldier', ['1', 'R']).
card_cmc('dwarven soldier', 2).
card_layout('dwarven soldier', 'normal').
card_power('dwarven soldier', 2).
card_toughness('dwarven soldier', 1).

% Found in: LEG
card_name('dwarven song', 'Dwarven Song').
card_type('dwarven song', 'Instant').
card_types('dwarven song', ['Instant']).
card_subtypes('dwarven song', []).
card_colors('dwarven song', ['R']).
card_text('dwarven song', 'Any number of target creatures become red until end of turn.').
card_mana_cost('dwarven song', ['R']).
card_cmc('dwarven song', 1).
card_layout('dwarven song', 'normal').

% Found in: ODY
card_name('dwarven strike force', 'Dwarven Strike Force').
card_type('dwarven strike force', 'Creature — Dwarf Berserker').
card_types('dwarven strike force', ['Creature']).
card_subtypes('dwarven strike force', ['Dwarf', 'Berserker']).
card_colors('dwarven strike force', ['R']).
card_text('dwarven strike force', 'Discard a card at random: Dwarven Strike Force gains first strike and haste until end of turn.').
card_mana_cost('dwarven strike force', ['4', 'R']).
card_cmc('dwarven strike force', 5).
card_layout('dwarven strike force', 'normal').
card_power('dwarven strike force', 4).
card_toughness('dwarven strike force', 3).

% Found in: WTH
card_name('dwarven thaumaturgist', 'Dwarven Thaumaturgist').
card_type('dwarven thaumaturgist', 'Creature — Dwarf Shaman').
card_types('dwarven thaumaturgist', ['Creature']).
card_subtypes('dwarven thaumaturgist', ['Dwarf', 'Shaman']).
card_colors('dwarven thaumaturgist', ['R']).
card_text('dwarven thaumaturgist', '{T}: Switch target creature\'s power and toughness until end of turn.').
card_mana_cost('dwarven thaumaturgist', ['2', 'R']).
card_cmc('dwarven thaumaturgist', 3).
card_layout('dwarven thaumaturgist', 'normal').
card_power('dwarven thaumaturgist', 1).
card_toughness('dwarven thaumaturgist', 2).
card_reserved('dwarven thaumaturgist').

% Found in: HML
card_name('dwarven trader', 'Dwarven Trader').
card_type('dwarven trader', 'Creature — Dwarf').
card_types('dwarven trader', ['Creature']).
card_subtypes('dwarven trader', ['Dwarf']).
card_colors('dwarven trader', ['R']).
card_text('dwarven trader', '').
card_mana_cost('dwarven trader', ['R']).
card_cmc('dwarven trader', 1).
card_layout('dwarven trader', 'normal').
card_power('dwarven trader', 1).
card_toughness('dwarven trader', 1).

% Found in: VIS
card_name('dwarven vigilantes', 'Dwarven Vigilantes').
card_type('dwarven vigilantes', 'Creature — Dwarf').
card_types('dwarven vigilantes', ['Creature']).
card_subtypes('dwarven vigilantes', ['Dwarf']).
card_colors('dwarven vigilantes', ['R']).
card_text('dwarven vigilantes', 'Whenever Dwarven Vigilantes attacks and isn\'t blocked, you may have it deal damage equal to its power to target creature. If you do, Dwarven Vigilantes assigns no combat damage this turn.').
card_mana_cost('dwarven vigilantes', ['2', 'R']).
card_cmc('dwarven vigilantes', 3).
card_layout('dwarven vigilantes', 'normal').
card_power('dwarven vigilantes', 2).
card_toughness('dwarven vigilantes', 2).

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB
card_name('dwarven warriors', 'Dwarven Warriors').
card_type('dwarven warriors', 'Creature — Dwarf Warrior').
card_types('dwarven warriors', ['Creature']).
card_subtypes('dwarven warriors', ['Dwarf', 'Warrior']).
card_colors('dwarven warriors', ['R']).
card_text('dwarven warriors', '{T}: Target creature with power 2 or less can\'t be blocked this turn.').
card_mana_cost('dwarven warriors', ['2', 'R']).
card_cmc('dwarven warriors', 3).
card_layout('dwarven warriors', 'normal').
card_power('dwarven warriors', 1).
card_toughness('dwarven warriors', 1).

% Found in: 3ED, ATQ
card_name('dwarven weaponsmith', 'Dwarven Weaponsmith').
card_type('dwarven weaponsmith', 'Creature — Dwarf Artificer').
card_types('dwarven weaponsmith', ['Creature']).
card_subtypes('dwarven weaponsmith', ['Dwarf', 'Artificer']).
card_colors('dwarven weaponsmith', ['R']).
card_text('dwarven weaponsmith', '{T}, Sacrifice an artifact: Put a +1/+1 counter on target creature. Activate this ability only during your upkeep.').
card_mana_cost('dwarven weaponsmith', ['1', 'R']).
card_cmc('dwarven weaponsmith', 2).
card_layout('dwarven weaponsmith', 'normal').
card_power('dwarven weaponsmith', 1).
card_toughness('dwarven weaponsmith', 1).

% Found in: TOR
card_name('dwell on the past', 'Dwell on the Past').
card_type('dwell on the past', 'Sorcery').
card_types('dwell on the past', ['Sorcery']).
card_subtypes('dwell on the past', []).
card_colors('dwell on the past', ['G']).
card_text('dwell on the past', 'Target player shuffles up to four target cards from his or her graveyard into his or her library.').
card_mana_cost('dwell on the past', ['G']).
card_cmc('dwell on the past', 1).
card_layout('dwell on the past', 'normal').

% Found in: ORI
card_name('dwynen\'s elite', 'Dwynen\'s Elite').
card_type('dwynen\'s elite', 'Creature — Elf Warrior').
card_types('dwynen\'s elite', ['Creature']).
card_subtypes('dwynen\'s elite', ['Elf', 'Warrior']).
card_colors('dwynen\'s elite', ['G']).
card_text('dwynen\'s elite', 'When Dwynen\'s Elite enters the battlefield, if you control another Elf, put a 1/1 green Elf Warrior creature token onto the battlefield.').
card_mana_cost('dwynen\'s elite', ['1', 'G']).
card_cmc('dwynen\'s elite', 2).
card_layout('dwynen\'s elite', 'normal').
card_power('dwynen\'s elite', 2).
card_toughness('dwynen\'s elite', 2).

% Found in: ORI
card_name('dwynen, gilt-leaf daen', 'Dwynen, Gilt-Leaf Daen').
card_type('dwynen, gilt-leaf daen', 'Legendary Creature — Elf Warrior').
card_types('dwynen, gilt-leaf daen', ['Creature']).
card_subtypes('dwynen, gilt-leaf daen', ['Elf', 'Warrior']).
card_supertypes('dwynen, gilt-leaf daen', ['Legendary']).
card_colors('dwynen, gilt-leaf daen', ['G']).
card_text('dwynen, gilt-leaf daen', 'Reach\nOther Elf creatures you control get +1/+1.\nWhenever Dwynen, Gilt-Leaf Daen attacks, you gain 1 life for each attacking Elf you control.').
card_mana_cost('dwynen, gilt-leaf daen', ['2', 'G', 'G']).
card_cmc('dwynen, gilt-leaf daen', 4).
card_layout('dwynen, gilt-leaf daen', 'normal').
card_power('dwynen, gilt-leaf daen', 3).
card_toughness('dwynen, gilt-leaf daen', 4).

% Found in: UDS
card_name('dying wail', 'Dying Wail').
card_type('dying wail', 'Enchantment — Aura').
card_types('dying wail', ['Enchantment']).
card_subtypes('dying wail', ['Aura']).
card_colors('dying wail', ['B']).
card_text('dying wail', 'Enchant creature\nWhen enchanted creature dies, target player discards two cards.').
card_mana_cost('dying wail', ['1', 'B']).
card_cmc('dying wail', 2).
card_layout('dying wail', 'normal').

% Found in: GTC
card_name('dying wish', 'Dying Wish').
card_type('dying wish', 'Enchantment — Aura').
card_types('dying wish', ['Enchantment']).
card_subtypes('dying wish', ['Aura']).
card_colors('dying wish', ['B']).
card_text('dying wish', 'Enchant creature you control\nWhen enchanted creature dies, target player loses X life and you gain X life, where X is its power.').
card_mana_cost('dying wish', ['1', 'B']).
card_cmc('dying wish', 2).
card_layout('dying wish', 'normal').

% Found in: RTR
card_name('dynacharge', 'Dynacharge').
card_type('dynacharge', 'Instant').
card_types('dynacharge', ['Instant']).
card_subtypes('dynacharge', []).
card_colors('dynacharge', ['R']).
card_text('dynacharge', 'Target creature you control gets +2/+0 until end of turn.\nOverload {2}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_mana_cost('dynacharge', ['R']).
card_cmc('dynacharge', 1).
card_layout('dynacharge', 'normal').

% Found in: ALL, ME2
card_name('dystopia', 'Dystopia').
card_type('dystopia', 'Enchantment').
card_types('dystopia', ['Enchantment']).
card_subtypes('dystopia', []).
card_colors('dystopia', ['B']).
card_text('dystopia', 'Cumulative upkeep—Pay 1 life. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAt the beginning of each player\'s upkeep, that player sacrifices a green or white permanent.').
card_mana_cost('dystopia', ['1', 'B', 'B']).
card_cmc('dystopia', 3).
card_layout('dystopia', 'normal').
card_reserved('dystopia').

% Found in: PO2, POR
card_name('déjà vu', 'Déjà Vu').
card_type('déjà vu', 'Sorcery').
card_types('déjà vu', ['Sorcery']).
card_subtypes('déjà vu', []).
card_colors('déjà vu', ['U']).
card_text('déjà vu', 'Return target sorcery card from your graveyard to your hand.').
card_mana_cost('déjà vu', ['2', 'U']).
card_cmc('déjà vu', 3).
card_layout('déjà vu', 'normal').

