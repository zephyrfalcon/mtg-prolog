% Card-specific data

% Found in: CSP
card_name('haakon, stromgald scourge', 'Haakon, Stromgald Scourge').
card_type('haakon, stromgald scourge', 'Legendary Creature — Zombie Knight').
card_types('haakon, stromgald scourge', ['Creature']).
card_subtypes('haakon, stromgald scourge', ['Zombie', 'Knight']).
card_supertypes('haakon, stromgald scourge', ['Legendary']).
card_colors('haakon, stromgald scourge', ['B']).
card_text('haakon, stromgald scourge', 'You may cast Haakon, Stromgald Scourge from your graveyard, but not from anywhere else.\nAs long as Haakon is on the battlefield, you may play Knight cards from your graveyard.\nWhen Haakon dies, you lose 2 life.').
card_mana_cost('haakon, stromgald scourge', ['1', 'B', 'B']).
card_cmc('haakon, stromgald scourge', 3).
card_layout('haakon, stromgald scourge', 'normal').
card_power('haakon, stromgald scourge', 3).
card_toughness('haakon, stromgald scourge', 3).

% Found in: VAN
card_name('haakon, stromgald scourge avatar', 'Haakon, Stromgald Scourge Avatar').
card_type('haakon, stromgald scourge avatar', 'Vanguard').
card_types('haakon, stromgald scourge avatar', ['Vanguard']).
card_subtypes('haakon, stromgald scourge avatar', []).
card_colors('haakon, stromgald scourge avatar', []).
card_text('haakon, stromgald scourge avatar', 'Pay 1 life: You may play target creature card in your graveyard this turn.\nWhenever you play a creature card from your graveyard, it becomes a black Zombie Knight.\nIf a Zombie Knight would be put into your graveyard from the battlefield, exile it instead.').
card_layout('haakon, stromgald scourge avatar', 'vanguard').

% Found in: DIS
card_name('haazda exonerator', 'Haazda Exonerator').
card_type('haazda exonerator', 'Creature — Human Cleric').
card_types('haazda exonerator', ['Creature']).
card_subtypes('haazda exonerator', ['Human', 'Cleric']).
card_colors('haazda exonerator', ['W']).
card_text('haazda exonerator', '{T}, Sacrifice Haazda Exonerator: Destroy target Aura.').
card_mana_cost('haazda exonerator', ['W']).
card_cmc('haazda exonerator', 1).
card_layout('haazda exonerator', 'normal').
card_power('haazda exonerator', 1).
card_toughness('haazda exonerator', 1).

% Found in: DIS
card_name('haazda shield mate', 'Haazda Shield Mate').
card_type('haazda shield mate', 'Creature — Human Soldier').
card_types('haazda shield mate', ['Creature']).
card_subtypes('haazda shield mate', ['Human', 'Soldier']).
card_colors('haazda shield mate', ['W']).
card_text('haazda shield mate', 'At the beginning of your upkeep, sacrifice Haazda Shield Mate unless you pay {W}{W}.\n{W}: The next time a source of your choice would deal damage to you this turn, prevent that damage.').
card_mana_cost('haazda shield mate', ['2', 'W']).
card_cmc('haazda shield mate', 3).
card_layout('haazda shield mate', 'normal').
card_power('haazda shield mate', 1).
card_toughness('haazda shield mate', 1).

% Found in: DGM
card_name('haazda snare squad', 'Haazda Snare Squad').
card_type('haazda snare squad', 'Creature — Human Soldier').
card_types('haazda snare squad', ['Creature']).
card_subtypes('haazda snare squad', ['Human', 'Soldier']).
card_colors('haazda snare squad', ['W']).
card_text('haazda snare squad', 'Whenever Haazda Snare Squad attacks, you may pay {W}. If you do, tap target creature an opponent controls.').
card_mana_cost('haazda snare squad', ['2', 'W']).
card_cmc('haazda snare squad', 3).
card_layout('haazda snare squad', 'normal').
card_power('haazda snare squad', 1).
card_toughness('haazda snare squad', 4).

% Found in: WWK, pWPN
card_name('hada freeblade', 'Hada Freeblade').
card_type('hada freeblade', 'Creature — Human Soldier Ally').
card_types('hada freeblade', ['Creature']).
card_subtypes('hada freeblade', ['Human', 'Soldier', 'Ally']).
card_colors('hada freeblade', ['W']).
card_text('hada freeblade', 'Whenever Hada Freeblade or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Hada Freeblade.').
card_mana_cost('hada freeblade', ['W']).
card_cmc('hada freeblade', 1).
card_layout('hada freeblade', 'normal').
card_power('hada freeblade', 0).
card_toughness('hada freeblade', 1).

% Found in: C13, ROE
card_name('hada spy patrol', 'Hada Spy Patrol').
card_type('hada spy patrol', 'Creature — Human Rogue').
card_types('hada spy patrol', ['Creature']).
card_subtypes('hada spy patrol', ['Human', 'Rogue']).
card_colors('hada spy patrol', ['U']).
card_text('hada spy patrol', 'Level up {2}{U} ({2}{U}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-2\n2/2\nHada Spy Patrol can\'t be blocked.\nLEVEL 3+\n3/3\nShroud (This creature can\'t be the target of spells or abilities.)\nHada Spy Patrol can\'t be blocked.').
card_mana_cost('hada spy patrol', ['1', 'U']).
card_cmc('hada spy patrol', 2).
card_layout('hada spy patrol', 'leveler').
card_power('hada spy patrol', 1).
card_toughness('hada spy patrol', 1).

% Found in: EVE
card_name('hag hedge-mage', 'Hag Hedge-Mage').
card_type('hag hedge-mage', 'Creature — Hag Shaman').
card_types('hag hedge-mage', ['Creature']).
card_subtypes('hag hedge-mage', ['Hag', 'Shaman']).
card_colors('hag hedge-mage', ['B', 'G']).
card_text('hag hedge-mage', 'When Hag Hedge-Mage enters the battlefield, if you control two or more Swamps, you may have target player discard a card.\nWhen Hag Hedge-Mage enters the battlefield, if you control two or more Forests, you may put target card from your graveyard on top of your library.').
card_mana_cost('hag hedge-mage', ['2', 'B/G']).
card_cmc('hag hedge-mage', 3).
card_layout('hag hedge-mage', 'normal').
card_power('hag hedge-mage', 2).
card_toughness('hag hedge-mage', 2).

% Found in: ZEN
card_name('hagra crocodile', 'Hagra Crocodile').
card_type('hagra crocodile', 'Creature — Crocodile').
card_types('hagra crocodile', ['Creature']).
card_subtypes('hagra crocodile', ['Crocodile']).
card_colors('hagra crocodile', ['B']).
card_text('hagra crocodile', 'Hagra Crocodile can\'t block.\nLandfall — Whenever a land enters the battlefield under your control, Hagra Crocodile gets +2/+2 until end of turn.').
card_mana_cost('hagra crocodile', ['3', 'B']).
card_cmc('hagra crocodile', 4).
card_layout('hagra crocodile', 'normal').
card_power('hagra crocodile', 3).
card_toughness('hagra crocodile', 1).

% Found in: ZEN
card_name('hagra diabolist', 'Hagra Diabolist').
card_type('hagra diabolist', 'Creature — Ogre Shaman Ally').
card_types('hagra diabolist', ['Creature']).
card_subtypes('hagra diabolist', ['Ogre', 'Shaman', 'Ally']).
card_colors('hagra diabolist', ['B']).
card_text('hagra diabolist', 'Whenever Hagra Diabolist or another Ally enters the battlefield under your control, you may have target player lose life equal to the number of Allies you control.').
card_mana_cost('hagra diabolist', ['4', 'B']).
card_cmc('hagra diabolist', 5).
card_layout('hagra diabolist', 'normal').
card_power('hagra diabolist', 3).
card_toughness('hagra diabolist', 2).

% Found in: BFZ
card_name('hagra sharpshooter', 'Hagra Sharpshooter').
card_type('hagra sharpshooter', 'Creature — Human Assassin Ally').
card_types('hagra sharpshooter', ['Creature']).
card_subtypes('hagra sharpshooter', ['Human', 'Assassin', 'Ally']).
card_colors('hagra sharpshooter', ['B']).
card_text('hagra sharpshooter', '{4}{B}: Target creature gets -1/-1 until end of turn.').
card_mana_cost('hagra sharpshooter', ['2', 'B']).
card_cmc('hagra sharpshooter', 3).
card_layout('hagra sharpshooter', 'normal').
card_power('hagra sharpshooter', 2).
card_toughness('hagra sharpshooter', 2).

% Found in: 10E, SOK
card_name('hail of arrows', 'Hail of Arrows').
card_type('hail of arrows', 'Instant').
card_types('hail of arrows', ['Instant']).
card_subtypes('hail of arrows', []).
card_colors('hail of arrows', ['W']).
card_text('hail of arrows', 'Hail of Arrows deals X damage divided as you choose among any number of target attacking creatures.').
card_mana_cost('hail of arrows', ['X', 'W']).
card_cmc('hail of arrows', 1).
card_layout('hail of arrows', 'normal').

% Found in: ALL, TSB
card_name('hail storm', 'Hail Storm').
card_type('hail storm', 'Instant').
card_types('hail storm', ['Instant']).
card_subtypes('hail storm', []).
card_colors('hail storm', ['G']).
card_text('hail storm', 'Hail Storm deals 2 damage to each attacking creature and 1 damage to you and each creature you control.').
card_mana_cost('hail storm', ['1', 'G', 'G']).
card_cmc('hail storm', 3).
card_layout('hail storm', 'normal').

% Found in: CHK
card_name('hair-strung koto', 'Hair-Strung Koto').
card_type('hair-strung koto', 'Artifact').
card_types('hair-strung koto', ['Artifact']).
card_subtypes('hair-strung koto', []).
card_colors('hair-strung koto', []).
card_text('hair-strung koto', 'Tap an untapped creature you control: Target player puts the top card of his or her library into his or her graveyard.').
card_mana_cost('hair-strung koto', ['6']).
card_cmc('hair-strung koto', 6).
card_layout('hair-strung koto', 'normal').

% Found in: MIR
card_name('hakim, loreweaver', 'Hakim, Loreweaver').
card_type('hakim, loreweaver', 'Legendary Creature — Human Wizard').
card_types('hakim, loreweaver', ['Creature']).
card_subtypes('hakim, loreweaver', ['Human', 'Wizard']).
card_supertypes('hakim, loreweaver', ['Legendary']).
card_colors('hakim, loreweaver', ['U']).
card_text('hakim, loreweaver', 'Flying\n{U}{U}: Return target Aura card from your graveyard to the battlefield attached to Hakim, Loreweaver. Activate this ability only during your upkeep and only if Hakim isn\'t enchanted.\n{U}{U}, {T}: Destroy all Auras attached to Hakim.').
card_mana_cost('hakim, loreweaver', ['3', 'U', 'U']).
card_cmc('hakim, loreweaver', 5).
card_layout('hakim, loreweaver', 'normal').
card_power('hakim, loreweaver', 2).
card_toughness('hakim, loreweaver', 4).
card_reserved('hakim, loreweaver').

% Found in: INV
card_name('halam djinn', 'Halam Djinn').
card_type('halam djinn', 'Creature — Djinn').
card_types('halam djinn', ['Creature']).
card_subtypes('halam djinn', ['Djinn']).
card_colors('halam djinn', ['R']).
card_text('halam djinn', 'Haste\nHalam Djinn gets -2/-2 as long as red is the most common color among all permanents or is tied for most common.').
card_mana_cost('halam djinn', ['5', 'R']).
card_cmc('halam djinn', 6).
card_layout('halam djinn', 'normal').
card_power('halam djinn', 6).
card_toughness('halam djinn', 5).

% Found in: ODY
card_name('halberdier', 'Halberdier').
card_type('halberdier', 'Creature — Human Barbarian').
card_types('halberdier', ['Creature']).
card_subtypes('halberdier', ['Human', 'Barbarian']).
card_colors('halberdier', ['R']).
card_text('halberdier', 'First strike').
card_mana_cost('halberdier', ['3', 'R']).
card_cmc('halberdier', 4).
card_layout('halberdier', 'normal').
card_power('halberdier', 3).
card_toughness('halberdier', 1).

% Found in: RAV
card_name('halcyon glaze', 'Halcyon Glaze').
card_type('halcyon glaze', 'Enchantment').
card_types('halcyon glaze', ['Enchantment']).
card_subtypes('halcyon glaze', []).
card_colors('halcyon glaze', ['U']).
card_text('halcyon glaze', 'Whenever you cast a creature spell, Halcyon Glaze becomes a 4/4 Illusion creature with flying in addition to its other types until end of turn.').
card_mana_cost('halcyon glaze', ['1', 'U', 'U']).
card_cmc('halcyon glaze', 3).
card_layout('halcyon glaze', 'normal').

% Found in: LEG, ME3
card_name('halfdane', 'Halfdane').
card_type('halfdane', 'Legendary Creature — Shapeshifter').
card_types('halfdane', ['Creature']).
card_subtypes('halfdane', ['Shapeshifter']).
card_supertypes('halfdane', ['Legendary']).
card_colors('halfdane', ['W', 'U', 'B']).
card_text('halfdane', 'At the beginning of your upkeep, change Halfdane\'s base power and toughness to the power and toughness of target creature other than Halfdane until the end of your next upkeep.').
card_mana_cost('halfdane', ['1', 'W', 'U', 'B']).
card_cmc('halfdane', 4).
card_layout('halfdane', 'normal').
card_power('halfdane', 3).
card_toughness('halfdane', 3).
card_reserved('halfdane').

% Found in: DDM, WWK
card_name('halimar depths', 'Halimar Depths').
card_type('halimar depths', 'Land').
card_types('halimar depths', ['Land']).
card_subtypes('halimar depths', []).
card_colors('halimar depths', []).
card_text('halimar depths', 'Halimar Depths enters the battlefield tapped.\nWhen Halimar Depths enters the battlefield, look at the top three cards of your library, then put them back in any order.\n{T}: Add {U} to your mana pool.').
card_layout('halimar depths', 'normal').

% Found in: WWK
card_name('halimar excavator', 'Halimar Excavator').
card_type('halimar excavator', 'Creature — Human Wizard Ally').
card_types('halimar excavator', ['Creature']).
card_subtypes('halimar excavator', ['Human', 'Wizard', 'Ally']).
card_colors('halimar excavator', ['U']).
card_text('halimar excavator', 'Whenever Halimar Excavator or another Ally enters the battlefield under your control, target player puts the top X cards of his or her library into his or her graveyard, where X is the number of Allies you control.').
card_mana_cost('halimar excavator', ['1', 'U']).
card_cmc('halimar excavator', 2).
card_layout('halimar excavator', 'normal').
card_power('halimar excavator', 1).
card_toughness('halimar excavator', 3).

% Found in: BFZ
card_name('halimar tidecaller', 'Halimar Tidecaller').
card_type('halimar tidecaller', 'Creature — Human Wizard Ally').
card_types('halimar tidecaller', ['Creature']).
card_subtypes('halimar tidecaller', ['Human', 'Wizard', 'Ally']).
card_colors('halimar tidecaller', ['U']).
card_text('halimar tidecaller', 'When Halimar Tidecaller enters the battlefield, you may return target card with awaken from your graveyard to your hand.\nLand creatures you control have flying.').
card_mana_cost('halimar tidecaller', ['2', 'U']).
card_cmc('halimar tidecaller', 3).
card_layout('halimar tidecaller', 'normal').
card_power('halimar tidecaller', 2).
card_toughness('halimar tidecaller', 3).

% Found in: ROE
card_name('halimar wavewatch', 'Halimar Wavewatch').
card_type('halimar wavewatch', 'Creature — Merfolk Soldier').
card_types('halimar wavewatch', ['Creature']).
card_subtypes('halimar wavewatch', ['Merfolk', 'Soldier']).
card_colors('halimar wavewatch', ['U']).
card_text('halimar wavewatch', 'Level up {2} ({2}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-4\n0/6\nLEVEL 5+\n6/6\nIslandwalk (This creature can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('halimar wavewatch', ['1', 'U']).
card_cmc('halimar wavewatch', 2).
card_layout('halimar wavewatch', 'leveler').
card_power('halimar wavewatch', 0).
card_toughness('halimar wavewatch', 3).

% Found in: MIR
card_name('hall of gemstone', 'Hall of Gemstone').
card_type('hall of gemstone', 'World Enchantment').
card_types('hall of gemstone', ['Enchantment']).
card_subtypes('hall of gemstone', []).
card_supertypes('hall of gemstone', ['World']).
card_colors('hall of gemstone', ['G']).
card_text('hall of gemstone', 'At the beginning of each player\'s upkeep, that player chooses a color. Until end of turn, lands tapped for mana produce mana of the chosen color instead of any other color.').
card_mana_cost('hall of gemstone', ['1', 'G', 'G']).
card_cmc('hall of gemstone', 3).
card_layout('hall of gemstone', 'normal').
card_reserved('hall of gemstone').

% Found in: CHK
card_name('hall of the bandit lord', 'Hall of the Bandit Lord').
card_type('hall of the bandit lord', 'Legendary Land').
card_types('hall of the bandit lord', ['Land']).
card_subtypes('hall of the bandit lord', []).
card_supertypes('hall of the bandit lord', ['Legendary']).
card_colors('hall of the bandit lord', []).
card_text('hall of the bandit lord', 'Hall of the Bandit Lord enters the battlefield tapped.\n{T}, Pay 3 life: Add {1} to your mana pool. If that mana is spent on a creature spell, it gains haste.').
card_layout('hall of the bandit lord', 'normal').

% Found in: JOU, pMGD
card_name('hall of triumph', 'Hall of Triumph').
card_type('hall of triumph', 'Legendary Artifact').
card_types('hall of triumph', ['Artifact']).
card_subtypes('hall of triumph', []).
card_supertypes('hall of triumph', ['Legendary']).
card_colors('hall of triumph', []).
card_text('hall of triumph', 'As Hall of Triumph enters the battlefield, choose a color.\nCreatures you control of the chosen color get +1/+1.').
card_mana_cost('hall of triumph', ['3']).
card_cmc('hall of triumph', 3).
card_layout('hall of triumph', 'normal').

% Found in: DST
card_name('hallow', 'Hallow').
card_type('hallow', 'Instant').
card_types('hallow', ['Instant']).
card_subtypes('hallow', []).
card_colors('hallow', ['W']).
card_text('hallow', 'Prevent all damage target spell would deal this turn. You gain life equal to the damage prevented this way.').
card_mana_cost('hallow', ['W']).
card_cmc('hallow', 1).
card_layout('hallow', 'normal').

% Found in: EVE
card_name('hallowed burial', 'Hallowed Burial').
card_type('hallowed burial', 'Sorcery').
card_types('hallowed burial', ['Sorcery']).
card_subtypes('hallowed burial', []).
card_colors('hallowed burial', ['W']).
card_text('hallowed burial', 'Put all creatures on the bottom of their owners\' libraries.').
card_mana_cost('hallowed burial', ['3', 'W', 'W']).
card_cmc('hallowed burial', 5).
card_layout('hallowed burial', 'normal').

% Found in: DIS, EXP, RTR
card_name('hallowed fountain', 'Hallowed Fountain').
card_type('hallowed fountain', 'Land — Plains Island').
card_types('hallowed fountain', ['Land']).
card_subtypes('hallowed fountain', ['Plains', 'Island']).
card_colors('hallowed fountain', []).
card_text('hallowed fountain', '({T}: Add {W} or {U} to your mana pool.)\nAs Hallowed Fountain enters the battlefield, you may pay 2 life. If you don\'t, Hallowed Fountain enters the battlefield tapped.').
card_layout('hallowed fountain', 'normal').

% Found in: ICE, MED
card_name('hallowed ground', 'Hallowed Ground').
card_type('hallowed ground', 'Enchantment').
card_types('hallowed ground', ['Enchantment']).
card_subtypes('hallowed ground', []).
card_colors('hallowed ground', ['W']).
card_text('hallowed ground', '{W}{W}: Return target nonsnow land you control to its owner\'s hand.').
card_mana_cost('hallowed ground', ['1', 'W']).
card_cmc('hallowed ground', 2).
card_layout('hallowed ground', 'normal').

% Found in: ODY
card_name('hallowed healer', 'Hallowed Healer').
card_type('hallowed healer', 'Creature — Human Cleric').
card_types('hallowed healer', ['Creature']).
card_subtypes('hallowed healer', ['Human', 'Cleric']).
card_colors('hallowed healer', ['W']).
card_text('hallowed healer', '{T}: Prevent the next 2 damage that would be dealt to target creature or player this turn.\nThreshold — {T}: Prevent the next 4 damage that would be dealt to target creature or player this turn. Activate this ability only if seven or more cards are in your graveyard.').
card_mana_cost('hallowed healer', ['2', 'W']).
card_cmc('hallowed healer', 3).
card_layout('hallowed healer', 'normal').
card_power('hallowed healer', 1).
card_toughness('hallowed healer', 1).

% Found in: ORI
card_name('hallowed moonlight', 'Hallowed Moonlight').
card_type('hallowed moonlight', 'Instant').
card_types('hallowed moonlight', ['Instant']).
card_subtypes('hallowed moonlight', []).
card_colors('hallowed moonlight', ['W']).
card_text('hallowed moonlight', 'Until end of turn, if a creature would enter the battlefield and it wasn\'t cast, exile it instead.\nDraw a card.').
card_mana_cost('hallowed moonlight', ['1', 'W']).
card_cmc('hallowed moonlight', 2).
card_layout('hallowed moonlight', 'normal').

% Found in: C14
card_name('hallowed spiritkeeper', 'Hallowed Spiritkeeper').
card_type('hallowed spiritkeeper', 'Creature — Avatar').
card_types('hallowed spiritkeeper', ['Creature']).
card_subtypes('hallowed spiritkeeper', ['Avatar']).
card_colors('hallowed spiritkeeper', ['W']).
card_text('hallowed spiritkeeper', 'Vigilance\nWhen Hallowed Spiritkeeper dies, put X 1/1 white Spirit creature tokens with flying onto the battlefield, where X is the number of creature cards in your graveyard.').
card_mana_cost('hallowed spiritkeeper', ['1', 'W', 'W']).
card_cmc('hallowed spiritkeeper', 3).
card_layout('hallowed spiritkeeper', 'normal').
card_power('hallowed spiritkeeper', 3).
card_toughness('hallowed spiritkeeper', 2).

% Found in: ICE
card_name('halls of mist', 'Halls of Mist').
card_type('halls of mist', 'Land').
card_types('halls of mist', ['Land']).
card_subtypes('halls of mist', []).
card_colors('halls of mist', []).
card_text('halls of mist', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nCreatures that attacked during their controller\'s last turn can\'t attack.').
card_layout('halls of mist', 'normal').
card_reserved('halls of mist').

% Found in: ZEN
card_name('halo hunter', 'Halo Hunter').
card_type('halo hunter', 'Creature — Demon').
card_types('halo hunter', ['Creature']).
card_subtypes('halo hunter', ['Demon']).
card_colors('halo hunter', ['B']).
card_text('halo hunter', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nWhen Halo Hunter enters the battlefield, destroy target Angel.').
card_mana_cost('halo hunter', ['2', 'B', 'B', 'B']).
card_cmc('halo hunter', 5).
card_layout('halo hunter', 'normal').
card_power('halo hunter', 6).
card_toughness('halo hunter', 3).

% Found in: SOM
card_name('halt order', 'Halt Order').
card_type('halt order', 'Instant').
card_types('halt order', ['Instant']).
card_subtypes('halt order', []).
card_colors('halt order', ['U']).
card_text('halt order', 'Counter target artifact spell.\nDraw a card.').
card_mana_cost('halt order', ['2', 'U']).
card_cmc('halt order', 3).
card_layout('halt order', 'normal').

% Found in: ISD
card_name('hamlet captain', 'Hamlet Captain').
card_type('hamlet captain', 'Creature — Human Warrior').
card_types('hamlet captain', ['Creature']).
card_subtypes('hamlet captain', ['Human', 'Warrior']).
card_colors('hamlet captain', ['G']).
card_text('hamlet captain', 'Whenever Hamlet Captain attacks or blocks, other Human creatures you control get +1/+1 until end of turn.').
card_mana_cost('hamlet captain', ['1', 'G']).
card_cmc('hamlet captain', 2).
card_layout('hamlet captain', 'normal').
card_power('hamlet captain', 2).
card_toughness('hamlet captain', 2).

% Found in: LRW, M13, pMEI
card_name('hamletback goliath', 'Hamletback Goliath').
card_type('hamletback goliath', 'Creature — Giant Warrior').
card_types('hamletback goliath', ['Creature']).
card_subtypes('hamletback goliath', ['Giant', 'Warrior']).
card_colors('hamletback goliath', ['R']).
card_text('hamletback goliath', 'Whenever another creature enters the battlefield, you may put X +1/+1 counters on Hamletback Goliath, where X is that creature\'s power.').
card_mana_cost('hamletback goliath', ['6', 'R']).
card_cmc('hamletback goliath', 7).
card_layout('hamletback goliath', 'normal').
card_power('hamletback goliath', 6).
card_toughness('hamletback goliath', 6).

% Found in: MMQ
card_name('hammer mage', 'Hammer Mage').
card_type('hammer mage', 'Creature — Human Spellshaper').
card_types('hammer mage', ['Creature']).
card_subtypes('hammer mage', ['Human', 'Spellshaper']).
card_colors('hammer mage', ['R']).
card_text('hammer mage', '{X}{R}, {T}, Discard a card: Destroy all artifacts with converted mana cost X or less.').
card_mana_cost('hammer mage', ['1', 'R']).
card_cmc('hammer mage', 2).
card_layout('hammer mage', 'normal').
card_power('hammer mage', 1).
card_toughness('hammer mage', 1).

% Found in: 6ED, 8ED, MIR, PD2, pJGP
card_name('hammer of bogardan', 'Hammer of Bogardan').
card_type('hammer of bogardan', 'Sorcery').
card_types('hammer of bogardan', ['Sorcery']).
card_subtypes('hammer of bogardan', []).
card_colors('hammer of bogardan', ['R']).
card_text('hammer of bogardan', 'Hammer of Bogardan deals 3 damage to target creature or player.\n{2}{R}{R}{R}: Return Hammer of Bogardan from your graveyard to your hand. Activate this ability only during your upkeep.').
card_mana_cost('hammer of bogardan', ['1', 'R', 'R']).
card_cmc('hammer of bogardan', 3).
card_layout('hammer of bogardan', 'normal').

% Found in: THS
card_name('hammer of purphoros', 'Hammer of Purphoros').
card_type('hammer of purphoros', 'Legendary Enchantment Artifact').
card_types('hammer of purphoros', ['Enchantment', 'Artifact']).
card_subtypes('hammer of purphoros', []).
card_supertypes('hammer of purphoros', ['Legendary']).
card_colors('hammer of purphoros', ['R']).
card_text('hammer of purphoros', 'Creatures you control have haste.\n{2}{R}, {T}, Sacrifice a land: Put a 3/3 colorless Golem enchantment artifact creature token onto the battlefield.').
card_mana_cost('hammer of purphoros', ['1', 'R', 'R']).
card_cmc('hammer of purphoros', 3).
card_layout('hammer of purphoros', 'normal').

% Found in: WWK
card_name('hammer of ruin', 'Hammer of Ruin').
card_type('hammer of ruin', 'Artifact — Equipment').
card_types('hammer of ruin', ['Artifact']).
card_subtypes('hammer of ruin', ['Equipment']).
card_colors('hammer of ruin', []).
card_text('hammer of ruin', 'Equipped creature gets +2/+0.\nWhenever equipped creature deals combat damage to a player, you may destroy target Equipment that player controls.\nEquip {2}').
card_mana_cost('hammer of ruin', ['2']).
card_cmc('hammer of ruin', 2).
card_layout('hammer of ruin', 'normal').

% Found in: RAV
card_name('hammerfist giant', 'Hammerfist Giant').
card_type('hammerfist giant', 'Creature — Giant Warrior').
card_types('hammerfist giant', ['Creature']).
card_subtypes('hammerfist giant', ['Giant', 'Warrior']).
card_colors('hammerfist giant', ['R']).
card_text('hammerfist giant', '{T}: Hammerfist Giant deals 4 damage to each creature without flying and each player.').
card_mana_cost('hammerfist giant', ['4', 'R', 'R']).
card_cmc('hammerfist giant', 6).
card_layout('hammerfist giant', 'normal').
card_power('hammerfist giant', 5).
card_toughness('hammerfist giant', 4).

% Found in: M15
card_name('hammerhand', 'Hammerhand').
card_type('hammerhand', 'Enchantment — Aura').
card_types('hammerhand', ['Enchantment']).
card_subtypes('hammerhand', ['Aura']).
card_colors('hammerhand', ['R']).
card_text('hammerhand', 'Enchant creature\nWhen Hammerhand enters the battlefield, target creature can\'t block this turn.\nEnchanted creature gets +1/+1 and has haste. (It can attack and {T} no matter when it came under your control.)').
card_mana_cost('hammerhand', ['R']).
card_cmc('hammerhand', 1).
card_layout('hammerhand', 'normal').

% Found in: STH, TPR
card_name('hammerhead shark', 'Hammerhead Shark').
card_type('hammerhead shark', 'Creature — Fish').
card_types('hammerhead shark', ['Creature']).
card_subtypes('hammerhead shark', ['Fish']).
card_colors('hammerhead shark', ['U']).
card_text('hammerhead shark', 'Hammerhead Shark can\'t attack unless defending player controls an Island.').
card_mana_cost('hammerhead shark', ['1', 'U']).
card_cmc('hammerhead shark', 2).
card_layout('hammerhead shark', 'normal').
card_power('hammerhead shark', 2).
card_toughness('hammerhead shark', 3).

% Found in: LEG, ME3
card_name('hammerheim', 'Hammerheim').
card_type('hammerheim', 'Legendary Land').
card_types('hammerheim', ['Land']).
card_subtypes('hammerheim', []).
card_supertypes('hammerheim', ['Legendary']).
card_colors('hammerheim', []).
card_text('hammerheim', '{T}: Add {R} to your mana pool.\n{T}: Target creature loses all landwalk abilities until end of turn.').
card_layout('hammerheim', 'normal').

% Found in: MMA, PLC
card_name('hammerheim deadeye', 'Hammerheim Deadeye').
card_type('hammerheim deadeye', 'Creature — Giant Warrior').
card_types('hammerheim deadeye', ['Creature']).
card_subtypes('hammerheim deadeye', ['Giant', 'Warrior']).
card_colors('hammerheim deadeye', ['R']).
card_text('hammerheim deadeye', 'Echo {5}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Hammerheim Deadeye enters the battlefield, destroy target creature with flying.').
card_mana_cost('hammerheim deadeye', ['3', 'R']).
card_cmc('hammerheim deadeye', 4).
card_layout('hammerheim deadeye', 'normal').
card_power('hammerheim deadeye', 3).
card_toughness('hammerheim deadeye', 3).

% Found in: CHK, MMA
card_name('hana kami', 'Hana Kami').
card_type('hana kami', 'Creature — Spirit').
card_types('hana kami', ['Creature']).
card_subtypes('hana kami', ['Spirit']).
card_colors('hana kami', ['G']).
card_text('hana kami', '{1}{G}, Sacrifice Hana Kami: Return target Arcane card from your graveyard to your hand.').
card_mana_cost('hana kami', ['G']).
card_cmc('hana kami', 1).
card_layout('hana kami', 'normal').
card_power('hana kami', 1).
card_toughness('hana kami', 1).

% Found in: CHK
card_name('hanabi blast', 'Hanabi Blast').
card_type('hanabi blast', 'Instant').
card_types('hanabi blast', ['Instant']).
card_subtypes('hanabi blast', []).
card_colors('hanabi blast', ['R']).
card_text('hanabi blast', 'Hanabi Blast deals 2 damage to target creature or player. Return Hanabi Blast to its owner\'s hand, then discard a card at random.').
card_mana_cost('hanabi blast', ['1', 'R', 'R']).
card_cmc('hanabi blast', 3).
card_layout('hanabi blast', 'normal').

% Found in: SOK
card_name('hand of cruelty', 'Hand of Cruelty').
card_type('hand of cruelty', 'Creature — Human Samurai').
card_types('hand of cruelty', ['Creature']).
card_subtypes('hand of cruelty', ['Human', 'Samurai']).
card_colors('hand of cruelty', ['B']).
card_text('hand of cruelty', 'Protection from white\nBushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_mana_cost('hand of cruelty', ['B', 'B']).
card_cmc('hand of cruelty', 2).
card_layout('hand of cruelty', 'normal').
card_power('hand of cruelty', 2).
card_toughness('hand of cruelty', 2).

% Found in: PO2, POR, S00, S99
card_name('hand of death', 'Hand of Death').
card_type('hand of death', 'Sorcery').
card_types('hand of death', ['Sorcery']).
card_subtypes('hand of death', []).
card_colors('hand of death', ['B']).
card_text('hand of death', 'Destroy target nonblack creature.').
card_mana_cost('hand of death', ['2', 'B']).
card_cmc('hand of death', 3).
card_layout('hand of death', 'normal').

% Found in: ROE
card_name('hand of emrakul', 'Hand of Emrakul').
card_type('hand of emrakul', 'Creature — Eldrazi').
card_types('hand of emrakul', ['Creature']).
card_subtypes('hand of emrakul', ['Eldrazi']).
card_colors('hand of emrakul', []).
card_text('hand of emrakul', 'You may sacrifice four Eldrazi Spawn rather than pay Hand of Emrakul\'s mana cost.\nAnnihilator 1 (Whenever this creature attacks, defending player sacrifices a permanent.)').
card_mana_cost('hand of emrakul', ['9']).
card_cmc('hand of emrakul', 9).
card_layout('hand of emrakul', 'normal').
card_power('hand of emrakul', 7).
card_toughness('hand of emrakul', 7).

% Found in: SOK
card_name('hand of honor', 'Hand of Honor').
card_type('hand of honor', 'Creature — Human Samurai').
card_types('hand of honor', ['Creature']).
card_subtypes('hand of honor', ['Human', 'Samurai']).
card_colors('hand of honor', ['W']).
card_text('hand of honor', 'Protection from black\nBushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_mana_cost('hand of honor', ['W', 'W']).
card_cmc('hand of honor', 2).
card_layout('hand of honor', 'normal').
card_power('hand of honor', 2).
card_toughness('hand of honor', 2).

% Found in: FEM, MED
card_name('hand of justice', 'Hand of Justice').
card_type('hand of justice', 'Creature — Avatar').
card_types('hand of justice', ['Creature']).
card_subtypes('hand of justice', ['Avatar']).
card_colors('hand of justice', ['W']).
card_text('hand of justice', '{T}, Tap three untapped white creatures you control: Destroy target creature.').
card_mana_cost('hand of justice', ['5', 'W']).
card_cmc('hand of justice', 6).
card_layout('hand of justice', 'normal').
card_power('hand of justice', 2).
card_toughness('hand of justice', 6).
card_reserved('hand of justice').

% Found in: DTK
card_name('hand of silumgar', 'Hand of Silumgar').
card_type('hand of silumgar', 'Creature — Human Warrior').
card_types('hand of silumgar', ['Creature']).
card_subtypes('hand of silumgar', ['Human', 'Warrior']).
card_colors('hand of silumgar', ['B']).
card_text('hand of silumgar', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_mana_cost('hand of silumgar', ['1', 'B']).
card_cmc('hand of silumgar', 2).
card_layout('hand of silumgar', 'normal').
card_power('hand of silumgar', 2).
card_toughness('hand of silumgar', 1).

% Found in: SOM
card_name('hand of the praetors', 'Hand of the Praetors').
card_type('hand of the praetors', 'Creature — Zombie').
card_types('hand of the praetors', ['Creature']).
card_subtypes('hand of the praetors', ['Zombie']).
card_colors('hand of the praetors', ['B']).
card_text('hand of the praetors', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nOther creatures you control with infect get +1/+1.\nWhenever you cast a creature spell with infect, target player gets a poison counter.').
card_mana_cost('hand of the praetors', ['3', 'B']).
card_cmc('hand of the praetors', 4).
card_layout('hand of the praetors', 'normal').
card_power('hand of the praetors', 3).
card_toughness('hand of the praetors', 2).

% Found in: TMP
card_name('hand to hand', 'Hand to Hand').
card_type('hand to hand', 'Enchantment').
card_types('hand to hand', ['Enchantment']).
card_subtypes('hand to hand', []).
card_colors('hand to hand', ['R']).
card_text('hand to hand', 'During combat, players can\'t cast instant spells or activate abilities that aren\'t mana abilities.').
card_mana_cost('hand to hand', ['2', 'R']).
card_cmc('hand to hand', 3).
card_layout('hand to hand', 'normal').

% Found in: UGL
card_name('handcuffs', 'Handcuffs').
card_type('handcuffs', 'Enchantment').
card_types('handcuffs', ['Enchantment']).
card_subtypes('handcuffs', []).
card_colors('handcuffs', ['B']).
card_text('handcuffs', 'Target player keeps both hands in contact with each other. If he or she does not, sacrifice Handcuffs and that player sacrifices three cards in play.').
card_mana_cost('handcuffs', ['3', 'B', 'B']).
card_cmc('handcuffs', 5).
card_layout('handcuffs', 'normal').

% Found in: GTC
card_name('hands of binding', 'Hands of Binding').
card_type('hands of binding', 'Sorcery').
card_types('hands of binding', ['Sorcery']).
card_subtypes('hands of binding', []).
card_colors('hands of binding', ['U']).
card_text('hands of binding', 'Tap target creature an opponent controls. That creature doesn\'t untap during its controller\'s next untap step.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_mana_cost('hands of binding', ['1', 'U']).
card_cmc('hands of binding', 2).
card_layout('hands of binding', 'normal').

% Found in: ORI
card_name('hangarback walker', 'Hangarback Walker').
card_type('hangarback walker', 'Artifact Creature — Construct').
card_types('hangarback walker', ['Artifact', 'Creature']).
card_subtypes('hangarback walker', ['Construct']).
card_colors('hangarback walker', []).
card_text('hangarback walker', 'Hangarback Walker enters the battlefield with X +1/+1 counters on it.\nWhen Hangarback Walker dies, put a 1/1 colorless Thopter artifact creature token with flying onto the battlefield for each +1/+1 counter on Hangarback Walker.\n{1}, {T}: Put a +1/+1 counter on Hangarback Walker.').
card_mana_cost('hangarback walker', ['X', 'X']).
card_cmc('hangarback walker', 0).
card_layout('hangarback walker', 'normal').
card_power('hangarback walker', 0).
card_toughness('hangarback walker', 0).

% Found in: CHK
card_name('hankyu', 'Hankyu').
card_type('hankyu', 'Artifact — Equipment').
card_types('hankyu', ['Artifact']).
card_subtypes('hankyu', ['Equipment']).
card_colors('hankyu', []).
card_text('hankyu', 'Equipped creature has \"{T}: Put an aim counter on Hankyu\" and \"{T}, Remove all aim counters from Hankyu: This creature deals damage to target creature or player equal to the number of aim counters removed this way.\"\nEquip {4} ({4}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('hankyu', ['1']).
card_cmc('hankyu', 1).
card_layout('hankyu', 'normal').

% Found in: VAN
card_name('hanna', 'Hanna').
card_type('hanna', 'Vanguard').
card_types('hanna', ['Vanguard']).
card_subtypes('hanna', []).
card_colors('hanna', []).
card_text('hanna', 'Spells you cast cost {1} less to cast.').
card_layout('hanna', 'vanguard').

% Found in: TMP
card_name('hanna\'s custody', 'Hanna\'s Custody').
card_type('hanna\'s custody', 'Enchantment').
card_types('hanna\'s custody', ['Enchantment']).
card_subtypes('hanna\'s custody', []).
card_colors('hanna\'s custody', ['W']).
card_text('hanna\'s custody', 'All artifacts have shroud. (They can\'t be the targets of spells or abilities.)').
card_mana_cost('hanna\'s custody', ['2', 'W']).
card_cmc('hanna\'s custody', 3).
card_layout('hanna\'s custody', 'normal').

% Found in: INV, pJGP
card_name('hanna, ship\'s navigator', 'Hanna, Ship\'s Navigator').
card_type('hanna, ship\'s navigator', 'Legendary Creature — Human Artificer').
card_types('hanna, ship\'s navigator', ['Creature']).
card_subtypes('hanna, ship\'s navigator', ['Human', 'Artificer']).
card_supertypes('hanna, ship\'s navigator', ['Legendary']).
card_colors('hanna, ship\'s navigator', ['W', 'U']).
card_text('hanna, ship\'s navigator', '{1}{W}{U}, {T}: Return target artifact or enchantment card from your graveyard to your hand.').
card_mana_cost('hanna, ship\'s navigator', ['1', 'W', 'U']).
card_cmc('hanna, ship\'s navigator', 3).
card_layout('hanna, ship\'s navigator', 'normal').
card_power('hanna, ship\'s navigator', 1).
card_toughness('hanna, ship\'s navigator', 2).

% Found in: AVR
card_name('hanweir lancer', 'Hanweir Lancer').
card_type('hanweir lancer', 'Creature — Human Knight').
card_types('hanweir lancer', ['Creature']).
card_subtypes('hanweir lancer', ['Human', 'Knight']).
card_colors('hanweir lancer', ['R']).
card_text('hanweir lancer', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Hanweir Lancer is paired with another creature, both creatures have first strike.').
card_mana_cost('hanweir lancer', ['2', 'R']).
card_cmc('hanweir lancer', 3).
card_layout('hanweir lancer', 'normal').
card_power('hanweir lancer', 2).
card_toughness('hanweir lancer', 2).

% Found in: ISD
card_name('hanweir watchkeep', 'Hanweir Watchkeep').
card_type('hanweir watchkeep', 'Creature — Human Warrior Werewolf').
card_types('hanweir watchkeep', ['Creature']).
card_subtypes('hanweir watchkeep', ['Human', 'Warrior', 'Werewolf']).
card_colors('hanweir watchkeep', ['R']).
card_text('hanweir watchkeep', 'Defender\nAt the beginning of each upkeep, if no spells were cast last turn, transform Hanweir Watchkeep.').
card_mana_cost('hanweir watchkeep', ['2', 'R']).
card_cmc('hanweir watchkeep', 3).
card_layout('hanweir watchkeep', 'double-faced').
card_power('hanweir watchkeep', 1).
card_toughness('hanweir watchkeep', 5).
card_sides('hanweir watchkeep', 'bane of hanweir').

% Found in: JUD
card_name('hapless researcher', 'Hapless Researcher').
card_type('hapless researcher', 'Creature — Human Wizard').
card_types('hapless researcher', ['Creature']).
card_subtypes('hapless researcher', ['Human', 'Wizard']).
card_colors('hapless researcher', ['U']).
card_text('hapless researcher', 'Sacrifice Hapless Researcher: Draw a card, then discard a card.').
card_mana_cost('hapless researcher', ['U']).
card_cmc('hapless researcher', 1).
card_layout('hapless researcher', 'normal').
card_power('hapless researcher', 1).
card_toughness('hapless researcher', 1).

% Found in: WWK
card_name('harabaz druid', 'Harabaz Druid').
card_type('harabaz druid', 'Creature — Human Druid Ally').
card_types('harabaz druid', ['Creature']).
card_subtypes('harabaz druid', ['Human', 'Druid', 'Ally']).
card_colors('harabaz druid', ['G']).
card_text('harabaz druid', '{T}: Add X mana of any one color to your mana pool, where X is the number of Allies you control.').
card_mana_cost('harabaz druid', ['1', 'G']).
card_cmc('harabaz druid', 2).
card_layout('harabaz druid', 'normal').
card_power('harabaz druid', 0).
card_toughness('harabaz druid', 1).

% Found in: MIR
card_name('harbinger of night', 'Harbinger of Night').
card_type('harbinger of night', 'Creature — Spirit').
card_types('harbinger of night', ['Creature']).
card_subtypes('harbinger of night', ['Spirit']).
card_colors('harbinger of night', ['B']).
card_text('harbinger of night', 'At the beginning of your upkeep, put a -1/-1 counter on each creature.').
card_mana_cost('harbinger of night', ['2', 'B', 'B']).
card_cmc('harbinger of night', 4).
card_layout('harbinger of night', 'normal').
card_power('harbinger of night', 2).
card_toughness('harbinger of night', 3).
card_reserved('harbinger of night').

% Found in: BOK
card_name('harbinger of spring', 'Harbinger of Spring').
card_type('harbinger of spring', 'Creature — Spirit').
card_types('harbinger of spring', ['Creature']).
card_subtypes('harbinger of spring', ['Spirit']).
card_colors('harbinger of spring', ['G']).
card_text('harbinger of spring', 'Protection from non-Spirit creatures\nSoulshift 4 (When this creature dies, you may return target Spirit card with converted mana cost 4 or less from your graveyard to your hand.)').
card_mana_cost('harbinger of spring', ['4', 'G']).
card_cmc('harbinger of spring', 5).
card_layout('harbinger of spring', 'normal').
card_power('harbinger of spring', 2).
card_toughness('harbinger of spring', 1).

% Found in: DTK, pMEI
card_name('harbinger of the hunt', 'Harbinger of the Hunt').
card_type('harbinger of the hunt', 'Creature — Dragon').
card_types('harbinger of the hunt', ['Creature']).
card_subtypes('harbinger of the hunt', ['Dragon']).
card_colors('harbinger of the hunt', ['R', 'G']).
card_text('harbinger of the hunt', 'Flying\n{2}{R}: Harbinger of the Hunt deals 1 damage to each creature without flying.\n{2}{G}: Harbinger of the Hunt deals 1 damage to each other creature with flying.').
card_mana_cost('harbinger of the hunt', ['3', 'R', 'G']).
card_cmc('harbinger of the hunt', 5).
card_layout('harbinger of the hunt', 'normal').
card_power('harbinger of the hunt', 5).
card_toughness('harbinger of the hunt', 3).

% Found in: ORI
card_name('harbinger of the tides', 'Harbinger of the Tides').
card_type('harbinger of the tides', 'Creature — Merfolk Wizard').
card_types('harbinger of the tides', ['Creature']).
card_subtypes('harbinger of the tides', ['Merfolk', 'Wizard']).
card_colors('harbinger of the tides', ['U']).
card_text('harbinger of the tides', 'You may cast Harbinger of the Tides as though it had flash if you pay {2} more to cast it. (You may cast it any time you could cast an instant.)\nWhen Harbinger of the Tides enters the battlefield, you may return target tapped creature an opponent controls to its owner\'s hand.').
card_mana_cost('harbinger of the tides', ['U', 'U']).
card_cmc('harbinger of the tides', 2).
card_layout('harbinger of the tides', 'normal').
card_power('harbinger of the tides', 2).
card_toughness('harbinger of the tides', 2).

% Found in: M13
card_name('harbor bandit', 'Harbor Bandit').
card_type('harbor bandit', 'Creature — Human Rogue').
card_types('harbor bandit', ['Creature']).
card_subtypes('harbor bandit', ['Human', 'Rogue']).
card_colors('harbor bandit', ['B']).
card_text('harbor bandit', 'Harbor Bandit gets +1/+1 as long as you control an Island.\n{1}{U}: Harbor Bandit can\'t be blocked this turn.').
card_mana_cost('harbor bandit', ['2', 'B']).
card_cmc('harbor bandit', 3).
card_layout('harbor bandit', 'normal').
card_power('harbor bandit', 2).
card_toughness('harbor bandit', 2).

% Found in: MIR
card_name('harbor guardian', 'Harbor Guardian').
card_type('harbor guardian', 'Creature — Gargoyle').
card_types('harbor guardian', ['Creature']).
card_subtypes('harbor guardian', ['Gargoyle']).
card_colors('harbor guardian', ['W', 'U']).
card_text('harbor guardian', 'Reach (This creature can block creatures with flying.)\nWhenever Harbor Guardian attacks, defending player may draw a card.').
card_mana_cost('harbor guardian', ['2', 'W', 'U']).
card_cmc('harbor guardian', 4).
card_layout('harbor guardian', 'normal').
card_power('harbor guardian', 3).
card_toughness('harbor guardian', 4).

% Found in: M11, M12, M13
card_name('harbor serpent', 'Harbor Serpent').
card_type('harbor serpent', 'Creature — Serpent').
card_types('harbor serpent', ['Creature']).
card_subtypes('harbor serpent', ['Serpent']).
card_colors('harbor serpent', ['U']).
card_text('harbor serpent', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nHarbor Serpent can\'t attack unless there are five or more Islands on the battlefield.').
card_mana_cost('harbor serpent', ['4', 'U', 'U']).
card_cmc('harbor serpent', 6).
card_layout('harbor serpent', 'normal').
card_power('harbor serpent', 5).
card_toughness('harbor serpent', 5).

% Found in: DTK
card_name('hardened berserker', 'Hardened Berserker').
card_type('hardened berserker', 'Creature — Human Berserker').
card_types('hardened berserker', ['Creature']).
card_subtypes('hardened berserker', ['Human', 'Berserker']).
card_colors('hardened berserker', ['R']).
card_text('hardened berserker', 'Whenever Hardened Berserker attacks, the next spell you cast this turn costs {1} less to cast.').
card_mana_cost('hardened berserker', ['2', 'R']).
card_cmc('hardened berserker', 3).
card_layout('hardened berserker', 'normal').
card_power('hardened berserker', 3).
card_toughness('hardened berserker', 2).

% Found in: KTK, pPRE
card_name('hardened scales', 'Hardened Scales').
card_type('hardened scales', 'Enchantment').
card_types('hardened scales', ['Enchantment']).
card_subtypes('hardened scales', []).
card_colors('hardened scales', ['G']).
card_text('hardened scales', 'If one or more +1/+1 counters would be placed on a creature you control, that many plus one +1/+1 counters are placed on it instead.').
card_mana_cost('hardened scales', ['G']).
card_cmc('hardened scales', 1).
card_layout('hardened scales', 'normal').

% Found in: DDG, M10
card_name('harm\'s way', 'Harm\'s Way').
card_type('harm\'s way', 'Instant').
card_types('harm\'s way', ['Instant']).
card_subtypes('harm\'s way', []).
card_colors('harm\'s way', ['W']).
card_text('harm\'s way', 'The next 2 damage that a source of your choice would deal to you and/or permanents you control this turn is dealt to target creature or player instead.').
card_mana_cost('harm\'s way', ['W']).
card_cmc('harm\'s way', 1).
card_layout('harm\'s way', 'normal').

% Found in: 6ED, MIR
card_name('harmattan efreet', 'Harmattan Efreet').
card_type('harmattan efreet', 'Creature — Efreet').
card_types('harmattan efreet', ['Creature']).
card_subtypes('harmattan efreet', ['Efreet']).
card_colors('harmattan efreet', ['U']).
card_text('harmattan efreet', 'Flying\n{1}{U}{U}: Target creature gains flying until end of turn.').
card_mana_cost('harmattan efreet', ['2', 'U', 'U']).
card_cmc('harmattan efreet', 4).
card_layout('harmattan efreet', 'normal').
card_power('harmattan efreet', 2).
card_toughness('harmattan efreet', 2).

% Found in: ROE
card_name('harmless assault', 'Harmless Assault').
card_type('harmless assault', 'Instant').
card_types('harmless assault', ['Instant']).
card_subtypes('harmless assault', []).
card_colors('harmless assault', ['W']).
card_text('harmless assault', 'Prevent all combat damage that would be dealt this turn by attacking creatures.').
card_mana_cost('harmless assault', ['2', 'W', 'W']).
card_cmc('harmless assault', 4).
card_layout('harmless assault', 'normal').

% Found in: ULG
card_name('harmonic convergence', 'Harmonic Convergence').
card_type('harmonic convergence', 'Instant').
card_types('harmonic convergence', ['Instant']).
card_subtypes('harmonic convergence', []).
card_colors('harmonic convergence', ['G']).
card_text('harmonic convergence', 'Put all enchantments on top of their owners\' libraries.').
card_mana_cost('harmonic convergence', ['2', 'G']).
card_cmc('harmonic convergence', 3).
card_layout('harmonic convergence', 'normal').

% Found in: TSP
card_name('harmonic sliver', 'Harmonic Sliver').
card_type('harmonic sliver', 'Creature — Sliver').
card_types('harmonic sliver', ['Creature']).
card_subtypes('harmonic sliver', ['Sliver']).
card_colors('harmonic sliver', ['W', 'G']).
card_text('harmonic sliver', 'All Slivers have \"When this permanent enters the battlefield, destroy target artifact or enchantment.\"').
card_mana_cost('harmonic sliver', ['1', 'G', 'W']).
card_cmc('harmonic sliver', 3).
card_layout('harmonic sliver', 'normal').
card_power('harmonic sliver', 1).
card_toughness('harmonic sliver', 1).

% Found in: ARC, C13, CMD, DD3_EVG, DD3_GVL, DDD, EVG, PLC, pMPR
card_name('harmonize', 'Harmonize').
card_type('harmonize', 'Sorcery').
card_types('harmonize', ['Sorcery']).
card_subtypes('harmonize', []).
card_colors('harmonize', ['G']).
card_text('harmonize', 'Draw three cards.').
card_mana_cost('harmonize', ['2', 'G', 'G']).
card_cmc('harmonize', 4).
card_layout('harmonize', 'normal').

% Found in: PO2
card_name('harmony of nature', 'Harmony of Nature').
card_type('harmony of nature', 'Sorcery').
card_types('harmony of nature', ['Sorcery']).
card_subtypes('harmony of nature', []).
card_colors('harmony of nature', ['G']).
card_text('harmony of nature', 'Tap any number of untapped creatures you control. You gain 4 life for each creature tapped this way.').
card_mana_cost('harmony of nature', ['2', 'G']).
card_cmc('harmony of nature', 3).
card_layout('harmony of nature', 'normal').

% Found in: JOU
card_name('harness by force', 'Harness by Force').
card_type('harness by force', 'Sorcery').
card_types('harness by force', ['Sorcery']).
card_subtypes('harness by force', []).
card_colors('harness by force', ['R']).
card_text('harness by force', 'Strive — Harness by Force costs {2}{R} more to cast for each target beyond the first.\nGain control of any number of target creatures until end of turn. Untap those creatures. They gain haste until end of turn.').
card_mana_cost('harness by force', ['1', 'R', 'R']).
card_cmc('harness by force', 3).
card_layout('harness by force', 'normal').

% Found in: LRW
card_name('harpoon sniper', 'Harpoon Sniper').
card_type('harpoon sniper', 'Creature — Merfolk Archer').
card_types('harpoon sniper', ['Creature']).
card_subtypes('harpoon sniper', ['Merfolk', 'Archer']).
card_colors('harpoon sniper', ['W']).
card_text('harpoon sniper', '{W}, {T}: Harpoon Sniper deals X damage to target attacking or blocking creature, where X is the number of Merfolk you control.').
card_mana_cost('harpoon sniper', ['2', 'W']).
card_cmc('harpoon sniper', 3).
card_layout('harpoon sniper', 'normal').
card_power('harpoon sniper', 2).
card_toughness('harpoon sniper', 2).

% Found in: GPT
card_name('harrier griffin', 'Harrier Griffin').
card_type('harrier griffin', 'Creature — Griffin').
card_types('harrier griffin', ['Creature']).
card_subtypes('harrier griffin', ['Griffin']).
card_colors('harrier griffin', ['W']).
card_text('harrier griffin', 'Flying\nAt the beginning of your upkeep, tap target creature.').
card_mana_cost('harrier griffin', ['5', 'W']).
card_cmc('harrier griffin', 6).
card_layout('harrier griffin', 'normal').
card_power('harrier griffin', 3).
card_toughness('harrier griffin', 3).

% Found in: C14, DDE, DDP, INV, TMP, TPR, ZEN, pMPR
card_name('harrow', 'Harrow').
card_type('harrow', 'Instant').
card_types('harrow', ['Instant']).
card_subtypes('harrow', []).
card_colors('harrow', ['G']).
card_text('harrow', 'As an additional cost to cast Harrow, sacrifice a land.\nSearch your library for up to two basic land cards and put them onto the battlefield. Then shuffle your library.').
card_mana_cost('harrow', ['2', 'G']).
card_cmc('harrow', 3).
card_layout('harrow', 'normal').

% Found in: DKA
card_name('harrowing journey', 'Harrowing Journey').
card_type('harrowing journey', 'Sorcery').
card_types('harrowing journey', ['Sorcery']).
card_subtypes('harrowing journey', []).
card_colors('harrowing journey', ['B']).
card_text('harrowing journey', 'Target player draws three cards and loses 3 life.').
card_mana_cost('harrowing journey', ['4', 'B']).
card_cmc('harrowing journey', 5).
card_layout('harrowing journey', 'normal').

% Found in: CHK
card_name('harsh deceiver', 'Harsh Deceiver').
card_type('harsh deceiver', 'Creature — Spirit').
card_types('harsh deceiver', ['Creature']).
card_subtypes('harsh deceiver', ['Spirit']).
card_colors('harsh deceiver', ['W']).
card_text('harsh deceiver', '{1}: Look at the top card of your library.\n{2}: Reveal the top card of your library. If it\'s a land card, untap Harsh Deceiver and it gets +1/+1 until end of turn. Activate this ability only once each turn.').
card_mana_cost('harsh deceiver', ['3', 'W']).
card_cmc('harsh deceiver', 4).
card_layout('harsh deceiver', 'normal').
card_power('harsh deceiver', 1).
card_toughness('harsh deceiver', 4).

% Found in: INV
card_name('harsh judgment', 'Harsh Judgment').
card_type('harsh judgment', 'Enchantment').
card_types('harsh judgment', ['Enchantment']).
card_subtypes('harsh judgment', []).
card_colors('harsh judgment', ['W']).
card_text('harsh judgment', 'As Harsh Judgment enters the battlefield, choose a color.\nIf an instant or sorcery spell of the chosen color would deal damage to you, it deals that damage to its controller instead.').
card_mana_cost('harsh judgment', ['2', 'W', 'W']).
card_cmc('harsh judgment', 4).
card_layout('harsh judgment', 'normal').

% Found in: ME4, POR
card_name('harsh justice', 'Harsh Justice').
card_type('harsh justice', 'Instant').
card_types('harsh justice', ['Instant']).
card_subtypes('harsh justice', []).
card_colors('harsh justice', ['W']).
card_text('harsh justice', 'Cast Harsh Justice only during the declare attackers step and only if you\'ve been attacked this step.\nThis turn, whenever an attacking creature deals combat damage to you, it deals that much damage to its controller.').
card_mana_cost('harsh justice', ['2', 'W']).
card_cmc('harsh justice', 3).
card_layout('harsh justice', 'normal').

% Found in: ONS
card_name('harsh mercy', 'Harsh Mercy').
card_type('harsh mercy', 'Sorcery').
card_types('harsh mercy', ['Sorcery']).
card_subtypes('harsh mercy', []).
card_colors('harsh mercy', ['W']).
card_text('harsh mercy', 'Each player chooses a creature type. Destroy all creatures that aren\'t of a type chosen this way. They can\'t be regenerated.').
card_mana_cost('harsh mercy', ['2', 'W']).
card_cmc('harsh mercy', 3).
card_layout('harsh mercy', 'normal').

% Found in: FRF
card_name('harsh sustenance', 'Harsh Sustenance').
card_type('harsh sustenance', 'Instant').
card_types('harsh sustenance', ['Instant']).
card_subtypes('harsh sustenance', []).
card_colors('harsh sustenance', ['W', 'B']).
card_text('harsh sustenance', 'Harsh Sustenance deals X damage to target creature or player and you gain X life, where X is the number of creatures you control.').
card_mana_cost('harsh sustenance', ['1', 'W', 'B']).
card_cmc('harsh sustenance', 3).
card_layout('harsh sustenance', 'normal').

% Found in: SOK
card_name('haru-onna', 'Haru-Onna').
card_type('haru-onna', 'Creature — Spirit').
card_types('haru-onna', ['Creature']).
card_subtypes('haru-onna', ['Spirit']).
card_colors('haru-onna', ['G']).
card_text('haru-onna', 'When Haru-Onna enters the battlefield, draw a card.\nWhenever you cast a Spirit or Arcane spell, you may return Haru-Onna to its owner\'s hand.').
card_mana_cost('haru-onna', ['3', 'G']).
card_cmc('haru-onna', 4).
card_layout('haru-onna', 'normal').
card_power('haru-onna', 2).
card_toughness('haru-onna', 1).

% Found in: EVE
card_name('harvest gwyllion', 'Harvest Gwyllion').
card_type('harvest gwyllion', 'Creature — Hag').
card_types('harvest gwyllion', ['Creature']).
card_subtypes('harvest gwyllion', ['Hag']).
card_colors('harvest gwyllion', ['W', 'B']).
card_text('harvest gwyllion', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)').
card_mana_cost('harvest gwyllion', ['2', 'W/B', 'W/B']).
card_cmc('harvest gwyllion', 4).
card_layout('harvest gwyllion', 'normal').
card_power('harvest gwyllion', 2).
card_toughness('harvest gwyllion', 4).

% Found in: NMS
card_name('harvest mage', 'Harvest Mage').
card_type('harvest mage', 'Creature — Human Spellshaper').
card_types('harvest mage', ['Creature']).
card_subtypes('harvest mage', ['Human', 'Spellshaper']).
card_colors('harvest mage', ['G']).
card_text('harvest mage', '{G}, {T}, Discard a card: Until end of turn, if you tap a land for mana, it produces one mana of a color of your choice instead of any other type and amount.').
card_mana_cost('harvest mage', ['G']).
card_cmc('harvest mage', 1).
card_layout('harvest mage', 'normal').
card_power('harvest mage', 1).
card_toughness('harvest mage', 1).

% Found in: ISD
card_name('harvest pyre', 'Harvest Pyre').
card_type('harvest pyre', 'Instant').
card_types('harvest pyre', ['Instant']).
card_subtypes('harvest pyre', []).
card_colors('harvest pyre', ['R']).
card_text('harvest pyre', 'As an additional cost to cast Harvest Pyre, exile X cards from your graveyard.\nHarvest Pyre deals X damage to target creature.').
card_mana_cost('harvest pyre', ['1', 'R']).
card_cmc('harvest pyre', 2).
card_layout('harvest pyre', 'normal').

% Found in: WTH
card_name('harvest wurm', 'Harvest Wurm').
card_type('harvest wurm', 'Creature — Wurm').
card_types('harvest wurm', ['Creature']).
card_subtypes('harvest wurm', ['Wurm']).
card_colors('harvest wurm', ['G']).
card_text('harvest wurm', 'When Harvest Wurm enters the battlefield, sacrifice it unless you return a basic land card from your graveyard to your hand.').
card_mana_cost('harvest wurm', ['1', 'G']).
card_cmc('harvest wurm', 2).
card_layout('harvest wurm', 'normal').
card_power('harvest wurm', 3).
card_toughness('harvest wurm', 2).

% Found in: JUD
card_name('harvester druid', 'Harvester Druid').
card_type('harvester druid', 'Creature — Human Druid').
card_types('harvester druid', ['Creature']).
card_subtypes('harvester druid', ['Human', 'Druid']).
card_colors('harvester druid', ['G']).
card_text('harvester druid', '{T}: Add to your mana pool one mana of any color that a land you control could produce.').
card_mana_cost('harvester druid', ['1', 'G']).
card_cmc('harvester druid', 2).
card_layout('harvester druid', 'normal').
card_power('harvester druid', 1).
card_toughness('harvester druid', 1).

% Found in: AVR
card_name('harvester of souls', 'Harvester of Souls').
card_type('harvester of souls', 'Creature — Demon').
card_types('harvester of souls', ['Creature']).
card_subtypes('harvester of souls', ['Demon']).
card_colors('harvester of souls', ['B']).
card_text('harvester of souls', 'Deathtouch\nWhenever another nontoken creature dies, you may draw a card.').
card_mana_cost('harvester of souls', ['4', 'B', 'B']).
card_cmc('harvester of souls', 6).
card_layout('harvester of souls', 'normal').
card_power('harvester of souls', 5).
card_toughness('harvester of souls', 5).

% Found in: JOU
card_name('harvestguard alseids', 'Harvestguard Alseids').
card_type('harvestguard alseids', 'Enchantment Creature — Nymph').
card_types('harvestguard alseids', ['Enchantment', 'Creature']).
card_subtypes('harvestguard alseids', ['Nymph']).
card_colors('harvestguard alseids', ['W']).
card_text('harvestguard alseids', 'Constellation — Whenever Harvestguard Alseids or another enchantment enters the battlefield under your control, prevent all damage that would be dealt to target creature this turn.').
card_mana_cost('harvestguard alseids', ['2', 'W']).
card_cmc('harvestguard alseids', 3).
card_layout('harvestguard alseids', 'normal').
card_power('harvestguard alseids', 2).
card_toughness('harvestguard alseids', 3).

% Found in: ARN, CHR, ME4
card_name('hasran ogress', 'Hasran Ogress').
card_type('hasran ogress', 'Creature — Ogre').
card_types('hasran ogress', ['Creature']).
card_subtypes('hasran ogress', ['Ogre']).
card_colors('hasran ogress', ['B']).
card_text('hasran ogress', 'Whenever Hasran Ogress attacks, it deals 3 damage to you unless you pay {2}.').
card_mana_cost('hasran ogress', ['B', 'B']).
card_cmc('hasran ogress', 2).
card_layout('hasran ogress', 'normal').
card_power('hasran ogress', 3).
card_toughness('hasran ogress', 2).

% Found in: EVE
card_name('hatchet bully', 'Hatchet Bully').
card_type('hatchet bully', 'Creature — Goblin Warrior').
card_types('hatchet bully', ['Creature']).
card_subtypes('hatchet bully', ['Goblin', 'Warrior']).
card_colors('hatchet bully', ['R']).
card_text('hatchet bully', '{2}{R}, {T}, Put a -1/-1 counter on a creature you control: Hatchet Bully deals 2 damage to target creature or player.').
card_mana_cost('hatchet bully', ['3', 'R']).
card_cmc('hatchet bully', 4).
card_layout('hatchet bully', 'normal').
card_power('hatchet bully', 3).
card_toughness('hatchet bully', 3).

% Found in: GPT
card_name('hatching plans', 'Hatching Plans').
card_type('hatching plans', 'Enchantment').
card_types('hatching plans', ['Enchantment']).
card_subtypes('hatching plans', []).
card_colors('hatching plans', ['U']).
card_text('hatching plans', 'When Hatching Plans is put into a graveyard from the battlefield, draw three cards.').
card_mana_cost('hatching plans', ['1', 'U']).
card_cmc('hatching plans', 2).
card_layout('hatching plans', 'normal').

% Found in: 10E, INV
card_name('hate weaver', 'Hate Weaver').
card_type('hate weaver', 'Creature — Zombie Wizard').
card_types('hate weaver', ['Creature']).
card_subtypes('hate weaver', ['Zombie', 'Wizard']).
card_colors('hate weaver', ['B']).
card_text('hate weaver', '{2}: Target blue or red creature gets +1/+0 until end of turn.').
card_mana_cost('hate weaver', ['1', 'B']).
card_cmc('hate weaver', 2).
card_layout('hate weaver', 'normal').
card_power('hate weaver', 2).
card_toughness('hate weaver', 1).

% Found in: EVE
card_name('hateflayer', 'Hateflayer').
card_type('hateflayer', 'Creature — Elemental').
card_types('hateflayer', ['Creature']).
card_subtypes('hateflayer', ['Elemental']).
card_colors('hateflayer', ['R']).
card_text('hateflayer', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\n{2}{R}, {Q}: Hateflayer deals damage equal to its power to target creature or player. ({Q} is the untap symbol.)').
card_mana_cost('hateflayer', ['5', 'R', 'R']).
card_cmc('hateflayer', 7).
card_layout('hateflayer', 'normal').
card_power('hateflayer', 5).
card_toughness('hateflayer', 5).

% Found in: EXO
card_name('hatred', 'Hatred').
card_type('hatred', 'Instant').
card_types('hatred', ['Instant']).
card_subtypes('hatred', []).
card_colors('hatred', ['B']).
card_text('hatred', 'As an additional cost to cast Hatred, pay X life.\nTarget creature gets +X/+0 until end of turn.').
card_mana_cost('hatred', ['3', 'B', 'B']).
card_cmc('hatred', 5).
card_layout('hatred', 'normal').
card_reserved('hatred').

% Found in: APC
card_name('haunted angel', 'Haunted Angel').
card_type('haunted angel', 'Creature — Angel').
card_types('haunted angel', ['Creature']).
card_subtypes('haunted angel', ['Angel']).
card_colors('haunted angel', ['W']).
card_text('haunted angel', 'Flying\nWhen Haunted Angel dies, exile Haunted Angel and each other player puts a 3/3 black Angel creature token with flying onto the battlefield.').
card_mana_cost('haunted angel', ['2', 'W']).
card_cmc('haunted angel', 3).
card_layout('haunted angel', 'normal').
card_power('haunted angel', 3).
card_toughness('haunted angel', 3).

% Found in: ONS
card_name('haunted cadaver', 'Haunted Cadaver').
card_type('haunted cadaver', 'Creature — Zombie').
card_types('haunted cadaver', ['Creature']).
card_subtypes('haunted cadaver', ['Zombie']).
card_colors('haunted cadaver', ['B']).
card_text('haunted cadaver', 'Whenever Haunted Cadaver deals combat damage to a player, you may sacrifice it. If you do, that player discards three cards.\nMorph {1}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('haunted cadaver', ['3', 'B']).
card_cmc('haunted cadaver', 4).
card_layout('haunted cadaver', 'normal').
card_power('haunted cadaver', 2).
card_toughness('haunted cadaver', 2).

% Found in: MMQ
card_name('haunted crossroads', 'Haunted Crossroads').
card_type('haunted crossroads', 'Enchantment').
card_types('haunted crossroads', ['Enchantment']).
card_subtypes('haunted crossroads', []).
card_colors('haunted crossroads', ['B']).
card_text('haunted crossroads', '{B}: Put target creature card from your graveyard on top of your library.').
card_mana_cost('haunted crossroads', ['2', 'B']).
card_cmc('haunted crossroads', 3).
card_layout('haunted crossroads', 'normal').

% Found in: C14, DKA
card_name('haunted fengraf', 'Haunted Fengraf').
card_type('haunted fengraf', 'Land').
card_types('haunted fengraf', ['Land']).
card_subtypes('haunted fengraf', []).
card_colors('haunted fengraf', []).
card_text('haunted fengraf', '{T}: Add {1} to your mana pool.\n{3}, {T}, Sacrifice Haunted Fengraf: Return a creature card at random from your graveyard to your hand.').
card_layout('haunted fengraf', 'normal').

% Found in: AVR
card_name('haunted guardian', 'Haunted Guardian').
card_type('haunted guardian', 'Artifact Creature — Construct').
card_types('haunted guardian', ['Artifact', 'Creature']).
card_subtypes('haunted guardian', ['Construct']).
card_colors('haunted guardian', []).
card_text('haunted guardian', 'Defender, first strike').
card_mana_cost('haunted guardian', ['2']).
card_cmc('haunted guardian', 2).
card_layout('haunted guardian', 'normal').
card_power('haunted guardian', 2).
card_toughness('haunted guardian', 1).

% Found in: M14, M15
card_name('haunted plate mail', 'Haunted Plate Mail').
card_type('haunted plate mail', 'Artifact — Equipment').
card_types('haunted plate mail', ['Artifact']).
card_subtypes('haunted plate mail', ['Equipment']).
card_colors('haunted plate mail', []).
card_text('haunted plate mail', 'Equipped creature gets +4/+4.\n{0}: Until end of turn, Haunted Plate Mail becomes a 4/4 Spirit artifact creature that\'s no longer an Equipment. Activate this ability only if you control no creatures.\nEquip {4} ({4}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('haunted plate mail', ['4']).
card_cmc('haunted plate mail', 4).
card_layout('haunted plate mail', 'normal').

% Found in: DGM
card_name('haunter of nightveil', 'Haunter of Nightveil').
card_type('haunter of nightveil', 'Creature — Spirit').
card_types('haunter of nightveil', ['Creature']).
card_subtypes('haunter of nightveil', ['Spirit']).
card_colors('haunter of nightveil', ['U', 'B']).
card_text('haunter of nightveil', 'Creatures your opponents control get -1/-0.').
card_mana_cost('haunter of nightveil', ['3', 'U', 'B']).
card_cmc('haunter of nightveil', 5).
card_layout('haunter of nightveil', 'normal').
card_power('haunter of nightveil', 3).
card_toughness('haunter of nightveil', 4).

% Found in: MIR
card_name('haunting apparition', 'Haunting Apparition').
card_type('haunting apparition', 'Creature — Spirit').
card_types('haunting apparition', ['Creature']).
card_subtypes('haunting apparition', ['Spirit']).
card_colors('haunting apparition', ['U', 'B']).
card_text('haunting apparition', 'Flying\nAs Haunting Apparition enters the battlefield, choose an opponent.\nHaunting Apparition\'s power is equal to 1 plus the number of green creature cards in the chosen player\'s graveyard.').
card_mana_cost('haunting apparition', ['1', 'U', 'B']).
card_cmc('haunting apparition', 3).
card_layout('haunting apparition', 'normal').
card_power('haunting apparition', '1+*').
card_toughness('haunting apparition', 2).

% Found in: M10, M11, ODY
card_name('haunting echoes', 'Haunting Echoes').
card_type('haunting echoes', 'Sorcery').
card_types('haunting echoes', ['Sorcery']).
card_subtypes('haunting echoes', []).
card_colors('haunting echoes', ['B']).
card_text('haunting echoes', 'Exile all cards from target player\'s graveyard other than basic land cards. For each card exiled this way, search that player\'s library for all cards with the same name as that card and exile them. Then that player shuffles his or her library.').
card_mana_cost('haunting echoes', ['3', 'B', 'B']).
card_cmc('haunting echoes', 5).
card_layout('haunting echoes', 'normal').

% Found in: TSP
card_name('haunting hymn', 'Haunting Hymn').
card_type('haunting hymn', 'Instant').
card_types('haunting hymn', ['Instant']).
card_subtypes('haunting hymn', []).
card_colors('haunting hymn', ['B']).
card_text('haunting hymn', 'Target player discards two cards. If you cast this spell during your main phase, that player discards four cards instead.').
card_mana_cost('haunting hymn', ['4', 'B', 'B']).
card_cmc('haunting hymn', 6).
card_layout('haunting hymn', 'normal').

% Found in: WTH
card_name('haunting misery', 'Haunting Misery').
card_type('haunting misery', 'Sorcery').
card_types('haunting misery', ['Sorcery']).
card_subtypes('haunting misery', []).
card_colors('haunting misery', ['B']).
card_text('haunting misery', 'As an additional cost to cast Haunting Misery, exile X creature cards from your graveyard.\nHaunting Misery deals X damage to target player.').
card_mana_cost('haunting misery', ['1', 'B', 'B']).
card_cmc('haunting misery', 3).
card_layout('haunting misery', 'normal').

% Found in: ATQ
card_name('haunting wind', 'Haunting Wind').
card_type('haunting wind', 'Enchantment').
card_types('haunting wind', ['Enchantment']).
card_subtypes('haunting wind', []).
card_colors('haunting wind', ['B']).
card_text('haunting wind', 'Whenever an artifact becomes tapped or a player activates an artifact\'s ability without {T} in its activation cost, Haunting Wind deals 1 damage to that artifact\'s controller.').
card_mana_cost('haunting wind', ['3', 'B']).
card_cmc('haunting wind', 4).
card_layout('haunting wind', 'normal').
card_reserved('haunting wind').

% Found in: DTK
card_name('haven of the spirit dragon', 'Haven of the Spirit Dragon').
card_type('haven of the spirit dragon', 'Land').
card_types('haven of the spirit dragon', ['Land']).
card_subtypes('haven of the spirit dragon', []).
card_colors('haven of the spirit dragon', []).
card_text('haven of the spirit dragon', '{T}: Add {1} to your mana pool.\n{T}: Add one mana of any color to your mana pool. Spend this mana only to cast a Dragon creature spell.\n{2}, {T}, Sacrifice Haven of the Spirit Dragon: Return target Dragon creature card or Ugin planeswalker card from your graveyard to your hand.').
card_layout('haven of the spirit dragon', 'normal').

% Found in: DKA
card_name('havengul lich', 'Havengul Lich').
card_type('havengul lich', 'Creature — Zombie Wizard').
card_types('havengul lich', ['Creature']).
card_subtypes('havengul lich', ['Zombie', 'Wizard']).
card_colors('havengul lich', ['U', 'B']).
card_text('havengul lich', '{1}: You may cast target creature card in a graveyard this turn. When you cast that card this turn, Havengul Lich gains all activated abilities of that card until end of turn.').
card_mana_cost('havengul lich', ['3', 'U', 'B']).
card_cmc('havengul lich', 5).
card_layout('havengul lich', 'normal').
card_power('havengul lich', 4).
card_toughness('havengul lich', 4).

% Found in: DKA
card_name('havengul runebinder', 'Havengul Runebinder').
card_type('havengul runebinder', 'Creature — Human Wizard').
card_types('havengul runebinder', ['Creature']).
card_subtypes('havengul runebinder', ['Human', 'Wizard']).
card_colors('havengul runebinder', ['U']).
card_text('havengul runebinder', '{2}{U}, {T}, Exile a creature card from your graveyard: Put a 2/2 black Zombie creature token onto the battlefield, then put a +1/+1 counter on each Zombie creature you control.').
card_mana_cost('havengul runebinder', ['2', 'U', 'U']).
card_cmc('havengul runebinder', 4).
card_layout('havengul runebinder', 'normal').
card_power('havengul runebinder', 2).
card_toughness('havengul runebinder', 2).

% Found in: AVR
card_name('havengul skaab', 'Havengul Skaab').
card_type('havengul skaab', 'Creature — Zombie Horror').
card_types('havengul skaab', ['Creature']).
card_subtypes('havengul skaab', ['Zombie', 'Horror']).
card_colors('havengul skaab', ['U']).
card_text('havengul skaab', 'Whenever Havengul Skaab attacks, return another creature you control to its owner\'s hand.').
card_mana_cost('havengul skaab', ['5', 'U']).
card_cmc('havengul skaab', 6).
card_layout('havengul skaab', 'normal').
card_power('havengul skaab', 4).
card_toughness('havengul skaab', 5).

% Found in: AVR
card_name('havengul vampire', 'Havengul Vampire').
card_type('havengul vampire', 'Creature — Vampire').
card_types('havengul vampire', ['Creature']).
card_subtypes('havengul vampire', ['Vampire']).
card_colors('havengul vampire', ['R']).
card_text('havengul vampire', 'Whenever Havengul Vampire deals combat damage to a player, put a +1/+1 counter on it.\nWhenever another creature dies, put a +1/+1 counter on Havengul Vampire.').
card_mana_cost('havengul vampire', ['3', 'R']).
card_cmc('havengul vampire', 4).
card_layout('havengul vampire', 'normal').
card_power('havengul vampire', 2).
card_toughness('havengul vampire', 2).

% Found in: 5ED, 6ED, BTD, C14, FEM, ME2
card_name('havenwood battleground', 'Havenwood Battleground').
card_type('havenwood battleground', 'Land').
card_types('havenwood battleground', ['Land']).
card_subtypes('havenwood battleground', []).
card_colors('havenwood battleground', []).
card_text('havenwood battleground', 'Havenwood Battleground enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\n{T}, Sacrifice Havenwood Battleground: Add {G}{G} to your mana pool.').
card_layout('havenwood battleground', 'normal').

% Found in: TSP
card_name('havenwood wurm', 'Havenwood Wurm').
card_type('havenwood wurm', 'Creature — Wurm').
card_types('havenwood wurm', ['Creature']).
card_subtypes('havenwood wurm', ['Wurm']).
card_colors('havenwood wurm', ['G']).
card_text('havenwood wurm', 'Flash (You may cast this spell any time you could cast an instant.)\nTrample').
card_mana_cost('havenwood wurm', ['6', 'G']).
card_cmc('havenwood wurm', 7).
card_layout('havenwood wurm', 'normal').
card_power('havenwood wurm', 5).
card_toughness('havenwood wurm', 6).

% Found in: TMP
card_name('havoc', 'Havoc').
card_type('havoc', 'Enchantment').
card_types('havoc', ['Enchantment']).
card_subtypes('havoc', []).
card_colors('havoc', ['R']).
card_text('havoc', 'Whenever an opponent casts a white spell, he or she loses 2 life.').
card_mana_cost('havoc', ['1', 'R']).
card_cmc('havoc', 2).
card_layout('havoc', 'normal').

% Found in: LGN
card_name('havoc demon', 'Havoc Demon').
card_type('havoc demon', 'Creature — Demon').
card_types('havoc demon', ['Creature']).
card_subtypes('havoc demon', ['Demon']).
card_colors('havoc demon', ['B']).
card_text('havoc demon', 'Flying\nWhen Havoc Demon dies, all creatures get -5/-5 until end of turn.').
card_mana_cost('havoc demon', ['5', 'B', 'B']).
card_cmc('havoc demon', 7).
card_layout('havoc demon', 'normal').
card_power('havoc demon', 5).
card_toughness('havoc demon', 5).

% Found in: RTR
card_name('havoc festival', 'Havoc Festival').
card_type('havoc festival', 'Enchantment').
card_types('havoc festival', ['Enchantment']).
card_subtypes('havoc festival', []).
card_colors('havoc festival', ['B', 'R']).
card_text('havoc festival', 'Players can\'t gain life.\nAt the beginning of each player\'s upkeep, that player loses half his or her life, rounded up.').
card_mana_cost('havoc festival', ['4', 'B', 'R']).
card_cmc('havoc festival', 6).
card_layout('havoc festival', 'normal').

% Found in: USG
card_name('hawkeater moth', 'Hawkeater Moth').
card_type('hawkeater moth', 'Creature — Insect').
card_types('hawkeater moth', ['Creature']).
card_subtypes('hawkeater moth', ['Insect']).
card_colors('hawkeater moth', ['G']).
card_text('hawkeater moth', 'Flying\nShroud (This creature can\'t be the target of spells or abilities.)').
card_mana_cost('hawkeater moth', ['3', 'G']).
card_cmc('hawkeater moth', 4).
card_layout('hawkeater moth', 'normal').
card_power('hawkeater moth', 1).
card_toughness('hawkeater moth', 2).

% Found in: HML
card_name('hazduhr the abbot', 'Hazduhr the Abbot').
card_type('hazduhr the abbot', 'Legendary Creature — Human Cleric').
card_types('hazduhr the abbot', ['Creature']).
card_subtypes('hazduhr the abbot', ['Human', 'Cleric']).
card_supertypes('hazduhr the abbot', ['Legendary']).
card_colors('hazduhr the abbot', ['W']).
card_text('hazduhr the abbot', '{X}, {T}: The next X damage that would be dealt this turn to target white creature you control is dealt to Hazduhr the Abbot instead.').
card_mana_cost('hazduhr the abbot', ['3', 'W', 'W']).
card_cmc('hazduhr the abbot', 5).
card_layout('hazduhr the abbot', 'normal').
card_power('hazduhr the abbot', 2).
card_toughness('hazduhr the abbot', 5).
card_reserved('hazduhr the abbot').

% Found in: ROE
card_name('haze frog', 'Haze Frog').
card_type('haze frog', 'Creature — Frog').
card_types('haze frog', ['Creature']).
card_subtypes('haze frog', ['Frog']).
card_colors('haze frog', ['G']).
card_text('haze frog', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Haze Frog enters the battlefield, prevent all combat damage that other creatures would deal this turn.').
card_mana_cost('haze frog', ['3', 'G', 'G']).
card_cmc('haze frog', 5).
card_layout('haze frog', 'normal').
card_power('haze frog', 2).
card_toughness('haze frog', 1).

% Found in: FUT
card_name('haze of rage', 'Haze of Rage').
card_type('haze of rage', 'Sorcery').
card_types('haze of rage', ['Sorcery']).
card_subtypes('haze of rage', []).
card_colors('haze of rage', ['R']).
card_text('haze of rage', 'Buyback {2} (You may pay an additional {2} as you cast this spell. If you do, put this card into your hand as it resolves.)\nCreatures you control get +1/+0 until end of turn.\nStorm (When you cast this spell, copy it for each spell cast before it this turn.)').
card_mana_cost('haze of rage', ['1', 'R']).
card_cmc('haze of rage', 2).
card_layout('haze of rage', 'normal').

% Found in: MIR
card_name('hazerider drake', 'Hazerider Drake').
card_type('hazerider drake', 'Creature — Drake').
card_types('hazerider drake', ['Creature']).
card_subtypes('hazerider drake', ['Drake']).
card_colors('hazerider drake', ['W', 'U']).
card_text('hazerider drake', 'Flying, protection from red').
card_mana_cost('hazerider drake', ['2', 'W', 'U']).
card_cmc('hazerider drake', 4).
card_layout('hazerider drake', 'normal').
card_power('hazerider drake', 2).
card_toughness('hazerider drake', 3).

% Found in: LEG, ME3
card_name('hazezon tamar', 'Hazezon Tamar').
card_type('hazezon tamar', 'Legendary Creature — Human Warrior').
card_types('hazezon tamar', ['Creature']).
card_subtypes('hazezon tamar', ['Human', 'Warrior']).
card_supertypes('hazezon tamar', ['Legendary']).
card_colors('hazezon tamar', ['W', 'R', 'G']).
card_text('hazezon tamar', 'When Hazezon Tamar enters the battlefield, put X 1/1 Sand Warrior creature tokens that are red, green, and white onto the battlefield at the beginning of your next upkeep, where X is the number of lands you control at that time.\nWhen Hazezon leaves the battlefield, exile all Sand Warriors.').
card_mana_cost('hazezon tamar', ['4', 'R', 'G', 'W']).
card_cmc('hazezon tamar', 7).
card_layout('hazezon tamar', 'normal').
card_power('hazezon tamar', 2).
card_toughness('hazezon tamar', 4).
card_reserved('hazezon tamar').

% Found in: PCY
card_name('hazy homunculus', 'Hazy Homunculus').
card_type('hazy homunculus', 'Creature — Homunculus Illusion').
card_types('hazy homunculus', ['Creature']).
card_subtypes('hazy homunculus', ['Homunculus', 'Illusion']).
card_colors('hazy homunculus', ['U']).
card_text('hazy homunculus', 'Hazy Homunculus can\'t be blocked as long as defending player controls an untapped land.').
card_mana_cost('hazy homunculus', ['1', 'U']).
card_cmc('hazy homunculus', 2).
card_layout('hazy homunculus', 'normal').
card_power('hazy homunculus', 1).
card_toughness('hazy homunculus', 1).

% Found in: CHK
card_name('he who hungers', 'He Who Hungers').
card_type('he who hungers', 'Legendary Creature — Spirit').
card_types('he who hungers', ['Creature']).
card_subtypes('he who hungers', ['Spirit']).
card_supertypes('he who hungers', ['Legendary']).
card_colors('he who hungers', ['B']).
card_text('he who hungers', 'Flying\n{1}, Sacrifice a Spirit: Target opponent reveals his or her hand. You choose a card from it. That player discards that card. Activate this ability only any time you could cast a sorcery.\nSoulshift 4 (When this creature dies, you may return target Spirit card with converted mana cost 4 or less from your graveyard to your hand.)').
card_mana_cost('he who hungers', ['4', 'B']).
card_cmc('he who hungers', 5).
card_layout('he who hungers', 'normal').
card_power('he who hungers', 3).
card_toughness('he who hungers', 2).

% Found in: 10E, ONS
card_name('head games', 'Head Games').
card_type('head games', 'Sorcery').
card_types('head games', ['Sorcery']).
card_subtypes('head games', []).
card_colors('head games', ['B']).
card_text('head games', 'Target opponent puts the cards from his or her hand on top of his or her library. Search that player\'s library for that many cards. The player puts those cards into his or her hand, then shuffles his or her library.').
card_mana_cost('head games', ['3', 'B', 'B']).
card_cmc('head games', 5).
card_layout('head games', 'normal').

% Found in: UNH
card_name('head to head', 'Head to Head').
card_type('head to head', 'Instant').
card_types('head to head', ['Instant']).
card_subtypes('head to head', []).
card_colors('head to head', ['W']).
card_text('head to head', 'You and target opponent play Seven Questions about the top card of that player\'s library. (That player looks at the card, then you ask up to six yes-or-no questions about the card that he or she answers truthfully. You guess the card\'s name—that\'s question seven—and the player reveals the card.) If you win, prevent all damage that would be dealt this turn by a source of your choice.').
card_mana_cost('head to head', ['W']).
card_cmc('head to head', 1).
card_layout('head to head', 'normal').

% Found in: ONS
card_name('headhunter', 'Headhunter').
card_type('headhunter', 'Creature — Human Cleric').
card_types('headhunter', ['Creature']).
card_subtypes('headhunter', ['Human', 'Cleric']).
card_colors('headhunter', ['B']).
card_text('headhunter', 'Whenever Headhunter deals combat damage to a player, that player discards a card.\nMorph {B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('headhunter', ['1', 'B']).
card_cmc('headhunter', 2).
card_layout('headhunter', 'normal').
card_power('headhunter', 1).
card_toughness('headhunter', 1).

% Found in: LEG
card_name('headless horseman', 'Headless Horseman').
card_type('headless horseman', 'Creature — Zombie Knight').
card_types('headless horseman', ['Creature']).
card_subtypes('headless horseman', ['Zombie', 'Knight']).
card_colors('headless horseman', ['B']).
card_text('headless horseman', '').
card_mana_cost('headless horseman', ['2', 'B']).
card_cmc('headless horseman', 3).
card_layout('headless horseman', 'normal').
card_power('headless horseman', 2).
card_toughness('headless horseman', 2).

% Found in: DKA
card_name('headless skaab', 'Headless Skaab').
card_type('headless skaab', 'Creature — Zombie Warrior').
card_types('headless skaab', ['Creature']).
card_subtypes('headless skaab', ['Zombie', 'Warrior']).
card_colors('headless skaab', ['U']).
card_text('headless skaab', 'As an additional cost to cast Headless Skaab, exile a creature card from your graveyard.\nHeadless Skaab enters the battlefield tapped.').
card_mana_cost('headless skaab', ['2', 'U']).
card_cmc('headless skaab', 3).
card_layout('headless skaab', 'normal').
card_power('headless skaab', 3).
card_toughness('headless skaab', 6).

% Found in: USG
card_name('headlong rush', 'Headlong Rush').
card_type('headlong rush', 'Instant').
card_types('headlong rush', ['Instant']).
card_subtypes('headlong rush', []).
card_colors('headlong rush', ['R']).
card_text('headlong rush', 'Attacking creatures gain first strike until end of turn.').
card_mana_cost('headlong rush', ['1', 'R']).
card_cmc('headlong rush', 2).
card_layout('headlong rush', 'normal').

% Found in: HML
card_name('headstone', 'Headstone').
card_type('headstone', 'Instant').
card_types('headstone', ['Instant']).
card_subtypes('headstone', []).
card_colors('headstone', ['B']).
card_text('headstone', 'Exile target card from a graveyard.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('headstone', ['1', 'B']).
card_cmc('headstone', 2).
card_layout('headstone', 'normal').

% Found in: 5ED, ICE, ME3
card_name('heal', 'Heal').
card_type('heal', 'Instant').
card_types('heal', ['Instant']).
card_subtypes('heal', []).
card_colors('heal', ['W']).
card_text('heal', 'Prevent the next 1 damage that would be dealt to target creature or player this turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('heal', ['W']).
card_cmc('heal', 1).
card_layout('heal', 'normal').

% Found in: LRW
card_name('heal the scars', 'Heal the Scars').
card_type('heal the scars', 'Instant').
card_types('heal the scars', ['Instant']).
card_subtypes('heal the scars', []).
card_colors('heal the scars', ['G']).
card_text('heal the scars', 'Regenerate target creature. You gain life equal to that creature\'s toughness.').
card_mana_cost('heal the scars', ['3', 'G']).
card_cmc('heal the scars', 4).
card_layout('heal the scars', 'normal').

% Found in: M13
card_name('healer of the pride', 'Healer of the Pride').
card_type('healer of the pride', 'Creature — Cat Cleric').
card_types('healer of the pride', ['Creature']).
card_subtypes('healer of the pride', ['Cat', 'Cleric']).
card_colors('healer of the pride', ['W']).
card_text('healer of the pride', 'Whenever another creature enters the battlefield under your control, you gain 2 life.').
card_mana_cost('healer of the pride', ['3', 'W']).
card_cmc('healer of the pride', 4).
card_layout('healer of the pride', 'normal').
card_power('healer of the pride', 2).
card_toughness('healer of the pride', 3).

% Found in: 5DN
card_name('healer\'s headdress', 'Healer\'s Headdress').
card_type('healer\'s headdress', 'Artifact — Equipment').
card_types('healer\'s headdress', ['Artifact']).
card_subtypes('healer\'s headdress', ['Equipment']).
card_colors('healer\'s headdress', []).
card_text('healer\'s headdress', 'Equipped creature gets +0/+2 and has \"{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\"\n{W}{W}: Attach Healer\'s Headdress to target creature you control.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('healer\'s headdress', ['2']).
card_cmc('healer\'s headdress', 2).
card_layout('healer\'s headdress', 'normal').

% Found in: ORI
card_name('healing hands', 'Healing Hands').
card_type('healing hands', 'Sorcery').
card_types('healing hands', ['Sorcery']).
card_subtypes('healing hands', []).
card_colors('healing hands', ['W']).
card_text('healing hands', 'Target player gains 4 life.\nDraw a card.').
card_mana_cost('healing hands', ['2', 'W']).
card_cmc('healing hands', 3).
card_layout('healing hands', 'normal').

% Found in: PLC
card_name('healing leaves', 'Healing Leaves').
card_type('healing leaves', 'Instant').
card_types('healing leaves', ['Instant']).
card_subtypes('healing leaves', []).
card_colors('healing leaves', ['G']).
card_text('healing leaves', 'Choose one —\n• Target player gains 3 life.\n• Prevent the next 3 damage that would be dealt to target creature or player this turn.').
card_mana_cost('healing leaves', ['G']).
card_cmc('healing leaves', 1).
card_layout('healing leaves', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, BRB, CED, CEI, DD3_DVD, DDC, ITP, LEA, LEB, ME4, MIR, RQS, USG
card_name('healing salve', 'Healing Salve').
card_type('healing salve', 'Instant').
card_types('healing salve', ['Instant']).
card_subtypes('healing salve', []).
card_colors('healing salve', ['W']).
card_text('healing salve', 'Choose one —\n• Target player gains 3 life.\n• Prevent the next 3 damage that would be dealt to target creature or player this turn.').
card_mana_cost('healing salve', ['W']).
card_cmc('healing salve', 1).
card_layout('healing salve', 'normal').

% Found in: SHM
card_name('heap doll', 'Heap Doll').
card_type('heap doll', 'Artifact Creature — Scarecrow').
card_types('heap doll', ['Artifact', 'Creature']).
card_subtypes('heap doll', ['Scarecrow']).
card_colors('heap doll', []).
card_text('heap doll', 'Sacrifice Heap Doll: Exile target card from a graveyard.').
card_mana_cost('heap doll', ['1']).
card_cmc('heap doll', 1).
card_layout('heap doll', 'normal').
card_power('heap doll', 1).
card_toughness('heap doll', 1).

% Found in: WTH
card_name('heart of bogardan', 'Heart of Bogardan').
card_type('heart of bogardan', 'Enchantment').
card_types('heart of bogardan', ['Enchantment']).
card_subtypes('heart of bogardan', []).
card_colors('heart of bogardan', ['R']).
card_text('heart of bogardan', 'Cumulative upkeep {2} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhen a player doesn\'t pay Heart of Bogardan\'s cumulative upkeep, Heart of Bogardan deals X damage to target player and each creature he or she controls, where X is twice the number of age counters on Heart of Bogardan minus 2.').
card_mana_cost('heart of bogardan', ['2', 'R', 'R']).
card_cmc('heart of bogardan', 4).
card_layout('heart of bogardan', 'normal').
card_reserved('heart of bogardan').

% Found in: 10E, BOK
card_name('heart of light', 'Heart of Light').
card_type('heart of light', 'Enchantment — Aura').
card_types('heart of light', ['Enchantment']).
card_subtypes('heart of light', ['Aura']).
card_colors('heart of light', ['W']).
card_text('heart of light', 'Enchant creature (Target a creature as you cast this. This card enters the battlefield attached to that creature.)\nPrevent all damage that would be dealt to and dealt by enchanted creature.').
card_mana_cost('heart of light', ['2', 'W']).
card_cmc('heart of light', 3).
card_layout('heart of light', 'normal').

% Found in: MMQ
card_name('heart of ramos', 'Heart of Ramos').
card_type('heart of ramos', 'Artifact').
card_types('heart of ramos', ['Artifact']).
card_subtypes('heart of ramos', []).
card_colors('heart of ramos', []).
card_text('heart of ramos', '{T}: Add {R} to your mana pool.\nSacrifice Heart of Ramos: Add {R} to your mana pool.').
card_mana_cost('heart of ramos', ['3']).
card_cmc('heart of ramos', 3).
card_layout('heart of ramos', 'normal').

% Found in: ALL, ME2
card_name('heart of yavimaya', 'Heart of Yavimaya').
card_type('heart of yavimaya', 'Land').
card_types('heart of yavimaya', ['Land']).
card_subtypes('heart of yavimaya', []).
card_colors('heart of yavimaya', []).
card_text('heart of yavimaya', 'If Heart of Yavimaya would enter the battlefield, sacrifice a Forest instead. If you do, put Heart of Yavimaya onto the battlefield. If you don\'t, put it into its owner\'s graveyard.\n{T}: Add {G} to your mana pool.\n{T}: Target creature gets +1/+1 until end of turn.').
card_layout('heart of yavimaya', 'normal').
card_reserved('heart of yavimaya').

% Found in: H09, TMP
card_name('heart sliver', 'Heart Sliver').
card_type('heart sliver', 'Creature — Sliver').
card_types('heart sliver', ['Creature']).
card_subtypes('heart sliver', ['Sliver']).
card_colors('heart sliver', ['R']).
card_text('heart sliver', 'All Sliver creatures have haste.').
card_mana_cost('heart sliver', ['1', 'R']).
card_cmc('heart sliver', 2).
card_layout('heart sliver', 'normal').
card_power('heart sliver', 1).
card_toughness('heart sliver', 1).

% Found in: UDS
card_name('heart warden', 'Heart Warden').
card_type('heart warden', 'Creature — Elf Druid').
card_types('heart warden', ['Creature']).
card_subtypes('heart warden', ['Elf', 'Druid']).
card_colors('heart warden', ['G']).
card_text('heart warden', '{T}: Add {G} to your mana pool.\n{2}, Sacrifice Heart Warden: Draw a card.').
card_mana_cost('heart warden', ['1', 'G']).
card_cmc('heart warden', 2).
card_layout('heart warden', 'normal').
card_power('heart warden', 1).
card_toughness('heart warden', 1).

% Found in: HML
card_name('heart wolf', 'Heart Wolf').
card_type('heart wolf', 'Creature — Wolf').
card_types('heart wolf', ['Creature']).
card_subtypes('heart wolf', ['Wolf']).
card_colors('heart wolf', ['R']).
card_text('heart wolf', 'First strike\n{T}: Target Dwarf creature gets +2/+0 and gains first strike until end of turn. When that creature leaves the battlefield this turn, sacrifice Heart Wolf. Activate this ability only during combat.').
card_mana_cost('heart wolf', ['3', 'R']).
card_cmc('heart wolf', 4).
card_layout('heart wolf', 'normal').
card_power('heart wolf', 2).
card_toughness('heart wolf', 2).
card_reserved('heart wolf').

% Found in: KTK
card_name('heart-piercer bow', 'Heart-Piercer Bow').
card_type('heart-piercer bow', 'Artifact — Equipment').
card_types('heart-piercer bow', ['Artifact']).
card_subtypes('heart-piercer bow', ['Equipment']).
card_colors('heart-piercer bow', []).
card_text('heart-piercer bow', 'Whenever equipped creature attacks, Heart-Piercer Bow deals 1 damage to target creature defending player controls.\nEquip {1}').
card_mana_cost('heart-piercer bow', ['2']).
card_cmc('heart-piercer bow', 2).
card_layout('heart-piercer bow', 'normal').

% Found in: CHK
card_name('heartbeat of spring', 'Heartbeat of Spring').
card_type('heartbeat of spring', 'Enchantment').
card_types('heartbeat of spring', ['Enchantment']).
card_subtypes('heartbeat of spring', []).
card_colors('heartbeat of spring', ['G']).
card_text('heartbeat of spring', 'Whenever a player taps a land for mana, that player adds one mana to his or her mana pool of any type that land produced.').
card_mana_cost('heartbeat of spring', ['2', 'G']).
card_cmc('heartbeat of spring', 3).
card_layout('heartbeat of spring', 'normal').

% Found in: VIS
card_name('hearth charm', 'Hearth Charm').
card_type('hearth charm', 'Instant').
card_types('hearth charm', ['Instant']).
card_subtypes('hearth charm', []).
card_colors('hearth charm', ['R']).
card_text('hearth charm', 'Choose one —\n• Destroy target artifact creature.\n• Attacking creatures get +1/+0 until end of turn.\n• Target creature with power 2 or less can\'t be blocked this turn.').
card_mana_cost('hearth charm', ['R']).
card_cmc('hearth charm', 1).
card_layout('hearth charm', 'normal').

% Found in: CHK
card_name('hearth kami', 'Hearth Kami').
card_type('hearth kami', 'Creature — Spirit').
card_types('hearth kami', ['Creature']).
card_subtypes('hearth kami', ['Spirit']).
card_colors('hearth kami', ['R']).
card_text('hearth kami', '{X}, Sacrifice Hearth Kami: Destroy target artifact with converted mana cost X.').
card_mana_cost('hearth kami', ['1', 'R']).
card_cmc('hearth kami', 2).
card_layout('hearth kami', 'normal').
card_power('hearth kami', 2).
card_toughness('hearth kami', 1).

% Found in: LRW
card_name('hearthcage giant', 'Hearthcage Giant').
card_type('hearthcage giant', 'Creature — Giant Warrior').
card_types('hearthcage giant', ['Creature']).
card_subtypes('hearthcage giant', ['Giant', 'Warrior']).
card_colors('hearthcage giant', ['R']).
card_text('hearthcage giant', 'When Hearthcage Giant enters the battlefield, put two 3/1 red Elemental Shaman creature tokens onto the battlefield.\nSacrifice an Elemental: Target Giant creature gets +3/+1 until end of turn.').
card_mana_cost('hearthcage giant', ['6', 'R', 'R']).
card_cmc('hearthcage giant', 8).
card_layout('hearthcage giant', 'normal').
card_power('hearthcage giant', 5).
card_toughness('hearthcage giant', 5).

% Found in: EVE, HOP, MM2
card_name('hearthfire hobgoblin', 'Hearthfire Hobgoblin').
card_type('hearthfire hobgoblin', 'Creature — Goblin Soldier').
card_types('hearthfire hobgoblin', ['Creature']).
card_subtypes('hearthfire hobgoblin', ['Goblin', 'Soldier']).
card_colors('hearthfire hobgoblin', ['W', 'R']).
card_text('hearthfire hobgoblin', 'Double strike').
card_mana_cost('hearthfire hobgoblin', ['R/W', 'R/W', 'R/W']).
card_cmc('hearthfire hobgoblin', 3).
card_layout('hearthfire hobgoblin', 'normal').
card_power('hearthfire hobgoblin', 2).
card_toughness('hearthfire hobgoblin', 2).

% Found in: EVE
card_name('heartlash cinder', 'Heartlash Cinder').
card_type('heartlash cinder', 'Creature — Elemental Warrior').
card_types('heartlash cinder', ['Creature']).
card_subtypes('heartlash cinder', ['Elemental', 'Warrior']).
card_colors('heartlash cinder', ['R']).
card_text('heartlash cinder', 'Haste\nChroma — When Heartlash Cinder enters the battlefield, it gets +X/+0 until end of turn, where X is the number of red mana symbols in the mana costs of permanents you control.').
card_mana_cost('heartlash cinder', ['1', 'R']).
card_cmc('heartlash cinder', 2).
card_layout('heartlash cinder', 'normal').
card_power('heartlash cinder', 1).
card_toughness('heartlash cinder', 1).

% Found in: BOK, CNS
card_name('heartless hidetsugu', 'Heartless Hidetsugu').
card_type('heartless hidetsugu', 'Legendary Creature — Ogre Shaman').
card_types('heartless hidetsugu', ['Creature']).
card_subtypes('heartless hidetsugu', ['Ogre', 'Shaman']).
card_supertypes('heartless hidetsugu', ['Legendary']).
card_colors('heartless hidetsugu', ['R']).
card_text('heartless hidetsugu', '{T}: Heartless Hidetsugu deals damage to each player equal to half that player\'s life total, rounded down.').
card_mana_cost('heartless hidetsugu', ['3', 'R', 'R']).
card_cmc('heartless hidetsugu', 5).
card_layout('heartless hidetsugu', 'normal').
card_power('heartless hidetsugu', 4).
card_toughness('heartless hidetsugu', 3).

% Found in: ISD
card_name('heartless summoning', 'Heartless Summoning').
card_type('heartless summoning', 'Enchantment').
card_types('heartless summoning', ['Enchantment']).
card_subtypes('heartless summoning', []).
card_colors('heartless summoning', ['B']).
card_text('heartless summoning', 'Creature spells you cast cost {2} less to cast.\nCreatures you control get -1/-1.').
card_mana_cost('heartless summoning', ['1', 'B']).
card_cmc('heartless summoning', 2).
card_layout('heartless summoning', 'normal').

% Found in: SHM
card_name('heartmender', 'Heartmender').
card_type('heartmender', 'Creature — Elemental').
card_types('heartmender', ['Creature']).
card_subtypes('heartmender', ['Elemental']).
card_colors('heartmender', ['W', 'G']).
card_text('heartmender', 'At the beginning of your upkeep, remove a -1/-1 counter from each creature you control.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('heartmender', ['2', 'G/W', 'G/W']).
card_cmc('heartmender', 4).
card_layout('heartmender', 'normal').
card_power('heartmender', 2).
card_toughness('heartmender', 2).

% Found in: DST
card_name('heartseeker', 'Heartseeker').
card_type('heartseeker', 'Artifact — Equipment').
card_types('heartseeker', ['Artifact']).
card_subtypes('heartseeker', ['Equipment']).
card_colors('heartseeker', []).
card_text('heartseeker', 'Equipped creature gets +2/+1 and has \"{T}, Unattach Heartseeker: Destroy target creature.\"\nEquip {5} ({5}: Attach to target creature you control. Equip only as a sorcery. This card enters the battlefield unattached and stays on the battlefield if the creature leaves.)').
card_mana_cost('heartseeker', ['4']).
card_cmc('heartseeker', 4).
card_layout('heartseeker', 'normal').

% Found in: DDP, ZEN
card_name('heartstabber mosquito', 'Heartstabber Mosquito').
card_type('heartstabber mosquito', 'Creature — Insect').
card_types('heartstabber mosquito', ['Creature']).
card_subtypes('heartstabber mosquito', ['Insect']).
card_colors('heartstabber mosquito', ['B']).
card_text('heartstabber mosquito', 'Kicker {2}{B} (You may pay an additional {2}{B} as you cast this spell.)\nFlying\nWhen Heartstabber Mosquito enters the battlefield, if it was kicked, destroy target creature.').
card_mana_cost('heartstabber mosquito', ['3', 'B']).
card_cmc('heartstabber mosquito', 4).
card_layout('heartstabber mosquito', 'normal').
card_power('heartstabber mosquito', 2).
card_toughness('heartstabber mosquito', 2).

% Found in: H09, STH
card_name('heartstone', 'Heartstone').
card_type('heartstone', 'Artifact').
card_types('heartstone', ['Artifact']).
card_subtypes('heartstone', []).
card_colors('heartstone', []).
card_text('heartstone', 'Activated abilities of creatures cost {1} less to activate. This effect can\'t reduce the amount of mana an ability costs to activate to less than one mana.').
card_mana_cost('heartstone', ['3']).
card_cmc('heartstone', 3).
card_layout('heartstone', 'normal').

% Found in: TMP, TPR
card_name('heartwood dryad', 'Heartwood Dryad').
card_type('heartwood dryad', 'Creature — Dryad').
card_types('heartwood dryad', ['Creature']).
card_subtypes('heartwood dryad', ['Dryad']).
card_colors('heartwood dryad', ['G']).
card_text('heartwood dryad', 'Heartwood Dryad can block creatures with shadow as though Heartwood Dryad had shadow.').
card_mana_cost('heartwood dryad', ['1', 'G']).
card_cmc('heartwood dryad', 2).
card_layout('heartwood dryad', 'normal').
card_power('heartwood dryad', 2).
card_toughness('heartwood dryad', 1).

% Found in: TMP, TPR
card_name('heartwood giant', 'Heartwood Giant').
card_type('heartwood giant', 'Creature — Giant').
card_types('heartwood giant', ['Creature']).
card_subtypes('heartwood giant', ['Giant']).
card_colors('heartwood giant', ['G']).
card_text('heartwood giant', '{T}, Sacrifice a Forest: Heartwood Giant deals 2 damage to target player.').
card_mana_cost('heartwood giant', ['3', 'G', 'G']).
card_cmc('heartwood giant', 5).
card_layout('heartwood giant', 'normal').
card_power('heartwood giant', 4).
card_toughness('heartwood giant', 4).

% Found in: MRD
card_name('heartwood shard', 'Heartwood Shard').
card_type('heartwood shard', 'Artifact').
card_types('heartwood shard', ['Artifact']).
card_subtypes('heartwood shard', []).
card_colors('heartwood shard', []).
card_text('heartwood shard', '{3}, {T} or {G}, {T}: Target creature gains trample until end of turn.').
card_mana_cost('heartwood shard', ['3']).
card_cmc('heartwood shard', 3).
card_layout('heartwood shard', 'normal').

% Found in: FUT
card_name('heartwood storyteller', 'Heartwood Storyteller').
card_type('heartwood storyteller', 'Creature — Treefolk').
card_types('heartwood storyteller', ['Creature']).
card_subtypes('heartwood storyteller', ['Treefolk']).
card_colors('heartwood storyteller', ['G']).
card_text('heartwood storyteller', 'Whenever a player casts a noncreature spell, each of that player\'s opponents may draw a card.').
card_mana_cost('heartwood storyteller', ['1', 'G', 'G']).
card_cmc('heartwood storyteller', 3).
card_layout('heartwood storyteller', 'normal').
card_power('heartwood storyteller', 2).
card_toughness('heartwood storyteller', 3).

% Found in: VAN
card_name('heartwood storyteller avatar', 'Heartwood Storyteller Avatar').
card_type('heartwood storyteller avatar', 'Vanguard').
card_types('heartwood storyteller avatar', ['Vanguard']).
card_subtypes('heartwood storyteller avatar', []).
card_colors('heartwood storyteller avatar', []).
card_text('heartwood storyteller avatar', 'The first creature spell you cast each turn costs {1} less to cast.\nThe first noncreature spell each opponent casts each turn costs {1} more to cast.').
card_layout('heartwood storyteller avatar', 'vanguard').

% Found in: TMP
card_name('heartwood treefolk', 'Heartwood Treefolk').
card_type('heartwood treefolk', 'Creature — Treefolk').
card_types('heartwood treefolk', ['Creature']).
card_subtypes('heartwood treefolk', ['Treefolk']).
card_colors('heartwood treefolk', ['G']).
card_text('heartwood treefolk', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('heartwood treefolk', ['2', 'G', 'G']).
card_cmc('heartwood treefolk', 4).
card_layout('heartwood treefolk', 'normal').
card_power('heartwood treefolk', 3).
card_toughness('heartwood treefolk', 4).

% Found in: STH
card_name('heat of battle', 'Heat of Battle').
card_type('heat of battle', 'Enchantment').
card_types('heat of battle', ['Enchantment']).
card_subtypes('heat of battle', []).
card_colors('heat of battle', ['R']).
card_text('heat of battle', 'Whenever a creature blocks, Heat of Battle deals 1 damage to that creature\'s controller.').
card_mana_cost('heat of battle', ['1', 'R']).
card_cmc('heat of battle', 2).
card_layout('heat of battle', 'normal').

% Found in: BRB, M15, ROE, USG
card_name('heat ray', 'Heat Ray').
card_type('heat ray', 'Instant').
card_types('heat ray', ['Instant']).
card_subtypes('heat ray', []).
card_colors('heat ray', ['R']).
card_text('heat ray', 'Heat Ray deals X damage to target creature.').
card_mana_cost('heat ray', ['X', 'R']).
card_cmc('heat ray', 1).
card_layout('heat ray', 'normal').

% Found in: LRW
card_name('heat shimmer', 'Heat Shimmer').
card_type('heat shimmer', 'Sorcery').
card_types('heat shimmer', ['Sorcery']).
card_subtypes('heat shimmer', []).
card_colors('heat shimmer', ['R']).
card_text('heat shimmer', 'Put a token that\'s a copy of target creature onto the battlefield. That token has haste and \"At the beginning of the end step, exile this permanent.\"').
card_mana_cost('heat shimmer', ['2', 'R']).
card_cmc('heat shimmer', 3).
card_layout('heat shimmer', 'normal').

% Found in: WTH
card_name('heat stroke', 'Heat Stroke').
card_type('heat stroke', 'Enchantment').
card_types('heat stroke', ['Enchantment']).
card_subtypes('heat stroke', []).
card_colors('heat stroke', ['R']).
card_text('heat stroke', 'At end of combat, destroy each creature that blocked or was blocked this turn.').
card_mana_cost('heat stroke', ['2', 'R']).
card_cmc('heat stroke', 3).
card_layout('heat stroke', 'normal').
card_reserved('heat stroke').

% Found in: VIS
card_name('heat wave', 'Heat Wave').
card_type('heat wave', 'Enchantment').
card_types('heat wave', ['Enchantment']).
card_subtypes('heat wave', []).
card_colors('heat wave', ['R']).
card_text('heat wave', 'Cumulative upkeep {R} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nBlue creatures can\'t block creatures you control.\nNonblue creatures can\'t block creatures you control unless their controller pays 1 life for each blocking creature he or she controls.').
card_mana_cost('heat wave', ['2', 'R']).
card_cmc('heat wave', 3).
card_layout('heat wave', 'normal').

% Found in: LEG
card_name('heaven\'s gate', 'Heaven\'s Gate').
card_type('heaven\'s gate', 'Instant').
card_types('heaven\'s gate', ['Instant']).
card_subtypes('heaven\'s gate', []).
card_colors('heaven\'s gate', ['W']).
card_text('heaven\'s gate', 'Any number of target creatures become white until end of turn.').
card_mana_cost('heaven\'s gate', ['W']).
card_cmc('heaven\'s gate', 1).
card_layout('heaven\'s gate', 'normal').

% Found in: SOM
card_name('heavy arbalest', 'Heavy Arbalest').
card_type('heavy arbalest', 'Artifact — Equipment').
card_types('heavy arbalest', ['Artifact']).
card_subtypes('heavy arbalest', ['Equipment']).
card_colors('heavy arbalest', []).
card_text('heavy arbalest', 'Equipped creature doesn\'t untap during its controller\'s untap step.\nEquipped creature has \"{T}: This creature deals 2 damage to target creature or player.\"\nEquip {4}').
card_mana_cost('heavy arbalest', ['3']).
card_cmc('heavy arbalest', 3).
card_layout('heavy arbalest', 'normal').

% Found in: 6ED, 7ED, WTH
card_name('heavy ballista', 'Heavy Ballista').
card_type('heavy ballista', 'Creature — Human Soldier').
card_types('heavy ballista', ['Creature']).
card_subtypes('heavy ballista', ['Human', 'Soldier']).
card_colors('heavy ballista', ['W']).
card_text('heavy ballista', '{T}: Heavy Ballista deals 2 damage to target attacking or blocking creature.').
card_mana_cost('heavy ballista', ['3', 'W']).
card_cmc('heavy ballista', 4).
card_layout('heavy ballista', 'normal').
card_power('heavy ballista', 2).
card_toughness('heavy ballista', 3).

% Found in: ME3, PTK
card_name('heavy fog', 'Heavy Fog').
card_type('heavy fog', 'Instant').
card_types('heavy fog', ['Instant']).
card_subtypes('heavy fog', []).
card_colors('heavy fog', ['G']).
card_text('heavy fog', 'Cast Heavy Fog only during the declare attackers step and only if you\'ve been attacked this step.\nPrevent all damage that would be dealt to you this turn by attacking creatures.').
card_mana_cost('heavy fog', ['1', 'G']).
card_cmc('heavy fog', 2).
card_layout('heavy fog', 'normal').

% Found in: ORI
card_name('heavy infantry', 'Heavy Infantry').
card_type('heavy infantry', 'Creature — Human Soldier').
card_types('heavy infantry', ['Creature']).
card_subtypes('heavy infantry', ['Human', 'Soldier']).
card_colors('heavy infantry', ['W']).
card_text('heavy infantry', 'When Heavy Infantry enters the battlefield, tap target creature an opponent controls.').
card_mana_cost('heavy infantry', ['4', 'W']).
card_cmc('heavy infantry', 5).
card_layout('heavy infantry', 'normal').
card_power('heavy infantry', 3).
card_toughness('heavy infantry', 4).

% Found in: DKA
card_name('heavy mattock', 'Heavy Mattock').
card_type('heavy mattock', 'Artifact — Equipment').
card_types('heavy mattock', ['Artifact']).
card_subtypes('heavy mattock', ['Equipment']).
card_colors('heavy mattock', []).
card_text('heavy mattock', 'Equipped creature gets +1/+1.\nAs long as equipped creature is a Human, it gets an additional +1/+1.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('heavy mattock', ['3']).
card_cmc('heavy mattock', 3).
card_layout('heavy mattock', 'normal').

% Found in: 5ED, 6ED, ICE, MED
card_name('hecatomb', 'Hecatomb').
card_type('hecatomb', 'Enchantment').
card_types('hecatomb', ['Enchantment']).
card_subtypes('hecatomb', []).
card_colors('hecatomb', ['B']).
card_text('hecatomb', 'When Hecatomb enters the battlefield, sacrifice Hecatomb unless you sacrifice four creatures.\nTap an untapped Swamp you control: Hecatomb deals 1 damage to target creature or player.').
card_mana_cost('hecatomb', ['1', 'B', 'B']).
card_cmc('hecatomb', 3).
card_layout('hecatomb', 'normal').

% Found in: CNS, DKA
card_name('heckling fiends', 'Heckling Fiends').
card_type('heckling fiends', 'Creature — Devil').
card_types('heckling fiends', ['Creature']).
card_subtypes('heckling fiends', ['Devil']).
card_colors('heckling fiends', ['R']).
card_text('heckling fiends', '{2}{R}: Target creature attacks this turn if able.').
card_mana_cost('heckling fiends', ['2', 'R']).
card_cmc('heckling fiends', 3).
card_layout('heckling fiends', 'normal').
card_power('heckling fiends', 2).
card_toughness('heckling fiends', 2).

% Found in: PLC, pREL
card_name('hedge troll', 'Hedge Troll').
card_type('hedge troll', 'Creature — Troll Cleric').
card_types('hedge troll', ['Creature']).
card_subtypes('hedge troll', ['Troll', 'Cleric']).
card_colors('hedge troll', ['G']).
card_text('hedge troll', 'Hedge Troll gets +1/+1 as long as you control a Plains.\n{W}: Regenerate Hedge Troll.').
card_mana_cost('hedge troll', ['2', 'G']).
card_cmc('hedge troll', 3).
card_layout('hedge troll', 'normal').
card_power('hedge troll', 2).
card_toughness('hedge troll', 2).

% Found in: DTK
card_name('hedonist\'s trove', 'Hedonist\'s Trove').
card_type('hedonist\'s trove', 'Enchantment').
card_types('hedonist\'s trove', ['Enchantment']).
card_subtypes('hedonist\'s trove', []).
card_colors('hedonist\'s trove', ['B']).
card_text('hedonist\'s trove', 'When Hedonist\'s Trove enters the battlefield, exile all cards from target opponent\'s graveyard.\nYou may play land cards exiled with Hedonist\'s Trove.\nYou may cast nonland cards exiled with Hedonist\'s Trove. You can\'t cast more than one spell this way each turn.').
card_mana_cost('hedonist\'s trove', ['5', 'B', 'B']).
card_cmc('hedonist\'s trove', 7).
card_layout('hedonist\'s trove', 'normal').

% Found in: BFZ
card_name('hedron archive', 'Hedron Archive').
card_type('hedron archive', 'Artifact').
card_types('hedron archive', ['Artifact']).
card_subtypes('hedron archive', []).
card_colors('hedron archive', []).
card_text('hedron archive', '{T}: Add {2} to your mana pool.\n{2}, {T}, Sacrifice Hedron Archive: Draw two cards.').
card_mana_cost('hedron archive', ['4']).
card_cmc('hedron archive', 4).
card_layout('hedron archive', 'normal').

% Found in: BFZ
card_name('hedron blade', 'Hedron Blade').
card_type('hedron blade', 'Artifact — Equipment').
card_types('hedron blade', ['Artifact']).
card_subtypes('hedron blade', ['Equipment']).
card_colors('hedron blade', []).
card_text('hedron blade', 'Equipped creature gets +1/+1.\nWhenever equipped creature becomes blocked by one or more colorless creatures, it gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('hedron blade', ['1']).
card_cmc('hedron blade', 1).
card_layout('hedron blade', 'normal').

% Found in: ZEN
card_name('hedron crab', 'Hedron Crab').
card_type('hedron crab', 'Creature — Crab').
card_types('hedron crab', ['Creature']).
card_subtypes('hedron crab', ['Crab']).
card_colors('hedron crab', ['U']).
card_text('hedron crab', 'Landfall — Whenever a land enters the battlefield under your control, target player puts the top three cards of his or her library into his or her graveyard.').
card_mana_cost('hedron crab', ['U']).
card_cmc('hedron crab', 1).
card_layout('hedron crab', 'normal').
card_power('hedron crab', 0).
card_toughness('hedron crab', 2).

% Found in: PC2
card_name('hedron fields of agadeem', 'Hedron Fields of Agadeem').
card_type('hedron fields of agadeem', 'Plane — Zendikar').
card_types('hedron fields of agadeem', ['Plane']).
card_subtypes('hedron fields of agadeem', ['Zendikar']).
card_colors('hedron fields of agadeem', []).
card_text('hedron fields of agadeem', 'Creatures with power 7 or greater can\'t attack or block.\nWhenever you roll {C}, put a 7/7 colorless Eldrazi creature token with annihilator 1 onto the battlefield. (Whenever it attacks, defending player sacrifices a permanent.)').
card_layout('hedron fields of agadeem', 'plane').

% Found in: ROE
card_name('hedron matrix', 'Hedron Matrix').
card_type('hedron matrix', 'Artifact — Equipment').
card_types('hedron matrix', ['Artifact']).
card_subtypes('hedron matrix', ['Equipment']).
card_colors('hedron matrix', []).
card_text('hedron matrix', 'Equipped creature gets +X/+X, where X is its converted mana cost.\nEquip {4}').
card_mana_cost('hedron matrix', ['4']).
card_cmc('hedron matrix', 4).
card_layout('hedron matrix', 'normal').

% Found in: WWK
card_name('hedron rover', 'Hedron Rover').
card_type('hedron rover', 'Artifact Creature — Construct').
card_types('hedron rover', ['Artifact', 'Creature']).
card_subtypes('hedron rover', ['Construct']).
card_colors('hedron rover', []).
card_text('hedron rover', 'Landfall — Whenever a land enters the battlefield under your control, Hedron Rover gets +2/+2 until end of turn.').
card_mana_cost('hedron rover', ['4']).
card_cmc('hedron rover', 4).
card_layout('hedron rover', 'normal').
card_power('hedron rover', 2).
card_toughness('hedron rover', 2).

% Found in: ZEN
card_name('hedron scrabbler', 'Hedron Scrabbler').
card_type('hedron scrabbler', 'Artifact Creature — Construct').
card_types('hedron scrabbler', ['Artifact', 'Creature']).
card_subtypes('hedron scrabbler', ['Construct']).
card_colors('hedron scrabbler', []).
card_text('hedron scrabbler', 'Landfall — Whenever a land enters the battlefield under your control, Hedron Scrabbler gets +1/+1 until end of turn.').
card_mana_cost('hedron scrabbler', ['2']).
card_cmc('hedron scrabbler', 2).
card_layout('hedron scrabbler', 'normal').
card_power('hedron scrabbler', 1).
card_toughness('hedron scrabbler', 1).

% Found in: ROE
card_name('hedron-field purists', 'Hedron-Field Purists').
card_type('hedron-field purists', 'Creature — Human Cleric').
card_types('hedron-field purists', ['Creature']).
card_subtypes('hedron-field purists', ['Human', 'Cleric']).
card_colors('hedron-field purists', ['W']).
card_text('hedron-field purists', 'Level up {2}{W} ({2}{W}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-4\n1/4\nIf a source would deal damage to you or a creature you control, prevent 1 of that damage.\nLEVEL 5+\n2/5\nIf a source would deal damage to you or a creature you control, prevent 2 of that damage.').
card_mana_cost('hedron-field purists', ['2', 'W']).
card_cmc('hedron-field purists', 3).
card_layout('hedron-field purists', 'leveler').
card_power('hedron-field purists', 0).
card_toughness('hedron-field purists', 3).

% Found in: BOK
card_name('heed the mists', 'Heed the Mists').
card_type('heed the mists', 'Sorcery — Arcane').
card_types('heed the mists', ['Sorcery']).
card_subtypes('heed the mists', ['Arcane']).
card_colors('heed the mists', ['U']).
card_text('heed the mists', 'Put the top card of your library into your graveyard, then draw cards equal to that card\'s converted mana cost.').
card_mana_cost('heed the mists', ['3', 'U', 'U']).
card_cmc('heed the mists', 5).
card_layout('heed the mists', 'normal').

% Found in: DD3_EVG, EVG, ONS
card_name('heedless one', 'Heedless One').
card_type('heedless one', 'Creature — Elf Avatar').
card_types('heedless one', ['Creature']).
card_subtypes('heedless one', ['Elf', 'Avatar']).
card_colors('heedless one', ['G']).
card_text('heedless one', 'Trample\nHeedless One\'s power and toughness are each equal to the number of Elves on the battlefield.').
card_mana_cost('heedless one', ['3', 'G']).
card_cmc('heedless one', 4).
card_layout('heedless one', 'normal').
card_power('heedless one', '*').
card_toughness('heedless one', '*').

% Found in: CSP
card_name('heidar, rimewind master', 'Heidar, Rimewind Master').
card_type('heidar, rimewind master', 'Legendary Creature — Human Wizard').
card_types('heidar, rimewind master', ['Creature']).
card_subtypes('heidar, rimewind master', ['Human', 'Wizard']).
card_supertypes('heidar, rimewind master', ['Legendary']).
card_colors('heidar, rimewind master', ['U']).
card_text('heidar, rimewind master', '{2}, {T}: Return target permanent to its owner\'s hand. Activate this ability only if you control four or more snow permanents.').
card_mana_cost('heidar, rimewind master', ['4', 'U']).
card_cmc('heidar, rimewind master', 5).
card_layout('heidar, rimewind master', 'normal').
card_power('heidar, rimewind master', 3).
card_toughness('heidar, rimewind master', 3).

% Found in: PCY
card_name('heightened awareness', 'Heightened Awareness').
card_type('heightened awareness', 'Enchantment').
card_types('heightened awareness', ['Enchantment']).
card_subtypes('heightened awareness', []).
card_colors('heightened awareness', ['U']).
card_text('heightened awareness', 'As Heightened Awareness enters the battlefield, discard your hand.\nAt the beginning of your draw step, draw an additional card.').
card_mana_cost('heightened awareness', ['3', 'U', 'U']).
card_cmc('heightened awareness', 5).
card_layout('heightened awareness', 'normal').

% Found in: KTK, pMGD
card_name('heir of the wilds', 'Heir of the Wilds').
card_type('heir of the wilds', 'Creature — Human Warrior').
card_types('heir of the wilds', ['Creature']).
card_subtypes('heir of the wilds', ['Human', 'Warrior']).
card_colors('heir of the wilds', ['G']).
card_text('heir of the wilds', 'Deathtouch\nFerocious — Whenever Heir of the Wilds attacks, if you control a creature with power 4 or greater, Heir of the Wilds gets +1/+1 until end of turn.').
card_mana_cost('heir of the wilds', ['1', 'G']).
card_cmc('heir of the wilds', 2).
card_layout('heir of the wilds', 'normal').
card_power('heir of the wilds', 2).
card_toughness('heir of the wilds', 2).

% Found in: AVR
card_name('heirs of stromkirk', 'Heirs of Stromkirk').
card_type('heirs of stromkirk', 'Creature — Vampire').
card_types('heirs of stromkirk', ['Creature']).
card_subtypes('heirs of stromkirk', ['Vampire']).
card_colors('heirs of stromkirk', ['R']).
card_text('heirs of stromkirk', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nWhenever Heirs of Stromkirk deals combat damage to a player, put a +1/+1 counter on it.').
card_mana_cost('heirs of stromkirk', ['2', 'R', 'R']).
card_cmc('heirs of stromkirk', 4).
card_layout('heirs of stromkirk', 'normal').
card_power('heirs of stromkirk', 2).
card_toughness('heirs of stromkirk', 2).

% Found in: THS
card_name('heliod\'s emissary', 'Heliod\'s Emissary').
card_type('heliod\'s emissary', 'Enchantment Creature — Elk').
card_types('heliod\'s emissary', ['Enchantment', 'Creature']).
card_subtypes('heliod\'s emissary', ['Elk']).
card_colors('heliod\'s emissary', ['W']).
card_text('heliod\'s emissary', 'Bestow {6}{W} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nWhenever Heliod\'s Emissary or enchanted creature attacks, tap target creature an opponent controls.\nEnchanted creature gets +3/+3.').
card_mana_cost('heliod\'s emissary', ['3', 'W']).
card_cmc('heliod\'s emissary', 4).
card_layout('heliod\'s emissary', 'normal').
card_power('heliod\'s emissary', 3).
card_toughness('heliod\'s emissary', 3).

% Found in: M15
card_name('heliod\'s pilgrim', 'Heliod\'s Pilgrim').
card_type('heliod\'s pilgrim', 'Creature — Human Cleric').
card_types('heliod\'s pilgrim', ['Creature']).
card_subtypes('heliod\'s pilgrim', ['Human', 'Cleric']).
card_colors('heliod\'s pilgrim', ['W']).
card_text('heliod\'s pilgrim', 'When Heliod\'s Pilgrim enters the battlefield, you may search your library for an Aura card, reveal it, put it into your hand, then shuffle your library.').
card_mana_cost('heliod\'s pilgrim', ['2', 'W']).
card_cmc('heliod\'s pilgrim', 3).
card_layout('heliod\'s pilgrim', 'normal').
card_power('heliod\'s pilgrim', 1).
card_toughness('heliod\'s pilgrim', 2).

% Found in: THS
card_name('heliod, god of the sun', 'Heliod, God of the Sun').
card_type('heliod, god of the sun', 'Legendary Enchantment Creature — God').
card_types('heliod, god of the sun', ['Enchantment', 'Creature']).
card_subtypes('heliod, god of the sun', ['God']).
card_supertypes('heliod, god of the sun', ['Legendary']).
card_colors('heliod, god of the sun', ['W']).
card_text('heliod, god of the sun', 'Indestructible\nAs long as your devotion to white is less than five, Heliod isn\'t a creature. (Each {W} in the mana costs of permanents you control counts toward your devotion to white.)\nOther creatures you control have vigilance.\n{2}{W}{W}: Put a 2/1 white Cleric enchantment creature token onto the battlefield.').
card_mana_cost('heliod, god of the sun', ['3', 'W']).
card_cmc('heliod, god of the sun', 4).
card_layout('heliod, god of the sun', 'normal').
card_power('heliod, god of the sun', 5).
card_toughness('heliod, god of the sun', 6).

% Found in: APC
card_name('helionaut', 'Helionaut').
card_type('helionaut', 'Creature — Human Soldier').
card_types('helionaut', ['Creature']).
card_subtypes('helionaut', ['Human', 'Soldier']).
card_colors('helionaut', ['W']).
card_text('helionaut', 'Flying\n{1}, {T}: Add one mana of any color to your mana pool.').
card_mana_cost('helionaut', ['2', 'W']).
card_cmc('helionaut', 3).
card_layout('helionaut', 'normal').
card_power('helionaut', 1).
card_toughness('helionaut', 2).

% Found in: 5DN
card_name('heliophial', 'Heliophial').
card_type('heliophial', 'Artifact').
card_types('heliophial', ['Artifact']).
card_subtypes('heliophial', []).
card_colors('heliophial', []).
card_text('heliophial', 'Sunburst (This enters the battlefield with a charge counter on it for each color of mana spent to cast it.)\n{2}, Sacrifice Heliophial: Heliophial deals damage equal to the number of charge counters on it to target creature or player.').
card_mana_cost('heliophial', ['5']).
card_cmc('heliophial', 5).
card_layout('heliophial', 'normal').

% Found in: DIS, MM2
card_name('helium squirter', 'Helium Squirter').
card_type('helium squirter', 'Creature — Beast Mutant').
card_types('helium squirter', ['Creature']).
card_subtypes('helium squirter', ['Beast', 'Mutant']).
card_colors('helium squirter', ['U']).
card_text('helium squirter', 'Graft 3 (This creature enters the battlefield with three +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\n{1}: Target creature with a +1/+1 counter on it gains flying until end of turn.').
card_mana_cost('helium squirter', ['4', 'U']).
card_cmc('helium squirter', 5).
card_layout('helium squirter', 'normal').
card_power('helium squirter', 0).
card_toughness('helium squirter', 0).

% Found in: EVE
card_name('helix pinnacle', 'Helix Pinnacle').
card_type('helix pinnacle', 'Enchantment').
card_types('helix pinnacle', ['Enchantment']).
card_subtypes('helix pinnacle', []).
card_colors('helix pinnacle', ['G']).
card_text('helix pinnacle', 'Shroud (This enchantment can\'t be the target of spells or abilities.)\n{X}: Put X tower counters on Helix Pinnacle.\nAt the beginning of your upkeep, if there are 100 or more tower counters on Helix Pinnacle, you win the game.').
card_mana_cost('helix pinnacle', ['G']).
card_cmc('helix pinnacle', 1).
card_layout('helix pinnacle', 'normal').

% Found in: LEG
card_name('hell swarm', 'Hell Swarm').
card_type('hell swarm', 'Instant').
card_types('hell swarm', ['Instant']).
card_subtypes('hell swarm', []).
card_colors('hell swarm', ['B']).
card_text('hell swarm', 'All creatures get -1/-0 until end of turn.').
card_mana_cost('hell swarm', ['B']).
card_cmc('hell swarm', 1).
card_layout('hell swarm', 'normal').

% Found in: 9ED, CHR, LEG
card_name('hell\'s caretaker', 'Hell\'s Caretaker').
card_type('hell\'s caretaker', 'Creature — Horror').
card_types('hell\'s caretaker', ['Creature']).
card_subtypes('hell\'s caretaker', ['Horror']).
card_colors('hell\'s caretaker', ['B']).
card_text('hell\'s caretaker', '{T}, Sacrifice a creature: Return target creature card from your graveyard to the battlefield. Activate this ability only during your upkeep.').
card_mana_cost('hell\'s caretaker', ['3', 'B']).
card_cmc('hell\'s caretaker', 4).
card_layout('hell\'s caretaker', 'normal').
card_power('hell\'s caretaker', 1).
card_toughness('hell\'s caretaker', 1).

% Found in: VAN
card_name('hell\'s caretaker avatar', 'Hell\'s Caretaker Avatar').
card_type('hell\'s caretaker avatar', 'Vanguard').
card_types('hell\'s caretaker avatar', ['Vanguard']).
card_subtypes('hell\'s caretaker avatar', []).
card_colors('hell\'s caretaker avatar', []).
card_text('hell\'s caretaker avatar', '{3}, Sacrifice a creature: Return target creature card from your graveyard to the battlefield.').
card_layout('hell\'s caretaker avatar', 'vanguard').

% Found in: ALA, DDN
card_name('hell\'s thunder', 'Hell\'s Thunder').
card_type('hell\'s thunder', 'Creature — Elemental').
card_types('hell\'s thunder', ['Creature']).
card_subtypes('hell\'s thunder', ['Elemental']).
card_colors('hell\'s thunder', ['R']).
card_text('hell\'s thunder', 'Flying, haste\nAt the beginning of the end step, sacrifice Hell\'s Thunder.\nUnearth {4}{R} ({4}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('hell\'s thunder', ['1', 'R', 'R']).
card_cmc('hell\'s thunder', 3).
card_layout('hell\'s thunder', 'normal').
card_power('hell\'s thunder', 4).
card_toughness('hell\'s thunder', 4).

% Found in: TOR
card_name('hell-bent raider', 'Hell-Bent Raider').
card_type('hell-bent raider', 'Creature — Human Barbarian').
card_types('hell-bent raider', ['Creature']).
card_subtypes('hell-bent raider', ['Human', 'Barbarian']).
card_colors('hell-bent raider', ['R']).
card_text('hell-bent raider', 'First strike, haste\nDiscard a card at random: Hell-Bent Raider gains protection from white until end of turn.').
card_mana_cost('hell-bent raider', ['1', 'R', 'R']).
card_cmc('hell-bent raider', 3).
card_layout('hell-bent raider', 'normal').
card_power('hell-bent raider', 2).
card_toughness('hell-bent raider', 2).

% Found in: ROE
card_name('hellcarver demon', 'Hellcarver Demon').
card_type('hellcarver demon', 'Creature — Demon').
card_types('hellcarver demon', ['Creature']).
card_subtypes('hellcarver demon', ['Demon']).
card_colors('hellcarver demon', ['B']).
card_text('hellcarver demon', 'Flying\nWhenever Hellcarver Demon deals combat damage to a player, sacrifice all other permanents you control and discard your hand. Exile the top six cards of your library. You may cast any number of nonland cards exiled this way without paying their mana costs.').
card_mana_cost('hellcarver demon', ['3', 'B', 'B', 'B']).
card_cmc('hellcarver demon', 6).
card_layout('hellcarver demon', 'normal').
card_power('hellcarver demon', 6).
card_toughness('hellcarver demon', 6).

% Found in: HOP, RAV
card_name('helldozer', 'Helldozer').
card_type('helldozer', 'Creature — Zombie Giant').
card_types('helldozer', ['Creature']).
card_subtypes('helldozer', ['Zombie', 'Giant']).
card_colors('helldozer', ['B']).
card_text('helldozer', '{B}{B}{B}, {T}: Destroy target land. If that land was nonbasic, untap Helldozer.').
card_mana_cost('helldozer', ['3', 'B', 'B', 'B']).
card_cmc('helldozer', 6).
card_layout('helldozer', 'normal').
card_power('helldozer', 6).
card_toughness('helldozer', 5).

% Found in: LEG, ME3
card_name('hellfire', 'Hellfire').
card_type('hellfire', 'Sorcery').
card_types('hellfire', ['Sorcery']).
card_subtypes('hellfire', []).
card_colors('hellfire', ['B']).
card_text('hellfire', 'Destroy all nonblack creatures. Hellfire deals X plus 3 damage to you, where X is the number of creatures that died this way.').
card_mana_cost('hellfire', ['2', 'B', 'B', 'B']).
card_cmc('hellfire', 5).
card_layout('hellfire', 'normal').
card_reserved('hellfire').

% Found in: DDH, ZEN
card_name('hellfire mongrel', 'Hellfire Mongrel').
card_type('hellfire mongrel', 'Creature — Elemental Hound').
card_types('hellfire mongrel', ['Creature']).
card_subtypes('hellfire mongrel', ['Elemental', 'Hound']).
card_colors('hellfire mongrel', ['R']).
card_text('hellfire mongrel', 'At the beginning of each opponent\'s upkeep, if that player has two or fewer cards in hand, Hellfire Mongrel deals 2 damage to him or her.').
card_mana_cost('hellfire mongrel', ['2', 'R']).
card_cmc('hellfire mongrel', 3).
card_layout('hellfire mongrel', 'normal').
card_power('hellfire mongrel', 2).
card_toughness('hellfire mongrel', 2).

% Found in: RTR
card_name('hellhole flailer', 'Hellhole Flailer').
card_type('hellhole flailer', 'Creature — Ogre Warrior').
card_types('hellhole flailer', ['Creature']).
card_subtypes('hellhole flailer', ['Ogre', 'Warrior']).
card_colors('hellhole flailer', ['B', 'R']).
card_text('hellhole flailer', 'Unleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)\n{2}{B}{R}, Sacrifice Hellhole Flailer: Hellhole Flailer deals damage equal to its power to target player.').
card_mana_cost('hellhole flailer', ['1', 'B', 'R']).
card_cmc('hellhole flailer', 3).
card_layout('hellhole flailer', 'normal').
card_power('hellhole flailer', 3).
card_toughness('hellhole flailer', 2).

% Found in: DIS
card_name('hellhole rats', 'Hellhole Rats').
card_type('hellhole rats', 'Creature — Rat').
card_types('hellhole rats', ['Creature']).
card_subtypes('hellhole rats', ['Rat']).
card_colors('hellhole rats', ['B', 'R']).
card_text('hellhole rats', 'Haste\nWhen Hellhole Rats enters the battlefield, target player discards a card. Hellhole Rats deals damage to that player equal to that card\'s converted mana cost.').
card_mana_cost('hellhole rats', ['2', 'B', 'R']).
card_cmc('hellhole rats', 4).
card_layout('hellhole rats', 'normal').
card_power('hellhole rats', 2).
card_toughness('hellhole rats', 2).

% Found in: DDP
card_name('hellion', 'Hellion').
card_type('hellion', ' Creature — Hellion').
card_types('hellion', ['Creature']).
card_subtypes('hellion', ['Hellion']).
card_colors('hellion', []).
card_text('hellion', '').
card_layout('hellion', 'token').
card_power('hellion', 4).
card_toughness('hellion', 4).

% Found in: M13
card_name('hellion crucible', 'Hellion Crucible').
card_type('hellion crucible', 'Land').
card_types('hellion crucible', ['Land']).
card_subtypes('hellion crucible', []).
card_colors('hellion crucible', []).
card_text('hellion crucible', '{T}: Add {1} to your mana pool.\n{1}{R}, {T}: Put a pressure counter on Hellion Crucible.\n{1}{R}, {T}, Remove two pressure counters from Hellion Crucible and sacrifice it: Put a 4/4 red Hellion creature token with haste onto the battlefield. (It can attack and {T} as soon as it comes under your control.)').
card_layout('hellion crucible', 'normal').

% Found in: DDP, PC2, ROE
card_name('hellion eruption', 'Hellion Eruption').
card_type('hellion eruption', 'Sorcery').
card_types('hellion eruption', ['Sorcery']).
card_subtypes('hellion eruption', []).
card_colors('hellion eruption', ['R']).
card_text('hellion eruption', 'Sacrifice all creatures you control, then put that many 4/4 red Hellion creature tokens onto the battlefield.').
card_mana_cost('hellion eruption', ['5', 'R']).
card_cmc('hellion eruption', 6).
card_layout('hellion eruption', 'normal').

% Found in: ARC, MM2, ZEN
card_name('hellkite charger', 'Hellkite Charger').
card_type('hellkite charger', 'Creature — Dragon').
card_types('hellkite charger', ['Creature']).
card_subtypes('hellkite charger', ['Dragon']).
card_colors('hellkite charger', ['R']).
card_text('hellkite charger', 'Flying, haste\nWhenever Hellkite Charger attacks, you may pay {5}{R}{R}. If you do, untap all attacking creatures and after this phase, there is an additional combat phase.').
card_mana_cost('hellkite charger', ['4', 'R', 'R']).
card_cmc('hellkite charger', 6).
card_layout('hellkite charger', 'normal').
card_power('hellkite charger', 5).
card_toughness('hellkite charger', 5).

% Found in: CON, PC2
card_name('hellkite hatchling', 'Hellkite Hatchling').
card_type('hellkite hatchling', 'Creature — Dragon').
card_types('hellkite hatchling', ['Creature']).
card_subtypes('hellkite hatchling', ['Dragon']).
card_colors('hellkite hatchling', ['R', 'G']).
card_text('hellkite hatchling', 'Devour 1 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with that many +1/+1 counters on it.)\nHellkite Hatchling has flying and trample if it devoured a creature.').
card_mana_cost('hellkite hatchling', ['2', 'R', 'G']).
card_cmc('hellkite hatchling', 4).
card_layout('hellkite hatchling', 'normal').
card_power('hellkite hatchling', 2).
card_toughness('hellkite hatchling', 2).

% Found in: MBS
card_name('hellkite igniter', 'Hellkite Igniter').
card_type('hellkite igniter', 'Creature — Dragon').
card_types('hellkite igniter', ['Creature']).
card_subtypes('hellkite igniter', ['Dragon']).
card_colors('hellkite igniter', ['R']).
card_text('hellkite igniter', 'Flying, haste\n{1}{R}: Hellkite Igniter gets +X/+0 until end of turn, where X is the number of artifacts you control.').
card_mana_cost('hellkite igniter', ['5', 'R', 'R']).
card_cmc('hellkite igniter', 7).
card_layout('hellkite igniter', 'normal').
card_power('hellkite igniter', 5).
card_toughness('hellkite igniter', 5).

% Found in: ALA, DRB
card_name('hellkite overlord', 'Hellkite Overlord').
card_type('hellkite overlord', 'Creature — Dragon').
card_types('hellkite overlord', ['Creature']).
card_subtypes('hellkite overlord', ['Dragon']).
card_colors('hellkite overlord', ['B', 'R', 'G']).
card_text('hellkite overlord', 'Flying, trample, haste\n{R}: Hellkite Overlord gets +1/+0 until end of turn.\n{B}{G}: Regenerate Hellkite Overlord.').
card_mana_cost('hellkite overlord', ['4', 'B', 'R', 'R', 'G']).
card_cmc('hellkite overlord', 8).
card_layout('hellkite overlord', 'normal').
card_power('hellkite overlord', 8).
card_toughness('hellkite overlord', 8).

% Found in: GTC
card_name('hellkite tyrant', 'Hellkite Tyrant').
card_type('hellkite tyrant', 'Creature — Dragon').
card_types('hellkite tyrant', ['Creature']).
card_subtypes('hellkite tyrant', ['Dragon']).
card_colors('hellkite tyrant', ['R']).
card_text('hellkite tyrant', 'Flying, trample\nWhenever Hellkite Tyrant deals combat damage to a player, gain control of all artifacts that player controls.\nAt the beginning of your upkeep, if you control twenty or more artifacts, you win the game.').
card_mana_cost('hellkite tyrant', ['4', 'R', 'R']).
card_cmc('hellkite tyrant', 6).
card_layout('hellkite tyrant', 'normal').
card_power('hellkite tyrant', 6).
card_toughness('hellkite tyrant', 5).

% Found in: DDN, GTC
card_name('hellraiser goblin', 'Hellraiser Goblin').
card_type('hellraiser goblin', 'Creature — Goblin Berserker').
card_types('hellraiser goblin', ['Creature']).
card_subtypes('hellraiser goblin', ['Goblin', 'Berserker']).
card_colors('hellraiser goblin', ['R']).
card_text('hellraiser goblin', 'Creatures you control have haste and attack each combat if able.').
card_mana_cost('hellraiser goblin', ['2', 'R']).
card_cmc('hellraiser goblin', 3).
card_layout('hellraiser goblin', 'normal').
card_power('hellraiser goblin', 2).
card_toughness('hellraiser goblin', 2).

% Found in: DDK, DKA
card_name('hellrider', 'Hellrider').
card_type('hellrider', 'Creature — Devil').
card_types('hellrider', ['Creature']).
card_subtypes('hellrider', ['Devil']).
card_colors('hellrider', ['R']).
card_text('hellrider', 'Haste\nWhenever a creature you control attacks, Hellrider deals 1 damage to defending player.').
card_mana_cost('hellrider', ['2', 'R', 'R']).
card_cmc('hellrider', 4).
card_layout('hellrider', 'normal').
card_power('hellrider', 3).
card_toughness('hellrider', 3).

% Found in: CON, DDK, PD2, pWPN
card_name('hellspark elemental', 'Hellspark Elemental').
card_type('hellspark elemental', 'Creature — Elemental').
card_types('hellspark elemental', ['Creature']).
card_subtypes('hellspark elemental', ['Elemental']).
card_colors('hellspark elemental', ['R']).
card_text('hellspark elemental', 'Trample, haste\nAt the beginning of the end step, sacrifice Hellspark Elemental.\nUnearth {1}{R} ({1}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('hellspark elemental', ['1', 'R']).
card_cmc('hellspark elemental', 2).
card_layout('hellspark elemental', 'normal').
card_power('hellspark elemental', 3).
card_toughness('hellspark elemental', 1).

% Found in: VIS
card_name('helm of awakening', 'Helm of Awakening').
card_type('helm of awakening', 'Artifact').
card_types('helm of awakening', ['Artifact']).
card_subtypes('helm of awakening', []).
card_colors('helm of awakening', []).
card_text('helm of awakening', 'Spells cost {1} less to cast.').
card_mana_cost('helm of awakening', ['2']).
card_cmc('helm of awakening', 2).
card_layout('helm of awakening', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB
card_name('helm of chatzuk', 'Helm of Chatzuk').
card_type('helm of chatzuk', 'Artifact').
card_types('helm of chatzuk', ['Artifact']).
card_subtypes('helm of chatzuk', []).
card_colors('helm of chatzuk', []).
card_text('helm of chatzuk', '{1}, {T}: Target creature gains banding until end of turn. (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding a player controls are blocking or being blocked by a creature, that player divides that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('helm of chatzuk', ['1']).
card_cmc('helm of chatzuk', 1).
card_layout('helm of chatzuk', 'normal').

% Found in: 5DN, pPRE
card_name('helm of kaldra', 'Helm of Kaldra').
card_type('helm of kaldra', 'Legendary Artifact — Equipment').
card_types('helm of kaldra', ['Artifact']).
card_subtypes('helm of kaldra', ['Equipment']).
card_supertypes('helm of kaldra', ['Legendary']).
card_colors('helm of kaldra', []).
card_text('helm of kaldra', 'Equipped creature has first strike, trample, and haste.\n{1}: If you control Equipment named Helm of Kaldra, Sword of Kaldra, and Shield of Kaldra, put a legendary 4/4 colorless Avatar creature token named Kaldra onto the battlefield and attach those Equipment to it.\nEquip {2}').
card_mana_cost('helm of kaldra', ['3']).
card_cmc('helm of kaldra', 3).
card_layout('helm of kaldra', 'normal').

% Found in: ALL, ME2
card_name('helm of obedience', 'Helm of Obedience').
card_type('helm of obedience', 'Artifact').
card_types('helm of obedience', ['Artifact']).
card_subtypes('helm of obedience', []).
card_colors('helm of obedience', []).
card_text('helm of obedience', '{X}, {T}: Target opponent puts cards from the top of his or her library into his or her graveyard until a creature card or X cards are put into that graveyard this way, whichever comes first. If a creature card is put into that graveyard this way, sacrifice Helm of Obedience and put that card onto the battlefield under your control. X can\'t be 0.').
card_mana_cost('helm of obedience', ['4']).
card_cmc('helm of obedience', 4).
card_layout('helm of obedience', 'normal').
card_reserved('helm of obedience').

% Found in: TMP
card_name('helm of possession', 'Helm of Possession').
card_type('helm of possession', 'Artifact').
card_types('helm of possession', ['Artifact']).
card_subtypes('helm of possession', []).
card_colors('helm of possession', []).
card_text('helm of possession', 'You may choose not to untap Helm of Possession during your untap step.\n{2}, {T}, Sacrifice a creature: Gain control of target creature for as long as you control Helm of Possession and Helm of Possession remains tapped.').
card_mana_cost('helm of possession', ['4']).
card_cmc('helm of possession', 4).
card_layout('helm of possession', 'normal').

% Found in: SHM
card_name('helm of the ghastlord', 'Helm of the Ghastlord').
card_type('helm of the ghastlord', 'Enchantment — Aura').
card_types('helm of the ghastlord', ['Enchantment']).
card_subtypes('helm of the ghastlord', ['Aura']).
card_colors('helm of the ghastlord', ['U', 'B']).
card_text('helm of the ghastlord', 'Enchant creature\nAs long as enchanted creature is blue, it gets +1/+1 and has \"Whenever this creature deals damage to an opponent, draw a card.\"\nAs long as enchanted creature is black, it gets +1/+1 and has \"Whenever this creature deals damage to an opponent, that player discards a card.\"').
card_mana_cost('helm of the ghastlord', ['3', 'U/B']).
card_cmc('helm of the ghastlord', 4).
card_layout('helm of the ghastlord', 'normal').

% Found in: ORI
card_name('helm of the gods', 'Helm of the Gods').
card_type('helm of the gods', 'Artifact — Equipment').
card_types('helm of the gods', ['Artifact']).
card_subtypes('helm of the gods', ['Equipment']).
card_colors('helm of the gods', []).
card_text('helm of the gods', 'Equipped creature gets +1/+1 for each enchantment you control.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('helm of the gods', ['1']).
card_cmc('helm of the gods', 1).
card_layout('helm of the gods', 'normal').

% Found in: DKA
card_name('helvault', 'Helvault').
card_type('helvault', 'Legendary Artifact').
card_types('helvault', ['Artifact']).
card_subtypes('helvault', []).
card_supertypes('helvault', ['Legendary']).
card_colors('helvault', []).
card_text('helvault', '{1}, {T}: Exile target creature you control.\n{7}, {T}: Exile target creature you don\'t control.\nWhen Helvault is put into a graveyard from the battlefield, return all cards exiled with it to the battlefield under their owners\' control.').
card_mana_cost('helvault', ['3']).
card_cmc('helvault', 3).
card_layout('helvault', 'normal').

% Found in: MRD
card_name('hematite golem', 'Hematite Golem').
card_type('hematite golem', 'Artifact Creature — Golem').
card_types('hematite golem', ['Artifact', 'Creature']).
card_subtypes('hematite golem', ['Golem']).
card_colors('hematite golem', []).
card_text('hematite golem', '{1}{R}: Hematite Golem gets +2/+0 until end of turn.').
card_mana_cost('hematite golem', ['4']).
card_cmc('hematite golem', 4).
card_layout('hematite golem', 'normal').
card_power('hematite golem', 1).
card_toughness('hematite golem', 4).

% Found in: ICE
card_name('hematite talisman', 'Hematite Talisman').
card_type('hematite talisman', 'Artifact').
card_types('hematite talisman', ['Artifact']).
card_subtypes('hematite talisman', []).
card_colors('hematite talisman', []).
card_text('hematite talisman', 'Whenever a player casts a red spell, you may pay {3}. If you do, untap target permanent.').
card_mana_cost('hematite talisman', ['2']).
card_cmc('hematite talisman', 2).
card_layout('hematite talisman', 'normal').

% Found in: FUT
card_name('henchfiend of ukor', 'Henchfiend of Ukor').
card_type('henchfiend of ukor', 'Creature — Ogre').
card_types('henchfiend of ukor', ['Creature']).
card_subtypes('henchfiend of ukor', ['Ogre']).
card_colors('henchfiend of ukor', ['R']).
card_text('henchfiend of ukor', 'Haste\nEcho {1}{B} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice this permanent unless you pay its echo cost.)\n{B/R}: Henchfiend of Ukor gets +1/+0 until end of turn.').
card_mana_cost('henchfiend of ukor', ['3', 'R']).
card_cmc('henchfiend of ukor', 4).
card_layout('henchfiend of ukor', 'normal').
card_power('henchfiend of ukor', 3).
card_toughness('henchfiend of ukor', 2).

% Found in: DDG, MMQ
card_name('henge guardian', 'Henge Guardian').
card_type('henge guardian', 'Artifact Creature — Dragon Wurm').
card_types('henge guardian', ['Artifact', 'Creature']).
card_subtypes('henge guardian', ['Dragon', 'Wurm']).
card_colors('henge guardian', []).
card_text('henge guardian', '{2}: Henge Guardian gains trample until end of turn.').
card_mana_cost('henge guardian', ['5']).
card_cmc('henge guardian', 5).
card_layout('henge guardian', 'normal').
card_power('henge guardian', 3).
card_toughness('henge guardian', 4).

% Found in: MMQ
card_name('henge of ramos', 'Henge of Ramos').
card_type('henge of ramos', 'Land').
card_types('henge of ramos', ['Land']).
card_subtypes('henge of ramos', []).
card_colors('henge of ramos', []).
card_text('henge of ramos', '{T}: Add {1} to your mana pool.\n{2}, {T}: Add one mana of any color to your mana pool.').
card_layout('henge of ramos', 'normal').

% Found in: KTK, pPRE
card_name('herald of anafenza', 'Herald of Anafenza').
card_type('herald of anafenza', 'Creature — Human Soldier').
card_types('herald of anafenza', ['Creature']).
card_subtypes('herald of anafenza', ['Human', 'Soldier']).
card_colors('herald of anafenza', ['W']).
card_text('herald of anafenza', 'Outlast {2}{W} ({2}{W}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)\nWhenever you activate Herald of Anafenza\'s outlast ability, put a 1/1 white Warrior creature token onto the battlefield.').
card_mana_cost('herald of anafenza', ['W']).
card_cmc('herald of anafenza', 1).
card_layout('herald of anafenza', 'normal').
card_power('herald of anafenza', 1).
card_toughness('herald of anafenza', 2).

% Found in: DTK
card_name('herald of dromoka', 'Herald of Dromoka').
card_type('herald of dromoka', 'Creature — Human Warrior').
card_types('herald of dromoka', ['Creature']).
card_subtypes('herald of dromoka', ['Human', 'Warrior']).
card_colors('herald of dromoka', ['W']).
card_text('herald of dromoka', 'Vigilance\nOther Warrior creatures you control have vigilance.').
card_mana_cost('herald of dromoka', ['1', 'W']).
card_cmc('herald of dromoka', 2).
card_layout('herald of dromoka', 'normal').
card_power('herald of dromoka', 2).
card_toughness('herald of dromoka', 2).

% Found in: BFZ
card_name('herald of kozilek', 'Herald of Kozilek').
card_type('herald of kozilek', 'Creature — Eldrazi Drone').
card_types('herald of kozilek', ['Creature']).
card_subtypes('herald of kozilek', ['Eldrazi', 'Drone']).
card_colors('herald of kozilek', []).
card_text('herald of kozilek', 'Devoid (This card has no color.)\nColorless spells you cast cost {1} less to cast.').
card_mana_cost('herald of kozilek', ['1', 'U', 'R']).
card_cmc('herald of kozilek', 3).
card_layout('herald of kozilek', 'normal').
card_power('herald of kozilek', 2).
card_toughness('herald of kozilek', 4).

% Found in: CSP
card_name('herald of leshrac', 'Herald of Leshrac').
card_type('herald of leshrac', 'Creature — Avatar').
card_types('herald of leshrac', ['Creature']).
card_subtypes('herald of leshrac', ['Avatar']).
card_colors('herald of leshrac', ['B']).
card_text('herald of leshrac', 'Flying\nCumulative upkeep—Gain control of a land you don\'t control. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nHerald of Leshrac gets +1/+1 for each land you control but don\'t own.\nWhen Herald of Leshrac leaves the battlefield, each player gains control of each land he or she owns that you control.').
card_mana_cost('herald of leshrac', ['6', 'B']).
card_cmc('herald of leshrac', 7).
card_layout('herald of leshrac', 'normal').
card_power('herald of leshrac', 2).
card_toughness('herald of leshrac', 4).

% Found in: USG
card_name('herald of serra', 'Herald of Serra').
card_type('herald of serra', 'Creature — Angel').
card_types('herald of serra', ['Creature']).
card_subtypes('herald of serra', ['Angel']).
card_colors('herald of serra', ['W']).
card_text('herald of serra', 'Flying, vigilance\nEcho {2}{W}{W} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_mana_cost('herald of serra', ['2', 'W', 'W']).
card_cmc('herald of serra', 4).
card_layout('herald of serra', 'normal').
card_power('herald of serra', 3).
card_toughness('herald of serra', 4).
card_reserved('herald of serra').

% Found in: ORI
card_name('herald of the pantheon', 'Herald of the Pantheon').
card_type('herald of the pantheon', 'Creature — Centaur Shaman').
card_types('herald of the pantheon', ['Creature']).
card_subtypes('herald of the pantheon', ['Centaur', 'Shaman']).
card_colors('herald of the pantheon', ['G']).
card_text('herald of the pantheon', 'Enchantment spells you cast cost {1} less to cast.\nWhenever you cast an enchantment spell, you gain 1 life.').
card_mana_cost('herald of the pantheon', ['1', 'G']).
card_cmc('herald of the pantheon', 2).
card_layout('herald of the pantheon', 'normal').
card_power('herald of the pantheon', 2).
card_toughness('herald of the pantheon', 2).

% Found in: BNG
card_name('herald of torment', 'Herald of Torment').
card_type('herald of torment', 'Enchantment Creature — Demon').
card_types('herald of torment', ['Enchantment', 'Creature']).
card_subtypes('herald of torment', ['Demon']).
card_colors('herald of torment', ['B']).
card_text('herald of torment', 'Bestow {3}{B}{B} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nFlying\nAt the beginning of your upkeep, you lose 1 life.\nEnchanted creature gets +3/+3 and has flying.').
card_mana_cost('herald of torment', ['1', 'B', 'B']).
card_cmc('herald of torment', 3).
card_layout('herald of torment', 'normal').
card_power('herald of torment', 3).
card_toughness('herald of torment', 3).

% Found in: AVR
card_name('herald of war', 'Herald of War').
card_type('herald of war', 'Creature — Angel').
card_types('herald of war', ['Creature']).
card_subtypes('herald of war', ['Angel']).
card_colors('herald of war', ['W']).
card_text('herald of war', 'Flying\nWhenever Herald of War attacks, put a +1/+1 counter on it.\nAngel spells and Human spells you cast cost {1} less to cast for each +1/+1 counter on Herald of War.').
card_mana_cost('herald of war', ['3', 'W', 'W']).
card_cmc('herald of war', 5).
card_layout('herald of war', 'normal').
card_power('herald of war', 3).
card_toughness('herald of war', 3).

% Found in: LRW
card_name('herbal poultice', 'Herbal Poultice').
card_type('herbal poultice', 'Artifact').
card_types('herbal poultice', ['Artifact']).
card_subtypes('herbal poultice', []).
card_colors('herbal poultice', []).
card_text('herbal poultice', '{3}, Sacrifice Herbal Poultice: Regenerate target creature.').
card_mana_cost('herbal poultice', ['0']).
card_cmc('herbal poultice', 0).
card_layout('herbal poultice', 'normal').

% Found in: TSP
card_name('herd gnarr', 'Herd Gnarr').
card_type('herd gnarr', 'Creature — Beast').
card_types('herd gnarr', ['Creature']).
card_subtypes('herd gnarr', ['Beast']).
card_colors('herd gnarr', ['G']).
card_text('herd gnarr', 'Whenever another creature enters the battlefield under your control, Herd Gnarr gets +2/+2 until end of turn.').
card_mana_cost('herd gnarr', ['3', 'G']).
card_cmc('herd gnarr', 4).
card_layout('herd gnarr', 'normal').
card_power('herd gnarr', 2).
card_toughness('herd gnarr', 2).

% Found in: DTK
card_name('herdchaser dragon', 'Herdchaser Dragon').
card_type('herdchaser dragon', 'Creature — Dragon').
card_types('herdchaser dragon', ['Creature']).
card_subtypes('herdchaser dragon', ['Dragon']).
card_colors('herdchaser dragon', ['G']).
card_text('herdchaser dragon', 'Flying, trample\nMegamorph {5}{G}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Herdchaser Dragon is turned face up, put a +1/+1 counter on each other Dragon creature you control.').
card_mana_cost('herdchaser dragon', ['5', 'G']).
card_cmc('herdchaser dragon', 6).
card_layout('herdchaser dragon', 'normal').
card_power('herdchaser dragon', 3).
card_toughness('herdchaser dragon', 3).

% Found in: ISD
card_name('heretic\'s punishment', 'Heretic\'s Punishment').
card_type('heretic\'s punishment', 'Enchantment').
card_types('heretic\'s punishment', ['Enchantment']).
card_subtypes('heretic\'s punishment', []).
card_colors('heretic\'s punishment', ['R']).
card_text('heretic\'s punishment', '{3}{R}: Choose target creature or player, then put the top three cards of your library into your graveyard.  Heretic\'s Punishment deals damage to that creature or player equal to the highest converted mana cost among those cards.').
card_mana_cost('heretic\'s punishment', ['4', 'R']).
card_cmc('heretic\'s punishment', 5).
card_layout('heretic\'s punishment', 'normal').

% Found in: MOR
card_name('heritage druid', 'Heritage Druid').
card_type('heritage druid', 'Creature — Elf Druid').
card_types('heritage druid', ['Creature']).
card_subtypes('heritage druid', ['Elf', 'Druid']).
card_colors('heritage druid', ['G']).
card_text('heritage druid', 'Tap three untapped Elves you control: Add {G}{G}{G} to your mana pool.').
card_mana_cost('heritage druid', ['G']).
card_cmc('heritage druid', 1).
card_layout('heritage druid', 'normal').
card_power('heritage druid', 1).
card_toughness('heritage druid', 1).

% Found in: USG
card_name('hermetic study', 'Hermetic Study').
card_type('hermetic study', 'Enchantment — Aura').
card_types('hermetic study', ['Enchantment']).
card_subtypes('hermetic study', ['Aura']).
card_colors('hermetic study', ['U']).
card_text('hermetic study', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals 1 damage to target creature or player.\"').
card_mana_cost('hermetic study', ['1', 'U']).
card_cmc('hermetic study', 2).
card_layout('hermetic study', 'normal').

% Found in: STH, TPR, VMA, pJGP
card_name('hermit druid', 'Hermit Druid').
card_type('hermit druid', 'Creature — Human Druid').
card_types('hermit druid', ['Creature']).
card_subtypes('hermit druid', ['Human', 'Druid']).
card_colors('hermit druid', ['G']).
card_text('hermit druid', '{G}, {T}: Reveal cards from the top of your library until you reveal a basic land card. Put that card into your hand and all other cards revealed this way into your graveyard.').
card_mana_cost('hermit druid', ['1', 'G']).
card_cmc('hermit druid', 2).
card_layout('hermit druid', 'normal').
card_power('hermit druid', 1).
card_toughness('hermit druid', 1).

% Found in: VAN
card_name('hermit druid avatar', 'Hermit Druid Avatar').
card_type('hermit druid avatar', 'Vanguard').
card_types('hermit druid avatar', ['Vanguard']).
card_subtypes('hermit druid avatar', []).
card_colors('hermit druid avatar', []).
card_text('hermit druid avatar', 'At the beginning of your upkeep, put a land card from your library chosen at random onto the battlefield.').
card_layout('hermit druid avatar', 'vanguard').

% Found in: MBS, pPRE
card_name('hero of bladehold', 'Hero of Bladehold').
card_type('hero of bladehold', 'Creature — Human Knight').
card_types('hero of bladehold', ['Creature']).
card_subtypes('hero of bladehold', ['Human', 'Knight']).
card_colors('hero of bladehold', ['W']).
card_text('hero of bladehold', 'Battle cry (Whenever this creature attacks, each other attacking creature gets +1/+0 until end of turn.)\nWhenever Hero of Bladehold attacks, put two 1/1 white Soldier creature tokens onto the battlefield tapped and attacking.').
card_mana_cost('hero of bladehold', ['2', 'W', 'W']).
card_cmc('hero of bladehold', 4).
card_layout('hero of bladehold', 'normal').
card_power('hero of bladehold', 3).
card_toughness('hero of bladehold', 4).

% Found in: BFZ
card_name('hero of goma fada', 'Hero of Goma Fada').
card_type('hero of goma fada', 'Creature — Human Knight Ally').
card_types('hero of goma fada', ['Creature']).
card_subtypes('hero of goma fada', ['Human', 'Knight', 'Ally']).
card_colors('hero of goma fada', ['W']).
card_text('hero of goma fada', 'Rally — Whenever Hero of Goma Fada or another Ally enters the battlefield under your control, creatures you control gain indestructible until end of turn.').
card_mana_cost('hero of goma fada', ['4', 'W']).
card_cmc('hero of goma fada', 5).
card_layout('hero of goma fada', 'normal').
card_power('hero of goma fada', 4).
card_toughness('hero of goma fada', 3).

% Found in: BNG
card_name('hero of iroas', 'Hero of Iroas').
card_type('hero of iroas', 'Creature — Human Soldier').
card_types('hero of iroas', ['Creature']).
card_subtypes('hero of iroas', ['Human', 'Soldier']).
card_colors('hero of iroas', ['W']).
card_text('hero of iroas', 'Aura spells you cast cost {1} less to cast.\nHeroic — Whenever you cast a spell that targets Hero of Iroas, put a +1/+1 counter on Hero of Iroas.').
card_mana_cost('hero of iroas', ['1', 'W']).
card_cmc('hero of iroas', 2).
card_layout('hero of iroas', 'normal').
card_power('hero of iroas', 2).
card_toughness('hero of iroas', 2).

% Found in: BNG
card_name('hero of leina tower', 'Hero of Leina Tower').
card_type('hero of leina tower', 'Creature — Human Warrior').
card_types('hero of leina tower', ['Creature']).
card_subtypes('hero of leina tower', ['Human', 'Warrior']).
card_colors('hero of leina tower', ['G']).
card_text('hero of leina tower', 'Heroic — Whenever you cast a spell that targets Hero of Leina Tower, you may pay {X}. If you do, put X +1/+1 counters on Hero of Leina Tower.').
card_mana_cost('hero of leina tower', ['G']).
card_cmc('hero of leina tower', 1).
card_layout('hero of leina tower', 'normal').
card_power('hero of leina tower', 1).
card_toughness('hero of leina tower', 1).

% Found in: MBS
card_name('hero of oxid ridge', 'Hero of Oxid Ridge').
card_type('hero of oxid ridge', 'Creature — Human Knight').
card_types('hero of oxid ridge', ['Creature']).
card_subtypes('hero of oxid ridge', ['Human', 'Knight']).
card_colors('hero of oxid ridge', ['R']).
card_text('hero of oxid ridge', 'Haste\nBattle cry (Whenever this creature attacks, each other attacking creature gets +1/+0 until end of turn.)\nWhenever Hero of Oxid Ridge attacks, creatures with power 1 or less can\'t block this turn.').
card_mana_cost('hero of oxid ridge', ['2', 'R', 'R']).
card_cmc('hero of oxid ridge', 4).
card_layout('hero of oxid ridge', 'normal').
card_power('hero of oxid ridge', 4).
card_toughness('hero of oxid ridge', 2).

% Found in: FRF
card_name('hero\'s blade', 'Hero\'s Blade').
card_type('hero\'s blade', 'Artifact — Equipment').
card_types('hero\'s blade', ['Artifact']).
card_subtypes('hero\'s blade', ['Equipment']).
card_colors('hero\'s blade', []).
card_text('hero\'s blade', 'Equipped creature gets +3/+2.\nWhenever a legendary creature enters the battlefield under your control, you may attach Hero\'s Blade to it.\nEquip {4}').
card_mana_cost('hero\'s blade', ['2']).
card_cmc('hero\'s blade', 2).
card_layout('hero\'s blade', 'normal').

% Found in: BOK
card_name('hero\'s demise', 'Hero\'s Demise').
card_type('hero\'s demise', 'Instant').
card_types('hero\'s demise', ['Instant']).
card_subtypes('hero\'s demise', []).
card_colors('hero\'s demise', ['B']).
card_text('hero\'s demise', 'Destroy target legendary creature.').
card_mana_cost('hero\'s demise', ['1', 'B']).
card_cmc('hero\'s demise', 2).
card_layout('hero\'s demise', 'normal').

% Found in: CPK, THS
card_name('hero\'s downfall', 'Hero\'s Downfall').
card_type('hero\'s downfall', 'Instant').
card_types('hero\'s downfall', ['Instant']).
card_subtypes('hero\'s downfall', []).
card_colors('hero\'s downfall', ['B']).
card_text('hero\'s downfall', 'Destroy target creature or planeswalker.').
card_mana_cost('hero\'s downfall', ['1', 'B', 'B']).
card_cmc('hero\'s downfall', 3).
card_layout('hero\'s downfall', 'normal').

% Found in: 6ED, S00, TMP
card_name('hero\'s resolve', 'Hero\'s Resolve').
card_type('hero\'s resolve', 'Enchantment — Aura').
card_types('hero\'s resolve', ['Enchantment']).
card_subtypes('hero\'s resolve', ['Aura']).
card_colors('hero\'s resolve', ['W']).
card_text('hero\'s resolve', 'Enchant creature\nEnchanted creature gets +1/+5.').
card_mana_cost('hero\'s resolve', ['1', 'W']).
card_cmc('hero\'s resolve', 2).
card_layout('hero\'s resolve', 'normal').

% Found in: PLC
card_name('heroes remembered', 'Heroes Remembered').
card_type('heroes remembered', 'Sorcery').
card_types('heroes remembered', ['Sorcery']).
card_subtypes('heroes remembered', []).
card_colors('heroes remembered', ['W']).
card_text('heroes remembered', 'You gain 20 life.\nSuspend 10—{W} (Rather than cast this card from your hand, you may pay {W} and exile it with ten time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_mana_cost('heroes remembered', ['6', 'W', 'W', 'W']).
card_cmc('heroes remembered', 9).
card_layout('heroes remembered', 'normal').

% Found in: JOU, pPRE
card_name('heroes\' bane', 'Heroes\' Bane').
card_type('heroes\' bane', 'Creature — Hydra').
card_types('heroes\' bane', ['Creature']).
card_subtypes('heroes\' bane', ['Hydra']).
card_colors('heroes\' bane', ['G']).
card_text('heroes\' bane', 'Heroes\' Bane enters the battlefield with four +1/+1 counters on it.\n{2}{G}{G}: Put X +1/+1 counters on Heroes\' Bane, where X is its power.').
card_mana_cost('heroes\' bane', ['3', 'G', 'G']).
card_cmc('heroes\' bane', 5).
card_layout('heroes\' bane', 'normal').
card_power('heroes\' bane', 0).
card_toughness('heroes\' bane', 0).

% Found in: BNG
card_name('heroes\' podium', 'Heroes\' Podium').
card_type('heroes\' podium', 'Legendary Artifact').
card_types('heroes\' podium', ['Artifact']).
card_subtypes('heroes\' podium', []).
card_supertypes('heroes\' podium', ['Legendary']).
card_colors('heroes\' podium', []).
card_text('heroes\' podium', 'Each legendary creature you control gets +1/+1 for each other legendary creature you control.\n{X}, {T}: Look at the top X cards of your library. You may reveal a legendary creature card from among them and put it into your hand. Put the rest on the bottom of your library in a random order.').
card_mana_cost('heroes\' podium', ['5']).
card_cmc('heroes\' podium', 5).
card_layout('heroes\' podium', 'normal').

% Found in: ARC, DDG, INV, RTR
card_name('heroes\' reunion', 'Heroes\' Reunion').
card_type('heroes\' reunion', 'Instant').
card_types('heroes\' reunion', ['Instant']).
card_subtypes('heroes\' reunion', []).
card_colors('heroes\' reunion', ['W', 'G']).
card_text('heroes\' reunion', 'Target player gains 7 life.').
card_mana_cost('heroes\' reunion', ['G', 'W']).
card_cmc('heroes\' reunion', 2).
card_layout('heroes\' reunion', 'normal').

% Found in: PLS
card_name('heroic defiance', 'Heroic Defiance').
card_type('heroic defiance', 'Enchantment — Aura').
card_types('heroic defiance', ['Enchantment']).
card_subtypes('heroic defiance', ['Aura']).
card_colors('heroic defiance', ['W']).
card_text('heroic defiance', 'Enchant creature\nEnchanted creature gets +3/+3 unless it shares a color with the most common color among all permanents or a color tied for most common.').
card_mana_cost('heroic defiance', ['1', 'W']).
card_cmc('heroic defiance', 2).
card_layout('heroic defiance', 'normal').

% Found in: FEM
card_name('heroism', 'Heroism').
card_type('heroism', 'Enchantment').
card_types('heroism', ['Enchantment']).
card_subtypes('heroism', []).
card_colors('heroism', ['W']).
card_text('heroism', 'Sacrifice a white creature: For each attacking red creature, prevent all combat damage that would be dealt by that creature this turn unless its controller pays {2}{R}.').
card_mana_cost('heroism', ['2', 'W']).
card_cmc('heroism', 3).
card_layout('heroism', 'normal').

% Found in: STH
card_name('hesitation', 'Hesitation').
card_type('hesitation', 'Enchantment').
card_types('hesitation', ['Enchantment']).
card_subtypes('hesitation', []).
card_colors('hesitation', ['U']).
card_text('hesitation', 'When a player casts a spell, sacrifice Hesitation and counter that spell.').
card_mana_cost('hesitation', ['1', 'U']).
card_cmc('hesitation', 2).
card_layout('hesitation', 'normal').

% Found in: FRF, FRF_UGIN
card_name('hewed stone retainers', 'Hewed Stone Retainers').
card_type('hewed stone retainers', 'Artifact Creature — Golem').
card_types('hewed stone retainers', ['Artifact', 'Creature']).
card_subtypes('hewed stone retainers', ['Golem']).
card_colors('hewed stone retainers', []).
card_text('hewed stone retainers', 'Cast Hewed Stone Retainers only if you\'ve cast another spell this turn.').
card_mana_cost('hewed stone retainers', ['3']).
card_cmc('hewed stone retainers', 3).
card_layout('hewed stone retainers', 'normal').
card_power('hewed stone retainers', 4).
card_toughness('hewed stone retainers', 4).

% Found in: CMD, RAV
card_name('hex', 'Hex').
card_type('hex', 'Sorcery').
card_types('hex', ['Sorcery']).
card_subtypes('hex', []).
card_colors('hex', ['B']).
card_text('hex', 'Destroy six target creatures.').
card_mana_cost('hex', ['4', 'B', 'B']).
card_cmc('hex', 6).
card_layout('hex', 'normal').

% Found in: NPH
card_name('hex parasite', 'Hex Parasite').
card_type('hex parasite', 'Artifact Creature — Insect').
card_types('hex parasite', ['Artifact', 'Creature']).
card_subtypes('hex parasite', ['Insect']).
card_colors('hex parasite', []).
card_text('hex parasite', '{X}{B/P}: Remove up to X counters from target permanent. For each counter removed this way, Hex Parasite gets +1/+0 until end of turn. ({B/P} can be paid with either {B} or 2 life.)').
card_mana_cost('hex parasite', ['1']).
card_cmc('hex parasite', 1).
card_layout('hex parasite', 'normal').
card_power('hex parasite', 1).
card_toughness('hex parasite', 1).

% Found in: MBS
card_name('hexplate golem', 'Hexplate Golem').
card_type('hexplate golem', 'Artifact Creature — Golem').
card_types('hexplate golem', ['Artifact', 'Creature']).
card_subtypes('hexplate golem', ['Golem']).
card_colors('hexplate golem', []).
card_text('hexplate golem', '').
card_mana_cost('hexplate golem', ['7']).
card_cmc('hexplate golem', 7).
card_layout('hexplate golem', 'normal').
card_power('hexplate golem', 5).
card_toughness('hexplate golem', 7).

% Found in: 7ED, 8ED, USG
card_name('hibernation', 'Hibernation').
card_type('hibernation', 'Instant').
card_types('hibernation', ['Instant']).
card_subtypes('hibernation', []).
card_colors('hibernation', ['U']).
card_text('hibernation', 'Return all green permanents to their owners\' hands.').
card_mana_cost('hibernation', ['2', 'U']).
card_cmc('hibernation', 3).
card_layout('hibernation', 'normal').

% Found in: H09, STH, TPR
card_name('hibernation sliver', 'Hibernation Sliver').
card_type('hibernation sliver', 'Creature — Sliver').
card_types('hibernation sliver', ['Creature']).
card_subtypes('hibernation sliver', ['Sliver']).
card_colors('hibernation sliver', ['U', 'B']).
card_text('hibernation sliver', 'All Slivers have \"Pay 2 life: Return this permanent to its owner\'s hand.\"').
card_mana_cost('hibernation sliver', ['U', 'B']).
card_cmc('hibernation sliver', 2).
card_layout('hibernation sliver', 'normal').
card_power('hibernation sliver', 2).
card_toughness('hibernation sliver', 2).

% Found in: CSP
card_name('hibernation\'s end', 'Hibernation\'s End').
card_type('hibernation\'s end', 'Enchantment').
card_types('hibernation\'s end', ['Enchantment']).
card_subtypes('hibernation\'s end', []).
card_colors('hibernation\'s end', ['G']).
card_text('hibernation\'s end', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhenever you pay Hibernation\'s End\'s cumulative upkeep, you may search your library for a creature card with converted mana cost equal to the number of age counters on Hibernation\'s End and put it onto the battlefield. If you do, shuffle your library.').
card_mana_cost('hibernation\'s end', ['4', 'G']).
card_cmc('hibernation\'s end', 5).
card_layout('hibernation\'s end', 'normal').

% Found in: MMQ
card_name('hickory woodlot', 'Hickory Woodlot').
card_type('hickory woodlot', 'Land').
card_types('hickory woodlot', ['Land']).
card_subtypes('hickory woodlot', []).
card_colors('hickory woodlot', []).
card_text('hickory woodlot', 'Hickory Woodlot enters the battlefield tapped with two depletion counters on it.\n{T}, Remove a depletion counter from Hickory Woodlot: Add {G}{G} to your mana pool. If there are no depletion counters on Hickory Woodlot, sacrifice it.').
card_layout('hickory woodlot', 'normal').

% Found in: USG
card_name('hidden ancients', 'Hidden Ancients').
card_type('hidden ancients', 'Enchantment').
card_types('hidden ancients', ['Enchantment']).
card_subtypes('hidden ancients', []).
card_colors('hidden ancients', ['G']).
card_text('hidden ancients', 'When an opponent casts an enchantment spell, if Hidden Ancients is an enchantment, Hidden Ancients becomes a 5/5 Treefolk creature.').
card_mana_cost('hidden ancients', ['1', 'G']).
card_cmc('hidden ancients', 2).
card_layout('hidden ancients', 'normal').

% Found in: DTK
card_name('hidden dragonslayer', 'Hidden Dragonslayer').
card_type('hidden dragonslayer', 'Creature — Human Warrior').
card_types('hidden dragonslayer', ['Creature']).
card_subtypes('hidden dragonslayer', ['Human', 'Warrior']).
card_colors('hidden dragonslayer', ['W']).
card_text('hidden dragonslayer', 'Lifelink\nMegamorph {2}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Hidden Dragonslayer is turned face up, destroy target creature with power 4 or greater an opponent controls.').
card_mana_cost('hidden dragonslayer', ['1', 'W']).
card_cmc('hidden dragonslayer', 2).
card_layout('hidden dragonslayer', 'normal').
card_power('hidden dragonslayer', 2).
card_toughness('hidden dragonslayer', 1).

% Found in: ULG
card_name('hidden gibbons', 'Hidden Gibbons').
card_type('hidden gibbons', 'Enchantment').
card_types('hidden gibbons', ['Enchantment']).
card_subtypes('hidden gibbons', []).
card_colors('hidden gibbons', ['G']).
card_text('hidden gibbons', 'When an opponent casts an instant spell, if Hidden Gibbons is an enchantment, Hidden Gibbons becomes a 4/4 Ape creature.').
card_mana_cost('hidden gibbons', ['G']).
card_cmc('hidden gibbons', 1).
card_layout('hidden gibbons', 'normal').

% Found in: USG
card_name('hidden guerrillas', 'Hidden Guerrillas').
card_type('hidden guerrillas', 'Enchantment').
card_types('hidden guerrillas', ['Enchantment']).
card_subtypes('hidden guerrillas', []).
card_colors('hidden guerrillas', ['G']).
card_text('hidden guerrillas', 'When an opponent casts an artifact spell, if Hidden Guerrillas is an enchantment, Hidden Guerrillas becomes a 5/3 Soldier creature with trample.').
card_mana_cost('hidden guerrillas', ['G']).
card_cmc('hidden guerrillas', 1).
card_layout('hidden guerrillas', 'normal').

% Found in: USG
card_name('hidden herd', 'Hidden Herd').
card_type('hidden herd', 'Enchantment').
card_types('hidden herd', ['Enchantment']).
card_subtypes('hidden herd', []).
card_colors('hidden herd', ['G']).
card_text('hidden herd', 'When an opponent plays a nonbasic land, if Hidden Herd is an enchantment, Hidden Herd becomes a 3/3 Beast creature.').
card_mana_cost('hidden herd', ['G']).
card_cmc('hidden herd', 1).
card_layout('hidden herd', 'normal').

% Found in: 10E, 6ED, PD3, PO2, WTH
card_name('hidden horror', 'Hidden Horror').
card_type('hidden horror', 'Creature — Horror').
card_types('hidden horror', ['Creature']).
card_subtypes('hidden horror', ['Horror']).
card_colors('hidden horror', ['B']).
card_text('hidden horror', 'When Hidden Horror enters the battlefield, sacrifice it unless you discard a creature card.').
card_mana_cost('hidden horror', ['1', 'B', 'B']).
card_cmc('hidden horror', 3).
card_layout('hidden horror', 'normal').
card_power('hidden horror', 4).
card_toughness('hidden horror', 4).

% Found in: DRK
card_name('hidden path', 'Hidden Path').
card_type('hidden path', 'Enchantment').
card_types('hidden path', ['Enchantment']).
card_subtypes('hidden path', []).
card_colors('hidden path', ['G']).
card_text('hidden path', 'Green creatures have forestwalk. (They can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('hidden path', ['2', 'G', 'G', 'G', 'G']).
card_cmc('hidden path', 6).
card_layout('hidden path', 'normal').
card_reserved('hidden path').

% Found in: USG
card_name('hidden predators', 'Hidden Predators').
card_type('hidden predators', 'Enchantment').
card_types('hidden predators', ['Enchantment']).
card_subtypes('hidden predators', []).
card_colors('hidden predators', ['G']).
card_text('hidden predators', 'When an opponent controls a creature with power 4 or greater, if Hidden Predators is an enchantment, Hidden Predators becomes a 4/4 Beast creature.').
card_mana_cost('hidden predators', ['G']).
card_cmc('hidden predators', 1).
card_layout('hidden predators', 'normal').

% Found in: STH
card_name('hidden retreat', 'Hidden Retreat').
card_type('hidden retreat', 'Enchantment').
card_types('hidden retreat', ['Enchantment']).
card_subtypes('hidden retreat', []).
card_colors('hidden retreat', ['W']).
card_text('hidden retreat', 'Put a card from your hand on top of your library: Prevent all damage that would be dealt by target instant or sorcery spell this turn.').
card_mana_cost('hidden retreat', ['2', 'W']).
card_cmc('hidden retreat', 3).
card_layout('hidden retreat', 'normal').

% Found in: USG
card_name('hidden spider', 'Hidden Spider').
card_type('hidden spider', 'Enchantment').
card_types('hidden spider', ['Enchantment']).
card_subtypes('hidden spider', []).
card_colors('hidden spider', ['G']).
card_text('hidden spider', 'When an opponent casts a creature spell with flying, if Hidden Spider is an enchantment, Hidden Spider becomes a 3/5 Spider creature with reach. (It can block creatures with flying.)').
card_mana_cost('hidden spider', ['G']).
card_cmc('hidden spider', 1).
card_layout('hidden spider', 'normal').

% Found in: USG
card_name('hidden stag', 'Hidden Stag').
card_type('hidden stag', 'Enchantment').
card_types('hidden stag', ['Enchantment']).
card_subtypes('hidden stag', []).
card_colors('hidden stag', ['G']).
card_text('hidden stag', 'Whenever an opponent plays a land, if Hidden Stag is an enchantment, Hidden Stag becomes a 3/2 Elk Beast creature.\nWhenever you play a land, if Hidden Stag is a creature, Hidden Stag becomes an enchantment.').
card_mana_cost('hidden stag', ['1', 'G']).
card_cmc('hidden stag', 2).
card_layout('hidden stag', 'normal').

% Found in: DGM
card_name('hidden strings', 'Hidden Strings').
card_type('hidden strings', 'Sorcery').
card_types('hidden strings', ['Sorcery']).
card_subtypes('hidden strings', []).
card_colors('hidden strings', ['U']).
card_text('hidden strings', 'You may tap or untap target permanent, then you may tap or untap another target permanent.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_mana_cost('hidden strings', ['1', 'U']).
card_cmc('hidden strings', 2).
card_layout('hidden strings', 'normal').

% Found in: DIS
card_name('hide', 'Hide').
card_type('hide', 'Instant').
card_types('hide', ['Instant']).
card_subtypes('hide', []).
card_colors('hide', ['W', 'R']).
card_text('hide', 'Put target artifact or enchantment on the bottom of its owner\'s library.').
card_mana_cost('hide', ['R', 'W']).
card_cmc('hide', 2).
card_layout('hide', 'split').
card_sides('hide', 'seek').

% Found in: DD3_GVL, DDD, DDE, HOP, ZEN
card_name('hideous end', 'Hideous End').
card_type('hideous end', 'Instant').
card_types('hideous end', ['Instant']).
card_subtypes('hideous end', []).
card_colors('hideous end', ['B']).
card_text('hideous end', 'Destroy target nonblack creature. Its controller loses 2 life.').
card_mana_cost('hideous end', ['1', 'B', 'B']).
card_cmc('hideous end', 3).
card_layout('hideous end', 'normal').

% Found in: CHK
card_name('hideous laughter', 'Hideous Laughter').
card_type('hideous laughter', 'Instant — Arcane').
card_types('hideous laughter', ['Instant']).
card_subtypes('hideous laughter', ['Arcane']).
card_colors('hideous laughter', ['B']).
card_text('hideous laughter', 'All creatures get -2/-2 until end of turn.\nSplice onto Arcane {3}{B}{B} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('hideous laughter', ['2', 'B', 'B']).
card_cmc('hideous laughter', 4).
card_layout('hideous laughter', 'normal').

% Found in: M12
card_name('hideous visage', 'Hideous Visage').
card_type('hideous visage', 'Sorcery').
card_types('hideous visage', ['Sorcery']).
card_subtypes('hideous visage', []).
card_colors('hideous visage', ['B']).
card_text('hideous visage', 'Creatures you control gain intimidate until end of turn. (Each of those creatures can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_mana_cost('hideous visage', ['2', 'B']).
card_cmc('hideous visage', 3).
card_layout('hideous visage', 'normal').

% Found in: SOK
card_name('hidetsugu\'s second rite', 'Hidetsugu\'s Second Rite').
card_type('hidetsugu\'s second rite', 'Instant').
card_types('hidetsugu\'s second rite', ['Instant']).
card_subtypes('hidetsugu\'s second rite', []).
card_colors('hidetsugu\'s second rite', ['R']).
card_text('hidetsugu\'s second rite', 'If target player has exactly 10 life, Hidetsugu\'s Second Rite deals 10 damage to that player.').
card_mana_cost('hidetsugu\'s second rite', ['3', 'R']).
card_cmc('hidetsugu\'s second rite', 4).
card_layout('hidetsugu\'s second rite', 'normal').

% Found in: 10E, EXO
card_name('high ground', 'High Ground').
card_type('high ground', 'Enchantment').
card_types('high ground', ['Enchantment']).
card_subtypes('high ground', []).
card_colors('high ground', ['W']).
card_text('high ground', 'Each creature you control can block an additional creature.').
card_mana_cost('high ground', ['W']).
card_cmc('high ground', 1).
card_layout('high ground', 'normal').

% Found in: MMQ, V12
card_name('high market', 'High Market').
card_type('high market', 'Land').
card_types('high market', ['Land']).
card_subtypes('high market', []).
card_colors('high market', []).
card_text('high market', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice a creature: You gain 1 life.').
card_layout('high market', 'normal').

% Found in: GTC
card_name('high priest of penance', 'High Priest of Penance').
card_type('high priest of penance', 'Creature — Human Cleric').
card_types('high priest of penance', ['Creature']).
card_subtypes('high priest of penance', ['Human', 'Cleric']).
card_colors('high priest of penance', ['W', 'B']).
card_text('high priest of penance', 'Whenever High Priest of Penance is dealt damage, you may destroy target nonland permanent.').
card_mana_cost('high priest of penance', ['W', 'B']).
card_cmc('high priest of penance', 2).
card_layout('high priest of penance', 'normal').
card_power('high priest of penance', 1).
card_toughness('high priest of penance', 1).

% Found in: MMQ
card_name('high seas', 'High Seas').
card_type('high seas', 'Enchantment').
card_types('high seas', ['Enchantment']).
card_subtypes('high seas', []).
card_colors('high seas', ['U']).
card_text('high seas', 'Red creature spells and green creature spells cost {1} more to cast.').
card_mana_cost('high seas', ['2', 'U']).
card_cmc('high seas', 3).
card_layout('high seas', 'normal').

% Found in: KTK, pPRE
card_name('high sentinels of arashin', 'High Sentinels of Arashin').
card_type('high sentinels of arashin', 'Creature — Bird Soldier').
card_types('high sentinels of arashin', ['Creature']).
card_subtypes('high sentinels of arashin', ['Bird', 'Soldier']).
card_colors('high sentinels of arashin', ['W']).
card_text('high sentinels of arashin', 'Flying\nHigh Sentinels of Arashin gets +1/+1 for each other creature you control with a +1/+1 counter on it.\n{3}{W}: Put a +1/+1 counter on target creature.').
card_mana_cost('high sentinels of arashin', ['3', 'W']).
card_cmc('high sentinels of arashin', 4).
card_layout('high sentinels of arashin', 'normal').
card_power('high sentinels of arashin', 3).
card_toughness('high sentinels of arashin', 4).

% Found in: FEM, MED, VMA, pMEI
card_name('high tide', 'High Tide').
card_type('high tide', 'Instant').
card_types('high tide', ['Instant']).
card_subtypes('high tide', []).
card_colors('high tide', ['U']).
card_text('high tide', 'Until end of turn, whenever a player taps an Island for mana, that player adds {U} to his or her mana pool (in addition to the mana the land produces).').
card_mana_cost('high tide', ['U']).
card_cmc('high tide', 1).
card_layout('high tide', 'normal').

% Found in: DKA
card_name('highborn ghoul', 'Highborn Ghoul').
card_type('highborn ghoul', 'Creature — Zombie').
card_types('highborn ghoul', ['Creature']).
card_subtypes('highborn ghoul', ['Zombie']).
card_colors('highborn ghoul', ['B']).
card_text('highborn ghoul', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_mana_cost('highborn ghoul', ['B', 'B']).
card_cmc('highborn ghoul', 2).
card_layout('highborn ghoul', 'normal').
card_power('highborn ghoul', 2).
card_toughness('highborn ghoul', 1).

% Found in: ZEN
card_name('highland berserker', 'Highland Berserker').
card_type('highland berserker', 'Creature — Human Berserker Ally').
card_types('highland berserker', ['Creature']).
card_subtypes('highland berserker', ['Human', 'Berserker', 'Ally']).
card_colors('highland berserker', ['R']).
card_text('highland berserker', 'Whenever Highland Berserker or another Ally enters the battlefield under your control, you may have Ally creatures you control gain first strike until end of turn.').
card_mana_cost('highland berserker', ['1', 'R']).
card_cmc('highland berserker', 2).
card_layout('highland berserker', 'normal').
card_power('highland berserker', 2).
card_toughness('highland berserker', 1).

% Found in: KTK
card_name('highland game', 'Highland Game').
card_type('highland game', 'Creature — Elk').
card_types('highland game', ['Creature']).
card_subtypes('highland game', ['Elk']).
card_colors('highland game', ['G']).
card_text('highland game', 'When Highland Game dies, you gain 2 life.').
card_mana_cost('highland game', ['1', 'G']).
card_cmc('highland game', 2).
card_layout('highland game', 'normal').
card_power('highland game', 2).
card_toughness('highland game', 1).

% Found in: POR
card_name('highland giant', 'Highland Giant').
card_type('highland giant', 'Creature — Giant').
card_types('highland giant', ['Creature']).
card_subtypes('highland giant', ['Giant']).
card_colors('highland giant', ['R']).
card_text('highland giant', '').
card_mana_cost('highland giant', ['2', 'R', 'R']).
card_cmc('highland giant', 4).
card_layout('highland giant', 'normal').
card_power('highland giant', 3).
card_toughness('highland giant', 4).

% Found in: CSP
card_name('highland weald', 'Highland Weald').
card_type('highland weald', 'Snow Land').
card_types('highland weald', ['Land']).
card_subtypes('highland weald', []).
card_supertypes('highland weald', ['Snow']).
card_colors('highland weald', []).
card_text('highland weald', 'Highland Weald enters the battlefield tapped.\n{T}: Add {R} or {G} to your mana pool.').
card_layout('highland weald', 'normal').

% Found in: KTK
card_name('highspire mantis', 'Highspire Mantis').
card_type('highspire mantis', 'Creature — Insect').
card_types('highspire mantis', ['Creature']).
card_subtypes('highspire mantis', ['Insect']).
card_colors('highspire mantis', ['W', 'R']).
card_text('highspire mantis', 'Flying, trample').
card_mana_cost('highspire mantis', ['2', 'R', 'W']).
card_cmc('highspire mantis', 4).
card_layout('highspire mantis', 'normal').
card_power('highspire mantis', 3).
card_toughness('highspire mantis', 3).

% Found in: 10E, 9ED, DDM, MMQ
card_name('highway robber', 'Highway Robber').
card_type('highway robber', 'Creature — Human Mercenary').
card_types('highway robber', ['Creature']).
card_subtypes('highway robber', ['Human', 'Mercenary']).
card_colors('highway robber', ['B']).
card_text('highway robber', 'When Highway Robber enters the battlefield, target opponent loses 2 life and you gain 2 life.').
card_mana_cost('highway robber', ['2', 'B', 'B']).
card_cmc('highway robber', 4).
card_layout('highway robber', 'normal').
card_power('highway robber', 2).
card_toughness('highway robber', 2).

% Found in: BOK, PC2
card_name('higure, the still wind', 'Higure, the Still Wind').
card_type('higure, the still wind', 'Legendary Creature — Human Ninja').
card_types('higure, the still wind', ['Creature']).
card_subtypes('higure, the still wind', ['Human', 'Ninja']).
card_supertypes('higure, the still wind', ['Legendary']).
card_colors('higure, the still wind', ['U']).
card_text('higure, the still wind', 'Ninjutsu {2}{U}{U} ({2}{U}{U}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Higure, the Still Wind deals combat damage to a player, you may search your library for a Ninja card, reveal it, and put it into your hand. If you do, shuffle your library.\n{2}: Target Ninja creature can\'t be blocked this turn.').
card_mana_cost('higure, the still wind', ['3', 'U', 'U']).
card_cmc('higure, the still wind', 5).
card_layout('higure, the still wind', 'normal').
card_power('higure, the still wind', 3).
card_toughness('higure, the still wind', 4).

% Found in: VAN
card_name('higure, the still wind avatar', 'Higure, the Still Wind Avatar').
card_type('higure, the still wind avatar', 'Vanguard').
card_types('higure, the still wind avatar', ['Vanguard']).
card_subtypes('higure, the still wind avatar', []).
card_colors('higure, the still wind avatar', []).
card_text('higure, the still wind avatar', 'Whenever a nontoken creature you control deals combat damage to an opponent, choose a creature card at random from your library, reveal that card, and put it into your hand. Then shuffle your library.').
card_layout('higure, the still wind avatar', 'vanguard').

% Found in: CHK, MM2
card_name('hikari, twilight guardian', 'Hikari, Twilight Guardian').
card_type('hikari, twilight guardian', 'Legendary Creature — Spirit').
card_types('hikari, twilight guardian', ['Creature']).
card_subtypes('hikari, twilight guardian', ['Spirit']).
card_supertypes('hikari, twilight guardian', ['Legendary']).
card_colors('hikari, twilight guardian', ['W']).
card_text('hikari, twilight guardian', 'Flying\nWhenever you cast a Spirit or Arcane spell, you may exile Hikari, Twilight Guardian. If you do, return it to the battlefield under its owner\'s control at the beginning of the next end step.').
card_mana_cost('hikari, twilight guardian', ['3', 'W', 'W']).
card_cmc('hikari, twilight guardian', 5).
card_layout('hikari, twilight guardian', 'normal').
card_power('hikari, twilight guardian', 4).
card_toughness('hikari, twilight guardian', 4).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 7ED, 8ED, 9ED, CED, CEI, DPA, ITP, LEA, LEB, POR, RQS
card_name('hill giant', 'Hill Giant').
card_type('hill giant', 'Creature — Giant').
card_types('hill giant', ['Creature']).
card_subtypes('hill giant', ['Giant']).
card_colors('hill giant', ['R']).
card_text('hill giant', '').
card_mana_cost('hill giant', ['3', 'R']).
card_cmc('hill giant', 4).
card_layout('hill giant', 'normal').
card_power('hill giant', 3).
card_toughness('hill giant', 3).

% Found in: LRW, MMA
card_name('hillcomber giant', 'Hillcomber Giant').
card_type('hillcomber giant', 'Creature — Giant Scout').
card_types('hillcomber giant', ['Creature']).
card_subtypes('hillcomber giant', ['Giant', 'Scout']).
card_colors('hillcomber giant', ['W']).
card_text('hillcomber giant', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('hillcomber giant', ['2', 'W', 'W']).
card_cmc('hillcomber giant', 4).
card_layout('hillcomber giant', 'normal').
card_power('hillcomber giant', 3).
card_toughness('hillcomber giant', 3).

% Found in: CHK, pMPR
card_name('hinder', 'Hinder').
card_type('hinder', 'Instant').
card_types('hinder', ['Instant']).
card_subtypes('hinder', []).
card_colors('hinder', ['U']).
card_text('hinder', 'Counter target spell. If that spell is countered this way, put that card on the top or bottom of its owner\'s library instead of into that player\'s graveyard.').
card_mana_cost('hinder', ['1', 'U', 'U']).
card_cmc('hinder', 3).
card_layout('hinder', 'normal').

% Found in: ALA
card_name('hindering light', 'Hindering Light').
card_type('hindering light', 'Instant').
card_types('hindering light', ['Instant']).
card_subtypes('hindering light', []).
card_colors('hindering light', ['W', 'U']).
card_text('hindering light', 'Counter target spell that targets you or a permanent you control.\nDraw a card.').
card_mana_cost('hindering light', ['W', 'U']).
card_cmc('hindering light', 2).
card_layout('hindering light', 'normal').

% Found in: SCG
card_name('hindering touch', 'Hindering Touch').
card_type('hindering touch', 'Instant').
card_types('hindering touch', ['Instant']).
card_subtypes('hindering touch', []).
card_colors('hindering touch', ['U']).
card_text('hindering touch', 'Counter target spell unless its controller pays {2}.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_mana_cost('hindering touch', ['3', 'U']).
card_cmc('hindering touch', 4).
card_layout('hindering touch', 'normal').

% Found in: GTC
card_name('hindervines', 'Hindervines').
card_type('hindervines', 'Instant').
card_types('hindervines', ['Instant']).
card_subtypes('hindervines', []).
card_colors('hindervines', ['G']).
card_text('hindervines', 'Prevent all combat damage that would be dealt this turn by creatures with no +1/+1 counters on them.').
card_mana_cost('hindervines', ['2', 'G']).
card_cmc('hindervines', 3).
card_layout('hindervines', 'normal').

% Found in: ODY
card_name('hint of insanity', 'Hint of Insanity').
card_type('hint of insanity', 'Sorcery').
card_types('hint of insanity', ['Sorcery']).
card_subtypes('hint of insanity', []).
card_colors('hint of insanity', ['B']).
card_text('hint of insanity', 'Target player reveals his or her hand. That player discards all nonland cards with the same name as another card in his or her hand.').
card_mana_cost('hint of insanity', ['2', 'B']).
card_cmc('hint of insanity', 3).
card_layout('hint of insanity', 'normal').

% Found in: ISD
card_name('hinterland harbor', 'Hinterland Harbor').
card_type('hinterland harbor', 'Land').
card_types('hinterland harbor', ['Land']).
card_subtypes('hinterland harbor', []).
card_colors('hinterland harbor', []).
card_text('hinterland harbor', 'Hinterland Harbor enters the battlefield tapped unless you control a Forest or an Island.\n{T}: Add {G} or {U} to your mana pool.').
card_layout('hinterland harbor', 'normal').

% Found in: DKA
card_name('hinterland hermit', 'Hinterland Hermit').
card_type('hinterland hermit', 'Creature — Human Werewolf').
card_types('hinterland hermit', ['Creature']).
card_subtypes('hinterland hermit', ['Human', 'Werewolf']).
card_colors('hinterland hermit', ['R']).
card_text('hinterland hermit', 'At the beginning of each upkeep, if no spells were cast last turn, transform Hinterland Hermit.').
card_mana_cost('hinterland hermit', ['1', 'R']).
card_cmc('hinterland hermit', 2).
card_layout('hinterland hermit', 'double-faced').
card_power('hinterland hermit', 2).
card_toughness('hinterland hermit', 1).
card_sides('hinterland hermit', 'hinterland scourge').

% Found in: DKA
card_name('hinterland scourge', 'Hinterland Scourge').
card_type('hinterland scourge', 'Creature — Werewolf').
card_types('hinterland scourge', ['Creature']).
card_subtypes('hinterland scourge', ['Werewolf']).
card_colors('hinterland scourge', ['R']).
card_text('hinterland scourge', 'Hinterland Scourge must be blocked if able.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Hinterland Scourge.').
card_layout('hinterland scourge', 'double-faced').
card_power('hinterland scourge', 3).
card_toughness('hinterland scourge', 2).

% Found in: 5ED, ICE
card_name('hipparion', 'Hipparion').
card_type('hipparion', 'Creature — Horse').
card_types('hipparion', ['Creature']).
card_subtypes('hipparion', ['Horse']).
card_colors('hipparion', ['W']).
card_text('hipparion', 'Hipparion can\'t block creatures with power 3 or greater unless you pay {1}.').
card_mana_cost('hipparion', ['1', 'W']).
card_cmc('hipparion', 2).
card_layout('hipparion', 'normal').
card_power('hipparion', 1).
card_toughness('hipparion', 3).

% Found in: MMQ
card_name('hired giant', 'Hired Giant').
card_type('hired giant', 'Creature — Giant').
card_types('hired giant', ['Creature']).
card_subtypes('hired giant', ['Giant']).
card_colors('hired giant', ['R']).
card_text('hired giant', 'When Hired Giant enters the battlefield, each other player may search his or her library for a land card and put that card onto the battlefield. Then each player who searched his or her library this way shuffles it.').
card_mana_cost('hired giant', ['3', 'R']).
card_cmc('hired giant', 4).
card_layout('hired giant', 'normal').
card_power('hired giant', 4).
card_toughness('hired giant', 4).

% Found in: BOK
card_name('hired muscle', 'Hired Muscle').
card_type('hired muscle', 'Creature — Human Warrior').
card_types('hired muscle', ['Creature']).
card_subtypes('hired muscle', ['Human', 'Warrior']).
card_colors('hired muscle', ['B']).
card_text('hired muscle', 'Whenever you cast a Spirit or Arcane spell, you may put a ki counter on Hired Muscle.\nAt the beginning of the end step, if there are two or more ki counters on Hired Muscle, you may flip it.').
card_mana_cost('hired muscle', ['1', 'B', 'B']).
card_cmc('hired muscle', 3).
card_layout('hired muscle', 'flip').
card_power('hired muscle', 2).
card_toughness('hired muscle', 2).
card_sides('hired muscle', 'scarmaker').

% Found in: DGM
card_name('hired torturer', 'Hired Torturer').
card_type('hired torturer', 'Creature — Human Rogue').
card_types('hired torturer', ['Creature']).
card_subtypes('hired torturer', ['Human', 'Rogue']).
card_colors('hired torturer', ['B']).
card_text('hired torturer', 'Defender\n{3}{B}, {T}: Target opponent loses 2 life, then reveals a card at random from his or her hand.').
card_mana_cost('hired torturer', ['2', 'B']).
card_cmc('hired torturer', 3).
card_layout('hired torturer', 'normal').
card_power('hired torturer', 2).
card_toughness('hired torturer', 3).

% Found in: CHK
card_name('hisoka\'s defiance', 'Hisoka\'s Defiance').
card_type('hisoka\'s defiance', 'Instant').
card_types('hisoka\'s defiance', ['Instant']).
card_subtypes('hisoka\'s defiance', []).
card_colors('hisoka\'s defiance', ['U']).
card_text('hisoka\'s defiance', 'Counter target Spirit or Arcane spell.').
card_mana_cost('hisoka\'s defiance', ['1', 'U']).
card_cmc('hisoka\'s defiance', 2).
card_layout('hisoka\'s defiance', 'normal').

% Found in: CHK
card_name('hisoka\'s guard', 'Hisoka\'s Guard').
card_type('hisoka\'s guard', 'Creature — Human Wizard').
card_types('hisoka\'s guard', ['Creature']).
card_subtypes('hisoka\'s guard', ['Human', 'Wizard']).
card_colors('hisoka\'s guard', ['U']).
card_text('hisoka\'s guard', 'You may choose not to untap Hisoka\'s Guard during your untap step.\n{1}{U}, {T}: Target creature you control other than Hisoka\'s Guard has shroud for as long as Hisoka\'s Guard remains tapped. (It can\'t be the target of spells or abilities.)').
card_mana_cost('hisoka\'s guard', ['1', 'U']).
card_cmc('hisoka\'s guard', 2).
card_layout('hisoka\'s guard', 'normal').
card_power('hisoka\'s guard', 1).
card_toughness('hisoka\'s guard', 1).

% Found in: CHK
card_name('hisoka, minamo sensei', 'Hisoka, Minamo Sensei').
card_type('hisoka, minamo sensei', 'Legendary Creature — Human Wizard').
card_types('hisoka, minamo sensei', ['Creature']).
card_subtypes('hisoka, minamo sensei', ['Human', 'Wizard']).
card_supertypes('hisoka, minamo sensei', ['Legendary']).
card_colors('hisoka, minamo sensei', ['U']).
card_text('hisoka, minamo sensei', '{2}{U}, Discard a card: Counter target spell if it has the same converted mana cost as the discarded card.').
card_mana_cost('hisoka, minamo sensei', ['2', 'U', 'U']).
card_cmc('hisoka, minamo sensei', 4).
card_layout('hisoka, minamo sensei', 'normal').
card_power('hisoka, minamo sensei', 1).
card_toughness('hisoka, minamo sensei', 3).

% Found in: ALA, PC2
card_name('hissing iguanar', 'Hissing Iguanar').
card_type('hissing iguanar', 'Creature — Lizard').
card_types('hissing iguanar', ['Creature']).
card_subtypes('hissing iguanar', ['Lizard']).
card_colors('hissing iguanar', ['R']).
card_text('hissing iguanar', 'Whenever another creature dies, you may have Hissing Iguanar deal 1 damage to target player.').
card_mana_cost('hissing iguanar', ['2', 'R']).
card_cmc('hissing iguanar', 3).
card_layout('hissing iguanar', 'normal').
card_power('hissing iguanar', 3).
card_toughness('hissing iguanar', 1).

% Found in: GPT
card_name('hissing miasma', 'Hissing Miasma').
card_type('hissing miasma', 'Enchantment').
card_types('hissing miasma', ['Enchantment']).
card_subtypes('hissing miasma', []).
card_colors('hissing miasma', ['B']).
card_text('hissing miasma', 'Whenever a creature attacks you, its controller loses 1 life.').
card_mana_cost('hissing miasma', ['1', 'B', 'B']).
card_cmc('hissing miasma', 3).
card_layout('hissing miasma', 'normal').

% Found in: DIS
card_name('hit', 'Hit').
card_type('hit', 'Instant').
card_types('hit', ['Instant']).
card_subtypes('hit', []).
card_colors('hit', ['B', 'R']).
card_text('hit', 'Target player sacrifices an artifact or creature. Hit deals damage to that player equal to that permanent\'s converted mana cost.').
card_mana_cost('hit', ['1', 'B', 'R']).
card_cmc('hit', 3).
card_layout('hit', 'split').
card_sides('hit', 'run').

% Found in: ORI
card_name('hitchclaw recluse', 'Hitchclaw Recluse').
card_type('hitchclaw recluse', 'Creature — Spider').
card_types('hitchclaw recluse', ['Creature']).
card_subtypes('hitchclaw recluse', ['Spider']).
card_colors('hitchclaw recluse', ['G']).
card_text('hitchclaw recluse', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('hitchclaw recluse', ['2', 'G']).
card_cmc('hitchclaw recluse', 3).
card_layout('hitchclaw recluse', 'normal').
card_power('hitchclaw recluse', 1).
card_toughness('hitchclaw recluse', 4).

% Found in: M10
card_name('hive mind', 'Hive Mind').
card_type('hive mind', 'Enchantment').
card_types('hive mind', ['Enchantment']).
card_subtypes('hive mind', []).
card_colors('hive mind', ['U']).
card_text('hive mind', 'Whenever a player casts an instant or sorcery spell, each other player copies that spell. Each of those players may choose new targets for his or her copy.').
card_mana_cost('hive mind', ['5', 'U']).
card_cmc('hive mind', 6).
card_layout('hive mind', 'normal').

% Found in: M14, pMGD
card_name('hive stirrings', 'Hive Stirrings').
card_type('hive stirrings', 'Sorcery').
card_types('hive stirrings', ['Sorcery']).
card_subtypes('hive stirrings', []).
card_colors('hive stirrings', ['W']).
card_text('hive stirrings', 'Put two 1/1 colorless Sliver creature tokens onto the battlefield.').
card_mana_cost('hive stirrings', ['2', 'W']).
card_cmc('hive stirrings', 3).
card_layout('hive stirrings', 'normal').

% Found in: TSP
card_name('hivestone', 'Hivestone').
card_type('hivestone', 'Artifact').
card_types('hivestone', ['Artifact']).
card_subtypes('hivestone', []).
card_colors('hivestone', []).
card_text('hivestone', 'Creatures you control are Slivers in addition to their other creature types.').
card_mana_cost('hivestone', ['2']).
card_cmc('hivestone', 2).
card_layout('hivestone', 'normal').

% Found in: MIR
card_name('hivis of the scale', 'Hivis of the Scale').
card_type('hivis of the scale', 'Legendary Creature — Viashino Shaman').
card_types('hivis of the scale', ['Creature']).
card_subtypes('hivis of the scale', ['Viashino', 'Shaman']).
card_supertypes('hivis of the scale', ['Legendary']).
card_colors('hivis of the scale', ['R']).
card_text('hivis of the scale', 'You may choose not to untap Hivis of the Scale during your untap step.\n{T}: Gain control of target Dragon for as long as you control Hivis and Hivis remains tapped.').
card_mana_cost('hivis of the scale', ['3', 'R', 'R']).
card_cmc('hivis of the scale', 5).
card_layout('hivis of the scale', 'normal').
card_power('hivis of the scale', 3).
card_toughness('hivis of the scale', 4).
card_reserved('hivis of the scale').

% Found in: ORI
card_name('hixus, prison warden', 'Hixus, Prison Warden').
card_type('hixus, prison warden', 'Legendary Creature — Human Soldier').
card_types('hixus, prison warden', ['Creature']).
card_subtypes('hixus, prison warden', ['Human', 'Soldier']).
card_supertypes('hixus, prison warden', ['Legendary']).
card_colors('hixus, prison warden', ['W']).
card_text('hixus, prison warden', 'Flash (You may cast this spell any time you could cast an instant.)\nWhenever a creature deals combat damage to you, if Hixus, Prison Warden entered the battlefield this turn, exile that creature until Hixus leaves the battlefield. (That creature returns under its owner\'s control.)').
card_mana_cost('hixus, prison warden', ['3', 'W', 'W']).
card_cmc('hixus, prison warden', 5).
card_layout('hixus, prison warden', 'normal').
card_power('hixus, prison warden', 4).
card_toughness('hixus, prison warden', 4).

% Found in: ICE
card_name('hoar shade', 'Hoar Shade').
card_type('hoar shade', 'Creature — Shade').
card_types('hoar shade', ['Creature']).
card_subtypes('hoar shade', ['Shade']).
card_colors('hoar shade', ['B']).
card_text('hoar shade', '{B}: Hoar Shade gets +1/+1 until end of turn.').
card_mana_cost('hoar shade', ['3', 'B']).
card_cmc('hoar shade', 4).
card_layout('hoar shade', 'normal').
card_power('hoar shade', 1).
card_toughness('hoar shade', 2).

% Found in: C14, SOM
card_name('hoard-smelter dragon', 'Hoard-Smelter Dragon').
card_type('hoard-smelter dragon', 'Creature — Dragon').
card_types('hoard-smelter dragon', ['Creature']).
card_subtypes('hoard-smelter dragon', ['Dragon']).
card_colors('hoard-smelter dragon', ['R']).
card_text('hoard-smelter dragon', 'Flying\n{3}{R}: Destroy target artifact. Hoard-Smelter Dragon gets +X/+0 until end of turn, where X is that artifact\'s converted mana cost.').
card_mana_cost('hoard-smelter dragon', ['4', 'R', 'R']).
card_cmc('hoard-smelter dragon', 6).
card_layout('hoard-smelter dragon', 'normal').
card_power('hoard-smelter dragon', 5).
card_toughness('hoard-smelter dragon', 5).

% Found in: LRW
card_name('hoarder\'s greed', 'Hoarder\'s Greed').
card_type('hoarder\'s greed', 'Sorcery').
card_types('hoarder\'s greed', ['Sorcery']).
card_subtypes('hoarder\'s greed', []).
card_colors('hoarder\'s greed', ['B']).
card_text('hoarder\'s greed', 'You lose 2 life and draw two cards, then clash with an opponent. If you win, repeat this process. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('hoarder\'s greed', ['3', 'B']).
card_cmc('hoarder\'s greed', 4).
card_layout('hoarder\'s greed', 'normal').

% Found in: M11, M15
card_name('hoarding dragon', 'Hoarding Dragon').
card_type('hoarding dragon', 'Creature — Dragon').
card_types('hoarding dragon', ['Creature']).
card_subtypes('hoarding dragon', ['Dragon']).
card_colors('hoarding dragon', ['R']).
card_text('hoarding dragon', 'Flying\nWhen Hoarding Dragon enters the battlefield, you may search your library for an artifact card, exile it, then shuffle your library.\nWhen Hoarding Dragon dies, you may put the exiled card into its owner\'s hand.').
card_mana_cost('hoarding dragon', ['3', 'R', 'R']).
card_cmc('hoarding dragon', 5).
card_layout('hoarding dragon', 'normal').
card_power('hoarding dragon', 4).
card_toughness('hoarding dragon', 4).

% Found in: PLS
card_name('hobble', 'Hobble').
card_type('hobble', 'Enchantment — Aura').
card_types('hobble', ['Enchantment']).
card_subtypes('hobble', ['Aura']).
card_colors('hobble', ['W']).
card_text('hobble', 'Enchant creature\nWhen Hobble enters the battlefield, draw a card.\nEnchanted creature can\'t attack.\nEnchanted creature can\'t block if it\'s black.').
card_mana_cost('hobble', ['2', 'W']).
card_cmc('hobble', 3).
card_layout('hobble', 'normal').

% Found in: EVE
card_name('hobgoblin dragoon', 'Hobgoblin Dragoon').
card_type('hobgoblin dragoon', 'Creature — Goblin Knight').
card_types('hobgoblin dragoon', ['Creature']).
card_subtypes('hobgoblin dragoon', ['Goblin', 'Knight']).
card_colors('hobgoblin dragoon', ['W', 'R']).
card_text('hobgoblin dragoon', 'Flying, first strike').
card_mana_cost('hobgoblin dragoon', ['2', 'R/W']).
card_cmc('hobgoblin dragoon', 3).
card_layout('hobgoblin dragoon', 'normal').
card_power('hobgoblin dragoon', 1).
card_toughness('hobgoblin dragoon', 2).

% Found in: BOK
card_name('hokori, dust drinker', 'Hokori, Dust Drinker').
card_type('hokori, dust drinker', 'Legendary Creature — Spirit').
card_types('hokori, dust drinker', ['Creature']).
card_subtypes('hokori, dust drinker', ['Spirit']).
card_supertypes('hokori, dust drinker', ['Legendary']).
card_colors('hokori, dust drinker', ['W']).
card_text('hokori, dust drinker', 'Lands don\'t untap during their controllers\' untap steps.\nAt the beginning of each player\'s upkeep, that player untaps a land he or she controls.').
card_mana_cost('hokori, dust drinker', ['2', 'W', 'W']).
card_cmc('hokori, dust drinker', 4).
card_layout('hokori, dust drinker', 'normal').
card_power('hokori, dust drinker', 2).
card_toughness('hokori, dust drinker', 2).

% Found in: BNG
card_name('hold at bay', 'Hold at Bay').
card_type('hold at bay', 'Instant').
card_types('hold at bay', ['Instant']).
card_subtypes('hold at bay', []).
card_colors('hold at bay', ['W']).
card_text('hold at bay', 'Prevent the next 7 damage that would be dealt to target creature or player this turn.').
card_mana_cost('hold at bay', ['1', 'W']).
card_cmc('hold at bay', 2).
card_layout('hold at bay', 'normal').

% Found in: GTC
card_name('hold the gates', 'Hold the Gates').
card_type('hold the gates', 'Enchantment').
card_types('hold the gates', ['Enchantment']).
card_subtypes('hold the gates', []).
card_colors('hold the gates', ['W']).
card_text('hold the gates', 'Creatures you control get +0/+1 for each Gate you control and have vigilance.').
card_mana_cost('hold the gates', ['2', 'W']).
card_cmc('hold the gates', 3).
card_layout('hold the gates', 'normal').

% Found in: CHK, DDN
card_name('hold the line', 'Hold the Line').
card_type('hold the line', 'Instant').
card_types('hold the line', ['Instant']).
card_subtypes('hold the line', []).
card_colors('hold the line', ['W']).
card_text('hold the line', 'Blocking creatures get +7/+7 until end of turn.').
card_mana_cost('hold the line', ['1', 'W', 'W']).
card_cmc('hold the line', 3).
card_layout('hold the line', 'normal').

% Found in: ODY
card_name('holistic wisdom', 'Holistic Wisdom').
card_type('holistic wisdom', 'Enchantment').
card_types('holistic wisdom', ['Enchantment']).
card_subtypes('holistic wisdom', []).
card_colors('holistic wisdom', ['G']).
card_text('holistic wisdom', '{2}, Exile a card from your hand: Return target card from your graveyard to your hand if it shares a card type with the card exiled this way. (Artifact, creature, enchantment, instant, land, planeswalker, sorcery, and tribal are card types.)').
card_mana_cost('holistic wisdom', ['1', 'G', 'G']).
card_cmc('holistic wisdom', 3).
card_layout('holistic wisdom', 'normal').

% Found in: 7ED, 9ED, BTD, S99, USG
card_name('hollow dogs', 'Hollow Dogs').
card_type('hollow dogs', 'Creature — Zombie Hound').
card_types('hollow dogs', ['Creature']).
card_subtypes('hollow dogs', ['Zombie', 'Hound']).
card_colors('hollow dogs', ['B']).
card_text('hollow dogs', 'Whenever Hollow Dogs attacks, it gets +2/+0 until end of turn.').
card_mana_cost('hollow dogs', ['4', 'B']).
card_cmc('hollow dogs', 5).
card_layout('hollow dogs', 'normal').
card_power('hollow dogs', 3).
card_toughness('hollow dogs', 3).

% Found in: LGN
card_name('hollow specter', 'Hollow Specter').
card_type('hollow specter', 'Creature — Specter').
card_types('hollow specter', ['Creature']).
card_subtypes('hollow specter', ['Specter']).
card_colors('hollow specter', ['B']).
card_text('hollow specter', 'Flying\nWhenever Hollow Specter deals combat damage to a player, you may pay {X}. If you do, that player reveals X cards from his or her hand and you choose one of them. That player discards that card.').
card_mana_cost('hollow specter', ['1', 'B', 'B']).
card_cmc('hollow specter', 3).
card_layout('hollow specter', 'normal').
card_power('hollow specter', 2).
card_toughness('hollow specter', 2).

% Found in: 5ED, FEM
card_name('hollow trees', 'Hollow Trees').
card_type('hollow trees', 'Land').
card_types('hollow trees', ['Land']).
card_subtypes('hollow trees', []).
card_colors('hollow trees', []).
card_text('hollow trees', 'Hollow Trees enters the battlefield tapped.\nYou may choose not to untap Hollow Trees during your untap step.\nAt the beginning of your upkeep, if Hollow Trees is tapped, put a storage counter on it.\n{T}, Remove any number of storage counters from Hollow Trees: Add {G} to your mana pool for each storage counter removed this way.').
card_layout('hollow trees', 'normal').

% Found in: PCY
card_name('hollow warrior', 'Hollow Warrior').
card_type('hollow warrior', 'Artifact Creature — Golem Warrior').
card_types('hollow warrior', ['Artifact', 'Creature']).
card_subtypes('hollow warrior', ['Golem', 'Warrior']).
card_colors('hollow warrior', []).
card_text('hollow warrior', 'Hollow Warrior can\'t attack or block unless you tap an untapped creature you control not declared as an attacking or blocking creature this combat. (This cost is paid as attackers or blockers are declared.)').
card_mana_cost('hollow warrior', ['4']).
card_cmc('hollow warrior', 4).
card_layout('hollow warrior', 'normal').
card_power('hollow warrior', 4).
card_toughness('hollow warrior', 4).

% Found in: SHM
card_name('hollowborn barghest', 'Hollowborn Barghest').
card_type('hollowborn barghest', 'Creature — Demon Hound').
card_types('hollowborn barghest', ['Creature']).
card_subtypes('hollowborn barghest', ['Demon', 'Hound']).
card_colors('hollowborn barghest', ['B']).
card_text('hollowborn barghest', 'At the beginning of your upkeep, if you have no cards in hand, each opponent loses 2 life.\nAt the beginning of each opponent\'s upkeep, if that player has no cards in hand, he or she loses 2 life.').
card_mana_cost('hollowborn barghest', ['5', 'B', 'B']).
card_cmc('hollowborn barghest', 7).
card_layout('hollowborn barghest', 'normal').
card_power('hollowborn barghest', 7).
card_toughness('hollowborn barghest', 6).

% Found in: DKA
card_name('hollowhenge beast', 'Hollowhenge Beast').
card_type('hollowhenge beast', 'Creature — Beast').
card_types('hollowhenge beast', ['Creature']).
card_subtypes('hollowhenge beast', ['Beast']).
card_colors('hollowhenge beast', ['G']).
card_text('hollowhenge beast', '').
card_mana_cost('hollowhenge beast', ['3', 'G', 'G']).
card_cmc('hollowhenge beast', 5).
card_layout('hollowhenge beast', 'normal').
card_power('hollowhenge beast', 5).
card_toughness('hollowhenge beast', 5).

% Found in: ISD
card_name('hollowhenge scavenger', 'Hollowhenge Scavenger').
card_type('hollowhenge scavenger', 'Creature — Elemental').
card_types('hollowhenge scavenger', ['Creature']).
card_subtypes('hollowhenge scavenger', ['Elemental']).
card_colors('hollowhenge scavenger', ['G']).
card_text('hollowhenge scavenger', 'Morbid — When Hollowhenge Scavenger enters the battlefield, if a creature died this turn, you gain 5 life.').
card_mana_cost('hollowhenge scavenger', ['3', 'G', 'G']).
card_cmc('hollowhenge scavenger', 5).
card_layout('hollowhenge scavenger', 'normal').
card_power('hollowhenge scavenger', 4).
card_toughness('hollowhenge scavenger', 5).

% Found in: DKA
card_name('hollowhenge spirit', 'Hollowhenge Spirit').
card_type('hollowhenge spirit', 'Creature — Spirit').
card_types('hollowhenge spirit', ['Creature']).
card_subtypes('hollowhenge spirit', ['Spirit']).
card_colors('hollowhenge spirit', ['W']).
card_text('hollowhenge spirit', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nWhen Hollowhenge Spirit enters the battlefield, remove target attacking or blocking creature from combat.').
card_mana_cost('hollowhenge spirit', ['3', 'W']).
card_cmc('hollowhenge spirit', 4).
card_layout('hollowhenge spirit', 'normal').
card_power('hollowhenge spirit', 2).
card_toughness('hollowhenge spirit', 2).

% Found in: SHM
card_name('hollowsage', 'Hollowsage').
card_type('hollowsage', 'Creature — Merfolk Wizard').
card_types('hollowsage', ['Creature']).
card_subtypes('hollowsage', ['Merfolk', 'Wizard']).
card_colors('hollowsage', ['B']).
card_text('hollowsage', 'Whenever Hollowsage becomes untapped, you may have target player discard a card.').
card_mana_cost('hollowsage', ['3', 'B']).
card_cmc('hollowsage', 4).
card_layout('hollowsage', 'normal').
card_power('hollowsage', 2).
card_toughness('hollowsage', 2).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('holy armor', 'Holy Armor').
card_type('holy armor', 'Enchantment — Aura').
card_types('holy armor', ['Enchantment']).
card_subtypes('holy armor', ['Aura']).
card_colors('holy armor', ['W']).
card_text('holy armor', 'Enchant creature\nEnchanted creature gets +0/+2.\n{W}: Enchanted creature gets +0/+1 until end of turn.').
card_mana_cost('holy armor', ['W']).
card_cmc('holy armor', 1).
card_layout('holy armor', 'normal').

% Found in: 10E, 8ED, 9ED, INV, LEG
card_name('holy day', 'Holy Day').
card_type('holy day', 'Instant').
card_types('holy day', ['Instant']).
card_subtypes('holy day', []).
card_colors('holy day', ['W']).
card_text('holy day', 'Prevent all combat damage that would be dealt this turn.').
card_mana_cost('holy day', ['W']).
card_cmc('holy day', 1).
card_layout('holy day', 'normal').

% Found in: AVR
card_name('holy justiciar', 'Holy Justiciar').
card_type('holy justiciar', 'Creature — Human Cleric').
card_types('holy justiciar', ['Creature']).
card_subtypes('holy justiciar', ['Human', 'Cleric']).
card_colors('holy justiciar', ['W']).
card_text('holy justiciar', '{2}{W}, {T}: Tap target creature. If that creature is a Zombie, exile it.').
card_mana_cost('holy justiciar', ['3', 'W']).
card_cmc('holy justiciar', 4).
card_layout('holy justiciar', 'normal').
card_power('holy justiciar', 2).
card_toughness('holy justiciar', 1).

% Found in: DRK, MED
card_name('holy light', 'Holy Light').
card_type('holy light', 'Instant').
card_types('holy light', ['Instant']).
card_subtypes('holy light', []).
card_colors('holy light', ['W']).
card_text('holy light', 'Nonwhite creatures get -1/-1 until end of turn.').
card_mana_cost('holy light', ['2', 'W']).
card_cmc('holy light', 3).
card_layout('holy light', 'normal').

% Found in: GTC
card_name('holy mantle', 'Holy Mantle').
card_type('holy mantle', 'Enchantment — Aura').
card_types('holy mantle', ['Enchantment']).
card_subtypes('holy mantle', ['Aura']).
card_colors('holy mantle', ['W']).
card_text('holy mantle', 'Enchant creature\nEnchanted creature gets +2/+2 and has protection from creatures.').
card_mana_cost('holy mantle', ['2', 'W', 'W']).
card_cmc('holy mantle', 4).
card_layout('holy mantle', 'normal').

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 7ED, 8ED, 9ED, CED, CEI, LEA, LEB, M10, M11
card_name('holy strength', 'Holy Strength').
card_type('holy strength', 'Enchantment — Aura').
card_types('holy strength', ['Enchantment']).
card_subtypes('holy strength', ['Aura']).
card_colors('holy strength', ['W']).
card_text('holy strength', 'Enchant creature\nEnchanted creature gets +1/+2.').
card_mana_cost('holy strength', ['W']).
card_cmc('holy strength', 1).
card_layout('holy strength', 'normal').

% Found in: FEM
card_name('homarid', 'Homarid').
card_type('homarid', 'Creature — Homarid').
card_types('homarid', ['Creature']).
card_subtypes('homarid', ['Homarid']).
card_colors('homarid', ['U']).
card_text('homarid', 'Homarid enters the battlefield with a tide counter on it.\nAt the beginning of your upkeep, put a tide counter on Homarid.\nAs long as there is exactly one tide counter on Homarid, it gets -1/-1.\nAs long as there are exactly three tide counters on Homarid, it gets +1/+1.\nWhenever there are four tide counters on Homarid, remove all tide counters from it.').
card_mana_cost('homarid', ['2', 'U']).
card_cmc('homarid', 3).
card_layout('homarid', 'normal').
card_power('homarid', 2).
card_toughness('homarid', 2).

% Found in: FEM
card_name('homarid shaman', 'Homarid Shaman').
card_type('homarid shaman', 'Creature — Homarid Shaman').
card_types('homarid shaman', ['Creature']).
card_subtypes('homarid shaman', ['Homarid', 'Shaman']).
card_colors('homarid shaman', ['U']).
card_text('homarid shaman', '{U}: Tap target green creature.').
card_mana_cost('homarid shaman', ['2', 'U', 'U']).
card_cmc('homarid shaman', 4).
card_layout('homarid shaman', 'normal').
card_power('homarid shaman', 2).
card_toughness('homarid shaman', 1).
card_reserved('homarid shaman').

% Found in: FEM, MED
card_name('homarid spawning bed', 'Homarid Spawning Bed').
card_type('homarid spawning bed', 'Enchantment').
card_types('homarid spawning bed', ['Enchantment']).
card_subtypes('homarid spawning bed', []).
card_colors('homarid spawning bed', ['U']).
card_text('homarid spawning bed', '{1}{U}{U}, Sacrifice a blue creature: Put X 1/1 blue Camarid creature tokens onto the battlefield, where X is the sacrificed creature\'s converted mana cost.').
card_mana_cost('homarid spawning bed', ['U', 'U']).
card_cmc('homarid spawning bed', 2).
card_layout('homarid spawning bed', 'normal').

% Found in: 5ED, FEM
card_name('homarid warrior', 'Homarid Warrior').
card_type('homarid warrior', 'Creature — Homarid Warrior').
card_types('homarid warrior', ['Creature']).
card_subtypes('homarid warrior', ['Homarid', 'Warrior']).
card_colors('homarid warrior', ['U']).
card_text('homarid warrior', '{U}: Homarid Warrior gains shroud until end of turn and doesn\'t untap during your next untap step. Tap Homarid Warrior. (A creature with shroud can\'t be the target of spells or abilities.)').
card_mana_cost('homarid warrior', ['4', 'U']).
card_cmc('homarid warrior', 5).
card_layout('homarid warrior', 'normal').
card_power('homarid warrior', 3).
card_toughness('homarid warrior', 3).

% Found in: C13, CMD
card_name('homeward path', 'Homeward Path').
card_type('homeward path', 'Land').
card_types('homeward path', ['Land']).
card_subtypes('homeward path', []).
card_colors('homeward path', []).
card_text('homeward path', '{T}: Add {1} to your mana pool.\n{T}: Each player gains control of all creatures he or she owns.').
card_layout('homeward path', 'normal').

% Found in: ISD
card_name('homicidal brute', 'Homicidal Brute').
card_type('homicidal brute', 'Creature — Human Mutant').
card_types('homicidal brute', ['Creature']).
card_subtypes('homicidal brute', ['Human', 'Mutant']).
card_colors('homicidal brute', ['R']).
card_text('homicidal brute', 'At the beginning of your end step, if Homicidal Brute didn\'t attack this turn, tap Homicidal Brute, then transform it.').
card_layout('homicidal brute', 'double-faced').
card_power('homicidal brute', 5).
card_toughness('homicidal brute', 1).

% Found in: AVR
card_name('homicidal seclusion', 'Homicidal Seclusion').
card_type('homicidal seclusion', 'Enchantment').
card_types('homicidal seclusion', ['Enchantment']).
card_subtypes('homicidal seclusion', []).
card_colors('homicidal seclusion', ['B']).
card_text('homicidal seclusion', 'As long as you control exactly one creature, that creature gets +3/+1 and has lifelink.').
card_mana_cost('homicidal seclusion', ['4', 'B']).
card_cmc('homicidal seclusion', 5).
card_layout('homicidal seclusion', 'normal').

% Found in: GTC
card_name('homing lightning', 'Homing Lightning').
card_type('homing lightning', 'Instant').
card_types('homing lightning', ['Instant']).
card_subtypes('homing lightning', []).
card_colors('homing lightning', ['R']).
card_text('homing lightning', 'Homing Lightning deals 4 damage to target creature and each other creature with the same name as that creature.').
card_mana_cost('homing lightning', ['2', 'R', 'R']).
card_cmc('homing lightning', 4).
card_layout('homing lightning', 'normal').

% Found in: FUT, H09
card_name('homing sliver', 'Homing Sliver').
card_type('homing sliver', 'Creature — Sliver').
card_types('homing sliver', ['Creature']).
card_subtypes('homing sliver', ['Sliver']).
card_colors('homing sliver', ['R']).
card_text('homing sliver', 'Each Sliver card in each player\'s hand has slivercycling {3}.\nSlivercycling {3} ({3}, Discard this card: Search your library for a Sliver card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('homing sliver', ['2', 'R']).
card_cmc('homing sliver', 3).
card_layout('homing sliver', 'normal').
card_power('homing sliver', 2).
card_toughness('homing sliver', 2).

% Found in: SOK
card_name('homura\'s essence', 'Homura\'s Essence').
card_type('homura\'s essence', 'Legendary Enchantment').
card_types('homura\'s essence', ['Enchantment']).
card_subtypes('homura\'s essence', []).
card_supertypes('homura\'s essence', ['Legendary']).
card_colors('homura\'s essence', ['R']).
card_text('homura\'s essence', 'Creatures you control get +2/+2 and have flying and \"{R}: This creature gets +1/+0 until end of turn.\"').
card_mana_cost('homura\'s essence', ['4', 'R', 'R']).
card_cmc('homura\'s essence', 6).
card_layout('homura\'s essence', 'flip').

% Found in: SOK
card_name('homura, human ascendant', 'Homura, Human Ascendant').
card_type('homura, human ascendant', 'Legendary Creature — Human Monk').
card_types('homura, human ascendant', ['Creature']).
card_subtypes('homura, human ascendant', ['Human', 'Monk']).
card_supertypes('homura, human ascendant', ['Legendary']).
card_colors('homura, human ascendant', ['R']).
card_text('homura, human ascendant', 'Homura, Human Ascendant can\'t block.\nWhen Homura dies, return it to the battlefield flipped.').
card_mana_cost('homura, human ascendant', ['4', 'R', 'R']).
card_cmc('homura, human ascendant', 6).
card_layout('homura, human ascendant', 'flip').
card_power('homura, human ascendant', 4).
card_toughness('homura, human ascendant', 4).
card_sides('homura, human ascendant', 'homura\'s essence').

% Found in: CHK
card_name('honden of cleansing fire', 'Honden of Cleansing Fire').
card_type('honden of cleansing fire', 'Legendary Enchantment — Shrine').
card_types('honden of cleansing fire', ['Enchantment']).
card_subtypes('honden of cleansing fire', ['Shrine']).
card_supertypes('honden of cleansing fire', ['Legendary']).
card_colors('honden of cleansing fire', ['W']).
card_text('honden of cleansing fire', 'At the beginning of your upkeep, you gain 2 life for each Shrine you control.').
card_mana_cost('honden of cleansing fire', ['3', 'W']).
card_cmc('honden of cleansing fire', 4).
card_layout('honden of cleansing fire', 'normal').

% Found in: CHK
card_name('honden of infinite rage', 'Honden of Infinite Rage').
card_type('honden of infinite rage', 'Legendary Enchantment — Shrine').
card_types('honden of infinite rage', ['Enchantment']).
card_subtypes('honden of infinite rage', ['Shrine']).
card_supertypes('honden of infinite rage', ['Legendary']).
card_colors('honden of infinite rage', ['R']).
card_text('honden of infinite rage', 'At the beginning of your upkeep, Honden of Infinite Rage deals damage to target creature or player equal to the number of Shrines you control.').
card_mana_cost('honden of infinite rage', ['2', 'R']).
card_cmc('honden of infinite rage', 3).
card_layout('honden of infinite rage', 'normal').

% Found in: CHK
card_name('honden of life\'s web', 'Honden of Life\'s Web').
card_type('honden of life\'s web', 'Legendary Enchantment — Shrine').
card_types('honden of life\'s web', ['Enchantment']).
card_subtypes('honden of life\'s web', ['Shrine']).
card_supertypes('honden of life\'s web', ['Legendary']).
card_colors('honden of life\'s web', ['G']).
card_text('honden of life\'s web', 'At the beginning of your upkeep, put a 1/1 colorless Spirit creature token onto the battlefield for each Shrine you control.').
card_mana_cost('honden of life\'s web', ['4', 'G']).
card_cmc('honden of life\'s web', 5).
card_layout('honden of life\'s web', 'normal').

% Found in: CHK
card_name('honden of night\'s reach', 'Honden of Night\'s Reach').
card_type('honden of night\'s reach', 'Legendary Enchantment — Shrine').
card_types('honden of night\'s reach', ['Enchantment']).
card_subtypes('honden of night\'s reach', ['Shrine']).
card_supertypes('honden of night\'s reach', ['Legendary']).
card_colors('honden of night\'s reach', ['B']).
card_text('honden of night\'s reach', 'At the beginning of your upkeep, target opponent discards a card for each Shrine you control.').
card_mana_cost('honden of night\'s reach', ['3', 'B']).
card_cmc('honden of night\'s reach', 4).
card_layout('honden of night\'s reach', 'normal').

% Found in: CHK
card_name('honden of seeing winds', 'Honden of Seeing Winds').
card_type('honden of seeing winds', 'Legendary Enchantment — Shrine').
card_types('honden of seeing winds', ['Enchantment']).
card_subtypes('honden of seeing winds', ['Shrine']).
card_supertypes('honden of seeing winds', ['Legendary']).
card_colors('honden of seeing winds', ['U']).
card_text('honden of seeing winds', 'At the beginning of your upkeep, draw a card for each Shrine you control.').
card_mana_cost('honden of seeing winds', ['4', 'U']).
card_cmc('honden of seeing winds', 5).
card_layout('honden of seeing winds', 'normal').

% Found in: 10E, 7ED, 8ED, 9ED, STH
card_name('honor guard', 'Honor Guard').
card_type('honor guard', 'Creature — Human Soldier').
card_types('honor guard', ['Creature']).
card_subtypes('honor guard', ['Human', 'Soldier']).
card_colors('honor guard', ['W']).
card_text('honor guard', '{W}: Honor Guard gets +0/+1 until end of turn.').
card_mana_cost('honor guard', ['W']).
card_cmc('honor guard', 1).
card_layout('honor guard', 'normal').
card_power('honor guard', 1).
card_toughness('honor guard', 1).

% Found in: M10, M11, M12, MD1, pMEI
card_name('honor of the pure', 'Honor of the Pure').
card_type('honor of the pure', 'Enchantment').
card_types('honor of the pure', ['Enchantment']).
card_subtypes('honor of the pure', []).
card_colors('honor of the pure', ['W']).
card_text('honor of the pure', 'White creatures you control get +1/+1.').
card_mana_cost('honor of the pure', ['1', 'W']).
card_cmc('honor of the pure', 2).
card_layout('honor of the pure', 'normal').

% Found in: MMQ
card_name('honor the fallen', 'Honor the Fallen').
card_type('honor the fallen', 'Instant').
card_types('honor the fallen', ['Instant']).
card_subtypes('honor the fallen', []).
card_colors('honor the fallen', ['W']).
card_text('honor the fallen', 'Exile all creature cards from all graveyards. You gain 1 life for each card exiled this way.').
card_mana_cost('honor the fallen', ['1', 'W']).
card_cmc('honor the fallen', 2).
card_layout('honor the fallen', 'normal').

% Found in: FRF
card_name('honor\'s reward', 'Honor\'s Reward').
card_type('honor\'s reward', 'Instant').
card_types('honor\'s reward', ['Instant']).
card_subtypes('honor\'s reward', []).
card_colors('honor\'s reward', ['W']).
card_text('honor\'s reward', 'You gain 4 life. Bolster 2. (Choose a creature with the least toughness among creatures you control and put two +1/+1 counters on it.)').
card_mana_cost('honor\'s reward', ['2', 'W']).
card_cmc('honor\'s reward', 3).
card_layout('honor\'s reward', 'normal').

% Found in: CHK
card_name('honor-worn shaku', 'Honor-Worn Shaku').
card_type('honor-worn shaku', 'Artifact').
card_types('honor-worn shaku', ['Artifact']).
card_subtypes('honor-worn shaku', []).
card_colors('honor-worn shaku', []).
card_text('honor-worn shaku', '{T}: Add {1} to your mana pool.\nTap an untapped legendary permanent you control: Untap Honor-Worn Shaku.').
card_mana_cost('honor-worn shaku', ['3']).
card_cmc('honor-worn shaku', 3).
card_layout('honor-worn shaku', 'normal').

% Found in: TSB, VIS
card_name('honorable passage', 'Honorable Passage').
card_type('honorable passage', 'Instant').
card_types('honorable passage', ['Instant']).
card_subtypes('honorable passage', []).
card_colors('honorable passage', ['W']).
card_text('honorable passage', 'The next time a source of your choice would deal damage to target creature or player this turn, prevent that damage. If damage from a red source is prevented this way, Honorable Passage deals that much damage to the source\'s controller.').
card_mana_cost('honorable passage', ['1', 'W']).
card_cmc('honorable passage', 2).
card_layout('honorable passage', 'normal').

% Found in: PLS
card_name('honorable scout', 'Honorable Scout').
card_type('honorable scout', 'Creature — Human Soldier Scout').
card_types('honorable scout', ['Creature']).
card_subtypes('honorable scout', ['Human', 'Soldier', 'Scout']).
card_colors('honorable scout', ['W']).
card_text('honorable scout', 'When Honorable Scout enters the battlefield, you gain 2 life for each black and/or red creature target opponent controls.').
card_mana_cost('honorable scout', ['W']).
card_cmc('honorable scout', 1).
card_layout('honorable scout', 'normal').
card_power('honorable scout', 1).
card_toughness('honorable scout', 1).

% Found in: ORI
card_name('honored hierarch', 'Honored Hierarch').
card_type('honored hierarch', 'Creature — Human Druid').
card_types('honored hierarch', ['Creature']).
card_subtypes('honored hierarch', ['Human', 'Druid']).
card_colors('honored hierarch', ['G']).
card_text('honored hierarch', 'Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)\nAs long as Honored Hierarch is renowned, it has vigilance and \"{T}: Add one mana of any color to your mana pool.\"').
card_mana_cost('honored hierarch', ['G']).
card_cmc('honored hierarch', 1).
card_layout('honored hierarch', 'normal').
card_power('honored hierarch', 1).
card_toughness('honored hierarch', 1).

% Found in: FRF
card_name('hooded assassin', 'Hooded Assassin').
card_type('hooded assassin', 'Creature — Human Assassin').
card_types('hooded assassin', ['Creature']).
card_subtypes('hooded assassin', ['Human', 'Assassin']).
card_colors('hooded assassin', ['B']).
card_text('hooded assassin', 'When Hooded Assassin enters the battlefield, choose one —\n• Put a +1/+1 counter on Hooded Assassin.\n• Destroy target creature that was dealt damage this turn.').
card_mana_cost('hooded assassin', ['2', 'B']).
card_cmc('hooded assassin', 3).
card_layout('hooded assassin', 'normal').
card_power('hooded assassin', 1).
card_toughness('hooded assassin', 2).

% Found in: C13
card_name('hooded horror', 'Hooded Horror').
card_type('hooded horror', 'Creature — Horror').
card_types('hooded horror', ['Creature']).
card_subtypes('hooded horror', ['Horror']).
card_colors('hooded horror', ['B']).
card_text('hooded horror', 'Hooded Horror can\'t be blocked as long as defending player controls the most creatures or is tied for the most.').
card_mana_cost('hooded horror', ['4', 'B']).
card_cmc('hooded horror', 5).
card_layout('hooded horror', 'normal').
card_power('hooded horror', 4).
card_toughness('hooded horror', 4).

% Found in: KTK
card_name('hooded hydra', 'Hooded Hydra').
card_type('hooded hydra', 'Creature — Snake Hydra').
card_types('hooded hydra', ['Creature']).
card_subtypes('hooded hydra', ['Snake', 'Hydra']).
card_colors('hooded hydra', ['G']).
card_text('hooded hydra', 'Hooded Hydra enters the battlefield with X +1/+1 counters on it.\nWhen Hooded Hydra dies, put a 1/1 green Snake creature token onto the battlefield for each +1/+1 counter on it.\nMorph {3}{G}{G}\nAs Hooded Hydra is turned face up, put five +1/+1 counters on it.').
card_mana_cost('hooded hydra', ['X', 'G', 'G']).
card_cmc('hooded hydra', 2).
card_layout('hooded hydra', 'normal').
card_power('hooded hydra', 0).
card_toughness('hooded hydra', 0).

% Found in: INV
card_name('hooded kavu', 'Hooded Kavu').
card_type('hooded kavu', 'Creature — Kavu').
card_types('hooded kavu', ['Creature']).
card_subtypes('hooded kavu', ['Kavu']).
card_colors('hooded kavu', ['R']).
card_text('hooded kavu', '{B}: Hooded Kavu gains fear until end of turn. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('hooded kavu', ['2', 'R']).
card_cmc('hooded kavu', 3).
card_layout('hooded kavu', 'normal').
card_power('hooded kavu', 2).
card_toughness('hooded kavu', 2).

% Found in: MMQ
card_name('hoodwink', 'Hoodwink').
card_type('hoodwink', 'Instant').
card_types('hoodwink', ['Instant']).
card_subtypes('hoodwink', []).
card_colors('hoodwink', ['U']).
card_text('hoodwink', 'Return target artifact, enchantment, or land to its owner\'s hand.').
card_mana_cost('hoodwink', ['1', 'U']).
card_cmc('hoodwink', 2).
card_layout('hoodwink', 'normal').

% Found in: EVE
card_name('hoof skulkin', 'Hoof Skulkin').
card_type('hoof skulkin', 'Artifact Creature — Scarecrow').
card_types('hoof skulkin', ['Artifact', 'Creature']).
card_subtypes('hoof skulkin', ['Scarecrow']).
card_colors('hoof skulkin', []).
card_text('hoof skulkin', '{3}: Target green creature gets +1/+1 until end of turn.').
card_mana_cost('hoof skulkin', ['3']).
card_cmc('hoof skulkin', 3).
card_layout('hoof skulkin', 'normal').
card_power('hoof skulkin', 2).
card_toughness('hoof skulkin', 2).

% Found in: LRW
card_name('hoofprints of the stag', 'Hoofprints of the Stag').
card_type('hoofprints of the stag', 'Tribal Enchantment — Elemental').
card_types('hoofprints of the stag', ['Tribal', 'Enchantment']).
card_subtypes('hoofprints of the stag', ['Elemental']).
card_colors('hoofprints of the stag', ['W']).
card_text('hoofprints of the stag', 'Whenever you draw a card, you may put a hoofprint counter on Hoofprints of the Stag.\n{2}{W}, Remove four hoofprint counters from Hoofprints of the Stag: Put a 4/4 white Elemental creature token with flying onto the battlefield. Activate this ability only during your turn.').
card_mana_cost('hoofprints of the stag', ['1', 'W']).
card_cmc('hoofprints of the stag', 2).
card_layout('hoofprints of the stag', 'normal').

% Found in: KTK
card_name('hooting mandrills', 'Hooting Mandrills').
card_type('hooting mandrills', 'Creature — Ape').
card_types('hooting mandrills', ['Creature']).
card_subtypes('hooting mandrills', ['Ape']).
card_colors('hooting mandrills', ['G']).
card_text('hooting mandrills', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nTrample').
card_mana_cost('hooting mandrills', ['5', 'G']).
card_cmc('hooting mandrills', 6).
card_layout('hooting mandrills', 'normal').
card_power('hooting mandrills', 4).
card_toughness('hooting mandrills', 4).

% Found in: ULG
card_name('hope and glory', 'Hope and Glory').
card_type('hope and glory', 'Instant').
card_types('hope and glory', ['Instant']).
card_subtypes('hope and glory', []).
card_colors('hope and glory', ['W']).
card_text('hope and glory', 'Untap two target creatures. Each of them gets +1/+1 until end of turn.').
card_mana_cost('hope and glory', ['1', 'W']).
card_cmc('hope and glory', 2).
card_layout('hope and glory', 'normal').

% Found in: VIS
card_name('hope charm', 'Hope Charm').
card_type('hope charm', 'Instant').
card_types('hope charm', ['Instant']).
card_subtypes('hope charm', []).
card_colors('hope charm', ['W']).
card_text('hope charm', 'Choose one —\n• Target creature gains first strike until end of turn.\n• Target player gains 2 life.\n• Destroy target Aura.').
card_mana_cost('hope charm', ['W']).
card_cmc('hope charm', 1).
card_layout('hope charm', 'normal').

% Found in: THS
card_name('hopeful eidolon', 'Hopeful Eidolon').
card_type('hopeful eidolon', 'Enchantment Creature — Spirit').
card_types('hopeful eidolon', ['Enchantment', 'Creature']).
card_subtypes('hopeful eidolon', ['Spirit']).
card_colors('hopeful eidolon', ['W']).
card_text('hopeful eidolon', 'Bestow {3}{W} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)\nEnchanted creature gets +1/+1 and has lifelink.').
card_mana_cost('hopeful eidolon', ['W']).
card_cmc('hopeful eidolon', 1).
card_layout('hopeful eidolon', 'normal').
card_power('hopeful eidolon', 1).
card_toughness('hopeful eidolon', 1).

% Found in: USG
card_name('hopping automaton', 'Hopping Automaton').
card_type('hopping automaton', 'Artifact Creature — Construct').
card_types('hopping automaton', ['Artifact', 'Creature']).
card_subtypes('hopping automaton', ['Construct']).
card_colors('hopping automaton', []).
card_text('hopping automaton', '{0}: Hopping Automaton gets -1/-1 and gains flying until end of turn.').
card_mana_cost('hopping automaton', ['3']).
card_cmc('hopping automaton', 3).
card_layout('hopping automaton', 'normal').
card_power('hopping automaton', 2).
card_toughness('hopping automaton', 2).

% Found in: KTK
card_name('horde ambusher', 'Horde Ambusher').
card_type('horde ambusher', 'Creature — Human Berserker').
card_types('horde ambusher', ['Creature']).
card_subtypes('horde ambusher', ['Human', 'Berserker']).
card_colors('horde ambusher', ['R']).
card_text('horde ambusher', 'Whenever Horde Ambusher blocks, it deals 1 damage to you.\nMorph—Reveal a red card in your hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Horde Ambusher is turned face up, target creature can\'t block this turn.').
card_mana_cost('horde ambusher', ['1', 'R']).
card_cmc('horde ambusher', 2).
card_layout('horde ambusher', 'normal').
card_power('horde ambusher', 2).
card_toughness('horde ambusher', 2).

% Found in: SHM
card_name('horde of boggarts', 'Horde of Boggarts').
card_type('horde of boggarts', 'Creature — Goblin').
card_types('horde of boggarts', ['Creature']).
card_subtypes('horde of boggarts', ['Goblin']).
card_colors('horde of boggarts', ['R']).
card_text('horde of boggarts', 'Menace (This creature can\'t be blocked except by two or more creatures.)\nHorde of Boggarts\'s power and toughness are each equal to the number of red permanents you control.').
card_mana_cost('horde of boggarts', ['3', 'R']).
card_cmc('horde of boggarts', 4).
card_layout('horde of boggarts', 'normal').
card_power('horde of boggarts', '*').
card_toughness('horde of boggarts', '*').

% Found in: LRW, MM2
card_name('horde of notions', 'Horde of Notions').
card_type('horde of notions', 'Legendary Creature — Elemental').
card_types('horde of notions', ['Creature']).
card_subtypes('horde of notions', ['Elemental']).
card_supertypes('horde of notions', ['Legendary']).
card_colors('horde of notions', ['W', 'U', 'B', 'R', 'G']).
card_text('horde of notions', 'Vigilance, trample, haste\n{W}{U}{B}{R}{G}: You may play target Elemental card from your graveyard without paying its mana cost.').
card_mana_cost('horde of notions', ['W', 'U', 'B', 'R', 'G']).
card_cmc('horde of notions', 5).
card_layout('horde of notions', 'normal').
card_power('horde of notions', 5).
card_toughness('horde of notions', 5).

% Found in: KTK, pFNM
card_name('hordeling outburst', 'Hordeling Outburst').
card_type('hordeling outburst', 'Sorcery').
card_types('hordeling outburst', ['Sorcery']).
card_subtypes('hordeling outburst', []).
card_colors('hordeling outburst', ['R']).
card_text('hordeling outburst', 'Put three 1/1 red Goblin creature tokens onto the battlefield.').
card_mana_cost('hordeling outburst', ['1', 'R', 'R']).
card_cmc('hordeling outburst', 3).
card_layout('hordeling outburst', 'normal').

% Found in: FUT
card_name('horizon canopy', 'Horizon Canopy').
card_type('horizon canopy', 'Land').
card_types('horizon canopy', ['Land']).
card_subtypes('horizon canopy', []).
card_colors('horizon canopy', []).
card_text('horizon canopy', '{T}, Pay 1 life: Add {G} or {W} to your mana pool.\n{1}, {T}, Sacrifice Horizon Canopy: Draw a card.').
card_layout('horizon canopy', 'normal').

% Found in: THS
card_name('horizon chimera', 'Horizon Chimera').
card_type('horizon chimera', 'Creature — Chimera').
card_types('horizon chimera', ['Creature']).
card_subtypes('horizon chimera', ['Chimera']).
card_colors('horizon chimera', ['U', 'G']).
card_text('horizon chimera', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying, trample\nWhenever you draw a card, you gain 1 life.').
card_mana_cost('horizon chimera', ['2', 'G', 'U']).
card_cmc('horizon chimera', 4).
card_layout('horizon chimera', 'normal').
card_power('horizon chimera', 3).
card_toughness('horizon chimera', 2).

% Found in: WWK
card_name('horizon drake', 'Horizon Drake').
card_type('horizon drake', 'Creature — Drake').
card_types('horizon drake', ['Creature']).
card_subtypes('horizon drake', ['Drake']).
card_colors('horizon drake', ['U']).
card_text('horizon drake', 'Flying, protection from lands').
card_mana_cost('horizon drake', ['1', 'U', 'U']).
card_cmc('horizon drake', 3).
card_layout('horizon drake', 'normal').
card_power('horizon drake', 3).
card_toughness('horizon drake', 1).

% Found in: THS
card_name('horizon scholar', 'Horizon Scholar').
card_type('horizon scholar', 'Creature — Sphinx').
card_types('horizon scholar', ['Creature']).
card_subtypes('horizon scholar', ['Sphinx']).
card_colors('horizon scholar', ['U']).
card_text('horizon scholar', 'Flying\nWhen Horizon Scholar enters the battlefield, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('horizon scholar', ['5', 'U']).
card_cmc('horizon scholar', 6).
card_layout('horizon scholar', 'normal').
card_power('horizon scholar', 4).
card_toughness('horizon scholar', 4).

% Found in: CHK
card_name('horizon seed', 'Horizon Seed').
card_type('horizon seed', 'Creature — Spirit').
card_types('horizon seed', ['Creature']).
card_subtypes('horizon seed', ['Spirit']).
card_colors('horizon seed', ['W']).
card_text('horizon seed', 'Whenever you cast a Spirit or Arcane spell, regenerate target creature.').
card_mana_cost('horizon seed', ['4', 'W']).
card_cmc('horizon seed', 5).
card_layout('horizon seed', 'normal').
card_power('horizon seed', 2).
card_toughness('horizon seed', 1).

% Found in: SOM
card_name('horizon spellbomb', 'Horizon Spellbomb').
card_type('horizon spellbomb', 'Artifact').
card_types('horizon spellbomb', ['Artifact']).
card_subtypes('horizon spellbomb', []).
card_colors('horizon spellbomb', []).
card_text('horizon spellbomb', '{2}, {T}, Sacrifice Horizon Spellbomb: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.\nWhen Horizon Spellbomb is put into a graveyard from the battlefield, you may pay {G}. If you do, draw a card.').
card_mana_cost('horizon spellbomb', ['1']).
card_cmc('horizon spellbomb', 1).
card_layout('horizon spellbomb', 'normal').

% Found in: CHR, LEG, ME4
card_name('horn of deafening', 'Horn of Deafening').
card_type('horn of deafening', 'Artifact').
card_types('horn of deafening', ['Artifact']).
card_subtypes('horn of deafening', []).
card_colors('horn of deafening', []).
card_text('horn of deafening', '{2}, {T}: Prevent all combat damage that would be dealt by target creature this turn.').
card_mana_cost('horn of deafening', ['4']).
card_cmc('horn of deafening', 4).
card_layout('horn of deafening', 'normal').

% Found in: STH
card_name('horn of greed', 'Horn of Greed').
card_type('horn of greed', 'Artifact').
card_types('horn of greed', ['Artifact']).
card_subtypes('horn of greed', []).
card_colors('horn of greed', []).
card_text('horn of greed', 'Whenever a player plays a land, that player draws a card.').
card_mana_cost('horn of greed', ['3']).
card_cmc('horn of greed', 3).
card_layout('horn of greed', 'normal').

% Found in: MMQ
card_name('horn of plenty', 'Horn of Plenty').
card_type('horn of plenty', 'Artifact').
card_types('horn of plenty', ['Artifact']).
card_subtypes('horn of plenty', []).
card_colors('horn of plenty', []).
card_text('horn of plenty', 'Whenever a player casts a spell, he or she may pay {1}. If that player does, he or she draws a card at the beginning of the next end step.').
card_mana_cost('horn of plenty', ['6']).
card_cmc('horn of plenty', 6).
card_layout('horn of plenty', 'normal').

% Found in: MMQ
card_name('horn of ramos', 'Horn of Ramos').
card_type('horn of ramos', 'Artifact').
card_types('horn of ramos', ['Artifact']).
card_subtypes('horn of ramos', []).
card_colors('horn of ramos', []).
card_text('horn of ramos', '{T}: Add {G} to your mana pool.\nSacrifice Horn of Ramos: Add {G} to your mana pool.').
card_mana_cost('horn of ramos', ['3']).
card_cmc('horn of ramos', 3).
card_layout('horn of ramos', 'normal').

% Found in: RTR
card_name('horncaller\'s chant', 'Horncaller\'s Chant').
card_type('horncaller\'s chant', 'Sorcery').
card_types('horncaller\'s chant', ['Sorcery']).
card_subtypes('horncaller\'s chant', []).
card_colors('horncaller\'s chant', ['G']).
card_text('horncaller\'s chant', 'Put a 4/4 green Rhino creature token with trample onto the battlefield, then populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_mana_cost('horncaller\'s chant', ['7', 'G']).
card_cmc('horncaller\'s chant', 8).
card_layout('horncaller\'s chant', 'normal').

% Found in: INV
card_name('horned cheetah', 'Horned Cheetah').
card_type('horned cheetah', 'Creature — Cat').
card_types('horned cheetah', ['Creature']).
card_subtypes('horned cheetah', ['Cat']).
card_colors('horned cheetah', ['W', 'G']).
card_text('horned cheetah', 'Whenever Horned Cheetah deals damage, you gain that much life.').
card_mana_cost('horned cheetah', ['2', 'G', 'W']).
card_cmc('horned cheetah', 4).
card_layout('horned cheetah', 'normal').
card_power('horned cheetah', 2).
card_toughness('horned cheetah', 2).

% Found in: 5DN
card_name('horned helm', 'Horned Helm').
card_type('horned helm', 'Artifact — Equipment').
card_types('horned helm', ['Artifact']).
card_subtypes('horned helm', ['Equipment']).
card_colors('horned helm', []).
card_text('horned helm', 'Equipped creature gets +1/+1 and has trample.\n{G}{G}: Attach Horned Helm to target creature you control.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('horned helm', ['2']).
card_cmc('horned helm', 2).
card_layout('horned helm', 'normal').

% Found in: PLS
card_name('horned kavu', 'Horned Kavu').
card_type('horned kavu', 'Creature — Kavu').
card_types('horned kavu', ['Creature']).
card_subtypes('horned kavu', ['Kavu']).
card_colors('horned kavu', ['R', 'G']).
card_text('horned kavu', 'When Horned Kavu enters the battlefield, return a red or green creature you control to its owner\'s hand.').
card_mana_cost('horned kavu', ['R', 'G']).
card_cmc('horned kavu', 2).
card_layout('horned kavu', 'normal').
card_power('horned kavu', 3).
card_toughness('horned kavu', 4).

% Found in: TMP, TPR
card_name('horned sliver', 'Horned Sliver').
card_type('horned sliver', 'Creature — Sliver').
card_types('horned sliver', ['Creature']).
card_subtypes('horned sliver', ['Sliver']).
card_colors('horned sliver', ['G']).
card_text('horned sliver', 'All Sliver creatures have trample.').
card_mana_cost('horned sliver', ['2', 'G']).
card_cmc('horned sliver', 3).
card_layout('horned sliver', 'normal').
card_power('horned sliver', 2).
card_toughness('horned sliver', 2).

% Found in: 8ED, MMQ
card_name('horned troll', 'Horned Troll').
card_type('horned troll', 'Creature — Troll').
card_types('horned troll', ['Creature']).
card_subtypes('horned troll', ['Troll']).
card_colors('horned troll', ['G']).
card_text('horned troll', '{G}: Regenerate Horned Troll.').
card_mana_cost('horned troll', ['2', 'G']).
card_cmc('horned troll', 3).
card_layout('horned troll', 'normal').
card_power('horned troll', 2).
card_toughness('horned troll', 2).

% Found in: 6ED, 7ED, 8ED, 9ED, M10, POR, TMP, TPR
card_name('horned turtle', 'Horned Turtle').
card_type('horned turtle', 'Creature — Turtle').
card_types('horned turtle', ['Creature']).
card_subtypes('horned turtle', ['Turtle']).
card_colors('horned turtle', ['U']).
card_text('horned turtle', '').
card_mana_cost('horned turtle', ['2', 'U']).
card_cmc('horned turtle', 3).
card_layout('horned turtle', 'normal').
card_power('horned turtle', 1).
card_toughness('horned turtle', 4).

% Found in: DDE
card_name('hornet', 'Hornet').
card_type('hornet', 'Artifact Creature — Insect').
card_types('hornet', ['Artifact', 'Creature']).
card_subtypes('hornet', ['Insect']).
card_colors('hornet', []).
card_text('hornet', 'Flying, haste').
card_layout('hornet', 'token').
card_power('hornet', 1).
card_toughness('hornet', 1).

% Found in: DDE, STH
card_name('hornet cannon', 'Hornet Cannon').
card_type('hornet cannon', 'Artifact').
card_types('hornet cannon', ['Artifact']).
card_subtypes('hornet cannon', []).
card_colors('hornet cannon', []).
card_text('hornet cannon', '{3}, {T}: Put a 1/1 colorless Insect artifact creature token with flying and haste named Hornet onto the battlefield. Destroy it at the beginning of the next end step.').
card_mana_cost('hornet cannon', ['4']).
card_cmc('hornet cannon', 4).
card_layout('hornet cannon', 'normal').

% Found in: LEG
card_name('hornet cobra', 'Hornet Cobra').
card_type('hornet cobra', 'Creature — Snake').
card_types('hornet cobra', ['Creature']).
card_subtypes('hornet cobra', ['Snake']).
card_colors('hornet cobra', ['G']).
card_text('hornet cobra', 'First strike').
card_mana_cost('hornet cobra', ['1', 'G', 'G']).
card_cmc('hornet cobra', 3).
card_layout('hornet cobra', 'normal').
card_power('hornet cobra', 2).
card_toughness('hornet cobra', 1).

% Found in: LRW
card_name('hornet harasser', 'Hornet Harasser').
card_type('hornet harasser', 'Creature — Goblin Shaman').
card_types('hornet harasser', ['Creature']).
card_subtypes('hornet harasser', ['Goblin', 'Shaman']).
card_colors('hornet harasser', ['B']).
card_text('hornet harasser', 'When Hornet Harasser dies, target creature gets -2/-2 until end of turn.').
card_mana_cost('hornet harasser', ['2', 'B', 'B']).
card_cmc('hornet harasser', 4).
card_layout('hornet harasser', 'normal').
card_power('hornet harasser', 2).
card_toughness('hornet harasser', 2).

% Found in: M15
card_name('hornet nest', 'Hornet Nest').
card_type('hornet nest', 'Creature — Insect').
card_types('hornet nest', ['Creature']).
card_subtypes('hornet nest', ['Insect']).
card_colors('hornet nest', ['G']).
card_text('hornet nest', 'Defender (This creature can\'t attack.)\nWhenever Hornet Nest is dealt damage, put that many 1/1 green Insect creature tokens with flying and deathtouch onto the battlefield. (Any amount of damage a creature with deathtouch deals to a creature is enough to destroy it.)').
card_mana_cost('hornet nest', ['2', 'G']).
card_cmc('hornet nest', 3).
card_layout('hornet nest', 'normal').
card_power('hornet nest', 0).
card_toughness('hornet nest', 2).

% Found in: CMD, M15
card_name('hornet queen', 'Hornet Queen').
card_type('hornet queen', 'Creature — Insect').
card_types('hornet queen', ['Creature']).
card_subtypes('hornet queen', ['Insect']).
card_colors('hornet queen', ['G']).
card_text('hornet queen', 'Flying\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nWhen Hornet Queen enters the battlefield, put four 1/1 green Insect creature tokens with flying and deathtouch onto the battlefield.').
card_mana_cost('hornet queen', ['4', 'G', 'G', 'G']).
card_cmc('hornet queen', 7).
card_layout('hornet queen', 'normal').
card_power('hornet queen', 2).
card_toughness('hornet queen', 2).

% Found in: M11
card_name('hornet sting', 'Hornet Sting').
card_type('hornet sting', 'Instant').
card_types('hornet sting', ['Instant']).
card_subtypes('hornet sting', []).
card_colors('hornet sting', ['G']).
card_text('hornet sting', 'Hornet Sting deals 1 damage to target creature or player.').
card_mana_cost('hornet sting', ['G']).
card_cmc('hornet sting', 1).
card_layout('hornet sting', 'normal').

% Found in: BOK, MMA
card_name('horobi\'s whisper', 'Horobi\'s Whisper').
card_type('horobi\'s whisper', 'Instant — Arcane').
card_types('horobi\'s whisper', ['Instant']).
card_subtypes('horobi\'s whisper', ['Arcane']).
card_colors('horobi\'s whisper', ['B']).
card_text('horobi\'s whisper', 'If you control a Swamp, destroy target nonblack creature.\nSplice onto Arcane—Exile four cards from your graveyard. (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('horobi\'s whisper', ['1', 'B', 'B']).
card_cmc('horobi\'s whisper', 3).
card_layout('horobi\'s whisper', 'normal').

% Found in: CHK
card_name('horobi, death\'s wail', 'Horobi, Death\'s Wail').
card_type('horobi, death\'s wail', 'Legendary Creature — Spirit').
card_types('horobi, death\'s wail', ['Creature']).
card_subtypes('horobi, death\'s wail', ['Spirit']).
card_supertypes('horobi, death\'s wail', ['Legendary']).
card_colors('horobi, death\'s wail', ['B']).
card_text('horobi, death\'s wail', 'Flying\nWhenever a creature becomes the target of a spell or ability, destroy that creature.').
card_mana_cost('horobi, death\'s wail', ['2', 'B', 'B']).
card_cmc('horobi, death\'s wail', 4).
card_layout('horobi, death\'s wail', 'normal').
card_power('horobi, death\'s wail', 4).
card_toughness('horobi, death\'s wail', 4).

% Found in: MIR
card_name('horrible hordes', 'Horrible Hordes').
card_type('horrible hordes', 'Artifact Creature — Spirit').
card_types('horrible hordes', ['Artifact', 'Creature']).
card_subtypes('horrible hordes', ['Spirit']).
card_colors('horrible hordes', []).
card_text('horrible hordes', 'Rampage 1 (Whenever this creature becomes blocked, it gets +1/+1 until end of turn for each creature blocking it beyond the first.)').
card_mana_cost('horrible hordes', ['3']).
card_cmc('horrible hordes', 3).
card_layout('horrible hordes', 'normal').
card_power('horrible hordes', 2).
card_toughness('horrible hordes', 2).

% Found in: BFZ
card_name('horribly awry', 'Horribly Awry').
card_type('horribly awry', 'Instant').
card_types('horribly awry', ['Instant']).
card_subtypes('horribly awry', []).
card_colors('horribly awry', []).
card_text('horribly awry', 'Devoid (This card has no color.)\nCounter target creature spell with converted mana cost 4 or less. If that spell is countered this way, exile it instead of putting it into its owner\'s graveyard.').
card_mana_cost('horribly awry', ['1', 'U']).
card_cmc('horribly awry', 2).
card_layout('horribly awry', 'normal').

% Found in: MBS
card_name('horrifying revelation', 'Horrifying Revelation').
card_type('horrifying revelation', 'Sorcery').
card_types('horrifying revelation', ['Sorcery']).
card_subtypes('horrifying revelation', []).
card_colors('horrifying revelation', ['B']).
card_text('horrifying revelation', 'Target player discards a card, then puts the top card of his or her library into his or her graveyard.').
card_mana_cost('horrifying revelation', ['B']).
card_cmc('horrifying revelation', 1).
card_layout('horrifying revelation', 'normal').

% Found in: 9ED, LEG
card_name('horror of horrors', 'Horror of Horrors').
card_type('horror of horrors', 'Enchantment').
card_types('horror of horrors', ['Enchantment']).
card_subtypes('horror of horrors', []).
card_colors('horror of horrors', ['B']).
card_text('horror of horrors', 'Sacrifice a Swamp: Regenerate target black creature. (The next time that creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_mana_cost('horror of horrors', ['3', 'B', 'B']).
card_cmc('horror of horrors', 5).
card_layout('horror of horrors', 'normal').

% Found in: GTC
card_name('horror of the dim', 'Horror of the Dim').
card_type('horror of the dim', 'Creature — Horror').
card_types('horror of the dim', ['Creature']).
card_subtypes('horror of the dim', ['Horror']).
card_colors('horror of the dim', ['B']).
card_text('horror of the dim', '{U}: Horror of the Dim gains hexproof until end of turn. (It can\'t be the target of spells or abilities your opponents control.)').
card_mana_cost('horror of the dim', ['4', 'B']).
card_cmc('horror of the dim', 5).
card_layout('horror of the dim', 'normal').
card_power('horror of the dim', 3).
card_toughness('horror of the dim', 4).

% Found in: 10E, USG
card_name('horseshoe crab', 'Horseshoe Crab').
card_type('horseshoe crab', 'Creature — Crab').
card_types('horseshoe crab', ['Creature']).
card_subtypes('horseshoe crab', ['Crab']).
card_colors('horseshoe crab', ['U']).
card_text('horseshoe crab', '{U}: Untap Horseshoe Crab.').
card_mana_cost('horseshoe crab', ['2', 'U']).
card_cmc('horseshoe crab', 3).
card_layout('horseshoe crab', 'normal').
card_power('horseshoe crab', 1).
card_toughness('horseshoe crab', 3).

% Found in: MOR
card_name('hostile realm', 'Hostile Realm').
card_type('hostile realm', 'Enchantment — Aura').
card_types('hostile realm', ['Enchantment']).
card_subtypes('hostile realm', ['Aura']).
card_colors('hostile realm', ['R']).
card_text('hostile realm', 'Enchant land\nEnchanted land has \"{T}: Target creature can\'t block this turn.\"').
card_mana_cost('hostile realm', ['2', 'R']).
card_cmc('hostile realm', 3).
card_layout('hostile realm', 'normal').

% Found in: DD2, DD3_JVC, LRW
card_name('hostility', 'Hostility').
card_type('hostility', 'Creature — Elemental Incarnation').
card_types('hostility', ['Creature']).
card_subtypes('hostility', ['Elemental', 'Incarnation']).
card_colors('hostility', ['R']).
card_text('hostility', 'Haste\nIf a spell you control would deal damage to an opponent, prevent that damage. Put a 3/1 red Elemental Shaman creature token with haste onto the battlefield for each 1 damage prevented this way.\nWhen Hostility is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_mana_cost('hostility', ['3', 'R', 'R', 'R']).
card_cmc('hostility', 6).
card_layout('hostility', 'normal').
card_power('hostility', 6).
card_toughness('hostility', 6).

% Found in: M15
card_name('hot soup', 'Hot Soup').
card_type('hot soup', 'Artifact — Equipment').
card_types('hot soup', ['Artifact']).
card_subtypes('hot soup', ['Equipment']).
card_colors('hot soup', []).
card_text('hot soup', 'Equipped creature can\'t be blocked.\nWhenever equipped creature is dealt damage, destroy it.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('hot soup', ['1']).
card_cmc('hot soup', 1).
card_layout('hot soup', 'normal').

% Found in: ICE
card_name('hot springs', 'Hot Springs').
card_type('hot springs', 'Enchantment — Aura').
card_types('hot springs', ['Enchantment']).
card_subtypes('hot springs', ['Aura']).
card_colors('hot springs', ['G']).
card_text('hot springs', 'Enchant land you control\nEnchanted land has \"{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\"').
card_mana_cost('hot springs', ['1', 'G']).
card_cmc('hot springs', 2).
card_layout('hot springs', 'normal').
card_reserved('hot springs').

% Found in: EVE
card_name('hotheaded giant', 'Hotheaded Giant').
card_type('hotheaded giant', 'Creature — Giant Warrior').
card_types('hotheaded giant', ['Creature']).
card_subtypes('hotheaded giant', ['Giant', 'Warrior']).
card_colors('hotheaded giant', ['R']).
card_text('hotheaded giant', 'Haste\nHotheaded Giant enters the battlefield with two -1/-1 counters on it unless you\'ve cast another red spell this turn.').
card_mana_cost('hotheaded giant', ['3', 'R']).
card_cmc('hotheaded giant', 4).
card_layout('hotheaded giant', 'normal').
card_power('hotheaded giant', 4).
card_toughness('hotheaded giant', 4).

% Found in: AVR
card_name('hound of griselbrand', 'Hound of Griselbrand').
card_type('hound of griselbrand', 'Creature — Elemental Hound').
card_types('hound of griselbrand', ['Creature']).
card_subtypes('hound of griselbrand', ['Elemental', 'Hound']).
card_colors('hound of griselbrand', ['R']).
card_text('hound of griselbrand', 'Double strike\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('hound of griselbrand', ['2', 'R', 'R']).
card_cmc('hound of griselbrand', 4).
card_layout('hound of griselbrand', 'normal').
card_power('hound of griselbrand', 2).
card_toughness('hound of griselbrand', 2).

% Found in: JOU
card_name('hour of need', 'Hour of Need').
card_type('hour of need', 'Instant').
card_types('hour of need', ['Instant']).
card_subtypes('hour of need', []).
card_colors('hour of need', ['U']).
card_text('hour of need', 'Strive — Hour of Need costs {1}{U} more to cast for each target beyond the first.\nExile any number of target creatures. For each creature exiled this way, its controller puts a 4/4 blue Sphinx creature token with flying onto the battlefield.').
card_mana_cost('hour of need', ['2', 'U']).
card_cmc('hour of need', 3).
card_layout('hour of need', 'normal').

% Found in: CMD, RAV
card_name('hour of reckoning', 'Hour of Reckoning').
card_type('hour of reckoning', 'Sorcery').
card_types('hour of reckoning', ['Sorcery']).
card_subtypes('hour of reckoning', []).
card_colors('hour of reckoning', ['W']).
card_text('hour of reckoning', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nDestroy all nontoken creatures.').
card_mana_cost('hour of reckoning', ['4', 'W', 'W', 'W']).
card_cmc('hour of reckoning', 7).
card_layout('hour of reckoning', 'normal').

% Found in: RTR
card_name('hover barrier', 'Hover Barrier').
card_type('hover barrier', 'Creature — Illusion Wall').
card_types('hover barrier', ['Creature']).
card_subtypes('hover barrier', ['Illusion', 'Wall']).
card_colors('hover barrier', ['U']).
card_text('hover barrier', 'Defender, flying').
card_mana_cost('hover barrier', ['2', 'U']).
card_cmc('hover barrier', 3).
card_layout('hover barrier', 'normal').
card_power('hover barrier', 0).
card_toughness('hover barrier', 6).

% Found in: DST
card_name('hoverguard observer', 'Hoverguard Observer').
card_type('hoverguard observer', 'Creature — Drone').
card_types('hoverguard observer', ['Creature']).
card_subtypes('hoverguard observer', ['Drone']).
card_colors('hoverguard observer', ['U']).
card_text('hoverguard observer', 'Flying\nHoverguard Observer can block only creatures with flying.').
card_mana_cost('hoverguard observer', ['2', 'U', 'U']).
card_cmc('hoverguard observer', 4).
card_layout('hoverguard observer', 'normal').
card_power('hoverguard observer', 3).
card_toughness('hoverguard observer', 3).

% Found in: 5DN, C14
card_name('hoverguard sweepers', 'Hoverguard Sweepers').
card_type('hoverguard sweepers', 'Creature — Drone').
card_types('hoverguard sweepers', ['Creature']).
card_subtypes('hoverguard sweepers', ['Drone']).
card_colors('hoverguard sweepers', ['U']).
card_text('hoverguard sweepers', 'Flying\nWhen Hoverguard Sweepers enters the battlefield, you may return up to two target creatures to their owners\' hands.').
card_mana_cost('hoverguard sweepers', ['6', 'U', 'U']).
card_cmc('hoverguard sweepers', 8).
card_layout('hoverguard sweepers', 'normal').
card_power('hoverguard sweepers', 5).
card_toughness('hoverguard sweepers', 6).

% Found in: NPH
card_name('hovermyr', 'Hovermyr').
card_type('hovermyr', 'Artifact Creature — Myr').
card_types('hovermyr', ['Artifact', 'Creature']).
card_subtypes('hovermyr', ['Myr']).
card_colors('hovermyr', []).
card_text('hovermyr', 'Flying, vigilance').
card_mana_cost('hovermyr', ['2']).
card_cmc('hovermyr', 2).
card_layout('hovermyr', 'normal').
card_power('hovermyr', 1).
card_toughness('hovermyr', 2).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, CED, CEI, ICE, LEA, LEB, ME4
card_name('howl from beyond', 'Howl from Beyond').
card_type('howl from beyond', 'Instant').
card_types('howl from beyond', ['Instant']).
card_subtypes('howl from beyond', []).
card_colors('howl from beyond', ['B']).
card_text('howl from beyond', 'Target creature gets +X/+0 until end of turn.').
card_mana_cost('howl from beyond', ['X', 'B']).
card_cmc('howl from beyond', 1).
card_layout('howl from beyond', 'normal').

% Found in: KTK
card_name('howl of the horde', 'Howl of the Horde').
card_type('howl of the horde', 'Sorcery').
card_types('howl of the horde', ['Sorcery']).
card_subtypes('howl of the horde', []).
card_colors('howl of the horde', ['R']).
card_text('howl of the horde', 'When you cast your next instant or sorcery spell this turn, copy that spell. You may choose new targets for the copy.\nRaid — If you attacked with a creature this turn, when you cast your next instant or sorcery spell this turn, copy that spell an additional time. You may choose new targets for the copy.').
card_mana_cost('howl of the horde', ['2', 'R']).
card_cmc('howl of the horde', 3).
card_layout('howl of the horde', 'normal').

% Found in: DPA, M10, M14, SHM
card_name('howl of the night pack', 'Howl of the Night Pack').
card_type('howl of the night pack', 'Sorcery').
card_types('howl of the night pack', ['Sorcery']).
card_subtypes('howl of the night pack', []).
card_colors('howl of the night pack', ['G']).
card_text('howl of the night pack', 'Put a 2/2 green Wolf creature token onto the battlefield for each Forest you control.').
card_mana_cost('howl of the night pack', ['6', 'G']).
card_cmc('howl of the night pack', 7).
card_layout('howl of the night pack', 'normal').

% Found in: AVR
card_name('howlgeist', 'Howlgeist').
card_type('howlgeist', 'Creature — Spirit Wolf').
card_types('howlgeist', ['Creature']).
card_subtypes('howlgeist', ['Spirit', 'Wolf']).
card_colors('howlgeist', ['G']).
card_text('howlgeist', 'Creatures with power less than Howlgeist\'s power can\'t block it.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('howlgeist', ['5', 'G']).
card_cmc('howlgeist', 6).
card_layout('howlgeist', 'normal').
card_power('howlgeist', 4).
card_toughness('howlgeist', 2).

% Found in: DD3_GVL, DDD, M10, M11
card_name('howling banshee', 'Howling Banshee').
card_type('howling banshee', 'Creature — Spirit').
card_types('howling banshee', ['Creature']).
card_subtypes('howling banshee', ['Spirit']).
card_colors('howling banshee', ['B']).
card_text('howling banshee', 'Flying\nWhen Howling Banshee enters the battlefield, each player loses 3 life.').
card_mana_cost('howling banshee', ['2', 'B', 'B']).
card_cmc('howling banshee', 4).
card_layout('howling banshee', 'normal').
card_power('howling banshee', 3).
card_toughness('howling banshee', 3).

% Found in: POR, S99
card_name('howling fury', 'Howling Fury').
card_type('howling fury', 'Sorcery').
card_types('howling fury', ['Sorcery']).
card_subtypes('howling fury', []).
card_colors('howling fury', ['B']).
card_text('howling fury', 'Target creature gets +4/+0 until end of turn.').
card_mana_cost('howling fury', ['2', 'B']).
card_cmc('howling fury', 3).
card_layout('howling fury', 'normal').

% Found in: ODY
card_name('howling gale', 'Howling Gale').
card_type('howling gale', 'Instant').
card_types('howling gale', ['Instant']).
card_subtypes('howling gale', []).
card_colors('howling gale', ['G']).
card_text('howling gale', 'Howling Gale deals 1 damage to each creature with flying and each player.\nFlashback {1}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('howling gale', ['1', 'G']).
card_cmc('howling gale', 2).
card_layout('howling gale', 'normal').

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, CED, CEI, CMD, LEA, LEB, M10
card_name('howling mine', 'Howling Mine').
card_type('howling mine', 'Artifact').
card_types('howling mine', ['Artifact']).
card_subtypes('howling mine', []).
card_colors('howling mine', []).
card_text('howling mine', 'At the beginning of each player\'s draw step, if Howling Mine is untapped, that player draws an additional card.').
card_mana_cost('howling mine', ['2']).
card_cmc('howling mine', 2).
card_layout('howling mine', 'normal').

% Found in: CNS, MMQ
card_name('howling wolf', 'Howling Wolf').
card_type('howling wolf', 'Creature — Wolf').
card_types('howling wolf', ['Creature']).
card_subtypes('howling wolf', ['Wolf']).
card_colors('howling wolf', ['G']).
card_text('howling wolf', 'When Howling Wolf enters the battlefield, you may search your library for up to three cards named Howling Wolf, reveal them, and put them into your hand. If you do, shuffle your library.').
card_mana_cost('howling wolf', ['2', 'G', 'G']).
card_cmc('howling wolf', 4).
card_layout('howling wolf', 'normal').
card_power('howling wolf', 2).
card_toughness('howling wolf', 2).

% Found in: ISD, pPRE
card_name('howlpack alpha', 'Howlpack Alpha').
card_type('howlpack alpha', 'Creature — Werewolf').
card_types('howlpack alpha', ['Creature']).
card_subtypes('howlpack alpha', ['Werewolf']).
card_colors('howlpack alpha', ['G']).
card_text('howlpack alpha', 'Each other creature you control that\'s a Werewolf or a Wolf gets +1/+1.\nAt the beginning of your end step, put a 2/2 green Wolf creature token onto the battlefield.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Howlpack Alpha.').
card_layout('howlpack alpha', 'double-faced').
card_power('howlpack alpha', 3).
card_toughness('howlpack alpha', 3).

% Found in: ISD
card_name('howlpack of estwald', 'Howlpack of Estwald').
card_type('howlpack of estwald', 'Creature — Werewolf').
card_types('howlpack of estwald', ['Creature']).
card_subtypes('howlpack of estwald', ['Werewolf']).
card_colors('howlpack of estwald', ['G']).
card_text('howlpack of estwald', 'At the beginning of each upkeep, if a player cast two or more spells last turn, transform Howlpack of Estwald.').
card_layout('howlpack of estwald', 'double-faced').
card_power('howlpack of estwald', 4).
card_toughness('howlpack of estwald', 6).

% Found in: LRW
card_name('howltooth hollow', 'Howltooth Hollow').
card_type('howltooth hollow', 'Land').
card_types('howltooth hollow', ['Land']).
card_subtypes('howltooth hollow', []).
card_colors('howltooth hollow', []).
card_text('howltooth hollow', 'Hideaway (This land enters the battlefield tapped. When it does, look at the top four cards of your library, exile one face down, then put the rest on the bottom of your library.)\n{T}: Add {B} to your mana pool.\n{B}, {T}: You may play the exiled card without paying its mana cost if each player has no cards in hand.').
card_layout('howltooth hollow', 'normal').

% Found in: C13, ME3, PTK
card_name('hua tuo, honored physician', 'Hua Tuo, Honored Physician').
card_type('hua tuo, honored physician', 'Legendary Creature — Human').
card_types('hua tuo, honored physician', ['Creature']).
card_subtypes('hua tuo, honored physician', ['Human']).
card_supertypes('hua tuo, honored physician', ['Legendary']).
card_colors('hua tuo, honored physician', ['G']).
card_text('hua tuo, honored physician', '{T}: Put target creature card from your graveyard on top of your library. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('hua tuo, honored physician', ['1', 'G', 'G']).
card_cmc('hua tuo, honored physician', 3).
card_layout('hua tuo, honored physician', 'normal').
card_power('hua tuo, honored physician', 1).
card_toughness('hua tuo, honored physician', 2).

% Found in: PTK
card_name('huang zhong, shu general', 'Huang Zhong, Shu General').
card_type('huang zhong, shu general', 'Legendary Creature — Human Soldier').
card_types('huang zhong, shu general', ['Creature']).
card_subtypes('huang zhong, shu general', ['Human', 'Soldier']).
card_supertypes('huang zhong, shu general', ['Legendary']).
card_colors('huang zhong, shu general', ['W']).
card_text('huang zhong, shu general', 'Huang Zhong, Shu General can\'t be blocked by more than one creature.').
card_mana_cost('huang zhong, shu general', ['2', 'W', 'W']).
card_cmc('huang zhong, shu general', 4).
card_layout('huang zhong, shu general', 'normal').
card_power('huang zhong, shu general', 2).
card_toughness('huang zhong, shu general', 3).

% Found in: JOU
card_name('hubris', 'Hubris').
card_type('hubris', 'Instant').
card_types('hubris', ['Instant']).
card_subtypes('hubris', []).
card_colors('hubris', ['U']).
card_text('hubris', 'Return target creature and all Auras attached to it to their owners\' hands.').
card_mana_cost('hubris', ['1', 'U']).
card_cmc('hubris', 2).
card_layout('hubris', 'normal').

% Found in: 6ED, 8ED, BTD, POR, VIS
card_name('hulking cyclops', 'Hulking Cyclops').
card_type('hulking cyclops', 'Creature — Cyclops').
card_types('hulking cyclops', ['Creature']).
card_subtypes('hulking cyclops', ['Cyclops']).
card_colors('hulking cyclops', ['R']).
card_text('hulking cyclops', 'Hulking Cyclops can\'t block.').
card_mana_cost('hulking cyclops', ['3', 'R', 'R']).
card_cmc('hulking cyclops', 5).
card_layout('hulking cyclops', 'normal').
card_power('hulking cyclops', 5).
card_toughness('hulking cyclops', 5).

% Found in: POR, S99, VMA
card_name('hulking goblin', 'Hulking Goblin').
card_type('hulking goblin', 'Creature — Goblin').
card_types('hulking goblin', ['Creature']).
card_subtypes('hulking goblin', ['Goblin']).
card_colors('hulking goblin', ['R']).
card_text('hulking goblin', 'Hulking Goblin can\'t block.').
card_mana_cost('hulking goblin', ['1', 'R']).
card_cmc('hulking goblin', 2).
card_layout('hulking goblin', 'normal').
card_power('hulking goblin', 2).
card_toughness('hulking goblin', 2).

% Found in: S99, UDS
card_name('hulking ogre', 'Hulking Ogre').
card_type('hulking ogre', 'Creature — Ogre').
card_types('hulking ogre', ['Creature']).
card_subtypes('hulking ogre', ['Ogre']).
card_colors('hulking ogre', ['R']).
card_text('hulking ogre', 'Hulking Ogre can\'t block.').
card_mana_cost('hulking ogre', ['2', 'R']).
card_cmc('hulking ogre', 3).
card_layout('hulking ogre', 'normal').
card_power('hulking ogre', 3).
card_toughness('hulking ogre', 3).

% Found in: C13, CMD, HOP, PLS
card_name('hull breach', 'Hull Breach').
card_type('hull breach', 'Sorcery').
card_types('hull breach', ['Sorcery']).
card_subtypes('hull breach', []).
card_colors('hull breach', ['R', 'G']).
card_text('hull breach', 'Choose one —\n• Destroy target artifact.\n• Destroy target enchantment.\n• Destroy target artifact and target enchantment.').
card_mana_cost('hull breach', ['R', 'G']).
card_cmc('hull breach', 2).
card_layout('hull breach', 'normal').

% Found in: MRD
card_name('hum of the radix', 'Hum of the Radix').
card_type('hum of the radix', 'Enchantment').
card_types('hum of the radix', ['Enchantment']).
card_subtypes('hum of the radix', []).
card_colors('hum of the radix', ['G']).
card_text('hum of the radix', 'Each artifact spell costs {1} more to cast for each artifact its controller controls.').
card_mana_cost('hum of the radix', ['2', 'G', 'G']).
card_cmc('hum of the radix', 4).
card_layout('hum of the radix', 'normal').

% Found in: AVR
card_name('human frailty', 'Human Frailty').
card_type('human frailty', 'Instant').
card_types('human frailty', ['Instant']).
card_subtypes('human frailty', []).
card_colors('human frailty', ['B']).
card_text('human frailty', 'Destroy target Human creature.').
card_mana_cost('human frailty', ['B']).
card_cmc('human frailty', 1).
card_layout('human frailty', 'normal').

% Found in: USG
card_name('humble', 'Humble').
card_type('humble', 'Instant').
card_types('humble', ['Instant']).
card_subtypes('humble', []).
card_colors('humble', ['W']).
card_text('humble', 'Until end of turn, target creature loses all abilities and has base power and toughness 0/1.').
card_mana_cost('humble', ['1', 'W']).
card_cmc('humble', 2).
card_layout('humble', 'normal').

% Found in: CHK
card_name('humble budoka', 'Humble Budoka').
card_type('humble budoka', 'Creature — Human Monk').
card_types('humble budoka', ['Creature']).
card_subtypes('humble budoka', ['Human', 'Monk']).
card_colors('humble budoka', ['G']).
card_text('humble budoka', 'Shroud (This creature can\'t be the target of spells or abilities.)').
card_mana_cost('humble budoka', ['1', 'G']).
card_cmc('humble budoka', 2).
card_layout('humble budoka', 'normal').
card_power('humble budoka', 2).
card_toughness('humble budoka', 2).

% Found in: FRF
card_name('humble defector', 'Humble Defector').
card_type('humble defector', 'Creature — Human Rogue').
card_types('humble defector', ['Creature']).
card_subtypes('humble defector', ['Human', 'Rogue']).
card_colors('humble defector', ['R']).
card_text('humble defector', '{T}: Draw two cards. Target opponent gains control of Humble Defector. Activate this ability only during your turn.').
card_mana_cost('humble defector', ['1', 'R']).
card_cmc('humble defector', 2).
card_layout('humble defector', 'normal').
card_power('humble defector', 2).
card_toughness('humble defector', 1).

% Found in: JOU
card_name('humbler of mortals', 'Humbler of Mortals').
card_type('humbler of mortals', 'Enchantment Creature — Elemental').
card_types('humbler of mortals', ['Enchantment', 'Creature']).
card_subtypes('humbler of mortals', ['Elemental']).
card_colors('humbler of mortals', ['G']).
card_text('humbler of mortals', 'Constellation — Whenever Humbler of Mortals or another enchantment enters the battlefield under your control, creatures you control gain trample until end of turn.').
card_mana_cost('humbler of mortals', ['4', 'G', 'G']).
card_cmc('humbler of mortals', 6).
card_layout('humbler of mortals', 'normal').
card_power('humbler of mortals', 5).
card_toughness('humbler of mortals', 5).

% Found in: TMP, TPR
card_name('humility', 'Humility').
card_type('humility', 'Enchantment').
card_types('humility', ['Enchantment']).
card_subtypes('humility', []).
card_colors('humility', ['W']).
card_text('humility', 'All creatures lose all abilities and have base power and toughness 1/1.').
card_mana_cost('humility', ['2', 'W', 'W']).
card_cmc('humility', 4).
card_layout('humility', 'normal').
card_reserved('humility').

% Found in: LEG, ME3
card_name('hunding gjornersen', 'Hunding Gjornersen').
card_type('hunding gjornersen', 'Legendary Creature — Human Warrior').
card_types('hunding gjornersen', ['Creature']).
card_subtypes('hunding gjornersen', ['Human', 'Warrior']).
card_supertypes('hunding gjornersen', ['Legendary']).
card_colors('hunding gjornersen', ['W', 'U']).
card_text('hunding gjornersen', 'Rampage 1 (Whenever this creature becomes blocked, it gets +1/+1 until end of turn for each creature blocking it beyond the first.)').
card_mana_cost('hunding gjornersen', ['3', 'W', 'U', 'U']).
card_cmc('hunding gjornersen', 6).
card_layout('hunding gjornersen', 'normal').
card_power('hunding gjornersen', 5).
card_toughness('hunding gjornersen', 4).

% Found in: THS
card_name('hundred-handed one', 'Hundred-Handed One').
card_type('hundred-handed one', 'Creature — Giant').
card_types('hundred-handed one', ['Creature']).
card_subtypes('hundred-handed one', ['Giant']).
card_colors('hundred-handed one', ['W']).
card_text('hundred-handed one', 'Vigilance\n{3}{W}{W}{W}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)\nAs long as Hundred-Handed One is monstrous, it has reach and can block an additional ninety-nine creatures each combat.').
card_mana_cost('hundred-handed one', ['2', 'W', 'W']).
card_cmc('hundred-handed one', 4).
card_layout('hundred-handed one', 'normal').
card_power('hundred-handed one', 3).
card_toughness('hundred-handed one', 5).

% Found in: CHK
card_name('hundred-talon kami', 'Hundred-Talon Kami').
card_type('hundred-talon kami', 'Creature — Spirit').
card_types('hundred-talon kami', ['Creature']).
card_subtypes('hundred-talon kami', ['Spirit']).
card_colors('hundred-talon kami', ['W']).
card_text('hundred-talon kami', 'Flying\nSoulshift 4 (When this creature dies, you may return target Spirit card with converted mana cost 4 or less from your graveyard to your hand.)').
card_mana_cost('hundred-talon kami', ['4', 'W']).
card_cmc('hundred-talon kami', 5).
card_layout('hundred-talon kami', 'normal').
card_power('hundred-talon kami', 2).
card_toughness('hundred-talon kami', 3).

% Found in: BOK
card_name('hundred-talon strike', 'Hundred-Talon Strike').
card_type('hundred-talon strike', 'Instant — Arcane').
card_types('hundred-talon strike', ['Instant']).
card_subtypes('hundred-talon strike', ['Arcane']).
card_colors('hundred-talon strike', ['W']).
card_text('hundred-talon strike', 'Target creature gets +1/+0 and gains first strike until end of turn.\nSplice onto Arcane—Tap an untapped white creature you control. (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('hundred-talon strike', ['W']).
card_cmc('hundred-talon strike', 1).
card_layout('hundred-talon strike', 'normal').

% Found in: LGN
card_name('hundroog', 'Hundroog').
card_type('hundroog', 'Creature — Beast').
card_types('hundroog', ['Creature']).
card_subtypes('hundroog', ['Beast']).
card_colors('hundroog', ['G']).
card_text('hundroog', 'Cycling {3} ({3}, Discard this card: Draw a card.)').
card_mana_cost('hundroog', ['6', 'G']).
card_cmc('hundroog', 7).
card_layout('hundroog', 'normal').
card_power('hundroog', 4).
card_toughness('hundroog', 7).

% Found in: CNS, DKA
card_name('hunger of the howlpack', 'Hunger of the Howlpack').
card_type('hunger of the howlpack', 'Instant').
card_types('hunger of the howlpack', ['Instant']).
card_subtypes('hunger of the howlpack', []).
card_colors('hunger of the howlpack', ['G']).
card_text('hunger of the howlpack', 'Put a +1/+1 counter on target creature.\nMorbid — Put three +1/+1 counters on that creature instead if a creature died this turn.').
card_mana_cost('hunger of the howlpack', ['G']).
card_cmc('hunger of the howlpack', 1).
card_layout('hunger of the howlpack', 'normal').

% Found in: DST
card_name('hunger of the nim', 'Hunger of the Nim').
card_type('hunger of the nim', 'Sorcery').
card_types('hunger of the nim', ['Sorcery']).
card_subtypes('hunger of the nim', []).
card_colors('hunger of the nim', ['B']).
card_text('hunger of the nim', 'Target creature gets +1/+0 until end of turn for each artifact you control.').
card_mana_cost('hunger of the nim', ['1', 'B']).
card_cmc('hunger of the nim', 2).
card_layout('hunger of the nim', 'normal').

% Found in: FRF
card_name('hungering yeti', 'Hungering Yeti').
card_type('hungering yeti', 'Creature — Yeti').
card_types('hungering yeti', ['Creature']).
card_subtypes('hungering yeti', ['Yeti']).
card_colors('hungering yeti', ['R']).
card_text('hungering yeti', 'As long as you control a green or blue permanent, you may cast Hungering Yeti as though it had flash. (You may cast it any time you could cast an instant.)').
card_mana_cost('hungering yeti', ['4', 'R']).
card_cmc('hungering yeti', 5).
card_layout('hungering yeti', 'normal').
card_power('hungering yeti', 4).
card_toughness('hungering yeti', 4).

% Found in: UGL
card_name('hungry hungry heifer', 'Hungry Hungry Heifer').
card_type('hungry hungry heifer', 'Creature — Cow').
card_types('hungry hungry heifer', ['Creature']).
card_subtypes('hungry hungry heifer', ['Cow']).
card_colors('hungry hungry heifer', ['G']).
card_text('hungry hungry heifer', 'During your upkeep, remove a counter from any card you control or sacrifice Hungry Hungry Heifer.').
card_mana_cost('hungry hungry heifer', ['2', 'G']).
card_cmc('hungry hungry heifer', 3).
card_layout('hungry hungry heifer', 'normal').
card_power('hungry hungry heifer', 3).
card_toughness('hungry hungry heifer', 3).

% Found in: 5ED, HML, MED
card_name('hungry mist', 'Hungry Mist').
card_type('hungry mist', 'Creature — Elemental').
card_types('hungry mist', ['Creature']).
card_subtypes('hungry mist', ['Elemental']).
card_colors('hungry mist', ['G']).
card_text('hungry mist', 'At the beginning of your upkeep, sacrifice Hungry Mist unless you pay {G}{G}.').
card_mana_cost('hungry mist', ['2', 'G', 'G']).
card_cmc('hungry mist', 4).
card_layout('hungry mist', 'normal').
card_power('hungry mist', 6).
card_toughness('hungry mist', 2).

% Found in: SHM
card_name('hungry spriggan', 'Hungry Spriggan').
card_type('hungry spriggan', 'Creature — Goblin Warrior').
card_types('hungry spriggan', ['Creature']).
card_subtypes('hungry spriggan', ['Goblin', 'Warrior']).
card_colors('hungry spriggan', ['G']).
card_text('hungry spriggan', 'Trample\nWhenever Hungry Spriggan attacks, it gets +3/+3 until end of turn.').
card_mana_cost('hungry spriggan', ['2', 'G']).
card_cmc('hungry spriggan', 3).
card_layout('hungry spriggan', 'normal').
card_power('hungry spriggan', 1).
card_toughness('hungry spriggan', 1).

% Found in: LRW
card_name('hunt down', 'Hunt Down').
card_type('hunt down', 'Sorcery').
card_types('hunt down', ['Sorcery']).
card_subtypes('hunt down', []).
card_colors('hunt down', ['G']).
card_text('hunt down', 'Target creature blocks target creature this turn if able.').
card_mana_cost('hunt down', ['G']).
card_cmc('hunt down', 1).
card_layout('hunt down', 'normal').

% Found in: THS
card_name('hunt the hunter', 'Hunt the Hunter').
card_type('hunt the hunter', 'Sorcery').
card_types('hunt the hunter', ['Sorcery']).
card_subtypes('hunt the hunter', []).
card_colors('hunt the hunter', ['G']).
card_text('hunt the hunter', 'Target green creature you control gets +2/+2 until end of turn. It fights target green creature an opponent controls.').
card_mana_cost('hunt the hunter', ['G']).
card_cmc('hunt the hunter', 1).
card_layout('hunt the hunter', 'normal').

% Found in: FRF, M14, M15
card_name('hunt the weak', 'Hunt the Weak').
card_type('hunt the weak', 'Sorcery').
card_types('hunt the weak', ['Sorcery']).
card_subtypes('hunt the weak', []).
card_colors('hunt the weak', ['G']).
card_text('hunt the weak', 'Put a +1/+1 counter on target creature you control. Then that creature fights target creature you don\'t control. (Each deals damage equal to its power to the other.)').
card_mana_cost('hunt the weak', ['3', 'G']).
card_cmc('hunt the weak', 4).
card_layout('hunt the weak', 'normal').

% Found in: RAV
card_name('hunted dragon', 'Hunted Dragon').
card_type('hunted dragon', 'Creature — Dragon').
card_types('hunted dragon', ['Creature']).
card_subtypes('hunted dragon', ['Dragon']).
card_colors('hunted dragon', ['R']).
card_text('hunted dragon', 'Flying, haste\nWhen Hunted Dragon enters the battlefield, target opponent puts three 2/2 white Knight creature tokens with first strike onto the battlefield.').
card_mana_cost('hunted dragon', ['3', 'R', 'R']).
card_cmc('hunted dragon', 5).
card_layout('hunted dragon', 'normal').
card_power('hunted dragon', 6).
card_toughness('hunted dragon', 6).

% Found in: AVR
card_name('hunted ghoul', 'Hunted Ghoul').
card_type('hunted ghoul', 'Creature — Zombie').
card_types('hunted ghoul', ['Creature']).
card_subtypes('hunted ghoul', ['Zombie']).
card_colors('hunted ghoul', ['B']).
card_text('hunted ghoul', 'Hunted Ghoul can\'t block Humans.').
card_mana_cost('hunted ghoul', ['B']).
card_cmc('hunted ghoul', 1).
card_layout('hunted ghoul', 'normal').
card_power('hunted ghoul', 1).
card_toughness('hunted ghoul', 2).

% Found in: RAV
card_name('hunted horror', 'Hunted Horror').
card_type('hunted horror', 'Creature — Horror').
card_types('hunted horror', ['Creature']).
card_subtypes('hunted horror', ['Horror']).
card_colors('hunted horror', ['B']).
card_text('hunted horror', 'Trample\nWhen Hunted Horror enters the battlefield, target opponent puts two 3/3 green Centaur creature tokens with protection from black onto the battlefield.').
card_mana_cost('hunted horror', ['B', 'B']).
card_cmc('hunted horror', 2).
card_layout('hunted horror', 'normal').
card_power('hunted horror', 7).
card_toughness('hunted horror', 7).

% Found in: RAV
card_name('hunted lammasu', 'Hunted Lammasu').
card_type('hunted lammasu', 'Creature — Lammasu').
card_types('hunted lammasu', ['Creature']).
card_subtypes('hunted lammasu', ['Lammasu']).
card_colors('hunted lammasu', ['W']).
card_text('hunted lammasu', 'Flying\nWhen Hunted Lammasu enters the battlefield, target opponent puts a 4/4 black Horror creature token onto the battlefield.').
card_mana_cost('hunted lammasu', ['2', 'W', 'W']).
card_cmc('hunted lammasu', 4).
card_layout('hunted lammasu', 'normal').
card_power('hunted lammasu', 5).
card_toughness('hunted lammasu', 5).

% Found in: RAV
card_name('hunted phantasm', 'Hunted Phantasm').
card_type('hunted phantasm', 'Creature — Spirit').
card_types('hunted phantasm', ['Creature']).
card_subtypes('hunted phantasm', ['Spirit']).
card_colors('hunted phantasm', ['U']).
card_text('hunted phantasm', 'Hunted Phantasm can\'t be blocked.\nWhen Hunted Phantasm enters the battlefield, target opponent puts five 1/1 red Goblin creature tokens onto the battlefield.').
card_mana_cost('hunted phantasm', ['1', 'U', 'U']).
card_cmc('hunted phantasm', 3).
card_layout('hunted phantasm', 'normal').
card_power('hunted phantasm', 4).
card_toughness('hunted phantasm', 6).

% Found in: C13, RAV
card_name('hunted troll', 'Hunted Troll').
card_type('hunted troll', 'Creature — Troll Warrior').
card_types('hunted troll', ['Creature']).
card_subtypes('hunted troll', ['Troll', 'Warrior']).
card_colors('hunted troll', ['G']).
card_text('hunted troll', 'When Hunted Troll enters the battlefield, target opponent puts four 1/1 blue Faerie creature tokens with flying onto the battlefield.\n{G}: Regenerate Hunted Troll.').
card_mana_cost('hunted troll', ['2', 'G', 'G']).
card_cmc('hunted troll', 4).
card_layout('hunted troll', 'normal').
card_power('hunted troll', 8).
card_toughness('hunted troll', 4).

% Found in: 10E, 8ED, 9ED, MMQ
card_name('hunted wumpus', 'Hunted Wumpus').
card_type('hunted wumpus', 'Creature — Beast').
card_types('hunted wumpus', ['Creature']).
card_subtypes('hunted wumpus', ['Beast']).
card_colors('hunted wumpus', ['G']).
card_text('hunted wumpus', 'When Hunted Wumpus enters the battlefield, each other player may put a creature card from his or her hand onto the battlefield.').
card_mana_cost('hunted wumpus', ['3', 'G']).
card_cmc('hunted wumpus', 4).
card_layout('hunted wumpus', 'normal').
card_power('hunted wumpus', 6).
card_toughness('hunted wumpus', 6).

% Found in: LRW
card_name('hunter of eyeblights', 'Hunter of Eyeblights').
card_type('hunter of eyeblights', 'Creature — Elf Assassin').
card_types('hunter of eyeblights', ['Creature']).
card_subtypes('hunter of eyeblights', ['Elf', 'Assassin']).
card_colors('hunter of eyeblights', ['B']).
card_text('hunter of eyeblights', 'When Hunter of Eyeblights enters the battlefield, put a +1/+1 counter on target creature you don\'t control.\n{2}{B}, {T}: Destroy target creature with a counter on it.').
card_mana_cost('hunter of eyeblights', ['3', 'B', 'B']).
card_cmc('hunter of eyeblights', 5).
card_layout('hunter of eyeblights', 'normal').
card_power('hunter of eyeblights', 3).
card_toughness('hunter of eyeblights', 3).

% Found in: LGN
card_name('hunter sliver', 'Hunter Sliver').
card_type('hunter sliver', 'Creature — Sliver').
card_types('hunter sliver', ['Creature']).
card_subtypes('hunter sliver', ['Sliver']).
card_colors('hunter sliver', ['R']).
card_text('hunter sliver', 'All Sliver creatures have provoke. (Whenever a Sliver attacks, its controller may have target creature defending player controls untap and block it if able.)').
card_mana_cost('hunter sliver', ['1', 'R']).
card_cmc('hunter sliver', 2).
card_layout('hunter sliver', 'normal').
card_power('hunter sliver', 1).
card_toughness('hunter sliver', 1).

% Found in: M15
card_name('hunter\'s ambush', 'Hunter\'s Ambush').
card_type('hunter\'s ambush', 'Instant').
card_types('hunter\'s ambush', ['Instant']).
card_subtypes('hunter\'s ambush', []).
card_colors('hunter\'s ambush', ['G']).
card_text('hunter\'s ambush', 'Prevent all combat damage that would be dealt by nongreen creatures this turn.').
card_mana_cost('hunter\'s ambush', ['2', 'G']).
card_cmc('hunter\'s ambush', 3).
card_layout('hunter\'s ambush', 'normal').

% Found in: M12
card_name('hunter\'s insight', 'Hunter\'s Insight').
card_type('hunter\'s insight', 'Instant').
card_types('hunter\'s insight', ['Instant']).
card_subtypes('hunter\'s insight', []).
card_colors('hunter\'s insight', ['G']).
card_text('hunter\'s insight', 'Choose target creature you control. Whenever that creature deals combat damage to a player or planeswalker this turn, draw that many cards.').
card_mana_cost('hunter\'s insight', ['2', 'G']).
card_cmc('hunter\'s insight', 3).
card_layout('hunter\'s insight', 'normal').

% Found in: BNG
card_name('hunter\'s prowess', 'Hunter\'s Prowess').
card_type('hunter\'s prowess', 'Sorcery').
card_types('hunter\'s prowess', ['Sorcery']).
card_subtypes('hunter\'s prowess', []).
card_colors('hunter\'s prowess', ['G']).
card_text('hunter\'s prowess', 'Until end of turn, target creature gets +3/+3 and gains trample and \"Whenever this creature deals combat damage to a player, draw that many cards.\"').
card_mana_cost('hunter\'s prowess', ['4', 'G']).
card_cmc('hunter\'s prowess', 5).
card_layout('hunter\'s prowess', 'normal').

% Found in: M11
card_name('hunters\' feast', 'Hunters\' Feast').
card_type('hunters\' feast', 'Sorcery').
card_types('hunters\' feast', ['Sorcery']).
card_subtypes('hunters\' feast', []).
card_colors('hunters\' feast', ['G']).
card_text('hunters\' feast', 'Any number of target players each gain 6 life.').
card_mana_cost('hunters\' feast', ['3', 'G']).
card_cmc('hunters\' feast', 4).
card_layout('hunters\' feast', 'normal').

% Found in: ME3, PTK
card_name('hunting cheetah', 'Hunting Cheetah').
card_type('hunting cheetah', 'Creature — Cat').
card_types('hunting cheetah', ['Creature']).
card_subtypes('hunting cheetah', ['Cat']).
card_colors('hunting cheetah', ['G']).
card_text('hunting cheetah', 'Whenever Hunting Cheetah deals damage to an opponent, you may search your library for a Forest card, reveal that card, put it into your hand, then shuffle your library.').
card_mana_cost('hunting cheetah', ['2', 'G']).
card_cmc('hunting cheetah', 3).
card_layout('hunting cheetah', 'normal').
card_power('hunting cheetah', 2).
card_toughness('hunting cheetah', 3).

% Found in: PLS
card_name('hunting drake', 'Hunting Drake').
card_type('hunting drake', 'Creature — Drake').
card_types('hunting drake', ['Creature']).
card_subtypes('hunting drake', ['Drake']).
card_colors('hunting drake', ['U']).
card_text('hunting drake', 'Flying\nWhen Hunting Drake enters the battlefield, put target red or green creature on top of its owner\'s library.').
card_mana_cost('hunting drake', ['4', 'U']).
card_cmc('hunting drake', 5).
card_layout('hunting drake', 'normal').
card_power('hunting drake', 2).
card_toughness('hunting drake', 2).

% Found in: JUD
card_name('hunting grounds', 'Hunting Grounds').
card_type('hunting grounds', 'Enchantment').
card_types('hunting grounds', ['Enchantment']).
card_subtypes('hunting grounds', []).
card_colors('hunting grounds', ['W', 'G']).
card_text('hunting grounds', 'Threshold — As long as seven or more cards are in your graveyard, Hunting Grounds has \"Whenever an opponent casts a spell, you may put a creature card from your hand onto the battlefield.\"').
card_mana_cost('hunting grounds', ['G', 'W']).
card_cmc('hunting grounds', 2).
card_layout('hunting grounds', 'normal').

% Found in: INV
card_name('hunting kavu', 'Hunting Kavu').
card_type('hunting kavu', 'Creature — Kavu').
card_types('hunting kavu', ['Creature']).
card_subtypes('hunting kavu', ['Kavu']).
card_colors('hunting kavu', ['R', 'G']).
card_text('hunting kavu', '{1}{R}{G}, {T}: Exile Hunting Kavu and target creature without flying that\'s attacking you.').
card_mana_cost('hunting kavu', ['1', 'R', 'G']).
card_cmc('hunting kavu', 3).
card_layout('hunting kavu', 'normal').
card_power('hunting kavu', 2).
card_toughness('hunting kavu', 3).

% Found in: ARC, TSB, UDS
card_name('hunting moa', 'Hunting Moa').
card_type('hunting moa', 'Creature — Bird Beast').
card_types('hunting moa', ['Creature']).
card_subtypes('hunting moa', ['Bird', 'Beast']).
card_colors('hunting moa', ['G']).
card_text('hunting moa', 'Echo {2}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Hunting Moa enters the battlefield or dies, put a +1/+1 counter on target creature.').
card_mana_cost('hunting moa', ['2', 'G']).
card_cmc('hunting moa', 3).
card_layout('hunting moa', 'normal').
card_power('hunting moa', 3).
card_toughness('hunting moa', 2).

% Found in: CMD, SCG
card_name('hunting pack', 'Hunting Pack').
card_type('hunting pack', 'Instant').
card_types('hunting pack', ['Instant']).
card_subtypes('hunting pack', []).
card_colors('hunting pack', ['G']).
card_text('hunting pack', 'Put a 4/4 green Beast creature token onto the battlefield.\nStorm (When you cast this spell, copy it for each spell cast before it this turn.)').
card_mana_cost('hunting pack', ['5', 'G', 'G']).
card_cmc('hunting pack', 7).
card_layout('hunting pack', 'normal').

% Found in: C14, MOR
card_name('hunting triad', 'Hunting Triad').
card_type('hunting triad', 'Tribal Sorcery — Elf').
card_types('hunting triad', ['Tribal', 'Sorcery']).
card_subtypes('hunting triad', ['Elf']).
card_colors('hunting triad', ['G']).
card_text('hunting triad', 'Put three 1/1 green Elf Warrior creature tokens onto the battlefield.\nReinforce 3—{3}{G} ({3}{G}, Discard this card: Put three +1/+1 counters on target creature.)').
card_mana_cost('hunting triad', ['3', 'G']).
card_cmc('hunting triad', 4).
card_layout('hunting triad', 'normal').

% Found in: PLC
card_name('hunting wilds', 'Hunting Wilds').
card_type('hunting wilds', 'Sorcery').
card_types('hunting wilds', ['Sorcery']).
card_subtypes('hunting wilds', []).
card_colors('hunting wilds', ['G']).
card_text('hunting wilds', 'Kicker {3}{G} (You may pay an additional {3}{G} as you cast this spell.)\nSearch your library for up to two Forest cards and put them onto the battlefield tapped. Then shuffle your library.\nIf Hunting Wilds was kicked, untap all Forests put onto the battlefield this way. They become 3/3 green creatures with haste that are still lands.').
card_mana_cost('hunting wilds', ['3', 'G']).
card_cmc('hunting wilds', 4).
card_layout('hunting wilds', 'normal').

% Found in: DKA
card_name('huntmaster of the fells', 'Huntmaster of the Fells').
card_type('huntmaster of the fells', 'Creature — Human Werewolf').
card_types('huntmaster of the fells', ['Creature']).
card_subtypes('huntmaster of the fells', ['Human', 'Werewolf']).
card_colors('huntmaster of the fells', ['R', 'G']).
card_text('huntmaster of the fells', 'Whenever this creature enters the battlefield or transforms into Huntmaster of the Fells, put a 2/2 green Wolf creature token onto the battlefield and you gain 2 life.\nAt the beginning of each upkeep, if no spells were cast last turn, transform Huntmaster of the Fells.').
card_mana_cost('huntmaster of the fells', ['2', 'R', 'G']).
card_cmc('huntmaster of the fells', 4).
card_layout('huntmaster of the fells', 'double-faced').
card_power('huntmaster of the fells', 2).
card_toughness('huntmaster of the fells', 2).
card_sides('huntmaster of the fells', 'ravager of the fells').

% Found in: 10E, 3ED, 4ED, 5ED, ATQ, MM2
card_name('hurkyl\'s recall', 'Hurkyl\'s Recall').
card_type('hurkyl\'s recall', 'Instant').
card_types('hurkyl\'s recall', ['Instant']).
card_subtypes('hurkyl\'s recall', []).
card_colors('hurkyl\'s recall', ['U']).
card_text('hurkyl\'s recall', 'Return all artifacts target player owns to his or her hand.').
card_mana_cost('hurkyl\'s recall', ['1', 'U']).
card_cmc('hurkyl\'s recall', 2).
card_layout('hurkyl\'s recall', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, ME3
card_name('hurloon minotaur', 'Hurloon Minotaur').
card_type('hurloon minotaur', 'Creature — Minotaur').
card_types('hurloon minotaur', ['Creature']).
card_subtypes('hurloon minotaur', ['Minotaur']).
card_colors('hurloon minotaur', ['R']).
card_text('hurloon minotaur', '').
card_mana_cost('hurloon minotaur', ['1', 'R', 'R']).
card_cmc('hurloon minotaur', 3).
card_layout('hurloon minotaur', 'normal').
card_power('hurloon minotaur', 2).
card_toughness('hurloon minotaur', 3).

% Found in: WTH
card_name('hurloon shaman', 'Hurloon Shaman').
card_type('hurloon shaman', 'Creature — Minotaur Shaman').
card_types('hurloon shaman', ['Creature']).
card_subtypes('hurloon shaman', ['Minotaur', 'Shaman']).
card_colors('hurloon shaman', ['R']).
card_text('hurloon shaman', 'When Hurloon Shaman dies, each player sacrifices a land.').
card_mana_cost('hurloon shaman', ['1', 'R', 'R']).
card_cmc('hurloon shaman', 3).
card_layout('hurloon shaman', 'normal').
card_power('hurloon shaman', 2).
card_toughness('hurloon shaman', 3).

% Found in: UGL
card_name('hurloon wrangler', 'Hurloon Wrangler').
card_type('hurloon wrangler', 'Creature — Minotaur').
card_types('hurloon wrangler', ['Creature']).
card_subtypes('hurloon wrangler', ['Minotaur']).
card_colors('hurloon wrangler', ['R']).
card_text('hurloon wrangler', 'Denimwalk (If defending player is wearing any clothing made of denim, this creature is unblockable.)').
card_mana_cost('hurloon wrangler', ['2', 'R']).
card_cmc('hurloon wrangler', 3).
card_layout('hurloon wrangler', 'normal').
card_power('hurloon wrangler', 2).
card_toughness('hurloon wrangler', 2).

% Found in: LRW
card_name('hurly-burly', 'Hurly-Burly').
card_type('hurly-burly', 'Sorcery').
card_types('hurly-burly', ['Sorcery']).
card_subtypes('hurly-burly', []).
card_colors('hurly-burly', ['R']).
card_text('hurly-burly', 'Choose one —\n• Hurly-Burly deals 1 damage to each creature without flying.\n• Hurly-Burly deals 1 damage to each creature with flying.').
card_mana_cost('hurly-burly', ['1', 'R']).
card_cmc('hurly-burly', 2).
card_layout('hurly-burly', 'normal').

% Found in: 4ED, ARN
card_name('hurr jackal', 'Hurr Jackal').
card_type('hurr jackal', 'Creature — Hound').
card_types('hurr jackal', ['Creature']).
card_subtypes('hurr jackal', ['Hound']).
card_colors('hurr jackal', ['R']).
card_text('hurr jackal', '{T}: Target creature can\'t be regenerated this turn.').
card_mana_cost('hurr jackal', ['R']).
card_cmc('hurr jackal', 1).
card_layout('hurr jackal', 'normal').
card_power('hurr jackal', 1).
card_toughness('hurr jackal', 1).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, ATH, BRB, CED, CEI, DKM, ICE, LEA, LEB, PO2, POR
card_name('hurricane', 'Hurricane').
card_type('hurricane', 'Sorcery').
card_types('hurricane', ['Sorcery']).
card_subtypes('hurricane', []).
card_colors('hurricane', ['G']).
card_text('hurricane', 'Hurricane deals X damage to each creature with flying and each player.').
card_mana_cost('hurricane', ['X', 'G']).
card_cmc('hurricane', 1).
card_layout('hurricane', 'normal').

% Found in: USG
card_name('hush', 'Hush').
card_type('hush', 'Sorcery').
card_types('hush', ['Sorcery']).
card_subtypes('hush', []).
card_colors('hush', ['G']).
card_text('hush', 'Destroy all enchantments.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('hush', ['3', 'G']).
card_cmc('hush', 4).
card_layout('hush', 'normal').

% Found in: M15
card_name('hushwing gryff', 'Hushwing Gryff').
card_type('hushwing gryff', 'Creature — Hippogriff').
card_types('hushwing gryff', ['Creature']).
card_subtypes('hushwing gryff', ['Hippogriff']).
card_colors('hushwing gryff', ['W']).
card_text('hushwing gryff', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nCreatures entering the battlefield don\'t cause abilities to trigger.').
card_mana_cost('hushwing gryff', ['2', 'W']).
card_cmc('hushwing gryff', 3).
card_layout('hushwing gryff', 'normal').
card_power('hushwing gryff', 2).
card_toughness('hushwing gryff', 1).

% Found in: DDN, RTR
card_name('hussar patrol', 'Hussar Patrol').
card_type('hussar patrol', 'Creature — Human Knight').
card_types('hussar patrol', ['Creature']).
card_subtypes('hussar patrol', ['Human', 'Knight']).
card_colors('hussar patrol', ['W', 'U']).
card_text('hussar patrol', 'Flash (You may cast this spell any time you could cast an instant.)\nVigilance').
card_mana_cost('hussar patrol', ['2', 'W', 'U']).
card_cmc('hussar patrol', 4).
card_layout('hussar patrol', 'normal').
card_power('hussar patrol', 2).
card_toughness('hussar patrol', 4).

% Found in: ICE, MED
card_name('hyalopterous lemure', 'Hyalopterous Lemure').
card_type('hyalopterous lemure', 'Creature — Spirit').
card_types('hyalopterous lemure', ['Creature']).
card_subtypes('hyalopterous lemure', ['Spirit']).
card_colors('hyalopterous lemure', ['B']).
card_text('hyalopterous lemure', '{0}: Hyalopterous Lemure gets -1/-0 and gains flying until end of turn.').
card_mana_cost('hyalopterous lemure', ['4', 'B']).
card_cmc('hyalopterous lemure', 5).
card_layout('hyalopterous lemure', 'normal').
card_power('hyalopterous lemure', 4).
card_toughness('hyalopterous lemure', 3).

% Found in: CPK, JOU
card_name('hydra broodmaster', 'Hydra Broodmaster').
card_type('hydra broodmaster', 'Creature — Hydra').
card_types('hydra broodmaster', ['Creature']).
card_subtypes('hydra broodmaster', ['Hydra']).
card_colors('hydra broodmaster', ['G']).
card_text('hydra broodmaster', '{X}{X}{G}: Monstrosity X. (If this creature isn\'t monstrous, put X +1/+1 counters on it and it becomes monstrous.)\nWhen Hydra Broodmaster becomes monstrous, put X X/X green Hydra creature tokens onto the battlefield.').
card_mana_cost('hydra broodmaster', ['4', 'G', 'G']).
card_cmc('hydra broodmaster', 6).
card_layout('hydra broodmaster', 'normal').
card_power('hydra broodmaster', 7).
card_toughness('hydra broodmaster', 7).

% Found in: CMD, CNS
card_name('hydra omnivore', 'Hydra Omnivore').
card_type('hydra omnivore', 'Creature — Hydra').
card_types('hydra omnivore', ['Creature']).
card_subtypes('hydra omnivore', ['Hydra']).
card_colors('hydra omnivore', ['G']).
card_text('hydra omnivore', 'Whenever Hydra Omnivore deals combat damage to an opponent, it deals that much damage to each other opponent.').
card_mana_cost('hydra omnivore', ['4', 'G', 'G']).
card_cmc('hydra omnivore', 6).
card_layout('hydra omnivore', 'normal').
card_power('hydra omnivore', 8).
card_toughness('hydra omnivore', 8).

% Found in: 5ED, ICE, MED
card_name('hydroblast', 'Hydroblast').
card_type('hydroblast', 'Instant').
card_types('hydroblast', ['Instant']).
card_subtypes('hydroblast', []).
card_colors('hydroblast', ['U']).
card_text('hydroblast', 'Choose one —\n• Counter target spell if it\'s red.\n• Destroy target permanent if it\'s red.').
card_mana_cost('hydroblast', ['U']).
card_cmc('hydroblast', 1).
card_layout('hydroblast', 'normal').

% Found in: GTC
card_name('hydroform', 'Hydroform').
card_type('hydroform', 'Instant').
card_types('hydroform', ['Instant']).
card_subtypes('hydroform', []).
card_colors('hydroform', ['U', 'G']).
card_text('hydroform', 'Target land becomes a 3/3 Elemental creature with flying until end of turn. It\'s still a land.').
card_mana_cost('hydroform', ['G', 'U']).
card_cmc('hydroform', 2).
card_layout('hydroform', 'normal').

% Found in: ORI
card_name('hydrolash', 'Hydrolash').
card_type('hydrolash', 'Instant').
card_types('hydrolash', ['Instant']).
card_subtypes('hydrolash', []).
card_colors('hydrolash', ['U']).
card_text('hydrolash', 'Attacking creatures get -2/-0 until end of turn.\nDraw a card.').
card_mana_cost('hydrolash', ['2', 'U']).
card_cmc('hydrolash', 3).
card_layout('hydrolash', 'normal').

% Found in: TOR
card_name('hydromorph guardian', 'Hydromorph Guardian').
card_type('hydromorph guardian', 'Creature — Elemental').
card_types('hydromorph guardian', ['Creature']).
card_subtypes('hydromorph guardian', ['Elemental']).
card_colors('hydromorph guardian', ['U']).
card_text('hydromorph guardian', '{U}, Sacrifice Hydromorph Guardian: Counter target spell that targets one or more creatures you control.').
card_mana_cost('hydromorph guardian', ['2', 'U']).
card_cmc('hydromorph guardian', 3).
card_layout('hydromorph guardian', 'normal').
card_power('hydromorph guardian', 2).
card_toughness('hydromorph guardian', 2).

% Found in: TOR
card_name('hydromorph gull', 'Hydromorph Gull').
card_type('hydromorph gull', 'Creature — Elemental Bird').
card_types('hydromorph gull', ['Creature']).
card_subtypes('hydromorph gull', ['Elemental', 'Bird']).
card_colors('hydromorph gull', ['U']).
card_text('hydromorph gull', 'Flying\n{U}, Sacrifice Hydromorph Gull: Counter target spell that targets one or more creatures you control.').
card_mana_cost('hydromorph gull', ['3', 'U', 'U']).
card_cmc('hydromorph gull', 5).
card_layout('hydromorph gull', 'normal').
card_power('hydromorph gull', 3).
card_toughness('hydromorph gull', 3).

% Found in: M13, M15
card_name('hydrosurge', 'Hydrosurge').
card_type('hydrosurge', 'Instant').
card_types('hydrosurge', ['Instant']).
card_subtypes('hydrosurge', []).
card_colors('hydrosurge', ['U']).
card_text('hydrosurge', 'Target creature gets -5/-0 until end of turn.').
card_mana_cost('hydrosurge', ['U']).
card_cmc('hydrosurge', 1).
card_layout('hydrosurge', 'normal').

% Found in: PC2, ROE
card_name('hyena umbra', 'Hyena Umbra').
card_type('hyena umbra', 'Enchantment — Aura').
card_types('hyena umbra', ['Enchantment']).
card_subtypes('hyena umbra', ['Aura']).
card_colors('hyena umbra', ['W']).
card_text('hyena umbra', 'Enchant creature\nEnchanted creature gets +1/+1 and has first strike.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_mana_cost('hyena umbra', ['W']).
card_cmc('hyena umbra', 1).
card_layout('hyena umbra', 'normal').

% Found in: ICE, MED
card_name('hymn of rebirth', 'Hymn of Rebirth').
card_type('hymn of rebirth', 'Sorcery').
card_types('hymn of rebirth', ['Sorcery']).
card_subtypes('hymn of rebirth', []).
card_colors('hymn of rebirth', ['W', 'G']).
card_text('hymn of rebirth', 'Put target creature card from a graveyard onto the battlefield under your control.').
card_mana_cost('hymn of rebirth', ['3', 'G', 'W']).
card_cmc('hymn of rebirth', 5).
card_layout('hymn of rebirth', 'normal').

% Found in: ATH, FEM, MED, V13, VMA
card_name('hymn to tourach', 'Hymn to Tourach').
card_type('hymn to tourach', 'Sorcery').
card_types('hymn to tourach', ['Sorcery']).
card_subtypes('hymn to tourach', []).
card_colors('hymn to tourach', ['B']).
card_text('hymn to tourach', 'Target player discards two cards at random.').
card_mana_cost('hymn to tourach', ['B', 'B']).
card_cmc('hymn to tourach', 2).
card_layout('hymn to tourach', 'normal').

% Found in: TSP
card_name('hypergenesis', 'Hypergenesis').
card_type('hypergenesis', 'Sorcery').
card_types('hypergenesis', ['Sorcery']).
card_subtypes('hypergenesis', []).
card_colors('hypergenesis', ['G']).
card_text('hypergenesis', 'Suspend 3—{1}{G}{G} (Rather than cast this card from your hand, pay {1}{G}{G} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)\nStarting with you, each player may put an artifact, creature, enchantment, or land card from his or her hand onto the battlefield. Repeat this process until no one puts a card onto the battlefield.').
card_layout('hypergenesis', 'normal').

% Found in: LEG
card_name('hyperion blacksmith', 'Hyperion Blacksmith').
card_type('hyperion blacksmith', 'Creature — Human Artificer').
card_types('hyperion blacksmith', ['Creature']).
card_subtypes('hyperion blacksmith', ['Human', 'Artificer']).
card_colors('hyperion blacksmith', ['R']).
card_text('hyperion blacksmith', '{T}: You may tap or untap target artifact an opponent controls.').
card_mana_cost('hyperion blacksmith', ['1', 'R', 'R']).
card_cmc('hyperion blacksmith', 3).
card_layout('hyperion blacksmith', 'normal').
card_power('hyperion blacksmith', 2).
card_toughness('hyperion blacksmith', 2).

% Found in: RTR, pPRE
card_name('hypersonic dragon', 'Hypersonic Dragon').
card_type('hypersonic dragon', 'Creature — Dragon').
card_types('hypersonic dragon', ['Creature']).
card_subtypes('hypersonic dragon', ['Dragon']).
card_colors('hypersonic dragon', ['U', 'R']).
card_text('hypersonic dragon', 'Flying, haste\nYou may cast sorcery cards as though they had flash. (You may cast them any time you could cast an instant.)').
card_mana_cost('hypersonic dragon', ['3', 'U', 'R']).
card_cmc('hypersonic dragon', 5).
card_layout('hypersonic dragon', 'normal').
card_power('hypersonic dragon', 4).
card_toughness('hypersonic dragon', 4).

% Found in: GPT
card_name('hypervolt grasp', 'Hypervolt Grasp').
card_type('hypervolt grasp', 'Enchantment — Aura').
card_types('hypervolt grasp', ['Enchantment']).
card_subtypes('hypervolt grasp', ['Aura']).
card_colors('hypervolt grasp', ['R']).
card_text('hypervolt grasp', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals 1 damage to target creature or player.\"\n{1}{U}: Return Hypervolt Grasp to its owner\'s hand.').
card_mana_cost('hypervolt grasp', ['2', 'R']).
card_cmc('hypervolt grasp', 3).
card_layout('hypervolt grasp', 'normal').

% Found in: DDM, INV
card_name('hypnotic cloud', 'Hypnotic Cloud').
card_type('hypnotic cloud', 'Sorcery').
card_types('hypnotic cloud', ['Sorcery']).
card_subtypes('hypnotic cloud', []).
card_colors('hypnotic cloud', ['B']).
card_text('hypnotic cloud', 'Kicker {4} (You may pay an additional {4} as you cast this spell.)\nTarget player discards a card. If Hypnotic Cloud was kicked, that player discards three cards instead.').
card_mana_cost('hypnotic cloud', ['1', 'B']).
card_cmc('hypnotic cloud', 2).
card_layout('hypnotic cloud', 'normal').

% Found in: JOU
card_name('hypnotic siren', 'Hypnotic Siren').
card_type('hypnotic siren', 'Enchantment Creature — Siren').
card_types('hypnotic siren', ['Enchantment', 'Creature']).
card_subtypes('hypnotic siren', ['Siren']).
card_colors('hypnotic siren', ['U']).
card_text('hypnotic siren', 'Bestow {5}{U}{U} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nFlying\nYou control enchanted creature.\nEnchanted creature gets +1/+1 and has flying.').
card_mana_cost('hypnotic siren', ['U']).
card_cmc('hypnotic siren', 1).
card_layout('hypnotic siren', 'normal').
card_power('hypnotic siren', 1).
card_toughness('hypnotic siren', 1).

% Found in: 10E, 2ED, 3ED, 4ED, 9ED, ATH, CED, CEI, LEA, LEB, M10, pMPR
card_name('hypnotic specter', 'Hypnotic Specter').
card_type('hypnotic specter', 'Creature — Specter').
card_types('hypnotic specter', ['Creature']).
card_subtypes('hypnotic specter', ['Specter']).
card_colors('hypnotic specter', ['B']).
card_text('hypnotic specter', 'Flying\nWhenever Hypnotic Specter deals damage to an opponent, that player discards a card at random.').
card_mana_cost('hypnotic specter', ['1', 'B', 'B']).
card_cmc('hypnotic specter', 3).
card_layout('hypnotic specter', 'normal').
card_power('hypnotic specter', 2).
card_toughness('hypnotic specter', 2).

% Found in: TOR
card_name('hypnox', 'Hypnox').
card_type('hypnox', 'Creature — Nightmare Horror').
card_types('hypnox', ['Creature']).
card_subtypes('hypnox', ['Nightmare', 'Horror']).
card_colors('hypnox', ['B']).
card_text('hypnox', 'Flying\nWhen Hypnox enters the battlefield, if you cast it from your hand, exile all cards from target opponent\'s hand.\nWhen Hypnox leaves the battlefield, return the exiled cards to their owner\'s hand.').
card_mana_cost('hypnox', ['8', 'B', 'B', 'B']).
card_cmc('hypnox', 11).
card_layout('hypnox', 'normal').
card_power('hypnox', 8).
card_toughness('hypnox', 8).

% Found in: TOR
card_name('hypochondria', 'Hypochondria').
card_type('hypochondria', 'Enchantment').
card_types('hypochondria', ['Enchantment']).
card_subtypes('hypochondria', []).
card_colors('hypochondria', ['W']).
card_text('hypochondria', '{W}, Discard a card: Prevent the next 3 damage that would be dealt to target creature or player this turn.\n{W}, Sacrifice Hypochondria: Prevent the next 3 damage that would be dealt to target creature or player this turn.').
card_mana_cost('hypochondria', ['1', 'W']).
card_cmc('hypochondria', 2).
card_layout('hypochondria', 'normal').

% Found in: ISD
card_name('hysterical blindness', 'Hysterical Blindness').
card_type('hysterical blindness', 'Instant').
card_types('hysterical blindness', ['Instant']).
card_subtypes('hysterical blindness', []).
card_colors('hysterical blindness', ['U']).
card_text('hysterical blindness', 'Creatures your opponents control get -4/-0 until end of turn.').
card_mana_cost('hysterical blindness', ['2', 'U']).
card_cmc('hysterical blindness', 3).
card_layout('hysterical blindness', 'normal').

% Found in: ONS
card_name('hystrodon', 'Hystrodon').
card_type('hystrodon', 'Creature — Beast').
card_types('hystrodon', ['Creature']).
card_subtypes('hystrodon', ['Beast']).
card_colors('hystrodon', ['G']).
card_text('hystrodon', 'Trample\nWhenever Hystrodon deals combat damage to a player, you may draw a card.\nMorph {1}{G}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('hystrodon', ['4', 'G']).
card_cmc('hystrodon', 5).
card_layout('hystrodon', 'normal').
card_power('hystrodon', 3).
card_toughness('hystrodon', 4).

% Found in: THS
card_name('hythonia the cruel', 'Hythonia the Cruel').
card_type('hythonia the cruel', 'Legendary Creature — Gorgon').
card_types('hythonia the cruel', ['Creature']).
card_subtypes('hythonia the cruel', ['Gorgon']).
card_supertypes('hythonia the cruel', ['Legendary']).
card_colors('hythonia the cruel', ['B']).
card_text('hythonia the cruel', 'Deathtouch\n{6}{B}{B}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)\nWhen Hythonia the Cruel becomes monstrous, destroy all non-Gorgon creatures.').
card_mana_cost('hythonia the cruel', ['4', 'B', 'B']).
card_cmc('hythonia the cruel', 6).
card_layout('hythonia the cruel', 'normal').
card_power('hythonia the cruel', 4).
card_toughness('hythonia the cruel', 6).

