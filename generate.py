#!/usr/bin/env python3
# generate.py
# Generate Prolog files from MtG JSON source.

import json
import os
import pprint
import re
import string
import sys

VERSION = "0.14"

PREDICATES = ["set/1", "set_name/2", "set_release_date/2", "set_type/2",
              "set_block/2", "set_border/2",
              "card/1", "card_in_set/2", "card_name/2", "card_type/2",
              "card_types/2", "card_subtypes/2", "card_colors/2",
              "card_text/2", "card_original_text/2", "card_original_type/2",
              "card_border/2", "card_layout/2", "card_number/2",
              "card_mana_cost/2", "card_cmc/2", "card_uid/2",
              "card_image_name/2", "card_power/2", "card_toughness/2",
              "card_flavor_text/2", "card_multiverse_id/2", "card_artist/2",
              "card_timeshifted/1", "card_reserved/1", "card_rarity/2",
              "card_sides/2", "card_loyalty/2", "card_supertypes/2",
              "card_watermark/2", "card_first_print/2",
              "card_ruling/3"]

COLORS = {
    'Blue': 'U',
    'Black': 'B',
    'White': 'W',
    'Red': 'R',
    'Green': 'G'
}

re_manacost = re.compile("\{(\S+?)\}")

# list of attributes found in the JSON files that apply to cards regardless of
# their version or set.
# NOTE: for collect_card_data() to "see" an attribute, it needs to be listed
# here!
CARD_ATTRS = ["name", "type", "types", "subtypes", "colors", "layout",
              "text", "manaCost", "cmc", "power", "toughness", "rulings",
              "reserved", "names", "loyalty", "supertypes"]
# attributes that may differ per version:
# originalText, originalType, artist, number, border, timeshifted, rarity
# surprisingly, imageName (not unique; same imageName used for split cards)

# create dictionary for card-only data with 27 buckets
cards = {c: {} for c in string.ascii_lowercase + "0"}

def quote_atom(s):
    # weird backslashes are used on Unhinged card (Magical Hacker)
    s = s.replace("\\|", "\\\\|")
    s = s.replace("\\r", "\\\\r")
    s = s.replace("\\/", "\\\\/")
    # apostrophes/newlines need to be quoted inside Prolog atoms
    s = s.replace("\n", "\\n")
    s = s.replace('"', '\\"') # not strictly necessary
    s = s.replace("'", "\\'")
    return "'" + s + "'"
q = quote_atom

re_number = re.compile("^[0-9.]+$")
def is_number(s):
    return bool(re_number.search(s))

def find_json_files(srcdir):
    return [os.path.join(srcdir, fn) for fn in os.listdir(srcdir) 
            if fn.endswith("-x.json")]

def patch(data):
    """ Patch any inconsistencies/irregularities found in mtgjson (which were
        probably caused by Gatherer, by the way)... """
    # PATCH for mtgjson 2.22.0
    for card in data['cards']:
        if card['name'] == "Will-O'-The-Wisp":
            card['name'] = "Will-o'-the-Wisp"
        # restore CHAOS -> {C}, broken (?) in 2.22.6
        if card.get('text'): 
            card['text'] = card['text'].replace('CHAOS', '{C}')

def load_card_data(srcdir):
    """ Load all card data into a list of dictionaries (one dict for each
        set). The list consists of tuples (key, data) where key is
        (releaseDate, set_code); this way, we can order the list easily by
        released date, which in turn is useful for determining in which set a
        card appeared first. """
    all_cards = []
    files = find_json_files(srcdir)
    for filename in files:
        with open(filename) as f:
            print("Loading: {0}...".format(filename))
            data = json.load(f)
            patch(data)
            key = (data['releaseDate'], data['name'])
            all_cards.append((key, data))
    all_cards.sort()
    return all_cards

def gen_set_files(all_cards):
    seen = {}
    for (release_date, set_code), data in all_cards:
        gen_set_file(data, seen)

def gen_set_file(data, seen):
    out_filename = os.path.join("sets", data['code'] + ".pl")
    set_atom = "'{0}'".format(data['code'])
    print("Writing: {0}...".format(out_filename))
    with open(out_filename, 'w') as g:
        g.write('% {0}\n\n'.format(data['name']))
        g.write("set({0}).\n".format(set_atom))
        g.write("set_name({0}, {1}).\n".format(set_atom, q(data['name'])))
        g.write("set_release_date({0}, {1}).\n".format(set_atom, q(data['releaseDate'])))
        g.write("set_border({0}, {1}).\n".format(set_atom, q(data['border'])))
        g.write("set_type({0}, {1}).\n".format(set_atom, q(data['type'])))
        if data.get('block', None):
            g.write("set_block({0}, {1}).\n".format(set_atom, q(data['block'])))

        for card in data['cards']:
            g.write("\n")
            try:
                write_card_data(g, card, set_atom, seen)
            except:
                import traceback; traceback.print_exc()
                print("Set: " + set_atom)
                pprint.pprint(card)
                sys.exit(1)

def collect_card_data(card, set_atom):
    """ Get the card attributes listed in CARD_ATTRS and add them to the cards
        dictionary, grouped by first character of the name. """
    # NOTE: cards like Fire/Ice will show up twice in cards-f.pl, etc (both
    # as 'fireice' but different card name and properties.
    DONT_CHECK = [] #["reserved"]

    card_id = card['name'].lower()
    key = card_id[0] if card_id[0] in string.ascii_lowercase else "0"
    d = cards[key].setdefault(card_id, {})
    if d:
        # sanity check -- make sure other versions of the card do indeed
        # have the same values for the given attributes
        for attr in CARD_ATTRS:
            # don't bother checking Un-sets, they're hopeless
            if set_atom[1:-1] in ['UGL']: break
            if attr in DONT_CHECK: break
            try:
                value = card[attr]
            except KeyError:
                pass
            else:
                if d[attr] != value:
                    print("Sanity check FAIL!")
                    print("Set: " + set_atom)
                    pprint.pprint(card)
                    print("Attribute '{0}': {1} != {2}".format(attr, d[attr],
                        value))
                    sys.exit(1)
        d['_sets'].add(set_atom[1:-1])
    else:
        d['id'] = card_id
        d['_sets'] = set(); d['_sets'].add(set_atom[1:-1]) 
        for attr in CARD_ATTRS:
            try:
                d[attr] = card[attr]
            except KeyError:
                pass

def write_card_data(out, card, set_atom, seen):
    """ Write card data for a given set. This only applies to the card attributes
        that may differ per set/version. We also store set info here. """
    # get version-agnostic data for this card and store it
    collect_card_data(card, set_atom)

    card_id = q(card['name'].lower())
    out.write("card_in_set({0}, {1}).\n".format(card_id, set_atom))
    # originalType differs per set, sometimes very much (think Interrupts)
    out.write("card_original_type({0}/{1}, {2}).\n".format(card_id, set_atom,
        q(card.get('originalType', card['type']))))

    orig_text = card.get('originalText', "")
    orig_text = orig_text.replace("\n", "\\n")
    out.write("card_original_text({0}/{1}, {2}).\n".format(card_id, set_atom,
        q(orig_text)))

    if card['name'].lower() not in seen:
        out.write("card_first_print({0}, {1}).\n".format(card_id, set_atom))
        seen[card['name'].lower()] = True

    # if 'border' isn't specified for a certain card, the set's border color
    # is assumed.
    if card.get('border', None):
        out.write("card_border({0}/{1}, {2}).\n".format(card_id, set_atom,
            q(card['border'])))

    out.write("card_image_name({0}/{1}, {2}).\n".format(card_id, set_atom,
        q(card['imageName'])))

    uid = "{0}:{1}:{2}".format(set_atom[1:-1], card['name'], card['imageName'])
    out.write("card_uid({0}/{1}, {2}).\n".format(card_id, set_atom, q(uid)))

    # rarity differs per set
    out.write("card_rarity({0}/{1}, {2}).\n".format(card_id, set_atom, 
        q(card['rarity'])))

    out.write("card_artist({0}/{1}, {2}).\n".format(card_id, set_atom,
        q(card['artist']))) 
    # may have multiple solutions in case of multiple versions

    if card.get('number', None):
        out.write("card_number({0}/{1}, {2}).\n".format(card_id, set_atom,
            q(card['number'])))

    if card.get('flavor', None):
        out.write("card_flavor_text({0}/{1}, {2}).\n".format(card_id, set_atom,
            q(card['flavor'])))

    if card.get('multiverseid', None):
        out.write("card_multiverse_id({0}/{1}, {2}).\n".format(card_id, set_atom,
            q(str(card['multiverseid']))))

    # some cards count as timeshifted in PLC/FUT; when they appear later in
    # other sets, they are not considered timeshifted there
    if card.get('timeshifted', None):
        out.write("card_timeshifted({0}/{1})\n.".format(card_id, set_atom))

    if card.get('watermark', None):
        out.write("card_watermark({0}/{1}, {2}).\n".format(card_id, set_atom,
            q(card['watermark'])))

def gen_card_files():
    for c in sorted(cards.keys()):
        filename = os.path.join("cards", "cards-{0}.pl".format(c))
        print(filename)
        with open(filename, 'w') as out:
            out.write("% Card-specific data\n\n")
            for card_id, card in sorted(cards[c].items(), key=lambda x: x[0]):
                try:
                    write_card(out, card)
                except:
                    import traceback; traceback.print_exc()
                    pprint.pprint(card)
                    sys.exit(1)

# XXX remove duplication with gen_card_files()
def gen_rulings_files():
    for c in sorted(cards.keys()):
        filename = os.path.join("rulings", "rulings-{0}.pl".format(c))
        print(filename)
        with open(filename, 'w') as out:
            out.write("% Rulings\n\n")
            for card_id, card in sorted(cards[c].items(), key=lambda x: x[0]):
                try:
                    write_rulings(out, card)
                except:
                    import traceback; traceback.print_exc()
                    pprint.pprint(card)
                    sys.exit(1)

def write_card(out, card):
    card_id = card['name'].lower()

    # write a comment indicating which sets the card is found in; this is
    # mostly meant as information for the reader. the database already has
    # this information elsewhere :)
    s = ", ".join(sorted(list(card['_sets'])))
    out.write("% Found in: {0}\n".format(s))

    out.write("card_name({0}, {1}).\n".format(q(card_id), q(card['name'])))

    # type text, e.g. "Creature - Horror"
    t = card['type']
    # correct "Summon" which still shows up in a few special sets
    if t.startswith("Summon "): t = t.replace("Summon ", "Creature ")
    if t.startswith("Interrupt"): t = t.replace("Interrupt", "Instant")
    out.write("card_type({0}, {1}).\n".format(q(card_id), q(t))) 

    # NOTE: if there are no types/subtypes attributes for a given card, we
    # could consider just not adding the rule, rather than adding it with an
    # empty list.

    # oracle types should be the same for all versions
    types = card.get('types', [])
    for idx, value in enumerate(types):
        # correct "Summon" type that still shows up in a few special sets
        if value == "Summon":
            types[idx] = "Creature"
        if value == "Interrupt":
            types[idx] = "Instant"
    types_list = ", ".join(q(t) for t in types)
    out.write("card_types({0}, [{1}]).\n".format(q(card_id), types_list))

    # same for subtypes
    subtypes_list = ", ".join(q(t) for t in card.get('subtypes', []))
    out.write("card_subtypes({0}, [{1}]).\n".format(q(card_id), subtypes_list))

    # same for supertypes
    if card.get('supertypes', None):
        supertypes_list = ", ".join(q(t) for t in card['supertypes'])
        out.write("card_supertypes({0}, [{1}]).\n".format(q(card_id), supertypes_list))

    # we only use one-letter symbols for colors. 
    # "colorless" doesn't show up in card['colors'] so it doesn't have a symbol.
    # assumption: each color only appears here once.
    # assumption: colors don't change (but what about misprints?)
    # note: not all JSON cards have the "colors" attribute, but in the Prolog
    # code every card should have a card_colors/2 rule.
    colors = ", ".join(q(COLORS[t]) for t in card.get('colors', []))
    out.write("card_colors({0}, [{1}]).\n".format(q(card_id), colors))

    text = card.get('text', "")
    out.write("card_text({0}, {1}).\n".format(q(card_id), q(text)))

    if card.get('manaCost', None):
        cost = card['manaCost']
        symbols = re_manacost.findall(cost)
        s = ", ".join([q(s) for s in symbols])
        out.write("card_mana_cost({0}, [{1}]).\n".format(q(card_id), s))

    if card.get('cmc', None) is not None:
        out.write("card_cmc({0}, {1}).\n".format(q(card_id), card['cmc']))

    # some sets don't have layout attribute (like Unglued); assume normal
    layout = card.get('layout', 'normal')
    out.write("card_layout({0}, {1}).\n".format(q(card_id), q(layout)))

    power = card.get('power', None)
    if power is not None:
        s = power if is_number(power) else q(power)
        if s.startswith("."): s = "0" + s
        out.write("card_power({0}, {1}).\n".format(q(card_id), s))

    toughness = card.get('toughness', None)
    if toughness is not None:
        s = toughness if is_number(toughness) else q(toughness)
        if s.startswith("."): s = "0" + s
        out.write("card_toughness({0}, {1}).\n".format(q(card_id), s))

    if card.get('reserved', None):
        out.write("card_reserved({0}).\n".format(q(card_id)))

    if card['layout'] in ['flip', 'split', 'double-faced']:
        if card['name'] == card['names'][0]:
            other_side_id = card['names'][1].lower()
            out.write("card_sides({0}, {1}).\n".format(q(card_id),
                q(other_side_id)))

    if card.get('loyalty', None) is not None:
        out.write("card_loyalty({0}, {1}).\n".format(q(card_id), card['loyalty']))


    out.write("\n")

def write_rulings(out, card):
    if not card.get('rulings', []): return
    card_id = card['name'].lower()
    for d in card['rulings']:
        out.write("card_ruling({0}, {1}, {2}).\n".format(q(card_id),
          q(d['date']), q(d['text'])))
    out.write("\n")

def write_discontiguous(out):
    """ Write 'discontiguous' predicates so SWI-Prolog doesn't complain so
        much. """
    INDENT = " " * 6
    out.write(":- discontiguous\n")
    s = (",\n" + INDENT).join(PREDICATES)
    out.write(INDENT + s + ".\n\n")

def gen_toplevel(srcdir):
    filenames = find_json_files(srcdir)
    with open("all.pl", 'w') as g:
        g.write('% Import the whole MtG database\n\n')
        write_discontiguous(g)
        for fn in filenames:
            shortname = os.path.basename(fn)[:-7] # remove "-x.json"
            pl_filename = os.path.join("sets", shortname + ".pl")
            g.write(":- include('{0}').\n".format(pl_filename))
        g.write("\n")
        for c in sorted(cards.keys()):
            pl_filename = os.path.join("cards", "cards-"+c+".pl")
            g.write(":- include('{0}').\n".format(pl_filename))
        g.write("\n")
        for c in sorted(cards.keys()):
            ru_filename = os.path.join("rulings", "rulings-"+c+".pl")
            g.write(":- include('{0}').\n".format(ru_filename))
        g.write("\n:- include(rules).\n")

if __name__ == "__main__":

    srcdir = sys.argv[1]
    all_cards = load_card_data(srcdir)
    gen_set_files(all_cards)
    gen_card_files()
    gen_rulings_files()
    gen_toplevel(srcdir)

